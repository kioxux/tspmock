package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ImportPercentageData;
import com.gys.business.service.PercentagePlanNewStrategy;
import com.gys.business.service.PercentagePlanV5Service;
import com.gys.business.service.StoreDataService;
import com.gys.business.service.data.CompadmOutData;
import com.gys.business.service.data.GetStoListOutData;
import com.gys.business.service.data.MessageParams;
import com.gys.business.service.data.StoreInData;
import com.gys.business.service.data.percentageplan.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.feign.OperateService;
import com.gys.util.BigDecimalUtil;
import com.gys.util.CommonUtil;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年11月11日 上午10:50
 */
@Transactional
@Service
public class PercentagePlanV5ServiceImpl implements PercentagePlanV5Service {

    @Autowired
    private TichengSaleplanZMapper tichengSaleplanZMapper;//销售提成方案主表mapper
    @Autowired
    private TichengSaleplanStoMapper tichengSaleplanStoMapper;//销售提成方案门店关系表

    @Autowired
    private TichengSaleplanMMapper tichengSaleplanMMapper;//销售提成-销售提成设置明细mapper

    @Autowired
    private TichengRejectClassMapper tichengRejectClassMapper;//销售提成-剔除分类

    @Autowired
    private TichengRejectProMapper tichengRejectProMapper;//销售提成-剔除商品设置

    @Autowired
    private TichengProplanBasicMapper tichengProplanBasicMapper;//单品提成方案主表mapper

    @Autowired
    private TichengProplanSettingMapper tichengProplanSettingMapper;//单品提成提成设置表

    @Autowired
    private TichengProplanProNMapper tichengProplanProNMapper;//单品提成提成商品设置表

    @Autowired
    private TichengProplanStoNMapper tichengProplanStoNMapper;//单品提成门店关联表
    @Autowired
    private GaiaDepDataMapper gaiaDepDataMapper;

    @Autowired
    private StoreDataService storeDataService;


    @Resource
    private OperateService operateService;

    private static final List<String> symbolList = new ArrayList<>(Arrays.asList("=", ">", ">=", "<", "<="));

    @Override
    public Integer basicInsert(PercentageBasicInData inData) {

        Integer id = 0;

        if (ObjectUtil.isEmpty(inData.getPlanName())) {
            throw new BusinessException("方案名称不能为空");
        }
        if (ObjectUtil.isEmpty(inData.getDateArr()[0])) {
            throw new BusinessException("起始日期不能为空");
        }
        if (ObjectUtil.isEmpty(inData.getDateArr()[1])) {
            throw new BusinessException("结束日期不能为空");
        }
        if (inData.getStoreCodes().length <= 0) {
            throw new BusinessException("适用门店必选");
        }

        inData.setPlanStartDate(inData.getDateArr()[0]);
        inData.setPlanEndDate(inData.getDateArr()[1]);

        PercentagePlanNewStrategy planNewStrategy = null;
        if ("1".equals(inData.getPlanType())) {
            //销售提成维护
            planNewStrategy = new PercentagePlanNewSaleImpl(tichengSaleplanZMapper, tichengSaleplanStoMapper, tichengSaleplanMMapper, tichengRejectClassMapper, tichengRejectProMapper, operateService);
        } else if ("2".equals(inData.getPlanType())) {
            //单品提成维护
            planNewStrategy = new PercentagePlanNewProImpl(tichengSaleplanZMapper, tichengProplanBasicMapper, tichengProplanSettingMapper, tichengProplanProNMapper, tichengProplanStoNMapper, operateService);
        }
        id = planNewStrategy.insert(inData);
        return id;
    }

    @Override
    public PageInfo<PercentageOutData> list(PercentageSearchInData inData, GetLoginOutData userInfo) {

        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        StoreInData storeInData = new StoreInData();
        storeInData.setClientId(userInfo.getClient());

//        if(!checkPermissionLeagal(userInfo, "B") && checkPermissionLeagal(userInfo, "S")){
//            Set<String> storeIds = new HashSet<>();
//            List<GetStoListOutData> outData = this.tichengSaleplanZMapper.getStoListByClientId(storeInData);
//            if (CollectionUtil.isNotEmpty(outData)) {
//                List<GetStoListOutData> stoListOutDataList = outData.stream().filter(x ->
//                        StrUtil.isNotBlank(x.getStoLegalPersonId()) && x.getStoLegalPersonId().equals(userInfo.getUserId())
//                ).collect(Collectors.toList());
//                if(CollectionUtil.isNotEmpty(stoListOutDataList)){
//                    stoListOutDataList.forEach(x->{
//                        storeIds.add(x.getStoCode());
//                    });
//                }
//            }
//            List<String> stos = new ArrayList<>();
//            if(CollectionUtil.isNotEmpty(storeIds)){
//                stos.forEach(x->{
//                    stos.add(x);
//                });
//                inData.setStos(stos);
//            }
//        }
        List<PercentageOutData> list = tichengSaleplanZMapper.selectTichengZList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(list)) {
            pageInfo = new PageInfo(list);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public Map<String, Object> tichengDetail(Long planId, String type, GetLoginOutData userInfo) {
        Map<String, Object> result = new HashMap<>();
        PercentagePlanNewStrategy planNewStrategy = null;
        if ("1".equals(type)) {
            //销售提成维护
            planNewStrategy = new PercentagePlanNewSaleImpl(tichengSaleplanZMapper, tichengSaleplanStoMapper, tichengSaleplanMMapper, tichengRejectClassMapper, tichengRejectProMapper, operateService);
        } else if ("2".equals(type)) {
            //单品提成维护
            planNewStrategy = new PercentagePlanNewProImpl(tichengSaleplanZMapper, tichengProplanBasicMapper, tichengProplanSettingMapper, tichengProplanProNMapper, tichengProplanStoNMapper, operateService);
        }
        Map<String, Object> res = planNewStrategy.tichengDetail(planId, type, userInfo);
        if (res != null) {
            result = res;
        }
        return result;
    }

    @Override
    public void approve(Long planId, String planStatus, String type) {
        PercentagePlanNewStrategy planNewStrategy = null;
        if ("1".equals(type)) {
            //销售提成维护
            planNewStrategy = new PercentagePlanNewSaleImpl(tichengSaleplanZMapper, tichengSaleplanStoMapper, tichengSaleplanMMapper, tichengRejectClassMapper, tichengRejectProMapper, operateService);
        } else if ("2".equals(type)) {
            //单品提成维护
            planNewStrategy = new PercentagePlanNewProImpl(tichengSaleplanZMapper, tichengProplanBasicMapper, tichengProplanSettingMapper, tichengProplanProNMapper, tichengProplanStoNMapper, operateService);
        }
        planNewStrategy.approve(planId, planStatus, type);
    }

    @Override
    public void deletePlan(Long planId, String type) {
        PercentagePlanNewStrategy planNewStrategy = null;
        if ("1".equals(type)) {
            //销售提成维护
            planNewStrategy = new PercentagePlanNewSaleImpl(tichengSaleplanZMapper, tichengSaleplanStoMapper, tichengSaleplanMMapper, tichengRejectClassMapper, tichengRejectProMapper, operateService);
        } else if ("2".equals(type)) {
            //单品提成维护
            planNewStrategy = new PercentagePlanNewProImpl(tichengSaleplanZMapper, tichengProplanBasicMapper, tichengProplanSettingMapper, tichengProplanProNMapper, tichengProplanStoNMapper, operateService);
        }
        planNewStrategy.deletePlan(planId, type);
    }

    @Override
    public PercentageProInData selectProductByClient(String client, String proCode) {
        PercentageProInData proOutData = tichengSaleplanZMapper.selectProductByClient(client, proCode);
        List<StoreOutData> storeOutDataList = storeDataService.getStoreList(client, null);
        for (StoreOutData outData : storeOutDataList) {
            if (ObjectUtil.isNotEmpty(outData.getStoAttribute())) {
                if ("2".equals(outData.getStoAttribute())) {
                    String movPrice = tichengSaleplanZMapper.selectMovPriceByDcCode(client, outData.getStoDcCode(), proCode);
                    if (ObjectUtil.isNotEmpty(movPrice)) {
                        proOutData.setProCostPrice(new BigDecimal(movPrice));
                        proOutData.setGrossProfitAmt(proOutData.getProPrice().subtract(proOutData.getProCostPrice()));
                        if (proOutData.getProPrice().compareTo(BigDecimal.ZERO) == 0) {
                            proOutData.setGrossProfitRate("0.00%");
                        } else {
                            proOutData.setGrossProfitRate(proOutData.getGrossProfitAmt().divide(proOutData.getProPrice(), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")) + "%");
                        }
                    }
                    break;
                }
            }
        }
        return proOutData;
    }

    @Override
    public List<PercentageProInData> getImportExcelDetailList(String clientId, List<ImportPercentageData> importInDataList) {
        ImportProInData inData = new ImportProInData();
        inData.setClientId(clientId);
        List<String> proCodes = new ArrayList<>();
        List<PercentageProInData> resultList = new ArrayList<>();
        for (ImportPercentageData data : importInDataList) {
            String proCode = data.getProCode();
            if (ObjectUtil.isNotEmpty(proCode)) {
                proCodes.add(proCode);
            }
        }
        if (proCodes.size() > 0 && proCodes != null) {
            inData.setProCodes(proCodes);
            resultList = tichengSaleplanZMapper.selectProductByProCodes(inData);
            if (resultList.size() > 0 && resultList != null) {
                for (ImportPercentageData data : importInDataList) {
                    String proCode = data.getProCode();
                    boolean f = true;
                    for (PercentageProInData result : resultList) {
                        if (result.getProCode().equals(proCode)) {
                            f = false;
                            break;
                        }
                    }
                    if (f) {
                        throw new BusinessException("商品编码:" + proCode + "信息不存在，请核实");
                    }
                }
                List<StoreOutData> storeOutDataList = storeDataService.getStoreList(clientId, null);
                for (StoreOutData outData : storeOutDataList) {
                    if (ObjectUtil.isNotEmpty(outData.getStoAttribute())) {
                        if ("2".equals(outData.getStoAttribute())) {
                            inData.setDcCode(outData.getStoDcCode());
                            List<Map<String, String>> movPriceList = tichengSaleplanZMapper.selectMovPriceListByDcCode(inData);
                            if (movPriceList.size() > 0 && movPriceList != null) {
                                for (Map<String, String> movPriceMap : movPriceList) {
                                    String proCode = movPriceMap.get("proCode");
                                    String movPrice = "0";
                                    if (movPriceMap.containsKey("movPrice")) {
                                        movPrice = movPriceMap.get("movPrice");
                                    }
                                    for (PercentageProInData result : resultList) {
                                        if (result.getProCode().equals(proCode)) {
                                            result.setProCostPrice(new BigDecimal(movPrice));
//                                            if(result.getProPrice()!=null && BigDecimal.ZERO.compareTo(result.getProPrice())==0){
//                                                System.out.println(result.getProCode());
//                                            }
                                            if(result.getProPrice()!=null){
                                                result.setGrossProfitAmt(result.getProPrice().subtract(result.getProCostPrice()));
                                                if(BigDecimal.ZERO.compareTo(result.getProPrice())==0){
                                                    result.setGrossProfitRate("0.00%");
                                                }else{
                                                    result.setGrossProfitRate(result.getGrossProfitAmt().divide(result.getProPrice(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")) + "%");
                                                }
                                            }else{
                                                result.setGrossProfitRate("0.00%");
                                                result.setGrossProfitAmt(BigDecimal.ZERO);
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            } else {
                throw new BusinessException("未查询到对应的商品信息");
            }
            for (PercentageProInData result : resultList) {
                for (ImportPercentageData data : importInDataList) {
                    String proCode = data.getProCode();
                    if (result.getProCode().equals(proCode)) {
                        if (StringUtils.isNotEmpty(data.getSeleQty())) {
                            result.setSaleQty(data.getSeleQty());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengAmt())) {
                            result.setTichengAmt(data.getTichengAmt());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengRate())) {
                            result.setTichengRate(data.getTichengRate());
                        }
                        if (StringUtils.isNotEmpty(data.getSeleQty2())) {
                            result.setSaleQty2(data.getSeleQty2());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengAmt2())) {
                            result.setTichengAmt2(data.getTichengAmt2());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengRate2())) {
                            result.setTichengRate2(data.getTichengRate2());
                        }
                        if (StringUtils.isNotEmpty(data.getSeleQty3())) {
                            result.setSaleQty3(data.getSeleQty3());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengAmt3())) {
                            result.setTichengAmt3(data.getTichengAmt3());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengRate3())) {
                            result.setTichengRate3(data.getTichengRate3());
                        }
                    }
                }
            }
        }
        return resultList;
    }

    @Override
    public List<Map<String, String>> selectStoList(Long planId, Long type) {
        List<Map<String, String>> result = new ArrayList<>();
        PercentagePlanNewStrategy planNewStrategy = null;
        if ("1".equals(type.toString())) {
            //销售提成维护
            planNewStrategy = new PercentagePlanNewSaleImpl(tichengSaleplanZMapper, tichengSaleplanStoMapper, tichengSaleplanMMapper, tichengRejectClassMapper, tichengRejectProMapper, operateService);
        } else if ("2".equals(type.toString())) {
            //单品提成维护
            planNewStrategy = new PercentagePlanNewProImpl(tichengSaleplanZMapper, tichengProplanBasicMapper, tichengProplanSettingMapper, tichengProplanProNMapper, tichengProplanStoNMapper, operateService);
        }
        result = planNewStrategy.selectStoList(planId, type);
//        if (type.intValue() == 1){
//            result = planMapper.selectSaleStoList(planId);
//        }else if(type.intValue() == 2){
//            result = planMapper.selectProStoList(planId);
//        }
        return result;
    }

    @Override
    public Map<String, Object> tichengDetailCopy(Long planId, Long type) {
        PercentagePlanNewStrategy planNewStrategy = null;
        if ("1".equals(type.toString())) {
            //销售提成维护
            planNewStrategy = new PercentagePlanNewSaleImpl(tichengSaleplanZMapper, tichengSaleplanStoMapper, tichengSaleplanMMapper, tichengRejectClassMapper, tichengRejectProMapper, operateService);
        } else if ("2".equals(type.toString())) {
            //单品提成维护
            planNewStrategy = new PercentagePlanNewProImpl(tichengSaleplanZMapper, tichengProplanBasicMapper, tichengProplanSettingMapper, tichengProplanProNMapper, tichengProplanStoNMapper, operateService);
        }
        return planNewStrategy.tichengDetailCopy(planId, type);
    }

    @Override
    public void stopPlan(Long planId, String planType, String stopType, String type) {
        PercentagePlanNewStrategy planNewStrategy = null;
        if ("1".equals(planType)) {
            //销售提成维护
            planNewStrategy = new PercentagePlanNewSaleImpl(tichengSaleplanZMapper, tichengSaleplanStoMapper, tichengSaleplanMMapper, tichengRejectClassMapper, tichengRejectProMapper, operateService);
        } else if ("2".equals(planType)) {
            //单品提成维护
            planNewStrategy = new PercentagePlanNewProImpl(tichengSaleplanZMapper, tichengProplanBasicMapper, tichengProplanSettingMapper, tichengProplanProNMapper, tichengProplanStoNMapper, operateService);
        }
        planNewStrategy.stopPlan(planId, planType, stopType, type);
    }

    @Override
    public void timerStopPlan() {
        //获取定时停用任务总列表
        List<PercentageOutData> planList = tichengSaleplanZMapper.selectPlanStopList();
        if (!CollectionUtils.isEmpty(planList)) {
            for (PercentageOutData outData : planList) {
                if (!(CommonUtil.getyyyyMMdd().compareTo(outData.getPlanStopDate()) == -1)) {
                    if ("1".equals(outData.getPlanType())) {
                        //变更方案状态为停用
                        tichengSaleplanZMapper.stopSalePlan(outData.getId(), "2", "", "");
                    } else {
                        //变更方案状态为停用
                        tichengProplanBasicMapper.stopProPlan(outData.getId(), "2", "", "");
                    }
                    //消息推送到APP
//                        List<Map<String,String>> result = planMapper.selectSaleStoList(outData.getId().longValue());
//                        String[] stoCodeArr = new String[result.size()];
//                        for (int i = 0; i < result.size();i++) {
//                            stoCodeArr[i] = result.get(i).get("stoCode");
//                        }
//                        outData.setStoreCodes(stoCodeArr);
                    PercentageInData inData = new PercentageInData();
                    inData.setClient(outData.getClient());
                    inData.setStoreCodes(outData.getStoreCodes());
                    List<String> userIds = tichengSaleplanZMapper.selectUserIdList(inData);
                    MessageParams messageParams = new MessageParams();
                    messageParams.setClient(outData.getClient());
                    messageParams.setUserIdList(userIds);
                    messageParams.setContentParmas(new ArrayList<String>() {{
                        add(outData.getPlanName());
                        add(outData.getPlanReason());
                    }});
                    messageParams.setId("MSG00011");
//                    log.info("销售提成消息参数:{}", messageParams.toString());
                    String message = operateService.sendMessageList(messageParams);
//                    log.info("销售提成消息参数:{}", message);
                }
            }
        }
    }

    @Override
    public SaleSettingOutData saleSettingInsert(SaleSettingInData inData, GetLoginOutData userInfo) {

        SaleSettingOutData result = new SaleSettingOutData();

        if (inData.getPlanId() == null || inData.getPlanId() == 0) {
            throw new BusinessException("请传入方案id");
        }
        Integer planId = inData.getPlanId();

        if (StrUtil.isNotBlank(inData.getPlanType()) && !"1".equals(inData.getPlanType())) {
            throw new BusinessException("方案类型不合法");
        }
        TichengSaleplanZ queryVo = new TichengSaleplanZ();
        queryVo.setId(planId);
        TichengSaleplanZ planMainDb = tichengSaleplanZMapper.selectByPrimaryKey(queryVo);
        if (planMainDb == null) {
            throw new BusinessException("查无此方案");
        }
        result.setPlanType(inData.getPlanType());
        //此处将主表中的一些配置项进行补充
        planMainDb.setPlanAmtWay(inData.getPlanAmtWay());
        planMainDb.setPlanRateWay(inData.getPlanRateWay());
        planMainDb.setPlanIfNegative(inData.getPlanIfNegative());
        if (StrUtil.isNotBlank(inData.getPlanRejectDiscountRateSymbol())) {
            //校验合法性，符号只能在指定范围内
            if (!symbolList.contains(inData.getPlanRejectDiscountRateSymbol())) {
                throw new BusinessException("剔除折扣率设置中符号为非法符号");
            }
        }
        if (StrUtil.isNotBlank(inData.getPlanReason())) {
            planMainDb.setPlanReason(inData.getPlanReason());
            planMainDb.setPlanStatus("0");
        }
        planMainDb.setPlanRejectDiscountRate(inData.getPlanRejectDiscountRate());
        planMainDb.setPlanRejectDiscountRateSymbol(inData.getPlanRejectDiscountRateSymbol());
        planMainDb.setPlanRejectPro(inData.getPlanRejectPro());
        tichengSaleplanZMapper.updateByPrimaryKey(planMainDb);

        //处理返回结果
        result.setPlanId(planId);
        result.setPlanAmtWay(planMainDb.getPlanAmtWay());
        result.setPlanRateWay(planMainDb.getPlanRateWay());
        result.setPlanIfNegative(planMainDb.getPlanIfNegative());
        result.setPlanRejectDiscountRate(planMainDb.getPlanRejectDiscountRate());
        result.setPlanRejectDiscountRateSymbol(planMainDb.getPlanRejectDiscountRateSymbol());
        result.setPlanRejectPro(planMainDb.getPlanRejectPro());

        if (ObjectUtil.isNotEmpty(planId)) {
            //新增销售提成明细GAIA_TICHENG_SALEPLAN_M
            tichengSaleplanMMapper.deleteSalePlanByPid(planId);
            if (CollUtil.isNotEmpty(inData.getSaleInDataList())) {
                List<PercentageSaleInData> saleList = new ArrayList<>();
                for (PercentageSaleInData saleItem : inData.getSaleInDataList()) {
                    saleItem.setClientId(userInfo.getClient());
                    saleItem.setPid(planId);
                    if (ObjectUtil.isEmpty(saleItem.getProSaleClass())) {
                        saleItem.setProSaleClass("");
                    }
                    saleList.add(saleItem);
                }
                if (saleList.size() > 0 && saleList != null) {
                    tichengSaleplanMMapper.batchSaleTichengList(saleList);
                }
            }
            //处理销售提成明细返回
            List<PercentageSaleInData> saleInDataList = new ArrayList<>();
            TichengSaleplanM quey = new TichengSaleplanM();
            quey.setPid(Long.parseLong(planId.toString()));
            quey.setDeleteFlag("0");
            List<TichengSaleplanM> tichengSaleplanMDbs = tichengSaleplanMMapper.select(quey);
            if (CollectionUtil.isNotEmpty(tichengSaleplanMDbs)) {
                for (TichengSaleplanM tichengSaleplanM : tichengSaleplanMDbs) {
                    PercentageSaleInData res = new PercentageSaleInData();
                    res.setId(tichengSaleplanM.getId());
                    res.setClientId(tichengSaleplanM.getClient());
                    res.setPid(tichengSaleplanM.getPid().intValue());
                    res.setMinDailySaleAmt(tichengSaleplanM.getMinDailySaleAmt());
                    res.setMaxDailySaleAmt(tichengSaleplanM.getMaxDailySaleAmt());
                    res.setProSaleClass(tichengSaleplanM.getProSaleClass());
                    res.setMinProMll(tichengSaleplanM.getMinProMll() == null ? "0" : tichengSaleplanM.getMinProMll().toPlainString());
                    res.setMaxProMll(tichengSaleplanM.getMaxProMll() == null ? "0" : tichengSaleplanM.getMaxProMll().toPlainString());
                    res.setTichengScale(tichengSaleplanM.getTichengScale() == null ? "0" : tichengSaleplanM.getTichengScale().toPlainString());
                    saleInDataList.add(res);
                }
            }
            result.setSaleInDataList(saleInDataList);


            if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
                //软删除剔除分类 GAIA_TICHENG_REJECT_CLASS
                tichengRejectClassMapper.deleteClassByPid(planId);
                List<PlanRejectClass> classList = new ArrayList<>();
                for (int i = 0; i < inData.getClassArr().length; i++) {
                    String[] c = inData.getClassArr()[i];
                    PlanRejectClass rejectClass = new PlanRejectClass();
                    rejectClass.setPid(planId);
                    rejectClass.setClient(userInfo.getClient());
                    rejectClass.setProBigClass(c[0]);
                    rejectClass.setProMidClass(c[1]);
                    rejectClass.setProClass(c[2]);
                    classList.add(rejectClass);
                }
                if (ObjectUtil.isNotEmpty(classList) && classList.size() > 0) {
                    tichengRejectClassMapper.batchPlanRejectClass(classList);
                }
            }

            //处理删除剔除分类返回结果
            TichengRejectClass queryRejectClass = new TichengRejectClass();
            queryRejectClass.setPid(Long.parseLong(planId.toString()));
            queryRejectClass.setDeleteFlag("0");
            List<TichengRejectClass> tichengRejectClasseDbs = tichengRejectClassMapper.select(queryRejectClass);
            if (CollectionUtil.isNotEmpty(tichengRejectClasseDbs)) {
                if (ObjectUtil.isNotEmpty(tichengRejectClasseDbs) && tichengRejectClasseDbs.size() > 0) {
                    String[][] rejectArr = new String[tichengRejectClasseDbs.size()][3];
                    for (int i = 0; i < tichengRejectClasseDbs.size(); i++) {
                        String[] arr = new String[3];
                        arr[0] = tichengRejectClasseDbs.get(i).getProBigClass();
                        arr[1] = tichengRejectClasseDbs.get(i).getProMidClass();
                        arr[2] = tichengRejectClasseDbs.get(i).getProClass();
                        rejectArr[i] = arr;
                    }
                    result.setClassArr(rejectArr);
                }
            }


            //GAIA_TICHENG_REJECT_PRO
            if (ObjectUtil.isNotEmpty(inData.getRejectProList()) && inData.getRejectProList().size() > 0) {
                tichengRejectProMapper.deleteRejectProByPid(planId);
                List<PercentageProInData> proList = new ArrayList<>();
                for (PercentageProInData proInData : inData.getRejectProList()) {
                    if (ObjectUtil.isNotEmpty(proInData.getProCode())) {
//                            if (ObjectUtil.isEmpty(proInData.getTichengAmt())) {
//                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额未填");
//                            }
                        proInData.setClientId(userInfo.getClient());
                        proInData.setPid(planId);
//                            proInData.setPlanProductWay(inData.getPlanProductWay());
                        proList.add(proInData);
                    }
                }
                if (proList.size() > 0 && proList != null) {
                    tichengRejectProMapper.batchSaleRejectProList(proList);
                }
            }

            //处理剔除商品返回结果
            List<PercentageSalePlanRemoveProData> rejectProList = new ArrayList<>();
            TichengRejectPro queryTichengRejectPro = new TichengRejectPro();
            queryTichengRejectPro.setPid(Long.parseLong(planId.toString()));
            queryTichengRejectPro.setDeleteFlag("0");
            List<TichengRejectPro> tichengRejectProDbs = tichengRejectProMapper.select(queryTichengRejectPro);
            if (CollectionUtil.isNotEmpty(tichengRejectProDbs)) {
                for (TichengRejectPro pro : tichengRejectProDbs) {
                    PercentageSalePlanRemoveProData res = new PercentageSalePlanRemoveProData();
                    res.setId(pro.getId());
                    res.setPid(Integer.parseInt(pro.getPid().toString()));
                    res.setProCode(pro.getProCode());
                    res.setProName(pro.getProName());
                    res.setProSpecs(pro.getProSpecs());
                    res.setProFactoryName(pro.getProFactoryName());
                    res.setProCostPrice(pro.getProCostPrice());
                    res.setProPrice(pro.getProPriceNormal());
                    rejectProList.add(res);
                }
            }
            result.setRejectProList(rejectProList);
        }
        return result;
    }

    @Override
    public ProSettingOutData proSettingInsert(ProSettingInData inData, GetLoginOutData userInfo) {
        ProSettingOutData result = new ProSettingOutData();

        //此处需要新建表，用于处理子方案体系，主表为GAIA_TICHENG_PROPLAN_BASIC
        if (inData.getPlanId() == null || inData.getPlanId() == 0) {
            throw new BusinessException("主方案id不能为空");
        }

        if (StrUtil.isBlank(inData.getCPlanName())) {
            throw new BusinessException("子方案名称不能为空");
        }

        TichengProplanBasic tichengProplanBasic = tichengProplanBasicMapper.selectByPrimaryKey(inData.getPlanId());
        if (tichengProplanBasic == null) {
            throw new BusinessException("查无此数据");
        }

        if (StrUtil.isNotBlank(inData.getPlanReason())) {
            tichengProplanBasic.setPlanReason(inData.getPlanReason());
            tichengProplanBasic.setPlanStatus("0");
            tichengProplanBasicMapper.updateByPrimaryKey(tichengProplanBasic);
        }

        //GAIA_TICHENG_PROPLAN_SETTING
        TichengProplanSetting tichengProplanSetting = new TichengProplanSetting();
        //提成设置表需要区分是否存在id，存在id为更新
        if (inData.getId() == null || inData.getId() == 0L) {
            //新增
            tichengProplanSetting.setClient(userInfo.getClient());
            tichengProplanSetting.setPid(Long.parseLong(inData.getPlanId().toString()));
            tichengProplanSetting.setPlanProductWay(inData.getPlanProductWay());
            tichengProplanSetting.setPliantPercentageType(inData.getPlanPercentageWay());
            tichengProplanSetting.setPlanType("2");
            if (StrUtil.isNotBlank(inData.getPlanRejectDiscountRateSymbol())) {
                //校验合法性，符号只能在指定范围内
                if (!symbolList.contains(inData.getPlanRejectDiscountRateSymbol())) {
                    throw new BusinessException("剔除折扣率设置中符号为非法符号");
                }
            }
            tichengProplanSetting.setCPlanName(inData.getCPlanName());
            tichengProplanSetting.setPlanRejectDiscountRateSymbol(inData.getPlanRejectDiscountRateSymbol());
            tichengProplanSetting.setPlanRejectDiscountRate(inData.getPlanRejectDiscountRate());
            tichengProplanSetting.setPlanIfNegative(inData.getPlanIfNegative());
            tichengProplanSetting.setDeleteFlag("0");
            tichengProplanSetting.setCreater(userInfo.getLoginName());
            tichengProplanSetting.setCreaterId(userInfo.getUserId());
            tichengProplanSetting.setCreateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengProplanSetting.setLastUpdateTime(LocalDateTime.now());
            tichengProplanSettingMapper.insertUseGeneratedKeys(tichengProplanSetting);
            tichengProplanSetting = tichengProplanSetting;
        } else {
            //修改
            TichengProplanSetting query = new TichengProplanSetting();
            query.setId(inData.getId());
            query.setDeleteFlag("0");
            TichengProplanSetting db = tichengProplanSettingMapper.selectOne(query);
            if (db == null) {
                throw new BusinessException("查无此数据");
            }
            db.setPlanProductWay(inData.getPlanProductWay());
            db.setPliantPercentageType(inData.getPlanPercentageWay());
            db.setPlanRejectDiscountRateSymbol(inData.getPlanRejectDiscountRateSymbol());
            db.setPlanRejectDiscountRate(inData.getPlanRejectDiscountRate());
            db.setPlanIfNegative(inData.getPlanIfNegative());
            db.setLastUpdateTime(LocalDateTime.now());
            db.setCPlanName(inData.getCPlanName());
            tichengProplanSettingMapper.updateByPrimaryKey(db);
            tichengProplanSetting = db;
        }

        Integer settingId = Integer.parseInt(tichengProplanSetting.getId().toString());
        //处理返回值
        result.setPlanId(Integer.parseInt(tichengProplanSetting.getPid().toString()));
        result.setId(tichengProplanSetting.getId());
        result.setPlanType(tichengProplanSetting.getPlanType());
        result.setPlanPercentageWay(tichengProplanSetting.getPliantPercentageType());
        result.setPlanProductWay(tichengProplanSetting.getPlanProductWay());
        result.setPlanRejectDiscountRateSymbol(tichengProplanSetting.getPlanRejectDiscountRateSymbol());
        result.setPlanRejectDiscountRate(tichengProplanSetting.getPlanRejectDiscountRate());
        result.setPlanIfNegative(tichengProplanSetting.getPlanIfNegative());
        result.setCPlanName(tichengProplanSetting.getCPlanName());
        //处理单品提成明细列表
        if (CollUtil.isNotEmpty(inData.getProInDataList())) {
            StringBuilder builder = new StringBuilder();
            builder.append("");
            //提成商品保存时需要进行校验，校验存在交叠日期内的提成方案（已审核状态），商品不能重复
            List<Map<String, Object>> inTimeDetailList = tichengProplanBasicMapper.selectProPlanDetailByClientAndTime(userInfo.getClient(), tichengProplanBasic.getPlanStartDate(), tichengProplanBasic.getPlanEndDate());
            if (CollectionUtil.isNotEmpty(inTimeDetailList)) {
                for (Map<String, Object> db : inTimeDetailList) {
                    String pro_code = (String) db.get("PRO_CODE");
                    String pro_name = (String) db.get("PRO_NAME");
                    String planName = (String) db.get("PLAN_NAME");
                    String cPlanName = (String) db.get("C_PLAN_NAME");
                    Long planId = (Long) db.get("ID");
                    //过滤自己，避免修改的时候出现无法保存的情况
                    if (tichengProplanBasic.getId() == planId.intValue()) {
                        continue;
                    }
                    for (PercentageProInData proInData : inData.getProInDataList()) {
                        if (StrUtil.isNotBlank(proInData.getProCode()) && proInData.getProCode().equals(pro_code)) {
                            builder.append(pro_code + "  " + pro_name + "已在" + planName + "中的子方案" + cPlanName + "中设置，不予审核;");
                        }
                    }
                }
                if (StrUtil.isNotBlank(builder.toString())) {
                    throw new BusinessException(builder.toString());
                }
            }


            tichengProplanProNMapper.deleteProPlanByPid(inData.getPlanId(), settingId);
            List<PercentageProInData> proList = new ArrayList<>();
            for (PercentageProInData proInData : inData.getProInDataList()) {
                if (ObjectUtil.isNotEmpty(proInData.getProCode())) {
                    if (StringUtils.isEmpty(proInData.getSaleQty())) {
                        throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量1未填");
                    } else {
                        if (new BigDecimal(proInData.getSaleQty()).compareTo(BigDecimal.ZERO) != 1) {
                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量1应大于0");
                        }
                    }
                    if (ObjectUtil.isNotEmpty(proInData.getTichengAmt()) || ObjectUtil.isNotEmpty(proInData.getTichengRate())) {//第1级别
                        if (new BigDecimal(proInData.getSaleQty()).compareTo(BigDecimal.ZERO) != 1) {
                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量1应大于0");
                        }
                        if (ObjectUtil.isNotEmpty(proInData.getTichengAmt()) && ObjectUtil.isNotEmpty(proInData.getTichengRate())) {
                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额1或提成比例1二选一");
                        }
                        if (StringUtils.isNotEmpty(proInData.getTichengRate()) && ObjectUtil.isEmpty(proInData.getTichengAmt())) {
                            if (new BigDecimal(proInData.getTichengRate()).compareTo(BigDecimal.ZERO) < 1 || new BigDecimal(proInData.getTichengRate()).compareTo(new BigDecimal(100)) == 1) {
                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成比例1应在0%到100%之间");
                            }
                        }
                        if (ObjectUtil.isNotEmpty(proInData.getSaleQty2()) || ObjectUtil.isNotEmpty(proInData.getTichengAmt2()) || ObjectUtil.isNotEmpty(proInData.getTichengRate2())) {
                            if (ObjectUtil.isEmpty(proInData.getSaleQty2())) {
                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量2未填");
                            }
                            if (ObjectUtil.isNotEmpty(proInData.getTichengAmt2()) || ObjectUtil.isNotEmpty(proInData.getTichengRate2())) {
                                if (new BigDecimal(proInData.getSaleQty2()).compareTo(new BigDecimal(proInData.getSaleQty())) != 1) {
                                    throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量2应大于达成数量1");
                                }
                                if (ObjectUtil.isNotEmpty(proInData.getTichengAmt2()) && ObjectUtil.isNotEmpty(proInData.getTichengRate2())) {
                                    throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额2或提成比例2二选一");
                                }
                                if (StringUtils.isNotEmpty(proInData.getTichengRate2()) && ObjectUtil.isEmpty(proInData.getTichengAmt2())) {
                                    if (new BigDecimal(proInData.getTichengRate2()).compareTo(BigDecimal.ZERO) < 1 || new BigDecimal(proInData.getTichengRate2()).compareTo(new BigDecimal(100)) == 1) {
                                        throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成比例2应在0%到100%之间");
                                    }
                                }
                                if (ObjectUtil.isNotEmpty(proInData.getSaleQty3()) || ObjectUtil.isNotEmpty(proInData.getTichengAmt3()) || ObjectUtil.isNotEmpty(proInData.getTichengRate3())) {
                                    if (ObjectUtil.isEmpty(proInData.getSaleQty3())) {
                                        throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量3未填");
                                    }
                                    if (ObjectUtil.isNotEmpty(proInData.getTichengAmt3()) || ObjectUtil.isNotEmpty(proInData.getTichengRate3())) {
                                        if (new BigDecimal(proInData.getSaleQty3()).compareTo(new BigDecimal(proInData.getSaleQty2())) != 1) {
                                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量3应大于达成数量2");
                                        }
                                        if (ObjectUtil.isNotEmpty(proInData.getTichengAmt3()) && ObjectUtil.isNotEmpty(proInData.getTichengRate3())) {
                                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额3或提成比例3二选一");
                                        }
                                        if (StringUtils.isNotEmpty(proInData.getTichengRate3()) && ObjectUtil.isEmpty(proInData.getTichengAmt3())) {
                                            if (new BigDecimal(proInData.getTichengRate3()).compareTo(BigDecimal.ZERO) < 1 || new BigDecimal(proInData.getTichengRate3()).compareTo(new BigDecimal(100)) == 1) {
                                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成比例3应在0%到100%之间");
                                            }
                                        }
                                        proInData.setTichengLevel("3");
                                    } else {
                                        throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额3或提成比例3未填");
                                    }
                                } else {
                                    proInData.setTichengLevel("2");
                                }
                            } else {
                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额2或提成比例2未填");
                            }
                        } else {
                            proInData.setTichengLevel("1");
                        }
                    } else {
                        throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额1或提成比例1未填");
                    }
                    proInData.setClientId(userInfo.getClient());
                    proInData.setPid(inData.getPlanId());
                    proInData.setSettingId(settingId);//补充层级id
                    proInData.setPlanProductWay(inData.getPlanProductWay());
                    proInData.setPlanPercentageWay(inData.getPlanPercentageWay());
                    proInData.setPlanRejectDiscountRate(inData.getPlanRejectDiscountRate());
                    proInData.setPlanRejectDiscountRateSymbol(inData.getPlanRejectDiscountRateSymbol());
                    proInData.setPlanIfNegative(inData.getPlanIfNegative());
                    proList.add(proInData);
                }
            }
            if (proList.size() > 0 && proList != null) {
                try {
                    tichengProplanProNMapper.batchProTichengList(proList);
                } catch (Exception e) {
                    throw new BusinessException("同时期方案下出现同商品设置");
                }

            }

            List<PercentageProInData> proInDataList = new ArrayList<>();
            TichengProplanProN query = new TichengProplanProN();
            query.setPid(Long.parseLong(inData.getPlanId().toString()));
            query.setDeleteFlag("0");
            query.setClient(userInfo.getClient());
            List<TichengProplanProN> tichengProplanProNDbs = tichengProplanProNMapper.select(query);
            if (CollectionUtil.isNotEmpty(tichengProplanProNDbs)) {
                for (TichengProplanProN tichengProplanProN : tichengProplanProNDbs) {
                    PercentageProInData res = new PercentageProInData();
                    res.setId(tichengProplanProN.getId());
                    res.setClientId(tichengProplanProN.getClient());
                    res.setPid(Integer.parseInt(tichengProplanProN.getPid().toString()));
                    res.setProCode(tichengProplanProN.getProCode());
                    res.setProName(tichengProplanProN.getProName());
                    res.setProSpecs(tichengProplanProN.getProSpecs());
                    res.setProFactoryName(tichengProplanProN.getProFactoryName());
                    res.setProCostPrice(tichengProplanProN.getProCostPrice());
                    res.setProPrice(tichengProplanProN.getProPriceNormal());
//                    res.setGrossProfitAmt(tichengProplanProN.getProPriceNormal());
//                    res.setGrossProfitRate();
                    res.setSaleQty(tichengProplanProN.getSaleQty() == null ? null : tichengProplanProN.getSaleQty().toPlainString());
                    res.setTichengAmt(tichengProplanProN.getTichengAmt() == null ? null : tichengProplanProN.getTichengAmt().toPlainString());
                    res.setTichengRate(tichengProplanProN.getTichengRate() == null ? null : tichengProplanProN.getTichengRate().toPlainString());

                    res.setSaleQty2(tichengProplanProN.getSaleQty2() == null ? null : tichengProplanProN.getSaleQty2().toPlainString());
                    res.setTichengAmt2(tichengProplanProN.getTichengAmt2() == null ? null : tichengProplanProN.getTichengAmt2().toPlainString());
                    res.setTichengRate2(tichengProplanProN.getTichengRate2() == null ? null : tichengProplanProN.getTichengRate2().toPlainString());

                    res.setSaleQty3(tichengProplanProN.getSaleQty3() == null ? null : tichengProplanProN.getSaleQty3().toPlainString());
                    res.setTichengAmt3(tichengProplanProN.getTichengAmt3() == null ? null : tichengProplanProN.getTichengAmt3().toPlainString());
                    res.setTichengRate3(tichengProplanProN.getTichengRate3() == null ? null : tichengProplanProN.getTichengRate3().toPlainString());
                    proInDataList.add(res);
                }
            }
            result.setProInDataList(proInDataList);
        }
        return result;
    }

    @Override
    public List<PercentageProInData> getPlanRejectProDetailList(String clientId, List<String> proCodes) {
        ImportProInData inData = new ImportProInData();
        List<PercentageProInData> resultList = new ArrayList<>();
        inData.setClientId(clientId);
        if (CollectionUtil.isEmpty(proCodes)) {
            throw new BusinessException("导入商品信息为空");
        }
        if (CollectionUtil.isNotEmpty(proCodes)) {
            inData.setProCodes(proCodes);
            resultList = tichengSaleplanZMapper.selectProductByProCodes(inData);
            Set<String> illegalProCodeSet = new HashSet<>();
            if (CollectionUtil.isNotEmpty(resultList)) {
                Map<String, String> dbProMap = resultList.stream().collect(Collectors.toMap(PercentageProInData::getProCode, PercentageProInData::getProCode));
                for (String proCode : proCodes) {
                    if (!dbProMap.containsKey(proCode)) {
                        illegalProCodeSet.add(proCode);
                    }
                }
                if (CollectionUtil.isNotEmpty(illegalProCodeSet)) {
                    //表示存在非法编码
                    throw new BusinessException("商品编码:" + StringUtils.join(Arrays.asList(illegalProCodeSet), ";") + "信息不存在，请核实");
                }

                if (CollectionUtil.isNotEmpty(resultList)) {
                    List<PercentageProInData> handleList = new ArrayList<>();
                    for (PercentageProInData proOutData : resultList) {
                        if (proOutData.getProPrice() == null || proOutData.getProPrice().compareTo(BigDecimal.ZERO) == 0) {
                            proOutData.setGrossProfitRate("0.00%");
                            proOutData.setGrossProfitAmt(BigDecimal.ZERO);
                        } else {
                            proOutData.setGrossProfitAmt(proOutData.getProPrice().subtract(BigDecimalUtil.format(proOutData.getProCostPrice(), 2).compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : proOutData.getProCostPrice().setScale(2, RoundingMode.HALF_UP)));
                            proOutData.setGrossProfitRate(proOutData.getGrossProfitAmt().divide(proOutData.getProPrice(), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")) + "%");
                        }
                        handleList.add(proOutData);
                    }
                    resultList = handleList;
                }

//                List<StoreOutData> storeOutDataList = storeDataService.getStoreList(clientId, null);
//                for (StoreOutData outData : storeOutDataList) {
//                    if (ObjectUtil.isNotEmpty(outData.getStoAttribute())) {
//                        if ("2".equals(outData.getStoAttribute())) {
//                            inData.setDcCode(outData.getStoDcCode());
//                            List<Map<String, String>> movPriceList = tichengSaleplanZMapper.selectMovPriceListByDcCode(inData);
//                            if (movPriceList.size() > 0 && movPriceList != null) {
//                                for (Map<String, String> movPriceMap : movPriceList) {
//                                    String proCode = movPriceMap.get("proCode");
//                                    String movPrice = "0";
//                                    if (movPriceMap.containsKey("movPrice")) {
//                                        movPrice = movPriceMap.get("movPrice");
//                                    }
//                                    for (PercentageProInData result : resultList) {
//                                        if (result.getProCode().equals(proCode)) {
//                                            result.setProCostPrice(new BigDecimal(movPrice));
//                                            result.setGrossProfitAmt(result.getProPrice().subtract(result.getProCostPrice()));
//                                            if (result.getProPrice().compareTo(BigDecimal.ZERO) == 0) {
//                                                result.setGrossProfitRate("0.00%");
//                                            } else {
//                                                result.setGrossProfitRate(result.getGrossProfitAmt().divide(result.getProPrice(), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")) + "%");
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                            break;
//                        }
//                    }
//                }
            } else {
                throw new BusinessException("未查询到对应的商品信息");
            }

        }
        return resultList;
    }

    @Override
    public void deleteSetting(Long id, String planType) {
        //先删除明细再删除主表
        tichengProplanSettingMapper.deleteProPlanSettingBySettingId(id);
        tichengProplanProNMapper.deleteProPlanBySettingId(Integer.parseInt(id.toString()));
    }

    @Override
    public boolean checkPermissionLeagal(GetLoginOutData userInfo, String type) {
        //公司维护-公司基础信息-部门中的商采部、运营部、财务部、人事行政部负责人；公司维护-公司基础信息-连锁中的公司负责人）及门店店长（公司维护-公司基础信息-门店中的店长）
        //以上的一些权限具备新增权限
        Map<String, Set<String>> userIdRoleSetMap = new HashMap<>();
        Boolean flag = false;
        //
        Example example = new Example(GaiaDepData.class);
        example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("depStatus", "1");
        example.orderBy("depId").asc();
        List<GaiaDepData> depList = gaiaDepDataMapper.selectByExample(example);
        if (CollectionUtil.isNotEmpty(depList)) {
            List<GaiaDepData> collect = depList.stream().filter(x -> "商采部".equals(x.getDepName()) || "运营部".equals(x.getDepName()) || "人事行政部".equals(x.getDepName())).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(collect)) {
                collect.forEach(x -> {
                    if (!userIdRoleSetMap.containsKey(x.getDepHeadId())) {
                        Set<String> roleCode = new HashSet<>();
                        roleCode.add("B");//代表具备公司维护-公司基础信息-部门中的商采部、运营部、财务部、人事行政部负责人；公司维护-公司基础信息-连锁中的公司负责人）中的权限
                        userIdRoleSetMap.put(x.getDepHeadId(), roleCode);
                    } else {
                        Set<String> roleCode = userIdRoleSetMap.get(x.getDepHeadId());
                        roleCode.add("B");
                        userIdRoleSetMap.put(x.getDepHeadId(), roleCode);
                    }
                });
            }
        }


        List<CompadmOutData> compadm = this.tichengSaleplanZMapper.compadmInfoByClientId(userInfo.getClient(), null);
        if (CollectionUtil.isNotEmpty(compadm)) {
            if (CollectionUtil.isNotEmpty(compadm)) {
                compadm.forEach(x -> {
                    if (!userIdRoleSetMap.containsKey(x.getCompadmLegalPerson())) {
                        Set<String> roleCode = new HashSet<>();
                        roleCode.add("B");//代表具备公司维护-公司基础信息-部门中的商采部、运营部、财务部、人事行政部负责人；公司维护-公司基础信息-连锁中的公司负责人）中的权限
                        userIdRoleSetMap.put(x.getCompadmLegalPerson(), roleCode);
                    } else {
                        Set<String> roleCode = userIdRoleSetMap.get(x.getCompadmLegalPerson());
                        roleCode.add("B");
                        userIdRoleSetMap.put(x.getCompadmLegalPerson(), roleCode);
                    }
                });
            }
        }
        StoreInData inData = new StoreInData();
        inData.setClientId(userInfo.getClient());
        List<GetStoListOutData> outData = this.tichengSaleplanZMapper.getStoListByClientId(inData);
        if (CollectionUtil.isNotEmpty(outData)) {
            if (CollectionUtil.isNotEmpty(outData)) {
                outData.forEach(x -> {
                    if (!userIdRoleSetMap.containsKey(x.getStoLegalPersonId())) {
                        Set<String> roleCode = new HashSet<>();
                        roleCode.add("S");//代表具备公司维护-公司基础信息-部门中的商采部、运营部、财务部、人事行政部负责人；公司维护-公司基础信息-连锁中的公司负责人）中的权限
                        userIdRoleSetMap.put(x.getStoLegalPersonId(), roleCode);
                    } else {
                        Set<String> roleCode = userIdRoleSetMap.get(x.getStoLegalPersonId());
                        roleCode.add("S");
                        userIdRoleSetMap.put(x.getStoLegalPersonId(), roleCode);
                    }
                });
            }
        }

        //处理是否具备权限
        Set<String> strings = userIdRoleSetMap.get(userInfo.getUserId());

        if (CollectionUtil.isNotEmpty(strings)) {
            if (strings.size() == 2) {
                return true;
            } else if (strings.size() == 1) {
                AtomicReference<String> userType = new AtomicReference<>("");
                strings.forEach(x -> {
                    userType.set(x);
                });
                if (userType.get().equals(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void syncData() {
        // 查询需要同步到 GAIA_TICHENG_PROPLAN_BASIC 表的数据
        List<TichengProplanBasic> tichengProplanBasics = tichengProplanBasicMapper.selectSyncData();

        List<Map<String, String>> maps = tichengSaleplanZMapper.selectMaxPlanCodeByClient();
        Map<String, String> temp = new HashMap<>();
        for (Map<String, String> map : maps) {
            temp.put(map.get("client"), map.get("maxPlanCode"));
        }

        int year = LocalDate.now().getYear();
        for (TichengProplanBasic tichengProplanBasic : tichengProplanBasics) {
            String client = tichengProplanBasic.getClient();
            String planCode = temp.get(client);
            if (StringUtils.isBlank(planCode)) {
                // 不存在新增 从 1 开始 ，规则为 CID + 年份 + 00000x  ,such as CID2021000003
                planCode = "CID" + year + "000001";
            } else {
                // 存在，截取自增位数 +1
                String prefix = planCode.substring(0, 7);
                String substring = planCode.substring(7);
                planCode = prefix + String.format("%06d", Integer.parseInt(substring) + 1);
            }
            temp.put(client, planCode);
            tichengProplanBasic.setPlanCode(planCode);
        }
        tichengProplanBasicMapper.insertBatch(tichengProplanBasics);

        // 查询需要同步到 GAIA_TICHENG_PROPLAN_SETTING 表的数据
        List<TichengProplanSetting> tichengProplanSettings = tichengProplanSettingMapper.selectSyncData();
        for (TichengProplanSetting tichengProplanSetting : tichengProplanSettings) {
            tichengProplanSettingMapper.insertUseGeneratedKeys(tichengProplanSetting);
            Long settingId = tichengProplanSetting.getId();
            Long planId = tichengProplanSetting.getPid();
            String planRejectDiscountRateSymbol = tichengProplanSetting.getPlanRejectDiscountRateSymbol();
            String planRejectDiscountRate = tichengProplanSetting.getPlanRejectDiscountRate();
            // 更新 GAIA_TICHENG_PROPLAN_PRO_N 表的 SETTING_ID 字段
            tichengProplanProNMapper.updateSettingIdByPlanId(planId, planRejectDiscountRateSymbol, planRejectDiscountRate, settingId);
        }

        // 更新 GAIA_TICHENG_REJECT_CLASS 表的 CLIENT
        tichengRejectClassMapper.updateClientByPlanId();
    }

    @Override
    public Map<String, Object> basicInit(GetLoginOutData userInfo) {
        Map<String, Object> resultMap = new HashMap<>();
        List<String> saleClassList = tichengSaleplanZMapper.getSaleClass(userInfo.getClient());
        List<String> proPostionList = tichengSaleplanZMapper.getProPosition(userInfo.getClient());
        if (CollectionUtil.isNotEmpty(saleClassList)) {
            saleClassList = saleClassList.stream().filter(x -> StrUtil.isNotBlank(x)).collect(Collectors.toList());
        }
        if (CollectionUtil.isNotEmpty(proPostionList)) {
            proPostionList = proPostionList.stream().filter(x -> StrUtil.isNotBlank(x)).collect(Collectors.toList());
        }
        resultMap.put("saleClassList", saleClassList);
        resultMap.put("proPostionList", proPostionList);
        return resultMap;
    }


    @Override
    public List<PercentageProInData> selectProductByClientProCodes(String client, List<String> proCodes) {
//        Map<String,PercentageProInData> resMap = new HashMap<>();
        List<PercentageProInData> resList = new ArrayList<>();
        if (CollectionUtil.isEmpty(proCodes)) {
            return resList;
        }
//        List<PercentageProInData> resList = new ArrayList<>();
        List<PercentageProInData> proOutDatas = tichengSaleplanZMapper.selectProductByClientProCodes(client, proCodes);
//        List<StoreOutData> storeOutDataList = storeDataService.getStoreList(client,null);
//        Map<String,String> proMovePriceMap = new HashMap<>();
//        for (StoreOutData outData : storeOutDataList) {
//            if (ObjectUtil.isNotEmpty(outData.getStoAttribute())){
//                if ("2".equals(outData.getStoAttribute())){
//                    List<Map<String,Object>> resMapList = tichengSaleplanZMapper.selectMovPriceByDcCodeProCodes(client,outData.getStoDcCode(),proCodes);
//                    if(CollectionUtil.isNotEmpty(resMapList)){
//                        resMapList.forEach(x->{
//                            String proCode = x.get("proCode")==null ? "": x.get("proCode").toString();
//                            BigDecimal movePrice = x.get("movePrice")==null?BigDecimal.ZERO:new BigDecimal(x.get("movePrice").toString());
//                            proMovePriceMap.put(proCode,movePrice.toPlainString());
//                        });
//                    }
//                }
//            }
//        }

        if (CollectionUtil.isNotEmpty(proOutDatas)) {
            for (PercentageProInData proOutData : proOutDatas) {
                if(proOutData.getProPrice()!=null ){
                    if(proOutData.getProCostPrice()!=null){
                        proOutData.setGrossProfitAmt(proOutData.getProPrice().subtract(proOutData.getProCostPrice()).setScale(2, RoundingMode.HALF_UP));
                        proOutData.setGrossProfitRate(proOutData.getGrossProfitAmt().divide(proOutData.getProPrice(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2) + "%");

                    }else{
                        proOutData.setGrossProfitRate("0.00%");
                        proOutData.setGrossProfitAmt(BigDecimal.ZERO);
                    }
                }
                resList.add(proOutData);
            }
        }
        return resList;
    }

}

