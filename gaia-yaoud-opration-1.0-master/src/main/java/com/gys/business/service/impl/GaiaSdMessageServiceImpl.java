package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdMessageMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.mapper.entity.GaiaStoreInSuggestionH;
import com.gys.business.mapper.entity.GaiaStoreOutSuggestionD;
import com.gys.business.mapper.entity.GaiaStoreOutSuggestionH;
import com.gys.business.service.GaiaSdMessageService;
import com.gys.business.service.InventoryInquiryService;
import com.gys.business.service.data.SysParamOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.enums.LoginMessageEnum;
import com.gys.common.enums.SdMessageTypeEnum;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.gys.util.UtilMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * @author xiayuan
 * @date 2020-12-23
 */
@Slf4j
@Service
public class GaiaSdMessageServiceImpl implements GaiaSdMessageService {

    @Autowired
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Autowired
    private InventoryInquiryService inventoryInquiryService;

    @Override
    public List<GaiaSdMessage> getAll(GetLoginOutData userInfo,GaiaSdMessage inData) {
        List<GaiaSdMessage> gaiaSdMessages = this.gaiaSdMessageMapper.selectByExample(userInfo.getClient(), userInfo.getDepId(),inData.getGsmFlag());
        if (CollUtil.isNotEmpty(gaiaSdMessages)) {
            IntStream.range(0, gaiaSdMessages.size()).forEach(i -> (gaiaSdMessages.get(i)).setIndex(i + 1));
        }
        return gaiaSdMessages;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateMessage(GetLoginOutData userInfo, GaiaSdMessage inData) {
        SimpleDateFormat format = new SimpleDateFormat(UtilMessage.HHMMSS);
        inData.setClient(userInfo.getClient());
        inData.setGsmId(userInfo.getDepId());
        inData.setGsmCheckEmp(userInfo.getUserId());
        inData.setGsmCheckDate(DateUtil.format(new Date(), UtilMessage.YYYYMMDD));
//        inData.setGsmCheckTime(format.format(new Date()));
        inData.setGsmCheckTime(CommonUtil.getHHmmss());
        inData.setGsmFlag(UtilMessage.Y);
        this.gaiaSdMessageMapper.updateByPrimaryKeySelective(inData);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void timerUpdateMessage() {
        //清除历史登录提醒消息
        gaiaSdMessageMapper.deleteMessageByClient("WEB");
        //插入新的消息（门店预警、商品实时库存查询）
        this.insertMessage();
        //查询仓库预警参数
        List<SysParamOutData> dcCodeList = gaiaSdMessageMapper.selectDcCodeByClient("");
        if (ObjectUtil.isNotEmpty(dcCodeList) && dcCodeList.size() > 0){
            for (SysParamOutData outData : dcCodeList) {
                //新增门店效期预警
                String voucherId = gaiaSdMessageMapper.selectNextVoucherId(outData.getClientId(),"company");
                String voucherIdFX = gaiaSdMessageMapper.selectNextVoucherIdFX(outData.getClientId(),"company");
                SysParamOutData data = gaiaSdMessageMapper.selectExpiryDayByClient(outData.getClientId(),outData.getDcCode());
                if (ObjectUtil.isNotEmpty(data)){
                    int count = 0;
                    List<Map<String,Object>> stockExpiry  = gaiaSdMessageMapper.selectStockExpiry(data.getClientId(),data.getGsspParam());
                    for (Map<String,Object> map : stockExpiry) {
                        if ("0".equals(map.get("icount").toString())) {
                            String strArr = voucherId.substring(8);
                            String result = String.format("%0" + strArr.length() + "d", Integer.parseInt(strArr) + count);
                            GaiaSdMessage msg = new GaiaSdMessage();
                            msg.setClient(outData.getClientId());
                            msg.setGsmArriveDate(CommonUtil.getyyyyMMdd());
                            msg.setGsmArriveTime(CommonUtil.getHHmmss());
                            msg.setGsmFlag("N");
                            msg.setGsmPage(LoginMessageEnum.CANGKU_XIAOQI_PAGE_URL.getCode());
                            msg.setGsmId(map.get("dcCode").toString());
                            msg.setGsmWarningDay(data.getGsspParam());
                            msg.setGsmType(LoginMessageEnum.CANGKU_XIAOQI_PAGE_CODE.getCode());
//                            msg.setGsmTypeName(LoginMessageEnum.CANGKU_XIAOQI_PAGE_CODE.getName());
                            msg.setGsmVoucherId("GSMC" + CommonUtil.getyyyyMMdd().substring(0, 4) + result);
//                            msg.setGsmValue("days=" + data.getGsspParam());
                            msg.setGsmPlatForm("WEB");
                            msg.setGsmRemark("仓库有<font color=\"#FF0000\">"+map.get("icount").toString()+"</font>条批次药品库存低于安全库存，请及时处理。");
                            gaiaSdMessageMapper.insertSelective(msg);
                            String strArrFX = voucherIdFX.substring(9);
                            String resultFX = String.format("%0" + strArrFX.length() + "d", Integer.parseInt(strArrFX) + count);
                            GaiaSdMessage msgFX = new GaiaSdMessage();
                            msgFX.setClient(outData.getClientId());
                            msgFX.setGsmArriveDate(CommonUtil.getyyyyMMdd());
                            msgFX.setGsmArriveTime(CommonUtil.getHHmmss());
                            msgFX.setGsmFlag("N");
                            msgFX.setGsmPage(LoginMessageEnum.CANGKU_XIAOQI_PAGE_URL.getCode());
                            msgFX.setGsmId(map.get("dcCode").toString());
                            msgFX.setGsmWarningDay(data.getGsspParam());
                            msgFX.setGsmType(LoginMessageEnum.CANGKU_XIAOQI_PAGE_CODE.getCode());
//                            msg.setGsmTypeName(LoginMessageEnum.CANGKU_XIAOQI_PAGE_CODE.getName());
                            msgFX.setGsmVoucherId("GSMFC" + CommonUtil.getyyyyMMdd().substring(0, 4) + resultFX);
//                            msgFX.setGsmValue("days=" + data.getGsspParam());
                            msgFX.setGsmPlatForm("FX");
                            msgFX.setGsmRemark("仓库有"+map.get("icount").toString()+"条批次药品库存低于安全库存，请及时处理。");
                            gaiaSdMessageMapper.insertSelective(msgFX);
                            count++;
                        }
                    }
                }
            }
        }
    }

    @Transactional
    @Override
    public void addMessage(GaiaSdMessage gaiaSdMessage) {
        try {
            gaiaSdMessageMapper.insertSelective(gaiaSdMessage);
        } catch (Exception e) {
            throw new BusinessException("新增消息异常", e);
        }
    }

    @Transactional
    @Override
    public void sendStoreSuggestionMessage(SdMessageTypeEnum messageTypeEnum, List<GaiaStoreInSuggestionH> inSuggestionHList, GaiaStoreInSuggestionH inSuggestionH) {
        try {
            switch (messageTypeEnum) {
                case STORE_OUT_SUGGESTION_CONFIRM:
                    transformStoreOutSuggestionMessage(messageTypeEnum, inSuggestionHList);
                    break;
                case STORE_IN_SUGGESTION_CONFIRM:
                    transformStoreInSuggestionMessage(messageTypeEnum, inSuggestionH);
                default:
                    log.error("<消息服务><门店调剂><未匹配的消息类型:{}>", messageTypeEnum.code);
            }
        } catch (Exception e) {
            log.error("<消息服务><门店调剂><产生消息异常：{}>", e.getMessage(), e);
        }
    }

    private void addStoreMessage(GaiaSdMessage gaiaSdMessage) {
        if (StringUtil.isEmpty(gaiaSdMessage.getGsmVoucherId())&&"WEB".equals(gaiaSdMessage.getGsmPlatForm())) {
            String voucherId = gaiaSdMessageMapper.selectNextVoucherId(gaiaSdMessage.getClient(), "store");
            gaiaSdMessage.setGsmVoucherId(voucherId);
        }
        else if (StringUtil.isEmpty(gaiaSdMessage.getGsmVoucherId())&&"FX".equals(gaiaSdMessage.getGsmPlatForm())) {
            String voucherId = gaiaSdMessageMapper.selectNextVoucherIdFX(gaiaSdMessage.getClient(), "store");
            gaiaSdMessage.setGsmVoucherId(voucherId);
        }
        addMessage(gaiaSdMessage);
    }

    private void transformStoreOutSuggestionMessage(SdMessageTypeEnum messageTypeEnum, List<GaiaStoreInSuggestionH> inSuggestionHList) {
        for (GaiaStoreInSuggestionH inSuggestionH : inSuggestionHList) {
            GaiaSdMessage webMessage = new GaiaSdMessage();
            webMessage.setClient(inSuggestionH.getClient());
            webMessage.setGsmId(inSuggestionH.getStoCode());
            webMessage.setGsmType(messageTypeEnum.code);
            webMessage.setGsmPage(messageTypeEnum.page);
            webMessage.setGsmRemark(String.format("您有门店调剂建议（%s）需确认，请于%s前确认！", inSuggestionH.getOutStoName(), com.gys.util.DateUtil.dateToString(inSuggestionH.getInvalidDate(), "MM月dd日")));
            webMessage.setGsmFlag("N");
            webMessage.setGsmWarningDay(inSuggestionH.getOutBillCode());
            webMessage.setGsmPlatForm("WEB");
            webMessage.setGsmDeleteFlag("0");
            webMessage.setGsmArriveDate(CommonUtil.getyyyyMMdd());
            webMessage.setGsmArriveTime(CommonUtil.getHHmmss());
            addStoreMessage(webMessage);
            GaiaSdMessage fxMessage = new GaiaSdMessage();
            BeanUtils.copyProperties(webMessage, fxMessage);
            fxMessage.setGsmVoucherId(null);
            fxMessage.setGsmPlatForm("FX");
            addStoreMessage(fxMessage);
        }
    }

    private void transformStoreInSuggestionMessage(SdMessageTypeEnum messageTypeEnum, GaiaStoreInSuggestionH inSuggestionH) {
        GaiaSdMessage webMessage = new GaiaSdMessage();
        webMessage.setClient(inSuggestionH.getClient());
        webMessage.setGsmId(inSuggestionH.getOutStoCode());
        webMessage.setGsmType(messageTypeEnum.code);
        webMessage.setGsmPage(messageTypeEnum.page);
        webMessage.setGsmRemark(String.format("您有门店调剂建议反馈（%s）需确认，请于%s前确认！", inSuggestionH.getStoName(), com.gys.util.DateUtil.dateToString(inSuggestionH.getInvalidDate(), "MM月dd日")));
        webMessage.setGsmFlag("N");
        webMessage.setGsmWarningDay(inSuggestionH.getOutBillCode());
        webMessage.setGsmPlatForm("WEB");
        webMessage.setGsmDeleteFlag("0");
        webMessage.setGsmArriveDate(CommonUtil.getyyyyMMdd());
        webMessage.setGsmArriveTime(CommonUtil.getHHmmss());
        addStoreMessage(webMessage);
        GaiaSdMessage fxMessage = new GaiaSdMessage();
        BeanUtils.copyProperties(webMessage, fxMessage);
        fxMessage.setGsmPlatForm("FX");
        fxMessage.setGsmVoucherId(null);
        addStoreMessage(fxMessage);
    }

    private void insertMessage(){
        List<SysParamOutData> expiryListPro = gaiaSdMessageMapper.selectExpiryListPro();
        for (SysParamOutData outData : expiryListPro) {
            //商品实时库存消息添加
            String voucherId = gaiaSdMessageMapper.selectNextVoucherId(outData.getClientId(),"all");
            String voucherIdfx = gaiaSdMessageMapper.selectNextVoucherIdFX(outData.getClientId(),"all");
            List<Map<String,Object>> proList = gaiaSdMessageMapper.selectProExpiry(outData.getClientId(),outData.getGsspParam());
            int count = 0;
            for (Map<String,Object> pro : proList) {
                if(!"0".equals(pro.get("icount").toString())) {
                    String strArr = voucherId.substring(8);
                    String result = String.format("%0" + strArr.length() + "d", Integer.parseInt(strArr) + count);
                    GaiaSdMessage msg = new GaiaSdMessage();
                    msg.setClient(outData.getClientId());
                    msg.setGsmArriveDate(CommonUtil.getyyyyMMdd());
                    msg.setGsmArriveTime(CommonUtil.getHHmmss());
                    msg.setGsmFlag("N");
                    msg.setGsmPage(LoginMessageEnum.XIAOQI_PAGE_PRO_URL.getCode());
                    msg.setGsmId(pro.get("site").toString());
                    msg.setGsmWarningDay(outData.getGsspParam());
                    msg.setGsmType(LoginMessageEnum.XIAOQI_PAGE_PRO_CODE.getCode());
//                    msg.setGsmTypeName(LoginMessageEnum.XIAOQI_PAGE_PRO_CODE.getName());
                    msg.setGsmVoucherId("GSMA" + CommonUtil.getyyyyMMdd().substring(0, 4) + result);
                    msg.setGsmValue("days=" + outData.getGsspParam());
                    msg.setGsmPlatForm("WEB");
                    msg.setGsmDeleteFlag("0");
                    msg.setGsmRemark("截至目前，公司共有<font color=\"#FF0000\">"+pro.get("icount").toString()+"</font>条近效期的商品已不足" + outData.getGsspParam() + "天，请尽快处理。");
                    gaiaSdMessageMapper.insertSelective(msg);
                    //fx消息提醒信息添加
                    String strArrFX = voucherIdfx.substring(9);
                    String resultFX = String.format("%0" + strArrFX.length() + "d", Integer.parseInt(strArrFX) + count);
                    GaiaSdMessage msgFX = new GaiaSdMessage();
                    msgFX.setClient(outData.getClientId());
                    msgFX.setGsmArriveDate(CommonUtil.getyyyyMMdd());
                    msgFX.setGsmArriveTime(CommonUtil.getHHmmss());
                    msgFX.setGsmFlag("N");
                    msgFX.setGsmPage(LoginMessageEnum.XIAOQI_PAGE_PRO_URL.getCode());
                    msgFX.setGsmId(pro.get("site").toString());
                    msgFX.setGsmWarningDay(outData.getGsspParam());
                    msgFX.setGsmType(LoginMessageEnum.XIAOQI_PAGE_PRO_CODE.getCode());
//                    msg.setGsmTypeName(LoginMessageEnum.XIAOQI_PAGE_PRO_CODE.getName());
                    msgFX.setGsmVoucherId("GSMFA" + CommonUtil.getyyyyMMdd().substring(0, 4) + resultFX);
                    msgFX.setGsmValue("days=" + outData.getGsspParam());
                    msgFX.setGsmPlatForm("FX");
                    msgFX.setGsmDeleteFlag("0");
                    msgFX.setGsmRemark("截至目前，公司共有"+pro.get("icount").toString()+"条近效期的商品已不足" + outData.getGsspParam() + "天，请尽快处理。");
                    gaiaSdMessageMapper.insertSelective(msgFX);
                    count++;
                }
            }
            //新增门店效期预警
            String voucherId1 = gaiaSdMessageMapper.selectNextVoucherId(outData.getClientId(),"store");
            String voucherId1fx = gaiaSdMessageMapper.selectNextVoucherIdFX(outData.getClientId(),"store");
            List<Map<String,Object>> storeExpiryList = gaiaSdMessageMapper.selectStoreExpiryCount(outData.getClientId(),outData.getGsspParam());
            int count1 = 0;
            List<GaiaSdMessage> stoMessage = new ArrayList<>();
            List<GaiaSdMessage> stoMessage1 = new ArrayList<>();
            for (Map<String,Object> sto : storeExpiryList) {
                if(!"0".equals(sto.get("icount").toString())) {
                    String strArr = voucherId1.substring(8);
                    String result = String.format("%0" + strArr.length() + "d", Integer.parseInt(strArr) + count1);
                    GaiaSdMessage msg = new GaiaSdMessage();
                    msg.setClient(outData.getClientId());
                    msg.setGsmArriveDate(CommonUtil.getyyyyMMdd());
                    msg.setGsmArriveTime(CommonUtil.getHHmmss());
                    msg.setGsmFlag("N");
                    msg.setGsmWarningDay(outData.getGsspParam());
                    msg.setGsmPage(LoginMessageEnum.XIAOQI_PAGE_URL.getCode());
                    msg.setGsmId(sto.get("stoCode").toString());
                    msg.setGsmType(LoginMessageEnum.XIAOQI_PAGE_CODE.getCode());
                    msg.setGsmVoucherId("GSMS" + CommonUtil.getyyyyMMdd().substring(0, 4) + result);
                    msg.setGsmValue("days=" + outData.getGsspParam());
                    msg.setGsmPlatForm("WEB");
                    msg.setGsmDeleteFlag("0");
                    String content = sto.get("stoName").toString() + "有<font color=\"#FF0000\">" +sto.get("icount").toString()+ "</font>条近效期的商品已不足"+outData.getGsspParam()+"天，";
                    if (!"0".equals(sto.get("icount2").toString())){
                        content += "有<font color=\"#FF0000\">"+sto.get("icount2").toString()+"</font>条商品已不足30天";
                    }
//                    if (!"0".equals(sto.get("icount3").toString())){
//                        content += "，有<font color=\"#FF0000\">" + sto.get("icount3").toString() + "</font>条已过有效期";
//                    }
                    content += "，请尽快处理。";
                    msg.setGsmRemark(content);
                    stoMessage.add(msg);

                    String strArrFX = voucherId1fx.substring(9);
                    String resultFX = String.format("%0" + strArrFX.length() + "d", Integer.parseInt(strArrFX) + count1);
                    GaiaSdMessage msgFX = new GaiaSdMessage();
                    msgFX.setClient(outData.getClientId());
                    msgFX.setGsmArriveDate(CommonUtil.getyyyyMMdd());
                    msgFX.setGsmArriveTime(CommonUtil.getHHmmss());
                    msgFX.setGsmFlag("N");
                    msgFX.setGsmWarningDay(outData.getGsspParam());
                    msgFX.setGsmPage(LoginMessageEnum.XIAOQI_PAGE_URL.getCode());
                    msgFX.setGsmId(sto.get("stoCode").toString());
                    msgFX.setGsmType(LoginMessageEnum.XIAOQI_PAGE_CODE.getCode());
                    msgFX.setGsmVoucherId("GSMFS" + CommonUtil.getyyyyMMdd().substring(0, 4) + resultFX);
                    msgFX.setGsmValue("days=" + outData.getGsspParam());
                    msgFX.setGsmPlatForm("FX");
                    msgFX.setGsmDeleteFlag("0");
                    String contentFX = sto.get("stoName").toString() + "有" +sto.get("icount").toString()+ "条近效期的商品已不足"+outData.getGsspParam()+"天，";
                    if (!"0".equals(sto.get("icount2").toString())){
                        contentFX += "有"+sto.get("icount2").toString()+"条商品已不足30天";
                    }
//                    if (!"0".equals(sto.get("icount3").toString())){
//                        contentFX += "有" + sto.get("icount3").toString() + "条已过有效期";
//                    }
                    contentFX += "，请尽快处理。";
                    msgFX.setGsmRemark(contentFX);
                    stoMessage1.add(msgFX);
//                gaiaSdMessageMapper.insertSelective(msg);
                    count1++;
                }
            }
            if (ObjectUtil.isNotEmpty(stoMessage) && stoMessage.size() > 0){
                if (ObjectUtil.isNotEmpty(stoMessage1) && stoMessage1.size() > 0){
                    stoMessage.addAll(stoMessage1);
                }
                gaiaSdMessageMapper.batchMessageSto(stoMessage);
            }
        }
    }

}
