package com.gys.business.service.takeAway.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.mapper.takeAway.GaiaYlCooperationDao;
import com.gys.business.service.StockService;
import com.gys.business.service.data.StoreOutData;
import com.gys.business.service.data.*;
import com.gys.business.service.data.takeaway.*;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.*;
import com.gys.common.data.yaolian.YlOrderInputBean;
import com.gys.common.data.yaolian.YlStockData;
import com.gys.common.data.yaolian.YlStockInputBean;
import com.gys.common.data.yaolian.common.RequestHead;
import com.gys.common.data.yaolian.common.YlResponseData;
import com.gys.common.enums.O2OorderStatusEnum;
import com.gys.common.exception.BusinessException;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import com.gys.util.UtilConst;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.XbServiceUtil;
import com.gys.util.takeAway.ddky.DdkyApiUtil;
import com.gys.util.takeAway.ddky.DdkyRefundBean;
import com.gys.util.takeAway.ddky.DdkyStockUpdateBean;
import com.gys.util.takeAway.ddky.DdkyStockUpdateDetailBean;
import com.gys.util.takeAway.eb.EbApiUtil;
import com.gys.util.takeAway.jd.JdApiUtil;
import com.gys.util.takeAway.mt.MtApiUtil;
import com.gys.util.takeAway.yl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.gys.util.takeAway.yl.YlApiUtil.getTimeStamp;

@Service
public class TakeAwayServiceImpl implements TakeAwayService {
    private static final Logger logger = LoggerFactory.getLogger(TakeAwayServiceImpl.class);
    @Resource
    private GaiaTAccountMapper gaiaTAccountMapper;
    @Resource
    private GaiaTOrderInfoMapper gaiaTOrderInfoMapper;
    @Resource
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Resource
    private RabbitTemplateHelper rabbitTemplate;
    @Resource
    private GaiaSdStockMapper sdStockMapper;
    @Resource
    private BaseService baseService;
    @Autowired
    private StockService stockService;
    @Resource
    private GaiaComputerInformationMapper computerInformationMapper;
    @Resource
    private GaiaSdSaleHMapper sdSaleHMapper;
    @Resource
    private GaiaStoreDataMapper storeDataMapper;
    @Resource
    private GaiaProductBusinessMapper productBusinessMapper;
    @Resource
    private GaiaSdStoreDataMapper gaiaSdStoreDataMapper;
    @Resource
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;
    @Resource
    private GaiaYlCooperationDao gaiaYlCooperationDao;

    @Override
    public GaiaTAccount getEbPlatformAccount(String platform, String appId, String shopId) {
        Example example = new Example(GaiaTAccount.class);
        example.createCriteria().andEqualTo("platform", platform).andEqualTo("appId", appId).andEqualTo("appStoId", shopId);
        return this.gaiaTAccountMapper.selectOneByExample(example);
    }

    @Override
    public GaiaTAccount getMtPlatformAccount(String platform, String appId, String shopId) {
        Example example = new Example(GaiaTAccount.class);
        example.createCriteria().andEqualTo("platform", platform).andEqualTo("appId", appId).andEqualTo("publicCode", shopId);
        return this.gaiaTAccountMapper.selectOneByExample(example);
    }

    @Override
    public List<GaiaTAccount> getAllAccountByAppId(String platform, String appId) {
        Example example = new Example(GaiaTAccount.class);
        example.createCriteria().andEqualTo("platform", platform).andEqualTo("appId", appId);
        return this.gaiaTAccountMapper.selectByExample(example);
    }

    @Override
    public List<GaiaTAccount> getAllAccountByClientAndStoCode(String platform, String client, String stoCode) {
        Example example = new Example(GaiaTAccount.class);
        example.createCriteria().andEqualTo("platform", platform).andEqualTo("stoCode", stoCode).andEqualTo("client", client);
        return this.gaiaTAccountMapper.selectByExample(example);
    }

    @Override
    public GaiaTAccount getPlatformAccountByStoCode(String platform, String appId, String stoCode, String client) {
        Example example = new Example(GaiaTAccount.class);
        example.createCriteria().andEqualTo("platform", platform)
                .andEqualTo("appId", appId)
                .andEqualTo("stoCode", stoCode)
                .andEqualTo("client", client);
        return this.gaiaTAccountMapper.selectOneByExample(example);
    }


    @Override
    public GaiaTOrderInfo selectStoCodeByPlatformsAndPlatformsOrderId(String platform, String platformsOrderId) {
        GaiaTOrderInfo gaiaTOrderInfo = gaiaTOrderInfoMapper.selectByPlatformsAndPlatformsOrderId(platformsOrderId, platform);
        return gaiaTOrderInfo;
    }

    @Override
    public String getJdSecret(String appKey) {
        return gaiaTAccountMapper.getJdSecret(appKey);
    }

    @Override
    public GaiaTAccount selectStoCodeByPlatformsAndPublicCodeAndAppId(String platforms, String publicCode, String appId) {
        return gaiaTAccountMapper.selectStoCodeByPlatformsAndPublicCodeAndAppId(platforms, publicCode, appId);
    }

    @Override
    public GaiaTAccount getAccountByClientAndStoCodeAndPlat(String client, String stoCode, String platform) {
        Example example = new Example(GaiaTAccount.class);
        example.createCriteria().andEqualTo("platform", platform).andEqualTo("client", client).andEqualTo("stoCode", stoCode);
        return this.gaiaTAccountMapper.selectOneByExample(example);
    }

    /**
     * 韦德定制化产物
     * 需绑定平台对应的外卖包装袋；如检查到订单中有处方药需绑定处方费
     * 饿了么外卖袋编码为：80029，如无库存，则不计入订单与销售单
     * 处方费编码为：9999，如无库存，则不计入订单与销售单
     */
    @Override
    public List<WeideProIdDto> getWeideEbProId(String client, String stoCode, boolean isPrescibe, String orderId) {
        if (!"10000072".equals(client)) {
            return null;
        }
        List<String> list = new ArrayList<>(isPrescibe ? 2 : 1);
        list.add("80029");
        if (isPrescibe) {
            list.add("9999");
        }
        List<WeideProIdDto> dtoList = gaiaTOrderInfoMapper.getWeideEbProId(client, stoCode, list);
        List<WeideProIdDto> collect = dtoList.stream().filter(r -> {
            BigDecimal qty = r.getQty();
            return qty != null && qty.compareTo(BigDecimal.ZERO) > 0;
        }).collect(Collectors.toList());
        logger.info("O2O韦德定制化，饿百查询需绑定外卖袋及处方编码，client:{}, stoCode:{}, orderId:{}, isPrescibe:{}, result:{}",
                client, stoCode, orderId, isPrescibe, JSON.toJSONString(collect));
        return collect;
    }

    /**
     * 韦德定制化产物
     * 需绑定平台对应的外卖包装袋；如检查到订单中有处方药需绑定处方费
     * 美团外卖袋编码为：81033、80037，优先扣减81033，如无库存则扣减80037，如两个编码都无库存，则不计入订单与销售单
     * 处方费编码为：9999，如无库存，则不计入订单与销售单
     */
    @Override
    public List<WeideProIdDto> getWeideMtProId(String client, String stoCode, boolean isPrescibe, String orderId) {
        if (!"10000072".equals(client)) {
            return null;
        }
        List<String> list = new ArrayList<>(isPrescibe ? 3 : 2);
        list.add("81033");
        list.add("80037");
        if (isPrescibe) {
            list.add("9999");
        }
        List<WeideProIdDto> dtoList = gaiaTOrderInfoMapper.getWeideEbProId(client, stoCode, list);
        List<WeideProIdDto> collect = dtoList.stream().filter(r -> {
            BigDecimal qty = r.getQty();
            return qty != null && qty.compareTo(BigDecimal.ZERO) > 0;
        }).collect(Collectors.toList());
        final AtomicInteger total = new AtomicInteger();
        collect.forEach(r -> {
            if ("81033".equals(r.getProId()) || "80037".equals(r.getProId())) {
                total.getAndIncrement();
            }
        });
        if (total.get() == 2) {
            logger.info("O2O韦德定制化，美团查询需绑定外卖袋及处方编码，两个编码都有库存，开始移除80037，client:{}, stoCode:{}, orderId:{},result:{}",
                    client, stoCode, orderId, JSON.toJSONString(collect));
            collect.remove(new WeideProIdDto("80037"));
        }
        logger.info("O2O韦德定制化，美团查询需绑定外卖袋及处方编码，client:{}, stoCode:{}, orderId:{}, isPrescibe:{}, result:{}",
                client, stoCode, orderId, isPrescibe, JSON.toJSONString(collect));
        return collect;
    }

    @Override
    public void execOrdersPush(Map<String, Object> order_info, String platform, String source, String secret, GaiaTAccount account) {
        String client = nullToEmpty(order_info.get("client"));
        String stoCode = nullToEmpty(order_info.get("stoCode"));
        String shop_no = nullToEmpty(order_info.get("shop_no"));
        String order_id = nullToEmpty(order_info.get("order_id"));
        String platforms_order_id = nullToEmpty(order_info.get("platforms_order_id"));
        String delivery_time = String.valueOf(order_info.get("delivery_time"));
        logger.debug("order_info:" + order_info);
        if (shop_no.equals("")) return;
        String result = "";
        try {
            platform = platform.toUpperCase();
            String rev = "";
            switch (platform) {
                case Constants.PLATFORMS_EB:
                    //相当于自动帮接单
                    rev = EbApiUtil.orderConfirm(source, secret, platforms_order_id);
//                    Map revmap = (Map) JSON.parse(rev);
//                    Map revbody = (Map) revmap.get("body");
//                    if (String.valueOf(revbody.get("errno")).equals("0")) {
//                        result = "ok";
//                    } else {
//                        result = "ERROR:" + String.valueOf(revbody.get("error")) + "\n";
//                    }
                    result = "ok";
                    break;
                case Constants.PLATFORMS_MT:
                    String accessToken = getAccessToken(account);
                    //客户如果开启自动接单会报错：{"error":{"code":808,"msg":"操作失败,订单已经确认过了"},"data":"ng"}
                    rev = MtApiUtil.orderConfirm(source, secret, platforms_order_id, accessToken);
                    //rev = MtApiUtil.orderConfirm(source, secret, platforms_order_id, "token_sk-3BTfvduGtcidW9D9I1Q");
                    result = "ok";
                    break;
                case Constants.PLATFORMS_YL:
                    result = "ok";
                    break;
                case Constants.PLATFORMS_DDKY:
                    result = "ok";
                    break;
                case Constants.PLATFORMS_MINI_MALL:
                    result = "ok";
                    break;
                case Constants.PLATFORMS_JGG:
                    result = "ok";
            }
        } catch (Exception e) {
            logger.error("商家确认订单! 平台订单ID：" + order_id + " 执行异常：" + e.getMessage());
            result = "ERROR" + e.getMessage();
        }

        if (!result.startsWith("ok")) {
            //消息队列只记录调用接口失败的情况
            //shop_no|
            logger.info("外卖-订单API：商家确认订单失败！ 平台订单ID：" + order_id);
            Map<String, Object> logMap = new HashMap<String, Object>();
            logMap.put("operatetype", "4");
            logMap.put("platform_type", "1");
            logMap.put("operatename", "deliveryOrderConfirm");
            logMap.put("remark", "订单API：商家确认订单");
            logMap.put("IsOk", "0");
            logMap.put("reason", "门店号："
                    + shop_no
                    + "订单号：" + order_id
                    + "平台订单号："
                    + nullToEmpty(order_info.get("platforms_order_id"))
                    + "错误原因："
                    + result);
            insertOperateLog(logMap);
        } else {
            String storeLogo = getStoreLogoByClientAndBrId(client, stoCode);
            order_info.put("storeLogo", storeLogo);
            pushToStoreMq(client, stoCode, "O2O", "newOrder", order_info);
            logger.info("推送外卖新订单, mq 推送， orderId:{}, storeLogo:{}, client:{}, stoCode:{}", order_id, storeLogo, client, stoCode);
        }
    }

    //获取门店下的主收银电脑标识
    @Override
    public String getStoreLogoByClientAndBrId(String client, String stoCode) {
        return computerInformationMapper.getMainSaleByClientAndBrId(client, stoCode);
    }

    @Override
    public JsonResult getSaleByOrderId(FxSaleQueryBean bean) {
        List<FxSaleDto> list = sdSaleHMapper.getSaleByOrderId(bean);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @Override
    public List<TakeAwayOrderOutData> getOrderList(GetTakeAwayInData inData) {
        return gaiaTOrderInfoMapper.getOrderList2(inData);
    }

    @Override
    public JsonResult syncStock(String client, String depId) {
        logger.info("sync stock, 外卖库存同步开始======== client:{}, depId:{}", client, depId);
        long start = System.currentTimeMillis();
        //查店铺对接的外卖平台
        List<GaiaTAccount> accountList = gaiaTAccountMapper.findByClientAndStoCode(client, depId);
        List<GaiaTAccount> list = accountList.stream().filter(r -> "Y".equalsIgnoreCase(r.getStockOnService())).collect(Collectors.toList());
        if (CollUtil.isEmpty(list)) {
            return JsonResult.fail(101, "请检查外卖库存开关是否打开");
        }
        List<JsonResult> resultList = Lists.newArrayList();
        for (GaiaTAccount gaiaTAccount : list) {
            String platform = gaiaTAccount.getPlatform();
            switch (platform) {
                case Constants.PLATFORMS_EB:
                    JsonResult ebResult = getEbStockSyncJsonResult(client, depId, gaiaTAccount);
                    if (ebResult != null) resultList.add(ebResult);
                    break;
                case Constants.PLATFORMS_MT:
                    JsonResult mtResult = getMtStockSyncJsonResult(client, depId, gaiaTAccount);
                    if (mtResult != null) resultList.add(mtResult);
                    break;
                case Constants.PLATFORMS_JD:
                    JsonResult jdResult = getJdStockSyncJsonResult(client, depId, gaiaTAccount);
                    if (jdResult != null) resultList.add(jdResult);
                    break;
                case Constants.PLATFORMS_YL:
                    JsonResult ylResult = getYlStockSyncJsonResult(client, depId, gaiaTAccount);
                    if (ylResult != null) resultList.add(ylResult);
                    break;
                case Constants.PLATFORMS_DDKY:
                    JsonResult ddkyResult = getDdkyStockSyncJsonResult(client, depId, gaiaTAccount);
                    if (ddkyResult != null) resultList.add(ddkyResult);
                    break;
            }
        }
        logger.info("sync stock, 外卖库存同步，耗时：" + (System.currentTimeMillis() - start) + "ms");
        if (CollUtil.isNotEmpty(resultList)) {
            long count = resultList.stream().filter(r -> !UtilConst.CODE_0.equals(r.getCode())).count();
            if (count > 0) {
                return JsonResult.fail(101, "库存同步失败");
            }
        }
        return JsonResult.success("同步成功", null);
    }

    private JsonResult getEbStockSyncJsonResult(String client, String depId, GaiaTAccount gaiaTAccount) {
        String result = EbApiUtil.skuList(gaiaTAccount.getAppId(), gaiaTAccount.getAppSecret(), gaiaTAccount.getPublicCode(), 1, EbApiUtil.STOCK_UPDATE_MAX);
        Map resultMap = (Map) JSON.parse(result);
        Map resultBody = (Map) resultMap.get("body");
        if (!"0".equals(String.valueOf(resultBody.get("errno")))) {
            logger.error("sync stock, EB外卖库存列表查询失败: body:{}, client:{},depId:{}", JSON.toJSONString(resultBody), client, depId);
            return JsonResult.fail(101, "库存同步失败");
        }

        Map<String, Object> skuInfoMap = getEbSkuInfos(resultBody, null);
        Integer pages = (Integer) skuInfoMap.get("pages");
        List<String> infos = (List<String>) skuInfoMap.get("list");
        if (pages > 1) {
            for (int i = 2; i <= pages; i++) {
                String resultTemp = EbApiUtil.skuList(gaiaTAccount.getAppId(), gaiaTAccount.getAppSecret(), gaiaTAccount.getPublicCode(), i, EbApiUtil.STOCK_UPDATE_MAX);
                Map resultMapTemp = (Map) JSON.parse(resultTemp);
                Map resultBodyTemp = (Map) resultMapTemp.get("body");
                if (!"0".equals(String.valueOf(resultBodyTemp.get("errno")))) {
                    logger.error("sync stock, EB外卖库存列表查询失败: body:{}, client:{},depId:{}", JSON.toJSONString(resultBody), client, depId);
                    return JsonResult.fail(101, "库存同步失败");
                } else {
                    getEbSkuInfos(resultBodyTemp, infos);
                }
            }
        }
        if (CollUtil.isEmpty(infos)) {
            return JsonResult.success("同步成功", null);
        }
        //查询商品库存
        List<GaiaSdStock> sdStockList = sdStockMapper.findByClientAndGssBrIdAndGssProIdIn(client, depId, infos);
        logger.info("sync stock, EB外卖商品列表查询, client:{}, stoCode:{}, publicCode:{}, prodIds'size:{}, prodIds:{}, query stockList size:{}", client, depId,
                gaiaTAccount.getPublicCode(), infos.size(), JSON.toJSONString(infos), sdStockList.size());
        if (CollUtil.isNotEmpty(sdStockList)) {
            //饿了么库存批量更新接口一次性只能处理<=100个商品
            int size = sdStockList.size();
            if (size > EbApiUtil.STOCK_UPDATE_MAX) {
                int page = size / EbApiUtil.STOCK_UPDATE_MAX;
                int left = size % EbApiUtil.STOCK_UPDATE_MAX;
                int total = page;
                if (left > 0) {
                    total++;
                }
                for (int i = 0; i < total; i++) {
                    List<GaiaSdStock> tempList;
                    if (left > 0 && i == total - 1) {
                        tempList = sdStockList.subList(EbApiUtil.STOCK_UPDATE_MAX * i, EbApiUtil.STOCK_UPDATE_MAX * i + left);
                    } else {
                        tempList = sdStockList.subList(EbApiUtil.STOCK_UPDATE_MAX * i, EbApiUtil.STOCK_UPDATE_MAX * (i + 1));
                    }
                    if (stockSyncDetail(tempList, Constants.PLATFORMS_EB, null)) return JsonResult.fail(101, "库存同步失败");
                }
            } else {
                if (stockSyncDetail(sdStockList, Constants.PLATFORMS_EB, null)) return JsonResult.fail(101, "库存同步失败");
            }
        }
        return null;
    }

    private JsonResult getMtStockSyncJsonResult(String client, String depId, GaiaTAccount gaiaTAccount) {
        String accessToken = getAccessToken(gaiaTAccount);
        String result = MtApiUtil.medicineList(gaiaTAccount.getAppId(), gaiaTAccount.getAppSecret(), gaiaTAccount.getPublicCode(),
                0, MtApiUtil.STOCK_UPDATE_MAX_COUNT, accessToken);
        if ("error".equals(result) || result.contains("error")) {
            logger.error("sync stock, MT外卖库存列表查询失败: body:{}, client:{},depId:{}", result, client, depId);
            return JsonResult.fail(101, "库存同步失败");
        }
        JSONObject jsonObject = JSONObject.parseObject(result);
        JSONArray data = jsonObject.getJSONArray("data");
        JSONObject extraInfo = jsonObject.getJSONObject("extra_info");
        Integer totalCount = extraInfo.getInteger("total_count");
        List<String> infos = Lists.newArrayList();
        getMtSkuInfos(data, infos);
        int sizeNow = data.size();
        if (sizeNow == 0) {
            return JsonResult.success("同步成功", null);
        }
        int remainder = totalCount % sizeNow;
        int total = totalCount / sizeNow;
        if (remainder > 0 || total > 1) {
            for (int i = 1; i <= total; i++) {
                String resultTemp = MtApiUtil.medicineList(gaiaTAccount.getAppId(), gaiaTAccount.getAppSecret(), gaiaTAccount.getPublicCode(),
                        MtApiUtil.STOCK_UPDATE_MAX_COUNT * i, MtApiUtil.STOCK_UPDATE_MAX_COUNT, accessToken);
                if ("error".equals(resultTemp) || resultTemp.contains("error")) {
                    logger.error("sync stock, MT外卖库存列表查询失败: body:{}, client:{},depId:{}", resultTemp, client, depId);
                    return JsonResult.fail(101, "库存同步失败");
                }
                JSONObject jsonObjectTemp = JSONObject.parseObject(resultTemp);
                JSONArray dataTemp = jsonObjectTemp.getJSONArray("data");
                getMtSkuInfos(dataTemp, infos);
            }
        }
        if (CollUtil.isEmpty(infos)) {
            return JsonResult.success("同步成功", null);
        }
        //查询商品库存
        List<GaiaSdStock> sdStockList = sdStockMapper.findByClientAndGssBrIdAndGssProIdIn(client, depId, infos);
        logger.info("sync stock, MT外卖商品列表查询, client:{}, stoCode:{}, publicCode:{}, prodIds'size:{}, prodIds:{}, query stockList size:{}", client, depId,
                gaiaTAccount.getPublicCode(), infos.size(), JSON.toJSONString(infos), sdStockList.size());
        if (CollUtil.isNotEmpty(sdStockList)) {
            //美团库存批量更新接口一次性只能处理<=200个商品
            int size = sdStockList.size();
            if (size > MtApiUtil.STOCK_UPDATE_MAX_COUNT) {
                int page = size / MtApiUtil.STOCK_UPDATE_MAX_COUNT;
                int left = size % MtApiUtil.STOCK_UPDATE_MAX_COUNT;
                int stockTotal = page;
                if (left > 0) {
                    stockTotal++;
                }
                for (int i = 0; i < stockTotal; i++) {
                    List<GaiaSdStock> tempList;
                    if (left > 0 && i == stockTotal - 1) {
                        tempList = sdStockList.subList(MtApiUtil.STOCK_UPDATE_MAX_COUNT * i, MtApiUtil.STOCK_UPDATE_MAX_COUNT * i + left);
                    } else {
                        tempList = sdStockList.subList(MtApiUtil.STOCK_UPDATE_MAX_COUNT * i, MtApiUtil.STOCK_UPDATE_MAX_COUNT * (i + 1));
                    }
                    if (stockSyncDetail(tempList, Constants.PLATFORMS_MT, accessToken))
                        return JsonResult.fail(101, "库存同步失败");
                }
            } else {
                if (stockSyncDetail(sdStockList, Constants.PLATFORMS_MT, accessToken))
                    return JsonResult.fail(101, "库存同步失败");
            }
        }
        return null;
    }

    private JsonResult getJdStockSyncJsonResult(String client, String depId, GaiaTAccount account) {
        String token = account.getToken();
        List<Map<String, Object>> data = JdApiUtil.getmedicineList(account.getAppId(), account.getAppSecret(), account.getToken(), account.getPublicCode());
        if (data == null) {
            logger.error("sync stock, JD外卖库存列表查询失败: body:{}, client:{},depId:{}", data, client, depId);
            return JsonResult.fail(101, "库存同步失败");
        }
        List<String> infos = Lists.newArrayList();
        getJdSkuInfos(data, infos);
        if (CollUtil.isEmpty(infos)) {
            return JsonResult.success("同步成功", null);
        }
        //查询商品库存
        List<GaiaSdStock> sdStockList = sdStockMapper.findByClientAndGssBrIdAndGssProIdIn(client, depId, infos);
        logger.info("sync stock, JD外卖商品列表查询, client:{}, stoCode:{}, publicCode:{}, prodIds'size:{}, prodIds:{}, query stockList size:{}", client, depId,
                account.getPublicCode(), infos.size(), JSON.toJSONString(infos), sdStockList.size());
        if (CollUtil.isNotEmpty(sdStockList)) {
            //京东库存批量更新接口一次性只能处理<=50个商品
            int size = sdStockList.size();
            if (size > JdApiUtil.STOCK_UPDATE_MAX_COUNT) {
                int page = size / JdApiUtil.STOCK_UPDATE_MAX_COUNT;
                int left = size % JdApiUtil.STOCK_UPDATE_MAX_COUNT;
                int stockTotal = page;
                if (left > 0) {
                    stockTotal++;
                }
                for (int i = 0; i < stockTotal; i++) {
                    List<GaiaSdStock> tempList;
                    if (left > 0 && i == stockTotal - 1) {
                        tempList = sdStockList.subList(JdApiUtil.STOCK_UPDATE_MAX_COUNT * i, JdApiUtil.STOCK_UPDATE_MAX_COUNT * i + left);
                    } else {
                        tempList = sdStockList.subList(JdApiUtil.STOCK_UPDATE_MAX_COUNT * i, JdApiUtil.STOCK_UPDATE_MAX_COUNT * (i + 1));
                    }
                    if (stockSyncDetail(tempList, Constants.PLATFORMS_JD, token))
                        return JsonResult.fail(101, "库存同步失败");
                }
            } else {
                if (stockSyncDetail(sdStockList, Constants.PLATFORMS_JD, token))
                    return JsonResult.fail(101, "库存同步失败");
            }
        }
        return null;
    }

    private JsonResult getYlStockSyncJsonResult(String client, String depId, GaiaTAccount account) {
        boolean success = syncStockToYaolian(client, Lists.newArrayList(depId), null, true);
        if (!success) {
            return JsonResult.fail(101, "库存同步失败");
        }
        return null;
    }

    private JsonResult getDdkyStockSyncJsonResult(String client, String depId, GaiaTAccount account) {
        String result = goodsSync(client, depId, null);
        if (!"全量同步成功".equalsIgnoreCase(result)) {
            return JsonResult.fail(101, "库存同步失败");
        }
        return null;
    }

    private void getJdSkuInfos(List<Map<String, Object>> data, List<String> infos) {
        //[{"msg":"成功","stockout":0,"code":0,"lockQty":0,"usableQty":0,"vendibility":0,"orderQty":0,"currentQty":0,"outSkuId":"40203008","skuId":2041143517}]
        for (Map<String, Object> map : data) {
            infos.add((String) map.get("outSkuId"));
        }
    }

    //accessToken MT、JD使用参数
    private boolean stockSyncDetail(List<GaiaSdStock> stockList, String platform, String accessToken) {
        try {
            List<Map<String, Object>> list = stockService.getPlatformParamMap(stockList);
            if (list != null) {
                logger.info("sync stock, 库存更新，调外卖平台库存更新接口======， paramMap:" + JSON.toJSONString(list));
                for (Map<String, Object> paramMap : list) {
                    if (!platform.equalsIgnoreCase((String) paramMap.get("platform"))) {
                        continue;
                    }
                    String res = baseService.updateStockWithMap(paramMap, accessToken);
                    logger.info("sync stock, 库存更新，调外卖平台库存更新接口======, result:" + res);
                    if (!"ok".equalsIgnoreCase(res)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("sync stock, 调外卖平台更新库存接口报错，error:{}", e.getMessage());
            return true;
        }
        return false;
    }

    /**
     * 获取EB商品编码
     *
     * @param resultBody
     * @param resultList null: 第一次调API
     * @return
     */
    private Map<String, Object> getEbSkuInfos(Map resultBody, List<String> resultList) {
        Map<String, Object> dataMap = (Map<String, Object>) resultBody.get("data");
        List<Map<String, Object>> list = (List<Map<String, Object>>) dataMap.get("list");
        List<String> infos = new ArrayList<>(list.size());
        if (CollUtil.isNotEmpty(list)) {
            for (Map<String, Object> map : list) {
                String customSkuId = (String) map.get("custom_sku_id");
                if (resultList != null) {
                    resultList.add(customSkuId);
                } else {
                    infos.add(customSkuId);
                }
            }
        }
        Map<String, Object> map = new HashMap<>(4);
        map.put("pages", dataMap.get("pages"));//总页数
        map.put("list", infos);//商品编码集合
        return map;
    }

    /**
     * 获取MT商品编码
     */
    private void getMtSkuInfos(JSONArray jsonArray, List<String> resultList) {
        for (Object o : jsonArray) {
            JSONObject jsonObject = (JSONObject) o;
            String appMedicineCode = jsonObject.getString("app_medicine_code");
            if (appMedicineCode != null) {
                resultList.add(appMedicineCode);
            }
        }
    }


    @Override
    public JsonResult sendAction(String orderNo, String client, String stoCode) {
//        GaiaTAccount gaiaTAcount = gaiaTAccountMapper.findByClientAndStoCode(client, stoCode);
//        String platformOrderId = gaiaTOrderInfoMapper.getPlatformOrderIdByOrderId(new GetTakeAwayInData(client, stoCode, orderNo));
//        if (platformOrderId == null) {
//            return JsonResult.error("订单异常");
//        }
//        if (gaiaTAcount != null) {
//            if (EbApiUtil.pickComplete(gaiaTAcount.getAppId(), gaiaTAcount.getAppSecret(), platformOrderId)) {
//                return JsonResult.success(null, "success");
//            } else {
//                return JsonResult.fail(101, "重复拣货");
//            }
//        } else {
//            return JsonResult.error("此门店未对接外卖");
//        }
        return null;
    }

    @Override
    public JsonResult returnOrder(TakeAwayOrderBean bean, String client, String stoCode) {
        GaiaTOrderInfo orderInfo = gaiaTOrderInfoMapper.getPlatformOrderIdByOrderId(new GetTakeAwayInData(client, stoCode, bean.getOrderNo()));
        if (orderInfo == null) {
            return JsonResult.fail(101, "此订单不存在");
        }
        String platform = orderInfo.getPlatforms();
        //状态不做拦截
//        String status = orderInfo.getStatus();
//        boolean b = "1".equalsIgnoreCase(status) || "6".equalsIgnoreCase(status);
//        if (!b) {
//            return JsonResult.fail(101, "此订单当前状态不可退单");
//        }

        //查店铺对接的外卖平台
        List<GaiaTAccount> accountList = gaiaTAccountMapper.findByClientAndStoCode(client, stoCode);
        GaiaTAccount account = accountList.stream().filter(r -> platform.equals(r.getPlatform()) &&
                "Y".equalsIgnoreCase(r.getOrderOnService())).findFirst().orElseThrow(() -> new BusinessException("请检查外卖订单开关是否打开"));
//        if (gaiaTAccount == null || !"Y".equalsIgnoreCase(gaiaTAccount.getOrderOnService())) {
//            return JsonResult.fail(101, "请检查外卖订单开关是否打开");
//        }
        String appId = account.getAppId();
        String appSecret = account.getAppSecret();
        switch (platform) {
            case Constants.PLATFORMS_EB:
                String ebResult = EbApiUtil.orderCancel(appId, appSecret, orderInfo.getPlatformsOrderId(), bean.getType(), bean.getReason());
                Map resultMap = (Map) JSON.parse(ebResult);
                Map resultBody = (Map) resultMap.get("body");
                if (!"0".equals(String.valueOf(resultBody.get("errno")))) {
                    logger.error("外卖订单取消失败: body:" + JSON.toJSONString(resultBody));
                    return JsonResult.fail(101, "退单失败");
                }
                break;
            case Constants.PLATFORMS_MT:
                String accessToken = getAccessToken(account);
                String mtResult = MtApiUtil.orderCancel(appId, appSecret, orderInfo.getPlatformsOrderId(), bean.getReason(), bean.getType(), accessToken);
                //String mtResult = MtApiUtil.orderCancel(appId, appSecret, orderInfo.getPlatformsOrderId(), bean.getType(), bean.getReason(), "token_sk-3BTfvduGtcidW9D9I1Q");
                if (!"ok".equalsIgnoreCase(mtResult)) {
                    return JsonResult.fail(101, "退单失败");
                }
                //设置退单原因
                gaiaTOrderInfoMapper.updateStatus(orderInfo.getPlatformsOrderId(), orderInfo.getPlatforms(), bean.getType(), bean.getReason());
                break;
        }
        return JsonResult.success("退单成功", null);
    }

    //品牌商入驻不是oauth认证方式是sig方式，即不用获取access_token //todo 因为有品牌商入驻和服务商入驻；1.考虑存在性以及适配性 2.引入的Jar里的配置文件只能是一种认证方式
    @Override
    public String getAccessToken(GaiaTAccount account) {
        /*String appId = account.getAppId();
        String appSecret = account.getAppSecret();
        String publicCode = account.getPublicCode();
        String appStoId = account.getAppStoId();
        String mtTokenKey = getMtTokenKey(account);
        String accessToken = (String) redisManager.get(mtTokenKey);
        if (accessToken != null) {
            logger.info("美团API, 直接从redis获取token，appId:{},publicCode:{},accessToken:{}", appId, publicCode, accessToken);
            return accessToken;
        }
        String mtRefreshTokenKey = getMtRefreshTokenKey(account);
        String refreshToken = (String) redisManager.get(mtRefreshTokenKey);
        if (refreshToken != null) {
            MtApiUtil.TokenResponse response = MtApiUtil.refreshToken(appId, publicCode, appSecret, refreshToken);
            if (response != null) {
                accessToken = response.getAccess_token();
                if (StrUtil.isNotEmpty(accessToken)) {
                    long expiresIn = response.getExpires_in();
                    refreshToken = response.getRefresh_token();
                    long reExpiresIn = response.getRe_expires_in();
                    redisManager.set(mtTokenKey, accessToken, expiresIn);
                    redisManager.set(mtRefreshTokenKey, refreshToken, reExpiresIn);
                    logger.info("美团API, token过期，refreshToken没过期，刷新token，appId:{},publicCode:{},accessToken:{}", appId, publicCode, accessToken);
                    return accessToken;
                } else {
                    logger.info("美团API, token过期，refreshToken没过期，刷新token，新token获取失败, appId:{},publicCode:{}", appId, publicCode);
                }
            }
            return accessToken;
        }
        MtApiUtil.TokenResponse tokenResponse = MtApiUtil.getAccessToken(appId, publicCode, appSecret);
        if (tokenResponse != null) {
            accessToken = tokenResponse.getAccess_token();
            if (StrUtil.isNotEmpty(accessToken)) {
                long expiresIn = tokenResponse.getExpires_in();
                refreshToken = tokenResponse.getRefresh_token();
                long reExpiresIn = tokenResponse.getRe_expires_in();
                redisManager.set(mtTokenKey, accessToken, expiresIn);
                redisManager.set(mtRefreshTokenKey, refreshToken, reExpiresIn);
                logger.info("美团API, token过期，refreshToken也过期，获取新token，appId:{},publicCode:{},accessToken:{}", appId, publicCode, accessToken);
                return accessToken;
            } else {
                logger.info("美团API, token过期，refreshToken也过期，获取新token，新token获取失败, appId:{},publicCode:{}", appId, publicCode);
            }
        }*/
        return null;
    }

    @Override
    public JsonResult syncPayTime(OrderPayTimeBean bean) {
        gaiaTOrderInfoMapper.updatePayTime(bean);
        logger.info("O2O同步支付时间,client:{}, stoCode:{}, orderId:{}, payTime:{}", bean.getClient(), bean.getStoCode(), bean.getOrderId(), bean.getPayTime());
        return JsonResult.success("同步支付时间成功", null);
    }

    @Override
    public JsonResult getPayTime(OrderPayTimeBean bean) {
        String payTime = gaiaTOrderInfoMapper.getPayTime(bean);
        return JsonResult.success(payTime, "提示：获取数据成功！");
    }

    @Transactional
    @Override
    public JsonResult setMainSale(FxMainSaleSetBean bean) {
        synchronized (bean.getClient() + bean.getStoCode()) {
            int matchedNum = computerInformationMapper.setMainSale(bean);
            int noMainMatchedNum = computerInformationMapper.setNoMainSale(bean);
            if (matchedNum > 0) {
                logger.info("FX设置主收银，主收银匹配数据条数:{}, 非主收银匹配数据条数:{}, storeLogo:{}", matchedNum, noMainMatchedNum, bean.getStoreLogo());
                List<String> list = computerInformationMapper.getNoMainSale(bean);
                if (CollUtil.isNotEmpty(list)) {
                    Map<String, Object> messageMap = new HashMap<>(2);
                    messageMap.put("noMainSale", list);
                    pushToStoreMq(bean.getClient(), bean.getStoCode(), "O2O", "mainSale", messageMap);
                    logger.info("FX设置主收银，mq推送主收银设置变更, storeLogo:{},client:{},stoCode:{},list:{}", bean.getStoreLogo(), bean.getClient(), bean.getStoCode(), JSON.toJSONString(list));
                }
                return JsonResult.success("主收银设置成功", null);
            }
        }
        logger.error("FX设置主收银，无匹配数据, storeLogo:{}", bean.getStoreLogo());
        return JsonResult.fail(101, "设置失败");
    }

    @Override
    public JsonResult getMainSale(FxMainSaleSetBean bean) {
        String mainSale = computerInformationMapper.getMainSale(bean);
        mainSale = "1".equalsIgnoreCase(mainSale) ? mainSale : "0";
        return JsonResult.success(mainSale, "提示：获取数据成功！");
    }

    @Override
    public JsonResult getByProdIdAndOrderNo(OrderDetailBean bean) {
        List<SaleDRebornData> list = gaiaSdSaleDMapper.getByProdIdAndOrderNo(bean);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @Override
    public JsonResult orderQuery(WebOrderQueryBean bean) {
        List<WebOrderDataDto> dtoList = getOrderQueryList(bean);
        WebOrderDataDto webOrderDataDto;
        if (CollUtil.isEmpty(dtoList)) {
            webOrderDataDto = new WebOrderDataDto();
        } else {
            webOrderDataDto = getSumDto(dtoList);
        }
        PageInfo<WebOrderDataDto> pageInfo = new PageInfo<>(dtoList, webOrderDataDto);
        return JsonResult.success(pageInfo, "提示：获取数据成功！");
    }

    @Override
    public void orderQueryOutput(WebOrderQueryBean bean, HttpServletRequest request, HttpServletResponse response) {
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String format = formatter.format(now);
        String file = "O2O订单汇总" + format + ".CSV";
        response.setContentType("application/vnd.ms-excel");
        try {
            response.setHeader("Content-disposition", "attachment;filename=" + file + ";" + "filename*=utf-8''" + file);
            bean.setPageSize(-1);
            List<WebOrderDataDto> dtoList = getOrderQueryList(bean);
            WebOrderDataDto webOrderDataDto;
            if (CollUtil.isEmpty(dtoList)) {
                webOrderDataDto = new WebOrderDataDto();
            } else {
                webOrderDataDto = getSumDto(dtoList);
            }
            webOrderDataDto.setIndex("合计");
            dtoList.add(webOrderDataDto);
            EasyExcel.write(response.getOutputStream(), WebOrderDataDto.class).sheet("sheet1").doWrite(dtoList);
        } catch (Exception e) {
            logger.error("O2O订单汇总导出失败：" + e.getMessage());
            throw new BusinessException("导出失败");
        }
    }

    @Override
    public JsonResult orderDetailQuery(WebOrderQueryBean bean) {
        List<WebOrderDetailDataDto> dtoList = getOrderDetailQuery(bean);
        WebOrderDetailDataDto dataDto;
        if (CollUtil.isEmpty(dtoList)) {
            dataDto = new WebOrderDetailDataDto();
        } else {
            dataDto = getDetailSumDto(dtoList);
        }
        PageInfo<WebOrderDetailDataDto> pageInfo = new PageInfo<>(dtoList, dataDto);
        return JsonResult.success(pageInfo, "提示：获取数据成功！");
    }

    @Override
    public void orderDetailQueryOutput(WebOrderQueryBean bean, HttpServletRequest request, HttpServletResponse response) {
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String format = formatter.format(now);
        String file = "O2O订单明细" + format + ".CSV";
        response.setContentType("application/vnd.ms-excel");
        try {
            response.setHeader("Content-disposition", "attachment;filename=" + file + ";" + "filename*=utf-8''" + file);
            bean.setPageSize(-1);
            List<WebOrderDetailDataDto> dtoList = getOrderDetailQuery(bean);
            WebOrderDetailDataDto dataDto;
            if (CollUtil.isEmpty(dtoList)) {
                dataDto = new WebOrderDetailDataDto();
            } else {
                dataDto = getDetailSumDto(dtoList);
            }
            dataDto.setIndex("合计");
            dtoList.add(dataDto);
            EasyExcel.write(response.getOutputStream(), WebOrderDetailDataDto.class).sheet("sheet1").doWrite(dtoList);
        } catch (Exception e) {
            logger.error("O2O订单明细导出失败：" + e.getMessage());
            throw new BusinessException("导出失败");
        }
    }

    @Override
    public String getJdToken(String venderId) {
        return gaiaTAccountMapper.getJdTokenByVenderId(venderId);
    }

    @Override
    public void updateJdToken(String venderId, String token) {
        gaiaTAccountMapper.updateJdToken(venderId, token);
    }

    @Override
    public GaiaTAccount getAccountByVenderId(String venderId) {
        return gaiaTAccountMapper.getAccountByVenderId(venderId);
    }

    @Override
    public String getAppSecretByToken(String token) {
        return gaiaTAccountMapper.getAppSecretByToken(token);
    }

    private List<WebOrderDataDto> getOrderQueryList(WebOrderQueryBean bean) {
        if (-1 != bean.getPageSize()) {
            PageHelper.startPage(bean.getPageNum(), bean.getPageSize());
        }
        String[] stoCodeOrName = bean.getStoCodeOrName();
        if (stoCodeOrName != null && stoCodeOrName.length != 0) {
            List<String> stoCodeList = Lists.newArrayList();
            for (String s : stoCodeOrName) {
                stoCodeList.add(s);
            }
            bean.setStoCodeList(stoCodeList);
        }
        //剔除退货单：H表 GSSH_RETURN_STATUS字段是医保相关，不可用；用GSSH_BILL_NO_RETURN，这个字段不为空说明是退货单
        List<WebOrderDataDto> dtoList = gaiaTOrderInfoMapper.orderQuery(bean);
        for (int i = 0; i < dtoList.size(); i++) {
            WebOrderDataDto dto = dtoList.get(i);
            dto.setIndex(String.valueOf(i + 1));
            dto.setStatus(O2OorderStatusEnum.getStatus(Integer.valueOf(dto.getStatus())));
        }
        return dtoList;
    }

    private List<WebOrderDetailDataDto> getOrderDetailQuery(WebOrderQueryBean bean) {
        if (-1 != bean.getPageSize()) {
            PageHelper.startPage(bean.getPageNum(), bean.getPageSize());
        }
        String proId = bean.getProId();
        if (StrUtil.isNotEmpty(proId)) {
            String[] split = proId.split(",");
            List<String> proIdList = new ArrayList<>(split.length);
            for (String s : split) {
                proIdList.add(s);
            }
            bean.setProIdList(proIdList);
        }
        List<WebOrderDetailDataDto> dtoList = gaiaTOrderInfoMapper.orderDetailQuery(bean);
        for (int i = 0; i < dtoList.size(); i++) {
            WebOrderDetailDataDto dto = dtoList.get(i);
            dto.setIndex(String.valueOf(i + 1));
        }
        return dtoList;
    }

    private WebOrderDataDto getSumDto(List<WebOrderDataDto> dtoList) {
        WebOrderDataDto dto = new WebOrderDataDto();
        //门店去重计数
        dto.setStoCode(String.valueOf(dtoList.stream().map(r -> r.getStoCode()).distinct().count()));
        //订单平台去重计数
        dto.setPlatform(String.valueOf(dtoList.stream().map(r -> r.getPlatform()).distinct().count()));
        //平台单号计数
        dto.setPlatformOrderId(String.valueOf(dtoList.stream().map(r -> r.getPlatformOrderId()).distinct().count()));
        //本地单号计数
        dto.setOrderId(String.valueOf(dtoList.stream().map(r -> r.getOrderId()).distinct().count()));
        //平台零售总额
        BigDecimal platformOriginalPrice = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getPlatformOriginalPrice()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getPlatformOriginalPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        platformOriginalPrice = platformOriginalPrice.setScale(2, RoundingMode.HALF_UP);
        dto.setPlatformOriginalPrice(String.valueOf(platformOriginalPrice));
        //平台折扣金额
        BigDecimal platformZkPrice = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getPlatformZkPrice()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getPlatformZkPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        platformZkPrice = platformZkPrice.setScale(2, RoundingMode.HALF_UP);
        dto.setPlatformZkPrice(String.valueOf(platformZkPrice));
        //平台配送费
        BigDecimal platformShippingFee = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getPlatformShippingFee()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getPlatformShippingFee()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        platformShippingFee = platformShippingFee.setScale(2, RoundingMode.HALF_UP);
        dto.setPlatformShippingFee(String.valueOf(platformShippingFee));
        //平台使用费
        BigDecimal platformUseFee = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getPlatformUseFee()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getPlatformUseFee()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        platformUseFee = platformUseFee.setScale(2, RoundingMode.HALF_UP);
        dto.setPlatformUseFee(String.valueOf(platformUseFee));
        //平台实收金额
        BigDecimal platformActualPrice = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getPlatformActualPrice()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getPlatformActualPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        platformActualPrice = platformActualPrice.setScale(2, RoundingMode.HALF_UP);
        dto.setPlatformActualPrice(String.valueOf(platformActualPrice));
        //顾客实付金额汇总
        BigDecimal customerPay = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getCustomerPay()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getCustomerPay()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        customerPay = customerPay.setScale(2, RoundingMode.HALF_UP);
        dto.setCustomerPay(String.valueOf(customerPay));
        //销售单号去重计数
        dto.setSaleOrderId(String.valueOf(dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getSaleOrderId())).map(r -> r.getSaleOrderId()).distinct().count()));
        //销售零售总额
        BigDecimal saleTotalAmt = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getSaleTotalAmt()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getSaleTotalAmt()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        saleTotalAmt = saleTotalAmt.setScale(2, RoundingMode.HALF_UP);
        dto.setSaleTotalAmt(String.valueOf(saleTotalAmt));
        //销售实收金额
        BigDecimal saleActualPrice = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getSaleActualPrice()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getSaleActualPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        saleActualPrice = saleActualPrice.setScale(2, RoundingMode.HALF_UP);
        dto.setSaleActualPrice(String.valueOf(saleActualPrice));
        //销售折扣金额汇总
        BigDecimal saleZkPrice = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getSaleZkPrice()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getSaleZkPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        saleZkPrice = saleZkPrice.setScale(2, RoundingMode.HALF_UP);
        dto.setSaleZkPrice(String.valueOf(saleZkPrice));
        return dto;
    }

    private WebOrderDetailDataDto getDetailSumDto(List<WebOrderDetailDataDto> dtoList) {
        WebOrderDetailDataDto dto = new WebOrderDetailDataDto();
        //平台单号计数
        dto.setPlatformOrderId(String.valueOf(dtoList.stream().map(r -> r.getPlatformOrderId()).distinct().count()));
        //本地单号计数
        dto.setOrderId(String.valueOf(dtoList.stream().map(r -> r.getOrderId()).distinct().count()));
        //数量汇总
        BigDecimal num = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getNum()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getNum()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        num = num.setScale(2, RoundingMode.HALF_UP);
        dto.setNum(String.valueOf(num));
//        //平台实收金额
//        dto.setPlatformActualPrice(String.valueOf(dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getPlatformActualPrice()))
//                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getPlatformActualPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO)));
//        //平台折扣金额
//        dto.setPlatformZkPrice(String.valueOf(dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getPlatformZkPrice()))
//                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getPlatformZkPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO)));
        //平台实收金额
//        dto.setPlatformActualPrice(String.valueOf(dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getPlatformActualPrice()))
//                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getPlatformActualPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO)));
        //销售实收金额
        BigDecimal saleActualPrice = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getSaleActualPrice()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getSaleActualPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        saleActualPrice = saleActualPrice.setScale(2, RoundingMode.HALF_UP);
        dto.setSaleActualPrice(String.valueOf(saleActualPrice));
        //销售折扣金额汇总
        BigDecimal saleZkPrice = dtoList.stream().filter(r -> StrUtil.isNotEmpty(r.getSaleZkPrice()))
                .map(r -> BigDecimal.valueOf(Double.valueOf(r.getSaleZkPrice()))).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
        saleZkPrice = saleZkPrice.setScale(2, RoundingMode.HALF_UP);
        dto.setSaleZkPrice(String.valueOf(saleZkPrice));
        return dto;
    }

    private String getMtRefreshTokenKey(GaiaTAccount account) {
        return "MT:refreshToken:" + account.getPublicCode();
    }

    private String getMtTokenKey(GaiaTAccount account) {
        return "MT:accessToken:" + account.getPublicCode();
    }

    @Override
    public JsonResult log(TakeAwayOrderBean bean, String client, String stoCode) {
        gaiaTOrderInfoMapper.getPlatformOrderIdByOrderId(new GetTakeAwayInData(client, stoCode, bean.getOrderNo()));

        return null;
    }

    @Override
    public void pushToStoreMq(String client, String stoCode, String type, String cmd, Map<String, Object> inData) {
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType(type);
        mqReceiveData.setCmd(cmd);
        mqReceiveData.setData(inData);
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, client + "." + stoCode, JSON.toJSON(mqReceiveData));
    }

    public static void insertOperateLog(Map<String, Object> param) {
        XbServiceUtil.service.insertOperatelogWithMap(param);
    }

    //商品组拆分
    public static void splitGroupOrder(Map<String, Object> order_info) {
        try {
            boolean needResetSeq = false;
            List<Map<String, Object>> maps = (List<Map<String, Object>>) order_info.get("detail");
            if (maps != null) {
                List<Map> details = new ArrayList<>();
                for (Map<String, Object> m : maps) {
                    List<Map<String, Object>> items = XbServiceUtil.service.selectGroupMedicine(m);
                    if (items.size() == 0) {
                        details.add(m);
                    } else {
                        if (items.size() > 1) {
                            needResetSeq = true;
                        }
                        double price = Double.parseDouble(nullToEmpty(m.get("price"))) / items.size();
                        for (Map<String, Object> item : items) {
                            item.put("price", String.format("%.2f", price));
                        }
                        details.addAll(items);
                    }
                }
                if (needResetSeq) {
                    int sequence = 0;
                    for (Map m : details) {
                        sequence++;
                        m.put("sequence", String.valueOf(sequence));
                    }
                }
                order_info.put("detail", details);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String nullToEmpty(Object param) {
        if (param == null) {
            return "";
        } else {
            return String.valueOf(param);
        }
    }

    @Override
    public YlResponseData queryStoreByYaolian(String client) {
    	List<GaiaStoreData> storeDataList = storeDataMapper.selectByYaolian(client);
        String cooperation = gaiaYlCooperationDao.getCooperationByClient(client);
        YlResponseData response = YlApiUtil.storeInitByYaolian(cooperation, storeDataList);
        return response;
    }

    //syncStock 是否是异动更新, true: 异动； false: 初次上传
    @Override
    public boolean syncStockToYaolian(String client,List<String> brIds,List<String> proIds, boolean syncStock) {
        Example example = new Example(GaiaSdStock.class);
        if (CollUtil.isEmpty(brIds)) {
            if (CollUtil.isNotEmpty(proIds)) {
                example.createCriteria()
                        .andEqualTo("clientId", client)
                        .andIn("gssProId", proIds);
            } else {
                example.createCriteria()
                        .andEqualTo("clientId", client)
                        .andGreaterThan("gssQty", "0");
            }
        } else {
            if (CollUtil.isNotEmpty(proIds)) {
                example.createCriteria()
                        .andEqualTo("clientId", client)
                        .andIn("gssBrId", brIds)
                        .andIn("gssProId", proIds);
            } else {
                example.createCriteria()
                        .andEqualTo("clientId", client)
                        .andIn("gssBrId", brIds)
                        .andGreaterThan("gssQty", "0");
            }
        }

        List<GaiaSdStock> stockList = sdStockMapper.selectByExample(example);

        List<YlStockData> stockDataList = Lists.newArrayList();
        String date = String.valueOf(new DateTime());
        for (GaiaSdStock stock : stockList) {
            YlStockData detail = new YlStockData();
            detail.setQuantity(stock.getGssQty());
            detail.setDrug_id(stock.getGssProId());
            detail.setStore_id(stock.getClientId() + UtilConst.GYS + stock.getGssBrId());
            detail.setCreate_time(date);
            detail.setUpdate_time(date);
            stockDataList.add(detail);
        }

        YlStockInputBean bean = new YlStockInputBean();
        bean.setStocks(stockDataList);
        String cooperation = gaiaYlCooperationDao.getCooperationByClient(client);
        return YlApiUtil.stockUpdate(cooperation, bean, syncStock);
    }

    @Override
    public JsonResult ylOrderInput(FxYlOrderInputBean bean) {
        String client = bean.getClient();
        String stoCode = bean.getStoCode();
        String orderId = bean.getOrderId();
        GetTakeAwayInData inData = new GetTakeAwayInData(client, stoCode, orderId);
        GaiaTOrderInfo orderInfo = gaiaTOrderInfoMapper.getPlatformOrderIdByOrderId(inData);
        if (orderInfo == null) {
            logger.error("药联-订单数据录入，查询不到对应的订单数据，client:{},stoCode:{},orderId:{}", client, stoCode, orderId);
            return JsonResult.fail(101, "不存在此订单");
        }
        YlOrderInputBean inputBean = new YlOrderInputBean();
        List<YlOrderBean> orders = Lists.newArrayList();
        YlOrderBean ylOrderBean = new YlOrderBean();
        ylOrderBean.setId(bean.getSaleOrderId());//销售单号
        ylOrderBean.setOrder_no(orderInfo.getPlatformsOrderId());
        ylOrderBean.setStore_id(client + UtilConst.GYS + stoCode);
        ylOrderBean.setCreate_time(DateUtil.format(orderInfo.getCreateTime(), DatePattern.NORM_DATETIME_PATTERN));
        String updateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        ylOrderBean.setUpdate_time(updateTime);
        ylOrderBean.setMember_id("");//对接方问了没回，传空串能成功
        ylOrderBean.setMember_name("");
        ylOrderBean.setMember_mobile("");
        ylOrderBean.setTotal_price(orderInfo.getOriginalPrice().toString());
        List<RebornData> orderDetailList = bean.getOrderDetail();
        List<YlOrderDetailBean> retailDetail = Lists.newArrayList();
        List<String> proIdList = orderDetailList.stream().map(r -> r.getGssdProId()).distinct().collect(Collectors.toList());
        List<GaiaProductBusiness> businessList = productBusinessMapper.selectByClientAndStoCodeAndProCode(client, stoCode, proIdList);
        Map<String, GaiaProductBusiness> productBusinessMap = businessList.stream().collect(Collectors.toMap(r -> r.getProSelfCode(), r -> r, (r, v) -> r));
        for (RebornData data : orderDetailList) {
            GaiaProductBusiness business = productBusinessMap.get(data.getGssdProId());
            YlOrderDetailBean detailBean = new YlOrderDetailBean();
            detailBean.setDrug_id(data.getGssdProId());
            detailBean.setCommon_name(data.getProCommonname());
            detailBean.setAmount(data.getGssdQty());
            detailBean.setPrice(data.getGssdPrc1());
            detailBean.setNumber(business != null ? business.getProRegisterNo() : "");//批准文号 有的商品没有
            detailBean.setForm(data.getProSpecs());
            detailBean.setPack(data.getProUnit());
            detailBean.setUnit_price(data.getGssdPrc1());
            detailBean.setBatch_number(data.getGssdBatchNo());
            detailBean.setExpirydate(data.getGssdValidDate());
            detailBean.setCode(business != null ? business.getProBarcode() : "");//条形码 取数据库第一个条形码  有的商品没有
            retailDetail.add(detailBean);
        }
        ylOrderBean.setRetailDetail(retailDetail);
        orders.add(ylOrderBean);
        inputBean.setOrders(orders);
        String cooperation = gaiaYlCooperationDao.getCooperationByClient(client);
        boolean success = YlApiUtil.orderDataInput(inputBean, cooperation);
        logger.error("药联-订单数据录入，client:{},stoCode:{},orderId:{},result:{}", client, stoCode, orderId, success);
        return success ? JsonResult.success(null, "提示：获取数据成功！") : JsonResult.fail(500, "药联订单录入失败");
    }

    @Override
    public YlResponseData commodityDataEntry(Map<String, Object> map) {
        TimeInterval timer = DateUtil.timer();
        if (StrUtil.isBlank((String) map.get("client"))) {
            throw new BusinessException("加盟商信息不可为空");
        }
        String client = (String) map.get("client");
        String cooperation = gaiaYlCooperationDao.getCooperationByClient(client);
        JsonRootBean bean = new JsonRootBean();
        RequestHead requestHead = new RequestHead();
        requestHead.setCooperation(cooperation);
        requestHead.setNonce(YlApiUtil.NONCE);
        String timestamp = getTimeStamp();
        requestHead.setTimestamp(timestamp);
        requestHead.setSign(YlApiUtil.getSign(timestamp, YlApiUtil.NONCE));
        requestHead.setTradeDate(DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN));
        bean.setRequestHead(requestHead);
        bean.setChannel("1");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        headers.add("Accept", "application/json,text/javascript,*/*;q=0.01");
        headers.add("mime-types", "application/javascript,text/css,application/json,application/xml,text/html,text/xml,text/plain");
        headers.add("min-response-size", "1024");
        List<Drugs> categorysList = new ArrayList<>();
        new Thread(() -> {
            YlResponseData response = null;
            if (CollUtil.isNotEmpty((List<String>) map.get("brIdList"))) {
                for (String brId : (List<String>) map.get("brIdList")) {
                    List<Drugs> categorys = this.productBusinessMapper.getAllProductByClientAndBrId((String) map.get("client"),brId);
                    extracted(categorys);
                    bean.setDrugs(categorys);
                    response = this.getYlResponseData(bean, restTemplate, headers, response, categorysList);

                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    logger.info("每次调用响应时间 : 用时 : " + timer.intervalRestart());
                }
            } else {
                StoreInData inData = new StoreInData();
                inData.setClientId((String) map.get("client"));
                List<StoreOutData> storeData = this.gaiaSdStoreDataMapper.getStoreData(inData);
                for (StoreOutData store : storeData) {
                    List<Drugs> categorys = this.productBusinessMapper.getAllProductByClientAndBrId((String) map.get("client"),store.getGsstBrId());
                    extracted(categorys);
                    bean.setDrugs(categorys);
                    response = this.getYlResponseData(bean, restTemplate, headers, response, categorysList);

                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    logger.info("每次调用响应时间 : 用时 : " + timer.intervalRestart());
                }
            }
        }).start();

        logger.info("总耗时 : 用时 : " + timer.intervalRestart());
        return null;
    }

    /**
     * FX查询当前门店外卖提示框是否弹出
     */
    @Override
    public boolean getTipPopByClientAndBrId(String client, String depId) {
        //订单自动计入销售	0：不自动；1：自动	； 自动则外卖订单提示不显示
        GaiaSdSystemPara para = gaiaSdSystemParaMapper.getAuthByClientAndBrId(client, depId, "ORDER_AUTO");
        return para == null ? true : "0".equals(para.getGsspPara());
    }

    @Override
    public String getMainSaleBtn(String client, String depId) {
       //ORDER_MAIN_SALE	外卖订单平台主收银是否可编辑(即按钮可不可以点击)	0：不可编辑；1：可编辑；
        GaiaSdSystemPara para = gaiaSdSystemParaMapper.getAuthByClientAndBrId(client, depId, "ORDER_MAIN_SALE");
        return para == null ? "0" : para.getGsspPara();
    }

    @Override
    public JsonResult ddkyReturnOrder(GetLoginOutData userInfo, DdkyRefundBean bean) {
        String client = userInfo.getClient();
        String stoCode = userInfo.getDepId();
        GaiaTAccount account = getAccountByClientAndStoCodeAndPlat(client, stoCode, Constants.PLATFORMS_DDKY);
        if (account == null) {
            return JsonResult.fail(101, "叮当快药退货失败，请检查账号配置正确！");
        }
        GaiaTOrderInfo orderInfo = gaiaTOrderInfoMapper.getPlatformOrderIdByOrderId(new GetTakeAwayInData(client, stoCode, bean.getOrderID()));
        if (orderInfo == null) {
            return JsonResult.fail(101, "查不到此订单数据");
        }
        bean.setOrderID(orderInfo.getPlatformsOrderId());
        bean.setCreatedBy(userInfo.getLoginName());
        boolean success = DdkyApiUtil.returnOrder(account.getCompanyId(), account.getAppSecret(), bean);
        if (success) {
            //成功后修改订单状态
            gaiaTOrderInfoMapper.updateDdkyStatus(orderInfo.getOrderId(), "4");
            return JsonResult.success("叮当快药退货成功", "提示：获取数据成功！");
        }
        return JsonResult.fail(101, "叮当快药退货失败");
    }

    /**
     * 叮当快药全量同步商品
     * 实测：只能撑得住100个一次
     * @param client
     * @param stoCode
     * @param proId 商品编码， 这个参数有值即为单个新品同步
     * @return
     */
    @Override
    public String goodsSync(String client, String stoCode, String proId) {
        GaiaTAccount account = getAccountByClientAndStoCodeAndPlat(client, stoCode, Constants.PLATFORMS_DDKY);
        if (account == null) {
            return "检查account表配置";
        }
        String companyId = account.getCompanyId();
        String appSecret = account.getAppSecret();
        String appStoId = account.getAppStoId();
        List<DdkyStockUpdateDetailBean> list = productBusinessMapper.getDdkyProductByClientAndBrId(client, stoCode, proId);
        if (CollUtil.isEmpty(list)) {
            return "当前门店没有任何商品";
        }
        DdkyStockUpdateBean bean = new DdkyStockUpdateBean();
        bean.setShopId(Long.parseLong(appStoId));

        //沙雕接口用GET同步商品，让分页同步就行，几百条都没事
        int size = list.size();
        List<Boolean> resultList = new ArrayList<>(size / DdkyApiUtil.SYNC_GOODS_MAX + 1);
        logger.info("叮当快药，全量同步商品，client:{}, stoCode:{}, totalSize:{}", client, stoCode, size);
        if (size > DdkyApiUtil.SYNC_GOODS_MAX) {
            int page = size / DdkyApiUtil.SYNC_GOODS_MAX;
            int left = size % DdkyApiUtil.SYNC_GOODS_MAX;
            int total = page;
            if (left > 0) {
                total++;
            }
            for (int i = 0; i < total; i++) {
                List<DdkyStockUpdateDetailBean> tempList;
                if (left > 0 && i == total - 1) {
                    tempList = list.subList(DdkyApiUtil.SYNC_GOODS_MAX * i, DdkyApiUtil.SYNC_GOODS_MAX * i + left);
                } else {
                    tempList = list.subList(DdkyApiUtil.SYNC_GOODS_MAX * i, DdkyApiUtil.SYNC_GOODS_MAX * (i + 1));
                }
                bean.setGoodsList(tempList);
                boolean success = DdkyApiUtil.syncGoods(companyId, appSecret, bean);
                resultList.add(success);
            }
        } else {
            bean.setGoodsList(list);
            boolean success = DdkyApiUtil.syncGoods(companyId, appSecret, bean);
            resultList.add(success);
        }
        logger.info("叮当快药，全量同步商品，client:{}, stoCode:{}, resultList:{}", client, stoCode, JSON.toJSONString(resultList));
        return resultList.contains(false) ? "全量同步失败" : "全量同步成功";
    }

    /**
     * 填充参数
     * @param categorys
     */
    private void extracted(List<Drugs> categorys) {
        categorys.forEach(drugs -> {
            drugs.setCategory_id(new String[]{"1"});
            drugs.setTags(new String[]{"1"});
        });
    }

    private YlResponseData getYlResponseData(JsonRootBean bean, RestTemplate restTemplate, HttpHeaders headers, YlResponseData response, List<Drugs> categorysList) {
        try {
            logger.info("药联-上传商品数据, inData:{}", JSON.toJSONString(bean));
            HttpEntity httpEntity = new HttpEntity(bean, headers);
            ResponseEntity<YlResponseData> responseEntity = restTemplate.postForEntity(YlApiUtil.SERVER_URL_PRD, httpEntity, YlResponseData.class);
            response = responseEntity.getBody();
            logger.info("药联-上传商品数据，result:{} ", JSON.toJSONString(response));
        } catch (Exception e) {
            logger.error("调用药联 商品数据录入接口 异常 : ", e);
        }
        categorysList.clear();
        return response;
    }
}
