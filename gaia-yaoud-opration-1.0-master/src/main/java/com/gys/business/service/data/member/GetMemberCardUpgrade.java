package com.gys.business.service.data.member;

import com.gys.business.mapper.entity.GaiaSdMemberClass;
import com.gys.business.service.data.GetQueryMemberOutData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/12 17:09
 * @Version 1.0.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetMemberCardUpgrade {
    //  会员信息
    private GetQueryMemberOutData memberInfo;
    //  会员
    private List<GaiaSdMemberClass> memberClass;
    //  0：不升级；1：自动升级；2：手动升级
    private String flag;
    //  0：不扣减积分 1：扣减积分
    private String type;
}
