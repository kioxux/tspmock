package com.gys.business.service;

import com.gys.business.service.data.*;
import org.apache.ibatis.javassist.bytecode.LineNumberAttribute;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface InventoryReportService {

    List<InventoryReportOutData> selectInventoyResportList(InventoryReportInData inData);

    /**
     * 盘点差异结果查询
     * @param inData
     * @return
     */
    List<DifferenceResultQueryOutData> getDifferenceResultQuery(DifferenceResultQueryInVo inData);

    /**
     * 盘点差异结果明细查询
     * @param inData
     * @return
     */
    List<DifferenceResultDetailedQueryOutData> getDifferenceResultDetailedQuery(DifferenceResultQueryInVo inData);

    /**
     * 门店盘点单据查询
     * @param inData
     * @return
     */
    List<InventoryDocumentsOutData> inventoryDocumentQuery(DifferenceResultQueryInVo inData);

    /**
     * 盘点差异详情
     * @param inData
     * @return
     */
    List<InventoryDetailsData> storeInventoryDetails(InventoryReportInData inData);
}
