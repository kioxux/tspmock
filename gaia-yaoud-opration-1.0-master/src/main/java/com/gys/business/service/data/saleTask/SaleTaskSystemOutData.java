package com.gys.business.service.data.saleTask;

        import io.swagger.annotations.ApiModel;
        import io.swagger.annotations.ApiModelProperty;
        import lombok.Data;

@Data
@ApiModel(value = "药德运维月度计划返回参数")
public class SaleTaskSystemOutData {
    private String staskTaskId;
    @ApiModelProperty(value = "任务名称 新建修改都是同一个名字")
    private String staskTaskName;
    @ApiModelProperty(value = "任务时间 起始日期")
    private String staskStartDate;
    @ApiModelProperty(value = "任务时间 结束日期")
    private String staskEndDate;
    @ApiModelProperty(value = "状态名称")
    private String staskSplanSta;
    @ApiModelProperty(value = "状态(S已保存，P已下发)")
    private String taskStatus;
    @ApiModelProperty(value = "创建人姓名")
    private String userName;
    @ApiModelProperty(value = "期间天数")
    private String monthDays;
    @ApiModelProperty(value = "推送状态 0 未推送 1 已推送")
    private String pushStatus;
    @ApiModelProperty(value = "创建时间")
    private String wtaskCreDate;
}
