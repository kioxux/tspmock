package com.gys.business.service.data;

import lombok.Data;

/**
 * @author Zhangchi
 * @since 2021/11/18/10:59
 */
@Data
public class PriceRangeInData {
    /**
     * 入参客户列表
     */
    private String[] clientId;

    private String client;
}
