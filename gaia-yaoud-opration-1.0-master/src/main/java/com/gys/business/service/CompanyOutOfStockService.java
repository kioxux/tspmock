package com.gys.business.service;

import com.gys.business.controller.app.form.DeleteForm;
import com.gys.business.controller.app.form.OperateForm;
import com.gys.business.controller.app.form.OutStockForm;
import com.gys.business.controller.app.form.PushForm;
import com.gys.business.controller.app.vo.CompanyOutStockVO;
import com.gys.business.controller.app.vo.DcPushVO;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/9 18:14
 **/
public interface CompanyOutOfStockService {

    JsonResult getValidProduct(ValidProductInData inData);

    JsonResult getValidProductChart(CompanyExpiryProChartInData inData);

    JsonResult getStockReport(CompanyStoreReportInData inData);

    JsonResult getCompanyOutOfStock(OutOfStockInData inData);

    JsonResult pushStoreOutOfStock(UpdateParaByStoreListInData inData);

    JsonResult choicePushStoreOutOfStock(UpdateParaByStoreListInData inData);

    CompanyOutStockVO getOutOfStockList(OutStockForm outStockForm);

    void deleteProduct(DeleteForm deleteForm);

    List<ProductRank> getCategoryModelList(String client, Integer analyseDay);

    CompanyOutOfStockInfoOutData getDcStoreDetailList(OperateForm operateForm);

    JsonResult getPushStoreHistory(PushStoreHistoryInData inData);

    JsonResult getPushWmsHistory(PushStoreHistoryInData inData);

    DcPushVO push(PushForm pushForm);

    List<DcReplenishVO> replenishList(DcReplenishForm replenishForm);

    List<DcReplenishDetailVO> replenishDetail(DcReplenishForm replenishForm);

    JsonResult getProductMidTypeReport(ProductMidTypeSaleInData inData);

    JsonResult getStoreStockListReport(AppStoreSaleInData inData);
}
