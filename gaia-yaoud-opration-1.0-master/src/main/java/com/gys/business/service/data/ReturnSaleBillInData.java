package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class ReturnSaleBillInData {
    /**
     * 退货单号
     */
    private String client;

    /**
     * 退货单号
     */
    private String brId;
    /**
     * 退货单号
     */
    @ApiModelProperty("退货单号")
    private String returnBillNo;

    /**
     * 单据状态
     */
    @ApiModelProperty("单据状态")
    private Boolean status;
}
