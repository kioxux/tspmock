package com.gys.business.service.data.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc: 设备维护记录请求实体
 * @author: ryan
 * @createTime: 2021/6/24 17:10
 **/
@Data
@ApiModel
public class AddDeviceMaintenanceForm {
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(hidden = true)
    private String deviceSite;
    @ApiModelProperty(value = "门店名称")
    private String storeName;
    @ApiModelProperty(value = "设备编号")
    private String deviceNo;
    @ApiModelProperty(value = "设备名称")
    private String deviceName;
    @ApiModelProperty(value = "设备型号")
    private String deviceSpec;
    @ApiModelProperty(value = "维护内容")
    private String checkRemarks;
    @ApiModelProperty(value = "检查情况")
    private String checkResult;
    @ApiModelProperty(value = "调控措施")
    private String checkStep;
}
