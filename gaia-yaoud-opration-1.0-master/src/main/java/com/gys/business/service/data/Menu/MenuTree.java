package com.gys.business.service.data.Menu;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/3/15 10:37
 * @Version 1.0.0
 **/
@Data
public class MenuTree {

    @ApiModelProperty(value = "序号")
    private Long id;


    /**
     * 菜单编码
     */
    @NotNull(message = "code不能为空")
    @ApiModelProperty(value = "菜单编码")
    private String code;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 标题
     */
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 路由/跳转地址
     */
    @ApiModelProperty(value = "路由/跳转地址")
    private String path;

    /**
     * 页面地址
     */
    @ApiModelProperty(value = "页面地址")
    private String pagePath;

    /**
     * 图标
     */
    @ApiModelProperty(value = "图标")
    private String icon;

    /**
     * 类型（1模块 2页面）
     */
    @ApiModelProperty(value = "类型（1模块 2页面")
    private Integer type;

    /**
     * 父节点（空为最高节点）
     */
    @ApiModelProperty(value = "父节点（空为最高节点")
    private Long parentId;

    /**
     * 节点数组（从一级开始）
     */
    @ApiModelProperty(value = "节点数组（从一级开始）")
    private String parentIdList;

    /**
     * 状态(0.禁用 1.启用 2.删除 )
     */
    @ApiModelProperty(value = "状态(1.启用 2.删除 )")
    private String status;

    /**
     * 1缓存  0不缓存
     */
    @ApiModelProperty(value = "true缓存 false不缓存")
    private Boolean keepAlive;

    /**
     * 1隐藏 0不隐藏
     */
    @Column(name = "true隐藏 false不隐藏")
    private Boolean detaisPage;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer seq;
    
    List<MenuTree> lists;
}
