package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetWfCreateLossCompadmInData {
    private int index = 0;
    private String lossDate = "";
    private String lossOrder = "";
    private String storeId = "";
    private String proCode = "";
    private String proName = "";
    private String proSpecs = "";
    private String batch = "";
    private String batchCost = "";
    private String tax = "";
    private String wmaCost = "";
    private String wmaCostTax = "";
    private String qty = "";
    private String reason = "";
    private String sccj = "";
    private String validUntil = "";
    private String billDate = "";
    private String serial = "";
}
