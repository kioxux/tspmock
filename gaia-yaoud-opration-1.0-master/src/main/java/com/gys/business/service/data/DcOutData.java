package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DcOutData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "连锁总部ID")
    private String compadmId;
    @ApiModelProperty(value = "连锁总部名称")
    private String compadmName;
    @ApiModelProperty(value = "统一社会信用代码")
    private String compadmNo;
    @ApiModelProperty(value = "DC编码")
    private String dcCode;
}
