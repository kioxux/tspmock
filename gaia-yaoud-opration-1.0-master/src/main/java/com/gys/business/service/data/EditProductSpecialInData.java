package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class EditProductSpecialInData implements Serializable {

    /**
     * 药德商品编码
     */
    @ApiModelProperty(value="药德商品编码")
    private String proCode;

    /**
     * 省
     */
    @ApiModelProperty(value="省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value="市")
    private String city;

    /**
     * 类型 1-季节，2-品牌
     */
    @ApiModelProperty(value="类型 1-季节，2-品牌")
    private String proType;

    /**
     * 标识 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌
     */
    @ApiModelProperty(value="标识 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌")
    private String oldProFlag;

    /**
     * 原状态 0-正常 1-停用
     */
    @ApiModelProperty(value="标识 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌")
    private String oldProStatus;

    /**
     * 修改后标识 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌
     */
    @ApiModelProperty(value="标识 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌")
    private String newProFlag;

    /**
     * 修改后状态 0-正常 1-停用
     */
    @ApiModelProperty(value="标识 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌")
    private String newProStatus;

    /**
     * 修改人
     */
    @ApiModelProperty(value="修改人")
    private String updateUser;
}
