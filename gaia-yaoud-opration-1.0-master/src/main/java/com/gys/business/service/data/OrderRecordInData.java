package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/12/24
 */
@Data
public class OrderRecordInData implements Serializable {
    private static final long serialVersionUID = 5516658373680301675L;


    /**
     * 加盟商
     */

    private String client;

    /**
     * 店号
     */
    private String brId;
    /**
     * 订购单号
     */
    private String voucherId;

    /**
     * 支付金额
     */
    private String gsphAmt;

    /**
     * 顾客手机
     */
    private String costoerMobile;

    /**
     * 起始日期
     */
    @NotBlank(message = "起始日期不可为空")
    private String startDate;

    /**
     * 结束日期
     */
    @NotBlank(message = "结束日期不可为空")
    private String endDate;

    /**
     * 订购日期
     */
    private String gsphDate;
}
