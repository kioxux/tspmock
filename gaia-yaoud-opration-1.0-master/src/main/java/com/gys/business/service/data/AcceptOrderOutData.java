package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AcceptOrderOutData {
    @ApiModelProperty(value = "收货单号")
    private String voucherId;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "商品通用名")
    private String proCommonName;
    @ApiModelProperty(value = "商品编码")
    private String proId;
    @ApiModelProperty(value = "收货日期")
    private String acceptDate;
    @ApiModelProperty(value = "收货行金额")
    private BigDecimal voucherAmt;
    @ApiModelProperty(value = "收货数量")
    private BigDecimal recipientQty;
    @ApiModelProperty(value = "零售价")
    private BigDecimal proPrice;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "生产厂家")
    private String factoryName;
    @ApiModelProperty(value = "有效期")
    private String validDate;
    @ApiModelProperty(value = "供应商")
    private String supName;
    @ApiModelProperty(value = "供应商编码")
    private String supCode;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    @ApiModelProperty(value = "收货价")
    private BigDecimal acceptPrice;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "收货人姓名")
    private String userNames;

    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "门店名称")
    private String stoShortName;

        //折扣率
    private BigDecimal discountRate;
    //折后单价
    private BigDecimal discountPrice;
    //折扣额
    private BigDecimal gsadRowRemark;
    @ApiModelProperty(value = "医保机构定点编码")
    private String stoYbCode;
    @ApiModelProperty(value = "国家贯标编码")
    private String medProductCode;
    @ApiModelProperty(value = "发票价")
    private BigDecimal invoicePrice;
    @ApiModelProperty(value = "发票额")
    private BigDecimal invoiceAmount;
    @ApiModelProperty(value = "营业员名称")
    private String busEmpName;

    //冷链标记：0否，1是
    @ApiModelProperty(value = "冷链标记：0否，1是")
    private String coldChainFlag;

    //冷链信息
    private ColdchainInfo coldchainInfo;

    @ApiModelProperty(value = "商品描述")
    private String proDepict;
}
