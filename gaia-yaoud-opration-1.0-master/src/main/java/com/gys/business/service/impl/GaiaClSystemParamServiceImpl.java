package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaClSystemParaMapper;
import com.gys.business.mapper.GaiaSdSystemParaMapper;
import com.gys.business.mapper.entity.GaiaSdEnv;
import com.gys.business.service.GaiaClSystemParamService;
import com.gys.business.service.data.UserInData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @desc:
 * @author: RULAISZ
 * @createTime: 2021/12/30 18:48
 */
@Service
public class GaiaClSystemParamServiceImpl implements GaiaClSystemParamService {

    @Autowired
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;

    @Autowired
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;

    @Override
    public GaiaSdEnv getEnv(UserInData inData) {
        GaiaSdEnv env = gaiaSdSystemParaMapper.getEnv(inData.getClient(), inData.getDepId());
        return env;
    }
}
