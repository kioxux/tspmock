package com.gys.business.service.salesReceiptsPriceUtil;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.entity.GaiaSdPromUnitarySet;
import com.gys.business.service.data.PromSingleInData;

import java.math.BigDecimal;

public class PromSingle {

    public static BigDecimal gsphPart1Repeat1(PromSingleInData singleInData) {
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = singleInData.getGaiaSdPromUnitarySet();
        BigDecimal money = singleInData.getMoney();
        BigDecimal normalPrice = singleInData.getNormalPrice();
        //销售数量
        BigDecimal num = new BigDecimal(String.valueOf(singleInData.getNum()));
        //达到数量1
        BigDecimal gspusQty1 = new BigDecimal(String.valueOf(Float.parseFloat(gaiaSdPromUnitarySet.getGspusQty1())));
        BigDecimal normalPriceTemp;
        BigDecimal Rebate1;
        if (num.compareTo(gspusQty1) > -1) {
            normalPriceTemp = new BigDecimal(0);
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc1())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc1();
                System.out.println("阶段一 : " + normalPriceTemp);
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate1())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate1())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
                System.out.println("阶段一 : " + normalPriceTemp);
            }

            BigDecimal[] decimals = num.divideAndRemainder(gspusQty1);

            if (decimals[0].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPriceTemp.multiply(decimals[0]);
            }
            System.out.println(money);
            if (decimals[1].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPrice.multiply(decimals[1]).add(money);
            }

            System.out.println(money);
            return money;
        } else {
            money = money.add(normalPrice.multiply(num));
        }
        return money;
    }

    public static BigDecimal gsphPart2Repeat1(PromSingleInData singleInData) {
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = singleInData.getGaiaSdPromUnitarySet();
        BigDecimal money = singleInData.getMoney();
        BigDecimal normalPrice = singleInData.getNormalPrice();
        //销售数量
        BigDecimal num = new BigDecimal(String.valueOf(singleInData.getNum()));
        //达到数量1
        BigDecimal gspusQty1 = new BigDecimal(String.valueOf(Float.parseFloat(gaiaSdPromUnitarySet.getGspusQty1())));
        //达到数量2
        BigDecimal gspusQty2 = new BigDecimal(String.valueOf(Float.parseFloat(gaiaSdPromUnitarySet.getGspusQty2())));
        BigDecimal normalPriceTemp;
        BigDecimal Rebate1;
        if (num.compareTo(gspusQty2) > -1) {
            normalPriceTemp = new BigDecimal(0);
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc2())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc2();
                System.out.println("阶段2 : " + normalPriceTemp);
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate2())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate2())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
                System.out.println("阶段2 : " + normalPriceTemp);
            }

            BigDecimal[] decimals = num.divideAndRemainder(gspusQty2);

            if (decimals[0].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPriceTemp.multiply(decimals[0]);
            }
            System.out.println(money);
            if (decimals[1].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPrice.multiply(decimals[1]).add(money);
            }

            System.out.println(money);
            return money;
        } else if (num.compareTo(gspusQty1) > -1) {
            normalPriceTemp = new BigDecimal(0);
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc1())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc1();
                System.out.println("阶段二中1 : " + normalPriceTemp);
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate1())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate1())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
                System.out.println("阶段二中1 : " + normalPriceTemp);
            }

            BigDecimal[] decimals = num.divideAndRemainder(gspusQty1);

            if (decimals[0].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPriceTemp.multiply(decimals[0]);
            }
            System.out.println(money);
            if (decimals[1].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPrice.multiply(decimals[1]).add(money);
            }
            System.out.println(money);
            return money;
        } else {
            money = money.add(normalPrice.multiply(num));
        }
        return money;
    }

    /**
     * 阶段三
     * 循环生效
     *
     * @param singleInData
     * @return
     */
    public static BigDecimal gsphPart3Repeat1(PromSingleInData singleInData) {
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = singleInData.getGaiaSdPromUnitarySet();

        BigDecimal money = singleInData.getMoney();
        //单品价格
        BigDecimal normalPrice = singleInData.getNormalPrice();
        //销售数量
        BigDecimal num = new BigDecimal(String.valueOf(singleInData.getNum()));
        System.out.println("销售数量" + num);
        //达到数量1
        BigDecimal gspusQty1 = new BigDecimal(String.valueOf(Float.parseFloat(gaiaSdPromUnitarySet.getGspusQty1())));
        //达到数量2
        BigDecimal gspusQty2 = new BigDecimal(String.valueOf(Float.parseFloat(gaiaSdPromUnitarySet.getGspusQty2())));
        //达到数量3
        BigDecimal gspusQty3 = new BigDecimal(String.valueOf(Float.parseFloat(gaiaSdPromUnitarySet.getGspusQty3())));

        BigDecimal normalPriceTemp;
        BigDecimal Rebate1;
        if (num.compareTo(gspusQty3) > -1) {
            normalPriceTemp = new BigDecimal(0);
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc3())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc3();
                System.out.println("阶段三 : " + normalPriceTemp);
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate3())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate3())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
                System.out.println("阶段三 : " + normalPriceTemp);
            }

            BigDecimal[] decimals = num.divideAndRemainder(gspusQty3);

            if (decimals[0].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPriceTemp.multiply(decimals[0]);
            }
            System.out.println(money);
            if (decimals[1].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPrice.multiply(decimals[1]).add(money);
            }

            System.out.println(money);
            return money;

        } else if ( num.compareTo(gspusQty2) > -1) {
            normalPriceTemp = new BigDecimal(0);
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc2())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc2();
                System.out.println("阶段三中2 : " + normalPriceTemp);
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate2())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate2())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
                System.out.println("阶段三中2 : " + normalPriceTemp);
            }

            BigDecimal[] decimals = num.divideAndRemainder(gspusQty2);

            if (decimals[0].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPriceTemp.multiply(decimals[0]);
            }
            System.out.println(money);
            if (decimals[1].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPrice.multiply(decimals[1]).add(money);
            }

            System.out.println(money);
            return money;
        } else if (num.compareTo(gspusQty1) > -1) {
            normalPriceTemp = new BigDecimal(0);
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc1())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc1();
                System.out.println("阶段三中1 : " + normalPriceTemp);
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate1())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate1())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
                System.out.println("阶段三中1 : " + normalPriceTemp);
            }

            BigDecimal[] decimals = num.divideAndRemainder(gspusQty1);

            if (decimals[0].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPriceTemp.multiply(decimals[0]);
            }
            System.out.println(money);
            if (decimals[1].compareTo(BigDecimal.ZERO) > -1) {
                money = normalPrice.multiply(decimals[1]).add(money);
            }
            System.out.println(money);
            return money;
        } else {
            money = money.add(normalPrice.multiply(num));
        }
        return money;
    }

    public static BigDecimal gsphPart3Repeat2(PromSingleInData singleInData) {
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = singleInData.getGaiaSdPromUnitarySet();
        BigDecimal normalPrice = singleInData.getNormalPrice();
        float num = singleInData.getNum();
        BigDecimal normalPriceTemp = new BigDecimal(0);
        new BigDecimal(0);
        int gspusQty3 = Integer.valueOf(gaiaSdPromUnitarySet.getGspusQty3());
        int gspusQty2 = Integer.valueOf(gaiaSdPromUnitarySet.getGspusQty2());
        int gspusQty1 = Integer.valueOf(gaiaSdPromUnitarySet.getGspusQty1());
        BigDecimal money;
        BigDecimal Rebate1;
        if (num >= (float) gspusQty3) {
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc3())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc3();
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate3())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate3())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
            }

            money = normalPriceTemp.multiply(new BigDecimal((double) num));
        } else if (num >= (float) gspusQty2) {
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc2())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc2();
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate2())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate2())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
            }

            money = normalPriceTemp.multiply(new BigDecimal((double) num));
        } else if (num >= (float) gspusQty1) {
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc1())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc1();
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate1())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate1())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
            }

            money = normalPriceTemp.multiply(new BigDecimal((double) num));
        } else {
            money = normalPrice.multiply(new BigDecimal((double) num));
        }

        return money;
    }

    public static BigDecimal gsphPart2Repeat2(PromSingleInData singleInData) {
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = singleInData.getGaiaSdPromUnitarySet();
        BigDecimal normalPrice = singleInData.getNormalPrice();
        float num = singleInData.getNum();
        BigDecimal normalPriceTemp = new BigDecimal(0);
        new BigDecimal(0);
        int gspusQty2 = Integer.valueOf(gaiaSdPromUnitarySet.getGspusQty2());
        int gspusQty1 = Integer.valueOf(gaiaSdPromUnitarySet.getGspusQty1());
        BigDecimal money;
        BigDecimal Rebate1;
        if (num >= (float) gspusQty2) {
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc2())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc2();
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate2())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate2())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
            }

            money = normalPriceTemp.multiply(new BigDecimal((double) num));
        } else if (num >= (float) gspusQty1) {
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc1())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc1();
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate1())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate1())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
            }

            money = normalPriceTemp.multiply(new BigDecimal((double) num));
        } else {
            money = normalPrice.multiply(new BigDecimal((double) num));
        }

        return money;
    }

    public static BigDecimal gsphPart1Repeat2(PromSingleInData singleInData) {
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = singleInData.getGaiaSdPromUnitarySet();
        BigDecimal normalPrice = singleInData.getNormalPrice();
        float num = singleInData.getNum();
        BigDecimal normalPriceTemp = new BigDecimal(0);
        new BigDecimal(0);
        int gspusQty1 = Integer.valueOf(gaiaSdPromUnitarySet.getGspusQty1());
        BigDecimal money;
        if (num >= (float) gspusQty1) {
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc1())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc1();
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate1())) {
                BigDecimal Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate1())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
            }

            money = normalPriceTemp.multiply(new BigDecimal((double) num));
        } else {
            money = normalPrice.multiply(new BigDecimal((double) num));
        }

        return money;
    }


    public static BigDecimal gsphPart3Repeat3(PromSingleInData singleInData) {
        System.out.println("singleInData : " + singleInData);
        GaiaSdPromUnitarySet gaiaSdPromUnitarySet = singleInData.getGaiaSdPromUnitarySet();

        BigDecimal normalPrice = singleInData.getNormalPrice();
        //数量
        float num = singleInData.getNum();
        BigDecimal normalPriceTemp = new BigDecimal(0);
        new BigDecimal(0);
        //促销三
        int gspusQty3 = Integer.valueOf(gaiaSdPromUnitarySet.getGspusQty3());
        //促销二
        int gspusQty2 = Integer.valueOf(gaiaSdPromUnitarySet.getGspusQty2());
        //促销一
        int gspusQty1 = Integer.valueOf(gaiaSdPromUnitarySet.getGspusQty1());

        BigDecimal money;
        BigDecimal Rebate1;
        if (num >= (float) gspusQty3) {
            //生成促销价3
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc3())) {
                //促销三
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc3();
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate3())) {
                //折扣三
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate3())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
            }
            //催销三的 数量 乘以 促销三的价格
            money = new BigDecimal(gspusQty3).multiply(normalPriceTemp);

            BigDecimal decimal = new BigDecimal(num).subtract(new BigDecimal(gspusQty3));
            BigDecimal bigDecimal = normalPrice.multiply(decimal);
            System.out.println(bigDecimal);
            return money = money.add(bigDecimal);
        } else if (num >= (float) gspusQty2) {
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc2())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc2();
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate2())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate2())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
            }

            money = normalPriceTemp.multiply(new BigDecimal(gspusQty2));
            Rebate1 = normalPrice.multiply(new BigDecimal((double) (num - (float) gspusQty2)));
            return money = money.add(Rebate1);
        } else if (num >= (float) gspusQty1) {
            if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusPrc1())) {
                normalPriceTemp = gaiaSdPromUnitarySet.getGspusPrc1();
            } else if (ObjectUtil.isNotEmpty(gaiaSdPromUnitarySet.getGspusRebate1())) {
                Rebate1 = (new BigDecimal(gaiaSdPromUnitarySet.getGspusRebate1())).divide(new BigDecimal(100));
                normalPriceTemp = normalPrice.multiply(Rebate1);
            }

            money = normalPriceTemp.multiply(new BigDecimal(gspusQty1));
            Rebate1 = normalPrice.multiply(new BigDecimal((double) (num - (float) gspusQty1)));
            money = money.add(Rebate1);
        } else {
            money = normalPrice.multiply(new BigDecimal((double) num));
        }

        return money;
    }
}
