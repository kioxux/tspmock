//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;
import java.util.List;

public class GetProductBaseInData implements Serializable {
    private static final long serialVersionUID = -8274059234243274401L;
    private Long id;
    private String productName;
    private Integer productNum;
    private List<Long> ids;
    protected int pageNum;
    protected int pageSize;

    public GetProductBaseInData() {
    }

    public Long getId() {
        return this.id;
    }

    public String getProductName() {
        return this.productName;
    }

    public Integer getProductNum() {
        return this.productNum;
    }

    public List<Long> getIds() {
        return this.ids;
    }

    public int getPageNum() {
        return this.pageNum;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setProductName(final String productName) {
        this.productName = productName;
    }

    public void setProductNum(final Integer productNum) {
        this.productNum = productNum;
    }

    public void setIds(final List<Long> ids) {
        this.ids = ids;
    }

    public void setPageNum(final int pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final int pageSize) {
        this.pageSize = pageSize;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetProductBaseInData)) {
            return false;
        } else {
            GetProductBaseInData other = (GetProductBaseInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$id = this.getId();
                Object other$id = other.getId();
                if (this$id == null) {
                    if (other$id != null) {
                        return false;
                    }
                } else if (!this$id.equals(other$id)) {
                    return false;
                }

                Object this$productName = this.getProductName();
                Object other$productName = other.getProductName();
                if (this$productName == null) {
                    if (other$productName != null) {
                        return false;
                    }
                } else if (!this$productName.equals(other$productName)) {
                    return false;
                }

                Object this$productNum = this.getProductNum();
                Object other$productNum = other.getProductNum();
                if (this$productNum == null) {
                    if (other$productNum != null) {
                        return false;
                    }
                } else if (!this$productNum.equals(other$productNum)) {
                    return false;
                }

                label46: {
                    Object this$ids = this.getIds();
                    Object other$ids = other.getIds();
                    if (this$ids == null) {
                        if (other$ids == null) {
                            break label46;
                        }
                    } else if (this$ids.equals(other$ids)) {
                        break label46;
                    }

                    return false;
                }

                if (this.getPageNum() != other.getPageNum()) {
                    return false;
                } else if (this.getPageSize() != other.getPageSize()) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetProductBaseInData;
    }

    public int hashCode() {

        int result = 1;
        Object $id = this.getId();
        result = result * 59 + ($id == null ? 43 : $id.hashCode());
        Object $productName = this.getProductName();
        result = result * 59 + ($productName == null ? 43 : $productName.hashCode());
        Object $productNum = this.getProductNum();
        result = result * 59 + ($productNum == null ? 43 : $productNum.hashCode());
        Object $ids = this.getIds();
        result = result * 59 + ($ids == null ? 43 : $ids.hashCode());
        result = result * 59 + this.getPageNum();
        result = result * 59 + this.getPageSize();
        return result;
    }

    public String toString() {
        return "GetProductBaseInData(id=" + this.getId() + ", productName=" + this.getProductName() + ", productNum=" + this.getProductNum() + ", ids=" + this.getIds() + ", pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ")";
    }
}
