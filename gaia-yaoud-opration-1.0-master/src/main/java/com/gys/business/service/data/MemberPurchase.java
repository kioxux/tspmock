package com.gys.business.service.data;


import lombok.Data;

/**
 * 禁止采购
 */
@Data
public class MemberPurchase {
    private String purchase;
}
