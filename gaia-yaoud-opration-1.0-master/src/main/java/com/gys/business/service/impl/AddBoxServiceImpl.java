//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import com.gys.business.mapper.GaiaProductBusinessMapper;
import com.gys.business.mapper.GaiaSdIndrawerDMapper;
import com.gys.business.mapper.GaiaSdIndrawerHMapper;
import com.gys.business.mapper.GaiaSdProductPriceMapper;
import com.gys.business.mapper.entity.GaiaProductBusiness;
import com.gys.business.mapper.entity.GaiaSdIndrawerD;
import com.gys.business.mapper.entity.GaiaSdIndrawerH;
import com.gys.business.mapper.entity.GaiaSdProductPrice;
import com.gys.business.service.AddBoxService;
import com.gys.business.service.data.AddAddBoxOutData;
import com.gys.business.service.data.AddBoxDetailOutData;
import com.gys.business.service.data.AddBoxInfoOutData;
import com.gys.business.service.data.GaiaSdIndrawerDOutData;
import com.gys.business.service.data.GetOneAddBoxOutData;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AddBoxServiceImpl implements AddBoxService {
    @Autowired
    private GaiaSdIndrawerHMapper gaiaSdIndrawerHMapper;
    @Autowired
    private GaiaSdIndrawerDMapper gaiaSdIndrawerDMapper;
    @Autowired
    private GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Autowired
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    public AddBoxServiceImpl() {
    }

    public GetOneAddBoxOutData getOneAddBox(AddBoxInfoOutData inData) {
        GaiaSdIndrawerH sdIndrawerH = new GaiaSdIndrawerH();
        sdIndrawerH.setClientId(inData.getClientId());
        sdIndrawerH.setGsihVoucherId(inData.getGsihVoucherId());
        sdIndrawerH.setGsihBrId(inData.getBrId());
        sdIndrawerH = (GaiaSdIndrawerH)this.gaiaSdIndrawerHMapper.selectOne(sdIndrawerH);
        if ("1".equals(sdIndrawerH.getGsihStatus())) {
            sdIndrawerH.setGsihStatus("已装斗");
        } else if ("0".equals(sdIndrawerH.getGsihStatus())) {
            sdIndrawerH.setGsihStatus("未装斗");
        }

        GetOneAddBoxOutData getOneAddBoxOutData = new GetOneAddBoxOutData();
        getOneAddBoxOutData.setGaiaSdIndrawerH(sdIndrawerH);
        GaiaSdIndrawerD sdIndrawerD = new GaiaSdIndrawerD();
        sdIndrawerD.setClientId(inData.getClientId());
        sdIndrawerD.setGsidVoucherId(inData.getGsihVoucherId());
        List<GaiaSdIndrawerD> sdIndrawerDList = this.gaiaSdIndrawerDMapper.select(sdIndrawerD);
        List<GaiaSdIndrawerDOutData> sdIndrawerDOutDataList = new ArrayList();

        for(int i = 0; i < sdIndrawerDList.size(); ++i) {
            GaiaSdIndrawerD item = (GaiaSdIndrawerD)sdIndrawerDList.get(i);
            GaiaSdIndrawerDOutData outData = new GaiaSdIndrawerDOutData();
            BeanUtils.copyProperties(item, outData);
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            gaiaProductBusiness.setClient(inData.getClientId());
            gaiaProductBusiness.setProSite(inData.getBrId());
            gaiaProductBusiness.setProSelfCode(item.getGsidProId());
            gaiaProductBusiness.setProStatus("0");
            gaiaProductBusiness = (GaiaProductBusiness)this.gaiaProductBusinessMapper.selectOne(gaiaProductBusiness);
            outData.setGsidProName(gaiaProductBusiness.getProName());
            outData.setProPlace(gaiaProductBusiness.getProPlace());
            outData.setProForm(gaiaProductBusiness.getProForm());
            outData.setProFactoryName(gaiaProductBusiness.getProFactoryName());
            outData.setProSpecs(gaiaProductBusiness.getProSpecs());
            outData.setProUnit(gaiaProductBusiness.getProUnit());
            GaiaSdProductPrice gaiaSdProductPrice = new GaiaSdProductPrice();
            gaiaSdProductPrice.setClientId(inData.getClientId());
            gaiaSdProductPrice.setGsppProId(item.getGsidProId());
            gaiaSdProductPrice.setGsppBrId(inData.getBrId());
            gaiaSdProductPrice = (GaiaSdProductPrice)this.gaiaSdProductPriceMapper.selectOne(gaiaSdProductPrice);
            DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
            outData.setNormalPrice(decimalFormat.format(gaiaSdProductPrice.getGsppPriceNormal()));
            outData.setGsadStockStatus("正常");
            outData.setProRegisterNo(gaiaProductBusiness.getProRegisterNo());
            sdIndrawerDOutDataList.add(outData);
        }

        getOneAddBoxOutData.setGaiaSdIndrawerDList(sdIndrawerDOutDataList);
        return getOneAddBoxOutData;
    }

    public List<AddBoxInfoOutData> getAddBox(AddBoxInfoOutData inData) {
        List<AddBoxInfoOutData> outData = this.gaiaSdIndrawerHMapper.getAddBox(inData);

        for(int i = 0; i < outData.size(); ++i) {
            ((AddBoxInfoOutData)outData.get(i)).setIndex(String.valueOf(i + 1));
        }

        return outData;
    }

    public void examine(GetOneAddBoxOutData inData) {
        GaiaSdIndrawerH sdIndrawerH = inData.getGaiaSdIndrawerH();
        sdIndrawerH.setClientId(inData.getClientId());
        GaiaSdIndrawerH sdIndrawerHOld = (GaiaSdIndrawerH)this.gaiaSdIndrawerHMapper.selectByPrimaryKey(sdIndrawerH);
        sdIndrawerHOld.setGsihEmp(sdIndrawerH.getGsihEmp());
        sdIndrawerHOld.setGsihStatus("1");
        sdIndrawerHOld.setGsihDate(DateUtil.format(new Date(), "yyyyMMdd"));
        this.gaiaSdIndrawerHMapper.updateByPrimaryKeySelective(sdIndrawerHOld);
        List<GaiaSdIndrawerDOutData> gaiaSdIndrawerDS = inData.getGaiaSdIndrawerDList();
        Iterator var5 = gaiaSdIndrawerDS.iterator();

        while(var5.hasNext()) {
            GaiaSdIndrawerDOutData item = (GaiaSdIndrawerDOutData)var5.next();
            item.setClientId(inData.getClientId());
            item.setGsidVoucherId(sdIndrawerHOld.getGsihVoucherId());
            item.setGsidStatus("1");
            item.setGsidDate(DateUtil.format(new Date(), "yyyyMMdd"));
            this.gaiaSdIndrawerDMapper.updateExa(item);
        }

    }

    @Transactional
    public void addAddBox(AddAddBoxOutData inData) {
        GaiaSdIndrawerH sdIndrawerH = new GaiaSdIndrawerH();
        sdIndrawerH.setClientId(inData.getClientId());
        String gsihVoucherId = this.gaiaSdIndrawerHMapper.selectMaxgsihVoucherId();
        sdIndrawerH.setGsihVoucherId(gsihVoucherId);
        sdIndrawerH.setGsihBrId(inData.getBrId());
        sdIndrawerH.setGsihDate(DateUtil.format(new Date(), "yyyyMMdd"));
        sdIndrawerH.setGsihStatus("1");
        sdIndrawerH.setGsihEmp(inData.getEmp());
        sdIndrawerH.setGsihRemaks("人工增加");
        this.gaiaSdIndrawerHMapper.insert(sdIndrawerH);

        for(int i = 0; i < inData.getAddBoxDetailOutDataList().size(); ++i) {
            AddBoxDetailOutData item = (AddBoxDetailOutData)inData.getAddBoxDetailOutDataList().get(i);
            GaiaSdIndrawerD sdIndrawerD = new GaiaSdIndrawerD();
            BeanUtils.copyProperties(item, sdIndrawerD);
            sdIndrawerD.setClientId(inData.getClientId());
            sdIndrawerD.setGsidVoucherId(gsihVoucherId);
            sdIndrawerD.setGsidSerial(String.valueOf(i + 1));
            sdIndrawerD.setGsidDate(DateUtil.format(new Date(), "yyyyMMdd"));
            sdIndrawerD.setGsidStatus("1");
            this.gaiaSdIndrawerDMapper.insert(sdIndrawerD);
        }

    }
}
