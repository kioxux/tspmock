package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.OasSicknessCodingMapper;
import com.gys.business.mapper.entity.OasSicknessCoding;
import com.gys.business.service.IOasSicknessCodingService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 疾病中类表 服务实现类
 * </p>
 *
 * @author flynn
 * @since 2021-09-02
 */
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class OasSicknessCodingServiceImpl implements IOasSicknessCodingService {
        @Autowired
        private OasSicknessCodingMapper oasSicknessCodingMapper;



        //============================  新增初始化  ==========================

        @Override
        public Map<String,Object> buildInit(GetLoginOutData userInfo) {
            if (userInfo == null) {
                throw new BusinessException("请重新登陆");
            }
            Map<String,Object> resultMap = new HashMap<>();
            //业务逻辑

            return resultMap;
        }

        //============================  新增初始化  ==========================






        //============================  新增  ==========================

        @Override
        public Object build(GetLoginOutData userInfo,Object inData) {
            if (userInfo == null) {
                throw new BusinessException("请重新登陆");
            }
            //封装入库实体
            OasSicknessCoding oasSicknessCoding = new OasSicknessCoding();
            BeanUtils.copyProperties(inData, oasSicknessCoding);
            //处理逻辑

            //处理逻辑
            oasSicknessCodingMapper.insert(oasSicknessCoding);
            return null;
        }


        //============================  新增  ==========================





        //============================  修改初始化  ==========================
        @Override
        public Map<String,Object> updateInit(GetLoginOutData userInfo, Integer id)  {

            if (userInfo == null) {
                throw new BusinessException("请重新登陆");
            }
            Map<String,Object> res = new HashMap<>();
            //查询出db对象
            OasSicknessCoding OasSicknessCodingDb = oasSicknessCodingMapper.selectByPrimaryKey(id);
            if (OasSicknessCodingDb == null) {
                throw new BusinessException("查询无此数据");
            }
            //业务逻辑
            return res;
        }

        //============================  修改初始化  ==========================





        //============================  修改  ==========================
        @Override
        public Object update(GetLoginOutData userInfo,Object inData)  {
            if (userInfo == null) {
                throw new BusinessException("请重新登陆");
            }
            //if (ObjectUtil.isEmpty(inData) || inData.getId() == null) {
            //    throw new BusinessException("请传入合法数据！");
            //}
            //查询出db对象
            //OasSicknessCoding OasSicknessCodingDb = oasSicknessCodingMapper.selectByPrimaryKey(id);
            //if (OasSicknessCodingDb == null) {
            //    throw new BusinessException("查询无此数据");
            //}
            //封装入库实体
            OasSicknessCoding oasSicknessCoding = new OasSicknessCoding();
            //处理更新逻辑


            //处理更新逻辑

            oasSicknessCodingMapper.updateByPrimaryKey(oasSicknessCoding);
            return null;
        }


        //============================  修改  ==========================





        //============================  删除  ==========================
        @Override
        public int delete(GetLoginOutData userInfo,Integer id)  {
            if (userInfo == null) {
                throw new BusinessException("请重新登陆");
            }
            if (id == null) {
                throw new BusinessException("请传入合法数据！");
            }
            OasSicknessCoding oasSicknessCodingDb = oasSicknessCodingMapper.selectByPrimaryKey(id);
            if (oasSicknessCodingDb == null) {
                throw new BusinessException("查无此数据！");
            }
            return oasSicknessCodingMapper.deleteByPrimaryKey(id);
        }

        //============================  删除  ==========================





        //============================  详情  ==========================
        @Override
        public Object getDetailById(GetLoginOutData userInfo,Integer id)  {
            Object res = new Object();
            if (userInfo == null) {
                throw new BusinessException("请重新登陆");
            }
            //查询出db对象
            OasSicknessCoding OasSicknessCodingDb = oasSicknessCodingMapper.selectByPrimaryKey(id);
            if (OasSicknessCodingDb == null) {
                throw new BusinessException("查询无此数据");
            }
            //业务逻辑处理最终返回值

            return res;
        }


        //============================  详情  ==========================





        //============================  获取列表（分页）  ==========================

        @Override
        public PageInfo<Object> getListPage(GetLoginOutData userInfo,Object inData) {
            if (userInfo == null) {
                throw new BusinessException("请重新登陆");
            }
            //if(inData.getPageSize()==null){
            //    inData.setPageSize(100);
            //}
            //if(inData.getPageNum()==null){
            //    inData.setPageNum(1);
            //}
            List<Object> res = new ArrayList<>();
            PageInfo pageInfo;

            //实际执行的sql，执行定制任务
            //PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
            if (ObjectUtil.isNotEmpty(res)) {
                //处理返回时可能要处理的转换工作

                pageInfo = new PageInfo(res);
            } else {
                pageInfo = new PageInfo();
            }
            return pageInfo;
        }


        //============================  获取列表（分页）  ==========================
}
