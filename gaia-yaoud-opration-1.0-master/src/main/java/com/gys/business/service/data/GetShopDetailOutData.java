//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetShopDetailOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    private String clientId;
    private String gssgIdDetail;
    private String gssgBrIdDetail;
    private String gssgBrIdSourceDetail;
    private String gssgBrNameDetail;
    private Integer indexDetail;
    private String updateUser;

    public GetShopDetailOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGssgIdDetail() {
        return this.gssgIdDetail;
    }

    public String getGssgBrIdDetail() {
        return this.gssgBrIdDetail;
    }

    public String getGssgBrIdSourceDetail() {
        return this.gssgBrIdSourceDetail;
    }

    public String getGssgBrNameDetail() {
        return this.gssgBrNameDetail;
    }

    public Integer getIndexDetail() {
        return this.indexDetail;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGssgIdDetail(final String gssgIdDetail) {
        this.gssgIdDetail = gssgIdDetail;
    }

    public void setGssgBrIdDetail(final String gssgBrIdDetail) {
        this.gssgBrIdDetail = gssgBrIdDetail;
    }

    public void setGssgBrIdSourceDetail(final String gssgBrIdSourceDetail) {
        this.gssgBrIdSourceDetail = gssgBrIdSourceDetail;
    }

    public void setGssgBrNameDetail(final String gssgBrNameDetail) {
        this.gssgBrNameDetail = gssgBrNameDetail;
    }

    public void setIndexDetail(final Integer indexDetail) {
        this.indexDetail = indexDetail;
    }

    public void setUpdateUser(final String updateUser) {
        this.updateUser = updateUser;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetShopDetailOutData)) {
            return false;
        } else {
            GetShopDetailOutData other = (GetShopDetailOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label95;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label95;
                    }

                    return false;
                }

                Object this$gssgIdDetail = this.getGssgIdDetail();
                Object other$gssgIdDetail = other.getGssgIdDetail();
                if (this$gssgIdDetail == null) {
                    if (other$gssgIdDetail != null) {
                        return false;
                    }
                } else if (!this$gssgIdDetail.equals(other$gssgIdDetail)) {
                    return false;
                }

                Object this$gssgBrIdDetail = this.getGssgBrIdDetail();
                Object other$gssgBrIdDetail = other.getGssgBrIdDetail();
                if (this$gssgBrIdDetail == null) {
                    if (other$gssgBrIdDetail != null) {
                        return false;
                    }
                } else if (!this$gssgBrIdDetail.equals(other$gssgBrIdDetail)) {
                    return false;
                }

                label74: {
                    Object this$gssgBrIdSourceDetail = this.getGssgBrIdSourceDetail();
                    Object other$gssgBrIdSourceDetail = other.getGssgBrIdSourceDetail();
                    if (this$gssgBrIdSourceDetail == null) {
                        if (other$gssgBrIdSourceDetail == null) {
                            break label74;
                        }
                    } else if (this$gssgBrIdSourceDetail.equals(other$gssgBrIdSourceDetail)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$gssgBrNameDetail = this.getGssgBrNameDetail();
                    Object other$gssgBrNameDetail = other.getGssgBrNameDetail();
                    if (this$gssgBrNameDetail == null) {
                        if (other$gssgBrNameDetail == null) {
                            break label67;
                        }
                    } else if (this$gssgBrNameDetail.equals(other$gssgBrNameDetail)) {
                        break label67;
                    }

                    return false;
                }

                Object this$indexDetail = this.getIndexDetail();
                Object other$indexDetail = other.getIndexDetail();
                if (this$indexDetail == null) {
                    if (other$indexDetail != null) {
                        return false;
                    }
                } else if (!this$indexDetail.equals(other$indexDetail)) {
                    return false;
                }

                Object this$updateUser = this.getUpdateUser();
                Object other$updateUser = other.getUpdateUser();
                if (this$updateUser == null) {
                    if (other$updateUser != null) {
                        return false;
                    }
                } else if (!this$updateUser.equals(other$updateUser)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetShopDetailOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gssgIdDetail = this.getGssgIdDetail();
        result = result * 59 + ($gssgIdDetail == null ? 43 : $gssgIdDetail.hashCode());
        Object $gssgBrIdDetail = this.getGssgBrIdDetail();
        result = result * 59 + ($gssgBrIdDetail == null ? 43 : $gssgBrIdDetail.hashCode());
        Object $gssgBrIdSourceDetail = this.getGssgBrIdSourceDetail();
        result = result * 59 + ($gssgBrIdSourceDetail == null ? 43 : $gssgBrIdSourceDetail.hashCode());
        Object $gssgBrNameDetail = this.getGssgBrNameDetail();
        result = result * 59 + ($gssgBrNameDetail == null ? 43 : $gssgBrNameDetail.hashCode());
        Object $indexDetail = this.getIndexDetail();
        result = result * 59 + ($indexDetail == null ? 43 : $indexDetail.hashCode());
        Object $updateUser = this.getUpdateUser();
        result = result * 59 + ($updateUser == null ? 43 : $updateUser.hashCode());
        return result;
    }

    public String toString() {
        return "GetShopDetailOutData(clientId=" + this.getClientId() + ", gssgIdDetail=" + this.getGssgIdDetail() + ", gssgBrIdDetail=" + this.getGssgBrIdDetail() + ", gssgBrIdSourceDetail=" + this.getGssgBrIdSourceDetail() + ", gssgBrNameDetail=" + this.getGssgBrNameDetail() + ", indexDetail=" + this.getIndexDetail() + ", updateUser=" + this.getUpdateUser() + ")";
    }
}
