//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class PromCouponSetOutData {
    private String clientId;
    private String gspcsVoucherId;
    private String gspcsSerial;
    private String gspcsActNo;
    private String gspcsCouponType;
    private String gspcsCouponRemarks;
    private String gspcsBeginDate;
    private String gspcsEndDate;
    private String gspcsBeginTime;
    private String gspcsEndTime;
    private BigDecimal gspcsTotalPaycheckAmt;
    private BigDecimal gspcsSinglePaycheckAmt;
    private BigDecimal gspcsSingleUseAmt;

    public PromCouponSetOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspcsVoucherId() {
        return this.gspcsVoucherId;
    }

    public String getGspcsSerial() {
        return this.gspcsSerial;
    }

    public String getGspcsActNo() {
        return this.gspcsActNo;
    }

    public String getGspcsCouponType() {
        return this.gspcsCouponType;
    }

    public String getGspcsCouponRemarks() {
        return this.gspcsCouponRemarks;
    }

    public String getGspcsBeginDate() {
        return this.gspcsBeginDate;
    }

    public String getGspcsEndDate() {
        return this.gspcsEndDate;
    }

    public String getGspcsBeginTime() {
        return this.gspcsBeginTime;
    }

    public String getGspcsEndTime() {
        return this.gspcsEndTime;
    }

    public BigDecimal getGspcsTotalPaycheckAmt() {
        return this.gspcsTotalPaycheckAmt;
    }

    public BigDecimal getGspcsSinglePaycheckAmt() {
        return this.gspcsSinglePaycheckAmt;
    }

    public BigDecimal getGspcsSingleUseAmt() {
        return this.gspcsSingleUseAmt;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspcsVoucherId(final String gspcsVoucherId) {
        this.gspcsVoucherId = gspcsVoucherId;
    }

    public void setGspcsSerial(final String gspcsSerial) {
        this.gspcsSerial = gspcsSerial;
    }

    public void setGspcsActNo(final String gspcsActNo) {
        this.gspcsActNo = gspcsActNo;
    }

    public void setGspcsCouponType(final String gspcsCouponType) {
        this.gspcsCouponType = gspcsCouponType;
    }

    public void setGspcsCouponRemarks(final String gspcsCouponRemarks) {
        this.gspcsCouponRemarks = gspcsCouponRemarks;
    }

    public void setGspcsBeginDate(final String gspcsBeginDate) {
        this.gspcsBeginDate = gspcsBeginDate;
    }

    public void setGspcsEndDate(final String gspcsEndDate) {
        this.gspcsEndDate = gspcsEndDate;
    }

    public void setGspcsBeginTime(final String gspcsBeginTime) {
        this.gspcsBeginTime = gspcsBeginTime;
    }

    public void setGspcsEndTime(final String gspcsEndTime) {
        this.gspcsEndTime = gspcsEndTime;
    }

    public void setGspcsTotalPaycheckAmt(final BigDecimal gspcsTotalPaycheckAmt) {
        this.gspcsTotalPaycheckAmt = gspcsTotalPaycheckAmt;
    }

    public void setGspcsSinglePaycheckAmt(final BigDecimal gspcsSinglePaycheckAmt) {
        this.gspcsSinglePaycheckAmt = gspcsSinglePaycheckAmt;
    }

    public void setGspcsSingleUseAmt(final BigDecimal gspcsSingleUseAmt) {
        this.gspcsSingleUseAmt = gspcsSingleUseAmt;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromCouponSetOutData)) {
            return false;
        } else {
            PromCouponSetOutData other = (PromCouponSetOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label167: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label167;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label167;
                    }

                    return false;
                }

                Object this$gspcsVoucherId = this.getGspcsVoucherId();
                Object other$gspcsVoucherId = other.getGspcsVoucherId();
                if (this$gspcsVoucherId == null) {
                    if (other$gspcsVoucherId != null) {
                        return false;
                    }
                } else if (!this$gspcsVoucherId.equals(other$gspcsVoucherId)) {
                    return false;
                }

                label153: {
                    Object this$gspcsSerial = this.getGspcsSerial();
                    Object other$gspcsSerial = other.getGspcsSerial();
                    if (this$gspcsSerial == null) {
                        if (other$gspcsSerial == null) {
                            break label153;
                        }
                    } else if (this$gspcsSerial.equals(other$gspcsSerial)) {
                        break label153;
                    }

                    return false;
                }

                Object this$gspcsActNo = this.getGspcsActNo();
                Object other$gspcsActNo = other.getGspcsActNo();
                if (this$gspcsActNo == null) {
                    if (other$gspcsActNo != null) {
                        return false;
                    }
                } else if (!this$gspcsActNo.equals(other$gspcsActNo)) {
                    return false;
                }

                label139: {
                    Object this$gspcsCouponType = this.getGspcsCouponType();
                    Object other$gspcsCouponType = other.getGspcsCouponType();
                    if (this$gspcsCouponType == null) {
                        if (other$gspcsCouponType == null) {
                            break label139;
                        }
                    } else if (this$gspcsCouponType.equals(other$gspcsCouponType)) {
                        break label139;
                    }

                    return false;
                }

                Object this$gspcsCouponRemarks = this.getGspcsCouponRemarks();
                Object other$gspcsCouponRemarks = other.getGspcsCouponRemarks();
                if (this$gspcsCouponRemarks == null) {
                    if (other$gspcsCouponRemarks != null) {
                        return false;
                    }
                } else if (!this$gspcsCouponRemarks.equals(other$gspcsCouponRemarks)) {
                    return false;
                }

                label125: {
                    Object this$gspcsBeginDate = this.getGspcsBeginDate();
                    Object other$gspcsBeginDate = other.getGspcsBeginDate();
                    if (this$gspcsBeginDate == null) {
                        if (other$gspcsBeginDate == null) {
                            break label125;
                        }
                    } else if (this$gspcsBeginDate.equals(other$gspcsBeginDate)) {
                        break label125;
                    }

                    return false;
                }

                label118: {
                    Object this$gspcsEndDate = this.getGspcsEndDate();
                    Object other$gspcsEndDate = other.getGspcsEndDate();
                    if (this$gspcsEndDate == null) {
                        if (other$gspcsEndDate == null) {
                            break label118;
                        }
                    } else if (this$gspcsEndDate.equals(other$gspcsEndDate)) {
                        break label118;
                    }

                    return false;
                }

                Object this$gspcsBeginTime = this.getGspcsBeginTime();
                Object other$gspcsBeginTime = other.getGspcsBeginTime();
                if (this$gspcsBeginTime == null) {
                    if (other$gspcsBeginTime != null) {
                        return false;
                    }
                } else if (!this$gspcsBeginTime.equals(other$gspcsBeginTime)) {
                    return false;
                }

                label104: {
                    Object this$gspcsEndTime = this.getGspcsEndTime();
                    Object other$gspcsEndTime = other.getGspcsEndTime();
                    if (this$gspcsEndTime == null) {
                        if (other$gspcsEndTime == null) {
                            break label104;
                        }
                    } else if (this$gspcsEndTime.equals(other$gspcsEndTime)) {
                        break label104;
                    }

                    return false;
                }

                label97: {
                    Object this$gspcsTotalPaycheckAmt = this.getGspcsTotalPaycheckAmt();
                    Object other$gspcsTotalPaycheckAmt = other.getGspcsTotalPaycheckAmt();
                    if (this$gspcsTotalPaycheckAmt == null) {
                        if (other$gspcsTotalPaycheckAmt == null) {
                            break label97;
                        }
                    } else if (this$gspcsTotalPaycheckAmt.equals(other$gspcsTotalPaycheckAmt)) {
                        break label97;
                    }

                    return false;
                }

                Object this$gspcsSinglePaycheckAmt = this.getGspcsSinglePaycheckAmt();
                Object other$gspcsSinglePaycheckAmt = other.getGspcsSinglePaycheckAmt();
                if (this$gspcsSinglePaycheckAmt == null) {
                    if (other$gspcsSinglePaycheckAmt != null) {
                        return false;
                    }
                } else if (!this$gspcsSinglePaycheckAmt.equals(other$gspcsSinglePaycheckAmt)) {
                    return false;
                }

                Object this$gspcsSingleUseAmt = this.getGspcsSingleUseAmt();
                Object other$gspcsSingleUseAmt = other.getGspcsSingleUseAmt();
                if (this$gspcsSingleUseAmt == null) {
                    if (other$gspcsSingleUseAmt != null) {
                        return false;
                    }
                } else if (!this$gspcsSingleUseAmt.equals(other$gspcsSingleUseAmt)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromCouponSetOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspcsVoucherId = this.getGspcsVoucherId();
        result = result * 59 + ($gspcsVoucherId == null ? 43 : $gspcsVoucherId.hashCode());
        Object $gspcsSerial = this.getGspcsSerial();
        result = result * 59 + ($gspcsSerial == null ? 43 : $gspcsSerial.hashCode());
        Object $gspcsActNo = this.getGspcsActNo();
        result = result * 59 + ($gspcsActNo == null ? 43 : $gspcsActNo.hashCode());
        Object $gspcsCouponType = this.getGspcsCouponType();
        result = result * 59 + ($gspcsCouponType == null ? 43 : $gspcsCouponType.hashCode());
        Object $gspcsCouponRemarks = this.getGspcsCouponRemarks();
        result = result * 59 + ($gspcsCouponRemarks == null ? 43 : $gspcsCouponRemarks.hashCode());
        Object $gspcsBeginDate = this.getGspcsBeginDate();
        result = result * 59 + ($gspcsBeginDate == null ? 43 : $gspcsBeginDate.hashCode());
        Object $gspcsEndDate = this.getGspcsEndDate();
        result = result * 59 + ($gspcsEndDate == null ? 43 : $gspcsEndDate.hashCode());
        Object $gspcsBeginTime = this.getGspcsBeginTime();
        result = result * 59 + ($gspcsBeginTime == null ? 43 : $gspcsBeginTime.hashCode());
        Object $gspcsEndTime = this.getGspcsEndTime();
        result = result * 59 + ($gspcsEndTime == null ? 43 : $gspcsEndTime.hashCode());
        Object $gspcsTotalPaycheckAmt = this.getGspcsTotalPaycheckAmt();
        result = result * 59 + ($gspcsTotalPaycheckAmt == null ? 43 : $gspcsTotalPaycheckAmt.hashCode());
        Object $gspcsSinglePaycheckAmt = this.getGspcsSinglePaycheckAmt();
        result = result * 59 + ($gspcsSinglePaycheckAmt == null ? 43 : $gspcsSinglePaycheckAmt.hashCode());
        Object $gspcsSingleUseAmt = this.getGspcsSingleUseAmt();
        result = result * 59 + ($gspcsSingleUseAmt == null ? 43 : $gspcsSingleUseAmt.hashCode());
        return result;
    }

    public String toString() {
        return "PromCouponSetOutData(clientId=" + this.getClientId() + ", gspcsVoucherId=" + this.getGspcsVoucherId() + ", gspcsSerial=" + this.getGspcsSerial() + ", gspcsActNo=" + this.getGspcsActNo() + ", gspcsCouponType=" + this.getGspcsCouponType() + ", gspcsCouponRemarks=" + this.getGspcsCouponRemarks() + ", gspcsBeginDate=" + this.getGspcsBeginDate() + ", gspcsEndDate=" + this.getGspcsEndDate() + ", gspcsBeginTime=" + this.getGspcsBeginTime() + ", gspcsEndTime=" + this.getGspcsEndTime() + ", gspcsTotalPaycheckAmt=" + this.getGspcsTotalPaycheckAmt() + ", gspcsSinglePaycheckAmt=" + this.getGspcsSinglePaycheckAmt() + ", gspcsSingleUseAmt=" + this.getGspcsSingleUseAmt() + ")";
    }
}
