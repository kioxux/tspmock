package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 月度库存查询数据库条件
 */
@Data
public class StockMouthInData implements Serializable {

    private static final long serialVersionUID = -3558428056560589716L;

    //加盟商
    private String clients;

    @ApiModelProperty(value = "地点合集", required = false)
    private List<String> sites;

    @ApiModelProperty(value = "年月", required = true)
    private String chooseDate;

    @ApiModelProperty(value = "库存推算选择模式，QC表示从期出上线月份更新至该月份  DQ表示仅更新选择月份", required = true)
    private String caculateType;


}
