//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.ClearBoxInfoOutData;
import com.gys.business.service.data.ExamineTipOutData;
import com.gys.common.data.GetLoginOutData;
import java.util.List;

public interface ExamineTipService {
    List<ExamineTipOutData> list(GetLoginOutData userInfo);

    ClearBoxInfoOutData getClearBoxInfo(ExamineTipOutData inData);
}
