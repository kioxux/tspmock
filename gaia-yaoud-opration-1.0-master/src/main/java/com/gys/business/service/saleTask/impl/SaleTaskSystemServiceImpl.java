package com.gys.business.service.saleTask.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSaletaskHplanNewMapper;
import com.gys.business.mapper.GaiaSaletaskPlanStoMapper;
import com.gys.business.mapper.entity.GaiaSaletaskHplanNew;
import com.gys.business.mapper.entity.GaiaSaletaskHplanSystem;
import com.gys.business.mapper.entity.GaiaSaletaskPlanSto;
import com.gys.business.mapper.SaleTaskSystemMapper;
import com.gys.business.service.data.MessageParams;
import com.gys.business.service.data.saleTask.SalePlanPushInData;
import com.gys.business.service.data.saleTask.SaleTaskSystemInData;
import com.gys.business.service.data.saleTask.SaleTaskSystemOutData;
import com.gys.business.service.data.saleTask.TaskSystemPushData;
import com.gys.business.service.saleTask.SaleTaskSystemService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.feign.OperateService;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class SaleTaskSystemServiceImpl implements SaleTaskSystemService {
    @Resource
    private SaleTaskSystemMapper saleTaskSystemMapper;
    @Resource
    private OperateService operateService;
    @Resource
    private GaiaSaletaskHplanNewMapper saletaskHplanNewMapper;
    @Resource
    private GaiaSaletaskPlanStoMapper saletaskPlanStoMapper;

    @Override
    public String insertSystemSalesPlan(GetLoginOutData userInfo, SaleTaskSystemInData inData) {
        String missionMonth = inData.getMonthPlan();//获取任务月份
        String taskId = "";
        if(ObjectUtil.isEmpty(inData.getStaskTaskName())) {
            throw new BusinessException("任务名称不能为空！");
        }
        if(ObjectUtil.isEmpty(inData.getYoySaleAmt()) && ObjectUtil.isEmpty(inData.getMomSaleAmt())){
            throw new BusinessException("营业额任务不能为空！");
        }
        if(ObjectUtil.isEmpty(inData.getYoySaleGross()) && ObjectUtil.isEmpty(inData.getMomSaleGross())){
            throw new BusinessException("毛利额任务不能为空！");
        }
        if(ObjectUtil.isEmpty(inData.getYoyMcardQty()) && ObjectUtil.isEmpty(inData.getMomMcardQty())){
            throw new BusinessException("会员卡任务不能为空！");
        }
        GaiaSaletaskHplanSystem gaiaSaletaskHplanSystem = new GaiaSaletaskHplanSystem();//创建月销售对象
        if(StringUtils.isEmpty(inData.getStaskTaskId())) {
            taskId = saleTaskSystemMapper.selectNextVoucherId();//获取任务id
            gaiaSaletaskHplanSystem.setStaskTaskId(taskId);//任务id
        }else {
            taskId = inData.getStaskTaskId();
        }
        int count = saleTaskSystemMapper.staskTaskNameByName(inData.getStaskTaskName(), missionMonth, taskId);
        if (count > 0) {
            throw new BusinessException("任务名称:" + inData.getStaskTaskName() + "已存在！");
        }
        //String ThismissionMonth = missionMonth.replace("-","");
        gaiaSaletaskHplanSystem.setMonthPlan(missionMonth);//任务月份
        gaiaSaletaskHplanSystem.setStaskTaskName(inData.getStaskTaskName());//任务名称
        //获取任务月份
        String year = missionMonth.substring(0, 4);//获取年
        String month = missionMonth.substring(4, 6);//获取月
        Integer yearS = 0; //转换年
        Integer monthS = 0;//转换月
        try {
            yearS = Integer.valueOf(year).intValue();
            monthS = Integer.valueOf(month).intValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        String firstDay = getFisrtDayOfMonth(yearS, monthS);//获取当前月的第一天
        String thisFirstDay = firstDay.replace("-", "");
        String lastDay = getLastDayOfMonth(yearS, monthS);
        String thisLastDay = lastDay.replace("-", "");
        gaiaSaletaskHplanSystem.setStaskStartDate(thisFirstDay);//起始日期
        gaiaSaletaskHplanSystem.setStaskEndDate(thisLastDay);//结束日期
        gaiaSaletaskHplanSystem.setYoyMonth(inData.getYoyMonth());//同比月份
        gaiaSaletaskHplanSystem.setMomMonth(inData.getMomMonth());//环比月份
        Integer numberDay = getDaysByYearMonth(yearS, monthS);//获取期间天数
//        gaiaSaletaskHplanSystem.setMonthDays(numberDay);//期间天数
        if(StringUtils.isNotEmpty(inData.getYoySaleAmt())){
            gaiaSaletaskHplanSystem.setYoySaleAmt(new BigDecimal(inData.getYoySaleAmt()));//营业额环比增长率
        }
        if(StringUtils.isNotEmpty(inData.getMomSaleAmt())){
            gaiaSaletaskHplanSystem.setMomSaleAmt(new BigDecimal(inData.getMomSaleAmt()));//营业额环比增长率
        }
        if(StringUtils.isNotEmpty(inData.getYoySaleGross())){
            gaiaSaletaskHplanSystem.setYoySaleGross(new BigDecimal(inData.getYoySaleGross()));//毛利额同比增长率
        }
        if(StringUtils.isNotEmpty(inData.getMomSaleGross())){
            gaiaSaletaskHplanSystem.setMomSaleGross(new BigDecimal(inData.getMomSaleGross()));//毛利额环比增长率
        }
        if(StringUtils.isNotEmpty(inData.getYoyMcardQty())){
            gaiaSaletaskHplanSystem.setYoyMcardQty(new BigDecimal(inData.getYoyMcardQty()));//会员卡同比增长率
        }
        if(StringUtils.isNotEmpty(inData.getMomMcardQty())){
            gaiaSaletaskHplanSystem.setMomMcardQty(new BigDecimal(inData.getMomMcardQty()));//会员卡环比增长率
        }
        gaiaSaletaskHplanSystem.setStaskSplanSta("S");//任务状态(S已保存，P已下发)
        gaiaSaletaskHplanSystem.setStaskCreId(userInfo.getUserId());//创建人id
        gaiaSaletaskHplanSystem.setStaskCreDate(DateUtil.format(new Date(), "yyyyMMdd"));//创建日期
        gaiaSaletaskHplanSystem.setStaskCreTime(DateUtil.format(new Date(), "HHmmss"));//创建时间
        gaiaSaletaskHplanSystem.setStaskType("0");//删除 0 未删除 1已删除 添加

        if(StringUtils.isEmpty(inData.getStaskTaskId())) {
            this.saleTaskSystemMapper.insert(gaiaSaletaskHplanSystem);
        }else {
            Example example = new Example(GaiaSaletaskHplanSystem.class);
            example.createCriteria().andEqualTo("staskTaskId", inData.getStaskTaskId());
            this.saleTaskSystemMapper.updateByExample(gaiaSaletaskHplanSystem,example);
        }
        return taskId;
    }

    @Override
    public List<SaleTaskSystemOutData> selectSalePlanList(SaleTaskSystemInData inData) {
        return this.saleTaskSystemMapper.selectSalePlanList(inData);
    }

    @Override
    public GaiaSaletaskHplanSystem selectSalePlanDetail(SaleTaskSystemInData inData) {
        return this.saleTaskSystemMapper.selectSystemPlanDetail(inData.getStaskTaskId());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void pushSalePlan(SalePlanPushInData inData) {
        GaiaSaletaskHplanSystem detail =  this.saleTaskSystemMapper.selectSystemPlanDetail(inData.getTaskId());
        GaiaSaletaskHplanNew clientDetail = new GaiaSaletaskHplanNew();
        BeanUtils.copyProperties(detail,clientDetail);
        if(ObjectUtil.isNotEmpty(inData.getClients())){
            List<SalePlanPushInData> list = new ArrayList<>();
            for (int i = 0;i < inData.getClients().length;i++){
                int count = this.saleTaskSystemMapper.selectCountByClient(inData.getClients()[i],inData.getTaskId());
                if (count > 0){
                    throw new BusinessException("该客户已推送该任务，请核对！");
                }
                SalePlanPushInData item = new SalePlanPushInData();
                item.setPushType(inData.getPushType());
                if("1".equals(inData.getPushType())){
                    item.setPushStatus("1");
//                    this.sendMessage(clientDetail,inData.getClients()[i]);
                    //立即推送添加门店明细表
                    clientDetail.setClient(inData.getClients()[i]);
                    this.savePlanToClient(clientDetail);
                }else if("2".equals(inData.getPushType())){
                    item.setPushStatus("0");
                }
                item.setTaskId(inData.getTaskId());
                item.setClient(inData.getClients()[i]);
                list.add(item);
            }
            this.saleTaskSystemMapper.batchPushUser(list);
            //更新任务状态
            TaskSystemPushData pushData = new TaskSystemPushData();
            pushData.setTaskId(inData.getTaskId());
            pushData.setPushType(inData.getPushType());
            if ("1".equals(inData.getPushType())){
                pushData.setPushStatus("1");
                pushData.setPlanStatus("P");
                Calendar calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR);
                if (hour < 10){
                    pushData.setPushDate(CommonUtil.getyyyyMMdd() + "0" + hour);
                } else {
                    pushData.setPushDate(CommonUtil.getyyyyMMdd() + hour);
                }
            }else if("2".equals(inData.getPushType())){
                pushData.setPushStatus("0");
                pushData.setPlanStatus("P");
                pushData.setPushDate(inData.getPushDate());
            }
            this.saleTaskSystemMapper.modifySystemPlanPushStatus(pushData);
        }else {
            throw new BusinessException("请选择要推送的人员！");
        }
    }

    @Override
    public void timerPushSalePlan() {
        List<GaiaSaletaskHplanSystem> saletaskHplanSystemList = this.saleTaskSystemMapper.selectNoPushPlan();
        for (GaiaSaletaskHplanSystem detail : saletaskHplanSystemList) {
            GaiaSaletaskHplanNew clientDetail = new GaiaSaletaskHplanNew();
            BeanUtils.copyProperties(detail,clientDetail);
            TaskSystemPushData pushData = new TaskSystemPushData();
            pushData.setTaskId(detail.getStaskTaskId());
            pushData.setPushStatus("1");
            pushData.setPlanStatus("P");
            List<String> clients = this.saleTaskSystemMapper.selectClientByTaskId(detail.getStaskTaskId());
            if (ObjectUtil.isNotEmpty(clients)) {
                for (int i = 0; i < clients.size(); i++) {
                    this.sendMessage(clientDetail,clients.get(i));
                }
            }
            this.saleTaskSystemMapper.modifySystemPlanPushStatus(pushData);
            //添加记录到用户月度计划表中
            this.savePlanToClient(clientDetail);
        }
    }

    @Override
    public void deleteSalePlan(SalePlanPushInData inData) {
        this.saleTaskSystemMapper.deleteTask(inData.getTaskId());
    }

    public void savePlanToClient(GaiaSaletaskHplanNew detail){
        //新增数据到用户表中
        GaiaSaletaskHplanNew saletaskHplanNew = new GaiaSaletaskHplanNew();
        BeanUtils.copyProperties(detail,saletaskHplanNew);
        saletaskHplanNew.setStaskCreId("药德");
        saletaskHplanNew.setStaskCreDate(CommonUtil.getyyyyMMdd());
        saletaskHplanNew.setStaskCreTime(CommonUtil.getHHmmss());
        saletaskHplanNew.setStaskSplanSta("P");
        saletaskHplanNewMapper.insert(saletaskHplanNew);
        //根据加盟商获取门店相关月计划数据
        List<GaiaSaletaskPlanSto> saletaskPlanStoList = this.saletaskPlanStoMapper.selectSaleStorePlanByClient(detail);
        if (ObjectUtil.isNotEmpty(saletaskPlanStoList) && saletaskPlanStoList.size() > 0){
            List<GaiaSaletaskPlanSto> result = new ArrayList<>();
            for (GaiaSaletaskPlanSto s : saletaskPlanStoList) {
                s.setClient(detail.getClient());
                s.setMonthPlan(detail.getMonthPlan());
                s.setStaskCreDate(CommonUtil.getyyyyMMdd());
                s.setYoySaleAmt(detail.getYoySaleAmt());
                s.setMomSaleAmt(detail.getMomSaleAmt());
                s.setYoySaleGross(detail.getYoySaleGross());
                s.setMomSaleGross(detail.getMomSaleGross());
                s.setYoyMcardQty(detail.getYoyMcardQty());
                s.setMomMcardQty(detail.getMomMcardQty());
                s.setStaskCreId("药德");
                s.setStaskIsShow("0");
                s.setStaskType("0");
                s.setStaskTaskId(detail.getStaskTaskId());
                if (ObjectUtil.isNotEmpty(s.getASaleAmt())
                        && ObjectUtil.isNotEmpty(s.getASaleGross())
                        && ObjectUtil.isNotEmpty(s.getAMcardQty())){
                    result.add(s);
                }
            }
            if (ObjectUtil.isNotEmpty(result) && result.size() > 0) {
                saletaskPlanStoMapper.insertList(result);
            }
        }
    }

    public void sendMessage(GaiaSaletaskHplanNew detail,String client){
        List<String>  userIds = this.saleTaskSystemMapper.selectUserIdList(client);
        //如果为立即推送，即为直接向用户发送消息
        MessageParams messageParams = new MessageParams();
        messageParams.setClient(client);
        messageParams.setUserIdList(userIds);
        messageParams.setContentParmas(new ArrayList<String>() {{
            add(detail.getStaskTaskName());
        }});
        messageParams.setId("MSG00012");
        log.info("销售提成消息参数:{}", messageParams.toString());
        String message = operateService.sendMessageList(messageParams);
        log.info("培训推送消息结果:{}", message);
        //下发任务到对应的用户月度维护，添加数据
//        GaiaSaletaskHplanNew saletaskHplan = new GaiaSaletaskHplanNew();
//        saletaskHplan.setClient(client);
//        saletaskHplan.setStaskTaskId(detail.getStaskTaskId());
//        saletaskHplan.setMonthPlan(detail.getMonthPlan());
//        saletaskHplan.setStaskType("0");
//        saletaskHplan.setStaskTaskName(detail.getStaskTaskName());
//        saletaskHplan.setMomSaleAmt(detail.getMomSaleAmt());
//        saletaskHplan.setMomSaleGross(detail.getMomSaleGross());
//        saletaskHplan.setMomMonth(detail.getMomMonth());
//        saletaskHplan.setMomMcardQty(detail.getMomMcardQty());
//        saletaskHplan.setYoyMonth(detail.getYoyMonth());
//        saletaskHplan.setYoySaleAmt(detail.getYoySaleAmt());
//        saletaskHplan.setYoySaleGross(detail.getYoySaleGross());
//        saletaskHplan.setYoyMcardQty(detail.getYoyMcardQty());
//        saletaskHplan.setStaskSplanSta("P");
//        saletaskHplan.setStaskCreDate(CommonUtil.getyyyyMMdd());
//        saletaskHplan.setStaskCreId("system");
//        saletaskHplan.setStaskCreTime(CommonUtil.getHHmmss());
//        this.saletaskHplanNewMapper.insert(saletaskHplan);
    }

    /**
     * 获取指定年月的第一天
     * @param year
     * @param month
     * @return
     */
    public static String getFisrtDayOfMonth(int year,int month){
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR,year);
        //设置月份
        cal.set(Calendar.MONTH, month-1);
        //获取某月最小天数
        int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最小天数
        cal.set(Calendar.DAY_OF_MONTH, firstDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String firstDayOfMonth = sdf.format(cal.getTime());
        return firstDayOfMonth;
    }

    /**
     * 获取指定年月的最后一天
     * @param year
     * @param month
     * @return
     */
    public static String getLastDayOfMonth(int year,int month)
    {
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR,year);
        //设置月份
        cal.set(Calendar.MONTH, month-1);
        //获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String lastDayOfMonth = sdf.format(cal.getTime());
        return lastDayOfMonth;
    }
    /**
     * 根据年 月 获取对应的月份 天数
     * */
    public static int getDaysByYearMonth(int year, int month) {

        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 获取上期 上月
     * @param date
     * @return
     */
    public static String getFisrtDayOfMonth(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
        Calendar calendar = Calendar.getInstance();
        Date time1 = new Date();
        try {
            // 将字符串转换成日期
            time1 = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.setTime(time1);
        // 设置为上一个月
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        return format.format(calendar.getTime());
    }
}
