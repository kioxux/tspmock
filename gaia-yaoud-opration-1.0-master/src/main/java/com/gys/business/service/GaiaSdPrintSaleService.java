package com.gys.business.service;

import com.gys.common.data.JsonResult;

public interface GaiaSdPrintSaleService {

    JsonResult getPrintSaleByClientAndBrId(String clientId, String brId);
}
