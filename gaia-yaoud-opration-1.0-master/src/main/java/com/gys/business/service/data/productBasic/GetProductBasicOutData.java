package com.gys.business.service.data.productBasic;

import com.alibaba.excel.annotation.ExcelProperty;
import com.gys.business.mapper.entity.GaiaProductBasicImage;
import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author ：gyx
 * @Date ：Created in 10:19 2021/10/14
 * @Description：商品主数据出参
 * @Modified By：gyx
 * @Version:
 */
@Data
public class GetProductBasicOutData {

    private String client;

    private String proSite;

    private String stoCode;

    //商品编码
    @ExcelProperty(value = "商品编码",index = 1)
    @NotBlank(message = "商品编码不能为空")
    private String proCode;
    //通用名称
    @ExcelProperty(value = "通用名称",index = 2)
    @NotBlank(message = "通用名称不能为空")
    private String proCommonName;
    //商品描述
    @ExcelProperty(value = "商品描述",index = 3)
    @NotBlank(message = "商品描述不能为空")
    private String proDepict;
    //助记码
    @ExcelProperty(value = "助记码",index = 4)
    @NotBlank(message = "助记码不能为空")
    private String proPym;
    //商品名称
    @ExcelProperty(value = "商品名称",index = 5)
    private String proName;
    //规格
    @ExcelProperty(value = "规格",index = 6)
    @NotBlank(message = "规格不能为空")
    private String proSpecs;
    //计量单位
    @ExcelProperty(value = "计量单位",index = 7)
    @NotBlank(message = "计量单位不能为空")
    private String proUnit;
    //剂型
    @ExcelProperty(value = "剂型",index = 8)
    private String proForm;
    //细分剂型
    @ExcelProperty(value = "细分剂型",index = 9)
    private String proPartForm;
    //最小剂量（以mg/ml计算）
    @ExcelProperty(value = "最小剂量（以mg/ml计算）",index = 10)
    private String proMinDose;
    //总剂量（以mg/ml计算）
    @ExcelProperty(value = "总剂量（以mg/ml计算）",index = 11)
    private String proTotalDose;
    //国际条码1
    @ExcelProperty(value = "国际条码1",index = 12)
    private String proBarCode;
    //国际条码2
    @ExcelProperty(value = "国际条码2",index = 13)
    private String proBarCode2;
    //批准文号分类
    @ExcelProperty(value = "批准文号分类",index = 14)
    @NotBlank(message = "批准文号分类不能为空")
    private String proRegisterClass;
    //批准文号
    @ExcelProperty(value = "批准文号",index = 15)
    private String proRegisterNo;
    //批准文号批准日期
    @ExcelProperty(value = "批准文号批准日期",index = 16)
    private String proRegisterDate;
    //批准文号失效日期
    @ExcelProperty(value = "批准文号失效日期",index = 17)
    private String proRegisterExDate;
    //商品分类
    @ExcelProperty(value = "商品分类",index = 18)
    @NotBlank(message = "商品分类不能为空")
    private String proClass;
    //商品分类描述
    @ExcelProperty(value = "商品分类描述",index = 19)
    @NotBlank(message = "商品分类描述不能为空")
    private String proClassName;
    //成分分类
    @ExcelProperty(value = "成分分类",index = 20)
    @NotBlank(message = "成分分类不能为空")
    private String proCompClass;
    //成分分类描述
    @ExcelProperty(value = "成分分类描述",index = 21)
    @NotBlank(message = "成分分类描述不能为空")
    private String proCompClassName;
    //处方类别
    @ExcelProperty(value = "处方类别",index = 22)
    private String proPresClass;
    //生产企业代码
    @ExcelProperty(value = "生产企业代码",index = 23)
    private String proFactoryCode;
    //生产企业
    @ExcelProperty(value = "生产企业",index = 24)
    @NotBlank(message = "生产企业不能为空")
    private String proFactoryName;
    //商标
    @ExcelProperty(value = "商标",index = 25)
    private String proMark;
    //品牌标识名
    @ExcelProperty(value = "品牌标识名",index = 26)
    private String proBrand;
    //品牌区分
    @ExcelProperty(value = "品牌区分",index = 27)
    private String proBrandClass;
    //保质期
    @ExcelProperty(value = "保质期",index = 28)
    private String proLife;
    ///保质期单位
    @ExcelProperty(value = "保质期单位",index = 29)
    private String proLifeUnit;
    //上市许可持有人
    @ExcelProperty(value = "上市许可持有人",index = 30)
    private String proHolder;
    //进项税率
    @ExcelProperty(value = "进项税率",index = 31)
    @NotBlank(message = "进项税率不能为空")
    private String proInputTax;
    //销项税率
    @ExcelProperty(value = "销项税率",index = 32)
    @NotBlank(message = "销项税率不能为空")
    private String proOutputTax;
    //药品本位码
    @ExcelProperty(value = "药品本位码",index = 33)
    private String proBasicCode;
    //税务分类编码
    @ExcelProperty(value = "税务分类编码",index = 34)
    private String proTaxClass;
    //管制特殊分类
    @ExcelProperty(value = "管制特殊分类",index = 35)
    private String proControlClass;
    //生产类别
    @ExcelProperty(value = "生产类别",index = 36)
    private String proProduceClass;
    //贮存条件
    @ExcelProperty(value = "贮存条件",index = 37)
    private String proStorageCondition;
    //商品仓储分区
    @ExcelProperty(value = "商品仓储分区",index = 38)
    private String proStorageArea;
    //长（以MM计算）
    @ExcelProperty(value = "长（以MM计算）",index = 39)
    private String proLong;
    //宽（以MM计算）
    @ExcelProperty(value = "宽（以MM计算）",index = 40)
    private String proWide;
    //高（以MM计算）
    @ExcelProperty(value = "高（以MM计算）",index = 41)
    private String proHigh;
    //中包装量
    @ExcelProperty(value = "中包装量",index = 42)
    private String proMidPackage;
    //大包装量
    @ExcelProperty(value = "大包装量",index = 43)
    private String proBigPackage;
    //启用电子监管码
    @ExcelProperty(value = "启用电子监管码",index = 44)
    private String proElectronicCode;
    //生产经营许可证号
    @ExcelProperty(value = "生产经营许可证号",index = 45)
    private String proQsCode;
    //最大销售量
    @ExcelProperty(value = "最大销售量",index = 46)
    private String proMaxSales;
    //说明书代码
    @ExcelProperty(value = "说明书代码",index = 47)
    private String proInstructionCode;
    //说明书内容
    @ExcelProperty(value = "说明书内容",index = 48)
    private String proInstruction;
    //国家医保品种
    @ExcelProperty(value = "国家医保品种",index = 49)
    private String proMedProDCT;
    //国家医保品种编码
    @ExcelProperty(value = "国家医保品种编码",index = 50)
    private String proMedProDCTCode;
    //生产国家
    @ExcelProperty(value = "生产国家",index = 51)
    private String proCountry;
    //产地
    @ExcelProperty(value = "产地",index = 52)
    private String proPlace;
    //状态
    @ExcelProperty(value = "状态",index = 53)
    private String proStatus;
    //可服用天数
    @ExcelProperty(value = "可服用天数",index = 54)
    private String proTakeDays;
    //用法用量
    @ExcelProperty(value = "用法用量",index = 55)
    private String proUsage;
    //禁忌说明
    @ExcelProperty(value = "禁忌说明",index = 56)
    private String proContraindication;
    //国家医保目录编号
    @ExcelProperty(value = "国家医保目录编号",index = 57)
    private String proMedListNum;
    //国家医保目录名称
    @ExcelProperty(value = "国家医保目录名称",index = 58)
    private String proMedListName;
    //国家医保医保目录剂型
    @ExcelProperty(value = "国家医保医保目录剂型",index = 59)
    private String proMedListForm;
    //注意事项
    @ExcelProperty(value = "注意事项",index = 60)
    private String proNoticeThings;
    //更新加盟商
    private String updateClient;
    //更新人
    @ExcelProperty(value = "更新人",index = 61)
    private String updater;
    //更新时间
    @ExcelProperty(value = "更新时间",index = 62)
    private String updateTime;

    // 商品图片列表
    private List<GaiaProductBasicImage> imageList;
}
