package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdReplenishConfig;

import java.util.List;
import java.util.Map;

import com.gys.business.service.data.sdReplenishConfig.GaiaSdReplenishConfigDto;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

/**
 * <p>
 * 门店补货参数 服务类
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-17
 */
public interface IGaiaSdReplenishConfigService {
    //新增
    Object build(GetLoginOutData userInfo, GaiaSdReplenishConfigDto inData);

    void addReplenishParam(GetLoginOutData userInfo, GaiaSdReplenishConfigDto inData);

    //修改初始化
    GaiaSdReplenishConfig updateInit(GetLoginOutData userInfo);

    //修改
    Object update(GetLoginOutData userInfo, GaiaSdReplenishConfigDto updateVo);

    //删除
    int delete(GetLoginOutData userInfo, Integer id);

    //详情
    Object getDetailById(GetLoginOutData userInfo);


    //获取分页列表
    PageInfo getListPage(GetLoginOutData userInfo, Object inData);

    Object getRemain(GetLoginOutData userInfo, String gsrhVoucherId);

    Object setRemain(GetLoginOutData userInfo, GaiaSdReplenishConfig gaiaSdReplenishConfig);
}
