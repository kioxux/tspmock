package com.gys.business.service.data.yaoshibang;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhangdong
 * @date 2021/6/17 10:38
 */
@Data
public class OrderQueryBean {

    @ApiModelProperty("下单开始日期")
    private String orderTimeBegin;
    @ApiModelProperty("下单结束日期")
    private String orderTimeEnd;
    @ApiModelProperty("客户id")
    private String customerId;
    @ApiModelProperty("客户名称")
    private String customerName;
    @ApiModelProperty("订单状态")
    private String status;
    private String clientId;
    private Integer pageNum = 0;
    private Integer pageSize = 15;

}
