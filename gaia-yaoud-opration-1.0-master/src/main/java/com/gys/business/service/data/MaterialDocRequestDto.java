package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class MaterialDocRequestDto implements Serializable {
    private static final long serialVersionUID = -3985023342963095970L;
    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "主键ID")
    private String guid;

    @ApiModelProperty(value = "序号")
    private String guidNo;

    @ApiModelProperty(value = "业务类型")
    private String matType;

    @ApiModelProperty(value = "过账日期")
    private String matPostDate;

    @ApiModelProperty(value = "抬头备注")
    private String matHeadRemark;

    @ApiModelProperty(value = "商品自编码")
    private String matProCode;

    @ApiModelProperty(value = "配送门店")
    private String matStoCode;

    @ApiModelProperty(value = "地点")
    private String matSiteCode;

    @ApiModelProperty(value = "库存地点（状态）")
    private String matLocationCode;

    @ApiModelProperty(value = "转移库存地点（状态）")
    private String matLocationTo;

    @ApiModelProperty(value = "批次")
    private String matBatch;

    @ApiModelProperty(value = "交易数量")
    private BigDecimal matQty;

    @ApiModelProperty(value = "交易金额")
    private BigDecimal matAmt;

    @ApiModelProperty(value = "交易价格")
    private BigDecimal matPrice;

    @ApiModelProperty(value = "基本计量单位")
    private String matUnit;

    @ApiModelProperty(value = "借/贷标识")
    private String matDebitCredit;

    @ApiModelProperty(value = "订单号")
    private String matPoId;

    @ApiModelProperty(value = "订单行号")
    private String matPoLineno;

    @ApiModelProperty(value = "业务单号")
    private String matDnId;

    @ApiModelProperty(value = "业务单行号")
    private String matDnLineno;

    @ApiModelProperty(value = "物料凭证行备注")
    private String matLineRemark;

    @ApiModelProperty(value = "创建人")
    private String matCreateBy;

    @ApiModelProperty(value = "创建日期")
    private String matCreateDate;

    @ApiModelProperty(value = "创建时间")
    private String matCreateTime;
}
