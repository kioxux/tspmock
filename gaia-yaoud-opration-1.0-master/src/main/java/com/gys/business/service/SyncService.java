package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;

public interface SyncService {
    GetSyncOutData selectProductInfo(GetLoginOutData userInfo);

    GetSyncOutData selectStoreInfo(GetLoginOutData userInfo);

    GetSyncOutData selectSaleInfo(GetLoginOutData userInfo);

    void uploadSaleInfo(GetSyncSdSaleHInData inData, GetLoginOutData userInfo);

    void stockSync(StockSyncData inData);

    void batchNoAdjustSync(batchNoAdjustSyncData inData);

    void memberSync(MemberSyncData inData);

    void proPriceSync(ProPriceSyncData inData);

    void mutilpleStoreProPriceSync(ProPriceSyncData2 inData);

    void supplierSync(SupplierSyncData inData);

    void batchControlSync(BatchControlSyncData inData);

    void productSync(ProductSyncData inData);

    void multipleStoreProductSync(ProductSyncData2 inData);
}
