package com.gys.business.service.data;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Api(value = "委托配送单入参")
public class TrustSendInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "委托配送单号")
    private String gswhVoucherId;
    @ApiModelProperty(value = "审核状态")
    private String gswhStatus;
    @ApiModelProperty(value = "商品编码")
    private String gswdProId;
    @ApiModelProperty(value = "批号")
    private String gswdBatchNo;
    @ApiModelProperty(value = "收货起始日期")
    private String startAcceptDate;
    @ApiModelProperty(value = "收货结束日期")
    private String endAcceptDate;
}
