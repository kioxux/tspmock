package com.gys.business.service.impl;

import com.gys.business.service.GaiaSdBatchImportDataService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * <p>
 * 门店手工流向数据表 服务实现类
 * </p>
 *
 * @author yifan.wang
 * @since 2021-08-04
 */
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class GaiaSdBatchImportDataServiceImpl implements GaiaSdBatchImportDataService {

}
