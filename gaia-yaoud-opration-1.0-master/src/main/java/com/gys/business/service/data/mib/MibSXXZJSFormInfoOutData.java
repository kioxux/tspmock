package com.gys.business.service.data.mib;

import lombok.Data;

/**
 * @Author ：gyx
 * @Date ：Created in 16:50 2021/12/27
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Data
public class MibSXXZJSFormInfoOutData {

    private String positionHis; //定点医疗机构                                      例子:(代县千金药店*部)

    private String seekMedicalAdviceId; //就诊Id                                   例子:140923Y21************

    private String accountNo;       //结算Id                                        例子:140100S000***********

    private String unit;        //单位                                              例子:元(保留两位小数)

    private String patientName; //患者姓名                                           例子:岳*喜

    private String sex;         //性别                                              例子:男

    private String age;         //年龄                                              例子:56

    private String personalCategory;   //人员类别                                   例子:在职

    private String insuranceType;   //险种类型                                      例子:职工基本医疗保险

    private String credentialsNo;   //证件号码                                      例子:1422*****************

    private String seekMedicalAdviceDate;   //就诊日期                              例子: 2021-11-25 18:52:08

    private String medicalCategory;     //医疗类别                                  例子:定点药店购药

    private String seekMedicalAddress;  //就医地                                   例子:代县

    private String insuredLand;     //参保地                                       例子:代县

    private String insuredUnit;     //参保单位                                      例子:山西二建集团有限公司

    private String medicalExpensesTotalAmt;     //医疗费总额                         例子:634.00

    private String actualStartingAmt;           //实际起付金额                        例子:0.00

    private String proportionOfBasicMedicalReimbursement; //基本医疗报销比例           例子:0

    private String basicMedicalInsurancePoolingFund;      //基本医疗保险统筹基金        例子:0.00

    private String personalAccountExpenditure;            //个人账户支出              例子:634.00

    private String individualAccountTotalExpenditure;     //个人账户共济支出            例子:0.00

    private String personalCashExpenditure;               //个人现金支出               例子:0.00

    private String personalAccountBalance;                //个人账户余额               例子:1585.92

    private String accountDate;                           //结算时间                   例子: 2021-11-25 18:52:01

    private String billEmp;                               //业务经办人                 例子: 代县千金药店*部

    private String printDate;                             //打印日期                   例子: 2021-11-25 18:52:03

}
