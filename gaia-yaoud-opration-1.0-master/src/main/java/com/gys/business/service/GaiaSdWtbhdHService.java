package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdWtbhdH;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/13 18:46
 **/
public interface GaiaSdWtbhdHService {

    GaiaSdWtbhdH add(GaiaSdWtbhdH gaiaSdWtbhdH);

    GaiaSdWtbhdH update(GaiaSdWtbhdH gaiaSdWtbhdH);
}
