package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class StoreStockReportOutData implements Serializable {

    @ApiModelProperty(value = "商品大类编码")
    private String bigTypeCode;

    @ApiModelProperty(value = "商品大类")
    private String bigType;

    @ApiModelProperty(value = "库存品项")
    private BigDecimal stockCount;

    @ApiModelProperty(value = "库存成本额")
    private BigDecimal costAmt;

    @ApiModelProperty(value = "周转天数")
    private BigDecimal turnoverDays;
}
