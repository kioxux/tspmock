package com.gys.business.service.appservice;

import com.gys.business.controller.app.form.SalesChangeStatisticForm;
import com.gys.business.controller.app.vo.SalesChangeStatisticVO;

import java.util.List;

public interface SalesChangeStatisticService {

    /**
     * @Author jiht
     * @Description 成分销售变化TOP排名统计
     * @Date 2021/12/3 14:24
     * @Param [inData]
     * @return java.util.List<com.gys.business.service.data.GrossConstructionOutData>
     **/
    List<SalesChangeStatisticVO> salesChangeStatistic(SalesChangeStatisticForm inData);

}
