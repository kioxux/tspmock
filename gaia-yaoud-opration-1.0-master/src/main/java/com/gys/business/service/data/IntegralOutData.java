package com.gys.business.service.data;

import lombok.Data;

/**
 * @desc: 出参
 * @author: ZhangChi
 * @createTime: 2021/11/29 14:16
 */
@Data
public class IntegralOutData {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 开始日期
     */
    private String startDate;

    /**
     * 结束日期
     */
    private String endDate;

    /**
     * 状态：0-保存，1-审核
     */
    private Integer state;

    /**
     * 卡类型编号1钻石，2白金，3金，4银，5普通
     */
    private String cardType;

    /**
     * 生效时段
     */
    private String effectiveTime;

    /**
     * 日期频率
     */
    private String dateFrequency;

    /**
     * 星期频率
     */
    private String weekFrequency;
}
