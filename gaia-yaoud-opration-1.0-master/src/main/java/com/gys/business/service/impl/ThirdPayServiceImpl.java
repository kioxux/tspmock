package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdSytAccountMapper;
import com.gys.business.mapper.GaiaSdSytOperationMapper;
import com.gys.business.mapper.entity.GaiaSdSytAccount;
import com.gys.business.mapper.entity.GaiaSdSytOperation;
import com.gys.business.service.ThirdPayService;
import com.gys.business.service.data.thirdPay.SytOperationInData;
import com.gys.business.service.data.thirdPay.SytOperationOutData;
import com.gys.business.service.data.thirdPay.SytRevokeOrderInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.ResultVO;
import com.gys.common.exception.BusinessException;
import com.gys.util.thirdPay.ThirdPayInterface;
import com.gys.util.thirdPay.shouYinTong.SytBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ThirdPayServiceImpl implements ThirdPayService {

    @Autowired
    private GaiaSdSytAccountMapper sytAccountMapper;

    @Autowired
    private GaiaSdSytOperationMapper sytOperationMapper;

    @Resource(type = SytBusiness.class)
    private ThirdPayInterface sytBusiness;

    @Override
    public ResultVO<Map<String, Object>> pay(GetLoginOutData userInfo, Map<String, Object> inData) {
        GaiaSdSytAccount account = getSytAccount(userInfo.getClient(),userInfo.getDepId());
        if(ObjectUtil.isEmpty(account)){
            throw new BusinessException("未设置收银通参数，不可使用！");
        }
        inData.put("client",userInfo.getClient());
        inData.put("brId",userInfo.getDepId());
        inData.put("channel", account.getChannel());
        inData.put("merId",account.getMerId());
        inData.put("key",account.getMd5Key());
        inData.put("userId",userInfo.getUserId());
        ResultVO<Map<String, Object>> res = sytBusiness.pay(inData);
        return res;
    }

    @Override
    public ResultVO<Map<String, Object>> query(GetLoginOutData userInfo, Map<String, Object> inData) {
        GaiaSdSytAccount account = getSytAccount(userInfo.getClient(),userInfo.getDepId());
        if(ObjectUtil.isEmpty(account)){
            throw new BusinessException("未设置收银通参数，不可使用！");
        }
        inData.put("channel", account.getChannel());
        inData.put("merId",account.getMerId());
        inData.put("key",account.getMd5Key());
        inData.put("client",userInfo.getClient());
        inData.put("brId",userInfo.getDepId());
        ResultVO<Map<String, Object>> res = sytBusiness.query(inData);
        return res;
    }

    @Override
    public ResultVO<Map<String, Object>> refundBack(GetLoginOutData userInfo, Map<String, Object> inData) {
        GaiaSdSytAccount account = getSytAccount(userInfo.getClient(),userInfo.getDepId());
        if(ObjectUtil.isEmpty(account)){
            throw new BusinessException("未设置收银通参数，不可使用！");
        }
        inData.put("client",userInfo.getClient());
        inData.put("brId",userInfo.getDepId());
        inData.put("channel", account.getChannel());
        inData.put("merId",account.getMerId());
        inData.put("key",account.getMd5Key());
        inData.put("userId",userInfo.getUserId());
        ResultVO<Map<String, Object>> res = sytBusiness.refundBack(inData);
        return res;
    }

    @Override
    public ResultVO<Map<String, Object>> revokePay(GetLoginOutData userInfo, Map<String, Object> inData) {
        GaiaSdSytAccount account = getSytAccount(userInfo.getClient(),userInfo.getDepId());
        if(ObjectUtil.isEmpty(account)){
            throw new BusinessException("未设置收银通参数，不可使用！");
        }
        inData.put("client",userInfo.getClient());
        inData.put("brId",userInfo.getDepId());
        inData.put("channel", account.getChannel());
        inData.put("merId",account.getMerId());
        inData.put("key",account.getMd5Key());
        inData.put("userId",userInfo.getUserId());
        ResultVO<Map<String, Object>> res = sytBusiness.revokePay(inData);
        return res;
    }

    public GaiaSdSytAccount getSytAccount(String client, String brId) {
        Example example = new Example(GaiaSdSytAccount.class);
        example.createCriteria().andEqualTo("client",client).andEqualTo("brId",brId);
        GaiaSdSytAccount res = sytAccountMapper.selectOneByExample(example);
        return res;
    }

    public List<SytOperationOutData> querySytOperation(GetLoginOutData userInfo, SytOperationInData inData) {
        inData.setClient(userInfo.getClient());
        return sytOperationMapper.querySytOperation(inData);
    }

    public void revokeOrder(SytRevokeOrderInData inData) {
        Example example = new Example(GaiaSdSytOperation.class);
        example.createCriteria().andEqualTo("client",inData.getClient()).andEqualTo("brId",inData.getStoreCode()).andEqualTo("outerNumber",inData.getOuterNumber());
        GaiaSdSytOperation data = sytOperationMapper.selectOneByExample(example);
        if(ObjectUtil.isEmpty(data)){
            throw new BusinessException("查无此订单！");
        }
        GetLoginOutData userInfo = new GetLoginOutData();
        userInfo.setClient(inData.getClient());
        userInfo.setDepId(inData.getStoreCode());
        Map<String, Object> params = new HashMap<>();
        params.put("outerNumber",data.getOuterNumber());
        params.put("oldOuterNumber",data.getOuterNumber());
        params.put("txnSeqId",data.getTxnSeqId());
        ResultVO<Map<String, Object>> res = revokePay(userInfo,params);
        if(ObjectUtil.isEmpty(res)){
            throw new BusinessException("撤单失败，返回为空！");
        }else{
            if(!res.isSuccess()) {
                throw new BusinessException(res.getMsg());
            }
        }
    }
}
