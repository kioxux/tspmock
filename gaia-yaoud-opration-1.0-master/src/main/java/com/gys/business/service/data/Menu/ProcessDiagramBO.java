package com.gys.business.service.data.Menu;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProcessDiagramBO {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "流程名称")
    private String flowName;

    @ApiModelProperty(value = "流程类型（1：父节点 2：子节点）")
    private String flowType;

    @ApiModelProperty(value = "父节点ID")
    private String paientId;

    @ApiModelProperty(value = "流程节点路径")
    private String routePath;

    @ApiModelProperty(value = "排序")
    private String sort;

//    @ApiModelProperty(value = "能打开的方式([WEB,APP])")
    private String canOpen;

    @ApiModelProperty(value = "图片")
    private String imgUrl;

    @ApiModelProperty(value = "能打开的方式([WEB,APP])")
    private String[] canOpenArr;

}
