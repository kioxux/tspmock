//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdCleandrawerDMapper;
import com.gys.business.mapper.GaiaSdCleandrawerHMapper;
import com.gys.business.mapper.GaiaSdExamineDMapper;
import com.gys.business.mapper.GaiaSdExamineHMapper;
import com.gys.business.mapper.GaiaSdStockBatchMapper;
import com.gys.business.mapper.entity.GaiaSdCleandrawerD;
import com.gys.business.mapper.entity.GaiaSdCleandrawerH;
import com.gys.business.mapper.entity.GaiaSdExamineD;
import com.gys.business.mapper.entity.GaiaSdExamineH;
import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.service.ExamineTipService;
import com.gys.business.service.data.ClearBoxInfoOutData;
import com.gys.business.service.data.ExamineTipOutData;
import com.gys.common.data.GetLoginOutData;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ExamineTipServiceImpl implements ExamineTipService {
    @Autowired
    private GaiaSdExamineHMapper gaiaSdExamineHMapper;
    @Autowired
    private GaiaSdExamineDMapper gaiaSdExamineDMapper;
    @Autowired
    private GaiaSdCleandrawerHMapper gaiaSdCleandrawerHMapper;
    @Autowired
    private GaiaSdCleandrawerDMapper gaiaSdCleandrawerDMapper;
    @Autowired
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;

    public ExamineTipServiceImpl() {
    }

    public List<ExamineTipOutData> list(GetLoginOutData userInfo) {
        GaiaSdExamineH sdExamineH = new GaiaSdExamineH();
        sdExamineH.setClientId(userInfo.getClient());
        List<GaiaSdExamineH> outData = this.gaiaSdExamineHMapper.select(sdExamineH);
        List<ExamineTipOutData> out = new ArrayList();

        for(int i = 0; i < outData.size(); ++i) {
            GaiaSdExamineH item = (GaiaSdExamineH)outData.get(i);
            GaiaSdCleandrawerH sdCleandrawerH = new GaiaSdCleandrawerH();
            sdCleandrawerH.setClientId(userInfo.getClient());
            sdCleandrawerH.setGschExaVoucherId(item.getGsehVoucherId());
            sdCleandrawerH = (GaiaSdCleandrawerH)this.gaiaSdCleandrawerHMapper.selectOne(sdCleandrawerH);
            if (!ObjectUtil.isNotEmpty(sdCleandrawerH)) {
                ExamineTipOutData temp = new ExamineTipOutData();
                BeanUtils.copyProperties(item, temp);
                DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
                String money = decimalFormat.format(item.getGsehTotalAmt());
                temp.setGsehTotalAmt(String.format("%.2f", Double.valueOf(money)));
                out.add(temp);
            }
        }

        return out;
    }

    @Transactional
    public ClearBoxInfoOutData getClearBoxInfo(ExamineTipOutData inData) {
        ClearBoxInfoOutData outData = new ClearBoxInfoOutData();
        GaiaSdCleandrawerH sdCleandrawerH = new GaiaSdCleandrawerH();
        sdCleandrawerH.setClientId(inData.getClientId());
        sdCleandrawerH.setGschExaVoucherId(inData.getGsehVoucherId());
        sdCleandrawerH = (GaiaSdCleandrawerH)this.gaiaSdCleandrawerHMapper.selectOne(sdCleandrawerH);
        if (ObjectUtil.isNotEmpty(sdCleandrawerH)) {
            outData.setGschVoucherId(sdCleandrawerH.getGschVoucherId());
            return outData;
        } else {
            String gschVoucherId = this.gaiaSdCleandrawerHMapper.selectMaxgschVoucherId();
            inData.setGschVoucherId(gschVoucherId);
            inData.setBrId(inData.getBrId());
            this.gaiaSdCleandrawerHMapper.insertData(inData);
            GaiaSdExamineD sdExamineD = new GaiaSdExamineD();
            sdExamineD.setClientId(inData.getClientId());
            sdExamineD.setGsedVoucherId(inData.getGsehVoucherId());
            List<GaiaSdExamineD> sdExamineDList = this.gaiaSdExamineDMapper.select(sdExamineD);

            for(int i = 0; i < sdExamineDList.size(); ++i) {
                GaiaSdExamineD item = (GaiaSdExamineD)sdExamineDList.get(i);
                GaiaSdCleandrawerD date = new GaiaSdCleandrawerD();
                date.setClientId(inData.getClientId());
                date.setGscdVoucherId(inData.getGschVoucherId());
                date.setGscdSerial(item.getGsedSerial());
                date.setGscdProId(item.getGsedProId());
                date.setGscdBatchNo(item.getGsedBatchNo());
                date.setGscdBatch(item.getGsedBatch());
                date.setGscdValidDate(item.getGsedValidDate());
                GaiaSdStockBatch gaiaSdStockBatch = new GaiaSdStockBatch();
                gaiaSdStockBatch.setClientId(inData.getClientId());
                gaiaSdStockBatch.setGssbBrId(inData.getBrId());
                gaiaSdStockBatch.setGssbProId(item.getGsedProId());
                gaiaSdStockBatch.setGssbBatch(item.getGsedBatch());
                gaiaSdStockBatch.setGssbBatchNo(item.getGsedBatchNo());
                gaiaSdStockBatch = (GaiaSdStockBatch)this.gaiaSdStockBatchMapper.selectOne(gaiaSdStockBatch);
                if (ObjectUtil.isNotEmpty(gaiaSdStockBatch)) {
                    date.setGscdQty(gaiaSdStockBatch.getGssbQty());
                } else {
                    date.setGscdQty("");
                }

                date.setGscdStatus("0");
                date.setGscdDate("");
                this.gaiaSdCleandrawerDMapper.insert(date);
            }

            outData.setGschVoucherId(inData.getGschVoucherId());
            return outData;
        }
    }
}
