package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.mapper.entity.GaiaSdSystemPara;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.IStoreMaintenService;
import com.gys.common.enums.GaiaSdSystemParamEnum;
import com.gys.common.enums.SdMessageTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author ：gyx
 * @Date ：Created in 9:38 2021/11/5
 * @Description：门店养护消息提醒service
 * @Modified By：gyx
 * @Version:
 */
@Service
public class StoreMaintenServiceImpl implements IStoreMaintenService {
    @Autowired
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;

    @Autowired
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Autowired
    private GaiaWmsYangHuMapper gaiaWmsYangHuMapper;

    @Autowired
    private GaiaSdMessageMapper gaiaSdMessageMapper;

    @Override
    public void dailyInsertStoreMaintenMessageJob() {
        List<String> clientList = gaiaFranchiseeMapper.getAllClientId();
        //当前月份
        String currentMonth = DateUtil.format(DateUtil.date(), "yyyyMM");
        //String currentMonth = "202108";
        for (String client : clientList) {
            //1.查找该加盟商在系统参数表中的设置
            List<GaiaStoreData> storeData = gaiaStoreDataMapper.findList(client);
            for (GaiaStoreData store : storeData) {
                Example example = new Example(GaiaSdSystemPara.class);
                example
                        .createCriteria()
                        .andEqualTo("clientId",client)
                        .andEqualTo("gsspBrId",store.getStoCode())
                        .andEqualTo("gsspId", GaiaSdSystemParamEnum.STORE_MAINTEN.code);
                GaiaSdSystemPara gaiaSdSystemPara = gaiaSdSystemParaMapper.selectOneByExample(example);
                if (ObjectUtil.isNull(gaiaSdSystemPara) || StrUtil.isEmpty(gaiaSdSystemPara.getGsspPara()))
                {
                    continue;
                }
                Integer systemParam = new Integer(gaiaSdSystemPara.getGsspPara());
                Integer count=gaiaWmsYangHuMapper.selectStoreMaintenData(client,store.getStoCode(),currentMonth);
                compareAndInsertMessage(client,store.getStoCode(),store.getStoShortName(),systemParam,count);
            }

        }

    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void compareAndInsertMessage(String client, String stoCode, String stoName, Integer systemParam, Integer count) {
        if (count.equals(0))
        {
            return;
        }
        if (count > systemParam)
        {
            insertMessage(client,stoCode,stoName, systemParam);
        }
        if (count < systemParam)
        {
            insertMessage(client,stoCode,stoName,count);
        }
    }

    private void insertMessage(String client, String stoCode, String stoName, int num)
    {
        String voucherIdWeb = gaiaSdMessageMapper.selectNextVoucherId(client, "store");
        GaiaSdMessage sdMessageWeb=new GaiaSdMessage();
        sdMessageWeb.setClient(client);
        sdMessageWeb.setGsmId(stoCode);
        sdMessageWeb.setGsmType(SdMessageTypeEnum.STORE_PRODUCT_MAINTEN.code);
        sdMessageWeb.setGsmPage(SdMessageTypeEnum.STORE_PRODUCT_MAINTEN.page);
        sdMessageWeb.setGsmPlatForm("WEB");
        sdMessageWeb.setGsmDeleteFlag("0");
        sdMessageWeb.setGsmVoucherId(voucherIdWeb);
        sdMessageWeb.setGsmArriveDate(DateUtil.format(DateUtil.date(),"yyyyMMdd"));
        sdMessageWeb.setGsmArriveTime(DateUtil.format(DateUtil.date(),"HHmmss"));
        sdMessageWeb.setGsmRemark(stoName+"今日有"+"<font color='red'>"+num+"</font>"+"条商品未养护,请及时查看并处理!");
        sdMessageWeb.setGsmFlag("N");
        sdMessageWeb.setGsmWarningDay(stoCode);
        gaiaSdMessageMapper.insertSelective(sdMessageWeb);

        String voucherIdFx = gaiaSdMessageMapper.selectNextVoucherIdFX(client, "store");
        GaiaSdMessage sdMessageFx=new GaiaSdMessage();
        sdMessageFx.setClient(client);
        sdMessageFx.setGsmId(stoCode);
        sdMessageFx.setGsmType(SdMessageTypeEnum.STORE_PRODUCT_MAINTEN.code);
        sdMessageFx.setGsmPage(SdMessageTypeEnum.STORE_PRODUCT_MAINTEN.page);
        sdMessageFx.setGsmPlatForm("FX");
        sdMessageFx.setGsmDeleteFlag("0");
        sdMessageFx.setGsmVoucherId(voucherIdFx);
        sdMessageFx.setGsmArriveDate(DateUtil.format(DateUtil.date(),"yyyyMMdd"));
        sdMessageFx.setGsmArriveTime(DateUtil.format(DateUtil.date(),"HHmmss"));
        sdMessageFx.setGsmRemark("您有"+num+"条商品未养护,请及时查看并处理!");
        sdMessageFx.setGsmFlag("N");
        sdMessageFx.setGsmValue("storeId="+stoCode+"&conserveMonth="+DateUtil.format(DateUtil.date(), "yyyyMM"));
        sdMessageFx.setGsmWarningDay(stoCode);
        gaiaSdMessageMapper.insertSelective(sdMessageFx);
    }
}
