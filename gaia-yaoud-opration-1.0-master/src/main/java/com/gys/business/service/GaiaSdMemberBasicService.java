package com.gys.business.service;

import com.gys.business.service.data.GetQueryMemberOutData;

import java.util.List;

public interface GaiaSdMemberBasicService {

    List<GetQueryMemberOutData> queryMemberByCondition(GetQueryMemberOutData getQueryMemberOutData);
}
