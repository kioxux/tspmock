package com.gys.business.service.data;

import lombok.Data;

@Data
public class ColdChainOutData {
    private String client;
    private String brId;

}
