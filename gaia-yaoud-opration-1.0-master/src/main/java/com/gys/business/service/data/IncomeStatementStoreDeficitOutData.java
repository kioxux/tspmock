package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "门店损耗")
public class IncomeStatementStoreDeficitOutData {
    @ApiModelProperty(value = "批号")
    private String batchNo;

    List<IncomeStatementStoreDeficitOutDataResult> stockDetailMap;
}
