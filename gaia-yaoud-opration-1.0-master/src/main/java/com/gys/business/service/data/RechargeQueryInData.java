package com.gys.business.service.data;

import lombok.Data;

@Data
public class RechargeQueryInData {
    private String client;
    private String brId;
    private String queryParam;
}
