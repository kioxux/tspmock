package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class IncomeStatementStockDetail {
    @ApiModelProperty(value = "有效期至")
    private String validUntil;

    @ApiModelProperty(value = "库存数量")
    private BigDecimal qty;
}
