//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetCosParaOutData {
    private String secretId;
    private String secretKey;
    private String regionName;
    private String bucketName;

    public GetCosParaOutData() {
    }

    public String getSecretId() {
        return this.secretId;
    }

    public String getSecretKey() {
        return this.secretKey;
    }

    public String getRegionName() {
        return this.regionName;
    }

    public String getBucketName() {
        return this.bucketName;
    }

    public void setSecretId(final String secretId) {
        this.secretId = secretId;
    }

    public void setSecretKey(final String secretKey) {
        this.secretKey = secretKey;
    }

    public void setRegionName(final String regionName) {
        this.regionName = regionName;
    }

    public void setBucketName(final String bucketName) {
        this.bucketName = bucketName;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetCosParaOutData)) {
            return false;
        } else {
            GetCosParaOutData other = (GetCosParaOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$secretId = this.getSecretId();
                    Object other$secretId = other.getSecretId();
                    if (this$secretId == null) {
                        if (other$secretId == null) {
                            break label59;
                        }
                    } else if (this$secretId.equals(other$secretId)) {
                        break label59;
                    }

                    return false;
                }

                Object this$secretKey = this.getSecretKey();
                Object other$secretKey = other.getSecretKey();
                if (this$secretKey == null) {
                    if (other$secretKey != null) {
                        return false;
                    }
                } else if (!this$secretKey.equals(other$secretKey)) {
                    return false;
                }

                Object this$regionName = this.getRegionName();
                Object other$regionName = other.getRegionName();
                if (this$regionName == null) {
                    if (other$regionName != null) {
                        return false;
                    }
                } else if (!this$regionName.equals(other$regionName)) {
                    return false;
                }

                Object this$bucketName = this.getBucketName();
                Object other$bucketName = other.getBucketName();
                if (this$bucketName == null) {
                    if (other$bucketName != null) {
                        return false;
                    }
                } else if (!this$bucketName.equals(other$bucketName)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetCosParaOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $secretId = this.getSecretId();
        result = result * 59 + ($secretId == null ? 43 : $secretId.hashCode());
        Object $secretKey = this.getSecretKey();
        result = result * 59 + ($secretKey == null ? 43 : $secretKey.hashCode());
        Object $regionName = this.getRegionName();
        result = result * 59 + ($regionName == null ? 43 : $regionName.hashCode());
        Object $bucketName = this.getBucketName();
        result = result * 59 + ($bucketName == null ? 43 : $bucketName.hashCode());
        return result;
    }

    public String toString() {
        return "GetCosParaOutData(secretId=" + this.getSecretId() + ", secretKey=" + this.getSecretKey() + ", regionName=" + this.getRegionName() + ", bucketName=" + this.getBucketName() + ")";
    }
}
