package com.gys.business.service;


import com.gys.business.mapper.entity.GaiaNbybProCatalog;
import com.gys.business.mapper.entity.MibSettlementZjnb;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.MibSettlementZjnbDto;
import com.gys.business.service.data.shanxi.SaleCountReportOutput;
import com.gys.business.service.data.zjnb.MibProductMatchDto;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

public interface ShanxiYbService {
    SaleCountReportOutput saleCountReport(InData inData, GetLoginOutData userInfo);
}
