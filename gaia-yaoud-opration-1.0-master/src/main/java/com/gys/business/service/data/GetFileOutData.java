package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetFileOutData {
   private String url;
   private String path;
}
