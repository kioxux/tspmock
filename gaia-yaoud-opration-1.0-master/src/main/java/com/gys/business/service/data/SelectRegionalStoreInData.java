package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 10:47 2021/10/15
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class SelectRegionalStoreInData implements Serializable {
    /**
     * 区域ID
     */
    @NotNull(message = "区域ID不能为空！")
    private Long id;

    /**
     * 加盟商
     */
    private String client;
}
