package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 退货所有信息
 *
 * @author xiaoyuan on 2020/12/11
 */
@Data
public class SaleReturnAllData implements Serializable {
    private static final long serialVersionUID = 8019422007970687389L;

    /**
     * 退货
     */
    private List<GetSaleReturnToData> returnToData;

    /**
     * 支付信息
     */
    private List<PaymentInformationOutData> payOutData;

}
