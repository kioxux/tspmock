package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AddProductSpecialInData implements Serializable {

    /**
     * 新增标识 1-商品 2-成分
     */
    @ApiModelProperty(value="新增标识 1-商品 2-成分")
    private String addFlag;

    /**
     * 商品编码(药德编码) 或者 成分编码(成分分类编码)
     */
    @ApiModelProperty(value="商品编码(药德编码) 或者 成分编码(成分分类编码)")
    private String proCodeOrCompCode;

    /**
     * 省
     */
    @ApiModelProperty(value="省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value="市")
    private String city;

    /**
     * 类型 1-季节，2-品牌
     */
    @ApiModelProperty(value="类型 1-季节，2-品牌")
    private String proType;

    /**
     * 标识 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌
     */
    @ApiModelProperty(value="标识 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌")
    private String proFlag;

    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private String createUser;
}
