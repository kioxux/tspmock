package com.gys.business.service.data.integralExchange;

import lombok.Data;

@Data
public class IntegralServeInData {
    private String clientId;//加盟商编码
    private String stoCode;//门店编码
    private String memberCordType;//会员卡类型
}
