package com.gys.business.service.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品调库-明细表(GaiaCommodityInventoryD)实体类
 *
 * @author makejava
 * @since 2021-10-19 16:01:15
 */
@Data
@CsvRow("调库单明细")
public class GaiaCommodityInventoryDOut implements Serializable {
    private static final long serialVersionUID = 769519039007682994L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店编码
     */
    @CsvCell(title = "店号",index = 2,fieldNo = 1)
    private String stoCode;
    /**
     * 门店名称
     */
    @CsvCell(title = "门店名称",index = 3,fieldNo = 1)
    private String stoName;
    /**
     * 商品调库单号
     */
    private String billCode;
    /**
     * 商品名称
     */
    @CsvCell(title = "商品名称",index = 5,fieldNo = 1)
    private String proName;
    /**
     * 商品编码
     */
    @CsvCell(title = "商品编码",index = 4,fieldNo = 1)
    private String proSelfCode;
    /**
     * 规格
     */
    @CsvCell(title = "规格",index = 6,fieldNo = 1)
    private String proSpces;
    /**
     * 生产厂家
     */
    @CsvCell(title = "生产厂家",index = 7,fieldNo = 1)
    private String proFactoryName;
    /**
     * 单位
     */
    @CsvCell(title = "单位",index = 8,fieldNo = 1)
    private String proUnit;
    /**
     * 月均销量
     */
    @CsvCell(title = "月均销量",index = 9,fieldNo = 1)
    private BigDecimal averageSalesQty;
    /**
     * 库存量
     */
    @CsvCell(title = "库存量",index = 10,fieldNo = 1)
    private BigDecimal inventoryQty;
    /**
     * 建议退库量
     */
    @CsvCell(title = "建议退库量",index = 11,fieldNo = 1)
    private BigDecimal suggestionReturnQty;
    /**
     * 建议退库成本额
     */
    @CsvCell(title = "建议退库成本额",index = 12,fieldNo = 1)
    private BigDecimal suggestionReturnCost;
    /**
     * 月均销售额
     */
    @CsvCell(title = "月均销售额",index = 13,fieldNo = 1)
    private BigDecimal averageSalesAmt;
    /**
     * 月均毛利额
     */
    @CsvCell(title = "月均毛利额",index = 14,fieldNo = 1)
    private BigDecimal averageSalesGross;
    /**
     * 月均毛利率
     */
    @CsvCell(title = "月均毛利率",index = 15,fieldNo = 1)
    private BigDecimal averageSalesGrossRate;
    /**
     * 成分分类
     */
    @CsvCell(title = "成分分类",index = 16,fieldNo = 1)
    private String proCompclass;
    /**
     * 商品分类
     */
    private String proClass;
    /**
     * 单据状态：0-未推送 1-已推送
     */
    private Integer status;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;
    @CsvCell(title = "成分分类描述",index = 17,fieldNo = 1)
    private String proCompName;
    @CsvCell(title = "大类",index = 18,fieldNo = 1)
    private String proBigClassCode;
    @CsvCell(title = "大类名称",index = 19,fieldNo = 1)
    private String proBigClassName;
    @CsvCell(title = "商品分类",index = 20,fieldNo = 1)
    private String proClassCode;
    @CsvCell(title = "商品分类描述",index = 21,fieldNo = 1)
    private String proClassName;
    @CsvCell(title = "推送情况",index = 1,fieldNo = 1)
    private String statusName;

    private String remark;

    private int param2;

    private BigDecimal cost;

}

