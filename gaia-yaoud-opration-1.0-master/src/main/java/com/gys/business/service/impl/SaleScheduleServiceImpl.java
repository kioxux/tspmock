//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaSdEmpGroupMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaSdEmpGroup;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.SaleScheduleService;
import com.gys.business.service.data.GetSaleScheduleInData;
import com.gys.business.service.data.GetSaleScheduleOutData;
import com.gys.business.service.data.GetSaleScheduleQueryInData;
import com.gys.business.service.data.GetUserOutData;
import com.gys.common.data.PageInfo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class SaleScheduleServiceImpl implements SaleScheduleService {
    @Autowired
    private GaiaSdEmpGroupMapper gaiaSdEmpGroupMapper;
    @Autowired
    private GaiaUserDataMapper gaiaUserDataMapper;

    public SaleScheduleServiceImpl() {
    }

    public PageInfo<GetSaleScheduleOutData> selectByPage(GetSaleScheduleQueryInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GetSaleScheduleOutData> outData = null;
        outData = this.gaiaSdEmpGroupMapper.selectByPage(inData);
        PageInfo pageInfo;
        if (CollectionUtils.isEmpty(outData)) {
            pageInfo = new PageInfo(new ArrayList());
        } else {
            for(int i = 0; i < outData.size(); ++i) {
                ((GetSaleScheduleOutData)outData.get(i)).setIndex(i + 1);
            }

            pageInfo = new PageInfo(outData);
        }

        return pageInfo;
    }

    public void update(List<GetSaleScheduleInData> inData) {
        inData.forEach((item) -> {
            this.gaiaSdEmpGroupMapper.update(item);
        });
    }

    public void delete(List<GetSaleScheduleInData> inData) {
        inData.forEach((item) -> {
            this.gaiaSdEmpGroupMapper.deleteData(item);
        });
    }

    public void addSave(GetSaleScheduleInData inData) {
        String gsegId = this.gaiaSdEmpGroupMapper.selectMaxId(inData);
        int gsegIdTemp = Integer.valueOf(gsegId) + 1;
        if (gsegIdTemp < 10) {
            gsegId = "0" + gsegIdTemp;
        } else {
            gsegId = String.valueOf(gsegIdTemp);
        }

        GaiaSdEmpGroup sdEmpGroup = new GaiaSdEmpGroup();
        BeanUtils.copyProperties(inData, sdEmpGroup);
        sdEmpGroup.setGsegId(gsegId);
        sdEmpGroup.setGsegEmp1(inData.getGsegEmpId1());
        sdEmpGroup.setGsegEmp2(inData.getGsegEmpId2());
        sdEmpGroup.setGsegEmp3(inData.getGsegEmpId3());
        sdEmpGroup.setGsegEmp4(inData.getGsegEmpId4());
        sdEmpGroup.setGsegEmp5(inData.getGsegEmpId5());
        this.gaiaSdEmpGroupMapper.insert(sdEmpGroup);
    }

    public List<GetUserOutData> queryUser(GetSaleScheduleInData inData) {
        GaiaUserData gaiaUserData = new GaiaUserData();
        gaiaUserData.setClient(inData.getClientId());
        gaiaUserData.setDepId(inData.getGsegBrId());
//        List<GaiaUserData> out = this.gaiaUserDataMapper.select(gaiaUserData);
        List<GaiaUserData> out = this.gaiaUserDataMapper.selectStoreUser(gaiaUserData);
        List<GetUserOutData> outDataList = new ArrayList();
        out.forEach((item) -> {
            if ("0".equals(item.getUserSta())) {
                GetUserOutData user = new GetUserOutData();
                user.setUserId(item.getUserId());
                user.setLoginName(item.getUserNam());
                outDataList.add(user);
            }

        });
        return outDataList;
    }
}
