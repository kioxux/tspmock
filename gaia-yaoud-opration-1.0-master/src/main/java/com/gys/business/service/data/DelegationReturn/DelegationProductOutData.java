package com.gys.business.service.data.DelegationReturn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "委托退货商品查询")
public class DelegationProductOutData {
    @ApiModelProperty(value = "商品编码")
    private String selfCode;
    @ApiModelProperty(value = "商品名称")
    private String proCommonname;
}
