package com.gys.business.service;


import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationD;

import java.util.List;

/**
 * 公司-新品评估-明细表(GaiaEntNewProductEvaluationD)表服务接口
 *
 * @author makejava
 * @since 2021-07-19 10:16:26
 */
public interface GaiaEntNewProductEvaluationDService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaEntNewProductEvaluationD queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaEntNewProductEvaluationD> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param gaiaEntNewProductEvaluationD 实例对象
     * @return 实例对象
     */
    GaiaEntNewProductEvaluationD insert(GaiaEntNewProductEvaluationD gaiaEntNewProductEvaluationD);

    /**
     * 修改数据
     *
     * @param gaiaEntNewProductEvaluationD 实例对象
     * @return 实例对象
     */
    GaiaEntNewProductEvaluationD update(GaiaEntNewProductEvaluationD gaiaEntNewProductEvaluationD);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
