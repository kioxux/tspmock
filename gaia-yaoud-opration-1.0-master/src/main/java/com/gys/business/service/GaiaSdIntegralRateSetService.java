package com.gys.business.service;

import com.gys.common.data.PageInfo;
import com.gys.common.request.InsertProductSettingReq;
import com.gys.common.request.SelectCheckIfInDbReq;
import com.gys.common.request.SelectInteProductListReq;
import com.gys.common.request.UpdateInteProductSettingReq;
import com.gys.common.response.InteProductSettingResponse;
import com.gys.common.response.SelectInteProductListResponse;
import com.gys.common.response.SelectProductStResponse;
import com.gys.common.response.SelectStoreListResponse;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 积分商品设置表 服务类
 * </p>
 *
 * @author wangyifan
 * @since 2021-07-28
 */
public interface GaiaSdIntegralRateSetService {

    /**
     * 初始化门店列表
     *
     * @param client 加盟商
     * @return
     */
    List<SelectStoreListResponse> setClientId(String client,String stoName);

    /**
     * 新增商品设置
     *
     * @param req
     * @return
     */
    InteProductSettingResponse insertProductSetting(InsertProductSettingReq req);

    /**
     * 会员积分商品列表
     *
     * @param req
     * @return
     */
    PageInfo<SelectInteProductListResponse> selectInteProductPageList(SelectInteProductListReq req);

    /**
     * 删除商品积分设置
     *
     * @param proCode
     * @param client
     * @return
     */
    InteProductSettingResponse deleteInteProductSetting(String id, String client);

    /**
     * 修改商品积分设置
     *
     * @param req
     * @return
     */
    InteProductSettingResponse updateInteProductSetting(UpdateInteProductSettingReq req);

    /**
     * 会员商品积分数据回显
     *
     * @param id
     * @param client
     * @return
     */
    SelectInteProductListResponse selectInteProductSetting(String id, String client);

    /**
     * 校验商品编码是否已存在
     *
     * @param req
     * @return
     */
    InteProductSettingResponse selectProductCode(SelectCheckIfInDbReq req);

    /**
     * 商品编码导入
     * @param client
     * @param file
     * @return
     */
    List<SelectProductStResponse> insertProductCodeImport(List<Map<String,String>> importInDataList, String client);

    /**
     * 初始化日期
     * @param client
     * @return
     */
    Map<String, Object> getPeriodDate();

}
