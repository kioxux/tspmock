package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AcceptVoucherOutData {
    @ApiModelProperty("拣货单号")
    private String pickProId;
    @ApiModelProperty("配送单数量")
    private String orderCount;
    @ApiModelProperty("总数量")
    private String totalCount;
    @ApiModelProperty("总金额")
    private String totalAmt;
    @ApiModelProperty("总件数")
    private String totalPickCount;
    @ApiModelProperty("总配送金额")
    private String psTotalAmt;
    private List<AcceptSentOrderOutData> orderList;
}
