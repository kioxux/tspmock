package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

@Data
public class BatchControlSyncData {
    private String client;
    private String site;
    private List<BatchControlItemData> items;
}
