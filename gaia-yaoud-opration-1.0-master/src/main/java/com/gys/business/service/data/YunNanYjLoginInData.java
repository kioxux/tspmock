package com.gys.business.service.data;

import lombok.Data;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/12 16:33
 */
@Data
public class YunNanYjLoginInData {
    private String instNo;
    private String registrationNo;
    private String cameraIp;
}
