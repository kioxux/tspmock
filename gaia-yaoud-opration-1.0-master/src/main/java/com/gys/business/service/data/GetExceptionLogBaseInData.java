//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetExceptionLogBaseInData implements Serializable {
    private static final long serialVersionUID = 7156472062322164683L;
    private Long id;
    private String message;
    private String cause;
    private String method;
    private String createTime;

    public GetExceptionLogBaseInData() {
    }

    public Long getId() {
        return this.id;
    }

    public String getMessage() {
        return this.message;
    }

    public String getCause() {
        return this.cause;
    }

    public String getMethod() {
        return this.method;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public void setCause(final String cause) {
        this.cause = cause;
    }

    public void setMethod(final String method) {
        this.method = method;
    }

    public void setCreateTime(final String createTime) {
        this.createTime = createTime;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetExceptionLogBaseInData)) {
            return false;
        } else {
            GetExceptionLogBaseInData other = (GetExceptionLogBaseInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$id = this.getId();
                    Object other$id = other.getId();
                    if (this$id == null) {
                        if (other$id == null) {
                            break label71;
                        }
                    } else if (this$id.equals(other$id)) {
                        break label71;
                    }

                    return false;
                }

                Object this$message = this.getMessage();
                Object other$message = other.getMessage();
                if (this$message == null) {
                    if (other$message != null) {
                        return false;
                    }
                } else if (!this$message.equals(other$message)) {
                    return false;
                }

                label57: {
                    Object this$cause = this.getCause();
                    Object other$cause = other.getCause();
                    if (this$cause == null) {
                        if (other$cause == null) {
                            break label57;
                        }
                    } else if (this$cause.equals(other$cause)) {
                        break label57;
                    }

                    return false;
                }

                Object this$method = this.getMethod();
                Object other$method = other.getMethod();
                if (this$method == null) {
                    if (other$method != null) {
                        return false;
                    }
                } else if (!this$method.equals(other$method)) {
                    return false;
                }

                Object this$createTime = this.getCreateTime();
                Object other$createTime = other.getCreateTime();
                if (this$createTime == null) {
                    if (other$createTime == null) {
                        return true;
                    }
                } else if (this$createTime.equals(other$createTime)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetExceptionLogBaseInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $id = this.getId();
        result = result * 59 + ($id == null ? 43 : $id.hashCode());
        Object $message = this.getMessage();
        result = result * 59 + ($message == null ? 43 : $message.hashCode());
        Object $cause = this.getCause();
        result = result * 59 + ($cause == null ? 43 : $cause.hashCode());
        Object $method = this.getMethod();
        result = result * 59 + ($method == null ? 43 : $method.hashCode());
        Object $createTime = this.getCreateTime();
        result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());
        return result;
    }

    public String toString() {
        return "GetExceptionLogBaseInData(id=" + this.getId() + ", message=" + this.getMessage() + ", cause=" + this.getCause() + ", method=" + this.getMethod() + ", createTime=" + this.getCreateTime() + ")";
    }
}
