//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "药品养护列表")
public class GetMedCheckOutData {
    @ApiModelProperty(value = "养护单号")
    private String gsmchVoucherId;
    @ApiModelProperty(value = "店名")
    private String gsmchBrName;
    @ApiModelProperty(value = "养护类型 1-一般养护 2-重点养护")
    private String gsmchType;
    @ApiModelProperty(value = "养护日期")
    private String gsmchDate;
    @ApiModelProperty(value = "养护人员")
    private String gsmchEmp;
    @ApiModelProperty(value = "审核人员")
    private String gsmchExamineEmp;
    @ApiModelProperty(value = "审核状态")
    private String gsmchStatus;
    private List<GetMedCheckDOutData> medCheckDs;

    public GetMedCheckOutData() {
    }

    public String getGsmchVoucherId() {
        return this.gsmchVoucherId;
    }

    public String getGsmchBrName() {
        return this.gsmchBrName;
    }

    public String getGsmchType() {
        return this.gsmchType;
    }

    public String getGsmchDate() {
        return this.gsmchDate;
    }

    public String getGsmchEmp() {
        return this.gsmchEmp;
    }

    public String getGsmchExamineEmp() {
        return this.gsmchExamineEmp;
    }

    public String getGsmchStatus() {
        return this.gsmchStatus;
    }

    public List<GetMedCheckDOutData> getMedCheckDs() {
        return this.medCheckDs;
    }

    public void setGsmchVoucherId(final String gsmchVoucherId) {
        this.gsmchVoucherId = gsmchVoucherId;
    }

    public void setGsmchBrName(final String gsmchBrName) {
        this.gsmchBrName = gsmchBrName;
    }

    public void setGsmchType(final String gsmchType) {
        this.gsmchType = gsmchType;
    }

    public void setGsmchDate(final String gsmchDate) {
        this.gsmchDate = gsmchDate;
    }

    public void setGsmchEmp(final String gsmchEmp) {
        this.gsmchEmp = gsmchEmp;
    }

    public void setGsmchExamineEmp(final String gsmchExamineEmp) {
        this.gsmchExamineEmp = gsmchExamineEmp;
    }

    public void setGsmchStatus(final String gsmchStatus) {
        this.gsmchStatus = gsmchStatus;
    }

    public void setMedCheckDs(final List<GetMedCheckDOutData> medCheckDs) {
        this.medCheckDs = medCheckDs;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetMedCheckOutData)) {
            return false;
        } else {
            GetMedCheckOutData other = (GetMedCheckOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label107: {
                    Object this$gsmchVoucherId = this.getGsmchVoucherId();
                    Object other$gsmchVoucherId = other.getGsmchVoucherId();
                    if (this$gsmchVoucherId == null) {
                        if (other$gsmchVoucherId == null) {
                            break label107;
                        }
                    } else if (this$gsmchVoucherId.equals(other$gsmchVoucherId)) {
                        break label107;
                    }

                    return false;
                }

                Object this$gsmchBrName = this.getGsmchBrName();
                Object other$gsmchBrName = other.getGsmchBrName();
                if (this$gsmchBrName == null) {
                    if (other$gsmchBrName != null) {
                        return false;
                    }
                } else if (!this$gsmchBrName.equals(other$gsmchBrName)) {
                    return false;
                }

                Object this$gsmchType = this.getGsmchType();
                Object other$gsmchType = other.getGsmchType();
                if (this$gsmchType == null) {
                    if (other$gsmchType != null) {
                        return false;
                    }
                } else if (!this$gsmchType.equals(other$gsmchType)) {
                    return false;
                }

                label86: {
                    Object this$gsmchDate = this.getGsmchDate();
                    Object other$gsmchDate = other.getGsmchDate();
                    if (this$gsmchDate == null) {
                        if (other$gsmchDate == null) {
                            break label86;
                        }
                    } else if (this$gsmchDate.equals(other$gsmchDate)) {
                        break label86;
                    }

                    return false;
                }

                label79: {
                    Object this$gsmchEmp = this.getGsmchEmp();
                    Object other$gsmchEmp = other.getGsmchEmp();
                    if (this$gsmchEmp == null) {
                        if (other$gsmchEmp == null) {
                            break label79;
                        }
                    } else if (this$gsmchEmp.equals(other$gsmchEmp)) {
                        break label79;
                    }

                    return false;
                }

                label72: {
                    Object this$gsmchExamineEmp = this.getGsmchExamineEmp();
                    Object other$gsmchExamineEmp = other.getGsmchExamineEmp();
                    if (this$gsmchExamineEmp == null) {
                        if (other$gsmchExamineEmp == null) {
                            break label72;
                        }
                    } else if (this$gsmchExamineEmp.equals(other$gsmchExamineEmp)) {
                        break label72;
                    }

                    return false;
                }

                Object this$gsmchStatus = this.getGsmchStatus();
                Object other$gsmchStatus = other.getGsmchStatus();
                if (this$gsmchStatus == null) {
                    if (other$gsmchStatus != null) {
                        return false;
                    }
                } else if (!this$gsmchStatus.equals(other$gsmchStatus)) {
                    return false;
                }

                Object this$medCheckDs = this.getMedCheckDs();
                Object other$medCheckDs = other.getMedCheckDs();
                if (this$medCheckDs == null) {
                    if (other$medCheckDs != null) {
                        return false;
                    }
                } else if (!this$medCheckDs.equals(other$medCheckDs)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetMedCheckOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $gsmchVoucherId = this.getGsmchVoucherId();
        result = result * 59 + ($gsmchVoucherId == null ? 43 : $gsmchVoucherId.hashCode());
        Object $gsmchBrName = this.getGsmchBrName();
        result = result * 59 + ($gsmchBrName == null ? 43 : $gsmchBrName.hashCode());
        Object $gsmchType = this.getGsmchType();
        result = result * 59 + ($gsmchType == null ? 43 : $gsmchType.hashCode());
        Object $gsmchDate = this.getGsmchDate();
        result = result * 59 + ($gsmchDate == null ? 43 : $gsmchDate.hashCode());
        Object $gsmchEmp = this.getGsmchEmp();
        result = result * 59 + ($gsmchEmp == null ? 43 : $gsmchEmp.hashCode());
        Object $gsmchExamineEmp = this.getGsmchExamineEmp();
        result = result * 59 + ($gsmchExamineEmp == null ? 43 : $gsmchExamineEmp.hashCode());
        Object $gsmchStatus = this.getGsmchStatus();
        result = result * 59 + ($gsmchStatus == null ? 43 : $gsmchStatus.hashCode());
        Object $medCheckDs = this.getMedCheckDs();
        result = result * 59 + ($medCheckDs == null ? 43 : $medCheckDs.hashCode());
        return result;
    }

    public String toString() {
        return "GetMedCheckOutData(gsmchVoucherId=" + this.getGsmchVoucherId() + ", gsmchBrName=" + this.getGsmchBrName() + ", gsmchType=" + this.getGsmchType() + ", gsmchDate=" + this.getGsmchDate() + ", gsmchEmp=" + this.getGsmchEmp() + ", gsmchExamineEmp=" + this.getGsmchExamineEmp() + ", gsmchStatus=" + this.getGsmchStatus() + ", medCheckDs=" + this.getMedCheckDs() + ")";
    }
}
