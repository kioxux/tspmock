package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaThirdPaymentTonglianMapper;
import com.gys.business.mapper.entity.GaiaThirdPaymentTonglian;
import com.gys.business.service.TongLianService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 通联支付接口
 * @author huxinxin
 */
@Service
public class TongLianServiceImpl implements TongLianService {
    @Resource
    private GaiaThirdPaymentTonglianMapper tonglianMapper;

    @Override
    public long insert(GaiaThirdPaymentTonglian inData) {
        tonglianMapper.insertRetuenKeys(inData);
        return inData.getId();
    }

    @Override
    public GaiaThirdPaymentTonglian selectById(GaiaThirdPaymentTonglian inData) {
        return tonglianMapper.selectById(inData.getId());
    }
}
