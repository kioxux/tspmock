package com.gys.business.service.data.DelegationReturn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "委托退货保存")
public class DelegationReturnInfoData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店")
    private String storeCode;
    @ApiModelProperty(value = "配送中心编码")
    private String stoChainHead;
    @ApiModelProperty(value = "操作人")
    private String userId;
    //列表是否展示了 退库单的全部数据
    @ApiModelProperty(value = "是否整单处理",required = true)
    private String voucherType;
    private List<RetrunApprovalInData> retrunApprovalInData;

}
