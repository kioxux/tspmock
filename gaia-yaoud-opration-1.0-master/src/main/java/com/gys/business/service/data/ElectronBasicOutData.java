package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回实体
 *
 * @author xiaoyuan on 2020/9/25
 */
@Data
public class ElectronBasicOutData implements Serializable {
    private static final long serialVersionUID = 3206560659606163825L;
    /**
     * 序号
     */
    @ApiModelProperty(value = "序号")
    private Integer index;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 电子券活动号
     */
    @ApiModelProperty(value = "电子券号")
    private String gsebId;
    /**
     * 电子券描述
     */
    @ApiModelProperty(value = "电子券描述")
    private String gsebName;

    /**
     * 面值
     */
    @ApiModelProperty(value = "面值")
    private String gsebAmt;

    /**
     * 是否启用  N为否，Y为是
     */
    @ApiModelProperty(value = "是否启用")
    private String gsebStatus;

    /**
     * 有效时长  按天
     */
    @ApiModelProperty(value = "有效天数")
    private String gsebDuration;
    private String gsebTypeName;
    private String gsebType;
}
