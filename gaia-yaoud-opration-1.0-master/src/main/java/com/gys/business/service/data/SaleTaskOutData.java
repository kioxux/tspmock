package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel
public class SaleTaskOutData {
    @ApiModelProperty(value = "月份")
    private String monthPlan;
    @ApiModelProperty(value = "天数")
    private Integer daysPlan;
    @ApiModelProperty(value = "营业额")
    private BigDecimal thSaleAmt;
    @ApiModelProperty(value = "毛利额")
    private BigDecimal thSaleGross;
    @ApiModelProperty(value = "会员卡")
    private BigDecimal thMcardQty;
    @ApiModelProperty(value = "创建人")
    private String userName;
    @ApiModelProperty(value = "创建日期")
    private String creDate;
    @ApiModelProperty(value = "状态")
    private String state;
    @ApiModelProperty(value = "状态名称")
    private String stateName;
    @ApiModelProperty(value = "门店数")
    private String count;
}
