package com.gys.business.service;

/**
 * @Author ：gyx
 * @Date ：Created in 9:37 2021/11/5
 * @Description：门店养护service
 * @Modified By：gyx
 * @Version:
 */
public interface IStoreMaintenService {

    void dailyInsertStoreMaintenMessageJob();

    void compareAndInsertMessage(String client, String stoCode, String stoName, Integer systemParam, Integer count);
}
