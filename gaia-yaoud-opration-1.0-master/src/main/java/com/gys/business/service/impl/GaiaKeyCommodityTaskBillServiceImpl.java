package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaKeyCommodityTaskBillDetailMapper;
import com.gys.business.mapper.GaiaKeyCommodityTaskBillRelationMapper;
import com.gys.business.mapper.GaiaKeyCommodityTaskBillStoreMapper;
import com.gys.business.mapper.entity.*;
import com.gys.business.mapper.GaiaKeyCommodityTaskBillMapper;
import com.gys.business.service.GaiaKeyCommodityTaskBillService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.DateUtil;
import com.gys.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 重点商品任务—主表(GaiaKeyCommodityTaskBill)表服务实现类
 *
 * @author makejava
 * @since 2021-09-01 15:37:55
 */
@Service("gaiaKeyCommodityTaskBillService")
@Slf4j
public class GaiaKeyCommodityTaskBillServiceImpl implements GaiaKeyCommodityTaskBillService {
    @Resource
    private GaiaKeyCommodityTaskBillMapper gaiaKeyCommodityTaskBillMapper;
    @Resource
    private GaiaKeyCommodityTaskBillStoreMapper gaiaKeyCommodityTaskBillStoreMapper;
    @Resource
    private GaiaKeyCommodityTaskBillDetailMapper gaiaKeyCommodityTaskBillDetailMapper;
    @Resource
    private GaiaKeyCommodityTaskBillRelationMapper gaiaKeyCommodityTaskBillRelationMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object insertBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData param) {
        if (ObjectUtil.isEmpty(param.getId())) {
            //新增
            if (CollectionUtils.isEmpty(param.getInsertStoCodeList())) {
                throw new BusinessException("请先选择该任务适用门店！");
            }
            List<String> insertStoCodeList = param.getInsertStoCodeList();
            Date now = new Date();
            //新增 GAIA_KEY_COMMODITY_TASK_BILL
            GaiaKeyCommodityTaskBill bill = insertBillInfo(userInfo, param, insertStoCodeList, now);
            insertStore(userInfo, param, now, bill, insertStoCodeList);
            return bill.getBillCode();
        } else {
            //编辑
            Date now = new Date();
            GaiaKeyCommodityTaskBill bill = new GaiaKeyCommodityTaskBill();
            bill.setId(param.getId());
            bill.setClient(param.getClient());
            bill.setBillCode(param.getBillCode());
            bill.setStartTime(param.getStartTime());
            bill.setEndTime(param.getEndTime());
            bill.setValidQty(DateUtil.getBetweenDay(param.getStartTime(), param.getEndTime()) + 1);
            GaiaKeyCommodityTaskBillInData inData = new GaiaKeyCommodityTaskBillInData();
            inData.setClient(param.getClient());
            inData.setBillCode(param.getBillCode());
            int count = gaiaKeyCommodityTaskBillDetailMapper.getCount(inData);
            List<String> insertStoCodeList = param.getInsertStoCodeList();
            if (!CollectionUtils.isEmpty(param.getInsertStoCodeList())) {
                if (count > 0) {
                    throw new BusinessException("该任务已存在任务明细，如需修改适用门店，请先删除商品明细中的门店明细重新添加！");
                }
                insertStore(userInfo, param, now, bill, insertStoCodeList);
            }
            if (!CollectionUtils.isEmpty(param.getDeleteStoCodeList())) {
                if (count > 0) {
                    throw new BusinessException("该任务已存在任务明细，如需修改适用门店，请先删除商品明细中的门店明细重新添加！");
                }
                List<String> deleteStoCodeList = param.getDeleteStoCodeList();
                for (String stoCode : deleteStoCodeList) {
                    GaiaKeyCommodityTaskBillStore store = new GaiaKeyCommodityTaskBillStore();
                    store.setStoCode(stoCode);
                    store.setBillCode(param.getBillCode());
                    store.setClient(param.getClient());
                    store.setIsDelete(1);
                    store.setUpdateUser(userInfo.getUserId());
                    store.setUpdateTime(now);
                    gaiaKeyCommodityTaskBillStoreMapper.updateByStoCode(store);
                }
            }
            List<String> stoCodeList = gaiaKeyCommodityTaskBillStoreMapper.getStoCode(param.getClient(), param.getBillCode(), null);
            if (CollectionUtils.isEmpty(stoCodeList)) {
                throw new BusinessException("适用门店数不能为0！请添加适用门店数！");
            }
            bill.setStoreQty(stoCodeList.size());
            //原始单据
            GaiaKeyCommodityTaskBill oriBill = gaiaKeyCommodityTaskBillMapper.queryById(param.getId());
            /**
             * 如果修改了任务时间 需要重新计算 GAIA_KEY_COMMODITY_TASK_BILL_DETAIL ，GAIA_KEY_COMMODITY_TASK_BILL_RELATION
             * 中的 同期和上期的 销售额，销售量
             */
            if (oriBill.getStartTime() != bill.getStartTime() || oriBill.getEndTime() != bill.getEndTime()) {
                //重算 GAIA_KEY_COMMODITY_TASK_BILL_RELATION
                List<GaiaKeyCommodityTaskBillRelation> relationList = getGaiaKeyCommodityTaskBillRelations(param.getClient(), param.getBillCode());
                if (!CollectionUtils.isEmpty(relationList)) {
                    for (GaiaKeyCommodityTaskBillRelation relation : relationList) {
                        //查询上期 和同期的 销售额  与 销量
                        GaiaKeyCommodityTaskBillRelation salesInfo = gaiaKeyCommodityTaskBillMapper.getSalesInfo(param.getClient(), stoCodeList
                                , relation.getProSelfCode(), param.getStartTime(), param.getEndTime());
                        relation.setSqSalesAmt(salesInfo.getSqSalesAmt());
                        relation.setSqSalesQty(salesInfo.getSqSalesQty());
                        relation.setTqSalesAmt(salesInfo.getTqSalesAmt());
                        relation.setTqSalesQty(salesInfo.getTqSalesQty());
                        relation.setUpdateUser(userInfo.getUserId());
                        relation.setUpdateTime(now);
                        gaiaKeyCommodityTaskBillRelationMapper.update(relation);
                    }
                }
                //重算 GAIA_KEY_COMMODITY_TASK_BILL_DETAIL
                GaiaKeyCommodityTaskBillDetail detailParam = new GaiaKeyCommodityTaskBillDetail();
                detailParam.setIsDelete(0);
                detailParam.setClient(param.getClient());
                detailParam.setBillCode(param.getBillCode());
                List<GaiaKeyCommodityTaskBillDetail> detailList = gaiaKeyCommodityTaskBillDetailMapper.queryAll(detailParam);
                if (!CollectionUtils.isEmpty(detailList)) {
                    for (GaiaKeyCommodityTaskBillDetail detail : detailList) {
                        List<String> stoCodes = new ArrayList<>();
                        stoCodes.add(detail.getStoCode());
                        //查询上期 和同期的 销售额  与 销量
                        GaiaKeyCommodityTaskBillRelation salesInfo = gaiaKeyCommodityTaskBillMapper.getSalesInfo(param.getClient(), stoCodes
                                , detail.getProSelfCode(), param.getStartTime(), param.getEndTime());
                        detail.setSqSalesAmt(salesInfo.getSqSalesAmt());
                        detail.setSqSalesQty(salesInfo.getSqSalesQty());
                        detail.setTqSalesAmt(salesInfo.getTqSalesAmt());
                        detail.setTqSalesQty(salesInfo.getTqSalesQty());
                        detail.setUpdateUser(userInfo.getUserId());
                        detail.setUpdateTime(now);
                        gaiaKeyCommodityTaskBillDetailMapper.update(detail);
                    }
                }
            }
            gaiaKeyCommodityTaskBillMapper.update(bill);
            return bill.getBillCode();
        }
    }

    private List<GaiaKeyCommodityTaskBillRelation> getGaiaKeyCommodityTaskBillRelations(String client, String billCode) {
        GaiaKeyCommodityTaskBillRelation relationParam = new GaiaKeyCommodityTaskBillRelation();
        relationParam.setIsDelete(0);
        relationParam.setClient(client);
        relationParam.setBillCode(billCode);
        return gaiaKeyCommodityTaskBillRelationMapper.queryAll(relationParam);
    }

    @Override
    public PageInfo<GaiaKeyCommodityTaskBill> listBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        List<GaiaKeyCommodityTaskBill> outData = gaiaKeyCommodityTaskBillMapper.listBillInfo(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBill inData) {
        Date now = new Date();
        //1.0 删除 GAIA_KEY_COMMODITY_TASK_BILL
        GaiaKeyCommodityTaskBill bill = gaiaKeyCommodityTaskBillMapper.queryById(inData.getId());
        if (ObjectUtil.isEmpty(bill)) {
            throw new BusinessException("未查询到该单据，请刷新页面！");
        }
        bill.setIsDelete(1);
        bill.setUpdateUser(userInfo.getUserId());
        bill.setUpdateTime(now);
        gaiaKeyCommodityTaskBillMapper.update(bill);
        //2.0 删除 GAIA_KEY_COMMODITY_TASK_BILL_DETAIL
        GaiaKeyCommodityTaskBillDetail detailParam = new GaiaKeyCommodityTaskBillDetail();
        detailParam.setIsDelete(0);
        detailParam.setClient(inData.getClient());
        detailParam.setBillCode(inData.getBillCode());
        List<GaiaKeyCommodityTaskBillDetail> billDetails = gaiaKeyCommodityTaskBillDetailMapper.queryAll(detailParam);
        if (!CollectionUtils.isEmpty(billDetails)) {
            for (GaiaKeyCommodityTaskBillDetail billDetail : billDetails) {
                billDetail.setIsDelete(1);
                billDetail.setUpdateUser(userInfo.getUserId());
                billDetail.setUpdateTime(now);
                gaiaKeyCommodityTaskBillDetailMapper.update(billDetail);
            }
        }
        //3.0 删除 GAIA_KEY_COMMODITY_TASK_BILL_RELATION
        List<GaiaKeyCommodityTaskBillRelation> relationList = getGaiaKeyCommodityTaskBillRelations(inData.getClient(), inData.getBillCode());
        if (!CollectionUtils.isEmpty(relationList)) {
            for (GaiaKeyCommodityTaskBillRelation relation : relationList) {
                relation.setIsDelete(1);
                relation.setUpdateUser(userInfo.getUserId());
                relation.setUpdateTime(now);
                gaiaKeyCommodityTaskBillRelationMapper.update(relation);
            }
        }
        //4.0 删除 GAIA_KEY_COMMODITY_TASK_BILL_STORE
        GaiaKeyCommodityTaskBillStore storeParam = new GaiaKeyCommodityTaskBillStore();
        storeParam.setIsDelete(0);
        storeParam.setClient(inData.getClient());
        storeParam.setBillCode(inData.getBillCode());
        List<GaiaKeyCommodityTaskBillStore> storeList = gaiaKeyCommodityTaskBillStoreMapper.queryAll(storeParam);
        if (!CollectionUtils.isEmpty(storeList)) {
            for (GaiaKeyCommodityTaskBillStore store : storeList) {
                store.setIsDelete(1);
                store.setUpdateUser(userInfo.getUserId());
                store.setUpdateTime(now);
                gaiaKeyCommodityTaskBillStoreMapper.update(store);
            }
        }
        return true;
    }

    @Override
    public boolean checkBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBill inData) {
        GaiaKeyCommodityTaskBill bill = gaiaKeyCommodityTaskBillMapper.queryById(inData.getId());
        if (ObjectUtil.isEmpty(bill)) {
            throw new BusinessException("为查询到该任务单！请刷新页面！");
        } else {
            bill.setStatus(inData.getStatus());
            bill.setUpdateTime(new Date());
            bill.setUpdateUser(userInfo.getUserId());
            gaiaKeyCommodityTaskBillMapper.update(bill);
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object copyBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData) {
        GaiaKeyCommodityTaskBill bill = gaiaKeyCommodityTaskBillMapper.queryById(inData.getId());
        if (ObjectUtil.isEmpty(bill)) {
            throw new BusinessException("未查询到该任务单！请刷新页面！");
        }
        //1.0 复制 GAIA_KEY_COMMODITY_TASK_BILL
        Date now = new Date();
        GaiaKeyCommodityTaskBill copyBill = new GaiaKeyCommodityTaskBill();
        BeanUtils.copyProperties(bill, copyBill);
        copyBill.setId(null);
        String billCode = gaiaKeyCommodityTaskBillMapper.getBillCode(inData);
        copyBill.setBillCode(billCode);
        copyBill.setStatus(0);
        copyBill.setBillName(bill.getBillName() + "+副本");
        copyBill.setCreateUser(userInfo.getUserId());
        copyBill.setCreateTime(now);
        copyBill.setUpdateUser(userInfo.getUserId());
        copyBill.setUpdateTime(now);
        gaiaKeyCommodityTaskBillMapper.insert(copyBill);
        //2.0 复制 GAIA_KEY_COMMODITY_TASK_BILL_RELATION
        List<GaiaKeyCommodityTaskBillRelation> relationList = getGaiaKeyCommodityTaskBillRelations(inData.getClient(), bill.getBillCode());
        if (!CollectionUtils.isEmpty(relationList)) {
            for (GaiaKeyCommodityTaskBillRelation relation : relationList) {
                relation.setId(null);
                relation.setBillCode(billCode);
                relation.setCreateUser(userInfo.getUserId());
                relation.setCreateTime(now);
                relation.setUpdateUser(userInfo.getUserId());
                relation.setUpdateTime(now);
            }
            gaiaKeyCommodityTaskBillRelationMapper.insertBatch(relationList);
        }
        //3.0 复制 GAIA_KEY_COMMODITY_TASK_BILL_DETAIL
        GaiaKeyCommodityTaskBillDetail detailParam = new GaiaKeyCommodityTaskBillDetail();
        detailParam.setIsDelete(0);
        detailParam.setClient(inData.getClient());
        detailParam.setBillCode(bill.getBillCode());
        List<GaiaKeyCommodityTaskBillDetail> detailList = gaiaKeyCommodityTaskBillDetailMapper.queryAll(detailParam);
        if (!CollectionUtils.isEmpty(detailList)) {
            for (GaiaKeyCommodityTaskBillDetail detail : detailList) {
                Integer level = gaiaKeyCommodityTaskBillStoreMapper.getStoreLevel(inData.getClient(), detail.getStoCode());
                if (ObjectUtil.isEmpty(level)) {
                    throw new BusinessException("请先维护，门店编码：" + detail.getStoCode() + "的门店销售等级！");
                }
                detail.setId(null);
                detail.setBillCode(billCode);
                detail.setStoLevel(level.toString());
                detail.setCreateUser(userInfo.getUserId());
                detail.setCreateTime(now);
                detail.setUpdateUser(userInfo.getUserId());
                detail.setUpdateTime(now);
            }
            gaiaKeyCommodityTaskBillDetailMapper.insertBatch(detailList);
        }
        //4.0 复制 GAIA_KEY_COMMODITY_TASK_BILL_STORE
        GaiaKeyCommodityTaskBillStore storeParam = new GaiaKeyCommodityTaskBillStore();
        storeParam.setIsDelete(0);
        storeParam.setClient(inData.getClient());
        storeParam.setBillCode(bill.getBillCode());
        List<GaiaKeyCommodityTaskBillStore> storeList = gaiaKeyCommodityTaskBillStoreMapper.queryAll(storeParam);
        if (!CollectionUtils.isEmpty(storeList)) {
            for (GaiaKeyCommodityTaskBillStore store : storeList) {
                Integer level = gaiaKeyCommodityTaskBillStoreMapper.getStoreLevel(inData.getClient(), store.getStoCode());
                if (ObjectUtil.isEmpty(level)) {
                    throw new BusinessException("请先维护，门店编码：" + store.getStoCode() + "的门店销售等级！");
                }
                store.setId(null);
                store.setStoLevel(level);
                store.setBillCode(billCode);
                store.setCreateUser(userInfo.getUserId());
                store.setCreateTime(now);
                store.setUpdateUser(userInfo.getUserId());
                store.setUpdateTime(now);
            }
            gaiaKeyCommodityTaskBillStoreMapper.insertBatch(storeList);
        }
        return true;
    }

    @Override
    public Object getBillInfoById(GetLoginOutData userInfo, GaiaKeyCommodityTaskBill inData) {
        GaiaKeyCommodityTaskBill bill = gaiaKeyCommodityTaskBillMapper.getBillInfoByCode(inData);
        if (ObjectUtil.isEmpty(bill)) {
            throw new BusinessException("未查询到该任务单！请刷新页面！");
        }
        GaiaKeyCommodityTaskBillOutData outData = new GaiaKeyCommodityTaskBillOutData();
        BeanUtils.copyProperties(bill, outData);
        //查询适配门店集合
        GaiaKeyCommodityTaskBillStore param = new GaiaKeyCommodityTaskBillStore();
        param.setClient(userInfo.getClient());
        param.setBillCode(inData.getBillCode());
        param.setIsDelete(0);
        List<String> stores = gaiaKeyCommodityTaskBillStoreMapper.listStoreInfo(param);
        outData.setInsertStoCodeList(stores);
        outData.setStartTimeStr(DateUtil.dateToString(bill.getStartTime(), "yyyy-MM-dd"));
        outData.setEndTimeStr(DateUtil.dateToString(bill.getEndTime(), "yyyy-MM-dd"));
        return outData;
    }

    @Override
    public GaiaKeyCommodityTaskBillRelation getGoodsInfoById(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData) {
        List<String> stoCodeList = gaiaKeyCommodityTaskBillStoreMapper.getStoCode(inData.getClient(), inData.getBillCode(), null);
        if (CollectionUtils.isEmpty(stoCodeList)) {
            throw new BusinessException("请先维护该任务适用门店！");
        }
        return gaiaKeyCommodityTaskBillMapper.getSalesInfo(inData.getClient(), stoCodeList, inData.getProCode(), inData.getStartTime(), inData.getEndTime());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveGoodsInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillRelationInData inData) {
        //1.0 校验是否已存在
        checkHas(userInfo, inData);
        GaiaKeyCommodityTaskBillRelation relation = new GaiaKeyCommodityTaskBillRelation();
        BeanUtils.copyProperties(inData, relation);
        Date now = new Date();
        if (ObjectUtil.isEmpty(inData.getId())) {
            //新增
            relation.setClient(userInfo.getClient());
            relation.setCreateTime(now);
            relation.setCreateUser(userInfo.getUserId());
            relation.setUpdateUser(userInfo.getUserId());
            relation.setUpdateTime(now);
            relation.setIsDelete(0);
            gaiaKeyCommodityTaskBillRelationMapper.insert(relation);
        } else {
            //编辑
            relation.setUpdateUser(userInfo.getUserId());
            relation.setUpdateTime(now);
            gaiaKeyCommodityTaskBillRelationMapper.update(relation);
        }
        return true;
    }

    private void updateBillPlanQty(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillRelationInData inData, Date now) {
        GaiaKeyCommodityTaskBill planInfo = gaiaKeyCommodityTaskBillRelationMapper.getPlanInfo(inData);
        GaiaKeyCommodityTaskBill param = new GaiaKeyCommodityTaskBill();
        param.setBillCode(inData.getBillCode());
        param.setClient(inData.getClient());
        GaiaKeyCommodityTaskBill info = gaiaKeyCommodityTaskBillMapper.getBillInfoByCode(param);
        if (ObjectUtil.isEmpty(planInfo)) {
            info.setSalesAmt(0.0);
            info.setSalesQty(0.0);
        } else {
            info.setSalesAmt(planInfo.getSalesAmt());
            info.setSalesQty(planInfo.getSalesQty());
        }
        info.setUpdateTime(now);
        info.setUpdateUser(userInfo.getUserId());
        gaiaKeyCommodityTaskBillMapper.update(info);
    }

    @Override
    public Object getGoodsTaskById(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillRelationInData inData) {
        GaiaKeyCommodityTaskBillRelation relation = gaiaKeyCommodityTaskBillRelationMapper.queryById(inData.getId());
        if (ObjectUtil.isEmpty(relation)) {
            throw new BusinessException("该条记录不存在，请刷新页面！");
        }
        GaiaKeyCommodityTaskBillRelationOutData outData = new GaiaKeyCommodityTaskBillRelationOutData();
        outData.setId(relation.getId());
        outData.setBillCode(relation.getBillCode());
        outData.setProSelfCode(relation.getProSelfCode());
        outData.setProName(relation.getProName());
        outData.setPlanSalesQty(relation.getPlanSalesQty());
        outData.setPlanSalesAmt(relation.getPlanSalesAmt());
        //1.0 查询门店等级信息
        inData.setBillCode(relation.getBillCode());
        List<LevelInfoOutData> levelInfos = gaiaKeyCommodityTaskBillDetailMapper.listLevelInfo(inData);
        outData.setLevelInfo(levelInfos);
        //2.0 设置信息
        GaiaKeyCommodityTaskBillInData param = new GaiaKeyCommodityTaskBillInData();
        param.setBillCode(relation.getBillCode());
        param.setClient(inData.getClient());
        param.setProSelfCode(relation.getProSelfCode());
        List<GaiaKeyCommodityTaskBillDetail> details = gaiaKeyCommodityTaskBillDetailMapper.listInfo(param);
        outData.setDetailList(details);
        return outData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object saveStoreInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillRelationInData inData) {
        List<LevelInfoOutData> levelInfos = inData.getLevelInfos();
        GaiaKeyCommodityTaskBill param = new GaiaKeyCommodityTaskBill();
        param.setBillCode(inData.getBillCode());
        param.setClient(inData.getClient());
        GaiaKeyCommodityTaskBill bill = gaiaKeyCommodityTaskBillMapper.getBillInfoByCode(param);
        GaiaKeyCommodityTaskBillRelation relationInfo = gaiaKeyCommodityTaskBillRelationMapper.getRelationInfoByCode(inData);
        List<GaiaKeyCommodityTaskBillDetailVO> storeList = gaiaKeyCommodityTaskBillDetailMapper.listStoreLevelInfo(inData);
        Map<String, List<GaiaKeyCommodityTaskBillDetailVO>> collect = storeList.stream().collect(Collectors.groupingBy(GaiaKeyCommodityTaskBillDetailVO::getLevelCode));
        List<GaiaKeyCommodityTaskBillDetail> detailList = new ArrayList<>();
        Date now = new Date();
        collect.forEach((key, entry) -> {
            for (LevelInfoOutData levelInfo : levelInfos) {
                if (key.equals(levelInfo.getLevelCode())) {
                    for (GaiaKeyCommodityTaskBillDetailVO vo : entry) {
                        GaiaKeyCommodityTaskBillDetail detail = new GaiaKeyCommodityTaskBillDetail();
                        detail.setClient(inData.getClient());
                        detail.setBillCode(inData.getBillCode());
                        detail.setProSelfCode(inData.getProSelfCode());
                        detail.setStoCode(vo.getStoCode());
                        detail.setStoName(vo.getStoName());
                        detail.setStoLevel(vo.getLevelCode());
                        detail.setValidQty(bill.getValidQty());
                        detail.setPrice(relationInfo.getPrice());
                        List<String> stoCodeList = new ArrayList<>();
                        stoCodeList.add(vo.getStoCode());
                        GaiaKeyCommodityTaskBillRelation salesInfo = gaiaKeyCommodityTaskBillMapper.getSalesInfo(inData.getClient(), stoCodeList, inData.getProSelfCode(), bill.getStartTime(), bill.getEndTime());
                        detail.setTqSalesAmt(salesInfo.getTqSalesAmt());
                        detail.setTqSalesQty(salesInfo.getTqSalesQty());
                        detail.setSqSalesAmt(salesInfo.getSqSalesAmt());
                        detail.setSqSalesQty(salesInfo.getSqSalesQty());
                        detail.setPlanSalesAmt(levelInfo.getAverageSalesAmt());
                        detail.setPlanSalesQty(levelInfo.getAverageSalesQty());
                        detail.setIsDelete(0);
                        detail.setCreateTime(now);
                        detail.setCreateUser(userInfo.getUserId());
                        detail.setUpdateTime(now);
                        detail.setUpdateUser(userInfo.getUserId());
                        detailList.add(detail);
                    }

                }
            }

        });
        gaiaKeyCommodityTaskBillDetailMapper.insertBatch(detailList);

        //更新 relation 计划量
        updateRelationPlanQty(userInfo, inData, now);
        //更新 主表的计划量
        inData.setProSelfCode(null);
        updateBillPlanQty(userInfo, inData, now);
        return true;
    }

    private void updateRelationPlanQty(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillRelationInData inData, Date now) {
        GaiaKeyCommodityTaskBill planInfo = gaiaKeyCommodityTaskBillRelationMapper.getPlanInfo(inData);
        GaiaKeyCommodityTaskBillRelationInData relationInData = new GaiaKeyCommodityTaskBillRelationInData();
        relationInData.setBillCode(inData.getBillCode());
        relationInData.setClient(inData.getClient());
        relationInData.setProSelfCode(inData.getProSelfCode());
        GaiaKeyCommodityTaskBillRelation relation = gaiaKeyCommodityTaskBillRelationMapper.getRelationInfoByCode(relationInData);
        if (ObjectUtil.isEmpty(planInfo)) {
            relation.setPlanSalesAmt(BigDecimal.ZERO);
            relation.setPlanSalesQty(BigDecimal.ZERO);
        } else {
            relation.setPlanSalesAmt(new BigDecimal(planInfo.getSalesAmt()));
            relation.setPlanSalesQty(new BigDecimal(planInfo.getSalesQty()));
        }
        relation.setUpdateTime(now);
        relation.setUpdateUser(userInfo.getUserId());
        gaiaKeyCommodityTaskBillRelationMapper.update(relation);
    }

    @Override
    public Object updateStoreInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillDetail inData) {
        GaiaKeyCommodityTaskBillDetail detail = gaiaKeyCommodityTaskBillDetailMapper.queryById(inData.getId());
        if (ObjectUtil.isEmpty(detail)) {
            throw new BusinessException("为查询到该记录，请刷新列表！");
        }
        Date now = new Date();
        inData.setUpdateUser(userInfo.getUserId());
        inData.setUpdateTime(now);
        gaiaKeyCommodityTaskBillDetailMapper.update(inData);
        GaiaKeyCommodityTaskBillRelationInData relationInData = new GaiaKeyCommodityTaskBillRelationInData();
        relationInData.setClient(inData.getClient());
        relationInData.setProSelfCode(detail.getProSelfCode());
        relationInData.setBillCode(detail.getBillCode());
        updateRelationPlanQty(userInfo, relationInData, now);
        relationInData.setProSelfCode(null);
        updateBillPlanQty(userInfo, relationInData, now);
        return true;
    }

    @Override
    public Object deleteBatchStoreInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData) {
        if (CollectionUtils.isEmpty(inData.getIdList())) {
            throw new BusinessException("请选择需要删除的记录！");
        }
        Date now = new Date();
        for (Long id : inData.getIdList()) {
            GaiaKeyCommodityTaskBillDetail detail = new GaiaKeyCommodityTaskBillDetail();
            detail.setId(id);
            detail.setUpdateTime(now);
            detail.setUpdateUser(userInfo.getUserId());
            detail.setIsDelete(1);
            gaiaKeyCommodityTaskBillDetailMapper.update(detail);
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object deleteBatchGoodsInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData) {
        if (CollectionUtils.isEmpty(inData.getIdList())) {
            throw new BusinessException("请选择需要删除的记录！");
        }
        Date now = new Date();
        for (Long id : inData.getIdList()) {
            GaiaKeyCommodityTaskBillRelation relation = gaiaKeyCommodityTaskBillRelationMapper.queryById(id);
            relation.setId(id);
            relation.setUpdateTime(now);
            relation.setUpdateUser(userInfo.getUserId());
            relation.setIsDelete(1);
            gaiaKeyCommodityTaskBillRelationMapper.update(relation);
            //删除 GaiaKeyCommodityTaskBillDetail
            GaiaKeyCommodityTaskBillInData billInData = new GaiaKeyCommodityTaskBillInData();
            billInData.setClient(relation.getClient());
            billInData.setBillCode(relation.getBillCode());
            billInData.setProSelfCode(relation.getProSelfCode());
            List<GaiaKeyCommodityTaskBillDetail> detailList = gaiaKeyCommodityTaskBillDetailMapper.listInfo(billInData);
            if (!CollectionUtils.isEmpty(detailList)) {
                for (GaiaKeyCommodityTaskBillDetail detail : detailList) {
                    detail.setUpdateUser(userInfo.getUserId());
                    detail.setUpdateTime(now);
                    detail.setIsDelete(1);
                    gaiaKeyCommodityTaskBillDetailMapper.update(detail);
                }
            }
        }
        //更新 主表的计划量
        GaiaKeyCommodityTaskBillRelationInData relationInData = new GaiaKeyCommodityTaskBillRelationInData();
        relationInData.setClient(inData.getClient());
        relationInData.setBillCode(inData.getBillCode());
        updateBillPlanQty(userInfo, relationInData, now);
        return true;
    }

    @Override
    public List<GaiaKeyCommodityTaskBillRelation> listGoodsInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData) {
        return gaiaKeyCommodityTaskBillRelationMapper.listInfo(inData);
    }

    @Override
    public List<GaiaKeyCommodityTaskBillDetail> listStoreInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData) {
        return gaiaKeyCommodityTaskBillDetailMapper.listInfo(inData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchImportGoods(MultipartFile file, GetLoginOutData userInfo, String billCode) {
        //检查文件以及数据完整性
        List<GaiaKeyCommodityTaskBillRelation> insertDataList = checkFileData(file);
        Date now = new Date();
        GaiaKeyCommodityTaskBill bill = new GaiaKeyCommodityTaskBill();
        bill.setClient(userInfo.getClient());
        bill.setBillCode(billCode);
        GaiaKeyCommodityTaskBill billInfo = gaiaKeyCommodityTaskBillMapper.getBillInfoByCode(bill);
        List<String> stoCode = gaiaKeyCommodityTaskBillStoreMapper.getStoCode(userInfo.getClient(), billCode, null);
        for (int i = 0; i < insertDataList.size(); i++) {
            GaiaKeyCommodityTaskBillRelation relation = insertDataList.get(i);
            if (StringUtils.isEmpty(relation.getProSelfCode())) {
                throw new BusinessException("第" + (i + 2) + "行数据，商品编码不能为空！");
            }
            if (ObjectUtil.isEmpty(relation.getPlanSalesAmtStr())) {
                throw new BusinessException("第" + (i + 2) + "行数据，本期计划销售额不能为空！");
            }
            if (ObjectUtil.isEmpty(relation.getPlanSalesQtyStr())) {
                throw new BusinessException("第" + (i + 2) + "行数据，本期计划销售量不能为空！");
            }
            GaiaKeyCommodityTaskBillInData param = new GaiaKeyCommodityTaskBillInData();
            param.setBillCode(billCode);
            param.setClient(userInfo.getClient());
            param.setProSelfCode(relation.getProSelfCode());
            int count = gaiaKeyCommodityTaskBillRelationMapper.getCount(param);
            if (count > 0) {
                throw new BusinessException("第" + (i + 2) + "行数据已存在！，请勿重复添加");
            }
            GaiaKeyCommodityTaskBillRelation insertInfo = gaiaKeyCommodityTaskBillRelationMapper.getProductInfo(param);
            if (ObjectUtil.isEmpty(insertInfo)) {
                throw new BusinessException("第" + (i + 2) + "行数据，系统中未查询到商品编码为:" + param.getProSelfCode() + "的商品！");
            }
            //查询同期上期销售信息
            GaiaKeyCommodityTaskBillRelation salesInfo = gaiaKeyCommodityTaskBillMapper.getSalesInfo(param.getClient(), stoCode
                    , param.getProSelfCode(), billInfo.getStartTime(), billInfo.getEndTime());
            insertInfo.setClient(userInfo.getClient());
            insertInfo.setBillCode(billCode);
            insertInfo.setTqSalesQty(salesInfo.getTqSalesQty());
            insertInfo.setTqSalesAmt(salesInfo.getTqSalesAmt());
            insertInfo.setSqSalesQty(salesInfo.getSqSalesQty());
            insertInfo.setSqSalesAmt(salesInfo.getSqSalesAmt());
            insertInfo.setPlanSalesAmt(new BigDecimal(relation.getPlanSalesAmtStr()));
            insertInfo.setPlanSalesQty(new BigDecimal(relation.getPlanSalesQtyStr()));
            insertInfo.setCreateUser(userInfo.getUserId());
            insertInfo.setCreateTime(now);
            insertInfo.setUpdateUser(userInfo.getUserId());
            insertInfo.setUpdateTime(now);
            insertInfo.setIsDelete(0);
            gaiaKeyCommodityTaskBillRelationMapper.insert(insertInfo);
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object batchImportStores(MultipartFile file, GetLoginOutData userInfo, String billCode, String proSelfCode) {
        //检查文件以及数据完整性
        List<GaiaKeyCommodityTaskBillDetail> insertDataList = checkFileDataForStore(file);
        Date now = new Date();
        GaiaKeyCommodityTaskBill bill = new GaiaKeyCommodityTaskBill();
        bill.setClient(userInfo.getClient());
        bill.setBillCode(billCode);
        GaiaKeyCommodityTaskBill billInfo = gaiaKeyCommodityTaskBillMapper.getBillInfoByCode(bill);
        GaiaKeyCommodityTaskBillRelationInData inData = new GaiaKeyCommodityTaskBillRelationInData();
        inData.setClient(userInfo.getClient());
        inData.setBillCode(billCode);
        List<GaiaKeyCommodityTaskBillDetailVO> storeList = gaiaKeyCommodityTaskBillDetailMapper.listStoreLevelInfo(inData);
        checkImportData(insertDataList, storeList);
        for (int i = 0; i < insertDataList.size(); i++) {
            GaiaKeyCommodityTaskBillDetail detail = insertDataList.get(i);
            if (StringUtils.isEmpty(detail.getStoCode())) {
                throw new BusinessException("第" + (i + 2) + "行数据，商品编码不能为空！");
            }
            if (ObjectUtil.isEmpty(detail.getPlanSalesAmtStr())) {
                throw new BusinessException("第" + (i + 2) + "行数据，本期计划销售额不能为空！");
            }
            if (ObjectUtil.isEmpty(detail.getPlanSalesQtyStr())) {
                throw new BusinessException("第" + (i + 2) + "行数据，本期计划销售量不能为空！");
            }
            GaiaKeyCommodityTaskBillInData param = new GaiaKeyCommodityTaskBillInData();
            param.setBillCode(billCode);
            param.setClient(userInfo.getClient());
            param.setProSelfCode(proSelfCode);
            param.setStoCode(detail.getStoCode());
            int count = gaiaKeyCommodityTaskBillDetailMapper.getCount(param);
            if (count > 0) {
                throw new BusinessException("第" + (i + 2) + "行数据已存在！，请勿重复添加");
            }
            GaiaKeyCommodityTaskBillRelation goodsInfo = gaiaKeyCommodityTaskBillRelationMapper.getProductInfo(param);
            GaiaKeyCommodityTaskBillDetail insertInfo = new GaiaKeyCommodityTaskBillDetail();
            if (ObjectUtil.isEmpty(insertInfo)) {
                throw new BusinessException("第" + (i + 2) + "行数据，系统中未查询到商品编码为:" + param.getProSelfCode() + "的商品！");
            }
            List<String> stoList = new ArrayList<>();
            stoList.add(detail.getStoCode());
            //查询同期上期销售信息
            GaiaKeyCommodityTaskBillRelation salesInfo = gaiaKeyCommodityTaskBillMapper.getSalesInfo(param.getClient(), stoList
                    , param.getProSelfCode(), billInfo.getStartTime(), billInfo.getEndTime());
            insertInfo.setClient(userInfo.getClient());
            insertInfo.setBillCode(billCode);
            insertInfo.setProSelfCode(proSelfCode);
            insertInfo.setStoCode(detail.getStoCode());
            for (GaiaKeyCommodityTaskBillDetailVO vo : storeList) {
                if (vo.getStoCode().equals(detail.getStoCode())) {
                    insertInfo.setStoName(vo.getStoName());
                    insertInfo.setStoLevel(vo.getLevelCode());
                    break;
                }
            }
            insertInfo.setValidQty(billInfo.getValidQty());
            insertInfo.setPrice(ObjectUtil.isEmpty(goodsInfo.getPrice()) ? BigDecimal.ZERO : goodsInfo.getPrice());
            insertInfo.setTqSalesQty(salesInfo.getTqSalesQty());
            insertInfo.setTqSalesAmt(salesInfo.getTqSalesAmt());
            insertInfo.setSqSalesQty(salesInfo.getSqSalesQty());
            insertInfo.setSqSalesAmt(salesInfo.getSqSalesAmt());
            insertInfo.setPlanSalesAmt(new BigDecimal(detail.getPlanSalesAmtStr()));
            insertInfo.setPlanSalesQty(new BigDecimal(detail.getPlanSalesQtyStr()));
            insertInfo.setCreateUser(userInfo.getUserId());
            insertInfo.setCreateTime(now);
            insertInfo.setUpdateUser(userInfo.getUserId());
            insertInfo.setUpdateTime(now);
            insertInfo.setIsDelete(0);
            gaiaKeyCommodityTaskBillDetailMapper.insert(insertInfo);
        }
        GaiaKeyCommodityTaskBillRelationInData param = new GaiaKeyCommodityTaskBillRelationInData();
        param.setBillCode(billCode);
        param.setClient(userInfo.getClient());
        updateBillPlanQty(userInfo, param, now);
        param.setProSelfCode(proSelfCode);
        updateRelationPlanQty(userInfo, param, now);
        return true;
    }

    @Override
    public Object assessStoreLevel(GetLoginOutData userInfo) {
        List<HashMap<String, Object>> stores = gaiaKeyCommodityTaskBillMapper.listStores();
        for (HashMap<String, Object> store : stores) {
            GaiaKeyCommodityTaskBillDetailVO salesInfo = gaiaKeyCommodityTaskBillMapper.getThreeMonthSalesInfo(store.get("CLIENT").toString(), store.get("stoCode").toString());
            if (ObjectUtil.isEmpty(salesInfo)) {
                GaiaKeyCommodityTaskBillDetailVO detailVO = new GaiaKeyCommodityTaskBillDetailVO();
                detailVO.setClient(store.get("CLIENT").toString());
                detailVO.setStoCode(store.get("stoCode").toString());
                detailVO.setLevelCode("5");
                gaiaKeyCommodityTaskBillMapper.updateStoreLevel(detailVO);
            } else {
                //x<0.5W E
                if (salesInfo.getAvaSalesAmt().compareTo(new BigDecimal("5000")) < 0) {
                    salesInfo.setLevelCode("5");
                } // 0.5w<=x<1W D
                else if (salesInfo.getAvaSalesAmt().compareTo(new BigDecimal("5000")) >= 0 && salesInfo.getAvaSalesAmt().compareTo(new BigDecimal("10000")) < 0) {
                    salesInfo.setLevelCode("4");
                }// 1w<=x<2w C
                else if (salesInfo.getAvaSalesAmt().compareTo(new BigDecimal("10000")) >= 0 && salesInfo.getAvaSalesAmt().compareTo(new BigDecimal("20000")) < 0) {
                    salesInfo.setLevelCode("3");
                }// 2w<=x<5w B
                else if (salesInfo.getAvaSalesAmt().compareTo(new BigDecimal("20000")) >= 0 && salesInfo.getAvaSalesAmt().compareTo(new BigDecimal("50000")) < 0) {
                    salesInfo.setLevelCode("2");
                }// 5w<=x A
                else if (salesInfo.getAvaSalesAmt().compareTo(new BigDecimal("50000")) >= 0) {
                    salesInfo.setLevelCode("1");
                }
                gaiaKeyCommodityTaskBillMapper.updateStoreLevel(salesInfo);
            }
        }
        return true;
    }

    @Override
    public Object getImportTaskHBTime(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData inData) {
        return gaiaKeyCommodityTaskBillMapper.getImportTaskHBTime(inData);
    }

    private void checkImportData(List<GaiaKeyCommodityTaskBillDetail> insertDataList, List<GaiaKeyCommodityTaskBillDetailVO> storeList) {
        //原始单据门店集合
        List<String> oriStoCodeList = storeList.stream().map(GaiaKeyCommodityTaskBillDetailVO::getStoCode).collect(Collectors.toList());
        //导入门店集合
        List<String> importStoCodeList = insertDataList.stream().map(GaiaKeyCommodityTaskBillDetail::getStoCode).collect(Collectors.toList());
        if (oriStoCodeList.size() != importStoCodeList.size() || !oriStoCodeList.containsAll(importStoCodeList)) {
            throw new BusinessException("导入数据，与任务单关联数量不符，请检查导入数据！");
        }
    }

    /**
     * 检查文件以及数据完整性
     *
     * @param file
     * @author SunJiaNan
     * @Date 2021-07-08
     */
    private List<GaiaKeyCommodityTaskBillRelation> checkFileData(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (ValidateUtil.isEmpty(fileName)) {
            throw new BusinessException("提示：上传文件名不能为空");
        }
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        if (!".xlsx".equals(suffixName)) {
            throw new BusinessException("提示：请检查文件格式是否为xlsx格式");
        }

        //创建sheet  第一页，第一行
        Sheet sheet = new Sheet(1, 0, GaiaKeyCommodityTaskBillRelation.class);
        //获取到 GaiaChannelProduct实体集合
        List<Object> excelDataList = null;
        try {
            excelDataList = EasyExcelFactory.read(file.getInputStream(), sheet);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("上传商品目录,读取excel失败: ", e);
            throw new BusinessException("提示：读取Excel失败");
        }
        if (ValidateUtil.isEmpty(excelDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //转换list泛型
        List<GaiaKeyCommodityTaskBillRelation> relationList = new ArrayList<>(excelDataList.size());
        try {
            relationList = JSON.parseArray(JSON.toJSONString(excelDataList), GaiaKeyCommodityTaskBillRelation.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据类型转换异常：", e);
            throw new BusinessException("提示：数据格式不正确, 请检查数据");
        }
        if (ValidateUtil.isEmpty(relationList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //校验表头
        GaiaKeyCommodityTaskBillRelation dataHead = relationList.get(0);
        String[] channelManagerExcelHead = CommonConstant.GAIA_KEY_GOODS_IMPORT;
        if (!channelManagerExcelHead[0].equals(dataHead.getProSelfCode()) && !channelManagerExcelHead[1].equals(dataHead.getPlanSalesAmtStr())
                && !channelManagerExcelHead[2].equals(dataHead.getPlanSalesQtyStr())) {
            throw new BusinessException("提示：请填写正确的表头");
        }
        //截取除表头以外数据
        List<GaiaKeyCommodityTaskBillRelation> insertData = relationList.subList(1, relationList.size());
        if (ValidateUtil.isEmpty(insertData)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        return insertData;
    }

    private List<GaiaKeyCommodityTaskBillDetail> checkFileDataForStore(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (ValidateUtil.isEmpty(fileName)) {
            throw new BusinessException("提示：上传文件名不能为空");
        }
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        if (!".xlsx".equals(suffixName)) {
            throw new BusinessException("提示：请检查文件格式是否为xlsx格式");
        }

        //创建sheet  第一页，第一行
        Sheet sheet = new Sheet(1, 0, GaiaKeyCommodityTaskBillDetail.class);
        //获取到 GaiaChannelProduct实体集合
        List<Object> excelDataList = null;
        try {
            excelDataList = EasyExcelFactory.read(file.getInputStream(), sheet);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("上传商品目录,读取excel失败: ", e);
            throw new BusinessException("提示：读取Excel失败");
        }
        if (ValidateUtil.isEmpty(excelDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //转换list泛型
        List<GaiaKeyCommodityTaskBillDetail> relationList = new ArrayList<>(excelDataList.size());
        try {
            relationList = JSON.parseArray(JSON.toJSONString(excelDataList), GaiaKeyCommodityTaskBillDetail.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据类型转换异常：", e);
            throw new BusinessException("提示：数据格式不正确, 请检查数据");
        }
        if (ValidateUtil.isEmpty(relationList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //校验表头
        GaiaKeyCommodityTaskBillDetail dataHead = relationList.get(0);
        String[] channelManagerExcelHead = CommonConstant.GAIA_KEY_STORES_IMPORT;
        if (!channelManagerExcelHead[0].equals(dataHead.getStoCode()) && !channelManagerExcelHead[1].equals(dataHead.getPlanSalesAmtStr())
                && !channelManagerExcelHead[2].equals(dataHead.getPlanSalesQtyStr())) {
            throw new BusinessException("提示：请填写正确的表头");
        }
        //截取除表头以外数据
        List<GaiaKeyCommodityTaskBillDetail> insertData = relationList.subList(1, relationList.size());
        if (ValidateUtil.isEmpty(insertData)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        return insertData;
    }

    private void checkHas(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillRelationInData inData) {
        GaiaKeyCommodityTaskBillInData param = new GaiaKeyCommodityTaskBillInData();
        param.setId(inData.getId());
        param.setClient(userInfo.getClient());
        param.setBillCode(inData.getBillCode());
        param.setProSelfCode(inData.getProSelfCode());
        int count = gaiaKeyCommodityTaskBillRelationMapper.getCount(param);
        if (count > 0) {
            throw new BusinessException("该商品已存在，请勿重复添加");
        }
    }

    private void insertStore(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData param, Date now, GaiaKeyCommodityTaskBill bill, List<String> insertStoCodeList) {
        List<GaiaKeyCommodityTaskBillStore> stores = new ArrayList<>();
        for (String stoCode : insertStoCodeList) {
            int count = gaiaKeyCommodityTaskBillStoreMapper.getCount(param.getClient(), param.getBillCode(), stoCode);
            if (count > 0) {
                throw new BusinessException("门店编码：" + stoCode + "已存在请勿重复添加！");
            }
            Integer level = gaiaKeyCommodityTaskBillStoreMapper.getStoreLevel(param.getClient(), stoCode);
            if (ObjectUtil.isEmpty(level)) {
                throw new BusinessException("请先维护，门店编码：" + stoCode + "的门店销售等级！");
            }
            GaiaKeyCommodityTaskBillStore store = new GaiaKeyCommodityTaskBillStore();
            setStoreInfo(userInfo, param, now, bill, stoCode, store,level);
            stores.add(store);
        }
        //新增 GAIA_KEY_COMMODITY_TASK_BILL_STORE
        gaiaKeyCommodityTaskBillStoreMapper.insertBatch(stores);
    }

    private void setStoreInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData param, Date now, GaiaKeyCommodityTaskBill bill, String stoCode, GaiaKeyCommodityTaskBillStore store, int level) {
        store.setClient(param.getClient());
        store.setBillCode(bill.getBillCode());
        store.setStoCode(stoCode);
        store.setStoLevel(level);
        store.setIsDelete(0);
        store.setCreateTime(now);
        store.setCreateUser(userInfo.getUserId());
        store.setUpdateTime(now);
        store.setUpdateUser(userInfo.getUserId());
    }

    private GaiaKeyCommodityTaskBill insertBillInfo(GetLoginOutData userInfo, GaiaKeyCommodityTaskBillInData param, List<String> insertStoCodeList, Date now) {
        GaiaKeyCommodityTaskBill bill = new GaiaKeyCommodityTaskBill();
        String billCode = gaiaKeyCommodityTaskBillMapper.getBillCode(param);
        bill.setClient(param.getClient());
        bill.setBillCode(billCode);
        bill.setBillName(param.getBillName());
        bill.setStartTime(param.getStartTime());
        bill.setEndTime(param.getEndTime());
        bill.setStoreQty(insertStoCodeList.size());
        bill.setValidQty(DateUtil.getBetweenDay(param.getStartTime(), param.getEndTime()) + 1);
        bill.setStatus(0);
        bill.setIsDelete(0);
        bill.setCreateTime(now);
        bill.setCreateUser(userInfo.getUserId());
        bill.setUpdateTime(now);
        bill.setUpdateUser(userInfo.getUserId());
        gaiaKeyCommodityTaskBillMapper.insert(bill);
        return bill;
    }
}
