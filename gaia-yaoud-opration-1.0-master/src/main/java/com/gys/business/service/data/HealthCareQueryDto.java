package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class HealthCareQueryDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    private Integer index;

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 商品描述
     */
    private String proDescribe;

    /**
     * 规格
     */
    private String specification;

    /**
     * 厂家
     */
    private String manufacturers;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 数量
     */
    private String proNum;

    /**
     * 价格
     */
    private String proPrice;

    /**
     * 销售单号
     */
    private String billNo;

    /**
     * 销售日期
     */
    private String saleDate;
}
