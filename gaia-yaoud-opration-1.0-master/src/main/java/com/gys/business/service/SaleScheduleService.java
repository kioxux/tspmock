//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetSaleScheduleInData;
import com.gys.business.service.data.GetSaleScheduleOutData;
import com.gys.business.service.data.GetSaleScheduleQueryInData;
import com.gys.business.service.data.GetUserOutData;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface SaleScheduleService {
    PageInfo<GetSaleScheduleOutData> selectByPage(GetSaleScheduleQueryInData inData);

    void update(List<GetSaleScheduleInData> inData);

    void delete(List<GetSaleScheduleInData> inData);

    void addSave(GetSaleScheduleInData inData);

    List<GetUserOutData> queryUser(GetSaleScheduleInData inData);
}
