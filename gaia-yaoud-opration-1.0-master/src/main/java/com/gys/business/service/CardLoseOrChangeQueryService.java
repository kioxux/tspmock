//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetCardLoseOrChangeInData;
import com.gys.business.service.data.GetCardLoseOrChangeOutData;
import java.util.List;

public interface CardLoseOrChangeQueryService {
    List<GetCardLoseOrChangeOutData> queryTableList(GetCardLoseOrChangeInData inData);
}
