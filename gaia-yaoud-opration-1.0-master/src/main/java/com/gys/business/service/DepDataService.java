package com.gys.business.service;


import com.gys.business.service.data.DcOutData;

import java.util.List;

public interface DepDataService {

    List<DcOutData> selectDepList(String client);

    List<DcOutData> selectDcAndStore(String client);

}
