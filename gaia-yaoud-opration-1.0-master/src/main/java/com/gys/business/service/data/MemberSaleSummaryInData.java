package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 会员销售汇总返回实体
 *
 * @author xiaoyuan on 2020/8/5
 */
@Data
public class MemberSaleSummaryInData implements Serializable {
    private static final long serialVersionUID = 4384172043637834678L;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "店号")
    private String brId;

    @NotBlank(message = "开始时间不能为空")
    @ApiModelProperty(value = "开始时间")
    private String beginDate;

    @NotBlank(message = "结束时间不能为空")
    @ApiModelProperty(value = "结束时间")
    private String endDate;

    /**
     * 是否开启查询白名单标志   0：不开启  1：开启
     */
    private String flag;
}
