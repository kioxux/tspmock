package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class GaiaCommodityInventoryHInData implements Serializable {
    private static final long serialVersionUID = 199492198981043655L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 商品调库单号
     */
    private String billCode;
    private String startTime;
    private String endTime;
    private String status;
    private String proSelfCode;
    private String stoCode;


}

