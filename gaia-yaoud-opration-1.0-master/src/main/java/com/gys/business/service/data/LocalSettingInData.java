package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
public class LocalSettingInData implements Serializable {
    private static final long serialVersionUID = -8274059234243274401L;
    private String clientId;
    private String gsstBrId;
    private String gsstBrName;
    private String gsstSaleMaxQty;
    private String gsstLastDay;
    private String gsstPdStartDate;
    private String gsstVersion;
    private String gsstMinDiscountRate;
    private BigDecimal gsstDyqMaxAmt;
    private String gsstDailyOpenDate;
    private String gsstDailyChangeDate;
    private String gsstDailyCloseDate;
    private String gsstJfQldate;
    private String gsstPsDate;
}
