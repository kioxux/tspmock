//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class GetSyncSdSaleHOutData {
    private String clientId;
    private String gsshBillNo;
    private String gsshBrId;
    private String gsshDate;
    private String gsshTime;
    private String gsshEmp;
    private String gsshTaxNo;
    private String gsshHykNo;
    private BigDecimal gsshZkAmt;
    private BigDecimal gsshYsAmt;
    private BigDecimal gsshRmbZlAmt;
    private BigDecimal gsshRmbAmt;
    private String gsshDyqNo;
    private BigDecimal gsshDyqAmt;
    private String gsshDyqType;
    private String gsshRechargeCardNo;
    private BigDecimal gsshRechargeCardAmt;
    private String gsshDzqczActno1;
    private BigDecimal gsshDzqczAmt1;
    private String gsshDzqczActno2;
    private BigDecimal gsshDzqczAmt2;
    private String gsshDzqczActno3;
    private BigDecimal gsshDzqczAmt3;
    private String gsshDzqczActno4;
    private BigDecimal gsshDzqczAmt4;
    private String gsshDzqczActno5;
    private BigDecimal gsshDzqczAmt5;
    private String gsshDzqdyActno1;
    private BigDecimal gsshDzqdyAmt1;
    private String gsshDzqdyActno2;
    private BigDecimal gsshDzqdyAmt2;
    private String gsshDzqdyActno3;
    private BigDecimal gsshDzqdyAmt3;
    private String gsshDzqdyActno4;
    private BigDecimal gsshDzqdyAmt4;
    private String gsshDzqdyActno5;
    private BigDecimal gsshDzqdyAmt5;
    private String gsshIntegralAdd;
    private String gsshIntegralExchange;
    private BigDecimal gsshIntegralExchangeAmt;
    private String gsshIntegralCash;
    private BigDecimal gsshIntegralCashAmt;
    private String gsshPaymentNo1;
    private BigDecimal gsshPaymentAmt1;
    private String gsshPaymentNo2;
    private BigDecimal gsshPaymentAmt2;
    private String gsshPaymentNo3;
    private BigDecimal gsshPaymentAmt3;
    private String gsshPaymentNo4;
    private BigDecimal gsshPaymentAmt4;
    private String gsshPaymentNo5;
    private BigDecimal gsshPaymentAmt5;
    private String gsshBillNoReturn;
    private String gsshEmpReturn;
    private String gsshPromotionType1;
    private String gsshPromotionType2;
    private String gsshPromotionType3;
    private String gsshPromotionType4;
    private String gsshPromotionType5;
    private String gsshRegisterVoucherId;
    private String gsshReplaceBrId;
    private String gsshReplaceSalerId;
    private String gsshHideFlag;
    private String gsshCallQty;
    private String gsshCallAllow;
    private String gsshEmpGroupName;
    private String gsshUpload;
    private String gsshHykPhone;

    public GetSyncSdSaleHOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsshBillNo() {
        return this.gsshBillNo;
    }

    public String getGsshBrId() {
        return this.gsshBrId;
    }

    public String getGsshDate() {
        return this.gsshDate;
    }

    public String getGsshTime() {
        return this.gsshTime;
    }

    public String getGsshEmp() {
        return this.gsshEmp;
    }

    public String getGsshTaxNo() {
        return this.gsshTaxNo;
    }

    public String getGsshHykNo() {
        return this.gsshHykNo;
    }

    public BigDecimal getGsshZkAmt() {
        return this.gsshZkAmt;
    }

    public BigDecimal getGsshYsAmt() {
        return this.gsshYsAmt;
    }

    public BigDecimal getGsshRmbZlAmt() {
        return this.gsshRmbZlAmt;
    }

    public BigDecimal getGsshRmbAmt() {
        return this.gsshRmbAmt;
    }

    public String getGsshDyqNo() {
        return this.gsshDyqNo;
    }

    public BigDecimal getGsshDyqAmt() {
        return this.gsshDyqAmt;
    }

    public String getGsshDyqType() {
        return this.gsshDyqType;
    }

    public String getGsshRechargeCardNo() {
        return this.gsshRechargeCardNo;
    }

    public BigDecimal getGsshRechargeCardAmt() {
        return this.gsshRechargeCardAmt;
    }

    public String getGsshDzqczActno1() {
        return this.gsshDzqczActno1;
    }

    public BigDecimal getGsshDzqczAmt1() {
        return this.gsshDzqczAmt1;
    }

    public String getGsshDzqczActno2() {
        return this.gsshDzqczActno2;
    }

    public BigDecimal getGsshDzqczAmt2() {
        return this.gsshDzqczAmt2;
    }

    public String getGsshDzqczActno3() {
        return this.gsshDzqczActno3;
    }

    public BigDecimal getGsshDzqczAmt3() {
        return this.gsshDzqczAmt3;
    }

    public String getGsshDzqczActno4() {
        return this.gsshDzqczActno4;
    }

    public BigDecimal getGsshDzqczAmt4() {
        return this.gsshDzqczAmt4;
    }

    public String getGsshDzqczActno5() {
        return this.gsshDzqczActno5;
    }

    public BigDecimal getGsshDzqczAmt5() {
        return this.gsshDzqczAmt5;
    }

    public String getGsshDzqdyActno1() {
        return this.gsshDzqdyActno1;
    }

    public BigDecimal getGsshDzqdyAmt1() {
        return this.gsshDzqdyAmt1;
    }

    public String getGsshDzqdyActno2() {
        return this.gsshDzqdyActno2;
    }

    public BigDecimal getGsshDzqdyAmt2() {
        return this.gsshDzqdyAmt2;
    }

    public String getGsshDzqdyActno3() {
        return this.gsshDzqdyActno3;
    }

    public BigDecimal getGsshDzqdyAmt3() {
        return this.gsshDzqdyAmt3;
    }

    public String getGsshDzqdyActno4() {
        return this.gsshDzqdyActno4;
    }

    public BigDecimal getGsshDzqdyAmt4() {
        return this.gsshDzqdyAmt4;
    }

    public String getGsshDzqdyActno5() {
        return this.gsshDzqdyActno5;
    }

    public BigDecimal getGsshDzqdyAmt5() {
        return this.gsshDzqdyAmt5;
    }

    public String getGsshIntegralAdd() {
        return this.gsshIntegralAdd;
    }

    public String getGsshIntegralExchange() {
        return this.gsshIntegralExchange;
    }

    public BigDecimal getGsshIntegralExchangeAmt() {
        return this.gsshIntegralExchangeAmt;
    }

    public String getGsshIntegralCash() {
        return this.gsshIntegralCash;
    }

    public BigDecimal getGsshIntegralCashAmt() {
        return this.gsshIntegralCashAmt;
    }

    public String getGsshPaymentNo1() {
        return this.gsshPaymentNo1;
    }

    public BigDecimal getGsshPaymentAmt1() {
        return this.gsshPaymentAmt1;
    }

    public String getGsshPaymentNo2() {
        return this.gsshPaymentNo2;
    }

    public BigDecimal getGsshPaymentAmt2() {
        return this.gsshPaymentAmt2;
    }

    public String getGsshPaymentNo3() {
        return this.gsshPaymentNo3;
    }

    public BigDecimal getGsshPaymentAmt3() {
        return this.gsshPaymentAmt3;
    }

    public String getGsshPaymentNo4() {
        return this.gsshPaymentNo4;
    }

    public BigDecimal getGsshPaymentAmt4() {
        return this.gsshPaymentAmt4;
    }

    public String getGsshPaymentNo5() {
        return this.gsshPaymentNo5;
    }

    public BigDecimal getGsshPaymentAmt5() {
        return this.gsshPaymentAmt5;
    }

    public String getGsshBillNoReturn() {
        return this.gsshBillNoReturn;
    }

    public String getGsshEmpReturn() {
        return this.gsshEmpReturn;
    }

    public String getGsshPromotionType1() {
        return this.gsshPromotionType1;
    }

    public String getGsshPromotionType2() {
        return this.gsshPromotionType2;
    }

    public String getGsshPromotionType3() {
        return this.gsshPromotionType3;
    }

    public String getGsshPromotionType4() {
        return this.gsshPromotionType4;
    }

    public String getGsshPromotionType5() {
        return this.gsshPromotionType5;
    }

    public String getGsshRegisterVoucherId() {
        return this.gsshRegisterVoucherId;
    }

    public String getGsshReplaceBrId() {
        return this.gsshReplaceBrId;
    }

    public String getGsshReplaceSalerId() {
        return this.gsshReplaceSalerId;
    }

    public String getGsshHideFlag() {
        return this.gsshHideFlag;
    }

    public String getGsshCallQty() {
        return this.gsshCallQty;
    }

    public String getGsshCallAllow() {
        return this.gsshCallAllow;
    }

    public String getGsshEmpGroupName() {
        return this.gsshEmpGroupName;
    }

    public String getGsshUpload() {
        return this.gsshUpload;
    }

    public String getGsshHykPhone() {
        return this.gsshHykPhone;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsshBillNo(final String gsshBillNo) {
        this.gsshBillNo = gsshBillNo;
    }

    public void setGsshBrId(final String gsshBrId) {
        this.gsshBrId = gsshBrId;
    }

    public void setGsshDate(final String gsshDate) {
        this.gsshDate = gsshDate;
    }

    public void setGsshTime(final String gsshTime) {
        this.gsshTime = gsshTime;
    }

    public void setGsshEmp(final String gsshEmp) {
        this.gsshEmp = gsshEmp;
    }

    public void setGsshTaxNo(final String gsshTaxNo) {
        this.gsshTaxNo = gsshTaxNo;
    }

    public void setGsshHykNo(final String gsshHykNo) {
        this.gsshHykNo = gsshHykNo;
    }

    public void setGsshZkAmt(final BigDecimal gsshZkAmt) {
        this.gsshZkAmt = gsshZkAmt;
    }

    public void setGsshYsAmt(final BigDecimal gsshYsAmt) {
        this.gsshYsAmt = gsshYsAmt;
    }

    public void setGsshRmbZlAmt(final BigDecimal gsshRmbZlAmt) {
        this.gsshRmbZlAmt = gsshRmbZlAmt;
    }

    public void setGsshRmbAmt(final BigDecimal gsshRmbAmt) {
        this.gsshRmbAmt = gsshRmbAmt;
    }

    public void setGsshDyqNo(final String gsshDyqNo) {
        this.gsshDyqNo = gsshDyqNo;
    }

    public void setGsshDyqAmt(final BigDecimal gsshDyqAmt) {
        this.gsshDyqAmt = gsshDyqAmt;
    }

    public void setGsshDyqType(final String gsshDyqType) {
        this.gsshDyqType = gsshDyqType;
    }

    public void setGsshRechargeCardNo(final String gsshRechargeCardNo) {
        this.gsshRechargeCardNo = gsshRechargeCardNo;
    }

    public void setGsshRechargeCardAmt(final BigDecimal gsshRechargeCardAmt) {
        this.gsshRechargeCardAmt = gsshRechargeCardAmt;
    }

    public void setGsshDzqczActno1(final String gsshDzqczActno1) {
        this.gsshDzqczActno1 = gsshDzqczActno1;
    }

    public void setGsshDzqczAmt1(final BigDecimal gsshDzqczAmt1) {
        this.gsshDzqczAmt1 = gsshDzqczAmt1;
    }

    public void setGsshDzqczActno2(final String gsshDzqczActno2) {
        this.gsshDzqczActno2 = gsshDzqczActno2;
    }

    public void setGsshDzqczAmt2(final BigDecimal gsshDzqczAmt2) {
        this.gsshDzqczAmt2 = gsshDzqczAmt2;
    }

    public void setGsshDzqczActno3(final String gsshDzqczActno3) {
        this.gsshDzqczActno3 = gsshDzqczActno3;
    }

    public void setGsshDzqczAmt3(final BigDecimal gsshDzqczAmt3) {
        this.gsshDzqczAmt3 = gsshDzqczAmt3;
    }

    public void setGsshDzqczActno4(final String gsshDzqczActno4) {
        this.gsshDzqczActno4 = gsshDzqczActno4;
    }

    public void setGsshDzqczAmt4(final BigDecimal gsshDzqczAmt4) {
        this.gsshDzqczAmt4 = gsshDzqczAmt4;
    }

    public void setGsshDzqczActno5(final String gsshDzqczActno5) {
        this.gsshDzqczActno5 = gsshDzqczActno5;
    }

    public void setGsshDzqczAmt5(final BigDecimal gsshDzqczAmt5) {
        this.gsshDzqczAmt5 = gsshDzqczAmt5;
    }

    public void setGsshDzqdyActno1(final String gsshDzqdyActno1) {
        this.gsshDzqdyActno1 = gsshDzqdyActno1;
    }

    public void setGsshDzqdyAmt1(final BigDecimal gsshDzqdyAmt1) {
        this.gsshDzqdyAmt1 = gsshDzqdyAmt1;
    }

    public void setGsshDzqdyActno2(final String gsshDzqdyActno2) {
        this.gsshDzqdyActno2 = gsshDzqdyActno2;
    }

    public void setGsshDzqdyAmt2(final BigDecimal gsshDzqdyAmt2) {
        this.gsshDzqdyAmt2 = gsshDzqdyAmt2;
    }

    public void setGsshDzqdyActno3(final String gsshDzqdyActno3) {
        this.gsshDzqdyActno3 = gsshDzqdyActno3;
    }

    public void setGsshDzqdyAmt3(final BigDecimal gsshDzqdyAmt3) {
        this.gsshDzqdyAmt3 = gsshDzqdyAmt3;
    }

    public void setGsshDzqdyActno4(final String gsshDzqdyActno4) {
        this.gsshDzqdyActno4 = gsshDzqdyActno4;
    }

    public void setGsshDzqdyAmt4(final BigDecimal gsshDzqdyAmt4) {
        this.gsshDzqdyAmt4 = gsshDzqdyAmt4;
    }

    public void setGsshDzqdyActno5(final String gsshDzqdyActno5) {
        this.gsshDzqdyActno5 = gsshDzqdyActno5;
    }

    public void setGsshDzqdyAmt5(final BigDecimal gsshDzqdyAmt5) {
        this.gsshDzqdyAmt5 = gsshDzqdyAmt5;
    }

    public void setGsshIntegralAdd(final String gsshIntegralAdd) {
        this.gsshIntegralAdd = gsshIntegralAdd;
    }

    public void setGsshIntegralExchange(final String gsshIntegralExchange) {
        this.gsshIntegralExchange = gsshIntegralExchange;
    }

    public void setGsshIntegralExchangeAmt(final BigDecimal gsshIntegralExchangeAmt) {
        this.gsshIntegralExchangeAmt = gsshIntegralExchangeAmt;
    }

    public void setGsshIntegralCash(final String gsshIntegralCash) {
        this.gsshIntegralCash = gsshIntegralCash;
    }

    public void setGsshIntegralCashAmt(final BigDecimal gsshIntegralCashAmt) {
        this.gsshIntegralCashAmt = gsshIntegralCashAmt;
    }

    public void setGsshPaymentNo1(final String gsshPaymentNo1) {
        this.gsshPaymentNo1 = gsshPaymentNo1;
    }

    public void setGsshPaymentAmt1(final BigDecimal gsshPaymentAmt1) {
        this.gsshPaymentAmt1 = gsshPaymentAmt1;
    }

    public void setGsshPaymentNo2(final String gsshPaymentNo2) {
        this.gsshPaymentNo2 = gsshPaymentNo2;
    }

    public void setGsshPaymentAmt2(final BigDecimal gsshPaymentAmt2) {
        this.gsshPaymentAmt2 = gsshPaymentAmt2;
    }

    public void setGsshPaymentNo3(final String gsshPaymentNo3) {
        this.gsshPaymentNo3 = gsshPaymentNo3;
    }

    public void setGsshPaymentAmt3(final BigDecimal gsshPaymentAmt3) {
        this.gsshPaymentAmt3 = gsshPaymentAmt3;
    }

    public void setGsshPaymentNo4(final String gsshPaymentNo4) {
        this.gsshPaymentNo4 = gsshPaymentNo4;
    }

    public void setGsshPaymentAmt4(final BigDecimal gsshPaymentAmt4) {
        this.gsshPaymentAmt4 = gsshPaymentAmt4;
    }

    public void setGsshPaymentNo5(final String gsshPaymentNo5) {
        this.gsshPaymentNo5 = gsshPaymentNo5;
    }

    public void setGsshPaymentAmt5(final BigDecimal gsshPaymentAmt5) {
        this.gsshPaymentAmt5 = gsshPaymentAmt5;
    }

    public void setGsshBillNoReturn(final String gsshBillNoReturn) {
        this.gsshBillNoReturn = gsshBillNoReturn;
    }

    public void setGsshEmpReturn(final String gsshEmpReturn) {
        this.gsshEmpReturn = gsshEmpReturn;
    }

    public void setGsshPromotionType1(final String gsshPromotionType1) {
        this.gsshPromotionType1 = gsshPromotionType1;
    }

    public void setGsshPromotionType2(final String gsshPromotionType2) {
        this.gsshPromotionType2 = gsshPromotionType2;
    }

    public void setGsshPromotionType3(final String gsshPromotionType3) {
        this.gsshPromotionType3 = gsshPromotionType3;
    }

    public void setGsshPromotionType4(final String gsshPromotionType4) {
        this.gsshPromotionType4 = gsshPromotionType4;
    }

    public void setGsshPromotionType5(final String gsshPromotionType5) {
        this.gsshPromotionType5 = gsshPromotionType5;
    }

    public void setGsshRegisterVoucherId(final String gsshRegisterVoucherId) {
        this.gsshRegisterVoucherId = gsshRegisterVoucherId;
    }

    public void setGsshReplaceBrId(final String gsshReplaceBrId) {
        this.gsshReplaceBrId = gsshReplaceBrId;
    }

    public void setGsshReplaceSalerId(final String gsshReplaceSalerId) {
        this.gsshReplaceSalerId = gsshReplaceSalerId;
    }

    public void setGsshHideFlag(final String gsshHideFlag) {
        this.gsshHideFlag = gsshHideFlag;
    }

    public void setGsshCallQty(final String gsshCallQty) {
        this.gsshCallQty = gsshCallQty;
    }

    public void setGsshCallAllow(final String gsshCallAllow) {
        this.gsshCallAllow = gsshCallAllow;
    }

    public void setGsshEmpGroupName(final String gsshEmpGroupName) {
        this.gsshEmpGroupName = gsshEmpGroupName;
    }

    public void setGsshUpload(final String gsshUpload) {
        this.gsshUpload = gsshUpload;
    }

    public void setGsshHykPhone(final String gsshHykPhone) {
        this.gsshHykPhone = gsshHykPhone;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSyncSdSaleHOutData)) {
            return false;
        } else {
            GetSyncSdSaleHOutData other = (GetSyncSdSaleHOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label827: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label827;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label827;
                    }

                    return false;
                }

                Object this$gsshBillNo = this.getGsshBillNo();
                Object other$gsshBillNo = other.getGsshBillNo();
                if (this$gsshBillNo == null) {
                    if (other$gsshBillNo != null) {
                        return false;
                    }
                } else if (!this$gsshBillNo.equals(other$gsshBillNo)) {
                    return false;
                }

                Object this$gsshBrId = this.getGsshBrId();
                Object other$gsshBrId = other.getGsshBrId();
                if (this$gsshBrId == null) {
                    if (other$gsshBrId != null) {
                        return false;
                    }
                } else if (!this$gsshBrId.equals(other$gsshBrId)) {
                    return false;
                }

                label806: {
                    Object this$gsshDate = this.getGsshDate();
                    Object other$gsshDate = other.getGsshDate();
                    if (this$gsshDate == null) {
                        if (other$gsshDate == null) {
                            break label806;
                        }
                    } else if (this$gsshDate.equals(other$gsshDate)) {
                        break label806;
                    }

                    return false;
                }

                label799: {
                    Object this$gsshTime = this.getGsshTime();
                    Object other$gsshTime = other.getGsshTime();
                    if (this$gsshTime == null) {
                        if (other$gsshTime == null) {
                            break label799;
                        }
                    } else if (this$gsshTime.equals(other$gsshTime)) {
                        break label799;
                    }

                    return false;
                }

                label792: {
                    Object this$gsshEmp = this.getGsshEmp();
                    Object other$gsshEmp = other.getGsshEmp();
                    if (this$gsshEmp == null) {
                        if (other$gsshEmp == null) {
                            break label792;
                        }
                    } else if (this$gsshEmp.equals(other$gsshEmp)) {
                        break label792;
                    }

                    return false;
                }

                Object this$gsshTaxNo = this.getGsshTaxNo();
                Object other$gsshTaxNo = other.getGsshTaxNo();
                if (this$gsshTaxNo == null) {
                    if (other$gsshTaxNo != null) {
                        return false;
                    }
                } else if (!this$gsshTaxNo.equals(other$gsshTaxNo)) {
                    return false;
                }

                label778: {
                    Object this$gsshHykNo = this.getGsshHykNo();
                    Object other$gsshHykNo = other.getGsshHykNo();
                    if (this$gsshHykNo == null) {
                        if (other$gsshHykNo == null) {
                            break label778;
                        }
                    } else if (this$gsshHykNo.equals(other$gsshHykNo)) {
                        break label778;
                    }

                    return false;
                }

                Object this$gsshZkAmt = this.getGsshZkAmt();
                Object other$gsshZkAmt = other.getGsshZkAmt();
                if (this$gsshZkAmt == null) {
                    if (other$gsshZkAmt != null) {
                        return false;
                    }
                } else if (!this$gsshZkAmt.equals(other$gsshZkAmt)) {
                    return false;
                }

                label764: {
                    Object this$gsshYsAmt = this.getGsshYsAmt();
                    Object other$gsshYsAmt = other.getGsshYsAmt();
                    if (this$gsshYsAmt == null) {
                        if (other$gsshYsAmt == null) {
                            break label764;
                        }
                    } else if (this$gsshYsAmt.equals(other$gsshYsAmt)) {
                        break label764;
                    }

                    return false;
                }

                Object this$gsshRmbZlAmt = this.getGsshRmbZlAmt();
                Object other$gsshRmbZlAmt = other.getGsshRmbZlAmt();
                if (this$gsshRmbZlAmt == null) {
                    if (other$gsshRmbZlAmt != null) {
                        return false;
                    }
                } else if (!this$gsshRmbZlAmt.equals(other$gsshRmbZlAmt)) {
                    return false;
                }

                Object this$gsshRmbAmt = this.getGsshRmbAmt();
                Object other$gsshRmbAmt = other.getGsshRmbAmt();
                if (this$gsshRmbAmt == null) {
                    if (other$gsshRmbAmt != null) {
                        return false;
                    }
                } else if (!this$gsshRmbAmt.equals(other$gsshRmbAmt)) {
                    return false;
                }

                label743: {
                    Object this$gsshDyqNo = this.getGsshDyqNo();
                    Object other$gsshDyqNo = other.getGsshDyqNo();
                    if (this$gsshDyqNo == null) {
                        if (other$gsshDyqNo == null) {
                            break label743;
                        }
                    } else if (this$gsshDyqNo.equals(other$gsshDyqNo)) {
                        break label743;
                    }

                    return false;
                }

                label736: {
                    Object this$gsshDyqAmt = this.getGsshDyqAmt();
                    Object other$gsshDyqAmt = other.getGsshDyqAmt();
                    if (this$gsshDyqAmt == null) {
                        if (other$gsshDyqAmt == null) {
                            break label736;
                        }
                    } else if (this$gsshDyqAmt.equals(other$gsshDyqAmt)) {
                        break label736;
                    }

                    return false;
                }

                Object this$gsshDyqType = this.getGsshDyqType();
                Object other$gsshDyqType = other.getGsshDyqType();
                if (this$gsshDyqType == null) {
                    if (other$gsshDyqType != null) {
                        return false;
                    }
                } else if (!this$gsshDyqType.equals(other$gsshDyqType)) {
                    return false;
                }

                Object this$gsshRechargeCardNo = this.getGsshRechargeCardNo();
                Object other$gsshRechargeCardNo = other.getGsshRechargeCardNo();
                if (this$gsshRechargeCardNo == null) {
                    if (other$gsshRechargeCardNo != null) {
                        return false;
                    }
                } else if (!this$gsshRechargeCardNo.equals(other$gsshRechargeCardNo)) {
                    return false;
                }

                label715: {
                    Object this$gsshRechargeCardAmt = this.getGsshRechargeCardAmt();
                    Object other$gsshRechargeCardAmt = other.getGsshRechargeCardAmt();
                    if (this$gsshRechargeCardAmt == null) {
                        if (other$gsshRechargeCardAmt == null) {
                            break label715;
                        }
                    } else if (this$gsshRechargeCardAmt.equals(other$gsshRechargeCardAmt)) {
                        break label715;
                    }

                    return false;
                }

                Object this$gsshDzqczActno1 = this.getGsshDzqczActno1();
                Object other$gsshDzqczActno1 = other.getGsshDzqczActno1();
                if (this$gsshDzqczActno1 == null) {
                    if (other$gsshDzqczActno1 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqczActno1.equals(other$gsshDzqczActno1)) {
                    return false;
                }

                Object this$gsshDzqczAmt1 = this.getGsshDzqczAmt1();
                Object other$gsshDzqczAmt1 = other.getGsshDzqczAmt1();
                if (this$gsshDzqczAmt1 == null) {
                    if (other$gsshDzqczAmt1 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqczAmt1.equals(other$gsshDzqczAmt1)) {
                    return false;
                }

                label694: {
                    Object this$gsshDzqczActno2 = this.getGsshDzqczActno2();
                    Object other$gsshDzqczActno2 = other.getGsshDzqczActno2();
                    if (this$gsshDzqczActno2 == null) {
                        if (other$gsshDzqczActno2 == null) {
                            break label694;
                        }
                    } else if (this$gsshDzqczActno2.equals(other$gsshDzqczActno2)) {
                        break label694;
                    }

                    return false;
                }

                label687: {
                    Object this$gsshDzqczAmt2 = this.getGsshDzqczAmt2();
                    Object other$gsshDzqczAmt2 = other.getGsshDzqczAmt2();
                    if (this$gsshDzqczAmt2 == null) {
                        if (other$gsshDzqczAmt2 == null) {
                            break label687;
                        }
                    } else if (this$gsshDzqczAmt2.equals(other$gsshDzqczAmt2)) {
                        break label687;
                    }

                    return false;
                }

                label680: {
                    Object this$gsshDzqczActno3 = this.getGsshDzqczActno3();
                    Object other$gsshDzqczActno3 = other.getGsshDzqczActno3();
                    if (this$gsshDzqczActno3 == null) {
                        if (other$gsshDzqczActno3 == null) {
                            break label680;
                        }
                    } else if (this$gsshDzqczActno3.equals(other$gsshDzqczActno3)) {
                        break label680;
                    }

                    return false;
                }

                Object this$gsshDzqczAmt3 = this.getGsshDzqczAmt3();
                Object other$gsshDzqczAmt3 = other.getGsshDzqczAmt3();
                if (this$gsshDzqczAmt3 == null) {
                    if (other$gsshDzqczAmt3 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqczAmt3.equals(other$gsshDzqczAmt3)) {
                    return false;
                }

                label666: {
                    Object this$gsshDzqczActno4 = this.getGsshDzqczActno4();
                    Object other$gsshDzqczActno4 = other.getGsshDzqczActno4();
                    if (this$gsshDzqczActno4 == null) {
                        if (other$gsshDzqczActno4 == null) {
                            break label666;
                        }
                    } else if (this$gsshDzqczActno4.equals(other$gsshDzqczActno4)) {
                        break label666;
                    }

                    return false;
                }

                Object this$gsshDzqczAmt4 = this.getGsshDzqczAmt4();
                Object other$gsshDzqczAmt4 = other.getGsshDzqczAmt4();
                if (this$gsshDzqczAmt4 == null) {
                    if (other$gsshDzqczAmt4 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqczAmt4.equals(other$gsshDzqczAmt4)) {
                    return false;
                }

                label652: {
                    Object this$gsshDzqczActno5 = this.getGsshDzqczActno5();
                    Object other$gsshDzqczActno5 = other.getGsshDzqczActno5();
                    if (this$gsshDzqczActno5 == null) {
                        if (other$gsshDzqczActno5 == null) {
                            break label652;
                        }
                    } else if (this$gsshDzqczActno5.equals(other$gsshDzqczActno5)) {
                        break label652;
                    }

                    return false;
                }

                Object this$gsshDzqczAmt5 = this.getGsshDzqczAmt5();
                Object other$gsshDzqczAmt5 = other.getGsshDzqczAmt5();
                if (this$gsshDzqczAmt5 == null) {
                    if (other$gsshDzqczAmt5 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqczAmt5.equals(other$gsshDzqczAmt5)) {
                    return false;
                }

                Object this$gsshDzqdyActno1 = this.getGsshDzqdyActno1();
                Object other$gsshDzqdyActno1 = other.getGsshDzqdyActno1();
                if (this$gsshDzqdyActno1 == null) {
                    if (other$gsshDzqdyActno1 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqdyActno1.equals(other$gsshDzqdyActno1)) {
                    return false;
                }

                label631: {
                    Object this$gsshDzqdyAmt1 = this.getGsshDzqdyAmt1();
                    Object other$gsshDzqdyAmt1 = other.getGsshDzqdyAmt1();
                    if (this$gsshDzqdyAmt1 == null) {
                        if (other$gsshDzqdyAmt1 == null) {
                            break label631;
                        }
                    } else if (this$gsshDzqdyAmt1.equals(other$gsshDzqdyAmt1)) {
                        break label631;
                    }

                    return false;
                }

                label624: {
                    Object this$gsshDzqdyActno2 = this.getGsshDzqdyActno2();
                    Object other$gsshDzqdyActno2 = other.getGsshDzqdyActno2();
                    if (this$gsshDzqdyActno2 == null) {
                        if (other$gsshDzqdyActno2 == null) {
                            break label624;
                        }
                    } else if (this$gsshDzqdyActno2.equals(other$gsshDzqdyActno2)) {
                        break label624;
                    }

                    return false;
                }

                Object this$gsshDzqdyAmt2 = this.getGsshDzqdyAmt2();
                Object other$gsshDzqdyAmt2 = other.getGsshDzqdyAmt2();
                if (this$gsshDzqdyAmt2 == null) {
                    if (other$gsshDzqdyAmt2 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqdyAmt2.equals(other$gsshDzqdyAmt2)) {
                    return false;
                }

                Object this$gsshDzqdyActno3 = this.getGsshDzqdyActno3();
                Object other$gsshDzqdyActno3 = other.getGsshDzqdyActno3();
                if (this$gsshDzqdyActno3 == null) {
                    if (other$gsshDzqdyActno3 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqdyActno3.equals(other$gsshDzqdyActno3)) {
                    return false;
                }

                label603: {
                    Object this$gsshDzqdyAmt3 = this.getGsshDzqdyAmt3();
                    Object other$gsshDzqdyAmt3 = other.getGsshDzqdyAmt3();
                    if (this$gsshDzqdyAmt3 == null) {
                        if (other$gsshDzqdyAmt3 == null) {
                            break label603;
                        }
                    } else if (this$gsshDzqdyAmt3.equals(other$gsshDzqdyAmt3)) {
                        break label603;
                    }

                    return false;
                }

                Object this$gsshDzqdyActno4 = this.getGsshDzqdyActno4();
                Object other$gsshDzqdyActno4 = other.getGsshDzqdyActno4();
                if (this$gsshDzqdyActno4 == null) {
                    if (other$gsshDzqdyActno4 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqdyActno4.equals(other$gsshDzqdyActno4)) {
                    return false;
                }

                Object this$gsshDzqdyAmt4 = this.getGsshDzqdyAmt4();
                Object other$gsshDzqdyAmt4 = other.getGsshDzqdyAmt4();
                if (this$gsshDzqdyAmt4 == null) {
                    if (other$gsshDzqdyAmt4 != null) {
                        return false;
                    }
                } else if (!this$gsshDzqdyAmt4.equals(other$gsshDzqdyAmt4)) {
                    return false;
                }

                label582: {
                    Object this$gsshDzqdyActno5 = this.getGsshDzqdyActno5();
                    Object other$gsshDzqdyActno5 = other.getGsshDzqdyActno5();
                    if (this$gsshDzqdyActno5 == null) {
                        if (other$gsshDzqdyActno5 == null) {
                            break label582;
                        }
                    } else if (this$gsshDzqdyActno5.equals(other$gsshDzqdyActno5)) {
                        break label582;
                    }

                    return false;
                }

                label575: {
                    Object this$gsshDzqdyAmt5 = this.getGsshDzqdyAmt5();
                    Object other$gsshDzqdyAmt5 = other.getGsshDzqdyAmt5();
                    if (this$gsshDzqdyAmt5 == null) {
                        if (other$gsshDzqdyAmt5 == null) {
                            break label575;
                        }
                    } else if (this$gsshDzqdyAmt5.equals(other$gsshDzqdyAmt5)) {
                        break label575;
                    }

                    return false;
                }

                label568: {
                    Object this$gsshIntegralAdd = this.getGsshIntegralAdd();
                    Object other$gsshIntegralAdd = other.getGsshIntegralAdd();
                    if (this$gsshIntegralAdd == null) {
                        if (other$gsshIntegralAdd == null) {
                            break label568;
                        }
                    } else if (this$gsshIntegralAdd.equals(other$gsshIntegralAdd)) {
                        break label568;
                    }

                    return false;
                }

                Object this$gsshIntegralExchange = this.getGsshIntegralExchange();
                Object other$gsshIntegralExchange = other.getGsshIntegralExchange();
                if (this$gsshIntegralExchange == null) {
                    if (other$gsshIntegralExchange != null) {
                        return false;
                    }
                } else if (!this$gsshIntegralExchange.equals(other$gsshIntegralExchange)) {
                    return false;
                }

                label554: {
                    Object this$gsshIntegralExchangeAmt = this.getGsshIntegralExchangeAmt();
                    Object other$gsshIntegralExchangeAmt = other.getGsshIntegralExchangeAmt();
                    if (this$gsshIntegralExchangeAmt == null) {
                        if (other$gsshIntegralExchangeAmt == null) {
                            break label554;
                        }
                    } else if (this$gsshIntegralExchangeAmt.equals(other$gsshIntegralExchangeAmt)) {
                        break label554;
                    }

                    return false;
                }

                Object this$gsshIntegralCash = this.getGsshIntegralCash();
                Object other$gsshIntegralCash = other.getGsshIntegralCash();
                if (this$gsshIntegralCash == null) {
                    if (other$gsshIntegralCash != null) {
                        return false;
                    }
                } else if (!this$gsshIntegralCash.equals(other$gsshIntegralCash)) {
                    return false;
                }

                label540: {
                    Object this$gsshIntegralCashAmt = this.getGsshIntegralCashAmt();
                    Object other$gsshIntegralCashAmt = other.getGsshIntegralCashAmt();
                    if (this$gsshIntegralCashAmt == null) {
                        if (other$gsshIntegralCashAmt == null) {
                            break label540;
                        }
                    } else if (this$gsshIntegralCashAmt.equals(other$gsshIntegralCashAmt)) {
                        break label540;
                    }

                    return false;
                }

                Object this$gsshPaymentNo1 = this.getGsshPaymentNo1();
                Object other$gsshPaymentNo1 = other.getGsshPaymentNo1();
                if (this$gsshPaymentNo1 == null) {
                    if (other$gsshPaymentNo1 != null) {
                        return false;
                    }
                } else if (!this$gsshPaymentNo1.equals(other$gsshPaymentNo1)) {
                    return false;
                }

                Object this$gsshPaymentAmt1 = this.getGsshPaymentAmt1();
                Object other$gsshPaymentAmt1 = other.getGsshPaymentAmt1();
                if (this$gsshPaymentAmt1 == null) {
                    if (other$gsshPaymentAmt1 != null) {
                        return false;
                    }
                } else if (!this$gsshPaymentAmt1.equals(other$gsshPaymentAmt1)) {
                    return false;
                }

                label519: {
                    Object this$gsshPaymentNo2 = this.getGsshPaymentNo2();
                    Object other$gsshPaymentNo2 = other.getGsshPaymentNo2();
                    if (this$gsshPaymentNo2 == null) {
                        if (other$gsshPaymentNo2 == null) {
                            break label519;
                        }
                    } else if (this$gsshPaymentNo2.equals(other$gsshPaymentNo2)) {
                        break label519;
                    }

                    return false;
                }

                label512: {
                    Object this$gsshPaymentAmt2 = this.getGsshPaymentAmt2();
                    Object other$gsshPaymentAmt2 = other.getGsshPaymentAmt2();
                    if (this$gsshPaymentAmt2 == null) {
                        if (other$gsshPaymentAmt2 == null) {
                            break label512;
                        }
                    } else if (this$gsshPaymentAmt2.equals(other$gsshPaymentAmt2)) {
                        break label512;
                    }

                    return false;
                }

                Object this$gsshPaymentNo3 = this.getGsshPaymentNo3();
                Object other$gsshPaymentNo3 = other.getGsshPaymentNo3();
                if (this$gsshPaymentNo3 == null) {
                    if (other$gsshPaymentNo3 != null) {
                        return false;
                    }
                } else if (!this$gsshPaymentNo3.equals(other$gsshPaymentNo3)) {
                    return false;
                }

                Object this$gsshPaymentAmt3 = this.getGsshPaymentAmt3();
                Object other$gsshPaymentAmt3 = other.getGsshPaymentAmt3();
                if (this$gsshPaymentAmt3 == null) {
                    if (other$gsshPaymentAmt3 != null) {
                        return false;
                    }
                } else if (!this$gsshPaymentAmt3.equals(other$gsshPaymentAmt3)) {
                    return false;
                }

                label491: {
                    Object this$gsshPaymentNo4 = this.getGsshPaymentNo4();
                    Object other$gsshPaymentNo4 = other.getGsshPaymentNo4();
                    if (this$gsshPaymentNo4 == null) {
                        if (other$gsshPaymentNo4 == null) {
                            break label491;
                        }
                    } else if (this$gsshPaymentNo4.equals(other$gsshPaymentNo4)) {
                        break label491;
                    }

                    return false;
                }

                Object this$gsshPaymentAmt4 = this.getGsshPaymentAmt4();
                Object other$gsshPaymentAmt4 = other.getGsshPaymentAmt4();
                if (this$gsshPaymentAmt4 == null) {
                    if (other$gsshPaymentAmt4 != null) {
                        return false;
                    }
                } else if (!this$gsshPaymentAmt4.equals(other$gsshPaymentAmt4)) {
                    return false;
                }

                Object this$gsshPaymentNo5 = this.getGsshPaymentNo5();
                Object other$gsshPaymentNo5 = other.getGsshPaymentNo5();
                if (this$gsshPaymentNo5 == null) {
                    if (other$gsshPaymentNo5 != null) {
                        return false;
                    }
                } else if (!this$gsshPaymentNo5.equals(other$gsshPaymentNo5)) {
                    return false;
                }

                label470: {
                    Object this$gsshPaymentAmt5 = this.getGsshPaymentAmt5();
                    Object other$gsshPaymentAmt5 = other.getGsshPaymentAmt5();
                    if (this$gsshPaymentAmt5 == null) {
                        if (other$gsshPaymentAmt5 == null) {
                            break label470;
                        }
                    } else if (this$gsshPaymentAmt5.equals(other$gsshPaymentAmt5)) {
                        break label470;
                    }

                    return false;
                }

                label463: {
                    Object this$gsshBillNoReturn = this.getGsshBillNoReturn();
                    Object other$gsshBillNoReturn = other.getGsshBillNoReturn();
                    if (this$gsshBillNoReturn == null) {
                        if (other$gsshBillNoReturn == null) {
                            break label463;
                        }
                    } else if (this$gsshBillNoReturn.equals(other$gsshBillNoReturn)) {
                        break label463;
                    }

                    return false;
                }

                label456: {
                    Object this$gsshEmpReturn = this.getGsshEmpReturn();
                    Object other$gsshEmpReturn = other.getGsshEmpReturn();
                    if (this$gsshEmpReturn == null) {
                        if (other$gsshEmpReturn == null) {
                            break label456;
                        }
                    } else if (this$gsshEmpReturn.equals(other$gsshEmpReturn)) {
                        break label456;
                    }

                    return false;
                }

                Object this$gsshPromotionType1 = this.getGsshPromotionType1();
                Object other$gsshPromotionType1 = other.getGsshPromotionType1();
                if (this$gsshPromotionType1 == null) {
                    if (other$gsshPromotionType1 != null) {
                        return false;
                    }
                } else if (!this$gsshPromotionType1.equals(other$gsshPromotionType1)) {
                    return false;
                }

                label442: {
                    Object this$gsshPromotionType2 = this.getGsshPromotionType2();
                    Object other$gsshPromotionType2 = other.getGsshPromotionType2();
                    if (this$gsshPromotionType2 == null) {
                        if (other$gsshPromotionType2 == null) {
                            break label442;
                        }
                    } else if (this$gsshPromotionType2.equals(other$gsshPromotionType2)) {
                        break label442;
                    }

                    return false;
                }

                Object this$gsshPromotionType3 = this.getGsshPromotionType3();
                Object other$gsshPromotionType3 = other.getGsshPromotionType3();
                if (this$gsshPromotionType3 == null) {
                    if (other$gsshPromotionType3 != null) {
                        return false;
                    }
                } else if (!this$gsshPromotionType3.equals(other$gsshPromotionType3)) {
                    return false;
                }

                label428: {
                    Object this$gsshPromotionType4 = this.getGsshPromotionType4();
                    Object other$gsshPromotionType4 = other.getGsshPromotionType4();
                    if (this$gsshPromotionType4 == null) {
                        if (other$gsshPromotionType4 == null) {
                            break label428;
                        }
                    } else if (this$gsshPromotionType4.equals(other$gsshPromotionType4)) {
                        break label428;
                    }

                    return false;
                }

                Object this$gsshPromotionType5 = this.getGsshPromotionType5();
                Object other$gsshPromotionType5 = other.getGsshPromotionType5();
                if (this$gsshPromotionType5 == null) {
                    if (other$gsshPromotionType5 != null) {
                        return false;
                    }
                } else if (!this$gsshPromotionType5.equals(other$gsshPromotionType5)) {
                    return false;
                }

                Object this$gsshRegisterVoucherId = this.getGsshRegisterVoucherId();
                Object other$gsshRegisterVoucherId = other.getGsshRegisterVoucherId();
                if (this$gsshRegisterVoucherId == null) {
                    if (other$gsshRegisterVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsshRegisterVoucherId.equals(other$gsshRegisterVoucherId)) {
                    return false;
                }

                label407: {
                    Object this$gsshReplaceBrId = this.getGsshReplaceBrId();
                    Object other$gsshReplaceBrId = other.getGsshReplaceBrId();
                    if (this$gsshReplaceBrId == null) {
                        if (other$gsshReplaceBrId == null) {
                            break label407;
                        }
                    } else if (this$gsshReplaceBrId.equals(other$gsshReplaceBrId)) {
                        break label407;
                    }

                    return false;
                }

                label400: {
                    Object this$gsshReplaceSalerId = this.getGsshReplaceSalerId();
                    Object other$gsshReplaceSalerId = other.getGsshReplaceSalerId();
                    if (this$gsshReplaceSalerId == null) {
                        if (other$gsshReplaceSalerId == null) {
                            break label400;
                        }
                    } else if (this$gsshReplaceSalerId.equals(other$gsshReplaceSalerId)) {
                        break label400;
                    }

                    return false;
                }

                Object this$gsshHideFlag = this.getGsshHideFlag();
                Object other$gsshHideFlag = other.getGsshHideFlag();
                if (this$gsshHideFlag == null) {
                    if (other$gsshHideFlag != null) {
                        return false;
                    }
                } else if (!this$gsshHideFlag.equals(other$gsshHideFlag)) {
                    return false;
                }

                Object this$gsshCallQty = this.getGsshCallQty();
                Object other$gsshCallQty = other.getGsshCallQty();
                if (this$gsshCallQty == null) {
                    if (other$gsshCallQty != null) {
                        return false;
                    }
                } else if (!this$gsshCallQty.equals(other$gsshCallQty)) {
                    return false;
                }

                label379: {
                    Object this$gsshCallAllow = this.getGsshCallAllow();
                    Object other$gsshCallAllow = other.getGsshCallAllow();
                    if (this$gsshCallAllow == null) {
                        if (other$gsshCallAllow == null) {
                            break label379;
                        }
                    } else if (this$gsshCallAllow.equals(other$gsshCallAllow)) {
                        break label379;
                    }

                    return false;
                }

                Object this$gsshEmpGroupName = this.getGsshEmpGroupName();
                Object other$gsshEmpGroupName = other.getGsshEmpGroupName();
                if (this$gsshEmpGroupName == null) {
                    if (other$gsshEmpGroupName != null) {
                        return false;
                    }
                } else if (!this$gsshEmpGroupName.equals(other$gsshEmpGroupName)) {
                    return false;
                }

                Object this$gsshUpload = this.getGsshUpload();
                Object other$gsshUpload = other.getGsshUpload();
                if (this$gsshUpload == null) {
                    if (other$gsshUpload != null) {
                        return false;
                    }
                } else if (!this$gsshUpload.equals(other$gsshUpload)) {
                    return false;
                }

                Object this$gsshHykPhone = this.getGsshHykPhone();
                Object other$gsshHykPhone = other.getGsshHykPhone();
                if (this$gsshHykPhone == null) {
                    if (other$gsshHykPhone != null) {
                        return false;
                    }
                } else if (!this$gsshHykPhone.equals(other$gsshHykPhone)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSyncSdSaleHOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsshBillNo = this.getGsshBillNo();
        result = result * 59 + ($gsshBillNo == null ? 43 : $gsshBillNo.hashCode());
        Object $gsshBrId = this.getGsshBrId();
        result = result * 59 + ($gsshBrId == null ? 43 : $gsshBrId.hashCode());
        Object $gsshDate = this.getGsshDate();
        result = result * 59 + ($gsshDate == null ? 43 : $gsshDate.hashCode());
        Object $gsshTime = this.getGsshTime();
        result = result * 59 + ($gsshTime == null ? 43 : $gsshTime.hashCode());
        Object $gsshEmp = this.getGsshEmp();
        result = result * 59 + ($gsshEmp == null ? 43 : $gsshEmp.hashCode());
        Object $gsshTaxNo = this.getGsshTaxNo();
        result = result * 59 + ($gsshTaxNo == null ? 43 : $gsshTaxNo.hashCode());
        Object $gsshHykNo = this.getGsshHykNo();
        result = result * 59 + ($gsshHykNo == null ? 43 : $gsshHykNo.hashCode());
        Object $gsshZkAmt = this.getGsshZkAmt();
        result = result * 59 + ($gsshZkAmt == null ? 43 : $gsshZkAmt.hashCode());
        Object $gsshYsAmt = this.getGsshYsAmt();
        result = result * 59 + ($gsshYsAmt == null ? 43 : $gsshYsAmt.hashCode());
        Object $gsshRmbZlAmt = this.getGsshRmbZlAmt();
        result = result * 59 + ($gsshRmbZlAmt == null ? 43 : $gsshRmbZlAmt.hashCode());
        Object $gsshRmbAmt = this.getGsshRmbAmt();
        result = result * 59 + ($gsshRmbAmt == null ? 43 : $gsshRmbAmt.hashCode());
        Object $gsshDyqNo = this.getGsshDyqNo();
        result = result * 59 + ($gsshDyqNo == null ? 43 : $gsshDyqNo.hashCode());
        Object $gsshDyqAmt = this.getGsshDyqAmt();
        result = result * 59 + ($gsshDyqAmt == null ? 43 : $gsshDyqAmt.hashCode());
        Object $gsshDyqType = this.getGsshDyqType();
        result = result * 59 + ($gsshDyqType == null ? 43 : $gsshDyqType.hashCode());
        Object $gsshRechargeCardNo = this.getGsshRechargeCardNo();
        result = result * 59 + ($gsshRechargeCardNo == null ? 43 : $gsshRechargeCardNo.hashCode());
        Object $gsshRechargeCardAmt = this.getGsshRechargeCardAmt();
        result = result * 59 + ($gsshRechargeCardAmt == null ? 43 : $gsshRechargeCardAmt.hashCode());
        Object $gsshDzqczActno1 = this.getGsshDzqczActno1();
        result = result * 59 + ($gsshDzqczActno1 == null ? 43 : $gsshDzqczActno1.hashCode());
        Object $gsshDzqczAmt1 = this.getGsshDzqczAmt1();
        result = result * 59 + ($gsshDzqczAmt1 == null ? 43 : $gsshDzqczAmt1.hashCode());
        Object $gsshDzqczActno2 = this.getGsshDzqczActno2();
        result = result * 59 + ($gsshDzqczActno2 == null ? 43 : $gsshDzqczActno2.hashCode());
        Object $gsshDzqczAmt2 = this.getGsshDzqczAmt2();
        result = result * 59 + ($gsshDzqczAmt2 == null ? 43 : $gsshDzqczAmt2.hashCode());
        Object $gsshDzqczActno3 = this.getGsshDzqczActno3();
        result = result * 59 + ($gsshDzqczActno3 == null ? 43 : $gsshDzqczActno3.hashCode());
        Object $gsshDzqczAmt3 = this.getGsshDzqczAmt3();
        result = result * 59 + ($gsshDzqczAmt3 == null ? 43 : $gsshDzqczAmt3.hashCode());
        Object $gsshDzqczActno4 = this.getGsshDzqczActno4();
        result = result * 59 + ($gsshDzqczActno4 == null ? 43 : $gsshDzqczActno4.hashCode());
        Object $gsshDzqczAmt4 = this.getGsshDzqczAmt4();
        result = result * 59 + ($gsshDzqczAmt4 == null ? 43 : $gsshDzqczAmt4.hashCode());
        Object $gsshDzqczActno5 = this.getGsshDzqczActno5();
        result = result * 59 + ($gsshDzqczActno5 == null ? 43 : $gsshDzqczActno5.hashCode());
        Object $gsshDzqczAmt5 = this.getGsshDzqczAmt5();
        result = result * 59 + ($gsshDzqczAmt5 == null ? 43 : $gsshDzqczAmt5.hashCode());
        Object $gsshDzqdyActno1 = this.getGsshDzqdyActno1();
        result = result * 59 + ($gsshDzqdyActno1 == null ? 43 : $gsshDzqdyActno1.hashCode());
        Object $gsshDzqdyAmt1 = this.getGsshDzqdyAmt1();
        result = result * 59 + ($gsshDzqdyAmt1 == null ? 43 : $gsshDzqdyAmt1.hashCode());
        Object $gsshDzqdyActno2 = this.getGsshDzqdyActno2();
        result = result * 59 + ($gsshDzqdyActno2 == null ? 43 : $gsshDzqdyActno2.hashCode());
        Object $gsshDzqdyAmt2 = this.getGsshDzqdyAmt2();
        result = result * 59 + ($gsshDzqdyAmt2 == null ? 43 : $gsshDzqdyAmt2.hashCode());
        Object $gsshDzqdyActno3 = this.getGsshDzqdyActno3();
        result = result * 59 + ($gsshDzqdyActno3 == null ? 43 : $gsshDzqdyActno3.hashCode());
        Object $gsshDzqdyAmt3 = this.getGsshDzqdyAmt3();
        result = result * 59 + ($gsshDzqdyAmt3 == null ? 43 : $gsshDzqdyAmt3.hashCode());
        Object $gsshDzqdyActno4 = this.getGsshDzqdyActno4();
        result = result * 59 + ($gsshDzqdyActno4 == null ? 43 : $gsshDzqdyActno4.hashCode());
        Object $gsshDzqdyAmt4 = this.getGsshDzqdyAmt4();
        result = result * 59 + ($gsshDzqdyAmt4 == null ? 43 : $gsshDzqdyAmt4.hashCode());
        Object $gsshDzqdyActno5 = this.getGsshDzqdyActno5();
        result = result * 59 + ($gsshDzqdyActno5 == null ? 43 : $gsshDzqdyActno5.hashCode());
        Object $gsshDzqdyAmt5 = this.getGsshDzqdyAmt5();
        result = result * 59 + ($gsshDzqdyAmt5 == null ? 43 : $gsshDzqdyAmt5.hashCode());
        Object $gsshIntegralAdd = this.getGsshIntegralAdd();
        result = result * 59 + ($gsshIntegralAdd == null ? 43 : $gsshIntegralAdd.hashCode());
        Object $gsshIntegralExchange = this.getGsshIntegralExchange();
        result = result * 59 + ($gsshIntegralExchange == null ? 43 : $gsshIntegralExchange.hashCode());
        Object $gsshIntegralExchangeAmt = this.getGsshIntegralExchangeAmt();
        result = result * 59 + ($gsshIntegralExchangeAmt == null ? 43 : $gsshIntegralExchangeAmt.hashCode());
        Object $gsshIntegralCash = this.getGsshIntegralCash();
        result = result * 59 + ($gsshIntegralCash == null ? 43 : $gsshIntegralCash.hashCode());
        Object $gsshIntegralCashAmt = this.getGsshIntegralCashAmt();
        result = result * 59 + ($gsshIntegralCashAmt == null ? 43 : $gsshIntegralCashAmt.hashCode());
        Object $gsshPaymentNo1 = this.getGsshPaymentNo1();
        result = result * 59 + ($gsshPaymentNo1 == null ? 43 : $gsshPaymentNo1.hashCode());
        Object $gsshPaymentAmt1 = this.getGsshPaymentAmt1();
        result = result * 59 + ($gsshPaymentAmt1 == null ? 43 : $gsshPaymentAmt1.hashCode());
        Object $gsshPaymentNo2 = this.getGsshPaymentNo2();
        result = result * 59 + ($gsshPaymentNo2 == null ? 43 : $gsshPaymentNo2.hashCode());
        Object $gsshPaymentAmt2 = this.getGsshPaymentAmt2();
        result = result * 59 + ($gsshPaymentAmt2 == null ? 43 : $gsshPaymentAmt2.hashCode());
        Object $gsshPaymentNo3 = this.getGsshPaymentNo3();
        result = result * 59 + ($gsshPaymentNo3 == null ? 43 : $gsshPaymentNo3.hashCode());
        Object $gsshPaymentAmt3 = this.getGsshPaymentAmt3();
        result = result * 59 + ($gsshPaymentAmt3 == null ? 43 : $gsshPaymentAmt3.hashCode());
        Object $gsshPaymentNo4 = this.getGsshPaymentNo4();
        result = result * 59 + ($gsshPaymentNo4 == null ? 43 : $gsshPaymentNo4.hashCode());
        Object $gsshPaymentAmt4 = this.getGsshPaymentAmt4();
        result = result * 59 + ($gsshPaymentAmt4 == null ? 43 : $gsshPaymentAmt4.hashCode());
        Object $gsshPaymentNo5 = this.getGsshPaymentNo5();
        result = result * 59 + ($gsshPaymentNo5 == null ? 43 : $gsshPaymentNo5.hashCode());
        Object $gsshPaymentAmt5 = this.getGsshPaymentAmt5();
        result = result * 59 + ($gsshPaymentAmt5 == null ? 43 : $gsshPaymentAmt5.hashCode());
        Object $gsshBillNoReturn = this.getGsshBillNoReturn();
        result = result * 59 + ($gsshBillNoReturn == null ? 43 : $gsshBillNoReturn.hashCode());
        Object $gsshEmpReturn = this.getGsshEmpReturn();
        result = result * 59 + ($gsshEmpReturn == null ? 43 : $gsshEmpReturn.hashCode());
        Object $gsshPromotionType1 = this.getGsshPromotionType1();
        result = result * 59 + ($gsshPromotionType1 == null ? 43 : $gsshPromotionType1.hashCode());
        Object $gsshPromotionType2 = this.getGsshPromotionType2();
        result = result * 59 + ($gsshPromotionType2 == null ? 43 : $gsshPromotionType2.hashCode());
        Object $gsshPromotionType3 = this.getGsshPromotionType3();
        result = result * 59 + ($gsshPromotionType3 == null ? 43 : $gsshPromotionType3.hashCode());
        Object $gsshPromotionType4 = this.getGsshPromotionType4();
        result = result * 59 + ($gsshPromotionType4 == null ? 43 : $gsshPromotionType4.hashCode());
        Object $gsshPromotionType5 = this.getGsshPromotionType5();
        result = result * 59 + ($gsshPromotionType5 == null ? 43 : $gsshPromotionType5.hashCode());
        Object $gsshRegisterVoucherId = this.getGsshRegisterVoucherId();
        result = result * 59 + ($gsshRegisterVoucherId == null ? 43 : $gsshRegisterVoucherId.hashCode());
        Object $gsshReplaceBrId = this.getGsshReplaceBrId();
        result = result * 59 + ($gsshReplaceBrId == null ? 43 : $gsshReplaceBrId.hashCode());
        Object $gsshReplaceSalerId = this.getGsshReplaceSalerId();
        result = result * 59 + ($gsshReplaceSalerId == null ? 43 : $gsshReplaceSalerId.hashCode());
        Object $gsshHideFlag = this.getGsshHideFlag();
        result = result * 59 + ($gsshHideFlag == null ? 43 : $gsshHideFlag.hashCode());
        Object $gsshCallQty = this.getGsshCallQty();
        result = result * 59 + ($gsshCallQty == null ? 43 : $gsshCallQty.hashCode());
        Object $gsshCallAllow = this.getGsshCallAllow();
        result = result * 59 + ($gsshCallAllow == null ? 43 : $gsshCallAllow.hashCode());
        Object $gsshEmpGroupName = this.getGsshEmpGroupName();
        result = result * 59 + ($gsshEmpGroupName == null ? 43 : $gsshEmpGroupName.hashCode());
        Object $gsshUpload = this.getGsshUpload();
        result = result * 59 + ($gsshUpload == null ? 43 : $gsshUpload.hashCode());
        Object $gsshHykPhone = this.getGsshHykPhone();
        result = result * 59 + ($gsshHykPhone == null ? 43 : $gsshHykPhone.hashCode());
        return result;
    }

    public String toString() {
        return "GetSyncSdSaleHOutData(clientId=" + this.getClientId() + ", gsshBillNo=" + this.getGsshBillNo() + ", gsshBrId=" + this.getGsshBrId() + ", gsshDate=" + this.getGsshDate() + ", gsshTime=" + this.getGsshTime() + ", gsshEmp=" + this.getGsshEmp() + ", gsshTaxNo=" + this.getGsshTaxNo() + ", gsshHykNo=" + this.getGsshHykNo() + ", gsshZkAmt=" + this.getGsshZkAmt() + ", gsshYsAmt=" + this.getGsshYsAmt() + ", gsshRmbZlAmt=" + this.getGsshRmbZlAmt() + ", gsshRmbAmt=" + this.getGsshRmbAmt() + ", gsshDyqNo=" + this.getGsshDyqNo() + ", gsshDyqAmt=" + this.getGsshDyqAmt() + ", gsshDyqType=" + this.getGsshDyqType() + ", gsshRechargeCardNo=" + this.getGsshRechargeCardNo() + ", gsshRechargeCardAmt=" + this.getGsshRechargeCardAmt() + ", gsshDzqczActno1=" + this.getGsshDzqczActno1() + ", gsshDzqczAmt1=" + this.getGsshDzqczAmt1() + ", gsshDzqczActno2=" + this.getGsshDzqczActno2() + ", gsshDzqczAmt2=" + this.getGsshDzqczAmt2() + ", gsshDzqczActno3=" + this.getGsshDzqczActno3() + ", gsshDzqczAmt3=" + this.getGsshDzqczAmt3() + ", gsshDzqczActno4=" + this.getGsshDzqczActno4() + ", gsshDzqczAmt4=" + this.getGsshDzqczAmt4() + ", gsshDzqczActno5=" + this.getGsshDzqczActno5() + ", gsshDzqczAmt5=" + this.getGsshDzqczAmt5() + ", gsshDzqdyActno1=" + this.getGsshDzqdyActno1() + ", gsshDzqdyAmt1=" + this.getGsshDzqdyAmt1() + ", gsshDzqdyActno2=" + this.getGsshDzqdyActno2() + ", gsshDzqdyAmt2=" + this.getGsshDzqdyAmt2() + ", gsshDzqdyActno3=" + this.getGsshDzqdyActno3() + ", gsshDzqdyAmt3=" + this.getGsshDzqdyAmt3() + ", gsshDzqdyActno4=" + this.getGsshDzqdyActno4() + ", gsshDzqdyAmt4=" + this.getGsshDzqdyAmt4() + ", gsshDzqdyActno5=" + this.getGsshDzqdyActno5() + ", gsshDzqdyAmt5=" + this.getGsshDzqdyAmt5() + ", gsshIntegralAdd=" + this.getGsshIntegralAdd() + ", gsshIntegralExchange=" + this.getGsshIntegralExchange() + ", gsshIntegralExchangeAmt=" + this.getGsshIntegralExchangeAmt() + ", gsshIntegralCash=" + this.getGsshIntegralCash() + ", gsshIntegralCashAmt=" + this.getGsshIntegralCashAmt() + ", gsshPaymentNo1=" + this.getGsshPaymentNo1() + ", gsshPaymentAmt1=" + this.getGsshPaymentAmt1() + ", gsshPaymentNo2=" + this.getGsshPaymentNo2() + ", gsshPaymentAmt2=" + this.getGsshPaymentAmt2() + ", gsshPaymentNo3=" + this.getGsshPaymentNo3() + ", gsshPaymentAmt3=" + this.getGsshPaymentAmt3() + ", gsshPaymentNo4=" + this.getGsshPaymentNo4() + ", gsshPaymentAmt4=" + this.getGsshPaymentAmt4() + ", gsshPaymentNo5=" + this.getGsshPaymentNo5() + ", gsshPaymentAmt5=" + this.getGsshPaymentAmt5() + ", gsshBillNoReturn=" + this.getGsshBillNoReturn() + ", gsshEmpReturn=" + this.getGsshEmpReturn() + ", gsshPromotionType1=" + this.getGsshPromotionType1() + ", gsshPromotionType2=" + this.getGsshPromotionType2() + ", gsshPromotionType3=" + this.getGsshPromotionType3() + ", gsshPromotionType4=" + this.getGsshPromotionType4() + ", gsshPromotionType5=" + this.getGsshPromotionType5() + ", gsshRegisterVoucherId=" + this.getGsshRegisterVoucherId() + ", gsshReplaceBrId=" + this.getGsshReplaceBrId() + ", gsshReplaceSalerId=" + this.getGsshReplaceSalerId() + ", gsshHideFlag=" + this.getGsshHideFlag() + ", gsshCallQty=" + this.getGsshCallQty() + ", gsshCallAllow=" + this.getGsshCallAllow() + ", gsshEmpGroupName=" + this.getGsshEmpGroupName() + ", gsshUpload=" + this.getGsshUpload() + ", gsshHykPhone=" + this.getGsshHykPhone() + ")";
    }
}
