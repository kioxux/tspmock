//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GaiaSdRechargeCardChangeInData;
import com.gys.business.service.data.GaiaSdRechargeCardLoseInData;
import com.gys.business.service.data.RechComsuInData;
import com.gys.business.service.data.RechComsuOutData;
import com.gys.business.service.data.RechargeCardInData;
import com.gys.business.service.data.RechargeCardOutData;
import com.gys.business.service.data.RechargeCardPaycheckInData;
import com.gys.business.service.data.RechargeCardSetInData;
import com.gys.business.service.data.RechargeCardSetOutData;
import com.gys.common.data.GetLoginOutData;
import java.util.List;

public interface RechargeCardService {
    void saveCardInfo(RechargeCardInData inData, GetLoginOutData userInfo);

    String generateCardCode(GetLoginOutData userInfo);

    void resetPassword(RechargeCardInData inData, GetLoginOutData userInfo);

    void modifyPassword(RechargeCardInData inData, GetLoginOutData userInfo);

    void savePayInfo(RechargeCardPaycheckInData inData, GetLoginOutData userInfo);

    List<RechargeCardOutData> getCardList(RechargeCardInData inData);

    List<RechComsuOutData> getRechComsuList(RechComsuInData inData);

    void saveSet(RechargeCardSetInData inData, GetLoginOutData userInfo);

    RechargeCardSetOutData getSetData(GetLoginOutData userInfo);

    void loseCard(GaiaSdRechargeCardLoseInData inData, GetLoginOutData userInfo);

    void changeCard(GaiaSdRechargeCardChangeInData inData, GetLoginOutData userInfo);

    RechargeCardOutData getCardInfo(RechargeCardInData inData);
}
