package com.gys.business.service.data;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 收银页面实体
 *
 * @author xiaoyuan on 2020/8/9
 */
@Data
public class RebornData implements Serializable {
    private static final long serialVersionUID = -3832865077427881996L;

    /**
     * 初始值数量
     */
    private String gssdOriQty;

    /**
     * 贴数
     */
    private String gssdDose;
    /**
     * 互斥条件
     */
    private String gsphExclusion;

    /**
     * 等于3 表示中药
     */
    private String proStorageAreas;

    /**
     * 序号
     */
    private String index;

    /**
     * 折扣
     */
    private String disc;

    private String isGift;

    private boolean special;

    /**
     * 状态
     */
    private String status;

    /**
     * 名称
     */
    private String proName;

    /**
     * (明细)加盟商
     */
    private String clientId;

    /**
     * (明细)销售单号
     */
    private String gssdBillNo;

    /**
     * (明细)店号
     */
    private String gssdBrId;

    /**
     * (明细)销售日期
     */
    private String gssdDate;

    /**
     * (明细)行号
     */
    private String gssdSerial;

    /**
     * (明细)商品编码
     */
    private String gssdProId;

    /**
     * (明细)商品批号
     */
    private String gssdBatchNo;

    /**
     * (明细)商品批次
     */
    private String gssdBatch;

    /**
     * (明细)商品有效期
     */
    private String gssdValidDate;

    /**
     * (明细)监管码/条形码
     */
    private String gssdStCode;

    /**
     * (明细)单品零售价
     */
    private String gssdPrc1;

    /**
     * (明细)单品应收价
     */
    private String gssdPrc2;

    /**
     * (明细)数量
     */
    private String gssdQty;

    /**
     * (明细)汇总应收金额
     */
    private String gssdAmt;

    /**
     * (明细)兑换单个积分
     */
    private String gssdIntegralExchangeSingle;

    /**
     * (明细)兑换积分汇总
     */
    private String gssdIntegralExchangeCollect;

    /**
     * (明细)单个加价兑换积分金额
     */
    private String gssdIntegralExchangeAmtSingle;

    /**
     * (明细)加价兑换积分金额汇总
     */
    private String gssdIntegralExchangeAmtCollect;

    /**
     * (明细)商品折扣总金额
     */
    private String gssdZkAmt;

    /**
     * (明细)积分兑换折扣金额
     */
    private String gssdZkJfdh;

    /**
     * (明细)积分抵现折扣金额
     */
    private String gssdZkJfdx;

    /**
     * (明细)电子券折扣金额
     */
    private String gssdZkDzq;

    /**
     * (明细)抵用券折扣金额
     */
    private String gssdZkDyq;

    /**
     * (明细)促销折扣金额
     */
    private String gssdZkPm;

    /**
     * (明细)营业员编号
     */
    private String gssdSalerId;
    /**
     * (明细)营业员姓名
     */
    private String gssdSalerName;

    /**
     * (明细)医生编号
     */
    private String gssdDoctorId;
    /**
     * (明细)医生姓名
     */
    private String gssdDoctorName;

    /**
     * (明细)参加促销主题编号
     */
    private String gssdPmSubjectId;

    /**
     * (明细)参加促销类型编码
     */
    private String gssdPmId;

    /**
     * (明细)参加促销类型说明
     */
    private String gssdPmContent;
    /**
     * (明细)参加促销编号
     */
    private String gssdPmActivityId;
    /**
     * (明细)参加促销活动名称
     */
    private String gssdPmActivityName;
    /**
     * (明细)参加促销活动参数
     */
    private String gssdPmActivityFlag;
    /**
     * (明细)赠品促销 条件数量缓存字段
     */
    private BigDecimal promGiftQtyTemp;
    /**
     * (明细)赠品促销 条件金额缓存字段
     */
    private BigDecimal promGiftAmtTemp;
    /**
     * (明细)赠品促销 赠送数量缓存字段
     */
    private BigDecimal promGiftSendQtyTemp;

    /**
     * (明细)是否登记处方信息 0否1是
     */
    private String gssdRecipelFlag;

    /**
     * (明细)是否为关联推荐
     */
    private String gssdRelationFlag;

    /**
     * (明细)特殊药品购买人身份证
     */
    private String gssdSpecialmedIdcard;

    /**
     * (明细)特殊药品购买人姓名
     */
    private String gssdSpecialmedName;

    /**
     * (明细)特殊药品购买人性别
     */
    private String gssdSpecialmedSex;

    /**
     * (明细)特殊药品购买人出生日期
     */
    private String gssdSpecialmedBirthday;

    /**
     * (明细)特殊药品购买人手机
     */
    private String gssdSpecialmedMobile;

    /**
     * (明细)特殊药品购买人地址
     */
    private String gssdSpecialmedAddress;
    /**
     * (明细)特殊药品电子监管码
     */
    private String gssdSpecialmedEcode;

    /**
     * (明细)价格状态
     */
    private String gssdProStatus;

    /**
     * (明细)批次成本价
     */
    private String gssdBatchCost;

    /**
     * (明细)批次成本税额
     */
    private String gssdBatchTax;

    /**
     * (明细)移动平均价
     */
    private String gssdMovPrice;

    /**
     * (明细)平均成本税额
     */
    private String gssdMovTax;

    /**
     * (明细)是否生成凭证 1-是 0-否
     */
    private String gssdAdFlag;
    /**
     * (明细)促销单号
     */
    private String voucherId;


    /**
     * (商品主数据) 商品编码
     */
    private String proCode;

    /**
     * (商品主数据)地点
     */
    private String proSite;

    /**
     * (商品主数据)商品自编码
     */
    private String proSelfCode;

    /**
     * (商品主数据)匹配状态
     */
    private String proMatchStatus;

    /**
     * (商品主数据)通用名称
     */
    private String proCommonname;

    /**
     * (商品主数据)商品描述
     */
    private String proDepict;

    /**
     * (商品主数据)助记码
     */
    private String proPym;


    /**
     * (商品主数据)规格
     */
    private String proSpecs;

    /**
     * (商品主数据)计量单位
     */
    private String proUnit;

    /**
     * (商品主数据)剂型
     */
    private String proForm;

    /**
     * (商品主数据)细分剂型
     */
    private String proPartform;

    /**
     * (商品主数据)最小剂量（以mg/ml计算）
     */
    private String proMindose;

    /**
     * (商品主数据)总剂量（以mg/ml计算）
     */
    private String proTotaldose;

    /**
     * (商品主数据)国际条形码1
     */
    private String proBarcode;

    /**
     * (商品主数据)国际条形码2
     */
    private String proBarcode2;

    /**
     * (商品主数据)批准文号分类
     */
    private String proRegisterClass;

    /**
     * (商品主数据)批准文号
     */
    private String proRegisterNo;

    /**
     * (商品主数据)批准文号批准日期
     */
    private String proRegisterDate;

    /**
     * (商品主数据)批准文号失效日期
     */
    private String proRegisterExdate;

    /**
     * (商品主数据)商品分类
     */
    private String proClass;

    /**
     * (商品主数据)商品分类描述
     */
    private String proClassName;

    /**
     * (商品主数据)成分分类
     */
    private String proCompclass;

    /**
     * (商品主数据)成分分类描述
     */
    private String proCompclassName;

    /**
     * (商品主数据)处方类别
     */
    private String proPresclass;

    /**
     * (商品主数据)生产企业代码
     */
    private String proFactoryCode;

    /**
     * (商品主数据)生产企业
     */
    private String proFactoryName;

    /**
     * (商品主数据)商标
     */
    private String proMark;

    /**
     * (商品主数据)品牌标识名
     */
    private String proBrand;

    /**
     * (商品主数据)品牌区分
     */
    private String proBrandClass;

    /**
     * (商品主数据)保质期
     */
    private String proLife;

    /**
     * (商品主数据)保质期单位
     */
    private String proLifeUnit;

    /**
     * (商品主数据)上市许可持有人
     */
    private String proHolder;

    /**
     * (商品主数据)进项税率
     */
    private String proInputTax;

    /**
     * (商品主数据)销项税率
     */
    private String proOutputTax;

    /**
     * (商品主数据)药品本位码
     */
    private String proBasicCode;

    /**
     * (商品主数据)税务分类编码
     */
    private String proTaxClass;

    /**
     * (商品主数据)管制特殊分类
     */
    private String proControlClass;

    /**
     * (商品主数据)生产类别
     */
    private String proProduceClass;

    /**
     * (商品主数据)贮存条件
     */
    private String proStorageCondition;

    /**
     * (商品主数据)商品仓储分区
     */
    private String proStorageArea;

    /**
     * (商品主数据)长（以MM计算）
     */
    private String proLong;

    /**
     * (商品主数据)宽（以MM计算）
     */
    private String proWide;

    /**
     * (商品主数据)高（以MM计算）
     */
    private String proHigh;

    /**
     * (商品主数据)中包装量
     */
    private String proMidPackage;

    /**
     * (商品主数据)大包装量
     */
    private String proBigPackage;

    /**
     * (商品主数据)启用电子监管码
     */
    private String proElectronicCode;

    /**
     * (商品主数据)生产经营许可证号
     */
    private String proQsCode;

    /**
     * (商品主数据)最大销售量
     */
    private String proMaxSales;

    /**
     * (商品主数据)说明书代码
     */
    private String proInstructionCode;

    /**
     * (商品主数据)说明书内容
     */
    private String proInstruction;

    /**
     * (商品主数据)国家医保品种
     */
    private String proMedProdct;

    /**
     * (商品主数据)商品状态
     */
    private String proStatus;

    /**
     * (商品主数据)国家医保品种编码
     */
    private String proMedProdctcode;

    /**
     * (商品主数据)生产国家
     */
    private String proCountry;

    /**
     * (商品主数据)产地
     */
    private String proPlace;

    /**
     * (商品主数据)可服用天数
     */
    private String proTakeDays;

    /**
     * (商品主数据)用法用量
     */
    private String proUsage;

    /**
     * (商品主数据)禁忌说明
     */
    private String proContraindication;

    /**
     * (商品主数据)商品定位
     */
    private String proPosition;

    /**
     * (商品主数据)禁止销售
     */
    private String proNoRetail;

    /**
     * (商品主数据)禁止采购
     */
    private String proNoPurchase;

    /**
     * (商品主数据)禁止配送
     */
    private String proNoDistributed;

    /**
     * (商品主数据)禁止退厂
     */
    private String proNoSupplier;

    /**
     * (商品主数据)禁止退仓
     */
    private String proNoDc;

    /**
     * (商品主数据)禁止调剂
     */
    private String proNoAdjust;

    /**
     * (商品主数据)禁止批发
     */
    private String proNoSale;

    /**
     * (商品主数据)禁止请货
     */
    private String proNoApply;

    /**
     * (商品主数据)是否拆零
     */
    private String proIfpart;

    /**
     * (商品主数据)拆零单位
     */
    private String proPartUint;

    /**
     * (商品主数据)拆零比例
     */
    private String proPartRate;

    /**
     * (商品主数据)采购单位
     */
    private String proPurchaseUnit;

    /**
     * (商品主数据)采购比例
     */
    private String proPurchaseRate;

    /**
     * (商品主数据)销售单位
     */
    private String proSaleUnit;

    /**
     * (商品主数据)销售比例
     */
    private String proSaleRate;

    /**
     * (商品主数据)最小订货量
     */
    private String proMinQty;

    /**
     * (商品主数据)是否医保
     */
    private String proIfMed;

    /**
     * (商品主数据)销售级别
     */
    private String proSlaeClass;

    /**
     * (商品主数据)限购数量
     */
    private String proLimitQty;

    /**
     * (商品主数据)中药规格
     */
    private String proMaxQty;

    /**
     * (商品主数据)按中包装量倍数配货
     */
    private String proPackageFlag;

    /**
     * (商品主数据)是否重点养护
     */
    private String proKeyCare;

    /**
     * (商品主数据)中药规格
     */
    private String proTcmSpecs;

    /**
     * (商品主数据)中药批准文号
     */
    private String proTcmRegisterNo;

    /**
     * (商品主数据)中药生产企业代码
     */
    private String proTcmFactoryCode;

    /**
     * (商品主数据)中药产地
     */
    private String proTcmPlace;


    /**
     * (门店批号库存表) 库存数量
     */
    private String gssbQty;

    /**
     *(缓存数据) 当前行商品状态
     */
    private String billItemStatus;

    /**
     *（缓存数据） 当前行商品折扣比例
     */
    private String billItemRate;


    /**
     * (GAIA_SD_PRODUCT_PRICE) 商品标准价格
     */
    private String proPriceNormal;

    /**
     * 医保 库存编码
     */
    private String stockCode;
    /**
     * 医保 商品编码
     */
    private String ybProCode;

    /**
     * 货位号
     */
    private String huowei;

    /**
     * 医保刷卡数量
     */
    private String proZdy1;
    /**
     * 等级
     */
    private String saleClass;


    /**
     * 销售单号
     */
    private String gsshBillNo;

    /**
     * 店名
     */
    private String gsshBrId;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 销售时间
     */
    private String gsshTime;

    /**
     * 收银工号
     */
    private String gsshEmp;

    /**
     * 发票号
     */
    private String gsshTaxNo;

    /**
     * 会员卡号
     */
    private String gsshHykNo;

    /**
     * 折扣金额
     */
    private BigDecimal gsshZkAmt;

    /**
     * 应收金额
     */
    private BigDecimal gsshYsAmt;

    /**
     * 应收金额
     */
    private BigDecimal gsshRmbZlAmt;

    /**
     * 现金收银金额
     */
    private BigDecimal gsshRmbAmt;

    /**
     * 抵用券卡号
     */
    private String gsshDyqNo;

    /**
     * 抵用券金额
     */
    private BigDecimal gsshDyqAmt;

    /**
     * 抵用原因
     */
    private String gsshDyqType;

    /**
     * 储值卡号
     */
    private String gsshRechargeCardNo;

    /**
     * 储值卡消费金额
     */
    private BigDecimal gsshRechargeCardAmt;

    /**
     * 电子券充值卡号1
     */
    private String gsshDzqczActno1;

    /**
     * 电子券充值金额1
     */
    private BigDecimal gsshDzqczAmt1;

    /**
     * 电子券抵用卡号1
     */
    private String gsshDzqdyActno1;

    /**
     * 电子券抵用金额1
     */
    private BigDecimal gsshDzqdyAmt1;

    /**
     * 增加积分
     */
    private String gsshIntegralAdd;

    /**
     * 兑换积分
     */
    private String gsshIntegralExchange;

    /**
     * 	加价兑换积分金额
     */
    private BigDecimal gsshIntegralExchangeAmt;

    /**
     * 抵现积分
     */
    private String gsshIntegralCash;

    /**
     * 抵现金额
     */
    private BigDecimal gsshIntegralCashAmt;

    /**
     * 支付方式1
     */
    private String gsshPaymentNo1;

    /**
     * 支付金额1
     */
    private BigDecimal gsshPaymentAmt1;

    /**
     * 退货原销售单号
     */
    private String gsshBillNoReturn;

    /**
     * 退货审核人员
     */
    private String gsshEmpReturn;

    /**
     * 参加促销活动类型1
     */
    private String gsshPromotionType1;

    /**
     * 参加促销活动类型2
     */
    private String gsshPromotionType2;


    /**
     * 参加促销活动类型3
     */
    private String gsshPromotionType3;

    /**
     * 参加促销活动类型4
     */
    private String gsshPromotionType4;

    /**
     * 参加促销活动类型5
     */
    private String gsshPromotionType5;

    /**
     * 手机销售单号
     */
    private String gsshRegisterVoucherId;

    /**
     * 代销售店号
     */
    private String gsshReplaceBrId;

    /**
     * 代销售营业员
     */
    private String gsshReplaceSalerId;

    /**
     * 是否挂起 1挂起 0不挂起
     */
    private String gsshHideFlag;

    /**
     * 是否允许调出销售 1为是，0为否
     */
    private String gsshCallAllow;

    /**
     * 移动销售码
     */
    private String gsshEmpGroupName;

    /**
     * 调用次数
     */
    private String gsshCallQty;

    /**
     * 支付方式2
     */
    private String gsshPaymentNo2;

    /**
     * 支付金额2
     */
    private BigDecimal gsshPaymentAmt2;

    /**
     * 支付方式3
     */
    private String gsshPaymentNo3;

    /**
     * 支付金额3
     */
    private BigDecimal gsshPaymentAmt3;

    /**
     * 支付方式4
     */
    private String gsshPaymentNo4;

    /**
     * 支付金额4
     */
    private BigDecimal gsshPaymentAmt4;

    /**
     * 支付方式5
     */
    private String gsshPaymentNo5;

    /**
     * 支付金额5
     */
    private BigDecimal gsshPaymentAmt5;

    /**
     * 电子券抵用卡号2
     */
    private String gsshDzqdyActno2;

    /**
     * 电子券抵用金额2
     */
    private BigDecimal gsshDzqdyAmt2;

    /**
     * 电子券抵用卡号3
     */
    private String gsshDzqdyActno3;

    /**
     * 电子券抵用金额3
     */
    private BigDecimal gsshDzqdyAmt3;

    /**
     * 电子券抵用卡号4
     */
    private String gsshDzqdyActno4;

    /**
     * 电子券抵用金额4
     */
    private BigDecimal gsshDzqdyAmt4;

    /**
     * 电子券抵用卡号5
     */
    private String gsshDzqdyActno5;

    /**
     * 电子券抵用金额5
     */
    private BigDecimal gsshDzqdyAmt5;

    /**
     * 零售价格
     */
    private BigDecimal gsshNormalAmt;

    /**
     * 是否日结 0/空为否1为是
     */
    private String gsshDayreport;

    /**
     * 记录代煎供应商编码 是否有值判断是否中药代煎
     */
    private String gsshCrFlag;

    /**
     * 代煎状态 0未开方，1已完成，2已退货
     */
    private String gsshCrStatus;

    /**
     * 订单退货状态：0：已退货，1：医保退货, 2:医保退货失败
     */
    private String gsshReturnStatus;
    /**
     * 退货数
     */
    private String returnQty;

    /**
     * 所属订单号
     */
    private String orderNo;
    /**
     * 库存批次表id
     */
    private String batchTableId;

    private BigDecimal originalPrice;//零售价*数量

    /**
     * 会员价
     */
    private String prcHy;
    /**
     * 门诊单流水号
     */
    private String clinicalSheetSn;

}
