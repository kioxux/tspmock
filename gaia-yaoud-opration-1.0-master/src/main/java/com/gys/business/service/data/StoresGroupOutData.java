package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 门店店型 出参
 *
 * @author XiaoZY on 2021/05/28
 */
@Data
public class StoresGroupOutData implements Serializable {


    @ApiModelProperty(value = "加盟商编码")
    private String clientId;
    @ApiModelProperty(value = "店型Id")
    private String gssgId;
    @ApiModelProperty(value = "店型名称")
    private String gssgIdName;
    /**
     * 店效级别id
     */
    private String effectId;
    /**
     * 店效级别名称
     */
    private String effectName;
    @ApiModelProperty(value = "门店code")
    private String stoCode;
    @ApiModelProperty(value = "门店名称")
    private String stoName;
    @ApiModelProperty(value = "门店属性")
    private String stoAttribute;

}
