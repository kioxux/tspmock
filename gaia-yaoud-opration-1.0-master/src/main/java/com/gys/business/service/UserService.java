//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetUserBaseInData;
import com.gys.business.service.data.GetUserOutData;

public interface UserService {
    GetUserOutData selectByAccount(GetUserBaseInData inData);
}
