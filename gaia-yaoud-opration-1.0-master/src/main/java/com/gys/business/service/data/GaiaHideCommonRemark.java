package com.gys.business.service.data;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

/**
 * @Author ：gyx
 * @Date ：Created in 17:14 2022/2/9
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GaiaHideCommonRemark {


    private String id;

    private String client;

    private String stoCode;

    private String content;

    private String deleteFlag;

    private String createTime;
}
