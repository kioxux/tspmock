package com.gys.business.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SelectDataEchoResponse {

    @ApiModelProperty(value = "疾病大类编码")
    private String largeSicknessCoding;

    @ApiModelProperty(value = "疾病大类名称")
    private String largeSicknessName;

    @ApiModelProperty(value = "疾病中类编码")
    private String middleSicknessCoding;

    @ApiModelProperty(value = "疾病中类名称")
    private String middleSicknessName;

    @ApiModelProperty(value = "成分细类编码")
    private String smallSicknessCoding;

    @ApiModelProperty(value = "成分细类名称")
    private String smallSicknessName;

}
