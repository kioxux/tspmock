//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaProductBasicMapper;
import com.gys.business.mapper.GaiaSdAllotMutualDMapper;
import com.gys.business.mapper.GaiaSdAllotMutualHMapper;
import com.gys.business.mapper.GaiaSdReturnDepotDMapper;
import com.gys.business.mapper.GaiaSdReturnDepotHMapper;
import com.gys.business.mapper.GaiaSdStockBatchMapper;
import com.gys.business.mapper.GaiaSdStockMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaSdAllotMutualD;
import com.gys.business.mapper.entity.GaiaSdAllotMutualH;
import com.gys.business.mapper.entity.GaiaSdReturnDepotD;
import com.gys.business.mapper.entity.GaiaSdReturnDepotH;
import com.gys.business.mapper.entity.GaiaSdStock;
import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.AcceptService;
import com.gys.business.service.AllotService;
import com.gys.business.service.ExamineService;
import com.gys.business.service.data.GetAcceptInData;
import com.gys.business.service.data.GetAllotDetailInData;
import com.gys.business.service.data.GetAllotDetailOutData;
import com.gys.business.service.data.GetAllotInData;
import com.gys.business.service.data.GetAllotOutData;
import com.gys.business.service.data.GetExamineInData;
import com.gys.business.service.data.GetProductInData;
import com.gys.business.service.data.GetQueryProVoOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

@Service
public class AllotServiceImpl implements AllotService {
    @Autowired
    private GaiaSdAllotMutualHMapper allotMutualHMapper;
    @Autowired
    private GaiaSdAllotMutualDMapper allotMutualDMapper;
    @Autowired
    private GaiaProductBasicMapper productBasicMapper;
    @Autowired
    private GaiaUserDataMapper userDataMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdReturnDepotHMapper returnDepotHMapper;
    @Autowired
    private GaiaSdReturnDepotDMapper returnDepotDMapper;
    @Autowired
    private GaiaSdStockMapper stockMapper;
    @Autowired
    private GaiaSdStockBatchMapper stockBatchMapper;
    @Autowired
    private AcceptService acceptService;
    @Autowired
    private ExamineService examineService;

    public AllotServiceImpl() {
    }

    public PageInfo<GetAllotOutData> selectList(GetAllotInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GetAllotOutData> outData = this.allotMutualHMapper.selectList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    public GetAllotOutData detail(GetAllotInData inData) {
        GetAllotOutData outData = new GetAllotOutData();
        Example example = new Example(GaiaSdAllotMutualH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsamhVoucherId", inData.getGsamhVoucherId()).andEqualTo("gsamhBrId", inData.getStoreCode());
        GaiaSdAllotMutualH allotMutualH = (GaiaSdAllotMutualH)this.allotMutualHMapper.selectOneByExample(example);
        outData.setGsamhVoucherId(allotMutualH.getGsamhVoucherId());
        Example exampleUser = new Example(GaiaUserData.class);
        exampleUser.createCriteria().andEqualTo("client", allotMutualH.getClientId()).andEqualTo("userId", allotMutualH.getGsamhEmp());
        GaiaUserData userData = (GaiaUserData)this.userDataMapper.selectOneByExample(exampleUser);
        outData.setGsamhEmp(userData.getUserNam());
        outData.setGsamhStatus(allotMutualH.getGsamhStatus());
        Example exampleStore = new Example(GaiaStoreData.class);
        exampleStore.createCriteria().andEqualTo("client", allotMutualH.getClientId()).andEqualTo("stoCode", allotMutualH.getGsamhFrom());
        GaiaStoreData storeData = (GaiaStoreData)this.storeDataMapper.selectOneByExample(exampleStore);
        outData.setGsamhFrom(storeData.getStoName());
        outData.setGsamhFromId(allotMutualH.getGsamhFrom());
        exampleStore = new Example(GaiaStoreData.class);
        exampleStore.createCriteria().andEqualTo("client", allotMutualH.getClientId()).andEqualTo("stoCode", allotMutualH.getGsamhTo());
        storeData = (GaiaStoreData)this.storeDataMapper.selectOneByExample(exampleStore);
        outData.setGsamhTo(storeData.getStoName());
        List<GetAllotDetailOutData> detailList = this.allotMutualDMapper.detailList(inData);
        outData.setAllotDetailOutDataList(detailList);
        return outData;
    }

    public List<GetAllotDetailOutData> detailList(GetAllotInData inData) {
        List<GetAllotDetailOutData> outData = this.allotMutualDMapper.detailList(inData);
        return outData;
    }

    @Transactional
    public void insert(GetAllotInData inData) {
        GaiaSdAllotMutualH allotMutualH = new GaiaSdAllotMutualH();
        allotMutualH.setClientId(inData.getClientId());
        String voucherId = this.allotMutualHMapper.selectNextVoucherId(inData.getClientId());
        allotMutualH.setGsamhVoucherId(voucherId);
        allotMutualH.setGsamhBrId(inData.getStoreCode());
        allotMutualH.setGsamhStatus("1");
        allotMutualH.setGsamhType("调剂单");
        allotMutualH.setGsamhDate(DateUtil.format(new Date(), "yyyyMMdd"));
        allotMutualH.setGsamhFrom(inData.getGsamhFrom());
        allotMutualH.setGsamhTo(inData.getGsamhTo());
        allotMutualH.setGsamhEmp(inData.getGsamhEmp());
        allotMutualH.setGsamhProcedure("1");
        int i = 1;
        BigDecimal totalAmt = BigDecimal.ZERO;
        BigDecimal totalQty = BigDecimal.ZERO;
        Iterator var7 = inData.getAllotDetailInDataList().iterator();

        while(var7.hasNext()) {
            GetAllotDetailInData poItem = (GetAllotDetailInData)var7.next();
            GaiaSdAllotMutualD allotMutualD = new GaiaSdAllotMutualD();
            allotMutualD.setClientId(inData.getClientId());
            allotMutualD.setGsamdVoucherId(voucherId);
            allotMutualD.setGsamdProId(poItem.getGsamdProId());
            allotMutualD.setGsamdSerial(String.valueOf(i++));
            allotMutualD.setGsamdDate(DateUtil.format(new Date(), "yyyyMMdd"));
            allotMutualD.setGsamdStockQty(poItem.getGsamdStockQty());
            allotMutualD.setGsamdRecipientQty(poItem.getGsamdRecipientQty());
            allotMutualD.setGsamdBatch(poItem.getGsamdBatch());
            allotMutualD.setGsamdBatchNo(poItem.getGsamdBatchNo());
            allotMutualD.setGsamdValidDate(poItem.getGsamdValidDate());
            allotMutualD.setGsamdStockStatus("1");
            if (ObjectUtil.isNotEmpty(poItem.getProPrice())) {
                totalAmt = totalAmt.add((new BigDecimal(poItem.getProPrice())).multiply(new BigDecimal(poItem.getGsamdRecipientQty())));
            }

            totalQty = totalQty.add(new BigDecimal(poItem.getGsamdRecipientQty()));
            this.allotMutualDMapper.insert(allotMutualD);
        }

        allotMutualH.setGsamhTotalAmt(totalAmt);
        allotMutualH.setGsamhTotalQty(totalQty.toString());
        this.allotMutualHMapper.insert(allotMutualH);
    }

    @Transactional
    public void approve(GetAllotInData inData) {
        Example example = new Example(GaiaSdAllotMutualH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsamhVoucherId", inData.getGsamhVoucherId()).andEqualTo("gsamhBrId", inData.getStoreCode());
        GaiaSdAllotMutualH allotMutualH = (GaiaSdAllotMutualH)this.allotMutualHMapper.selectOneByExample(example);
        Example exampleD = new Example(GaiaSdAllotMutualD.class);
        exampleD.createCriteria().andEqualTo("clientId", allotMutualH.getClientId()).andEqualTo("gsamdVoucherId", allotMutualH.getGsamhVoucherId()).andEqualTo("gsamdDate", allotMutualH.getGsamhDate());
        List<GaiaSdAllotMutualD> allotMutualDList = this.allotMutualDMapper.selectByExample(exampleD);
        allotMutualH.setGsamhStatus(inData.getGsamhStatus());
        if ("3".equals(inData.getGsamhStatus())) {
            allotMutualH.setGsamhEmp1(inData.getGsamhEmp1());
        }

        if ("4".equals(inData.getGsamhStatus())) {
            allotMutualH.setGsamhFinishDate(DateUtil.format(new Date(), "yyyyMMdd"));
            GaiaSdReturnDepotH depotH = new GaiaSdReturnDepotH();
            depotH.setClientId(inData.getClientId());
            String voucherId = this.returnDepotHMapper.selectNextVoucherId(inData.getClientId());
            depotH.setGsrdhVoucherId(voucherId);
            depotH.setGsrdhStatus("3");
            depotH.setGsrdhType("1");
            depotH.setGsrdhDate(DateUtil.format(new Date(), "yyyyMMdd"));
            depotH.setGsrdhFrom(allotMutualH.getGsamhFrom());
            depotH.setGsrdhTo(allotMutualH.getGsamhTo());
            depotH.setGsrdhEmp(allotMutualH.getGsamhEmp());
            depotH.setGsrdhProcedure("0");
            depotH.setGsrdhTotalAmt(allotMutualH.getGsamhTotalAmt());
            depotH.setGsrdhTotalQty(allotMutualH.getGsamhTotalQty());
            this.returnDepotHMapper.insert(depotH);
            int i = 1;
            Iterator var9 = allotMutualDList.iterator();

            while(var9.hasNext()) {
                GaiaSdAllotMutualD allotMutualD = (GaiaSdAllotMutualD)var9.next();
                GaiaSdReturnDepotD depotD = new GaiaSdReturnDepotD();
                depotD.setClientId(inData.getClientId());
                depotD.setGsrddVoucherId(voucherId);
                depotD.setGsrddProId(allotMutualD.getGsamdProId());
                depotD.setGsrddSerial(String.valueOf(i++));
                depotD.setGsrddDate(DateUtil.format(new Date(), "yyyyMMdd"));
                depotD.setGsrddRetdepQty(allotMutualD.getGsamdRecipientQty());
                depotD.setGsrddReviseQty(allotMutualD.getGsamdRecipientQty());
                depotD.setGsrddStartQty(allotMutualD.getGsamdRecipientQty());
                depotD.setGsrddStockQty(allotMutualD.getGsamdStockQty());
                depotD.setGsrddBatch(allotMutualD.getGsamdBatch());
                depotD.setGsrddBatchNo(allotMutualD.getGsamdBatchNo());
                depotD.setGsrddValidDate(allotMutualD.getGsamdValidDate());
                depotD.setGsrddStockStatus("1");
                this.returnDepotDMapper.insert(depotD);
            }

            allotMutualH.setGsamhInvoicesId(depotH.getGsrdhVoucherId());
            String psVoucherId = "1000000000";
            GetAcceptInData acceptInData = new GetAcceptInData();
            acceptInData.setClientId(inData.getClientId());
            acceptInData.setGsahPsVoucherId(psVoucherId);
            String acceptVoucherId = this.acceptService.save(acceptInData);
            GetExamineInData examineInData = new GetExamineInData();
            examineInData.setClientId(inData.getClientId());
            examineInData.setGsehVoucherAcceptId(acceptVoucherId);
            this.examineService.save(examineInData);
        }

        allotMutualH.setGsamhProcedure(inData.getGsamhStatus());
        Iterator var16 = allotMutualDList.iterator();

        while(var16.hasNext()) {
            GaiaSdAllotMutualD allotMutualD = (GaiaSdAllotMutualD)var16.next();
            allotMutualD.setGsamdStockStatus(inData.getGsamhStatus());
            this.allotMutualDMapper.updateByPrimaryKeySelective(allotMutualD);
            if ("4".equals(inData.getGsamhStatus())) {
                Example example1 = new Example(GaiaSdStock.class);
                example1.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", allotMutualH.getGsamhFrom()).andEqualTo("gssProId", allotMutualD.getGsamdProId());
                GaiaSdStock stockFrom = (GaiaSdStock)this.stockMapper.selectOneByExample(example1);
                if (ObjectUtil.isNotEmpty(stockFrom)) {
                    stockFrom.setGssQty((new BigDecimal(stockFrom.getGssQty())).subtract(new BigDecimal(allotMutualD.getGsamdRecipientQty())).toString());
                    this.stockMapper.updateByPrimaryKeySelective(stockFrom);
                }

                Example example2 = new Example(GaiaSdStock.class);
                example2.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", allotMutualH.getGsamhTo()).andEqualTo("gssProId", allotMutualD.getGsamdProId());
                GaiaSdStock stockTo = (GaiaSdStock)this.stockMapper.selectOneByExample(example2);
                if (ObjectUtil.isNotEmpty(stockTo)) {
                    stockTo.setGssQty((new BigDecimal(stockTo.getGssQty())).add(new BigDecimal(allotMutualD.getGsamdRecipientQty())).toString());
                    this.stockMapper.updateByPrimaryKeySelective(stockTo);
                }

                Example exampleBatch1 = new Example(GaiaSdStockBatch.class);
                exampleBatch1.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssbBrId", allotMutualH.getGsamhFrom()).andEqualTo("gssbProId", allotMutualD.getGsamdProId()).andEqualTo("gssbBatchNo", allotMutualD.getGsamdBatchNo()).andEqualTo("gssbBatch", allotMutualD.getGsamdBatch());
                GaiaSdStockBatch stockBatchFrom = (GaiaSdStockBatch)this.stockBatchMapper.selectOneByExample(exampleBatch1);
                if (ObjectUtil.isNotEmpty(stockBatchFrom)) {
                    stockBatchFrom.setGssbQty((new BigDecimal(stockBatchFrom.getGssbQty())).subtract(new BigDecimal(allotMutualD.getGsamdRecipientQty())).toString());
                    this.stockBatchMapper.updateByPrimaryKeySelective(stockBatchFrom);
                }

                Example exampleBatch2 = new Example(GaiaSdStockBatch.class);
                exampleBatch2.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssbBrId", allotMutualH.getGsamhTo()).andEqualTo("gssbProId", allotMutualD.getGsamdProId()).andEqualTo("gssbBatchNo", allotMutualD.getGsamdBatchNo()).andEqualTo("gssbBatch", allotMutualD.getGsamdBatch());
                GaiaSdStockBatch stockBatchTo = (GaiaSdStockBatch)this.stockBatchMapper.selectOneByExample(exampleBatch2);
                if (ObjectUtil.isNotEmpty(stockBatchTo)) {
                    stockBatchTo.setGssbQty((new BigDecimal(stockBatchTo.getGssbQty())).add(new BigDecimal(allotMutualD.getGsamdRecipientQty())).toString());
                    this.stockBatchMapper.updateByPrimaryKeySelective(stockBatchTo);
                }
            }
        }

        this.allotMutualHMapper.updateByPrimaryKeySelective(allotMutualH);
    }

    @Transactional
    public void update(GetAllotInData inData) {
        Example example = new Example(GaiaSdAllotMutualH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsamhVoucherId", inData.getGsamhVoucherId()).andEqualTo("gsamhBrId", inData.getStoreCode());
        GaiaSdAllotMutualH allotMutualH = (GaiaSdAllotMutualH)this.allotMutualHMapper.selectOneByExample(example);
        allotMutualH.setGsamhFrom(inData.getGsamhFrom());
        allotMutualH.setGsamhProcedure(inData.getGsamhStatus());
        Example exampleD = new Example(GaiaSdAllotMutualD.class);
        exampleD.createCriteria().andEqualTo("clientId", allotMutualH.getClientId()).andEqualTo("gsamdVoucherId", allotMutualH.getGsamhVoucherId()).andEqualTo("gsamdDate", allotMutualH.getGsamhDate());
        this.allotMutualDMapper.deleteByExample(exampleD);
        int i = 1;
        BigDecimal totalAmt = BigDecimal.ZERO;
        BigDecimal totalQty = BigDecimal.ZERO;
        Iterator var8 = inData.getAllotDetailInDataList().iterator();

        while(var8.hasNext()) {
            GetAllotDetailInData detailInData = (GetAllotDetailInData)var8.next();
            GaiaSdAllotMutualD allotMutualD = new GaiaSdAllotMutualD();
            allotMutualD.setClientId(inData.getClientId());
            allotMutualD.setGsamdVoucherId(allotMutualH.getGsamhVoucherId());
            allotMutualD.setGsamdProId(detailInData.getGsamdProId());
            allotMutualD.setGsamdSerial(String.valueOf(i++));
            allotMutualD.setGsamdDate(DateUtil.format(new Date(), "yyyyMMdd"));
            allotMutualD.setGsamdStockQty(detailInData.getGsamdStockQty());
            allotMutualD.setGsamdRecipientQty(detailInData.getGsamdRecipientQty());
            allotMutualD.setGsamdBatch(detailInData.getGsamdBatch());
            allotMutualD.setGsamdBatchNo(detailInData.getGsamdBatchNo());
            allotMutualD.setGsamdValidDate(detailInData.getGsamdValidDate());
            allotMutualD.setGsamdStockStatus("1");
            if (ObjectUtil.isNotEmpty(detailInData.getProPrice())) {
                totalAmt = totalAmt.add((new BigDecimal(detailInData.getProPrice())).multiply(new BigDecimal(detailInData.getGsamdRecipientQty())));
            }

            totalQty = totalQty.add(new BigDecimal(detailInData.getGsamdRecipientQty()));
            this.allotMutualDMapper.insert(allotMutualD);
        }

        this.allotMutualHMapper.updateByPrimaryKeySelective(allotMutualH);
    }

    public List<GetQueryProVoOutData> queryPro(GetProductInData inData) {
        List<GetQueryProVoOutData> outData = this.productBasicMapper.queryProFromBatch(inData);
        return outData;
    }

    public List<GetQueryProVoOutData> exportIn(GetAllotInData inData) {
        List<GetQueryProVoOutData> outData = new ArrayList();
        Iterator var3 = inData.getAllotDetailInDataList().iterator();

        while(var3.hasNext()) {
            GetAllotDetailInData detailInData = (GetAllotDetailInData)var3.next();
            GetProductInData productInData = new GetProductInData();
            productInData.setClientId(inData.getClientId());
            productInData.setGspgProId(detailInData.getGsamdProId());
            productInData.setStoreCode(inData.getStoreCode());
            productInData.setBatch(detailInData.getGsamdBatch());
            productInData.setBatchNo(detailInData.getGsamdBatchNo());
            List<GetQueryProVoOutData> list = this.productBasicMapper.queryProFromBatch(productInData);
            if (!ObjectUtil.isNotEmpty(list)) {
                throw new BusinessException("商品编码:" + detailInData.getGsamdProId() + "，批次:" + detailInData.getGsamdBatch() + "，批号:" + detailInData.getGsamdBatchNo() + "的商品不存在！");
            }

            ((GetQueryProVoOutData)list.get(0)).setAllotQty(detailInData.getGsamdRecipientQty());
            outData.addAll(list);
        }

        return outData;
    }
}
