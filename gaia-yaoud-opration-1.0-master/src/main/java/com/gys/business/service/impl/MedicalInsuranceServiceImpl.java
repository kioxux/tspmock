package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdStockBatchMapper;
import com.gys.business.mapper.GaiaSdStockMapper;
import com.gys.business.mapper.MedicalInsuranceMapper;
import com.gys.business.mapper.entity.GaiaSdBatchChange;
import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.service.MedicalInsuranceService;
import com.gys.business.service.data.GetSaleReturnToData;
import com.gys.business.service.data.RebornData;
import com.gys.business.service.data.SalesInquireData;
import com.gys.business.service.data.StockChangeInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MedicalInsuranceServiceImpl implements MedicalInsuranceService {
    @Resource
    MedicalInsuranceMapper medicalInsuranceMapper;

    @Resource
    private GaiaSdStockMapper stockMapper;

    @Resource
    private GaiaSdStockBatchMapper stockBatchMapper;

    /**
     * 医保库存编码匹配
     * @param client
     * @param brId
     * @param inData
     * @return
     */
    @Override
    public List<RebornData> selectMedProList(String client, String brId, List<RebornData> inData) {
        //易联众
        Map<String,List<GaiaSdStockBatch>> currentBatchStock = new HashMap<>();
        List<RebornData> resListData =new ArrayList<>();
        //查询出所有的商品批次库存
        for(RebornData item : inData){
            if(!currentBatchStock.containsKey(item.getGssdProId())){
                Example example = new Example(GaiaSdStockBatch.class);
                example.createCriteria()
                        .andEqualTo("clientId",client)
                        .andEqualTo("gssbBrId",brId)
                        .andEqualTo("gssbProId",item.getGssdProId())
                        .andEqualTo("gssbBatchNo",item.getGssdBatchNo())
                        .andGreaterThan("gssbQty", "0");
                example.setOrderByClause("GSSB_BATCH ASC");
                List<GaiaSdStockBatch> temp = stockBatchMapper.selectByExample(example);
                currentBatchStock.put(item.getGssdProId(),temp);
            }
        }

        //计算出批号数量的批次分摊
        for(RebornData item : inData){
            List<GaiaSdStockBatch> tempBatchStock= currentBatchStock.get(item.getGssdProId());
            BigDecimal cacheQty =new BigDecimal(item.getGssdQty());
            for(GaiaSdStockBatch batchItem : tempBatchStock){
                if(cacheQty.compareTo(BigDecimal.ZERO)>0){
                    if(new BigDecimal(batchItem.getGssbQty()).compareTo(BigDecimal.ZERO)>0){
                        if(cacheQty.compareTo(new BigDecimal(batchItem.getGssbQty()))>0){ //需求数大于批次库存
                            RebornData temp = new RebornData();
                            BeanUtils.copyProperties(item,temp);
                            temp.setGssdBatch(batchItem.getGssbBatch());
                            temp.setGssdQty(batchItem.getGssbQty());
                            resListData.add(temp);
                            cacheQty = cacheQty.subtract(new BigDecimal(batchItem.getGssbQty())); //需求量减去库存数
                        }else{//需求数小于等于于批次库存
                            batchItem.setGssbQty(new BigDecimal(batchItem.getGssbQty()).subtract(cacheQty).toString()); //库存数减去需求量
                            RebornData temp = new RebornData();
                            BeanUtils.copyProperties(item,temp);
                            temp.setGssdBatch(batchItem.getGssbBatch());
                            temp.setGssdQty(cacheQty.toString());
                            resListData.add(temp);
                            break;
                        }
                    }
                }else{
                    break;
                }
            }
        }

        resListData.forEach( item -> {
            RebornData  res = medicalInsuranceMapper.selectMedProList(client,brId,item);
            if(!ObjectUtils.isEmpty(res)){
                item.setProCommonname(res.getProCommonname());
                item.setProForm(res.getProForm());
                item.setProMedProdct(res.getProMedProdct());
                item.setProMedProdctcode(res.getProMedProdctcode());
                item.setProIfMed(res.getProIfMed());
                item.setStockCode(res.getStockCode());
                item.setYbProCode(res.getYbProCode());
            }
        });
        return resListData;
    }

    /**
     * 医保编码匹配
     * @param client
     * @param brId
     * @param inData
     * @return
     */
    @Override
    public List<RebornData> selectMedProList2(String client, String brId, List<RebornData> inData) {
        List<RebornData> outData = new ArrayList<>();
        //东软
        for(RebornData item : inData){
            RebornData  res = medicalInsuranceMapper.selectMedProList2(client,brId,item);
            if(!ObjectUtils.isEmpty(res)){
                if("1".equals(res.getProIfMed())&&StringUtils.isNotBlank(res.getYbProCode())){
                    item.setProCommonname(res.getProCommonname());
                    item.setProForm(res.getProForm());
                    item.setProMedProdct(res.getProMedProdct());
                    item.setProMedProdctcode(res.getProMedProdctcode());
                    item.setProIfMed(res.getProIfMed());
                    item.setStockCode(res.getStockCode());
                    item.setYbProCode(res.getYbProCode());
                    outData.add(item);
                }
            }
        }
        return outData;
    }

    @Override
    public List<RebornData> matchMedProList(String client, String brId, List<GaiaSdBatchChange> inData) {
        //易联众
        List<RebornData> resListData = medicalInsuranceMapper.matchMedProList(client,brId,inData);
        return resListData;
    }

    @Override
    public List<RebornData> matchReturnProBatchList(String client, String brId, SalesInquireData inData) {
        //筛选退货数量大于0的
        List<GetSaleReturnToData> saleReturnToDataList = inData.getReturnToData().stream().filter(item->StringUtils.isNotEmpty(item.getGssdReturnQty())).collect(Collectors.toList());
        //查询退货的批次异动表
        List<StockChangeInfo> stockChangeInfos = medicalInsuranceMapper.selectReturnBatchChange(client,brId,inData.getBillNo(),saleReturnToDataList);
        List<GaiaSdBatchChange> stockList = new ArrayList<>();
        //按效期的倒序和批次的倒序排序
        saleReturnToDataList.forEach(item -> {
            List<StockChangeInfo> tempList = stockChangeInfos.stream()
                    .filter(batchChangeItem->batchChangeItem.getGsbcSerial().equals(item.getGssdSerial()))
                    .sorted(Comparator.comparing(StockChangeInfo::getGssbValidDate).reversed()
                            .thenComparing(StockChangeInfo::getGsbcBatch).reversed())
                    .collect(Collectors.toList());
            BigDecimal returnQtyTemp = new BigDecimal(item.getGssdReturnQty());
            for(int i =0; i<tempList.size(); i++) {
                if(returnQtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                    StockChangeInfo data = tempList.get(i);
                    if(data.getGsbcQty().compareTo(returnQtyTemp) > -1) {
                        GaiaSdBatchChange batchChangeTemp = new GaiaSdBatchChange();
                        batchChangeTemp.setGsbcBatch(data.getGsbcBatch());
                        batchChangeTemp.setGsbcBatchNo(data.getGsbcBatchNo());
                        batchChangeTemp.setClient(data.getClient());
                        batchChangeTemp.setGsbcProId(data.getGsbcProId());
                        batchChangeTemp.setGsbcDate(data.getGsbcDate());
                        batchChangeTemp.setGsbcBrId(data.getGsbcBrId());
                        batchChangeTemp.setGsbcQty(returnQtyTemp);
                        batchChangeTemp.setGsbcSerial(data.getGsbcSerial());
                        batchChangeTemp.setGsbcVoucherId(data.getGsbcVoucherId());
                        stockList.add(batchChangeTemp);
                        continue;
                    } else {
                        GaiaSdBatchChange batchChangeTemp = new GaiaSdBatchChange();
                        batchChangeTemp.setGsbcBatch(data.getGsbcBatch());
                        batchChangeTemp.setGsbcBatchNo(data.getGsbcBatchNo());
                        batchChangeTemp.setClient(data.getClient());
                        batchChangeTemp.setGsbcProId(data.getGsbcProId());
                        batchChangeTemp.setGsbcDate(data.getGsbcDate());
                        batchChangeTemp.setGsbcBrId(data.getGsbcBrId());
                        batchChangeTemp.setGsbcQty(data.getGsbcQty());
                        batchChangeTemp.setGsbcSerial(data.getGsbcSerial());
                        batchChangeTemp.setGsbcVoucherId(data.getGsbcVoucherId());
                        stockList.add(batchChangeTemp);
                        returnQtyTemp = returnQtyTemp.subtract(data.getGsbcQty());
                    }
                } else continue;
            }
        });
        List<RebornData> resListData = medicalInsuranceMapper.matchMedProList(client,brId,stockList);
        return resListData;
    }
}
