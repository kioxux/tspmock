package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

@Data
public class SaleProxyReplenishmentsInputData {
    private String saleNo;
    private String supplier;
    private String client;
    private String storeCode;
    private String userId;
    private List<SaleProxyReplenishmentsDetailInputData> commodityList;
}
