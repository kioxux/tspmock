package com.gys.business.service.data.mib.mibEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：gyx
 * @Date ：Created in 14:02 2021/12/28
 * @Description：险种类别枚举
 * @Modified By：gyx
 * @Version:
 */
@Getter
@AllArgsConstructor
public enum InsuTypeEnum {
    ZGJBYLBX("310","职工基本医疗保险"),
    CXJMJBYLBX("390","城乡居民基本医疗保险"),
    GWYYLBZ("320","公务员医疗补助"),
    CXJMDBYLBX("392","城乡居民大病医疗保险"),
    DEYLFYBC("330","大额医疗费用补偿"),
    SYBX("510","生育保险"),
    LXRYYLBZ("340","离休人员医疗保障"),
    CQZHBX("410","长期照护保险");

    private String code;
    private String value;

    public static InsuTypeEnum getEnumByCode(String code){
        for (InsuTypeEnum insuTypeEnum : values()) {
            if (code.equals(insuTypeEnum.getCode())) {
                return insuTypeEnum;
            }
        }
        return null;
    }
}
