package com.gys.business.service.data.yaoshibang;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zhangdong
 * @date 2021/6/18 20:51
 */
@Data
public class SoItem {

    @ApiModelProperty(value = "商品编码", name = "soProCode")
    private String soProCode;
    @ApiModelProperty(value = "销售订单数量", name = "soQty")
    private BigDecimal soQty;
    @ApiModelProperty(value = "订单单位", name = "soUnit")
    private String soUnit;
    @ApiModelProperty(value = "销售订单单价", name = "soPrice")
    private BigDecimal soPrice;
    @ApiModelProperty(value = "订单行金额", name = "soLineAmt")
    private BigDecimal soLineAmt;
    @ApiModelProperty(value = "地点", name = "soSiteCode")
    private String soSiteCode;
    @ApiModelProperty(value = "库存地点", name = "soLocationCode")
    private String soLocationCode;
    @ApiModelProperty(value = "批次", name = "soBatch")
    private String soBatch;
    @ApiModelProperty(value = "税率", name = "soRate")
    private String soRate;
    @ApiModelProperty(value = "计划发货日期", name = "soDeliveryDate")
    private String soDeliveryDate;
    @ApiModelProperty(value = "订单行备注", name = "soLineRemark")
    private String soLineRemark;
    @ApiModelProperty(value = "生产批号", name = "soBatchNo")
    private String soBatchNo;
    @ApiModelProperty(value = "货位号", name = "gsrdHwh")
    private String gsrdHwh;
    @ApiModelProperty(value = "有效期至", name = "batExpiryDate")
    private String batExpiryDate;
    /**
     * 行号
     */
    private String gsrdSerial;

    /**
     * 补货单号
     */
    private String gsrdVoucherId;

}
