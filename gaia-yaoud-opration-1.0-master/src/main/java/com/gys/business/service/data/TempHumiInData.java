//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "温湿度信息")
public class TempHumiInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "单号")
    private String gsthVoucherId;
    @ApiModelProperty(value = "门店")
    private String gsthBrId;
    @ApiModelProperty(value = "店名")
    private String gsthBrName;
    @ApiModelProperty(value = "地点")
    private String gsthArea;
    @ApiModelProperty(value = "检查日期")
    private String gsthDate;
    @ApiModelProperty(value = "检查时间")
    private String gsthTime;
    @ApiModelProperty(value = "库内温度")
    private String gsthTemp;
    @ApiModelProperty(value = "库内湿度")
    private String gsthHumi;
    @ApiModelProperty(value = "调控措施")
    private String gsthCheckStep;
    @ApiModelProperty(value = "调控后温度")
    private String gsthCheckTemp;
    @ApiModelProperty(value = "调控后湿度")
    private String gsthCheckHumi;
    @ApiModelProperty(value = "修改人员")
    private String gsthUpdateEmp;
    @ApiModelProperty(value = "修改日期")
    private String gsthUpdateDate;
    @ApiModelProperty(value = "修改时间")
    private String gsthUpdateTime;
    @ApiModelProperty(value = "审核状态")
    private String gsthStatus;
    @ApiModelProperty(value = "")
    private Integer pageNum;
    @ApiModelProperty(value = "")
    private Integer pageSize;

    @ApiModelProperty(value = "门店")
    private String[] brIdList;

}
