//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetMeituanInData;

public interface MeituanService {
    void orderPayedPush(String params);

    void orderReminderPush(String params);

    void orderCancelPush(String params);

    void orderRefundAllPush(String params);

    void orderRefundPush(String params);

    void deliveryStatusPush(String params);

    void orderStatusPush(String params);

    void orderGetOrderDetail(GetMeituanInData inData);

    void orderConfirm(GetMeituanInData inData);

    void orderCancel(GetMeituanInData inData);

    void orderPreparationMealComplete(GetMeituanInData inData);

    void orderRefundAgree(GetMeituanInData inData);

    void orderRefundReject(GetMeituanInData inData);

    void medicineStock(GetMeituanInData inData);
}
