package com.gys.business.service;

import com.gys.business.service.data.RechargeChangeInData;
import com.gys.business.service.data.RechargeChangeOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.response.Result;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author xiaoyuan
 */
public interface RechargeChangeService {

    /**
     * 储值卡充值消费明细分页查询
     * @param inData
     * @param userInfo
     * @return
     */
    List<RechargeChangeOutData> rechargeChangePage(RechargeChangeInData inData, GetLoginOutData userInfo);

    /**
     * 导出消费明细查询
     * @param inData
     * @param response
     * @param userInfo
     * @throws IOException
     */
    Result export(RechargeChangeInData inData, HttpServletResponse response, GetLoginOutData userInfo) ;
}
