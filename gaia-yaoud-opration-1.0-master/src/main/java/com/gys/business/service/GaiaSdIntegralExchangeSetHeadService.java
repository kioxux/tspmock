package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.service.data.*;
import com.gys.common.data.GaiaSdIntegralExchangeRecord;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.NewExchangeTotalNumBean;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
* @author RULAISZ
* @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_HEAD(门店积分换购商品设置主表)】的数据库操作Service
* @createDate 2021-11-29 09:40:14
*/
public interface GaiaSdIntegralExchangeSetHeadService {
    PageInfo<IntegralOutData> getIntegralExchangeList(IntegralInData inData);

    String setDetail(IntegralExchangeSetInData inData);
    JsonResult batchExcelImport(GetLoginOutData loginOutData, MultipartFile file);

    void downLoadTemplate(GetLoginOutData loginOutData, HttpServletResponse response);

    void updateDetail(IntegralExchangeSetInData inData);

    void audit(AuditIntegralExchangeInData inData);
    List<IntegralExchangeStore> getStoreList(IntegralInData inData);

    CopyIntegralExchangeOutData getActivityData(IntegralInData inData);

    List<ProductPriceInfo> getPrice(ProductPriceInData inData);

    void setEndTime(SetEndTimeIntegralExchangeInData inData);

    JsonResult addRecord(List<GaiaSdIntegralExchangeRecord> list);

    JsonResult exchangeTotalNum(NewExchangeTotalNumBean bean);

    JsonResult returnQty(String client, String stoCode, String saleBillNo, Map<String, BigDecimal> map);
}
