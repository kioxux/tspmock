package com.gys.business.service.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel
@CsvRow("储值卡金额异动")
public class RechargeChangeOutData implements Serializable {

    private static final long serialVersionUID = -5385068771666478622L;
    /**
     * 序号
     */
    @CsvCell(title = "序号", index = 1, fieldNo = 1)
    @ApiModelProperty(value = "序号")
    private Integer index;

    /**
     * 储值卡卡号
     */
    @CsvCell(title = "储值卡卡号", index = 2, fieldNo = 1)
    @ApiModelProperty(value = "储值卡卡号")
    private String gsrcId;

    /**
     * 会员卡号
     */
    @CsvCell(title = "会员卡号", index = 3, fieldNo = 1)
    @ApiModelProperty(value = "会员卡号")
    private String gsrcMemberCardId;

    /**
     * 姓名
     */
    @CsvCell(title = "姓名", index = 4, fieldNo = 1)
    @ApiModelProperty(value = "姓名")
    private String gsrcName;

    /**
     * 性别
     */
    @CsvCell(title = "性别", index = 5, fieldNo = 1)
    @ApiModelProperty(value = "性别")
    private String gsrcSex;

    /**
     * 手机
     */
    @CsvCell(title = "手机", index = 6, fieldNo = 1)
    @ApiModelProperty(value = "手机")
    private String gsrcMobile;

    /**
     * 日期
     */
    @CsvCell(title = "日期", index = 7, fieldNo = 1)
    @ApiModelProperty(value = "日期")
    private String gsrcDate;

    /**
     * 时间
     */
    @CsvCell(title = "时间", index = 8, fieldNo = 1)
    @ApiModelProperty(value = "时间")
    private String gsrcTime;

    /**
     * 门店编码
     */
    @CsvCell(title = "门店编码", index = 9, fieldNo = 1)
    @ApiModelProperty(value = "门店编码")
    private String gsrcBrId;

    /**
     * 门店名称
     */
    @CsvCell(title = "门店名称", index = 10, fieldNo = 1)
    @ApiModelProperty(value = "门店名称")
    private String stoShortName;

    /**
     * 操作人员
     */
    @CsvCell(title = "操作人员", index = 11, fieldNo = 1)
    @ApiModelProperty(value = "操作人员")
    private String gsrcEmp;

    /**
     * 异动单号
     */
    @Id
    @CsvCell(title = "单号", index = 12, fieldNo = 1)
    @ApiModelProperty(value = "单号")
    private String gsrcVoucherId;

    /**
     * 储值卡初始金额
     */
    @CsvCell(title = "储值卡初始金额", index = 13, fieldNo = 1)
    @ApiModelProperty(value = "储值卡初始金额")
    private BigDecimal gsrcInitialAmt;
    /**
     * 储值卡异动金额
     */
    @CsvCell(title = "储值卡异动金额", index = 14, fieldNo = 1)
    @ApiModelProperty(value = "储值卡异动金额")
    private BigDecimal gsrcChangeAmt;
    /**
     * 储值卡结果金额
     */
    @CsvCell(title = "储值卡结果金额", index = 15, fieldNo = 1)
    @ApiModelProperty(value = "储值卡结果金额")
    private BigDecimal gsrcResultAmt;

    /**
     * 异动类型 1：充值 2：退款 3：消费 4：退货
     */
    @CsvCell(title = "异动类型", index = 16, fieldNo = 1)
    @ApiModelProperty(value ="异动类型 1：充值 2：退款 3：消费 4：退货")
    private String gsrcType;
}
