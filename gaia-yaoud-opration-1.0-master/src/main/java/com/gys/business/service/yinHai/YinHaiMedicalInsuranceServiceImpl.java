package com.gys.business.service.yinHai;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.gys.business.mapper.YinHaiMedicalInsuranceMapper;
import com.gys.business.service.YinHaiMedicalInsuranceService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.yinHaiModel.*;
import com.gys.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.dynamic.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class YinHaiMedicalInsuranceServiceImpl implements YinHaiMedicalInsuranceService {

    @Autowired
    private YinHaiMedicalInsuranceMapper yinHaiMapper;


    @Override
    public JsonResult insertCancelStockInfo(GetLoginOutData inData) {
        //查询退供应商主单
        List<CancelStockInput> cancelStockInfoList = yinHaiMapper.getCancelSupplierStockInfo(inData);
        //查询退供应商明细
        List<CancelStockDetailInput> cancelStockDetailList = yinHaiMapper.getCancelSupplierStockDetail(inData);
        //将供应商主单与明细通过单号进行匹配 如果相同，则关联到同一笔中
        if(ValidateUtil.isNotEmpty(cancelStockInfoList)) {
            for(CancelStockInput cancelStock : cancelStockInfoList) {
                String returnOrderNo = cancelStock.getReturnOrderNo();
                List<CancelStockDetailInput> resultDetail = cancelStockDetailList.stream().filter(item -> returnOrderNo.equals(item.getReturnOrderNo()))
                        .collect(Collectors.toList());
                cancelStock.setDetail(resultDetail);
                cancelStock.setMethod("insertCancelStockInfo");
                cancelStock.setOrgid("机构编码-待定");
                cancelStock.setPorgid("企业编码-待定");
                cancelStock.setThStatus("1");
            }
        }

        //查询退库房主单
        List<CancelStockInput> cancelStorageInfoList = yinHaiMapper.getCancelStorageInfo(inData);
        //查询退库房明细
        List<CancelStockDetailInput> cancelStorageDetail = yinHaiMapper.getCancelStorageDetail(inData);
        //将退库房主单与明细通过单号进行匹配，如果相同，则关联到同一笔中
        for(CancelStockInput cancelStorageInfo : cancelStorageInfoList) {
            String returnOrderNo = cancelStorageInfo.getReturnOrderNo();
            List<CancelStockDetailInput> resultDetail = cancelStorageDetail.stream().filter(item -> returnOrderNo.equals(item.getReturnOrderNo()))
                    .collect(Collectors.toList());
            cancelStorageInfo.setDetail(resultDetail);
            cancelStorageInfo.setMethod("insertCancelStockInfo");
            cancelStorageInfo.setOrgid("机构编码-待定");
            cancelStorageInfo.setPorgid("企业编码-待定");
            cancelStorageInfo.setThStatus("1");
        }

        //推送数据逻辑 待开发

        return JsonResult.success(cancelStorageInfoList, "success");
    }


    @Override
    public JsonResult addOrder(GetLoginOutData inData) {
        //获取销售信息
        List<AddOrderInput> orderInfo = yinHaiMapper.getOrderInfo(inData);
        //获取销售明细信息
        List<AddOrderDetailInput> orderDetail = yinHaiMapper.getOrderDetail(inData);

        //将明细列表 转为Map  key为销售单号 value为相同单号的list
        Map<String, List<AddOrderDetailInput>> resultDetailMap = orderDetail.stream().collect(Collectors.groupingBy((item -> item.getBillNo())));
        if(ValidateUtil.isNotEmpty(orderInfo)) {
            for(AddOrderInput order : orderInfo) {
                //使用销售单号去明细中匹配 如果匹配到了 则表示有对应明细
                List<AddOrderDetailInput> resultDetail = resultDetailMap.get(order.getBillNo());
                order.setDetail(resultDetail);
                order.setMethod("addOrder");
                order.setOrgid("机构编码-待定");
                order.setPorgid("企业编码-待定");
                order.setSfPay("1");
            }
        }

        //数据推送逻辑 待开发

        return JsonResult.success(orderInfo, "success");
    }

    @Override
    public JsonResult insertInventoryManagerOrders(GetLoginOutData inData) {
        //查询损益单据信息
        List<InventoryManagerOrderInput> inventoryManagerOrder = yinHaiMapper.getInventoryManagerOrders(inData);

        //查询损益明细单据
        List<InventoryManagerOrderDetailInput> inventoryManagerOrderDetail = yinHaiMapper.getInventoryManagerOrderDetail(inData);
        //将明细数据按单号进行分组 key为单号 value为单号对应的list
        Map<String, List<InventoryManagerOrderDetailInput>> groupByOrderDetailMap = new HashMap(16);
        if(ValidateUtil.isNotEmpty(inventoryManagerOrderDetail)) {
            groupByOrderDetailMap = inventoryManagerOrderDetail.stream().collect(Collectors.groupingBy(item -> item.getVoucherId()));
        }

        List<InventoryManagerOrderInput> result = new ArrayList<>();
        //拆单 如果损益类型是盘点时，根据数据的正或负 进行拆单，负值为报损，正值为报溢
        for(InventoryManagerOrderInput managerOrder : inventoryManagerOrder) {
            //获取损益类型
            String imType = managerOrder.getImType();
            //使用单号从明细map中获取对应数据列表
            List<InventoryManagerOrderDetailInput> groupByDetail = groupByOrderDetailMap.get(managerOrder.getVoucherId());
            //当损益类型为盘点时  进行分单处理
            if("0".equals(imType)) {
                Map<String, List<InventoryManagerOrderDetailInput>> flagMap = groupByDetail.stream().collect(Collectors.groupingBy(item -> item.getFlag()));
                //向list添加一个或者两个对象
                //获取报损单
                List<InventoryManagerOrderDetailInput> lossList = flagMap.get("1");
                if(ValidateUtil.isNotEmpty(lossList)) {
                    //汇总报损金额
                    BigDecimal lossMoney = lossList.stream().map(InventoryManagerOrderDetailInput :: getMoney).reduce(BigDecimal.ZERO, BigDecimal :: add);
                    InventoryManagerOrderInput managerOrderInput = new InventoryManagerOrderInput();
                    BeanUtil.copyProperties(managerOrder, managerOrderInput);
                    managerOrderInput.setImType("1");
                    managerOrderInput.setImMoney(lossMoney);
                    managerOrderInput.setDetail(lossList);
                    result.add(managerOrderInput);
                }
                //获取报溢单
                List<InventoryManagerOrderDetailInput> profitList = flagMap.get("2");
                if(ValidateUtil.isNotEmpty(profitList)) {
                    //汇总报溢金额
                    BigDecimal profitMoney = profitList.stream().map(InventoryManagerOrderDetailInput :: getMoney).reduce(BigDecimal.ZERO, BigDecimal :: add);
                    InventoryManagerOrderInput managerOrderInput = new InventoryManagerOrderInput();
                    BeanUtil.copyProperties(managerOrder, managerOrderInput);
                    managerOrderInput.setImType("2");
                    managerOrderInput.setImMoney(profitMoney);
                    managerOrderInput.setDetail(profitList);
                    result.add(managerOrderInput);
                }
            } else {
                //当损益类型不为盘点时 直接赋值
                managerOrder.setDetail(groupByDetail);
                result.add(managerOrder);
            }
            managerOrder.setMethod("insertInventoryManagerOrders");
            managerOrder.setPorgid("企业编码-待定");
        }
        //数据推送逻辑 待开发

        return JsonResult.success(result, "success");
    }


    @Override
    public JsonResult getOrderModifyInfo(GetLoginOutData inData) {
        //查询采购修正数据
        OrderModifyInput orderModifyInfo = yinHaiMapper.getOrderModifyInfo(inData);
        orderModifyInfo.setMethod("orderModify");
        orderModifyInfo.setPorgid("企业编码-待定");
        orderModifyInfo.setOrgid("门店编码-待定");

        //数据推送逻辑 待开发

        return JsonResult.success(orderModifyInfo, "success");
    }

    /**
     * 查询单位信息
     * @param inData
     * @return
     */
    @Override
    public List<ClientInfoInput> listSendClientInfo(ClientInfoInput inData) {
        return yinHaiMapper.listSendClientInfo(inData);
    }

    /**
     * 查询商品信息
     * @param inData
     * @return
     */
    @Override
    public List<GoodsInfoInput> listSendGoodsInfo(GoodsInfoInput inData) {
        return yinHaiMapper.listSendGoodsInfo(inData);
    }

    /**
     * 查询入库信息
     * @param inData
     * @return
     */
    @Override
    public List<StockInfoInput> listSendStockDetailInfo(StockDetailInfoInput inData) {
        List<StockDetailInfoInput> stockDetailInfoInputs = yinHaiMapper.listSendStockDetailInfo(inData);
        Map<String, List<StockDetailInfoInput>> map = stockDetailInfoInputs.stream().collect(Collectors.groupingBy(StockDetailInfoInput::getShdh));
        List<StockInfoInput> stockInfoInputs = new ArrayList<>();
        map.forEach((key, entry)->{
            StockInfoInput stockInfoInput = new StockInfoInput();
            stockInfoInput.setShdh(entry.get(0).getShdh());
            stockInfoInput.setRkrq(entry.get(0).getRkrq());
            stockInfoInput.setWldwdm(entry.get(0).getWldwdm());
            stockInfoInput.setSupplier(entry.get(0).getSupplier());
            stockInfoInput.setJehj(entry.get(0).getJehjH());
            stockInfoInput.setAae011(entry.get(0).getAae011());
            stockInfoInput.setAae036(entry.get(0).getAae036());
            stockInfoInput.setShr(entry.get(0).getShr());
            stockInfoInput.setShsj(entry.get(0).getShsj());
            stockInfoInput.setShYsrOne(entry.get(0).getShYsrOne());
            stockInfoInput.setShYsdateOne(entry.get(0).getShYsdateOne());
            stockInfoInput.setShlx(entry.get(0).getShlx());
            stockInfoInput.setShStatus(entry.get(0).getShStatus());
            stockInfoInput.setDetails(entry);
            stockInfoInputs.add(stockInfoInput);
        });
        return stockInfoInputs;
    }

    /**
     * 插入对接方返回的单位编码
     * @param inData
     * @param userInfo
     * @return
     */
    @Override
    public Boolean insertSwapClientInfo(List<ClientInfoInput> inData, GetLoginOutData userInfo) {
        if (CollectionUtil.isNotEmpty(inData)){
            inData.forEach(item ->{

            });
            //insert
        }
        return true;
    }

    /**
     * 插入对接方返回的商品编码
     * @param inData
     * @param userInfo
     * @return
     */
    @Override
    public Boolean insertSwapGoodsInfo(List<GoodsInfoInput> inData, GetLoginOutData userInfo) {
        if (CollectionUtil.isNotEmpty(inData)){
            inData.forEach(item ->{

            });
            //insert

        }
        return true;
    }

    @Override
    public Boolean insertSwapStockInfo(List<StockDetailInfoInput> inData, GetLoginOutData userInfo) {
        return null;
    }

    @Override
    public List<TransforStockInfoInput> listSendTransforStockDetailInfo(TransforStockInfoInput inData, GetLoginOutData userInfo) {
        List<TransforStockDetailnput> list = yinHaiMapper.listSendTransforStockDetailInfo(inData);
        Map<String, List<TransforStockDetailnput>> map = list.stream().collect(Collectors.groupingBy(TransforStockDetailnput::getTgGivenNo));
        List<TransforStockInfoInput> stockInfoInputs = new ArrayList<>();
        map.forEach((key, entry)->{
            BigDecimal toMoney = entry.stream().map(TransforStockDetailnput::getTdTotal).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal tgAmount = entry.stream().map(TransforStockDetailnput::getTdAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
            TransforStockInfoInput stockInfoInput = new TransforStockInfoInput();
            stockInfoInput.setPorgid(entry.get(0).getPorgid());
            stockInfoInput.setTgGivenNo(entry.get(0).getTgGivenNo());
            stockInfoInput.setTgGivenFrom(entry.get(0).getTgGivenFrom());
            stockInfoInput.setTgGivenTo(entry.get(0).getTgGivenTo());
            stockInfoInput.setTgMoney(toMoney);
            stockInfoInput.setTgAmount(tgAmount);
            stockInfoInput.setAae011(entry.get(0).getAae011());
            stockInfoInput.setAae036(entry.get(0).getAae036());
            stockInfoInput.setTgOutCheck1(entry.get(0).getTgOutCheck1());
            stockInfoInput.setTgOutCheckTime1(entry.get(0).getTgOutCheckTime1());
            stockInfoInput.setTgInCheck1(entry.get(0).getTgInCheck1());
            stockInfoInput.setTgInCheckTime1(entry.get(0).getTgInCheckTime1());
            stockInfoInput.setDetails(entry);
            stockInfoInputs.add(stockInfoInput);
        });
        return stockInfoInputs;
    }
}
