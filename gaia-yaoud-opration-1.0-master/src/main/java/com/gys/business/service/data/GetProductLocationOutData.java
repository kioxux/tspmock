//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class GetProductLocationOutData {
    private String gsplProId;
    private String gsplProName;
    private String gsplArea;
    private String gsplGroup;
    private String gsplShelf;
    private String gsplStorey;
    private String gsplSeat;
    private String name;
    private List<GetProductLocationOutData> areaList;
    private List<GetProductLocationOutData> groupList;
    private List<GetProductLocationOutData> shelfList;
    private List<GetProductLocationOutData> storeyList;
    private List<GetProductLocationOutData> seatList;

    public GetProductLocationOutData() {
    }

    public String getGsplProId() {
        return this.gsplProId;
    }

    public String getGsplProName() {
        return this.gsplProName;
    }

    public String getGsplArea() {
        return this.gsplArea;
    }

    public String getGsplGroup() {
        return this.gsplGroup;
    }

    public String getGsplShelf() {
        return this.gsplShelf;
    }

    public String getGsplStorey() {
        return this.gsplStorey;
    }

    public String getGsplSeat() {
        return this.gsplSeat;
    }

    public String getName() {
        return this.name;
    }

    public List<GetProductLocationOutData> getAreaList() {
        return this.areaList;
    }

    public List<GetProductLocationOutData> getGroupList() {
        return this.groupList;
    }

    public List<GetProductLocationOutData> getShelfList() {
        return this.shelfList;
    }

    public List<GetProductLocationOutData> getStoreyList() {
        return this.storeyList;
    }

    public List<GetProductLocationOutData> getSeatList() {
        return this.seatList;
    }

    public void setGsplProId(final String gsplProId) {
        this.gsplProId = gsplProId;
    }

    public void setGsplProName(final String gsplProName) {
        this.gsplProName = gsplProName;
    }

    public void setGsplArea(final String gsplArea) {
        this.gsplArea = gsplArea;
    }

    public void setGsplGroup(final String gsplGroup) {
        this.gsplGroup = gsplGroup;
    }

    public void setGsplShelf(final String gsplShelf) {
        this.gsplShelf = gsplShelf;
    }

    public void setGsplStorey(final String gsplStorey) {
        this.gsplStorey = gsplStorey;
    }

    public void setGsplSeat(final String gsplSeat) {
        this.gsplSeat = gsplSeat;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setAreaList(final List<GetProductLocationOutData> areaList) {
        this.areaList = areaList;
    }

    public void setGroupList(final List<GetProductLocationOutData> groupList) {
        this.groupList = groupList;
    }

    public void setShelfList(final List<GetProductLocationOutData> shelfList) {
        this.shelfList = shelfList;
    }

    public void setStoreyList(final List<GetProductLocationOutData> storeyList) {
        this.storeyList = storeyList;
    }

    public void setSeatList(final List<GetProductLocationOutData> seatList) {
        this.seatList = seatList;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetProductLocationOutData)) {
            return false;
        } else {
            GetProductLocationOutData other = (GetProductLocationOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label167: {
                    Object this$gsplProId = this.getGsplProId();
                    Object other$gsplProId = other.getGsplProId();
                    if (this$gsplProId == null) {
                        if (other$gsplProId == null) {
                            break label167;
                        }
                    } else if (this$gsplProId.equals(other$gsplProId)) {
                        break label167;
                    }

                    return false;
                }

                Object this$gsplProName = this.getGsplProName();
                Object other$gsplProName = other.getGsplProName();
                if (this$gsplProName == null) {
                    if (other$gsplProName != null) {
                        return false;
                    }
                } else if (!this$gsplProName.equals(other$gsplProName)) {
                    return false;
                }

                label153: {
                    Object this$gsplArea = this.getGsplArea();
                    Object other$gsplArea = other.getGsplArea();
                    if (this$gsplArea == null) {
                        if (other$gsplArea == null) {
                            break label153;
                        }
                    } else if (this$gsplArea.equals(other$gsplArea)) {
                        break label153;
                    }

                    return false;
                }

                Object this$gsplGroup = this.getGsplGroup();
                Object other$gsplGroup = other.getGsplGroup();
                if (this$gsplGroup == null) {
                    if (other$gsplGroup != null) {
                        return false;
                    }
                } else if (!this$gsplGroup.equals(other$gsplGroup)) {
                    return false;
                }

                label139: {
                    Object this$gsplShelf = this.getGsplShelf();
                    Object other$gsplShelf = other.getGsplShelf();
                    if (this$gsplShelf == null) {
                        if (other$gsplShelf == null) {
                            break label139;
                        }
                    } else if (this$gsplShelf.equals(other$gsplShelf)) {
                        break label139;
                    }

                    return false;
                }

                Object this$gsplStorey = this.getGsplStorey();
                Object other$gsplStorey = other.getGsplStorey();
                if (this$gsplStorey == null) {
                    if (other$gsplStorey != null) {
                        return false;
                    }
                } else if (!this$gsplStorey.equals(other$gsplStorey)) {
                    return false;
                }

                label125: {
                    Object this$gsplSeat = this.getGsplSeat();
                    Object other$gsplSeat = other.getGsplSeat();
                    if (this$gsplSeat == null) {
                        if (other$gsplSeat == null) {
                            break label125;
                        }
                    } else if (this$gsplSeat.equals(other$gsplSeat)) {
                        break label125;
                    }

                    return false;
                }

                label118: {
                    Object this$name = this.getName();
                    Object other$name = other.getName();
                    if (this$name == null) {
                        if (other$name == null) {
                            break label118;
                        }
                    } else if (this$name.equals(other$name)) {
                        break label118;
                    }

                    return false;
                }

                Object this$areaList = this.getAreaList();
                Object other$areaList = other.getAreaList();
                if (this$areaList == null) {
                    if (other$areaList != null) {
                        return false;
                    }
                } else if (!this$areaList.equals(other$areaList)) {
                    return false;
                }

                label104: {
                    Object this$groupList = this.getGroupList();
                    Object other$groupList = other.getGroupList();
                    if (this$groupList == null) {
                        if (other$groupList == null) {
                            break label104;
                        }
                    } else if (this$groupList.equals(other$groupList)) {
                        break label104;
                    }

                    return false;
                }

                label97: {
                    Object this$shelfList = this.getShelfList();
                    Object other$shelfList = other.getShelfList();
                    if (this$shelfList == null) {
                        if (other$shelfList == null) {
                            break label97;
                        }
                    } else if (this$shelfList.equals(other$shelfList)) {
                        break label97;
                    }

                    return false;
                }

                Object this$storeyList = this.getStoreyList();
                Object other$storeyList = other.getStoreyList();
                if (this$storeyList == null) {
                    if (other$storeyList != null) {
                        return false;
                    }
                } else if (!this$storeyList.equals(other$storeyList)) {
                    return false;
                }

                Object this$seatList = this.getSeatList();
                Object other$seatList = other.getSeatList();
                if (this$seatList == null) {
                    if (other$seatList != null) {
                        return false;
                    }
                } else if (!this$seatList.equals(other$seatList)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetProductLocationOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $gsplProId = this.getGsplProId();
        result = result * 59 + ($gsplProId == null ? 43 : $gsplProId.hashCode());
        Object $gsplProName = this.getGsplProName();
        result = result * 59 + ($gsplProName == null ? 43 : $gsplProName.hashCode());
        Object $gsplArea = this.getGsplArea();
        result = result * 59 + ($gsplArea == null ? 43 : $gsplArea.hashCode());
        Object $gsplGroup = this.getGsplGroup();
        result = result * 59 + ($gsplGroup == null ? 43 : $gsplGroup.hashCode());
        Object $gsplShelf = this.getGsplShelf();
        result = result * 59 + ($gsplShelf == null ? 43 : $gsplShelf.hashCode());
        Object $gsplStorey = this.getGsplStorey();
        result = result * 59 + ($gsplStorey == null ? 43 : $gsplStorey.hashCode());
        Object $gsplSeat = this.getGsplSeat();
        result = result * 59 + ($gsplSeat == null ? 43 : $gsplSeat.hashCode());
        Object $name = this.getName();
        result = result * 59 + ($name == null ? 43 : $name.hashCode());
        Object $areaList = this.getAreaList();
        result = result * 59 + ($areaList == null ? 43 : $areaList.hashCode());
        Object $groupList = this.getGroupList();
        result = result * 59 + ($groupList == null ? 43 : $groupList.hashCode());
        Object $shelfList = this.getShelfList();
        result = result * 59 + ($shelfList == null ? 43 : $shelfList.hashCode());
        Object $storeyList = this.getStoreyList();
        result = result * 59 + ($storeyList == null ? 43 : $storeyList.hashCode());
        Object $seatList = this.getSeatList();
        result = result * 59 + ($seatList == null ? 43 : $seatList.hashCode());
        return result;
    }

    public String toString() {
        return "GetProductLocationOutData(gsplProId=" + this.getGsplProId() + ", gsplProName=" + this.getGsplProName() + ", gsplArea=" + this.getGsplArea() + ", gsplGroup=" + this.getGsplGroup() + ", gsplShelf=" + this.getGsplShelf() + ", gsplStorey=" + this.getGsplStorey() + ", gsplSeat=" + this.getGsplSeat() + ", name=" + this.getName() + ", areaList=" + this.getAreaList() + ", groupList=" + this.getGroupList() + ", shelfList=" + this.getShelfList() + ", storeyList=" + this.getStoreyList() + ", seatList=" + this.getSeatList() + ")";
    }
}
