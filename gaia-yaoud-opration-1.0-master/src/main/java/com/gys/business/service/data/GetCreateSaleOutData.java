package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdSaleD;
import com.gys.business.mapper.entity.GaiaSdSaleH;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GetCreateSaleOutData implements Serializable {
    private static final long serialVersionUID = 2988012820978285417L;
    private GaiaSdSaleH gaiaSdSaleH;
    private List<GaiaSdSaleD> gaiaSdSaleDList;
    private List<List<SelectRecipelDrugsOutData>> selectDataIndexList;
    private List<GiftPromOutData> giftPromOutDataList;
    private String hideRemark;
}
