package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2021/11/30 14:31
 */
@Data
public class ProductPriceInData {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 商品编码
     */
    @NotBlank(message = "提示：请选择商品")
    private String proCode;

    /**
     * 门店编码
     */
    @NotEmpty(message = "提示：请选择门店")
    private String[] stoCode;
}
