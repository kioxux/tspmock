package com.gys.business.service.data.thirdPay;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel(value = "收银通撤单入参")
@Data
public class SytRevokeOrderInData {
    @ApiModelProperty(value = "加盟商编码")
    @NotNull
    private String client;
    @ApiModelProperty(value = "门店编码")
    @NotNull
    private String storeCode;
    @ApiModelProperty(value = "订单号（外部）")
    @NotNull
    private String outerNumber;
}
