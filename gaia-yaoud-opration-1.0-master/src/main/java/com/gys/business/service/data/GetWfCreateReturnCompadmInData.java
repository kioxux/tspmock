//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetWfCreateReturnCompadmInData {
    private String storeId = "";
    private String returnDate = "";
    private String returnOrder = "";
    private String proCode = "";
    private String proName = "";
    private String proSpecs = "";
    private String batch = "";
    private String batchCost = "";
    private String tax = "";
    private String wmaCost = "";
    private String wmaCostTax = "";
    private String applyQty = "";
    private String returnQty = "";
    private String acceptQty = "";
    private String rejectQty = "";
    private String sccj = "";

    public GetWfCreateReturnCompadmInData() {
    }

    public String getStoreId() {
        return this.storeId;
    }

    public String getReturnDate() {
        return this.returnDate;
    }

    public String getReturnOrder() {
        return this.returnOrder;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getBatch() {
        return this.batch;
    }

    public String getBatchCost() {
        return this.batchCost;
    }

    public String getTax() {
        return this.tax;
    }

    public String getWmaCost() {
        return this.wmaCost;
    }

    public String getWmaCostTax() {
        return this.wmaCostTax;
    }

    public String getApplyQty() {
        return this.applyQty;
    }

    public String getReturnQty() {
        return this.returnQty;
    }

    public String getAcceptQty() {
        return this.acceptQty;
    }

    public String getRejectQty() {
        return this.rejectQty;
    }

    public String getSccj() {
        return this.sccj;
    }

    public void setStoreId(final String storeId) {
        this.storeId = storeId;
    }

    public void setReturnDate(final String returnDate) {
        this.returnDate = returnDate;
    }

    public void setReturnOrder(final String returnOrder) {
        this.returnOrder = returnOrder;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setBatch(final String batch) {
        this.batch = batch;
    }

    public void setBatchCost(final String batchCost) {
        this.batchCost = batchCost;
    }

    public void setTax(final String tax) {
        this.tax = tax;
    }

    public void setWmaCost(final String wmaCost) {
        this.wmaCost = wmaCost;
    }

    public void setWmaCostTax(final String wmaCostTax) {
        this.wmaCostTax = wmaCostTax;
    }

    public void setApplyQty(final String applyQty) {
        this.applyQty = applyQty;
    }

    public void setReturnQty(final String returnQty) {
        this.returnQty = returnQty;
    }

    public void setAcceptQty(final String acceptQty) {
        this.acceptQty = acceptQty;
    }

    public void setRejectQty(final String rejectQty) {
        this.rejectQty = rejectQty;
    }

    public void setSccj(final String sccj) {
        this.sccj = sccj;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetWfCreateReturnCompadmInData)) {
            return false;
        } else {
            GetWfCreateReturnCompadmInData other = (GetWfCreateReturnCompadmInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label203: {
                    Object this$storeId = this.getStoreId();
                    Object other$storeId = other.getStoreId();
                    if (this$storeId == null) {
                        if (other$storeId == null) {
                            break label203;
                        }
                    } else if (this$storeId.equals(other$storeId)) {
                        break label203;
                    }

                    return false;
                }

                Object this$returnDate = this.getReturnDate();
                Object other$returnDate = other.getReturnDate();
                if (this$returnDate == null) {
                    if (other$returnDate != null) {
                        return false;
                    }
                } else if (!this$returnDate.equals(other$returnDate)) {
                    return false;
                }

                Object this$returnOrder = this.getReturnOrder();
                Object other$returnOrder = other.getReturnOrder();
                if (this$returnOrder == null) {
                    if (other$returnOrder != null) {
                        return false;
                    }
                } else if (!this$returnOrder.equals(other$returnOrder)) {
                    return false;
                }

                label182: {
                    Object this$proCode = this.getProCode();
                    Object other$proCode = other.getProCode();
                    if (this$proCode == null) {
                        if (other$proCode == null) {
                            break label182;
                        }
                    } else if (this$proCode.equals(other$proCode)) {
                        break label182;
                    }

                    return false;
                }

                label175: {
                    Object this$proName = this.getProName();
                    Object other$proName = other.getProName();
                    if (this$proName == null) {
                        if (other$proName == null) {
                            break label175;
                        }
                    } else if (this$proName.equals(other$proName)) {
                        break label175;
                    }

                    return false;
                }

                label168: {
                    Object this$proSpecs = this.getProSpecs();
                    Object other$proSpecs = other.getProSpecs();
                    if (this$proSpecs == null) {
                        if (other$proSpecs == null) {
                            break label168;
                        }
                    } else if (this$proSpecs.equals(other$proSpecs)) {
                        break label168;
                    }

                    return false;
                }

                Object this$batch = this.getBatch();
                Object other$batch = other.getBatch();
                if (this$batch == null) {
                    if (other$batch != null) {
                        return false;
                    }
                } else if (!this$batch.equals(other$batch)) {
                    return false;
                }

                label154: {
                    Object this$batchCost = this.getBatchCost();
                    Object other$batchCost = other.getBatchCost();
                    if (this$batchCost == null) {
                        if (other$batchCost == null) {
                            break label154;
                        }
                    } else if (this$batchCost.equals(other$batchCost)) {
                        break label154;
                    }

                    return false;
                }

                Object this$tax = this.getTax();
                Object other$tax = other.getTax();
                if (this$tax == null) {
                    if (other$tax != null) {
                        return false;
                    }
                } else if (!this$tax.equals(other$tax)) {
                    return false;
                }

                label140: {
                    Object this$wmaCost = this.getWmaCost();
                    Object other$wmaCost = other.getWmaCost();
                    if (this$wmaCost == null) {
                        if (other$wmaCost == null) {
                            break label140;
                        }
                    } else if (this$wmaCost.equals(other$wmaCost)) {
                        break label140;
                    }

                    return false;
                }

                Object this$wmaCostTax = this.getWmaCostTax();
                Object other$wmaCostTax = other.getWmaCostTax();
                if (this$wmaCostTax == null) {
                    if (other$wmaCostTax != null) {
                        return false;
                    }
                } else if (!this$wmaCostTax.equals(other$wmaCostTax)) {
                    return false;
                }

                Object this$applyQty = this.getApplyQty();
                Object other$applyQty = other.getApplyQty();
                if (this$applyQty == null) {
                    if (other$applyQty != null) {
                        return false;
                    }
                } else if (!this$applyQty.equals(other$applyQty)) {
                    return false;
                }

                label119: {
                    Object this$returnQty = this.getReturnQty();
                    Object other$returnQty = other.getReturnQty();
                    if (this$returnQty == null) {
                        if (other$returnQty == null) {
                            break label119;
                        }
                    } else if (this$returnQty.equals(other$returnQty)) {
                        break label119;
                    }

                    return false;
                }

                label112: {
                    Object this$acceptQty = this.getAcceptQty();
                    Object other$acceptQty = other.getAcceptQty();
                    if (this$acceptQty == null) {
                        if (other$acceptQty == null) {
                            break label112;
                        }
                    } else if (this$acceptQty.equals(other$acceptQty)) {
                        break label112;
                    }

                    return false;
                }

                Object this$rejectQty = this.getRejectQty();
                Object other$rejectQty = other.getRejectQty();
                if (this$rejectQty == null) {
                    if (other$rejectQty != null) {
                        return false;
                    }
                } else if (!this$rejectQty.equals(other$rejectQty)) {
                    return false;
                }

                Object this$sccj = this.getSccj();
                Object other$sccj = other.getSccj();
                if (this$sccj == null) {
                    if (other$sccj != null) {
                        return false;
                    }
                } else if (!this$sccj.equals(other$sccj)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetWfCreateReturnCompadmInData;
    }

    public int hashCode() {

        int result = 1;
        Object $storeId = this.getStoreId();
        result = result * 59 + ($storeId == null ? 43 : $storeId.hashCode());
        Object $returnDate = this.getReturnDate();
        result = result * 59 + ($returnDate == null ? 43 : $returnDate.hashCode());
        Object $returnOrder = this.getReturnOrder();
        result = result * 59 + ($returnOrder == null ? 43 : $returnOrder.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $batch = this.getBatch();
        result = result * 59 + ($batch == null ? 43 : $batch.hashCode());
        Object $batchCost = this.getBatchCost();
        result = result * 59 + ($batchCost == null ? 43 : $batchCost.hashCode());
        Object $tax = this.getTax();
        result = result * 59 + ($tax == null ? 43 : $tax.hashCode());
        Object $wmaCost = this.getWmaCost();
        result = result * 59 + ($wmaCost == null ? 43 : $wmaCost.hashCode());
        Object $wmaCostTax = this.getWmaCostTax();
        result = result * 59 + ($wmaCostTax == null ? 43 : $wmaCostTax.hashCode());
        Object $applyQty = this.getApplyQty();
        result = result * 59 + ($applyQty == null ? 43 : $applyQty.hashCode());
        Object $returnQty = this.getReturnQty();
        result = result * 59 + ($returnQty == null ? 43 : $returnQty.hashCode());
        Object $acceptQty = this.getAcceptQty();
        result = result * 59 + ($acceptQty == null ? 43 : $acceptQty.hashCode());
        Object $rejectQty = this.getRejectQty();
        result = result * 59 + ($rejectQty == null ? 43 : $rejectQty.hashCode());
        Object $sccj = this.getSccj();
        result = result * 59 + ($sccj == null ? 43 : $sccj.hashCode());
        return result;
    }

    public String toString() {
        return "GetWfCreateReturnCompadmInData(storeId=" + this.getStoreId() + ", returnDate=" + this.getReturnDate() + ", returnOrder=" + this.getReturnOrder() + ", proCode=" + this.getProCode() + ", proName=" + this.getProName() + ", proSpecs=" + this.getProSpecs() + ", batch=" + this.getBatch() + ", batchCost=" + this.getBatchCost() + ", tax=" + this.getTax() + ", wmaCost=" + this.getWmaCost() + ", wmaCostTax=" + this.getWmaCostTax() + ", applyQty=" + this.getApplyQty() + ", returnQty=" + this.getReturnQty() + ", acceptQty=" + this.getAcceptQty() + ", rejectQty=" + this.getRejectQty() + ", sccj=" + this.getSccj() + ")";
    }
}
