package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/10/28
 */
@Data
public class ElectronicSubjectQueryInData implements Serializable {
    private static final long serialVersionUID = -3846775654674486564L;

    private String client;

    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    @ApiModelProperty(value = "主题描述")
    private String gsetsName;

    @ApiModelProperty(value = "状态")
    private String gsebsStatus;

    @NotBlank(message = "创建起始日期不可为空")
    @ApiModelProperty(value = "创建起始日期")
    private String gsetsCreateStartDate;

    @NotBlank(message = "创建结束日期不可为空")
    @ApiModelProperty(value = "创建结束日期")
    private String gsetsCreateEndDate;
}
