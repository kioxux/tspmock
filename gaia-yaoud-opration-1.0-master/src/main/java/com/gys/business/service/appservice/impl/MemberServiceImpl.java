package com.gys.business.service.appservice.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.GaiaSdElectronChangeMapper;
import com.gys.business.mapper.GaiaSdMemberBasicMapper;
import com.gys.business.mapper.GaiaSdMemberCardDataMapper;
import com.gys.business.mapper.GaiaSdStoreDataMapper;
import com.gys.business.mapper.entity.GaiaDataSynchronized;
import com.gys.business.mapper.entity.GaiaSdMemberBasic;
import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import com.gys.business.mapper.entity.GaiaSdStoreData;
import com.gys.business.service.CacheService;
import com.gys.business.service.MemberAddOrEditService;
import com.gys.business.service.appservice.MemberService;
import com.gys.business.service.data.ElectronDetailOutData;
import com.gys.business.service.data.GetMemberInData;
import com.gys.business.service.data.GetQueryMemberInData;
import com.gys.business.service.data.GetQueryMemberOutData;
import com.gys.common.data.MemberAddDto;
import com.gys.common.exception.BusinessException;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import com.gys.util.CommonUtil;
import com.gys.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xiaoyuan on 2021/3/8
 */
@Service("appMemberServiceImpl")
public class MemberServiceImpl implements MemberService {
    private static final Logger log = LoggerFactory.getLogger(MemberServiceImpl.class);

    @Autowired
    private GaiaSdMemberBasicMapper gaiaSdMemberBasicMapper;

    @Autowired
    private GaiaSdMemberCardDataMapper gaiaSdMemberCardDataMapper;

    @Autowired
    private GaiaSdStoreDataMapper gaiaSdStoreDataMapper;

    @Autowired
    private RabbitTemplateHelper rabbitTemplate;

    @Autowired
    private CacheService synchronizedService;

    //电子券异动表
    @Autowired
    private GaiaSdElectronChangeMapper gaiaSdElectronChangeMapper;
    @Autowired
    private MemberAddOrEditService memberAddOrEditService;



    @Override
    public GetQueryMemberOutData queryMember(GetQueryMemberInData inData) {
        if (StrUtil.isBlank(inData.getPhoneOrCardId())) {
            throw new BusinessException("手机号或卡号不可为空!");
        }
        List<GetQueryMemberOutData> memberOutData = this.gaiaSdMemberBasicMapper.queryAppMember(inData);
        if (CollUtil.isEmpty(memberOutData)) {
            return null;
        }
        if (memberOutData.size() > 1) {
            throw new BusinessException("当前手机号存在多个会员,请仔细核查!");
        }
        GetQueryMemberOutData outData = memberOutData.get(0);
        GaiaSdMemberCardData param = new GaiaSdMemberCardData();
        param.setClient(inData.getClientId());
        param.setGsmbcBrId(inData.getBrId());
        param.setGsmbcCardId(outData.getCardNum());
        List<ElectronDetailOutData> electronDetailOut = this.gaiaSdElectronChangeMapper.queryCanUseElectron(param);
        List<ElectronDetailOutData> electronDetailOutData = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(electronDetailOut)) {
            Map<String, List<ElectronDetailOutData>> aa = electronDetailOut.stream().collect(Collectors.groupingBy(ElectronDetailOutData::getGsecId));

            aa.forEach((key, value) -> {
                electronDetailOutData.add(value.stream().max(Comparator.comparing(e -> e.getGsebsCreateDate() + e.getGsebsCreateTime())).get());
            });
        }
        if (CollUtil.isEmpty(electronDetailOutData)) {
            outData.setElNum("0");
        }else {
            outData.setElNum(String.valueOf(electronDetailOutData.size()));
        }

        return outData;
    }

    @Override
    public boolean addMember(GetMemberInData inData) {
        GetMemberInData getMemberInData = new GetMemberInData();
        getMemberInData.setClientId(inData.getClientId());
        getMemberInData.setBrId(inData.getBrId());
        getMemberInData.setGmbMobile(inData.getGmbMobile());
        if (StrUtil.isBlank(inData.getGmbMobile()) || !Util.checkPhone(inData.getGmbMobile())) {
            throw new BusinessException("手机号格式不正确!");
        }
        if (StrUtil.isNotBlank(inData.getGmbCredentials()) && !IdcardUtil.isValidCard(inData.getGmbCredentials())) {
            throw new BusinessException("身份证格式不正确!");
        }
        if (this.gaiaSdMemberBasicMapper.selectMembersByCondition(getMemberInData).size() > 0) {
            throw new BusinessException("提示：手机号已存在");
        }
        GaiaSdMemberBasic memberTemp = new GaiaSdMemberBasic();
        memberTemp.setClientId(inData.getClientId());
        memberTemp.setGsmbMobile(inData.getGmbMobile());
        GaiaSdMemberBasic member = this.gaiaSdMemberBasicMapper.selectOne(memberTemp);
        if (ObjectUtil.isEmpty(member))  {
            member = new GaiaSdMemberBasic();
            String randomUUID = IdUtil.randomUUID();
            member.setGsmbMemberId(randomUUID);
            member.setClientId(inData.getClientId());
            member.setGsmbMobile(inData.getGmbMobile());
            member.setGsmbName(inData.getGmbName());
            member.setGsmbAddress(inData.getGmbAddress());
            member.setGsmbTel(inData.getGmbTel());
            member.setGsmbSex(inData.getGmbSex());
            member.setGsmbCredentials(inData.getGmbCredentials());
            member.setGsmbAge(inData.getGmbAge());
            member.setGsmbBirth(DateUtil.format(DateUtil.parse(inData.getGmbBirth()), DatePattern.PURE_DATE_PATTERN));
            member.setGsmbBbName(inData.getGmbBbName());
            member.setGsmbBbSex(inData.getGmbBbSex());
            member.setGsmbBbAge(inData.getGmbBbAge());
            member.setGsmbContactAllowed("1");
            member.setGsmbUpdateBrId(inData.getBrId());
            member.setGsmbUpdateSaler(inData.getSaler());
            member.setGsmbUpdateDate(DateUtil.format(new Date(), "yyyyMMdd"));
            this.gaiaSdMemberBasicMapper.insertMember(member);

            Map<String, Object> map = new HashMap<>(16);
            map.put("CLIENT", inData.getClientId());
            map.put("GSMB_MEMBER_ID", randomUUID);
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(inData.getClientId());
            aSynchronized.setSite(inData.getBrId());
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_SD_MEMBER_BASIC");
            aSynchronized.setCommand("insert");
            aSynchronized.setSourceNo("会员新增");
            aSynchronized.setArg(JSON.toJSONString(map));
            this.synchronizedService.addOne(aSynchronized);
        }else {
            throw new BusinessException("该会员已存在,请勿重复注册!");
        }
        String cardId = CommonUtil.generateCardNumber(inData.getBrId());
        // 会员卡记录
        GaiaSdMemberCardData gaiaSdMemberCardData = new GaiaSdMemberCardData();
        gaiaSdMemberCardData.setClient(inData.getClientId());
        gaiaSdMemberCardData.setGsmbcBrId(inData.getBrId());
        gaiaSdMemberCardData.setGsmbcMemberId(member.getGsmbMemberId());
        gaiaSdMemberCardData.setGsmbcCardId(cardId);
        gaiaSdMemberCardData.setGsmbcClassId(inData.getGmbCardClass());
        gaiaSdMemberCardData.setGsmbcType("1");
        gaiaSdMemberCardData.setGsmbcChannel("0");
        gaiaSdMemberCardData.setGsmbcStatus("0");

        gaiaSdMemberCardData.setGsmbcIntegral("0");
        gaiaSdMemberCardData.setGsmbcCreateDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        gaiaSdMemberCardData.setGsmbcCreateSaler(inData.getSaler());
        gaiaSdMemberCardData.setGsmbcIntegralLastdate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));

        GaiaSdStoreData gaiaSdStoreData = new GaiaSdStoreData();
        gaiaSdStoreData.setClientId(inData.getClientId());
        gaiaSdStoreData.setGsstBrId(inData.getBrId());
        gaiaSdStoreData = this.gaiaSdStoreDataMapper.selectByPrimaryKey(gaiaSdStoreData);
        if (ObjectUtil.isNotEmpty(gaiaSdStoreData)) {
            gaiaSdMemberCardData.setGsmbcZeroDate(gaiaSdStoreData.getGsstJfQldate());
        }

        this.gaiaSdMemberCardDataMapper.insert(gaiaSdMemberCardData);

        Map<String, Object> map = new HashMap<>(16);
        map.put("CLIENT", inData.getClientId());
        map.put("GSMBC_MEMBER_ID", member.getGsmbMemberId());
        map.put("GSMBC_CARD_ID", inData.getGmbCardId());
        map.put("GSMBC_BR_ID", inData.getBrId());
        GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
        aSynchronized.setClient(inData.getClientId());
        aSynchronized.setSite(inData.getBrId());
        aSynchronized.setVersion(IdUtil.randomUUID());
        aSynchronized.setTableName("GAIA_SD_MEMBER_CARD");
        aSynchronized.setCommand("insert");
        aSynchronized.setSourceNo("会员新增");
        aSynchronized.setArg(JSON.toJSONString(map));
        this.synchronizedService.addOne(aSynchronized);

        /*MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("addMember");
        mqReceiveData.setData(inData.getGmbCardId());
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClientId() + "." + inData.getBrId(), JSON.toJSON(mqReceiveData));*/

        log.info("推送新增会员信息，移动收银 begin========");
        MemberAddDto dto = new MemberAddDto(member, gaiaSdMemberCardData);
        memberAddOrEditService.pushMemberInfo(inData.getClientId(), dto);
        return true;
    }
}
