//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class PromAssoResultOutData {
    private String clientId;
    private String gsparVoucherId;
    private String gsparSerial;
    private String gsparProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private String gsparSeriesId;
    private BigDecimal gsparGiftPrc;
    private String gsparGiftRebate;

    public PromAssoResultOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsparVoucherId() {
        return this.gsparVoucherId;
    }

    public String getGsparSerial() {
        return this.gsparSerial;
    }

    public String getGsparProId() {
        return this.gsparProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public String getGsparSeriesId() {
        return this.gsparSeriesId;
    }

    public BigDecimal getGsparGiftPrc() {
        return this.gsparGiftPrc;
    }

    public String getGsparGiftRebate() {
        return this.gsparGiftRebate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsparVoucherId(final String gsparVoucherId) {
        this.gsparVoucherId = gsparVoucherId;
    }

    public void setGsparSerial(final String gsparSerial) {
        this.gsparSerial = gsparSerial;
    }

    public void setGsparProId(final String gsparProId) {
        this.gsparProId = gsparProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setGsparSeriesId(final String gsparSeriesId) {
        this.gsparSeriesId = gsparSeriesId;
    }

    public void setGsparGiftPrc(final BigDecimal gsparGiftPrc) {
        this.gsparGiftPrc = gsparGiftPrc;
    }

    public void setGsparGiftRebate(final String gsparGiftRebate) {
        this.gsparGiftRebate = gsparGiftRebate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromAssoResultOutData)) {
            return false;
        } else {
            PromAssoResultOutData other = (PromAssoResultOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label143: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label143;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label143;
                    }

                    return false;
                }

                Object this$gsparVoucherId = this.getGsparVoucherId();
                Object other$gsparVoucherId = other.getGsparVoucherId();
                if (this$gsparVoucherId == null) {
                    if (other$gsparVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsparVoucherId.equals(other$gsparVoucherId)) {
                    return false;
                }

                Object this$gsparSerial = this.getGsparSerial();
                Object other$gsparSerial = other.getGsparSerial();
                if (this$gsparSerial == null) {
                    if (other$gsparSerial != null) {
                        return false;
                    }
                } else if (!this$gsparSerial.equals(other$gsparSerial)) {
                    return false;
                }

                label122: {
                    Object this$gsparProId = this.getGsparProId();
                    Object other$gsparProId = other.getGsparProId();
                    if (this$gsparProId == null) {
                        if (other$gsparProId == null) {
                            break label122;
                        }
                    } else if (this$gsparProId.equals(other$gsparProId)) {
                        break label122;
                    }

                    return false;
                }

                label115: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label115;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label115;
                    }

                    return false;
                }

                Object this$gspgSpecs = this.getGspgSpecs();
                Object other$gspgSpecs = other.getGspgSpecs();
                if (this$gspgSpecs == null) {
                    if (other$gspgSpecs != null) {
                        return false;
                    }
                } else if (!this$gspgSpecs.equals(other$gspgSpecs)) {
                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                label94: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label94;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label94;
                    }

                    return false;
                }

                label87: {
                    Object this$gsparSeriesId = this.getGsparSeriesId();
                    Object other$gsparSeriesId = other.getGsparSeriesId();
                    if (this$gsparSeriesId == null) {
                        if (other$gsparSeriesId == null) {
                            break label87;
                        }
                    } else if (this$gsparSeriesId.equals(other$gsparSeriesId)) {
                        break label87;
                    }

                    return false;
                }

                Object this$gsparGiftPrc = this.getGsparGiftPrc();
                Object other$gsparGiftPrc = other.getGsparGiftPrc();
                if (this$gsparGiftPrc == null) {
                    if (other$gsparGiftPrc != null) {
                        return false;
                    }
                } else if (!this$gsparGiftPrc.equals(other$gsparGiftPrc)) {
                    return false;
                }

                Object this$gsparGiftRebate = this.getGsparGiftRebate();
                Object other$gsparGiftRebate = other.getGsparGiftRebate();
                if (this$gsparGiftRebate == null) {
                    if (other$gsparGiftRebate != null) {
                        return false;
                    }
                } else if (!this$gsparGiftRebate.equals(other$gsparGiftRebate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromAssoResultOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsparVoucherId = this.getGsparVoucherId();
        result = result * 59 + ($gsparVoucherId == null ? 43 : $gsparVoucherId.hashCode());
        Object $gsparSerial = this.getGsparSerial();
        result = result * 59 + ($gsparSerial == null ? 43 : $gsparSerial.hashCode());
        Object $gsparProId = this.getGsparProId();
        result = result * 59 + ($gsparProId == null ? 43 : $gsparProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $gsparSeriesId = this.getGsparSeriesId();
        result = result * 59 + ($gsparSeriesId == null ? 43 : $gsparSeriesId.hashCode());
        Object $gsparGiftPrc = this.getGsparGiftPrc();
        result = result * 59 + ($gsparGiftPrc == null ? 43 : $gsparGiftPrc.hashCode());
        Object $gsparGiftRebate = this.getGsparGiftRebate();
        result = result * 59 + ($gsparGiftRebate == null ? 43 : $gsparGiftRebate.hashCode());
        return result;
    }

    public String toString() {
        return "PromAssoResultOutData(clientId=" + this.getClientId() + ", gsparVoucherId=" + this.getGsparVoucherId() + ", gsparSerial=" + this.getGsparSerial() + ", gsparProId=" + this.getGsparProId() + ", gspgProName=" + this.getGspgProName() + ", gspgSpecs=" + this.getGspgSpecs() + ", proPlace=" + this.getProPlace() + ", proPrice=" + this.getProPrice() + ", gsparSeriesId=" + this.getGsparSeriesId() + ", gsparGiftPrc=" + this.getGsparGiftPrc() + ", gsparGiftRebate=" + this.getGsparGiftRebate() + ")";
    }
}
