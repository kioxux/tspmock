package com.gys.business.service.data.takeaway;

import com.gys.business.service.data.RebornData;
import lombok.Data;

import java.util.List;

/**
 * @author ZhangDong
 * @date 2021/9/9 9:53
 */
@Data
public class FxYlOrderInputBean {

    private String orderId;//药联订单号
    private String saleOrderId;//销售单号
    private List<RebornData> orderDetail;

    private String client;
    private String stoCode;
}
