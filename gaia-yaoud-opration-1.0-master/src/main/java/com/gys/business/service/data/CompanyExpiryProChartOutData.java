package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CompanyExpiryProChartOutData implements Serializable {

    @ApiModelProperty(value = "门店号")
    private String brId;

    @ApiModelProperty(value = "门店名")
    private String brName;

    @ApiModelProperty(value = "类型 store-门店 dc-仓库")
    private String type;

    @ApiModelProperty(value = "效期品项数")
    private Integer validProCount;

    @ApiModelProperty(value = "效期成本额")
    private BigDecimal costAmt;
}
