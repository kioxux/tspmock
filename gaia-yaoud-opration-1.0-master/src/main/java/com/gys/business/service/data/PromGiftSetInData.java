package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromGiftSetInData implements Serializable {
    private static final long serialVersionUID = 5086046002359794866L;
    private String clientId;
    private String gspgsVoucherId;
    private String gspgsSerial;
    private String gspgsSeriesProId;
    private String gspgsReachQty1;
    private BigDecimal gspgsReachAmt1;
    private String gspgsResultQty1;
    private String gspgsReachQty2;
    private BigDecimal gspgsReachAmt2;
    private String gspgsResultQty2;
    private String gspgsReachQty3;
    private BigDecimal gspgsReachAmt3;
    private String gspgsResultQty3;
}
