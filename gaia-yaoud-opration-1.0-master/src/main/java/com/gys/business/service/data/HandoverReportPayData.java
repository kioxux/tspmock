package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2021/1/28
 */
@Data
public class HandoverReportPayData implements Serializable {

    private static final long serialVersionUID = 6815872791400658731L;

    /**
     * 销售单号
     */
    private String billNo;

    /**
     * 支付编码
     */
    private String gsspmId;

    /**
     * 支付,名称
     */
    private String gspmName;

    /**
     * 金额
     */
    private String paymentAmount;
}
