package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 新增主题传参
 *
 * @author xiaoyuan on 2020/9/27
 */
@Data
public class ThemeSettingVO implements Serializable {
    private static final long serialVersionUID = 231113474925761996L;

    private String client;
    /**
     * 创建人
     */
    private String gsetsCreateEmp;
    /**
     * 创建日期
     */
    private String gsetsCreateDate;
    /**
     * 创建时间
     */
    private String gsetsCreateTime;
    /**
     * 最大赠送次数
     */
    @Length(max = 10,message = "最大赠送次数最大长度50!")
    private String gsetsMaxQty;

    /**
     * 主题单号
     */
    private String gsetsId;

    @NotBlank(message = "主题属性不可为空")
    @ApiModelProperty(value = "主题属性 NM 新会员 / FM 需维系会员 / CM 消费型 ")
    private String gsetsFlag;

    @Length(max = 50,message = "主题描述最大长度50!")
    @NotBlank(message = "主题描述不可为空")
    @ApiModelProperty(value = "主题描述")
    private String gsetsName;


}
