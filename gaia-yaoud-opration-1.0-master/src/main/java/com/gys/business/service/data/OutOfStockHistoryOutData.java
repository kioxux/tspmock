package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class OutOfStockHistoryOutData implements Serializable {

    private static final long serialVersionUID = -3781934088499015328L;

    @ApiModelProperty(value = "补货日期")
    private String replenishDate;

    @ApiModelProperty(value = "补货品数量")
    private int totalCount;

    @ApiModelProperty(value = "补货品月均销售额")
    private BigDecimal totalAvgAmt;

    @ApiModelProperty(value = "补货品月均毛利额")
    private BigDecimal totalAvgProfitAmt;

    @ApiModelProperty(value = "补货品历史明细列表")
    private List<OutOfStockOutListData> detailList;

}
