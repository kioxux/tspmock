package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 选择电子券传参
 *
 * @author xiaoyuan on 2020/9/27
 */
@Data
public class ElectronAutoSetInVO implements Serializable {
    private static final long serialVersionUID = -3289462946510971088L;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 主题单号
     */
    @ApiModelProperty(value = "主题单号")
    private String gseasId;

    /**
     * 主题属性
     */
    @ApiModelProperty(value = "主题属性")
    private String gseasFlag;


    @ApiModelProperty(value = "面值")
    private String gsebAmt;

    /**
     * 电子券活动号
     */
    @ApiModelProperty(value = "电子券活动号")
    private String gsebId;

    /**
     * 发送数量
     */
    @ApiModelProperty(value = "发送数量")
    private String gseasQty;

    @ApiModelProperty(value = "主题描述")
    private String gsebName;

    /**
     * 起始时间
     */
    private String gseasBeginDate;

    /**
     * 结束时间
     */
    private String gseasEndDate;

    /**
     * 创建时间
     */
    private String gseasCreateDate;

    /**
     * 创建时间
     */
    private String gseasCreateTime;

    private String gseasValid;
}
