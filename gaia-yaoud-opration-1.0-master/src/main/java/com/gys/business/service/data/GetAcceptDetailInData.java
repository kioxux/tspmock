//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "补货明细")
public class GetAcceptDetailInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "收货单号")
    private String gsadVoucherId;
    @ApiModelProperty(value = "收货日期")
    private String gsadDate;
    @ApiModelProperty(value = "行号")
    private String gsadSerial;
    @ApiModelProperty(value = "商品编码")
    private String gsadProId;
    @ApiModelProperty(value = "批号")
    private String gsadBatchNo;
    @ApiModelProperty(value = "批次")
    private String gsadBatch;
    @ApiModelProperty(value = "生产日期")
    private String gsadMadeDate;
    @ApiModelProperty(value = "有效期")
    private String gsadValidDate;
    @ApiModelProperty(value = "单据数量")
    private String gsadInvoicesQty;
    @ApiModelProperty(value = "到货数量")
    private String gsadRecipientQty;
    @ApiModelProperty(value = "拒收数量")
    private String gsadRefuseQty;
    @ApiModelProperty(value = "箱号")
    private String gsadGsadPackNo;
    @ApiModelProperty(value = "库存状态")
    private String gsadStockStatus;
    @ApiModelProperty(value = "药品名称")
    private String proName;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "剂型")
    private String proForm;
    @ApiModelProperty(value = "价格")
    private String proPrice;
    @ApiModelProperty(value = "单位")
    private String proUnit;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;
}
