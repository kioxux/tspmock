package com.gys.business.service.yaomeng;

import com.gys.business.mapper.entity.GaiaSdSaleRecipel;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.yaomeng.GaiaPrescribeInfo;
import com.gys.common.data.yaomeng.RecipelInfoQueryBean;
import com.gys.common.data.yaomeng.RecipelQuestionInfoBean;
import com.gys.util.yaomeng.YaomengDrugDto;
import com.gys.util.yaomeng.YaomengSpeedSimpleBean;

import java.util.List;
import java.util.Map;

/**
 * @author ZhangDong
 * @date 2021/11/4 16:53
 */
public interface YaomengService {

    String isPrescribe(String client, String depId);

    JsonResult drugCheck(String client, String stoCode, List<String> drugs);

    JsonResult drugDisease(String client, String stoCode, List<String> drugs);

    JsonResult checkLimit(String client, String stoCode, List<String> erpNo, String idCard);

    JsonResult dialogueQuestion(String client, List<String> diseaseIds);

    JsonResult getDoctor(String client);

    JsonResult speedSimple(String client, String stoCode, YaomengSpeedSimpleBean bean);

    JsonResult getPrescriptionFileUrl(String no, String pharmacist);

    JsonResult submitRecipelQuestionInfo(GetLoginOutData userInfo, RecipelQuestionInfoBean bean);

    GaiaPrescribeInfo getPrescribeBySaleOrderIdLimit(String saleOrderId);

    JsonResult saveEleRecipelInfo(GetLoginOutData userInfo, List<GaiaSdSaleRecipel> list);

    JsonResult getEleRecipelInfoByGssrSaleBillNo(String client, Map<String, String> map);

    JsonResult recipelInfoQuery(RecipelInfoQueryBean bean);

    JsonResult recipelAgain(String client, String depId, String saleBillNo);
    /**
     * 根据商品编码查询 药盟编码
     * @param proSelfCodeList  药德商标自编码集合
     * @param client       加盟商
     * @param stoCode  门店编码
     * @return
     */
    List<YaomengDrugDto> listYMRelation(List<String> proSelfCodeList,String client ,String stoCode);

    JsonResult saveRecipelIdAndFileUrl(String saleBillNo, String recipelId, String fileUrl);

    JsonResult getRecipelProFromSaleD(String client, String depId, String saleBillNo);

    JsonResult saveVoucherIdAndPhone(List<GaiaSdSaleRecipel> list);

    JsonResult autoCheckRecipelInfo(GetLoginOutData userInfo, List<String> list);

    String downLoadAndUploadFile(String httpUrl);
}
