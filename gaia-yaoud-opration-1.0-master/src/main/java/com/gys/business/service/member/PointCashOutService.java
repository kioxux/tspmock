package com.gys.business.service.member;

import com.gys.common.data.JsonResult;
import com.gys.common.data.member.*;

/**
 * @author wu mao yin
 * @Title: 积分抵现
 * @date 2021/12/815:09
 */
public interface PointCashOutService {

    /**
     * 查询积分规则设置列表
     *
     * @param pointSettingSearchDTO pointSettingSearchDTO
     * @return JsonResult
     */
    JsonResult selectPointRuleSettingList(PointSettingSearchDTO pointSettingSearchDTO);

    /**
     * 新增修改积分规则设置
     *
     * @param pointSettingDTO pointSettingDTO
     * @return JsonResult
     */
    JsonResult saveOrUpdatePointRuleSetting(PointSettingDTO pointSettingDTO);

    /**
     * 删除积分规则设置
     *
     * @param client  client
     * @param planIds planIds
     * @return JsonResult
     */
    JsonResult deletePointRuleSetting(String client, String planIds);

    /**
     * 根据加盟商获取规则下拉列表
     *
     * @param client client
     * @return JsonResult
     */
    JsonResult selectPlanIdList(String client);

    /**
     * 新增修改积分抵现活动
     *
     * @param pointCashOutActivityDTO pointCashOutActivityDTO
     * @return JsonResult
     */
    JsonResult saveOrUpdateIntegralCashActivity(PointCashOutActivityDTO pointCashOutActivityDTO);

    /**
     * 修改结束日期
     *
     * @param client    加盟商
     * @param voucherId 单号
     * @param endDate   日期
     * @param endTime   时间
     * @return JsonResult
     */
    JsonResult updateEndDateTime(String client, String voucherId, String endDate, String endTime);

    /**
     * 审核活动
     *
     * @param client    加盟商
     * @param reviewer  审核人
     * @param voucherId 单号
     * @return JsonResult
     */
    JsonResult reviewActivity(String client, String reviewer, String voucherId);

    /**
     * 根据单号查看关联门店
     *
     * @param client    加盟商
     * @param voucherId 单号
     * @return JsonResult
     */
    JsonResult selectStoListByVoucherId(String client, String voucherId);

    /**
     * 根据单号查看关联商品
     *
     * @param pointProSearchDTO pointProSearchDTO
     * @return JsonResult
     */
    JsonResult selectProListByVoucherId(PointProSearchDTO pointProSearchDTO);

    /**
     * 查询积分抵现活动列表
     *
     * @param pointCashOutActivitySearchDTO pointCashOutActivitySearchDTO
     * @return JsonResult
     */
    JsonResult selectIntegralCashActivity(PointCashOutActivitySearchDTO pointCashOutActivitySearchDTO);

    /**
     * 删除积分抵现活动
     *
     * @param client    client
     * @param voucherId voucherId
     * @return JsonResult
     */
    JsonResult deleteIntegralCashActivity(String client, String voucherId);

}
