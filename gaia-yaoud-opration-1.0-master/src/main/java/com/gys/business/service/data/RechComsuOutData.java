//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class RechComsuOutData {
    private String clientId;
    private String cardNo;
    private String mobile;
    private String stoNo;
    private String stoName;
    private String queryType;
    private String name;
    private String date;
    private String time;
    private String emp;
    private String empName;
    private Integer index;
    private BigDecimal rechargeAmt;
    private BigDecimal consumeAmt;
    private String sex;

    public RechComsuOutData(String clientId, String cardNo, String mobile, String stoNo, String stoName, String queryType, String name, String date, String time, String emp, String empName, Integer index, BigDecimal rechargeAmt, BigDecimal consumeAmt, String sex) {
        this.clientId = clientId;
        this.cardNo = cardNo;
        this.mobile = mobile;
        this.stoNo = stoNo;
        this.stoName = stoName;
        this.queryType = queryType;
        this.name = name;
        this.date = date;
        this.time = time;
        this.emp = emp;
        this.empName = empName;
        this.index = index;
        this.rechargeAmt = rechargeAmt;
        this.consumeAmt = consumeAmt;
        this.sex = sex;
    }

    public RechComsuOutData() {
    }

    public static RechComsuOutData.RechComsuOutDataBuilder builder() {
        return new RechComsuOutData.RechComsuOutDataBuilder();
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getCardNo() {
        return this.cardNo;
    }

    public String getMobile() {
        return this.mobile;
    }

    public String getStoNo() {
        return this.stoNo;
    }

    public String getStoName() {
        return this.stoName;
    }

    public String getQueryType() {
        return this.queryType;
    }

    public String getName() {
        return this.name;
    }

    public String getDate() {
        return this.date;
    }

    public String getTime() {
        return this.time;
    }

    public String getEmp() {
        return this.emp;
    }

    public String getEmpName() {
        return this.empName;
    }

    public Integer getIndex() {
        return this.index;
    }

    public BigDecimal getRechargeAmt() {
        return this.rechargeAmt;
    }

    public BigDecimal getConsumeAmt() {
        return this.consumeAmt;
    }

    public String getSex() {
        return this.sex;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setCardNo(final String cardNo) {
        this.cardNo = cardNo;
    }

    public void setMobile(final String mobile) {
        this.mobile = mobile;
    }

    public void setStoNo(final String stoNo) {
        this.stoNo = stoNo;
    }

    public void setStoName(final String stoName) {
        this.stoName = stoName;
    }

    public void setQueryType(final String queryType) {
        this.queryType = queryType;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public void setTime(final String time) {
        this.time = time;
    }

    public void setEmp(final String emp) {
        this.emp = emp;
    }

    public void setEmpName(final String empName) {
        this.empName = empName;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public void setRechargeAmt(final BigDecimal rechargeAmt) {
        this.rechargeAmt = rechargeAmt;
    }

    public void setConsumeAmt(final BigDecimal consumeAmt) {
        this.consumeAmt = consumeAmt;
    }

    public void setSex(final String sex) {
        this.sex = sex;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RechComsuOutData)) {
            return false;
        } else {
            RechComsuOutData other = (RechComsuOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label191: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label191;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label191;
                    }

                    return false;
                }

                Object this$cardNo = this.getCardNo();
                Object other$cardNo = other.getCardNo();
                if (this$cardNo == null) {
                    if (other$cardNo != null) {
                        return false;
                    }
                } else if (!this$cardNo.equals(other$cardNo)) {
                    return false;
                }

                Object this$mobile = this.getMobile();
                Object other$mobile = other.getMobile();
                if (this$mobile == null) {
                    if (other$mobile != null) {
                        return false;
                    }
                } else if (!this$mobile.equals(other$mobile)) {
                    return false;
                }

                label170: {
                    Object this$stoNo = this.getStoNo();
                    Object other$stoNo = other.getStoNo();
                    if (this$stoNo == null) {
                        if (other$stoNo == null) {
                            break label170;
                        }
                    } else if (this$stoNo.equals(other$stoNo)) {
                        break label170;
                    }

                    return false;
                }

                label163: {
                    Object this$stoName = this.getStoName();
                    Object other$stoName = other.getStoName();
                    if (this$stoName == null) {
                        if (other$stoName == null) {
                            break label163;
                        }
                    } else if (this$stoName.equals(other$stoName)) {
                        break label163;
                    }

                    return false;
                }

                Object this$queryType = this.getQueryType();
                Object other$queryType = other.getQueryType();
                if (this$queryType == null) {
                    if (other$queryType != null) {
                        return false;
                    }
                } else if (!this$queryType.equals(other$queryType)) {
                    return false;
                }

                Object this$name = this.getName();
                Object other$name = other.getName();
                if (this$name == null) {
                    if (other$name != null) {
                        return false;
                    }
                } else if (!this$name.equals(other$name)) {
                    return false;
                }

                label142: {
                    Object this$date = this.getDate();
                    Object other$date = other.getDate();
                    if (this$date == null) {
                        if (other$date == null) {
                            break label142;
                        }
                    } else if (this$date.equals(other$date)) {
                        break label142;
                    }

                    return false;
                }

                label135: {
                    Object this$time = this.getTime();
                    Object other$time = other.getTime();
                    if (this$time == null) {
                        if (other$time == null) {
                            break label135;
                        }
                    } else if (this$time.equals(other$time)) {
                        break label135;
                    }

                    return false;
                }

                Object this$emp = this.getEmp();
                Object other$emp = other.getEmp();
                if (this$emp == null) {
                    if (other$emp != null) {
                        return false;
                    }
                } else if (!this$emp.equals(other$emp)) {
                    return false;
                }

                label121: {
                    Object this$empName = this.getEmpName();
                    Object other$empName = other.getEmpName();
                    if (this$empName == null) {
                        if (other$empName == null) {
                            break label121;
                        }
                    } else if (this$empName.equals(other$empName)) {
                        break label121;
                    }

                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                label107: {
                    Object this$rechargeAmt = this.getRechargeAmt();
                    Object other$rechargeAmt = other.getRechargeAmt();
                    if (this$rechargeAmt == null) {
                        if (other$rechargeAmt == null) {
                            break label107;
                        }
                    } else if (this$rechargeAmt.equals(other$rechargeAmt)) {
                        break label107;
                    }

                    return false;
                }

                Object this$consumeAmt = this.getConsumeAmt();
                Object other$consumeAmt = other.getConsumeAmt();
                if (this$consumeAmt == null) {
                    if (other$consumeAmt != null) {
                        return false;
                    }
                } else if (!this$consumeAmt.equals(other$consumeAmt)) {
                    return false;
                }

                Object this$sex = this.getSex();
                Object other$sex = other.getSex();
                if (this$sex == null) {
                    if (other$sex != null) {
                        return false;
                    }
                } else if (!this$sex.equals(other$sex)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RechComsuOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $cardNo = this.getCardNo();
        result = result * 59 + ($cardNo == null ? 43 : $cardNo.hashCode());
        Object $mobile = this.getMobile();
        result = result * 59 + ($mobile == null ? 43 : $mobile.hashCode());
        Object $stoNo = this.getStoNo();
        result = result * 59 + ($stoNo == null ? 43 : $stoNo.hashCode());
        Object $stoName = this.getStoName();
        result = result * 59 + ($stoName == null ? 43 : $stoName.hashCode());
        Object $queryType = this.getQueryType();
        result = result * 59 + ($queryType == null ? 43 : $queryType.hashCode());
        Object $name = this.getName();
        result = result * 59 + ($name == null ? 43 : $name.hashCode());
        Object $date = this.getDate();
        result = result * 59 + ($date == null ? 43 : $date.hashCode());
        Object $time = this.getTime();
        result = result * 59 + ($time == null ? 43 : $time.hashCode());
        Object $emp = this.getEmp();
        result = result * 59 + ($emp == null ? 43 : $emp.hashCode());
        Object $empName = this.getEmpName();
        result = result * 59 + ($empName == null ? 43 : $empName.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $rechargeAmt = this.getRechargeAmt();
        result = result * 59 + ($rechargeAmt == null ? 43 : $rechargeAmt.hashCode());
        Object $consumeAmt = this.getConsumeAmt();
        result = result * 59 + ($consumeAmt == null ? 43 : $consumeAmt.hashCode());
        Object $sex = this.getSex();
        result = result * 59 + ($sex == null ? 43 : $sex.hashCode());
        return result;
    }

    public String toString() {
        return "RechComsuOutData(clientId=" + this.getClientId() + ", cardNo=" + this.getCardNo() + ", mobile=" + this.getMobile() + ", stoNo=" + this.getStoNo() + ", stoName=" + this.getStoName() + ", queryType=" + this.getQueryType() + ", name=" + this.getName() + ", date=" + this.getDate() + ", time=" + this.getTime() + ", emp=" + this.getEmp() + ", empName=" + this.getEmpName() + ", index=" + this.getIndex() + ", rechargeAmt=" + this.getRechargeAmt() + ", consumeAmt=" + this.getConsumeAmt() + ", sex=" + this.getSex() + ")";
    }

    public static class RechComsuOutDataBuilder {
        private String clientId;
        private String cardNo;
        private String mobile;
        private String stoNo;
        private String stoName;
        private String queryType;
        private String name;
        private String date;
        private String time;
        private String emp;
        private String empName;
        private Integer index;
        private BigDecimal rechargeAmt;
        private BigDecimal consumeAmt;
        private String sex;

        RechComsuOutDataBuilder() {
        }

        public RechComsuOutData.RechComsuOutDataBuilder clientId(final String clientId) {
            this.clientId = clientId;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder cardNo(final String cardNo) {
            this.cardNo = cardNo;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder mobile(final String mobile) {
            this.mobile = mobile;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder stoNo(final String stoNo) {
            this.stoNo = stoNo;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder stoName(final String stoName) {
            this.stoName = stoName;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder queryType(final String queryType) {
            this.queryType = queryType;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder date(final String date) {
            this.date = date;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder time(final String time) {
            this.time = time;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder emp(final String emp) {
            this.emp = emp;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder empName(final String empName) {
            this.empName = empName;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder index(final Integer index) {
            this.index = index;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder rechargeAmt(final BigDecimal rechargeAmt) {
            this.rechargeAmt = rechargeAmt;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder consumeAmt(final BigDecimal consumeAmt) {
            this.consumeAmt = consumeAmt;
            return this;
        }

        public RechComsuOutData.RechComsuOutDataBuilder sex(final String sex) {
            this.sex = sex;
            return this;
        }

        public RechComsuOutData build() {
            return new RechComsuOutData(this.clientId, this.cardNo, this.mobile, this.stoNo, this.stoName, this.queryType, this.name, this.date, this.time, this.emp, this.empName, this.index, this.rechargeAmt, this.consumeAmt, this.sex);
        }

        public String toString() {
            return "RechComsuOutData.RechComsuOutDataBuilder(clientId=" + this.clientId + ", cardNo=" + this.cardNo + ", mobile=" + this.mobile + ", stoNo=" + this.stoNo + ", stoName=" + this.stoName + ", queryType=" + this.queryType + ", name=" + this.name + ", date=" + this.date + ", time=" + this.time + ", emp=" + this.emp + ", empName=" + this.empName + ", index=" + this.index + ", rechargeAmt=" + this.rechargeAmt + ", consumeAmt=" + this.consumeAmt + ", sex=" + this.sex + ")";
        }
    }
}
