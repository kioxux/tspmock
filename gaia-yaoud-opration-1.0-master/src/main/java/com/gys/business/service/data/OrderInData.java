package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 传参实体
 *
 * @author xiaoyuan on 2020/12/23
 */
@Data
public class OrderInData implements Serializable {
    private static final long serialVersionUID = -264755462702640256L;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 店号
     */
    @ApiModelProperty(value = "店号")
    private String brId;

    /**
     * 查询编码
     */
    private String nameOrCode;

    /**
     * local : 本地 / Nationwide : 全国
     */
    private String type;

    /**
     * 订购单号
     */
    @ApiModelProperty(value = "订购单号")
    private String voucherId;

    /**
     * 订购日期
     */
    @ApiModelProperty(value = "订购日期")
    private String gspdDate;

    @ApiModelProperty(value = "传参 : web 表示 web请求")
    private String flag;


}
