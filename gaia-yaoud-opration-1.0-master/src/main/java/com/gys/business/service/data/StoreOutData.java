//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "门店查询")
public class StoreOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "店号")
    private String gsstBrId;
    @ApiModelProperty(value = "店名")
    private String gsstBrName;
    @ApiModelProperty(value = "设置闭店时间")
    private String gsstDailyCloseDate;
}
