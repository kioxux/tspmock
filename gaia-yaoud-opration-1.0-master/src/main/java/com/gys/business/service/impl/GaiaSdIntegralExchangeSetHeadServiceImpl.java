package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CommonService;
import com.gys.business.service.GaiaSdIntegralExchangeSetHeadService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.integralExchange.IntegralExchangeProductVo;
import com.gys.common.data.GaiaSdIntegralExchangeRecord;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.NewExchangeTotalNumBean;
import com.gys.common.enums.SerialCodeTypeEnum;
import com.gys.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author RULAISZ
 * @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_HEAD(门店积分换购商品设置主表)】的数据库操作Service实现
 * @createDate 2021-11-29 09:40:14
 */
@Service
@Slf4j
public class GaiaSdIntegralExchangeSetHeadServiceImpl implements GaiaSdIntegralExchangeSetHeadService {

    @Autowired
    private GaiaSdIntegralExchangeSetHeadMapper gaiaSdIntegralExchangeSetHeadMapper;
    @Autowired
    private GaiaSdIntegralExchangeSetDetailMapper gaiaSdIntegralExchangeSetDetailMapper;
    @Autowired
    private GaiaSdIntegralExchangeSetProDetailMapper gaiaSdIntegralExchangeSetProDetailMapper;
    @Autowired
    private GaiaSdIntegralExchangeSetTimeMapper gaiaSdIntegralExchangeSetTimeMapper;
    @Autowired
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Autowired
    private GaiaSdStoreDataMapper gaiaSdStoreDataMapper;
    @Autowired
    private CommonService commonService;
    @Resource
    private GaiaSdIntegralExchangeRecordDao gaiaSdIntegralExchangeRecordDao;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String setDetail(IntegralExchangeSetInData inData) {
        //判断时间区间内是否包含时间段
        checkTimeInteral(inData);
        //判断限制个数是否为倍数
        checkProduct(inData);
        //根据设置日期和星期生成全部日期
        Set<String> days = getAllDate(inData);
        //判断（加盟商，门店，会员等级）每个商品是否有时间冲突
        checkProductDate(inData, days);
        //保存
        String voucherId = commonService.getSerialCode(inData.getClient(), SerialCodeTypeEnum.INTEGRAL_EXCHANGE);
        List<GaiaSdIntegralExchangeSetHead> headList = new ArrayList<>();
        List<GaiaSdIntegralExchangeSetDetail> detailList = new ArrayList<>();
        List<GaiaSdIntegralExchangeSetProDetail> proDetailList = new ArrayList<>();
        List<GaiaSdIntegralExchangeSetTime> times = new ArrayList<>();
        saveIntegralExchange(inData, days, voucherId,headList,detailList,proDetailList,times);
        if (!ObjectUtils.isEmpty(headList)){
            gaiaSdIntegralExchangeSetHeadMapper.batchInsert(headList);
        }
        if (!ObjectUtils.isEmpty(detailList)){
            gaiaSdIntegralExchangeSetDetailMapper.batchInsert(detailList);
        }
        if (!ObjectUtils.isEmpty(proDetailList)){
            gaiaSdIntegralExchangeSetProDetailMapper.batchInsert(proDetailList);
        }
        if (!ObjectUtils.isEmpty(times)){
            gaiaSdIntegralExchangeSetTimeMapper.batchInsert(times);
        }
        return voucherId;
    }

    @Override
    public void updateDetail(IntegralExchangeSetInData inData) {
        if (StringUtils.isEmpty(inData.getVoucherId())){
            throw new BusinessException("提示：请选择单号");
        }
        List<GaiaSdIntegralExchangeSetHead> oldHead = gaiaSdIntegralExchangeSetHeadMapper.getByClientAndVoucherId(inData.getClient(),inData.getVoucherId());
        List<GaiaSdIntegralExchangeSetDetail> oldDetail = gaiaSdIntegralExchangeSetDetailMapper.getByClientAndVoucherId(inData.getClient(),inData.getVoucherId());
        if (!ObjectUtils.isEmpty(oldHead) && oldHead.get(0).getStatus() == 1){
            throw new BusinessException("提示：该单已审核不能修改");
        }
        //判断时间区间内是否包含时间段
        checkTimeInteral(inData);
        //判断限制个数是否为倍数
        checkProduct(inData);
        //根据设置日期和星期生成全部日期
        Set<String> days = getAllDate(inData);
        //判断（加盟商，门店，会员等级）每个商品是否有时间冲突
        checkProductDate(inData, days);
        //删除该单号时段和商品所有数据
        gaiaSdIntegralExchangeSetHeadMapper.deleteByClientAndVoucherId(inData.getClient(),inData.getVoucherId());
        gaiaSdIntegralExchangeSetDetailMapper.deleteByClientAndVoucherId(inData.getClient(),inData.getVoucherId());
        gaiaSdIntegralExchangeSetTimeMapper.deleteByClientAndVoucherId(inData.getClient(),inData.getVoucherId());
        gaiaSdIntegralExchangeSetProDetailMapper.deleteByClientAndVoucherId(inData.getClient(),inData.getVoucherId());
        //重新保存
        List<GaiaSdIntegralExchangeSetHead> headList = new ArrayList<>();
        List<GaiaSdIntegralExchangeSetDetail> detailList = new ArrayList<>();
        List<GaiaSdIntegralExchangeSetProDetail> proDetailList = new ArrayList<>();
        List<GaiaSdIntegralExchangeSetTime> times = new ArrayList<>();
        saveIntegralExchange(inData, days, inData.getVoucherId(),headList,detailList,proDetailList,times);
        boolean deleteFlag = ObjectUtils.isEmpty(inData.getProList());
        for (GaiaSdIntegralExchangeSetHead head : headList) {
            GaiaSdIntegralExchangeSetHead old = oldHead.stream().filter(
                    o -> o.getClient().equals(head.getClient())
                            && o.getVoucherId().equals(head.getVoucherId())
                            && o.getStoCode().equals(head.getStoCode())
            ).findFirst().orElse(null);
            if (!ObjectUtils.isEmpty(old)){
                head.setId(old.getId());
                head.setCreateUser(old.getCreateUser());
                head.setCreateTime(old.getCreateTime());
            }
            head.setUpdateTime(inData.getUpdateTime());
            head.setUpdateUser(inData.getUpdateUser());
            if (deleteFlag){
                head.setDeleteFlag(1);
            }
        }

        for (GaiaSdIntegralExchangeSetDetail detail : detailList) {
            GaiaSdIntegralExchangeSetDetail old = oldDetail.stream().filter(
                    o -> o.getClient().equals(detail.getClient())
                            && o.getVoucherId().equals(detail.getVoucherId())
                            && o.getStoCode().equals(detail.getStoCode())
                            &&o.getProSelfCode().equals(detail.getProSelfCode())
            ).findFirst().orElse(null);
            if (!ObjectUtils.isEmpty(old)){
                detail.setId(old.getId());
                detail.setCreateUser(old.getCreateUser());
                detail.setCreateTime(old.getCreateTime());
            }
            detail.setUpdateTime(inData.getUpdateTime());
            detail.setUpdateUser(inData.getUpdateUser());
            if (deleteFlag){
                detail.setDeleteFlag("1");
            }
        }
        if (!ObjectUtils.isEmpty(headList)){
            gaiaSdIntegralExchangeSetHeadMapper.batchInsert(headList);
        }
        if (!ObjectUtils.isEmpty(detailList)){
            gaiaSdIntegralExchangeSetDetailMapper.batchInsert(detailList);
        }
        if (!ObjectUtils.isEmpty(proDetailList)){
            gaiaSdIntegralExchangeSetProDetailMapper.batchInsert(proDetailList);
        }
        if (!ObjectUtils.isEmpty(times)){
            gaiaSdIntegralExchangeSetTimeMapper.batchInsert(times);
        }
    }

    @Override
    public void audit(AuditIntegralExchangeInData inData) {
        gaiaSdIntegralExchangeSetHeadMapper.updateStatus(inData);
    }

    /**
     * 生成保存数据
     * @param inData
     * @param days
     * @param voucherId
     * @param headList
     * @param detailList
     * @param proDetailList
     * @param times
     */
    private void saveIntegralExchange(IntegralExchangeSetInData inData, Set<String> days, String voucherId,
                                      List<GaiaSdIntegralExchangeSetHead> headList,
                                      List<GaiaSdIntegralExchangeSetDetail> detailList,
                                      List<GaiaSdIntegralExchangeSetProDetail> proDetailList,
                                      List<GaiaSdIntegralExchangeSetTime> times) {
        List<GaiaSdStoreData> storeDataList = gaiaSdStoreDataMapper.getStoreByClientAndStoCodes(inData.getClient(), inData.getStoCodeList());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse("1970-01-01 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //1、保存主表
        for (String stoCode : inData.getStoCodeList()) {
            GaiaSdStoreData storeData = storeDataList.stream().filter(o -> o.getGsstBrId().equals(stoCode)).findFirst().orElse(null);
            String stoName = storeData!=null?storeData.getGsstBrName():"";
            GaiaSdIntegralExchangeSetHead head = new GaiaSdIntegralExchangeSetHead();
            head.setClient(inData.getClient());
            head.setVoucherId(voucherId);
            head.setStoCode(stoCode);
            head.setStoName(stoName);
            head.setStoGroup(StringUtils.isEmpty(inData.getStoGroup())?"": inData.getStoGroup());
            head.setMemberClass(inData.getMemberClass());
            head.setStartDate(inData.getStartDate());
            head.setEndDate(inData.getEndDate());
            head.setDateFrequency(StringUtils.isEmpty(inData.getDateFrequency())?"": inData.getDateFrequency());
            head.setWeekFrequency(StringUtils.isEmpty(inData.getWeekFrequency())?"": inData.getWeekFrequency());
            head.setTimeInterval(ObjectUtils.isEmpty(inData.getTimeInterval())?0:1);
            head.setStatus(0);
            head.setDeleteFlag(0);
            head.setCreateUser(inData.getCreateUser());
            head.setCreateTime(inData.getCreateTime());
            head.setUpdateUser("");
            head.setUpdateTime(date);
            headList.add(head);

            //2、保存明细表
            for (IntegralExchangeProductVo productVo : inData.getProList()) {
                GaiaSdIntegralExchangeSetDetail detail = new GaiaSdIntegralExchangeSetDetail();
                BeanUtils.copyProperties(productVo,detail);
                detail.setClient(inData.getClient());
                detail.setVoucherId(voucherId);
                detail.setStoCode(stoCode);
                detail.setDeleteFlag("0");
                detail.setCreateTime(inData.getCreateTime());
                detail.setCreateUser(inData.getCreateUser());
                detail.setUpdateUser("");
                detail.setUpdateTime(date);
                detailList.add(detail);

                for (String day : days) {
                    //3、保存商品表
                    GaiaSdIntegralExchangeSetProDetail proDetail = new GaiaSdIntegralExchangeSetProDetail();
                    proDetail.setClient(inData.getClient());
                    proDetail.setProSelfCode(productVo.getProSelfCode());
                    proDetail.setVoucherId(voucherId);
                    proDetail.setStoCode(stoCode);
                    proDetail.setExchangeDate(day);
                    proDetail.setCreateTime(inData.getCreateTime());
                    proDetail.setCreateUser(inData.getCreateUser());
                    proDetailList.add(proDetail);
                }
            }
        }
        //4、保存时段表
        if (!ObjectUtils.isEmpty(inData.getTimeInterval())){
            for (String time : inData.getTimeInterval()) {
                String[] split = time.split("-");
                if (split.length != 2) {
                    throw new BusinessException("提示：" + time + "时间段设置有误");
                }
                GaiaSdIntegralExchangeSetTime setTime = new GaiaSdIntegralExchangeSetTime();
                setTime.setClient(inData.getClient());
                setTime.setVoucherId(voucherId);
                setTime.setStartTime(split[0]);
                setTime.setEndTime(split[1]);
                setTime.setCreateTime(inData.getCreateTime());
                setTime.setCreateUser(inData.getCreateUser());
                times.add(setTime);
            }
        }
    }

    /**
     * 判断（加盟商，门店，会员等级）每个商品是否有时间冲突
     * @param inData
     * @param days
     */
    private void checkProductDate(IntegralExchangeSetInData inData, Set<String> days) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        List<IntegralExchangeProductVo> proList = inData.getProList();
        List<String> stoCodeList = inData.getStoCodeList();
        String memberClass = inData.getMemberClass();
        List<String> memberClassList = Arrays.asList(memberClass.split(","));

        for (IntegralExchangeProductVo vo : proList) {
            List<GaiaSdIntegralExchangeSetProDetail> list = gaiaSdIntegralExchangeSetProDetailMapper.getByClientAndProIdAndExchangeDatesWithOutVoucherId(inData.getClient(),inData.getVoucherId(),vo.getProSelfCode(), days);
            for (GaiaSdIntegralExchangeSetProDetail proDetail : list) {
                if (!stoCodeList.contains(proDetail.getStoCode())){
                    continue;
                }
                GaiaSdIntegralExchangeSetHead head = gaiaSdIntegralExchangeSetHeadMapper.getByClientAndVoucherIdAndStoCode(proDetail.getClient(),proDetail.getVoucherId(),proDetail.getStoCode());
                String oldMemberClass = head.getMemberClass() == null?"":head.getMemberClass();
                List<String> oldMemberClassList = Arrays.asList(oldMemberClass.split(","));
                boolean flag = false;
                for (String s : oldMemberClassList) {
                    if (memberClassList.contains(s)){
                        flag = true;
                        break;
                    }
                }
                if (!flag){
                    continue;
                }
                if (!(inData.getStartDate().compareTo(head.getEndDate()) < 0 && inData.getEndDate().compareTo(head.getStartDate()) > 0)){
                    continue;
                }
                List<String> timeInterval = inData.getTimeInterval();
                if (ObjectUtils.isEmpty(timeInterval)){
                    timeInterval = new ArrayList<>();
                    timeInterval.add("00:00:00-23:59:59");
                }
                List<GaiaSdIntegralExchangeSetTime> oldTimes = gaiaSdIntegralExchangeSetTimeMapper.getByClientAndVoucherId(head.getClient(),head.getVoucherId());
                if (ObjectUtils.isEmpty(oldTimes)){
                    oldTimes = new ArrayList<>();
                    GaiaSdIntegralExchangeSetTime setTime = new GaiaSdIntegralExchangeSetTime();
                    setTime.setStartTime("00:00:00");
                    setTime.setEndTime("23:59:59");
                    oldTimes.add(setTime);
                }
                for (GaiaSdIntegralExchangeSetTime oldTime : oldTimes) {
                    for (String time : timeInterval) {
                        String[] split = time.split("-");
                        if (split.length != 2) {
                            throw new BusinessException("提示：" + time + "时间段设置有误");
                        }
                        Date startTime;
                        Date endTime;
                        Date oldStartTime;
                        Date oldEndTime;
                        try {
                            //startTime = timeFormat.parse(split[0]);
                            //oldStartTime = timeFormat.parse(oldTime.getStartTime());
                            //endTime = timeFormat.parse(split[1]);
                            //oldEndTime = timeFormat.parse(oldTime.getEndTime());
                            startTime = (proDetail.getExchangeDate().equals(dateFormat.format(inData.getStartDate())) && timeFormat.parse(split[0]).compareTo(timeFormat.parse(timeFormat.format(inData.getStartDate()))) < 0)?timeFormat.parse(timeFormat.format(inData.getStartDate())):timeFormat.parse(split[0]);
                            endTime = (proDetail.getExchangeDate().equals(dateFormat.format(inData.getEndDate())) && timeFormat.parse(split[1]).compareTo(timeFormat.parse(timeFormat.format(inData.getEndDate()))) > 0)?timeFormat.parse(timeFormat.format(inData.getEndDate())):timeFormat.parse(split[1]);
                            oldStartTime = (proDetail.getExchangeDate().equals(dateFormat.format(head.getStartDate())) && timeFormat.parse(oldTime.getStartTime()).compareTo(timeFormat.parse(timeFormat.format(head.getStartDate()))) < 0)?timeFormat.parse(timeFormat.format(head.getStartDate())):timeFormat.parse(oldTime.getStartTime());
                            oldEndTime = (proDetail.getExchangeDate().equals(dateFormat.format(head.getEndDate())) && timeFormat.parse(oldTime.getEndTime()).compareTo(timeFormat.parse(timeFormat.format(head.getEndDate()))) > 0)?timeFormat.parse(timeFormat.format(head.getEndDate())):timeFormat.parse(oldTime.getEndTime());
                        } catch (ParseException e) {
                            e.printStackTrace();
                            throw new BusinessException("提示：时间段设置有误,请检查");
                        }
                        if (oldStartTime.compareTo(endTime) <= 0 && oldEndTime.compareTo(startTime) >= 0){
                            throw new BusinessException("商品" + vo.getProSelfCode() + "   与" + head.getVoucherId() + "单号的生效时间段有冲突，请检查");
                        }
                    }
                }
            }
        }
    }

    /**
     * 判断限制个数是否为倍数
     * @param inData
     */
    private void checkProduct(IntegralExchangeSetInData inData) {
        List<IntegralExchangeProductVo> proList = inData.getProList();
        for (int i = 0; i < proList.size(); i++) {
            if (ObjectUtils.isEmpty(proList.get(i).getProSelfCode())){
                throw new BusinessException("提示：第" + (i+1) + "行商品编码为空");
            }
            String proSelfCode = proList.get(i).getProSelfCode();
            long count = proList.stream().filter(o -> o.getProSelfCode().equals(proSelfCode)).count();
            if (count > 1){
                throw new BusinessException("提示：商品列表 " + proSelfCode + " 该商品重复");
            }
            if (proList.get(i).getBuyAmt() == null){
                throw new BusinessException("提示：第" + (i+1) + "行买满金额为空");
            }
            if (proList.get(i).getExchangeIntegra() == null){
                throw new BusinessException("提示：第" + (i+1) + "行所需积分为空");
            }
            if (proList.get(i).getExchangeAmt() == null){
                throw new BusinessException("提示：第" + (i+1) + "行换购金额为空");
            }
            if (proList.get(i).getExchangeQty() == null){
                throw new BusinessException("提示：第" + (i+1) + "行兑换数量为空");
            }
            if (proList.get(i).getPeriodMaxQty() == null){
                throw new BusinessException("提示：第" + (i+1) + "行限兑数量为空");
            }
            if (proList.get(i).getPeriodDays() == null){
                throw new BusinessException("提示：第" + (i+1) + "行限兑期间天数为空");
            }
            if (StringUtils.isEmpty(proList.get(i).getIntegralFlag())){
                throw new BusinessException("提示：第" + (i+1) + "行请选择是否积分");
            }
            if (!("1".equals(proList.get(i).getIntegralFlag()) || "0".equals(proList.get(i).getIntegralFlag()))){
                throw new BusinessException("提示：第" + (i+1) + "行是否积分格式有误，请检查");
            }
            BigDecimal exchangeQty = proList.get(i).getExchangeQty();
            if (exchangeQty.intValue() <= 0 || new BigDecimal(exchangeQty.intValue()).compareTo(exchangeQty)!=0){
                throw new BusinessException("提示：第" + (i+1) + "行兑换数量需设置为大于0的正整数");
            }
            BigDecimal periodDays = proList.get(i).getPeriodDays();
            if (periodDays.intValue() < 0 || new BigDecimal(periodDays.intValue()).compareTo(periodDays) != 0){
                throw new BusinessException("提示：第" + (i+1) + "行限兑天数需设置为大于或等于0的正整数");
            }
            /*if (periodDays.compareTo(BigDecimal.ZERO) > 0){
                BigDecimal periodMaxQty = proList.get(i).getPeriodMaxQty();
                if (periodMaxQty.intValue() < 0 || new BigDecimal(periodMaxQty.intValue()).compareTo(periodMaxQty) != 0){
                    throw new BusinessException("提示：第" + (i+1) + "行当限兑天数大于0时,限兑数量也需设置为大于0的正整数");
                }
            }*/
            if(proList.get(i).getExchangeIntegra().compareTo(BigDecimal.ONE) < 0){
                throw new BusinessException("提示：第" + (i+1) + "行所需积分需设置大于1");
            }
            BigDecimal periodMaxQty = proList.get(i).getPeriodMaxQty();
            if (periodMaxQty.intValue() < 0 || new BigDecimal(periodMaxQty.intValue()).compareTo(periodMaxQty) != 0){
                throw new BusinessException("提示：第" + (i+1) + "行限兑数量需设置为大于或等于0的正整数");
            }
            if (
                    (periodDays.compareTo(BigDecimal.ZERO)>0&&!(periodMaxQty.compareTo(BigDecimal.ZERO)>0))
                    ||(!(periodDays.compareTo(BigDecimal.ZERO)>0)&&periodMaxQty.compareTo(BigDecimal.ZERO)>0)
            ){
                throw new BusinessException("提示：第" + (i+1) + "行限兑数量和限兑天数需同时设置为0或大于0的正整数");
            }
            //判断限兑数量是否为兑换数量的整数倍
            if (proList.get(i).getExchangeQty().compareTo(BigDecimal.ZERO) > 0){
                BigDecimal divide = proList.get(i).getPeriodMaxQty().divide(proList.get(i).getExchangeQty(), 10, BigDecimal.ROUND_HALF_UP);
                if (new BigDecimal(divide.intValue()).compareTo(divide) != 0){
                    throw new BusinessException("提示：第" + (i+1) + "行限兑数量不是兑换数量的整数倍");
                }
            }
        }
    }

    /**
     * 根据设置日期和星期生成全部日期
     * @param inData
     * @return
     */
    private Set<String> getAllDate(IntegralExchangeSetInData inData) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String dateFrequency = inData.getDateFrequency();
        List<String> dateFrequencyList = new ArrayList<>();
        if (!StringUtils.isEmpty(dateFrequency)){
            String[] split = dateFrequency.split(",");
            dateFrequencyList = Arrays.asList(split);
        }
        String weekFrequency = inData.getWeekFrequency();
        List<String> weekFrequencyList = new ArrayList<>();
        if (!StringUtils.isEmpty(weekFrequency)){
            String[] split = weekFrequency.split(",");
            weekFrequencyList = Arrays.asList(split);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        try {
            calendar.setTime(dateFormat.parse(dateFormat.format(inData.getStartDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        HashSet<String> days = new HashSet<>();
        String[] week = {"7","1","2","3","4","5","6"};
        while (inData.getEndDate().compareTo(calendar.getTime()) >= 0){
            String dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH) + "";
            String dayOfWeek = week[calendar.get(Calendar.DAY_OF_WEEK) - 1];
            if (!ObjectUtils.isEmpty(dateFrequencyList) && dateFrequencyList.contains(dayOfMonth)){
                days.add(dateFormat.format(calendar.getTime()));
            }
            if (!ObjectUtils.isEmpty(weekFrequencyList) && weekFrequencyList.contains(dayOfWeek)){
                days.add(dateFormat.format(calendar.getTime()));
            }
            if (ObjectUtils.isEmpty(dateFrequencyList) && ObjectUtils.isEmpty(weekFrequencyList)){
                days.add(dateFormat.format(calendar.getTime()));
            }
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        return days;
    }

    /**
     * 判断时间区间内是否包含时间段
     * @param inData
     */
    private void checkTimeInteral(IntegralExchangeSetInData inData) {
        if (inData.getEndDate().compareTo(inData.getStartDate()) < 0) {
            throw new BusinessException("提示：结束日期需大于开始日期");
        }
        List<String> timeInterval = inData.getTimeInterval();
        if (ObjectUtils.isEmpty(timeInterval)) {
            return;
        }
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<HashMap<String, Date>> timeList = new ArrayList<>();
        for (String time : timeInterval) {
            HashMap<String, Date> timeMap = new HashMap<>();
            if (StringUtils.isEmpty(time)) {
                throw new BusinessException("提示：时间段设置有空值");
            }
            String startDate = dateFormat.format(inData.getStartDate());
            String endDate = dateFormat.format(inData.getEndDate());
            String[] split = time.split("-");
            if (split.length != 2) {
                throw new BusinessException("提示：" + time + "时间段设置有误");
            }
            Date sStartTimeInterval;
            Date sEndTimeInterval;
            Date eStartTimeInterval;
            Date eEndTimeInterval;
            Date startTime;
            Date endTime;
            try {
                startTime = timeFormat.parse(split[0]);
                endTime = timeFormat.parse(split[1]);
                sStartTimeInterval = dateTimeFormat.parse(startDate + " " + split[0]);
                sEndTimeInterval = dateTimeFormat.parse(startDate + " " + split[1]);
                eStartTimeInterval = dateTimeFormat.parse(endDate + " " + split[0]);
                eEndTimeInterval = dateTimeFormat.parse(endDate + " " + split[1]);
            } catch (ParseException e) {
                e.printStackTrace();
                throw new BusinessException("提示：时间段设置格式有误");
            }
            if (endTime.compareTo(startTime)<0){
                throw new BusinessException("提示：" + time + " 时间段设置有误");
            }
            if (!((sEndTimeInterval.compareTo(inData.getStartDate()) > 0 && sStartTimeInterval.compareTo(inData.getEndDate()) < 0)
                    || (eStartTimeInterval.compareTo(inData.getEndDate()) < 0) && eEndTimeInterval.compareTo(inData.getStartDate()) > 0)) {
                throw new BusinessException("提示：" + time + " 时间段不在时间范围内");
            }
            timeMap.put("startTime",startTime);
            timeMap.put("endTime",endTime);
            timeList.add(timeMap);
        }
        for (HashMap<String, Date> timeMap : timeList) {
            List<HashMap<String, Date>> collect = timeList.stream().filter(o -> timeMap.get("startTime").compareTo(o.get("endTime")) <= 0 && timeMap.get("endTime").compareTo(o.get("startTime")) >= 0).collect(Collectors.toList());
            if (collect.size() > 1){
                throw new BusinessException("提示："
                        + timeFormat.format(collect.get(0).get("startTime")) + "-" + timeFormat.format(collect.get(0).get("endTime"))
                        + " 和 "
                        + timeFormat.format(collect.get(1).get("startTime")) + "-" + timeFormat.format(collect.get(1).get("endTime"))
                        + " 时间段有冲突"
                );
            }
        }
    }

    @Override
    public PageInfo<IntegralOutData> getIntegralExchangeList(IntegralInData inData) {
        if (StringUtils.isBlank(inData.getStartEffectiveDate()) || StringUtils.isBlank(inData.getEndEffectiveDate())){
            throw new BusinessException("生效日期未填写完整！");
        }
        if (inData.getPageNum() == null){
            inData.setPageNum(1);
        }
        if (inData.getPageSize() == null){
            inData.setPageSize(50);
        }
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<IntegralOutData> outData = gaiaSdIntegralExchangeSetHeadMapper.getIntegralExchangeList(inData);
        PageInfo<IntegralOutData> info = new PageInfo<>(outData);
        for (IntegralOutData integralOutData : outData){
            integralOutData.setDateFrequency(exchangeDate(integralOutData.getDateFrequency()));
            integralOutData.setWeekFrequency(exchangeWeek(integralOutData.getWeekFrequency()));
            String effectiveTime = "";
            //查询:生效时段
            List<GaiaSdIntegralExchangeSetTime> timeList = gaiaSdIntegralExchangeSetTimeMapper.getEffectiveTime(integralOutData.getClient(),integralOutData.getVoucherId());
            if (timeList.size() > 0){
                for (GaiaSdIntegralExchangeSetTime timeData : timeList){
                    effectiveTime = effectiveTime + timeData.getStartTime() + '-' + timeData.getEndTime() + ',';
                }
            }
            if (StringUtils.isNotBlank(effectiveTime)){
                effectiveTime = effectiveTime.substring(0,effectiveTime.lastIndexOf(','));
            }
            integralOutData.setEffectiveTime(effectiveTime);
        }
        return info;
    }

    public static String exchangeDate(String str){
        if (StringUtils.isBlank(str)){
            return "";
        }
        String[] split = str.split(",");
        StringBuffer buffer = new StringBuffer("");
        for (String s : split) {
            buffer.append(s).append("号").append(",");
        }
        String substring = buffer.substring(0, buffer.length() - 1);
        return substring;
    }

    public static String exchangeWeek(String str){
        if (StringUtils.isBlank(str)){
            return "";
        }
        List<String> weekList = Arrays.asList(str.split(","));
        Map<String,String> map = new LinkedHashMap<>();
        map.put("1","星期一");
        map.put("2","星期二");
        map.put("3","星期三");
        map.put("4","星期四");
        map.put("5","星期五");
        map.put("6","星期六");
        map.put("7","星期日");
        Set<String> set = map.keySet();
        String res = "";
        for (String week : weekList){
            for (String key : set){
                if (week.equals(key)){
                    res = res + map.get(week).toString() + ",";
                }
            }
        }
        res = res.substring(0,res.lastIndexOf(','));
        return res;
    }

    @Override
    public List<IntegralExchangeStore> getStoreList(IntegralInData inData) {
        return  gaiaSdIntegralExchangeSetHeadMapper.getStoreList(inData);
    }

    @Override
    public CopyIntegralExchangeOutData getActivityData(IntegralInData inData) {
        CopyIntegralExchangeOutData outData = gaiaSdIntegralExchangeSetHeadMapper.getActivityData(inData.getClient(),inData.getVoucherId());
        List<IntegralExchangeStore> storeList = gaiaSdIntegralExchangeSetHeadMapper.getStoreList(inData);
        List<GaiaSdIntegralExchangeSetTime> timeList = gaiaSdIntegralExchangeSetTimeMapper.getEffectiveTime(inData.getClient(),inData.getVoucherId());
        List<IntegralExchangeSetDetail> detailList = gaiaSdIntegralExchangeSetDetailMapper.detailList(inData.getClient(),inData.getVoucherId());
        if (storeList.size() > 0){
            outData.setStoCodeList(storeList);
        }
        if (timeList.size() > 0){
            outData.setTimeList(timeList);
        }
        if (detailList.size() > 0){
            outData.setProList(detailList);
        }
        return outData;
    }
    /**
     * 积分换购批量导入
     *
     * @param loginOutData
     * @param file
     * @return
     */
    @Override
    public JsonResult batchExcelImport(GetLoginOutData loginOutData, MultipartFile file) {

        List<IntegralExchangeProductVo> dataList=new ArrayList<>();
        List<String> proCode=new ArrayList<>();
        try {
            dataList = EasyExcel.read(file.getInputStream())
                    .head(IntegralExchangeProductVo.class)
                    .sheet(0).doReadSync();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (CollUtil.isEmpty(dataList)) {
            throw new BusinessException("导入文件为空");
        }
        for (IntegralExchangeProductVo productVo : dataList) {
            if ("是".equals(productVo.getIntegralFlag())) {
                productVo.setIntegralFlag("1");
            }
            if ("否".equals(productVo.getIntegralFlag())) {
                productVo.setIntegralFlag("0");
            }
        }
        List<String> proSelfCodeList = dataList.stream().map(IntegralExchangeProductVo::getProSelfCode).collect(Collectors.toList());
        Example example=new Example(GaiaProductBusiness.class);
        example.createCriteria()
                .andEqualTo("client",loginOutData.getClient())
                .andIn("proSelfCode",proSelfCodeList);
        List<GaiaProductBusiness> productBusinessList = gaiaProductBusinessMapper.selectByExample(example);
        for (IntegralExchangeProductVo data : dataList) {
            for (GaiaProductBusiness product : productBusinessList) {
                if (data.getProSelfCode().equals(product.getProSelfCode())) {
                    data.setProductName(product.getProCommonname());
                    data.setSpec(product.getProSpecs());
                    data.setUnit(product.getProUnit());
                }
            }
        }
        proCode=dataList.stream().filter(data->{return StringUtils.isEmpty(data.getProductName());}).map(IntegralExchangeProductVo::getProSelfCode).collect(Collectors.toList());
        dataList=dataList.stream().filter(data->Objects.nonNull(data.getProductName())).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(proCode)) {
            StringBuffer sb=new StringBuffer();
            for (String p : proCode) {
                sb.append(p);
                sb.append(",");
            }
            return JsonResult.success(dataList,"导入的下列商品在门店中不存在，请检查:"+sb.toString());
        } else {
            return JsonResult.success(dataList,"导入成功");
        }
    }

    /**
     * excel模板下载
     * @param loginOutData
     * @param response
     */
    @Override
    public void downLoadTemplate(GetLoginOutData loginOutData, HttpServletResponse response) {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = "积分换购商品设置模板导出";
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        List<List<String>> head=getHead();
        try {
            EasyExcel.write(response.getOutputStream())
                    .registerWriteHandler(getStyle())
                    .head(head)
                    .sheet(0)
                    .doWrite(new ArrayList());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private HorizontalCellStyleStrategy getStyle() {

        WriteCellStyle headWriteCellStyle = new WriteCellStyle();
        // 背景白色
        headWriteCellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        WriteFont headWriteFont = new WriteFont();
        headWriteFont.setFontName("宋体");
        headWriteFont.setFontHeightInPoints((short)10);
        headWriteFont.setBold(false);   //不加粗
        headWriteCellStyle.setWriteFont(headWriteFont);
        headWriteCellStyle.setDataFormat((short) 49);//文本格式

        // 内容的策略
        WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
        // 这里需要指定 FillPatternType 为FillPatternType.SOLID_FOREGROUND 不然无法显示背景颜色.头默认了 FillPatternType所以可以不指定
        contentWriteCellStyle.setFillPatternType(FillPatternType.SOLID_FOREGROUND);
        // 背景白色
        contentWriteCellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        contentWriteCellStyle.setDataFormat((short) 49);    //文本格式
        contentWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);//垂直居中
        contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);//水平居中
        WriteFont contentWriteFont = new WriteFont();
        contentWriteFont.setFontHeightInPoints((short)10);
        contentWriteFont.setFontName("宋体");
        contentWriteFont.setBold(false);    //不加粗
        contentWriteCellStyle.setWriteFont(contentWriteFont);
        // 这个策略是 头是头的样式 内容是内容的样式 其他的策略可以自己实现
        HorizontalCellStyleStrategy horizontalCellStyleStrategy =
                new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);
        return horizontalCellStyleStrategy;
    }

    private List<List<String>> getHead() {
        List<List<String>> head=new ArrayList<>();
        head.add(new ArrayList<String>(){{add("商品编码");}});
        head.add(new ArrayList<String>(){{add("买满金额");}});
        head.add(new ArrayList<String>(){{add("所需积分");}});
        head.add(new ArrayList<String>(){{add("换购金额");}});
        head.add(new ArrayList<String>(){{add("兑换数量");}});
        head.add(new ArrayList<String>(){{add("限兑数量");}});
        head.add(new ArrayList<String>(){{add("限兑期间天数");}});
        head.add(new ArrayList<String>(){{add("是否积分");}});
        return head;
    }

//    查看：零售价
    public List<ProductPriceInfo> getPrice(ProductPriceInData inData){
        return gaiaSdIntegralExchangeSetDetailMapper.getPrice(inData);
    }

    @Override
    public void setEndTime(SetEndTimeIntegralExchangeInData inData) {
        List<GaiaSdIntegralExchangeSetHead> headList = gaiaSdIntegralExchangeSetHeadMapper.getByClientAndVoucherId(inData.getClient(), inData.getVoucherId());
        if (ObjectUtils.isEmpty(headList) && ObjectUtils.isEmpty(headList.get(0))){
            throw new BusinessException("提示：单号不存在");
        }
        if (headList.get(0).getStartDate().compareTo(inData.getEndDate()) > 0){
            throw new BusinessException("提示：结束日期不能小于开始日期");
        }
        List<String> stoCodeList = headList.stream().map(GaiaSdIntegralExchangeSetHead::getStoCode).collect(Collectors.toList());
        IntegralExchangeSetInData setInData = new IntegralExchangeSetInData();
        BeanUtils.copyProperties(headList.get(0),setInData);
        setInData.setStoCodeList(stoCodeList);
        setInData.setEndDate(inData.getEndDate());
        setInData.setCreateUser(inData.getUpdateUser());
        setInData.setCreateTime(inData.getUpdateTime());
        List<GaiaSdIntegralExchangeSetDetail> detailList = gaiaSdIntegralExchangeSetDetailMapper.getByClientAndVoucherIdGroupByProSelfCode(inData.getClient(),inData.getVoucherId());
        List<IntegralExchangeProductVo> productVos = new ArrayList<>();
        for (GaiaSdIntegralExchangeSetDetail detail : detailList) {
            IntegralExchangeProductVo productVo = new IntegralExchangeProductVo();
            BeanUtils.copyProperties(detail,productVo);
            productVos.add(productVo);
        }
        setInData.setProList(productVos);
        //根据设置日期和星期生成全部日期
        Set<String> days = getAllDate(setInData);
        //判断（加盟商，门店，会员等级）每个商品是否有时间冲突
        checkProductDate(setInData, days);
        List<GaiaSdIntegralExchangeSetHead> newHeadList = new ArrayList<>();
        List<GaiaSdIntegralExchangeSetDetail> newDetailList = new ArrayList<>();
        List<GaiaSdIntegralExchangeSetProDetail> proDetailList = new ArrayList<>();
        List<GaiaSdIntegralExchangeSetTime> times = new ArrayList<>();
        saveIntegralExchange(setInData, days, inData.getVoucherId(),newHeadList,newDetailList,proDetailList,times);
        //删除PRO_DETAIL
        gaiaSdIntegralExchangeSetProDetailMapper.deleteByClientAndVoucherId(inData.getClient(),inData.getVoucherId());

        if(!ObjectUtils.isEmpty(proDetailList)) {
            gaiaSdIntegralExchangeSetProDetailMapper.batchInsert(proDetailList);
        }

        gaiaSdIntegralExchangeSetHeadMapper.updateEndDate(inData);
    }

    @Override
    public JsonResult addRecord(List<GaiaSdIntegralExchangeRecord> list) {
        gaiaSdIntegralExchangeRecordDao.insertBatch(list);
        return JsonResult.success();
    }

    @Override
    public JsonResult exchangeTotalNum(NewExchangeTotalNumBean bean) {
        LocalDate now = LocalDate.now();
        String days = bean.getDays();
        int dayNum = Integer.parseInt(days);
        LocalDate start = now.minusDays(dayNum - 1);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String startTemp = start.format(dateTimeFormatter);
        bean.setStartDate(startTemp);
        List<GaiaSdIntegralExchangeRecord> list = gaiaSdIntegralExchangeRecordDao.exchangeTotalNum(bean);
        return JsonResult.success(list);
    }

    /**
     * 新积分换购商品退货
     * @param client
     * @param stoCode
     * @param saleBillNo
     * @param map key proId; value 总退货数
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult returnQty(String client, String stoCode, String saleBillNo, Map<String, BigDecimal> map) {
        List<String> proIdList = map.keySet().stream().collect(Collectors.toList());
        List<GaiaSdIntegralExchangeRecord> list = gaiaSdIntegralExchangeRecordDao.getBySaleBillNoAndProId(client, stoCode, saleBillNo, proIdList);
        if (CollUtil.isEmpty(list)) {
            return JsonResult.success();
        }
        List<GaiaSdIntegralExchangeRecord> resultAddList = new ArrayList<>(proIdList.size());
        String dateNow = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        for (GaiaSdIntegralExchangeRecord record : list) {
            if (new BigDecimal(record.getNum()).compareTo(BigDecimal.ZERO) < 0) {
                continue;
            }
            GaiaSdIntegralExchangeRecord temp = new GaiaSdIntegralExchangeRecord();
            BeanUtil.copyProperties(record, temp);
            BigDecimal bigDecimal = map.get(record.getProId());
            bigDecimal = bigDecimal.setScale(0, RoundingMode.DOWN);
            temp.setNum(bigDecimal.negate().toString());
            temp.setDate(dateNow);
            temp.setId(null);
            resultAddList.add(temp);
        }
        gaiaSdIntegralExchangeRecordDao.insertBatch(resultAddList);
        log.info("新积分换购，退货处理，client:{}, stoCode:{}, saleBillNo:{}, map:{}, list:{}, resultAddList:{}",
                client, stoCode, saleBillNo, JSON.toJSONString(map), JSON.toJSONString(list), JSON.toJSONString(resultAddList));
        return JsonResult.success();
    }
}
