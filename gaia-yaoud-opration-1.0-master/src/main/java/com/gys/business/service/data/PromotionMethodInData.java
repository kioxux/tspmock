package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 方式查询vo
 *
 * @author xiaoyuan on 2020/8/12
 */
@Data
public class PromotionMethodInData implements Serializable {
    private static final long serialVersionUID = 7108293417399786553L;

    /**
     * 商品编码
     */
    private String code;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String brId;

    /**
     * 数量
     */
    private String num;

    /**
     * 判断是否会员
     */
    private String isMember;

    /**
     * 商品集合
     */
    private List<RebornData> rebornDataList;

    /**
     * 商品编码集合
     */
    private List<String> proLists;
}
