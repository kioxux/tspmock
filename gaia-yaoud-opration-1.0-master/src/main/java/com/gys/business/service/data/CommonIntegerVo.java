package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommonIntegerVo implements Serializable {
    private static final long serialVersionUID = -6311116521300067518L;

    private Integer lable;

    private Object value;

    public CommonIntegerVo(Integer lable, String value) {
        this.lable = lable;
        this.value = value;
    }


}
