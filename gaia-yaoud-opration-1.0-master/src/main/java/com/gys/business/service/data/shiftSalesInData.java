package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
//@ApiModel(value = "请求参数明细")
public class shiftSalesInData implements Serializable {
    private static final long serialVersionUID = 7725607941479363760L;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "店号")
    private String gsdhBrId;

    @ApiModelProperty(value = "销售班次  早班/中班/晚班/通班")
    private String shift;

    @ApiModelProperty(value = "收银员id")
    private String cashier;

    @NotBlank(message = "日期不能为空")
    @ApiModelProperty(value = "日期")
    private String date;

}
