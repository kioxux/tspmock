package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdChinmedRelateH;
import com.gys.business.mapper.entity.GaiaSupplierBusiness;
import com.gys.business.service.data.GetSaleReturnDetailOutData;
import com.gys.business.service.data.LoginParam;
import com.gys.business.service.data.QueryParam;
import com.gys.business.service.data.WebService.Prescrip;
import com.gys.business.service.data.ZydjSaleH;
import com.gys.common.webService.InterfaceVO;

import java.util.List;

public interface TlSaleService {
    List<GaiaSupplierBusiness> querySupplier(String client, String brId);
    List<ZydjSaleH> queryDjSaleOrder(QueryParam inData);
    List<GetSaleReturnDetailOutData> queryDjSaleOrderDetail(QueryParam inData);
    InterfaceVO tlSaveAndPush(String client, String brId,Prescrip inData);
    InterfaceVO tlLogin(LoginParam loginParam);
    InterfaceVO tlGetStatus(String uniqueId,String pid);
    GaiaSdChinmedRelateH queryChinmedRelateDByBillNo(QueryParam inData);
}
