package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class GaiaProductSpecialEditRecordInData extends GaiaProductSpecialInData implements Serializable {

    /**
     * 修改起始日期
     */
    @ApiModelProperty(value="修改起始日期")
    private String beginDate;

    /**
     * 修改结束日期
     */
    @ApiModelProperty(value="修改结束日期")
    private String endDate;

    /**
     * 标识修改后值
     */
    @ApiModelProperty(value="标识修改后值")
    private String flagChangeTo;

    /**
     * 状态修改后值
     */
    @ApiModelProperty(value="状态修改后值")
    private String statusChangeTo;
}
