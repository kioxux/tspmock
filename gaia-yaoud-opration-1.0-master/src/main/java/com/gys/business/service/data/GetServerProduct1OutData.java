package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GetServerProduct1OutData implements Serializable {
    private static final long serialVersionUID = 6544229644079267758L;

    @ApiModelProperty(value = "序号")
    private Integer index1;

    @ApiModelProperty(value = "批号库存数量")
    private String gssbQty1 ;

    @ApiModelProperty(value = "总库存数量")
    private String gssQty1;

    @ApiModelProperty(value = "商品编码")
    private String proCode1;

    @ApiModelProperty(value = "商品名称")
    private String proName1;

    @ApiModelProperty(value = "商品通用名")
    private String proCommonName1;

    @ApiModelProperty(value = "商品价格")
    private String proAmount1;

    @ApiModelProperty(value = "规格")
    private String proSpecs1;

    @ApiModelProperty(value = "厂家名称")
    private String proFactoryName1;

    @ApiModelProperty(value = "产地")
    private String proPlace1;

    @ApiModelProperty(value = "剂型")
    private String proForm1;


    private String proBarcode1;
    private String proRegisterNo1;

    @ApiModelProperty(value = " 处方类别 1-处方药，2-甲类OTC，3-乙类OTC，4-双跨处方")
    private String proPresclass1;

    @ApiModelProperty(value = "成分分类")
    private String proCompclass1;
    /**
     * 有效期
     */
    @ApiModelProperty(value = "有效期")
    private String vaildDate1;


    @ApiModelProperty(value = "计量单位")
    private String proUnit1;

    @ApiModelProperty(value = "批号")
    private String batchNo1;
    /**
     * 批次
     */
    @ApiModelProperty(value = "批次")
    private String batch1;


    @ApiModelProperty(value = "税率值")
    private String movTax1;

    @ApiModelProperty(value = "批次表库存数量")
    private String proStock1;


    private String priority1;
    private String priority3;

    @ApiModelProperty(value = "商品定位")
    private String proPosition1;
    /**
     * 含税毛利额
     */
    @ApiModelProperty(value = "含税毛利额")
    private String grossProfit1;

    /**
     * 医保刷卡数量
     */
    @ApiModelProperty(value = "含税毛利额")
    private String proZdy1;
    /**
     * 货位号
     */
    @ApiModelProperty(value = "货位号")
    private String huowei;
    /**
     * 是否医保
     */
    @ApiModelProperty(value = "是否医保")
    private String proIfMed;

    @ApiModelProperty(value = "等级")
    private String saleClass;

    @ApiModelProperty(value = "不打折")
    private String proBdz;

    @ApiModelProperty(value = "用法用量")
    private String proUsage1;

    @ApiModelProperty(value = "禁忌说明")
    private String proContraindication1;

    @ApiModelProperty(value = "提示")
    private String gsstExplain51;

    @ApiModelProperty(value = "禁忌")
    private String gsscExplain51;

    @ApiModelProperty(value = "商品会员信息")
    private GetPromByProCodeOutData prom;

    @ApiModelProperty(value = "商品促销信息")
    private List<PromotionalConditionData> conditionList1;

    @ApiModelProperty(value = "图片路径")
    private String imageUrl1 = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201609%2F24%2F20160924025917_XKWzG.thumb.700_0.jpeg&refer=http%3A%2F%2Fb-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1617861328&t=5f4002e61120b4475898362f57380857";

}
