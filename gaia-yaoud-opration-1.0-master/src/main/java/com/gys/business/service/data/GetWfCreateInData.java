package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetWfCreateInData {
    private String token;

    private String wfDefineCode;

    private String wfTitle;

    private String wfDescription;

    private String wfOrder;

    private String wfSite;

    Object wfDetail;

    private GetWfNewPhysicalWorkflowInData newWorkflowInData;
}
