//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class PromAssoSetOutData {
    private String clientId;
    private String gspasVoucherId;
    private String gspasSerial;
    private String gspasSeriesId;
    private BigDecimal gspasAmt;
    private String gspasRebate;
    private String gspasQty;

    public PromAssoSetOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspasVoucherId() {
        return this.gspasVoucherId;
    }

    public String getGspasSerial() {
        return this.gspasSerial;
    }

    public String getGspasSeriesId() {
        return this.gspasSeriesId;
    }

    public BigDecimal getGspasAmt() {
        return this.gspasAmt;
    }

    public String getGspasRebate() {
        return this.gspasRebate;
    }

    public String getGspasQty() {
        return this.gspasQty;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspasVoucherId(final String gspasVoucherId) {
        this.gspasVoucherId = gspasVoucherId;
    }

    public void setGspasSerial(final String gspasSerial) {
        this.gspasSerial = gspasSerial;
    }

    public void setGspasSeriesId(final String gspasSeriesId) {
        this.gspasSeriesId = gspasSeriesId;
    }

    public void setGspasAmt(final BigDecimal gspasAmt) {
        this.gspasAmt = gspasAmt;
    }

    public void setGspasRebate(final String gspasRebate) {
        this.gspasRebate = gspasRebate;
    }

    public void setGspasQty(final String gspasQty) {
        this.gspasQty = gspasQty;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromAssoSetOutData)) {
            return false;
        } else {
            PromAssoSetOutData other = (PromAssoSetOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label95;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label95;
                    }

                    return false;
                }

                Object this$gspasVoucherId = this.getGspasVoucherId();
                Object other$gspasVoucherId = other.getGspasVoucherId();
                if (this$gspasVoucherId == null) {
                    if (other$gspasVoucherId != null) {
                        return false;
                    }
                } else if (!this$gspasVoucherId.equals(other$gspasVoucherId)) {
                    return false;
                }

                Object this$gspasSerial = this.getGspasSerial();
                Object other$gspasSerial = other.getGspasSerial();
                if (this$gspasSerial == null) {
                    if (other$gspasSerial != null) {
                        return false;
                    }
                } else if (!this$gspasSerial.equals(other$gspasSerial)) {
                    return false;
                }

                label74: {
                    Object this$gspasSeriesId = this.getGspasSeriesId();
                    Object other$gspasSeriesId = other.getGspasSeriesId();
                    if (this$gspasSeriesId == null) {
                        if (other$gspasSeriesId == null) {
                            break label74;
                        }
                    } else if (this$gspasSeriesId.equals(other$gspasSeriesId)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$gspasAmt = this.getGspasAmt();
                    Object other$gspasAmt = other.getGspasAmt();
                    if (this$gspasAmt == null) {
                        if (other$gspasAmt == null) {
                            break label67;
                        }
                    } else if (this$gspasAmt.equals(other$gspasAmt)) {
                        break label67;
                    }

                    return false;
                }

                Object this$gspasRebate = this.getGspasRebate();
                Object other$gspasRebate = other.getGspasRebate();
                if (this$gspasRebate == null) {
                    if (other$gspasRebate != null) {
                        return false;
                    }
                } else if (!this$gspasRebate.equals(other$gspasRebate)) {
                    return false;
                }

                Object this$gspasQty = this.getGspasQty();
                Object other$gspasQty = other.getGspasQty();
                if (this$gspasQty == null) {
                    if (other$gspasQty != null) {
                        return false;
                    }
                } else if (!this$gspasQty.equals(other$gspasQty)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromAssoSetOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspasVoucherId = this.getGspasVoucherId();
        result = result * 59 + ($gspasVoucherId == null ? 43 : $gspasVoucherId.hashCode());
        Object $gspasSerial = this.getGspasSerial();
        result = result * 59 + ($gspasSerial == null ? 43 : $gspasSerial.hashCode());
        Object $gspasSeriesId = this.getGspasSeriesId();
        result = result * 59 + ($gspasSeriesId == null ? 43 : $gspasSeriesId.hashCode());
        Object $gspasAmt = this.getGspasAmt();
        result = result * 59 + ($gspasAmt == null ? 43 : $gspasAmt.hashCode());
        Object $gspasRebate = this.getGspasRebate();
        result = result * 59 + ($gspasRebate == null ? 43 : $gspasRebate.hashCode());
        Object $gspasQty = this.getGspasQty();
        result = result * 59 + ($gspasQty == null ? 43 : $gspasQty.hashCode());
        return result;
    }

    public String toString() {
        return "PromAssoSetOutData(clientId=" + this.getClientId() + ", gspasVoucherId=" + this.getGspasVoucherId() + ", gspasSerial=" + this.getGspasSerial() + ", gspasSeriesId=" + this.getGspasSeriesId() + ", gspasAmt=" + this.getGspasAmt() + ", gspasRebate=" + this.getGspasRebate() + ", gspasQty=" + this.getGspasQty() + ")";
    }
}
