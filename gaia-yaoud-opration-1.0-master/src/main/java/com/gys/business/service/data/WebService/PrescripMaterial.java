package com.gys.business.service.data.WebService;

import lombok.Data;

@Data
public class PrescripMaterial {
    //医院药品编码
    private String cltItmId;
    //物料编号
    private String itmID;
    //物料名称
    private String itmName;
    //规格
    private String itmSpec;
    //产地
    private String place;
    //计量
    private String unit;
    //基本数量
    private String quantity;
    //核算单价
    private String price;
    //自由文本
    private String freeTxt;

}
