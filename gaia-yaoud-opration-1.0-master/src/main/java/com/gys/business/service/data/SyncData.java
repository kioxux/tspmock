package com.gys.business.service.data;

import lombok.Data;

@Data
public class SyncData {
    private String client;
    private String brId;
    private String param;
    private String type;
}
