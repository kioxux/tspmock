package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaGrossMarginIntervalMapper;
import com.gys.business.mapper.entity.GaiaGrossMarginInterval;
import com.gys.business.service.GrossMarginIntervalService;
import com.gys.business.service.data.CustomerOutData;
import com.gys.business.service.data.GrossMarginInData;
import com.gys.business.service.data.GrossMarginOutData;
import com.gys.business.service.data.GrossMarginOutDataCSV;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Zhangchi
 * @since 2021/09/16/9:21
 */

@Service
public class GrossMarginIntervalImpl implements GrossMarginIntervalService {
    @Resource
    private GaiaGrossMarginIntervalMapper gaiaGrossMarginIntervalMapper;
//    @Resource
//    private GrossConstructionService grossConstructionService;
//    @Resource
//    private GrossConstructionMapper grossConstructionMapper;
//    @Resource
//    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Override
    public List<CustomerOutData> selectCustomer(){
        return this.gaiaGrossMarginIntervalMapper.selectCustomer();
    }

    @Override
    public List<GrossMarginOutData> selectGrossMarginInterval(GrossMarginInData inData){
        List<GrossMarginOutData> list = this.gaiaGrossMarginIntervalMapper.selectGrossMarginInterval(inData);
        for (GrossMarginOutData outData : list) {
            if (outData.getEndValue() != null && outData.getEndValue().compareTo(new BigDecimal("999999")) == 0 && !outData.getIntervalType().equals("E")) {
                outData.setEndValue(null);
            }
            if (outData.getIntervalType().equals("E")) {
                outData.setEndValue(new BigDecimal("999999"));
            }
        }
        return list;
    }

    @Override
    public List<GrossMarginOutDataCSV> selectGrossMarginIntervalCSV(GrossMarginInData inData){
        List<GrossMarginOutDataCSV> list = this.gaiaGrossMarginIntervalMapper.selectGrossMarginIntervalCSV(inData);
        for (GrossMarginOutDataCSV dataCSV : list) {
            if (dataCSV.getEndValue() != null && dataCSV.getEndValue().equals("999999.00%") && !dataCSV.getIntervalType().equals("E")) {
                dataCSV.setEndValue("");
            }
            if (dataCSV.getIntervalType().equals("E")) {
                dataCSV.setEndValue("999999.00%");
            }
        }
        return list;
    }

    @Override
    public void saveGrossMargin(GetLoginOutData userInfo, List<GrossMarginOutData> inDataList){
        //修改数据校验
        if (check(inDataList)) {
            //获取修改人姓名
            GrossMarginOutData updateName = this.gaiaGrossMarginIntervalMapper.selectUserName(userInfo.getClient(),userInfo.getUserId());
            //修改保存更新
            for (GrossMarginOutData outData : inDataList) {
                GaiaGrossMarginInterval gaiaGrossMarginInterval = this.gaiaGrossMarginIntervalMapper.findGrossMargin(outData);
                if (gaiaGrossMarginInterval == null
                        || outData.getStartValue() == null || outData.getEndValue() == null
                        || gaiaGrossMarginInterval.getStartValue().compareTo(outData.getStartValue()) != 0 || gaiaGrossMarginInterval.getEndValue().compareTo(outData.getEndValue()) != 0) {
                    GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                    interval.setStartValue(outData.getStartValue());
                    interval.setEndValue(outData.getEndValue());
                    interval.setUpdateUser(updateName.getUpdateUser());
                    interval.setUpdateTime(new Date());
                    interval.setClient(outData.getClient());
                    interval.setIntervalType(outData.getIntervalType());
                    this.gaiaGrossMarginIntervalMapper.saveGrossMarginInterval(interval);
                }
            }
//            List<String> collect = inDataList.stream().map(o -> o.getClient()).distinct().collect(Collectors.toList());
//            threadPoolTaskExecutor.execute(()->{
//                List<String> clients = collect;
//                for (String c : clients) {
//                    List<String> months = grossConstructionMapper.findMonthByClient(c);
//                    Map<String, List<String>> map = Maps.of("clients", clients, "dates", months);
//                    grossConstructionService.grossConstructionJob(JSON.toJSONString(map));
//                }
//            });
        }
    }

    //毛利区间划分：修改数据校验
    public static boolean check(List<GrossMarginOutData> inDataList){
        //按照加盟商对集合分组
        Map<String, List<GrossMarginOutData>> customerMap;
        customerMap = inDataList.stream().collect(Collectors.groupingBy(GrossMarginOutData::getClient));
        //遍历加盟商毛利区间集合
        for (Map.Entry<String,List<GrossMarginOutData>> entry: customerMap.entrySet()){
            //便利每个加盟商的集合
            List<GrossMarginOutData> curList = entry.getValue();
            //根据毛利区间类型排序
            curList.sort(Comparator.comparing(GrossMarginOutData::getIntervalType));
            //判断连续性
            for (int i = 0; i < curList.size(); i++){
                if (curList.get(i).getStartValue() == null){
                    for (int j = i + 1; j < curList.size(); j++){
                        if (curList.get(j).getStartValue() != null){
                            throw new BusinessException("客户ID为"+curList.get(j).getClient()+"的毛利区间存在中断情况，请重新填写！");
                        }
                    }
                }
            }
            for (GrossMarginOutData outData : curList) {
                //判断起始毛利率大于结束毛利率
                if (outData.getStartValue() != null) {
                    if (outData.getEndValue() != null) {
                        if (outData.getStartValue().compareTo(outData.getEndValue()) >= 0) {
                            throw new BusinessException("客户ID为" + outData.getClient() + "的" + outData.getIntervalType() + "档存在起始毛利率不小于结束毛利率情况，请重新填写！");
                        }
                    } else {
                        outData.setEndValue(new BigDecimal("999999"));
                    }
                } else {
                    outData.setEndValue(null);
                }
            }
        }
        return true;
    }

    @Override
    public void insertData(){
        List<GrossMarginOutData> grossMargin;
        grossMargin = this.gaiaGrossMarginIntervalMapper.selectClient();
        if (grossMargin.size() > 0){
            for (GrossMarginOutData outData : grossMargin) {
                for (int i = 0; i < 5; i++) {
                    if (i == 0) {
                        GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                        interval.setClient(outData.getClient());
                        interval.setIntervalType("A");
                        interval.setStartValue(new BigDecimal("-999999"));
                        interval.setEndValue(new BigDecimal("0"));
                        this.gaiaGrossMarginIntervalMapper.insertGrossMarginData(interval);
                    }
                    if (i == 1) {
                        GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                        interval.setClient(outData.getClient());
                        interval.setIntervalType("B");
                        interval.setStartValue(new BigDecimal("0"));
                        interval.setEndValue(new BigDecimal("10"));
                        this.gaiaGrossMarginIntervalMapper.insertGrossMarginData(interval);
                    }
                    if (i == 2) {
                        GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                        interval.setClient(outData.getClient());
                        interval.setIntervalType("C");
                        interval.setStartValue(new BigDecimal("10"));
                        interval.setEndValue(new BigDecimal("35"));
                        this.gaiaGrossMarginIntervalMapper.insertGrossMarginData(interval);
                    }
                    if (i == 3) {
                        GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                        interval.setClient(outData.getClient());
                        interval.setIntervalType("D");
                        interval.setStartValue(new BigDecimal("35"));
                        interval.setEndValue(new BigDecimal("50"));
                        this.gaiaGrossMarginIntervalMapper.insertGrossMarginData(interval);
                    }
                    if (i == 4) {
                        GaiaGrossMarginInterval interval = new GaiaGrossMarginInterval();
                        interval.setClient(outData.getClient());
                        interval.setIntervalType("E");
                        interval.setStartValue(new BigDecimal("50"));
                        interval.setEndValue(new BigDecimal("999999"));
                        this.gaiaGrossMarginIntervalMapper.insertGrossMarginData(interval);
                    }
                }
            }
        }
    }
}
