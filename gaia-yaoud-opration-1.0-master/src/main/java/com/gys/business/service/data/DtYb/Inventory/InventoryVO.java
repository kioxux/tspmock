package com.gys.business.service.data.DtYb.Inventory;

import lombok.Data;

import java.util.List;

/**
 * @Description 大同易联众 门店库存
 * @Author huxinxin
 * @Date 2021/5/12 17:56
 * @Version 1.0.0
 **/
@Data
public class InventoryVO {
    /*
     *上传库存时间
     **/
    private String inventoryDate;
    /*
     *
     **/
    private String inventoryCode;

    private List<InventoryProVO> proList;
}
