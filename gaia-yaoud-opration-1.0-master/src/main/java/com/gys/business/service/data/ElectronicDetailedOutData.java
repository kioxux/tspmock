package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 子表信息
 *
 * @author xiaoyuan on 2020/10/4
 */
@Data
public class ElectronicDetailedOutData implements Serializable {
    private static final long serialVersionUID = 2079165340327635593L;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 主题单号
     */
    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    /**
     * 业务单号
     */
    @ApiModelProperty(value = "业务单号")
    private String gsebsId;

    @ApiModelProperty(value = "电子券描述")
    private String gsebName;

    @ApiModelProperty(value = "面值")
    private String gsebAmt;

    /**
     * 电子券活动号
     */
    @ApiModelProperty(value = "电子券活动号")
    private String gsebId;

    /**
     * 达到数量1
     */
    @ApiModelProperty(value = "达到数量1")
    private String gsecsReachQty1;

    /**
     * 达到金额1
     */
    @ApiModelProperty(value = "达到金额1")
    private String gsecsReachAmt1;

    /**
     * 送券数量1
     */
    @ApiModelProperty(value = "送券数量1")
    private String gsecsResultQty1;


    /**
     * 是否重复   N为否，Y为是
     */
    @ApiModelProperty(value = "是否重复   N为否，Y为是")
    private String gsecsFlag1;

    /**
     * 是否全场商品   N为否，Y为是
     */
    @ApiModelProperty(value = "是否全场商品   N为否，Y为是")
    private String gsecsFlag2;

    /**
     * 商品组名
     */
    @ApiModelProperty(value = "商品组名")
    private String gsecsProGroup;


    /**
     * 创建日期
     */
    private String gsecsCreateDate;

    /**
     * 创建时间
     */
    private String gsecsCreateTime;

    /**
     * 是否有效
     */
    private String gsecsValid;
    private String gsebTypeName;
    private String gsebType;

    /**
     * 商品组名
     */
    @ApiModelProperty(value = "商品详细信息")
    private List<FuzzyQueryOfProductGroupOutData> groupOutData;

    @ApiModelProperty(value = "商品编码数组")
    private List<String> gspgProIds;

}
