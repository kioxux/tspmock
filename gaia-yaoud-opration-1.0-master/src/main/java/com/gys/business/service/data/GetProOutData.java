//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetProOutData implements Serializable {
    private String proName;
    private String proCode;
    private String proSpecs;

    public GetProOutData() {
    }

    public String getProName() {
        return this.proName;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetProOutData)) {
            return false;
        } else {
            GetProOutData other = (GetProOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$proName = this.getProName();
                    Object other$proName = other.getProName();
                    if (this$proName == null) {
                        if (other$proName == null) {
                            break label47;
                        }
                    } else if (this$proName.equals(other$proName)) {
                        break label47;
                    }

                    return false;
                }

                Object this$proCode = this.getProCode();
                Object other$proCode = other.getProCode();
                if (this$proCode == null) {
                    if (other$proCode != null) {
                        return false;
                    }
                } else if (!this$proCode.equals(other$proCode)) {
                    return false;
                }

                Object this$proSpecs = this.getProSpecs();
                Object other$proSpecs = other.getProSpecs();
                if (this$proSpecs == null) {
                    if (other$proSpecs != null) {
                        return false;
                    }
                } else if (!this$proSpecs.equals(other$proSpecs)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetProOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        return result;
    }

    public String toString() {
        return "GetProOutData(proName=" + this.getProName() + ", proCode=" + this.getProCode() + ", proSpecs=" + this.getProSpecs() + ")";
    }
}
