package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class StockReportHeadOutData implements Serializable {

    @ApiModelProperty(value = "公司库存品项数")
    private BigDecimal compStockCount;

    @ApiModelProperty(value = "公司库存成本额")
    private BigDecimal compAmt;

    @ApiModelProperty(value = "公司库存周转天数")
    private BigDecimal compTurnoverDays;

    @ApiModelProperty(value = "门店库存品项数")
    private BigDecimal storeStockCount;

    @ApiModelProperty(value = "门店库存成本额")
    private BigDecimal storeAmt;

    @ApiModelProperty(value = "门店库存周转天数")
    private BigDecimal storeTurnoverDays;

    @ApiModelProperty(value = "仓库库存品项数")
    private BigDecimal wmsStockCount;

    @ApiModelProperty(value = "仓库库存成本额")
    private BigDecimal wmsAmt;

    @ApiModelProperty(value = "仓库周转天数")
    private BigDecimal wmsTurnoverDays;
}
