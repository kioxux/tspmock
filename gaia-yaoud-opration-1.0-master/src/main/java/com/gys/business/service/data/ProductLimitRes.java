package com.gys.business.service.data;

import cn.hutool.core.collection.CollectionUtil;
import com.gys.business.mapper.entity.ProductLimitSto;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class ProductLimitRes implements Serializable {

    private static final long serialVersionUID = -3219245455075763575L;

    private Integer id;

    private String client;

    private String proId;

    private String proCommonName ;// "通用名称";

    private String specs ;// "规格";

    private String unit ;// "计量单位";

    private String proFactoryName ;// "生产企业";

    private String proPlace;//产地

    private BigDecimal priceNormal ;// "零售价";

    //限购类型. 0：每单限购，1：会员限购
    private String limitType;

    private String limitTypeStr;

    public String getLimitTypeStr() {
        String res = "";
        if("0".equals(limitType)){
            res = "每单限购";
        }else if("1".equals(limitType)){
            res = "会员限购";
        }
        return res;
    }

    //限购方式 0：每天，1：合计
    private String limitFlag;

    private String limitFlagStr;

    public String getLimitFlagStr() {
        String res = "";
        if("0".equals(limitType)){
            res = "-";
        }else if("1".equals(limitType)){
            if("0".equals(limitFlag)){
                res = "每天";
            }else if("1".equals(limitFlag)){
                res = "合计";
            }
        }
        return res;
    }

    //限购数量
    private String limitQty;

    //是否一直生效. 0：否，1：是
    private String ifAllTime;

    private String ifAllTimeStr;

    public String getIfAllTimeStr() {
        String res = "";
        if("0".equals(ifAllTime)){
            res = "否";
        }else if("1".equals(ifAllTime)){
            res = "是";
        }
        return res;
    }

    //起始日期
    private String beginDate;

    //结束日期
    private String endDate;

    private String inTimeStr;

    public String getInTimeStr() {
        String res = "";
        if("0".equals(ifAllTime)){
            res = beginDate + "-" + endDate;
        }else if("1".equals(ifAllTime)){
            res = "-";
        }
        return res;
    }

    //星期频率
    private String weekFrequency;

    //日期频率
    private String dateFrequency;

    //是否所有门店0：否，1：是
    private String ifAllSto;

    private String ifAllStoStr;

    public String getIfAllStoStr() {
        String res = "";
        if("0".equals(ifAllSto)){
            res = "否";
        }else if("1".equals(ifAllSto)){
            res = "是";
        }
        return res;
    }

    List<ProductLimitSto> stoList;

    private List<String> sites;

    public List<String> getSites() {
        List<String> res = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(stoList)){
            stoList.forEach(x->{
                res.add(x.getBrId());
            });
        }
        return res;
    }
    //是否销售/原价销售  0/null:不可销售 ,1: 原价销售
    private Integer ifSale;
}
