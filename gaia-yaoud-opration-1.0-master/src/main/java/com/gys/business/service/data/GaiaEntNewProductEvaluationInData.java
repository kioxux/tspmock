package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 公司-新品评估-明细表(GaiaEntNewProductEvaluationD)实体类
 *
 * @author makejava
 * @since 2021-07-19 10:16:25
 */
@Data
public class GaiaEntNewProductEvaluationInData implements Serializable {
    private static final long serialVersionUID = 796644975143025513L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 商品评估单号
     */
    private String billCode;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 门店ID
     */
    private String storeId;
    /**
     * 销售额
     */
    private Double salesAmt;
    /**
     * 销量
     */
    private Double salesQuantity;
    /**
     * 毛利额
     */
    private Double gross;
    /**
     * 建议处置状态：0-淘汰 1-转正常
     */
    private Integer suggestedStatus;
    /**
     * 商品状态：0-淘汰 1-转正常
     */
    private Integer status;
    /**
     * 处理状态：0-未处理 1-已处理
     */
    private Integer dealStatus;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;

    private String commentClass;
    //必备状态
    private String mustHaveStatus;
    private BigDecimal dpddprice;
    private BigDecimal classAveragePrice;
    private int salesStoreQty;
    private int storeQty;
    private int invStoreQty;
    private int sendRange;

    private String startTime;
    private String endTime;
    private String proName;
    private String stoAttribute;
    private List<String> proCompBigCodeList;
    private List<String> billCodeList;
    private int order;




}
