package com.gys.business.service.data;

import lombok.Data;

@Data
public class IntegralExchangeInData {
    private String client;
    private String brId;
    private String proCode;
}
