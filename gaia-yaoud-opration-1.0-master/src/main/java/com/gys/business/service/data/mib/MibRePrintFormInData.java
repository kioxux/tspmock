package com.gys.business.service.data.mib;

import lombok.Data;

/**
 * @Author ：gyx
 * @Date ：Created in 10:57 2021/12/29
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Data
public class MibRePrintFormInData {
    private String client;

    private String stoCode;
    /**
     * 消息id
     */
    private String msgId;
    /**
     * 人员编码
     */
    private String psnNo;
    /**
     * 结算id
     */
    private String setlId;
    /**
     * 就诊id
     */
    private String mdtrtId;
}
