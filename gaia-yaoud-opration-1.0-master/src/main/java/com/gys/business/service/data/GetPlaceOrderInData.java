package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GetPlaceOrderInData {
    private static final long serialVersionUID = 5525835479681983307L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "操作类型 1 分配下单 2 批量导入")
    private String type;
    @ApiModelProperty(value = "下单传入参数列表")
    List<GetReplenishDetailOutData> orderList;
    @ApiModelProperty(value = "导入传入参数列表")
    List<ImportInData> importInDataList;
    @ApiModelProperty(value = "连锁公司编码")
    private String dcCode;
    @ApiModelProperty(value = "商品编码")
    private List<String> proIds;

}
