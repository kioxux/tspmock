//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class GetSyncSdSaleDOutData {
    private String clientId;
    private String gssdBillNo;
    private String gssdBrId;
    private String gssdDate;
    private String gssdSerial;
    private String gssdProId;
    private String gssdBatchNo;
    private String gssdBatch;
    private String gssdValidDate;
    private String gssdStCode;
    private String gssdPrc1;
    private String gssdPrc2;
    private String gssdQty;
    private String gssdAmt;
    private String gssdIntegralExchangeSingle;
    private String gssdIntegralExchangeCollect;
    private String gssdIntegralExchangeAmtSingle;
    private String gssdIntegralExchangeAmtCollect;
    private String gssdZkAmt;
    private String gssdZkJfdh;
    private String gssdZkJfdx;
    private String gssdZkDzq;
    private String gssdZkDyq;
    private String gssdZkPm;
    private String gssdSalerId;
    private String gssdDoctorId;
    private String gssdPmSubjectId;
    private String gssdPmId;
    private String gssdPmContent;
    private String gssdRecipelFlag;
    private String gssdRelationFlag;
    private String gssdSpecialmedIdcard;
    private String gssdSpecialmedName;
    private String gssdSpecialmedSex;
    private String gssdSpecialmedBirthday;
    private String gssdSpecialmedMobile;
    private String gssdSpecialmedAddress;
    private BigDecimal gssdBactchCost;
    private BigDecimal gssdBatchTax;
    private BigDecimal gssdMovPrice;
    private BigDecimal gssdMovTax;
    private String gssdAdFlag;

    public GetSyncSdSaleDOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGssdBillNo() {
        return this.gssdBillNo;
    }

    public String getGssdBrId() {
        return this.gssdBrId;
    }

    public String getGssdDate() {
        return this.gssdDate;
    }

    public String getGssdSerial() {
        return this.gssdSerial;
    }

    public String getGssdProId() {
        return this.gssdProId;
    }

    public String getGssdBatchNo() {
        return this.gssdBatchNo;
    }

    public String getGssdBatch() {
        return this.gssdBatch;
    }

    public String getGssdValidDate() {
        return this.gssdValidDate;
    }

    public String getGssdStCode() {
        return this.gssdStCode;
    }

    public String getGssdPrc1() {
        return this.gssdPrc1;
    }

    public String getGssdPrc2() {
        return this.gssdPrc2;
    }

    public String getGssdQty() {
        return this.gssdQty;
    }

    public String getGssdAmt() {
        return this.gssdAmt;
    }

    public String getGssdIntegralExchangeSingle() {
        return this.gssdIntegralExchangeSingle;
    }

    public String getGssdIntegralExchangeCollect() {
        return this.gssdIntegralExchangeCollect;
    }

    public String getGssdIntegralExchangeAmtSingle() {
        return this.gssdIntegralExchangeAmtSingle;
    }

    public String getGssdIntegralExchangeAmtCollect() {
        return this.gssdIntegralExchangeAmtCollect;
    }

    public String getGssdZkAmt() {
        return this.gssdZkAmt;
    }

    public String getGssdZkJfdh() {
        return this.gssdZkJfdh;
    }

    public String getGssdZkJfdx() {
        return this.gssdZkJfdx;
    }

    public String getGssdZkDzq() {
        return this.gssdZkDzq;
    }

    public String getGssdZkDyq() {
        return this.gssdZkDyq;
    }

    public String getGssdZkPm() {
        return this.gssdZkPm;
    }

    public String getGssdSalerId() {
        return this.gssdSalerId;
    }

    public String getGssdDoctorId() {
        return this.gssdDoctorId;
    }

    public String getGssdPmSubjectId() {
        return this.gssdPmSubjectId;
    }

    public String getGssdPmId() {
        return this.gssdPmId;
    }

    public String getGssdPmContent() {
        return this.gssdPmContent;
    }

    public String getGssdRecipelFlag() {
        return this.gssdRecipelFlag;
    }

    public String getGssdRelationFlag() {
        return this.gssdRelationFlag;
    }

    public String getGssdSpecialmedIdcard() {
        return this.gssdSpecialmedIdcard;
    }

    public String getGssdSpecialmedName() {
        return this.gssdSpecialmedName;
    }

    public String getGssdSpecialmedSex() {
        return this.gssdSpecialmedSex;
    }

    public String getGssdSpecialmedBirthday() {
        return this.gssdSpecialmedBirthday;
    }

    public String getGssdSpecialmedMobile() {
        return this.gssdSpecialmedMobile;
    }

    public String getGssdSpecialmedAddress() {
        return this.gssdSpecialmedAddress;
    }

    public BigDecimal getGssdBactchCost() {
        return this.gssdBactchCost;
    }

    public BigDecimal getGssdBatchTax() {
        return this.gssdBatchTax;
    }

    public BigDecimal getGssdMovPrice() {
        return this.gssdMovPrice;
    }

    public BigDecimal getGssdMovTax() {
        return this.gssdMovTax;
    }

    public String getGssdAdFlag() {
        return this.gssdAdFlag;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGssdBillNo(final String gssdBillNo) {
        this.gssdBillNo = gssdBillNo;
    }

    public void setGssdBrId(final String gssdBrId) {
        this.gssdBrId = gssdBrId;
    }

    public void setGssdDate(final String gssdDate) {
        this.gssdDate = gssdDate;
    }

    public void setGssdSerial(final String gssdSerial) {
        this.gssdSerial = gssdSerial;
    }

    public void setGssdProId(final String gssdProId) {
        this.gssdProId = gssdProId;
    }

    public void setGssdBatchNo(final String gssdBatchNo) {
        this.gssdBatchNo = gssdBatchNo;
    }

    public void setGssdBatch(final String gssdBatch) {
        this.gssdBatch = gssdBatch;
    }

    public void setGssdValidDate(final String gssdValidDate) {
        this.gssdValidDate = gssdValidDate;
    }

    public void setGssdStCode(final String gssdStCode) {
        this.gssdStCode = gssdStCode;
    }

    public void setGssdPrc1(final String gssdPrc1) {
        this.gssdPrc1 = gssdPrc1;
    }

    public void setGssdPrc2(final String gssdPrc2) {
        this.gssdPrc2 = gssdPrc2;
    }

    public void setGssdQty(final String gssdQty) {
        this.gssdQty = gssdQty;
    }

    public void setGssdAmt(final String gssdAmt) {
        this.gssdAmt = gssdAmt;
    }

    public void setGssdIntegralExchangeSingle(final String gssdIntegralExchangeSingle) {
        this.gssdIntegralExchangeSingle = gssdIntegralExchangeSingle;
    }

    public void setGssdIntegralExchangeCollect(final String gssdIntegralExchangeCollect) {
        this.gssdIntegralExchangeCollect = gssdIntegralExchangeCollect;
    }

    public void setGssdIntegralExchangeAmtSingle(final String gssdIntegralExchangeAmtSingle) {
        this.gssdIntegralExchangeAmtSingle = gssdIntegralExchangeAmtSingle;
    }

    public void setGssdIntegralExchangeAmtCollect(final String gssdIntegralExchangeAmtCollect) {
        this.gssdIntegralExchangeAmtCollect = gssdIntegralExchangeAmtCollect;
    }

    public void setGssdZkAmt(final String gssdZkAmt) {
        this.gssdZkAmt = gssdZkAmt;
    }

    public void setGssdZkJfdh(final String gssdZkJfdh) {
        this.gssdZkJfdh = gssdZkJfdh;
    }

    public void setGssdZkJfdx(final String gssdZkJfdx) {
        this.gssdZkJfdx = gssdZkJfdx;
    }

    public void setGssdZkDzq(final String gssdZkDzq) {
        this.gssdZkDzq = gssdZkDzq;
    }

    public void setGssdZkDyq(final String gssdZkDyq) {
        this.gssdZkDyq = gssdZkDyq;
    }

    public void setGssdZkPm(final String gssdZkPm) {
        this.gssdZkPm = gssdZkPm;
    }

    public void setGssdSalerId(final String gssdSalerId) {
        this.gssdSalerId = gssdSalerId;
    }

    public void setGssdDoctorId(final String gssdDoctorId) {
        this.gssdDoctorId = gssdDoctorId;
    }

    public void setGssdPmSubjectId(final String gssdPmSubjectId) {
        this.gssdPmSubjectId = gssdPmSubjectId;
    }

    public void setGssdPmId(final String gssdPmId) {
        this.gssdPmId = gssdPmId;
    }

    public void setGssdPmContent(final String gssdPmContent) {
        this.gssdPmContent = gssdPmContent;
    }

    public void setGssdRecipelFlag(final String gssdRecipelFlag) {
        this.gssdRecipelFlag = gssdRecipelFlag;
    }

    public void setGssdRelationFlag(final String gssdRelationFlag) {
        this.gssdRelationFlag = gssdRelationFlag;
    }

    public void setGssdSpecialmedIdcard(final String gssdSpecialmedIdcard) {
        this.gssdSpecialmedIdcard = gssdSpecialmedIdcard;
    }

    public void setGssdSpecialmedName(final String gssdSpecialmedName) {
        this.gssdSpecialmedName = gssdSpecialmedName;
    }

    public void setGssdSpecialmedSex(final String gssdSpecialmedSex) {
        this.gssdSpecialmedSex = gssdSpecialmedSex;
    }

    public void setGssdSpecialmedBirthday(final String gssdSpecialmedBirthday) {
        this.gssdSpecialmedBirthday = gssdSpecialmedBirthday;
    }

    public void setGssdSpecialmedMobile(final String gssdSpecialmedMobile) {
        this.gssdSpecialmedMobile = gssdSpecialmedMobile;
    }

    public void setGssdSpecialmedAddress(final String gssdSpecialmedAddress) {
        this.gssdSpecialmedAddress = gssdSpecialmedAddress;
    }

    public void setGssdBactchCost(final BigDecimal gssdBactchCost) {
        this.gssdBactchCost = gssdBactchCost;
    }

    public void setGssdBatchTax(final BigDecimal gssdBatchTax) {
        this.gssdBatchTax = gssdBatchTax;
    }

    public void setGssdMovPrice(final BigDecimal gssdMovPrice) {
        this.gssdMovPrice = gssdMovPrice;
    }

    public void setGssdMovTax(final BigDecimal gssdMovTax) {
        this.gssdMovTax = gssdMovTax;
    }

    public void setGssdAdFlag(final String gssdAdFlag) {
        this.gssdAdFlag = gssdAdFlag;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSyncSdSaleDOutData)) {
            return false;
        } else {
            GetSyncSdSaleDOutData other = (GetSyncSdSaleDOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gssdBillNo = this.getGssdBillNo();
                Object other$gssdBillNo = other.getGssdBillNo();
                if (this$gssdBillNo == null) {
                    if (other$gssdBillNo != null) {
                        return false;
                    }
                } else if (!this$gssdBillNo.equals(other$gssdBillNo)) {
                    return false;
                }

                Object this$gssdBrId = this.getGssdBrId();
                Object other$gssdBrId = other.getGssdBrId();
                if (this$gssdBrId == null) {
                    if (other$gssdBrId != null) {
                        return false;
                    }
                } else if (!this$gssdBrId.equals(other$gssdBrId)) {
                    return false;
                }

                label494: {
                    Object this$gssdDate = this.getGssdDate();
                    Object other$gssdDate = other.getGssdDate();
                    if (this$gssdDate == null) {
                        if (other$gssdDate == null) {
                            break label494;
                        }
                    } else if (this$gssdDate.equals(other$gssdDate)) {
                        break label494;
                    }

                    return false;
                }

                label487: {
                    Object this$gssdSerial = this.getGssdSerial();
                    Object other$gssdSerial = other.getGssdSerial();
                    if (this$gssdSerial == null) {
                        if (other$gssdSerial == null) {
                            break label487;
                        }
                    } else if (this$gssdSerial.equals(other$gssdSerial)) {
                        break label487;
                    }

                    return false;
                }

                Object this$gssdProId = this.getGssdProId();
                Object other$gssdProId = other.getGssdProId();
                if (this$gssdProId == null) {
                    if (other$gssdProId != null) {
                        return false;
                    }
                } else if (!this$gssdProId.equals(other$gssdProId)) {
                    return false;
                }

                label473: {
                    Object this$gssdBatchNo = this.getGssdBatchNo();
                    Object other$gssdBatchNo = other.getGssdBatchNo();
                    if (this$gssdBatchNo == null) {
                        if (other$gssdBatchNo == null) {
                            break label473;
                        }
                    } else if (this$gssdBatchNo.equals(other$gssdBatchNo)) {
                        break label473;
                    }

                    return false;
                }

                label466: {
                    Object this$gssdBatch = this.getGssdBatch();
                    Object other$gssdBatch = other.getGssdBatch();
                    if (this$gssdBatch == null) {
                        if (other$gssdBatch == null) {
                            break label466;
                        }
                    } else if (this$gssdBatch.equals(other$gssdBatch)) {
                        break label466;
                    }

                    return false;
                }

                Object this$gssdValidDate = this.getGssdValidDate();
                Object other$gssdValidDate = other.getGssdValidDate();
                if (this$gssdValidDate == null) {
                    if (other$gssdValidDate != null) {
                        return false;
                    }
                } else if (!this$gssdValidDate.equals(other$gssdValidDate)) {
                    return false;
                }

                Object this$gssdStCode = this.getGssdStCode();
                Object other$gssdStCode = other.getGssdStCode();
                if (this$gssdStCode == null) {
                    if (other$gssdStCode != null) {
                        return false;
                    }
                } else if (!this$gssdStCode.equals(other$gssdStCode)) {
                    return false;
                }

                label445: {
                    Object this$gssdPrc1 = this.getGssdPrc1();
                    Object other$gssdPrc1 = other.getGssdPrc1();
                    if (this$gssdPrc1 == null) {
                        if (other$gssdPrc1 == null) {
                            break label445;
                        }
                    } else if (this$gssdPrc1.equals(other$gssdPrc1)) {
                        break label445;
                    }

                    return false;
                }

                label438: {
                    Object this$gssdPrc2 = this.getGssdPrc2();
                    Object other$gssdPrc2 = other.getGssdPrc2();
                    if (this$gssdPrc2 == null) {
                        if (other$gssdPrc2 == null) {
                            break label438;
                        }
                    } else if (this$gssdPrc2.equals(other$gssdPrc2)) {
                        break label438;
                    }

                    return false;
                }

                Object this$gssdQty = this.getGssdQty();
                Object other$gssdQty = other.getGssdQty();
                if (this$gssdQty == null) {
                    if (other$gssdQty != null) {
                        return false;
                    }
                } else if (!this$gssdQty.equals(other$gssdQty)) {
                    return false;
                }

                label424: {
                    Object this$gssdAmt = this.getGssdAmt();
                    Object other$gssdAmt = other.getGssdAmt();
                    if (this$gssdAmt == null) {
                        if (other$gssdAmt == null) {
                            break label424;
                        }
                    } else if (this$gssdAmt.equals(other$gssdAmt)) {
                        break label424;
                    }

                    return false;
                }

                Object this$gssdIntegralExchangeSingle = this.getGssdIntegralExchangeSingle();
                Object other$gssdIntegralExchangeSingle = other.getGssdIntegralExchangeSingle();
                if (this$gssdIntegralExchangeSingle == null) {
                    if (other$gssdIntegralExchangeSingle != null) {
                        return false;
                    }
                } else if (!this$gssdIntegralExchangeSingle.equals(other$gssdIntegralExchangeSingle)) {
                    return false;
                }

                label410: {
                    Object this$gssdIntegralExchangeCollect = this.getGssdIntegralExchangeCollect();
                    Object other$gssdIntegralExchangeCollect = other.getGssdIntegralExchangeCollect();
                    if (this$gssdIntegralExchangeCollect == null) {
                        if (other$gssdIntegralExchangeCollect == null) {
                            break label410;
                        }
                    } else if (this$gssdIntegralExchangeCollect.equals(other$gssdIntegralExchangeCollect)) {
                        break label410;
                    }

                    return false;
                }

                Object this$gssdIntegralExchangeAmtSingle = this.getGssdIntegralExchangeAmtSingle();
                Object other$gssdIntegralExchangeAmtSingle = other.getGssdIntegralExchangeAmtSingle();
                if (this$gssdIntegralExchangeAmtSingle == null) {
                    if (other$gssdIntegralExchangeAmtSingle != null) {
                        return false;
                    }
                } else if (!this$gssdIntegralExchangeAmtSingle.equals(other$gssdIntegralExchangeAmtSingle)) {
                    return false;
                }

                Object this$gssdIntegralExchangeAmtCollect = this.getGssdIntegralExchangeAmtCollect();
                Object other$gssdIntegralExchangeAmtCollect = other.getGssdIntegralExchangeAmtCollect();
                if (this$gssdIntegralExchangeAmtCollect == null) {
                    if (other$gssdIntegralExchangeAmtCollect != null) {
                        return false;
                    }
                } else if (!this$gssdIntegralExchangeAmtCollect.equals(other$gssdIntegralExchangeAmtCollect)) {
                    return false;
                }

                Object this$gssdZkAmt = this.getGssdZkAmt();
                Object other$gssdZkAmt = other.getGssdZkAmt();
                if (this$gssdZkAmt == null) {
                    if (other$gssdZkAmt != null) {
                        return false;
                    }
                } else if (!this$gssdZkAmt.equals(other$gssdZkAmt)) {
                    return false;
                }

                label382: {
                    Object this$gssdZkJfdh = this.getGssdZkJfdh();
                    Object other$gssdZkJfdh = other.getGssdZkJfdh();
                    if (this$gssdZkJfdh == null) {
                        if (other$gssdZkJfdh == null) {
                            break label382;
                        }
                    } else if (this$gssdZkJfdh.equals(other$gssdZkJfdh)) {
                        break label382;
                    }

                    return false;
                }

                label375: {
                    Object this$gssdZkJfdx = this.getGssdZkJfdx();
                    Object other$gssdZkJfdx = other.getGssdZkJfdx();
                    if (this$gssdZkJfdx == null) {
                        if (other$gssdZkJfdx == null) {
                            break label375;
                        }
                    } else if (this$gssdZkJfdx.equals(other$gssdZkJfdx)) {
                        break label375;
                    }

                    return false;
                }

                Object this$gssdZkDzq = this.getGssdZkDzq();
                Object other$gssdZkDzq = other.getGssdZkDzq();
                if (this$gssdZkDzq == null) {
                    if (other$gssdZkDzq != null) {
                        return false;
                    }
                } else if (!this$gssdZkDzq.equals(other$gssdZkDzq)) {
                    return false;
                }

                label361: {
                    Object this$gssdZkDyq = this.getGssdZkDyq();
                    Object other$gssdZkDyq = other.getGssdZkDyq();
                    if (this$gssdZkDyq == null) {
                        if (other$gssdZkDyq == null) {
                            break label361;
                        }
                    } else if (this$gssdZkDyq.equals(other$gssdZkDyq)) {
                        break label361;
                    }

                    return false;
                }

                label354: {
                    Object this$gssdZkPm = this.getGssdZkPm();
                    Object other$gssdZkPm = other.getGssdZkPm();
                    if (this$gssdZkPm == null) {
                        if (other$gssdZkPm == null) {
                            break label354;
                        }
                    } else if (this$gssdZkPm.equals(other$gssdZkPm)) {
                        break label354;
                    }

                    return false;
                }

                Object this$gssdSalerId = this.getGssdSalerId();
                Object other$gssdSalerId = other.getGssdSalerId();
                if (this$gssdSalerId == null) {
                    if (other$gssdSalerId != null) {
                        return false;
                    }
                } else if (!this$gssdSalerId.equals(other$gssdSalerId)) {
                    return false;
                }

                Object this$gssdDoctorId = this.getGssdDoctorId();
                Object other$gssdDoctorId = other.getGssdDoctorId();
                if (this$gssdDoctorId == null) {
                    if (other$gssdDoctorId != null) {
                        return false;
                    }
                } else if (!this$gssdDoctorId.equals(other$gssdDoctorId)) {
                    return false;
                }

                label333: {
                    Object this$gssdPmSubjectId = this.getGssdPmSubjectId();
                    Object other$gssdPmSubjectId = other.getGssdPmSubjectId();
                    if (this$gssdPmSubjectId == null) {
                        if (other$gssdPmSubjectId == null) {
                            break label333;
                        }
                    } else if (this$gssdPmSubjectId.equals(other$gssdPmSubjectId)) {
                        break label333;
                    }

                    return false;
                }

                label326: {
                    Object this$gssdPmId = this.getGssdPmId();
                    Object other$gssdPmId = other.getGssdPmId();
                    if (this$gssdPmId == null) {
                        if (other$gssdPmId == null) {
                            break label326;
                        }
                    } else if (this$gssdPmId.equals(other$gssdPmId)) {
                        break label326;
                    }

                    return false;
                }

                Object this$gssdPmContent = this.getGssdPmContent();
                Object other$gssdPmContent = other.getGssdPmContent();
                if (this$gssdPmContent == null) {
                    if (other$gssdPmContent != null) {
                        return false;
                    }
                } else if (!this$gssdPmContent.equals(other$gssdPmContent)) {
                    return false;
                }

                label312: {
                    Object this$gssdRecipelFlag = this.getGssdRecipelFlag();
                    Object other$gssdRecipelFlag = other.getGssdRecipelFlag();
                    if (this$gssdRecipelFlag == null) {
                        if (other$gssdRecipelFlag == null) {
                            break label312;
                        }
                    } else if (this$gssdRecipelFlag.equals(other$gssdRecipelFlag)) {
                        break label312;
                    }

                    return false;
                }

                Object this$gssdRelationFlag = this.getGssdRelationFlag();
                Object other$gssdRelationFlag = other.getGssdRelationFlag();
                if (this$gssdRelationFlag == null) {
                    if (other$gssdRelationFlag != null) {
                        return false;
                    }
                } else if (!this$gssdRelationFlag.equals(other$gssdRelationFlag)) {
                    return false;
                }

                label298: {
                    Object this$gssdSpecialmedIdcard = this.getGssdSpecialmedIdcard();
                    Object other$gssdSpecialmedIdcard = other.getGssdSpecialmedIdcard();
                    if (this$gssdSpecialmedIdcard == null) {
                        if (other$gssdSpecialmedIdcard == null) {
                            break label298;
                        }
                    } else if (this$gssdSpecialmedIdcard.equals(other$gssdSpecialmedIdcard)) {
                        break label298;
                    }

                    return false;
                }

                Object this$gssdSpecialmedName = this.getGssdSpecialmedName();
                Object other$gssdSpecialmedName = other.getGssdSpecialmedName();
                if (this$gssdSpecialmedName == null) {
                    if (other$gssdSpecialmedName != null) {
                        return false;
                    }
                } else if (!this$gssdSpecialmedName.equals(other$gssdSpecialmedName)) {
                    return false;
                }

                Object this$gssdSpecialmedSex = this.getGssdSpecialmedSex();
                Object other$gssdSpecialmedSex = other.getGssdSpecialmedSex();
                if (this$gssdSpecialmedSex == null) {
                    if (other$gssdSpecialmedSex != null) {
                        return false;
                    }
                } else if (!this$gssdSpecialmedSex.equals(other$gssdSpecialmedSex)) {
                    return false;
                }

                Object this$gssdSpecialmedBirthday = this.getGssdSpecialmedBirthday();
                Object other$gssdSpecialmedBirthday = other.getGssdSpecialmedBirthday();
                if (this$gssdSpecialmedBirthday == null) {
                    if (other$gssdSpecialmedBirthday != null) {
                        return false;
                    }
                } else if (!this$gssdSpecialmedBirthday.equals(other$gssdSpecialmedBirthday)) {
                    return false;
                }

                label270: {
                    Object this$gssdSpecialmedMobile = this.getGssdSpecialmedMobile();
                    Object other$gssdSpecialmedMobile = other.getGssdSpecialmedMobile();
                    if (this$gssdSpecialmedMobile == null) {
                        if (other$gssdSpecialmedMobile == null) {
                            break label270;
                        }
                    } else if (this$gssdSpecialmedMobile.equals(other$gssdSpecialmedMobile)) {
                        break label270;
                    }

                    return false;
                }

                label263: {
                    Object this$gssdSpecialmedAddress = this.getGssdSpecialmedAddress();
                    Object other$gssdSpecialmedAddress = other.getGssdSpecialmedAddress();
                    if (this$gssdSpecialmedAddress == null) {
                        if (other$gssdSpecialmedAddress == null) {
                            break label263;
                        }
                    } else if (this$gssdSpecialmedAddress.equals(other$gssdSpecialmedAddress)) {
                        break label263;
                    }

                    return false;
                }

                Object this$gssdBactchCost = this.getGssdBactchCost();
                Object other$gssdBactchCost = other.getGssdBactchCost();
                if (this$gssdBactchCost == null) {
                    if (other$gssdBactchCost != null) {
                        return false;
                    }
                } else if (!this$gssdBactchCost.equals(other$gssdBactchCost)) {
                    return false;
                }

                label249: {
                    Object this$gssdBatchTax = this.getGssdBatchTax();
                    Object other$gssdBatchTax = other.getGssdBatchTax();
                    if (this$gssdBatchTax == null) {
                        if (other$gssdBatchTax == null) {
                            break label249;
                        }
                    } else if (this$gssdBatchTax.equals(other$gssdBatchTax)) {
                        break label249;
                    }

                    return false;
                }

                label242: {
                    Object this$gssdMovPrice = this.getGssdMovPrice();
                    Object other$gssdMovPrice = other.getGssdMovPrice();
                    if (this$gssdMovPrice == null) {
                        if (other$gssdMovPrice == null) {
                            break label242;
                        }
                    } else if (this$gssdMovPrice.equals(other$gssdMovPrice)) {
                        break label242;
                    }

                    return false;
                }

                Object this$gssdMovTax = this.getGssdMovTax();
                Object other$gssdMovTax = other.getGssdMovTax();
                if (this$gssdMovTax == null) {
                    if (other$gssdMovTax != null) {
                        return false;
                    }
                } else if (!this$gssdMovTax.equals(other$gssdMovTax)) {
                    return false;
                }

                Object this$gssdAdFlag = this.getGssdAdFlag();
                Object other$gssdAdFlag = other.getGssdAdFlag();
                if (this$gssdAdFlag == null) {
                    if (other$gssdAdFlag != null) {
                        return false;
                    }
                } else if (!this$gssdAdFlag.equals(other$gssdAdFlag)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSyncSdSaleDOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gssdBillNo = this.getGssdBillNo();
        result = result * 59 + ($gssdBillNo == null ? 43 : $gssdBillNo.hashCode());
        Object $gssdBrId = this.getGssdBrId();
        result = result * 59 + ($gssdBrId == null ? 43 : $gssdBrId.hashCode());
        Object $gssdDate = this.getGssdDate();
        result = result * 59 + ($gssdDate == null ? 43 : $gssdDate.hashCode());
        Object $gssdSerial = this.getGssdSerial();
        result = result * 59 + ($gssdSerial == null ? 43 : $gssdSerial.hashCode());
        Object $gssdProId = this.getGssdProId();
        result = result * 59 + ($gssdProId == null ? 43 : $gssdProId.hashCode());
        Object $gssdBatchNo = this.getGssdBatchNo();
        result = result * 59 + ($gssdBatchNo == null ? 43 : $gssdBatchNo.hashCode());
        Object $gssdBatch = this.getGssdBatch();
        result = result * 59 + ($gssdBatch == null ? 43 : $gssdBatch.hashCode());
        Object $gssdValidDate = this.getGssdValidDate();
        result = result * 59 + ($gssdValidDate == null ? 43 : $gssdValidDate.hashCode());
        Object $gssdStCode = this.getGssdStCode();
        result = result * 59 + ($gssdStCode == null ? 43 : $gssdStCode.hashCode());
        Object $gssdPrc1 = this.getGssdPrc1();
        result = result * 59 + ($gssdPrc1 == null ? 43 : $gssdPrc1.hashCode());
        Object $gssdPrc2 = this.getGssdPrc2();
        result = result * 59 + ($gssdPrc2 == null ? 43 : $gssdPrc2.hashCode());
        Object $gssdQty = this.getGssdQty();
        result = result * 59 + ($gssdQty == null ? 43 : $gssdQty.hashCode());
        Object $gssdAmt = this.getGssdAmt();
        result = result * 59 + ($gssdAmt == null ? 43 : $gssdAmt.hashCode());
        Object $gssdIntegralExchangeSingle = this.getGssdIntegralExchangeSingle();
        result = result * 59 + ($gssdIntegralExchangeSingle == null ? 43 : $gssdIntegralExchangeSingle.hashCode());
        Object $gssdIntegralExchangeCollect = this.getGssdIntegralExchangeCollect();
        result = result * 59 + ($gssdIntegralExchangeCollect == null ? 43 : $gssdIntegralExchangeCollect.hashCode());
        Object $gssdIntegralExchangeAmtSingle = this.getGssdIntegralExchangeAmtSingle();
        result = result * 59 + ($gssdIntegralExchangeAmtSingle == null ? 43 : $gssdIntegralExchangeAmtSingle.hashCode());
        Object $gssdIntegralExchangeAmtCollect = this.getGssdIntegralExchangeAmtCollect();
        result = result * 59 + ($gssdIntegralExchangeAmtCollect == null ? 43 : $gssdIntegralExchangeAmtCollect.hashCode());
        Object $gssdZkAmt = this.getGssdZkAmt();
        result = result * 59 + ($gssdZkAmt == null ? 43 : $gssdZkAmt.hashCode());
        Object $gssdZkJfdh = this.getGssdZkJfdh();
        result = result * 59 + ($gssdZkJfdh == null ? 43 : $gssdZkJfdh.hashCode());
        Object $gssdZkJfdx = this.getGssdZkJfdx();
        result = result * 59 + ($gssdZkJfdx == null ? 43 : $gssdZkJfdx.hashCode());
        Object $gssdZkDzq = this.getGssdZkDzq();
        result = result * 59 + ($gssdZkDzq == null ? 43 : $gssdZkDzq.hashCode());
        Object $gssdZkDyq = this.getGssdZkDyq();
        result = result * 59 + ($gssdZkDyq == null ? 43 : $gssdZkDyq.hashCode());
        Object $gssdZkPm = this.getGssdZkPm();
        result = result * 59 + ($gssdZkPm == null ? 43 : $gssdZkPm.hashCode());
        Object $gssdSalerId = this.getGssdSalerId();
        result = result * 59 + ($gssdSalerId == null ? 43 : $gssdSalerId.hashCode());
        Object $gssdDoctorId = this.getGssdDoctorId();
        result = result * 59 + ($gssdDoctorId == null ? 43 : $gssdDoctorId.hashCode());
        Object $gssdPmSubjectId = this.getGssdPmSubjectId();
        result = result * 59 + ($gssdPmSubjectId == null ? 43 : $gssdPmSubjectId.hashCode());
        Object $gssdPmId = this.getGssdPmId();
        result = result * 59 + ($gssdPmId == null ? 43 : $gssdPmId.hashCode());
        Object $gssdPmContent = this.getGssdPmContent();
        result = result * 59 + ($gssdPmContent == null ? 43 : $gssdPmContent.hashCode());
        Object $gssdRecipelFlag = this.getGssdRecipelFlag();
        result = result * 59 + ($gssdRecipelFlag == null ? 43 : $gssdRecipelFlag.hashCode());
        Object $gssdRelationFlag = this.getGssdRelationFlag();
        result = result * 59 + ($gssdRelationFlag == null ? 43 : $gssdRelationFlag.hashCode());
        Object $gssdSpecialmedIdcard = this.getGssdSpecialmedIdcard();
        result = result * 59 + ($gssdSpecialmedIdcard == null ? 43 : $gssdSpecialmedIdcard.hashCode());
        Object $gssdSpecialmedName = this.getGssdSpecialmedName();
        result = result * 59 + ($gssdSpecialmedName == null ? 43 : $gssdSpecialmedName.hashCode());
        Object $gssdSpecialmedSex = this.getGssdSpecialmedSex();
        result = result * 59 + ($gssdSpecialmedSex == null ? 43 : $gssdSpecialmedSex.hashCode());
        Object $gssdSpecialmedBirthday = this.getGssdSpecialmedBirthday();
        result = result * 59 + ($gssdSpecialmedBirthday == null ? 43 : $gssdSpecialmedBirthday.hashCode());
        Object $gssdSpecialmedMobile = this.getGssdSpecialmedMobile();
        result = result * 59 + ($gssdSpecialmedMobile == null ? 43 : $gssdSpecialmedMobile.hashCode());
        Object $gssdSpecialmedAddress = this.getGssdSpecialmedAddress();
        result = result * 59 + ($gssdSpecialmedAddress == null ? 43 : $gssdSpecialmedAddress.hashCode());
        Object $gssdBactchCost = this.getGssdBactchCost();
        result = result * 59 + ($gssdBactchCost == null ? 43 : $gssdBactchCost.hashCode());
        Object $gssdBatchTax = this.getGssdBatchTax();
        result = result * 59 + ($gssdBatchTax == null ? 43 : $gssdBatchTax.hashCode());
        Object $gssdMovPrice = this.getGssdMovPrice();
        result = result * 59 + ($gssdMovPrice == null ? 43 : $gssdMovPrice.hashCode());
        Object $gssdMovTax = this.getGssdMovTax();
        result = result * 59 + ($gssdMovTax == null ? 43 : $gssdMovTax.hashCode());
        Object $gssdAdFlag = this.getGssdAdFlag();
        result = result * 59 + ($gssdAdFlag == null ? 43 : $gssdAdFlag.hashCode());
        return result;
    }

    public String toString() {
        return "GetSyncSdSaleDOutData(clientId=" + this.getClientId() + ", gssdBillNo=" + this.getGssdBillNo() + ", gssdBrId=" + this.getGssdBrId() + ", gssdDate=" + this.getGssdDate() + ", gssdSerial=" + this.getGssdSerial() + ", gssdProId=" + this.getGssdProId() + ", gssdBatchNo=" + this.getGssdBatchNo() + ", gssdBatch=" + this.getGssdBatch() + ", gssdValidDate=" + this.getGssdValidDate() + ", gssdStCode=" + this.getGssdStCode() + ", gssdPrc1=" + this.getGssdPrc1() + ", gssdPrc2=" + this.getGssdPrc2() + ", gssdQty=" + this.getGssdQty() + ", gssdAmt=" + this.getGssdAmt() + ", gssdIntegralExchangeSingle=" + this.getGssdIntegralExchangeSingle() + ", gssdIntegralExchangeCollect=" + this.getGssdIntegralExchangeCollect() + ", gssdIntegralExchangeAmtSingle=" + this.getGssdIntegralExchangeAmtSingle() + ", gssdIntegralExchangeAmtCollect=" + this.getGssdIntegralExchangeAmtCollect() + ", gssdZkAmt=" + this.getGssdZkAmt() + ", gssdZkJfdh=" + this.getGssdZkJfdh() + ", gssdZkJfdx=" + this.getGssdZkJfdx() + ", gssdZkDzq=" + this.getGssdZkDzq() + ", gssdZkDyq=" + this.getGssdZkDyq() + ", gssdZkPm=" + this.getGssdZkPm() + ", gssdSalerId=" + this.getGssdSalerId() + ", gssdDoctorId=" + this.getGssdDoctorId() + ", gssdPmSubjectId=" + this.getGssdPmSubjectId() + ", gssdPmId=" + this.getGssdPmId() + ", gssdPmContent=" + this.getGssdPmContent() + ", gssdRecipelFlag=" + this.getGssdRecipelFlag() + ", gssdRelationFlag=" + this.getGssdRelationFlag() + ", gssdSpecialmedIdcard=" + this.getGssdSpecialmedIdcard() + ", gssdSpecialmedName=" + this.getGssdSpecialmedName() + ", gssdSpecialmedSex=" + this.getGssdSpecialmedSex() + ", gssdSpecialmedBirthday=" + this.getGssdSpecialmedBirthday() + ", gssdSpecialmedMobile=" + this.getGssdSpecialmedMobile() + ", gssdSpecialmedAddress=" + this.getGssdSpecialmedAddress() + ", gssdBactchCost=" + this.getGssdBactchCost() + ", gssdBatchTax=" + this.getGssdBatchTax() + ", gssdMovPrice=" + this.getGssdMovPrice() + ", gssdMovTax=" + this.getGssdMovTax() + ", gssdAdFlag=" + this.getGssdAdFlag() + ")";
    }
}
