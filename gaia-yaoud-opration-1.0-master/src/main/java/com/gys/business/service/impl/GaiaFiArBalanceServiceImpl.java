package com.gys.business.service.impl;

import com.gys.business.mapper.entity.GaiaFiArBalance;
import com.gys.business.mapper.GaiaFiArBalanceMapper;
import com.gys.business.service.GaiaFiArBalanceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 应收期末余额(GaiaFiArBalance)表服务实现类
 *
 * @author makejava
 * @since 2021-09-18 15:29:15
 */
@Service("gaiaFiArBalanceService")
public class GaiaFiArBalanceServiceImpl implements GaiaFiArBalanceService {
    @Resource
    private GaiaFiArBalanceMapper gaiaFiArBalanceDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public GaiaFiArBalance queryById(Long id) {
        return this.gaiaFiArBalanceDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<GaiaFiArBalance> queryAllByLimit(int offset, int limit) {
        return this.gaiaFiArBalanceDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param gaiaFiArBalance 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaFiArBalance insert(GaiaFiArBalance gaiaFiArBalance) {
        this.gaiaFiArBalanceDao.insert(gaiaFiArBalance);
        return gaiaFiArBalance;
    }

    /**
     * 修改数据
     *
     * @param gaiaFiArBalance 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaFiArBalance update(GaiaFiArBalance gaiaFiArBalance) {
        this.gaiaFiArBalanceDao.update(gaiaFiArBalance);
        return this.queryById(gaiaFiArBalance.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.gaiaFiArBalanceDao.deleteById(id) > 0;
    }
}
