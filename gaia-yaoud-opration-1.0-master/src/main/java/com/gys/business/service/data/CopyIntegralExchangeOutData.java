package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdIntegralExchangeSetTime;
import lombok.Data;

import java.util.List;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2021/11/30 11:03
 */
@Data
public class CopyIntegralExchangeOutData {
    /**
     * 活动门店
     */
    private List<IntegralExchangeStore>  stoCodeList;

    /**
     * 会员卡类型
     */
    private String memberClass;

    /**
     * 开始日期
     */
    private String startDate;

    /**
     * 结束日期
     */
    private String endDate;

    /**
     * 日期频率
     */
    private String dateFrequency;

    /**
     * 星期频率
     */
    private String weekFrequency;

    /**
     * 起止时间
     */
    private List<GaiaSdIntegralExchangeSetTime> timeList;

    /**
     * 积分换购明细
     */
    private List<IntegralExchangeSetDetail> proList;
}
