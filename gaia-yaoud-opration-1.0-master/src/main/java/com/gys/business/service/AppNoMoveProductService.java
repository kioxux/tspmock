package com.gys.business.service;

import com.gys.business.service.data.AppNoMoveInData;
import com.gys.business.service.data.NoMoveProInData;
import com.gys.common.data.JsonResult;

public interface AppNoMoveProductService {

    JsonResult getBigTypeList(AppNoMoveInData inData);

    JsonResult getMidTypeList(AppNoMoveInData inData);

    JsonResult getStoreList(NoMoveProInData inData);
}
