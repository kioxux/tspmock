package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 添加实体
 *
 * @author xiaoyuan on 2020/8/13
 */
@Data
public class PendingOrderInData implements Serializable {
    private static final long serialVersionUID = -7244921568135872426L;

    private String client;
    private String brId;

    /**
     *  总价
     */
    private String  totalPrice;

    /**
     * 会员卡号
     */
    private String cardNum;

    /**
     * 表格数据
     */
    private List<RebornData> dataList;
}
