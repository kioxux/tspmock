package com.gys.business.service;

import com.gys.business.service.data.Menu.MenuInData;
import com.gys.business.service.data.Menu.MenuOutData;
import com.gys.business.service.data.Menu.MenuTree;
import com.gys.business.service.data.MenuConfig.MenuConfigInput;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

public interface MenuService {

    void insertMenu(GetLoginOutData userInfo, MenuInData inData);

    void updateMenu(GetLoginOutData userInfo, MenuInData inData);

    List<Object> selectMenuTree();

    MenuOutData selectMenu(MenuInData inData);

    void deleteMenu(GetLoginOutData userInfo, MenuInData inData);

    void updateMenuTree(GetLoginOutData userInfo, List<MenuTree> inData);

    List<Object> selectMenuConfig(MenuConfigInput inData);
}
