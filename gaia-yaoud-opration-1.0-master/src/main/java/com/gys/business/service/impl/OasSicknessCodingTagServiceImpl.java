package com.gys.business.service.impl;

import com.gys.business.mapper.OasSicknessCodingTagMapper;
import com.gys.business.service.IOasSicknessCodingTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * <p>
 * 疾病大类-疾病中类关系表 服务实现类
 * </p>
 *
 * @author flynn
 * @since 2021-09-02
 */
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class OasSicknessCodingTagServiceImpl implements IOasSicknessCodingTagService {
    @Autowired
    private OasSicknessCodingTagMapper oasSicknessCodingTagMapper;


}
