package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class DsfPordWtproOutData {

    /**
     * 商品名称
     */
    private String dsfProName;

    /**
     * 三方编号
     */
    private String idDsf;

    /**
     * 规格
     */
    private String dsfProSpecs;

    /**
     * 生产厂家
     */
    private String dsfProFactoryName;

    /**
     * 产地
     */
    private String dsfProPlace;

    /**
     * 国际条形码
     */
    private String dsfProBarcode;

    /**
     * 批准文号
     */
    private String dsfProRegisterNo;

    /**
     * 单位
     */
    private String dsfProUnit;

    /**
     * 委托配送第三方商品编码对照表主键编号
     */
    private Long id;
    /**
     *商品编码
     */
    private String proSelfCode;

    /**
     * 转换比率
     */
    private BigDecimal proRate;

    /**
     *  公司商品名称
     */
    private String proName;


    /**
     * 公司商品规格
     */
    private String proSpecs;

    /**
     * 公司商品生产厂家
     */
    private String proFactoryName;

    /**
     * 公司商品产地
     */
    private String proPlace;

    /**
     * 公司商品国际条形码
     */
    private String proBarcode;

    /**
     * 公司商品批准文号
     */
    private String proRegisterNo;

    /**
     * 单位
     */
    private String proUnit;

}
