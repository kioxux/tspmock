//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class DeviceInfoOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "设备编号")
    private String gsdiId;
    @ApiModelProperty(value = "设备名称")
    private String gsdiName;
    @ApiModelProperty(value = "门店编码")
    private String gsthBrId;
    @ApiModelProperty(value = "门店名称")
    private String gsthBrName;
    @ApiModelProperty(value = "设备数量")
    private String gsdiQty;
    @ApiModelProperty(value = "身边型号")
    private String gsdiModel;
    @ApiModelProperty(value = "生产厂家")
    private String gsdiFactory;
    @ApiModelProperty(value = "启用日期")
    private String gsdiStartDate;
    @ApiModelProperty(value = "登记日期")
    private String gsdiUpdateDat;
    @ApiModelProperty(value = "登记人员")
    private String gsthUpdateEmp;
    @ApiModelProperty(value = "是否有说明书：1-是 0-否")
    private String gsdiInstruction;
    @ApiModelProperty(value = "是否检验合格：1-是 0-否")
    private String gsdiQualify;
    @ApiModelProperty(value = "是否复核：1-是 0-否")
    private String gsdiCheck;
    @ApiModelProperty(value = "序号")
    private Integer index;
    @ApiModelProperty(hidden = true, value = "登记人员")
    private String userName;

    public DeviceInfoOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsdiId() {
        return this.gsdiId;
    }

    public String getGsdiName() {
        return this.gsdiName;
    }

    public String getGsthBrId() {
        return this.gsthBrId;
    }

    public String getGsthBrName() {
        return this.gsthBrName;
    }

    public String getGsdiQty() {
        return this.gsdiQty;
    }

    public String getGsdiModel() {
        return this.gsdiModel;
    }

    public String getGsdiFactory() {
        return this.gsdiFactory;
    }

    public String getGsdiStartDate() {
        return this.gsdiStartDate;
    }

    public String getGsdiUpdateDat() {
        return this.gsdiUpdateDat;
    }

    public String getGsthUpdateEmp() {
        return this.gsthUpdateEmp;
    }

    public String getGsdiInstruction() {
        return this.gsdiInstruction;
    }

    public String getGsdiQualify() {
        return this.gsdiQualify;
    }

    public String getGsdiCheck() {
        return this.gsdiCheck;
    }

    public Integer getIndex() {
        return this.index;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsdiId(final String gsdiId) {
        this.gsdiId = gsdiId;
    }

    public void setGsdiName(final String gsdiName) {
        this.gsdiName = gsdiName;
    }

    public void setGsthBrId(final String gsthBrId) {
        this.gsthBrId = gsthBrId;
    }

    public void setGsthBrName(final String gsthBrName) {
        this.gsthBrName = gsthBrName;
    }

    public void setGsdiQty(final String gsdiQty) {
        this.gsdiQty = gsdiQty;
    }

    public void setGsdiModel(final String gsdiModel) {
        this.gsdiModel = gsdiModel;
    }

    public void setGsdiFactory(final String gsdiFactory) {
        this.gsdiFactory = gsdiFactory;
    }

    public void setGsdiStartDate(final String gsdiStartDate) {
        this.gsdiStartDate = gsdiStartDate;
    }

    public void setGsdiUpdateDat(final String gsdiUpdateDat) {
        this.gsdiUpdateDat = gsdiUpdateDat;
    }

    public void setGsthUpdateEmp(final String gsthUpdateEmp) {
        this.gsthUpdateEmp = gsthUpdateEmp;
    }

    public void setGsdiInstruction(final String gsdiInstruction) {
        this.gsdiInstruction = gsdiInstruction;
    }

    public void setGsdiQualify(final String gsdiQualify) {
        this.gsdiQualify = gsdiQualify;
    }

    public void setGsdiCheck(final String gsdiCheck) {
        this.gsdiCheck = gsdiCheck;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof DeviceInfoOutData)) {
            return false;
        } else {
            DeviceInfoOutData other = (DeviceInfoOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label203: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label203;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label203;
                    }

                    return false;
                }

                Object this$gsdiId = this.getGsdiId();
                Object other$gsdiId = other.getGsdiId();
                if (this$gsdiId == null) {
                    if (other$gsdiId != null) {
                        return false;
                    }
                } else if (!this$gsdiId.equals(other$gsdiId)) {
                    return false;
                }

                Object this$gsdiName = this.getGsdiName();
                Object other$gsdiName = other.getGsdiName();
                if (this$gsdiName == null) {
                    if (other$gsdiName != null) {
                        return false;
                    }
                } else if (!this$gsdiName.equals(other$gsdiName)) {
                    return false;
                }

                label182: {
                    Object this$gsthBrId = this.getGsthBrId();
                    Object other$gsthBrId = other.getGsthBrId();
                    if (this$gsthBrId == null) {
                        if (other$gsthBrId == null) {
                            break label182;
                        }
                    } else if (this$gsthBrId.equals(other$gsthBrId)) {
                        break label182;
                    }

                    return false;
                }

                label175: {
                    Object this$gsthBrName = this.getGsthBrName();
                    Object other$gsthBrName = other.getGsthBrName();
                    if (this$gsthBrName == null) {
                        if (other$gsthBrName == null) {
                            break label175;
                        }
                    } else if (this$gsthBrName.equals(other$gsthBrName)) {
                        break label175;
                    }

                    return false;
                }

                label168: {
                    Object this$gsdiQty = this.getGsdiQty();
                    Object other$gsdiQty = other.getGsdiQty();
                    if (this$gsdiQty == null) {
                        if (other$gsdiQty == null) {
                            break label168;
                        }
                    } else if (this$gsdiQty.equals(other$gsdiQty)) {
                        break label168;
                    }

                    return false;
                }

                Object this$gsdiModel = this.getGsdiModel();
                Object other$gsdiModel = other.getGsdiModel();
                if (this$gsdiModel == null) {
                    if (other$gsdiModel != null) {
                        return false;
                    }
                } else if (!this$gsdiModel.equals(other$gsdiModel)) {
                    return false;
                }

                label154: {
                    Object this$gsdiFactory = this.getGsdiFactory();
                    Object other$gsdiFactory = other.getGsdiFactory();
                    if (this$gsdiFactory == null) {
                        if (other$gsdiFactory == null) {
                            break label154;
                        }
                    } else if (this$gsdiFactory.equals(other$gsdiFactory)) {
                        break label154;
                    }

                    return false;
                }

                Object this$gsdiStartDate = this.getGsdiStartDate();
                Object other$gsdiStartDate = other.getGsdiStartDate();
                if (this$gsdiStartDate == null) {
                    if (other$gsdiStartDate != null) {
                        return false;
                    }
                } else if (!this$gsdiStartDate.equals(other$gsdiStartDate)) {
                    return false;
                }

                label140: {
                    Object this$gsdiUpdateDat = this.getGsdiUpdateDat();
                    Object other$gsdiUpdateDat = other.getGsdiUpdateDat();
                    if (this$gsdiUpdateDat == null) {
                        if (other$gsdiUpdateDat == null) {
                            break label140;
                        }
                    } else if (this$gsdiUpdateDat.equals(other$gsdiUpdateDat)) {
                        break label140;
                    }

                    return false;
                }

                Object this$gsthUpdateEmp = this.getGsthUpdateEmp();
                Object other$gsthUpdateEmp = other.getGsthUpdateEmp();
                if (this$gsthUpdateEmp == null) {
                    if (other$gsthUpdateEmp != null) {
                        return false;
                    }
                } else if (!this$gsthUpdateEmp.equals(other$gsthUpdateEmp)) {
                    return false;
                }

                Object this$gsdiInstruction = this.getGsdiInstruction();
                Object other$gsdiInstruction = other.getGsdiInstruction();
                if (this$gsdiInstruction == null) {
                    if (other$gsdiInstruction != null) {
                        return false;
                    }
                } else if (!this$gsdiInstruction.equals(other$gsdiInstruction)) {
                    return false;
                }

                label119: {
                    Object this$gsdiQualify = this.getGsdiQualify();
                    Object other$gsdiQualify = other.getGsdiQualify();
                    if (this$gsdiQualify == null) {
                        if (other$gsdiQualify == null) {
                            break label119;
                        }
                    } else if (this$gsdiQualify.equals(other$gsdiQualify)) {
                        break label119;
                    }

                    return false;
                }

                label112: {
                    Object this$gsdiCheck = this.getGsdiCheck();
                    Object other$gsdiCheck = other.getGsdiCheck();
                    if (this$gsdiCheck == null) {
                        if (other$gsdiCheck == null) {
                            break label112;
                        }
                    } else if (this$gsdiCheck.equals(other$gsdiCheck)) {
                        break label112;
                    }

                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                Object this$userName = this.getUserName();
                Object other$userName = other.getUserName();
                if (this$userName == null) {
                    if (other$userName != null) {
                        return false;
                    }
                } else if (!this$userName.equals(other$userName)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof DeviceInfoOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsdiId = this.getGsdiId();
        result = result * 59 + ($gsdiId == null ? 43 : $gsdiId.hashCode());
        Object $gsdiName = this.getGsdiName();
        result = result * 59 + ($gsdiName == null ? 43 : $gsdiName.hashCode());
        Object $gsthBrId = this.getGsthBrId();
        result = result * 59 + ($gsthBrId == null ? 43 : $gsthBrId.hashCode());
        Object $gsthBrName = this.getGsthBrName();
        result = result * 59 + ($gsthBrName == null ? 43 : $gsthBrName.hashCode());
        Object $gsdiQty = this.getGsdiQty();
        result = result * 59 + ($gsdiQty == null ? 43 : $gsdiQty.hashCode());
        Object $gsdiModel = this.getGsdiModel();
        result = result * 59 + ($gsdiModel == null ? 43 : $gsdiModel.hashCode());
        Object $gsdiFactory = this.getGsdiFactory();
        result = result * 59 + ($gsdiFactory == null ? 43 : $gsdiFactory.hashCode());
        Object $gsdiStartDate = this.getGsdiStartDate();
        result = result * 59 + ($gsdiStartDate == null ? 43 : $gsdiStartDate.hashCode());
        Object $gsdiUpdateDat = this.getGsdiUpdateDat();
        result = result * 59 + ($gsdiUpdateDat == null ? 43 : $gsdiUpdateDat.hashCode());
        Object $gsthUpdateEmp = this.getGsthUpdateEmp();
        result = result * 59 + ($gsthUpdateEmp == null ? 43 : $gsthUpdateEmp.hashCode());
        Object $gsdiInstruction = this.getGsdiInstruction();
        result = result * 59 + ($gsdiInstruction == null ? 43 : $gsdiInstruction.hashCode());
        Object $gsdiQualify = this.getGsdiQualify();
        result = result * 59 + ($gsdiQualify == null ? 43 : $gsdiQualify.hashCode());
        Object $gsdiCheck = this.getGsdiCheck();
        result = result * 59 + ($gsdiCheck == null ? 43 : $gsdiCheck.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $userName = this.getUserName();
        result = result * 59 + ($userName == null ? 43 : $userName.hashCode());
        return result;
    }

    public String toString() {
        return "DeviceInfoOutData(clientId=" + this.getClientId() + ", gsdiId=" + this.getGsdiId() + ", gsdiName=" + this.getGsdiName() + ", gsthBrId=" + this.getGsthBrId() + ", gsthBrName=" + this.getGsthBrName() + ", gsdiQty=" + this.getGsdiQty() + ", gsdiModel=" + this.getGsdiModel() + ", gsdiFactory=" + this.getGsdiFactory() + ", gsdiStartDate=" + this.getGsdiStartDate() + ", gsdiUpdateDat=" + this.getGsdiUpdateDat() + ", gsthUpdateEmp=" + this.getGsthUpdateEmp() + ", gsdiInstruction=" + this.getGsdiInstruction() + ", gsdiQualify=" + this.getGsdiQualify() + ", gsdiCheck=" + this.getGsdiCheck() + ", index=" + this.getIndex() + ", userName=" + this.getUserName() + ")";
    }
}
