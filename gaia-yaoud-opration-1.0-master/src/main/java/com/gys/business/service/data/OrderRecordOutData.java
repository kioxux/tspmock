package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xiaoyuan on 2020/12/24
 */
@Data
public class OrderRecordOutData implements Serializable {

    private static final long serialVersionUID = 4866717188255584295L;

    @ApiModelProperty(value = "序号")
    private Integer index;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 订购门店
     */
    @ApiModelProperty(value = "订购门店编码")
    private String gsphBrId;

    /**
     * 订购名店名称
     */
    @ApiModelProperty(value = "订购名店名称")
    private String brName;

    /**
     * 订购单号
     */
    @ApiModelProperty(value = "订购单号")
    private String gsphVoucherId;

    /**
     * 订购日期
     */
    @ApiModelProperty(value = "订购日期")
    private String gsphDate;

    /**
     * 订购时间
     */
    @ApiModelProperty(value = "订购时间")
    private String gsphTime;

    /**
     * 订购金额
     */
    @ApiModelProperty(value = "订购金额")
    private BigDecimal gsphAmt;

    /**
     * 顾客姓名
     */
    @ApiModelProperty(value = "顾客姓名")
    private String gsphCostoerName;

    /**
     * 顾客手机
     */
    @ApiModelProperty(value = "顾客手机")
    private String gsphCostoerMobile;

    /**
     * 顾客身份证
     */
    @ApiModelProperty(value = "顾客身份证")
    private String gsphCostoerId;
    /**
     * 顾客备注
     */
    @ApiModelProperty(value = "顾客备注")
    private String gsphcostoerRemarks;

    /**
     * 会员卡号
     */
    @ApiModelProperty(value = "会员卡号")
    private String gsphMemberId;

    /**
     * 录入员工
     */
    @ApiModelProperty(value = "录入员工")
    private String gsphEmp;

    /**
     * 是否调用
     */
    @ApiModelProperty(value = "是否调用")
    private String gsphFlag;

    /**
     * 调用日期
     */
    @ApiModelProperty(value = "调用日期")
    private String gsphUpdateDate;

    /**
     * 调用时间
     */
    @ApiModelProperty(value = "调用时间")
    private String gsphUpdateTime;

    /**
     * 销售单号
     */
    @ApiModelProperty(value = "销售单号")
    private String gsphBillNo;

    /**
     * 单据类型：DG:订购单；HX:核销单
     */
    @ApiModelProperty(value = "订购状态")
    private String gsphType;

    /**
     * 订购单状态：1正常，2退款，3核销
     */
    @ApiModelProperty(value = "订购单状态")
    private String gsphStatus;
}
