package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "APP盘点请求实体")
public class GetPhysicalCountInData {
    @ApiModelProperty(hidden = true)
    private String clientId;

    @ApiModelProperty(hidden = true)
    private String gspcBrId;
    private String gspcDate;
    private String gspcType;

    private String gspcRowNo;

    private String gspcTime;
    @ApiModelProperty(value = "商品编号")
    private String gspcProId;

    @ApiModelProperty(value = "批号")
    private String gspcBatchNo;

    @ApiModelProperty(value = "库存数量")
    private String gspcStockQty;

    @ApiModelProperty(value = "复盘数量")
    private String gspcQty;
    private String gsplArea;
    private String gsplGroup;
    private String gsplShelf;
    private String gsplStorey;
    private String gsplSeat;
    private String gspgId;
    @ApiModelProperty(value = "差异单号")
    private String gspcdVoucherId;

    @ApiModelProperty(value = "盘点单号")
    private String gspcVoucherId;

    private String gspcdPcSecondQty;

    @ApiModelProperty(value = "复盘差异")
    private String gspcdDiffSecondQty;
    private String diffQty;
    private List<GetPhysicalCountInData> physicals;
}
