package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhangdong
 * @date 2021/4/15 14:00
 */
@Data
public class TakeAwayOrderBean {

    @NotNull
    private String orderNo;
    @NotNull
    private String type;//取消原因分类码
    @NotNull
    private String reason;//取消原因分类描述
}
