package com.gys.business.service.data;

import com.gys.business.service.data.integralExchange.IntegralExchangeProductVo;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @desc:
 * @author: RULAISZ
 * @createTime: 2021/11/29 10:28
 */
@Data
public class IntegralExchangeSetInData {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 门店
     */
    @NotEmpty(message = "提示：请选择要设置的门店")
    private List<String> stoCodeList;

    /**
     * 门店类别
     */
    private String stoGroup;

    /**
     * 会员卡等级 0：全选 多选逗号分隔
     */
    @NotBlank(message = "提示：请选择会员卡等级")
    private String memberClass;

    /**
     * 开始日期 yyyy-MM-dd HH:mm:ss
     */
    @NotNull(message = "提示：请选择开始时间")
    private Date startDate;

    /**
     * 结束日期 yyyy-MM-dd HH:mm:ss
     */
    @NotNull(message = "提示：请选择结束时间")
    private Date endDate;

    /**
     * 日期频率 逗号分隔
     */
    private String dateFrequency;

    /**
     * 星期频率 逗号分隔
     */
    private String weekFrequency;

    /**
     * 时间段
     */
    private List<String> timeInterval;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 商品列表
     */
    private List<IntegralExchangeProductVo> proList;

}
