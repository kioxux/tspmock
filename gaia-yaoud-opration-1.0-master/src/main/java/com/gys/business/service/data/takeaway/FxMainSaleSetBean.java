package com.gys.business.service.data.takeaway;

import lombok.Data;

/**
 * @author ZhangDong
 * @date 2021/8/30 14:27
 */
@Data
public class FxMainSaleSetBean {

    private String storeLogo;//门店标识

    private String client;
    private String stoCode;
}
