package com.gys.business.service.data;

import lombok.Data;


import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2020/12/24
 */
@Data
public class OrderSaveInData implements Serializable {
    private static final long serialVersionUID = -1171156046677314877L;

    /**
     * 加盟
     */
    private String client;

    /**
     * 店号
     */
    private String brId;

    /**
     * 原订购单号
     */
    private String voucherId;

    /**
     * 订单金额
     */
    private String gsphAmt;

    /**
     * 订单详细
     */
    private List<PreordDDetailsOutData> details;

    /**
     * 支付详情
     */
    private List<PreordPayMsgDetails> payMsgDetails;

    /**
     * 顾客姓名
     */
    private String costoerName;

    /**
     * 顾客手机
     */
    private String costoerMobile;

    /**
     * 身份证
     */
    private String costoerId;

    /**
     * 会员卡号
     */
    private String memberId;

    /**
     * 生成备注
     */
    private String costoerRemarks;
}
