package com.gys.business.service.data;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class InventoryInquiryOutData implements Serializable {
    private static final long serialVersionUID = 8458845923235734876L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "店号")
    private String gssmBrId;

    @ApiModelProperty(value = "店店名")
    private String gssmBrName;

    @ExcelProperty(value = "商品编号",index = 1)
    @ApiModelProperty(value = "商品编号")
    private String gssmProId;

    @ApiModelProperty(value = "通用名称")
    private String gssmProCommonname;
    @ApiModelProperty(value = "国际条形码")
    private String proBarcode;

    @ExcelProperty(value = "商品编号",index = 2)
    @ApiModelProperty(value = "商品名")
    private String gssmProName;

    @ExcelProperty(value = "零售价",index = 3)
    @ApiModelProperty(value = "零售价")
    private BigDecimal retailPrice;

    @ApiModelProperty(value = "会员价")
    private BigDecimal memberPrice;
    @ApiModelProperty(value = "会员日价")
    private BigDecimal memberDayPrice;

    @ExcelProperty(value = "批号",index = 4)
    @ApiModelProperty(value = "批号")
    private String gssmBatchNo;

    @ApiModelProperty(value = "批次")
    private String gssmBatch;

    @ExcelProperty(value = "有效期至",index = 5)
    @ApiModelProperty(value = "效期")
    private String validUntil;

    @ApiModelProperty(value = "医保类型")
    private String yblx;

    @ApiModelProperty(value = "医保编码")
    private String medProdctCode;

    @ApiModelProperty(value = "是否医保")
    private String ifMed;

    @ExcelProperty(value = "库存",index = 6)
    @ApiModelProperty(value = "数量")
    private BigDecimal qty;

    @ExcelProperty(value = "厂家",index = 7)
    @ApiModelProperty(value = "生产企业")
    private String factory;

    @ExcelProperty(value = "产地",index = 8)
    @ApiModelProperty(value = "产地")
    private String origin;

    @ExcelProperty(value = "剂型",index = 9)
    @ApiModelProperty(value = "剂型")
    private String dosageForm;

    @ExcelProperty(value = "单位",index = 10)
    @ApiModelProperty(value = "计量单位")
    private String unit;

    @ExcelProperty(value = "规格",index = 11)
    @ApiModelProperty(value = "规格")
    private String format;

    @ExcelProperty(value = "批准文号",index = 12)
    @ApiModelProperty(value = "批准文号")
    private String approvalNum;

    @ApiModelProperty(value = "序号")
    private Integer index;

    @ApiModelProperty(value = "成本价 ps:暂时不用")
    private BigDecimal matMovPrice;

    @ExcelProperty(value = "成本价",index = 14)
    @ApiModelProperty(value = "加点后成本价")
    private BigDecimal addPrice;

    @ExcelProperty(value = "成本额",index = 15)
    @ApiModelProperty(value = "成本额")
    private BigDecimal costAmount;

    @ApiModelProperty(value = "零售额")
    private BigDecimal retailSales ;

    @ExcelProperty(value = "成本额",index = 13)
    @ApiModelProperty(value = "定位")
    private String proPosition;

    @ApiModelProperty(value = "货位号")
    private String hwhBm;

    @ApiModelProperty(value = "退库未确认数量")
    private BigDecimal unconfirmedWithdrawalQty;

    @ApiModelProperty(value = "商品大类编码")
    private String bigClass;
    @ApiModelProperty(value = "商品中类编码")
    private String midClass;
    @ApiModelProperty(value = "商品分类编码")
    private String proClass;
    @ApiModelProperty(value = "供应商编码")
    private String supSelfCode;
    @ApiModelProperty(value = "供应商编名称")
    private String supName;
    @ApiModelProperty(value = "效期天数")
    private String expiryData;
    @ApiModelProperty(value = "药品本位码")
    private String proBasicCode;
    @ApiModelProperty(value = "国家医保编码")
    private String proMedProdctCode;


    //商品自分类
    private String prosClass;
    //销售级别
    private String saleClass;
    //禁止采购
    private String purchase;
    //自定义1
    private String zdy1;
    //自定义2
    private String zdy2;
    //自定义3
    private String zdy3;
    //自定义4
    private String zdy4;
    //自定义5
    private String zdy5;
}
