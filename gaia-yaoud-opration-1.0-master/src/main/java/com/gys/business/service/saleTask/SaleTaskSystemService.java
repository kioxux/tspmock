package com.gys.business.service.saleTask;

import com.gys.business.mapper.entity.GaiaSaletaskHplanSystem;
import com.gys.business.service.data.MonthOutData;
import com.gys.business.service.data.saleTask.SalePlanPushInData;
import com.gys.business.service.data.saleTask.SaleTaskSystemInData;
import com.gys.business.service.data.saleTask.SaleTaskSystemOutData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

public interface SaleTaskSystemService {
    String insertSystemSalesPlan(GetLoginOutData userInfo, SaleTaskSystemInData inData);

    List<SaleTaskSystemOutData> selectSalePlanList(SaleTaskSystemInData inData);

    GaiaSaletaskHplanSystem selectSalePlanDetail(SaleTaskSystemInData inData);

    void pushSalePlan(SalePlanPushInData inData);

    void timerPushSalePlan();

    void deleteSalePlan(SalePlanPushInData inData);
}
