package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 查询返回实体
 *
 * @author xiaoyuan on 2020/9/9
 */
@Data
public class ProfitAndLossRecordOutData implements Serializable {
    private static final long serialVersionUID = 2001285195196577659L;

    @ApiModelProperty(value = "序号")
    private Integer index;

    @ApiModelProperty(value = "创建日期")
    private String gsishDate;

    @ApiModelProperty(value = "商品编码")
    private String gsisdProId;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "零售价")
    private BigDecimal gsppPriceNormal;

    @ApiModelProperty(value = "批号")
    private String gsisdBatchNo;

    @ApiModelProperty(value = "批次")
    private String gsisdBatch;

    @ApiModelProperty(value = "库存数量")
    private BigDecimal gsisdStockQty;

    @ApiModelProperty(value = "损益原因")
    private String gsisdIsCause;

    @ApiModelProperty(value = "实际数量")
    private BigDecimal gsisdRealQty;

    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;

    @ApiModelProperty(value = "产地")
    private String proPlace;

    @ApiModelProperty(value = "剂型")
    private String proForm;

    @ApiModelProperty(value = "计量单位")
    private String proUnit;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;

    @ApiModelProperty(value = "库存状态")
    private String gsisdStockStatus;

    @ApiModelProperty(value = "gsisdValidUntil")
    private String gsisdValidUntil;

    @ApiModelProperty(value = "损益单号")
    private String gsisdVoucherId;

    @ApiModelProperty(value = "行号")
    private String gsisdSerial;

    @ApiModelProperty(value = "损益类型")
    private String gsishIsType;

    @ApiModelProperty(value = "移动平均价")
    private BigDecimal matMovPrice;

    @ApiModelProperty(value = "差异量")
    private BigDecimal difference;

    @ApiModelProperty(value = "成本额")
    private BigDecimal costAmount;

    @ApiModelProperty(value = "零售额")
    private BigDecimal retailSales;

    @ApiModelProperty(value = "门店名称")
    private String storeName;

    @ApiModelProperty(value = "门店编码")
    private String storeCode;

    @ApiModelProperty(value = "过账数量(实际损溢的数量)")
    private BigDecimal diffQty;
}
