package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "批次商品信息")
public class GetStockBatchProOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店")
    private String gssbBrId;
    @ApiModelProperty(value = "商品编码")
    private String gssbProId;
    @ApiModelProperty(value = "供应商")
    private String supplierId;
    @ApiModelProperty(value = "批号")
    private String gssbBatchNo;
    @ApiModelProperty(value = "批次")
    private String gssbBatch;
    @ApiModelProperty(value = "数量")
    private String gssbQty;
    @ApiModelProperty(value = "有效期")
    private String gssbVaildDate;
    @ApiModelProperty(value = "修改日期")
    private String gssbUpdateDate;
    @ApiModelProperty(value = "修改人员")
    private String gssbUpdateEmp;
    @ApiModelProperty(value = "商品下拉框（预留字段）")
    private String proIdOrName;

}
