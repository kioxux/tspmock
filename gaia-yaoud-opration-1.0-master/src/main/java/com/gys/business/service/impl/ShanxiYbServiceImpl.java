package com.gys.business.service.impl;
import com.google.common.collect.Lists;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaNbybProCatalog;
import com.gys.business.mapper.entity.GaiaSdSaleH;
import com.gys.business.mapper.entity.MibSettlementZjnb;
import com.gys.business.mapper.entity.User;
import com.gys.business.service.ShanxiYbService;
import com.gys.business.service.ZjnbYbService;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.MibSettlementZjnbDto;
import com.gys.business.service.data.SaleDDetail;
import com.gys.business.service.data.shanxi.SaleCountReportOutput;
import com.gys.business.service.data.shanxi.SaleCountReportVO;
import com.gys.business.service.data.zjnb.MibProductMatchDto;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.CopyUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ShanxiYbServiceImpl implements ShanxiYbService {
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd/HHmmss/");

    @Resource
    private MibSetlInfoMapper mibSetlInfoMapper;
    @Override
    public SaleCountReportOutput saleCountReport(InData inData, GetLoginOutData userInfo){
        SaleCountReportOutput saleCountReportOutputs = new SaleCountReportOutput() ;
        saleCountReportOutputs.setStoCode(userInfo.getDepId());
        saleCountReportOutputs.setStoName(userInfo.getDepName());
        saleCountReportOutputs.setUserCode(userInfo.getUserId());
        saleCountReportOutputs.setUserName(userInfo.getLoginName());
        List<SaleCountReportVO> saleCountReportList = mibSetlInfoMapper.saleCountReport(inData);
        List<SaleCountReportVO> saleCountReportOutData = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(saleCountReportList)){
            SaleCountReportVO saleCountReport1 =saleCountReportList.stream()
                    .filter(item -> "门诊".equals(item.getMedType()) && "在职".equals(item.getPsnType()))
                    .findFirst().orElse(null);;
            if (ObjectUtil.isEmpty(saleCountReport1)){
                saleCountReport1 = new SaleCountReportVO();
                saleCountReport1.setMedType("门诊");
                saleCountReport1.setPsnType("在职");
                saleCountReport1.setPersonTime("0");
                saleCountReport1.setMedfeeSumamt("0");
                saleCountReport1.setAccPay("0");
                saleCountReport1.setPsnCashPay("0");
                saleCountReport1.setFundPaySumamt("0");
                saleCountReport1.setHifmiPay("0");
                saleCountReport1.setCvlservPay("0");
            }
            saleCountReportOutData.add(saleCountReport1);

            SaleCountReportVO saleCountReport2 =saleCountReportList.stream()
                    .filter(item -> "门诊".equals(item.getMedType()) && "退休".equals(item.getPsnType()))
                    .findFirst().orElse(null);;
            if (ObjectUtil.isEmpty(saleCountReport2)){
                saleCountReport2 = new SaleCountReportVO();
                saleCountReport2.setMedType("门诊");
                saleCountReport2.setPsnType("退休");
                saleCountReport2.setPersonTime("0");
                saleCountReport2.setMedfeeSumamt("0");
                saleCountReport2.setAccPay("0");
                saleCountReport2.setPsnCashPay("0");
                saleCountReport2.setFundPaySumamt("0");
                saleCountReport2.setHifmiPay("0");
                saleCountReport2.setCvlservPay("0");
            }
            saleCountReportOutData.add(saleCountReport2);

            SaleCountReportVO saleCountReport3=saleCountReportList.stream()
                    .filter(item -> "慢性病门诊".equals(item.getMedType()) && "在职".equals(item.getPsnType()))
                    .findFirst().orElse(null);;
            if (ObjectUtil.isEmpty(saleCountReport3)){
                saleCountReport3 = new SaleCountReportVO();
                saleCountReport3.setMedType("慢性病门诊");
                saleCountReport3.setPsnType("在职");
                saleCountReport3.setPersonTime("0");
                saleCountReport3.setMedfeeSumamt("0");
                saleCountReport3.setAccPay("0");
                saleCountReport3.setPsnCashPay("0");
                saleCountReport3.setFundPaySumamt("0");
                saleCountReport3.setHifmiPay("0");
                saleCountReport3.setCvlservPay("0");
            }
            saleCountReportOutData.add(saleCountReport3);

            SaleCountReportVO saleCountReport4=saleCountReportList.stream()
                    .filter(item -> "慢性病门诊".equals(item.getMedType()) && "退休".equals(item.getPsnType()))
                    .findFirst().orElse(null);;
            if (ObjectUtil.isEmpty(saleCountReport4)){
                saleCountReport4 = new SaleCountReportVO();
                saleCountReport4.setMedType("慢性病门诊");
                saleCountReport4.setPsnType("退休");
                saleCountReport4.setPersonTime("0");
                saleCountReport4.setMedfeeSumamt("0");
                saleCountReport4.setAccPay("0");
                saleCountReport4.setPsnCashPay("0");
                saleCountReport4.setFundPaySumamt("0");
                saleCountReport4.setHifmiPay("0");
                saleCountReport4.setCvlservPay("0");
            }
            saleCountReportOutData.add(saleCountReport4);

            SaleCountReportVO saleCountReport5=saleCountReportList.stream()
                    .filter(item -> "住院".equals(item.getMedType()) && "在职".equals(item.getPsnType()))
                    .findFirst().orElse(null);;
            if (ObjectUtil.isEmpty(saleCountReport5)){
                saleCountReport5 = new SaleCountReportVO();
                saleCountReport5.setMedType("住院");
                saleCountReport5.setPsnType("在职");
                saleCountReport5.setPersonTime("0");
                saleCountReport5.setMedfeeSumamt("0");
                saleCountReport5.setAccPay("0");
                saleCountReport5.setPsnCashPay("0");
                saleCountReport5.setFundPaySumamt("0");
                saleCountReport5.setHifmiPay("0");
                saleCountReport5.setCvlservPay("0");
            }
            saleCountReportOutData.add(saleCountReport5);

            SaleCountReportVO saleCountReport6=saleCountReportList.stream()
                    .filter(item -> "住院".equals(item.getMedType()) && "退休".equals(item.getPsnType()))
                    .findFirst().orElse(null);;
            if (ObjectUtil.isEmpty(saleCountReport6)){
                saleCountReport6 = new SaleCountReportVO();
                saleCountReport6.setMedType("住院");
                saleCountReport6.setPsnType("退休");
                saleCountReport6.setPersonTime("0");
                saleCountReport6.setMedfeeSumamt("0");
                saleCountReport6.setAccPay("0");
                saleCountReport6.setPsnCashPay("0");
                saleCountReport6.setFundPaySumamt("0");
                saleCountReport6.setHifmiPay("0");
                saleCountReport6.setCvlservPay("0");
            }
            saleCountReportOutData.add(saleCountReport6);

        }
        saleCountReportOutputs.setSaleCountReports(saleCountReportOutData);
        return saleCountReportOutputs;
    }

}
