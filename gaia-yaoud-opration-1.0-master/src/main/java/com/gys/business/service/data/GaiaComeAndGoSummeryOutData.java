package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 15:50 2021/7/26
 * @Description：往来管理汇总表数据
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GaiaComeAndGoSummeryOutData {

//结果展示
    private String client;

    private String dcCode;

    private String dcName;

    private String stoCode;

    private String stoName;



    private String residualAmt;

    private String endAmt;

    private List<GaiaComeAndGoPaymentData> paymentRealAmtMapList;

//summary字段
    private String amt;

    private String realAmt;

    private String paymentId;

    private String paymentName;
}
