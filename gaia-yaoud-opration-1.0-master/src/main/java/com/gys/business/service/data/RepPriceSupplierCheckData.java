package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSupplierBusiness;
import lombok.Data;

import java.util.List;

@Data
public class RepPriceSupplierCheckData {
    private String pgId;
    private List<GaiaSupplierBusiness> gaiaSupplierBusinessesList;
}
