package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class GetRestOrderDetailOutData implements Serializable {
    private static final long serialVersionUID = -640182967059019662L;
    /**
     * 行号
     */
    private String gssdSerial;

    /**
     * 商品编码
     */
    private String gssdProId;

    /**
     * 数量
     */
    private String gssdQty;

    /**
     * 单品零售价
     */
    private String gssdPrc1;

    /**
     * 商品批号
     */
    private String gssdBatchNo;

    /**
     * 商品批次
     */
    private String gssdBatch;

    /**
     * 商品有效期
     */
    private String gssdValidDate;

    /**
     * 单品应收价
     */
    private String gssdPrc2;

    /**
     * 汇总应收金额
     */
    private String gssdAmt;

    /**
     * 营业员编号
     */
    private String gssdSalerId;

    /**
     * 医生编号
     */
    private String gssdDoctorId;

    /**
     * 医生名称
     */
    private String doctorName;

    /**
     * 参加促销主题编号
     */
    private String gssdPmSubjectId;

    /**
     * 参加促销类型编码
     */
    private String gssdPmId;

    /**
     * 参加促销类型说明
     */
    private String gssdPmContent;

    private String gssdPmActivityId;//促销单号id
    private String gssdPmActivityFlag;//促销参数

    /**
     * 商品名
     */
    private String gssdProName;
    /**
     * 商品通用名称
     */
    private String gssdProCommonName;

    /**
     * 规格
     */
    private String spec;

    /**
     * 生产企业
     */
    private String proFactoryName;

    /**
     * 用法用量
     */
    private String proUsage;

    /**
     * 禁忌说明
     */
    private String proContraindication;

    /**
     *
     */
    private GetSalesReceiptsTableOutData getSalesReceiptsTableOutData;

    /**
     * 等于3 表示中药
     */
    private String proStorageAreas;

    /**
     * 处方类别
     */
    private String proPresclass;

    /**
     * 货位号编号
     */
    private String wmHwhBm;

    /**
     * 税率至
     */
    private String gssdMovTax;

    /**
     * 贴数
     */
    private String gssdDose;

    /**
     * 定位
     */
    private String positioning;

    /**
     * 营业员名称
     */
    private String gssdSalerName;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 销售时间
     */
    private String gsshTime;

    /**
     * 医保刷卡数量
     */
    private String proZdy1;

    /**
     * 状态
     */
    private String proStatus;

    /**
     * 医保编码
     */
    private String proIfMed;
    /**
     * 等级
     */
    private String saleClass;
    /**
     * 不打折 1：是
     */
    private String proBdz;
}
