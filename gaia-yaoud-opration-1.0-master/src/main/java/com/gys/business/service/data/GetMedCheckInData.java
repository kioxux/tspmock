//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "药品养护查询条件")
public class GetMedCheckInData {
    @ApiModelProperty(value = "供应商")
    private String clientId;
    @ApiModelProperty(value = "养护单号")
    private String gsmchVoucherId;
    @ApiModelProperty(value = "门店")
    private String gsmchBrId;
    @ApiModelProperty(value = "养护类型 1-一般养护 2-重点养护")
    private String gsmchType;
    @ApiModelProperty(value = "养护日期")
    private String gsmchDate;
    @ApiModelProperty(value = "养护人员")
    private String gsmchEmp;
    @ApiModelProperty(value = "审核状态 0-未审核 1-已审核")
    private String gsmchStatus;
    @ApiModelProperty(value = "养护商品编码")
    private String gsmcdProId;
    @ApiModelProperty(value = "养护商品批号")
    private String gsmcdBatchNo;
    private List<GetMedCheckDInData> medCheckDs;

    public GetMedCheckInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsmchVoucherId() {
        return this.gsmchVoucherId;
    }

    public String getGsmchBrId() {
        return this.gsmchBrId;
    }

    public String getGsmchType() {
        return this.gsmchType;
    }

    public String getGsmchDate() {
        return this.gsmchDate;
    }

    public String getGsmchEmp() {
        return this.gsmchEmp;
    }

    public String getGsmchStatus() {
        return this.gsmchStatus;
    }

    public String getGsmcdProId() {
        return this.gsmcdProId;
    }

    public String getGsmcdBatchNo() {
        return this.gsmcdBatchNo;
    }

    public List<GetMedCheckDInData> getMedCheckDs() {
        return this.medCheckDs;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsmchVoucherId(final String gsmchVoucherId) {
        this.gsmchVoucherId = gsmchVoucherId;
    }

    public void setGsmchBrId(final String gsmchBrId) {
        this.gsmchBrId = gsmchBrId;
    }

    public void setGsmchType(final String gsmchType) {
        this.gsmchType = gsmchType;
    }

    public void setGsmchDate(final String gsmchDate) {
        this.gsmchDate = gsmchDate;
    }

    public void setGsmchEmp(final String gsmchEmp) {
        this.gsmchEmp = gsmchEmp;
    }

    public void setGsmchStatus(final String gsmchStatus) {
        this.gsmchStatus = gsmchStatus;
    }

    public void setGsmcdProId(final String gsmcdProId) {
        this.gsmcdProId = gsmcdProId;
    }

    public void setGsmcdBatchNo(final String gsmcdBatchNo) {
        this.gsmcdBatchNo = gsmcdBatchNo;
    }

    public void setMedCheckDs(final List<GetMedCheckDInData> medCheckDs) {
        this.medCheckDs = medCheckDs;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetMedCheckInData)) {
            return false;
        } else {
            GetMedCheckInData other = (GetMedCheckInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gsmchVoucherId = this.getGsmchVoucherId();
                Object other$gsmchVoucherId = other.getGsmchVoucherId();
                if (this$gsmchVoucherId == null) {
                    if (other$gsmchVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsmchVoucherId.equals(other$gsmchVoucherId)) {
                    return false;
                }

                Object this$gsmchBrId = this.getGsmchBrId();
                Object other$gsmchBrId = other.getGsmchBrId();
                if (this$gsmchBrId == null) {
                    if (other$gsmchBrId != null) {
                        return false;
                    }
                } else if (!this$gsmchBrId.equals(other$gsmchBrId)) {
                    return false;
                }

                label110: {
                    Object this$gsmchType = this.getGsmchType();
                    Object other$gsmchType = other.getGsmchType();
                    if (this$gsmchType == null) {
                        if (other$gsmchType == null) {
                            break label110;
                        }
                    } else if (this$gsmchType.equals(other$gsmchType)) {
                        break label110;
                    }

                    return false;
                }

                label103: {
                    Object this$gsmchDate = this.getGsmchDate();
                    Object other$gsmchDate = other.getGsmchDate();
                    if (this$gsmchDate == null) {
                        if (other$gsmchDate == null) {
                            break label103;
                        }
                    } else if (this$gsmchDate.equals(other$gsmchDate)) {
                        break label103;
                    }

                    return false;
                }

                Object this$gsmchEmp = this.getGsmchEmp();
                Object other$gsmchEmp = other.getGsmchEmp();
                if (this$gsmchEmp == null) {
                    if (other$gsmchEmp != null) {
                        return false;
                    }
                } else if (!this$gsmchEmp.equals(other$gsmchEmp)) {
                    return false;
                }

                label89: {
                    Object this$gsmchStatus = this.getGsmchStatus();
                    Object other$gsmchStatus = other.getGsmchStatus();
                    if (this$gsmchStatus == null) {
                        if (other$gsmchStatus == null) {
                            break label89;
                        }
                    } else if (this$gsmchStatus.equals(other$gsmchStatus)) {
                        break label89;
                    }

                    return false;
                }

                label82: {
                    Object this$gsmcdProId = this.getGsmcdProId();
                    Object other$gsmcdProId = other.getGsmcdProId();
                    if (this$gsmcdProId == null) {
                        if (other$gsmcdProId == null) {
                            break label82;
                        }
                    } else if (this$gsmcdProId.equals(other$gsmcdProId)) {
                        break label82;
                    }

                    return false;
                }

                Object this$gsmcdBatchNo = this.getGsmcdBatchNo();
                Object other$gsmcdBatchNo = other.getGsmcdBatchNo();
                if (this$gsmcdBatchNo == null) {
                    if (other$gsmcdBatchNo != null) {
                        return false;
                    }
                } else if (!this$gsmcdBatchNo.equals(other$gsmcdBatchNo)) {
                    return false;
                }

                Object this$medCheckDs = this.getMedCheckDs();
                Object other$medCheckDs = other.getMedCheckDs();
                if (this$medCheckDs == null) {
                    if (other$medCheckDs != null) {
                        return false;
                    }
                } else if (!this$medCheckDs.equals(other$medCheckDs)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetMedCheckInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsmchVoucherId = this.getGsmchVoucherId();
        result = result * 59 + ($gsmchVoucherId == null ? 43 : $gsmchVoucherId.hashCode());
        Object $gsmchBrId = this.getGsmchBrId();
        result = result * 59 + ($gsmchBrId == null ? 43 : $gsmchBrId.hashCode());
        Object $gsmchType = this.getGsmchType();
        result = result * 59 + ($gsmchType == null ? 43 : $gsmchType.hashCode());
        Object $gsmchDate = this.getGsmchDate();
        result = result * 59 + ($gsmchDate == null ? 43 : $gsmchDate.hashCode());
        Object $gsmchEmp = this.getGsmchEmp();
        result = result * 59 + ($gsmchEmp == null ? 43 : $gsmchEmp.hashCode());
        Object $gsmchStatus = this.getGsmchStatus();
        result = result * 59 + ($gsmchStatus == null ? 43 : $gsmchStatus.hashCode());
        Object $gsmcdProId = this.getGsmcdProId();
        result = result * 59 + ($gsmcdProId == null ? 43 : $gsmcdProId.hashCode());
        Object $gsmcdBatchNo = this.getGsmcdBatchNo();
        result = result * 59 + ($gsmcdBatchNo == null ? 43 : $gsmcdBatchNo.hashCode());
        Object $medCheckDs = this.getMedCheckDs();
        result = result * 59 + ($medCheckDs == null ? 43 : $medCheckDs.hashCode());
        return result;
    }

    public String toString() {
        return "GetMedCheckInData(clientId=" + this.getClientId() + ", gsmchVoucherId=" + this.getGsmchVoucherId() + ", gsmchBrId=" + this.getGsmchBrId() + ", gsmchType=" + this.getGsmchType() + ", gsmchDate=" + this.getGsmchDate() + ", gsmchEmp=" + this.getGsmchEmp() + ", gsmchStatus=" + this.getGsmchStatus() + ", gsmcdProId=" + this.getGsmcdProId() + ", gsmcdBatchNo=" + this.getGsmcdBatchNo() + ", medCheckDs=" + this.getMedCheckDs() + ")";
    }
}
