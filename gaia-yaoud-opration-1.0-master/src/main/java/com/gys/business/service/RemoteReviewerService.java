//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.PrescriptionDetailInfoOutData;
import com.gys.business.service.data.PrescriptionInfoInData;
import com.gys.business.service.data.PrescriptionInfoOutData;
import com.gys.common.data.GetLoginOutData;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

public interface RemoteReviewerService {
    List<PrescriptionInfoOutData> getPrescriptionList(PrescriptionInfoInData inData);

    List<PrescriptionDetailInfoOutData> getPrescriptionDetailList(PrescriptionInfoInData inData);

    void approve(PrescriptionInfoInData inData, GetLoginOutData userInfo);

    void exportOut(HttpServletResponse response, PrescriptionInfoInData inData) throws Exception;

    String queryPhoto(PrescriptionInfoInData inData);
}
