package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Ordering;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CommonService;
import com.gys.business.service.ReplenishWebService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.replenishParam.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.*;
import com.gys.common.exception.BusinessException;
import com.gys.common.kylin.RowMapper;
import com.gys.common.redis.RedisManager;
import com.gys.common.response.Result;
import com.gys.util.*;
import com.qcloud.cos.utils.CollectionUtils;
import com.qcloud.cos.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings("all")
@Service
@Slf4j
public class ReplenishWebServiceImpl implements ReplenishWebService {
    @Autowired
    private GaiaSdReplenishHMapper replenishHMapper;
    @Autowired
    private GaiaSdReplenishDMapper replenishDMapper;
    @Autowired
    private GaiaSdStockMapper stockMapper;
    @Autowired
    private GaiaSdSystemParaMapper sdSystemParaMapper;
    @Autowired
    private GaiaSdSystemParaDefaultMapper sdSystemParaDefaultMapper;
    @Autowired
    private GaiaPoHeaderMapper poHeaderMapper;
    @Autowired
    private GaiaPoItemMapper poItemMapper;
    @Autowired
    private GaiaProductBusinessMapper productBusinessMapper;
    @Autowired
    private GaiaProductBasicMapper productBasicMapper;
    @Autowired
    private GaiaSoItemMapper soItemMapper;
    @Autowired
    private GaiaBatchStockMapper batchStockMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdStoreDataMapper sdStoreDataMapper;
    @Autowired
    private GaiaSupplierBusinessMapper supplierBusinessMapper;
    @Autowired
    private GaiaSdReplenishParaMapper replenishParaMapper;
    @Autowired
    private GaiaMaterialAssessMapper materialAssessMapper;
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Autowired
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Autowired
    private GaiaProductChangeMapper gaiaProductChangeMapper;
    @Autowired
    private GaiaSdReplenishOriDMapper gaiaSdReplenishOriDMapper;
    @Autowired
    private GaiaSdReplenishComparedDMapper gaiaSdReplenishComparedDMapper;
    @Autowired
    private GaiaMaterialAssessMapper gaiaMaterialAssessMapper;
    @Autowired
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Autowired
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;
    @Autowired
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;
    @Autowired
    private GaiaSdReplenishHistoryMapper gaiaSdReplenishHistoryMapper;
    @Autowired
    private CosUtils cosUtils;
    @Autowired
    private GaiaSdStoresGroupMapper storesGroupMapper;
    @Autowired
    private GaiaSdOutOfStockPushRecordMapper outOfStockPushRecordMapper;
    @Value("${penghui.client}")
    private String penghuiClient;
    @Autowired
    private CommonMapper commonMapper;
    @Autowired
    private SystemParaMapper systemParaMapper;
    @Autowired
    private GaiaProductSpecialMapper productSpecialMapper;
    @Autowired
    private GaiaFranchiseeMapper franchiseeMapper;
    @Autowired
    private ReplenishGiftMapper replenishGiftMapper;
    @Resource(name = "kylinJdbcTemplateFactory")
    private JdbcTemplate kylinJdbcTemplate;
    @Autowired
    private ReplenishWebService replenishWebService;
    @Resource
    private GaiaWmsDiaoboZMapper gaiaWmsDiaoboZMapper;
    @Autowired
    private CommonService commonService;



    @Override
    public PageInfo<GetReplenishOutData> selectList(GetReplenishInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) || ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isNotEmpty(inData.getGsrhDate())) {
            try {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date = format.parse(inData.getGsrhDate());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                String dateString = simpleDateFormat.format(date);
                inData.setGsrhDate(dateString);
            } catch (ParseException e) {
                e.getMessage();
            }
        }
        log.info("查询补货单列表开始");
        List<GetReplenishOutData> outData = this.replenishHMapper.selectList(inData);
//        for (GetReplenishOutData data : outData) {
//            data.setGsrhDate(this.changeDateType(data.getGsrhDate()));
//        }
        log.info("查询补货单列表结束");
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public List<GetReplenishDetailOutData> detailList(GetReplenishInData inData) {
        log.info("新增补货开始");
        List<ReplenishGiftOutParam> giftList = new ArrayList<>();
        StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(),inData.getStoCode());
        if (storeOutData != null){
            if (ObjectUtil.isNotEmpty(storeOutData.getStoDcCode())){
                inData.setDcCode(storeOutData.getStoDcCode());
                //赠品列表查询
                giftList = replenishGiftMapper.selectGiftList(inData.getClientId(),storeOutData.getStoDcCode(),"","");
            }
        }

        //查询该门店是否允许 补货 禁采商品
        GaiaSdSystemPara para = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClientId(), inData.getStoCode(), "REPLENISH_NO_PURCHASE");
        if (ObjectUtil.isNotEmpty(para) && "1".equals(para.getGsspPara())) {
            inData.setFlag("1");
        }
        inData.setParamCode("REPLENISH_MINQTY");//最小陈列量参数
        log.info("查询所有补货商品开始");
        List<GetReplenishDetailOutData> outDataList = productBasicMapper.queryProFromReplenish(inData);
        log.info("查询所有补货商品结束");
        log.info("查询商品7、30、90天销量开始");
        Map<String, GetReplenishOutData> saleMap = this.getSalesMap(inData);
        log.info("查询商品7、30、90天销量结束");
        log.info("查询商品库存开始");
        Map<String, String> stockMap = this.getStockMap(inData);
        log.info("查询商品库存结束");
        log.info("查询商品批次库存开始");
        Map<String, GetReplenishOutData> costMap = this.getCostMap(inData);
        log.info("查询商品批次库存结束");
        log.info("查询最小陈列量参数开始");
        GetReplenishOutData param = this.getParam(inData);
//        Map<String, QuerySupplierData> supplierNameMap = this.getSupplierNameMap(inData.getClientId(),inData.getStoCode());
        log.info("查询最小陈列量参数结束");
        log.info("查询商品补货公式开始");
        Map<String, String> countMap = this.getCountMap(inData,saleMap,stockMap,outDataList);
        log.info("查询商品补货公式结束");
//        Map<String, String> amtMap = this.getVoucherAmtMap(inData);
        Iterator var9 = outDataList.iterator();
        while (var9.hasNext()) {
            GetReplenishDetailOutData detailOutData = (GetReplenishDetailOutData) var9.next();
            inData.setProductCode(detailOutData.getGsrdProId());
            GetReplenishOutData replenishOutData = saleMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNull(replenishOutData)) {
                detailOutData.setGsrdSalesDays1("0");
                detailOutData.setGsrdSalesDays2("0");
                detailOutData.setGsrdSalesDays3("0");
            } else {
                detailOutData.setGsrdSalesDays1(replenishOutData.getSalesCountOne());
                detailOutData.setGsrdSalesDays2(replenishOutData.getSalesCountTwo());
                detailOutData.setGsrdSalesDays3(replenishOutData.getSalesCountThree());
            }

            detailOutData.setGsrdStockStore(stockMap.get(detailOutData.getGsrdProId()));
            replenishOutData = costMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNotNull(replenishOutData)) {
                detailOutData.setGsrdAverageCost(replenishOutData.getAverageCost());
                detailOutData.setGsrdGrossMargin(replenishOutData.getBatchCost());
            }
//            QuerySupplierData supplierData = supplierNameMap.get(detailOutData.getGsrdProId());
//            if (ObjectUtil.isNotEmpty(supplierData)) {
//                detailOutData.setGsrdSupName(supplierData.getSupName());
//            }
//            log.info("最小陈列量参数：{}"+param.getParam());
            if(ObjectUtil.isEmpty(param.getParam())){
                param.setParam("0");
            }
            detailOutData.setGsrdDisplayMin(param.getParam());
            String count = countMap.get(detailOutData.getGsrdProId());
//            log.info("补货公式补货量参数：{}"+count);
            // 建议补货量为0的不显示
            if (StrUtil.isNotBlank(count) && !"0".equals(count) && (new BigDecimal(count)).compareTo(new BigDecimal(0))==1) {
                detailOutData.setGsrdNeedQty(count);
                detailOutData.setGsrdProposeQty(count);
            } else {
                var9.remove();
                continue;
            }
            if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1){
                BigDecimal voucherAmt = ((new BigDecimal(detailOutData.getMatTotalAmt()).add(new BigDecimal(detailOutData.getMatRateAmt())))).divide(new BigDecimal(detailOutData.getMatTotalQty()),4,BigDecimal.ROUND_HALF_UP);
                detailOutData.setVoucherAmt(voucherAmt.toString());
            }else{
                if (ObjectUtil.isEmpty(detailOutData.getMatMovPrice())){
                    detailOutData.setMatMovPrice("0");
                }
                detailOutData.setVoucherAmt(detailOutData.getMatMovPrice());
            }
            BigDecimal a = BigDecimal.ZERO;
            BigDecimal b = BigDecimal.ZERO;
            if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1){
                a = new BigDecimal(detailOutData.getVoucherAmt());
            }else {
                String num = detailOutData.getGsrdInputTaxValue().replace("%", "");
                BigDecimal rate = new BigDecimal(num).divide(new BigDecimal("100"),4,BigDecimal.ROUND_HALF_UP);
                a = new BigDecimal(detailOutData.getMatMovPrice()).multiply(new BigDecimal("1").add(rate)).setScale(4,BigDecimal.ROUND_HALF_UP);
            }
            if (ObjectUtil.isNotEmpty(detailOutData.getApAddAmt())) {
                b = a.add(new BigDecimal(detailOutData.getApAddAmt()));
            } else if(ObjectUtil.isNotEmpty(detailOutData.getApAddRate()) && ObjectUtil.isEmpty(detailOutData.getApAddAmt())){
                if (ObjectUtil.isEmpty(detailOutData.getApAddRate())) {
                    if (ObjectUtil.isEmpty(detailOutData.getAddAmt())) {
                        if (ObjectUtil.isNotEmpty(detailOutData.getAddRate())) {
                            b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                        }
                    } else {
                        b = a.add(new BigDecimal(detailOutData.getAddAmt()));
                    }
                } else {
                    b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                }
            }else if(ObjectUtil.isEmpty(detailOutData.getAddRate())
                    && ObjectUtil.isEmpty(detailOutData.getApAddAmt())
                    && ObjectUtil.isNotEmpty(detailOutData.getApCataloguePrice())){
                b = new BigDecimal(detailOutData.getApCataloguePrice());
            }
            if (b.compareTo(BigDecimal.ZERO) == 1) {
                detailOutData.setCheckOutAmt(b.toString());
            }
            // 格式化销售量
            detailOutData.setGsrdSalesDays1(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays1(), ".0000"));
            detailOutData.setGsrdSalesDays2(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays2(), ".0000"));
            detailOutData.setGsrdSalesDays3(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays3(), ".0000"));
            detailOutData.setGsrdProposeQty(StrUtil.removeSuffix(detailOutData.getGsrdProposeQty(), ".0000"));
            detailOutData.setGsrdNeedQty(StrUtil.removeSuffix(detailOutData.getGsrdNeedQty(), ".0000"));
            detailOutData.setGsrdStockStore(StrUtil.isEmpty(StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000")) ? "0" : StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000"));
        }
        outDataList = new Ordering<GetReplenishDetailOutData>() {
            @Override
            public int compare(GetReplenishDetailOutData left, GetReplenishDetailOutData right) {
                int leftQty = NumberUtil.parseInt(left.getGsrdNeedQty());
                int rightQty = NumberUtil.parseInt(right.getGsrdNeedQty());
                return rightQty - leftQty;
            }
        }.immutableSortedCopy(outDataList);
        List<GetReplenishDetailOutData> resultList = new ArrayList<>();
        //过滤补货商品不为赠品并标记商品是否带赠品
        for (GetReplenishDetailOutData data : outDataList) {
            data.setHasGift("0");
            boolean zp = true;
            if (ObjectUtil.isNotEmpty(giftList) && giftList.size() > 0){
                for(int i = 0;i < giftList.size();i ++){
                    zp = true;
                    ReplenishGiftOutParam gift = giftList.get(i);
                    if (gift.getGiftCode().trim().equals(data.getGsrdProId())){
                        zp = false;
                        break;
                    }else {
                        if (!gift.getGiftCode().trim().equals(data.getGsrdProId())){//补货商品非赠品
                            if (gift.getProCode().equals(data.getGsrdProId())){//该商品带赠品
                                data.setHasGift("1");
                            }
                        }
                    }
                }
            }
            if (zp) {
                resultList.add(data);
            }
        }
        if (!CollectionUtils.isNullOrEmpty(resultList)) {
            String keyName = "REP-" + inData.getClientId() + "-" + inData.getStoCode() + "-" + inData.getUserId();
            redisManager.set(keyName, JSON.toJSONString(resultList));
        }
        //委托第三方编码匹配
        String wtpsParam = storeDataMapper.selectWtpsParam(inData.getClientId(),"WTPS_SET");
        if (StringUtils.isNullOrEmpty(wtpsParam)){
            wtpsParam = "0";
        }
        if ("1".equals(wtpsParam)){
            //获取商品匹配编码
            List<Map<String,String>> proThreeCodeList = replenishDMapper.selectProThreeCode(inData.getClientId(),"");
            //获取第三方仓库库存
            List<GaiaSdDsfStock> proThreeStockList = replenishDMapper.selectProThreeStock(inData.getClientId(),"");
            //获取特殊商品分类编码
            List<SpecialClass> specialClassList = replenishDMapper.selectSpecialClass(inData.getClientId(),inData.getStoCode(),"");
            for (GetReplenishDetailOutData detail : resultList) {
                for (SpecialClass specialClass : specialClassList) {
                    if ("1".equals(specialClass.getStockType())) {
                        if (specialClass.getBigClass().equals(detail.getBigClass().split("-")[0])) {
                            //显示第三方商品编码
                            boolean f = true;
                            for (Map<String, String> map : proThreeCodeList) {
                                if (map.get("proCode").equals(detail.getGsrdProId())) {
                                    detail.setProThreeCode(map.get("proThreeCode"));
                                    detail.setHasProThreeCode("1");
                                    f = false;
                                }
                            }
                            if (f) {
                                detail.setHasProThreeCode("0");
                            }
                            BigDecimal stock = BigDecimal.ZERO;
                            if (ObjectUtil.isNotEmpty(detail.getGsrdStockDepot())) {
                                stock = new BigDecimal(detail.getGsrdStockDepot());
                            }
                            //商品仓库库存累加
                            for (GaiaSdDsfStock c : proThreeStockList) {
                                if (c.getProId().equals(detail.getGsrdProId())) {
                                    detail.setGsrdStockDepot(String.valueOf(stock.add(c.getQty())));
                                }
                            }
                        }
                    }
                }
            }
        }
        log.info("在途量查询开始");
        //计算在途量
        try {
            resultList = getProductOnWayNumber(inData.getClientId(), inData.getStoCode(), true, resultList);
        } catch (Exception e) {
            log.error("在途量计算错误：{}",e.getMessage(), e);
            throw new BusinessException("提示：在途量计算错误");
        }
        log.info("在途量查询结束");
        log.info("新增补货获取结束");
        return resultList;
    }


    /**
     * 获取在途量
     * @param resultList
     * @return
     */
    private List<GetReplenishDetailOutData> getProductOnWayNumber(String client, String brId, boolean flag, List<GetReplenishDetailOutData> replenishList) {
        //查询商品在途量   默认查询当日
        List<ReplenishHOnWayOutData> productOnWayList = replenishHMapper.getProductOnWayList(client, brId, CommonUtil.getyyyyMMdd());
        //如果查询为空则不处理在途量数据
        if(ValidateUtil.isEmpty(productOnWayList)) {
            return replenishList;
        }
        //将在途量转为map  key为商品编码 value为在途数量
        Map<String, BigDecimal> onWayNumberMap = productOnWayList.stream().collect(Collectors.toMap(ReplenishHOnWayOutData::getProductId,
                ReplenishHOnWayOutData::getOnWayNumber));

        for(GetReplenishDetailOutData replenish : replenishList) {
            //获取补货公式商品编码
            String proId = replenish.getGsrdProId();
            //去查询到的在途量map中匹配是否有相同的商品编码 如果有则赋值 否则为0
            BigDecimal onWayNumber = onWayNumberMap.get(proId);
            if(ValidateUtil.isNotEmpty(onWayNumber)) {
                replenish.setOnWayNumber(onWayNumber);
            } else {
                replenish.setOnWayNumber(BigDecimal.ZERO);
            }
        }
        //如果flag等于false  表示最终补货量不计算在途  只显示在途量
        if(!flag) {
            return replenishList;
        }

        //如果等于true 表示 计算最终补货量 = 当前补货量 - 在途量
        List<GetReplenishDetailOutData> resultList = new ArrayList<>();
        //计算最终补货量 = 当前补货量 - 在途量  如果为0 则剔除数据
        for(GetReplenishDetailOutData replenish : replenishList) {
            String gsrdNeedQty = replenish.getGsrdNeedQty();        //补货量
            BigDecimal onWayNumber = replenish.getOnWayNumber();    //在途量

            //如果在途数量为0 则直不做计算 直接使用该数据
            if(onWayNumber.compareTo(BigDecimal.ZERO) == 0) {
                resultList.add(replenish);
                continue;
            }
            //如果当前补货量不为空  使用当前补货量 - 在途量 = 最终补货量
            if(ValidateUtil.isNotEmpty(gsrdNeedQty)) {
                BigDecimal needQty = new BigDecimal(gsrdNeedQty);
                //如果当前补货量 大于 在途量 也就是结果一定为正数 则进行计算   反之：当前补货量 - 在途量 小于等于 0  则剔除该数据
                if(needQty.compareTo(onWayNumber) == 1) {
                    BigDecimal resultNeedQty = needQty.subtract(onWayNumber);   //补货数量 减 在途数量
                    replenish.setGsrdNeedQty(resultNeedQty.setScale(0, BigDecimal.ROUND_HALF_UP).toString());         //最终补货量
                    replenish.setGsrdProposeQty(resultNeedQty.setScale(0, BigDecimal.ROUND_HALF_UP).toString());      //建议补货量
                    resultList.add(replenish);
                }
            }
        }
        return resultList;
    }

    @Override
    @Transactional
    public GetReplenishOutData getDetail(GetReplenishInData inData) {
        log.info("获取补货单明细开始");
        Map<String,ReplenishGiftOutParam> giftMap = new HashMap();
        StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(),inData.getStoCode());
        if (storeOutData != null){
            if (ObjectUtil.isNotEmpty(storeOutData.getStoDcCode())){
                inData.setDcCode(storeOutData.getStoDcCode());
                List<ReplenishGiftOutParam> giftList = replenishGiftMapper.selectGiftList(inData.getClientId(),storeOutData.getStoDcCode(),"","");
                giftMap = giftList.stream().collect(Collectors.toMap(ReplenishGiftOutParam::getProCode, x -> x, (a, b) -> b));
            }
        }
        GetReplenishOutData outData = new GetReplenishOutData();
        Example example = new Example(GaiaSdReplenishH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId());
        GaiaSdReplenishH replenishH = (GaiaSdReplenishH) this.replenishHMapper.selectOneByExample(example);
        outData.setGsrhType(replenishH.getGsrhType());
        outData.setGsrhDate(this.changeDateType(replenishH.getGsrhDate()));
        outData.setGsrhStatus(replenishH.getGsrhStatus());
        outData.setGsrhTotalAmt(replenishH.getGsrhTotalAmt());
        outData.setGsrhTotalQty(replenishH.getGsrhTotalQty());
        outData.setGsrhEmp(replenishH.getGsrhEmp());
        outData.setGsrhPattern(replenishH.getGsrhPattern());
        outData.setGsrhVoucherId(inData.getGsrhVoucherId());
        log.info("补货单："+ inData.getGsrhVoucherId() +"明细查询开始");
        List<GetReplenishDetailOutData> detailList = this.replenishDMapper.queryDetailList(inData);
        log.info("补货单："+ inData.getGsrhVoucherId() +"明细查询结束");
        if (detailList.size() > 0) {
            for(GetReplenishDetailOutData detailOutData : detailList){
                detailOutData.setHasGift("0");
                ReplenishGiftOutParam gift = giftMap.get(detailOutData.getGsrdProId());
                if (ObjectUtil.isNotEmpty(gift)){
                    detailOutData.setHasGift("1");
                }
                if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1) {
                    BigDecimal voucherAmt = ((new BigDecimal(detailOutData.getMatTotalAmt()).add(new BigDecimal(detailOutData.getMatRateAmt())))).divide(new BigDecimal(detailOutData.getMatTotalQty()), 4, BigDecimal.ROUND_HALF_UP);
                    detailOutData.setVoucherAmt(voucherAmt.toString());
                } else {
                    if (ObjectUtil.isEmpty(detailOutData.getMatMovPrice())) {
                        detailOutData.setMatMovPrice("0");
                    }
                    detailOutData.setVoucherAmt(detailOutData.getMatMovPrice());
                }
                BigDecimal a = BigDecimal.ZERO;
                BigDecimal b = BigDecimal.ZERO;
                if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1){
                    a = new BigDecimal(detailOutData.getVoucherAmt());
                }else {
                    String num = detailOutData.getGsrdInputTaxValue().replace("%", "");
                    BigDecimal rate = new BigDecimal(num).divide(new BigDecimal("100"),4,BigDecimal.ROUND_HALF_UP);
                    a = new BigDecimal(detailOutData.getMatMovPrice()).multiply(new BigDecimal("1").add(rate)).setScale(4,BigDecimal.ROUND_HALF_UP);
                }
                if (ObjectUtil.isNotEmpty(detailOutData.getApAddAmt())) {
                    b = a.add(new BigDecimal(detailOutData.getApAddAmt()));
                } else if(ObjectUtil.isNotEmpty(detailOutData.getApAddRate()) && ObjectUtil.isEmpty(detailOutData.getApAddAmt())){
                    if (ObjectUtil.isEmpty(detailOutData.getApAddRate())) {
                        if (ObjectUtil.isEmpty(detailOutData.getAddAmt())) {
                            if (ObjectUtil.isNotEmpty(detailOutData.getAddRate())) {
                                b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                            }
                        } else {
                            b = a.add(new BigDecimal(detailOutData.getAddAmt()));
                        }
                    } else {
                        b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                    }
                }else if(ObjectUtil.isEmpty(detailOutData.getAddRate())
                        && ObjectUtil.isEmpty(detailOutData.getApAddAmt())
                        && ObjectUtil.isNotEmpty(detailOutData.getApCataloguePrice())){
                    b = new BigDecimal(detailOutData.getApCataloguePrice());
                }
                if (b.compareTo(BigDecimal.ZERO) == 1) {
                    detailOutData.setCheckOutAmt(b.toString());
                }
                //格式化补货量
                if(org.apache.commons.lang3.StringUtils.isNotBlank(detailOutData.getGsrdNeedQty())&&detailOutData.getGsrdNeedQty().matches("^[0-9]+(.[0-9]+)?$")){
                    DecimalFormat decimalFormat = new DecimalFormat("#.####");
                    detailOutData.setGsrdNeedQty(decimalFormat.format(new BigDecimal(detailOutData.getGsrdNeedQty())));
                }
            }
            log.info("补货单："+ inData.getGsrhVoucherId() +",委托第三方编码匹配开始");
            //委托第三方编码匹配
            String wtpsParam = storeDataMapper.selectWtpsParam(inData.getClientId(),"WTPS_SET");
            if (StringUtils.isNullOrEmpty(wtpsParam)){
                wtpsParam = "0";
            }
            if ("1".equals(wtpsParam)){
                //获取商品匹配编码
                List<Map<String,String>> proThreeCodeList = replenishDMapper.selectProThreeCode(inData.getClientId(),"");
                //获取第三方仓库库存
                List<GaiaSdDsfStock> proThreeStockList = replenishDMapper.selectProThreeStock(inData.getClientId(),"");
                //获取特殊商品分类编码
                List<SpecialClass> specialClassList = replenishDMapper.selectSpecialClass(inData.getClientId(),inData.getStoCode(),"");
                for (GetReplenishDetailOutData detail : detailList) {
                    for (SpecialClass specialClass : specialClassList) {
                        if ("1".equals(specialClass.getStockType())) {
                            if (specialClass.getBigClass().equals(detail.getBigClass().split("-")[0])) {
                                //显示第三方商品编码
                                boolean f = true;
                                for (Map<String, String> map : proThreeCodeList) {
                                    if (map.get("proCode").equals(detail.getGsrdProId())) {
                                        detail.setProThreeCode(map.get("proThreeCode"));
                                        detail.setHasProThreeCode("1");
                                        f = false;
                                    }
                                }
                                if (f) {
                                    detail.setHasProThreeCode("0");
                                }
                                BigDecimal stock = BigDecimal.ZERO;
                                if (ObjectUtil.isNotEmpty(detail.getGsrdStockDepot())) {
                                    stock = new BigDecimal(detail.getGsrdStockDepot());
                                }
                                //商品仓库库存累加
                                for (GaiaSdDsfStock c : proThreeStockList) {
                                    if (c.getProId().equals(detail.getGsrdProId())) {
                                        detail.setGsrdStockDepot(stock.add(c.getQty()).toString());
                                    }
                                }
                            }
                        }
                    }
                }
                log.info("补货单："+ inData.getGsrhVoucherId() +"是委托第三方单");
            }
            log.info("补货单："+ inData.getGsrhVoucherId() +",委托第三方编码匹配结束");
            log.info("补货单："+ inData.getGsrhVoucherId() +",冷链第三方编码匹配开始");
            //冷链第三方编码匹配
            String coldChainParam = storeDataMapper.selectWtpsParam(inData.getClientId(),"COLD_CHAIN");
            if (StringUtils.isNullOrEmpty(coldChainParam)){
                coldChainParam = "0";
            }
            if ("1".equals(coldChainParam)) {
                List<Map<String, String>> proThreeCodeList = replenishDMapper.selectProThreeCode(inData.getClientId(), "");
                if (!CollectionUtils.isNullOrEmpty(proThreeCodeList)) {
                    for (GetReplenishDetailOutData detail : detailList) {
                        for (Map<String, String> map : proThreeCodeList) {
                            if (map.get("proCode").equals(detail.getGsrdProId())) {
                                detail.setProThreeCode(map.get("proThreeCode"));
                                detail.setHasProThreeCode("1");
                                break;
                            }
                        }
                    }
                }
                log.info("补货单："+ inData.getGsrhVoucherId() +"是冷链第三方单");
            }
            log.info("补货单："+ inData.getGsrhVoucherId() +",冷链第三方编码匹配结束");
            log.info("补货单："+ inData.getGsrhVoucherId() +"在途量查询开始");
            try {
                detailList = getProductOnWayNumber(inData.getClientId(), inData.getStoCode(), false, detailList);
            } catch (Exception e) {
                log.error("在途量计算错误：{}",e.getMessage(), e);
                throw new BusinessException("提示：在途量计算错误");
            }
            log.info("补货单："+ inData.getGsrhVoucherId() +"在途量查询结束");
            outData.setDetailList(detailList);
            log.info("补货单："+ inData.getGsrhVoucherId() +"明细查询结束");
        }
        return outData;
    }


    @Override
    public String selectNextVoucherId(GaiaSdReplenishH inData) {
        String voucherId = commonMapper.selectNextCodeByReplenish(inData.getClientId(),"PD");
//        inData.setGsrhBrId(null);
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(voucherId)){//存在则变更code_val表信息
            commonMapper.addBatchToCode(inData.getClientId(),voucherId,"PD");
        }
        return commonMapper.selectNextCode(inData.getClientId(),"PD",6);
    }

    @Override
    public void deleteDetail(GetReplenishInData inData) {
        this.replenishDMapper.deleteDetail(inData);
    }

    @Override
    public GetReplenishDetailOutData replenishDetail(GetReplenishInData inData) {
        List<ReplenishGiftOutParam> giftList = new ArrayList<>();
        StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(),inData.getStoCode());
        if (storeOutData != null){
            if (ObjectUtil.isNotEmpty(storeOutData.getStoDcCode())){
                inData.setDcCode(storeOutData.getStoDcCode());
                List<ReplenishGiftOutParam> gift = replenishGiftMapper.selectGiftList(inData.getClientId(),storeOutData.getStoDcCode(),"",inData.getProductCode());//查询商品是否为赠品
                if (ObjectUtil.isNotEmpty(gift) && gift.size() > 0){
                    throw new BusinessException("该商品为赠品商品，不能手工添加补货！");
                }
                giftList = replenishGiftMapper.selectGiftList(inData.getClientId(),storeOutData.getStoDcCode(),inData.getProductCode(),"");//查询商品是否有赠品
            }
        }
        //根据补货单号判断新加入商品是否为此集合商品
        GetReplenishOutData replenishOutData = new GetReplenishOutData();
        if (ObjectUtil.isNotEmpty(inData.getGsrhVoucherId())){
            replenishOutData = replenishHMapper.selectReplenishOrder(inData.getClientId(),inData.getStoCode(),inData.getGsrhVoucherId());
        }
        GetReplenishDetailOutData detailOutData = productBasicMapper.queryProFromReplenishDetail(inData);
        String bigClass = detailOutData.getBigClass().split("-")[0];
        //获取特殊商品分类编码
        List<SpecialClass> specialClassList = replenishDMapper.selectSpecialClass(inData.getClientId(),inData.getStoCode(),bigClass);
        if (ObjectUtil.isNotEmpty(detailOutData)) {
            //分单设置拦截
            if (ObjectUtil.isNotEmpty(replenishOutData)) {
                if ("1".equals(replenishOutData.getGsrhPart())) {
                    if (!"3".equals(detailOutData.getProStorageCondition())) {
                        throw new BusinessException("商品:"+detailOutData.getGsrdProId()+"不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                    }
                } else if ("2".equals(replenishOutData.getGsrhPart())) {
                    if (!"5".equals(detailOutData.getProControlClass())) {
                        throw new BusinessException("商品:"+detailOutData.getGsrdProId()+"不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                    }
                } else if ("3".equals(replenishOutData.getGsrhPart())) {
                    if (!"4".equals(detailOutData.getProControlClass())) {
                        throw new BusinessException("商品"+detailOutData.getGsrdProId()+"不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                    }
                } else if ("4".equals(replenishOutData.getGsrhPart())) {
                    if (!"11".equals(detailOutData.getProControlClass())) {
                        throw new BusinessException("商品"+detailOutData.getGsrdProId()+"不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                    }
                } else if ("5".equals(replenishOutData.getGsrhPart())) {
                    if (!"3".equals(detailOutData.getProStorageArea())) {
                        throw new BusinessException("商品"+detailOutData.getGsrdProId()+"不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                    }
                }else {
                    if (ObjectUtil.isNotEmpty(replenishOutData)){
                        if ("1".equals(replenishOutData.getIsSpecial())) {
                            String gsrhAddr = replenishOutData.getGsrhAddr();
                            String clientId = inData.getClientId();
                            if ("3".equals(replenishOutData.getGsrhType())) {//委托配送单
                                SpecialClass specialClass = specialClassList.stream().filter(f -> clientId.equals(f.getClientId()) && "1".equals(f.getStockType()) && f.getStockCode().equals(gsrhAddr) && bigClass.equals(f.getBigClass())).findAny().orElse(null);
                                if (ObjectUtil.isEmpty(specialClass)){
                                    throw new BusinessException("商品"+detailOutData.getGsrdProId()+"不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                                }
                            }else {//配送中心配送单
                                SpecialClass specialClass = specialClassList.stream().filter(f -> clientId.equals(f.getClientId()) && "0".equals(f.getStockType()) && f.getStockCode().equals(gsrhAddr) && bigClass.equals(f.getBigClass())).findAny().orElse(null);
                                if (ObjectUtil.isEmpty(specialClass)){
                                    throw new BusinessException("商品"+detailOutData.getGsrdProId()+"不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                                }
                            }
                        }else if("0".equals(replenishOutData.getIsSpecial())){
                            if("1".equals(detailOutData.getIsSpecial())){
                                throw new BusinessException("商品"+detailOutData.getGsrdProId()+"不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                            }
                        }
                    }
                }
            }
            detailOutData.setHasGift("0");
            if (ObjectUtil.isNotEmpty(giftList) && giftList.size() > 0){
                detailOutData.setHasGift("1");
            }
            //非原始单数据
            detailOutData.setIsOriginal(Boolean.FALSE);

            inData.setParamCode("REPLENISH_MINQTY");
            GetReplenishOutData sales = this.getSales(inData);
            GetReplenishOutData stocks = this.getStock(inData);
            GetReplenishOutData costs = this.getCost(inData);
//            GetReplenishOutData counts = this.getCount(inData);
//        GetReplenishOutData amts = this.getVoucherAmt(inData);
            GetReplenishOutData param = this.getParam(inData);
            if (ObjectUtil.isNull(sales)) {
                detailOutData.setGsrdSalesDays1("0");
                detailOutData.setGsrdSalesDays2("0");
                detailOutData.setGsrdSalesDays3("0");
            } else {
                detailOutData.setGsrdSalesDays1(sales.getSalesCountOne());
                detailOutData.setGsrdSalesDays2(sales.getSalesCountTwo());
                detailOutData.setGsrdSalesDays3(sales.getSalesCountThree());
            }

            detailOutData.setGsrdStockStore(stocks.getStock());
            if (ObjectUtil.isNotNull(costs)) {
                detailOutData.setGsrdAverageCost(costs.getAverageCost());
                detailOutData.setGsrdGrossMargin(costs.getBatchCost());
            }
            detailOutData.setGsrdDisplayMin(param.getParam());
            // 建议补货量为0的不显示
//            if (ObjectUtil.isNotNull(counts)) {
//                detailOutData.setGsrdNeedQty(counts.getGsrhTotalQty());
//                detailOutData.setGsrdProposeQty(counts.getGsrhTotalQty());
//            } else {
            detailOutData.setGsrdNeedQty("0");
            detailOutData.setGsrdProposeQty("0");
//            }
            if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1) {
                BigDecimal voucherAmt = ((new BigDecimal(detailOutData.getMatTotalAmt()).add(new BigDecimal(detailOutData.getMatRateAmt())))).divide(new BigDecimal(detailOutData.getMatTotalQty()), 4, BigDecimal.ROUND_HALF_UP);
                detailOutData.setVoucherAmt(voucherAmt.toString());
            } else {
                if (ObjectUtil.isEmpty(detailOutData.getMatMovPrice())) {
                    detailOutData.setMatMovPrice("0");
                }
                detailOutData.setVoucherAmt(detailOutData.getMatMovPrice());
            }
            BigDecimal a = BigDecimal.ZERO;
            BigDecimal b = BigDecimal.ZERO;
            if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1) {
                a = new BigDecimal(detailOutData.getVoucherAmt());
            } else {
                String num = detailOutData.getGsrdInputTaxValue().replace("%", "");
                BigDecimal rate = new BigDecimal(num).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP);
                a = new BigDecimal(detailOutData.getMatMovPrice()).multiply(new BigDecimal("1").add(rate)).setScale(4, BigDecimal.ROUND_HALF_UP);
            }
            if (ObjectUtil.isNotEmpty(detailOutData.getApAddAmt())) {
                b = a.add(new BigDecimal(detailOutData.getApAddAmt()));
            } else if(ObjectUtil.isNotEmpty(detailOutData.getApAddRate()) && ObjectUtil.isEmpty(detailOutData.getApAddAmt())){
                if (ObjectUtil.isEmpty(detailOutData.getApAddRate())) {
                    if (ObjectUtil.isEmpty(detailOutData.getAddAmt())) {
                        if (ObjectUtil.isNotEmpty(detailOutData.getAddRate())) {
                            b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                        }
                    } else {
                        b = a.add(new BigDecimal(detailOutData.getAddAmt()));
                    }
                } else {
                    b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                }
            }else if(ObjectUtil.isEmpty(detailOutData.getAddRate())
                    && ObjectUtil.isEmpty(detailOutData.getApAddAmt())
                    && ObjectUtil.isNotEmpty(detailOutData.getApCataloguePrice())){
                b = new BigDecimal(detailOutData.getApCataloguePrice());
            }
            if (b.compareTo(BigDecimal.ZERO) == 1) {
                detailOutData.setCheckOutAmt(b.toString());
            }
            // 格式化销售量
            detailOutData.setGsrdSalesDays1(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays1(), ".0000"));
            detailOutData.setGsrdSalesDays2(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays2(), ".0000"));
            detailOutData.setGsrdSalesDays3(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays3(), ".0000"));
            detailOutData.setGsrdProposeQty(StrUtil.removeSuffix(detailOutData.getGsrdProposeQty(), ".0000"));
            detailOutData.setGsrdNeedQty(StrUtil.removeSuffix(detailOutData.getGsrdNeedQty(), ".0000"));
            detailOutData.setGsrdStockStore(StrUtil.isEmpty(StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000")) ? "0" : StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000"));
            //委托第三方编码匹配
            String wtpsParam = storeDataMapper.selectWtpsParam(inData.getClientId(),"WTPS_SET");
            if (StringUtils.isNullOrEmpty(wtpsParam)){
                wtpsParam = "0";
            }
            if ("1".equals(wtpsParam)){
                //获取商品匹配编码
                List<Map<String,String>> proThreeCodeList = replenishDMapper.selectProThreeCode(inData.getClientId(),detailOutData.getGsrdProId());
                //获取第三方仓库库存
                List<GaiaSdDsfStock> proThreeStockList = replenishDMapper.selectProThreeStock(inData.getClientId(),detailOutData.getGsrdProId());
                for (SpecialClass specialClass : specialClassList) {
                    if ("1".equals(specialClass.getStockType())) {
                        if (specialClass.getBigClass().equals(detailOutData.getBigClass().split("-")[0])) {
                            //显示第三方商品编码
                            boolean f = true;
                            for (Map<String, String> map : proThreeCodeList) {
                                if (map.get("proCode").equals(detailOutData.getGsrdProId())) {
                                    detailOutData.setProThreeCode(map.get("proThreeCode"));
                                    detailOutData.setHasProThreeCode("1");
                                    f = false;
                                }
                            }
                            if (f) {
                                detailOutData.setHasProThreeCode("0");
                            }
                            BigDecimal stock = BigDecimal.ZERO;
                            if (ObjectUtil.isNotEmpty(detailOutData.getGsrdStockDepot())) {
                                stock = new BigDecimal(detailOutData.getGsrdStockDepot());
                            }
                            //商品仓库库存累加
                            for (GaiaSdDsfStock c : proThreeStockList) {
                                if (c.getProId().equals(detailOutData.getGsrdProId())) {
                                    detailOutData.setGsrdStockDepot(stock.add(c.getQty()).toString());
                                }
                            }
                        }
                    }
                }
            }
        }else{
            throw new BusinessException("查询商品不存在");
        }
        return detailOutData;
    }

    @Override
    public String addRepPrice(RatioProInData inData) {
        Map<String,String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(),inData.getBrId());
        String pgId = productBasicMapper.selectNextPoId(inData.getClientId());
        if (stoInfo.get("stoAttribute").equals("2")){
            inData.setBrId(stoInfo.get("stoChainHead"));
        }
        List<RatioProDetail> list = this.productBasicMapper.selectRatioStatus(inData);
        if (list.size() > 0) {
            inData.setPgId(list.get(0).getPgId());
            if (!list.get(0).getRepStatus().equals("2")) {
                productBasicMapper.updateStatus(inData);
            }
        }
        List<RatioProDetail> proDetail = inData.getProDetail();
        if (proDetail.size() > 0) {
            int i = 1;
            for (RatioProDetail detail : proDetail) {
                detail.setClientId(inData.getClientId());
                detail.setPgId(pgId);
                detail.setPgItem(String.valueOf(i));
                detail.setRepType("0");
                detail.setBrId(inData.getBrId());
                if (stoInfo.get("stoAttribute").equals("2")) {
                    detail.setRepType("1");
                    detail.setBrId(stoInfo.get("stoChainHead"));
                }
                detail.setRepDate(CommonUtil.getyyyyMMdd());
                detail.setCreateDate(CommonUtil.getyyyyMMdd());
                detail.setCreateTime(CommonUtil.getHHmmss());
                detail.setCreateUser(inData.getUserId());
                detail.setRepStatus("0");
                if (ObjectUtil.isEmpty(detail.getGsrdFlag())){
                    detail.setGsrdFlag("");
                }
                i++;
            }
            this.productBasicMapper.addRepPrice(proDetail);
        }
        return pgId;
    }

    @Override
    public Map<String, Object> selectRatioStatus(RatioProInData inData) {
        Map<String,String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(),inData.getBrId());
        Map<String,String> map = new HashMap<>();
        map.put("client",inData.getClientId());
        map.put("supSite",inData.getBrId());
        if (stoInfo.get("stoAttribute").equals("2")){
            inData.setBrId(stoInfo.get("stoChainHead"));
            map.put("supSite",stoInfo.get("stoDcCode"));
        }
        List<Map<String,String>> supList = productBasicMapper.selectSupplierList(map);
        Map<String,Object> result = new HashMap<>();
        if (ObjectUtil.isNotEmpty(inData.getPgId())) {
            map.put("pgId",inData.getPgId());
            List<Map<String,String>> supGetList = productBasicMapper.selectGetPriceBySup(map);
            List<RatioProDetail> list = this.productBasicMapper.selectRatioStatus(inData);
            if (list.size() > 0){
                if ("1".equals(list.get(0).getRepStatus())){
                    if (supList.size() > 0) {
                        BigDecimal a = (new BigDecimal(supGetList.size()).divide(new BigDecimal(supList.size()),2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100"))).setScale(2, RoundingMode.HALF_UP);
                        result.put("repStatus", "1");
                        result.put("speed",a);
                        result.put("msg", "比价中");
                    }
                }else if("0".equals(list.get(0).getRepStatus())){
                    result.put("repStatus", "0");
                    result.put("speed",0);
                    result.put("msg", "比价中");
                }else if("2".equals(list.get(0).getRepStatus())){
                    result.put("repStatus", "2");
                    result.put("speed",100);
                    result.put("msg", "比价已完成");
                }else {
                    result.put("repStatus", "3");
                    result.put("speed",100);
                    result.put("msg", "由于其他人员正在比价，当前比价已中断，您可以刷新页面查看比价结果");
                }
            }
        }else{
            result.put("repStatus", "0");
            result.put("speed",0);
            result.put("msg", "比价中");
        }
        return result;
    }

    @Override
    public Map<String, Object> sortImportData(GetReplenishInData inData) {
        Map<String,Object> results = new HashMap<>();
        List<GetReplenishDetailInData> detailList = inData.getDetailList();
        List<GetReplenishDetailInData> result = detailList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGsrdLastSupid()))), ArrayList::new)
        );
        for (GetReplenishDetailInData item : result){
            Map<String,Object> map = new HashMap<>();
            List<GetReplenishDetailInData> itemlist = new ArrayList<>();
            for (GetReplenishDetailInData detail : detailList) {
                if (detail.getGsrdLastSupid().equals(item.getGsrdLastSupid())){
                    itemlist.add(detail);
                }
            }
            results.put(item.getGsrdLastSupName(),itemlist);
        }
        return results;
    }

    @Override
    public List<GetReplenishDetailOutData> detaiAutolList(GetReplenishInData inData) {
        GetAcceptInData inData1 = new GetAcceptInData();
        inData1.setClientId(inData.getClientId());
        inData1.setStoreCode(inData.getStoCode());
        Map<String,String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(),inData.getStoCode());
        if (stoInfo.get("stoAttribute").equals("2")){
            inData1.setStoreCode(stoInfo.get("stoChainHead"));
            inData.setStoCode(null);
            inData.setStoChainHead(stoInfo.get("stoChainHead"));
            inData.setDcCode(stoInfo.get("stoDcCode"));
        }
        List<GetReplenishDetailOutData> outDataList = productBasicMapper.queryProAutoReplenish(inData);
        inData.setParamCode("REPLENISH_MINQTY");
        Map<String, GetReplenishOutData> saleMap = this.getSalesMap(inData);
        Map<String, String> stockMap = this.getStockMap(inData);
//        Map<String, GetReplenishOutData> costMap = this.getCostMap(inData);;
        Map<String, String> amtMap = this.getVoucherAmtMap(inData);
        GetReplenishOutData param = this.getParam(inData);
        Map<String, QuerySupplierData> supplierNameMap = this.getSupplierNameMap(inData.getClientId(),inData.getStoCode());

        Iterator var9 = outDataList.iterator();
        while (var9.hasNext()) {
            GetReplenishDetailOutData detailOutData = (GetReplenishDetailOutData) var9.next();
            inData.setProductCode(detailOutData.getGsrdProId());
            GetReplenishOutData replenishOutData = saleMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNull(replenishOutData)) {
                detailOutData.setGsrdSalesDays1("0");
                detailOutData.setGsrdSalesDays2("0");
                detailOutData.setGsrdSalesDays3("0");
            } else {
                detailOutData.setGsrdSalesDays1(replenishOutData.getSalesCountOne());
                detailOutData.setGsrdSalesDays2(replenishOutData.getSalesCountTwo());
                detailOutData.setGsrdSalesDays3(replenishOutData.getSalesCountThree());
            }

            detailOutData.setGsrdStockStore(stockMap.get(detailOutData.getGsrdProId()));
//            replenishOutData = costMap.get(detailOutData.getGsrdProId());
//            if (ObjectUtil.isNotNull(replenishOutData)) {
//                detailOutData.setGsrdAverageCost(replenishOutData.getAverageCost());
//                detailOutData.setGsrdGrossMargin(replenishOutData.getBatchCost());
//            }
//            QuerySupplierData supplierData = supplierNameMap.get(detailOutData.getGsrdProId());
//            if (ObjectUtil.isNotEmpty(supplierData)) {
//                detailOutData.setGsrdSupName(supplierData.getSupName());
//            }
            detailOutData.setGsrdDisplayMin(param.getParam());
            String amt = amtMap.get(inData.getProductCode());
            if (StrUtil.isNotBlank(amt)) {
                detailOutData.setVoucherAmt(amt);
            } else {
                detailOutData.setVoucherAmt("0");
            }
            // 格式化销售量
            detailOutData.setGsrdSalesDays1(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays1(), ".0000"));
            detailOutData.setGsrdSalesDays2(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays2(), ".0000"));
            detailOutData.setGsrdSalesDays3(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays3(), ".0000"));
            detailOutData.setGsrdProposeQty(StrUtil.removeSuffix(detailOutData.getGsrdProposeQty(), ".0000"));
            detailOutData.setGsrdNeedQty(StrUtil.removeSuffix(detailOutData.getGsrdNeedQty(), ".0000"));
            detailOutData.setGsrdStockStore(StrUtil.isEmpty(StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000")) ? "0" : StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000"));
        }

        outDataList = new Ordering<GetReplenishDetailOutData>() {
            @Override
            public int compare(GetReplenishDetailOutData left, GetReplenishDetailOutData right) {
                int leftQty = NumberUtil.parseInt(left.getGsrdNeedQty());
                int rightQty = NumberUtil.parseInt(right.getGsrdNeedQty());
                return rightQty - leftQty;
            }
        }.immutableSortedCopy(outDataList);
        //放置可选供应商
        List<SupPriceRatioOutData> supPrice =  this.supplierBusinessMapper.getRatioSupPrice(inData1);
        for (GetReplenishDetailOutData data : outDataList) {
            data.setSupCode1("");
            data.setSupName1("");
            data.setSupPrice1("");
            data.setSupUpdateDate1("");
            data.setSupCode2("");
            data.setSupName2("");
            data.setSupPrice2("");
            data.setSupUpdateDate2("");
            data.setSupCode3("");
            data.setSupName3("");
            data.setSupPrice3("");
            data.setSupUpdateDate3("");
            if (ObjectUtil.isNotNull(supPrice) && supPrice.size() > 0){
                List<SupPriceRatioOutData> result = supPrice.stream().filter(u->u.getProCode().equals(data.getGsrdProId())).collect(Collectors.toList());
                if (ObjectUtil.isNotNull(result) && result.size() > 0) {
                    for (int i = 0; i < result.size(); i++) {
                        String dateString = "";
                        if (ObjectUtil.isNotEmpty(result.get(i).getSupUpdateDate())
                                && ObjectUtil.isNotEmpty(result.get(i).getSupUpdateTime())) {
                            try {
                                DateFormat format = new SimpleDateFormat("yyyyMMdd");
                                Date dateTime1 = format.parse(result.get(i).getSupUpdateDate());
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String date1 = formatter.format(dateTime1);
                                DateFormat format1 = new SimpleDateFormat("HHmmss");
                                Date dateTime2 = format1.parse(result.get(i).getSupUpdateTime());
                                SimpleDateFormat formatter1 = new SimpleDateFormat("HH:mm:ss");
                                String time = formatter1.format(dateTime2);
                                dateString = date1 + " " + time;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if (i == 0){
                            data.setSupCode1(result.get(0).getSupCode());
                            data.setSupName1(result.get(0).getSupName());
                            data.setSupPrice1(result.get(0).getProPrice());
                            data.setSupUpdateDate1(dateString);
                        }else if (i == 1){
                            data.setSupCode2(result.get(1).getSupCode());
                            data.setSupName2(result.get(1).getSupName());
                            data.setSupPrice2(result.get(1).getProPrice());
                            data.setSupUpdateDate2(dateString);
                        }else if (i == 2){
                            data.setSupCode3(result.get(2).getSupCode());
                            data.setSupName3(result.get(2).getSupName());
                            data.setSupPrice3(result.get(2).getProPrice());
                            data.setSupUpdateDate3(dateString);
                        }
                    }
                }
            }
        }
        return outDataList;
    }

    @Override
    public void saveAutoPrice(GetReplenishInData inData, GetLoginOutData userInfo) {
        GetAcceptInData inData1 = new GetAcceptInData();
        inData1.setClientId(inData.getClientId());
        inData1.setStoreCode(userInfo.getDepId());
        Map<String,String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(),userInfo.getDepId());
        String dcCode = "";
        if (stoInfo.get("stoAttribute").equals("2")){
            inData1.setStoreCode(stoInfo.get("stoChainHead"));
            dcCode = stoInfo.get("stoDcCode");
        }else{
            dcCode = userInfo.getDepId();
        }
//        inData.setDcCode(compadmInfo.get(0).getDcCode());
        List<GetReplenishInData> replenishInDataList = new ArrayList<>();
        List<GetReplenishDetailInData> results = new ArrayList<>();
        List<GetReplenishDetailInData> orderList = inData.getDetailList();
        results = inData.getDetailList().stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGsrdLastSupid() + f.getGsrdBrId() + f.getGsrdVoucherId()))), ArrayList::new)
        );
        for (GetReplenishDetailInData detail : results) {
            if (ObjectUtil.isNotEmpty(detail.getGsrdLastSupid())) {
                detail.setClientId(inData.getClientId());
                GetReplenishInData replenishInData = new GetReplenishInData();
                replenishInData.setClientId(inData.getClientId());
                replenishInData.setGsrhVoucherId(detail.getGsrdVoucherId());
                replenishInData.setStoCode(detail.getGsrdBrId());
//                replenishInData.setGsrhType("3");
                replenishInData.setGsrhPattern("0");
                if (ObjectUtil.isNotEmpty(dcCode)) {
                    replenishInData.setDcCode(dcCode);
                }
                List<GetReplenishDetailInData> detailList = new ArrayList<>();
                for (GetReplenishDetailInData item : orderList) {
                    if (ObjectUtil.isNotEmpty(item.getGsrdLastSupid())) {
                        if (detail.getGsrdLastSupid().equals(item.getGsrdLastSupid())
                                && detail.getGsrdBrId().equals(item.getGsrdBrId())
                                && detail.getGsrdVoucherId().equals(item.getGsrdVoucherId())) {
                            GetReplenishDetailInData detailInData = new GetReplenishDetailInData();
                            detailInData.setClientId(inData.getClientId());
                            detailInData.setGsrdDate(item.getGsrdDate());
                            detailInData.setGsrdProUnit(item.getGsrdProUnit());
                            detailInData.setGsrdProId(item.getGsrdProId());
                            detailInData.setGsrdProName(item.getGsrdProName());
                            detailInData.setGsrdBrId(item.getGsrdBrId());
                            detailInData.setGsrdBrName(item.getGsrdBrName());
                            detailInData.setGsrdAverageCost(item.getGsrdAverageCost());
                            detailInData.setGsrdNeedQty(item.getGsrdNeedQty());
                            detailInData.setGsrdProposeQty(item.getGsrdProposeQty());
                            detailInData.setGsrdLastSupid(item.getGsrdLastSupid());
                            detailInData.setGsrdLastSupName(item.getGsrdLastSupName());
                            detailInData.setGsrdProPrice(item.getGsrdProPrice());
                            if (ObjectUtil.isEmpty(item.getGsrdAverageCost())) {
                                item.setGsrdAverageCost(BigDecimal.ZERO);
                            }
                            detailInData.setVoucherAmt(item.getGsrdAverageCost().multiply(new BigDecimal(item.getGsrdNeedQty())).toString());
                            detailInData.setGsrdDisplayMin(item.getGsrdDisplayMin());
                            if (ObjectUtil.isNotEmpty(item.getGsrdGrossMargin())) {
                                detailInData.setGsrdGrossMargin(item.getGsrdGrossMargin().toString());
                            }
                            detailInData.setGsrdSalesDays1(item.getGsrdSalesDays1());
                            detailInData.setGsrdSalesDays2(item.getGsrdSalesDays2());
                            detailInData.setGsrdSalesDays3(item.getGsrdSalesDays3());
                            detailInData.setGsrdSerial(item.getGsrdSerial());
                            detailInData.setGsrdVoucherId(item.getGsrdVoucherId());
                            detailInData.setGsrdPackMidsize(item.getGsrdPackMidsize());
                            detailInData.setGsrdStockDepot(item.getGsrdStockDepot());
                            detailInData.setGsrdStockStore(item.getGsrdStockStore());
//                    detailInData.setGsrdProRate(item.getGsrdTaxTate());
                            detailInData.setGsrdTaxTate(item.getGsrdTaxTate());
                            detailInData.setGsrdLastPrice(item.getGsrdLastPrice());
                            detailInData.setPoPrice(item.getPoPrice());
                            detailInData.setGsrdProCompany(item.getGsrdProCompany());
                            detailInData.setGsrdProPec(item.getGsrdProPec());
                            detailList.add(detailInData);
                        }
                    }
                }
                replenishInData.setDetailList(detailList);
                if (ObjectUtil.isNotEmpty(inData.getGsrhVoucherId())){
                    this.update(replenishInData);
                }else{
                    this.insert(replenishInData,userInfo);
                }
                String vv = this.insertPo(replenishInData, userInfo);
                replenishInData.setGsrhVoucherId(vv);
                replenishInDataList.add(replenishInData);
            }
        }
        for (GetReplenishInData replenishInData : replenishInDataList){
            String voucherId = replenishInData.getGsrhVoucherId();
            List<GetReplenishDetailInData> detailInDataList = replenishInData.getDetailList();
            for (GetReplenishDetailInData detailInData : detailInDataList){
                detailInData.setGsrdVoucherId(voucherId);
            }
        }
//        List<GetReplenishDetailInData> outItemDataList = new ArrayList<>();
//        for (GetReplenishInData replenishInData : replenishInDataList){
//            outItemDataList.addAll(replenishInData.getDetailList());
//        }
//        List<GetReplenishDetailInData> sortList = outItemDataList.stream().collect(
//                Collectors.collectingAndThen(
//                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGsrdLastSupid()))), ArrayList::new)
//        );
//        List<GetReplenishInData> outDataList = new ArrayList<>();
//        for (GetReplenishDetailInData sort : sortList){
//            GetReplenishInData outData = new GetReplenishInData();
//            outData.setGspgProId(sort.getGsrdLastSupid());
//            List<GetReplenishDetailInData> item = new ArrayList<>();
//            for (GetReplenishDetailInData replenishInData : outItemDataList) {
//                if (replenishInData.getGsrdLastSupid().equals(sort.getGsrdLastSupid())){
//                    item.add(replenishInData);
//                }
//            }
//            outData.setDetailList(item);
//            outDataList.add(outData);
//        }
    }

    @Override
    public Map<String, Object> detailAutoProList(GetReplenishInData inData) {
        Map<String, Object> map = new HashMap<>();
        GetAcceptInData inData1 = new GetAcceptInData();
        inData1.setClientId(inData.getClientId());
        inData1.setStoreCode(inData.getStoCode());
        List<GetReplenishDetailOutData> outDataList = productBasicMapper.queryProFromReplenish(inData);
        inData.setParamCode("REPLENISH_MINQTY");
        Map<String, GetReplenishOutData> saleMap = this.getSalesMap(inData);
        Map<String, String> stockMap = this.getStockMap(inData);
        Map<String, GetReplenishOutData> costMap = this.getCostMap(inData);
        GetReplenishOutData param = this.getParam(inData);
        Map<String, String> countMap = this.getCountMap(inData,saleMap,stockMap,outDataList);
        Map<String, String> amtMap = this.getVoucherAmtMap(inData);
        Map<String, QuerySupplierData> supplierNameMap = this.getSupplierNameMap(inData.getClientId(),inData.getStoCode());

        Iterator var9 = outDataList.iterator();
        while (var9.hasNext()) {
            GetReplenishDetailOutData detailOutData = (GetReplenishDetailOutData) var9.next();
            inData.setProductCode(detailOutData.getGsrdProId());
            GetReplenishOutData replenishOutData = saleMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNull(replenishOutData)) {
                detailOutData.setGsrdSalesDays1("0");
                detailOutData.setGsrdSalesDays2("0");
                detailOutData.setGsrdSalesDays3("0");
            } else {
                detailOutData.setGsrdSalesDays1(replenishOutData.getSalesCountOne());
                detailOutData.setGsrdSalesDays2(replenishOutData.getSalesCountTwo());
                detailOutData.setGsrdSalesDays3(replenishOutData.getSalesCountThree());
            }

            detailOutData.setGsrdStockStore(stockMap.get(detailOutData.getGsrdProId()));
//            replenishOutData = costMap.get(detailOutData.getGsrdProId());
//            if (ObjectUtil.isNotNull(replenishOutData)) {
//                detailOutData.setGsrdAverageCost(replenishOutData.getAverageCost());
//                detailOutData.setGsrdGrossMargin(replenishOutData.getBatchCost());
//            }
//            QuerySupplierData supplierData = supplierNameMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNotEmpty(detailOutData.getGsrdLastSupid())) {
                detailOutData.setGsrdSupName(detailOutData.getGsrdLastSupid());
                detailOutData.setGsrdSupName(detailOutData.getGsrdLastSupName());
                if (ObjectUtil.isNotEmpty(detailOutData.getGsrdLastPrice())){
                    detailOutData.setPoPrice(detailOutData.getGsrdLastPrice());
                }
            }
            detailOutData.setGsrdDisplayMin(param.getParam());
            String count = countMap.get(detailOutData.getGsrdProId());
            // 建议补货量为0的不显示
            if (StrUtil.isNotBlank(count) && !"0".equals(count)) {
                detailOutData.setGsrdNeedQty(count);
                detailOutData.setGsrdProposeQty(count);
            } else {
                var9.remove();
                continue;
            }
            if (ObjectUtil.isNotEmpty(detailOutData.getGsrdDisplayMin())){
                if (new BigDecimal(detailOutData.getGsrdDisplayMin()).compareTo(new BigDecimal(detailOutData.getGsrdNeedQty())) == 1){
                    detailOutData.setGsrdNeedQty(detailOutData.getGsrdDisplayMin());
                    detailOutData.setGsrdProposeQty(detailOutData.getGsrdDisplayMin());
                }
            }
            String amt = amtMap.get(inData.getProductCode());
            if (StrUtil.isNotBlank(amt)) {
                detailOutData.setVoucherAmt(amt);
            } else {
                detailOutData.setVoucherAmt("0");
            }

            // 格式化销售量
            detailOutData.setGsrdSalesDays1(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays1(), ".0000"));
            detailOutData.setGsrdSalesDays2(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays2(), ".0000"));
            detailOutData.setGsrdSalesDays3(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays3(), ".0000"));
            detailOutData.setGsrdProposeQty(StrUtil.removeSuffix(detailOutData.getGsrdProposeQty(), ".0000"));
            detailOutData.setGsrdNeedQty(StrUtil.removeSuffix(detailOutData.getGsrdNeedQty(), ".0000"));
            detailOutData.setGsrdStockStore(StrUtil.isEmpty(StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000")) ? "0" : StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000"));
        }

        outDataList = new Ordering<GetReplenishDetailOutData>() {
            @Override
            public int compare(GetReplenishDetailOutData left, GetReplenishDetailOutData right) {
                int leftQty = NumberUtil.parseInt(left.getGsrdNeedQty());
                int rightQty = NumberUtil.parseInt(right.getGsrdNeedQty());
                return rightQty - leftQty;
            }
        }.immutableSortedCopy(outDataList);
        List<GetReplenishDetailOutData> resultList = new ArrayList<>();
        for (GetReplenishDetailOutData data : outDataList) {
            if (ObjectUtil.isNotEmpty(param)) {
                if (new BigDecimal(data.getGsrdProposeQty()).compareTo(new BigDecimal(1)) == 1) {
                    if (new BigDecimal(data.getGsrdProposeQty()).compareTo(new BigDecimal(param.getParam())) == -1){
                        data.setGsrdNeedQty(param.getParam());
                        data.setGsrdProposeQty(param.getParam());
                    }
                    resultList.add(data);
                }
            }

        }
        //放置可选供应商
        List<SupPriceRatioOutData> supPrice =  this.supplierBusinessMapper.getRatioSupPrice(inData1);
        for (GetReplenishDetailOutData data : resultList) {
            data.setSupCode1("");
            data.setSupName1("");
            data.setSupPrice1("");
            data.setSupUpdateDate1("");
            data.setSupCode2("");
            data.setSupName2("");
            data.setSupPrice2("");
            data.setSupUpdateDate2("");
            data.setSupCode3("");
            data.setSupName3("");
            data.setSupPrice3("");
            data.setSupUpdateDate3("");
            if (ObjectUtil.isNotNull(supPrice) && supPrice.size() > 0){
                List<SupPriceRatioOutData> result = supPrice.stream().filter(u->u.getProCode().equals(data.getGsrdProId())).collect(Collectors.toList());
                if (ObjectUtil.isNotNull(result) && result.size() > 0) {
                    for (int i = 0; i < result.size(); i++) {
                        String dateString = "";
                        if (ObjectUtil.isNotEmpty(result.get(i).getSupUpdateDate())
                                && ObjectUtil.isNotEmpty(result.get(i).getSupUpdateTime())) {
                            try {
                                DateFormat format = new SimpleDateFormat("yyyyMMdd");
                                Date dateTime1 = format.parse(result.get(i).getSupUpdateDate());
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String date1 = formatter.format(dateTime1);
                                DateFormat format1 = new SimpleDateFormat("HHmmss");
                                Date dateTime2 = format1.parse(result.get(i).getSupUpdateTime());
                                SimpleDateFormat formatter1 = new SimpleDateFormat("HH:mm:ss");
                                String time = formatter1.format(dateTime2);
                                dateString = date1 + " " + time;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if (i == 0){
                            data.setSupCode1(result.get(0).getSupCode());
                            data.setSupName1(result.get(0).getSupName());
                            data.setSupPrice1(result.get(0).getProPrice());
                            data.setSupUpdateDate1(dateString);
                        }else if (i == 1){
                            data.setSupCode2(result.get(1).getSupCode());
                            data.setSupName2(result.get(1).getSupName());
                            data.setSupPrice2(result.get(1).getProPrice());
                            data.setSupUpdateDate2(dateString);
                        }else if (i == 2){
                            data.setSupCode3(result.get(2).getSupCode());
                            data.setSupName3(result.get(2).getSupName());
                            data.setSupPrice3(result.get(2).getProPrice());
                            data.setSupUpdateDate3(dateString);
                        }
                    }
                }
            }
        }
        BigDecimal proCount = new BigDecimal(resultList.size());//商品品类数
        BigDecimal proTotalCount = BigDecimal.ZERO;//补货总数
        BigDecimal proTotalAmt = BigDecimal.ZERO;
        for (GetReplenishDetailOutData item : resultList){
            if (ObjectUtil.isEmpty(item.getGsrdNeedQty())){
                item.setGsrdNeedQty("0");
            }
            proTotalCount = proTotalCount.add(new BigDecimal(item.getGsrdNeedQty()));
            if (ObjectUtil.isEmpty(item.getGsrdProPrice())){
                item.setGsrdProPrice("0");
            }
            proTotalAmt = proTotalAmt.add(new BigDecimal(item.getGsrdNeedQty()).multiply(new BigDecimal(item.getGsrdProPrice())));
        }
        map.put("outDataList",outDataList);
        map.put("proCount",proCount);
        map.put("proTotalCount",proTotalCount);
        map.put("proTotalAmt",proTotalAmt);
        return map;
    }

    @Override
    public Map<String,String> isPriceRatio(GetLoginOutData userInfo) {
//        String isPriceRatio = storeDataMapper.selectStoPriceRatio(userInfo.getClient(),userInfo.getDepId());
        String isPriceRatio = storeDataMapper.selectStoPriceComparison(userInfo.getClient(),userInfo.getDepId(),"PRICE_COMPARISON");
        if (ObjectUtil.isEmpty(isPriceRatio)){
            isPriceRatio = "0";
        }
        Map<String,String> result = new HashMap<>();
        result.put("isPriceRatio",isPriceRatio);
        return result;
    }

    @Override
    public Map<String,String> stoAttribute(GetLoginOutData userInfo) {
        String stoAttribute = storeDataMapper.selectStoPriceRatio(userInfo.getClient(),userInfo.getDepId());
//        String isPriceRatio = storeDataMapper.selectStoPriceComparison(userInfo.getClient(),userInfo.getDepId(),"PRICE_COMPARISON");
        if (ObjectUtil.isEmpty(stoAttribute)) {
            stoAttribute = "0";
        }
        Map<String,String> result = new HashMap<>();
        result.put("stoAttribute",stoAttribute);
        return result;
    }

    @Override
    public boolean judgment(GetLoginOutData userInfo) {
//        String globalName = replenishDMapper.judgment(userInfo);
        String globalType = replenishDMapper.globalType(userInfo);

        if ("0".equals(globalType)){
            return true;
        }
        return false;
    }

    @Override
    public Map<String, Object> detailRatioedList(GetReplenishInData inData) {
        Map<String,String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(),inData.getStoCode());
        String stoCode = inData.getStoCode();
        if (stoInfo.get("stoAttribute").equals("2")){
            stoCode = stoInfo.get("stoChainHead");
        }
        List<Map<String,String>> ratioProList = replenishDMapper.queryRatioProList(inData.getClientId(),stoCode,inData.getRatioCode());
        Map<String, Object> map = new HashMap<>();
        GetAcceptInData inData1 = new GetAcceptInData();
        inData1.setClientId(inData.getClientId());
        inData1.setStoreCode(inData.getStoCode());
        String[] gsrdProIds = new String[ratioProList.size()];
        if (ratioProList != null && ratioProList.size() > 0) {
            for (int i = 0; i < ratioProList.size(); i++) {
                gsrdProIds[i] = ratioProList.get(i).get("proId");
            }
            inData.setGspgProIds(gsrdProIds);
        }
        List<GetReplenishDetailOutData> outDataList = productBasicMapper.queryProFromReplenish(inData);
        inData.setParamCode("REPLENISH_MINQTY");
        Map<String, GetReplenishOutData> saleMap = this.getSalesMap(inData);
        Map<String, String> stockMap = this.getStockMap(inData);
        Map<String, GetReplenishOutData> costMap = this.getCostMap(inData);
//        Map<String, String> countMap = this.getCountMap(inData);
        Map<String, String> amtMap = this.getVoucherAmtMap(inData);
        GetReplenishOutData param = this.getParam(inData);
        Map<String, QuerySupplierData> supplierNameMap = this.getSupplierNameMap(inData.getClientId(),inData.getStoCode());

        Iterator var9 = outDataList.iterator();
        while (var9.hasNext()) {
            GetReplenishDetailOutData detailOutData = (GetReplenishDetailOutData) var9.next();
            inData.setProductCode(detailOutData.getGsrdProId());
            GetReplenishOutData replenishOutData = saleMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNull(replenishOutData)) {
                detailOutData.setGsrdSalesDays1("0");
                detailOutData.setGsrdSalesDays2("0");
                detailOutData.setGsrdSalesDays3("0");
            } else {
                detailOutData.setGsrdSalesDays1(replenishOutData.getSalesCountOne());
                detailOutData.setGsrdSalesDays2(replenishOutData.getSalesCountTwo());
                detailOutData.setGsrdSalesDays3(replenishOutData.getSalesCountThree());
            }

            detailOutData.setGsrdStockStore(stockMap.get(detailOutData.getGsrdProId()));
            replenishOutData = costMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNotNull(replenishOutData)) {
                detailOutData.setGsrdAverageCost(replenishOutData.getAverageCost());
                detailOutData.setGsrdGrossMargin(replenishOutData.getBatchCost());
            }
            QuerySupplierData supplierData = supplierNameMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNotEmpty(supplierData)) {
                detailOutData.setGsrdSupName(supplierData.getSupName());
            }
            detailOutData.setGsrdDisplayMin(param.getParam());
//            String count = countMap.get(detailOutData.getGsrdProId());
            for (Map<String,String> ratioMap : ratioProList) {
                if (ratioMap.get("proId").equals(detailOutData.getGsrdProId())){
                    detailOutData.setGsrdNeedQty(ratioMap.get("gsrdNeedQty"));
                    detailOutData.setGsrdProposeQty(ratioMap.get("gsrdNeedQty"));
                    detailOutData.setGsrdFlag(ratioMap.get("gsrdFlag"));
                }
            }
            // 建议补货量为0的不显示
//            if (StrUtil.isNotBlank(count) && !"0".equals(count)) {
//                detailOutData.setGsrdNeedQty(count);
//                detailOutData.setGsrdProposeQty(count);
//            } else {
//                var9.remove();
//                continue;
//            }

            String amt = amtMap.get(inData.getProductCode());
            if (StrUtil.isNotBlank(amt)) {
                detailOutData.setVoucherAmt(amt);
            } else {
                detailOutData.setVoucherAmt("0");
            }
            // 格式化销售量
            detailOutData.setGsrdSalesDays1(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays1(), ".0000"));
            detailOutData.setGsrdSalesDays2(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays2(), ".0000"));
            detailOutData.setGsrdSalesDays3(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays3(), ".0000"));
            detailOutData.setGsrdProposeQty(StrUtil.removeSuffix(detailOutData.getGsrdProposeQty(), ".0000"));
            detailOutData.setGsrdNeedQty(StrUtil.removeSuffix(detailOutData.getGsrdNeedQty(), ".0000"));
            detailOutData.setGsrdStockStore(StrUtil.isEmpty(StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000")) ? "0" : StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000"));
        }

        outDataList = new Ordering<GetReplenishDetailOutData>() {
            @Override
            public int compare(GetReplenishDetailOutData left, GetReplenishDetailOutData right) {
                int leftQty = NumberUtil.parseInt(left.getGsrdNeedQty());
                int rightQty = NumberUtil.parseInt(right.getGsrdNeedQty());
                return rightQty - leftQty;
            }
        }.immutableSortedCopy(outDataList);
        //放置可选供应商
        List<SupPriceRatioOutData> supPrice =  this.supplierBusinessMapper.getRatioSupPrice(inData1);
        for (GetReplenishDetailOutData data : outDataList) {
            data.setSupCode1("");
            data.setSupName1("");
            data.setSupPrice1("");
            data.setSupUpdateDate1("");
            data.setSupCode2("");
            data.setSupName2("");
            data.setSupPrice2("");
            data.setSupUpdateDate2("");
            data.setSupCode3("");
            data.setSupName3("");
            data.setSupPrice3("");
            data.setSupUpdateDate3("");
            if (ObjectUtil.isNotNull(supPrice) && supPrice.size() > 0){
                List<SupPriceRatioOutData> result = supPrice.stream().filter(u->u.getProCode().equals(data.getGsrdProId())).collect(Collectors.toList());
                if (ObjectUtil.isNotNull(result) && result.size() > 0) {
                    for (int i = 0; i < result.size(); i++) {
                        String dateString = "";
                        if (ObjectUtil.isNotEmpty(result.get(i).getSupUpdateDate())
                                && ObjectUtil.isNotEmpty(result.get(i).getSupUpdateTime())) {
                            try {
                                DateFormat format = new SimpleDateFormat("yyyyMMdd");
                                Date dateTime1 = format.parse(result.get(i).getSupUpdateDate());
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String date1 = formatter.format(dateTime1);
                                DateFormat format1 = new SimpleDateFormat("HHmmss");
                                Date dateTime2 = format1.parse(result.get(i).getSupUpdateTime());
                                SimpleDateFormat formatter1 = new SimpleDateFormat("HH:mm:ss");
                                String time = formatter1.format(dateTime2);
                                dateString = date1 + " " + time;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if (i == 0){
                            data.setSupCode1(result.get(0).getSupCode());
                            data.setSupName1(result.get(0).getSupName());
                            data.setSupPrice1(result.get(0).getProPrice());
                            data.setSupUpdateDate1(dateString);
                        }else if (i == 1){
                            data.setSupCode2(result.get(1).getSupCode());
                            data.setSupName2(result.get(1).getSupName());
                            data.setSupPrice2(result.get(1).getProPrice());
                            data.setSupUpdateDate2(dateString);
                        }else if (i == 2){
                            data.setSupCode3(result.get(2).getSupCode());
                            data.setSupName3(result.get(2).getSupName());
                            data.setSupPrice3(result.get(2).getProPrice());
                            data.setSupUpdateDate3(dateString);
                        }
                    }
                }
            }
        }
        BigDecimal proCount = new BigDecimal(outDataList.size());//商品品类数
        BigDecimal proTotalCount = BigDecimal.ZERO;//补货总数
        BigDecimal proTotalAmt = BigDecimal.ZERO;
        for (GetReplenishDetailOutData item : outDataList){
            if (ObjectUtil.isEmpty(item.getGsrdNeedQty())){
                item.setGsrdNeedQty("0");
            }
            proTotalCount = proTotalCount.add(new BigDecimal(item.getGsrdNeedQty()));
            if (ObjectUtil.isEmpty(item.getGsrdProPrice())){
                item.setGsrdProPrice("0");
            }
            proTotalAmt = proTotalAmt.add(new BigDecimal(item.getGsrdNeedQty()).multiply(new BigDecimal(item.getGsrdProPrice())));
        }
        map.put("outDataList",outDataList);
        map.put("proCount",proCount);
        map.put("proTotalCount",proTotalCount);
        map.put("proTotalAmt",proTotalAmt);
        return map;
    }

    @Override
    public Map<String, Object> hasReplenished(GetReplenishInData inData) {
        String gsahDate = CommonUtil.getyyyyMMdd();
        int count = replenishHMapper.selectReplenishCount(inData.getClientId(),inData.getStoCode(),gsahDate);
        Map<String,Object> result = new HashMap<>();
        String msg1 = "检查到今天已有";
        String msg2 = "";
        String msg3 = "";
        String msg4 = "/";
        String msg5 = "，是否继续补货？";
        if (count > 0){
            result.put("status","11");
            msg2 = "正常补货单";
        }else{
            result.put("status","0");
            result.put("msg","正常补货");
        }
        String keyName = inData.getClientId() + "-" + inData.getStoCode() + "-" + CommonUtil.getyyyyMMdd() + "-" + inData.getGsrhPattern();
        Boolean k = redisManager.hasKey(keyName);
        if (k){
            result.put("hasCache","1");
            msg3 = "未完成补货单";
        }else {
            result.put("hasCache","0");
        }
        if (ObjectUtil.isNotEmpty(msg2)){
            if (ObjectUtil.isNotEmpty(msg3)){
                String msg = msg1 + msg2 + msg4 + msg3 + msg5;
                result.put("msg",msg);
            }else{
                String msg = msg1 + msg2 + msg5;
                result.put("msg",msg);
            }
        }else {
            if (ObjectUtil.isNotEmpty(msg3)) {
                String msg = msg1 + msg3 + msg5;
                result.put("msg", msg);
            }
        }
        return result;
    }

    @Transactional
    public void update(GetReplenishInData inData) {
        GetLoginOutData loginOutData = new GetLoginOutData();
        loginOutData.setDepId(inData.getStoCode());
        loginOutData.setClient(inData.getClientId());
        StoreOutData storeData = storeDataMapper.queryStoLeader(loginOutData);
        inData.setGsrhType(storeData.getStoDeliveryMode());
        Example example = new Example(GaiaSdReplenishH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId())
                .andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId())
                .andEqualTo("gsrhBrId", inData.getStoCode());
        GaiaSdReplenishH replenishH = replenishHMapper.selectOneByExample(example);
        inData.setGsrhDate(replenishH.getGsrhDate());
        replenishH.setGsrhFlag("1");
        this.replenishHMapper.update(replenishH);
    }

    @Override
    @Transactional
    public Map<String,Object> insert(GetReplenishInData inData,GetLoginOutData loginOutData) {
        //蓬晖配送业务判断与其他流程无关
        if (penghuiClient.equals(inData.getClientId())){
            for (GetReplenishDetailInData detail : inData.getDetailList()) {
                detail.setIsSpecial("1");
            }
        }
        ReplenishPart replenishPart = replenishHMapper.selectReplenishPart(loginOutData.getClient(),loginOutData.getDepId());//分单设置查询
        String isIntegerQty = storeDataMapper.selectStoPriceComparison(loginOutData.getClient(),loginOutData.getDepId(),"BH_QTY_INTEGER");
        if(org.apache.commons.lang3.StringUtils.isEmpty(isIntegerQty)){
            isIntegerQty = "0";
        }
        log.info("新增补货单接口调用开始");
        List<GetReplenishDetailInData> detailList = inData.getDetailList();
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(detailList)) {
            for (int i = 0; i < detailList.size(); i++) {
                if (org.apache.commons.lang3.StringUtils.isNotBlank(detailList.get(i).getGsrdNeedQty())) {
                    if (!detailList.get(i).getGsrdNeedQty().matches("^[0-9]+(.[0-9]+)?$")) {
                        throw new BusinessException("补货数量必须是数字类型,错误行号:第{"+(i+1)+"}行");
                    }
                    if("1".equals(isIntegerQty)){
//                        Pattern p2 = Pattern.compile("^[0-9]*[1-9][0-9]*$");//判断正整数
                        Pattern p2 = Pattern.compile("^\\d+$");//0和正整数
                        Matcher matcher = p2.matcher(detailList.get(i).getGsrdNeedQty());
                        System.out.println(matcher.matches());
                        if (!matcher.matches()){
                            throw new BusinessException("第"+detailList.get(i).getGsrdSerial()+"行补货数量不能为小数！");
                        }
                    }
                }
            }
        }
        String keyName = inData.getClientId() + "-" + inData.getStoCode() + "-" + CommonUtil.getyyyyMMdd() + "-" + inData.getGsrhPattern();
        Boolean re = redisManager.hasKey(keyName);
        if (re){
            redisManager.delete(keyName);
        }
        String gsrhVoucherId = "";
        StoreOutData storeData = storeDataMapper.queryStoLeader(loginOutData);
        List<GetReplenishDetailInData> specialDetails = new ArrayList<>();
        List<GetReplenishDetailInData> commonDetails = new ArrayList<>();
        if (ObjectUtil.isEmpty(inData.getGsrhDate())){
            inData.setGsrhDate(CommonUtil.getyyyyMMdd());
        }else{
            inData.setGsrhDate(CommonUtil.parseWebDate(inData.getGsrhDate()));
        }
        if(org.apache.commons.lang3.StringUtils.isEmpty(inData.getGsrhEmp())){
            inData.setGsrhEmp(loginOutData.getUserId());
        }
        Map<String, String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(), inData.getStoCode());
        if (ObjectUtil.isNotEmpty(inData.getGsrhVoucherId())) {
            log.info("修改补货单"+ inData.getGsrhVoucherId() +"开始");
            String a = inData.getGsrhVoucherId().substring(0,2);
            if("PD".equals(a)) {
                Example example = new Example(GaiaSdReplenishH.class);
                example.createCriteria().andEqualTo("clientId", inData.getClientId())
                        .andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId())
                        .andEqualTo("gsrhBrId", inData.getStoCode());
                GaiaSdReplenishH replenishH = replenishHMapper.selectOneByExample(example);
                //已审核的补货单无法修改
                if ("1".equals(replenishH.getGsrhStatus())){
                    throw new BusinessException("该补货单已审核不可修改，请核对！");
                }
                //单据类型不同校验
                if (replenishH.getGsrhPattern().equals(inData.getGsrhPattern())) {
                    saveDetails(inData, replenishH);
                    this.replenishHMapper.update(replenishH);
                    gsrhVoucherId = inData.getGsrhVoucherId();
                    //只有正常补货的时候才需要 保存原始推荐数据以及 对比数据
                    //编辑-补货单时
                    updateOriAndCompareData(inData, replenishH);
                }else {
                    throw new BusinessException("修改单据与原单据补货类型不符，请核对后操作！");
                }
                log.info("修改补货单"+ inData.getGsrhVoucherId() +"结束");
            }else {
                throw new BusinessException("不支持该类型补货单修改！");
            }
        } else {
            for (GetReplenishDetailInData detail : inData.getDetailList()) {//区分特殊品与普通品
                String needQty = org.apache.commons.lang3.StringUtils.deleteWhitespace(detail.getGsrdNeedQty());
                if(org.apache.commons.lang3.StringUtils.isNotEmpty(needQty)) {
                    detail.setGsrdNeedQty(needQty);
                }else {
                    throw new BusinessException("商品编码为：" + detail.getGsrdProId() + "的商品未填写补货数量");
                }
                detail.setGsrhPart("0");//设置分单类型
                if (ObjectUtil.isNotEmpty(replenishPart)) {
                    if ("1".equals(replenishPart.getFlag1()) && "3".equals(detail.getProStorageCondition())) {
                        detail.setGsrhPart("1");
                    }
                    if ("1".equals(replenishPart.getFlag2()) && "5".equals(detail.getProControlClass())) {
                        detail.setGsrhPart("2");
                    }
                    if ("1".equals(replenishPart.getFlag3()) && "4".equals(detail.getProControlClass())) {
                        detail.setGsrhPart("3");
                    }
                    if ("1".equals(replenishPart.getFlag4()) && "11".equals(detail.getProControlClass())) {
                        detail.setGsrhPart("4");
                    }
                    if ("1".equals(replenishPart.getFlag5()) && "3".equals(detail.getProStorageArea())) {
                        detail.setGsrhPart("5");
                    }
                }
                if (detail.getIsSpecial().equals("0")
                        || ObjectUtil.isEmpty(detail.getIsSpecial())){
                    commonDetails.add(detail);
                }else if (detail.getIsSpecial().equals("1")){
                    specialDetails.add(detail);
                }
            }
            List<JSONObject> commonJsonList = new ArrayList<>();
            List<JSONObject> specialJsonList = new ArrayList<>();
            //获取redis 中的推荐数据
            getDateFromRedis(inData,commonJsonList,specialJsonList);

            if (commonDetails.size() > 0 && commonDetails != null) {
                List<GetReplenishDetailInData> partList = commonDetails.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGsrhPart()))), ArrayList::new)
                );
                for (GetReplenishDetailInData b : partList) {
                    List<GetReplenishDetailInData> itemList = new ArrayList<>();
                    for (GetReplenishDetailInData c : commonDetails) {
                        if (b.getGsrhPart().equals(c.getGsrhPart())) {
                            itemList.add(c);
                        }
                    }
                    if (ObjectUtil.isNotEmpty(itemList) && itemList.size() > 0){
                        inData.setDetailList(itemList);
                        if (ObjectUtil.isNull(storeData.getStoDeliveryMode())) {
                            inData.setGsrhType("1");
                        } else {
                            inData.setGsrhType(storeData.getStoDeliveryMode());
                        }
                        GaiaSdReplenishH loginData = new GaiaSdReplenishH();
    //            loginData.setGsrhBrId(inData.getStoCode());
                        loginData.setClientId(inData.getClientId());
    //        String voucherId =  this.queryNextVoucherId(loginData);
                        if (ObjectUtil.isEmpty(inData.getGsrhDate())) {
                            inData.setGsrhDate(CommonUtil.getyyyyMMdd());
                        }
                        String voucherId = commonService.selectNextVoucherId(loginData);//补货单号调整
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        log.info("新增普通补货单：" + voucherId + "开始");
                        inData.setGsrhVoucherId(voucherId);
                        GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
                        replenishH.setClientId(inData.getClientId());
                        replenishH.setGsrhVoucherId(inData.getGsrhVoucherId());
                        replenishH.setGsrhBrId(inData.getStoCode());
                        replenishH.setGsrhStatus("0");
    //                Map<String, String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(), inData.getStoCode());
                        if (stoInfo.get("stoAttribute").equals("2")) {
                            replenishH.setGsrhAddr(stoInfo.get("stoDcCode"));
                        }
                        replenishH.setGsrhType(inData.getGsrhType());
                        replenishH.setGsrhDate(inData.getGsrhDate());
                        replenishH.setGsrhCreateTime(CommonUtil.getHHmmss());
                        replenishH.setGsrhFlag("0");
                        replenishH.setGsrhPart(b.getGsrhPart());
                        replenishH.setGsrhEmp(inData.getGsrhEmp());
                        if (ObjectUtil.isNull(inData.getGsrhPattern())) {
                            replenishH.setGsrhPattern("0");
                            inData.setGsrhPattern("0");
                        } else {
                            replenishH.setGsrhPattern(inData.getGsrhPattern());
                        }
                        saveDetails(inData, replenishH);
                        this.replenishHMapper.insert(replenishH);
                        if (ObjectUtil.isNotEmpty(inData.getIsPublish())) {
                            if (inData.getIsPublish().equals("1")) {
                                inData.setDcCode(inData.getStoCode());
                                this.insertPo(inData, loginOutData);
                            }
                        }
                        gsrhVoucherId = voucherId;
                        //根据商品编码去重
                        List<GetReplenishDetailInData> commonDetails1 = commonDetails.stream().filter(clcb -> !clcb.getGsrdProId().isEmpty()).distinct().collect(Collectors.toList());
                        //只有正常补货的时候才需要 保存原始推荐数据以及 对比数据

                        //保存 正常商品 原始推荐数据以及 对比数据
                        insertCommonOriAndCompareDate(inData, commonDetails1, commonJsonList, replenishH);
                        log.info("新增普通补货单：" + gsrhVoucherId + "结束");
                    }
                }
            }
            if (specialDetails.size() > 0 && specialDetails != null) {
                List<SpecialClass> specialClassList = replenishDMapper.selectSpecialClass(inData.getClientId(),inData.getStoCode(),"");
                List<SpecialClass> results = specialClassList.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getClientId() + f.getStockCode() + f.getStockType()))), ArrayList::new)
                );
                for (SpecialClass specialClass : results) {
                    GetReplenishInData specialInData = new GetReplenishInData();
                    specialInData.setClientId(inData.getClientId());
                    specialInData.setStoCode(inData.getStoCode());
                    specialInData.setGsrhDate(inData.getGsrhDate());
                    specialInData.setGsrhPattern(inData.getGsrhPattern());
                    specialInData.setDcCode(specialClass.getStockCode());
                    if ("1".equals(specialClass.getStockType())){
                        specialInData.setGsrhType("3");
                    }else {
                        if("4".equals(stoInfo.get("deliveryMode"))) {
                            specialInData.setGsrhType("4");
                        }else {
                            specialInData.setGsrhType("2");
                        }
                    }
                    List<GetReplenishDetailInData> items = new ArrayList<>();
                    for (SpecialClass a : specialClassList) {
                        if (a.getStockType().equals(specialClass.getStockType()) && a.getStockCode().equals(specialClass.getStockCode())) {
                            for (GetReplenishDetailInData specialDetail : specialDetails) {
                                if (specialDetail.getBigClass().split("-")[0].equals(a.getBigClass())) {
                                    items.add(specialDetail);
                                }
                            }
                        }
                    }
                    if (items.size() > 0 && items != null) {
                        List<GetReplenishDetailInData> partList = items.stream().collect(
                                Collectors.collectingAndThen(
                                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGsrhPart()))), ArrayList::new)
                        );
                        for (GetReplenishDetailInData b : partList){
                            List<GetReplenishDetailInData> itemList = new ArrayList<>();
                            for (GetReplenishDetailInData c : items){
                                if (b.getGsrhPart().equals(c.getGsrhPart())){
                                    itemList.add(c);
                                }
                            }
                            if (ObjectUtil.isNotEmpty(itemList) && itemList.size() > 0) {
                                specialInData.setDetailList(itemList);

//                    inData.setGsrhDate(CommonUtil.parseWebDate(inData.getGsrhDate()));
                                GaiaSdReplenishH loginData = new GaiaSdReplenishH();
                                //            loginData.setGsrhBrId(inData.getStoCode());
                                loginData.setClientId(inData.getClientId());
                                //        String voucherId =  this.queryNextVoucherId(loginData);
                                if (ObjectUtil.isEmpty(inData.getGsrhDate())) {
                                    specialInData.setGsrhDate(CommonUtil.getyyyyMMdd());
                                }
                                String voucherId = commonService.selectNextVoucherId(loginData);
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                specialInData.setGsrhVoucherId(voucherId);
                                log.info("新增特殊补货单：" + voucherId + "开始");
                                GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
                                replenishH.setClientId(specialInData.getClientId());
                                replenishH.setGsrhVoucherId(specialInData.getGsrhVoucherId());
                                replenishH.setGsrhBrId(specialInData.getStoCode());
                                replenishH.setGsrhPart(b.getGsrhPart());
                                replenishH.setGsrhStatus("0");
                                replenishH.setGsrhSpecial("1");
//                        Map<String, String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(), inData.getStoCode());
//                        if (stoInfo.get("stoAttribute").equals("2")) {
//                            replenishH.setGsrhAddr(stoInfo.get("stoDcCode"));
//                        }
                                replenishH.setGsrhEmp(inData.getGsrhEmp());
                                replenishH.setGsrhAddr(specialInData.getDcCode());
                                replenishH.setGsrhType(specialInData.getGsrhType());
                                replenishH.setGsrhDate(specialInData.getGsrhDate());
                                replenishH.setGsrhCreateTime(CommonUtil.getHHmmss());
                                replenishH.setGsrhPattern(specialInData.getGsrhPattern());
                                replenishH.setGsrhFlag("0");
                                if (ObjectUtil.isNull(specialInData.getGsrhPattern())) {
                                    replenishH.setGsrhPattern("0");
                                } else {
                                    replenishH.setGsrhPattern(specialInData.getGsrhPattern());
                                }
                                saveDetails(specialInData, replenishH);
                                this.replenishHMapper.insert(replenishH);
                                //根据商品编码去重
                                List<GetReplenishDetailInData> specialDetails1 = specialDetails.stream().filter(clcb -> !clcb.getGsrdProId().isEmpty()).distinct().collect(Collectors.toList());

                                //只有正常补货的时候才需要 保存原始推荐数据以及 对比数据
                                //保存特殊商品 原始推荐数据以及 对比数据
                                insertSpecialOriAndCompareData(inData, specialDetails1, specialJsonList, items, replenishH);
                                log.info("新增特殊补货单：" + voucherId + "开始");
                            }
                        }
                    }
                }
            }
        }
        Map<String,Object> map = new HashMap(8);
        String msg = checkCreditQuotaAndZqts(inData,null,false);
        map.put("gsrhVoucherId",gsrhVoucherId);
        map.put("msg",msg);
        return map;
    }

    private void updateOriAndCompareData(GetReplenishInData inData, GaiaSdReplenishH replenishH) {
        // 1.先根据单号查询推荐单
        Example oriDExample = new Example(GaiaSdReplenishOriD.class);
        oriDExample.createCriteria().andEqualTo("client", replenishH.getClientId()).andEqualTo("gsrdBrId", replenishH.getGsrhBrId()).andEqualTo("gsrdVoucherId", replenishH.getGsrhVoucherId());
        List<GaiaSdReplenishOriD> oriDList = gaiaSdReplenishOriDMapper.selectByExample(oriDExample);
        // 2.删除该单号所有零时表
        Example compareDExample = new Example(GaiaSdReplenishComparedD.class);
        compareDExample.createCriteria().andEqualTo("client", replenishH.getClientId()).andEqualTo("gsrdBrId", replenishH.getGsrhBrId()).andEqualTo("gsrdVoucherId", replenishH.getGsrhVoucherId());
        gaiaSdReplenishComparedDMapper.deleteByExample(compareDExample);
        List<GetReplenishDetailInData> detailList = inData.getDetailList();
        if (!CollectionUtils.isNullOrEmpty(detailList)) {
            //进行比较 并插入 补货单比较结果明细表
            //插入 commonDetail 中所有的数据
            List<GaiaSdReplenishComparedD> updateDetailInsertList = new ArrayList<>();
            for (GetReplenishDetailInData data : detailList) {
                GaiaSdReplenishComparedD comparedD = new GaiaSdReplenishComparedD();
                comparedD.setClient(inData.getClientId());
                comparedD.setGsrdBrId(inData.getStoCode());
                comparedD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
                comparedD.setGsrdDate(replenishH.getGsrhDate());
                comparedD.setGsrdProId(data.getGsrdProId());
                comparedD.setGsrdNeedQty(new BigDecimal(data.getGsrdNeedQty()));
                comparedD.setGsrdCrDate(CommonUtil.getyyyyMMdd());
                comparedD.setGsrdCrId(inData.getUserId());
                comparedD.setGsrdCrTime(CommonUtil.getHHmmss());
                comparedD.setGsrdProposeQty(BigDecimal.ZERO);
                comparedD.setGsrdStatus("A");
                if (!CollectionUtils.isNullOrEmpty(oriDList)) {
                    for (GaiaSdReplenishOriD oriD : oriDList) {
                        if (data.getGsrdProId().equals(oriD.getGsrdProId())) {
                            BigDecimal gsrdNeedQty = StringUtils.isNullOrEmpty(data.getGsrdNeedQty()) ? BigDecimal.ZERO : new BigDecimal(data.getGsrdNeedQty());
                            BigDecimal gsrdProposeQty = ObjectUtil.isEmpty(oriD.getGsrdProposeQty()) ? BigDecimal.ZERO : oriD.getGsrdProposeQty();
                            int compare = gsrdNeedQty.compareTo(gsrdProposeQty);
                            if (compare != 0) {
                                comparedD.setGsrdStatus("M");
                            } else {
                                comparedD.setGsrdStatus(null);
                            }
                            comparedD.setGsrdProposeQty(gsrdProposeQty);
                            break;
                        }
                    }
                }
                updateDetailInsertList.add(comparedD);
            }
            //插入commonJsonList 有但是 commonDetail 没有的
            if (!CollectionUtils.isNullOrEmpty(oriDList)) {
                for (GaiaSdReplenishOriD oriD : oriDList) {
                    boolean flag = true;
                    for (GetReplenishDetailInData detail : detailList) {
                        if (detail.getGsrdProId().equals(oriD.getGsrdProId())) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        GaiaSdReplenishComparedD comparedD = new GaiaSdReplenishComparedD();
                        comparedD.setClient(inData.getClientId());
                        comparedD.setGsrdBrId(inData.getStoCode());
                        comparedD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
                        comparedD.setGsrdDate(replenishH.getGsrhDate());
                        comparedD.setGsrdProId(oriD.getGsrdProId());
//                            comparedD.setGsrdPoPrc(oriD.getGsrdPoPrc());
//                            comparedD.setGsrdAccPrc(oriD.getGsrdAccPrc());
                        comparedD.setGsrdNeedQty(BigDecimal.ZERO);
                        comparedD.setGsrdCrDate(CommonUtil.getyyyyMMdd());
                        comparedD.setGsrdCrId(inData.getUserId());
                        comparedD.setGsrdCrTime(CommonUtil.getHHmmss());
                        comparedD.setGsrdProposeQty(ObjectUtil.isEmpty(oriD.getGsrdProposeQty()) ? BigDecimal.ZERO : oriD.getGsrdProposeQty());
                        comparedD.setGsrdStatus("D");
                        updateDetailInsertList.add(comparedD);
                    }
                }
            }
            List<GaiaSdReplenishComparedD> collect = updateDetailInsertList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdReplenishComparedD :: getGsrdProId))), ArrayList::new));
            gaiaSdReplenishComparedDMapper.insertLists(collect);
        }
    }

    private void insertSpecialOriAndCompareData(GetReplenishInData inData, List<GetReplenishDetailInData> commonDetails, List<JSONObject> specialJsonList, List<GetReplenishDetailInData> items, GaiaSdReplenishH replenishH) {
        if (!CollectionUtils.isNullOrEmpty(specialJsonList)) {
            List<JSONObject> specialJsonList1 = specialJsonList.stream().filter(clcb -> !clcb.get("gsrdProId").toString().isEmpty()).distinct().collect(Collectors.toList());
            List<GaiaSdReplenishOriD> spOriDList = new ArrayList<>();
            //插入原始补货记录
            for (GetReplenishDetailInData item : items) {
                for (JSONObject jsonObject : specialJsonList1) {
                    if (item.getDcCode().equals(jsonObject.get("dcCode").toString()) && item.getGsrdProId().equals(jsonObject.get("gsrdProId").toString())) {
                        GaiaSdReplenishOriD oriD = new GaiaSdReplenishOriD();
                        oriD.setClient(inData.getClientId());
                        oriD.setGsrdBrId(inData.getStoCode());
                        oriD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
                        oriD.setGsrdDate(replenishH.getGsrhDate());
                        oriD.setGsrdProId(jsonObject.get("gsrdProId").toString());
                        oriD.setGsrdProposeQty(new BigDecimal(jsonObject.get("gsrdProposeQty").toString()));
//                                        oriD.setGsrdAccPrc(new BigDecimal(jsonObject.get("poPrice").toString()));
//                                        oriD.setGsrdPoPrc(new BigDecimal(jsonObject.get("gsrdProPrice").toString()));
                        oriD.setGsrdCrId(inData.getUserId());
                        oriD.setGsrdCrTime(CommonUtil.getHHmmss());
                        oriD.setGsrdCrDate(CommonUtil.getyyyyMMdd());
                        oriD.setGsrdWmKcsl(new BigDecimal(jsonObject.get("gsrdStockStore").toString()));
                        spOriDList.add(oriD);
                    }
                }
            }
            if (!CollectionUtils.isNullOrEmpty(spOriDList)) {
                List<GaiaSdReplenishOriD> collect = spOriDList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdReplenishOriD :: getGsrdProId))), ArrayList::new));
                gaiaSdReplenishOriDMapper.insertLists(collect);
            }
            List<GaiaSdReplenishComparedD> spCompareaDList = new ArrayList<>();
            for (GetReplenishDetailInData item : items) {
                GaiaSdReplenishComparedD comparedD = new GaiaSdReplenishComparedD();
                comparedD.setClient(inData.getClientId());
                comparedD.setGsrdBrId(inData.getStoCode());
                comparedD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
                comparedD.setGsrdDate(replenishH.getGsrhDate());
                comparedD.setGsrdProId(item.getGsrdProId());
//                                comparedD.setGsrdPoPrc(item.getGsrdProPrice());
//                                comparedD.setGsrdAccPrc(StringUtils.isNullOrEmpty(item.getPoPrice()) ? BigDecimal.ZERO : new BigDecimal(item.getPoPrice()));
                comparedD.setGsrdNeedQty(new BigDecimal(item.getGsrdNeedQty()));
                comparedD.setGsrdCrDate(CommonUtil.getyyyyMMdd());
                comparedD.setGsrdCrId(inData.getUserId());
                comparedD.setGsrdCrTime(CommonUtil.getHHmmss());
                comparedD.setGsrdProposeQty(BigDecimal.ZERO);
                comparedD.setGsrdStatus("A");
                if (!CollectionUtils.isNullOrEmpty(spOriDList)) {
                    for (GaiaSdReplenishOriD oriD : spOriDList) {
                        if (item.getGsrdProId().equals(oriD.getGsrdProId())) {
                            BigDecimal gsrdNeedQty = StringUtils.isNullOrEmpty(item.getGsrdNeedQty()) ? BigDecimal.ZERO : new BigDecimal(item.getGsrdNeedQty());
                            BigDecimal gsrdProposeQty = ObjectUtil.isEmpty(oriD.getGsrdProposeQty()) ? BigDecimal.ZERO : oriD.getGsrdProposeQty();
                            int compare = gsrdNeedQty.compareTo(gsrdProposeQty);
                            if (compare != 0) {
                                comparedD.setGsrdStatus("M");
                            } else {
                                comparedD.setGsrdStatus(null);
                            }
                            comparedD.setGsrdProposeQty(gsrdProposeQty);
                            break;
                        }
                    }
                }
                spCompareaDList.add(comparedD);
            }
            //插入spOriDList 有但是 specialDetail 没有的
            for (GaiaSdReplenishOriD spOri : spOriDList) {
                boolean flag = true;
                for (GetReplenishDetailInData commonDetail : commonDetails) {
                    if (commonDetail.getGsrdProId().equals(spOri.getGsrdProId())) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    GaiaSdReplenishComparedD comparedD = new GaiaSdReplenishComparedD();
                    comparedD.setClient(inData.getClientId());
                    comparedD.setGsrdBrId(inData.getStoCode());
                    comparedD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
                    comparedD.setGsrdDate(replenishH.getGsrhDate());
                    comparedD.setGsrdProId(spOri.getGsrdProId());
//                                    comparedD.setGsrdPoPrc(spOri.getGsrdPoPrc());
//                                    comparedD.setGsrdAccPrc(spOri.getGsrdAccPrc());
                    comparedD.setGsrdNeedQty(BigDecimal.ZERO);
                    comparedD.setGsrdCrDate(CommonUtil.getyyyyMMdd());
                    comparedD.setGsrdCrId(inData.getUserId());
                    comparedD.setGsrdCrTime(CommonUtil.getHHmmss());
                    comparedD.setGsrdProposeQty(spOri.getGsrdProposeQty());
                    comparedD.setGsrdStatus("D");
                    spCompareaDList.add(comparedD);
                }
            }
            List<GaiaSdReplenishComparedD> collect = spCompareaDList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdReplenishComparedD :: getGsrdProId))), ArrayList::new));
            // 2.对比补货单据和推荐单并保存临时表
            gaiaSdReplenishComparedDMapper.insertLists(collect);
        }
    }

    private void insertCommonOriAndCompareDate(GetReplenishInData inData, List<GetReplenishDetailInData> commonDetails, List<JSONObject> commonJsonList, GaiaSdReplenishH replenishH) {
        if (!CollectionUtils.isNullOrEmpty(commonJsonList)) {
            //商品编码去重
            List<JSONObject> commonJsonList1 = commonJsonList.stream().filter(clcb -> !clcb.get("gsrdProId").toString().isEmpty()).distinct().collect(Collectors.toList());
            //插入原始补货明细表
            List<GaiaSdReplenishOriD> oriDList = new ArrayList<>();
            for (JSONObject jsonObject : commonJsonList1) {
                GaiaSdReplenishOriD oriD = new GaiaSdReplenishOriD();
                oriD.setClient(inData.getClientId());
                oriD.setGsrdBrId(inData.getStoCode());
                oriD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
                oriD.setGsrdDate(replenishH.getGsrhDate());
                oriD.setGsrdProId(jsonObject.get("gsrdProId").toString());
                oriD.setGsrdProposeQty(new BigDecimal(jsonObject.get("gsrdProposeQty").toString()));
//                        oriD.setGsrdAccPrc(ObjectUtil.isNotEmpty(jsonObject.get("poPrice").toString()) ? new BigDecimal(jsonObject.get("poPrice").toString()) : BigDecimal.ZERO);
//                        oriD.setGsrdPoPrc(ObjectUtil.isNotEmpty(jsonObject.get("gsrdProPrice").toString()) ? new BigDecimal(jsonObject.get("gsrdProPrice").toString()) : BigDecimal.ZERO);
                oriD.setGsrdCrId(inData.getUserId());
                oriD.setGsrdCrTime(CommonUtil.getHHmmss());
                oriD.setGsrdCrDate(CommonUtil.getyyyyMMdd());
                oriD.setGsrdWmKcsl(new BigDecimal(jsonObject.get("gsrdStockStore").toString()));
                oriDList.add(oriD);
            }
            if (!CollectionUtils.isNullOrEmpty(oriDList)) {
                List<GaiaSdReplenishOriD> collect = oriDList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdReplenishOriD :: getGsrdProId))), ArrayList::new));
                gaiaSdReplenishOriDMapper.insertLists(collect);
            }
            //进行比较 并插入 补货单比较结果明细表
            //插入 commonDetail 中所有的数据
            List<GaiaSdReplenishComparedD> commonDetailInsertList = new ArrayList<>();
            for (GetReplenishDetailInData commonDetail : commonDetails) {
                GaiaSdReplenishComparedD comparedD = new GaiaSdReplenishComparedD();
                comparedD.setClient(inData.getClientId());
                comparedD.setGsrdBrId(inData.getStoCode());
                comparedD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
                comparedD.setGsrdDate(replenishH.getGsrhDate());
                comparedD.setGsrdProId(commonDetail.getGsrdProId());
//                        comparedD.setGsrdPoPrc(commonDetail.getGsrdProPrice());
//                        comparedD.setGsrdAccPrc(StringUtils.isNullOrEmpty(commonDetail.getPoPrice()) ? BigDecimal.ZERO : new BigDecimal(commonDetail.getPoPrice()));
                comparedD.setGsrdNeedQty(new BigDecimal(commonDetail.getGsrdNeedQty()));
                comparedD.setGsrdCrDate(CommonUtil.getyyyyMMdd());
                comparedD.setGsrdCrId(inData.getUserId());
                comparedD.setGsrdCrTime(CommonUtil.getHHmmss());
                comparedD.setGsrdProposeQty(BigDecimal.ZERO);
                comparedD.setGsrdStatus("A");
                for (JSONObject jsonObject : commonJsonList1) {
                    if (commonDetail.getGsrdProId().equals(jsonObject.get("gsrdProId").toString())) {
                        BigDecimal gsrdNeedQty = StringUtils.isNullOrEmpty(commonDetail.getGsrdNeedQty()) ? BigDecimal.ZERO : new BigDecimal(commonDetail.getGsrdNeedQty());
                        BigDecimal gsrdProposeQty = ObjectUtil.isEmpty(jsonObject.get("gsrdProposeQty")) ? BigDecimal.ZERO : new BigDecimal(jsonObject.get("gsrdProposeQty").toString());
                        int compare = gsrdNeedQty.compareTo(gsrdProposeQty);
                        if (compare != 0) {
                            comparedD.setGsrdStatus("M");
                        } else {
                            comparedD.setGsrdStatus(null);
                        }
                        comparedD.setGsrdProposeQty(gsrdProposeQty);
                        break;
                    }
                }
                commonDetailInsertList.add(comparedD);
            }
            //插入commonJsonList 有但是 commonDetail 没有的
            for (JSONObject jsonObject : commonJsonList1) {
                boolean flag = true;
                for (GetReplenishDetailInData commonDetail : commonDetails) {
                    if (commonDetail.getGsrdProId().equals(jsonObject.get("gsrdProId").toString())) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    GaiaSdReplenishComparedD comparedD = new GaiaSdReplenishComparedD();
                    comparedD.setClient(inData.getClientId());
                    comparedD.setGsrdBrId(inData.getStoCode());
                    comparedD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
                    comparedD.setGsrdDate(replenishH.getGsrhDate());
                    comparedD.setGsrdProId(jsonObject.get("gsrdProId").toString());
//                            comparedD.setGsrdPoPrc(new BigDecimal(jsonObject.get("gsrdProPrice").toString()));
//                            comparedD.setGsrdAccPrc(new BigDecimal(jsonObject.get("poPrice").toString()));
                    comparedD.setGsrdNeedQty(BigDecimal.ZERO);
                    comparedD.setGsrdCrDate(CommonUtil.getyyyyMMdd());
                    comparedD.setGsrdCrId(inData.getUserId());
                    comparedD.setGsrdCrTime(CommonUtil.getHHmmss());
                    comparedD.setGsrdProposeQty(ObjectUtil.isEmpty(jsonObject.get("gsrdProposeQty")) ? BigDecimal.ZERO : new BigDecimal(jsonObject.get("gsrdProposeQty").toString()));
                    comparedD.setGsrdStatus("D");
                    commonDetailInsertList.add(comparedD);
                }
            }
            List<GaiaSdReplenishComparedD> collect = commonDetailInsertList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdReplenishComparedD :: getGsrdProId))), ArrayList::new));
            gaiaSdReplenishComparedDMapper.insertLists(collect);
        } else {
            //不用比较直接 全部是新增
            List<GaiaSdReplenishComparedD> comparedDList = new ArrayList<>();
            for (GetReplenishDetailInData commonDetail : commonDetails) {
                GaiaSdReplenishComparedD comparedD = new GaiaSdReplenishComparedD();
                comparedD.setClient(inData.getClientId());
                comparedD.setGsrdBrId(inData.getStoCode());
                comparedD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
                comparedD.setGsrdDate(replenishH.getGsrhDate());
                comparedD.setGsrdProId(commonDetail.getGsrdProId());
//                        comparedD.setGsrdPoPrc(commonDetail.getGsrdProPrice());
//                        comparedD.setGsrdAccPrc(StringUtils.isNullOrEmpty(commonDetail.getPoPrice()) ? BigDecimal.ZERO : new BigDecimal(commonDetail.getPoPrice()));
                comparedD.setGsrdNeedQty(new BigDecimal(commonDetail.getGsrdNeedQty()));
                comparedD.setGsrdProposeQty(BigDecimal.ZERO);
                comparedD.setGsrdStatus("A");
                comparedD.setGsrdCrDate(CommonUtil.getyyyyMMdd());
                comparedD.setGsrdCrId(inData.getUserId());
                comparedD.setGsrdCrTime(CommonUtil.getHHmmss());
                comparedDList.add(comparedD);
            }
            List<GaiaSdReplenishComparedD> collect = comparedDList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdReplenishComparedD :: getGsrdProId))), ArrayList::new));
            gaiaSdReplenishComparedDMapper.insertLists(collect);
        }
    }

    private void getDateFromRedis(GetReplenishInData inData, List<JSONObject> commonJsonList, List<JSONObject> specialJsonList) {
        // 1.先获取reids推荐数据
        String key = "REP-" + inData.getClientId() + "-" + inData.getStoCode() + "-" + inData.getUserId();
        Boolean res = redisManager.hasKey(key);
        if (res) {
            String detailList = redisManager.get(key).toString();
            JSONArray jsonArray = JSONObject.parseArray(detailList);
            if (!CollectionUtils.isNullOrEmpty(jsonArray)) {
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = JSONObject.parseObject(jsonArray.get(i).toString());
                    if ("0".equals(jsonObject.get("isSpecial").toString())) {
                        commonJsonList.add(jsonObject);
                    } else if ("1".equals(jsonObject.get("isSpecial"))) {
                        specialJsonList.add(jsonObject);
                    }
                }
            }
        }
    }

    public void saveDetails(GetReplenishInData inData, GaiaSdReplenishH replenishH) {
        if (ObjectUtil.isNotEmpty(replenishH)) {
            replenishH.setGsrhStatus("0");
            replenishH.setGsrhPattern(inData.getGsrhPattern());
            replenishH.setGsrhType(inData.getGsrhType());
            replenishH.setGsrhDate(inData.getGsrhDate());
            BigDecimal amount = BigDecimal.ZERO;
            BigDecimal count = BigDecimal.ZERO;
            GetReplenishOutData outData = new GetReplenishOutData();
            outData.setGsrhType(replenishH.getGsrhType());
            outData.setGsrhDate(replenishH.getGsrhDate());
            outData.setGsrhStatus(replenishH.getGsrhStatus());
            outData.setGsrhTotalAmt(replenishH.getGsrhTotalAmt());
            outData.setGsrhTotalQty(replenishH.getGsrhTotalQty());
            outData.setGsrhEmp(replenishH.getGsrhEmp());
            List<GetReplenishDetailOutData> detailList = this.replenishDMapper.queryDetailList(inData);
            if (ObjectUtil.isNotEmpty(detailList) && detailList.size() > 0) {
                Example exampleStore = new Example(GaiaSdReplenishD.class);
                exampleStore.createCriteria().andEqualTo("clientId", inData.getClientId())
                        .andEqualTo("gsrdVoucherId", inData.getGsrhVoucherId())
                        .andEqualTo("gsrdBrId", inData.getStoCode());
                this.replenishDMapper.deleteByExample(exampleStore);
                this.replenishGiftMapper.deleteGiftDetail(inData.getClientId(),inData.getStoCode(),inData.getGsrhVoucherId());//删除赠品
            }
            int n = 1;
            //根据补货商品获取赠品并把相同商品编码的赠品累加补货数量
            List<GetReplenishDetailOutData> giftList = new ArrayList<>();
            for (GetReplenishDetailInData detail : inData.getDetailList()) {
                detail.setIsGift("0");
                //获取赠品相关列表
                if ("1".equals(detail.getHasGift())) {
                    List<GetReplenishDetailOutData> itemList = this.giftList(detail, replenishH);
                    if (ObjectUtil.isNotEmpty(giftList) && giftList.size() > 0){
                        for (int i = 0;i < itemList.size();i++){
                            GetReplenishDetailOutData item = itemList.get(i);
                            boolean f = true;
                            for (int j = 0;j < giftList.size();j++){
                                GetReplenishDetailOutData gift = giftList.get(j);
                                if(gift.getGsrdProId().equals(item.getGsrdProId())){
                                    BigDecimal qty = new BigDecimal(gift.getGsrdNeedQty()).add(new BigDecimal(item.getGsrdNeedQty()));
                                    gift.setGsrdNeedQty(qty.toString());
                                    gift.setGsrdProposeQty(qty.toString());
                                    f = false;
                                }
                            }
                            if (f){
                                giftList.add(item);
                            }
                        }
                    }else {
                        giftList.addAll(itemList);
                    }
                }
            }
            List<GetReplenishDetailInData> proList = inData.getDetailList();
            if (ObjectUtil.isNotEmpty(giftList) && giftList.size() > 0){
                for (GetReplenishDetailOutData data : giftList){
                    GetReplenishDetailInData detail = new GetReplenishDetailInData();
                    BeanUtils.copyProperties(data,detail);
                    detail.setIsGift("1");
                    proList.add(detail);
                }
            }
            for (GetReplenishDetailInData detail : proList) {
                if(ObjectUtil.isEmpty(detail.getGsrdNeedQty())){
                    throw new BusinessException(detail.getGsrdProName() + "(" + detail.getGsrdProId() + ")" + "补货数量未填写，请检查");
                }
                if (StrUtil.isEmpty(detail.getGsrdNeedQty())) {
                    count = count.add(new BigDecimal("0"));
                } else {
                    count = count.add(new BigDecimal(detail.getGsrdNeedQty()));
                }
                GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
                replenishD.setClientId(inData.getClientId());
                replenishD.setGsrdVoucherId(inData.getGsrhVoucherId());
                replenishD.setGsrdBrId(inData.getStoCode());
                //是否修改原始单标记
                replenishD.setGsrdIsEditOriginal(detail.getIsEditOriginal());
                if (org.apache.commons.lang3.StringUtils.isEmpty(detail.getIsGift())){
                    replenishD.setGsrdIsGift("0");
                }else {
                    replenishD.setGsrdIsGift(detail.getIsGift());
                }
                replenishD.setGsrdDate(inData.getGsrhDate());
                if (ObjectUtil.isNotEmpty(detail.getGsrdSerial())) {
                    replenishD.setGsrdSerial(detail.getGsrdSerial());
                }else{
                    replenishD.setGsrdSerial(String.valueOf(n));
                }
                replenishD.setGsrdProId(detail.getGsrdProId());
                replenishD.setGsrdAverageCost(detail.getGsrdAverageCost());
                replenishD.setGsrdDisplayMin(detail.getGsrdDisplayMin());
                replenishD.setGsrdGrossMargin(detail.getGsrdGrossMargin());
                replenishD.setGsrdNeedQty(detail.getGsrdNeedQty().replaceFirst("^0*", "").length() == 0 ? "0" : detail.getGsrdNeedQty().replaceFirst("^0*", ""));
                replenishD.setGsrdPackMidsize(detail.getGsrdPackMidsize());
                if (ObjectUtil.isNotEmpty(detail.getGsrdProposeQty())) {
                    replenishD.setGsrdProposeQty(detail.getGsrdProposeQty());
                }else {
                    replenishD.setGsrdProposeQty(replenishD.getGsrdNeedQty());
                }
                replenishD.setGsrdSalesDays1(ValidateUtil.isEmpty(detail.getGsrdSalesDays1()) ? null : detail.getGsrdSalesDays1());
                replenishD.setGsrdSalesDays2(ValidateUtil.isEmpty(detail.getGsrdSalesDays2()) ? null : detail.getGsrdSalesDays2());
                replenishD.setGsrdSalesDays3(ValidateUtil.isEmpty(detail.getGsrdSalesDays3()) ? null : detail.getGsrdSalesDays3());
                replenishD.setGsrdStockDepot(detail.getGsrdStockDepot());
                replenishD.setGsrdStockStore(detail.getGsrdStockStore());
                replenishD.setGsrdLastSupid(detail.getGsrdLastSupid());
                if(ObjectUtil.isNotEmpty(detail.getGsrdProPrice())) {
                    replenishD.setGsrdProPrice(detail.getGsrdProPrice());
                }
                if (ObjectUtil.isEmpty(detail.getGsrdProPrice())){
                    detail.setGsrdProPrice(BigDecimal.ZERO);
                }
                replenishD.setGsrdProPrice(detail.getGsrdProPrice());
                replenishD.setGsrdVoucherAmt(new BigDecimal(StrUtil.isEmpty(detail.getVoucherAmt()) ? "0" : detail.getVoucherAmt()));
                if(ObjectUtil.isNotEmpty(detail.getGsrdTaxTate())) {
                    replenishD.setGsrdTaxRate(detail.getGsrdTaxTate());
                }else {
                    replenishD.setGsrdTaxRate("0%");
                }
                if (ObjectUtil.isNotEmpty(detail.getGsrdFlag())){
                    replenishD.setGsrdFlag(detail.getGsrdFlag());
                }
                this.replenishDMapper.insert(replenishD);
                if (ObjectUtil.isEmpty(detail.getGsrdProPrice())){
                    detail.setGsrdProPrice(BigDecimal.ZERO);
                }
                if (ObjectUtil.isNotEmpty(detail.getGsrdProPrice())) {
                    if (StrUtil.isEmpty(detail.getGsrdNeedQty())) {
                        amount = amount.add(detail.getGsrdProPrice().multiply(new BigDecimal("0")));
                    } else {
                        amount = amount.add(detail.getGsrdProPrice().multiply(new BigDecimal(detail.getGsrdNeedQty())));
                    }
                }
                n ++;
            }
            replenishH.setGsrhTotalAmt(amount);
            replenishH.setGsrhTotalQty(count.toString());
        }
    }

    public void saveDetailsForColdChain(GetReplenishInData inData, GaiaSdReplenishH replenishH) {
        if (ObjectUtil.isNotEmpty(replenishH)) {
            replenishH.setGsrhStatus("0");
            replenishH.setGsrhPattern(inData.getGsrhPattern());
            replenishH.setGsrhType(inData.getGsrhType());
            replenishH.setGsrhDate(inData.getGsrhDate());
            BigDecimal amount = BigDecimal.ZERO;
            BigDecimal count = BigDecimal.ZERO;
            GetReplenishOutData outData = new GetReplenishOutData();
            outData.setGsrhType(replenishH.getGsrhType());
            outData.setGsrhDate(replenishH.getGsrhDate());
            outData.setGsrhStatus(replenishH.getGsrhStatus());
            outData.setGsrhTotalAmt(replenishH.getGsrhTotalAmt());
            outData.setGsrhTotalQty(replenishH.getGsrhTotalQty());
            outData.setGsrhEmp(replenishH.getGsrhEmp());
            List<GetReplenishDetailOutData> detailList = this.replenishDMapper.queryDetailList(inData);
            if (CollUtil.isNotEmpty(detailList)) {
                for (GetReplenishDetailOutData detail : detailList) {
                    Example exampleStore = new Example(GaiaSdReplenishD.class);
                    exampleStore.createCriteria().andEqualTo("clientId", inData.getClientId())
                            .andEqualTo("gsrdVoucherId", inData.getGsrhVoucherId())
                            .andEqualTo("gsrdBrId", inData.getStoCode());
                    this.replenishDMapper.deleteByExample(exampleStore);

                }
            }
            int n = 1;
            for (GetReplenishDetailInData detail : inData.getDetailList()) {
                if(ObjectUtil.isEmpty(detail.getGsrdNeedQty())){
                    throw new BusinessException(detail.getGsrdProName() + "(" + detail.getGsrdProId() + ")" + "补货数量未填写，请检查");
                }
                if (StrUtil.isEmpty(detail.getGsrdNeedQty())) {
                    count = count.add(new BigDecimal("0"));
                } else {
                    count = count.add(new BigDecimal(detail.getGsrdNeedQty()));
                }
                GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
                replenishD.setClientId(inData.getClientId());
                replenishD.setGsrdVoucherId(inData.getGsrhVoucherId());
                replenishD.setGsrdBrId(inData.getStoCode());
                replenishD.setGsrdDate(inData.getGsrhDate());
                if (ObjectUtil.isNotEmpty(detail.getGsrdSerial())) {
                    replenishD.setGsrdSerial(detail.getGsrdSerial());
                }else{
                    replenishD.setGsrdSerial(String.valueOf(n));
                }
                replenishD.setGsrdProId(detail.getGsrdProId());
                replenishD.setGsrdAverageCost(detail.getGsrdAverageCost());
                replenishD.setGsrdDisplayMin(detail.getGsrdDisplayMin());
                replenishD.setGsrdGrossMargin(detail.getGsrdGrossMargin());
                replenishD.setGsrdNeedQty(detail.getGsrdNeedQty().replaceFirst("^0*", "").length() == 0 ? "0" : detail.getGsrdNeedQty().replaceFirst("^0*", ""));
                replenishD.setGsrdPackMidsize(detail.getGsrdPackMidsize());
                if (ObjectUtil.isNotEmpty(detail.getGsrdProposeQty())) {
                    replenishD.setGsrdProposeQty(detail.getGsrdProposeQty());
                }else {
                    replenishD.setGsrdProposeQty(replenishD.getGsrdNeedQty());
                }
                replenishD.setGsrdSalesDays1(ValidateUtil.isEmpty(detail.getGsrdSalesDays1()) ? null : detail.getGsrdSalesDays1());
                replenishD.setGsrdSalesDays2(ValidateUtil.isEmpty(detail.getGsrdSalesDays2()) ? null : detail.getGsrdSalesDays2());
                replenishD.setGsrdSalesDays3(ValidateUtil.isEmpty(detail.getGsrdSalesDays3()) ? null : detail.getGsrdSalesDays3());
                replenishD.setGsrdStockDepot(detail.getGsrdStockDepot());
                replenishD.setGsrdStockStore(detail.getGsrdStockStore());
                replenishD.setGsrdLastSupid(detail.getGsrdLastSupid());
                GaiaProductBusiness gaiaProductBusiness = productBusinessMapper.selectByClientAndProCode(inData.getClientId(), detail.getGsrdProId());
                if (ObjectUtil.isNotEmpty(gaiaProductBusiness)) {
                    replenishD.setGsrdSendPrice(gaiaProductBusiness.getProCgj());
                }
                if(ObjectUtil.isNotEmpty(detail.getGsrdProPrice())) {
                    replenishD.setGsrdProPrice(detail.getGsrdProPrice());
                }
                if (ObjectUtil.isEmpty(detail.getGsrdProPrice())){
                    detail.setGsrdProPrice(BigDecimal.ZERO);
                }
                replenishD.setGsrdProPrice(detail.getGsrdProPrice());
                replenishD.setGsrdVoucherAmt(new BigDecimal(StrUtil.isEmpty(detail.getVoucherAmt()) ? "0" : detail.getVoucherAmt()));
                if(ObjectUtil.isNotEmpty(detail.getGsrdTaxTate())) {
                    replenishD.setGsrdTaxRate(detail.getGsrdTaxTate());
                }else {
                    replenishD.setGsrdTaxRate("0%");
                }
                if (ObjectUtil.isNotEmpty(detail.getGsrdFlag())){
                    replenishD.setGsrdFlag(detail.getGsrdFlag());
                }
                this.replenishDMapper.insert(replenishD);
                if (ObjectUtil.isEmpty(detail.getGsrdProPrice())){
                    detail.setGsrdProPrice(BigDecimal.ZERO);
                }
                if (ObjectUtil.isNotEmpty(detail.getGsrdProPrice())) {
                    if (StrUtil.isEmpty(detail.getGsrdNeedQty())) {
                        amount = amount.add(detail.getGsrdProPrice().multiply(new BigDecimal("0")));
                    } else {
                        amount = amount.add(detail.getGsrdProPrice().multiply(new BigDecimal(detail.getGsrdNeedQty())));
                    }
                }
                n ++;
            }
            replenishH.setGsrhTotalAmt(amount);
            replenishH.setGsrhTotalQty(count.toString());
        }
    }

    @Transactional
    public String insertPo(GetReplenishInData inData, GetLoginOutData userInfo) {
        GaiaPoHeader poHeader = new GaiaPoHeader();
        poHeader.setClient(inData.getClientId());
        poHeader.setPoCompanyCode(inData.getDcCode());
        poHeader.setPoDeliveryTypeStore(inData.getStoCode());
        if (ObjectUtil.isNotNull(inData.getDetailList().get(0).getSupCode())){
            poHeader.setPoSupplierId(inData.getDetailList().get(0).getSupCode());
        }else{
            if (ObjectUtil.isNotNull(inData.getDetailList().get(0).getGsrdLastSupid())){
                poHeader.setPoSupplierId(inData.getDetailList().get(0).getGsrdLastSupid());
            }else {
                poHeader.setPoSupplierId("");
            }
        }
        String voucherId = poHeaderMapper.selectNextVoucherId(inData.getClientId());
        poHeader.setPoId(voucherId);
        poHeader.setPoType("Z001");
        poHeader.setPoSubjectType("1");
        poHeader.setPoDate(CommonUtil.getyyyyMMdd());
        poHeader.setPoHeadRemark("");
        if (ObjectUtil.isNotEmpty(inData.getDetailList().get(0).getPayTerm())) {
            poHeader.setPoPaymentId(inData.getDetailList().get(0).getPayTerm());
        }else{
            poHeader.setPoPaymentId("");
        }
        poHeader.setPoCreateBy(userInfo.getUserId());
        poHeader.setPoCreateDate(CommonUtil.getyyyyMMdd());
        poHeader.setPoCreateTime(CommonUtil.getHHmmss());
        if (ObjectUtil.isNotEmpty(inData.getGsrhVoucherId())) {
            poHeader.setPoReqId(inData.getGsrhVoucherId());
        }
        poHeader.setPoApproveStatus("0");
        this.poHeaderMapper.insert(poHeader);
        int i = 1;
        for (GetReplenishDetailInData detail : inData.getDetailList()) {
//            BatchInfoInData batchInfoInData = new BatchInfoInData();
//            batchInfoInData.setClient(inData.getClientId());
//            batchInfoInData.setProSite(userInfo.getDepId());
//            batchInfoInData.setBatProCode(detail.getGsrdProId());
//            batchInfoInData.setBatPoId(voucherId);
//            batchInfoInData.setBatPoLineno(StrUtil.toString(i));
//            batchInfoInData.setBatProductDate(detail.getProMadeDate());
//            batchInfoInData.setBatExpiryDate(detail.get);
//            batchInfoInData.setBatSupplierCode(detail.get);
//            batchInfoInData.setBatPoPrice(detail.getGsrdProPrice().toString());
//            batchInfoInData.setBatReceiveQty(detail.getGsrdNeedQty());
//            batchInfoInData.setBatBatchNo(detail.getProBatchNo());
//            batchInfoInData.setBatCreateDate(CommonUtil.getyyyyMMdd());
//            batchInfoInData.setBatCreateTime(CommonUtil.getHHmmss());
//            batchInfoInData.setBatCreateUser(userInfo.getUserId());
//            batchInfoInData.setBatSourceNo(voucherId);
//            log.info("saveBatchInfo传入参数:{}", JSON.toJSONString(batchInfoInData));
//            String wmsResult = wmsService.saveBatchInfo(batchInfoInData);
//            log.info("saveBatchInfo返回:{}", JSON.toJSONString(wmsResult));
//            BaseResponse batchInfoRes =  JSONObject.parseObject(wmsResult, BaseResponse.class);
//            if(batchInfoRes.getCode() != HttpStatus.SC_OK){
//                throw new BusinessException("调用生成批次接口失败");
//            }
            if (org.apache.commons.lang3.StringUtils.isEmpty(detail.getSupCode())
                    || org.apache.commons.lang3.StringUtils.isEmpty(detail.getGsrdLastSupid())){
                throw new BusinessException("商品:"+detail.getGsrdProId()+"请选择供应商");
            }
            GaiaPoItem poItem = new GaiaPoItem();
            poItem.setClient(inData.getClientId());
            poItem.setPoId(voucherId);
            poItem.setPoLineNo(StrUtil.toString(i));
            poItem.setPoProCode(detail.getGsrdProId());
            poItem.setPoQty(new BigDecimal(detail.getGsrdNeedQty()));
            poItem.setPoUnit(detail.getGsrdProUnit());
            if (ObjectUtil.isEmpty(detail.getPoPrice())){
                detail.setPoPrice("0");
            }
            poItem.setPoPrice(new BigDecimal(detail.getPoPrice()));
            if (ObjectUtil.isNotEmpty(inData.getDcCode())) {
                poItem.setPoSiteCode(inData.getDcCode());
            }
            poItem.setPoLocationCode("1000");
            poItem.setPoLineAmt(new BigDecimal(detail.getGsrdNeedQty()).multiply(poItem.getPoPrice()));
            //批号新增
//            poItem.setPoBatch(batchInfoRes.getData().getBatBatch());
            if (ObjectUtil.isNotEmpty(detail.getGsrdInputTax())) {
                poItem.setPoRate(detail.getGsrdInputTax());
            }else {
                poItem.setPoRate("J0");
            }
            poItem.setPoDeliveryDate(DateUtil.format(new Date(), "yyyyMMdd"));
            poItem.setPoLineRemark("");
            poItem.setPoCompleteFlag("0");
            poItem.setPoLineDelete("0");
            poItem.setPoCompleteInvoice("0");
            this.poItemMapper.insert(poItem);
            i++;
        }
        return voucherId;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void approve(GetReplenishInData inData, GetLoginOutData userInfo) {
        log.info("补货单："+ inData.getGsrhVoucherId() +"审核开始");
        if (!ObjectUtil.isEmpty(inData.getGsrhVoucherId())) {
            BigDecimal titleAmt = new BigDecimal("0.0000");
            Example exampleStore = new Example(GaiaStoreData.class);
            exampleStore.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("stoCode", inData.getStoCode());
            GaiaStoreData storeData = storeDataMapper.selectOneByExample(exampleStore);
            List<GaiaSdReplenishD> list = new ArrayList<>();
            List<GetReplenishDetailOutData> detailList = this.replenishDMapper.queryDetailList(inData);
            String msg = "";
            for (GetReplenishDetailOutData data : detailList) {
//                if (ObjectUtil.isEmpty(data.getGsrdProPrice())){
//                    msg = data.getGsrdProId() + "-" + data.getGsrdProName() + "价格不存在,";
//                }
                GetReplenishInData movData = new GetReplenishInData();
                movData.setClientId(userInfo.getClient());
                movData.setProductCode(data.getGsrdProId());
                movData.setStoCode(inData.getStoCode());
//                String mov = "";
//                mov = replenishDMapper.getMovPrice(movData);
//                if (ObjectUtil.isNotEmpty(mov)) {
//                    GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
//                    replenishD.setClientId(userInfo.getClient());
//                    replenishD.setGsrdProId(data.getGsrdProId());
//                    replenishD.setGsrdBrId(userInfo.getDepId());
//                    replenishD.setGsrdVoucherId(inData.getGsrhVoucherId());
//                    BigDecimal movPrice = new BigDecimal(mov);
//                    replenishD.setGsrdAverageCost(movPrice);
//                    replenishD.setGsrdVoucherAmt(movPrice.multiply(new BigDecimal(data.getGsrdNeedQty())));
//                    titleAmt = titleAmt.add(replenishD.getGsrdVoucherAmt());
//                    list.add(replenishD);
//                } else {
                BigDecimal proPrice = BigDecimal.ZERO;
                if (ObjectUtil.isNotEmpty(data.getGsrdProPrice())) {
                    proPrice = new BigDecimal(data.getGsrdProPrice());
                }
                titleAmt = titleAmt.add(proPrice.multiply(new BigDecimal(data.getGsrdNeedQty())));
//                }
            }
            checkCreditQuotaAndZqts(inData,titleAmt,true);
//            if (ObjectUtil.isNotEmpty(msg)){
//                throw new BusinessException(msg + "请核实！");
//            }

            if (list.size() > 0) {
                replenishDMapper.updatePrice(list);
            }
            Example example = new Example(GaiaSdReplenishH.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId());
            GaiaSdReplenishH replenishH = replenishHMapper.selectOneByExample(example);
            if("1".equals(replenishH.getGsrhStatus())){
                throw new BusinessException("该补货单已审核，请勿重复审核！");
            }
            replenishH.setGsrhStatus("1");
            replenishH.setGsrhTime(CommonUtil.getHHmmss());
            replenishH.setGsrhCheckTime(CommonUtil.getyyyyMMdd());
            replenishH.setGsrhEmp(userInfo.getUserId());
//            replenishH.setGsrhType(storeData.getStoAttribute());
//            replenishH.setGsrhEmp(inData.getGsrhEmp());
            replenishH.setGsrhTotalAmt(titleAmt);
            replenishHMapper.updateByPrimaryKeySelective(replenishH);
            log.info("补货单："+ inData.getGsrhVoucherId() +"审核结束");
        }
    }

    @Override
    public GetReplenishOutData getStock(GetReplenishInData inData) {
        Map<String, String> stockMap = this.getStockMap(inData);
        GetReplenishOutData outData = new GetReplenishOutData();
        outData.setStock(StrUtil.isEmpty(StrUtil.removeSuffix(stockMap.get(inData.getProductCode()), ".0000")) ? "0" : StrUtil.removeSuffix(stockMap.get(inData.getProductCode()), ".0000"));
        return outData;
    }

    private Map<String, String> getStockMap(GetReplenishInData inData) {
        Example example = new Example(GaiaSdStock.class);
        if (ObjectUtil.isNotEmpty(inData.getProductCode())){
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", inData.getStoCode())
                    .andEqualTo("gssProId", inData.getProductCode());
        }else{
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", inData.getStoCode());
        }
        List<GaiaSdStock> stockList = this.stockMapper.selectByExample(example);
        Map<String, String> map = stockList.stream().collect(Collectors.toMap(GaiaSdStock::getGssProId, GaiaSdStock::getGssQty, (a, b) -> b));
        return map;
    }

    @Override
    public GetReplenishOutData getParam(GetReplenishInData inData) {
        Example example = new Example(GaiaSdSystemPara.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsspBrId", inData.getStoCode()).andEqualTo("gsspId", inData.getParamCode());
        GaiaSdSystemPara systemPara = (GaiaSdSystemPara) this.sdSystemParaMapper.selectOneByExample(example);
        GetReplenishOutData outData = new GetReplenishOutData();
        if (ObjectUtil.isNotEmpty(systemPara)) {
            outData.setParam(systemPara.getGsspPara());
        } else {
//            if("REPLENISH_COSTPRICE".equals(inData.getParamCode())){
//                outData.setParam("80");
//            }else {
            Example exampleD = new Example(GaiaSdSystemParaDefault.class);
            exampleD.createCriteria().andEqualTo("gsspId", inData.getParamCode());
            GaiaSdSystemParaDefault systemParaDefault = (GaiaSdSystemParaDefault) this.sdSystemParaDefaultMapper.selectOneByExample(exampleD);
            if (ObjectUtil.isNotEmpty(systemParaDefault)) {
                outData.setParam(systemParaDefault.getGsspPara());
            }
//            }
        }

        return outData;
    }

    public GetReplenishOutData getParamPrice(GetReplenishInData inData) {
        inData.setParamCode("REPLENISH_COSTPRICE");//成本价设置参数
        Example example = new Example(GaiaSdSystemPara.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsspBrId", inData.getStoCode()).andEqualTo("gsspId", inData.getParamCode());
//        GaiaSdSystemPara systemPara = (GaiaSdSystemPara) this.sdSystemParaMapper.selectOneByExample(example);
        GaiaSdSystemPara systemPara = this.sdSystemParaMapper.getAuthByClientAndBrId(inData.getClientId(),inData.getStoCode(),inData.getParamCode());
        GetReplenishOutData outData = new GetReplenishOutData();
        if (ObjectUtil.isNotEmpty(systemPara) && org.apache.commons.lang3.StringUtils.isNotEmpty(systemPara.getGsspPara())) {
            log.info("判断参数："+systemPara.getGsspPara());
            outData.setParam(systemPara.getGsspPara());
        } else {
            log.info("默认参数：" + 80);
//            Example exampleD = new Example(GaiaSdSystemParaDefault.class);
//            exampleD.createCriteria().andEqualTo("gsspId", inData.getParamCode());
//            GaiaSdSystemParaDefault systemParaDefault = (GaiaSdSystemParaDefault) this.sdSystemParaDefaultMapper.selectOneByExample(exampleD);
//            if (ObjectUtil.isNotEmpty(systemParaDefault)) {
//                outData.setParam(systemParaDefault.getGsspPara());
//            }
            outData.setParam("80");
        }
        log.info("返回参数："+outData);
        return outData;
    }

    @Override
    public GetReplenishOutData getSales(GetReplenishInData inData) {
        Map<String, GetReplenishOutData> saleMap = this.getSalesMap(inData);
        GetReplenishOutData outData = (GetReplenishOutData) saleMap.get(inData.getProductCode());
        if (ObjectUtil.isNull(outData)) {
            outData = new GetReplenishOutData();
            outData.setSalesCountOne("0");
            outData.setSalesCountTwo("0");
            outData.setSalesCountThree("0");
        }
        outData.setSalesCountOne(StrUtil.removeSuffix(outData.getSalesCountOne(), ".0000"));
        outData.setSalesCountTwo(StrUtil.removeSuffix(outData.getSalesCountTwo(), ".0000"));
        outData.setSalesCountThree(StrUtil.removeSuffix(outData.getSalesCountThree(), ".0000"));

        return outData;
    }

    private Map<String, GetReplenishOutData> getSalesMap(GetReplenishInData inData) {
         //List<GetReplenishOutData> list = this.soItemMapper.selectCountByDate(inData);
        String clientId = inData.getClientId();
        String stoCode = inData.getStoCode();
        String productCode = inData.getProductCode();
        //麒麟处理
        StringBuilder sqlBuilder = new StringBuilder()
                .append(" SELECT ")
                .append("  x.GSSD_PRO_ID , COALESCE(y.type7, '7', y.type7) AS type7,COALESCE(y.STORE_SALE_QTY7, 0, y.STORE_SALE_QTY7) STORE_SALE_QTY7,COALESCE(z.type30, '30', z.type30) type30, COALESCE(z.STORE_SALE_QTY30 , 0, z.STORE_SALE_QTY30) STORE_SALE_QTY30, COALESCE(x.type90, '90', x.type90) type90, COALESCE(x.STORE_SALE_QTY90, 0, x.STORE_SALE_QTY90) STORE_SALE_QTY90")
                .append(" FROM")
                .append("( SELECT  DT90.GCD_AREA_90 AS TYPE90,   A90.GSSD_PRO_ID,  SUM(A90.GSSD_QTY) AS STORE_SALE_QTY90   FROM   GAIA_SD_SALE_D A90 INNER JOIN GAIA_STORE_DATA B90 ON A90.CLIENT = B90.CLIENT  AND A90.GSSD_BR_ID = B90.STO_CODE INNER JOIN V_GAIA_CAL_DT_AREA DT90 ON A90.GSSD_DATE = DT90.GCD_DATE ")
                .append(" WHERE");
                if(org.apache.commons.lang3.StringUtils.isNotBlank(productCode)){
                 sqlBuilder.append(" A90.CLIENT ='"+clientId+"' AND A90.GSSD_BR_ID='"+stoCode+"' AND A90.GSSD_PRO_ID= '"+productCode+"'");
                }else {
                 sqlBuilder.append(" A90.CLIENT ='"+clientId+"' AND A90.GSSD_BR_ID='"+stoCode+"'");
                }

                sqlBuilder.append(" AND DT90.GCD_AREA_90 = '90'     GROUP BY  A90.GSSD_PRO_ID,DT90.GCD_AREA_90) x ")
                .append(" LEFT JOIN")
                .append(" (    SELECT     DT7.GCD_AREA_7 AS TYPE7,     A7.GSSD_PRO_ID,      SUM(A7.GSSD_QTY) AS STORE_SALE_QTY7   FROM     GAIA_SD_SALE_D A7    INNER JOIN GAIA_STORE_DATA B7 ON A7.CLIENT = B7.CLIENT    AND A7.GSSD_BR_ID = B7.STO_CODE      INNER JOIN V_GAIA_CAL_DT_AREA DT7 ON A7.GSSD_DATE = DT7.GCD_DATE")
                .append( " WHERE");
                if(org.apache.commons.lang3.StringUtils.isNotBlank(productCode)){
                    sqlBuilder.append( " A7.CLIENT ='"+clientId+"' AND A7.GSSD_BR_ID='"+stoCode+"' AND A7.GSSD_PRO_ID='"+productCode+"'");
                }else {
                    sqlBuilder.append( " A7.CLIENT ='"+clientId+"'AND A7.GSSD_BR_ID='"+stoCode+"'");
                }

                    sqlBuilder.append(" AND DT7.GCD_AREA_7 = '7'    GROUP BY  A7.GSSD_PRO_ID,DT7.GCD_AREA_7) y ON x.GSSD_PRO_ID = y.GSSD_PRO_ID ")
                .append(" LEFT JOIN")
                .append(" ( SELECT DT30.GCD_AREA_30 AS TYPE30, A30.GSSD_PRO_ID,SUM(A30.GSSD_QTY) AS STORE_SALE_QTY30 FROM GAIA_SD_SALE_D A30 INNER JOIN GAIA_STORE_DATA B30 ON A30.CLIENT = B30.CLIENT AND A30.GSSD_BR_ID = B30.STO_CODE INNER JOIN V_GAIA_CAL_DT_AREA DT30 ON A30.GSSD_DATE = DT30.GCD_DATE")
                .append(" WHERE");
                if(org.apache.commons.lang3.StringUtils.isNotBlank(productCode)){
                    sqlBuilder.append( " A30.CLIENT ='"+clientId+"'AND A30.GSSD_BR_ID='"+stoCode+"'AND A30.GSSD_PRO_ID='"+productCode+"'");
                }else {
                    sqlBuilder.append( " A30.CLIENT ='"+clientId+"'AND A30.GSSD_BR_ID='"+stoCode+"'");
                }

                    sqlBuilder.append("AND DT30.GCD_AREA_30 = '30'    GROUP BY   A30.GSSD_PRO_ID,DT30.GCD_AREA_30) z ON x.GSSD_PRO_ID = z.GSSD_PRO_ID")
                .append(" ORDER BY x.GSSD_PRO_ID") ;
                log.info("sql统计报文：{}", sqlBuilder.toString());
                List<KylinSaleVo> query = kylinJdbcTemplate.query(sqlBuilder.toString(), RowMapper.getDefault(KylinSaleVo.class));
              // 转换
                List<GetReplenishOutData> list= new ArrayList<>();
                if(org.apache.commons.collections4.CollectionUtils.isNotEmpty(query)){
                   for(KylinSaleVo vo:query){
                       GetReplenishOutData getReplenishOutData=new GetReplenishOutData();
                        getReplenishOutData.setProId(vo.getGssdProId());
                        getReplenishOutData.setSalesCountOne(new BigDecimal(vo.getStoreSaleQty90()).stripTrailingZeros().toPlainString());
                        getReplenishOutData.setSalesCountTwo(new BigDecimal(vo.getStoreSaleQty30()).stripTrailingZeros().toPlainString());
                        getReplenishOutData.setSalesCountThree(new BigDecimal(vo.getStoreSaleQty7()).stripTrailingZeros().toPlainString());
                        list.add(getReplenishOutData);
                   }
                }
              Map<String, GetReplenishOutData> map = list.stream().collect(Collectors.toMap(GetReplenishOutData::getProId, x -> x, (a, b) -> b));
        return map;
    }

    private Map<String, QuerySupplierData> getSupplierNameMap(String clientId,String brId) {
        List<QuerySupplierData> list = this.replenishDMapper.querySupplierName(clientId,brId);
        Map<String, QuerySupplierData> map = list.stream().collect(Collectors.toMap(QuerySupplierData::getPoProCode, x -> x, (a, b) -> b));
        return map;
    }

    @Override
    public GetReplenishOutData getCost(GetReplenishInData inData) {
        Map<String, GetReplenishOutData> costMap = this.getCostMap(inData);
        GetReplenishOutData outData = (GetReplenishOutData) costMap.get(inData.getProductCode());
        if (ObjectUtil.isNull(outData)) {
            outData = new GetReplenishOutData();
        }

        return outData;
    }

    private Map<String, GetReplenishOutData> getCostMap(GetReplenishInData inData) {
        Map<String, GetReplenishOutData> map = new HashMap();
        Example example = new Example(GaiaBatchStock.class);
        example.setOrderByClause("BAT_CREATE_DATE DESC");
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("batLocationCode", inData.getStoCode()).andEqualTo("batProCode", inData.getProductCode());
        List<GaiaBatchStock> batchStockList = this.batchStockMapper.selectByExample(example);

        batchStockList.stream().filter(batchStock -> !ObjectUtil.isNotNull(map.get(batchStock.getBatProCode()))).forEach(batchStock -> {
            GetReplenishOutData outData = new GetReplenishOutData();
            outData.setAverageCost(batchStock.getBatMovPrice());
            outData.setBatchCost(batchStock.getBatBatchCost());
            map.put(batchStock.getBatProCode(), outData);
        });
        return map;
    }

//    @Override
//    public GetReplenishOutData getCount(GetReplenishInData inData) {
//        GetReplenishOutData outData = new GetReplenishOutData();
//        Map<String, String> countMap = this.getCountMap(inData);
//        String count = (String) countMap.get(inData.getProductCode());
//        if (StrUtil.isBlank(count)) {
//            count = "0";
//        }
//
//        outData.setGsrhTotalQty(count);
//        return outData;
//    }

    private Map<String, String> getCountMap(GetReplenishInData inData,Map<String, GetReplenishOutData> saleMap,Map<String, String> stockMap,List<GetReplenishDetailOutData> proList) {
        Map<String, String> map = new HashMap();
        Example exampleStore = new Example(GaiaStoreData.class);
        log.info("设置默认最小陈列量开始");
        exampleStore.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("stoCode", inData.getStoCode());
        GaiaStoreData storeData = (GaiaStoreData) this.storeDataMapper.selectOneByExample(exampleStore);
        int days = 7;
        Example examplePara;
        if (StrUtil.isBlank(storeData.getStoChainHead())) {
            GetAcceptInData acceptInData = new GetAcceptInData();
            acceptInData.setClientId(inData.getClientId());
            acceptInData.setGsadProId(inData.getProductCode());
            List<GetPoOutData> list = this.poHeaderMapper.poList(acceptInData);
            if (ObjectUtil.isNotEmpty(list)) {
                GetPoOutData poOutData = (GetPoOutData) list.get(0);
                if(ObjectUtil.isNotEmpty(poOutData.getPoSupplierId())) {
                    examplePara = new Example(GaiaSupplierBusiness.class);
                    examplePara.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("supSelfCode", poOutData.getPoSupplierId()).andEqualTo("supSite", inData.getStoCode());
                    GaiaSupplierBusiness supplierBusiness = (GaiaSupplierBusiness) this.supplierBusinessMapper.selectOneByExample(examplePara);
                    if (ObjectUtil.isNotEmpty(supplierBusiness)) {
                        if(ObjectUtil.isNotEmpty(supplierBusiness.getSupLeadTime())){
                            days = Integer.parseInt(supplierBusiness.getSupLeadTime());
                        }
                    }
                }
            }
        } else {
            Example exampleSD = new Example(GaiaSdStoreData.class);
            exampleSD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsstBrId", inData.getStoCode());
            GaiaSdStoreData sdStoreData = (GaiaSdStoreData) this.sdStoreDataMapper.selectOneByExample(exampleSD);
            if (ObjectUtil.isNotEmpty(sdStoreData)) {
                if (ObjectUtil.isNotEmpty(sdStoreData.getGsstPsDate())) {
                    days = Integer.parseInt(sdStoreData.getGsstPsDate());
                }
            }
        }
        log.info("设置默认最小陈列量结束");
        List<GetReplenishOutData> list = this.soItemMapper.selectCountList(inData);
        Map<String, List<Integer>> saleDayMap = new HashMap();
        for(GetReplenishOutData sale: list){
            List<Integer> saleDayList = (List) saleDayMap.get(sale.getProId());
            if (CollUtil.isEmpty((Collection) saleDayList)) {
                saleDayList = new ArrayList();
            }

            if (sale.getSaleQty() != null) {
                (saleDayList).add((new BigDecimal(sale.getSaleQty())).intValue());
            }

            saleDayMap.put(sale.getProId(), saleDayList);
        }

        examplePara = new Example(GaiaSdReplenishPara.class);
        examplePara.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsepBrId", inData.getStoCode());
        List<GaiaSdReplenishPara> replenishParaList = this.replenishParaMapper.selectByExample(examplePara);
        Map<String, GaiaSdReplenishPara> replenishParaMap = new HashMap();
        replenishParaList.forEach((x) -> {
            GaiaSdReplenishPara var10000 = (GaiaSdReplenishPara) replenishParaMap.put(x.getGsepId(), x);
        });
        BigDecimal d = new BigDecimal(2);
        Example exampleP = new Example(GaiaSdSystemPara.class);
        exampleP.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsspBrId", inData.getStoCode()).andEqualTo("gsspId", inData.getParamCode());
        GaiaSdSystemPara systemPara = (GaiaSdSystemPara) this.sdSystemParaMapper.selectOneByExample(exampleP);
        GetReplenishOutData priceParam = this.getParamPrice(inData);
        log.info("参数设置结束");
        if (ObjectUtil.isNotEmpty(systemPara)) {
            d = new BigDecimal(systemPara.getGsspPara());
        } else {
            Example exampleD = new Example(GaiaSdSystemParaDefault.class);
            exampleD.createCriteria().andEqualTo("gsspId", inData.getParamCode());
            GaiaSdSystemParaDefault systemParaDefault = (GaiaSdSystemParaDefault) this.sdSystemParaDefaultMapper.selectOneByExample(exampleD);
            if (ObjectUtil.isNotEmpty(systemParaDefault)) {
                d = new BigDecimal(systemParaDefault.getGsspPara());
            }
        }

//        List<GetReplenishDetailOutData> proList = this.productBasicMapper.queryProFromReplenish(inData);
        for(GetReplenishDetailOutData pro:proList){
            BigDecimal xd = BigDecimal.ZERO;
//            log.info("成本价开始：" + pro.getGsrdProId());
            if(ObjectUtil.isNotEmpty(pro.getMatMovPrice())){
                if(ObjectUtil.isNotEmpty(priceParam.getParam())){
                    if(new BigDecimal(pro.getMatMovPrice()).compareTo(new BigDecimal(priceParam.getParam())) == -1){
                        xd = d;
                    }
                }else {
                    if(new BigDecimal(pro.getMatMovPrice()).compareTo(new BigDecimal(80)) == -1){
                        xd = d;
                    }
                }
            }else{
                xd = d;
            }
//            log.info("成本价结束：" + xd);
            BigDecimal Q = BigDecimal.ZERO;
            BigDecimal X = BigDecimal.ZERO;
            BigDecimal S = BigDecimal.ZERO;
            BigDecimal T = BigDecimal.ZERO;
            new GetReplenishOutData();
            if (StrUtil.isNotBlank((CharSequence) stockMap.get(pro.getGsrdProId()))) {
                T = new BigDecimal((String) stockMap.get(pro.getGsrdProId()));
            }
//            log.info("销售数量列表开始");
            GetReplenishOutData saleData = (GetReplenishOutData) saleMap.get(pro.getGsrdProId());
//            log.info("销售数据：" + saleData.toString());
            if (ObjectUtil.isNotNull(saleData) && (StrUtil.isBlank(saleData.getSalesCountTwo()) || "0".equals(saleData.getSalesCountTwo())) && StrUtil.isNotBlank(saleData.getSalesCountOne()) && !"0".equals(saleData.getSalesCountOne())) {
                map.put(pro.getGsrdProId(), xd.setScale(0, 4).toString());
            } else {
                String paramId = "";
                int sumQty = 0;
                List<Integer> salesCountList = (List) saleDayMap.get(pro.getGsrdProId());
                if (salesCountList == null) {
                    salesCountList = new ArrayList();
                }

                for (int qty : salesCountList) {
                    sumQty += qty;
                }
//                log.info("销售数据设置等级开始");
                if (((List) salesCountList).size() <= 15) {
                    X = new BigDecimal(sumQty);
                    if (sumQty < 5) {
                        paramId = "1";
                    }

                    if (sumQty >= 5 && sumQty < 10) {
                        paramId = "2";
                    }

                    if (sumQty >= 10 && sumQty < 15) {
                        paramId = "3";
                    }

                    if (sumQty >= 15 && sumQty < 30) {
                        paramId = "4";
                    }

                    if (sumQty >= 30) {
                        paramId = "5";
                    }
                }
//                log.info("销售数据设置等级结束");
                BigDecimal c;
                if (((List) salesCountList).size() > 15) {
                    BigDecimal aveg = (new BigDecimal(sumQty)).divide(new BigDecimal(((List) salesCountList).size()), 2, 4);
                    c = BigDecimal.ZERO;

                    for (int qty : salesCountList) {
                        c = c.add((new BigDecimal(qty)).subtract(aveg).multiply((new BigDecimal(qty)).subtract(aveg)));
                    }

                    BigDecimal a = c.divide(new BigDecimal(((List) salesCountList).size()), 2, 4);
                    BigDecimal diff = new BigDecimal(Math.sqrt(a.doubleValue()));

//                    BigDecimal a = new BigDecimal(Math.sqrt(c.doubleValue()));
//                    BigDecimal diff = a.divide(new BigDecimal(((List) salesCountList).size()), 2, 4);
                    if (diff.compareTo(BigDecimal.ZERO)!=0) {
                        Iterator it = ((List) salesCountList).iterator();
                        while (it.hasNext()) {
                            BigDecimal b = (new BigDecimal((Integer) it.next())).subtract(aveg).divide(diff, 2, 4);
                            if (b.compareTo(new BigDecimal(3)) > 0) {
                                it.remove();
                            }
                        }

                        for (int qty : salesCountList) {
//                    Iterator var52 = ((List) salesCountList).iterator();
//
//                    while (var52.hasNext()) {
//                        qty = (Integer) var52.next();
                            BigDecimal b = (new BigDecimal(qty)).subtract(aveg).divide(diff, 2, 4);
                            if (b.compareTo(new BigDecimal(3)) > 0) {
                                ((List) salesCountList).remove(qty);
                            }
                        }
                    }

                    int sumQty1 = 0;
                    for (int qty : salesCountList) {
                        sumQty1 += qty;
                    }

//                        if (((List) salesCountList).size() <= 15) {
                    X = new BigDecimal(sumQty1);
                    if (sumQty < 5) {
                        paramId = "1";
                    }
                    if (sumQty >= 5 && sumQty < 10) {
                        paramId = "2";
                    }

                    if (sumQty >= 10 && sumQty < 15) {
                        paramId = "3";
                    }

                    if (sumQty >= 15 && sumQty < 30) {
                        paramId = "4";
                    }

                    if (sumQty >= 30) {
                        paramId = "5";
                    }
//                        }
                }
//                log.info("销售数量列表结束");
//                log.info("补货参数开始");
                GaiaSdReplenishPara replenishPara = (GaiaSdReplenishPara) replenishParaMap.get(paramId);
                if (replenishPara != null) {
                    if (days <= 7) {
                        Q = replenishPara.getGsepPara1();
                    } else if (days > 7 && days <= 15) {
                        Q = replenishPara.getGsepPara2();
                    } else if (days > 15) {
                        Q = replenishPara.getGsepPara3();
                    }

                    S = X.multiply(Q).subtract(T);
                    if (S.compareTo(new BigDecimal(0)) <= 0) {
                        map.put(pro.getGsrdProId(), "0");
                    } else {
                        if ("3".equals(pro.getProStorageArea())) {
                            c = S.divide(new BigDecimal(pro.getProMidPackage()),1,4);
                            String[] a = c.toString().split("\\.");
//                            if (Integer.parseInt(a[1]) >= 8) {
//                                S = new BigDecimal(Integer.parseInt(a[0]) + 1);
//                            } else {
//                                S = c;
//                            }
                            BigDecimal[] resRemainder= S.divideAndRemainder(new BigDecimal(pro.getProMidPackage()));
                            if (Integer.parseInt(a[1]) >= 8) {
                                S=resRemainder[0].multiply(new BigDecimal(pro.getProMidPackage())).add(new BigDecimal(pro.getProMidPackage()));
                            }else{
                                S=S;
                            }
                        }

                        if (xd.subtract(T).compareTo(S) > 0) {
                            map.put(pro.getGsrdProId(), xd.subtract(T).toString());
                        } else {
                            map.put(pro.getGsrdProId(), S.setScale(0, 4).toString());
                        }
                    }
                }
//                log.info("补货参数结束");
            }
        }

        return map;

    }

    @Override
    @Transactional
    public boolean isRepeat(GetReplenishInData inData) {
        Example example = new Example(GaiaSdReplenishH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrhDate", inData.getGsrhDate()).andEqualTo("gsrhPattern", "0");
        List<GaiaSdReplenishH> replenishHList = this.replenishHMapper.selectByExample(example);
        System.out.println("replenishHList 查询:" + replenishHList);
        return ObjectUtil.isNotEmpty(replenishHList);
    }

    @Override
    public GetReplenishOutData getVoucherAmt(GetReplenishInData inData) {
        GetReplenishOutData outData = new GetReplenishOutData();
        Map<String, String> amtMap = this.getVoucherAmtMap(inData);
        String amt = (String) amtMap.get(inData.getProductCode());
        if (StrUtil.isBlank(amt)) {
            amt = "0";
        }

        outData.setVoucherAmt(amt);
        return outData;
    }


    private Map<String, String> getVoucherAmtMap(GetReplenishInData inData) {
        Example example = new Example(GaiaMaterialAssess.class);
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("matAssessSite", inData.getStoCode());
        List<GaiaMaterialAssess> materialAssessList = this.materialAssessMapper.selectByExample(example);
        Map<String, String> map = materialAssessList.stream().collect(Collectors.toMap(GaiaMaterialAssess::getMatProCode, x -> x.getMatMovPrice().toString(), (a, b) -> b));
        return map;
    }

    private String changeDateType(String dateStr) {
        String dateString = "";
        try {
            DateFormat format = new SimpleDateFormat("yyyyMMdd");
            Date date = format.parse(dateStr);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateString = simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.getMessage();
        }
        return dateString;
    }

    private String changeDateType2(String dateStr) {
        String dateString = "";
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(dateStr);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            dateString = simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.getMessage();
        }
        return dateString;
    }

    @Override
    public Map<String,String> showWareHouseStock(GetLoginOutData userInfo) {
        String isShowWareHouseStock = storeDataMapper.selectStoPriceComparison(userInfo.getClient(),userInfo.getDepId(),"DEPOTSTOCK_SHOW");
        if (ObjectUtil.isEmpty(isShowWareHouseStock)){
            isShowWareHouseStock = "0";
        }
        Map<String,String> result = new HashMap<>();
        result.put("isShow",isShowWareHouseStock);
        String isShowDepotPrice = storeDataMapper.selectStoPriceComparison(userInfo.getClient(),userInfo.getDepId(),"DEPOTPRICE_SHOW");
        if (ObjectUtil.isEmpty(isShowDepotPrice)){
            isShowDepotPrice = "0";
        }
        result.put("isShowDepotPrice",isShowDepotPrice);
        //是否开启委托配送相应功能 0：关 1：开	  默认或没有值为0
        String isWtpsParam = storeDataMapper.selectWtpsParam(userInfo.getClient(),"WTPS_SET");
        if (StringUtils.isNullOrEmpty(isWtpsParam)){
            isWtpsParam = "0";
        }
        result.put("isWtpsParam",isWtpsParam);
        //补货数量是否必须整数（0-否，缺省；1-是（整数））
        String isIntegerQty = storeDataMapper.selectStoPriceComparison(userInfo.getClient(),userInfo.getDepId(),"BH_QTY_INTEGER");
        if (ObjectUtil.isEmpty(isIntegerQty)){
            isIntegerQty = "0";
        }
        result.put("isIntegerQty",isIntegerQty);
        return result;
    }

    @Override
    public void insertToRedis(GetReplenishInData inData) {
        if(ObjectUtil.isNotEmpty(inData.getDetailList()) && inData.getDetailList().size() > 0) {
            String keyName = inData.getClientId() + "-" + inData.getStoCode() + "-" + CommonUtil.getyyyyMMdd() + "-" + inData.getGsrhPattern();
            Integer second = 12 * 3600;
            redisManager.set(keyName, JSON.toJSONString(inData), second.longValue());
        }
    }

    @Override
    public GetReplenishInData selectFromRedis(GetReplenishInData inData) {
        String keyName = inData.getClientId() + "-" + inData.getStoCode() + "-" + CommonUtil.getyyyyMMdd() + "-" + inData.getGsrhPattern();
        Object obj = redisManager.get(keyName);
        System.out.println(obj);
        GetReplenishInData result = JSONArray.parseObject(obj.toString(), GetReplenishInData.class);
        return result;
    }

    @Override
    public JsonResult getOutOfStockExplain(OutOfStockInData inData) {
        //如果传入日期为空，则默认查询当日起始
        if(ObjectUtil.isEmpty(inData.getQueryDate())) {
            inData.setQueryDate(com.gys.util.DateUtil.getCurrentDate());
        }
        //配置数据 天数
        GaiaSdSystemPara systemPara = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClientId(), inData.getBrId(), "PRO_STOCK_DAYS");
        if(ValidateUtil.isEmpty(systemPara)) {
            inData.setDays(CommonConstant.OUT_OF_STOCK_DAY_90);
        } else {
            inData.setDays(Integer.valueOf(systemPara.getGsspPara()));
        }
        //缺断货汇总
        OutOfStockOutData outOfStockExplain = productBusinessMapper.getOutOfStockExplain(inData);

        //查询缺断货商品数据加入补货计划追溯时间
        GaiaSdSystemPara systemParaFillDay = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClientId(), inData.getBrId(), "PRO_FILL_DAYS");
        if(ValidateUtil.isEmpty(systemParaFillDay)) {
            inData.setFillDays(7);
        } else {
            inData.setFillDays(Integer.valueOf(systemParaFillDay.getGsspPara()));
        }
        //缺断货列表
        List<OutOfStockOutListData> outOfStockExplainList = productBusinessMapper.getOutOfStockExplainList(inData);
        //判断是否直营管理 如果是直营管理门店 则不可以淘汰，如果非直营管理可以淘汰
        GaiaSdStoresGroup storesGroup = storesGroupMapper.getByClientAndStoreAndType(inData.getClientId(), inData.getBrId(), "DX0003");
        //修改推送消息读取状态
        try {
            readMsg(inData);
        } catch (Exception e) {
            log.error("修改消息状态失败：", e.getMessage());
            throw new BusinessException("提示：修改消息状态失败");
        }
        //返回参数
        Map<String, Object> resultMap = new HashMap<>(3);
        resultMap.put("outOfStockExplain", outOfStockExplain);
        resultMap.put("outOfStockExplainList", outOfStockExplainList);
        resultMap.put("isDieOut", ValidateUtil.isNotEmpty(storesGroup) ? false : true);
        return JsonResult.success(resultMap, "success");
    }


    @Override
    @Transactional
    public JsonResult addStoreReplenishPlan(StoreReplenishPlanInData inData) {
        //获取参数
        List<OutOfStockOutListData> outOfStockOutList = inData.getOutOfStockOutList();
        if(ValidateUtil.isEmpty(outOfStockOutList)) {
            throw new BusinessException("提示：请至少选择一项补货商品");
        }

        List<OutOfStockOutListData> normalProList = new ArrayList<>();
        List<OutOfStockOutListData> specialProList = new ArrayList<>();
        //判断是否有特殊商品
        for(OutOfStockOutListData outOfStock : outOfStockOutList) {
            String isSpecial = outOfStock.getIsSpecial();
            if(CommonConstant.SPECIAL_0.equals(isSpecial)) {          //普通商品
                normalProList.add(outOfStock);
            } else {
                specialProList.add(outOfStock);  //特殊商品
            }
        }

        //加盟商
        String clientId = inData.getClientId();
        //门店
        String brId = inData.getBrId();
        //补货日期
        String date = CommonUtil.getyyyyMMdd();
        //补货类型
        String type = "";
        Map<String, String> stoInfo = productBasicMapper.selectStoreAttribute(clientId, brId);
        if(ValidateUtil.isNotEmpty(stoInfo)) {
            type = stoInfo.get("deliveryMode");
            if(ValidateUtil.isEmpty(type)) {
                type = CommonConstant.TYPE_2;
            }
        }
        //补货人员
        String userId = inData.getUserId();

        BigDecimal replenishPrice = BigDecimal.ZERO;
        GaiaSdSystemPara systemPara = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClientId(), inData.getBrId(), "PRO_REPLENISH_PRICE");
        if(ValidateUtil.isEmpty(systemPara)) {
            replenishPrice = new BigDecimal(CommonConstant.COMPARE_PRICE_100);
        } else {
            replenishPrice = new BigDecimal(systemPara.getGsspPara());
        }
        //处理普通商品
        if(ValidateUtil.isNotEmpty(normalProList)) {
            //补货地点
            String address = "";
            if(ValidateUtil.isNotEmpty(stoInfo)) {
                if("2".equals(stoInfo.get("stoAttribute"))) {
                    address = stoInfo.get("stoDcCode");
                }
            }
            //补货数量  缺断货商品加入补货计划，补货量计算，平均售价>=100元，补货量=2；平均售价<100元，补货量=3。平均售价=月均销售额/月均销量
            Map<String, BigDecimal> proNumberMap = new HashMap<>(normalProList.size());
            BigDecimal replenishQty = BigDecimal.ZERO;
            for(OutOfStockOutListData outOfStock : normalProList) {
                BigDecimal avgAmt = outOfStock.getAvgAmt();
                BigDecimal avgQty = outOfStock.getAvgQty();
                BigDecimal proNumber = calculationNumber(avgAmt, avgQty, replenishPrice);
                replenishQty = replenishQty.add(proNumber);
                proNumberMap.put(outOfStock.getProductId(), proNumber);
            }
            //补货金额汇总
            BigDecimal replenishTotalAmt = BigDecimal.ZERO;

            for(OutOfStockOutListData outOfStock : normalProList) {
                String productId = outOfStock.getProductId();
                BigDecimal price = outOfStock.getPriceNormal();
                BigDecimal number = proNumberMap.get(productId);
                if(ValidateUtil.isNotEmpty(price)) {
                    replenishTotalAmt = replenishTotalAmt.add(price.multiply(number));
                } else {
                    replenishTotalAmt = replenishTotalAmt.add(BigDecimal.ZERO);
                }
            }
            //组装参数
            GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
            replenishH.setClientId(clientId);
            replenishH.setGsrhVoucherId(commonService.selectNextVoucherId(replenishH));
            replenishH.setGsrhBrId(brId);
            replenishH.setGsrhDate(date);
            replenishH.setGsrhType(type);
            replenishH.setGsrhAddr(address);
            replenishH.setGsrhTotalAmt(replenishTotalAmt);
            replenishH.setGsrhTotalQty(replenishQty.toString());
            replenishH.setGsrhEmp(userId);
            replenishH.setGsrhStatus(CommonConstant.STATUS_0);
            replenishH.setGsrhFlag(CommonConstant.FLAG_0);
            replenishH.setGsrhPattern(inData.getPattern());
            replenishH.setGsrhSpecial(CommonConstant.SPECIAL_0);
            replenishH.setGsrhCreateTime(CommonUtil.getHHmmss());
            replenishH.setGsrhSource(CommonConstant.SOURCE_1);
            replenishHMapper.insert(replenishH);
            //处理普通商品明细列表
            insertReplenishD(normalProList, replenishH, replenishPrice);
        }

        //处理特殊商品
        if(ValidateUtil.isNotEmpty(specialProList)) {
            Map<String, OutOfStockOutListData> dcCodeMap = new HashMap<>(16);
            for(OutOfStockOutListData outOfStock : specialProList) {
                String dcCode = outOfStock.getDcCode();
                if(dcCodeMap.containsKey(dcCode)) {   //如果已经存在这个dcCode
                    //获取原有对象
                    OutOfStockOutListData oldOutOfStock = dcCodeMap.get(dcCode);
                    BigDecimal oldProPrice = oldOutOfStock.getAvgAmt();
                    BigDecimal oldProNumber = oldOutOfStock.getAvgQty();

                    //获取当前对象
                    BigDecimal newAvgAmt = outOfStock.getAvgAmt();
                    BigDecimal newAvgQty = outOfStock.getAvgQty();
                    BigDecimal newProNumber = calculationNumber(newAvgAmt, newAvgQty, replenishPrice);
                    //计算金额
                    BigDecimal newProPrice = outOfStock.getPriceNormal();
                    if(ValidateUtil.isEmpty(newProPrice)) {
                        newProPrice = BigDecimal.ZERO;
                    }
                    BigDecimal newProAmt = newProPrice.multiply(newProNumber);
                    oldOutOfStock.setAvgAmt(oldProPrice.add(newProAmt));
                    oldOutOfStock.setAvgQty(newProNumber.add(oldProNumber));
                    dcCodeMap.put(dcCode, oldOutOfStock);
                } else {    //不存在这个dcCode
                    //第一次进入时 计算好价格以及数量
                    BigDecimal avgAmt = outOfStock.getAvgAmt();
                    BigDecimal avgQty = outOfStock.getAvgQty();
                    BigDecimal proNumber = calculationNumber(avgAmt, avgQty, replenishPrice);
                    BigDecimal proPrice = outOfStock.getPriceNormal();
                    if(ValidateUtil.isEmpty(proPrice)) {
                        proPrice = BigDecimal.ZERO;
                    }
                    //汇总金额/数量
                    BigDecimal proAmt = proPrice.multiply(proNumber);

                    //创建新的对象赋值(不直接使用当前对象，因为会使对象中的属性值改变,导致下面重新使用对象时属性值不准确)
                    OutOfStockOutListData newOutOfStock = new OutOfStockOutListData();
                    newOutOfStock.setAvgAmt(proAmt);
                    newOutOfStock.setAvgQty(proNumber);
                    dcCodeMap.put(dcCode, newOutOfStock);
                }
            }

            Set<String> dcCodeSet = dcCodeMap.keySet();
            for(String dcCode : dcCodeSet) {
                OutOfStockOutListData outOfStock = dcCodeMap.get(dcCode);
                //组装参数
                GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
                replenishH.setClientId(clientId);
                replenishH.setGsrhVoucherId(commonService.selectNextVoucherId(replenishH));
                replenishH.setGsrhBrId(brId);
                replenishH.setGsrhDate(date);
                replenishH.setGsrhType(type);
                replenishH.setGsrhAddr(stoInfo.get("stoDcCode"));
                replenishH.setGsrhTotalAmt(outOfStock.getAvgAmt());
                replenishH.setGsrhTotalQty(outOfStock.getAvgQty().toString());
                replenishH.setGsrhEmp(userId);
                replenishH.setGsrhStatus(CommonConstant.STATUS_0);
                replenishH.setGsrhFlag(CommonConstant.FLAG_0);
                replenishH.setGsrhPattern(inData.getPattern());
                replenishH.setGsrhSpecial(CommonConstant.SPECIAL_1);
                replenishH.setGsrhCreateTime(CommonUtil.getHHmmss());
                replenishH.setGsrhSource(CommonConstant.SOURCE_1);
                replenishHMapper.insert(replenishH);

                //处理特殊商品明细列表
                List<OutOfStockOutListData> insertList = new ArrayList<>();
                for(OutOfStockOutListData outOfStockExt : specialProList) {
                    String dcCodeExt = outOfStockExt.getDcCode();
                    if(dcCode.equals(dcCodeExt)) {
                        insertList.add(outOfStockExt);
                    }
                }
                insertReplenishD(insertList, replenishH, replenishPrice);
            }
        }

        //加入补货历史
        insertReplenishHistory(clientId, brId, outOfStockOutList);
        //推送消息
        sendMessage(clientId, brId);
        //返回参数
        Map<String, Object> resultMap = new HashMap<>(3);
        resultMap.put("replenishNumber", outOfStockOutList.size());
        resultMap.put("avgAmt", outOfStockOutList.stream().map(OutOfStockOutListData :: getAvgAmt).reduce(BigDecimal.ZERO, BigDecimal :: add));
        resultMap.put("avgProfitAmt", outOfStockOutList.stream().map(OutOfStockOutListData :: getAvgProfitAmt).reduce(BigDecimal.ZERO, BigDecimal :: add));
        return JsonResult.success(resultMap, "success");
    }


    /**
     * 插入补货明细表
     * @param detailList
     * @param replenishH
     */
    private void insertReplenishD(List<OutOfStockOutListData> detailList, GaiaSdReplenishH replenishH, BigDecimal replenishPrice) {
        int serialNo = 1;
        for(OutOfStockOutListData outOfStock : detailList) {
            GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
            replenishD.setClientId(replenishH.getClientId());
            replenishD.setGsrdBrId(replenishH.getGsrhBrId());
            replenishD.setGsrdVoucherId(replenishH.getGsrhVoucherId());
            replenishD.setGsrdDate(replenishH.getGsrhDate());
            replenishD.setGsrdSerial(String.valueOf(serialNo ++));
            replenishD.setGsrdProId(outOfStock.getProductId());
            replenishD.setGsrdStockStore(outOfStock.getStoreStock());

            //补货量
            BigDecimal avgAmt = outOfStock.getAvgAmt();
            BigDecimal avgQty = outOfStock.getAvgQty();
            BigDecimal proNumber = calculationNumber(avgAmt, avgQty, replenishPrice);
            replenishD.setGsrdProposeQty(proNumber.toString());
            replenishD.setGsrdNeedQty(proNumber.toString());
            //订单价 补货量 * 零售价
            BigDecimal proPrice = outOfStock.getPriceNormal();
            if(ValidateUtil.isEmpty(proPrice)) {
                proPrice = BigDecimal.ZERO;
            } else {
                proPrice = proPrice.multiply(proNumber);
            }
            replenishD.setGsrdVoucherAmt(proPrice);
            replenishDMapper.insert(replenishD);
        }
    }


    /**
     * 通过月均销售额以及销售数量 计算补货数量
     * @param avgAmt  月均销售额
     * @param avgQty  月均销售数量
     * @return
     */
    private BigDecimal calculationNumber(BigDecimal avgAmt, BigDecimal avgQty, BigDecimal replenishPrice) {
        BigDecimal result = null;
        if(ValidateUtil.isNotEmpty(avgAmt) && ValidateUtil.isNotEmpty(avgQty) && ValidateUtil.isNotEmpty(replenishPrice)) {
            long avgPrice = avgAmt.divide(avgQty, 2, BigDecimal.ROUND_HALF_UP).longValue();
            if(avgPrice >= replenishPrice.longValue()) {
                result = new BigDecimal(CommonConstant.REPLENISH_QTY_2);
            } else {
                result = new BigDecimal(CommonConstant.REPLENISH_QTY_3);
            }
        }
        return result;
    }


    /**
     * 发送消息
     * @param cliendId
     * @param brId
     */
    private void sendMessage(String cliendId, String brId) {
        GaiaSdMessage message = new GaiaSdMessage();
        message.setClient(cliendId);
        message.setGsmId(brId);
        message.setGsmVoucherId(gaiaSdMessageMapper.selectNextVoucherIdFX(cliendId, CommonConstant.MESSAGE_QUERY_STORE));
        message.setGsmType(CommonConstant.MESSAGE_GAIA_SD_0104);
        message.setGsmRemark(CommonConstant.MESSAGE_DESC);
        message.setGsmFlag(CommonConstant.MESSAGE_FLAG_N);
        message.setGsmPage(CommonConstant.MESSAGE_PAGE_REPLENISHMENT);
        message.setGsmWarningDay(CommonConstant.MESSAGE_WARNING_DAY_180);
        message.setGsmArriveDate(CommonUtil.getyyyyMMdd());
        message.setGsmArriveTime(CommonUtil.getHHmmss());
        message.setGsmPlatForm(CommonConstant.MESSAGE_PLATFORM_TYPE_FX);
        message.setGsmDeleteFlag(CommonConstant.MESSAGE_DELETE_FLAG_0);
        gaiaSdMessageMapper.insertSelective(message);
    }


    /**
     * 加入补货历史
     * @param client
     * @param brId
     * @param detitalList
     */
    private void insertReplenishHistory(String client, String brId, List<OutOfStockOutListData> detailList) {
        List<GaiaSdReplenishHistoryInData> historyList = new ArrayList<GaiaSdReplenishHistoryInData>();
        if(ValidateUtil.isNotEmpty(detailList)) {
            for(OutOfStockOutListData data: detailList) {
                GaiaSdReplenishHistoryInData historyInData = new GaiaSdReplenishHistoryInData();
                historyInData.setClient(client);
                historyInData.setGsrhhBrId(brId);
                historyInData.setGsrhhProId(data.getProductId());
                historyInData.setGsrhhDate(CommonUtil.getyyyyMMdd());
                historyInData.setGsrhhAvgQty(data.getAvgQty());
                historyInData.setGsrhhAvgAmt(data.getAvgAmt());
                historyInData.setGsrhhAvgProfitAmt(data.getAvgProfitAmt());
                historyList.add(historyInData);
            }
        }
        gaiaSdReplenishHistoryMapper.batchInsert(historyList);
    }


    /**
     * 补货记录历史查询
     * @param inData
     * @return
     */
    @Override
    public JsonResult getOutOfStockHistory(OutOfStockInData inData) {
        List<OutOfStockOutListData> outOfStockHistoryList = gaiaSdReplenishHistoryMapper.getOutOfStockHistoryList(inData);

        //返回集合
        List<OutOfStockHistoryOutData> historyList = new ArrayList<>();
        if(ValidateUtil.isNotEmpty(outOfStockHistoryList)) {
            //根据日期分组并且汇总的销售额
            Map<String, BigDecimal> totalAmtMap = outOfStockHistoryList.stream().collect(Collectors.groupingBy(OutOfStockOutListData :: getReplenishDate,
                    CollectorsUtil.summingBigDecimal(OutOfStockOutListData :: getAvgAmt)));
            //根据日期分组并且汇总的毛利额
            Map<String, BigDecimal> totalProfitAmtMap = outOfStockHistoryList.stream().collect(Collectors.groupingBy(OutOfStockOutListData :: getReplenishDate,
                    CollectorsUtil.summingBigDecimal(OutOfStockOutListData :: getAvgProfitAmt)));

            Set<String> dateSet = totalAmtMap.keySet();
            for(String date : dateSet) {
                //根据日期将明细列表进行分组 将相同日期组装到同一个list中
                List<OutOfStockOutListData> detailList = outOfStockHistoryList.stream().filter(item -> date.equals(item.getReplenishDate()))
                        .collect(Collectors.toList());
                //获取对应日期的总月均销售额
                BigDecimal totalAmt = totalAmtMap.get(date).setScale(0, BigDecimal.ROUND_HALF_UP);
                //获取对应日期的总月均毛利额
                BigDecimal totalProfit = totalProfitAmtMap.get(date).setScale(0, BigDecimal.ROUND_HALF_UP);
                //获取对应日期补货品数量
                int totalCount = detailList.size();
                //组装返回对象
                OutOfStockHistoryOutData history = new OutOfStockHistoryOutData();
                history.setDetailList(detailList);
                history.setReplenishDate(date);
                history.setTotalAvgAmt(totalAmt);
                history.setTotalAvgProfitAmt(totalProfit);
                history.setTotalCount(totalCount);
                historyList.add(history);
            }
        }
        return JsonResult.success(historyList, "success");
    }


    /**
     * 淘汰商品
     * @param productIdList
     * @param userInfo
     * @return
     */
    @Override
    @Transactional
    public JsonResult invalidOutOfStock(InvalidStockInData inData) {
        //获取参数
        String clientId = inData.getClientId();
        String brId = inData.getBrId();
        List<String> productIdList = inData.getProductIdList();
        if (ValidateUtil.isEmpty(productIdList)) {
            throw new BusinessException("提示：请至少选择一项淘汰品！");
        }

        for (String productId : productIdList) {
            GaiaProductBusiness primaryKey = new GaiaProductBusiness();
            primaryKey.setClient(clientId);
            primaryKey.setProSite(brId);
            primaryKey.setProSelfCode(productId);
            //查询商品是否存在
            GaiaProductBusiness productBusiness = productBusinessMapper.selectByPrimaryKey(primaryKey);
            if (ValidateUtil.isEmpty(productBusiness)) {
                throw new BusinessException("提示：当前产品编号不存在,请重新选择！");
            }
            //变更前值
            String proChangeFrom = productBusiness.getProPosition();

            //更新为淘汰状态
            InvalidOutOfStockInData invalidInData = new InvalidOutOfStockInData();
            invalidInData.setClientId(clientId);
            invalidInData.setBrId(brId);
            invalidInData.setProductId(productId);
            invalidInData.setPosition(CommonConstant.PRO_POSITION_T);
            productBusinessMapper.updateByProductId(invalidInData);

            //插入变更信息表
            GaiaProductChangeInData productChangeInData = new GaiaProductChangeInData();
            productChangeInData.setClient(clientId);
            productChangeInData.setProSite(brId);
            productChangeInData.setProSelfCode(productId);
            productChangeInData.setProCommonname(productBusiness.getProCommonname());    //通用名
            productChangeInData.setProSpecs(productBusiness.getProSpecs());              //规格
            productChangeInData.setProFactoryName(productBusiness.getProFactoryName());  //生产厂家
            productChangeInData.setProRegisterNo(productBusiness.getProRegisterNo());    //批准文号
            productChangeInData.setProChangeField(CommonConstant.PRO_POSITION_COLUMN_NAME);  //变更字段
            productChangeInData.setProChangeFrom(proChangeFrom);                         //变更前值
            productChangeInData.setProChangeTo(CommonConstant.PRO_POSITION_T);           //变更后值
            productChangeInData.setProChangeUser(inData.getUserId());                  //变更人
            productChangeInData.setProChangeDate(CommonUtil.getyyyyMMdd());              //变更日期
            productChangeInData.setProChangeTime(CommonUtil.getHHmmss());                //变更时间
            productChangeInData.setProSfsp(CommonConstant.PRO_SFSP_1);                   //是否审批结束
            productChangeInData.setProSfty(CommonConstant.PRO_SFTY_1);                   //审批状态
            productChangeInData.setProRemarks("淘汰商品");                                 //备注
            gaiaProductChangeMapper.insert(productChangeInData);
        }
        return JsonResult.success("", "success");
    }

    @Override
    public HashMap<String, Object> listDifferentReplenish(GetReplenishInData inData, GetLoginOutData userInfo) {
        //返回结果
        HashMap<String,Object> result = new HashMap<>();

        //查询出的门店差异数据
        List<HashMap<String, Object>> differentReplenish = gaiaSdReplenishComparedDMapper.listDifferentReplenish(inData);

        //统计数据
        HashMap<String,BigDecimal> statistics = new HashMap<>();
        //新增数量汇总
        BigDecimal addTotal = BigDecimal.ZERO;
        //删除数量汇总
        BigDecimal deleteTotal = BigDecimal.ZERO;
        //修改数量汇总
        BigDecimal modifyTotal = BigDecimal.ZERO;
        //店铺去重计数
        Set<String> stoSet = new HashSet<>();
        //原单品项数汇总
        BigDecimal orginalItemsTotal = BigDecimal.ZERO;
        //创建日期去重计数
        Set<String> createDateSet = new HashSet<>();
        for (HashMap different : differentReplenish) {
            BigDecimal add = new BigDecimal(different.get("add").toString());
            BigDecimal delete = new BigDecimal(different.get("delete").toString());
            BigDecimal modify = new BigDecimal(different.get("modify").toString());
            BigDecimal orginalItems = new BigDecimal(different.get("orginalItems").toString());
            addTotal = addTotal.add(add);
            deleteTotal = deleteTotal.add(delete);
            modifyTotal = modifyTotal.add(modify);
            orginalItemsTotal = orginalItemsTotal.add(orginalItems);
            //利用set进行店铺去重
            stoSet.add((String) different.get("stoCode"));
            //利用set进行创建日期去重
            createDateSet.add((String) different.get("checkDate"));
        }
        statistics.put("add",addTotal);
        statistics.put("delete",deleteTotal);
        statistics.put("modify",modifyTotal);
        statistics.put("orginalItems", orginalItemsTotal);
        statistics.put("stoCode", BigDecimal.valueOf(stoSet.size()));
        statistics.put("creDate", BigDecimal.valueOf(createDateSet.size()));
        statistics.put("voucherId", BigDecimal.valueOf(differentReplenish.size()));

        result.put("DifferentReplenishData",differentReplenish);
        result.put("statistics",statistics);
        return result;
    }

    @Override
    public HashMap<String, Object> listDifferentReplenishDetail(GetReplenishInData inData, GetLoginOutData userInfo) {
        HashMap<String, Object> res = gaiaSdReplenishComparedDMapper.getDifferentReplenish(inData);
        List<HashMap<String, Object>> hashMaps = gaiaSdReplenishComparedDMapper.listDifferentReplenishDetail(inData);
        //商品编码计数
        Set<String> proIdSet = new HashSet<>();
        //建议补货量汇总
        BigDecimal proposeQtyTotal = BigDecimal.ZERO;
        //补货量汇总
        BigDecimal needQtyTotal = BigDecimal.ZERO;
        //差异量汇总
        BigDecimal diffQtyTotal = BigDecimal.ZERO;
        //门店库存汇总
        BigDecimal storeQtyTotal = BigDecimal.ZERO;
        for (HashMap<String, Object> hashMap : hashMaps) {
            if(new BigDecimal(hashMap.get("needQty").toString()).compareTo(BigDecimal.ZERO) == 0){
                hashMap.put("status","删除");
            }
            BigDecimal diffQty = new BigDecimal(hashMap.get("diffQty").toString());
            BigDecimal proposeQty = new BigDecimal(hashMap.get("proposeQty").toString());
            BigDecimal needQty = new BigDecimal(hashMap.get("needQty").toString());
            BigDecimal storeQty = new BigDecimal(ObjectUtil.isNotEmpty(hashMap.get("storeQty").toString()) ? hashMap.get("storeQty").toString() : "0");

            diffQtyTotal = diffQtyTotal.add(diffQty);
            proposeQtyTotal = proposeQtyTotal.add(proposeQty);
            needQtyTotal = needQtyTotal.add(needQty);
            storeQtyTotal = storeQtyTotal.add(storeQty);
            //利用set进行商品编码去重
            proIdSet.add((String) hashMap.get("proId"));
        }
        //统计数据
        HashMap<String,BigDecimal> statistics = new HashMap<>();
        statistics.put("proId", BigDecimal.valueOf(proIdSet.size()));
        statistics.put("diffQty",diffQtyTotal);
        statistics.put("proposeQty",proposeQtyTotal);
        statistics.put("needQty",needQtyTotal);
        statistics.put("storeQty",storeQtyTotal);

        res.put("details", hashMaps);
        res.put("statistics", statistics);
        return res;
    }

    @Override
    public HashMap<String, Object> listOriReplenishDetail(GetReplenishInData inData, GetLoginOutData userInfo) {
        HashMap<String, Object> resp = gaiaSdReplenishComparedDMapper.getDifferentReplenish(inData);
        List<HashMap<String, Object>> resList = gaiaSdReplenishComparedDMapper.listOriReplenishDetail(inData);
        for (Map<String, Object> res : resList) {
            if (new BigDecimal(res.get("matTotalQty").toString()).compareTo(BigDecimal.ZERO) == -1) {
                BigDecimal voucherAmt = ((new BigDecimal(res.get("matTotalQty").toString()).add(new BigDecimal(res.get("matRateAmt").toString())))).divide(new BigDecimal(res.get("matTotalQty").toString()), 4, BigDecimal.ROUND_HALF_UP);
                res.put("voucherAmt", voucherAmt);
            } else {
                if (ObjectUtil.isEmpty(res.get("matMovPrice"))) {
                    res.put("matMovPrice", "0");
                }
                res.put("voucherAmt", res.get("matMovPrice"));
            }
            BigDecimal a = BigDecimal.ZERO;
            BigDecimal b = BigDecimal.ZERO;
            if (new BigDecimal(res.get("matTotalQty").toString()).compareTo(BigDecimal.ZERO) == -1) {
                a = new BigDecimal(res.get("voucherAmt").toString());
            } else {
                String num = res.get("gsrdInputTaxValue").toString().replace("%", "");
                BigDecimal rate = new BigDecimal(num).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP);
                a = new BigDecimal(res.get("matMovPrice").toString()).multiply(new BigDecimal("1").add(rate)).setScale(4, BigDecimal.ROUND_HALF_UP);
            }
            if (ObjectUtil.isEmpty(res.get("apAddAmt"))) {
                if (ObjectUtil.isEmpty(res.get("apAddRate"))) {
                    if (ObjectUtil.isEmpty(res.get("addAmt"))) {
                        if (ObjectUtil.isNotEmpty(res.get("addRate"))) {
                            b = a.multiply(new BigDecimal("1").add(new BigDecimal(res.get("addRate").toString()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                        }
                    } else {
                        b = a.add(new BigDecimal(res.get("addAmt").toString()));
                    }
                } else {
                    b = a.multiply(new BigDecimal("1").add(new BigDecimal(res.get("apAddRate").toString()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                }
            } else {
                b = a.add(new BigDecimal(res.get("apAddAmt").toString()));
            }
            res.put("checkOutAmt", b.toString());
        }
        resp.put("details", resList);
        return resp;
    }

    public List<GetReplenishDetailOutData> addSupInfo(List<GetReplenishDetailOutData> detail, GetReplenishInData inData) {
        List<SupInfo> lastSupList = replenishDMapper.nearDateSupList(inData);
        List<SupInfo> minPriceSupList = replenishDMapper.minPriceSupList(inData);
        for (GetReplenishDetailOutData a : detail) {
            //String gsrdBrId = a.getGsrdBrId();
            String proCode = a.getGsrdProId();
            for (SupInfo info : lastSupList) {
                String proCode1 = info.getProCode();
                if (proCode.equals(proCode1)) {
                    String supName = info.getSupName();
                    String supCode = info.getSupCode();
                    String poPrice = info.getPoPrice();
                    a.setGsrdLastPrice(poPrice);
                    a.setGsrdLastSupid(supCode);
                    a.setGsrdLastSupName(supName);
                    a.setGsrdSupid(supCode);
                    a.setGsrdSupName(supName);
                    a.setPoPrice(poPrice);
                }
            }
            for (SupInfo info : minPriceSupList) {
                String proCode1 = info.getProCode();
                if (proCode.equals(proCode1)) {
                    String supName = info.getSupName();
                    String supCode = info.getSupCode();
                    String poPrice = info.getPoPrice();
                    a.setGsrdMinPrice(poPrice);
                    a.setGsrdMinSupid(supCode);
                    a.setGsrdMinSupName(supName);
                }
            }
        }
        return detail;
    }


    @Override
    public JsonResult getStoreStockReport(StoreStockReportInData inData) {
        //查询库存列表
        List<StoreStockReportOutData> storeStockReportList = gaiaMaterialAssessMapper.getStoreStockReport(inData);

        //返回参数
        Map<String, Object> result = new HashMap<>(16);
        //组装列表数据
        List<StoreStockReportOutData> resultList = new ArrayList<>();
        if(ValidateUtil.isNotEmpty(storeStockReportList)) {
            //计算列表中的每个周转天数
            for(StoreStockReportOutData storeStockReport : storeStockReportList) {
                //计算周转天数
                BigDecimal costAmt = storeStockReport.getCostAmt();
                BigDecimal saleAmt = gaiaSdSaleDMapper.getSaleAmtByDateRange(inData.getClientId(), inData.getBrId(), storeStockReport.getBigTypeCode());
                if(ValidateUtil.isNotEmpty(costAmt) && ValidateUtil.isNotEmpty(saleAmt)) {
                    if(saleAmt.longValue() != 0) {
                        BigDecimal turnoverDays = costAmt.divide(saleAmt, 5, BigDecimal.ROUND_HALF_UP)
                                .multiply(new BigDecimal(30)).setScale(0, BigDecimal.ROUND_HALF_UP);
                        storeStockReport.setTurnoverDays(turnoverDays);
                    }
                }
            }
            //计算合计数据
            BigDecimal totalStockCount = storeStockReportList.stream().map(StoreStockReportOutData :: getStockCount).reduce(BigDecimal.ZERO, BigDecimal :: add);
            BigDecimal totalCostAmt = storeStockReportList.stream().map(StoreStockReportOutData :: getCostAmt).reduce(BigDecimal.ZERO, BigDecimal :: add);
            //查询30天内销售额
            BigDecimal saleAmt = gaiaSdSaleDMapper.getSaleAmtByDateRange(inData.getClientId(), inData.getBrId(), null);

            BigDecimal totalTurnoverDays = null;
            if(ValidateUtil.isNotEmpty(saleAmt) && 0 != BigDecimal.ZERO.compareTo(saleAmt)) {
                //计算合计周转天数
                totalTurnoverDays = totalCostAmt.divide(saleAmt, 5, BigDecimal.ROUND_HALF_UP)
                        .multiply(new BigDecimal(30)).setScale(0, BigDecimal.ROUND_HALF_UP);
            }

            //手动不保留小数
            for(StoreStockReportOutData temp : storeStockReportList) {
                temp.setCostAmt(temp.getCostAmt().setScale(0, BigDecimal.ROUND_HALF_UP));
            }
            //合计数据
            StoreStockReportOutData totalStoreStock = new StoreStockReportOutData();
            totalStoreStock.setBigType("合计");
            totalStoreStock.setStockCount(totalStockCount);
            totalStoreStock.setCostAmt(totalCostAmt.setScale(0 , BigDecimal.ROUND_HALF_UP));
            totalStoreStock.setTurnoverDays(totalTurnoverDays);
            resultList.add(totalStoreStock);
            resultList.addAll(storeStockReportList);

            //获取库存品项数图表数据
            Map<String, BigDecimal> stockCountChartInfo = getStockCountChartInfo(storeStockReportList);
            //获取库存成本额图表数据
            Map<String, BigDecimal> costAmtChartInfo = getCostAmtChartInfo(storeStockReportList, totalCostAmt);
            //获取周转天数图表数据
            Map<String, BigDecimal> turnoverDaysChartInfo = getTurnoverDaysChartInfo(storeStockReportList, inData);
            result.put("stockCountChart", stockCountChartInfo);
            result.put("costAmtChart", costAmtChartInfo);
            result.put("turnoverDaysChart", turnoverDaysChartInfo);
        }

        result.put("list", resultList);
        return JsonResult.success(result, "success");
    }


    /**
     * 获取库存品项数图表数据
     * @return
     */
    private Map<String, BigDecimal> getStockCountChartInfo(List<StoreStockReportOutData> storeStockReportList) {
        Map<String, BigDecimal> resultMap = new HashMap<>(3);
        BigDecimal totalNonDrug = BigDecimal.ZERO;
        for(StoreStockReportOutData storeStock : storeStockReportList) {
            if (ValidateUtil.isNotEmpty(storeStock)) {
                String bigType = storeStock.getBigType();
                BigDecimal stockCount = storeStock.getStockCount();
                switch (bigType) {
                    case "A药品":
                        resultMap.put("stockCountA", stockCount);
                        break;
                    case "B中药":
                        resultMap.put("stockCountB", stockCount);
                        break;
                    default:
                        //将库存品项数汇总（不包括成分大类A药品 以及 B中药）
                        totalNonDrug = totalNonDrug.add(stockCount);
                        break;
                }
            }
        }
        resultMap.put("nonDrug", totalNonDrug);
        return resultMap;
    }


    /**
     * 获取库存成本额图表数据
     * @param storeStockReportList
     * @return
     */
    private Map<String, BigDecimal> getCostAmtChartInfo(List<StoreStockReportOutData> storeStockReportList, BigDecimal totalCostAmt) {
        Map<String, BigDecimal> resultMap = new HashMap<>(3);
        BigDecimal totalNonDrug = BigDecimal.ZERO;
        BigDecimal bigDecimal = new BigDecimal(100);
        for(StoreStockReportOutData storeStock : storeStockReportList) {
            if(ValidateUtil.isNotEmpty(storeStock)) {
                String bigType = storeStock.getBigType();
                BigDecimal costAmt = storeStock.getCostAmt();
                switch (bigType) {
                    case "A药品":
                        if(ValidateUtil.isNotEmpty(totalCostAmt) && 0 != BigDecimal.ZERO.compareTo(totalCostAmt)) {
                            BigDecimal costAmtA = costAmt.divide(totalCostAmt, 5, BigDecimal.ROUND_HALF_UP).multiply(bigDecimal)
                                    .setScale(2, BigDecimal.ROUND_HALF_UP);
                            resultMap.put("costAmtA", costAmtA);
                        } else {
                            resultMap.put("costAmtA", BigDecimal.ZERO);
                        }
                        break;
                    case "B中药":
                        if(ValidateUtil.isNotEmpty(totalCostAmt) && 0 != BigDecimal.ZERO.compareTo(totalCostAmt)) {
                            BigDecimal costAmtB = costAmt.divide(totalCostAmt, 5, BigDecimal.ROUND_HALF_UP).multiply(bigDecimal)
                                    .setScale(2, BigDecimal.ROUND_HALF_UP);
                            resultMap.put("costAmtB", costAmtB);
                        } else {
                            resultMap.put("costAmtB", BigDecimal.ZERO);
                        }
                        break;
                    default:
                        //将销售成本额汇总（不包括成分大类A药品 以及 B中药）
                        totalNonDrug = totalNonDrug.add(costAmt);
                        break;
                }
            }
        }
        if(ValidateUtil.isNotEmpty(totalCostAmt) && 0 != BigDecimal.ZERO.compareTo(totalCostAmt)) {
            BigDecimal nonDrugProp = totalNonDrug.divide(totalCostAmt, 5, BigDecimal.ROUND_HALF_UP).multiply(bigDecimal)
                    .setScale(2, BigDecimal.ROUND_HALF_UP);;
            resultMap.put("nonDrug", nonDrugProp);
        } else {
            resultMap.put("nonDrug", BigDecimal.ZERO);
        }
        return resultMap;
    }


    /**
     * 获取周转天数图表数据
     * @param storeStockReportList
     * @return
     */
    private Map<String, BigDecimal> getTurnoverDaysChartInfo(List<StoreStockReportOutData> storeStockReportList, StoreStockReportInData inData) {
        Map<String, BigDecimal> resultMap = new HashMap<>(3);
        //非药品的库存成本额
        BigDecimal totalNonDrug = new BigDecimal("0");

        for(StoreStockReportOutData storeStock : storeStockReportList) {
            String bigType = storeStock.getBigType();
            BigDecimal turnoverDays = storeStock.getTurnoverDays();
            switch (bigType) {
                case "A药品":
                    resultMap.put("turnoverDaysA", turnoverDays);
                    break;
                case "B中药":
                    resultMap.put("turnoverDaysB", turnoverDays);
                    break;
                default:
                    //将销售成本额汇总（不包括成分大类A药品 以及 B中药）
                    totalNonDrug = totalNonDrug.add(storeStock.getCostAmt());
                    break;
            }
        }
        //查询除去药品成分分类 A 以及 B商品的销售成本额
        BigDecimal saleAmtByDateRange = gaiaSdSaleDMapper.getSaleAmtByDateRange(inData.getClientId(), inData.getBrId(), "other");
        if(ValidateUtil.isEmpty(saleAmtByDateRange) || ValidateUtil.isEmpty(totalNonDrug)) {
            resultMap.put("nonDrug", null);
            return resultMap;
        }
        if(0 != BigDecimal.ZERO.compareTo(saleAmtByDateRange)) {
            BigDecimal nonDrugTurnoverDays = totalNonDrug.divide(saleAmtByDateRange, 5, BigDecimal.ROUND_HALF_UP)
                    .multiply(new BigDecimal(30)).setScale(0, BigDecimal.ROUND_HALF_UP);
            resultMap.put("nonDrug", nonDrugTurnoverDays);
        } else {
            resultMap.put("nonDrug", null);
        }
        return resultMap;
    }


    @Override
    public JsonResult getValidProductInfo(ValidProductInData inData) {
        //查询配置天数
        GaiaSdSystemPara systemPara = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClientId(), inData.getBrId(), "EXPIRYDATE_WARNING_STO");
        if(ValidateUtil.isEmpty(systemPara)) {
            inData.setConfigDays(CommonConstant.VALID_PRODUCT_180);
        } else {
            inData.setConfigDays(systemPara.getGsspPara());
        }
        //查询效期商品界面表头数据
        ValidProductOutData validProductInfo = gaiaSdStockBatchMapper.getValidProductInfo(inData);

        //查询明细列表
        List<ValidProductListOutData> validProductList = gaiaSdStockBatchMapper.getValidProductList(inData);
        if(ValidateUtil.isNotEmpty(validProductList)) {
            validProductInfo.setValidProductList(validProductList);
        }
        return JsonResult.success(validProductInfo, "success");
    }

    @Override
    public Result exportData(GetReplenishInData inData, GetLoginOutData userInfo) {
        //查询出的门店差异数据
        List<HashMap<String, Object>> differentReplenish = gaiaSdReplenishComparedDMapper.listDifferentReplenish(inData);

        //组装导出内容
        List<List<Object>> dataList = new ArrayList<>();
        for(HashMap<String,Object> different : differentReplenish) {
            //计算excel多少行数据
            inData.setGsrhVoucherId((String) different.get("voucherId"));
            //调用查询接口 查询数据
            List<HashMap<String, Object>> hashMaps = gaiaSdReplenishComparedDMapper.listDifferentReplenishDetail(inData);
            if (hashMaps.size() > 0){
                Integer index = 1;
                for (HashMap<String, Object> hashMap : hashMaps) {
                    List<Object> line = new ArrayList<>();
                    //序号
                    line.add(index);
                    index ++;
                    //门店编号
                    line.add(different.get("stoCode"));
                    //门店名称
                    line.add(different.get("stoName"));
                    //创建日期
                    line.add(different.get("creDate"));
                    //创建时间
                    line.add(different.get("creTime"));
                    //创建人
                    line.add(different.get("creName"));
                    //审核日期
                    line.add(different.get("checkDate"));
                    //审核时间
                    line.add(different.get("checkTime"));
                    //审核人
                    line.add(different.get("checkName"));
                    //补货单号
                    line.add(different.get("voucherId"));
                    //商品编码
                    line.add(hashMap.get("proId"));
                    //商品名称
                    line.add(hashMap.get("proName"));
                    //规格
                    line.add(hashMap.get("proSpecs"));
                    //单位
                    line.add(hashMap.get("unit"));
                    //厂家
                    line.add(hashMap.get("factory"));
                    //零售价
                    line.add(hashMap.get("proPrice"));
                    //建议补货数量
                    line.add(hashMap.get("proposeQty"));
                    //补货量
                    line.add(hashMap.get("needQty"));
                    //差异量
                    line.add(hashMap.get("diffQty"));
                    //状态
                    line.add(hashMap.get("status"));

                    dataList.add(line);
                }
            }
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.PRODUCT_REPLENISHMENT_DIFFERENCES_EXPORT_EXCEL_HEAD);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add(CommonConstant.PRODUCT_REPLENISHMENT_DIFFERENCES_EXPORT_SHEET_NAME);
                }});
        Result uploadResult = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.PRODUCT_REPLENISHMENT_DIFFERENCES_EXPORT_SHEET_NAME + "-" + CommonUtil.getyyyyMMdd() + ".xls";
            uploadResult = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}",e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}",e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;
    }

    @Override
    public JsonResult cancelReplenish(GetReplenishInData inData) {
        String clientId = inData.getClientId();
        String stoCode = inData.getStoCode();
        String voucherId = inData.getGsrhVoucherId();
        if(ValidateUtil.isEmpty(voucherId)) {
            throw new BusinessException("提示：单号不能为空");
        }

        GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
        replenishH.setClientId(clientId);
        replenishH.setGsrhBrId(stoCode);
        replenishH.setGsrhVoucherId(voucherId);
        //查询单号是否存在
        GaiaSdReplenishH unique = replenishHMapper.getUnique(replenishH);
        if(ValidateUtil.isEmpty(unique)) {
            throw new BusinessException("提示：该单号不存在, 请确认");
        }

        //作废状态
        replenishH.setGsrhDate(unique.getGsrhDate());
        replenishH.setGsrhStatus("2");
        replenishHMapper.updateByPrimaryKeySelective(replenishH);
        return JsonResult.success("", "提示：作废成功！");
    }

    /**
     * 读取推送消息
     * @param inData
     * @param type
     */
    private void readMsg(OutOfStockInData inData) {
        GaiaSdOutOfStockPushRecord outOfStockPushRecord = new GaiaSdOutOfStockPushRecord();
        outOfStockPushRecord.setClient(inData.getClientId());
        outOfStockPushRecord.setBrId(inData.getBrId());
        outOfStockPushRecord.setType(1);
        List<GaiaSdOutOfStockPushRecord> outOfStockPushRecordList = outOfStockPushRecordMapper.getListByCondition(outOfStockPushRecord);
        if(ValidateUtil.isEmpty(outOfStockPushRecordList)) {
            return;
        }
        //获取推送时间最新的数据
        GaiaSdOutOfStockPushRecord minOutOfStockPushRecord = outOfStockPushRecordList.get(0);

        //更新已读状态
        GaiaSdOutOfStockPushRecord outOfStockPushRecordUpdate = new GaiaSdOutOfStockPushRecord();
        outOfStockPushRecordUpdate.setId(minOutOfStockPushRecord.getId());
        outOfStockPushRecordUpdate.setStatus(1);
        outOfStockPushRecordUpdate.setReadTime(new Date());
        outOfStockPushRecordUpdate.setReadEmp(inData.getUserId());
        outOfStockPushRecordMapper.updateByPrimaryKeySelective(outOfStockPushRecordUpdate);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object replenishDetailForColdChain(GetReplenishInData inData) {
        StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(), inData.getStoCode());
        if (storeOutData != null) {
            if (ObjectUtil.isNotEmpty(storeOutData.getStoDcCode())) {
                inData.setDcCode(storeOutData.getStoDcCode());
            }
        }
        GetReplenishDetailOutData detailOutData = productBasicMapper.queryProFromReplenishDetail(inData);
        if (ObjectUtil.isNotEmpty(detailOutData)) {
            inData.setParamCode("REPLENISH_MINQTY");
            GetReplenishOutData sales = this.getSales(inData);
            GetReplenishOutData stocks = this.getStock(inData);
            GetReplenishOutData costs = this.getCost(inData);
            GetReplenishOutData param = this.getParam(inData);
            if (ObjectUtil.isNull(sales)) {
                detailOutData.setGsrdSalesDays1("0");
                detailOutData.setGsrdSalesDays2("0");
                detailOutData.setGsrdSalesDays3("0");
            } else {
                detailOutData.setGsrdSalesDays1(sales.getSalesCountOne());
                detailOutData.setGsrdSalesDays2(sales.getSalesCountTwo());
                detailOutData.setGsrdSalesDays3(sales.getSalesCountThree());
            }

            detailOutData.setGsrdStockStore(stocks.getStock());
            if (ObjectUtil.isNotNull(costs)) {
                detailOutData.setGsrdAverageCost(costs.getAverageCost());
                detailOutData.setGsrdGrossMargin(costs.getBatchCost());
            }
            detailOutData.setGsrdDisplayMin(param.getParam());
            // 建议补货量为0的不显示
//            if (ObjectUtil.isNotNull(counts)) {
//                detailOutData.setGsrdNeedQty(counts.getGsrhTotalQty());
//                detailOutData.setGsrdProposeQty(counts.getGsrhTotalQty());
//            } else {
            detailOutData.setGsrdNeedQty("0");
            detailOutData.setGsrdProposeQty("0");
//            }
            if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1) {
                BigDecimal voucherAmt = ((new BigDecimal(detailOutData.getMatTotalAmt()).add(new BigDecimal(detailOutData.getMatRateAmt())))).divide(new BigDecimal(detailOutData.getMatTotalQty()), 4, BigDecimal.ROUND_HALF_UP);
                detailOutData.setVoucherAmt(voucherAmt.toString());
            } else {
                if (ObjectUtil.isEmpty(detailOutData.getMatMovPrice())) {
                    detailOutData.setMatMovPrice("0");
                }
                detailOutData.setVoucherAmt(detailOutData.getMatMovPrice());
            }
            BigDecimal a = BigDecimal.ZERO;
            BigDecimal b = BigDecimal.ZERO;
            if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1) {
                a = new BigDecimal(detailOutData.getVoucherAmt());
            } else {
                String num = detailOutData.getGsrdInputTaxValue().replace("%", "");
                BigDecimal rate = new BigDecimal(num).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP);
                a = new BigDecimal(detailOutData.getMatMovPrice()).multiply(new BigDecimal("1").add(rate)).setScale(4, BigDecimal.ROUND_HALF_UP);
            }
            if (ObjectUtil.isNotEmpty(detailOutData.getApAddAmt())) {
                b = a.add(new BigDecimal(detailOutData.getApAddAmt()));
            } else if (ObjectUtil.isNotEmpty(detailOutData.getApAddRate()) && ObjectUtil.isEmpty(detailOutData.getApAddAmt())) {
                if (ObjectUtil.isEmpty(detailOutData.getApAddRate())) {
                    if (ObjectUtil.isEmpty(detailOutData.getAddAmt())) {
                        if (ObjectUtil.isNotEmpty(detailOutData.getAddRate())) {
                            b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                        }
                    } else {
                        b = a.add(new BigDecimal(detailOutData.getAddAmt()));
                    }
                } else {
                    b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                }
            } else if (ObjectUtil.isEmpty(detailOutData.getAddRate())
                    && ObjectUtil.isEmpty(detailOutData.getApAddAmt())
                    && ObjectUtil.isNotEmpty(detailOutData.getApCataloguePrice())) {
                b = new BigDecimal(detailOutData.getApCataloguePrice());
            }
            if (b.compareTo(BigDecimal.ZERO) == 1) {
                detailOutData.setCheckOutAmt(b.toString());
            }
            // 格式化销售量
            detailOutData.setGsrdSalesDays1(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays1(), ".0000"));
            detailOutData.setGsrdSalesDays2(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays2(), ".0000"));
            detailOutData.setGsrdSalesDays3(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays3(), ".0000"));
            detailOutData.setGsrdProposeQty(StrUtil.removeSuffix(detailOutData.getGsrdProposeQty(), ".0000"));
            detailOutData.setGsrdNeedQty(StrUtil.removeSuffix(detailOutData.getGsrdNeedQty(), ".0000"));
            detailOutData.setGsrdStockStore(StrUtil.isEmpty(StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000")) ? "0" : StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000"));
            //委托第三方编码匹配
            //获取商品匹配编码
            List<Map<String, String>> proThreeCodeList = replenishDMapper.selectProThreeCode(inData.getClientId(), detailOutData.getGsrdProId());
            if (CollectionUtils.isNullOrEmpty(proThreeCodeList)) {
                String bigClass = detailOutData.getBigClass().split("-")[0];
                //获取第三方仓库库存
                List<GaiaSdDsfStock> proThreeStockList = replenishDMapper.selectProThreeStock(inData.getClientId(), detailOutData.getGsrdProId());
                //获取特殊商品分类编码
                List<SpecialClass> specialClassList = replenishDMapper.selectSpecialClass(inData.getClientId(), inData.getStoCode(), bigClass);
                //根据补货单号判断新加入商品是否为此集合商品
                GetReplenishOutData replenishOutData = new GetReplenishOutData();
                if (ObjectUtil.isNotEmpty(inData.getGsrhVoucherId())) {
                    replenishOutData = replenishHMapper.selectReplenishOrder(inData.getClientId(), inData.getStoCode(), inData.getGsrhVoucherId());
                }
                if (ObjectUtil.isNotEmpty(replenishOutData)) {
                    if ("1".equals(replenishOutData.getIsSpecial())) {
                        String gsrhAddr = replenishOutData.getGsrhAddr();
                        String clientId = inData.getClientId();
                        if ("3".equals(replenishOutData.getGsrhType())) {//委托配送单
                            SpecialClass specialClass = specialClassList.stream().filter(f -> clientId.equals(f.getClientId()) && "1".equals(f.getStockType()) && f.getStockCode().equals(gsrhAddr) && bigClass.equals(f.getBigClass())).findAny().orElse(null);
                            if (ObjectUtil.isEmpty(specialClass)) {
                                throw new BusinessException("该商品不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                            }
                        } else {//配送中心配送单
                            SpecialClass specialClass = specialClassList.stream().filter(f -> clientId.equals(f.getClientId()) && "0".equals(f.getStockType()) && f.getStockCode().equals(gsrhAddr) && bigClass.equals(f.getBigClass())).findAny().orElse(null);
                            if (ObjectUtil.isEmpty(specialClass)) {
                                throw new BusinessException("该商品不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                            }
                        }
                    } else if ("0".equals(replenishOutData.getIsSpecial())) {
                        if ("1".equals(detailOutData.getIsSpecial())) {
                            throw new BusinessException("该商品不属于此单补货商品分类，请至其他商品分类的补货单中增加！");
                        }
                    }
                }
                for (SpecialClass specialClass : specialClassList) {
                    if ("1".equals(specialClass.getStockType())) {
                        if (specialClass.getBigClass().equals(detailOutData.getBigClass().split("-")[0])) {
                            //显示第三方商品编码
                            boolean f = true;
                            for (Map<String, String> map : proThreeCodeList) {
                                if (map.get("proCode").equals(detailOutData.getGsrdProId())) {
                                    detailOutData.setProThreeCode(map.get("proThreeCode"));
                                    detailOutData.setHasProThreeCode("1");
                                    f = false;
                                }
                            }
                            if (f) {
                                detailOutData.setHasProThreeCode("0");
                            }
                            BigDecimal stock = BigDecimal.ZERO;
                            if (ObjectUtil.isNotEmpty(detailOutData.getGsrdStockDepot())) {
                                stock = new BigDecimal(detailOutData.getGsrdStockDepot());
                            }
                            //商品仓库库存累加
                            for (GaiaSdDsfStock c : proThreeStockList) {
                                if (c.getProId().equals(detailOutData.getGsrdProId())) {
                                    detailOutData.setGsrdStockDepot(stock.add(c.getQty()).toString());
                                }
                            }
                        }
                    }
                }
            }
            List<Map<String, String>> threeCodeList = replenishDMapper.selectProThreeCode(inData.getClientId(), inData.getProductCode());
            if (!CollectionUtils.isNullOrEmpty(threeCodeList)) {
                detailOutData.setProThreeCode(threeCodeList.get(0).get("proThreeCode"));
            }


        } else {
            throw new BusinessException("该商品无第三方编码，无法进行补货！");
        }
        return detailOutData;
    }

    @Autowired
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object insertReplenishDetailForColdChain(GetReplenishInData inData) {
        String keyName = inData.getClientId() + "-" + inData.getStoCode() + "-" + CommonUtil.getyyyyMMdd() + "-" + inData.getGsrhPattern();
        Boolean re = redisManager.hasKey(keyName);
        if (re){
            redisManager.delete(keyName);
        }
        inData.setGsrhDate(CommonUtil.getyyyyMMdd());
        GaiaClSystemPara systemPara = gaiaClSystemParaMapper.selectByPrimaryKey(inData.getClientId(), "SUPPLIER_CODE");
        String stoCode = "";
        if (ObjectUtil.isNotEmpty(systemPara)) {
            stoCode = systemPara.getGcspPara1();
        }else {
            throw new BusinessException("系统中没有维护当前加盟商的第三方供应商！");
        }
        if (ObjectUtil.isEmpty(inData.getGsrhVoucherId())) {
            //新增
            List<GetReplenishDetailInData> detailList = inData.getDetailList();
            BigDecimal gsrhTotalAmt = BigDecimal.ZERO;
            BigDecimal qty = BigDecimal.ZERO;
            //新增GAIA_SD_REPLENISH_H
            GaiaSdReplenishH loginData = new GaiaSdReplenishH();
            loginData.setClientId(inData.getClientId());
            String voucherId = commonService.selectNextVoucherId(loginData);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            inData.setGsrhVoucherId(voucherId);
            GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
            replenishH.setClientId(inData.getClientId());
            replenishH.setGsrhVoucherId(voucherId);
            replenishH.setGsrhBrId(inData.getStoCode());
            replenishH.setGsrhDate(CommonUtil.getyyyyMMdd());
            replenishH.setGsrhType("2");
            replenishH.setGsrhAddr(stoCode);
            replenishH.setGsrhStatus("0");
            replenishH.setGsrhFlag("0");
            //正常补货
            replenishH.setGsrhPattern("0");
            replenishH.setGsrhCreateTime(CommonUtil.getHHmmss());
            //补货来源  4-国药冷链
            replenishH.setGsrhSource("4");
            //新增GAIA_SD_REPLENISH_D
            saveDetailsForColdChain(inData, replenishH);
            this.replenishHMapper.insert(replenishH);
            return voucherId;
        } else {
            //编辑
            Example example = new Example(GaiaSdReplenishH.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId())
                    .andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId())
                    .andEqualTo("gsrhBrId", inData.getStoCode());
            GaiaSdReplenishH replenishH = replenishHMapper.selectOneByExample(example);
            saveDetailsForColdChain(inData, replenishH);
            this.replenishHMapper.update(replenishH);
            return inData.getVoucherId();
        }
    }

    @Override
    public GetReplenishDetailOutData replenishDetailList(GetReplenishInData inData) {
        String[] gspgProIds = inData.getGspgProIds();
        List<GetReplenishDetailOutData> tableInfoList = inData.getTableInfoList();
        if (ObjectUtil.isEmpty(gspgProIds)) {
            throw new BusinessException("请选择需要新增的药品！");
        }
        List<GetReplenishInData> pickInfoList = new ArrayList<>();
        StringBuilder msg = new StringBuilder();
        msg.append("您输入的商品在本单中已存在，行号为");
        boolean s = false;
        int a = 1;
        for (String gspgProId : gspgProIds) {
            for (int i = 0;i < tableInfoList.size();i++){
                GetReplenishDetailOutData b = tableInfoList.get(i);
                if (gspgProId.equals(b.getGsrdProId())){
                    s = true;
                    msg.append(b.getIndex() + ",");
                    break;
                }
            }
            GetReplenishInData data = new GetReplenishInData();
            data.setProductCode(gspgProId);
            pickInfoList.add(data);
            a ++;
        }
        if (s){
            throw new BusinessException(msg.substring(0,msg.length()-1).toString());
        }
        List<GetReplenishInData> pickInfoSet = pickInfoList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GetReplenishInData :: getProductCode))), ArrayList::new));
        //查询出的选择商品信息集合
        List<GetReplenishDetailOutData> pickResultInfoList = new ArrayList<>();
        //查询选择的消息
        for (GetReplenishInData pickInfo : pickInfoSet) {
            pickInfo.setClientId(inData.getClientId());
            pickInfo.setStoCode(inData.getStoCode());
            pickInfo.setGsrhVoucherId(inData.getGsrhVoucherId());
            GetReplenishDetailOutData detailOutData = this.replenishDetail(pickInfo);
            pickResultInfoList.add(detailOutData);
        }
        GetReplenishDetailOutData result = new GetReplenishDetailOutData();
        //返回页面的桌面集合
        List<GetReplenishDetailOutData> tableResultInfoList = new ArrayList<>();
        //重复集合
        List<GetReplenishDetailOutData> repeatInfoList = new ArrayList<>();
        if (CollectionUtils.isNullOrEmpty(tableInfoList)) {
            //查询新增数据 并返回
            tableResultInfoList.addAll(pickResultInfoList);
            result.setTableInfoList(tableResultInfoList);
        } else {
            List<GetReplenishDetailOutData> collect1 = tableInfoList.stream().filter(indata -> !StringUtils.isNullOrEmpty(indata.getGsrdProId())).collect(Collectors.toList());
            if (CollectionUtils.isNullOrEmpty(collect1)) {
                tableResultInfoList.addAll(pickResultInfoList);
                result.setTableInfoList(tableResultInfoList);
            } else {
                //对页面数据去重
                List<GetReplenishDetailOutData> tableInfoSet1 = collect1.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GetReplenishDetailOutData::getGsrdProId))), ArrayList::new));
                //按照原来页面数据排序
                List<GetReplenishDetailOutData> tableInfoSet = tableInfoSet1.stream().sorted(Comparator.comparing(GetReplenishDetailOutData::getIndex)).collect(Collectors.toList());
                //判断是否重复，并返回重复数据，
                for (GetReplenishDetailOutData pickInfo : pickResultInfoList) {
                    boolean flag = false;
                    for (GetReplenishDetailOutData table : tableInfoSet) {
                        if (table.getGsrdProId().equals(pickInfo.getGsrdProId())) {
                            //有重复
                            repeatInfoList.add(pickInfo);
                            flag = true;
                            continue;
                        }
                    }
                    if (!flag) {
                        tableInfoSet.add(pickInfo);
                    }
                }
                result.setTableInfoList(tableInfoSet);
            }
        }
        result.setRepeatInfoList(repeatInfoList);
        return result;
    }


    @Override
    public List<ReplenishParamOutData> selectReplenishParamList(ReplenishParamInData inData) {
        return systemParaMapper.selectReplenishParamList(inData);
    }

    @Override
    public void batchAddReplenishParam(ReplenishParamInData inData) {
        List<ReplenishParam> list = new ArrayList<>();
        if(ObjectUtil.isNotEmpty(inData.getStoCodes()) && inData.getStoCodes().length > 0){
            for(int i = 0;i < inData.getStoCodes().length;i++){
                String stoCode = inData.getStoCodes()[i];
                ReplenishParam param = new ReplenishParam();
                param.setClientId(inData.getClientId());
                param.setStoCode(stoCode);
                param.setFlag1(inData.getFlag1());
                param.setFlag2(inData.getFlag2());
                param.setFlag3(inData.getFlag3());
                param.setFlag4(inData.getFlag4());
                param.setFlag5(inData.getFlag5());
                param.setFlag6(inData.getFlag6());
                param.setFlag7(inData.getFlag7());
                param.setUpdateEmp(inData.getUpdateEmp());
                param.setUpdateDate(CommonUtil.getyyyyMMdd());
                param.setUpdateTime(CommonUtil.getHHmmss());
                list.add(param);
            }
        }else {
            throw new BusinessException("未选择门店");
        }
        if (ObjectUtil.isNotEmpty(list) && list.size() > 0){
            systemParaMapper.batchAddReplenishParam(list);
        }
    }

    @Override
    public List<ReplenishGiftOutParam> selectGiftList(GetReplenishDetailInData inData) {
        List<ReplenishGiftOutParam> result = new ArrayList<>();
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(inData.getGsrdVoucherId())){
            result = replenishGiftMapper.selectGiftByRecord(inData.getClientId(),inData.getGsrdBrId(),inData.getGsrdProId(),inData.getGsrdVoucherId());
        }else {
            StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(),inData.getGsrdBrId());
            if (storeOutData != null){
                if (ObjectUtil.isNotEmpty(storeOutData.getStoDcCode())){
                    inData.setDcCode(storeOutData.getStoDcCode());
//                    //赠品列表查询
//                    giftList = replenishGiftMapper.selectGiftList(inData.getClientId(),storeOutData.getStoDcCode(),"","");
                }
            }
            result = replenishGiftMapper.selectGiftBySet(inData.getClientId(),inData.getDcCode(),inData.getGsrdProId());
            for (ReplenishGiftOutParam gift : result) {
                BigDecimal qty = new BigDecimal(inData.getGsrdNeedQty()).divide(gift.getProQty().divide(gift.getGiftQty(),4,BigDecimal.ROUND_HALF_UP),0,BigDecimal.ROUND_DOWN);
                gift.setNeedQty(qty.toString());
            }
        }
        return result;
    }

    public List<GetReplenishDetailOutData> giftList(GetReplenishDetailInData inData,GaiaSdReplenishH replenishH){
        List<GetReplenishDetailOutData> resultList = new ArrayList<>();
        List<ReplenishGiftOutParam> giftList = new ArrayList<>();
        StoreOutData storeOutData = storeDataMapper.getStoreData(replenishH.getClientId(),replenishH.getGsrhBrId());
        if (storeOutData != null){
            if (ObjectUtil.isNotEmpty(storeOutData.getStoDcCode())){
                inData.setDcCode(storeOutData.getStoDcCode());
                giftList = replenishGiftMapper.selectGiftDetailList(replenishH.getClientId(),storeOutData.getStoDcCode(),inData.getGsrdProId(),"");//查询商品是否有赠品
            }
        }
        List<ReplenishGift> giftRecordList = new ArrayList();
        if (ObjectUtil.isNotEmpty(giftList) && giftList.size() > 0) {
            for (int i = 0; i < giftList.size(); i++) {
                ReplenishGift giftRecord = new ReplenishGift();
                giftRecord.setClientId(replenishH.getClientId());
                giftRecord.setStoCode(replenishH.getGsrhBrId());
                giftRecord.setGsrgDproId(inData.getGsrdProId());//带赠品商品编码
                ReplenishGiftOutParam gift = giftList.get(i);
                giftRecord.setGsrgProId(gift.getGiftCode());//赠品编码
                giftRecord.setGsrgVoucherId(replenishH.getGsrhVoucherId());
                GetReplenishInData data = new GetReplenishInData();
                data.setDcCode(inData.getDcCode());
                data.setClientId(replenishH.getClientId());
                data.setStoCode(replenishH.getGsrhBrId());
                data.setProductCode(gift.getGiftCode());
                GetReplenishDetailOutData detailOutData = productBasicMapper.queryProFromReplenishDetail(data);
                if (ObjectUtil.isNotEmpty(detailOutData)) {
                    //非原始单数据
                    data.setParamCode("REPLENISH_MINQTY");
                    GetReplenishOutData sales = this.getSales(data);
                    GetReplenishOutData stocks = this.getStock(data);
                    GetReplenishOutData costs = this.getCost(data);
                    GetReplenishOutData param = this.getParam(data);
                    if (ObjectUtil.isNull(sales)) {
                        detailOutData.setGsrdSalesDays1("0");
                        detailOutData.setGsrdSalesDays2("0");
                        detailOutData.setGsrdSalesDays3("0");
                    } else {
                        detailOutData.setGsrdSalesDays1(sales.getSalesCountOne());
                        detailOutData.setGsrdSalesDays2(sales.getSalesCountTwo());
                        detailOutData.setGsrdSalesDays3(sales.getSalesCountThree());
                    }

                    detailOutData.setGsrdStockStore(stocks.getStock());
                    if (ObjectUtil.isNotNull(costs)) {
                        detailOutData.setGsrdAverageCost(costs.getAverageCost());
                        detailOutData.setGsrdGrossMargin(costs.getBatchCost());
                    }
                    detailOutData.setGsrdDisplayMin(param.getParam());
                    // 建议补货量为0的不显示
                    //赠品数量计算公式：补货数量/(赠品关联商品数量/赠品数量) 向上取整
                    BigDecimal qty = new BigDecimal(inData.getGsrdNeedQty()).divide(gift.getProQty().divide(gift.getGiftQty(),4,BigDecimal.ROUND_UP),0,BigDecimal.ROUND_DOWN);
                    detailOutData.setGsrdNeedQty(qty.toString());
                    detailOutData.setGsrdProposeQty(qty.toString());
                    giftRecord.setGsrgQty(qty);
                    if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1) {
                        BigDecimal voucherAmt = ((new BigDecimal(detailOutData.getMatTotalAmt()).add(new BigDecimal(detailOutData.getMatRateAmt())))).divide(new BigDecimal(detailOutData.getMatTotalQty()), 4, BigDecimal.ROUND_HALF_UP);
                        detailOutData.setVoucherAmt(voucherAmt.toString());
                    } else {
                        if (ObjectUtil.isEmpty(detailOutData.getMatMovPrice())) {
                            detailOutData.setMatMovPrice("0");
                        }
                        detailOutData.setVoucherAmt(detailOutData.getMatMovPrice());
                    }
                    BigDecimal a = BigDecimal.ZERO;
                    BigDecimal b = BigDecimal.ZERO;
                    if (new BigDecimal(detailOutData.getMatTotalQty()).compareTo(BigDecimal.ZERO) == -1) {
                        a = new BigDecimal(detailOutData.getVoucherAmt());
                    } else {
                        String num = detailOutData.getGsrdInputTaxValue().replace("%", "");
                        BigDecimal rate = new BigDecimal(num).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP);
                        a = new BigDecimal(detailOutData.getMatMovPrice()).multiply(new BigDecimal("1").add(rate)).setScale(4, BigDecimal.ROUND_HALF_UP);
                    }
                    if (ObjectUtil.isNotEmpty(detailOutData.getApAddAmt())) {
                        b = a.add(new BigDecimal(detailOutData.getApAddAmt()));
                    } else if (ObjectUtil.isNotEmpty(detailOutData.getApAddRate()) && ObjectUtil.isEmpty(detailOutData.getApAddAmt())) {
                        if (ObjectUtil.isEmpty(detailOutData.getApAddRate())) {
                            if (ObjectUtil.isEmpty(detailOutData.getAddAmt())) {
                                if (ObjectUtil.isNotEmpty(detailOutData.getAddRate())) {
                                    b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                                }
                            } else {
                                b = a.add(new BigDecimal(detailOutData.getAddAmt()));
                            }
                        } else {
                            b = a.multiply(new BigDecimal("1").add(new BigDecimal(detailOutData.getApAddRate()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
                        }
                    } else if (ObjectUtil.isEmpty(detailOutData.getAddRate())
                            && ObjectUtil.isEmpty(detailOutData.getApAddAmt())
                            && ObjectUtil.isNotEmpty(detailOutData.getApCataloguePrice())) {
                        b = new BigDecimal(detailOutData.getApCataloguePrice());
                    }
                    if (b.compareTo(BigDecimal.ZERO) == 1) {
                        detailOutData.setCheckOutAmt(b.toString());
                    }
                    // 格式化销售量
                    detailOutData.setGsrdSalesDays1(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays1(), ".0000"));
                    detailOutData.setGsrdSalesDays2(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays2(), ".0000"));
                    detailOutData.setGsrdSalesDays3(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays3(), ".0000"));
                    detailOutData.setGsrdProposeQty(StrUtil.removeSuffix(detailOutData.getGsrdProposeQty(), ".0000"));
                    detailOutData.setGsrdNeedQty(StrUtil.removeSuffix(detailOutData.getGsrdNeedQty(), ".0000"));
                    detailOutData.setGsrdStockStore(StrUtil.isEmpty(StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000")) ? "0" : StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000"));
                    giftRecordList.add(giftRecord);
                } else {
                    throw new BusinessException("查询商品不存在");
                }
                resultList.add(detailOutData);
            }
        }
        if (ObjectUtil.isNotEmpty(giftRecordList) && giftRecordList.size() > 0){
            replenishGiftMapper.addGiftDetail(giftRecordList);
        }
        return resultList;
    }

    private String checkCreditQuotaAndZqts(GetReplenishInData inData, BigDecimal titleAmt, boolean flag) {
        //门店补货校验信用金额和账期控制
        StoreOutData storeOutData = storeDataMapper.getStoreData(inData.getClientId(), inData.getStoCode());
        StringBuilder stringBuilder = new StringBuilder();
        if (storeOutData != null) {
            //启用信用管理
            if ("1".equals(storeOutData.getStoCreditFlag()) && null != storeOutData.getStoCreditQuota()) {
                BigDecimal totalAmt = BigDecimal.valueOf(0);
                BigDecimal gsrhTotalAmt = BigDecimal.valueOf(0);
                if (flag) {
                    gsrhTotalAmt = titleAmt;
                } else {
                    if (ObjectUtil.isNotEmpty(inData.getDetailList())) {
                        for (GetReplenishDetailInData data : inData.getDetailList()) {
                            BigDecimal proPrice = BigDecimal.ZERO;
                            if (ObjectUtil.isNotEmpty(data.getGsrdProPrice())) {
                                proPrice = data.getGsrdProPrice();
                            }
                            BigDecimal gsrdNeedQty = BigDecimal.ZERO;
                            if (ObjectUtil.isNotEmpty(data.getGsrdNeedQty())) {
                                gsrdNeedQty = data.getGsrdProPrice();
                            }
                            gsrhTotalAmt = gsrhTotalAmt.add(proPrice.multiply(gsrdNeedQty)).setScale(4, BigDecimal.ROUND_HALF_UP);
                        }
                    }
                }
                BigDecimal stoArAmt = storeOutData.getStoArAmt();
                if(null == stoArAmt){
                    stoArAmt = BigDecimal.ZERO;
                }
                //如果余额+本次汇总配送金额>信用额度
                totalAmt = totalAmt.add(gsrhTotalAmt).add(stoArAmt).setScale(4, BigDecimal.ROUND_HALF_UP);
                if (1 == totalAmt.compareTo(storeOutData.getStoCreditQuota())) {
                    //总授信额度A元，欠款金额B元，剩余额度C元，本单金额D元，已不足！
                    //A：主数据里的授信额度
                    //B：主数据里的应收余额
                    //C：A-B
                    //D：本单金额
                    BigDecimal arrearsAmt = storeOutData.getStoCreditQuota().subtract(stoArAmt).setScale(4, BigDecimal.ROUND_HALF_UP);

                    stringBuilder.append("总授信额度为").append(storeOutData.getStoCreditQuota()).append("元，欠款金额为").append(stoArAmt).append("元，剩余额度为").append(arrearsAmt).append("元，本次金额").append(gsrhTotalAmt).append("元，已不足!");
                    if (flag) {
                        throw new BusinessException(stringBuilder.toString());
                    }

                }
            }
            if ("0".equals(storeOutData.getStoZqlx())) {
                //如果是账期类型，检查当前日期往前推账期天数，如存在往来余额 > 0,报错
                if (null != storeOutData.getStoZqts()) {
                    Date date = DateUtils.addDays(new Date(), -storeOutData.getStoZqts());
                    String queryDate = DateUtil.format(date, "yyyyMMdd");
                    BigDecimal amount = gaiaWmsDiaoboZMapper.getZqAmount(storeOutData.getClient(), storeOutData.getStoCode(), queryDate);
                    stringBuilder.append("账期天数为").append(storeOutData.getStoZqts()).append("天，已有").append(amount).append("元超过账期尚未付款，请付款后再下单。");
                    if (flag && BigDecimal.ZERO.compareTo(amount) < 0) {
                        throw new BusinessException(stringBuilder.toString());
                    }
                }
            } else if ("1".equals(storeOutData.getStoZqlx())) {
                //如果是固定账期，并且欠款标志为0，报错提示
                if ("0".equals(storeOutData.getStoArrearsFlag())) {
                    stringBuilder.append("不允许门店补货。");
                    if (flag) {
                        throw new BusinessException(stringBuilder.toString());
                    }
                }
            }
        }
        return stringBuilder.toString();
    }
}
