package com.gys.business.service;

import com.gys.business.service.data.disease.RelevanceProductInData;
import com.gys.business.service.data.disease.RelevanceProductOutData;
import com.gys.business.service.data.disease.SelectCardTagRequest;
import com.gys.business.service.data.disease.SelectCardTagResponse;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/6 13:41
 * @Version 1.0.0
 **/
public interface DiseaseService {
    List<RelevanceProductOutData> getRelevanceProduct(GetLoginOutData userInfo,RelevanceProductInData inData);

    /**
     * 会员贴标签
     */
    void setMemberLabel(String startDate,String endDate,Boolean delete);

    List<SelectCardTagResponse> getCardTag(SelectCardTagRequest request);
}
