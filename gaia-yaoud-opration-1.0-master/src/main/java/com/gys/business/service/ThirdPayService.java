package com.gys.business.service;

import com.gys.business.service.data.thirdPay.SytOperationInData;
import com.gys.business.service.data.thirdPay.SytOperationOutData;
import com.gys.business.service.data.thirdPay.SytRevokeOrderInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.ResultVO;

import java.util.List;
import java.util.Map;

public interface ThirdPayService {
    ResultVO<Map<String, Object>> pay(GetLoginOutData userInfo, Map<String,Object> inData);
    ResultVO<Map<String, Object>> query(GetLoginOutData userInfo, Map<String,Object> inData);
    ResultVO<Map<String, Object>> refundBack(GetLoginOutData userInfo, Map<String,Object> inData);
    ResultVO<Map<String, Object>> revokePay(GetLoginOutData userInfo, Map<String,Object> inData);

    List<SytOperationOutData> querySytOperation(GetLoginOutData userInfo, SytOperationInData inData);
    void revokeOrder(SytRevokeOrderInData inData);
}
