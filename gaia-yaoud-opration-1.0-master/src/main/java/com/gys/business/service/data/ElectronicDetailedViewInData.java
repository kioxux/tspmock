package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 电子券详情查看
 *
 * @author xiaoyuan on 2020/10/4
 */
@Data
public class ElectronicDetailedViewInData implements Serializable {
    private static final long serialVersionUID = 8313984642168562379L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @NotBlank(message = "主题单号不可为空")
    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    @NotBlank(message = "审核状态可为空")
    @ApiModelProperty(value = "审核状态 N为否，Y为是")
    private String gsebsStatus;


}
