
package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdPrintSaleMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaSdPrintSale;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.SaleBillSetService;
import com.gys.business.service.data.GaiaSdPrintSaleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaleBillSetServiceImpl implements SaleBillSetService {
    @Autowired
    private GaiaSdPrintSaleMapper gaiaSdPrintSaleMapper;

    public void save(GaiaSdPrintSale inData) {
        GaiaSdPrintSale gaiaSdPrintSale = this.gaiaSdPrintSaleMapper.selectByPrimaryKey(inData);
        if (ObjectUtil.isEmpty(gaiaSdPrintSale)) {
            this.gaiaSdPrintSaleMapper.insert(inData);
        } else {
            this.gaiaSdPrintSaleMapper.updateByPrimaryKeySelective(inData);
        }

    }

    public GaiaSdPrintSale get(GaiaSdPrintSale inData) {
        GaiaSdPrintSale gaiaSdPrintSale = this.gaiaSdPrintSaleMapper.selectByPrimaryKey(inData);
        return gaiaSdPrintSale;
    }
}
