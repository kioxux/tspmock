//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class PromGiftResultOutData {
    private String clientId;
    private String gspgcVoucherId;
    private String gspgcSerial;
    private String gspgcProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private String gspgcSeriesId;
    private BigDecimal gspgcGiftPrc;
    private String gspgcGiftRebate;

    public PromGiftResultOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspgcVoucherId() {
        return this.gspgcVoucherId;
    }

    public String getGspgcSerial() {
        return this.gspgcSerial;
    }

    public String getGspgcProId() {
        return this.gspgcProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public String getGspgcSeriesId() {
        return this.gspgcSeriesId;
    }

    public BigDecimal getGspgcGiftPrc() {
        return this.gspgcGiftPrc;
    }

    public String getGspgcGiftRebate() {
        return this.gspgcGiftRebate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspgcVoucherId(final String gspgcVoucherId) {
        this.gspgcVoucherId = gspgcVoucherId;
    }

    public void setGspgcSerial(final String gspgcSerial) {
        this.gspgcSerial = gspgcSerial;
    }

    public void setGspgcProId(final String gspgcProId) {
        this.gspgcProId = gspgcProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setGspgcSeriesId(final String gspgcSeriesId) {
        this.gspgcSeriesId = gspgcSeriesId;
    }

    public void setGspgcGiftPrc(final BigDecimal gspgcGiftPrc) {
        this.gspgcGiftPrc = gspgcGiftPrc;
    }

    public void setGspgcGiftRebate(final String gspgcGiftRebate) {
        this.gspgcGiftRebate = gspgcGiftRebate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromGiftResultOutData)) {
            return false;
        } else {
            PromGiftResultOutData other = (PromGiftResultOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label143: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label143;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label143;
                    }

                    return false;
                }

                Object this$gspgcVoucherId = this.getGspgcVoucherId();
                Object other$gspgcVoucherId = other.getGspgcVoucherId();
                if (this$gspgcVoucherId == null) {
                    if (other$gspgcVoucherId != null) {
                        return false;
                    }
                } else if (!this$gspgcVoucherId.equals(other$gspgcVoucherId)) {
                    return false;
                }

                Object this$gspgcSerial = this.getGspgcSerial();
                Object other$gspgcSerial = other.getGspgcSerial();
                if (this$gspgcSerial == null) {
                    if (other$gspgcSerial != null) {
                        return false;
                    }
                } else if (!this$gspgcSerial.equals(other$gspgcSerial)) {
                    return false;
                }

                label122: {
                    Object this$gspgcProId = this.getGspgcProId();
                    Object other$gspgcProId = other.getGspgcProId();
                    if (this$gspgcProId == null) {
                        if (other$gspgcProId == null) {
                            break label122;
                        }
                    } else if (this$gspgcProId.equals(other$gspgcProId)) {
                        break label122;
                    }

                    return false;
                }

                label115: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label115;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label115;
                    }

                    return false;
                }

                Object this$gspgSpecs = this.getGspgSpecs();
                Object other$gspgSpecs = other.getGspgSpecs();
                if (this$gspgSpecs == null) {
                    if (other$gspgSpecs != null) {
                        return false;
                    }
                } else if (!this$gspgSpecs.equals(other$gspgSpecs)) {
                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                label94: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label94;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label94;
                    }

                    return false;
                }

                label87: {
                    Object this$gspgcSeriesId = this.getGspgcSeriesId();
                    Object other$gspgcSeriesId = other.getGspgcSeriesId();
                    if (this$gspgcSeriesId == null) {
                        if (other$gspgcSeriesId == null) {
                            break label87;
                        }
                    } else if (this$gspgcSeriesId.equals(other$gspgcSeriesId)) {
                        break label87;
                    }

                    return false;
                }

                Object this$gspgcGiftPrc = this.getGspgcGiftPrc();
                Object other$gspgcGiftPrc = other.getGspgcGiftPrc();
                if (this$gspgcGiftPrc == null) {
                    if (other$gspgcGiftPrc != null) {
                        return false;
                    }
                } else if (!this$gspgcGiftPrc.equals(other$gspgcGiftPrc)) {
                    return false;
                }

                Object this$gspgcGiftRebate = this.getGspgcGiftRebate();
                Object other$gspgcGiftRebate = other.getGspgcGiftRebate();
                if (this$gspgcGiftRebate == null) {
                    if (other$gspgcGiftRebate != null) {
                        return false;
                    }
                } else if (!this$gspgcGiftRebate.equals(other$gspgcGiftRebate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromGiftResultOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspgcVoucherId = this.getGspgcVoucherId();
        result = result * 59 + ($gspgcVoucherId == null ? 43 : $gspgcVoucherId.hashCode());
        Object $gspgcSerial = this.getGspgcSerial();
        result = result * 59 + ($gspgcSerial == null ? 43 : $gspgcSerial.hashCode());
        Object $gspgcProId = this.getGspgcProId();
        result = result * 59 + ($gspgcProId == null ? 43 : $gspgcProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $gspgcSeriesId = this.getGspgcSeriesId();
        result = result * 59 + ($gspgcSeriesId == null ? 43 : $gspgcSeriesId.hashCode());
        Object $gspgcGiftPrc = this.getGspgcGiftPrc();
        result = result * 59 + ($gspgcGiftPrc == null ? 43 : $gspgcGiftPrc.hashCode());
        Object $gspgcGiftRebate = this.getGspgcGiftRebate();
        result = result * 59 + ($gspgcGiftRebate == null ? 43 : $gspgcGiftRebate.hashCode());
        return result;
    }

    public String toString() {
        return "PromGiftResultOutData(clientId=" + this.getClientId() + ", gspgcVoucherId=" + this.getGspgcVoucherId() + ", gspgcSerial=" + this.getGspgcSerial() + ", gspgcProId=" + this.getGspgcProId() + ", gspgProName=" + this.getGspgProName() + ", gspgSpecs=" + this.getGspgSpecs() + ", proPlace=" + this.getProPlace() + ", proPrice=" + this.getProPrice() + ", gspgcSeriesId=" + this.getGspgcSeriesId() + ", gspgcGiftPrc=" + this.getGspgcGiftPrc() + ", gspgcGiftRebate=" + this.getGspgcGiftRebate() + ")";
    }
}
