package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class EssentialProductOrderOutData implements Serializable {

    @ApiModelProperty(value = "铺货单号")
    private String orderId;

    @ApiModelProperty(value = "保存时间")
    private String createTime;

    @ApiModelProperty(value = "确认时间")
    private String confirmTime;

    @ApiModelProperty(value = "铺货品次")
    private Integer proCount;

    @ApiModelProperty(value = "铺货门店数")
    private Integer storeCount;

    @ApiModelProperty(value = "数据来源")
    private String sourceDate;

    @ApiModelProperty(value = "单据状态 0-未铺货确认 1-铺货已确认 9-已失效")
    private String status;

}
