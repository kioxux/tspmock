package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

/**
 * 移动收银订单明细
 */
@Data
public class AppMobileOrderInfoDto {
    private String client;
    private String brId;

    /**
     * 会员ID
     */
    private String memberId;

    /**
     * 会员卡号
     */
    private String cardNum;
    /**
     * 会员姓名
     */
    private String name;
    /**
     * 会员手机号
     */
    private String mobile;
    /**
     * 会员性别
     */
    private String sex;
    /**
     *  总价
     */
    private String  totalPrice;

    /**
     * 商品明细
     */
    private List<RebornData> dataList;
}
