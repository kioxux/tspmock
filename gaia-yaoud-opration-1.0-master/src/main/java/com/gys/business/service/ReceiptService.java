package com.gys.business.service;

import com.gys.business.service.data.ReceiptInData;
import com.gys.business.service.data.ReceiptOutData;
import com.gys.business.service.data.ReceiptPayOutData;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface ReceiptService {

    /**
     * 根据单号获取 交易数据
     * @param inData
     * @return
     */
    ReceiptOutData findReceipt(ReceiptInData inData);

    /**
     * 根据单号 查询支付方式
     * @param inData
     * @return
     */
    List<ReceiptPayOutData> findTypeByOrderNumber(ReceiptInData inData);
}
