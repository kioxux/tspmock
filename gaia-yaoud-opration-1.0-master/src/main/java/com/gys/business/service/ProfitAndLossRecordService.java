package com.gys.business.service;

import com.gys.business.service.data.ProfitAndLossRecordInData;
import com.gys.common.data.PageInfo;

/**
 * @author xiaoyuan
 */
public interface ProfitAndLossRecordService {
    /**
     * 条件列表查询
     * @param inData
     * @return
     */
    PageInfo<ProfitAndLossRecordOutDataObj> conditionQuery(ProfitAndLossRecordInData inData);
}
