package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaFranchiseeMapper;
import com.gys.business.mapper.GaiaProcessDiagramMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaFranchisee;
import com.gys.business.mapper.entity.GaiaProcessDiagram;
import com.gys.business.service.GaiaProcessDiagramService;
import com.gys.business.service.data.Menu.ProcessDiagramBO;
import com.gys.business.service.data.Menu.ProcessDiagramOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.enums.EnumUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.BlockSet;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class GaiaProcessDiagramServiceImpl implements GaiaProcessDiagramService {

    @Autowired
    private GaiaProcessDiagramMapper processDiagramMapper;
    @Autowired
    private GaiaUserDataMapper userDataMapper;
    @Autowired
    private GaiaFranchiseeMapper franchiseeMapper;
    @Override
    public List<ProcessDiagramOutData> getProcessDiagram(GetLoginOutData outData, String type) {
        String pId  = EnumUtil.MenuFlow.getValue(type);
        if (StringUtils.isEmpty(pId)){
           return null;
        }
        //查询数据
        List<ProcessDiagramBO> processDiagram = processDiagramMapper.getProcessDiagram(pId);
        //返回参数
        List<ProcessDiagramOutData> outList = new ArrayList<>();
        if(ObjectUtil.isNotEmpty(processDiagram)){
            //找到一级菜单
            List<ProcessDiagramBO> processTypeList = processDiagram.stream().filter(item -> item.getFlowType().equals("1")).collect(Collectors.toList());
            //循环 父级菜单
            for (ProcessDiagramBO processType : processTypeList){

                ProcessDiagramOutData pdod = new ProcessDiagramOutData();
                pdod.setFlowName(processType.getFlowName());
                //循环菜单 找到对应的子菜单
                List<ProcessDiagramBO> pbList = new ArrayList<>();
                for (ProcessDiagramBO process:processDiagram){
                    if(processType.getId().equals(process.getPaientId())){
                        //赋值
                        ProcessDiagramBO processDiagramBO = new ProcessDiagramBO();
                        BeanUtils.copyProperties(process, processDiagramBO);
                        //组织架构 在数据库没有维护 写死
                        if(pId.equals("5") && process.getFlowType().equals("2")){
                            if(process.getId().equals("FRANC_TYPE2")){
                                processDiagramBO.setRoutePath("/compadmList");
                            }else if(process.getId().equals("FRANC_TYPE3")){
                                processDiagramBO.setRoutePath("/wholesaleList");
                            }else if(process.getId().equals("STORE")){
                                processDiagramBO.setRoutePath("/store");
                            }else if(process.getId().equals("DEP")){
                                processDiagramBO.setRoutePath("/deptStore/dept");
                            }
                        }
                        processDiagramBO.setCanOpenArr(processDiagramBO.getCanOpen().split(","));
                        pbList.add(processDiagramBO);
                    }
                }
                //子菜单排序
                pbList.stream().sorted(Comparator.comparing(ProcessDiagramBO::getSort)).collect(Collectors.toList());
                pdod.setProcessDiagramBO(pbList);
                outList.add(pdod);
            }
        }else {
            return new ArrayList<>();
        }
        return outList;
    }


    @Override
    public boolean checkUserAuth(GetLoginOutData userInfo,String menuId) {
        GaiaFranchisee franchisee = (GaiaFranchisee)this.franchiseeMapper.selectByPrimaryKey(userInfo.getClient());
        if(menuId.equals("DEP")){
            return true;
        }else if(menuId.equals("STORE")){
            return franchisee.getFrancType1().equals("1");
        }else if(menuId.equals("FRANC_TYPE2")){
            return franchisee.getFrancType2().equals("1");
        }else if(menuId.equals("FRANC_TYPE3")){
            return franchisee.getFrancType3().equals("1");
        }else {
            Map map = new HashMap();
            map.put("client",userInfo.getClient());
            map.put("userId",userInfo.getUserId());
            map.put("menuId",menuId);
            //查询当前用户权限
            Integer auth = processDiagramMapper.checkUserAuth(map);
            if(auth>0){//查询条数大于零  有权限
                return true;
            }
            return false;
        }

    }
}
