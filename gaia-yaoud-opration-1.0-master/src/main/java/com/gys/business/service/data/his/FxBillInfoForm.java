package com.gys.business.service.data.his;

import lombok.Data;


/**
 * @author wavesen
 */
@Data
public class FxBillInfoForm {
    private String billNo;
}
