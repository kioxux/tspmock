package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @desc:
 * @author: RULAISZ
 * @createTime: 2021/11/30 16:47
 */
@Data
public class AuditIntegralExchangeInData {
    /**
     * 加盟商
     */
    private String client;

    /**`
     * 单号
     */
    @NotBlank(message = "提示：请选择单号")
    private String voucherId;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 修改时间
     */
    private Date updateTime;
}
