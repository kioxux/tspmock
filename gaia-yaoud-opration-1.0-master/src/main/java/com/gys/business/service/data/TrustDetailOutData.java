package com.gys.business.service.data;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Api(value = "委托配送单明细商品出参")
public class TrustDetailOutData {
    @ApiModelProperty(value = "商品自编码")
    private String proCode;
    @ApiModelProperty(value = "紫石商品编码")
    private String gswdProCode;
    @ApiModelProperty(value = "药德商品是否存在 0 存在 1 不存在")
    private String hasDsfCode;
    @ApiModelProperty(value = "通用名")
    private String gswdCommonName;
    @ApiModelProperty(value = "规格")
    private String gswdSpecs;
    @ApiModelProperty(value = "单位")
    private String gswdUnit;
    @ApiModelProperty(value = "厂家")
    private String gswdFactoryName;
    @ApiModelProperty(value = "零售价")
    private BigDecimal gswdNormalPrice;
    @ApiModelProperty(value = "配送价")
    private BigDecimal psPrice;
    @ApiModelProperty(value = "到货数量")
    private BigDecimal gswdQty;
    @ApiModelProperty(value = "批号")
    private String gswdBatchNo;
    @ApiModelProperty(value = "有效期")
    private String gswdExpiryDate;
    @ApiModelProperty(value = "批次")
    private String gswdBatch;
    @ApiModelProperty(value = "产地")
    private String gswdPlace;
    @ApiModelProperty(value = "剂型")
    private String gswdPorm;
    @ApiModelProperty(value = "批准文号")
    private String gswdRegisterNo;
    @ApiModelProperty(value = "生产日期")
    private String gswdMadeDate;
    @ApiModelProperty(value = "税率")
    private String gswdInputTax;
    @ApiModelProperty(value = "拒绝数量")
    private BigDecimal refuseQty;
    @ApiModelProperty(value = "拒绝原因")
    private String refuseReason;
    @ApiModelProperty(value = "到货数量")
    private BigDecimal dhQty;
    @ApiModelProperty(value = "配送金额")
    private BigDecimal psAmt;
}
