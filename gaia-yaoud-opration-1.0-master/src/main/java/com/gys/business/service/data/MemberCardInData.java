//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class MemberCardInData {
    private String clientId;
    private String gsmbCardId;
    private String gsmbName;
    private String gsmbMobile;
    private String gsmbSex;
    private String gsmbTel;
    private String gsmbAddress;

    public MemberCardInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsmbCardId() {
        return this.gsmbCardId;
    }

    public String getGsmbName() {
        return this.gsmbName;
    }

    public String getGsmbMobile() {
        return this.gsmbMobile;
    }

    public String getGsmbSex() {
        return this.gsmbSex;
    }

    public String getGsmbTel() {
        return this.gsmbTel;
    }

    public String getGsmbAddress() {
        return this.gsmbAddress;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsmbCardId(final String gsmbCardId) {
        this.gsmbCardId = gsmbCardId;
    }

    public void setGsmbName(final String gsmbName) {
        this.gsmbName = gsmbName;
    }

    public void setGsmbMobile(final String gsmbMobile) {
        this.gsmbMobile = gsmbMobile;
    }

    public void setGsmbSex(final String gsmbSex) {
        this.gsmbSex = gsmbSex;
    }

    public void setGsmbTel(final String gsmbTel) {
        this.gsmbTel = gsmbTel;
    }

    public void setGsmbAddress(final String gsmbAddress) {
        this.gsmbAddress = gsmbAddress;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MemberCardInData)) {
            return false;
        } else {
            MemberCardInData other = (MemberCardInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label95;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label95;
                    }

                    return false;
                }

                Object this$gsmbCardId = this.getGsmbCardId();
                Object other$gsmbCardId = other.getGsmbCardId();
                if (this$gsmbCardId == null) {
                    if (other$gsmbCardId != null) {
                        return false;
                    }
                } else if (!this$gsmbCardId.equals(other$gsmbCardId)) {
                    return false;
                }

                Object this$gsmbName = this.getGsmbName();
                Object other$gsmbName = other.getGsmbName();
                if (this$gsmbName == null) {
                    if (other$gsmbName != null) {
                        return false;
                    }
                } else if (!this$gsmbName.equals(other$gsmbName)) {
                    return false;
                }

                label74: {
                    Object this$gsmbMobile = this.getGsmbMobile();
                    Object other$gsmbMobile = other.getGsmbMobile();
                    if (this$gsmbMobile == null) {
                        if (other$gsmbMobile == null) {
                            break label74;
                        }
                    } else if (this$gsmbMobile.equals(other$gsmbMobile)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$gsmbSex = this.getGsmbSex();
                    Object other$gsmbSex = other.getGsmbSex();
                    if (this$gsmbSex == null) {
                        if (other$gsmbSex == null) {
                            break label67;
                        }
                    } else if (this$gsmbSex.equals(other$gsmbSex)) {
                        break label67;
                    }

                    return false;
                }

                Object this$gsmbTel = this.getGsmbTel();
                Object other$gsmbTel = other.getGsmbTel();
                if (this$gsmbTel == null) {
                    if (other$gsmbTel != null) {
                        return false;
                    }
                } else if (!this$gsmbTel.equals(other$gsmbTel)) {
                    return false;
                }

                Object this$gsmbAddress = this.getGsmbAddress();
                Object other$gsmbAddress = other.getGsmbAddress();
                if (this$gsmbAddress == null) {
                    if (other$gsmbAddress != null) {
                        return false;
                    }
                } else if (!this$gsmbAddress.equals(other$gsmbAddress)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof MemberCardInData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsmbCardId = this.getGsmbCardId();
        result = result * 59 + ($gsmbCardId == null ? 43 : $gsmbCardId.hashCode());
        Object $gsmbName = this.getGsmbName();
        result = result * 59 + ($gsmbName == null ? 43 : $gsmbName.hashCode());
        Object $gsmbMobile = this.getGsmbMobile();
        result = result * 59 + ($gsmbMobile == null ? 43 : $gsmbMobile.hashCode());
        Object $gsmbSex = this.getGsmbSex();
        result = result * 59 + ($gsmbSex == null ? 43 : $gsmbSex.hashCode());
        Object $gsmbTel = this.getGsmbTel();
        result = result * 59 + ($gsmbTel == null ? 43 : $gsmbTel.hashCode());
        Object $gsmbAddress = this.getGsmbAddress();
        result = result * 59 + ($gsmbAddress == null ? 43 : $gsmbAddress.hashCode());
        return result;
    }

    public String toString() {
        return "MemberCardInData(clientId=" + this.getClientId() + ", gsmbCardId=" + this.getGsmbCardId() + ", gsmbName=" + this.getGsmbName() + ", gsmbMobile=" + this.getGsmbMobile() + ", gsmbSex=" + this.getGsmbSex() + ", gsmbTel=" + this.getGsmbTel() + ", gsmbAddress=" + this.getGsmbAddress() + ")";
    }
}
