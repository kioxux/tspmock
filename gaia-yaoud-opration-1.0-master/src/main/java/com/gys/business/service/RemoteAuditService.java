//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.RemoteAuditHInData;
import com.gys.common.data.GetLoginOutData;

public interface RemoteAuditService {
    void insertRemoteAudit(RemoteAuditHInData inData, GetLoginOutData userInfo);
}
