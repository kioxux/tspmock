package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SaleTaskHplan {
    private String client;
    /**
     * 月份
     */
    @ApiModelProperty("月份")
    private String monthPlan;
    /**
     * 销售日期
     */
    @ApiModelProperty("销售日期")
    private String daysPlan;
    /**
     * 销售日期
     */
    @ApiModelProperty("状态")
    private String splanSta;
}
