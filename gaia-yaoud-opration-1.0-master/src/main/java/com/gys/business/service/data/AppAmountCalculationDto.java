package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2021/3/18
 */
@Data
public class AppAmountCalculationDto implements Serializable {
    private static final long serialVersionUID = 8876927562508449326L;

    @ApiModelProperty(value = "商品列表")
    private List<AppAmountCalculationVo> proList;

    @ApiModelProperty(value = "可生效促销列表")
    private List<AppPromData> promList;

    @ApiModelProperty(value = "实收金额")
    private String actualAmount;

    @ApiModelProperty(value = "优惠减(折让金额)")
    private String discountedPrice;

    @ApiModelProperty(value = "销售码")
    private String salesCode;

    @ApiModelProperty(value = "手机销售单号")
    private String appBillNo;
}
