package com.gys.business.service.data;

import lombok.Data;

/**
 * @author zhangdong
 * @date 2021/7/20 16:10
 */
@Data
public class OrderDetailBean {

    private String orderId;//订单编码
    private String proId;//商品编码

    private String client;
    private String stoCode;
}
