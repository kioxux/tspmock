package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xiaoyuan
 */
@Data
public class PromUnitarySetInData implements Serializable {
    private static final long serialVersionUID = 4871732372942764375L;
    private String clientId;
    private String gspusVoucherId;
    private String gspusSerial;
    private String gspusProId;
    private String gspusVaild;
    private String gspusQty1;
    private BigDecimal gspusPrc1;
    private String gspusRebate1;
    private String gspusQty2;
    private BigDecimal gspusPrc2;
    private String gspusRebate2;
    private String gspusQty3;
    private BigDecimal gspusPrc3;
    private String gspusRebate3;
    private String gspusMemFlag;
    private String gspusInteFlag;
    private String gspusInteRate;
}
