package com.gys.business.service.yaomeng;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.gys.business.mapper.GaiaProductYmDmMapper;
import com.gys.business.mapper.GaiaSdSaleRecipelMapper;
import com.gys.business.mapper.GaiaSdSystemParaMapper;
import com.gys.business.mapper.entity.GaiaSdSaleRecipel;
import com.gys.business.mapper.entity.GaiaSdSystemPara;
import com.gys.business.mapper.yaomeng.GaiaPrescribeDetailDao;
import com.gys.business.mapper.yaomeng.GaiaPrescribeInfoDao;
import com.gys.business.service.CommonService;
import com.gys.business.service.RemoteReviewService;
import com.gys.business.service.data.GetFileOutData;
import com.gys.business.service.data.RebornData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.yaomeng.*;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import com.gys.util.yaomeng.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static com.gys.util.yaomeng.YaomengApiUtil.readInputStream;

/**
 * @author ZhangDong
 * @date 2021/11/4 16:54
 */
@Service
@Slf4j
public class YaomengServiceImpl implements YaomengService {
    @Resource
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;
    @Resource
    private GaiaPrescribeDetailDao prescribeDetailDao;
    @Resource
    private GaiaPrescribeInfoDao prescribeInfoDao;
    @Resource
    private GaiaSdSaleRecipelMapper gaiaSdSaleRecipelMapper;
    @Resource
    private CommonService commonService;
    @Autowired
    private RemoteReviewService remoteReviewService;

    @Resource
    private GaiaProductYmDmMapper gaiaProductYmDmMapper;
    @Value("${yaomeng.serverUrl}")
    private String yaomengServerUrl;


    /**
     * 药盟查询参数，是否需要等待回传处方就可以继续收银
     *
     * @param client
     * @param depId
     * @return
     */
    @Override
    public String isPrescribe(String client, String depId) {
        //isPrescribe 默认为N或当门店该值未配置时，表示不需要等待回传处方就可以继续收银。当isPrescribe为Y时，则表示需要等待处方回传成功后才可以继续收银。
        GaiaSdSystemPara para = gaiaSdSystemParaMapper.getAuthByClientAndBrId(client, depId, "isPrescribe");
        return para == null ? "N" : para.getGsspPara();
    }

    @Override
    public JsonResult drugCheck(String client, String stoCode, List<String> drugs) {
        if (CollUtil.isEmpty(drugs)) {
            return JsonResult.success();
        }
        //未对码集合
        List<String> noDrugList = new ArrayList<>(drugs.size());
        //查出对码集合
        List<YaomengDrugDto> yaomengDrugDtos = listYMRelation(drugs, client, stoCode);
        Map<String, YaomengDrugDto> map = yaomengDrugDtos.stream().collect(Collectors.toMap(r -> r.getProSelfCode(), r -> r, (r, v) -> r));
        for (String drug : drugs) {
            YaomengDrugDto yaomengDrugDto = map.get(drug);
            if (yaomengDrugDto == null || yaomengDrugDto.getDrugId() == null) {
                noDrugList.add(drug);
            }
        }
        //对码操作不需要我们做了，他们对好码放到数据表，新增商品也是如此
//        for (String drug : drugs) {
//////            List<YaomengDrugDto> list = YaomengApiUtil.drugList("yd001", drug, null, null, null);
//////            if (CollUtil.isEmpty(list)) {
//////                noDrugList.add(drug);
//////            }
//////        }
        if (CollUtil.isEmpty(noDrugList)) {
            return JsonResult.success();
        }
        return JsonResult.fail(101, JSON.toJSONString(noDrugList));
    }

    @Override
    public JsonResult drugDisease(String client, String stoCode, List<String> drugs) {
        if (CollUtil.isEmpty(drugs)) {
            return JsonResult.success();
        }
        List<YaomengDrugDto> result = new ArrayList<>();
        //查出对码集合
        List<YaomengDrugDto> yaomengDrugDtos = listYMRelation(drugs, client, stoCode);
        Map<String, YaomengDrugDto> map = yaomengDrugDtos.stream().collect(Collectors.toMap(r -> r.getProSelfCode(), r -> r, (r, v) -> r));
        for (String erpNo : drugs) {
            YaomengDrugDto yaomengDrugDto = map.get(erpNo);
            //第一步是验证对码，理论上不存在未空的情况
            if (yaomengDrugDto != null) {
                List<YaomengDiseaseDto> list = new ArrayList<>();
                list.addAll(YaomengApiUtil.drugDisease(yaomengServerUrl, client, yaomengDrugDto.getDrugId(), yaomengDrugDto.getErpNo()));
                if (ObjectUtil.isNotEmpty(list)){
                    yaomengDrugDto.setYaomengDiseaseDtoList(list);
                }
                result.add(yaomengDrugDto);
            }
        }
        return ObjectUtil.isNotEmpty(result) ? JsonResult.success(result) : null;
    }

    @Override
    public JsonResult checkLimit(String client, String stoCode, List<String> erpNo, String idCard) {
        if (CollUtil.isEmpty(erpNo)) {
            return JsonResult.success();
        }
        //查出对码集合
        List<YaomengDrugDto> yaomengDrugDtos = listYMRelation(erpNo, client, stoCode);
        List<Long> durgIdList = yaomengDrugDtos.stream().map(r -> r.getDrugId()).distinct().collect(Collectors.toList());
        List<Long> list = Lists.newArrayList(durgIdList);
        YaomengCheckLimitBean bean = new YaomengCheckLimitBean(list, idCard);
        int age = IdcardUtil.getAgeByIdCard(idCard);
        bean.setAge(age);
        int gender = IdcardUtil.getGenderByIdCard(idCard);
        bean.setSex(gender);
        String result = YaomengApiUtil.checkLimit(yaomengServerUrl, client, bean);
        return JsonResult.success(result);
    }

    @Override
    public JsonResult dialogueQuestion(String client, List<String> diseaseIds) {
        if (CollUtil.isEmpty(diseaseIds)) {
            return JsonResult.success();
        }
        List<Long> tempList = diseaseIds.stream().map(r -> Long.valueOf(r)).collect(Collectors.toList());
        List<YaomengDialogueQuestionDto> list = YaomengApiUtil.dialogueQuestion(yaomengServerUrl, client, tempList);
        return JsonResult.success(list);
    }

    @Override
    public JsonResult getDoctor(String client) {
        YaomengDoctorDto dto = YaomengApiUtil.getDoctor(yaomengServerUrl, client);
        return JsonResult.success(dto);
    }

    @Override
    public JsonResult speedSimple(String client, String stoCode, YaomengSpeedSimpleBean bean) {
        List<YaomengErpDrugBean> erpDrugs = bean.getErpDrugs();
        List<String> erpNoList = erpDrugs.stream().map(r -> r.getErpNo()).collect(Collectors.toList());
        List<YaomengDrugDto> yaomengDrugDtos = listYMRelation(erpNoList, client, stoCode);
        Map<String, YaomengDrugDto> map = yaomengDrugDtos.stream().collect(Collectors.toMap(r -> r.getProSelfCode(), r -> r, (r, v) -> r));
        for (YaomengErpDrugBean erpDrug : erpDrugs) {
            YaomengDrugDto dto = map.get(erpDrug.getErpNo());
            erpDrug.setDrugId(dto.getDrugId());
            erpDrug.setErpNo(dto.getErpNo());
        }
        YaomengSpeedSimpleRep rep = YaomengApiUtil.speedSimple(yaomengServerUrl, client, bean);
        if (rep == null) {
            return JsonResult.fail(101, "提交图文开方失败");
        } else {
            String errorMsgTemp = rep.getErrorMsgTemp();
            if (errorMsgTemp != null) {
                return JsonResult.fail(101, errorMsgTemp);
            }
        }
        return JsonResult.success(rep);
    }

    @Override
    public JsonResult getPrescriptionFileUrl(String no, String pharmacist) {
        String prescriptionFileUrl = YaomengApiUtil.getPrescriptionFileUrl(yaomengServerUrl, no, pharmacist);
        return JsonResult.success(prescriptionFileUrl);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult submitRecipelQuestionInfo(GetLoginOutData userInfo, RecipelQuestionInfoBean bean) {
        //删除前面开方的数据
        prescribeInfoDao.deleteByPreBillNo(bean.getPreBillNo());
        prescribeDetailDao.deleteByPreBillNo(bean.getPreBillNo());
        log.info("药盟提交问诊信息, 先删除前开方数据, billNo:{}", bean.getPreBillNo());
        String client = userInfo.getClient();
        String depId = userInfo.getDepId();
        List<YaomengDiseaseBean> diseaseList = bean.getDiseaseList();
        List<YaomengQuestionBean> questionList = bean.getQuestionList();
        List<GaiaPrescribeInfo> infoList = new ArrayList<>(diseaseList.size());
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        for (YaomengDiseaseBean diseaseBean : diseaseList) {
            GaiaPrescribeInfo prescribeInfo = new GaiaPrescribeInfo();
            BeanUtil.copyProperties(bean, prescribeInfo);
            prescribeInfo.setClient(client);
            prescribeInfo.setPreBrId(depId);
            prescribeInfo.setPreDate(localDate);
            prescribeInfo.setPreTime(localTime);
            prescribeInfo.setPreIllId(diseaseBean.getPreIllId());
            prescribeInfo.setPreIllName(diseaseBean.getPreIllName());
            infoList.add(prescribeInfo);
        }
        prescribeInfoDao.insertList(infoList);
        List<GaiaPrescribeDetail> detailList = new ArrayList<>(questionList.size());
        for (YaomengQuestionBean questionBean : questionList) {
            GaiaPrescribeDetail prescribeDetail = new GaiaPrescribeDetail();
            prescribeDetail.setClient(client);
            prescribeDetail.setPreBrId(depId);
            prescribeDetail.setPreEmp(bean.getPreEmp());
            prescribeDetail.setPreBillNo(bean.getPreBillNo());
            prescribeDetail.setPreQuestion(questionBean.getPreQuestion());
            prescribeDetail.setPreAnswer(questionBean.getPreAnswer());
            prescribeDetail.setPreDate(localDate);
            prescribeDetail.setPreTime(localTime);
            detailList.add(prescribeDetail);
        }
        prescribeDetailDao.insertList(detailList);
        log.info("药盟提交问诊信息，data:{}", JSON.toJSONString(bean));
        return JsonResult.success();
    }

    @Override
    public GaiaPrescribeInfo getPrescribeBySaleOrderIdLimit(String saleOrderId) {
        GaiaPrescribeInfo gaiaPrescribeInfo = prescribeInfoDao.getPrescribeBySaleOrderIdLimit(saleOrderId);
        return gaiaPrescribeInfo;
    }

    @Override
    public JsonResult saveEleRecipelInfo(GetLoginOutData userInfo, List<GaiaSdSaleRecipel> list) {
        if (CollUtil.isEmpty(list)) {
            return JsonResult.success();
        }
        String gssrSaleBillNo = list.get(0).getGssrSaleBillNo();
        if (gssrSaleBillNo == null) {
            return JsonResult.fail(101, "上传的数据里无销售单信息");
        }
        int effectSize = gaiaSdSaleRecipelMapper.deleteBySaleBillNo(gssrSaleBillNo);
        log.info("药盟上传处方数据，先删除前开方数据，effectSize:{}", effectSize);
        gaiaSdSaleRecipelMapper.insertLists(list);
        return JsonResult.success();
    }

    /**
     * @Description TODO
     * @param map
     * @return com.gys.common.data.JsonResult
     * @date 创建日期:2021/12/22 16:10
     * @auther hanWanLu
     * @auther 中文:韩万路
     * @功能:通过销售单号获取电子开方信息
    **/
    @Override
    public JsonResult getEleRecipelInfoByGssrSaleBillNo(String client, Map<String, String> map) {
        if (CollUtil.isEmpty(map)) {
            return JsonResult.success();
        }
        String gssrSaleBillNo = map.get("GSSR_SALE_BILL_NO");
        if (gssrSaleBillNo == null) {
            return JsonResult.fail(101, "销售单号为空");
        }
        /**
         * 通过销售单号获取
         */
//        ArrayList<GaiaSdSaleRecipel> gaiaSdSaleRecipels = gaiaSdSaleRecipelMapper.queryReviewBySaleBillNo(client, gssrSaleBillNo);
        ArrayList<GaiaSdSaleRecipel> gaiaSdSaleRecipels = gaiaSdSaleRecipelMapper.getEleRecipelInfoByGssrSaleBillNo(client, gssrSaleBillNo);
        return JsonResult.success(gaiaSdSaleRecipels);
    }
    @Override
    public JsonResult recipelInfoQuery(RecipelInfoQueryBean bean) {
        List<RecipelInfoQueryDto> list = gaiaSdSaleRecipelMapper.recipelInfoQuery(bean);
        return JsonResult.success(list);
    }

    @Override
    public JsonResult recipelAgain(String client, String depId, String saleBillNo) {
        List<GaiaSdSaleRecipel> recipelList = gaiaSdSaleRecipelMapper.getBySaleBillNo(saleBillNo);
        if (CollUtil.isEmpty(recipelList)) {
            log.error("药盟重开方，查无开方数据, saleBillNo:{}", saleBillNo);
            return JsonResult.fail(101, "查无开方数据");
        }
        GaiaSdSaleRecipel recipel = recipelList.get(0);
        String gssrRecipelId = recipel.getGssrRecipelId();
        if (StrUtil.isNotEmpty(gssrRecipelId)) {
            return JsonResult.fail(101, "此销售单已开方");
        }

        List<GaiaPrescribeInfo> prescribeInfos = prescribeInfoDao.getBySaleBillNo(saleBillNo);
        if (CollUtil.isEmpty(prescribeInfos)) {
            log.error("药盟重开方，查无会诊信息, saleBillNo:{}", saleBillNo);
            return JsonResult.fail(101, "查无会诊信息");
        }
        List<GaiaPrescribeDetail> prescribeDetails = prescribeDetailDao.getBySaleBillNo(saleBillNo);
        if (CollUtil.isEmpty(prescribeInfos)) {
            log.error("药盟重开方，查无会话信息, saleBillNo:{}", saleBillNo);
            return JsonResult.fail(101, "查无会话信息");
        }
        GaiaPrescribeInfo prescribeInfo = prescribeInfos.get(0);
        YaomengSpeedSimpleBean bean = new YaomengSpeedSimpleBean();
        bean.setDoctorId(prescribeInfo.getPreDoctorId());
        bean.setOrderId(saleBillNo);
        String idcard = recipel.getGssrCustIdcard();
        bean.setIdCard(idcard);
        String birthday = IdcardUtil.getBirthByIdCard(idcard);
        bean.setBirthday(birthday);
        bean.setAge(Integer.parseInt(recipel.getGssrCustAge()));
        bean.setName(recipel.getGssrCustName());
        bean.setSex(Integer.parseInt(recipel.getGssrCustSex()));
        //医患对话
        List<YaomengChatBean> chatBeans = new ArrayList<>(prescribeDetails.size() * 2);
        for (int i = 0; i < prescribeDetails.size(); i++) {
            GaiaPrescribeDetail detail = prescribeDetails.get(i);
            YaomengChatBean chatBean = new YaomengChatBean(detail.getPreQuestion(), 1, true);
            YaomengChatBean chatBean1 = new YaomengChatBean(detail.getPreAnswer(), 0, i != prescribeDetails.size() - 1 ? true : false);
            chatBeans.add(chatBean);
            chatBeans.add(chatBean1);
        }
        bean.setChats(chatBeans);
        //病症信息
        List<YaomengDiseaseDto> diagnosis = prescribeInfos.stream().map(r -> new YaomengDiseaseDto(r.getPreIllId(), r.getPreIllName())).collect(Collectors.toList());
        bean.setDiagnosis(diagnosis);
        List<String> erpNoList = recipelList.stream().map(r -> r.getGssrProId()).collect(Collectors.toList());
        List<YaomengDrugDto> yaomengDrugDtos = listYMRelation(erpNoList, client, depId);
        Map<String, YaomengDrugDto> drugDtoMap = yaomengDrugDtos.stream().collect(Collectors.toMap(r -> r.getProSelfCode(), r -> r, (r, v) -> r));
        if (yaomengDrugDtos.size() != recipelList.size()) {
            log.error("药盟重开方，查询商品对码关系，size不匹配，saleBillNo:{},erpNoList:{},yaomengDrugDtos:{}", saleBillNo, JSON.toJSONString(erpNoList), JSON.toJSONString(yaomengDrugDtos));
            return JsonResult.fail(101, "部分商品未对码，请先完成对码");
        }
        List<YaomengErpDrugBean> erpDrugs = recipelList.stream().map(r -> {
            BigDecimal bigDecimal = BigDecimal.valueOf(Double.valueOf(r.getGssrQty()));
            BigDecimal scale = bigDecimal.setScale(0, RoundingMode.HALF_UP);
            //修改为proCode即药德编码和药盟对码
            YaomengDrugDto yaomengDrugDto = drugDtoMap.get(r.getGssrProId());
            return new YaomengErpDrugBean(yaomengDrugDto.getErpNo(), yaomengDrugDto.getDrugId(), scale.intValue());
        }).collect(Collectors.toList());
        bean.setErpDrugs(erpDrugs);
        YaomengSpeedSimpleRep simpleRep = YaomengApiUtil.speedSimple(yaomengServerUrl, client, bean);
        if (simpleRep == null) {
            return JsonResult.fail(101, "重开方失败，请稍后重试");
        } else {
            String errorMsgTemp = simpleRep.getErrorMsgTemp();
            if (errorMsgTemp != null) {
                return JsonResult.fail(101, errorMsgTemp);
            }
        }
        String no = simpleRep.getNo();
        String prescriptionFileUrl = YaomengApiUtil.getPrescriptionFileUrl(yaomengServerUrl, no, null);
        Map<String, Object> map = YaomengApiUtil.downLoadFile(prescriptionFileUrl);
        File file = null;
        if (map != null && (file = (File) map.get("file")) != null) {
            GetFileOutData outData = commonService.fileUploadLocal(file);
            String url = outData.getUrl();
            log.info("药盟重开方下载完处方图片，上传到服务器, url:" + prescriptionFileUrl + ",result:" + JSON.toJSONString(outData));
            prescriptionFileUrl = url;
            if (file != null) {
                try {
                    file.delete();
                } catch (Exception e) {
                    log.error("药盟重开方下载完处方图片，上传到服务器结束后，删除临时图片文件失败:" + e);
                }
            }
        }
        try {
            saveRecipelIdAndFileUrl(saleBillNo, no, prescriptionFileUrl);
        } catch (Exception e) {
            log.error("药盟重开方，填充处方单号和图片路径error:{}", e);
        }
        return JsonResult.success();
    }

    //填充处方单号和图片路径
    @Override
    public JsonResult saveRecipelIdAndFileUrl(String saleBillNo, String recipelId, String fileUrl) {
        gaiaSdSaleRecipelMapper.saveRecipelIdAndFileUrl(saleBillNo, recipelId, fileUrl);
        return JsonResult.success();
    }

    @Override
    public JsonResult getRecipelProFromSaleD(String client, String depId, String saleBillNo) {
        List<RebornData> list = gaiaSdSaleRecipelMapper.getRecipelProFromSaleD(client, depId, saleBillNo);
        return JsonResult.success(list);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult saveVoucherIdAndPhone(List<GaiaSdSaleRecipel> list) {
        if (CollUtil.isEmpty(list)) {
            return JsonResult.success();
        }
        gaiaSdSaleRecipelMapper.saveVoucherId(list);
        gaiaSdSaleRecipelMapper.savePhoneTypeAndHospital(list.get(0));
        return JsonResult.success();
    }

    @Override
    public JsonResult autoCheckRecipelInfo(GetLoginOutData userInfo, List<String> billNoList) {
        GaiaSdSystemPara para = gaiaSdSystemParaMapper.getAuthByClientAndBrId(userInfo.getClient(), userInfo.getDepId(), "RECISALE_REMOTE_AUTO");
        //自动审核
        //查询门店默认药师
        GetLoginOutData pharmacist = gaiaSdSaleRecipelMapper.getDefoultPharmacist(userInfo.getClient(), userInfo.getDepId());
        if (ObjectUtil.isEmpty(pharmacist) || StringUtil.isBlank(pharmacist.getUserId()) || StringUtil.isBlank(pharmacist.getLoginName()) ) {
            throw new BusinessException("该门店没有维护默认审核药师，\n无法自动审核！请手动审核该处方！");
        }
        if (ObjectUtil.isNotEmpty(para) && "1".equals(para.getGsspPara())) {
            remoteReviewService.approveReview(pharmacist, billNoList);
            // 如果client == 10000308 ，审核通过调http://172.19.1.13:3000/project/17/interface/api/7439 接口
            if (StringUtil.equals(userInfo.getClient(),"10000308")){
                remoteReviewService.remoteCallYunNan(pharmacist,billNoList);
            }
        }
        return JsonResult.success();
    }

    public String downLoadAndUploadFile(String httpUrl) {
        Map<String, Object> map = downLoadFile(httpUrl);
        if (map == null) {
            return httpUrl;
        }
        File file = (File)map.get("file");
        if (file == null) {
            return httpUrl;
        }
        GetFileOutData fileOutData = commonService.fileUploadLocal(file);
        log.info("药盟下载完处方图片，上传到服务器, url:" + httpUrl + ",result:" + JSON.toJSONString(fileOutData));
        String url = fileOutData.getUrl();
        if (file != null) {
            try {
                file.delete();
            } catch (Exception e) {
                log.error("药盟下载完处方图片，上传到服务器结束后，删除临时图片文件失败:" + e);
            }
        }
        return url;
    }
    //药盟下载处方图片
    public static Map<String, Object> downLoadFile(String httpUrl) {
        log.info("药盟开始下载处方图片, url:" + httpUrl);
        Map<String, Object> map = new HashMap<>(2);
        URL url = null;
        InputStream inputStream = null;
        FileOutputStream fos = null;
        int random = ThreadLocalRandom.current().nextInt(10000, 99999);
        String fileTempName = "yaomeng" + random + ".jpg";
        map.put("fileName", fileTempName);
        try {
            url = new URL(httpUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //设置超时间为3秒
            conn.setConnectTimeout(3 * 1000);
            //防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

            //得到输入流
            inputStream = conn.getInputStream();
            //获取自己数组
            byte[] getData = readInputStream(inputStream);

            //文件保存位置
            File saveDir = new File(fileTempName);
            if (!saveDir.exists()) {
                saveDir.createNewFile();
            }
            fos = new FileOutputStream(saveDir);
            fos.write(getData);
            map.put("file", saveDir);
        } catch (IOException e) {
            log.error("药盟下载处方图片失败, url:" + httpUrl + "，error:" + e);
            return null;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        log.info("药盟结束下载处方图片, url:" + httpUrl + "，保存的图片名:" + fileTempName);
        return map;
    }

    @Override
    public List<YaomengDrugDto> listYMRelation(List<String> proSelfCodeList, String client, String stoCode) {
        if (CollectionUtil.isEmpty(proSelfCodeList)) {
            throw new BusinessException("药德商品编码不得为空！");
        }
        return gaiaProductYmDmMapper.listYMRelation(proSelfCodeList, client, stoCode);
    }


}
