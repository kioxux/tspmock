package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class GaiaWmsHudiaoZOutData implements Serializable {
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 调入门店
     */
    private String wmDrmd;

    /**
     * 调出门店
     */
    private String wmDqmd;

    /**
     * 调剂单号
     */
    private String wmTjdh;

    /**
     * 创建日期
     */
    private String wmCjrq;

    /**
     * 创建时间
     */
    private String wmCjsj;

    /**
     * 创建人
     */
    private String wmCjr;

    /**
     * 调出确认日期
     */
    private String wmDqrq;

    /**
     * 调出确认时间
     */
    private String wmDqsj;

    /**
     * 调出确认人
     */
    private String wmDqr;

    /**
     * 调入确认日期
     */
    private String wmDrrq;

    /**
     * 调入确认时间
     */
    private String wmDrsj;

    /**
     * 调入确认人
     */
    private String wmDrr;

    /**
     * 关闭日期
     */
    private String wmGbrq;

    /**
     * 关闭时间
     */
    private String wmGbsj;

    /**
     * 关闭人
     */
    private String wmGbr;

    /**
     * 单据状态
     */
    private String wmDjzt;

    /**
     * 审核人
     */
    private String wmShr;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 开始日期
     */
    private String startTime;
    /**
     * 结束日期
     */
    private String endTime;
    /**
     * 通用名
     */
    private String proCommonname;
    /**
     *规格
     */
    private String proSpecs;
    /**
     *厂家
     */
    private String proFactoryName;
    /**
     *单位
     */
    private String proUnit;
    /**
     *批号
     */
    private String wmPh;
    /**
     *调剂数量
     */
    private BigDecimal wmTjsl;
    /**
     * 金额
     */
    private BigDecimal wmCkj;
    private BigDecimal wmTjsld;
    /**
     *有效期
     */
    private String wmYxq;
    /**
     * 门店名称
     */
    private String stoName;

    /**
     *是否虚拟商品 0-否，1-是
     */
    private String proXnp;

    /**
     * 参考价
     */
    private String proCgj;

    private String wmXh;
    private String wmXh1;
    private String proName;
    private BigDecimal gssQty;
    private String gsppPriceNormal;//零售价
    private String total;
    private String proStorageArea;
    private String tax;
    private String wmHwhBm;
    private String proZdy1;
    private String proIfMed;
    private String proPosition;
    private String proMedQty;
    private String proMedProdctcode;
    private String proSlaeClass;
    private String proBdz;
    private String gssbQty;
    private String proIfpart;
    private String proPartUint;
    private String proPartRate;
    private String proCompclass;
    private String proPresclass;
    private String proTqtsts;




}