package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2021/3/22
 */
@Data
public class AppParameterVo implements Serializable {
    private static final long serialVersionUID = 2880506867698705349L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String brId;

    /**
     * 销售单号
     */
    private String gsshBillNo;

    /**
     * 销售时间
     */
    private String gsshDate;


    /**
     * 销售码
     */
    private String salesCode;

    /**
     * app销售单
     */
    private String appSalesSlip;

    /**
     * 收银员
     */
    private String cashier;

    /**
     * 收银时间
     */
    private String cashierTime;

    /**
     * 起始日期
     */
    private String startDate;

    /**
     * 结束日期
     */
    private String endDate;

    /**
     * 会员id
     */
    private String memberId;

    /**
     * 应收金额
     */
    private String amountReceivable;

    /**
     * 状态
     */
    private String status;

    /**
     * 商品编码
     */
    private String proCode;

}
