package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.PageInfo;
import com.gys.common.data.SupplierInfoDTO;
import com.gys.common.response.Result;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author wavesen.shen
 */
public interface SalesSummaryOfSalesmenReportService {
    /***
     *
     * @param inData
     * @return
     */
    PageInfo<GetSalesSummaryOfSalesmenReportOutData> queryReport(@RequestBody GetSalesSummaryOfSalesmenReportInData inData);

    List<GetEmpOutData> getUserListByClient(String clientId);

    Map<String,Object> querySalesSummaryOfSalesmenList(@RequestBody GetSalesSummaryOfSalesmenReportInData inData);

    List<Map<String,Object>> selectSaleList(Map<String,Object> inData);

    PageInfo getSalespersonsSalesDetails(GetSalesSummaryOfSalesmenReportInData inData);

    PageInfo getSalespersonsSalesDetailsByClient(GetSalesSummaryOfSalesmenReportInData inData);


    List<SalespersonsSalesDetailsOutData> getSalespersonsSalesByPro(GetSalesSummaryOfSalesmenReportInData inData);

    List<SalespersonsSalesDetailsOutData> getSalespersonsSalesByUser(GetSalesSummaryOfSalesmenReportInData inData);
    List<SalespersonsSalesDetailsOutData> getSalespersonsSalesByDoctor(GetSalesSummaryOfSalesmenReportInData inData);


    PageInfo unionQueryReport(GetSalesSummaryOfSalesmenReportInData inData);

    Result unionQueryExport(GetSalesSummaryOfSalesmenReportInData inData);

    /**
     * 商品销售明细表导出
     * @param inData
     * @param response
     * @return
     */
    void exportSalespersonsSalesDetails(GetSalesSummaryOfSalesmenReportInData inData, HttpServletResponse response);

    List<SupplierInfoDTO> getSupByClient(String client);
}
