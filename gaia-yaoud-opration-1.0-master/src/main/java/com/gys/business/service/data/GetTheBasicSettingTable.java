package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 获得电子券基础设置表id
 *
 * @author xiaoyuan on 2020/9/28
 */
@Data
public class GetTheBasicSettingTable implements Serializable {
    private static final long serialVersionUID = -2606686163676932325L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String gsebsBrId;

    /**
     * 自增id
     */
    private String gsebsIds;
}
