package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class GetProductNewInData {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店")
    private String proSite;
    @ApiModelProperty(value = "查询内容")
    private String content;
    @NotNull
    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "查询条件")
    private List<ProSearchConditionInfo> searchConditionInfos;

    @ApiModelProperty(value = "查询条件")
    private List<StringBuilder> strSqls;
//    @ApiModelProperty(value = "需要查询的列")
//    private List<String> lableArr;

}
