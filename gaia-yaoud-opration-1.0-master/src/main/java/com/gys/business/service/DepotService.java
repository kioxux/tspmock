//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface DepotService {
    PageInfo<GetDepotOutData> selectList(GetDepotInData inData);

    GetDepotOutData detail(GetDepotInData inData);

    List<GetDepotDetailOutData> detailList(GetDepotInData inData);

    String insert(GetDepotInData inData, GetLoginOutData userInfo);

    void update(GetDepotInData inData, GetLoginOutData userInfo);

    void approve(GetDepotInData inData);

    PageInfo<GetQueryProVoOutData> queryPro(GetProductInData inData);

    void approveDepot(GetWfApproveInData inData);

    List<GetQueryProVoOutData> exportIn(GetDepotInData inData);

    GetDepotPoDetailOutData getPoDetail(GetDepotInData inData);

    List<GetStockBatchProOutData> getStockBatchPro(GetStockBatchProInData inData);
}
