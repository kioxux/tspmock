package com.gys.business.service.data.disease;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class MemberLabelResponse {


    @ApiModelProperty(value = "类型0成分标签1疾病标签")
    private String type;

    @ApiModelProperty(value = "会员卡号")
    private String cardId;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "门店")
    private String brId;

    @ApiModelProperty(value = "销售单号")
    private String billNo;

    @ApiModelProperty(value = "时间")
    private String gssdDate;

    @ApiModelProperty(value = "金额")
    private String amt;

    @ApiModelProperty(value = "商品成分编码")
    private String proCompClass;


    @ApiModelProperty(value = "疾病编码")
    private String diseaseCode;


    @ApiModelProperty(value = "成分编码（用不到）")
    private String classificationCode;

    List<MemberLabelResponse> memberLabelList;

}
