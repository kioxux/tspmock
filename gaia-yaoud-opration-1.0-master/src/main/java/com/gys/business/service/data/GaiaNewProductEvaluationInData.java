package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class GaiaNewProductEvaluationInData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店")
    private String storeId;

    private String billCode;
    private String dealStatus;
    private String status;
    private String type;
    private String[] proCompBigCode;

}
