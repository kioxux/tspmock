package com.gys.business.service.data;

import lombok.Data;

@Data
public class ZydjSaleH {
    private int index;
    private String client;
    private String brId;
    private String billNo;
    private String saleDate;
    private String saleTime;
    private String saleAmt;
    private String memberCard;
    private String memberName;
    private String memberSex;
    private String memberAge;
    private String phone;
    private String pid;
    private String createDate;
    private String status;
    private String status2;
    private String salerId;
    private String saler;
    private String supplierCode;
    private String supplierName;
    private String gsshBillNoReturn;
    private String address;
    private String quantity;
    private String doctor;
    private String memberId;
}
