package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class MemberCardOutData implements Serializable {
    private static final long serialVersionUID = 3473621368127099807L;
    private String clientId;
    private String clientName;
    private String gsmbCardId;
    private String gsmbName;
    private String gsmbMobile;
    private String gsmbSex;
    private String gsmbTel;
    private String gsmbAddress;
}
