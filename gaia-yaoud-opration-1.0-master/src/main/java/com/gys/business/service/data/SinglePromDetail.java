package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author shentao
 */
@Data
public class SinglePromDetail {
    private String serial;
    private String proCode;
    private String proName;
    private String proCommonName;
    private BigDecimal normalPrice;
    private BigDecimal reachQty;
    //折扣：dicount；促销价：price
    private String preferenceType;
    private BigDecimal preference;
    private BigDecimal repeatNum;
}
