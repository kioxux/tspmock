package com.gys.business.service;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/23 00:18
 */
public interface DcOutStockJobService {

    void execJob();

}
