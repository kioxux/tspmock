package com.gys.business.service;

import com.gys.business.service.data.GaiaLicenseQualificationInData;
import com.gys.common.data.PageInfo;

import java.util.List;
import java.util.Map;

public interface LicenseQualificationService {
    List<Map<String,String>> listSite(Map<String,String> queryMap);

    PageInfo<GaiaLicenseQualificationOutData> list(GaiaLicenseQualificationInData inData);

    List<Map<String,String>> listSup(Map<String, String> queryMap);

    List<Map<String,String>> listCus(Map<String, String> queryMap);

    void dailyInsertToMessage();

    List<Map<String,String>> listSto(Map<String, String> queryMap);
}
