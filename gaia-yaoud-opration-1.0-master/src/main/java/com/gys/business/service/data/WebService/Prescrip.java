package com.gys.business.service.data.WebService;

import lombok.Data;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.List;

@Data
public class Prescrip {
    private String uniqueId;
    //处方编号
    private String pid;
    //患者
    private String patient;
    //性别
    private String gender;
    //年龄
    private String age;
    //联系电话
    private String phone;
    //地址
    private String address;
    //诊断
    private String diagnosis;
    //医生
    private String doctor;
    //药品名称
    private String drugName;
    //剂型
    private String forms;
    //数量
    private String quantity;
    //用法用量
    private String dosage;
    //开方日期
    private String date;
    //发运方式：1：患者自取 3：EMS到付 4：EMS寄付 5：顺丰到付 6：顺丰寄付
    private String ship;
    //加工方式：JG00201
    private String type;
    //加工单价
    private String price;
    //处方状态：审核付款(院方)Y/N
    private String status;
    //备注：
    private String remarks;
    //收件人
    private String tcmUse;
    //门诊号
    private String opEmHpNo;
    //住院号
    private String rxCode;
    //病区+床位号
    private String tcmDecoct;
    //组套编码
    private String docIdCard;
    //物料
    private List<PrescripMaterial> materials;

    public String ParseXml() {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("Prescrip");
        root.addElement("PID").addText(getPid()==null?"":getPid());
        root.addElement("Patient").addText(getPatient()==null?"":getPatient());
        root.addElement("Gender").addText(getGender()==null?"":getGender());
        root.addElement("Age").addText(getAge()==null?"":getAge());
        root.addElement("Phone").addText(getPhone()==null?"":getPhone());
        root.addElement("Address").addText(getAddress()==null?"":getAddress());
        root.addElement("Diagnosis").addText(getDiagnosis()==null?"":getDiagnosis());
        root.addElement("Doctor").addText(getDoctor()==null?"":getDoctor());
        root.addElement("DrugName").addText(getDrugName()==null?"":getDrugName());
        root.addElement("Forms").addText(getForms()==null?"":getForms());
        root.addElement("Quantity").addText(getQuantity()==null?"":getQuantity());
        root.addElement("Dosage").addText(getDosage()==null?"":getDosage());
        root.addElement("Date").addText(getDate()==null?"":getDate());
        root.addElement("Ship").addText(getShip()==null?"":getShip());
        root.addElement("Price").addText(getPrice()==null?"":getPrice());
        root.addElement("Status").addText(getStatus()==null?"":getStatus());
        root.addElement("Type").addText(getType()==null?"":getType());
        root.addElement("Remarks").addText(getRemarks()==null?"":getRemarks());
        root.addElement("TCM_USE").addText(getTcmUse()==null?"":getTcmUse());
        root.addElement("OP_EM_HP_NO").addText(getOpEmHpNo()==null?"":getOpEmHpNo());
        root.addElement("RX_CODE").addText(getOpEmHpNo()==null?"":getOpEmHpNo());
        root.addElement("TCM_DECOCT").addText(getTcmDecoct()==null?"":getTcmDecoct());
        root.addElement("DOC_IDCARD").addText(getDocIdCard()==null?"":getDocIdCard());
        for(PrescripMaterial item : materials){
            Element materialNode = root.addElement("Material");
            materialNode.addElement("CltItmID").addText(item.getCltItmId()==null?"":item.getCltItmId());
            materialNode.addElement("ItmID").addText(item.getItmID()==null?"":item.getItmID());
            materialNode.addElement("ItmName").addText(item.getItmName()==null?"":item.getItmName());
            materialNode.addElement("ItmSpec").addText(item.getItmSpec()==null?"":item.getItmSpec());
            materialNode.addElement("Place").addText(item.getPlace()==null?"":item.getPlace());
            materialNode.addElement("Unit").addText(item.getUnit()==null?"":item.getUnit());
            materialNode.addElement("Quantity").addText(item.getQuantity()==null?"":item.getQuantity());
            materialNode.addElement("Price").addText(item.getPrice()==null?"":item.getPrice());
            materialNode.addElement("FreeTxt").addText(item.getFreeTxt()==null?"":item.getFreeTxt());
        }
        return document.asXML();
    }
}
