package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GaiaSdDefecProHInData {

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "地点")
    private String brId;

    @ApiModelProperty(value = "单号")
    private String gsdhVoucherId;

    @ApiModelProperty(value = "备注")
    private String gsdhRemaks;

    @ApiModelProperty(value = "人员")
    private String gsdhEmp;

    private String token;

    List<GaiaSdDefecProDInData> gaiaSdDefecProDInData;

}
