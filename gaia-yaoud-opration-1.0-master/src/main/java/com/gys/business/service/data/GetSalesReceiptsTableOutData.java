package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan
 */
@Data
public class GetSalesReceiptsTableOutData implements Serializable {
    private static final long serialVersionUID = -2865374130757232008L;
    private String proCode;
    private String num;
    private String index;
    private String total;
    private String price;
    private String proUnit;
    private String prcAmount;
    private String disc;
    private String proName;
    private String proCommonName;
    private String status;
    private String proCompclass;
    private String storageArea;
    private String proPym;
    private String proUsage;
    private String proContraindication;
    private String proControlClass;
    private String proPresclass;
    private String proSpecs;
    private String proFactoryName;
    private String proPlace;
    private String proMedProdct;
    private String proIfPart;
    private String gsplSeat;
    private String gssdStCode;
    private String empName;
    private String empId;
    private String doctorName;//医生名称
    private String doctorId;
    private String gssdBatchNo;
    private String gssdValidDate;
    private String gssdSalerId;//营业员id 要显示名称
    private String gssdSalerName;//营业员名称
    private String gssdPmId;//促销单号id
    private String gssdPmActivityId;//促销单号id
    private String gssdPmActivityFlag;//促销参数
    private GetPdAndExpOutData pdAndExpData;
    private List<GetPdAndExpOutData> pdAndExpOutDataList;
    private List<GetPromByProCodeOutData> promByProCodeOutDataList;
    private GetPromByProCodeOutData selectPromByProCodeOutData;
    private String gsphExclusion;
    private String isGift;
    private SaleActInfo saleActInfo;
    /**
     * 初始值数量
     */
    private String gssdOriQty;

    /**
     * 贴数
     */
    private String gssdDose;

    /**
     * 商品定位
     */
    private String proPosition;

    /**
     * 税率值
     */
    private String taxCodeValue;

    /**
     * 医保限量
     */
    private String proMedQty;
    /**
     * 拆零单位
     */
    private String proPartUint;
    /**
     * 拆零比率
     */
    private String proPartRate;
    /**
     *  会员价
     */
    private String prcHy;
    /**
     * 销项税率
     */
    private String proOutputTax;
    /**
     * 销项税率
     */
    private String outputTaxValue;
    /**
     * 国家医保编码
     */
    private String proMedProdctcode;

    /**
     * 医生编码
     */
    private String gssdDoctorId;


    /**
     * 状态
     */
    private String proStatus;

    /**
     * 库存
     */
    private String gssQty;
    /**
     * 医保编码
     */
    private String proIfMed;
    /**
     * 等级
     */
    private String saleClass;
    /**
     * 不打折 1：是
     */
    private String proBdz;

    /**
     * 当前商品批号总存库
     */
    private String batchNoQty;

    /**
     * 特殊药品购买人身份证
     */
    private String gssdSpecialmedIdcard;

    /**
     * 特殊药品购买人姓名
     */
    private String gssdSpecialmedName;

    /**
     * 特殊药品购买人性别
     */
    private String gssdSpecialmedSex;

    /**
     * 特殊药品购买人出生日期
     */
    private String gssdSpecialmedBirthday;

    /**
     * 特殊药品购买人手机
     */
    private String gssdSpecialmedMobile;

    /**
     * 特殊药品购买人地址
     */
    private String gssdSpecialmedAddress;

    /**
     * 特殊药品电子监管码
     */
    private String gssdSpecialmedEcode;

    /**
     * 特殊属性：1-防疫
     */
    private String proTssx;

    /**
     * 特殊分类是否开启 0 否 1 是
     */
    private String specialClassSet;

}
