package com.gys.business.service.takeAway.impl;

import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.takeAway.BaseMapperPTS;
import com.gys.business.service.takeAway.BaseServicePTS;
import com.gys.common.config.takeAway.SwitchConfig;
import com.gys.util.ConfigUtil;
import com.gys.util.takeAway.eb.EbApiUtil;
import com.gys.util.takeAway.jd.JdApiUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("BaseServicePTS")
@Transactional("transactionManager_pts")
public class BaseServicePTSImpl implements BaseServicePTS {
    private static final Logger logger = LoggerFactory.getLogger(BaseServiceImpl.class);
    private BaseMapperPTS baseMapperPTS;

    public BaseMapperPTS getBaseMapperPTS() {
        return baseMapperPTS;
    }

    @Autowired
    public void setBaseMapperPTS(BaseMapperPTS baseMapperPTS) {
        this.baseMapperPTS = baseMapperPTS;
    }

    @Override
    public List<Map<String, Object>> selectShopInfoByMap(String buid) {
        return baseMapperPTS.selectShopInfoByMap(buid);
    }

    @Override
    public List<Map<String, Object>> queryMedicine(String shop_no) {
        List<Map<String, Object>> list = null;
        try {
            list = baseMapperPTS.queryMedicine(shop_no);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return list;
    }

    @Override
    public Map<String, Object> updateMedicineInfo(String shop_no, Map<String, Object> param) {
        Map<String, Object> result = new HashMap<>();
        String appId = String.valueOf(ConfigUtil.getshop(shop_no).get("MT").get("app_id"));
        String appSecret = String.valueOf(ConfigUtil.getshop(shop_no).get("MT").get("app_secret"));
        String source = String.valueOf(ConfigUtil.getshop(shop_no).get("EB").get("source"));
        String secret = String.valueOf(ConfigUtil.getshop(shop_no).get("EB").get("secret"));
        String app_key = String.valueOf(ConfigUtil.getshop(shop_no).get("JD").get("app_key"));
        String app_secret = String.valueOf(ConfigUtil.getshop(shop_no).get("JD").get("app_secret"));
        String token = String.valueOf(ConfigUtil.getshop(shop_no).get("JD").get("token"));

        String appPoiCode = shop_no;
        Map<String, String> catUpdateMap = new HashMap<>();

        List<Map<String, Object>> shopMedicineList = (List<Map<String, Object>>) param.get("list");

        //region 开始饿百业务处理
        if (!SwitchConfig.checkMedicineSwitch(shop_no, "EB")) {
            logger.info("饿百门店：[" + shop_no + "]药品更新开关未打开！");
        } else {
            logger.info("开始更新饿百门店：[" + shop_no + "]药品信息！");
            //todo 具体执行事务
            //获取药品分类清单，
            //对比药品分类清单，不存在的则创建，并建立对应关系。
            //需要新增药品分类清单，对应的平台，对应关系表。

            //药品分类关系处理完成后开始更新药品信息。
            //获取平台药品清单，不存在的则创建，存在的则更新。

            //根据线上药品价格清单，直接更新库存。
            List<Map<String, Object>> ebMedicineList = EbApiUtil.getmedicineList(source, secret, appPoiCode);
            Map<String, Object> queryMap = new HashMap<>();
            List<Map<String, String>> queryItemList = new ArrayList<>();
            queryMap.put("shop_no", appPoiCode);
            queryMap.put("list", queryItemList);
            for (Map<String, Object> medicineMap : ebMedicineList) {
                //每50个产品，提交一次库存更新。
                if (medicineMap.get("custom_sku_id") != null) {
                    Map<String, String> queryItem = new HashMap<>();
                    queryItem.put("medicine_code", String.valueOf(medicineMap.get("custom_sku_id")));
                    queryItemList.add(queryItem);
                    if (queryItemList.size() >= 50) {
                        List<Map<String, Object>> ptsmedicineList = baseMapperPTS.queryMedicineStock(queryMap);
                        StringBuilder upcsb = new StringBuilder();
                        for (Map<String, Object> m : ptsmedicineList) {
                            String stock = String.valueOf(m.get("STOCK"));
                            if (Double.valueOf(stock) < 0) {
                                stock = "0";
                            }
                            upcsb.append(String.valueOf(m.get("MEDICINE_CODE"))).append(":").append(stock).append(";");
                        }
                        String streb = upcsb.toString();
                        if (!streb.equals("")) {
                            streb = streb.substring(0, streb.length() - 1);
                        }
                        String rev = EbApiUtil.medicineStock(source, secret, appPoiCode, streb);
                        Map revmap = (Map) JSON.parse(rev);
                        Map revbody = (Map) revmap.get("body");
                        if (!String.valueOf(revbody.get("errno")).equals("0")) {
                            logger.info("饿百更新库存：" + String.valueOf(revbody.get("error")));
                        }
                        queryItemList.clear();
                    }
                }
            }
            if (queryItemList.size() > 0) {
                List<Map<String, Object>> ptsmedicineList = baseMapperPTS.queryMedicineStock(queryMap);
                StringBuilder upcsb = new StringBuilder();
                for (Map<String, Object> m : ptsmedicineList) {
                    String stock = String.valueOf(m.get("STOCK"));
                    if (Double.valueOf(stock) < 0) {
                        stock = "0";
                    }
                    upcsb.append(String.valueOf(m.get("MEDICINE_CODE"))).append(":").append(stock).append(";");
                }
                String streb = upcsb.toString();
                if (!streb.equals("")) {
                    streb = streb.substring(0, streb.length() - 1);
                }
                String rev = EbApiUtil.medicineStock(source, secret, appPoiCode, streb);
                Map revmap = (Map) JSON.parse(rev);
                Map revbody = (Map) revmap.get("body");
                if (!String.valueOf(revbody.get("errno")).equals("0")) {
                    logger.info("饿百更新零头库存：" + String.valueOf(revbody.get("error")));
                }
                queryItemList.clear();
            }
        }
        //endregion

        //region 开始京东业务处理
        if (!SwitchConfig.checkMedicineSwitch(shop_no, "JD")) {
            logger.info("京东门店：[" + shop_no + "]药品更新开关未打开！");
        } else {
            logger.info("开始更新京东门店：[" + shop_no + "]药品信息！");
            //todo 具体执行事务

            List<Map<String, Object>> ebMedicineList = JdApiUtil.getmedicineList(app_key, app_secret, token, appPoiCode);
            Map<String, Object> queryMap = new HashMap<>();
            List<Map<String, String>> queryItemList = new ArrayList<>();
            queryMap.put("shop_no", appPoiCode);
            queryMap.put("list", queryItemList);
            for (Map<String, Object> medicineMap : ebMedicineList) {
                //每50个产品，提交一次库存更新。
                if (medicineMap.get("outSkuId") != null) {
                    Map<String, String> queryItem = new HashMap<>();
                    queryItem.put("medicine_code", String.valueOf(medicineMap.get("outSkuId")));
                    queryItemList.add(queryItem);
                    if (queryItemList.size() >= 50) {
                        List<Map<String, Object>> ptsmedicineList = baseMapperPTS.queryMedicineStock(queryMap);
                        List<Map<String, Object>> medicineInfoList = new ArrayList<>();
                        for (Map<String, Object> m : ptsmedicineList) {
                            String stock = String.valueOf(m.get("STOCK"));
                            if (Double.valueOf(stock) < 0) {
                                stock = "0";
                            }
                            Map<String, Object> medicineInfo = new HashMap<>();
                            medicineInfo.put("outSkuId", String.valueOf(m.get("MEDICINE_CODE")));
                            medicineInfo.put("stockQty", Double.valueOf(stock).intValue());
                            medicineInfoList.add(medicineInfo);
                        }
                        Map<String, Object> inputparam = new HashMap<>();
                        inputparam.put("outStationNo", appPoiCode);
                        inputparam.put("userPin", "service");
                        inputparam.put("skuStockList", medicineInfoList);
                        String rev = JdApiUtil.medicineStock(app_key, app_secret, token, appPoiCode, inputparam);
                        Map revmap = (Map) JSON.parse(rev);
                        if (!String.valueOf(revmap.get("code")).equals("0")) {
                            logger.info("京东更新库存：" + String.valueOf(revmap.get("msg")));
                        }
                        queryItemList.clear();
                    }
                }
            }
            if (queryItemList.size() > 0) {
                List<Map<String, Object>> ptsmedicineList = baseMapperPTS.queryMedicineStock(queryMap);
                List<Map<String, Object>> medicineInfoList = new ArrayList<>();
                for (Map<String, Object> m : ptsmedicineList) {
                    Map<String, Object> medicineInfo = new HashMap<>();
                    medicineInfo.put("outSkuId", String.valueOf(m.get("MEDICINE_CODE")));
                    medicineInfo.put("stockQty", Double.valueOf(String.valueOf(m.get("STOCK"))).intValue());
                    medicineInfoList.add(medicineInfo);
                }
                Map<String, Object> inputparam = new HashMap<>();
                inputparam.put("outStationNo", appPoiCode);
                inputparam.put("userPin", "service");
                inputparam.put("skuStockList", medicineInfoList);
                String rev = JdApiUtil.medicineStock(app_key, app_secret, token, appPoiCode, inputparam);
                Map revmap = (Map) JSON.parse(rev);
                if (!String.valueOf(revmap.get("code")).equals("0")) {
                    logger.info("京东更新零头库存：" + String.valueOf(revmap.get("msg")));
                }
                queryItemList.clear();
            }
        }
        //endregion

        return result;
    }

    /**
     * 查询pts药品库存
     *
     * @param param
     * @return
     * @author：Li.Deman
     * @date：2019/3/11
     */
    @Override
    public List<Map<String, Object>> queryMedicineStock(Map<String, Object> param) {
        List<Map<String, Object>> list = null;
        try {
            list = baseMapperPTS.queryMedicineStock(param);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return list;
    }

    //查询有变化的商品基础信息
    @Override
    public List<Map<String, Object>> queryModifyMedicine(Map<String, Object> param) {
        return baseMapperPTS.queryModifyMedicine(param);
    }

    //更新PTS内数据的读写标记，标识数据已经被处理了
    @Override
    public String updateMedicineO2OStatus(Map<String, Object> param) {
        String result;
        try {
            if (((List<Map<String, Object>>) param.get("list")).size() > 0) {
                baseMapperPTS.updateMedicineO2OStatus(param);
            }
            result = "ok";
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(String.valueOf(ex));
            result = "ERROR:" + ex.getMessage();
        }
        return result;
    }

    @Override
    public List<Map<String,Object>> queryMedicineStockByUpc(Map<String, Object> param){
        return baseMapperPTS.queryMedicineStockByUpc(param);
    }
}
