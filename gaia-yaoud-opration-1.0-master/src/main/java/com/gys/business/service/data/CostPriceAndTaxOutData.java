package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CostPriceAndTaxOutData implements Serializable {


    /**
     * 门店编码+商品自编码拼接字符串
     */
    @ApiModelProperty(value="门店编码+商品自编码拼接字符串")
    private String storeProSelfCodeStr;

    /**
     * 成本价
     */
    @ApiModelProperty(value="成本价")
    private BigDecimal costPrice;

    /**
     * 税率
     */
    @ApiModelProperty(value="税率")
    private String taxCodeValue;
}
