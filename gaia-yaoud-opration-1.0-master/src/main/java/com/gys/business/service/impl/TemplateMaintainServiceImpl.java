package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.TemplateMaintainMapper;
import com.gys.business.service.TemplateMaintainService;
import com.gys.common.data.wechatTemplate.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TemplateMaintainServiceImpl implements TemplateMaintainService {

    @Autowired
    private TemplateMaintainMapper templateMaintainMapper;

    @Override
    public PageInfo<GaiaMouldMaintainVO> queryTemplateList(String mouldName, String status, Integer pageNum, Integer pageSize) {
        ArrayList<GaiaMouldMaintainVO> gaiaMouldMaintainVOS = new ArrayList<>();
        List<GaiaMouldMaintainDTO> gaiaMouldMaintainDTOS = templateMaintainMapper.queryAllTemplate(mouldName, status, pageNum*pageSize-pageSize, pageSize);
        if (gaiaMouldMaintainDTOS.size()>0){
            for (GaiaMouldMaintainDTO gaiaMouldMaintainDTO : gaiaMouldMaintainDTOS) {
                GaiaMouldMaintainVO gaiaMouldMaintainVO = new GaiaMouldMaintainVO();
                BeanUtils.copyProperties(gaiaMouldMaintainDTO,gaiaMouldMaintainVO);
                gaiaMouldMaintainVO.setExample(Arrays.asList(gaiaMouldMaintainDTO.getExample().substring(1, gaiaMouldMaintainDTO.getExample().length() - 1).replaceAll("\\s", "").split(",")));
                if (!ObjectUtils.isEmpty(gaiaMouldMaintainVO.getGaiaMouldMaintainDS())){
                    Collections.sort(gaiaMouldMaintainVO.getGaiaMouldMaintainDS());
                }
                gaiaMouldMaintainVOS.add(gaiaMouldMaintainVO);
            }
        }
        PageInfo pageInfo = new PageInfo(gaiaMouldMaintainVOS);
        pageInfo.setPageNum(pageNum);
        pageInfo.setPageSize(pageSize);
        pageInfo.setTotal(templateMaintainMapper.countStartTemplate(status));
        pageInfo.setList(gaiaMouldMaintainVOS);
        return pageInfo;
    }


    @Override
    @Transactional
    public void insertTemplate(String client, GaiaMouldMaintainVO gaiaMouldMaintainVO) {
        GaiaMouldMaintainH gaiaMouldMaintainH = new GaiaMouldMaintainH();
        BeanUtils.copyProperties(gaiaMouldMaintainVO, gaiaMouldMaintainH);
        gaiaMouldMaintainH.setMouldCode(countMouldCode());
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        gaiaMouldMaintainH.setCreateDate(DateUtil.format(new Date(), format));
        gaiaMouldMaintainH.setCreateClient(client);
        gaiaMouldMaintainH.setDelFlag("0");
        gaiaMouldMaintainH.setExample(gaiaMouldMaintainVO.getExample().toString());
        templateMaintainMapper.insertTemplateH(gaiaMouldMaintainH);
        ArrayList<GaiaMouldMaintainD> gaiaMouldMaintainDs = gaiaMouldMaintainVO.getGaiaMouldMaintainDS();
        if (!ObjectUtils.isEmpty(gaiaMouldMaintainDs)) {
            int lineNo = 1;
            for (GaiaMouldMaintainD gaiaMouldMaintainDDTO : gaiaMouldMaintainDs) {
                GaiaMouldMaintainD gaiaMouldMaintainD = new GaiaMouldMaintainD();
                BeanUtils.copyProperties(gaiaMouldMaintainDDTO, gaiaMouldMaintainD);
                gaiaMouldMaintainD.setLineNo(lineNo);
                gaiaMouldMaintainD.setMouldCode(gaiaMouldMaintainH.getMouldCode());
                gaiaMouldMaintainD.setDelFlag("0");
                templateMaintainMapper.insertTemplateD(gaiaMouldMaintainD);
                lineNo++;
            }
        }

    }

    @Override
    @Transactional
    public void delTemplate(String mouldCode) {
        templateMaintainMapper.delTemplateH(mouldCode);
        templateMaintainMapper.delTemplateD(mouldCode);
    }


    @Override
    public void startTemplate(String mouldCode) {
        // 状态（0：停用，1：启用）
        String status = "1";
        templateMaintainMapper.updateTemplateStatus(mouldCode, status);
    }


    @Override
    public void stopTemplate(String mouldCode) {
        // 状态（0：停用，1：启用）
        String status = "0";
        templateMaintainMapper.updateTemplateStatus(mouldCode, status);
    }


    @Override
    @Transactional
    public void updateTemplate(GaiaMouldMaintainVO gaiaMouldMaintainVO) {
        // 更新主表
        GaiaMouldMaintainH gaiaMouldMaintainH = new GaiaMouldMaintainH();
        BeanUtils.copyProperties(gaiaMouldMaintainVO, gaiaMouldMaintainH);
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        gaiaMouldMaintainH.setUpdateDate(DateUtil.format(new Date(), format));
        gaiaMouldMaintainH.setExample(gaiaMouldMaintainVO.getExample().toString());
        templateMaintainMapper.updateTemplateH(gaiaMouldMaintainH);
        // 删除明细表内容
        templateMaintainMapper.delTemplateD(gaiaMouldMaintainVO.getMouldCode());
        // 更新明细表
        ArrayList<GaiaMouldMaintainD> gaiaMouldMaintainDS = gaiaMouldMaintainVO.getGaiaMouldMaintainDS();
        if (!ObjectUtils.isEmpty(gaiaMouldMaintainDS)) {
            int lineNo = 1;
            for (GaiaMouldMaintainD gaiaMouldMaintainDDTO : gaiaMouldMaintainDS) {
                GaiaMouldMaintainD gaiaMouldMaintainD = new GaiaMouldMaintainD();
                BeanUtils.copyProperties(gaiaMouldMaintainDDTO, gaiaMouldMaintainD);
                gaiaMouldMaintainD.setMouldCode(gaiaMouldMaintainH.getMouldCode());
                gaiaMouldMaintainD.setLineNo(lineNo);
                templateMaintainMapper.insertTemplateD(gaiaMouldMaintainD);
                lineNo++;
            }
        }
    }


    @Override
    public GaiaMouldMaintainVO queryTemplateByCode(String mouldCode) {
        GaiaMouldMaintainVO gaiaMouldMaintainVO = new GaiaMouldMaintainVO();
        GaiaMouldMaintainDTO gaiaMouldMaintainDTO = templateMaintainMapper.queryTemplateByCode(mouldCode);
        BeanUtils.copyProperties(gaiaMouldMaintainDTO,gaiaMouldMaintainVO);
        List<String> strings = Arrays.asList(gaiaMouldMaintainDTO.getExample().substring(1, gaiaMouldMaintainDTO.getExample().length() - 1).replaceAll("\\s", "").split(","));
        gaiaMouldMaintainVO.setExample(strings);
        if (!ObjectUtils.isEmpty(gaiaMouldMaintainVO.getGaiaMouldMaintainDS())){
            Collections.sort(gaiaMouldMaintainVO.getGaiaMouldMaintainDS());
        }
        return gaiaMouldMaintainVO;
    }

    // 返回模板编码：YD-XXXX
    private String countMouldCode() {
        String count = templateMaintainMapper.countTemplate().toString();
        while (count.length() < 3) {
            count = "0" + count;
        }
        return "YD-" + count;
    }


}
