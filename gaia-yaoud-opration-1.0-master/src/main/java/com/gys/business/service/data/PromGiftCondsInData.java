package com.gys.business.service.data;


import lombok.Data;

import java.io.Serializable;

@Data
public class PromGiftCondsInData implements Serializable {
    private static final long serialVersionUID = -2837959181671623663L;
    private String clientId;
    private String gspgcVoucherId;
    private String gspgcSerial;
    private String gspgcProId;
    private String gspgcSeriesId;
    private String gspgcMemFlag;
    private String gspgcInteFlag;
    private String gspgcInteRate;
}
