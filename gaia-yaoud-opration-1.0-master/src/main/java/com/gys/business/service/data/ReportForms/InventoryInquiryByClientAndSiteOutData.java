package com.gys.business.service.data.ReportForms;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class InventoryInquiryByClientAndSiteOutData implements Serializable {
    private static final long serialVersionUID = 8458845923235734876L;
    @ApiModelProperty(value = "加盟商")
    @ExcelIgnore
    private String clientId;

    @ApiModelProperty(value = "店号")
    @ExcelProperty(value = "店号", index = 0)
    private String gssmBrId;

    @ApiModelProperty(value = "序号")
    @ExcelIgnore
    private Integer index;
    @ApiModelProperty(value = "入库日期")
    @ExcelIgnore
    private String inDate;
    @ApiModelProperty(value = "地点")
    @ExcelProperty(value = "地点", index = 1)
    private String gssBrName;

    @ApiModelProperty(value = "商品编号")
    @ExcelProperty(value = "商品编号", index = 2)
    private String gssmProId;

    @ApiModelProperty(value = "商品名")
    @ExcelProperty(value = "商品名称", index = 4)
    private String gssmProName;
    @ApiModelProperty(value = "国际条形码")
    @ExcelProperty(value = "国际条形码",index = 3)
    private String proBarcode;
    @ApiModelProperty(value = "商品通用名称")
    @ExcelProperty(value = "商品通用名称", index = 5)
    private String  proCommonName;
    @ApiModelProperty(value = "自定义1")
    @ExcelProperty(value = "自定义1",index = 31)
    private String zdy1;
    @ApiModelProperty(value = "自定义2")
    @ExcelProperty(value = "自定义2",index = 32)
    private String zdy2;
    @ApiModelProperty(value = "自定义3")
    @ExcelProperty(value = "自定义3",index = 33)
    private String zdy3;
    @ApiModelProperty(value = "自定义4")
    @ExcelProperty(value = "自定义4",index = 34)
    private String zdy4;
    @ApiModelProperty(value = "自定义5")
    @ExcelProperty(value = "自定义5",index = 35)
    private String zdy5;
    @ApiModelProperty(value = "批号")
    @ExcelIgnore
    private String gssmBatchNo;

    @ApiModelProperty(value = "有效期至")
    @ExcelIgnore
    private String validUntil;

    @ApiModelProperty(value = "去税成本价")
    @ExcelIgnore
    private BigDecimal matMovPrice;

    @ApiModelProperty(value = "去税成本额")
    @ExcelIgnore
    private BigDecimal costAmount;

    @ApiModelProperty(value = "含税成本价")
    @ExcelIgnore
    private BigDecimal matMovRatePrice;

    @ApiModelProperty(value = "含税成本额")
    @ExcelIgnore
    private BigDecimal costRateAmount;

    @ApiModelProperty(value = "加点后含税成本价")
    @ExcelProperty(value = "成本价",index = 17)
    private BigDecimal addPrice;

    @ApiModelProperty(value = "加点后含税成本额")
    @ExcelProperty(value = "成本额",index = 18)
    private BigDecimal addAmount;

    @ApiModelProperty(value = "生产厂家")
    @ExcelProperty(value = "生产厂家", index = 11)
    private String factory;

    @ApiModelProperty(value = "产地")
    @ExcelProperty(value = "产地",index = 12)
    private String origin;

    @ApiModelProperty(value = "剂型")
    @ExcelProperty(value = "剂型",index = 13)
    private String dosageForm;

    @ApiModelProperty(value = "单位")
    @ExcelProperty(value = "单位",index = 14)
    private String unit;

    @ApiModelProperty(value = "规格")
    @ExcelProperty(value = "规格",index = 15)
    private String format;

    @ApiModelProperty(value = "批准文号")
    @ExcelProperty(value = "批准文号",index = 16)
    private String approvalNum;

    @ApiModelProperty(value = "总数量")
    @ExcelProperty(value = "库存",index = 6)
    private BigDecimal qty;
    @ApiModelProperty(value = "商品大类编码")
    @ExcelProperty(value = "商品大类编码",index = 19)
    private String bigClass;
    @ApiModelProperty(value = "商品中类编码")
    @ExcelProperty(value = "商品中类编码",index = 20)
    private String midClass;
    @ApiModelProperty(value = "商品分类编码")
    @ExcelProperty(value = "商品分类编码",index = 21)
    private String proClass;
    @ApiModelProperty(value = "医保编码")
    @ExcelProperty(value = "医保编码",index = 24)
    private String medProdctCode;
    @ApiModelProperty(value = "商品定位")
    @ExcelProperty(value = "商品定位",index = 28)
    private String proPosition;
    @ApiModelProperty(value = "医保刷卡数量")
    @ExcelProperty(value = "医保刷卡数量", index = 25)
    private String proMedQty;
    @ApiModelProperty(value = "是否批发")
    @ExcelProperty(value = "是否批发",index = 26)
    private String isWholeSale;
    @ApiModelProperty(value = "是否医保")
    @ExcelProperty(value = "是否医保",index = 22)
    private String ifMed;
    @ApiModelProperty(value = "医保类型")
    @ExcelProperty(value = "医保类型",index = 23)
    private String yblx;
    @ApiModelProperty(value = "会员价")
    @ExcelProperty(value = "会员价",index = 9)
    private BigDecimal memberPrice;
    @ApiModelProperty(value = "会员日价")
    @ExcelProperty(value = "会员日价",index = 10)
    private BigDecimal memberDayPrice;
    @ApiModelProperty(value = "零售额")
    @ExcelProperty(value = "零售额", index = 7)
    private BigDecimal retailSales ;
    @ApiModelProperty(value = "效期天数")
    @ExcelIgnore
    private Integer expiryData;
    @ApiModelProperty(value = "零售价")
    @ExcelProperty(value = "零售价",index = 8)
    private BigDecimal retailPrice;
    @ApiModelProperty(value = "供应商编码")
    @ExcelIgnore
    private String supplierCode;
    @ApiModelProperty(value = "供应商名称")
    @ExcelIgnore
    private String supplierName;

    //商品自分类
    @ExcelProperty(value = "商品自分类",index = 29)
    private String prosClass;
    //销售级别
    @ExcelProperty(value = "销售级别",index = 27)
    private String saleClass;
    //禁止采购
    @ExcelProperty(value = "禁止采购",index = 30)
    private String purchase;

    /**
     * 门店属性
     */
    @ExcelProperty(value = "门店属性",index = 36)
    private String stoAttribute;

    /**
     * 店效级别
     */
    @ExcelProperty(value = "店效级别",index = 41)
    private String storeEfficiencyLevel;

    /**
     * 是否医保店
     */
    @ExcelProperty(value = "是否医保店",index = 37)
    private String stoIfMedical;

    /**
     * 纳税属性
     */
    @ExcelProperty(value = "纳税属性",index = 39)
    private String stoTaxClass;

    /**
     * 是否直营管理
     */
    @ExcelProperty(value = "是否直营管理",index = 42)
    private String directManaged;

    /**
     * DTP
     */
    @ExcelProperty(value = "DTP",index = 38)
    private String stoIfDtp;

    /**
     *管理区域
     */
    @ExcelProperty(value = "管理区域",index = 43)
    private String managementArea;

    /**
     * 店型
     */
    @ExcelProperty(value = "铺货店型",index = 40)
    private String shopType;

    @ExcelProperty(value = "业务员编码",index = 44)
    @ApiModelProperty(value = "业务员")
    private String saleCode;

    @ExcelProperty(value = "业务员姓名",index = 45)
    @ApiModelProperty(value = "业务员")
    private String saleName;

    /**
     * 生产日期
     */
    private String productDate;
}
