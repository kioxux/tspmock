package com.gys.business.service;


import com.gys.business.service.data.ReplenishmentRecordInData;
import com.gys.business.service.data.ReplenishmentRecordOutData;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface DocumentManagementService {

    /**
     * 补货记录查询
     * @param inData
     * @return
     */
    List<ReplenishmentRecordOutData> getReplenishmentRecord(ReplenishmentRecordInData inData);
}
