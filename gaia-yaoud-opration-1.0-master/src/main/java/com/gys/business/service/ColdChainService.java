

package com.gys.business.service;

import cn.hutool.json.JSONObject;
import com.gys.business.service.data.ColdChainInData;


import java.util.List;

public interface ColdChainService {

    List<JSONObject> listColdChainGoods(ColdChainInData inData);

}
