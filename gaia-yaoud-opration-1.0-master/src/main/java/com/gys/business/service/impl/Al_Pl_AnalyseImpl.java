package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaAlPlExecMapper;
import com.gys.business.mapper.entity.GaiaAlPlQjmd;
import com.gys.business.service.Al_Pl_Analyse;
import com.gys.common.kylin.RowMapper;
import com.gys.common.kylin.TestData;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Component
@Log4j2
public class Al_Pl_AnalyseImpl implements Al_Pl_Analyse {

    @Autowired
    GaiaAlPlExecMapper gaiaAlPlExecMapper;

    @Resource(name = "kylinJdbcTemplateFactory")
    private JdbcTemplate kylinJdbcTemplate;

    @Override
    public void execAnalyse(){

    }

    @Override
    public List<TestData> getDistinctIds() {
        StringBuilder sqlBuilder = new StringBuilder()
                .append(" select ")
                .append(" gssd_date as \"SaleDate\",count (distinct gssd_bill_no) as \"AvgBill\",sum(gssd_amt) as \"TotalAmt\",round(sum(gssd_amt)/count(distinct gssd_bill_no),2) \"AvgBillAmt\" ")
                .append(" FROM GAIA_SD_SALE_D ")
                .append(" group by gssd_date ");
        log.info(sqlBuilder);
        return kylinJdbcTemplate.query(sqlBuilder.toString(), RowMapper.getDefault(TestData.class));
    }

    @Override
    public List<GaiaAlPlQjmd> getSaleSummaryByArea(String area){
        GaiaAlPlQjmd inQjmd=new GaiaAlPlQjmd();
        inQjmd.setAplArea(area);
        List<GaiaAlPlQjmd> result= gaiaAlPlExecMapper.getSaleSummaryByArea(inQjmd);
        return result;
    }

    @Override
    public List<GaiaAlPlQjmd> getSaleInfoByComp(HashMap<String,Object> inHashMap){
        return gaiaAlPlExecMapper.getSaleInfoByComp(inHashMap);
    }


//    SELECT
//    GSSD.GSSD_DATE 销售日期,
//    GSSD.GSSD_PRO_ID 销售商品门店内部编码,
//    GPB.PRO_NAME 销售商品名称,
//    GPB.PRO_COMPCLASS 销售商品主成分,
//    SUM( GSSD.GSSD_QTY ) 销售数量,
//    SUM( GSSD.GSSD_AMT  ) 销售额,
//    CASE WHEN SUM(GSSD.GSSD_QTY)>0 THEN SUM( GSSD.GSSD_AMT  )/SUM( GSSD.GSSD_QTY )
//    WHEN SUM(GSSD.GSSD_QTY)<=9 THEN MAX(GSSD.GSSD_PRC2) end as 平均售价,
//    MAX(GSSD.GSSD_PRC2) 最高价,
//    MIN(GSSD.GSSD_PRC2) 最低价,
//    SUM(GSSD.GSSD_AMT)-SUM(GSSD.GSSD_MOV_PRICE) 毛利额,
//    CASE WHEN SUM(GSSD.GSSD_AMT) >0 THEN  (SUM(GSSD.GSSD_AMT)-SUM(GSSD.GSSD_MOV_PRICE))/SUM(GSSD.GSSD_AMT)*100
//    WHEN SUM(GSSD.GSSD_AMT)<=0 THEN  0  end as 毛利率
//            FROM
//    GAIA_SD_SALE_D GSSD
//    INNER JOIN GAIA_PRODUCT_BUSINESS GPB ON GSSD.CLIENT = GPB.CLIENT
//    AND GSSD.GSSD_BR_ID = GPB.PRO_SITE
//    AND GSSD.GSSD_PRO_ID = GPB.PRO_SELF_CODE
//            WHERE
//    GSSD.GSSD_DATE >='2020-07-01'
//    and GSSD.GSSD_DATE <='2020-09-09'
//    GROUP BY
//    GSSD.GSSD_DATE,GSSD.GSSD_PRO_ID,GPB.PRO_NAME,GPB.PRO_COMPCLASS

}
