package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 库存审计需求
 * 507 库存审计-月度库存推算、查询功能
 * 用户点击推算功能，用于接受前端传递条件使用
 * 检查客户必输，年月必输，一旦不符合条件，返回检验情况
 */
@Data
public class StockMouthCaculateCondition implements Serializable {


    private static final long serialVersionUID = 3731027981710384392L;

    public static final String QC = "QC";

    public static final String DQ = "DQ";

    private Map<String,List<String>> clientSiteMap;

    @ApiModelProperty(value = "客户，即加盟商集合", required = true)
    private List<String> clients;

    @ApiModelProperty(value = "地点合集", required = false)
    private List<String> sites;

    @ApiModelProperty(value = "年月", required = true)
    private String chooseDate;

    @ApiModelProperty(value = "库存推算选择模式，QC表示从期出上线月份更新至该月份  DQ表示仅更新选择月份", required = true)
    private String caculateType;

    @ApiModelProperty(value = "页面展示数量", required = false)
    public int pageSize;

    @ApiModelProperty(value = "页数", required = false)
    private int pageNum;


}
