package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/11/2
 */
@Data
public class ElectronicReviewInData implements Serializable {
    private static final long serialVersionUID = 7699550436822125329L;

    private String client;

    @NotBlank(message = "主题单号不可为空")
    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    @ApiModelProperty(value = "是否审核 N为否，Y为是")
    private String gsebsStatus;

    @ApiModelProperty(value = "是否停用 N为否，Y为是")
    private String deactivateStatus;
}
