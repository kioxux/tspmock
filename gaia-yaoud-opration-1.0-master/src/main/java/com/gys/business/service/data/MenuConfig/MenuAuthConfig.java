package com.gys.business.service.data.MenuConfig;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/4/6 19:48
 * @Version 1.0.0
 **/
@Data
@ApiModel(value = "权限分配")
public class MenuAuthConfig {
    /**
     * 菜单编码
     */
    @ApiModelProperty(name = "菜单编码")
    private Long menuConfigId;
    /**
     * 权限编码
     */
    @ApiModelProperty(name = "权限编码")
    private Long authId;

    /**
     * 权限名称
     */
    @ApiModelProperty(name = "权限名称")
    private String authName;

    /**
     * 权限状态 1 选择 2 未选中
     */
    @ApiModelProperty(name = "权限名称")
    private Integer authState;
}
