package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdWtbhdHMapper;
import com.gys.business.mapper.entity.GaiaSdWtbhdH;
import com.gys.business.service.GaiaSdWtbhdHService;
import com.gys.common.exception.BusinessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/13 18:47
 **/
@Service("gaiaSdWtbhdHService")
public class GaiaSdWtbhdHServiceImpl implements GaiaSdWtbhdHService {
    @Resource
    private GaiaSdWtbhdHMapper gaiaSdWtbhdHMapper;

    @Transactional
    @Override
    public GaiaSdWtbhdH add(GaiaSdWtbhdH gaiaSdWtbhdH) {
        try {
            gaiaSdWtbhdHMapper.add(gaiaSdWtbhdH);
        } catch (Exception e) {
            throw new BusinessException("保存三方补货主记录异常", e);
        }
        return gaiaSdWtbhdHMapper.selectByPrimaryKey(gaiaSdWtbhdH);
    }

    @Transactional
    @Override
    public GaiaSdWtbhdH update(GaiaSdWtbhdH gaiaSdWtbhdH) {
        try {
            gaiaSdWtbhdHMapper.update(gaiaSdWtbhdH);
        } catch (Exception e) {
            throw new BusinessException("更新三方补货主记录异常", e);
        }
        return gaiaSdWtbhdHMapper.selectByPrimaryKey(gaiaSdWtbhdH);
    }
}
