//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.gys.business.mapper.GaiaThirdParaMapper;
import com.gys.business.mapper.entity.GaiaThirdPara;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.data.GetCosParaOutData;
import com.gys.common.exception.BusinessException;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class ThirdParaServiceImpl implements ThirdParaService {
    @Autowired
    private GaiaThirdParaMapper thirdParaMapper;

    public ThirdParaServiceImpl() {
    }

    public GetCosParaOutData getCosPara() {
        Example example = new Example(GaiaThirdPara.class);
        example.createCriteria().andEqualTo("gtpType", "1");
        List<GaiaThirdPara> thirdParaList = this.thirdParaMapper.selectByExample(example);
        if (CollUtil.isEmpty(thirdParaList)) {
            throw new BusinessException("提示：未配置对象存储参数");
        } else {
            GetCosParaOutData outData = new GetCosParaOutData();
            Iterator var4 = thirdParaList.iterator();

            while(var4.hasNext()) {
                GaiaThirdPara thirdPara = (GaiaThirdPara)var4.next();
                if ("TENCENT_COS_SECRET_ID".equals(thirdPara.getGtpId())) {
                    outData.setSecretId(thirdPara.getGtpPara());
                } else if ("TENCENT_COS_SECRET_KEY".equals(thirdPara.getGtpId())) {
                    outData.setSecretKey(thirdPara.getGtpPara());
                } else if ("TENCENT_COS_REGION_NAME".equals(thirdPara.getGtpId())) {
                    outData.setRegionName(thirdPara.getGtpPara());
                } else if ("TENCENT_COS_BUCKET_NAME".equals(thirdPara.getGtpId())) {
                    outData.setBucketName(thirdPara.getGtpPara());
                }
            }

            return outData;
        }
    }
}
