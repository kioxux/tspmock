package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromGiftResultInData implements Serializable {
    private static final long serialVersionUID = -1705717750340294566L;
    private String clientId;
    private String gspgcVoucherId;
    private String gspgcSerial;
    private String gspgcProId;
    private String gspgcSeriesId;
    private BigDecimal gspgcGiftPrc;
    private String gspgcGiftRebate;
}
