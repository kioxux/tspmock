package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 供应商名称实体
 *
 * @author xiaoyuan on 2020/8/28
 */
@Data
public class QuerySupplierData implements Serializable {
    private static final long serialVersionUID = 8519470090615294019L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 采购凭证号
     */
    private String poId;

    /**
     * 创建日期
     */
    private String poCreateDate;

    /**
     * 创建时间
     */
    private String poCreateTime;

    /**
     * 供应商编码
     */
    private String poSupplierId;

    /**
     * 商品id
     */
    private String poProCode;

    /**
     * 供应商名称
     */
    private String supName;
}
