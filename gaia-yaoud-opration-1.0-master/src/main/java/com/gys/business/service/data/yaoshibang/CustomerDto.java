package com.gys.business.service.data.yaoshibang;

import com.gys.business.mapper.entity.GaiaSoBatchH;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @author zhangdong
 * @date 2021/6/18 17:13
 */
@Data
@NoArgsConstructor
public class CustomerDto {

    @ApiModelProperty(value = "数据主键-为前端唯一区分数据用")
    private Long id;
    /**
     * 客户ID
     */
    @ApiModelProperty(value = "客户ID")
    private String soCustomerid;

    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String soDrugstorename;

    public CustomerDto(GaiaSoBatchH h) {
        this.id = h.getId();
        this.soCustomerid = h.getSoCustomerid();
        this.soDrugstorename = h.getSoDrugstorename();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CustomerDto that = (CustomerDto) o;

        return new EqualsBuilder()
                .append(soCustomerid, that.soCustomerid)
                .append(soDrugstorename, that.soDrugstorename)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(soCustomerid)
                .append(soDrugstorename)
                .toHashCode();
    }
}
