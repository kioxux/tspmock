package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.gys.business.mapper.GaiaBillOfApMapper;
import com.gys.business.mapper.entity.GaiaBillOfAp;
import com.gys.business.mapper.entity.GaiaFiApBalance;
import com.gys.business.mapper.GaiaFiApBalanceMapper;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.IGaiaFiApBalanceService;
import com.gys.business.service.data.billOfAp.GaiaBillOfApExcelDto;
import com.gys.business.service.data.billOfAp.GaiaBillOfApParam;
import com.gys.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;


/**
 * <p>
 * 供应商应付期末余额 服务实现类
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class GaiaFiApBalanceServiceImpl implements IGaiaFiApBalanceService {
    @Autowired
    private GaiaFiApBalanceMapper fiApBalanceMapper;
    @Autowired
    private GaiaBillOfApMapper gaiaBillOfApMapper;
    @Autowired
    private GaiaBillOfApServiceImpl ofApService;

    @Override
    public void monthlySettlement(LocalDate day) {
        //找出小于等于当天的记录（若要统计6月   则传6.30）
        String strDay = StringUtils.left(DateUtil.formatDate(day), 6);
        //开始时间
        String startDate = DateUtil.formatDate(day);

        String lastDay = DateUtil.getLastDay(day.getYear(), day.getMonthValue());
        String strYear = String.valueOf(day.getYear());
        String strMonth = String.valueOf(day.getMonthValue());

        String uponLastDay = DateUtil.getDate(day.getYear(), day.getMonthValue());
        String uponStrYear = String.valueOf(day.getYear());
        String uponStrMonth = String.valueOf(day.getMonthValue());

        //根据加盟商 地点 供应商编码进行分组
        List<GaiaBillOfAp> billOfApList =  gaiaBillOfApMapper.selectBillOfApGroupBy();

        if(CollUtil.isNotEmpty(billOfApList)){
            for (GaiaBillOfAp gaiaBillOfAp : billOfApList) {
                Example gaiaFiApBalanceExample = Example.builder(GaiaFiApBalance.class).build();
                //获取前两月
                gaiaFiApBalanceExample.createCriteria()
                        .andEqualTo("client", gaiaBillOfAp.getClient())
                        .andEqualTo("dcCode", gaiaBillOfAp.getDcCode())
                        .andEqualTo("supSelfCode", gaiaBillOfAp.getSupSelfCode())
                        .andEqualTo("balanceYear", uponStrYear)
                        .andEqualTo("balanceMonth", uponStrMonth);
                gaiaFiApBalanceExample.setOrderByClause("CLIENT limit 1");
                GaiaFiApBalance gaiaFiApBalance = fiApBalanceMapper.selectOneByExample(gaiaFiApBalanceExample);
                //期初
                BigDecimal beginningOfPeriodAmt = BigDecimal.ZERO;
                //期间发生
                BigDecimal period = BigDecimal.ZERO;
                //期间付款
                BigDecimal payment = BigDecimal.ZERO;
                //其他消费
                BigDecimal rest = BigDecimal.ZERO;
                //期末
                BigDecimal endOfTermAmt = BigDecimal.ZERO;
                //为空
                if(Objects.isNull(gaiaFiApBalance)){
                    //那就统计所有
                    Example gaiaBillOfApExample = Example.builder(GaiaBillOfAp.class).build();
                    gaiaBillOfApExample.createCriteria()
                            .andEqualTo("client", gaiaBillOfAp.getClient())
                            .andEqualTo("dcCode",gaiaBillOfAp.getDcCode())
                            .andEqualTo("supSelfCode",gaiaBillOfAp.getSupSelfCode())
                            .andLessThan("paymentDate", startDate);
                    List<GaiaBillOfAp> gaiaBillOfAps = gaiaBillOfApMapper.selectByExample(gaiaBillOfApExample);

                    if(CollUtil.isNotEmpty(gaiaBillOfAps)){
                        //计算期初金额
                        beginningOfPeriodAmt = beginningOfPeriodAmt.add(gaiaBillOfAps.stream().map(GaiaBillOfAp::getAmountOfPayment).reduce(BigDecimal.ZERO, BigDecimal::add));
                    }
                    List<GaiaBillOfAp> amt = getAmt(gaiaBillOfAp, startDate, lastDay, "8888");
                    if(CollUtil.isNotEmpty(amt)){
                        GaiaBillOfAp billOfAp = amt.get(0);
                        if(Convert.toLong(uponLastDay)<Convert.toLong(billOfAp.getPaymentDate())){
                            //加上8888
                            beginningOfPeriodAmt = beginningOfPeriodAmt.add(billOfAp.getAmountOfPayment());
                        }
                    }
                    List<GaiaBillOfAp> billOfPeriod = getAmt(gaiaBillOfAp, startDate, lastDay, "9999");
                    if(CollUtil.isNotEmpty(billOfPeriod)){
                        period = period.add(billOfPeriod.stream().map(GaiaBillOfAp::getAmountOfPayment).reduce(BigDecimal.ZERO,BigDecimal::add));
                    }
                    //计算期间付款
                    List<GaiaBillOfAp> billOfPayment = getAmt(gaiaBillOfAp, startDate, lastDay, "6666");
                    if(CollUtil.isNotEmpty(billOfPayment)){
                        payment = payment.add(billOfPayment.stream().map(GaiaBillOfAp::getAmountOfPayment).reduce(BigDecimal.ZERO,BigDecimal::add));
                    }
                    //计算其他
                    List<GaiaBillOfAp> billOfRest = getAmt(gaiaBillOfAp, startDate, lastDay, "");
                    if(CollUtil.isNotEmpty(billOfRest)){
                        rest = rest.add(billOfRest.stream().map(GaiaBillOfAp::getAmountOfPayment).reduce(BigDecimal.ZERO,BigDecimal::add));
                    }
                    //计算期末
                    endOfTermAmt = endOfTermAmt.add(beginningOfPeriodAmt.add(period).add(payment).add(rest));
                    //存表
                    fiApBalanceMapper.insert(
                            new GaiaFiApBalance()
                                    .setClient(gaiaBillOfAp.getClient())
                                    .setSupCode(gaiaBillOfAp.getSupCode())
                                    .setSupSelfCode(gaiaBillOfAp.getSupSelfCode())
                                    .setSupName(gaiaBillOfAp.getSupName())
                                    .setResidualAmount(endOfTermAmt)
                                    .setDcCode(gaiaBillOfAp.getDcCode())
                                    .setBalanceYear(strYear)
                                    .setBalanceMonth(strMonth)
                    );
                }else {
                    beginningOfPeriodAmt = beginningOfPeriodAmt.add(gaiaFiApBalance.getResidualAmount());
                    //有统计好的数据
                    List<GaiaBillOfAp> billOfPeriod = getAmt(gaiaBillOfAp, startDate, lastDay, "9999");
                    if(CollUtil.isNotEmpty(billOfPeriod)){
                        period = period.add(billOfPeriod.stream().map(GaiaBillOfAp::getAmountOfPayment).reduce(BigDecimal.ZERO,BigDecimal::add));
                    }
                    //计算期间付款
                    List<GaiaBillOfAp> billOfPayment = getAmt(gaiaBillOfAp, startDate, lastDay, "6666");
                    if(CollUtil.isNotEmpty(billOfPayment)){
                        payment = payment.add(billOfPayment.stream().map(GaiaBillOfAp::getAmountOfPayment).reduce(BigDecimal.ZERO,BigDecimal::add));
                    }
                    //计算其他
                    List<GaiaBillOfAp> billOfRest = getAmt(gaiaBillOfAp, startDate, lastDay, "");
                    if(CollUtil.isNotEmpty(billOfRest)){
                        rest = rest.add(billOfRest.stream().map(GaiaBillOfAp::getAmountOfPayment).reduce(BigDecimal.ZERO,BigDecimal::add));
                    }
                    //计算期末
                    endOfTermAmt = endOfTermAmt.add(beginningOfPeriodAmt.add(period).add(payment).add(rest));
                    //存表
                    fiApBalanceMapper.insert(
                            new GaiaFiApBalance()
                                    .setClient(gaiaBillOfAp.getClient())
                                    .setSupCode(gaiaBillOfAp.getSupCode())
                                    .setSupSelfCode(gaiaBillOfAp.getSupSelfCode())
                                    .setSupName(gaiaBillOfAp.getSupName())
                                    .setResidualAmount(endOfTermAmt)
                                    .setDcCode(gaiaBillOfAp.getDcCode())
                                    .setBalanceYear(strYear)
                                    .setBalanceMonth(strMonth)
                    );
                }
            }
        }
    }
    private List<GaiaBillOfAp> getAmt(GaiaBillOfAp gaiaBillOfAp,String startDate,String endDate,String type){
        //计算期间发生
        Example gaiaBillOf = Example.builder(GaiaBillOfAp.class).build();
        Example.Criteria criteria = gaiaBillOf.createCriteria();
        criteria
                .andEqualTo("client", gaiaBillOfAp.getClient())
                .andEqualTo("dcCode",gaiaBillOfAp.getDcCode())
                .andEqualTo("supSelfCode",gaiaBillOfAp.getSupSelfCode())
                .andGreaterThanOrEqualTo("paymentDate",startDate)
                .andLessThanOrEqualTo("paymentDate",endDate);
        if(StrUtil.isNotBlank(type)){
            criteria.andEqualTo("paymentId",type);
        }else {
            criteria.andNotEqualTo("paymentId","6666");
            criteria.andNotEqualTo("paymentId","9999");
            criteria.andNotEqualTo("paymentId","8888");
        }
        List<GaiaBillOfAp> billOfs = gaiaBillOfApMapper.selectByExample(gaiaBillOf);
        return billOfs;
    }
}
