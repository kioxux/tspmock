package com.gys.business.service.data.ReportForms;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "供应商往来单据查询")
@CsvRow
public class SupplierDealOutDataCSV {


    @CsvCell(title = "序号",index = 0,fieldNo = 1)
    private String index;

    @ApiModelProperty(value = "商品编码")
    @CsvCell(title = "商品编码",index = 7,fieldNo = 1)
    private String proCode;

    @ApiModelProperty(value = "商品名称")
    @CsvCell(title = "商品名称",index = 8,fieldNo = 1)
    private String proName;

    @ApiModelProperty(value = "规格")
    @CsvCell(title = "规格",index = 9,fieldNo = 1)
    private String specs;

    @ApiModelProperty(value = "单位")
    @CsvCell(title = "单位",index = 10,fieldNo = 1)
    private String unit;

    @ApiModelProperty(value = "生产厂家")
    @CsvCell(title = "生产厂家",index = 11,fieldNo = 1)
    private String factoryName;

    @ApiModelProperty(value = "业务名称")
    @CsvCell(title = "业务名称",index = 4,fieldNo = 1)
    private String matName;

    @ApiModelProperty(value = "发生时间")
    @CsvCell(title = "发生时间",index = 2,fieldNo = 1)
    private String postDate;

    @ApiModelProperty(value = "业务单据号")
    @CsvCell(title = "业务单据号",index = 3,fieldNo = 1)
    private String dnId;

    @ApiModelProperty(value = "去税金额")
    @CsvCell(title = "去税成本额",index = 15,fieldNo = 1)
    private BigDecimal batAmt;

    @ApiModelProperty(value = "税金")
    @CsvCell(title = "税金",index = 16,fieldNo = 1)
    private BigDecimal rateBat;

    @ApiModelProperty(value = "合计金额")
    @CsvCell(title = "含税成本额",index = 17,fieldNo = 1)
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "供应商编码")
    @CsvCell(title = "供应商编码",index = 18,fieldNo = 1)
    private String supCode;

    @ApiModelProperty(value = "供应商名称")
    @CsvCell(title = "供应商名称",index = 19,fieldNo = 1)
    private String supName;

    @ApiModelProperty(value = "入库地点编码")
    @CsvCell(title = "入库地点",index = 20,fieldNo = 1)
    private String siteCode;

    @ApiModelProperty(value = "入库地点")
    @CsvCell(title = "规格",index = 21,fieldNo = 1)
    private String siteName;

    @ApiModelProperty(value = "门店编码")
    @CsvCell(title = "门店编码",index = 5,fieldNo = 1)
    private String stoCode;

    @ApiModelProperty(value = "门店名称")
    @CsvCell(title = "门店名称",index = 6,fieldNo = 1)
    private String stoName;

    @ApiModelProperty(value = "数量")
    @CsvCell(title = "数量",index = 12,fieldNo = 1)
    private BigDecimal qty;

    @ApiModelProperty(value = "单价")
    @CsvCell(title = "单价",index = 13,fieldNo = 1)
    private BigDecimal price;

    @ApiModelProperty(value = "开单金额")
    @CsvCell(title = "开单金额",index = 14,fieldNo = 1)
    private BigDecimal billingAmount;

    @ApiModelProperty(value = "批号")
    @CsvCell(title = "批号",index = 1,fieldNo = 1)
    private String batchNo;

    @ApiModelProperty(value = "姓名")
    @CsvCell(title = "经办人",index = 22,fieldNo = 1)
    private String  ueserInfo;

    @ApiModelProperty("发票价")
    @CsvCell(title = "发票价",index = 23,fieldNo = 1)
    private String batFph;

    @ApiModelProperty("发票金额")
    @CsvCell(title = "发票金额",index = 24,fieldNo = 1)
    private String batFpje;

    @ApiModelProperty("抬头备注")
    @CsvCell(title = "抬头备注",index = 26,fieldNo = 1)
    private String poHeadRemark;

    @ApiModelProperty(value = "供应商业务员")
    @CsvCell(title = "业务员",index = 25,fieldNo = 1)
    private String  poSupplierSalesmanInfo;

    @ApiModelProperty(value = "税率")
    @CsvCell(title = "税率",index = 27,fieldNo = 1)
    private String taxCodeValue;


    /**
     * 生产日期
     */
    @CsvCell(title = "生产日期",index = 28,fieldNo = 1)
    private String  batProductDate;

    /**
     * 有效期
     */
    @CsvCell(title = "有效期",index = 29,fieldNo = 1)
    private String batExpiryDat;


}
