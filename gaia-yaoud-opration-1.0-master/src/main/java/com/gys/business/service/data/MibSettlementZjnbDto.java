package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class MibSettlementZjnbDto implements Serializable {
    private static final long serialVersionUID = -3235822174002205848L;

    /**
     * id
     */
    private Long guid;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String stoCode;
    /**
     * 时间
     */
    private String receiptTime;
    /**
     * 卡类型
     */
    private String cardType;

    /**
     * 卡内数据
     */
    private String cardData;

    /**
     * 姓名
     */
    private String name;

    /**
     * 身份证号（社会保障号）
     */
    private String id;

    /**
     * 性别
     */
    private String sex;

    /**
     * 出生日期
     */
    private String birthday;

    /**
     * 区县代码
     */
    private String areaCode;

    /**
     * 参保人员类别
     */
    private String personalType;

    /**
     * 参保人员子类别
     */
    private String subPersonalType;

    /**
     * 享受待遇类别
     */
    private String treatmentCategory;

    /**
     * 帐户状态
     */
    private String accountStatus;

    /**
     * 异地居住标识
     */
    private String nonLocalSign;

    /**
     * 交易监控标识
     */
    private String monitoringTag;

    /**
     * 代配药标志
     */
    private String dispensingMark;

    /**
     * 单位医保号
     */
    private String unitInsuranceNo;

    /**
     * 系统保留
     */
    private String accSystemRetention;

    /**
     * 登记类别
     */
    private String regType;

    /**
     * 登记医疗机构
     */
    private String organization;

    /**
     * 登记时间
     */
    private String regTime;

    /**
     * 就诊编号
     */
    private String visitNumber;

    /**
     * 住院/家床编号
     */
    private String inpatientNumber;

    /**
     * 综合减负标志
     */
    private String loadReductionMark;

    /**
     * 民政优抚对象标识
     */
    private String objectMark;

    /**
     * 社保卡刷卡是否需要输入密码
     */
    private String isCheckPwd;

    /**
     * 预授权标识
     */
    private String authorizationID;

    /**
     * 第三方代扣签约标识
     */
    private String thirdPartyWithholding;

    /**
     * 系统保留
     */
    private String otherSystemRetention;

    /**
     * 医疗费用总额
     */
    private BigDecimal medFeeSumAmt;

    /**
     * 医疗费用自费应付总额
     */
    private BigDecimal fulAmtOwnPayAmt;

    /**
     * 医疗费用自付应付总额
     */
    private BigDecimal selfPayAmt;

    /**
     * 现金支付数(现金A)
     */
    private BigDecimal cashPayA;

    /**
     * 现金支付数(现金B)
     */
    private BigDecimal cashPayB;

    /**
     * 现金支付数(现金C)
     */
    private BigDecimal cashPayC;

    /**
     * 现金支付数(现金D)
     */
    private BigDecimal cashPayD;

    /**
     * 支付_统筹总额
     */
    private BigDecimal poolPay;

    /**
     * 救助支付数
     */
    private BigDecimal salvaPay;

    /**
     * 个人当年帐户支付数
     */
    private BigDecimal psnAcctPay;

    /**
     * 支付_历年账户总额
     */
    private BigDecimal psnAcctHisPay;

    /**
     * 支付_社会救助总额
     */
    private BigDecimal socialSalvaPay;

    /**
     * 公补基金支付数
     */
    private BigDecimal publicFundPay;

    /**
     * 支付_家庭账户总额
     */
    private BigDecimal familyAcctPay;

    /**
     * 支付_其他支付项总额
     */
    private BigDecimal othPay;

    /**
     * 支付_医疗救助总额
     */
    private BigDecimal mafPay;

    /**
     * 个人帐户当年(结算前)余额
     */
    private BigDecimal psnAcctBalBef;

    /**
     * 个人帐户当年(结算后)余额
     */
    private BigDecimal psnAcctBalAft;

    /**
     * 个人帐户历年(结算前)余额
     */
    private BigDecimal psnAcctHisBalBef;

    /**
     * 个人帐户历年(结算后)余额
     */
    private BigDecimal psnAcctHisBalAft;

    /**
     * 门诊自负段(结算前)累计
     */
    private BigDecimal outPayTotBef;

    /**
     * 门诊自负段(结算后)累计
     */
    private BigDecimal outPayTotAft;

    /**
     * 住院起付线(结算前)累计
     */
    private BigDecimal hosStartTotBef;

    /**
     * 住院起付线(结算后)累计
     */
    private BigDecimal hosStartTotAft;

    /**
     * 年度门诊费用(结算前)累计
     */
    private BigDecimal outYearTotBef;

    /**
     * 年度门诊费用(结算后)累计
     */
    private BigDecimal outYearTotAft;

    /**
     * 年度住院费用(结算前)累计
     */
    private BigDecimal hosYearTotBef;

    /**
     * 年度住院费用(结算后)累计
     */
    private BigDecimal hosYearTotAft;

    /**
     * 年度特病(结算前)累计
     */
    private BigDecimal speYearTotBef;

    /**
     * 年度特病(结算后)累计
     */
    private BigDecimal speYearTotAft;

    /**
     * 住院共负段自付[ 、是指住院类交易中“个人承担”部分发生数。](结算前)累计
     */
    private BigDecimal hosTotBef;

    /**
     * 住院共负段自付(结算后)累计
     */
    private BigDecimal hosTotAft;

    /**
     * 门诊封顶线下[ 、目前城镇居民有封顶线的概念，需要参考此项指标。](结算前)累计
     */
    private BigDecimal outTopBef;

    /**
     * 门诊封顶线下(结算后)累计
     */
    private BigDecimal outTopAft;

    /**
     * 门院封顶线下(结算前)累计
     */
    private BigDecimal hosTopBef;

    /**
     * 住院封顶线下(结算后)累计
     */
    private BigDecimal hosTopAft;

    /**
     * 特病封顶线下(结算前)累计
     */
    private BigDecimal speTopBef;

    /**
     * 特病封顶线下(结算后)累计
     */
    private BigDecimal speTopAft;

    /**
     * 家庭账户(结算前)余额
     */
    private BigDecimal familyAcctBef;

    /**
     * 家庭账户(结算后)余额
     */
    private BigDecimal familyAcctAft;

    /**
     * 其他累计2(结算前)累计
     */
    private BigDecimal othTotBef;

    /**
     * 其他累计2(结算后)累计
     */
    private BigDecimal othTotAft;

    /**
     * 医疗救（补）助支付(结算前)累计
     */
    private BigDecimal mafPayBef;

    /**
     * 就诊编号
     */
    private String clinicNum;

    /**
     * 结算交易流水号
     */
    private String detailSerialCode;

    /**
     * 明细账单号
     */
    private String detailBillCode;

    /**
     * 医疗救（补）助支付(结算后)累计
     */
    private BigDecimal mafPayAft;

    /**
     * 自负应付总额
     */
    private BigDecimal selfPayTotal;

    /**
     * 承担应付总额
     */
    private BigDecimal amountPayTotal;

    /**
     * 自付个账抵扣前应付总额
     */
    private BigDecimal selfPayBeforeTotal;

    /**
     * 总数
     */
    private int amount;

    /**
     * 改账后总金额
     */
    private BigDecimal changeAmount;

    /**
     * 改账申请号
     */
    private String changeAccountSn;

    /**
     * 库存流水号
     */
    private String ywdh;

    /**
     * 库存流水号
     */
    private String detailSerialCodeBef;

    /**
     * 是否处方
     */
    private String zfType;

    /**
     *
     */
    private String userName;

    /**
     * 商品集合
     */
    private List<SaleDDetail> saleDList;
}
