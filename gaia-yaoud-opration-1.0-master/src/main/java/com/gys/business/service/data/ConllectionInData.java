//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdPromCouponBasic;
import java.util.List;

public class ConllectionInData {
    private List<GetProductInData> productInData;
    private List<GetShopInData> shopInData;
    private List<GetSaleScheduleInData> saleScheduleInData;
    private List<GetShopDetailOutData> shopDetailOutData;
    private List<GetProductDetailOutData> productDetailOutData;
    private List<TempHumiInData> tempHumiInData;
    private List<GaiaSdSaleRecipelRecordInData> gaiaSdSaleRecipelRecordInData;
    private List<GaiaSdPromCouponBasic> promCouponBasicList;

    public ConllectionInData() {
    }

    public List<GetProductInData> getProductInData() {
        return this.productInData;
    }

    public List<GetShopInData> getShopInData() {
        return this.shopInData;
    }

    public List<GetSaleScheduleInData> getSaleScheduleInData() {
        return this.saleScheduleInData;
    }

    public List<GetShopDetailOutData> getShopDetailOutData() {
        return this.shopDetailOutData;
    }

    public List<GetProductDetailOutData> getProductDetailOutData() {
        return this.productDetailOutData;
    }

    public List<TempHumiInData> getTempHumiInData() {
        return this.tempHumiInData;
    }

    public List<GaiaSdSaleRecipelRecordInData> getGaiaSdSaleRecipelRecordInData() {
        return this.gaiaSdSaleRecipelRecordInData;
    }

    public List<GaiaSdPromCouponBasic> getPromCouponBasicList() {
        return this.promCouponBasicList;
    }

    public void setProductInData(final List<GetProductInData> productInData) {
        this.productInData = productInData;
    }

    public void setShopInData(final List<GetShopInData> shopInData) {
        this.shopInData = shopInData;
    }

    public void setSaleScheduleInData(final List<GetSaleScheduleInData> saleScheduleInData) {
        this.saleScheduleInData = saleScheduleInData;
    }

    public void setShopDetailOutData(final List<GetShopDetailOutData> shopDetailOutData) {
        this.shopDetailOutData = shopDetailOutData;
    }

    public void setProductDetailOutData(final List<GetProductDetailOutData> productDetailOutData) {
        this.productDetailOutData = productDetailOutData;
    }

    public void setTempHumiInData(final List<TempHumiInData> tempHumiInData) {
        this.tempHumiInData = tempHumiInData;
    }

    public void setGaiaSdSaleRecipelRecordInData(final List<GaiaSdSaleRecipelRecordInData> gaiaSdSaleRecipelRecordInData) {
        this.gaiaSdSaleRecipelRecordInData = gaiaSdSaleRecipelRecordInData;
    }

    public void setPromCouponBasicList(final List<GaiaSdPromCouponBasic> promCouponBasicList) {
        this.promCouponBasicList = promCouponBasicList;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ConllectionInData)) {
            return false;
        } else {
            ConllectionInData other = (ConllectionInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label107: {
                    Object this$productInData = this.getProductInData();
                    Object other$productInData = other.getProductInData();
                    if (this$productInData == null) {
                        if (other$productInData == null) {
                            break label107;
                        }
                    } else if (this$productInData.equals(other$productInData)) {
                        break label107;
                    }

                    return false;
                }

                Object this$shopInData = this.getShopInData();
                Object other$shopInData = other.getShopInData();
                if (this$shopInData == null) {
                    if (other$shopInData != null) {
                        return false;
                    }
                } else if (!this$shopInData.equals(other$shopInData)) {
                    return false;
                }

                Object this$saleScheduleInData = this.getSaleScheduleInData();
                Object other$saleScheduleInData = other.getSaleScheduleInData();
                if (this$saleScheduleInData == null) {
                    if (other$saleScheduleInData != null) {
                        return false;
                    }
                } else if (!this$saleScheduleInData.equals(other$saleScheduleInData)) {
                    return false;
                }

                label86: {
                    Object this$shopDetailOutData = this.getShopDetailOutData();
                    Object other$shopDetailOutData = other.getShopDetailOutData();
                    if (this$shopDetailOutData == null) {
                        if (other$shopDetailOutData == null) {
                            break label86;
                        }
                    } else if (this$shopDetailOutData.equals(other$shopDetailOutData)) {
                        break label86;
                    }

                    return false;
                }

                label79: {
                    Object this$productDetailOutData = this.getProductDetailOutData();
                    Object other$productDetailOutData = other.getProductDetailOutData();
                    if (this$productDetailOutData == null) {
                        if (other$productDetailOutData == null) {
                            break label79;
                        }
                    } else if (this$productDetailOutData.equals(other$productDetailOutData)) {
                        break label79;
                    }

                    return false;
                }

                label72: {
                    Object this$tempHumiInData = this.getTempHumiInData();
                    Object other$tempHumiInData = other.getTempHumiInData();
                    if (this$tempHumiInData == null) {
                        if (other$tempHumiInData == null) {
                            break label72;
                        }
                    } else if (this$tempHumiInData.equals(other$tempHumiInData)) {
                        break label72;
                    }

                    return false;
                }

                Object this$gaiaSdSaleRecipelRecordInData = this.getGaiaSdSaleRecipelRecordInData();
                Object other$gaiaSdSaleRecipelRecordInData = other.getGaiaSdSaleRecipelRecordInData();
                if (this$gaiaSdSaleRecipelRecordInData == null) {
                    if (other$gaiaSdSaleRecipelRecordInData != null) {
                        return false;
                    }
                } else if (!this$gaiaSdSaleRecipelRecordInData.equals(other$gaiaSdSaleRecipelRecordInData)) {
                    return false;
                }

                Object this$promCouponBasicList = this.getPromCouponBasicList();
                Object other$promCouponBasicList = other.getPromCouponBasicList();
                if (this$promCouponBasicList == null) {
                    if (other$promCouponBasicList != null) {
                        return false;
                    }
                } else if (!this$promCouponBasicList.equals(other$promCouponBasicList)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ConllectionInData;
    }

    public int hashCode() {

        int result = 1;
        Object $productInData = this.getProductInData();
        result = result * 59 + ($productInData == null ? 43 : $productInData.hashCode());
        Object $shopInData = this.getShopInData();
        result = result * 59 + ($shopInData == null ? 43 : $shopInData.hashCode());
        Object $saleScheduleInData = this.getSaleScheduleInData();
        result = result * 59 + ($saleScheduleInData == null ? 43 : $saleScheduleInData.hashCode());
        Object $shopDetailOutData = this.getShopDetailOutData();
        result = result * 59 + ($shopDetailOutData == null ? 43 : $shopDetailOutData.hashCode());
        Object $productDetailOutData = this.getProductDetailOutData();
        result = result * 59 + ($productDetailOutData == null ? 43 : $productDetailOutData.hashCode());
        Object $tempHumiInData = this.getTempHumiInData();
        result = result * 59 + ($tempHumiInData == null ? 43 : $tempHumiInData.hashCode());
        Object $gaiaSdSaleRecipelRecordInData = this.getGaiaSdSaleRecipelRecordInData();
        result = result * 59 + ($gaiaSdSaleRecipelRecordInData == null ? 43 : $gaiaSdSaleRecipelRecordInData.hashCode());
        Object $promCouponBasicList = this.getPromCouponBasicList();
        result = result * 59 + ($promCouponBasicList == null ? 43 : $promCouponBasicList.hashCode());
        return result;
    }

    public String toString() {
        return "ConllectionInData(productInData=" + this.getProductInData() + ", shopInData=" + this.getShopInData() + ", saleScheduleInData=" + this.getSaleScheduleInData() + ", shopDetailOutData=" + this.getShopDetailOutData() + ", productDetailOutData=" + this.getProductDetailOutData() + ", tempHumiInData=" + this.getTempHumiInData() + ", gaiaSdSaleRecipelRecordInData=" + this.getGaiaSdSaleRecipelRecordInData() + ", promCouponBasicList=" + this.getPromCouponBasicList() + ")";
    }
}
