package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class GaiaProductEvaluationDetailOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店")
    private String storeId;
    @ApiModelProperty(value = "品项数")
    private BigDecimal qty;
    @ApiModelProperty(value = "销售额")
    private BigDecimal salesAmt;
    @ApiModelProperty(value = "品项数")
    private BigDecimal gross;
    @ApiModelProperty(value = "单据号")
    private String billCode;
    @ApiModelProperty(value = "商品编码")
    private String proSelfCode;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "生产厂商")
    private String proFactoryName;
    @ApiModelProperty(value = "成分大类编码")
    private String proCompBigCode;
    @ApiModelProperty(value = "商品名称")
    private String proName;
    @ApiModelProperty(value = "状态")
    private int status;
    @ApiModelProperty(value = "建议处置状态")
    private int suggestedStatus;
    @ApiModelProperty(value = "处理状态")
    private int dealStatus;
    private int salesQty;
    private Date billDate;
    private Date updateTime;
    private String updateUser;
}
