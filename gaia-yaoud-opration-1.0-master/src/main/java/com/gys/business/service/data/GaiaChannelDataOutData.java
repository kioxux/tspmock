package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@ApiModel(value="com-gys-business-service-data-GaiaChannelData")
@Data
public class GaiaChannelDataOutData {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 渠道
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 渠道名称
    */
    @ApiModelProperty(value="渠道")
    private String gcdChannel;

    /**
    * 渠道名称
    */
    @ApiModelProperty(value="渠道名称")
    private String gcdChannelName;

    /**
     * 状态（0-正常，1-停用）
     */
    @ApiModelProperty(value="状态（0-正常，1-停用）")
    private String gcdStatus;
    private String gcdStatusName;

    /**
    * 创建人
    */
    @ApiModelProperty(value="创建人")
    private String gcdCreateBy;

    /**
    * 创建日期
    */
    @ApiModelProperty(value="创建日期")
    private Date gcdCreateDate;


    /**
    * 更新人
    */
    @ApiModelProperty(value="更新人")
    private String gcdUpdateBy;

    /**
    * 更新日期
    */
    @ApiModelProperty(value="更新日期")
    private Date gcdUpdateDate;


    /**
    * 是否删除：0-正常 1-删除
    */
    @ApiModelProperty(value="是否删除：0-正常 1-删除")
    private int isDelete;


}