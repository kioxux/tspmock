package com.gys.business.service.data.yaoshibang;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/18 20:50
 */
@Data
public class GaiaSoHeader {

    @ApiModelProperty(value = "订单类型", name = "soType")
    private String soType;
    @ApiModelProperty(value = "客户信息", name = "soCustomerId")
    private String soCustomerId;
    @ApiModelProperty(value = "销售主体", name = "soCompanyCode")
    private String soCompanyCode;
    @ApiModelProperty(value = "凭证日期", name = "soDate")
    private String soDate;
    @ApiModelProperty(value = "收款条款", name = "soPaymentId")
    private String soPaymentId;
    @ApiModelProperty(value = "抬头备注", name = "soHeadRemark")
    private String soHeadRemark;
    @ApiModelProperty(value = "销售订单明细", name = "soItemList")
    private List<SoItem> soItemList;
    @ApiModelProperty(value = "客户2", name = "soCustomer2")
    private String soCustomer2;
    /**
     * 请货单列表
     */
    private List<String> voucherIdList;

    /**
     * 用户信息：自动执行时无登录用户
     */
    private TokenUser tokenUser;

}
