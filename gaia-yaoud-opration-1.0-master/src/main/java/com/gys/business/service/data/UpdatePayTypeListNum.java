package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class UpdatePayTypeListNum {

    @ApiModelProperty("单号")
    private String voucherId;

    @ApiModelProperty("门店编码")
    private String stoCode;

    @ApiModelProperty("支付字段列表")
    private List<String> payTypeList;

    @ApiModelProperty("支付字段修改值列表")
    private List<String> payTypeValueList;
}
