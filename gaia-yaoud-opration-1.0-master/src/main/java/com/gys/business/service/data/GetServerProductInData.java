package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetServerProductInData {
    private String proCode;
    private String proName;
    private String proAmount;
    private String proSpecs;
    private String proFactoryName;
    private String proPlace;
    private String proForm;
    private String proBarcode;
    private String proRegisterNo;
    private String proCompclass;
    private String proUnit;
    private String batchNo;
    private String proStock;
    private String priority1;
    private String priority3;
    private String status;
}
