package com.gys.business.service;

import com.gys.business.service.data.DelegationReturn.*;
import com.gys.common.data.GetLoginOutData;

import java.util.List;


public interface DelegationReturnService {
    List<DelegationReturnOutData> selectDelegationList(DelegationReturnInData data);

    List<DelegationProductOutData> getDelegationProductList(DelegationReturnInData data);

    List<ExcelReturnApprovalData> selectByExport();

    boolean check(List<String> data,GetLoginOutData userInfo );

    void save(DelegationReturnInfoData data);


}
