//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdCleandrawerD;

public class GaiaSdCleandrawerDOutData extends GaiaSdCleandrawerD {
    private String gscdProName;
    private String normalPrice;
    private String proForm;
    private String proSpecs;
    private String proPlace;
    private String proUnit;
    private String proFactoryName;
    private String gsadStockStatus;
    private String proRegisterNo;
    private String qtySour;

    public GaiaSdCleandrawerDOutData() {
    }

    public String getGscdProName() {
        return this.gscdProName;
    }

    public String getNormalPrice() {
        return this.normalPrice;
    }

    public String getProForm() {
        return this.proForm;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getGsadStockStatus() {
        return this.gsadStockStatus;
    }

    public String getProRegisterNo() {
        return this.proRegisterNo;
    }

    public String getQtySour() {
        return this.qtySour;
    }

    public void setGscdProName(final String gscdProName) {
        this.gscdProName = gscdProName;
    }

    public void setNormalPrice(final String normalPrice) {
        this.normalPrice = normalPrice;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setGsadStockStatus(final String gsadStockStatus) {
        this.gsadStockStatus = gsadStockStatus;
    }

    public void setProRegisterNo(final String proRegisterNo) {
        this.proRegisterNo = proRegisterNo;
    }

    public void setQtySour(final String qtySour) {
        this.qtySour = qtySour;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GaiaSdCleandrawerDOutData)) {
            return false;
        } else {
            GaiaSdCleandrawerDOutData other = (GaiaSdCleandrawerDOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$gscdProName = this.getGscdProName();
                Object other$gscdProName = other.getGscdProName();
                if (this$gscdProName == null) {
                    if (other$gscdProName != null) {
                        return false;
                    }
                } else if (!this$gscdProName.equals(other$gscdProName)) {
                    return false;
                }

                Object this$normalPrice = this.getNormalPrice();
                Object other$normalPrice = other.getNormalPrice();
                if (this$normalPrice == null) {
                    if (other$normalPrice != null) {
                        return false;
                    }
                } else if (!this$normalPrice.equals(other$normalPrice)) {
                    return false;
                }

                Object this$proForm = this.getProForm();
                Object other$proForm = other.getProForm();
                if (this$proForm == null) {
                    if (other$proForm != null) {
                        return false;
                    }
                } else if (!this$proForm.equals(other$proForm)) {
                    return false;
                }

                label110: {
                    Object this$proSpecs = this.getProSpecs();
                    Object other$proSpecs = other.getProSpecs();
                    if (this$proSpecs == null) {
                        if (other$proSpecs == null) {
                            break label110;
                        }
                    } else if (this$proSpecs.equals(other$proSpecs)) {
                        break label110;
                    }

                    return false;
                }

                label103: {
                    Object this$proPlace = this.getProPlace();
                    Object other$proPlace = other.getProPlace();
                    if (this$proPlace == null) {
                        if (other$proPlace == null) {
                            break label103;
                        }
                    } else if (this$proPlace.equals(other$proPlace)) {
                        break label103;
                    }

                    return false;
                }

                Object this$proUnit = this.getProUnit();
                Object other$proUnit = other.getProUnit();
                if (this$proUnit == null) {
                    if (other$proUnit != null) {
                        return false;
                    }
                } else if (!this$proUnit.equals(other$proUnit)) {
                    return false;
                }

                label89: {
                    Object this$proFactoryName = this.getProFactoryName();
                    Object other$proFactoryName = other.getProFactoryName();
                    if (this$proFactoryName == null) {
                        if (other$proFactoryName == null) {
                            break label89;
                        }
                    } else if (this$proFactoryName.equals(other$proFactoryName)) {
                        break label89;
                    }

                    return false;
                }

                label82: {
                    Object this$gsadStockStatus = this.getGsadStockStatus();
                    Object other$gsadStockStatus = other.getGsadStockStatus();
                    if (this$gsadStockStatus == null) {
                        if (other$gsadStockStatus == null) {
                            break label82;
                        }
                    } else if (this$gsadStockStatus.equals(other$gsadStockStatus)) {
                        break label82;
                    }

                    return false;
                }

                Object this$proRegisterNo = this.getProRegisterNo();
                Object other$proRegisterNo = other.getProRegisterNo();
                if (this$proRegisterNo == null) {
                    if (other$proRegisterNo != null) {
                        return false;
                    }
                } else if (!this$proRegisterNo.equals(other$proRegisterNo)) {
                    return false;
                }

                Object this$qtySour = this.getQtySour();
                Object other$qtySour = other.getQtySour();
                if (this$qtySour == null) {
                    if (other$qtySour != null) {
                        return false;
                    }
                } else if (!this$qtySour.equals(other$qtySour)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GaiaSdCleandrawerDOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $gscdProName = this.getGscdProName();
        result = result * 59 + ($gscdProName == null ? 43 : $gscdProName.hashCode());
        Object $normalPrice = this.getNormalPrice();
        result = result * 59 + ($normalPrice == null ? 43 : $normalPrice.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $gsadStockStatus = this.getGsadStockStatus();
        result = result * 59 + ($gsadStockStatus == null ? 43 : $gsadStockStatus.hashCode());
        Object $proRegisterNo = this.getProRegisterNo();
        result = result * 59 + ($proRegisterNo == null ? 43 : $proRegisterNo.hashCode());
        Object $qtySour = this.getQtySour();
        result = result * 59 + ($qtySour == null ? 43 : $qtySour.hashCode());
        return result;
    }

    public String toString() {
        return "GaiaSdCleandrawerDOutData(gscdProName=" + this.getGscdProName() + ", normalPrice=" + this.getNormalPrice() + ", proForm=" + this.getProForm() + ", proSpecs=" + this.getProSpecs() + ", proPlace=" + this.getProPlace() + ", proUnit=" + this.getProUnit() + ", proFactoryName=" + this.getProFactoryName() + ", gsadStockStatus=" + this.getGsadStockStatus() + ", proRegisterNo=" + this.getProRegisterNo() + ", qtySour=" + this.getQtySour() + ")";
    }
}
