package com.gys.business.service;

import com.gys.business.service.data.SelectData;
import com.gys.common.data.GaiaBillOfArInData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

public interface SelectService {
    List<SelectData> getCustomer( Map<String,String> map);

    List<SelectData> getProClass();

    List<SelectData> getProBigClass();

    List<SelectData> getProMidClass();

    List<Map<String,Object>> selectProBySaleD(Map<String, Object> inData);

    List<Map<String,Object>> selectMemberCard(Map<String,Object> inData);

    List<Map> getMemberByClientOrBrId(String client, String brId);

    Map selectStaff(String client, String brid);

    List<Map<String,Object>> selectCashier(String client, String brId);

    List<Map<String,Object>> selectAssistant(String client, String brId);

    List<Map<String,Object>> selectDoctor(String client, String brId);

    List<Map<String,Object>> listCustomer(GetLoginOutData loginUser, GaiaBillOfArInData inData);

    List<Map<String,Object>> listDc(GetLoginOutData loginUser, GaiaBillOfArInData inData);
}
