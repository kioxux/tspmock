package com.gys.business.service.data;

import com.gys.business.mapper.entity.MonthlyPlanVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;


@ApiModel(value = "月销售任务列表查询条件", description = "")
@Data
public class MonthOutData {

    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店编码")
    private String BrId;
    @ApiModelProperty(value = "门店编码 集合")
    private String[] siteArr;
    @ApiModelProperty(value = "店名简称")
    private String brName;
    @ApiModelProperty(value = "任务名称 新建修改都是同一个名字")
    private String staskTaskName;
    @ApiModelProperty(value = "任务时间 起始日期")
    private String staskStartDate;
    @ApiModelProperty(value = "任务时间 结束日期")
    private String staskEndDate;
    @ApiModelProperty(value = "状态名称")
    private String staskSplanSta;
    @ApiModelProperty(value = "状态 P 待处理 S 已保存 A 已审核 D 已停用 I 已忽略")
    private String planStatus;
    @ApiModelProperty(value = "创建人姓名")
    private String userName;
    @ApiModelProperty(value = "期间天数")
    private String monthDays;
    @ApiModelProperty(value = "日均毛利率")
    private String dayGrossMargin;
    @ApiModelProperty(value = "日均毛利额")
    private String dailyProfitAmt;
    @ApiModelProperty(value = "营业额")
    private String dailyPayAmt;
    @ApiModelProperty(value = "毛利额")
    private String grossProfit;
    @ApiModelProperty(value = "毛利率")
    private String grossMargin;
    @ApiModelProperty(value = "会员卡")
    private String membershipCard;
    @ApiModelProperty(value = "任务创建日期")
    private String creationDate;
    @ApiModelProperty(value = "门店数")
    private String stoQty;

    /**
     * 上面属于返回值
     *
     * 以下属于传参
     */


    @ApiModelProperty(value = "任务月份")
    //@NotEmpty(message = "monthPlan 任务月份不可为空!")
    private String monthPlan;

    @ApiModelProperty(value = "任务id")
    private String staskTaskId;

    @ApiModelProperty(value = "同比月份")
    @NotEmpty(message = "同比月份不可为空!")
    private String yoyMonth;

    @ApiModelProperty(value = "环比月份")
    @NotNull(message = "环比月份不可为空!")
    private String momMonth;

    @ApiModelProperty(value = "营业额同比增长率")
    @NotNull(message = "营业额同比增长率不可为空!")
    private String yoySaleAmt;

    @ApiModelProperty(value = "营业额环比增长率")
    @NotNull(message = "营业额环比增长率不可为空!")
    private String momSaleAmt;


    @ApiModelProperty(value = "毛利额同比增长率")
    @NotNull(message = "毛利额同比增长率不可为空!")
    private String yoySaleGross;

    @ApiModelProperty(value = "毛利额环比增长率")
    @NotNull(message = "毛利额环比增长率不可为空!")
    private String momSaleGross;

    @ApiModelProperty(value = "会员卡同比增长率")
    @NotNull(message = "会员卡同比增长率不可为空!")
    private String yoyMcardQty;

    @ApiModelProperty(value = "会员卡环比增长率")
    @NotNull(message = "会员卡环比增长率不可为空!")
    private String momMcardQty;

    List<MonthOutDataVo> monthOutDataVo;//vo对象

    List<SalesSummaryData>  salesSummaryData;
    List<MonthlyPlanVo> monthlyPlanVos;
    List<GetStoreInData> getStoreInData;
    List<AuditData> auditData;//审核

}
