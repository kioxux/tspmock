package com.gys.business.service.data;

import lombok.Data;

@Data
public class ProductBusinessEsData {
    private String client;

    private String proSite;

    private String proClassName;

    private String proPym;

    private String proSelfCode;

    private String proBarcode;

    private String proBarcode2;

    private String proCommonname;

    private String proName;

    private String proFactoryName;

    private String proRegisterNo;
}
