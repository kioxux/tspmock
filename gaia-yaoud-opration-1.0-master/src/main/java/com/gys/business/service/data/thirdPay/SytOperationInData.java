package com.gys.business.service.data.thirdPay;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel(value = "收银通操作记录查询入参")
@Data
public class SytOperationInData {
    private String client;
    @ApiModelProperty(value = "开始日期 例：20210101")
    private String startDate;
    @ApiModelProperty(value = "结束日期 例：20210202")
    private String endDate;
    @ApiModelProperty(value = "门店编码列表")
    private List<String> storeCode;
    @ApiModelProperty(value = "交易类型 01:消费，34：退款")
    private String transType;
    @ApiModelProperty(value = "交易结果 0：失败，1：成功")
    private String result;
}
