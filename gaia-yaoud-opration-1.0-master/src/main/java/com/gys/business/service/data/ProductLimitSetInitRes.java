package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
@ApiModel
public class ProductLimitSetInitRes implements Serializable {

    private static final long serialVersionUID = -7981973462436223162L;

    Map<String,String> stores;

    Map<String,String> limitTypes;

    Map<String,String> limitFlag;

    Map<String, String> yesOrNo;

}
