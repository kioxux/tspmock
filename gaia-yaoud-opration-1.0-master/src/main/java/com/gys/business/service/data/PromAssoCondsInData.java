package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class PromAssoCondsInData implements Serializable {
    private static final long serialVersionUID = -7075028743305868197L;
    private String clientId;
    private String gspacVoucherId;
    private String gspacSerial;
    private String gspacProId;
    private String gspacQty;
    private String gspacSeriesId;
    private String gspacMemFlag;
    private String gspacInteFlag;
    private String gspacInteRate;
}
