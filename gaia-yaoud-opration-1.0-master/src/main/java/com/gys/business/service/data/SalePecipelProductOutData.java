package com.gys.business.service.data;

import lombok.Data;

/**
 * @desc: 处方登记查询：商品明细
 * @author: ZhangChi
 * @createTime: 2021/12/24 10:39
 */
@Data
public class SalePecipelProductOutData {
    /**
     * 商品编码
     */
    private String gssrProId;

    /**
     * 商品批号
     */
    private String gssrBatchNo;

    /**
     * 数量
     */
    private String gssrQty;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 生产企业
     */
    private String proFactoryName;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 剂型
     */
    private String proForm;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 有效期
     */
    private String gssbVaildDate;
}
