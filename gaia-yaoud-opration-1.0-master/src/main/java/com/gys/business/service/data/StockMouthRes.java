package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class StockMouthRes implements Serializable {
    private static final long serialVersionUID = 4009959654123834392L;

    @ApiModelProperty(value = "加盟商code")
    private String client;

    @ApiModelProperty(value = "加盟商名称")
    private String francName;

    @ApiModelProperty(value = "地点（门店/仓库）code")
    private String siteCode;

    @ApiModelProperty(value = "地点（门店/仓库）名称")
    private String siteName;

    @ApiModelProperty(value = "年月")
    private String yearMonths;

    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "商品规格")
    private String proSpecs;

    @ApiModelProperty(value = "生产厂家")
    private String proFactoryName;

    @ApiModelProperty(value = "单位")
    private String proUnit;

    @ApiModelProperty(value = "批次")
    private String gsmBatch;

    @ApiModelProperty(value = "库存数量")
    private BigDecimal gsmStockQty;

    @ApiModelProperty(value = "合计金额")
    private BigDecimal gsmStockAmt;

    @ApiModelProperty(value = "去税金额")
    private BigDecimal gsmStockAmtWithOutTaxAmt;

    @ApiModelProperty(value = "税金")
    private BigDecimal gsmTaxAmt;


    @ApiModelProperty(value = "最后更新时间")
    private Date lastUpdateTime;

    public BigDecimal getGsmStockAmtWithOutTaxAmt() {
        return this.gsmStockAmt.subtract(this.gsmTaxAmt);
    }

}
