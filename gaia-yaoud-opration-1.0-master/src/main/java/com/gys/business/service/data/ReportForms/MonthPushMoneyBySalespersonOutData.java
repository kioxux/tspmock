package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel
public class MonthPushMoneyBySalespersonOutData {
    private String client;
    @ApiModelProperty(value = "营业员编码")
    private String salerId;
    @ApiModelProperty(value = "营业员名称")
    private String salerName;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "门店名称")
    private String stoName;
    @ApiModelProperty(value = "销售天数")
    private BigDecimal days;
    @ApiModelProperty(value = "提成级别")
    private String saleClass;
    @ApiModelProperty(value = "实收金额")
    private BigDecimal amt;
    @ApiModelProperty(value = "提成合计")
    private BigDecimal deductionWage;
    @ApiModelProperty(value = "提成占比")
    private BigDecimal deductionRate;

    private Integer pageNum;
    private Integer pageSize;
}
