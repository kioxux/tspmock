package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 9:54 2021/7/23
 * @Description：保存-FX医生加点入参
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GaiaDoctorAddPointInData {

    @ApiModelProperty(value = "用户")
    private String client;

    @ApiModelProperty(value = "医生编码")
    private String gsdaDrId;

    @ApiModelProperty(value = "医生姓名")
    private String gsdaDrName;

    @ApiModelProperty(value = "加点率")
    private String gsdaRate;

    @ApiModelProperty(value = "加点金额")
    private String gsdaAmt;

    @ApiModelProperty(value = "修改日期")
    private String gsdaUpdateDate;

    @ApiModelProperty(value = "修改时间")
    private String gsdaUpdateTime;

    @ApiModelProperty(value = "修改用户")
    private String gsdaUpdateEmp;
}
