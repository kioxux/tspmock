package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaPriceRangeMapper;
import com.gys.business.mapper.entity.GaiaPriceRange;
import com.gys.business.service.GaiaPriceRangeService;
import com.gys.business.service.data.PriceRangeInData;
import com.gys.business.service.data.PriceRangeOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Zhangchi
 * @since 2021/11/18/10:41
 */

@Service
@Slf4j
public class GaiaPriceRangeServiceImpl implements GaiaPriceRangeService {
    @Resource
    GaiaPriceRangeMapper gaiaPriceRangeMapper;

    public JsonResult getAreaList() {
        List<Map<String, String>> areaList = this.gaiaPriceRangeMapper.getAreaList();
        return JsonResult.success(areaList, "提示：获取成功！");
    }

    public List<PriceRangeOutData> getPriceRangeByClient(PriceRangeInData priceRangeInData){
        List<PriceRangeOutData> list = this.gaiaPriceRangeMapper.getPriceRangeByClient(priceRangeInData);
        for (PriceRangeOutData outData:list) {
            if (outData.getEndPrice() != null
                    && outData.getEndPrice() == 999999
                    && !outData.getRangeLevel().equals("D")
            ){
                outData.setEndPrice(null);
            }
            if (outData.getRangeLevel().equals("D")){
                outData.setEndPrice(999999);
            }
        }
        return list;
    }

    @Override
    public void savePriceRange(GetLoginOutData userInfo, List<PriceRangeOutData> inDataList) {
        //修改数据校验
        if (checkInData(inDataList)) {
            //获取修改人姓名
            String updateName = this.gaiaPriceRangeMapper.selectUserName(userInfo.getClient(),userInfo.getUserId());
            //修改保存更新
            for (PriceRangeOutData outData : inDataList) {
                GaiaPriceRange range = new GaiaPriceRange();
                range.setCity(outData.getCityId());
                range.setStartPrice(outData.getStartPrice());
                range.setEndPrice(outData.getEndPrice());
                range.setPriceInterval(outData.getPriceInterval());
                range.setUpdateUser(updateName);
                range.setUpdateTime(new Date());
                range.setClient(outData.getClient());
                range.setRangeLevel(outData.getRangeLevel());
                this.gaiaPriceRangeMapper.savePriceRange(range);
            }
        }
    }

    //校验数据,逻辑
    public boolean checkInData(List<PriceRangeOutData> inDataList){
        //按照加盟商对集合分组
        Map<String, List<PriceRangeOutData>> customerMap = inDataList.stream().collect(Collectors.groupingBy(PriceRangeOutData::getClient));
        //遍历加盟商价格区间集合
        for (Map.Entry<String,List<PriceRangeOutData>> entry: customerMap.entrySet()){
            //遍历每个加盟商的集合
            List<PriceRangeOutData> currentList = entry.getValue();
            //根据价格区间类型排序
            currentList.sort(Comparator.comparing(PriceRangeOutData::getRangeLevel));
            for (PriceRangeOutData priceRangeOutData : currentList) {
                //城市必填
                if (StringUtils.isBlank(priceRangeOutData.getCity())){
                    throw new BusinessException("城市不能为空！");
                }
                if (priceRangeOutData.getStartPrice() != null && priceRangeOutData.getPriceInterval() == null){
                    throw new BusinessException("客户ID为"+priceRangeOutData.getClient()+"的价格间隔未填写完整，请重新填写！");
                }
                //判断起始价格大于结束价格
                if (priceRangeOutData.getStartPrice() != null) {
                    if (priceRangeOutData.getEndPrice() != null) {
                        if (priceRangeOutData.getStartPrice() >= priceRangeOutData.getEndPrice()) {
                            throw new BusinessException("客户ID为" + priceRangeOutData.getClient() + "的" + priceRangeOutData.getRangeLevel() + "档存在起始价格不小于结束价格情况，请重新填写！");
                        }
                    } else {
                        priceRangeOutData.setEndPrice(999999);
                    }
                } else {
                    priceRangeOutData.setEndPrice(null);
                    priceRangeOutData.setPriceInterval(null);
                }
                //判断价格间隔符合价格区间条件
                if (priceRangeOutData .getStartPrice() != null
                        && (priceRangeOutData.getEndPrice() - priceRangeOutData.getStartPrice() <= priceRangeOutData.getPriceInterval()
                        || priceRangeOutData.getPriceInterval() > 20)
                ){
                    throw new BusinessException("客户ID为"+priceRangeOutData.getClient()+"的价格间隔不满足条件，请重新填写！");
                }
            }
            for (int i = 0; i < currentList.size(); i++){
                //判断价格区间连续
                if (currentList.get(i).getStartPrice() == null){
                    for (int j = i + 1; j < currentList.size(); j++){
                        if (currentList.get(j).getStartPrice() != null){
                            throw new BusinessException("客户ID为"+currentList.get(j).getClient()+"的价格区间存在中断情况，请重新填写！");
                        }
                    }
                }
                if(i < currentList.size() - 1
                        && currentList.get(i + 1).getPriceInterval() != null
                        && currentList.get(i).getPriceInterval() >= currentList.get(i + 1).getPriceInterval())
                {
                    throw new BusinessException("客户ID为"+currentList.get(i).getClient()+"的价格间隔不满足递增条件，请重新填写！");
                }
            }
        }
        return true;
    }

    @Override
    public void insertPriceRange() {
        List<PriceRangeOutData> priceRangeOutData = this.gaiaPriceRangeMapper.getClient();
        if (priceRangeOutData.size() > 0){
            for (PriceRangeOutData outData : priceRangeOutData){
                for (int i = 'A'; i <= 'D'; i++){
                    if (i == 'A'){
                        GaiaPriceRange gaiaPriceRange = new GaiaPriceRange();
                        gaiaPriceRange.setClient(outData.getClient());
                        gaiaPriceRange.setCity(outData.getCity());
                        gaiaPriceRange.setRangeLevel("A");
                        gaiaPriceRange.setStartPrice(0);
                        gaiaPriceRange.setEndPrice(10);
                        gaiaPriceRange.setPriceInterval(1);
                        this.gaiaPriceRangeMapper.insertPriceRange(gaiaPriceRange);
                    }
                    if (i == 'B'){
                        GaiaPriceRange gaiaPriceRange = new GaiaPriceRange();
                        gaiaPriceRange.setClient(outData.getClient());
                        gaiaPriceRange.setCity(outData.getCity());
                        gaiaPriceRange.setRangeLevel("B");
                        gaiaPriceRange.setStartPrice(10);
                        gaiaPriceRange.setEndPrice(50);
                        gaiaPriceRange.setPriceInterval(5);
                        this.gaiaPriceRangeMapper.insertPriceRange(gaiaPriceRange);
                    }
                    if (i == 'C'){
                        GaiaPriceRange gaiaPriceRange = new GaiaPriceRange();
                        gaiaPriceRange.setClient(outData.getClient());
                        gaiaPriceRange.setCity(outData.getCity());
                        gaiaPriceRange.setRangeLevel("C");
                        gaiaPriceRange.setStartPrice(50);
                        gaiaPriceRange.setEndPrice(100);
                        gaiaPriceRange.setPriceInterval(10);
                        this.gaiaPriceRangeMapper.insertPriceRange(gaiaPriceRange);
                    }
                    if (i == 'D'){
                        GaiaPriceRange gaiaPriceRange = new GaiaPriceRange();
                        gaiaPriceRange.setClient(outData.getClient());
                        gaiaPriceRange.setCity(outData.getCity());
                        gaiaPriceRange.setRangeLevel("D");
                        gaiaPriceRange.setStartPrice(100);
                        gaiaPriceRange.setEndPrice(999999);
                        gaiaPriceRange.setPriceInterval(20);
                        this.gaiaPriceRangeMapper.insertPriceRange(gaiaPriceRange);
                    }
                }
            }
        }
    }
}
