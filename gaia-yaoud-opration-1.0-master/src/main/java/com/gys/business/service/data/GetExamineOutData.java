package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
@Data
public class GetExamineOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "验收单号")
    private String gsehVoucherId;
    @ApiModelProperty(value = "验收日期")
    private String gsehDate;
    @ApiModelProperty(value = "收货单号")
    private String gsehVoucherAcceptId;
    @ApiModelProperty(value = "发货地点")
    private String gsehFrom;
    @ApiModelProperty(value = "收货地点")
    private String gsehTo;
    @ApiModelProperty(value = "单据类型")
    private String gsehType;
    @ApiModelProperty(value = "验收状态")
    private String gsehStatus;
    @ApiModelProperty(value = "验收金额")
    private BigDecimal gsehTotalAmt;
    @ApiModelProperty(value = "验收数量")
    private BigDecimal gsehTotalQty;
    @ApiModelProperty(value = "备注")
    private String gsehRemaks;
    private List<GetExamineDetailOutData> acceptDetailInDataList;
    @ApiModelProperty(value = "")
    private String gsadProId;
    @ApiModelProperty(value = "是否可驳回 0 是 1 否")
    private Integer isReject;
    @ApiModelProperty(value = "医保机构定点编码")
    private String stoYbCode;
}
