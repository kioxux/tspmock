package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class DsfPordWtproModInData {

    /**
     * 加盟商
     */
    private String client;
    /**
     * 委托配送商品变更列表
     */
    List<DsfPordWtproModReqInData> list;
}
