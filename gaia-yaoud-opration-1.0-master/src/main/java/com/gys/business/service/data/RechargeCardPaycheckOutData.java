package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class RechargeCardPaycheckOutData {
    @ApiModelProperty(value = "支付方式1")
    private String gsrcpPaymentNo1;
    @ApiModelProperty(value = "支付方式名称1")
    private String gsrcpPaymentName1;
    @ApiModelProperty(value = "支付金额1")
    private BigDecimal gsrcpPaymentAmt1;
    @ApiModelProperty(value = "支付方2")
    private String gsrcpPaymentNo2;
    @ApiModelProperty(value = "支付方式名称2")
    private String gsrcpPaymentName2;
    @ApiModelProperty(value = "支付金额2")
    private BigDecimal gsrcpPaymentAmt2;
    @ApiModelProperty(value = "支付方式3")
    private String gsrcpPaymentNo3;
    @ApiModelProperty(value = "支付方式名称3")
    private String gsrcpPaymentName3;
    @ApiModelProperty(value = "支付金额3")
    private BigDecimal gsrcpPaymentAmt3;
    @ApiModelProperty(value = "支付方式4")
    private String gsrcpPaymentNo4;
    @ApiModelProperty(value = "支付方式名称4")
    private String gsrcpPaymentName4;
    @ApiModelProperty(value = "支付金额4")
    private BigDecimal gsrcpPaymentAmt4;
}
