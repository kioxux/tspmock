package com.gys.business.service;

/**
 * @Author ：gyx
 * @Date ：Created in 13:57 2021/11/19
 * @Description：会员活跃度service
 * @Modified By：gyx
 * @Version:
 */
public interface IMemberActiveService {
    void dailyMaintenMemberActive(String param);
}
