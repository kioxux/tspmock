package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2021/4/20
 */
@Data
public class CommodityCostDto implements Serializable {
    private static final long serialVersionUID = -4233255125304033594L;


    /**
     * 商品编码
     */
    private String gsppProId;

    /**
     * 零售价
     */
    private String gsppPriceNormal;

    /**
     * 成本价
     */
    private String averagePrice;

    /**
     * 通用名
     */
    private String commonName;


    /**
     * 移动平均价
     */
    private String matMovPrice;
}
