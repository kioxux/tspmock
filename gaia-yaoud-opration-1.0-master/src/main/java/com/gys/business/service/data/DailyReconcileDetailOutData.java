package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DailyReconcileDetailOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "单号")
    private String gspddVoucherId;
    @ApiModelProperty(value = "支付类型  1.销售支付 2.储值卡充值 3.挂号支付")
    private String gspddPayType;
    @ApiModelProperty(value = "销售支付方式")
    private String gspddSalePaymethodId;
    @ApiModelProperty(value = "销售应收金额")
    private String gspddSaleReceivableAmt;
    @ApiModelProperty(value = "销售对账金额")
    private String gspddSaleInputAmt;
    @ApiModelProperty(value = "储值卡充值支付方式")
    private String gspddRecardPaymethodId;
    @ApiModelProperty(value = "储值卡充值应收金额")
    private String gspddRecardReceivableAmt;
    @ApiModelProperty(value = "储值卡对账金额")
    private String gspddRecardInputAmt;
    @ApiModelProperty(value = "行号")
    private Integer indexDetail;
}
