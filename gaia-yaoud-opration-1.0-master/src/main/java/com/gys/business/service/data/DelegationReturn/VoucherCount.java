package com.gys.business.service.data.DelegationReturn;

import lombok.Data;

@Data
public class VoucherCount {
    private String voucherId;
    private Integer counts;

    public VoucherCount(String voucherId, Integer counts) {
        this.voucherId = voucherId;
        this.counts = counts;
    }
}
