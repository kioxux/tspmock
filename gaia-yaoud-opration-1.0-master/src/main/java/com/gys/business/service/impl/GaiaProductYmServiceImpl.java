package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaProductYmDmMapper;
import com.gys.business.mapper.GaiaProductYmMapper;
import com.gys.business.mapper.entity.GaiaProductYmDm;
import com.gys.business.service.GaiaProductYmService;
import com.gys.business.service.data.GaiaProductYmInData;
import com.gys.business.service.data.GaiaProductYmOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.excel.ObjectUtil;
import com.gys.util.yaomeng.YaomengApiUtil;
import com.gys.util.yaomeng.YaomengDrugAddBean;
import com.gys.util.yaomeng.YaomengDrugDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 药盟商品信息(GaiaProductYm)表服务实现类
 *
 * @author XIAOZY
 * @since 2021-11-11 20:26:22
 */
@Service("gaiaProductYmService")
public class GaiaProductYmServiceImpl implements GaiaProductYmService {
    @Resource
    private GaiaProductYmMapper gaiaProductYmMapper;
    @Resource
    private GaiaProductYmDmMapper gaiaProductYmDmMapper;

   /* @Override
    public List<GaiaProductYmOutData> listProInfo(GaiaProductYmInData param) {
        return gaiaProductYmMapper.listProInfo(param);
    }

    @Override
    public List<GaiaProductYmOutData> listYMProInfo(GaiaProductYmInData param) {
        return gaiaProductYmMapper.listYMProInfo(param);
    }*/

    /*@Override
    @Transactional
    public boolean saveYMProInfo(GaiaProductYmInData param, GetLoginOutData userInfo) {
        //1.0先查询 对码关系是否已存在  erp药品对饮关系
        GaiaProductYmDm dm = gaiaProductYmDmMapper.getDMDetail(param);
        //2.0
        Date now = new Date();
        if (ObjectUtil.isEmpty(dm)) {
            //新增
            GaiaProductYmDm insert = new GaiaProductYmDm();
            insert.setClient(param.getClient());
            insert.setDmBrId(param.getStoCode());
            insert.setProCode(param.getProCode());
            insert.setProSelfCode(param.getProSelfCode());
            insert.setYmProCode(param.getYmProCode());
            insert.setIsDelete(0);
            insert.setCreateUser(userInfo.getUserId());
            insert.setCreateTime(now);
            insert.setUpdateUser(userInfo.getUserId());
            insert.setUpdateTime(now);
            gaiaProductYmDmMapper.insert(insert);
            addToYaoMeng(insert);
        } else {
            //更新
            dm.setYmProCode(param.getYmProCode());
            dm.setUpdateUser(userInfo.getUserId());
            dm.setUpdateTime(now);
            gaiaProductYmDmMapper.update(dm);
            addToYaoMeng(dm);
        }
        return true;
    }*/

    /*private void addToYaoMeng(GaiaProductYmDm insert) {
        List<YaomengDrugAddBean> addList = new ArrayList<>();
        YaomengDrugAddBean addBean = new YaomengDrugAddBean();
        addBean.setDrugId(Long.valueOf(insert.getYmProCode()));
        addBean.setErpNo(insert.getProSelfCode());
        addList.add(addBean);
        //todo 药盟正式编码  后期修改
        YaomengApiUtil.drugAdd("yd001",addList);
    }*/

    /*@Override
    public List<GaiaProductYmOutData> listDMInfo(GaiaProductYmInData param, GetLoginOutData userInfo) {
        return gaiaProductYmDmMapper.listDMInfo(param);
    }*/
}
