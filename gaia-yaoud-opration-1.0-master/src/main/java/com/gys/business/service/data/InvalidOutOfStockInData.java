package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class InvalidOutOfStockInData implements Serializable {

    private static final long serialVersionUID = 6815872791400658731L;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String clientId;

    /**
     * 店号
     */
    @ApiModelProperty(value = "店号")
    private String brId;

    /**
     * 商品定位 T-淘汰 D-删除
     */
    @ApiModelProperty(value = "商品定位 T-淘汰 D-删除")
    private String position;

    /**
     * 商品自编码
     */
    @ApiModelProperty(value = "商品自编码")
    private String productId;
}
