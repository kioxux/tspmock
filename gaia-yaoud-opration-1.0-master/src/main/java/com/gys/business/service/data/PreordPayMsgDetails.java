package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xiaoyuan on 2020/12/24
 */
@Data
public class PreordPayMsgDetails implements Serializable {
    private static final long serialVersionUID = 7071433843616937018L;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 订购门店
     */
    @ApiModelProperty(value = "订购门店")
    private String gsppmBrId;

    /**
     * 订购单号
     */
    @ApiModelProperty(value = "订购单号")
    private String gsppmVoucherId;

    /**
     * 订购日期
     */
    @ApiModelProperty(value = "订购日期")
    private String gsppmDate;

    /**
     * 支付编码
     */
    @ApiModelProperty(value = "支付编码")
    private String gsppmId;

    /**
     * 支付名称
     */
    @ApiModelProperty(value = "支付名称")
    private String gsppmName;

    /**
     * 支付卡号
     */
    @ApiModelProperty(value = "支付卡号")
    private String gsppmCardNo;

    /**
     * 支付金额
     */
    @ApiModelProperty(value = "支付金额")
    private BigDecimal gsppmAmt;

    /**
     * RMB收款金额
     */
    @ApiModelProperty(value = "RMB收款金额")
    private BigDecimal gsppmRmbAmt;

    /**
     * 找零金额
     */
    @ApiModelProperty(value = "找零金额")
    private BigDecimal gsppmZlAmt;

    /**
     * 是否日结 0/空为否1为是
     */
    @ApiModelProperty(value = "是否日结")
    private String gsppmDayreport;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String gspmRemark1;


}
