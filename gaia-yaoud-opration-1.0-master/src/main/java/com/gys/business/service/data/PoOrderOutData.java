package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PoOrderOutData {
    private String poId;//采购订单号
    private String poDate;//订单日期
    private String supId;//供应商编码
    private String supName;//供应商名称
    private String supplier;//供应商（供应商编码-供应商名称）
    private Integer lineNum;//订单行数
    private BigDecimal poQty;//订单总数量
    private BigDecimal poAmt;//订单总金额
}
