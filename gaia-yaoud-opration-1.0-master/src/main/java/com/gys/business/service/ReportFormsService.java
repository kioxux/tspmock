package com.gys.business.service;

import com.gys.business.service.data.GetPayInData;
import com.gys.business.service.data.GetPayTypeOutData;
import com.gys.business.service.data.ReportForms.*;
import com.gys.common.data.FuzzyQueryForConditionInData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ReportFormsService {
    PageInfo<SupplierDealOutData> selectSupplierDealPage(SupplierDealInData inData);

    PageInfo<SupplierDealOutData> selectSupplierDealDetailPage(SupplierDealInData inData);

    PageInfo<WholesaleSaleOutData> selectWholesaleSalePage(WholesaleSaleInData data);

    PageInfo<WholesaleSaleOutData> selectWholesaleSaleDetailPage(WholesaleSaleInData data);

    PageInfo<BusinessDocumentOutData> selectBusinessDocumentPage(BusinessDocumentInData data);

    PageInfo<BusinessDocumentOutData> selectBusinessDocumentDetailPage(BusinessDocumentInData data);

    PageInfo<StoreRateSellOutData> selectStoreRateSellDetailPage(StoreRateSellInData data);

    PageInfo<StoreRateSellOutData>  selectStoreRateSellPage(StoreRateSellInData data);

    PageInfo selectStoreSaleByDay(StoreSaleDayInData inData);

    PageInfo selectStoreSaleByDayAndClient(StoreSaleDayInData inData);

    PageInfo selectInventoryChangeSummary(InventoryChangeSummaryInData data);

    PageInfo selectInventoryChangeSummaryDetail(InventoryChangeSummaryInData data);

    PageInfo selectPayDayreport(PayDayreportInData inData);

    PageInfo selectProductSalesBySupplier(ProductSalesBySupplierInData inData);

    PageInfo selectProductSalesBySupplierByStore(ProductSalesBySupplierInData inData);

    PageInfo selectStoreSaleByDate(StoreSaleDateInData data);

    PageInfo selectProductSaleByClient(StoreProductSaleClientInData data);

    PageInfo selectProductSaleByStore(StoreProductSaleStoreInData data);

    JsonResult selectDistributionData(StoreDistributaionDataInData data);

    JsonResult getDcCode(String client);

    JsonResult getCusList(String client, Map<String,String> dcCode);

    List<StoreDistributaionDataOutData> exportCSV(StoreDistributaionDataInData inData);

    PageInfo<InventoryChangeCheckOutData> selectInventoryStockCheckListBySto(InventoryChangeCheckInData data);

    void checkListStoExport(InventoryChangeCheckInData data, HttpServletResponse response);
    /**
     * 供应商单据明细列表
     * @param request 权限校验
     * @param data 请求参数
     * @return
     */
    PageInfo<SelectSupplierDetailPageListResponse> selectSupplierDetailPageList(SelectSupplierDetailPageListData data);

    /**
     * 供应商单据明细列表导出
     * @param data
     * @return
     */
    SXSSFWorkbook export(SelectSupplierDetailPageListData data);

    /**
     * 供应商单据汇总列表
     * @param
     * @param data
     * @return
     */
    PageInfo<SelectSupplierDetailPageListResponse> selectSupplierSummaryPageList(SelectSupplierSummaryPageList data);
    /**
     * 供应商单据汇总列表导出
     * @param
     * @param data
     * @return
     */
    SXSSFWorkbook supplierExport(SelectSupplierSummaryPageList data);


    void checkListDcExport(InventoryChangeCheckInData data, HttpServletResponse response);

    PageInfo<InventoryChangeCheckOutData> selectInventoryStockCheckListByDc(InventoryChangeCheckInData data);

    List<String> selectSoHeadRemark(FuzzyQueryForConditionInData conditionQuery);


    void selectWholesaleSaleDetailPageExport(HttpServletResponse response, WholesaleSaleInData data);


    void selectWholesaleSalePageExport(HttpServletResponse response, WholesaleSaleInData data);

    // 查询csv列表
    List<SupplierDealOutDataCSV> exportSupplierDealDetailPage(SupplierDealInData data);

    // 根据加盟商获取支付方式列表
    List<GetPayTypeOutData> payTypeListByClient(GetPayInData inData);

    void updatePayTypeListValue(String client, List<HashMap<String, String>> updateList);

    PageInfo<WholesaleSaleOutData> selectDistributionInvoiceByBatch(WholesaleSaleInData data);

    void exportDistributionInvoiceByBatch(HttpServletResponse response, WholesaleSaleInData data);

    PageInfo<WholesaleSaleOutData> selectDistributionInvoiceDetailByBatch(WholesaleSaleInData data);

    void exportDistributionInvoiceDetailByBatch(HttpServletResponse response, WholesaleSaleInData data);
}
