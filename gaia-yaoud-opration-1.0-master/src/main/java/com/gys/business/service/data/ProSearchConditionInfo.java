package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ProSearchConditionInfo {
    private static final long serialVersionUID = -8599651801018827232L;
    //1:AND 并且　2:or 或者
//    @ApiModelProperty(value = "关系")
//    private String relation;
    @ApiModelProperty(value = "名称")
    private String designation;
    //1:包含 2:等于 3:不等于
    @ApiModelProperty(value = "条件")
    private String condition;
    @ApiModelProperty(value = "内容")
    private String content;

}
