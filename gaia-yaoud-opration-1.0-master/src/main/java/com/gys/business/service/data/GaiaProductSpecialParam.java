package com.gys.business.service.data;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value="com-gys-business-service-data-GaiaProductSpecialParam")
public class GaiaProductSpecialParam extends BaseRowModel implements Serializable {

    /**
     * 序号
     */
    @ApiModelProperty(value="序号")
    @ExcelProperty(index = 0)
    private String index;

    /**
    * 省份
    */
    @ApiModelProperty(value="省份")
    @ExcelProperty(index = 1)
    private String proProv;

    /**
    * 地市
    */
    @ApiModelProperty(value="地市")
    @ExcelProperty(index = 2)
    private String proCity;

    /**
    * 参数名称-季节 1-春，2-夏，3-秋，4-冬
    */
    @ApiModelProperty(value="参数名称-季节 1-春，2-夏，3-秋，4-冬")
    @ExcelProperty(index = 3)
    private String proName;

    /**
     * 省份列表
     */
    @ApiModelProperty(value="省份列表")
    private List<String> provinceList;

    /**
     * 地市列表
     */
    @ApiModelProperty(value="地市列表")
    private List<String> cityList;

    /**
    * 开始日期
    */
    @ApiModelProperty(value="开始日期")
    @ExcelProperty(index = 4)
    private String proStartDate;

    /**
    * 结束日期
    */
    @ApiModelProperty(value="结束日期")
    @ExcelProperty(index = 5)
    private String proEndDate;

    /**
    * 创建人
    */
    @ApiModelProperty(value="创建人")
    private String proCreateUser;

    /**
    * 创建日期
    */
    @ApiModelProperty(value="创建日期")
    private String proCreateDate;

    /**
    * 创建时间
    */
    @ApiModelProperty(value="创建时间")
    private String proCreateTime;

    /**
    * 最后修改人
    */
    @ApiModelProperty(value="最后修改人")
    private String lastUpdateUser;

    /**
    * 最后修改日期
    */
    @ApiModelProperty(value="最后修改日期")
    private String lastUpdateDate;

    /**
    * 最后修改时间
    */
    @ApiModelProperty(value="最后修改时间")
    private String lastUpdateTime;

    /**
     * 分组
     */
    @ApiModelProperty(value="分组")
    @ExcelProperty(index = 6)
    private String group;

    /**
     * 省代码
     */
    @ApiModelProperty(value="省代码")
    private String proProvCode;

    /**
     * 市代码
     */
    @ApiModelProperty(value="市代码")
    private String proCityCode;

    /**
     * 季节代码
     */
    @ApiModelProperty(value="季节代码")
    private String proSeason;

    /**
     * 查询标识 1-春 2-夏 3-秋 4-冬
     */
    @ApiModelProperty(value="查询标识 1-春 2-夏 3-秋 4-冬")
    private String flag;

    /**
     * 修改起始日期
     */
    @ApiModelProperty(value="修改起始日期")
    private String beginDate;

    /**
     * 修改结束日期
     */
    @ApiModelProperty(value="修改结束日期")
    private String endDate;

    private static final long serialVersionUID = 1L;


}