package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
@Data
public class GetExamineInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "店号")
    private String gsehBrId;
    @ApiModelProperty(value = "验收单号")
    private String gsehVoucherId;
    private String gsehDate;
    @ApiModelProperty(value = "收货单号")
    private String gsehVoucherAcceptId;
    private String gsehFrom;
    private String gsehTo;
    private String gsehType;
    private String gsehStatus;
    private BigDecimal gsehTotalAmt;
    private String gsehTotalQty;
    private String gsehRemaks;
    private List<GetExamineDetailInData> examineDetailInDataList;
    private String gsedProId;
    private String storeCode;
    @ApiModelProperty(value = "验收人员")
    private String gsedEmp;
    private String gsedBatchNo;
    private Integer pageNum;
    private Integer pageSize;
    private String sendType;
    //是否排除CS 1:是 0:否
    private String notCS;
}
