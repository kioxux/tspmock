package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StockChangeInfo {
    private String client;

    /**
     * 店号
     */
    private String gsbcBrId;

    /**
     * 销售单号
     */
    private String gsbcVoucherId;

    /**
     * 异动日期
     */
    private String gsbcDate;

    /**
     * 行号
     */
    private String gsbcSerial;

    /**
     * 商品id
     */
    private String gsbcProId;

    /**
     * 批次
     */
    private String gsbcBatch;

    /**
     * 批号
     */
    private String gsbcBatchNo;

    /**
     * 数量
     */
    private BigDecimal gsbcQty;

    /**
     * 效期
     */
    private String gssbValidDate;
}
