package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 库存审计平台-对比结果查询参数类
 */
@Data
public class StockCompareCondition implements Serializable {


    private static final long serialVersionUID = -3130974915594670888L;
    @ApiModelProperty(value = "客户-地点集合", required = true)
    private Map<String, List<String>> clientSiteMap;

    private List<String> clients;

    private List<String> sites;

    @ApiModelProperty(value = "年月日", required = true)
    private String chooseDate;

    @ApiModelProperty(value = "对比任务号", required = true)
    private String compareTaskNo;

    @ApiModelProperty(value = "页面展示数量", required = false)
    public int pageSize;

    @ApiModelProperty(value = "页数", required = false)
    private int pageNum;

}
