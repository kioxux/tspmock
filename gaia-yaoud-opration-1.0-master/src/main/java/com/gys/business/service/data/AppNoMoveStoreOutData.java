package com.gys.business.service.data;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class AppNoMoveStoreOutData implements Serializable {

    @ApiModelProperty(value = "门店编码")
    private String brId;

    @ApiModelProperty(value = "门店名")
    private String brName;

    @ApiModelProperty(value = "不动销库存品项数")
    private BigDecimal noMoveCount;

    @ApiModelProperty(value = "不动销库存成本额")
    private BigDecimal noMoveCostAmt;

    @ApiModelProperty(value = "总成本额")
    private BigDecimal totalCostAmt;

    @ApiModelProperty(value = "不动销库存占比")
    private BigDecimal noMoveCostAmtProp;
}
