package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 支付信息表
 *
 * @author xiaoyuan on 2020/12/11
 */
@Data
public class PaymentInformationOutData implements Serializable {
    private static final long serialVersionUID = 2798300943607343050L;

    private Integer index;

    /**
     * 退货金额
     */
    private String returnAmount;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String gsspmBrId;

    /**
     * 日期
     */
    private String gsspmDate;

    /**
     * 销售单号
     */
    private String gsspmBillNo;

    /**
     * 支付编码
     */
    private String gsspmId;

    /**
     * 支付类型 1.销售支付 2.储值卡充值 3.挂号支付
     */
    private String gsspmType;

    /**
     * 支付名称
     */
    private String gsspmName;

    /**
     * 支付卡号
     */
    private String gsspmCardNo;

    /**
     * 支付金额
     */
    private String gsspmAmt;

    /**
     * RMB收款金额
     */
    private String gsspmRmbAmt;

    /**
     * 找零金额
     */
    private String gsspmZlAmt;

    /**
     * 是否日结 0/空为否1为是
     */
    private String gsspmDayreport;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 抵现积分
     */
    private String gsshIntegralCash;

    /**
     * 抵现金额
     */
    private String gsshIntegralCashAmt;

    /**
     * 抵用券卡号
     */
    private String gsshDyqNo;

    /**
     * 抵用券金额
     */
    private String gsshDyqAmt;

    /**
     * 抵用券金额
     */
    private String gsecAmt;

    /**
     * 是否为接口
     */
    private String gspmFalg;

    /**
     * 退货限制天数
     */
    private String gspmPayBackDays;

    /**
     * 是否可退现金 /0:否/1:是
     */
    private String gspmPayBackCash;

}
