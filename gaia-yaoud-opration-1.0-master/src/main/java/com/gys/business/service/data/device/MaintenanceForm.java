package com.gys.business.service.data.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/24 17:22
 **/
@Data
@ApiModel
public class MaintenanceForm {
    @ApiModelProperty(value = "单号")
    private String voucherId;
    @ApiModelProperty(value = "审核状态：1-审核 0-未审核")
    private String status;
    @ApiModelProperty(value = "检查日期")
    private String checkDate;
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(hidden = true)
    private String gsdcBrId;


    /**
     * 地点集合
     */
    private List<String> siteCodes;
}
