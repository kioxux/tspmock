package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class PushStoreHistoryData implements Serializable {

    @ApiModelProperty(value = "日期（周或月）")
    private String weeksOrMonths;

    @ApiModelProperty(value = "门店名")
    private String storeName;

    @ApiModelProperty(value = "推送次数")
    private String totalCount;

    @ApiModelProperty(value = "已完成次数")
    private String finishCount;

    @ApiModelProperty(value = "未完成次数")
    private String notFinishCount;

}
