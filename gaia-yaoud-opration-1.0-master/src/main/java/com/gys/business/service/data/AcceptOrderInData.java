package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AcceptOrderInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店")
    private String stoCode;
    @ApiModelProperty(value = "收货单号")
    private String voucherId;
    @ApiModelProperty(value = "商品名 商品编码 助记码（模糊匹配）")
    private String nameOrCode;
    @ApiModelProperty(value = "起始时间")
    private String startDate;
    @ApiModelProperty(value = "结束时间")
    private String endDate;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    @ApiModelProperty(value = "供应商名称或编码（模糊匹配）")
    private String supNameOrCode;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "门店")
    private String[] brIdList;

}
