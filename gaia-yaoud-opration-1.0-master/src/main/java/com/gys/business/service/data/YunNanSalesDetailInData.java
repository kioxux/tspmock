package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/12 18:09
 */
@Data
public class YunNanSalesDetailInData {
    @ApiModelProperty("唯一编号")
    private String seqNo;
    @ApiModelProperty("机构编号")
    private String instNo;
    @ApiModelProperty("药店编号")
    private String storeNo;
    @ApiModelProperty("药店名称")
    private String storeName;
    @ApiModelProperty("药店所在区域")
    private Integer areaNo;
    @ApiModelProperty("销售日期")
    private String date;
    @ApiModelProperty("订单编号")
    private String orderNo;
    @ApiModelProperty("药品本位码")
    private String nativeCode;
    @ApiModelProperty("药品编号")
    private String drugNo;
    @ApiModelProperty("药品名称")
    private String drugName;
    @ApiModelProperty("药品规格")
    private String spec;
    @ApiModelProperty("生产厂商")
    private String manufacturer;
    @ApiModelProperty("药品批次号")
    private String batchNo;
    @ApiModelProperty("销售数量")
    private BigDecimal amount;
    @ApiModelProperty("药品单位")
    private String unit;
    @ApiModelProperty("药店剩余库存量")
    private BigDecimal stock;
    @ApiModelProperty("买药人手机号")
    private String buyerPhone;

}
