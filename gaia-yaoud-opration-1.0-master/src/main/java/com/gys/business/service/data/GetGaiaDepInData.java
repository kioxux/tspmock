//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetGaiaDepInData {
    private String stoCode;
    private String stoName;

    public GetGaiaDepInData() {
    }

    public String getStoCode() {
        return this.stoCode;
    }

    public String getStoName() {
        return this.stoName;
    }

    public void setStoCode(final String stoCode) {
        this.stoCode = stoCode;
    }

    public void setStoName(final String stoName) {
        this.stoName = stoName;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetGaiaDepInData)) {
            return false;
        } else {
            GetGaiaDepInData other = (GetGaiaDepInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$stoCode = this.getStoCode();
                Object other$stoCode = other.getStoCode();
                if (this$stoCode == null) {
                    if (other$stoCode != null) {
                        return false;
                    }
                } else if (!this$stoCode.equals(other$stoCode)) {
                    return false;
                }

                Object this$stoName = this.getStoName();
                Object other$stoName = other.getStoName();
                if (this$stoName == null) {
                    if (other$stoName != null) {
                        return false;
                    }
                } else if (!this$stoName.equals(other$stoName)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetGaiaDepInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $stoCode = this.getStoCode();
        result = result * 59 + ($stoCode == null ? 43 : $stoCode.hashCode());
        Object $stoName = this.getStoName();
        result = result * 59 + ($stoName == null ? 43 : $stoName.hashCode());
        return result;
    }

    public String toString() {
        return "GetGaiaDepInData(stoCode=" + this.getStoCode() + ", stoName=" + this.getStoName() + ")";
    }
}
