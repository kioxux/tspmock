package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@ApiModel(value="com-gys-business-service-data-GaiaChannelProduct")
@Data
public class GaiaChannelProductOutData {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 渠道
    */
    @ApiModelProperty(value="渠道")
    private String gcdChannel;
    private String gcdChannelName;

    /**
    * 门店
    */
    @ApiModelProperty(value="门店")
    private String gcpStoreCode;

    /**
    * 商品编码
    */
    @ApiModelProperty(value="商品编码")
    private String gcpProCode;

    /**
    * 渠道价格
    */
    @ApiModelProperty(value="渠道价格")
    private BigDecimal gcpChannelPrice;

    /**
    * 状态（1-上架，2-下架）
    */
    @ApiModelProperty(value="状态（1-上架，2-下架）")
    private String gcpStatus;

    @ApiModelProperty(value="门店名称")
    private String stoName;

    @ApiModelProperty(value="单位")
    private String proUnit;

    @ApiModelProperty(value="定位")
    private String proPosition;

    @ApiModelProperty(value="销售等级")
    private String proSlaeClass;

    @ApiModelProperty(value="成本价")
    private BigDecimal gsppPriceNormal;

    @ApiModelProperty(value="销售价")
    private BigDecimal price;

    @ApiModelProperty(value="库存")
    private BigDecimal gssQty;

    /**
    * 创建人
    */
    @ApiModelProperty(value="创建人")
    private String gcpCreateBy;

    /**
    * 创建日期
    */
    @ApiModelProperty(value="创建日期")
    private String gcpCreateDate;

    /**
    * 创建时间
    */
    @ApiModelProperty(value="创建时间")
    private String gcpCreateTime;

    /**
    * 更新人
    */
    @ApiModelProperty(value="更新人")
    private String gcpUpdateBy;

    /**
    * 更新日期
    */
    @ApiModelProperty(value="更新日期")
    private String gcpUpdateDate;

    /**
    * 更新时间
    */
    @ApiModelProperty(value="更新时间")
    private String gcpUpdateTime;

    @ApiModelProperty(value="通用名")
    private String proCommonname;

    @ApiModelProperty(value="规格")
    private String proSpecs;

    @ApiModelProperty(value="厂商")
    private String proFactoryName;

    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;

    private BigDecimal grossRate;

    private String grossRateC;



}