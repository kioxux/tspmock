//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class ProEffectInData {
    private String gssbProId;
    private String proCode;
    private String proClass;
    private String effectDay;
    private String clientId;
    private String brId;
    private Integer pageNum;
    private Integer pageSize;

    public ProEffectInData() {
    }

    public String getGssbProId() {
        return this.gssbProId;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getProClass() {
        return this.proClass;
    }

    public String getEffectDay() {
        return this.effectDay;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public Integer getPageNum() {
        return this.pageNum;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setGssbProId(final String gssbProId) {
        this.gssbProId = gssbProId;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setProClass(final String proClass) {
        this.proClass = proClass;
    }

    public void setEffectDay(final String effectDay) {
        this.effectDay = effectDay;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public void setPageNum(final Integer pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final Integer pageSize) {
        this.pageSize = pageSize;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ProEffectInData)) {
            return false;
        } else {
            ProEffectInData other = (ProEffectInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label107: {
                    Object this$gssbProId = this.getGssbProId();
                    Object other$gssbProId = other.getGssbProId();
                    if (this$gssbProId == null) {
                        if (other$gssbProId == null) {
                            break label107;
                        }
                    } else if (this$gssbProId.equals(other$gssbProId)) {
                        break label107;
                    }

                    return false;
                }

                Object this$proCode = this.getProCode();
                Object other$proCode = other.getProCode();
                if (this$proCode == null) {
                    if (other$proCode != null) {
                        return false;
                    }
                } else if (!this$proCode.equals(other$proCode)) {
                    return false;
                }

                Object this$proClass = this.getProClass();
                Object other$proClass = other.getProClass();
                if (this$proClass == null) {
                    if (other$proClass != null) {
                        return false;
                    }
                } else if (!this$proClass.equals(other$proClass)) {
                    return false;
                }

                label86: {
                    Object this$effectDay = this.getEffectDay();
                    Object other$effectDay = other.getEffectDay();
                    if (this$effectDay == null) {
                        if (other$effectDay == null) {
                            break label86;
                        }
                    } else if (this$effectDay.equals(other$effectDay)) {
                        break label86;
                    }

                    return false;
                }

                label79: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label79;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label79;
                    }

                    return false;
                }

                label72: {
                    Object this$brId = this.getBrId();
                    Object other$brId = other.getBrId();
                    if (this$brId == null) {
                        if (other$brId == null) {
                            break label72;
                        }
                    } else if (this$brId.equals(other$brId)) {
                        break label72;
                    }

                    return false;
                }

                Object this$pageNum = this.getPageNum();
                Object other$pageNum = other.getPageNum();
                if (this$pageNum == null) {
                    if (other$pageNum != null) {
                        return false;
                    }
                } else if (!this$pageNum.equals(other$pageNum)) {
                    return false;
                }

                Object this$pageSize = this.getPageSize();
                Object other$pageSize = other.getPageSize();
                if (this$pageSize == null) {
                    if (other$pageSize != null) {
                        return false;
                    }
                } else if (!this$pageSize.equals(other$pageSize)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ProEffectInData;
    }

    public int hashCode() {

        int result = 1;
        Object $gssbProId = this.getGssbProId();
        result = result * 59 + ($gssbProId == null ? 43 : $gssbProId.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $proClass = this.getProClass();
        result = result * 59 + ($proClass == null ? 43 : $proClass.hashCode());
        Object $effectDay = this.getEffectDay();
        result = result * 59 + ($effectDay == null ? 43 : $effectDay.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        Object $pageNum = this.getPageNum();
        result = result * 59 + ($pageNum == null ? 43 : $pageNum.hashCode());
        Object $pageSize = this.getPageSize();
        result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());
        return result;
    }

    public String toString() {
        return "ProEffectInData(gssbProId=" + this.getGssbProId() + ", proCode=" + this.getProCode() + ", proClass=" + this.getProClass() + ", effectDay=" + this.getEffectDay() + ", clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ", pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ")";
    }
}
