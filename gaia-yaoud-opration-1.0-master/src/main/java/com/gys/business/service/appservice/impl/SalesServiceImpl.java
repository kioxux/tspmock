package com.gys.business.service.appservice.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.SalesReceiptsService;
import com.gys.business.service.appservice.SalesService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.util.CommonUtil;
import com.gys.util.CopyUtil;
import com.gys.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static cn.hutool.core.date.DateUtil.between;
import static cn.hutool.core.date.DateUtil.parse;

/**
 * @author xiaoyuan on 2021/3/8
 */
@SuppressWarnings("all")
@Service("appSalesServiceImpl")
public class SalesServiceImpl implements SalesService {

    @Autowired
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    @Autowired
    private GaiaSdServerRelevancyMapper serverRelevancyMapper;

    @Autowired
    private GaiaSdServerDiffMapper serverDiffMapper;

    @Autowired
    private GaiaSdServerContraMapper serverContraMapper;

    @Autowired
    private GaiaSdServerTipsMapper serverTipsMapper;

    @Autowired
    private GaiaProductBasicMapper productBasicMapper;

    @Autowired
    private GaiaSdSrRecordMapper recordMapper;

    @Autowired
    private GaiaSdPromHeadMapper gaiaSdPromHeadMapper;

    @Autowired
    private GaiaSdPromUnitarySetMapper gaiaSdPromUnitarySetMapper;

    @Autowired
    private GaiaSdPromGiftCondsMapper gaiaSdPromGiftCondsMapper;

    @Autowired
    private GaiaSdPromSeriesCondsMapper gaiaSdPromSeriesCondsMapper;

    @Autowired
    private GaiaSdPromGiftResultMapper gaiaSdPromGiftResultMapper;

    @Autowired
    private GaiaSdSaleRecipelMapper gaiaSdSaleRecipelMapper;

    @Autowired
    private GaiaSdSaleHandHMapper saleHMapper;

    @Autowired
    private GaiaSdSaleHandDMapper saleDMapper;

    @Autowired
    private GaiaSdMemberCardDataMapper memberCardDataMapper;

    @Autowired
    private GaiaSdMemberBasicMapper memberBasicMapper;

    @Autowired
    private GaiaSdMemberClassMapper memberClassMapper;

    @Autowired
    private SalesReceiptsService salesReceiptsService;

    @Autowired
    private RedisManager redisManager;

    private static final String SALES_CODE = "sales_code";

    @Override
    @Transactional(rollbackFor = Exception.class)
    public QueryProductAppData queryProduct(GetQueryProductInData inData) {
        QueryProductAppData appData = new QueryProductAppData();
        //商品信息查询
        List<GetQueryProductOutData> outData = this.gaiaProductBusinessMapper.queryProductApp(inData);
        if (CollUtil.isEmpty(outData)) {
            return appData;
        }
        //药事推荐等信息
        //药事推荐商品会员促销查询
        GetServerOutData serverOutData = this.drugRecommendatio(inData, outData.get(0).getProCode());
        //获取所有的商品编码
        List<String> proLists = outData.stream().map(GetQueryProductOutData::getProCode).collect(Collectors.toList());

        //查询所有的 商品会员促销信息
        inData.setProList(proLists);
        this.currentMerchandiseMembers(inData, outData);
        //商品信息
        appData.setProductData(outData);

        //推荐会员促销1
        this.recommendation1(inData, serverOutData);
        //推荐会员促销2
        this.recommendation2(inData, serverOutData);


        //查询当前商品的所有 促销信息
        this.findProductPromotionByAppCpde(outData, inData, proLists);
        //药事推荐信息
        appData.setServerOutData(serverOutData);

        //药事推荐保存
        List<GaiaSdSrRecord> recordList = this.getGaiaSdSrRecords(inData, outData.get(0), serverOutData);
        this.recordMapper.insertBatchs(recordList);

        return appData;
    }

    /**
     * 药事推荐保存
     *
     * @param inData
     * @param outData
     * @param serverOutData
     * @return
     */
    public List<GaiaSdSrRecord> getGaiaSdSrRecords(GetQueryProductInData inData, GetQueryProductOutData outData, GetServerOutData serverOutData) {
        //销售单号
        String billNo = CommonUtil.generateBillNo(inData.getBrId());
        List<GaiaSdSrRecord> recordList = new ArrayList<>();

        GaiaSdSrRecord sdSrRecord = new GaiaSdSrRecord();
        sdSrRecord.setClientId(inData.getClientId());
        sdSrRecord.setGssrBillNo(billNo);
        sdSrRecord.setGssrBrId(inData.getBrId());
        int index = recordMapper.selectCount(sdSrRecord) + 1;
        sdSrRecord.setGssrProId(outData.getProCode());
        sdSrRecord.setGssrDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        sdSrRecord.setGssrTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
        sdSrRecord.setGssrReleProType("1");
        sdSrRecord.setGssrCompclass(outData.getProCompclass());
        sdSrRecord.setGssrSerial(String.valueOf(index++));
        sdSrRecord.setGssrStockQty(StringUtils.isEmpty(outData.getProGssQty()) ? null : outData.getProGssQty());
        sdSrRecord.setGssrFlag("A1");
        sdSrRecord.setGssrPtiority1(outData.getGssrPriority1());
        sdSrRecord.setGssrPtiority3(outData.getGssrPriority3());
        sdSrRecord.setGssrStatus("1");
        recordList.add(sdSrRecord);

        List<GetServerProduct1OutData> list1 = serverOutData.getProductList1();
        if (CollUtil.isNotEmpty(list1)) {
            for (GetServerProduct1OutData data1 : list1) {
                GaiaSdSrRecord record = new GaiaSdSrRecord();
                record.setClientId(inData.getClientId());
                record.setGssrBillNo(billNo);
                record.setGssrBrId(inData.getBrId());
                record.setGssrDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                record.setGssrTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
                record.setGssrProId(data1.getProCode1());
                record.setGssrCompclass(data1.getProCompclass1());
                record.setGssrReleProType("2");
                record.setGssrSerial(String.valueOf(index++));
                record.setGssrStockQty(StringUtils.isEmpty(data1.getGssbQty1()) ? null : data1.getGssbQty1());
                record.setGssrFlag("A1");
                record.setGssrPtiority1(data1.getPriority1());
                record.setGssrPtiority3(data1.getPriority3());
                record.setGssrStatus("1");
                if (!recordList.contains(record)) {
                    recordList.add(record);
                }
            }
        }
        List<GetServerProduct2OutData> list2 = serverOutData.getProductList2();
        if (CollUtil.isNotEmpty(list2)) {
            for (GetServerProduct2OutData data2 : list2) {
                GaiaSdSrRecord record = new GaiaSdSrRecord();
                record.setClientId(inData.getClientId());
                record.setGssrBillNo(billNo);
                record.setGssrBrId(inData.getBrId());
                record.setGssrDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                record.setGssrTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
                record.setGssrProId(data2.getProCode2());
                record.setGssrCompclass(data2.getProCompclass2());
                record.setGssrReleProType("2");
                record.setGssrSerial(String.valueOf(index++));
                record.setGssrStockQty(StringUtils.isEmpty(data2.getGssbQty2()) ? null : data2.getGssbQty2());
                record.setGssrFlag("A1");
                record.setGssrPtiority1(data2.getPriority1());
                record.setGssrPtiority3(data2.getPriority3());
                record.setGssrStatus("1");
                if (!recordList.contains(record)) {
                    recordList.add(record);
                }
            }
        }
        return recordList;
    }

    private void findProductPromotionByAppCpde(List<GetQueryProductOutData> outData, GetQueryProductInData inData, List<String> proLists) {
        PromotionMethodInData methodInData = new PromotionMethodInData();
        methodInData.setClient(inData.getClientId());
        methodInData.setBrId(inData.getBrId());
        methodInData.setIsMember(inData.getMemberId());
        methodInData.setProLists(proLists);

        List<PromotionalConditionData> appCodeList = gaiaSdPromHeadMapper.findProductPromotionByAppCode(methodInData);
        if (CollUtil.isNotEmpty(appCodeList)) {
            for (GetQueryProductOutData outDatum : outData) {
                List<PromotionalConditionData> list = new LinkedList();
                for (PromotionalConditionData conditionData : appCodeList) {
                    if ("assign1".equals(conditionData.getGsphPara3())) { //全场商品
                        list.add(conditionData);
                    } else if (outDatum.getProCode().equals(conditionData.getProCode())) {
                        list.add(conditionData);
                    }
                }
                outDatum.setConditionList(list);
            }

        }

    }

    public List<GetServerProductInData> getGetServerProductInData(GetQueryProductInData inData, GetServerInData serverInData2) {
        GaiaSdSrRecord sdSrRecord = new GaiaSdSrRecord();
        sdSrRecord.setClientId(inData.getClientId());
        sdSrRecord.setGssrBillNo(serverInData2.getBillNo());
        sdSrRecord.setGssrBrId(serverInData2.getStoreCode());
        int index = recordMapper.selectCount(sdSrRecord) + 1;
        Example example = new Example(GaiaSdSrRecord.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssrBrId", serverInData2.getStoreCode()).andEqualTo("gssrBillNo", serverInData2.getBillNo()).andEqualTo("gssrReleProType", serverInData2.getType()).andLike("gssrFlag", serverInData2.getCallMethod() + "%");
        List<GaiaSdSrRecord> srRecord = this.recordMapper.selectByExample(example);
        String flag = "A0";
        if (CollUtil.isNotEmpty(srRecord)) {
            int size = 0;
            for (GaiaSdSrRecord item : srRecord) {
                if (Integer.parseInt(item.getGssrFlag().substring(1)) > size) {
                    size = Integer.parseInt(item.getGssrFlag().substring(1));
                }
            }
            flag = serverInData2.getCallMethod() + size;
        }
        List<GetServerProductInData> productInDataList = new ArrayList<>();
        for (GetServerProductInData product : serverInData2.getProductList()) {
            GaiaSdSrRecord record = new GaiaSdSrRecord();
            record.setClientId(inData.getClientId());
            record.setGssrBillNo(serverInData2.getBillNo());
            record.setGssrBrId(serverInData2.getStoreCode());
            record.setGssrDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
            record.setGssrTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
            record.setGssrProId(product.getProCode());
            record.setGssrCompclass(product.getProCompclass());
            record.setGssrReleProType(serverInData2.getType());
            record.setGssrSerial(String.valueOf(index++));
            record.setGssrStockQty(StringUtils.isEmpty(product.getProStock()) ? null : product.getProStock());
            record.setGssrFlag("A" + (Integer.parseInt(flag.substring(1)) + 1));
            record.setGssrPtiority1(product.getPriority1());
            record.setGssrPtiority3(product.getPriority3());
            record.setGssrStatus("1");
            productInDataList.add(product);
        }
        return productInDataList;
    }

    @Override
    public List<GetPdAndExpOutData> batchQuery(GetQueryProductInData inData) {
        return this.gaiaProductBusinessMapper.queryStockAndExpGroupByBatchNo(inData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public AppAmountCalculationDto amountCalculation(AppAmountVo inData, GetLoginOutData userInfo) {
        AppAmountCalculationDto dto = new AppAmountCalculationDto();
        if (inData.isFlag()) {
            //锁住 当前订单保证一人
            String lockCode = userInfo.getClient() + "/" + userInfo.getDepId() + "/" + SALES_CODE;
            if (this.redisManager.tryLock(lockCode, 400)) {
                try {
                    //Todo:库存验证

                    //下单保存
                    String registerBillNo = CommonUtil.registerBillNo(inData.getBrId());//手机移动销售订单号
                    String billNo = CommonUtil.generateBillNo(inData.getBrId());//销售单号
                    String saleCode = this.saleHMapper.getMaxByBrId(inData.getClient(), inData.getBrId(), DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_PATTERN));
                    //销售主表
                    GaiaSdSaleHandH sdSaleH = this.getGaiaSdSaleH(inData, registerBillNo, billNo, saleCode);
                    //判断是否存在处方信息
                    if (Util.isAllFieldNull(inData.getInputData())) {
                        List<GaiaSdSaleRecipel> recipelList = this.getGaiaSdSaleRecipels(inData, billNo);
                        this.gaiaSdSaleRecipelMapper.insertLists(recipelList);
                    }
                    //销售明细
                    List<GaiaSdSaleHandD> saleDList = this.getGaiaSdSaleDS(inData, billNo);
                    if (CollUtil.isEmpty(saleDList)) {
                        throw new BusinessException("无销售商品信息,请仔细核查~!");
                    }
                    this.saleHMapper.insert(sdSaleH);
                    this.saleDMapper.insertList(saleDList);
                    //下单
                    dto.setAppBillNo(registerBillNo);
                    dto.setSalesCode(saleCode);
                } finally {
                    this.redisManager.unLock(lockCode);
                }
            } else {
                throw new BusinessException("服务异常~!");
            }
        } else {
            //金额计算
            BigDecimal originalPrice = BigDecimal.ZERO;
            BigDecimal realPrice = BigDecimal.ZERO;
            BigDecimal discountPrice = BigDecimal.ZERO;

//            List<String> otherStatus = new ArrayList<>();
//            otherStatus.add("GJ");
//            otherStatus.add("JF");
//            otherStatus.add("CL");
            //查询会员卡折扣
            BigDecimal memberDiscount = new BigDecimal("100");
            if (StrUtil.isNotEmpty(inData.getMemberId())) {
                Example example = new Example(GaiaSdMemberCardData.class);
                example.createCriteria().andEqualTo("gsmbcCardId", inData.getMemberId()).andEqualTo("client", inData.getClient());
                GaiaSdMemberCardData gaiaSdMemberCardData = memberCardDataMapper.selectOneByExample(example);

                example = new Example(GaiaSdMemberClass.class);
                example.createCriteria().andEqualTo("gsmcId", gaiaSdMemberCardData.getGsmbcClassId()).andEqualTo("clientId", inData.getClient());
                GaiaSdMemberClass gaiaSdMemberClass = memberClassMapper.selectOneByExample(example);
                gaiaSdMemberClass.getGsmcDiscountRate();
                memberDiscount = new BigDecimal(gaiaSdMemberClass.getGsmcDiscountRate());
            }

            for (AppAmountCalculationVo vo : inData.getVoList()) {
                if (StrUtil.isNotEmpty(inData.getMemberId()) && StrUtil.isNotEmpty(vo.getMemberPrice())) {
                    BigDecimal totalPrice = new BigDecimal(vo.getMemberPrice()).multiply(new BigDecimal(vo.getProQty()));
                    realPrice = realPrice.add(totalPrice);
                    vo.setTotalAmt(totalPrice.toString());
                    vo.setStatus("会员价");
                    vo.setRealPrice(vo.getMemberPrice().toString());
                } else if (memberDiscount.compareTo(new BigDecimal("100")) < 0) {
                    BigDecimal totalPrice = new BigDecimal(vo.getPrcAmount()).multiply(new BigDecimal(vo.getProQty())).multiply(memberDiscount).divide(new BigDecimal("100"));
                    realPrice = realPrice.add(totalPrice);
                    vo.setTotalAmt(totalPrice.toString());
                    vo.setMemberPrice(new BigDecimal(vo.getPrcAmount()).multiply(memberDiscount).divide(new BigDecimal("100")).toString());
                    vo.setStatus("会员卡折扣");
                    vo.setRealPrice(new BigDecimal(vo.getPrcAmount()).multiply(memberDiscount).divide(new BigDecimal("100")).toString());
                } else {
                    BigDecimal totalPrice = new BigDecimal(vo.getPrcAmount()).multiply(new BigDecimal(vo.getProQty()));
                    realPrice = realPrice.add(totalPrice);
                    vo.setTotalAmt(totalPrice.toString());
                    vo.setRealPrice(vo.getPrcAmount());
                }
                BigDecimal totalPrice = new BigDecimal(vo.getPrcAmount()).multiply(new BigDecimal(vo.getProQty()));
                originalPrice = originalPrice.add(totalPrice);
            }

            dto.setActualAmount(CommonUtil.bigDecimalTo2fStr(realPrice));
            dto.setDiscountedPrice(CommonUtil.bigDecimalTo2fStr(originalPrice.subtract(realPrice)));
            dto.setProList(inData.getVoList());
        }
        return dto;
    }

    public GaiaSdSaleHandH getGaiaSdSaleH(AppAmountVo inData, String registerBillNo, String billNo, String saleCode) {
        GaiaSdSaleHandH sdSaleH = new GaiaSdSaleHandH();
        sdSaleH.setClientId(inData.getClient());//加盟商
        sdSaleH.setGsshCallAllow("1");//是否允许调出
        sdSaleH.setGsshBrId(inData.getBrId());//店号
        sdSaleH.setGsshBillNo(billNo);//订单号
        sdSaleH.setGsshDate(DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_PATTERN));//销售日期
        sdSaleH.setGsshTime(DateUtil.format(DateUtil.date(), DatePattern.PURE_TIME_PATTERN));//销售时间
        sdSaleH.setGsshEmp(inData.getUserId());//收银工号
        if (StrUtil.isNotBlank(inData.getMemberId())) {
            sdSaleH.setGsshHykNo(inData.getMemberId());//会员卡号
        }

        sdSaleH.setGsshZkAmt(StrUtil.isBlank(inData.getDiscountedPrice()) ? BigDecimal.ZERO : new BigDecimal(inData.getDiscountedPrice()));//折让金额
        sdSaleH.setGsshYsAmt(new BigDecimal(inData.getActualAmount()));//应收金额
        sdSaleH.setGsshNormalAmt(new BigDecimal(inData.getActualAmount()));//零售金额
        //sdSaleH.setGsshIntegralExchange(StringUtils.isEmpty(currentSaleReturnOutData.getGsshIntegralExchange()) ? null : currentSaleReturnOutData.getGsshIntegralExchange());//兑换积分
        //sdSaleH.setGsshIntegralExchangeAmt(StringUtils.isEmpty(currentSaleReturnOutData.getGsshIntegralExchangeAmt()) ? null : currentSaleReturnOutData.getGsshIntegralExchangeAmt());//加价兑换积分金额
        sdSaleH.setGsshHideFlag("2");//表示手机收银
        sdSaleH.setGsshRegisterVoucherId(registerBillNo);//手机销售订单
        sdSaleH.setGsshEmpGroupName(saleCode);//移动销售码
        return sdSaleH;
    }

    public List<GaiaSdSaleHandD> getGaiaSdSaleDS(AppAmountVo inData, String billNo) {
        List<GaiaSdSaleHandD> saleDList = new ArrayList<>();
        for (AppAmountCalculationVo datum : inData.getVoList()) {
            GaiaSdSaleHandD sdSaleD = new GaiaSdSaleHandD();
            sdSaleD.setClientId(inData.getClient());//加盟商
            sdSaleD.setGssdBillNo(billNo);//销售单号
            sdSaleD.setGssdBrId(inData.getBrId());//店号
            sdSaleD.setGssdDate(DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_PATTERN));//销售日期
            sdSaleD.setGssdSerial(String.valueOf(datum.getIndex()));//序号
            sdSaleD.setGssdProId(datum.getProCode());//商品id
            sdSaleD.setGssdBatchNo(datum.getGssbBatchNo());//批号信息
            sdSaleD.setGssdValidDate(datum.getGssbValidDate());//有效期
            //sdSaleD.setGssdStCode(item.getBillItemRate());//折扣
            sdSaleD.setGssdPrc1(new BigDecimal(datum.getPrcAmount()));//零售价
            sdSaleD.setGssdPrc2(new BigDecimal(datum.getRealPrice()));//实收价格
            sdSaleD.setGssdQty(datum.getProQty());//购买数量
            sdSaleD.setGssdOriQty(new BigDecimal(datum.getProQty()));//购买初始数量
            sdSaleD.setGssdDose("1");//中药贴数
            sdSaleD.setGssdAmt(new BigDecimal(datum.getTotalAmt()));//汇总总金额
            sdSaleD.setGssdZkAmt(new BigDecimal(datum.getPrcAmount()).multiply(new BigDecimal(datum.getProQty())).subtract(new BigDecimal(datum.getTotalAmt())));//商品折扣金额
            sdSaleD.setGssdProStatus("正常");//价格状态
            sdSaleD.setGssdSalerId(inData.getUserId());//营业员编号
            sdSaleD.setGssdMovTax(new BigDecimal(datum.getGsrdInputTaxValue().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP));//税率

            sdSaleD.setGssdSpecialmedIdcard(StrUtil.isBlank(datum.getGssdSpecialmedIdcard()) ? null : datum.getGssdSpecialmedIdcard());//特殊药品购买人身份证
            sdSaleD.setGssdSpecialmedName(StrUtil.isBlank(datum.getGssdSpecialmedName()) ? null : datum.getGssdSpecialmedName());//特殊药品购买人姓名
            sdSaleD.setGssdSpecialmedSex(StrUtil.isBlank(datum.getGssdSpecialmedSex()) ? null : datum.getGssdSpecialmedSex());//特殊药品购买人性别
            sdSaleD.setGssdSpecialmedBirthday(StrUtil.isBlank(DateUtil.format(DateUtil.parse(datum.getGssdSpecialmedBirthday()), DatePattern.PURE_DATE_PATTERN)) ? null : DateUtil.format(DateUtil.parse(datum.getGssdSpecialmedBirthday()), DatePattern.PURE_DATE_PATTERN));//特殊药品购买人出生日期
            sdSaleD.setGssdSpecialmedMobile(StrUtil.isBlank(datum.getGssdSpecialmedMobile()) ? null : datum.getGssdSpecialmedMobile());//特殊药品购买人手机
            sdSaleD.setGssdSpecialmedAddress(StrUtil.isBlank(datum.getGssdSpecialmedAddress()) ? null : datum.getGssdSpecialmedAddress());//特殊药品购买人地址
            sdSaleD.setGssdSpecialmedEcode(StrUtil.isBlank(datum.getGssdSpecialmedEcode()) ? null : datum.getGssdSpecialmedEcode());//特殊药品电子监管码
            saleDList.add(sdSaleD);
        }
        return saleDList;
    }


    public List<GaiaSdSaleRecipel> getGaiaSdSaleRecipels(AppAmountVo inData, String billNo) {
        inData.getInputData().setClientId(inData.getClient());
        inData.getInputData().setGssrBrId(inData.getBrId());
        List<GaiaSdSaleRecipel> recipelList = new ArrayList<>();
        for (SelectRecipelDrugsOutData drugsOutData : inData.getInputData().getRecipelDrugsList()) {
            if (StrUtil.isBlank(inData.getInputData().getName())) {
                throw new BusinessException("姓名不可为空!");
            }
            if (StrUtil.isBlank(inData.getInputData().getSex())) {
                throw new BusinessException("性别不可为空!");
            }
            if (StrUtil.isBlank(inData.getInputData().getSex())) {
                throw new BusinessException("年龄不可为空!");
            }
            if (StrUtil.isBlank(inData.getInputData().getPhone()) || !Util.checkPhone(inData.getInputData().getPhone())) {
                throw new BusinessException("请正确填写手机号!");
            }
            if (StrUtil.isNotBlank(inData.getInputData().getCardId()) && !IdcardUtil.isValidCard(inData.getInputData().getCardId())) {
                throw new BusinessException("请正确输入身份证格式!");
            }
            GaiaSdSaleRecipel gaiaSdSaleRecipel = new GaiaSdSaleRecipel();
            gaiaSdSaleRecipel.setClientId(inData.getInputData().getClientId());
            String nextVoucherId = this.gaiaSdSaleRecipelMapper.selectNextVoucherId();
            gaiaSdSaleRecipel.setGssrVoucherId(nextVoucherId);
            gaiaSdSaleRecipel.setGssrBrId(inData.getInputData().getGssrBrId());
            gaiaSdSaleRecipel.setGssrBrName(inData.getInputData().getGssrBrName());
            gaiaSdSaleRecipel.setGssrDate(DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_PATTERN));
            gaiaSdSaleRecipel.setGssrTime(DateUtil.format(DateUtil.date(), DatePattern.NORM_TIME_PATTERN));
            gaiaSdSaleRecipel.setGssrEmp(inData.getInputData().getGssrEmp());
            gaiaSdSaleRecipel.setGssrSaleBrId(inData.getInputData().getGssrBrId());
            gaiaSdSaleRecipel.setGssrSaleDate(DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_PATTERN));
            gaiaSdSaleRecipel.setGssrSaleBillNo(billNo);
            gaiaSdSaleRecipel.setGssrCustName((inData.getInputData().getName()));
            gaiaSdSaleRecipel.setGssrCustSex((inData.getInputData().getSex()));
            gaiaSdSaleRecipel.setGssrCustAge((inData.getInputData().getAge()));
            gaiaSdSaleRecipel.setGssrCustIdcard((inData.getInputData().getCardId()));
            gaiaSdSaleRecipel.setGssrCustMobile((inData.getInputData().getPhone()));
            gaiaSdSaleRecipel.setGssrCheckStatus("0");
            gaiaSdSaleRecipel.setGssrProId(drugsOutData.getProCode());
            gaiaSdSaleRecipel.setGssrBatchNo((drugsOutData.getGssdBatch()));
            gaiaSdSaleRecipel.setGssrQty((drugsOutData.getNum()));
            gaiaSdSaleRecipel.setGssrSymptom((inData.getInputData().getSymptom()));
            gaiaSdSaleRecipel.setGssrDiagnose((inData.getInputData().getDiacrisis()));
            gaiaSdSaleRecipel.setGssrRecipelId(inData.getInputData().getPrescriptionNumber());//处方单号
            gaiaSdSaleRecipel.setGssrRecipelHospital(inData.getInputData().getPrescriptionHospital());//处方医院
            gaiaSdSaleRecipel.setGssrRecipelDepartment(inData.getInputData().getDepartment());//处方科室
            gaiaSdSaleRecipel.setGssrRecipelDoctor(inData.getInputData().getDoctors());//处方医生
            gaiaSdSaleRecipel.setGssrType("1");
            recipelList.add(gaiaSdSaleRecipel);
        }
        return recipelList;
    }

    @Override
    public List<AppParameterDto> getAllListByApp(AppParameterVo inData) {
        return this.saleHMapper.getAllListByApp(inData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateAppSalesStatus(AppParameterVo inData) {
        GaiaSdSaleHandH gaiaSdSaleH = this.saleHMapper.selectByBillNo(inData.getGsshBillNo(), inData.getClient(), inData.getBrId());
        if (ObjectUtil.isEmpty(gaiaSdSaleH)) {
            throw new BusinessException("该订单不存在,请仔细核对!");
        }
        gaiaSdSaleH.setGsshHideFlag("3");//作废状态
        this.saleHMapper.updateByAppPrimaryKey(gaiaSdSaleH);
    }

    @Override
    public List<RebornData> getAllListByAppBillNo(AppParameterVo inData) {
        List<RebornData> billNo = this.saleHMapper.getAllListByAppBillNo(inData);
        billNo.forEach(rebornData -> rebornData.setIndex(rebornData.getGssdSerial()));
        return billNo;
    }

    @Override
    public List<AppParameterDetailsDto> getAppByDetails(AppParameterVo inData) {
        return this.saleHMapper.getAppByDetails(inData);
    }

    @Override
    public AppMobileOrderInfoDto getAllListByAppBillNoV2(AppParameterVo inData) {
        //订单详情
        GaiaSdSaleHandH handH = this.saleHMapper.selectByBillNo(inData.getGsshBillNo(), inData.getClient(), inData.getBrId());
        if (ObjectUtil.isEmpty(handH)) {
            throw new BusinessException("查不到此订单！");
        } else {
            AppMobileOrderInfoDto res = new AppMobileOrderInfoDto();
            res.setClient(handH.getClientId());
            res.setBrId(handH.getGsshBrId());
            res.setTotalPrice(handH.getGsshYsAmt().toString());
            if (StrUtil.isNotEmpty(handH.getGsshHykNo())) {
                Example example = new Example(GaiaSdMemberCardData.class);
                example.createCriteria().andEqualTo("client", handH.getClientId()).andEqualTo("gsmbcCardId", handH.getGsshHykNo());
                GaiaSdMemberCardData memberCardData = memberCardDataMapper.selectOneByExample(example);
                if (ObjectUtil.isNotEmpty(memberCardData)) {
                    example = new Example(GaiaSdMemberBasic.class);
                    example.createCriteria().andEqualTo("clientId", handH.getClientId()).andEqualTo("gsmbMemberId", memberCardData.getGsmbcMemberId());
                    GaiaSdMemberBasic memberBasic = memberBasicMapper.selectOneByExample(example);
                    res.setCardNum(handH.getGsshHykNo());
                    res.setMemberId(memberCardData.getGsmbcMemberId());
                    res.setName(memberBasic.getGsmbName());
                    res.setMobile(memberBasic.getGsmbMobile());
                    res.setSex(memberBasic.getGsmbSex());
                }
            }
            //订单明细
            List<RebornData> billDetails = this.saleHMapper.getAllListByAppBillNo(inData);
            billDetails.forEach(rebornData -> rebornData.setIndex(rebornData.getGssdSerial()));
            res.setDataList(billDetails);
            return res;
        }
    }

    @Override
    public GetServerOutData drugRecommendation(GetQueryProductInData inData) {
        //药事推荐
        GetServerOutData serverOutData = this.drugRecommendatio(inData, inData.getNameOrCode());
        //推荐会员促销1
        this.recommendation1(inData, serverOutData);
        //推荐会员促销2
        this.recommendation2(inData, serverOutData);
        //查询当前商品的所有 促销信息
//        this.findProductPromotionByAppCpde(inData, methodInData);
        return serverOutData;
    }

    @Override
    public AppAmountCalculationDto amountCalculation2(AppAmountVo inData, GetLoginOutData userInfo) {
        AppAmountCalculationDto dto = new AppAmountCalculationDto();

        //金额计算
        BigDecimal originalPrice = BigDecimal.ZERO;
        BigDecimal realPrice = BigDecimal.ZERO;
        BigDecimal discountPrice = BigDecimal.ZERO;


        //查询所有有效的单品促销活动
        List<GaiaSdPromUnitarySet> unitarySetList = gaiaSdPromUnitarySetMapper.getSingleProm(inData.getClient(), inData.getBrId(), inData.getMemberId());

        if (ObjectUtil.isNotEmpty(inData.getSelectedProm())) {
            if ("PROM_SINGLE".equals(inData.getSelectedProm().getPromType())) {
                inData.getVoList().stream()
                        .filter(row -> (ObjectUtil.isNotEmpty(row.getActivityProm()) && inData.getSelectedProm().getPromId().equals(row.getActivityProm().getPromId())) || ObjectUtil.isEmpty(row.getActivityProm()))
                        .filter(row -> row.getProCode().equals(inData.getSelectedProm().getSinglePromDetail().getProCode()))
                        .forEach(row -> {
                            row.setActivityProm(inData.getSelectedProm());
                        });
            }
        }

        //遍历商品列表促销列表
        Set<AppPromData> promList = new HashSet<>();
        inData.getVoList().stream().filter(item ->
                ObjectUtil.isNotEmpty(item.getActivityProm())
        ).forEach(item -> promList.add(item.getActivityProm()));

        //查询活动明细，筛选有效的促销活动
        if (CollUtil.isNotEmpty(promList)) {
            //查询单品促销
            promList.stream().filter(item -> "PROM_SINGLE".equals(item.getPromType())).forEach(item -> {
                if (ObjectUtil.isEmpty(unitarySetList)) {
                    //查不到单品促销活动，则删除促销价格，删除商品所关联的促销活动信息
                    inData.getVoList().stream()
                            .filter(row -> ObjectUtil.isNotEmpty(row.getActivityProm()))
                            .filter(row -> item.getPromId().equals(row.getActivityProm().getPromId())&&item.getSinglePromDetail().getProCode().equals(row.getProCode()))
                            .forEach(row -> {
                        row.setPromPrice(null);
                        row.setActivityProm(null);
                    });
                } else {
                    //当前活动促销详情
                    GaiaSdPromUnitarySet promUnitarySet = unitarySetList.stream().filter(unitarySet ->
                            item.getPromId().equals(unitarySet.getGspusVoucherId()) && item.getSinglePromDetail().getSerial().equals(unitarySet.getGspusSerial())
                    ).findFirst().orElse(null);
                    if (ObjectUtil.isEmpty(promUnitarySet)) {
                        //查不到对应单品促销活动，则删除促销价格，删除商品所关联的促销活动信息
                        inData.getVoList().stream()
                                .filter(row -> ObjectUtil.isNotEmpty(row.getActivityProm()))
                                .filter(row -> item.getPromId().equals(row.getActivityProm().getPromId())&&item.getSinglePromDetail().getProCode().equals(row.getProCode()))
                                .forEach(row -> {
                                    row.setPromPrice(null);
                                    row.setActivityProm(null);
                                });
                    } else {
                        //把商品列表中，参加此活动的相同商品数量做个统计
                        BigDecimal joinPromQty = inData.getVoList().stream().filter(row -> ObjectUtil.isNotEmpty(row.getActivityProm()))
                                .filter(row -> item.getPromId().equals(row.getActivityProm().getPromId())&&item.getSinglePromDetail().getProCode().equals(row.getProCode()))
                                .map(product -> new BigDecimal(product.getProQty())).reduce(BigDecimal.ZERO, BigDecimal::add);
                        if (joinPromQty.compareTo(item.getSinglePromDetail().getReachQty()) > -1) {
                            //计算促销
                            caculateSiangleProm(inData, item, promUnitarySet, joinPromQty);
                        } else {
                            //数量不足，自动降阶，向下寻找
                            if ("3".equals(item.getPart())) {
                                //判断2阶
                                if (joinPromQty.compareTo(new BigDecimal(promUnitarySet.getGspusQty2())) > -1) {
                                    item.setPart("2");
                                    item.getSinglePromDetail().setReachQty(new BigDecimal(promUnitarySet.getGspusQty2()));
                                    if (ObjectUtil.isNotEmpty(promUnitarySet.getGspusPrc2())) {
                                        item.getSinglePromDetail().setPreferenceType("price");
                                        item.getSinglePromDetail().setPreference(promUnitarySet.getGspusPrc2());
                                    } else if (NumberUtil.isNumber(promUnitarySet.getGspusRebate2())) {
                                        item.getSinglePromDetail().setPreferenceType("discount");
                                        item.getSinglePromDetail().setPreference(new BigDecimal(promUnitarySet.getGspusRebate2()));
                                    }
                                    item.getSinglePromDetail().setReachQty(new BigDecimal(promUnitarySet.getGspusQty2()));
                                    item.setPart("2");
                                    caculateSiangleProm(inData, item, promUnitarySet, joinPromQty);
                                } else if (joinPromQty.compareTo(new BigDecimal(promUnitarySet.getGspusQty1())) > -1) {//判断1阶
                                    if (ObjectUtil.isNotEmpty(promUnitarySet.getGspusPrc1())) {
                                        item.getSinglePromDetail().setPreferenceType("price");
                                        item.getSinglePromDetail().setPreference(promUnitarySet.getGspusPrc1());
                                    } else if (NumberUtil.isNumber(promUnitarySet.getGspusRebate1())) {
                                        item.getSinglePromDetail().setPreferenceType("discount");
                                        item.getSinglePromDetail().setPreference(new BigDecimal(promUnitarySet.getGspusRebate1()));
                                    }
                                    item.getSinglePromDetail().setReachQty(new BigDecimal(promUnitarySet.getGspusQty1()));
                                    item.setPart("1");
                                    caculateSiangleProm(inData, item, promUnitarySet, joinPromQty);
                                }
                            } else if ("2".equals(item.getPart())) {
                                //检查1阶
                                if (joinPromQty.compareTo(new BigDecimal(promUnitarySet.getGspusQty1())) > -1) {//判断1阶
                                    if (ObjectUtil.isNotEmpty(promUnitarySet.getGspusPrc1())) {
                                        item.getSinglePromDetail().setPreferenceType("price");
                                        item.getSinglePromDetail().setPreference(promUnitarySet.getGspusPrc1());
                                    } else if (NumberUtil.isNumber(promUnitarySet.getGspusRebate1())) {
                                        item.getSinglePromDetail().setPreferenceType("discount");
                                        item.getSinglePromDetail().setPreference(new BigDecimal(promUnitarySet.getGspusRebate1()));
                                    }
                                    item.getSinglePromDetail().setReachQty(new BigDecimal(promUnitarySet.getGspusQty1()));
                                    item.setPart("1");
                                    caculateSiangleProm(inData, item, promUnitarySet, joinPromQty);
                                }
                            } else if ("1".equals(item.getPart())) {
                                inData.getVoList().stream().filter(row -> ObjectUtil.isNotEmpty(row.getActivityProm()))
                                        .filter(row -> item.getPromId().equals(row.getActivityProm().getPromId())&&item.getSinglePromDetail().getProCode().equals(row.getProCode()))
                                        .forEach(row -> {
                                            row.setPromPrice(null);
                                            row.setActivityProm(null);
                                        });
                            }
                        }
                    }
                }
            });
        }

        //根据新增的商品 找到满足条件的促销活动
        if (CollUtil.isNotEmpty(inData.getChangeList())) {
            PromotionMethodInData promotionMethodInData = new PromotionMethodInData();
            promotionMethodInData.setClient(inData.getClient());
            promotionMethodInData.setBrId(inData.getBrId());
            promotionMethodInData.setIsMember(StringUtils.isEmpty(inData.getMemberId()) ? "0" : "1");
            promotionMethodInData.setCode(inData.getChangeList().get(0).getProCode());
            List<PromotionalConditionData> promotionalConditionData = gaiaSdPromHeadMapper.findProductPromotionByCode2(promotionMethodInData);
            if (CollUtil.isNotEmpty(promotionalConditionData)) {
                List<AppPromData> preProm = new ArrayList();
                for (PromotionalConditionData prom : promotionalConditionData) {
                    if ("PROM_SINGLE".equals(prom.getType())) {
                        //遍历汇总出此商品的总数量(单品条件)
                        BigDecimal singleQty = inData.getVoList().stream()
                                .filter(item ->item.getProCode().equals(inData.getChangeList().get(0).getProCode()))
                                .filter(item -> item.getActivityProm() == null
                                        || (item.getActivityProm() != null && item.getActivityProm().getPromId().equals(prom.getVoucherId())))
                                .map(item -> new BigDecimal(item.getProQty())).reduce(BigDecimal.ZERO, BigDecimal::add);

                        BigDecimal maxPart = BigDecimal.ZERO;
                        BigDecimal maxRepeatNum = BigDecimal.ZERO;
                        BigDecimal reachQty = BigDecimal.ZERO;

                        //计算出当前活动最大阶数和重复次数
                        if ("3".equals(prom.getPart())) {
                            if (singleQty.compareTo(new BigDecimal(prom.getQtyThreeStage())) > -1) {
                                reachQty = new BigDecimal(prom.getQtyThreeStage());
                                maxPart = new BigDecimal("3");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyThreeStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            } else if (singleQty.compareTo(new BigDecimal(prom.getQtyTwoStage())) > -1) {
                                reachQty = new BigDecimal(prom.getQtyTwoStage());
                                maxPart = new BigDecimal("2");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyTwoStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            } else if (singleQty.compareTo(new BigDecimal(prom.getQtyOneStage())) > -1) {
                                reachQty = new BigDecimal(prom.getQtyOneStage());
                                maxPart = new BigDecimal("1");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyOneStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            }
                        } else if ("2".equals(prom.getPart())) {
                            if (singleQty.compareTo(new BigDecimal(prom.getQtyTwoStage())) > -1) {
                                reachQty = new BigDecimal(prom.getQtyTwoStage());
                                maxPart = new BigDecimal("2");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyTwoStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            } else if (singleQty.compareTo(new BigDecimal(prom.getQtyOneStage())) > -1) {
                                reachQty = new BigDecimal(prom.getQtyOneStage());
                                maxPart = new BigDecimal("1");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyOneStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            }
                        } else if ("1".equals(prom.getPart())) {
                            if (singleQty.compareTo(new BigDecimal(prom.getQtyOneStage())) > -1) {
                                reachQty = new BigDecimal(prom.getQtyOneStage());
                                maxPart = new BigDecimal("1");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyOneStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            }
                        }
                        if (maxPart.compareTo(BigDecimal.ZERO) > 0) {
                            //找出当前促销是否已生效
                            List<AppAmountCalculationVo> promProlist = inData.getVoList().stream().filter(item -> item.getProCode().equals(inData.getChangeList().get(0).getProCode()))
                                    .filter(item -> item.getActivityProm() != null && item.getActivityProm().getPromId().equals(prom.getVoucherId()))
                                    .collect(Collectors.toList());

                            if (CollUtil.isNotEmpty(promProlist)) {
                                if("repeat3".equals(prom.getGsphPara1())&&ObjectUtil.isNotEmpty(promProlist.get(0).getActivityProm())&&new BigDecimal(promProlist.get(0).getActivityProm().getPart()).compareTo(maxPart)==0){
                                    continue;
                                }
                                if (maxPart.compareTo(new BigDecimal(promProlist.get(0).getActivityProm().getPart())) > 0) {
                                    AppPromData data = new AppPromData();
                                    data.setPromId(prom.getVoucherId());
                                    data.setPromType(prom.getType());
                                    data.setPromName(prom.getName());
                                    data.setTheme(prom.getTheme());
                                    data.setRemarks(prom.getRemarks());
                                    data.setPart(maxPart.toString());
                                    data.setPara1(prom.getGsphPara1());
                                    data.setPara2(prom.getGsphPara2());
                                    data.setPara3(prom.getGsphPara3());
                                    data.setPara4(prom.getGsphPara4());
                                    SinglePromDetail singlePromDetail = new SinglePromDetail();
                                    singlePromDetail.setProCode(inData.getChangeList().get(0).getProCode());
                                    singlePromDetail.setRepeatNum(maxRepeatNum);
                                    singlePromDetail.setProName(inData.getChangeList().get(0).getProCommonName());
                                    singlePromDetail.setProCommonName(inData.getChangeList().get(0).getProCommonName());
                                    singlePromDetail.setSerial(prom.getSerial());
                                    singlePromDetail.setReachQty(reachQty);
                                    singlePromDetail.setNormalPrice(new BigDecimal(inData.getChangeList().get(0).getPrcAmount()));
                                    if (maxPart.compareTo(new BigDecimal("3")) == 0) {
                                        if (ObjectUtil.isNotEmpty(prom.getPriceThree())) {
                                            singlePromDetail.setPreferenceType("price");
                                            singlePromDetail.setPreference(new BigDecimal(prom.getPriceThree()));
                                        } else if (NumberUtil.isNumber(prom.getRebateThree())) {
                                            singlePromDetail.setPreferenceType("discount");
                                            singlePromDetail.setPreference(new BigDecimal(prom.getRebateThree()));
                                        }
                                    } else if (maxPart.compareTo(new BigDecimal("2")) == 0) {
                                        if (ObjectUtil.isNotEmpty(prom.getPriceTwo())) {
                                            singlePromDetail.setPreferenceType("price");
                                            singlePromDetail.setPreference(new BigDecimal(prom.getPriceTwo()));
                                        } else if (NumberUtil.isNumber(prom.getRebateTwo())) {
                                            singlePromDetail.setPreferenceType("discount");
                                            singlePromDetail.setPreference(new BigDecimal(prom.getRebateTwo()));
                                        }
                                    } else if (maxPart.compareTo(new BigDecimal("1")) == 0) {
                                        if (ObjectUtil.isNotEmpty(prom.getPriceOne())) {
                                            singlePromDetail.setPreferenceType("price");
                                            singlePromDetail.setPreference(new BigDecimal(prom.getPriceOne()));
                                        } else if (NumberUtil.isNumber(prom.getRebateOne())) {
                                            singlePromDetail.setPreferenceType("discount");
                                            singlePromDetail.setPreference(new BigDecimal(prom.getRebateOne()));
                                        }
                                    }
                                    data.setSinglePromDetail(singlePromDetail);
                                    preProm.add(data);
                                } else if (maxPart.compareTo(new BigDecimal(promProlist.get(0).getActivityProm().getPart())) == 0) {
                                    if (maxRepeatNum.compareTo(promProlist.get(0).getActivityProm().getSinglePromDetail().getRepeatNum()) > 0) {
                                        AppPromData data = new AppPromData();
                                        data.setPromId(prom.getVoucherId());
                                        data.setPromType(prom.getType());
                                        data.setPromName(prom.getName());
                                        data.setTheme(prom.getTheme());
                                        data.setRemarks(prom.getRemarks());
                                        data.setPart(maxPart.toString());
                                        data.setPara1(prom.getGsphPara1());
                                        data.setPara2(prom.getGsphPara2());
                                        data.setPara3(prom.getGsphPara3());
                                        data.setPara4(prom.getGsphPara4());
                                        SinglePromDetail singlePromDetail = new SinglePromDetail();
                                        singlePromDetail.setProCode(inData.getChangeList().get(0).getProCode());
                                        singlePromDetail.setRepeatNum(maxRepeatNum);
                                        singlePromDetail.setProName(inData.getChangeList().get(0).getProCommonName());
                                        singlePromDetail.setProCommonName(inData.getChangeList().get(0).getProCommonName());
                                        singlePromDetail.setSerial(prom.getSerial());
                                        singlePromDetail.setReachQty(reachQty);
                                        singlePromDetail.setNormalPrice(new BigDecimal(inData.getChangeList().get(0).getPrcAmount()));
                                        if (maxPart.compareTo(new BigDecimal("3")) == 0) {
                                            if (ObjectUtil.isNotEmpty(prom.getPriceThree())) {
                                                singlePromDetail.setPreferenceType("price");
                                                singlePromDetail.setPreference(new BigDecimal(prom.getPriceThree()));
                                            } else if (NumberUtil.isNumber(prom.getRebateThree())) {
                                                singlePromDetail.setPreferenceType("discount");
                                                singlePromDetail.setPreference(new BigDecimal(prom.getRebateThree()));
                                            }
                                        } else if (maxPart.compareTo(new BigDecimal("2")) == 0) {
                                            if (ObjectUtil.isNotEmpty(prom.getPriceTwo())) {
                                                singlePromDetail.setPreferenceType("price");
                                                singlePromDetail.setPreference(new BigDecimal(prom.getPriceTwo()));
                                            } else if (NumberUtil.isNumber(prom.getRebateTwo())) {
                                                singlePromDetail.setPreferenceType("discount");
                                                singlePromDetail.setPreference(new BigDecimal(prom.getRebateTwo()));
                                            }
                                        } else if (maxPart.compareTo(new BigDecimal("1")) == 0) {
                                            if (ObjectUtil.isNotEmpty(prom.getPriceOne())) {
                                                singlePromDetail.setPreferenceType("price");
                                                singlePromDetail.setPreference(new BigDecimal(prom.getPriceOne()));
                                            } else if (NumberUtil.isNumber(prom.getRebateOne())) {
                                                singlePromDetail.setPreferenceType("discount");
                                                singlePromDetail.setPreference(new BigDecimal(prom.getRebateOne()));
                                            }
                                        }
                                        data.setSinglePromDetail(singlePromDetail);
                                        preProm.add(data);
                                    }
                                }
                            } else {
                                AppPromData data = new AppPromData();
                                data.setPromId(prom.getVoucherId());
                                data.setPromType(prom.getType());
                                data.setPromName(prom.getName());
                                data.setTheme(prom.getTheme());
                                data.setRemarks(prom.getRemarks());
                                data.setPart(maxPart.toString());
                                data.setPara1(prom.getGsphPara1());
                                data.setPara2(prom.getGsphPara2());
                                data.setPara3(prom.getGsphPara3());
                                data.setPara4(prom.getGsphPara4());
                                SinglePromDetail singlePromDetail = new SinglePromDetail();
                                singlePromDetail.setProCode(inData.getChangeList().get(0).getProCode());
                                singlePromDetail.setRepeatNum(maxRepeatNum);
                                singlePromDetail.setProName(inData.getChangeList().get(0).getProCommonName());
                                singlePromDetail.setProCommonName(inData.getChangeList().get(0).getProCommonName());
                                singlePromDetail.setSerial(prom.getSerial());
                                singlePromDetail.setReachQty(reachQty);
                                singlePromDetail.setNormalPrice(new BigDecimal(inData.getChangeList().get(0).getPrcAmount()));
                                if (maxPart.compareTo(new BigDecimal("3")) == 0) {
                                    if (ObjectUtil.isNotEmpty(prom.getPriceThree())) {
                                        singlePromDetail.setPreferenceType("price");
                                        singlePromDetail.setPreference(new BigDecimal(prom.getPriceThree()));
                                    } else if (NumberUtil.isNumber(prom.getRebateThree())) {
                                        singlePromDetail.setPreferenceType("discount");
                                        singlePromDetail.setPreference(new BigDecimal(prom.getRebateThree()));
                                    }
                                } else if (maxPart.compareTo(new BigDecimal("2")) == 0) {
                                    if (ObjectUtil.isNotEmpty(prom.getPriceTwo())) {
                                        singlePromDetail.setPreferenceType("price");
                                        singlePromDetail.setPreference(new BigDecimal(prom.getPriceTwo()));
                                    } else if (NumberUtil.isNumber(prom.getRebateTwo())) {
                                        singlePromDetail.setPreferenceType("discount");
                                        singlePromDetail.setPreference(new BigDecimal(prom.getRebateTwo()));
                                    }
                                } else if (maxPart.compareTo(new BigDecimal("1")) == 0) {
                                    if (ObjectUtil.isNotEmpty(prom.getPriceOne())) {
                                        singlePromDetail.setPreferenceType("price");
                                        singlePromDetail.setPreference(new BigDecimal(prom.getPriceOne()));
                                    } else if (NumberUtil.isNumber(prom.getRebateOne())) {
                                        singlePromDetail.setPreferenceType("discount");
                                        singlePromDetail.setPreference(new BigDecimal(prom.getRebateOne()));
                                    }
                                }
                                data.setSinglePromDetail(singlePromDetail);
                                preProm.add(data);
                            }
                        }
                    }
                }
                dto.setPromList(new ArrayList<>());
                dto.getPromList().addAll(preProm);
            }
        }

        //根据所有商品 找出最近的促销条件
        Set<String> proSet = new HashSet<>();
        inData.getVoList().forEach(item -> proSet.add(item.getProCode()));
        proSet.forEach(pro -> {
            PromotionMethodInData methodInData = new PromotionMethodInData();
            methodInData.setClient(inData.getClient());
            methodInData.setBrId(inData.getBrId());
            methodInData.setIsMember(StringUtils.isEmpty(inData.getMemberId()) ? "0" : "1");
            methodInData.setCode(pro);
            List<PromotionalConditionData> productPromotionByCode2 = gaiaSdPromHeadMapper.findProductPromotionByCode2(methodInData);
            if (CollUtil.isNotEmpty(productPromotionByCode2)) {
                for (PromotionalConditionData prom : productPromotionByCode2) {
                    if ("PROM_SINGLE".equals(prom.getType())) {
                        //遍历汇总出此商品的总数量(单品条件)
                        BigDecimal singleQty = inData.getVoList().stream()
                                .filter(item -> (item.getActivityProm() == null&&item.getProCode().equals(pro))
                                        || (item.getActivityProm() != null && item.getActivityProm().getPromId().equals(prom.getVoucherId())))
                                .map(item -> new BigDecimal(item.getProQty())).reduce(BigDecimal.ZERO, BigDecimal::add);

                        BigDecimal maxPart = BigDecimal.ZERO;
                        BigDecimal maxRepeatNum = BigDecimal.ZERO;
                        BigDecimal reachQty = BigDecimal.ZERO;

                        //计算出当前活动最大阶数和重复次数
                        if ("3".equals(prom.getPart())) {
                            if (singleQty.compareTo(new BigDecimal(prom.getQtyOneStage())) < 1) {
                                reachQty = new BigDecimal(prom.getQtyOneStage());
                                maxPart = new BigDecimal("1");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyOneStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            } else if (singleQty.compareTo(new BigDecimal(prom.getQtyTwoStage())) < 1) {
                                reachQty = new BigDecimal(prom.getQtyTwoStage());
                                maxPart = new BigDecimal("2");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyTwoStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            } else if (singleQty.compareTo(new BigDecimal(prom.getQtyThreeStage())) < 1) {
                                reachQty = new BigDecimal(prom.getQtyThreeStage());
                                maxPart = new BigDecimal("3");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyThreeStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            }
                        } else if ("2".equals(prom.getPart())) {
                            if (singleQty.compareTo(new BigDecimal(prom.getQtyOneStage())) < 1) {
                                reachQty = new BigDecimal(prom.getQtyOneStage());
                                maxPart = new BigDecimal("1");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyOneStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            } else if (singleQty.compareTo(new BigDecimal(prom.getQtyTwoStage())) < 1) {
                                reachQty = new BigDecimal(prom.getQtyTwoStage());
                                maxPart = new BigDecimal("2");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyTwoStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            }
                        } else if ("1".equals(prom.getPart())) {
                            if (singleQty.compareTo(new BigDecimal(prom.getQtyOneStage())) < 1) {
                                reachQty = new BigDecimal(prom.getQtyOneStage());
                                maxPart = new BigDecimal("1");
                                BigDecimal[] divideAndRemainder = singleQty.divideAndRemainder(new BigDecimal(prom.getQtyOneStage()));
                                maxRepeatNum = divideAndRemainder[0];
                            }
                        }
                        if (maxPart.compareTo(BigDecimal.ZERO) > 0) {
                            AppPromData data = new AppPromData();
                            data.setPromId(prom.getVoucherId());
                            data.setPromType(prom.getType());
                            data.setPromName(prom.getName());
                            data.setTheme(prom.getTheme());
                            data.setRemarks(prom.getRemarks());
                            data.setPart(maxPart.toString());
                            data.setPara1(prom.getGsphPara1());
                            data.setPara2(prom.getGsphPara2());
                            data.setPara3(prom.getGsphPara3());
                            data.setPara4(prom.getGsphPara4());
                            SinglePromDetail singlePromDetail = new SinglePromDetail();
                            AppAmountCalculationVo aa = inData.getVoList().stream().filter(p -> p.getProCode().equals(pro)).findFirst().orElse(null);
                            singlePromDetail.setProCode(aa.getProCode());
                            singlePromDetail.setRepeatNum(maxRepeatNum);
                            singlePromDetail.setProName(aa.getProCommonName());
                            singlePromDetail.setProCommonName(aa.getProCommonName());
                            singlePromDetail.setSerial(prom.getSerial());
                            singlePromDetail.setReachQty(reachQty);
                            singlePromDetail.setNormalPrice(new BigDecimal(aa.getPrcAmount()));
                            if (maxPart.compareTo(new BigDecimal("3")) == 0) {
                                if (ObjectUtil.isNotEmpty(prom.getPriceThree())) {
                                    singlePromDetail.setPreferenceType("price");
                                    singlePromDetail.setPreference(new BigDecimal(prom.getPriceThree()));
                                } else if (NumberUtil.isNumber(prom.getRebateThree())) {
                                    singlePromDetail.setPreferenceType("discount");
                                    singlePromDetail.setPreference(new BigDecimal(prom.getRebateThree()));
                                }
                            } else if (maxPart.compareTo(new BigDecimal("2")) == 0) {
                                if (ObjectUtil.isNotEmpty(prom.getPriceTwo())) {
                                    singlePromDetail.setPreferenceType("price");
                                    singlePromDetail.setPreference(new BigDecimal(prom.getPriceTwo()));
                                } else if (NumberUtil.isNumber(prom.getRebateTwo())) {
                                    singlePromDetail.setPreferenceType("discount");
                                    singlePromDetail.setPreference(new BigDecimal(prom.getRebateTwo()));
                                }
                            } else if (maxPart.compareTo(new BigDecimal("1")) == 0) {
                                if (ObjectUtil.isNotEmpty(prom.getPriceOne())) {
                                    singlePromDetail.setPreferenceType("price");
                                    singlePromDetail.setPreference(new BigDecimal(prom.getPriceOne()));
                                } else if (NumberUtil.isNumber(prom.getRebateOne())) {
                                    singlePromDetail.setPreferenceType("discount");
                                    singlePromDetail.setPreference(new BigDecimal(prom.getRebateOne()));
                                }
                            }
                            data.setSinglePromDetail(singlePromDetail);
                            inData.getVoList().forEach(item -> {
                                if (data.getSinglePromDetail().getProCode().equals(item.getProCode())) {
                                    if (item.getPrePromList() == null) {
                                        List<AppPromData> promDataList = new ArrayList<>();
                                        item.setPrePromList(promDataList);
                                    }
                                    if (CollUtil.isNotEmpty(item.getPrePromList())) {
                                        for (AppPromData promData : item.getPrePromList()) {
                                            if (promData.getPromId().equals(data.getPromId()) && promData.getSinglePromDetail().getSerial().equals(data.getSinglePromDetail().getSerial())) {
                                                item.getPrePromList().remove(promData);
                                                break;
                                            }
                                        }
                                    }
                                    item.getPrePromList().add(data);
                                }

                            });
                        }
                    }
                }
            }
        });

        //查询会员卡折扣
        BigDecimal memberDiscount = new BigDecimal("100");
        if (StrUtil.isNotEmpty(inData.getMemberId())) {
            Example example = new Example(GaiaSdMemberCardData.class);
            example.createCriteria().andEqualTo("gsmbcCardId", inData.getMemberId()).andEqualTo("client", inData.getClient());
            GaiaSdMemberCardData gaiaSdMemberCardData = memberCardDataMapper.selectOneByExample(example);

            example = new Example(GaiaSdMemberClass.class);
            example.createCriteria().andEqualTo("gsmcId", gaiaSdMemberCardData.getGsmbcClassId()).andEqualTo("clientId", inData.getClient());
            GaiaSdMemberClass gaiaSdMemberClass = memberClassMapper.selectOneByExample(example);
            gaiaSdMemberClass.getGsmcDiscountRate();
            memberDiscount = new BigDecimal(gaiaSdMemberClass.getGsmcDiscountRate());
        }

        for (AppAmountCalculationVo vo : inData.getVoList()) {
            if (ObjectUtil.isNotEmpty(vo.getActivityProm())) {
//                BigDecimal totalPrice = new BigDecimal(vo.getTotalAmt());
                realPrice = realPrice.add(new BigDecimal(vo.getTotalAmt()));
//                vo.setTotalAmt(totalPrice.toString());
//                vo.setStatus("促销价");
//                vo.setRealPrice(vo.getMemberPrice().toString());
            } else if (StrUtil.isNotEmpty(inData.getMemberId()) && StrUtil.isNotEmpty(vo.getMemberPrice())) {
                BigDecimal totalPrice = new BigDecimal(vo.getMemberPrice()).multiply(new BigDecimal(vo.getProQty()));
                realPrice = realPrice.add(totalPrice);
                vo.setTotalAmt(totalPrice.toString());
                vo.setStatus("会员价");
                vo.setRealPrice(vo.getMemberPrice().toString());
            } else if (memberDiscount.compareTo(new BigDecimal("100")) < 0) {
                BigDecimal totalPrice = new BigDecimal(vo.getPrcAmount()).multiply(new BigDecimal(vo.getProQty())).multiply(memberDiscount).divide(new BigDecimal("100"));
                realPrice = realPrice.add(totalPrice);
                vo.setTotalAmt(totalPrice.toString());
                vo.setMemberPrice(new BigDecimal(vo.getPrcAmount()).multiply(memberDiscount).divide(new BigDecimal("100")).toString());
                vo.setStatus("会员卡折扣");
                vo.setRealPrice(new BigDecimal(vo.getPrcAmount()).multiply(memberDiscount).divide(new BigDecimal("100")).toString());
            } else {
                BigDecimal totalPrice = new BigDecimal(vo.getPrcAmount()).multiply(new BigDecimal(vo.getProQty()));
                realPrice = realPrice.add(totalPrice);
                vo.setTotalAmt(totalPrice.toString());
                vo.setRealPrice(vo.getPrcAmount());
            }
            BigDecimal totalPrice = new BigDecimal(vo.getPrcAmount()).multiply(new BigDecimal(vo.getProQty()));
            originalPrice = originalPrice.add(totalPrice);
        }

        dto.setActualAmount(CommonUtil.bigDecimalTo2fStr(realPrice));
        dto.setDiscountedPrice(CommonUtil.bigDecimalTo2fStr(originalPrice.subtract(realPrice)));
        dto.setProList(inData.getVoList());
        System.out.println("RES:" + JSON.toJSONString(dto));
        return dto;
    }

    /**
     * 当前商品的所有会员价
     *
     * @param inData
     * @param outData
     */
    public void currentMerchandiseMembers(GetQueryProductInData inData, List<GetQueryProductOutData> outData) {
        List<GetPromByProCodeOutData> codeMemberAll = this.gaiaSdPromHeadMapper.promByProCodeMemberAll(inData);
        if (ObjectUtil.isNotEmpty(codeMemberAll)) {
            for (GetQueryProductOutData outDatum : outData) {
                for (GetPromByProCodeOutData codeOutData : codeMemberAll) {
                    GetPromByProCodeOutData copy = CopyUtil.copy(codeOutData, GetPromByProCodeOutData.class);
                    if ("PROM_HYRJ".equals(copy.getPromotionTypeId()) && outDatum.getProCode().equals(copy.getProCode())) { //会员日价
                        if (StrUtil.isNotEmpty(copy.getDiscMem())) {
                            copy.setPriceMem(new BigDecimal(copy.getDiscMem()).multiply(new BigDecimal(outDatum.getPrcAmount())).divide(new BigDecimal("100")).stripTrailingZeros().toPlainString());
                        } else {
                            copy.setPriceMem(new BigDecimal(copy.getPriceMem()).stripTrailingZeros().toPlainString());
                        }
                        outDatum.setProm(copy);    //当前商品的 会员信息
                        break;
                    } else if ("PROM_HYRZ".equals(copy.getPromotionTypeId())) {//会员日折扣
                        BigDecimal divide = new BigDecimal(copy.getDiscMem()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP);
                        BigDecimal multiply = new BigDecimal(outDatum.getPrcAmount()).multiply(divide).setScale(2, BigDecimal.ROUND_HALF_UP);
                        copy.setPriceMem(multiply.stripTrailingZeros().toPlainString());//折扣后价格
                        outDatum.setProm(copy);    //当前商品的 会员信息
                        break;
                    } else if ("PROM_HYJ".equals(copy.getPromotionTypeId()) && outDatum.getProCode().equals(copy.getProCode())) {//会员价
                        if (StrUtil.isNotEmpty(copy.getDiscMem())) {
                            copy.setPriceMem(new BigDecimal(copy.getDiscMem()).multiply(new BigDecimal(outDatum.getPrcAmount())).divide(new BigDecimal("100")).stripTrailingZeros().toPlainString());
                        } else {
                            copy.setPriceMem(new BigDecimal(copy.getPriceMem()).stripTrailingZeros().toPlainString());
                        }
                        outDatum.setProm(copy);    //当前商品的 会员信息
                        break;
                    }
                }
            }
        }

    }

    /**
     * 药品推荐 判断是否会员,促销1
     *
     * @param inData
     * @param serverOutData
     */
    public void recommendation1(GetQueryProductInData inData, GetServerOutData serverOutData) {
        if (CollUtil.isNotEmpty(serverOutData.getProductList1())) {
            //获取所有的商品编码
            List<String> proLists1 = serverOutData.getProductList1().stream().map(GetServerProduct1OutData::getProCode1).collect(Collectors.toList());
            GetQueryProductInData data = new GetQueryProductInData();
            data.setProList(proLists1);
            data.setClientId(inData.getClientId());
            data.setBrId(inData.getBrId());
            //查询所有的 商品会员信息
            List<GetPromByProCodeOutData> codeMemberAll1 = this.gaiaSdPromHeadMapper.promByProCodeMemberAll(data);

            //促销商品
            PromotionMethodInData methodInData = new PromotionMethodInData();
            methodInData.setClient(inData.getClientId());
            methodInData.setBrId(inData.getBrId());
            methodInData.setProLists(proLists1);
            List<PromotionalConditionData> appCodeList = gaiaSdPromHeadMapper.findProductPromotionByAppCode(methodInData);

            if (CollUtil.isNotEmpty(codeMemberAll1)) {
                for (GetServerProduct1OutData product1OutData : serverOutData.getProductList1()) {
                    for (GetPromByProCodeOutData codeOutData : codeMemberAll1) {
                        GetPromByProCodeOutData copy = CopyUtil.copy(codeOutData, GetPromByProCodeOutData.class);
                        if ("PROM_HYRJ".equals(copy.getPromotionTypeId()) && product1OutData.getProCode1().equals(copy.getProCode())) { //会员日价
                            product1OutData.setProm(copy);    //当前商品的 会员信息
                            break;
                        } else if ("PROM_HYRZ".equals(copy.getPromotionTypeId())) {//会员日折扣
                            BigDecimal divide = new BigDecimal(copy.getDiscMem()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP);
                            BigDecimal multiply = new BigDecimal(product1OutData.getProAmount1()).multiply(divide).setScale(2, BigDecimal.ROUND_HALF_UP);
                            copy.setPriceMem(multiply.toString());//折扣后价格
                            product1OutData.setProm(copy);    //当前商品的 会员信息
                            break;
                        } else if ("PROM_HYJ".equals(copy.getPromotionTypeId()) && product1OutData.getProCode1().equals(copy.getProCode())) {//会员价
                            product1OutData.setProm(copy);    //当前商品的 会员信息
                            break;
                        }
                    }
                }

                if (CollUtil.isNotEmpty(appCodeList)) {
                    for (GetServerProduct1OutData product1OutData : serverOutData.getProductList1()) {
                        List<PromotionalConditionData> list = new LinkedList();
                        for (PromotionalConditionData conditionData : appCodeList) {
                            if ("assign1".equals(conditionData.getGsphPara3())) { //全场商品
                                list.add(conditionData);
                            } else if (product1OutData.getProCode1().equals(conditionData.getProCode())) {
                                list.add(conditionData);
                            }
                        }
                        product1OutData.setConditionList1(list);
                    }
                }

            }


        }


    }

    /**
     * 药品推荐判断是否 会员,促销2
     *
     * @param inData
     * @param serverOutData
     */
    public void recommendation2(GetQueryProductInData inData, GetServerOutData serverOutData) {
        if (CollUtil.isNotEmpty(serverOutData.getProductList2())) {
            //获取所有的商品编码
            List<String> proLists2 = serverOutData.getProductList2().stream().map(GetServerProduct2OutData::getProCode2).collect(Collectors.toList());

            //查询所有的 商品会员信息
            GetQueryProductInData data = new GetQueryProductInData();
            data.setProList(proLists2);
            data.setClientId(inData.getClientId());
            data.setBrId(inData.getBrId());
            List<GetPromByProCodeOutData> codeMemberAll2 = this.gaiaSdPromHeadMapper.promByProCodeMemberAll(data);

            PromotionMethodInData methodInData = new PromotionMethodInData();
            methodInData.setClient(inData.getClientId());
            methodInData.setBrId(inData.getBrId());
            methodInData.setProLists(proLists2);
            List<PromotionalConditionData> appCodeList = gaiaSdPromHeadMapper.findProductPromotionByAppCode(methodInData);


            if (CollUtil.isNotEmpty(codeMemberAll2)) {
                for (GetServerProduct2OutData product2OutData : serverOutData.getProductList2()) {
                    for (GetPromByProCodeOutData codeOutData : codeMemberAll2) {
                        GetPromByProCodeOutData copy = CopyUtil.copy(codeOutData, GetPromByProCodeOutData.class);
                        if ("PROM_HYRJ".equals(copy.getPromotionTypeId()) && product2OutData.getProCode2().equals(copy.getProCode())) { //会员日价
                            product2OutData.setProm(copy);    //当前商品的 会员信息
                            break;
                        } else if ("PROM_HYRZ".equals(copy.getPromotionTypeId())) {//会员日折扣
                            BigDecimal divide = new BigDecimal(copy.getDiscMem()).divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP);
                            BigDecimal multiply = new BigDecimal(product2OutData.getProAmount2()).multiply(divide).setScale(2, BigDecimal.ROUND_HALF_UP);
                            copy.setPriceMem(multiply.toString());//折扣后价格
                            product2OutData.setProm(copy);    //当前商品的 会员信息
                            break;
                        } else if ("PROM_HYJ".equals(copy.getPromotionTypeId()) && product2OutData.getProCode2().equals(copy.getProCode())) {//会员价
                            product2OutData.setProm(copy);    //当前商品的 会员信息
                            break;
                        }
                    }
                }

                if (CollUtil.isNotEmpty(appCodeList)) {
                    for (GetServerProduct2OutData product2OutData : serverOutData.getProductList2()) {
                        List<PromotionalConditionData> list = new LinkedList();
                        for (PromotionalConditionData conditionData : appCodeList) {
                            if ("assign1".equals(conditionData.getGsphPara3())) { //全场商品
                                list.add(conditionData);
                            } else if (product2OutData.getProCode2().equals(conditionData.getProCode())) {
                                list.add(conditionData);
                            }
                        }
                        product2OutData.setConditionList2(list);
                    }
                }

            }


        }
    }

    /**
     * 药事推荐
     *
     * @param inData
     * @return
     */
    private GetServerOutData drugRecommendatio(GetQueryProductInData inData, String proCode) {
        GetServerOutData outData = new GetServerOutData();
        //主要成分
        String compclass = null;
        //剂型 集合
        String partform = null;
        //1.遍历商品列表 将所有成分和剂型分别保存到list
        GetProductInfoQueryInData getProductInfoQueryInData = new GetProductInfoQueryInData();
        getProductInfoQueryInData.setClient(inData.getClientId());
        getProductInfoQueryInData.setGspp_br_id(inData.getBrId());
        getProductInfoQueryInData.setGspgProId(proCode);
        GetProductInfoQueryOutData queryOutData = this.productBasicMapper.getProductBasicListAByApp(getProductInfoQueryInData);

        outData.setContraExplain5(Optional.ofNullable(queryOutData.getGsscExplain5()).orElse(""));
        outData.setTipsExplain5(Optional.ofNullable(queryOutData.getGsstExplain5()).orElse(""));
        outData.setDiffExplain5(Optional.ofNullable(queryOutData.getGssdExplain5()).orElse(""));

        if (ObjectUtil.isNotEmpty(queryOutData)) {
            partform = Optional.ofNullable(queryOutData.getProForm()).orElse("");//关联成分成分编码
            compclass = Optional.ofNullable(queryOutData.getProCompclass()).orElse("");//主成分分类编码
        }

        //2.按所有成分 按主成分优先级 单店优先级和关联成分优先级 查询出所有不包含主成分的所有关联成分
        GaiaSdServerRelevancy relevancy = new GaiaSdServerRelevancy();
        relevancy.setClientId(inData.getClientId());
        relevancy.setGssrBrId(inData.getBrId());
        relevancy.setGssrCompclass(compclass);
        relevancy.setGssrReleCompclass(compclass);
        List<GaiaSdServerRelevancy> list = this.serverRelevancyMapper.getAllByCompClass(relevancy);
        if (CollUtil.isEmpty(list)) {
            return outData;
        }
        Map<String, Object> map = new HashMap<>();
        //3.遍历所有关联成分 查询出有库存的商品 筛选出前两个
        List<GetServerProduct1OutData> tipsList1;
        List<GetServerProduct1OutData> tipsList1One = new ArrayList<>();
        List<GetServerProduct1OutData> tipsList1Two = new ArrayList<>();
        List<GetServerProduct1OutData> tipsList1Three = new ArrayList<>();

        List<GetServerProduct2OutData> tipsList2;
        List<GetServerProduct2OutData> tipsList2One = new ArrayList<>();
        List<GetServerProduct2OutData> tipsList2Two = new ArrayList<>();
        List<GetServerProduct2OutData> tipsList2Three = new ArrayList<>();
        int j = 0;
        for (GaiaSdServerRelevancy serverRelevancy : list) {
            String compClass = serverRelevancy.getGssrCompclass();//主成分分类
            String releCompclass = serverRelevancy.getGssrReleCompclass();//关联成分

            if (ObjectUtil.isNull(map.get(releCompclass)) && j < 2) {
                if (CollUtil.isEmpty(outData.getProductList1())) {
                    tipsList1 = this.productBasicMapper.getProductListByCompclass1(releCompclass, inData.getBrId(), inData.getClientId());
                    if (CollUtil.isNotEmpty(tipsList1)) {
                        for (GetServerProduct1OutData getServerProduct1OutData : tipsList1) {
                            if (StrUtil.isBlank(getServerProduct1OutData.getVaildDate1())) {
                                tipsList1Three.add(getServerProduct1OutData);
                                continue;
                            }
                            String vaildDate1 = getServerProduct1OutData.getVaildDate1();
                            long between = between(new Date(), parse(vaildDate1), DateUnit.DAY);
                            if (between < 180) {
                                tipsList1One.add(getServerProduct1OutData);
                            } else {
                                tipsList1Two.add(getServerProduct1OutData);
                            }
                            getServerProduct1OutData.setPriority1(serverRelevancy.getGssrPriority1().toString());
                            getServerProduct1OutData.setPriority3(serverRelevancy.getGssrPriority3().toString());
                            //添加商品个税
                            getServerProduct1OutData.setMovTax1(new BigDecimal(getServerProduct1OutData.getMovTax1().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());

                        }
                        if (CollUtil.isNotEmpty(tipsList1One)) {
                            if (tipsList1One.size() > 2) {
                                List<GetServerProduct1OutData> outData1 = tipsList1One.stream().sorted(Comparator.comparing(GetServerProduct1OutData::getGrossProfit1).reversed()).limit(2).collect(Collectors.toList());
                                outData.setProductList1(outData1);
                            } else {
                                if (CollUtil.isNotEmpty(tipsList1Two)) {
                                    tipsList1Two.forEach(tipsList1One::add);
                                    List<GetServerProduct1OutData> collect = tipsList1One.stream()
                                            .sorted(Comparator.comparing(GetServerProduct1OutData::getVaildDate1).reversed())
                                            .sorted(Comparator.comparing(GetServerProduct1OutData::getGrossProfit1).reversed())
                                            .limit(2).collect(Collectors.toList());
                                    if (CollUtil.isNotEmpty(collect)) {
                                        outData.setProductList1(collect);
                                    } else {
                                        List<GetServerProduct1OutData> dataList = tipsList1Three.stream()
                                                .sorted(Comparator.comparing(GetServerProduct1OutData::getBatch1))
                                                .limit(2).collect(Collectors.toList());
                                        outData.setProductList1(dataList);
                                    }
                                }
                            }
                        } else {
                            if (CollUtil.isNotEmpty(tipsList1Two)) {
                                List<GetServerProduct1OutData> collect = tipsList1Two.stream()
                                        .sorted(Comparator.comparing(GetServerProduct1OutData::getVaildDate1).reversed())
                                        .sorted(Comparator.comparing(GetServerProduct1OutData::getGrossProfit1).reversed())
                                        .limit(2).collect(Collectors.toList());
                                if (CollUtil.isNotEmpty(collect)) {
                                    outData.setProductList1(collect);
                                } else {
                                    List<GetServerProduct1OutData> dataList = tipsList1Three.stream()
                                            .sorted(Comparator.comparing(GetServerProduct1OutData::getBatch1))
                                            .limit(2).collect(Collectors.toList());
                                    outData.setProductList1(dataList);
                                }
                            }
                        }

                        outData.setRelevancyExplain5(serverRelevancy.getGssrExplain5());
                        j++;
                    }
                } else {
                    tipsList2 = this.productBasicMapper.getProductListByCompclass2(releCompclass, inData.getBrId(), inData.getClientId());
                    if (CollUtil.isNotEmpty(tipsList2)) {
                        for (GetServerProduct2OutData getServerProduct2OutData : tipsList2) {
                            if (StrUtil.isBlank(getServerProduct2OutData.getVaildDate2())) {
                                tipsList2Three.add(getServerProduct2OutData);
                                continue;
                            }
                            String vaildDate2 = getServerProduct2OutData.getVaildDate2();
                            long between = between(new Date(), parse(vaildDate2), DateUnit.DAY);
                            if (between < 180) {
                                tipsList2One.add(getServerProduct2OutData);
                            } else {
                                tipsList2Two.add(getServerProduct2OutData);
                            }

                            getServerProduct2OutData.setPriority1(serverRelevancy.getGssrPriority1().toString());
                            getServerProduct2OutData.setPriority3(serverRelevancy.getGssrPriority3().toString());
                            //添加商品个税
                            getServerProduct2OutData.setMovTax2(new BigDecimal(getServerProduct2OutData.getMovTax2().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                        }

                        if (CollUtil.isNotEmpty(tipsList2One)) {
                            if (tipsList2One.size() > 2) {
                                List<GetServerProduct2OutData> outData2 = tipsList2One.stream().sorted(Comparator.comparing(GetServerProduct2OutData::getGrossProfit2).reversed()).limit(2).collect(Collectors.toList());
                                outData.setProductList2(outData2);
                            } else {
                                if (CollUtil.isNotEmpty(tipsList2Two)) {
                                    tipsList2Two.forEach(tipsList2One::add);
                                    List<GetServerProduct2OutData> collect = tipsList2One.stream()
                                            .sorted(Comparator.comparing(GetServerProduct2OutData::getVaildDate2).reversed())
                                            .sorted(Comparator.comparing(GetServerProduct2OutData::getGrossProfit2).reversed())
                                            .limit(2).collect(Collectors.toList());

                                    if (CollUtil.isNotEmpty(collect)) {
                                        outData.setProductList2(collect);
                                    } else {
                                        List<GetServerProduct2OutData> dataList = tipsList2Three.stream()
                                                .sorted(Comparator.comparing(GetServerProduct2OutData::getBatch2))
                                                .limit(2).collect(Collectors.toList());
                                        outData.setProductList2(dataList);
                                    }
                                }
                            }
                        } else {
                            if (CollUtil.isNotEmpty(tipsList2Two)) {
                                List<GetServerProduct2OutData> collect = tipsList2Two.stream()
                                        .sorted(Comparator.comparing(GetServerProduct2OutData::getVaildDate2).reversed())
                                        .sorted(Comparator.comparing(GetServerProduct2OutData::getGrossProfit2).reversed())
                                        .limit(2).collect(Collectors.toList());
                                if (CollUtil.isNotEmpty(collect)) {
                                    outData.setProductList2(collect);
                                } else {
                                    List<GetServerProduct2OutData> dataList = tipsList2Three.stream()
                                            .sorted(Comparator.comparing(GetServerProduct2OutData::getBatch2))
                                            .limit(2).collect(Collectors.toList());
                                    outData.setProductList2(dataList);
                                }
                            }
                        }

                        outData.setRelevancyExplain10(serverRelevancy.getGssrExplain5());
                        j++;
                    }
                }
            }
            map.put(releCompclass, serverRelevancy);
        }

        return outData;
    }

    /**
     * 计算单品促销
     *
     * @param inData         订单数据
     * @param item           生效的促销
     * @param promUnitarySet 促销规则
     * @return 是否生效
     */
    private void caculateSiangleProm(AppAmountVo inData, AppPromData item, GaiaSdPromUnitarySet promUnitarySet, BigDecimal joinPromQty) {
        BigDecimal subQty = joinPromQty.subtract(item.getSinglePromDetail().getReachQty());
        BigDecimal totalAmt = joinPromQty.multiply(item.getSinglePromDetail().getNormalPrice());
        BigDecimal repeatNum = BigDecimal.ZERO;
        //数量满足，按当前阶数计算价格
        if ("price".equals(item.getSinglePromDetail().getPreferenceType())) {
            //计算出总价
            if ("repeat1".equals(item.getPara1())) { //循环生效
                BigDecimal[] divideAndRemainder = joinPromQty.divideAndRemainder(item.getSinglePromDetail().getReachQty());
                repeatNum = divideAndRemainder[0];
                if ("3".equals(item.getPart())) {
                    totalAmt = promUnitarySet.getGspusPrc3().multiply(divideAndRemainder[0]).add(divideAndRemainder[1].multiply(item.getSinglePromDetail().getNormalPrice()));
                } else if ("2".equals(item.getPart())) {
                    totalAmt = promUnitarySet.getGspusPrc2().multiply(divideAndRemainder[0]).add(divideAndRemainder[1].multiply(item.getSinglePromDetail().getNormalPrice()));
                } else if ("1".equals(item.getPart())) {
                    totalAmt = promUnitarySet.getGspusPrc1().multiply(divideAndRemainder[0]).add(divideAndRemainder[1].multiply(item.getSinglePromDetail().getNormalPrice()));
                }
            } else if ("repeat3".equals(item.getPara1())) {//只生效一次
                repeatNum = new BigDecimal("1");
                if ("3".equals(item.getPart())) {
                    totalAmt = promUnitarySet.getGspusPrc3().add(subQty.multiply(item.getSinglePromDetail().getNormalPrice()));
                } else if ("2".equals(item.getPart())) {
                    totalAmt = promUnitarySet.getGspusPrc2().add(subQty.multiply(item.getSinglePromDetail().getNormalPrice()));
                } else if ("1".equals(item.getPart())) {
                    totalAmt = promUnitarySet.getGspusPrc1().add(subQty.multiply(item.getSinglePromDetail().getNormalPrice()));
                }
            }
        } else if ("discount".equals(item.getSinglePromDetail().getPreferenceType())) {
            //计算出总价
            if ("repeat1".equals(item.getPara1())) { //循环生效
                BigDecimal[] divideAndRemainder = joinPromQty.divideAndRemainder(item.getSinglePromDetail().getReachQty());
                repeatNum = divideAndRemainder[0];
                if ("3".equals(item.getPart())) {
                    totalAmt = new BigDecimal(promUnitarySet.getGspusRebate3()).multiply(new BigDecimal(promUnitarySet.getGspusQty3())).multiply(divideAndRemainder[0]).divide(new BigDecimal("100")).multiply(item.getSinglePromDetail().getNormalPrice()).add(divideAndRemainder[1].multiply(item.getSinglePromDetail().getNormalPrice()));
                } else if ("2".equals(item.getPart())) {
                    totalAmt = new BigDecimal(promUnitarySet.getGspusRebate2()).multiply(new BigDecimal(promUnitarySet.getGspusQty2())).multiply(divideAndRemainder[0]).divide(new BigDecimal("100")).multiply(item.getSinglePromDetail().getNormalPrice()).add(divideAndRemainder[1].multiply(item.getSinglePromDetail().getNormalPrice()));
                } else if ("1".equals(item.getPart())) {
                    totalAmt = new BigDecimal(promUnitarySet.getGspusRebate1()).multiply(new BigDecimal(promUnitarySet.getGspusQty1())).multiply(divideAndRemainder[0]).divide(new BigDecimal("100")).multiply(item.getSinglePromDetail().getNormalPrice()).add(divideAndRemainder[1].multiply(item.getSinglePromDetail().getNormalPrice()));
                }
            } else if ("repeat3".equals(item.getPara1())) {//只生效一次
                repeatNum = new BigDecimal("1");
                if ("3".equals(item.getPart())) {
                    totalAmt = new BigDecimal(promUnitarySet.getGspusRebate3()).multiply(new BigDecimal(promUnitarySet.getGspusQty3())).divide(new BigDecimal("100")).multiply(item.getSinglePromDetail().getNormalPrice()).add(subQty.multiply(item.getSinglePromDetail().getNormalPrice()));
                } else if ("2".equals(item.getPart())) {
                    totalAmt = new BigDecimal(promUnitarySet.getGspusRebate2()).multiply(new BigDecimal(promUnitarySet.getGspusQty2())).divide(new BigDecimal("100")).multiply(item.getSinglePromDetail().getNormalPrice()).add(subQty.multiply(item.getSinglePromDetail().getNormalPrice()));
                } else if ("1".equals(item.getPart())) {
                    totalAmt = new BigDecimal(promUnitarySet.getGspusRebate1()).multiply(new BigDecimal(promUnitarySet.getGspusQty1())).divide(new BigDecimal("100")).multiply(item.getSinglePromDetail().getNormalPrice()).add(subQty.multiply(item.getSinglePromDetail().getNormalPrice()));
                }
            }
        }
        item.getSinglePromDetail().setRepeatNum(repeatNum);
        BigDecimal tempAmt = totalAmt;
        //平摊至每一行，若有尾差记录到最后一行
        for (AppAmountCalculationVo row : inData.getVoList()) {
            if(ObjectUtil.isNotEmpty(row.getActivityProm())){
                if (item.getPromId().equals(row.getActivityProm().getPromId())&&item.getSinglePromDetail().getProCode().equals(row.getProCode())) {
                    BigDecimal rowTotalPrice = new BigDecimal(row.getProQty()).divide(joinPromQty, 4, BigDecimal.ROUND_HALF_UP).multiply(totalAmt);
                    row.setTotalAmt(rowTotalPrice.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                    row.setRealPrice(rowTotalPrice.divide(new BigDecimal(row.getProQty()), 4, BigDecimal.ROUND_HALF_UP).setScale(4, BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString());
                    row.setPromPrice(rowTotalPrice.divide(new BigDecimal(row.getProQty()), 4, BigDecimal.ROUND_HALF_UP).setScale(4, BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString());
                    tempAmt = tempAmt.subtract(new BigDecimal(row.getTotalAmt()));
                    row.setActivityProm(item);
                    row.setStatus("单品促销");
                }
            }
        }
        //有尾差则记录到最后一行
        if (tempAmt.compareTo(BigDecimal.ZERO) != 0) {
            for (int i = inData.getVoList().size() - 1; i >= 0; i--) {
                AppAmountCalculationVo row = inData.getVoList().get(i);
                if(ObjectUtil.isNotEmpty(row.getActivityProm())) {
                    if (item.getPromId().equals(row.getActivityProm().getPromId()) && item.getSinglePromDetail().getProCode().equals(row.getProCode())) {
                        BigDecimal rowTotalPrice = new BigDecimal(row.getTotalAmt()).add(tempAmt);
                        row.setTotalAmt(rowTotalPrice.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                        row.setRealPrice(rowTotalPrice.divide(new BigDecimal(row.getProQty()), 4, BigDecimal.ROUND_HALF_UP).setScale(4, BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString());
                        row.setPromPrice(rowTotalPrice.divide(new BigDecimal(row.getProQty()), 4, BigDecimal.ROUND_HALF_UP).setScale(4, BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString());
                        break;
                    }
                }
            }
        }
    }

}
