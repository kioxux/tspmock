package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * 传参实体
 *
 * @author xiaoyuan on 2020/9/28
 */
@Data
public class ElectronVoucherInVO implements Serializable {
    private static final long serialVersionUID = 4240447563450091267L;

    @ApiModelProperty(value = "加盟商")
    private String client;

   // @NotBlank(message = "起始不可为空")
    @ApiModelProperty(value = "起始店号")
    private String brStart;

   // @NotBlank(message = "店号不可为空")
    @ApiModelProperty(value = "结束店号")
    private String brEnd;

    @ApiModelProperty(value = "主题单号")
    private String gsetsId;


    @ApiModelProperty(value = "起始日期")
    private String gsebsBeginDate;


    @ApiModelProperty(value = "结束日期")
    private String gsebsEndDate;

    @ApiModelProperty(value = "起始时间")
    private String gsebsBeginTime;

    @ApiModelProperty(value = "结束时间")
    private String gsebsEndTime;

    @ApiModelProperty(value = "0为送券，1为用券")
    private String gsebsType;

    @Valid
    @NotEmpty(message = "活动等信息不可为空")
    @ApiModelProperty(value = "电子券系列" ,required = true)
    private List<ElectronChildInVO> childInVOS;

    @ApiModelProperty(value = "审核状态  是否审核   N为否，Y为是")
    private String gsebsStatus;

    @NotBlank(message = "店号状态不可为空")
    @ApiModelProperty(value = "店号状态")
    private String storeType;

    @ApiModelProperty(value = "业务单号 修改时不可为空!")
    private String gsebsId;

    @ApiModelProperty(value = "店号分类")
    private String storeGroup;

    @ApiModelProperty(value = "店号集合")
    private List<String> storeIds;

    /**
     * 促销商品是否参与
     */
    @ApiModelProperty(value = "催销商品是否参与")
    private String gsebsProm;

    /**
     * 创建日期
     */
    private String gsebsCreateDate;

    /**
     * 创建时间
     */
    private String gsebsCreateTime;

    /**
     * 最大赠送次数
     */
    private String gsebsMaxQty;

    /**
     * 备注
     */
    private String gsebsRemark;

    /**
     * 商品组id
     */
    private String groupId;
    /**
     * NULL/0为按固定日期范围生效（现有逻辑），为1则按券有效天数生效
     */
    private String gsebsRule;
    /**
     *循环日期 按/分割
     */
    private String gsebsMonth;
    /**
     *循环星期 按/分割
     */
    private String gsebsWeek;
    /**
     *发券后有效天数
     */
    private String gsebsValidDay;



}
