package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MemberConditionInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "门店编号列表")
    private List<String> brIdList;

    @ApiModelProperty(value = "会员名称")
    private String memberName;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "性别")
    private String sex;

    @ApiModelProperty(value = "创建卡起始日期")
    private String startDate;

    @ApiModelProperty(value = "创建卡结束日期")
    private String endDate;

    @ApiModelProperty(value = "最后消费日期(起始)")
    private String nearConsumeStartDate;

    @ApiModelProperty(value = "最后消费日期(结束)")
    private String nearConsumeEndDate;

    @ApiModelProperty(value = "卡类别")
    private String cardType;

    @ApiModelProperty(value = "办卡员工")
    private String inputCardEmp;

    @ApiModelProperty(value = "会员卡号")
    private String memberCardId;

    @ApiModelProperty(value = "页码")
    private int pageNum;

    @ApiModelProperty(value = "每页显示条数")
    private int pageSize;

    @ApiModelProperty(value = "卡状态")
    private String gsmbcStatus;
}
