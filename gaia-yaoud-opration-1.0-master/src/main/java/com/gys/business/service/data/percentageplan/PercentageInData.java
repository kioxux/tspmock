package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class  PercentageInData {
    @ApiModelProperty(value = "主键id")
    private Integer id;
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店编码组")
    private String[] storeCodes;
    @ApiModelProperty(value = "方案名称")
    private String planName;
    @ApiModelProperty(value = "适用月份(新版不使用)")
    private String planSuitMonth;
    @ApiModelProperty(value = "提成方式: 0 按销售额提成 1 按毛利额提成")
    private String planAmtWay;
    @ApiModelProperty(value = "提成方式:0 按商品毛利率  1 按销售毛利率")
    private String planRateWay;
    @ApiModelProperty(value = "负毛利率商品是否不参与销售提成 0 是 1 否")
    private String planIfNegative;
    @ApiModelProperty(value = "单品提成方式 0 不参与销售提成 1 参与销售提成")
    private String planProductWay;
    @ApiModelProperty(value = "提成方式 1 按零售额提成 2 按销售额提成 3 按毛利额提成")
    private String planPercentageWay;
    @ApiModelProperty(value = "创建人编码")
    private String planCreaterId;
    @ApiModelProperty(value = "创建人")
    private String planCreater;
    @ApiModelProperty(value = "销售提成明细列表")
    private List<PercentageSaleInData> saleInDataList;
    @ApiModelProperty(value = "单品提成明细列表")
    private List<PercentageProInData> proInDataList;
    @ApiModelProperty(value = "单品剔除明细列表")
    private List<PercentageProInData> rejectProList;
    @ApiModelProperty(value = "页数")
    private Integer pageNum;
    @ApiModelProperty(value = "每页个数")
    private Integer pageSize;
    @ApiModelProperty(value = "方案起始日期")
    private String planStartDate;
    @ApiModelProperty(value = "方案结束日期")
    private String planEndDate;
    @ApiModelProperty(value = "提成类型 1 销售 2 单品")
    private String planType;
    @ApiModelProperty(value = "门店分配比例（默认0）")
    private String planScaleSto;
    @ApiModelProperty(value = "员工分配比例（默认100）")
    private String planScaleSaler;
    @ApiModelProperty(value = "时间数组（planStartDate,planEndDate）")
    private String[] dateArr;
    @ApiModelProperty(value = "剔除商品设置：0 否 1 是")
    private String planRejectPro;


    @ApiModelProperty(value = "剔除折扣率 操作符号")
    private String planRejectDiscountRateSymbol;

    @ApiModelProperty(value = "剔除折扣率 百分比值")
    private String planRejectDiscountRate;

    @ApiModelProperty(value = "剔除商品分类")
    private String[][] classArr;
    @ApiModelProperty(value = "操作状态 0 未删除 1 已删除 2 暂存（试算）")
    private String deleteFlag;
    @ApiModelProperty(value = "审核状态 0 已保存 1 已审核 2 已停用 3 反审核")
    private String planStatus;
    @ApiModelProperty(value = "已审核提醒内容/修改原因提醒/停用原因")
    private String planReason;
    @ApiModelProperty(value = "试算时间数组（planStartDate,planEndDate）")
    private String[] trialDateArr;
    private String[] planDate;
}
