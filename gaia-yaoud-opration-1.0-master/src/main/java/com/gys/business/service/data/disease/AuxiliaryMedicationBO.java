package com.gys.business.service.data.disease;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/7 15:34
 * @Version 1.0.0
 **/
@Data
public class AuxiliaryMedicationBO {
    //  辅助用药1/预防保健2
    private String auxiliaryMedication;
    //  商品编码
    private String proCode;
    //  商品通用名称
    private String proCommonName;
    //  规格
    private String specs;
    //  关联说明
    private String associationDescription;
    //  库存数量
    private BigDecimal qty;
    //  零售价
    private BigDecimal priceNormal;
    //  毛利额
    private BigDecimal grossProfit;
    //  效期
    private String vaildDate;
    //  效期
    private String sort;
}
