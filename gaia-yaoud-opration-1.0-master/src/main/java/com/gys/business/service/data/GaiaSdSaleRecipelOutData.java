//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "处方登记")
public class GaiaSdSaleRecipelOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "审方单号")
    private String gssrVoucherId;
    @ApiModelProperty(value = "审方类型")
    private String gssrType;
    @ApiModelProperty(value = "登记店号")
    private String gssrBrId;
    @ApiModelProperty(value = "登记店名")
    private String gssrBrName;
    @ApiModelProperty(value = "登记日期")
    private String gssrDate;
    @ApiModelProperty(value = "登记时间")
    private String gssrTime;
    @ApiModelProperty(value = "登记人员")
    private String gssrEmp;
    @ApiModelProperty(value = "上传状态")
    private String gssrUploadFlag;
    @ApiModelProperty(value = "销售门店")
    private String gssrSaleBrId;
    @ApiModelProperty(value = "销售门店名称")
    private String gssrSaleBrName;
    @ApiModelProperty(value = "销售日期")
    private String gssrSaleDate;
    @ApiModelProperty(value = "销售单号")
    private String gssrSaleBillNo;
    @ApiModelProperty(value = "顾客姓名")
    private String gssrCustName;
    @ApiModelProperty(value = "顾客性别")
    private String gssrCustSex;
    @ApiModelProperty(value = "顾客年龄")
    private String gssrCustAge;
    @ApiModelProperty(value = "顾客身份证")
    private String gssrCustIdcard;
    @ApiModelProperty(value = "顾客手机")
    private String gssrCustMobile;
    @ApiModelProperty(value = "审方药师编号")
    private String gssrPharmacistId;
    @ApiModelProperty(value = "审方药师姓名")
    private String gssrPharmacistName;
    @ApiModelProperty(value = "审方日期")
    private String gssrCheckDate;
    @ApiModelProperty(value = "审方时间")
    private String gssrCheckTime;
    @ApiModelProperty(value = "审方状态")
    private String gssrCheckStatus;
    @ApiModelProperty(value = "商品编码")
    private String gssrProId;
    @ApiModelProperty(value = "商品批号")
    private String gssrBatchNo;
    @ApiModelProperty(value = "数量")
    private String gssrQty;
    @ApiModelProperty(value = "处方编号")
    private String gssrRecipelId;
    @ApiModelProperty(value = "处方医院")
    private String gssrRecipelHospital;
    @ApiModelProperty(value = "厨房科室")
    private String gssrRecipelDepartment;
    @ApiModelProperty(value = "处方医生")
    private String gssrRecipelDoctor;
    @ApiModelProperty(value = "症状")
    private String gssrSymptom;
    @ApiModelProperty(value = "诊断")
    private String gssrDiagnose;
    @ApiModelProperty(value = "处方图片编号")
    private String gssrRecipelPicId;
    @ApiModelProperty(value = "处方图片名称")
    private String gssrRecipelPicName;
    @ApiModelProperty(value = "处方图片地址")
    private String gssrRecipelPicAddress;
    @ApiModelProperty(value = "")
    private Integer index;
    @ApiModelProperty(value = "商品名称")
    private String proName;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "单位")
    private String proUnit;
    @ApiModelProperty(value = "价格")
    private String price;
    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "剂型")
    private String proForm;

    /**
     * 处方类别 1-单轨处方、2-甲类OTC、3-乙类OTC、4-双跨处方、5-双规处方
     */
    @ApiModelProperty(value = "处方类别")
    private String proPresclass;

    /**
     * 是否登记双规处方 0：不登记  1：登记
     */
    @ApiModelProperty(value = "是否登记双规处方")
    private String recisaleFlag;

    /**
     * 处方图片 0：无   1:有
     */

    private String isPicture;

    /**
     * 是否跟新图片：0否  1是
     */
    private String isUpload;

    /**
     * 是否允许补登记处方图片：0否  1是
     */
    private String patchUpload;

    /**
     * 登记人员姓名
     */
    private String gssrEmpName;

    public String getIsUpload() {
        if ("".equals(isUpload) || isUpload == null){
            return "0";
        }
        return isUpload;
    }

    public void setIsUpload(String isUpload) {
        this.isUpload = isUpload;
    }

    public String getIsPicture() {
        if ("".equals(isPicture) || isPicture == null){
            return "0";
        }
        return isPicture;
    }

    public void setIsPicture(String isPicture) {
        this.isPicture = isPicture;
    }

    public String getRecisaleFlag() {
        return recisaleFlag;
    }

    public void setRecisaleFlag(String recisaleFlag) {
        this.recisaleFlag = recisaleFlag;
    }

    public String getProPresclass() {
        return proPresclass;
    }

    public void setProPresclass(String proPresclass) {
        this.proPresclass = proPresclass;
    }

    public GaiaSdSaleRecipelOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGssrVoucherId() {
        return this.gssrVoucherId;
    }

    public String getGssrType() {
        return this.gssrType;
    }

    public String getGssrBrId() {
        return this.gssrBrId;
    }

    public String getGssrBrName() {
        return this.gssrBrName;
    }

    public String getGssrDate() {
        return this.gssrDate;
    }

    public String getGssrTime() {
        return this.gssrTime;
    }

    public String getGssrEmp() {
        return this.gssrEmp;
    }

    public String getGssrUploadFlag() {
        return this.gssrUploadFlag;
    }

    public String getGssrSaleBrId() {
        return this.gssrSaleBrId;
    }

    public String getGssrSaleBrName() {
        return this.gssrSaleBrName;
    }

    public String getGssrSaleDate() {
        return this.gssrSaleDate;
    }

    public String getGssrSaleBillNo() {
        return this.gssrSaleBillNo;
    }

    public String getGssrCustName() {
        return this.gssrCustName;
    }

    public String getGssrCustSex() {
        return this.gssrCustSex;
    }

    public String getGssrCustAge() {
        return this.gssrCustAge;
    }

    public String getGssrCustIdcard() {
        return this.gssrCustIdcard;
    }

    public String getGssrCustMobile() {
        return this.gssrCustMobile;
    }

    public String getGssrPharmacistId() {
        return this.gssrPharmacistId;
    }

    public String getGssrPharmacistName() {
        return this.gssrPharmacistName;
    }

    public String getGssrCheckDate() {
        return this.gssrCheckDate;
    }

    public String getGssrCheckTime() {
        return this.gssrCheckTime;
    }

    public String getGssrCheckStatus() {
        return this.gssrCheckStatus;
    }

    public String getGssrProId() {
        return this.gssrProId;
    }

    public String getGssrBatchNo() {
        return this.gssrBatchNo;
    }

    public String getGssrQty() {
        return this.gssrQty;
    }

    public String getGssrRecipelId() {
        return this.gssrRecipelId;
    }

    public String getGssrRecipelHospital() {
        return this.gssrRecipelHospital;
    }

    public String getGssrRecipelDepartment() {
        return this.gssrRecipelDepartment;
    }

    public String getGssrRecipelDoctor() {
        return this.gssrRecipelDoctor;
    }

    public String getGssrSymptom() {
        return this.gssrSymptom;
    }

    public String getGssrDiagnose() {
        return this.gssrDiagnose;
    }

    public String getGssrRecipelPicId() {
        return this.gssrRecipelPicId;
    }

    public String getGssrRecipelPicName() {
        return this.gssrRecipelPicName;
    }

    public String getGssrRecipelPicAddress() {
        return this.gssrRecipelPicAddress;
    }

    public Integer getIndex() {
        return this.index;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getPrice() {
        return this.price;
    }

    public String getProRegisterNo() {
        return this.proRegisterNo;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProForm() {
        return this.proForm;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGssrVoucherId(final String gssrVoucherId) {
        this.gssrVoucherId = gssrVoucherId;
    }

    public void setGssrType(final String gssrType) {
        this.gssrType = gssrType;
    }

    public void setGssrBrId(final String gssrBrId) {
        this.gssrBrId = gssrBrId;
    }

    public void setGssrBrName(final String gssrBrName) {
        this.gssrBrName = gssrBrName;
    }

    public void setGssrDate(final String gssrDate) {
        this.gssrDate = gssrDate;
    }

    public void setGssrTime(final String gssrTime) {
        this.gssrTime = gssrTime;
    }

    public void setGssrEmp(final String gssrEmp) {
        this.gssrEmp = gssrEmp;
    }

    public void setGssrUploadFlag(final String gssrUploadFlag) {
        this.gssrUploadFlag = gssrUploadFlag;
    }

    public void setGssrSaleBrId(final String gssrSaleBrId) {
        this.gssrSaleBrId = gssrSaleBrId;
    }

    public void setGssrSaleBrName(final String gssrSaleBrName) {
        this.gssrSaleBrName = gssrSaleBrName;
    }

    public void setGssrSaleDate(final String gssrSaleDate) {
        this.gssrSaleDate = gssrSaleDate;
    }

    public void setGssrSaleBillNo(final String gssrSaleBillNo) {
        this.gssrSaleBillNo = gssrSaleBillNo;
    }

    public void setGssrCustName(final String gssrCustName) {
        this.gssrCustName = gssrCustName;
    }

    public void setGssrCustSex(final String gssrCustSex) {
        this.gssrCustSex = gssrCustSex;
    }

    public void setGssrCustAge(final String gssrCustAge) {
        this.gssrCustAge = gssrCustAge;
    }

    public void setGssrCustIdcard(final String gssrCustIdcard) {
        this.gssrCustIdcard = gssrCustIdcard;
    }

    public void setGssrCustMobile(final String gssrCustMobile) {
        this.gssrCustMobile = gssrCustMobile;
    }

    public void setGssrPharmacistId(final String gssrPharmacistId) {
        this.gssrPharmacistId = gssrPharmacistId;
    }

    public void setGssrPharmacistName(final String gssrPharmacistName) {
        this.gssrPharmacistName = gssrPharmacistName;
    }

    public void setGssrCheckDate(final String gssrCheckDate) {
        this.gssrCheckDate = gssrCheckDate;
    }

    public void setGssrCheckTime(final String gssrCheckTime) {
        this.gssrCheckTime = gssrCheckTime;
    }

    public void setGssrCheckStatus(final String gssrCheckStatus) {
        this.gssrCheckStatus = gssrCheckStatus;
    }

    public void setGssrProId(final String gssrProId) {
        this.gssrProId = gssrProId;
    }

    public void setGssrBatchNo(final String gssrBatchNo) {
        this.gssrBatchNo = gssrBatchNo;
    }

    public void setGssrQty(final String gssrQty) {
        this.gssrQty = gssrQty;
    }

    public void setGssrRecipelId(final String gssrRecipelId) {
        this.gssrRecipelId = gssrRecipelId;
    }

    public void setGssrRecipelHospital(final String gssrRecipelHospital) {
        this.gssrRecipelHospital = gssrRecipelHospital;
    }

    public void setGssrRecipelDepartment(final String gssrRecipelDepartment) {
        this.gssrRecipelDepartment = gssrRecipelDepartment;
    }

    public void setGssrRecipelDoctor(final String gssrRecipelDoctor) {
        this.gssrRecipelDoctor = gssrRecipelDoctor;
    }

    public void setGssrSymptom(final String gssrSymptom) {
        this.gssrSymptom = gssrSymptom;
    }

    public void setGssrDiagnose(final String gssrDiagnose) {
        this.gssrDiagnose = gssrDiagnose;
    }

    public void setGssrRecipelPicId(final String gssrRecipelPicId) {
        this.gssrRecipelPicId = gssrRecipelPicId;
    }

    public void setGssrRecipelPicName(final String gssrRecipelPicName) {
        this.gssrRecipelPicName = gssrRecipelPicName;
    }

    public void setGssrRecipelPicAddress(final String gssrRecipelPicAddress) {
        this.gssrRecipelPicAddress = gssrRecipelPicAddress;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setPrice(final String price) {
        this.price = price;
    }

    public void setProRegisterNo(final String proRegisterNo) {
        this.proRegisterNo = proRegisterNo;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public String getPatchUpload() {
        if ("".equals(patchUpload) || patchUpload == null){
            return "0";
        }
        return patchUpload;
    }

    public void setPatchUpload(String patchUpload) {
        this.patchUpload = patchUpload;
    }


    public String getGssrEmpName() {
        return gssrEmpName;
    }

    public void setGssrEmpName(String gssrEmpName) {
        this.gssrEmpName = gssrEmpName;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GaiaSdSaleRecipelOutData)) {
            return false;
        } else {
            GaiaSdSaleRecipelOutData other = (GaiaSdSaleRecipelOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label539: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label539;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label539;
                    }

                    return false;
                }

                Object this$gssrVoucherId = this.getGssrVoucherId();
                Object other$gssrVoucherId = other.getGssrVoucherId();
                if (this$gssrVoucherId == null) {
                    if (other$gssrVoucherId != null) {
                        return false;
                    }
                } else if (!this$gssrVoucherId.equals(other$gssrVoucherId)) {
                    return false;
                }

                Object this$gssrType = this.getGssrType();
                Object other$gssrType = other.getGssrType();
                if (this$gssrType == null) {
                    if (other$gssrType != null) {
                        return false;
                    }
                } else if (!this$gssrType.equals(other$gssrType)) {
                    return false;
                }

                label518: {
                    Object this$gssrBrId = this.getGssrBrId();
                    Object other$gssrBrId = other.getGssrBrId();
                    if (this$gssrBrId == null) {
                        if (other$gssrBrId == null) {
                            break label518;
                        }
                    } else if (this$gssrBrId.equals(other$gssrBrId)) {
                        break label518;
                    }

                    return false;
                }

                label511: {
                    Object this$gssrBrName = this.getGssrBrName();
                    Object other$gssrBrName = other.getGssrBrName();
                    if (this$gssrBrName == null) {
                        if (other$gssrBrName == null) {
                            break label511;
                        }
                    } else if (this$gssrBrName.equals(other$gssrBrName)) {
                        break label511;
                    }

                    return false;
                }

                label504: {
                    Object this$gssrDate = this.getGssrDate();
                    Object other$gssrDate = other.getGssrDate();
                    if (this$gssrDate == null) {
                        if (other$gssrDate == null) {
                            break label504;
                        }
                    } else if (this$gssrDate.equals(other$gssrDate)) {
                        break label504;
                    }

                    return false;
                }

                Object this$gssrTime = this.getGssrTime();
                Object other$gssrTime = other.getGssrTime();
                if (this$gssrTime == null) {
                    if (other$gssrTime != null) {
                        return false;
                    }
                } else if (!this$gssrTime.equals(other$gssrTime)) {
                    return false;
                }

                label490: {
                    Object this$gssrEmp = this.getGssrEmp();
                    Object other$gssrEmp = other.getGssrEmp();
                    if (this$gssrEmp == null) {
                        if (other$gssrEmp == null) {
                            break label490;
                        }
                    } else if (this$gssrEmp.equals(other$gssrEmp)) {
                        break label490;
                    }

                    return false;
                }

                Object this$gssrUploadFlag = this.getGssrUploadFlag();
                Object other$gssrUploadFlag = other.getGssrUploadFlag();
                if (this$gssrUploadFlag == null) {
                    if (other$gssrUploadFlag != null) {
                        return false;
                    }
                } else if (!this$gssrUploadFlag.equals(other$gssrUploadFlag)) {
                    return false;
                }

                label476: {
                    Object this$gssrSaleBrId = this.getGssrSaleBrId();
                    Object other$gssrSaleBrId = other.getGssrSaleBrId();
                    if (this$gssrSaleBrId == null) {
                        if (other$gssrSaleBrId == null) {
                            break label476;
                        }
                    } else if (this$gssrSaleBrId.equals(other$gssrSaleBrId)) {
                        break label476;
                    }

                    return false;
                }

                Object this$gssrSaleBrName = this.getGssrSaleBrName();
                Object other$gssrSaleBrName = other.getGssrSaleBrName();
                if (this$gssrSaleBrName == null) {
                    if (other$gssrSaleBrName != null) {
                        return false;
                    }
                } else if (!this$gssrSaleBrName.equals(other$gssrSaleBrName)) {
                    return false;
                }

                Object this$gssrSaleDate = this.getGssrSaleDate();
                Object other$gssrSaleDate = other.getGssrSaleDate();
                if (this$gssrSaleDate == null) {
                    if (other$gssrSaleDate != null) {
                        return false;
                    }
                } else if (!this$gssrSaleDate.equals(other$gssrSaleDate)) {
                    return false;
                }

                label455: {
                    Object this$gssrSaleBillNo = this.getGssrSaleBillNo();
                    Object other$gssrSaleBillNo = other.getGssrSaleBillNo();
                    if (this$gssrSaleBillNo == null) {
                        if (other$gssrSaleBillNo == null) {
                            break label455;
                        }
                    } else if (this$gssrSaleBillNo.equals(other$gssrSaleBillNo)) {
                        break label455;
                    }

                    return false;
                }

                label448: {
                    Object this$gssrCustName = this.getGssrCustName();
                    Object other$gssrCustName = other.getGssrCustName();
                    if (this$gssrCustName == null) {
                        if (other$gssrCustName == null) {
                            break label448;
                        }
                    } else if (this$gssrCustName.equals(other$gssrCustName)) {
                        break label448;
                    }

                    return false;
                }

                Object this$gssrCustSex = this.getGssrCustSex();
                Object other$gssrCustSex = other.getGssrCustSex();
                if (this$gssrCustSex == null) {
                    if (other$gssrCustSex != null) {
                        return false;
                    }
                } else if (!this$gssrCustSex.equals(other$gssrCustSex)) {
                    return false;
                }

                Object this$gssrCustAge = this.getGssrCustAge();
                Object other$gssrCustAge = other.getGssrCustAge();
                if (this$gssrCustAge == null) {
                    if (other$gssrCustAge != null) {
                        return false;
                    }
                } else if (!this$gssrCustAge.equals(other$gssrCustAge)) {
                    return false;
                }

                label427: {
                    Object this$gssrCustIdcard = this.getGssrCustIdcard();
                    Object other$gssrCustIdcard = other.getGssrCustIdcard();
                    if (this$gssrCustIdcard == null) {
                        if (other$gssrCustIdcard == null) {
                            break label427;
                        }
                    } else if (this$gssrCustIdcard.equals(other$gssrCustIdcard)) {
                        break label427;
                    }

                    return false;
                }

                Object this$gssrCustMobile = this.getGssrCustMobile();
                Object other$gssrCustMobile = other.getGssrCustMobile();
                if (this$gssrCustMobile == null) {
                    if (other$gssrCustMobile != null) {
                        return false;
                    }
                } else if (!this$gssrCustMobile.equals(other$gssrCustMobile)) {
                    return false;
                }

                Object this$gssrPharmacistId = this.getGssrPharmacistId();
                Object other$gssrPharmacistId = other.getGssrPharmacistId();
                if (this$gssrPharmacistId == null) {
                    if (other$gssrPharmacistId != null) {
                        return false;
                    }
                } else if (!this$gssrPharmacistId.equals(other$gssrPharmacistId)) {
                    return false;
                }

                label406: {
                    Object this$gssrPharmacistName = this.getGssrPharmacistName();
                    Object other$gssrPharmacistName = other.getGssrPharmacistName();
                    if (this$gssrPharmacistName == null) {
                        if (other$gssrPharmacistName == null) {
                            break label406;
                        }
                    } else if (this$gssrPharmacistName.equals(other$gssrPharmacistName)) {
                        break label406;
                    }

                    return false;
                }

                label399: {
                    Object this$gssrCheckDate = this.getGssrCheckDate();
                    Object other$gssrCheckDate = other.getGssrCheckDate();
                    if (this$gssrCheckDate == null) {
                        if (other$gssrCheckDate == null) {
                            break label399;
                        }
                    } else if (this$gssrCheckDate.equals(other$gssrCheckDate)) {
                        break label399;
                    }

                    return false;
                }

                label392: {
                    Object this$gssrCheckTime = this.getGssrCheckTime();
                    Object other$gssrCheckTime = other.getGssrCheckTime();
                    if (this$gssrCheckTime == null) {
                        if (other$gssrCheckTime == null) {
                            break label392;
                        }
                    } else if (this$gssrCheckTime.equals(other$gssrCheckTime)) {
                        break label392;
                    }

                    return false;
                }

                Object this$gssrCheckStatus = this.getGssrCheckStatus();
                Object other$gssrCheckStatus = other.getGssrCheckStatus();
                if (this$gssrCheckStatus == null) {
                    if (other$gssrCheckStatus != null) {
                        return false;
                    }
                } else if (!this$gssrCheckStatus.equals(other$gssrCheckStatus)) {
                    return false;
                }

                label378: {
                    Object this$gssrProId = this.getGssrProId();
                    Object other$gssrProId = other.getGssrProId();
                    if (this$gssrProId == null) {
                        if (other$gssrProId == null) {
                            break label378;
                        }
                    } else if (this$gssrProId.equals(other$gssrProId)) {
                        break label378;
                    }

                    return false;
                }

                Object this$gssrBatchNo = this.getGssrBatchNo();
                Object other$gssrBatchNo = other.getGssrBatchNo();
                if (this$gssrBatchNo == null) {
                    if (other$gssrBatchNo != null) {
                        return false;
                    }
                } else if (!this$gssrBatchNo.equals(other$gssrBatchNo)) {
                    return false;
                }

                label364: {
                    Object this$gssrQty = this.getGssrQty();
                    Object other$gssrQty = other.getGssrQty();
                    if (this$gssrQty == null) {
                        if (other$gssrQty == null) {
                            break label364;
                        }
                    } else if (this$gssrQty.equals(other$gssrQty)) {
                        break label364;
                    }

                    return false;
                }

                Object this$gssrRecipelId = this.getGssrRecipelId();
                Object other$gssrRecipelId = other.getGssrRecipelId();
                if (this$gssrRecipelId == null) {
                    if (other$gssrRecipelId != null) {
                        return false;
                    }
                } else if (!this$gssrRecipelId.equals(other$gssrRecipelId)) {
                    return false;
                }

                Object this$gssrRecipelHospital = this.getGssrRecipelHospital();
                Object other$gssrRecipelHospital = other.getGssrRecipelHospital();
                if (this$gssrRecipelHospital == null) {
                    if (other$gssrRecipelHospital != null) {
                        return false;
                    }
                } else if (!this$gssrRecipelHospital.equals(other$gssrRecipelHospital)) {
                    return false;
                }

                label343: {
                    Object this$gssrRecipelDepartment = this.getGssrRecipelDepartment();
                    Object other$gssrRecipelDepartment = other.getGssrRecipelDepartment();
                    if (this$gssrRecipelDepartment == null) {
                        if (other$gssrRecipelDepartment == null) {
                            break label343;
                        }
                    } else if (this$gssrRecipelDepartment.equals(other$gssrRecipelDepartment)) {
                        break label343;
                    }

                    return false;
                }

                label336: {
                    Object this$gssrRecipelDoctor = this.getGssrRecipelDoctor();
                    Object other$gssrRecipelDoctor = other.getGssrRecipelDoctor();
                    if (this$gssrRecipelDoctor == null) {
                        if (other$gssrRecipelDoctor == null) {
                            break label336;
                        }
                    } else if (this$gssrRecipelDoctor.equals(other$gssrRecipelDoctor)) {
                        break label336;
                    }

                    return false;
                }

                Object this$gssrSymptom = this.getGssrSymptom();
                Object other$gssrSymptom = other.getGssrSymptom();
                if (this$gssrSymptom == null) {
                    if (other$gssrSymptom != null) {
                        return false;
                    }
                } else if (!this$gssrSymptom.equals(other$gssrSymptom)) {
                    return false;
                }

                Object this$gssrDiagnose = this.getGssrDiagnose();
                Object other$gssrDiagnose = other.getGssrDiagnose();
                if (this$gssrDiagnose == null) {
                    if (other$gssrDiagnose != null) {
                        return false;
                    }
                } else if (!this$gssrDiagnose.equals(other$gssrDiagnose)) {
                    return false;
                }

                label315: {
                    Object this$gssrRecipelPicId = this.getGssrRecipelPicId();
                    Object other$gssrRecipelPicId = other.getGssrRecipelPicId();
                    if (this$gssrRecipelPicId == null) {
                        if (other$gssrRecipelPicId == null) {
                            break label315;
                        }
                    } else if (this$gssrRecipelPicId.equals(other$gssrRecipelPicId)) {
                        break label315;
                    }

                    return false;
                }

                Object this$gssrRecipelPicName = this.getGssrRecipelPicName();
                Object other$gssrRecipelPicName = other.getGssrRecipelPicName();
                if (this$gssrRecipelPicName == null) {
                    if (other$gssrRecipelPicName != null) {
                        return false;
                    }
                } else if (!this$gssrRecipelPicName.equals(other$gssrRecipelPicName)) {
                    return false;
                }

                Object this$gssrRecipelPicAddress = this.getGssrRecipelPicAddress();
                Object other$gssrRecipelPicAddress = other.getGssrRecipelPicAddress();
                if (this$gssrRecipelPicAddress == null) {
                    if (other$gssrRecipelPicAddress != null) {
                        return false;
                    }
                } else if (!this$gssrRecipelPicAddress.equals(other$gssrRecipelPicAddress)) {
                    return false;
                }

                label294: {
                    Object this$index = this.getIndex();
                    Object other$index = other.getIndex();
                    if (this$index == null) {
                        if (other$index == null) {
                            break label294;
                        }
                    } else if (this$index.equals(other$index)) {
                        break label294;
                    }

                    return false;
                }

                label287: {
                    Object this$proName = this.getProName();
                    Object other$proName = other.getProName();
                    if (this$proName == null) {
                        if (other$proName == null) {
                            break label287;
                        }
                    } else if (this$proName.equals(other$proName)) {
                        break label287;
                    }

                    return false;
                }

                label280: {
                    Object this$proSpecs = this.getProSpecs();
                    Object other$proSpecs = other.getProSpecs();
                    if (this$proSpecs == null) {
                        if (other$proSpecs == null) {
                            break label280;
                        }
                    } else if (this$proSpecs.equals(other$proSpecs)) {
                        break label280;
                    }

                    return false;
                }

                Object this$proUnit = this.getProUnit();
                Object other$proUnit = other.getProUnit();
                if (this$proUnit == null) {
                    if (other$proUnit != null) {
                        return false;
                    }
                } else if (!this$proUnit.equals(other$proUnit)) {
                    return false;
                }

                label266: {
                    Object this$price = this.getPrice();
                    Object other$price = other.getPrice();
                    if (this$price == null) {
                        if (other$price == null) {
                            break label266;
                        }
                    } else if (this$price.equals(other$price)) {
                        break label266;
                    }

                    return false;
                }

                Object this$proRegisterNo = this.getProRegisterNo();
                Object other$proRegisterNo = other.getProRegisterNo();
                if (this$proRegisterNo == null) {
                    if (other$proRegisterNo != null) {
                        return false;
                    }
                } else if (!this$proRegisterNo.equals(other$proRegisterNo)) {
                    return false;
                }

                label252: {
                    Object this$proFactoryName = this.getProFactoryName();
                    Object other$proFactoryName = other.getProFactoryName();
                    if (this$proFactoryName == null) {
                        if (other$proFactoryName == null) {
                            break label252;
                        }
                    } else if (this$proFactoryName.equals(other$proFactoryName)) {
                        break label252;
                    }

                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                Object this$proForm = this.getProForm();
                Object other$proForm = other.getProForm();
                if (this$proForm == null) {
                    if (other$proForm != null) {
                        return false;
                    }
                } else if (!this$proForm.equals(other$proForm)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GaiaSdSaleRecipelOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gssrVoucherId = this.getGssrVoucherId();
        result = result * 59 + ($gssrVoucherId == null ? 43 : $gssrVoucherId.hashCode());
        Object $gssrType = this.getGssrType();
        result = result * 59 + ($gssrType == null ? 43 : $gssrType.hashCode());
        Object $gssrBrId = this.getGssrBrId();
        result = result * 59 + ($gssrBrId == null ? 43 : $gssrBrId.hashCode());
        Object $gssrBrName = this.getGssrBrName();
        result = result * 59 + ($gssrBrName == null ? 43 : $gssrBrName.hashCode());
        Object $gssrDate = this.getGssrDate();
        result = result * 59 + ($gssrDate == null ? 43 : $gssrDate.hashCode());
        Object $gssrTime = this.getGssrTime();
        result = result * 59 + ($gssrTime == null ? 43 : $gssrTime.hashCode());
        Object $gssrEmp = this.getGssrEmp();
        result = result * 59 + ($gssrEmp == null ? 43 : $gssrEmp.hashCode());
        Object $gssrUploadFlag = this.getGssrUploadFlag();
        result = result * 59 + ($gssrUploadFlag == null ? 43 : $gssrUploadFlag.hashCode());
        Object $gssrSaleBrId = this.getGssrSaleBrId();
        result = result * 59 + ($gssrSaleBrId == null ? 43 : $gssrSaleBrId.hashCode());
        Object $gssrSaleBrName = this.getGssrSaleBrName();
        result = result * 59 + ($gssrSaleBrName == null ? 43 : $gssrSaleBrName.hashCode());
        Object $gssrSaleDate = this.getGssrSaleDate();
        result = result * 59 + ($gssrSaleDate == null ? 43 : $gssrSaleDate.hashCode());
        Object $gssrSaleBillNo = this.getGssrSaleBillNo();
        result = result * 59 + ($gssrSaleBillNo == null ? 43 : $gssrSaleBillNo.hashCode());
        Object $gssrCustName = this.getGssrCustName();
        result = result * 59 + ($gssrCustName == null ? 43 : $gssrCustName.hashCode());
        Object $gssrCustSex = this.getGssrCustSex();
        result = result * 59 + ($gssrCustSex == null ? 43 : $gssrCustSex.hashCode());
        Object $gssrCustAge = this.getGssrCustAge();
        result = result * 59 + ($gssrCustAge == null ? 43 : $gssrCustAge.hashCode());
        Object $gssrCustIdcard = this.getGssrCustIdcard();
        result = result * 59 + ($gssrCustIdcard == null ? 43 : $gssrCustIdcard.hashCode());
        Object $gssrCustMobile = this.getGssrCustMobile();
        result = result * 59 + ($gssrCustMobile == null ? 43 : $gssrCustMobile.hashCode());
        Object $gssrPharmacistId = this.getGssrPharmacistId();
        result = result * 59 + ($gssrPharmacistId == null ? 43 : $gssrPharmacistId.hashCode());
        Object $gssrPharmacistName = this.getGssrPharmacistName();
        result = result * 59 + ($gssrPharmacistName == null ? 43 : $gssrPharmacistName.hashCode());
        Object $gssrCheckDate = this.getGssrCheckDate();
        result = result * 59 + ($gssrCheckDate == null ? 43 : $gssrCheckDate.hashCode());
        Object $gssrCheckTime = this.getGssrCheckTime();
        result = result * 59 + ($gssrCheckTime == null ? 43 : $gssrCheckTime.hashCode());
        Object $gssrCheckStatus = this.getGssrCheckStatus();
        result = result * 59 + ($gssrCheckStatus == null ? 43 : $gssrCheckStatus.hashCode());
        Object $gssrProId = this.getGssrProId();
        result = result * 59 + ($gssrProId == null ? 43 : $gssrProId.hashCode());
        Object $gssrBatchNo = this.getGssrBatchNo();
        result = result * 59 + ($gssrBatchNo == null ? 43 : $gssrBatchNo.hashCode());
        Object $gssrQty = this.getGssrQty();
        result = result * 59 + ($gssrQty == null ? 43 : $gssrQty.hashCode());
        Object $gssrRecipelId = this.getGssrRecipelId();
        result = result * 59 + ($gssrRecipelId == null ? 43 : $gssrRecipelId.hashCode());
        Object $gssrRecipelHospital = this.getGssrRecipelHospital();
        result = result * 59 + ($gssrRecipelHospital == null ? 43 : $gssrRecipelHospital.hashCode());
        Object $gssrRecipelDepartment = this.getGssrRecipelDepartment();
        result = result * 59 + ($gssrRecipelDepartment == null ? 43 : $gssrRecipelDepartment.hashCode());
        Object $gssrRecipelDoctor = this.getGssrRecipelDoctor();
        result = result * 59 + ($gssrRecipelDoctor == null ? 43 : $gssrRecipelDoctor.hashCode());
        Object $gssrSymptom = this.getGssrSymptom();
        result = result * 59 + ($gssrSymptom == null ? 43 : $gssrSymptom.hashCode());
        Object $gssrDiagnose = this.getGssrDiagnose();
        result = result * 59 + ($gssrDiagnose == null ? 43 : $gssrDiagnose.hashCode());
        Object $gssrRecipelPicId = this.getGssrRecipelPicId();
        result = result * 59 + ($gssrRecipelPicId == null ? 43 : $gssrRecipelPicId.hashCode());
        Object $gssrRecipelPicName = this.getGssrRecipelPicName();
        result = result * 59 + ($gssrRecipelPicName == null ? 43 : $gssrRecipelPicName.hashCode());
        Object $gssrRecipelPicAddress = this.getGssrRecipelPicAddress();
        result = result * 59 + ($gssrRecipelPicAddress == null ? 43 : $gssrRecipelPicAddress.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $price = this.getPrice();
        result = result * 59 + ($price == null ? 43 : $price.hashCode());
        Object $proRegisterNo = this.getProRegisterNo();
        result = result * 59 + ($proRegisterNo == null ? 43 : $proRegisterNo.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        return result;
    }

    public String toString() {
        return "GaiaSdSaleRecipelOutData(clientId=" + this.getClientId() + ", gssrVoucherId=" + this.getGssrVoucherId() + ", gssrType=" + this.getGssrType() + ", gssrBrId=" + this.getGssrBrId() + ", gssrBrName=" + this.getGssrBrName() + ", gssrDate=" + this.getGssrDate() + ", gssrTime=" + this.getGssrTime() + ", gssrEmp=" + this.getGssrEmp() + ", gssrUploadFlag=" + this.getGssrUploadFlag() + ", gssrSaleBrId=" + this.getGssrSaleBrId() + ", gssrSaleBrName=" + this.getGssrSaleBrName() + ", gssrSaleDate=" + this.getGssrSaleDate() + ", gssrSaleBillNo=" + this.getGssrSaleBillNo() + ", gssrCustName=" + this.getGssrCustName() + ", gssrCustSex=" + this.getGssrCustSex() + ", gssrCustAge=" + this.getGssrCustAge() + ", gssrCustIdcard=" + this.getGssrCustIdcard() + ", gssrCustMobile=" + this.getGssrCustMobile() + ", gssrPharmacistId=" + this.getGssrPharmacistId() + ", gssrPharmacistName=" + this.getGssrPharmacistName() + ", gssrCheckDate=" + this.getGssrCheckDate() + ", gssrCheckTime=" + this.getGssrCheckTime() + ", gssrCheckStatus=" + this.getGssrCheckStatus() + ", gssrProId=" + this.getGssrProId() + ", gssrBatchNo=" + this.getGssrBatchNo() + ", gssrQty=" + this.getGssrQty() + ", gssrRecipelId=" + this.getGssrRecipelId() + ", gssrRecipelHospital=" + this.getGssrRecipelHospital() + ", gssrRecipelDepartment=" + this.getGssrRecipelDepartment() + ", gssrRecipelDoctor=" + this.getGssrRecipelDoctor() + ", gssrSymptom=" + this.getGssrSymptom() + ", gssrDiagnose=" + this.getGssrDiagnose() + ", gssrRecipelPicId=" + this.getGssrRecipelPicId() + ", gssrRecipelPicName=" + this.getGssrRecipelPicName() + ", gssrRecipelPicAddress=" + this.getGssrRecipelPicAddress() + ", index=" + this.getIndex() + ", proName=" + this.getProName() + ", proSpecs=" + this.getProSpecs() + ", proUnit=" + this.getProUnit() + ", price=" + this.getPrice() + ", proRegisterNo=" + this.getProRegisterNo() + ", proFactoryName=" + this.getProFactoryName() + ", proPlace=" + this.getProPlace() + ", proForm=" + this.getProForm() + ")";
    }
}
