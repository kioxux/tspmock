package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@ApiModel(value = "月销售任务传参", description = "")
@Data
public class SalesTaskData {
    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "任务月份")
    private String monthPlan;

    @ApiModelProperty(value = "任务id")
    private String staskTaskId;

    @ApiModelProperty(value = "任务名称")
    private String staskTaskName;

    @ApiModelProperty(value = "同比月份")
    @NotEmpty(message = "同比月份不可为空!")
    private String yoyMonth;

    @ApiModelProperty(value = "环比月份")
    @NotNull(message = "环比月份不可为空!")
    private String momMonth;

    @ApiModelProperty(value = "营业额同比增长率")
    @NotNull(message = "营业额同比增长率不可为空!")
    private String yoySaleAmt;

    @ApiModelProperty(value = "营业额环比增长率")
    @NotNull(message = "营业额环比增长率不可为空!")
    private String momSaleAmt;


    @ApiModelProperty(value = "毛利额同比增长率")
    @NotNull(message = "毛利额同比增长率不可为空!")
    private String yoySaleGross;

    @ApiModelProperty(value = "毛利额环比增长率")
    @NotNull(message = "毛利额环比增长率不可为空!")
    private String momSaleGross;

    @ApiModelProperty(value = "会员卡同比增长率")
    @NotNull(message = "会员卡同比增长率不可为空!")
    private String yoyMcardQty;

    @ApiModelProperty(value = "会员卡环比增长率")
    @NotNull(message = "会员卡环比增长率不可为空!")
    private String momMcardQty;

    List<MonthOutDataVo> monthOutDataVo;//vo对象
}
