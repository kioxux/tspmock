package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdBatchChange;

public interface GaiaSdBatchChangeService {
    /**
     * 新增
     * @param batchChange
     */
    void insert(GaiaSdBatchChange batchChange);
}
