package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaAnnualTrainingPlanMapper;
import com.gys.business.mapper.entity.GaiaAnnualTrainingPlan;
import com.gys.business.service.AnnualTrainingPlanService;
import com.gys.business.service.data.AnnualTrainingPlanInData;
import com.gys.business.service.data.AnnualTrainingPlanOutData;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2022/1/3 18:38
 */
@Slf4j
@Service
public class AnnualTrainingPlanServiceImpl implements AnnualTrainingPlanService {
    @Resource
    private GaiaAnnualTrainingPlanMapper annualTrainingPlanMapper;
    @Resource
    private CosUtils cosUtils;

    @Override
    public void saveAnnualTrainingPlan(List<GaiaAnnualTrainingPlan> plan, String client, String user) {
        for (GaiaAnnualTrainingPlan annualTrainingPlan : plan){
            GaiaAnnualTrainingPlan trainingPlan = new GaiaAnnualTrainingPlan();
            if (StringUtils.isBlank(annualTrainingPlan.getGatpVoucherId())){
                //获取单号
                String voucherId = annualTrainingPlanMapper.getNextGatpVoucherId(client);
                BeanUtils.copyProperties(annualTrainingPlan,trainingPlan);
                trainingPlan.setGatpVoucherId(voucherId);
                trainingPlan.setClient(client);
                annualTrainingPlanMapper.insertAnnualTrainingPlan(trainingPlan);
            }else {
                //获取ID
                Long id = annualTrainingPlanMapper.getIdByClientAndVoucherId(client,annualTrainingPlan.getGatpVoucherId());
                annualTrainingPlan.setId(id);
                BeanUtils.copyProperties(annualTrainingPlan,trainingPlan);
                trainingPlan.setGatpUpdate(user);
                trainingPlan.setGatpUpdateDate(new Date());
                annualTrainingPlanMapper.updateAnnualTrainingPlan(trainingPlan);
            }
        }
    }

    @Override
    public List<AnnualTrainingPlanOutData> getAnnualTrainingPlanList(AnnualTrainingPlanInData inData) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            if (StringUtils.isNotBlank(inData.getGatpTrainingStartDate())){
                Date startDate;
                String str = inData.getGatpTrainingStartDate()+"000000";
                startDate = simpleDateFormat.parse(str);
                inData.setStartDate(startDate);
            }
            if (StringUtils.isNotBlank(inData.getGatpTrainingEndDate())){
                Date endDate;
                String str = inData.getGatpTrainingEndDate()+"235959";
                endDate = simpleDateFormat.parse(str);
                inData.setEndDate(endDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return annualTrainingPlanMapper.getAnnualTrainingPlanList(inData);
    }

    @Override
    public void deleteAnnualTrainingPlan(List<GaiaAnnualTrainingPlan> plan, String client) {
        for (GaiaAnnualTrainingPlan annualTrainingPlan : plan){
            Long id = annualTrainingPlanMapper.getIdByClientAndVoucherId(client,annualTrainingPlan.getGatpVoucherId());
            annualTrainingPlanMapper.deletePlanById(id);
        }
    }

    @Override
    public Result exportAnnualTrainingPlanList(AnnualTrainingPlanInData inData) {
        Result result;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            if (StringUtils.isNotBlank(inData.getGatpTrainingStartDate())){
                Date startDate;
                String str = inData.getGatpTrainingStartDate()+"000000";
                startDate = simpleDateFormat.parse(str);
                inData.setStartDate(startDate);
            }
            if (StringUtils.isNotBlank(inData.getGatpTrainingEndDate())){
                Date endDate;
                String str = inData.getGatpTrainingEndDate()+"235959";
                endDate = simpleDateFormat.parse(str);
                inData.setEndDate(endDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<AnnualTrainingPlanOutData> list = annualTrainingPlanMapper.getAnnualTrainingPlanList(inData);
        String name = "年度培训计划导出";
        if (list.size() > 0){
            CsvFileInfo csvFileInfo = CsvClient.getCsvByte(list,name, Collections.singletonList((short) 1));
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                assert csvFileInfo != null;
                outputStream.write(csvFileInfo.getFileContent());
                result = cosUtils.uploadFile(outputStream, csvFileInfo.getFileName());
            } catch (IOException e){
                log.error("导出文件失败:{}",e.getMessage(), e);
                throw new BusinessException("导出文件失败！");
            } finally {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    log.error("关闭流异常：{}",e.getMessage(),e);
                    throw new BusinessException("关闭流异常！");
                }
            }
            return result;
        }else{
            throw new BusinessException("提示：数据为空！");
        }
    }
}
