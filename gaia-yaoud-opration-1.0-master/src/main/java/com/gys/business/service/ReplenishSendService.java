package com.gys.business.service;

import com.gys.business.service.data.GetPlaceOrderInData;
import com.gys.business.service.data.GetReplenishDetailOutData;
import com.gys.business.service.data.GetReplenishInData;
import com.gys.business.service.data.RatioProInData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

public interface ReplenishSendService {
    Map<String, Object> getProDetailList(GetReplenishInData inData, GetLoginOutData userInfo);

    List<GetReplenishDetailOutData> getReplenishDetailList(GetReplenishInData inData,GetLoginOutData userInfo);

    List<GetReplenishInData> placeOrder(GetPlaceOrderInData inData,GetLoginOutData userInfo);

    List<GetReplenishDetailOutData> getImportExcelDetailList(GetPlaceOrderInData inData);

    String addSendRepPrice(RatioProInData inData);

    Map<String, Object> selectSendRatioStatus(RatioProInData inData);
}
