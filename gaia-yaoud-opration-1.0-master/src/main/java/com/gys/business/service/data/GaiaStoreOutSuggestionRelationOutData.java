package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class GaiaStoreOutSuggestionRelationOutData implements Serializable {
    private static final long serialVersionUID = 504188157994966306L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 商品调出单号
     */
    private String billCode;
    /**
     * 调出门店
     */
    private String outStoCode;
    /**
     * 调入门店
     */
    private String inStoCode;
    /**
     * 调入店名
     */
    private String inStoName;
    /**
     * 序号
     */
    private Integer sort;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;

    private int status;

}

