package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.service.data.percentageplan.*;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

public interface PercentagePlanV5Service {
    /**
     * 提成方案基础设置保存/更新
     * @param inData
     * @return
     */
    Integer basicInsert(PercentageBasicInData inData);

    PageInfo<PercentageOutData> list(PercentageSearchInData inData,GetLoginOutData userInfo);

    Map<String,Object> tichengDetail(Long planId, String type,GetLoginOutData userInfo);

    void approve(Long planId, String planStatus, String type);

    void deletePlan(Long planId, String type);

    PercentageProInData selectProductByClient(String client, String proCode);

    List<PercentageProInData> selectProductByClientProCodes(String client, List<String> proCode);

    List<PercentageProInData> getImportExcelDetailList(String clientId, List<ImportPercentageData> importInDataList);

    List<Map<String,String>>selectStoList(Long planId, Long type);

    Map<String, Object> tichengDetailCopy(Long planId, Long type);

    void stopPlan(Long planId, String planType, String stopType, String type);

    void timerStopPlan();

    SaleSettingOutData saleSettingInsert(SaleSettingInData inData, GetLoginOutData userInfo);

    ProSettingOutData proSettingInsert(ProSettingInData inData, GetLoginOutData userInfo);

    List<PercentageProInData> getPlanRejectProDetailList(String clientId, List<String> proCodes);

    void deleteSetting(Long id, String planType);

    /**
     * type B 表示后端权限 S表示门店权限
     * @param userInfo
     * @param type
     * @return
     */
    boolean checkPermissionLeagal(GetLoginOutData userInfo,String type);

    void syncData();

    Map<String, Object> basicInit(GetLoginOutData userInfo);
}
