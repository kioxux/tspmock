package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class OutOfStockInData implements Serializable {

    private static final long serialVersionUID = -3781934088499015325L;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "门店编码")
    private String brId;

    @ApiModelProperty(value = "查询日期")
    private String queryDate;

    //@ApiModelProperty(value = "月均")
    //private Integer monthAvg;

    @ApiModelProperty(value = "配置查询天数")
    private Integer days;

    @ApiModelProperty(value = "缺断货商品数据加入补货计划追溯时间")
    private Integer fillDays;

    @ApiModelProperty(value = "排序字段 1-销售额 2-毛利额 3-销量")
    private Integer orderByFlag;

    @ApiModelProperty(value = "排序字段 1-前 2-后")
    private Integer topOrLast;

    @ApiModelProperty(value = "显示条数")
    private Integer pageSize;

    @ApiModelProperty(value = "用户编号")
    private String userId;

    @ApiModelProperty(value = "门店类型 null-所有店 1-直营店 2-加盟店 3-其他")
    private String storeType;

    private List<ProductRank> rankList;

    /**
     * 直营店列表
     */
    private List<String> directStoreList;
}
