package com.gys.business.service.data.disease;

import lombok.Data;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/7 15:34
 * @Version 1.0.0
 **/
@Data
public class SchemeBO {

    //  疾病编码
    private String diseaseCode;
    //  方案代码
    private String schemeCode;

    private List<RecommendProductBO> auxiliaryMedications;
}
