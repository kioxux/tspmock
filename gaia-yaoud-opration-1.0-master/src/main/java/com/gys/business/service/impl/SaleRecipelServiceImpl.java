//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaClSystemParaMapper;
import com.gys.business.mapper.GaiaSdSaleRecipelMapper;
import com.gys.business.mapper.GaiaSdSystemParaMapper;
import com.gys.business.mapper.entity.GaiaSdSaleRecipel;
import com.gys.business.service.SaleRecipelService;
import com.gys.business.service.data.*;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class SaleRecipelServiceImpl implements SaleRecipelService {
    @Autowired
    private GaiaSdSaleRecipelMapper gaiaSdSaleRecipelMapper;
    @Resource
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;
    @Resource
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;


    public SaleRecipelServiceImpl() {
    }

    public PageInfo<GaiaSdSaleRecipelOutData> getAuditedSaleRecipelList(GaiaSdSaleRecipelInData inData) {

        if (ObjectUtil.isEmpty(inData.getBrIdList()) || inData.getBrIdList().length < 0) {
            throw new BusinessException("请输入门店");
        }

        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GaiaSdSaleRecipelOutData> recipelList = this.gaiaSdSaleRecipelMapper.getAuditedSaleRecipelList(inData);
        //参数判断和值设置
        setValue(inData, recipelList);
        PageInfo pageInfo;
        if (CollectionUtils.isEmpty(recipelList)) {
            pageInfo = new PageInfo(new ArrayList());
        } else {
            for (int i = 0; i < recipelList.size(); ++i) {
                (recipelList.get(i)).setIndex(i + 1);
                if (StrUtil.isBlank((recipelList.get(i)).getGssrRecipelPicAddress())) {
                }
            }
            pageInfo = new PageInfo(recipelList);
        }
        return pageInfo;
    }

    //判断处方登记参数、权限参数、是否上传了处方图片
    public void setValue(GaiaSdSaleRecipelInData inData, List<GaiaSdSaleRecipelOutData> list) {
        //判断当前加盟商是否需要双规处方登记
        GaiaClSystemPara gaiaClSystemPara = gaiaClSystemParaMapper.getParaByClientAndParaId(inData.getClient(), "RECISALE_FLAG");
        if (ObjectUtil.isNotNull(gaiaClSystemPara) && ObjectUtil.isNotEmpty(gaiaClSystemPara)) {
            String param = gaiaClSystemPara.getGcspPara1();
            if ("1".equals(param)) {
                for (GaiaSdSaleRecipelOutData outData : list) {
                    outData.setRecisaleFlag("1");
                }
            }
        }
        //判断当前门店是否有权限在处方登记查询处更新处方图片
        String isUpload = gaiaSdSystemParaMapper.getParaByClientAndStoCode(inData.getClient(), inData.getStoCode(), "UPLOAD_PRESCRIPTION_PICTURE");
        if ("1".equals(isUpload)) {
            for (GaiaSdSaleRecipelOutData outData : list) {
                outData.setIsUpload("1");
            }
        }
        //查看单子是否有图片
        for (GaiaSdSaleRecipelOutData outData : list) {
            if (StringUtils.isNotBlank(outData.getGssrRecipelPicAddress())) {
                outData.setIsPicture("1");
            }
        }
        //判断当前门店是否有权限在处方登记查询处补登记处方图片
        String patchUpload = gaiaSdSystemParaMapper.getParaByClientAndStoCode(inData.getClient(), inData.getStoCode(), "RX_PIC_ADD");
        if ("1".equals(patchUpload)) {
            for (GaiaSdSaleRecipelOutData outData : list) {
                outData.setPatchUpload("1");
            }
        }
    }

    @Override
    public List<SalePecipelProductOutData> getProductInfo(SalePecipelProductInData inData) {
        if (StringUtils.isBlank(inData.getGssrSaleBillNo())){
            throw new BusinessException("提示：销售单号为空！");
        }
        return gaiaSdSaleRecipelMapper.getProductByClientAndBillNo(inData.getClient(),inData.getGssrSaleBillNo());
    }

    public PageInfo<GaiaSdSaleRecipelRecordOutData> getSaleRecipelRecordList(GaiaSdSaleRecipelRecordInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GaiaSdSaleRecipelRecordOutData> recipelRecordList = this.gaiaSdSaleRecipelMapper.getSaleRecipeRecordlList(inData);
        PageInfo pageInfo;
        if (CollectionUtils.isEmpty(recipelRecordList)) {
            pageInfo = new PageInfo(new ArrayList());
        } else {
            for (int i = 0; i < recipelRecordList.size(); ++i) {
                (recipelRecordList.get(i)).setIndex(i + 1);
            }

            pageInfo = new PageInfo(recipelRecordList);
        }

        return pageInfo;
    }

    @Transactional
    public void addOrEditRecipelRecord(List<GaiaSdSaleRecipelRecordInData> inDataList) {
        for (int i = 0; i < inDataList.size(); ++i) {
            GaiaSdSaleRecipelRecordInData inData = inDataList.get(i);
            GaiaSdSaleRecipel out = this.gaiaSdSaleRecipelMapper.selectByPrimaryKey(inData);
            GaiaSdSaleRecipel gaiaSdSaleRecipel = new GaiaSdSaleRecipel();
            if (ObjectUtil.isNotEmpty(out)) {
                BeanUtils.copyProperties(inData, gaiaSdSaleRecipel);
                this.gaiaSdSaleRecipelMapper.updateByPrimaryKey(gaiaSdSaleRecipel);
            } else {
                inData.setGssrVoucherId(this.gaiaSdSaleRecipelMapper.selectNextVoucherId());
                BeanUtils.copyProperties(inData, gaiaSdSaleRecipel);
                this.gaiaSdSaleRecipelMapper.insert(gaiaSdSaleRecipel);
            }
        }

    }

    @Transactional
    public void auditRecipelRecord(List<GaiaSdSaleRecipelRecordInData> inDataList) {
        for (int i = 0; i < inDataList.size(); ++i) {
            GaiaSdSaleRecipelRecordInData inData = (GaiaSdSaleRecipelRecordInData) inDataList.get(i);
            GaiaSdSaleRecipel out = (GaiaSdSaleRecipel) this.gaiaSdSaleRecipelMapper.selectByPrimaryKey(inData);
            if (!ObjectUtil.isNotEmpty(out)) {
                throw new BusinessException(StrUtil.format("该单价暂未生成处方记录！", new Object[]{out.getGssrVoucherId()}));
            }

            GaiaSdSaleRecipel gaiaSdSaleRecipel = new GaiaSdSaleRecipel();
            BeanUtils.copyProperties(inData, gaiaSdSaleRecipel);
            this.gaiaSdSaleRecipelMapper.updateByPrimaryKey(gaiaSdSaleRecipel);
        }

    }

    @Override
    public List<ControlledDrugsOutData> getControlledDrugsManage(ControlledDrugsInData inData) {
        if (StringUtils.isNotEmpty(inData.getGssdDate())) {
            inData.setGssdDate(CommonUtil.parseWebDate(inData.getGssdDate()));
        }
        if (StringUtils.isNotEmpty(inData.getGssdSpecialmedBirthday())) {
            inData.setGssdSpecialmedBirthday(CommonUtil.parseWebDate(inData.getGssdSpecialmedBirthday()));
        }
        List<ControlledDrugsOutData> controlledDrugsOutData = this.gaiaSdSaleRecipelMapper.getControlledDrugsManage(inData);
        if (ObjectUtil.isEmpty(controlledDrugsOutData)) {
            controlledDrugsOutData = new ArrayList<>();
        }
        return controlledDrugsOutData;
    }

    @Override
    public int updatePicture(SaleRecipelPictureInData inData) {
        if (StringUtils.isBlank(inData.getGssrSaleBillNo())){
            throw new BusinessException("提示：销售单号不能为空！");
        }
        if (StringUtils.isBlank(inData.getPictureUrl())){
            throw new BusinessException("提示：图片地址不能为空！");
        }
        int count = 0;
        //查询主键
        List<String> IdList = gaiaSdSaleRecipelMapper.getVoucherId(inData.getClient(),inData.getGssrSaleBillNo());
        //更新图片路径
        if (CollectionUtils.isNotEmpty(IdList)){
            for (String gssrVoucherId : IdList){
                gaiaSdSaleRecipelMapper.savePictureUrl(inData.getPictureUrl(),inData.getClient(),gssrVoucherId);
                count++;
            }
        }
        return count;
    }
}
