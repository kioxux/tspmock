package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.PromoteService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.CopyUtil;
import com.gys.util.PromUtil;
import com.gys.util.UtilMessage;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PromoteServiceImpl implements PromoteService {
    @Autowired
    private GaiaSdSaleHMapper saleHMapper;
    @Autowired
    private GaiaSdSaleDMapper saleDMapper;
    @Autowired
    private GaiaSdPromHeadMapper promHeadMapper;
    @Autowired
    private GaiaSdPromParaMapper promParaMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdStoresGroupMapper sdStoresGroupMapper;
    @Autowired
    private GaiaSdPromUnitarySetMapper sdPromUnitarySetMapper;
    @Autowired
    private GaiaSdPromSeriesCondsMapper sdPromSeriesCondsMapper;
    @Autowired
    private GaiaSdPromSeriesSetMapper sdPromSeriesSetMapper;
    @Autowired
    private GaiaSdPromGiftCondsMapper sdPromGiftCondsMapper;
    @Autowired
    private GaiaSdPromGiftSetMapper sdPromGiftSetMapper;
    @Autowired
    private GaiaSdPromGiftResultMapper sdPromGiftResultMapper;
    @Autowired
    private GaiaSdPromCouponSetMapper sdPromCouponSetMapper;
    @Autowired
    private GaiaSdPromCouponGrantMapper sdPromCouponGrantMapper;
    @Autowired
    private GaiaSdPromCouponUseMapper sdPromCouponUseMapper;
    @Autowired
    private GaiaSdPromHySetMapper sdPromHySetMapper;
    @Autowired
    private GaiaSdPromHyrDiscountMapper sdPromHyrDiscountMapper;
    @Autowired
    private GaiaSdPromHyrPriceMapper sdPromHyrPriceMapper;
    @Autowired
    private GaiaSdPromAssoCondsMapper sdPromAssoCondsMapper;
    @Autowired
    private GaiaSdPromAssoSetMapper sdPromAssoSetMapper;
    @Autowired
    private GaiaSdPromAssoResultMapper sdPromAssoResultMapper;

    @Override
    public PageInfo<PromoteOutData> getList(PromoteInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<PromoteOutData> outData = this.promHeadMapper.getList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PromoteInData inData, GetLoginOutData userInfo) {
        Example example = new Example(GaiaSdPromHead.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andEqualTo("gsphVoucherId", inData.getGsphVoucherId());
        List<GaiaSdPromHead> promHead = this.promHeadMapper.selectByExample(example);
        for (GaiaSdPromHead sdPromHead : promHead) {
            sdPromHead.setGsphBeginDate(DateUtil.format(DateUtil.parse(inData.getGsphBeginDate(), "yyyy-MM-dd"), "yyyyMMdd"));
            sdPromHead.setGsphEndDate(DateUtil.format(DateUtil.parse(inData.getGsphEndDate(), "yyyy-MM-dd"), "yyyyMMdd"));
            sdPromHead.setGsphBeginTime(DateUtil.format(DateUtil.parse(inData.getGsphBeginTime(), "HH:mm:ss"), "HHmmss"));
            sdPromHead.setGsphEndTime(DateUtil.format(DateUtil.parse(inData.getGsphEndTime(), "HH:mm:ss"), "HHmmss"));
            this.promHeadMapper.updateByPrimaryKeySelective(sdPromHead);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insert(PromoteInData inData, GetLoginOutData userInfo) {
        List<String> storeIds = new ArrayList();
        Example example;
        List<GaiaStoreData> list;
        if ("1".equals(inData.getStoreType())) {
            example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("stoStatus", "0").andGreaterThanOrEqualTo("stoCode", inData.getStoreStart()).andLessThanOrEqualTo("stoCode", inData.getStoreEnd());
            list = this.storeDataMapper.selectByExample(example);
            storeIds = list.stream().map(GaiaStoreData::getStoCode).collect(Collectors.toList());
        } else if ("2".equals(inData.getStoreType())) {
            example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("stoStatus", "0").andEqualTo("stogCode", inData.getStoreGroup());
            list = this.storeDataMapper.selectByExample(example);
            storeIds = list.stream().map(GaiaStoreData::getStoCode).collect(Collectors.toList());
        } else if ("3".equals(inData.getStoreType())) {
            example = new Example(GaiaStoreData.class);
            example.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("stoStatus", "0").andIn("stoCode", inData.getStoreIds());
            list = this.storeDataMapper.selectByExample(example);
            storeIds = list.stream().map(GaiaStoreData::getStoCode).collect(Collectors.toList());
        }

        if (ObjectUtil.isEmpty(storeIds)) {
            throw new BusinessException(UtilMessage.NOT_MATCHED_TO_THE_CORRESPONDING_STORE);
        } else {
            String voucherId = this.promHeadMapper.selectMaxId(userInfo.getClient());
            List<GaiaSdPromHead> dataList = new ArrayList<>();
            for (String storeId : storeIds) {
                GaiaSdPromHead promHead = new GaiaSdPromHead();
                promHead.setClientId(userInfo.getClient());
                promHead.setGsphVoucherId(voucherId);
                promHead.setGsphBrId(storeId);
                promHead.setGsphName(inData.getGsphName());
                promHead.setGsphTheme(inData.getGsphTheme());
                promHead.setGsphType(inData.getGsphType());
                promHead.setGsphDateFrequency(inData.getGsphDateFrequency());
                promHead.setGsphWeekFrequency(inData.getGsphWeekFrequency());
                promHead.setGsphStatus(UtilMessage.ZERO);
                promHead.setGsphPart(inData.getGsphPart());
                promHead.setGsphRemarks(inData.getGsphRemarks());
                promHead.setGsphPara1(inData.getGsphPara1());
                promHead.setGsphPara2(inData.getGsphPara2());
                promHead.setGsphPara3(inData.getGsphPara3());
                promHead.setGsphPara4(inData.getGsphPara4());
                promHead.setGsphExclusion(inData.getGsphExclusion());
                promHead.setGsphBeginDate(DateUtil.format(DateUtil.parse(inData.getGsphBeginDate(), "yyyy-MM-dd"), "yyyyMMdd"));
                promHead.setGsphEndDate(DateUtil.format(DateUtil.parse(inData.getGsphEndDate(), "yyyy-MM-dd"), "yyyyMMdd"));
                promHead.setGsphBeginTime(DateUtil.format(DateUtil.parse(inData.getGsphBeginTime(), "HH:mm:ss"), "HHmmss"));
                promHead.setGsphEndTime(DateUtil.format(DateUtil.parse(inData.getGsphEndTime(), "HH:mm:ss"), "HHmmss"));
                dataList.add(promHead);
            }
            this.promHeadMapper.insertToList(dataList);
            //单品
            if (UtilMessage.PROM_SINGLE.equals(inData.getGsphType())) {
                if (CollUtil.isEmpty(inData.getPromUnitarySetInDataList())) {
                    throw new BusinessException(UtilMessage.SELECT_PRODUCT_INFORMATION);
                }
                //单品表保存
                this.savePromUnitarySet(userInfo.getClient(), voucherId, inData.getPromUnitarySetInDataList(), false);
            }

            //系列
            if (UtilMessage.PROM_SERIES.equals(inData.getGsphType())) {
                if (CollUtil.isEmpty(inData.getPromSeriesCondsInDataList()) || CollUtil.isEmpty(inData.getPromSeriesSetInDataList())) {
                    throw new BusinessException(UtilMessage.SERIES_SETTING_CANNOT_BE_EMPTY);
                }
                if (CollUtil.isNotEmpty(inData.getPromSeriesCondsInDataList()) && CollUtil.isNotEmpty(inData.getPromSeriesSetInDataList())) {
                    //条件设置
                    this.savePromSeriesConds(userInfo.getClient(), voucherId, inData.getPromSeriesCondsInDataList(), false);
                    //编码设置
                    this.savePromSeriesSet(userInfo.getClient(), voucherId, inData.getPromSeriesSetInDataList(), false);
                }

            }

            if (UtilMessage.PROM_GIFT.equals(inData.getGsphType())) {
                if (ObjectUtil.isNotEmpty(inData.getPromGiftCondsInDataList())) {
                    this.savePromGiftConds(userInfo.getClient(), voucherId, inData.getPromGiftCondsInDataList(), false);
                }

                if (ObjectUtil.isNotEmpty(inData.getPromGiftSetInDataList())) {
                    this.savePromGiftSet(userInfo.getClient(), voucherId, inData.getPromGiftSetInDataList(), false);
                }

                if (ObjectUtil.isNotEmpty(inData.getPromGiftResultInDataList())) {
                    this.savePromGiftResult(userInfo.getClient(), voucherId, inData.getPromGiftResultInDataList(), false);
                }
            }

            if (UtilMessage.PROM_COUPON.equals(inData.getGsphType())) {
                if (ObjectUtil.isNotEmpty(inData.getPromCouponSetInDataList())) {
                    this.savePromCouponSet(userInfo.getClient(), voucherId, inData.getPromCouponSetInDataList(), false);
                }

                if (ObjectUtil.isNotEmpty(inData.getPromCouponGrantInDataList())) {
                    this.savePromCouponGrant(userInfo.getClient(), voucherId, inData.getPromCouponGrantInDataList(), false);
                }

                if (ObjectUtil.isNotEmpty(inData.getPromCouponUseInDataList())) {
                    this.savePromCouponUse(userInfo.getClient(), voucherId, inData.getPromCouponUseInDataList(), false);
                }
            }

            if (UtilMessage.PROM_HY.equals(inData.getGsphType())) {
                if (ObjectUtil.isNotEmpty(inData.getPromHySetInDataList())) {
                    this.savePromHySet(userInfo.getClient(), voucherId, inData.getPromHySetInDataList(), false);
                }

                if (ObjectUtil.isNotEmpty(inData.getPromHyrDiscountInDataList())) {
                    this.savePromHyrDiscount(userInfo.getClient(), voucherId, inData.getPromHyrDiscountInDataList(), false);
                }

                if (ObjectUtil.isNotEmpty(inData.getPromHyrPriceInDataList())) {
                    this.savePromHyrPrice(userInfo.getClient(), voucherId, inData.getPromHyrPriceInDataList(), false);
                }
            }

            if (UtilMessage.PROM_COMBIN.equals(inData.getGsphType())) {
                if (ObjectUtil.isNotEmpty(inData.getPromAssoCondsInDataList())) {
                    this.savePromAssoConds(userInfo.getClient(), voucherId, inData.getPromAssoCondsInDataList(), false);
                }

                if (ObjectUtil.isNotEmpty(inData.getPromAssoSetInDataList())) {
                    this.savePromAssoSet(userInfo.getClient(), voucherId, inData.getPromAssoSetInDataList(), false);
                }

                if (ObjectUtil.isNotEmpty(inData.getPromAssoResultInDataList())) {
                    this.savePromAssoResult(userInfo.getClient(), voucherId, inData.getPromAssoResultInDataList(), false);
                }
            }

        }
    }

    @Override
    public PromoteOutData detail(PromoteInData inData) {
        PromoteOutData outData = new PromoteOutData();
        Example example = new Example(GaiaSdPromHead.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsphVoucherId", inData.getGsphVoucherId());
        List<GaiaSdPromHead> list = this.promHeadMapper.selectByExample(example);
        List<String> storeIds = list.stream().map(GaiaSdPromHead::getGsphBrId).collect(Collectors.toList());

        outData.setStoreList(storeIds);
        GaiaSdPromHead object = (GaiaSdPromHead) list.get(0);
        outData.setClientId(object.getClientId());
        outData.setGsphVoucherId(object.getGsphVoucherId());
        outData.setGsphName(object.getGsphName());
        outData.setGsphTheme(object.getGsphTheme());
        outData.setGsphType(object.getGsphType());
        outData.setGsphDateFrequency(object.getGsphDateFrequency());
        outData.setGsphWeekFrequency(object.getGsphWeekFrequency());
        outData.setGsphStatus(object.getGsphStatus());
        outData.setGsphPart(object.getGsphPart());
        outData.setGsphRemarks(object.getGsphRemarks());
        outData.setGsphPara1(object.getGsphPara1());
        outData.setGsphPara2(object.getGsphPara2());
        outData.setGsphPara3(object.getGsphPara3());
        outData.setGsphPara4(object.getGsphPara4());
        outData.setGsphExclusion(object.getGsphExclusion());
        if (ObjectUtil.isNotEmpty(object.getGsphBeginDate())) {
            outData.setGsphBeginDate(DateUtil.format(DateUtil.parse(object.getGsphBeginDate(), "yyyyMMdd"), "yyyy-MM-dd"));
        }

        if (ObjectUtil.isNotEmpty(object.getGsphEndDate())) {
            outData.setGsphEndDate(DateUtil.format(DateUtil.parse(object.getGsphEndDate(), "yyyyMMdd"), "yyyy-MM-dd"));
        }

        if (ObjectUtil.isNotEmpty(object.getGsphBeginTime())) {
            outData.setGsphBeginTime(DateUtil.format(DateUtil.parse(object.getGsphBeginTime(), "HHmmss"), "HH:mm:ss"));
        }

        if (ObjectUtil.isNotEmpty(object.getGsphEndTime())) {
            outData.setGsphEndTime(DateUtil.format(DateUtil.parse(object.getGsphEndTime(), "HHmmss"), "HH:mm:ss"));
        }

        List promAssoCondsOutDataList;
        if ("PROM_SINGLE".equals(object.getGsphType())) {
            promAssoCondsOutDataList = this.sdPromUnitarySetMapper.getDetail(inData);
            outData.setPromUnitarySetInDataList(promAssoCondsOutDataList);
        }

        List promAssoSetOutDataList;
        if ("PROM_SERIES".equals(object.getGsphType())) {
            promAssoCondsOutDataList = this.sdPromSeriesCondsMapper.getDetail(inData);
            outData.setPromSeriesCondsInDataList(promAssoCondsOutDataList);
            promAssoSetOutDataList = this.sdPromSeriesSetMapper.getDetail(inData);
            outData.setPromSeriesSetInDataList(promAssoSetOutDataList);
        }

        List promAssoResultOutDataList;
        if ("PROM_GIFT".equals(object.getGsphType())) {
            promAssoCondsOutDataList = this.sdPromGiftCondsMapper.getDetail(inData);
            outData.setPromGiftCondsInDataList(promAssoCondsOutDataList);
            promAssoSetOutDataList = this.sdPromGiftSetMapper.getDetail(inData);
            outData.setPromGiftSetInDataList(promAssoSetOutDataList);
            promAssoResultOutDataList = this.sdPromGiftResultMapper.getDetail(inData);
            outData.setPromGiftResultInDataList(promAssoResultOutDataList);
        }

        if ("PROM_COUPON".equals(object.getGsphType())) {
            promAssoCondsOutDataList = this.sdPromCouponSetMapper.getDetail(inData);
            outData.setPromCouponSetInDataList(promAssoCondsOutDataList);
            promAssoSetOutDataList = this.sdPromCouponGrantMapper.getDetail(inData);
            outData.setPromCouponGrantInDataList(promAssoSetOutDataList);
            promAssoResultOutDataList = this.sdPromCouponUseMapper.getDetail(inData);
            outData.setPromCouponUseInDataList(promAssoResultOutDataList);
        }

        if ("PROM_HY".equals(object.getGsphType())) {
            promAssoCondsOutDataList = this.sdPromHySetMapper.getDetail(inData);
            outData.setPromHySetInDataList(promAssoCondsOutDataList);
            promAssoSetOutDataList = this.sdPromHyrDiscountMapper.getDetail(inData);
            outData.setPromHyrDiscountInDataList(promAssoSetOutDataList);
            promAssoResultOutDataList = this.sdPromHyrPriceMapper.getDetail(inData);
            outData.setPromHyrPriceInDataList(promAssoResultOutDataList);
        }

        if ("PROM_COMBIN".equals(object.getGsphType())) {
            promAssoCondsOutDataList = this.sdPromAssoCondsMapper.getDetail(inData);
            outData.setPromAssoCondsInDataList(promAssoCondsOutDataList);
            promAssoSetOutDataList = this.sdPromAssoSetMapper.getDetail(inData);
            outData.setPromAssoSetInDataList(promAssoSetOutDataList);
            promAssoResultOutDataList = this.sdPromAssoResultMapper.getDetail(inData);
            outData.setPromAssoResultInDataList(promAssoResultOutDataList);
        }

        return outData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approve(PromoteInData inData, GetLoginOutData userInfo) {
        Example example = new Example(GaiaSdPromHead.class);
        example.createCriteria().andEqualTo("clientId", userInfo.getClient()).andIn("gsphVoucherId", inData.getIdList());
        List<GaiaSdPromHead> promHeadList = this.promHeadMapper.selectByExample(example);
        Iterator var5 = promHeadList.iterator();

        GaiaSdPromHead promHead;
        while (var5.hasNext()) {
            promHead = (GaiaSdPromHead) var5.next();
            if (!"0".equals(promHead.getGsphStatus())) {
                throw new BusinessException("存在已审核数据");
            }
        }

        var5 = promHeadList.iterator();

        while (var5.hasNext()) {
            promHead = (GaiaSdPromHead) var5.next();
            promHead.setGsphStatus(inData.getGsphStatus());
            this.promHeadMapper.updateByPrimaryKeySelective(promHead);
        }

    }

    @Override
    public PromoteOutData getParam(PromoteInData inData, GetLoginOutData userInfo) {
        PromoteOutData outData = new PromoteOutData();
        Example exampleOne = new Example(GaiaSdPromPara.class);
        exampleOne.createCriteria().andEqualTo("gsppType", "para1").andEqualTo("gsppPromType", inData.getGsphType());
        List<GaiaSdPromPara> promParasOne = this.promParaMapper.selectByExample(exampleOne);
        List<ParamOutData> paramOneList = new ArrayList();
        Iterator var7 = promParasOne.iterator();

        while (var7.hasNext()) {
            GaiaSdPromPara promPara = (GaiaSdPromPara) var7.next();
            ParamOutData paramOutData = new ParamOutData();
            paramOutData.setKey(promPara.getGsppPara());
            paramOutData.setValue(promPara.getGsppName());
            paramOneList.add(paramOutData);
        }

        Example exampleTwo = new Example(GaiaSdPromPara.class);
        exampleTwo.createCriteria().andEqualTo("gsppType", "para2").andEqualTo("gsppPromType", inData.getGsphType());
        List<GaiaSdPromPara> promParasTwo = this.promParaMapper.selectByExample(exampleTwo);
        List<ParamOutData> paramTwoList = new ArrayList();
        Iterator var10 = promParasTwo.iterator();

        while (var10.hasNext()) {
            GaiaSdPromPara promPara = (GaiaSdPromPara) var10.next();
            ParamOutData paramOutData = new ParamOutData();
            paramOutData.setKey(promPara.getGsppPara());
            paramOutData.setValue(promPara.getGsppName());
            paramTwoList.add(paramOutData);
        }

        Example exampleThree = new Example(GaiaSdPromPara.class);
        exampleThree.createCriteria().andEqualTo("gsppType", "para3").andEqualTo("gsppPromType", inData.getGsphType());
        List<GaiaSdPromPara> promParasThree = this.promParaMapper.selectByExample(exampleThree);
        List<ParamOutData> paramThreeList = new ArrayList();
        Iterator var13 = promParasThree.iterator();

        while (var13.hasNext()) {
            GaiaSdPromPara promPara = (GaiaSdPromPara) var13.next();
            ParamOutData paramOutData = new ParamOutData();
            paramOutData.setKey(promPara.getGsppPara());
            paramOutData.setValue(promPara.getGsppName());
            paramThreeList.add(paramOutData);
        }

        Example exampleFour = new Example(GaiaSdPromPara.class);
        exampleFour.createCriteria().andEqualTo("gsppType", "para4").andEqualTo("gsppPromType", inData.getGsphType());
        List<GaiaSdPromPara> promParasFour = this.promParaMapper.selectByExample(exampleFour);
        List<ParamOutData> paramFourList = new ArrayList();
        Iterator var16 = promParasFour.iterator();

        while (var16.hasNext()) {
            GaiaSdPromPara promPara = (GaiaSdPromPara) var16.next();
            ParamOutData paramOutData = new ParamOutData();
            paramOutData.setKey(promPara.getGsppPara());
            paramOutData.setValue(promPara.getGsppName());
            paramFourList.add(paramOutData);
        }

        Example exampleEx = new Example(GaiaSdPromPara.class);
        exampleEx.createCriteria().andEqualTo("gsppType", "exclusion").andEqualTo("gsppPromType", inData.getGsphType());
        List<GaiaSdPromPara> promParasEx = this.promParaMapper.selectByExample(exampleEx);
        List<ParamOutData> paramExList = new ArrayList();
        Iterator var19 = promParasEx.iterator();

        while (var19.hasNext()) {
            GaiaSdPromPara promPara = (GaiaSdPromPara) var19.next();
            ParamOutData paramOutData = new ParamOutData();
            paramOutData.setKey(promPara.getGsppPara());
            paramOutData.setValue(promPara.getGsppName());
            paramExList.add(paramOutData);
        }

        outData.setParamOneList(paramOneList);
        outData.setParamTwoList(paramTwoList);
        outData.setParamThreeList(paramThreeList);
        outData.setParamFourList(paramFourList);
        outData.setExclusionList(paramExList);
        return outData;
    }

    /**
     * 单品保存
     *
     * @param clientId
     * @param voucherId
     * @param promUnitarySetInDataList
     * @param isUpdate
     */
    @Transactional(rollbackFor = Exception.class)
    public void savePromUnitarySet(String clientId, String voucherId, List<PromUnitarySetInData> promUnitarySetInDataList, boolean isUpdate) {
        List<GaiaSdPromUnitarySet> setList = new ArrayList<>();
        for (PromUnitarySetInData inData : promUnitarySetInDataList) {
            GaiaSdPromUnitarySet data = CopyUtil.copy(inData, GaiaSdPromUnitarySet.class);
            data.setClientId(clientId);
            data.setGspusVoucherId(voucherId);
            data.setGspusSerial(inData.getGspusSerial());
            setList.add(data);
        }
        this.sdPromUnitarySetMapper.insertToList(setList);

    }

    /**
     * 系列:商品条件设置
     *
     * @param clientId
     * @param voucherId
     * @param promSeriesCondsInDataList
     * @param isUpdate
     */
    @Transactional(rollbackFor = Exception.class)
    public void savePromSeriesConds(String clientId, String voucherId, List<PromSeriesCondsInData> promSeriesCondsInDataList, boolean isUpdate) {
        List<GaiaSdPromSeriesConds> condsList = new ArrayList<>();
        for (PromSeriesCondsInData inData : promSeriesCondsInDataList) {
            GaiaSdPromSeriesConds data = CopyUtil.copy(inData, GaiaSdPromSeriesConds.class);
            data.setClientId(clientId);
            data.setGspscVoucherId(voucherId);
            condsList.add(data);
        }
        this.sdPromSeriesCondsMapper.insertToList(condsList);

    }

    /**
     * 系列:系列编码设置
     *
     * @param clientId
     * @param voucherId
     * @param promSeriesSetInDataList
     * @param isUpdate
     */
    @Transactional(rollbackFor = Exception.class)
    public void savePromSeriesSet(String clientId, String voucherId, List<PromSeriesSetInData> promSeriesSetInDataList, boolean isUpdate) {
        List<GaiaSdPromSeriesSet> setList = new ArrayList<>();
        for (PromSeriesSetInData inData : promSeriesSetInDataList) {
            GaiaSdPromSeriesSet data = CopyUtil.copy(inData, GaiaSdPromSeriesSet.class);
            data.setClientId(clientId);
            data.setGspssVoucherId(voucherId);
            setList.add(data);
        }
        this.sdPromSeriesSetMapper.insertToList(setList);
    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromGiftConds(String clientId, String voucherId, List<PromGiftCondsInData> promGiftCondsInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promGiftCondsInDataList.iterator();

        while (var6.hasNext()) {
            PromGiftCondsInData inData = (PromGiftCondsInData) var6.next();
            GaiaSdPromGiftConds data = new GaiaSdPromGiftConds();
            data.setClientId(clientId);
            data.setGspgcVoucherId(voucherId);
            data.setGspgcSerial(String.valueOf(index++));
            data.setGspgcSeriesId(inData.getGspgcSeriesId());
            data.setGspgcInteFlag(inData.getGspgcInteFlag());
            data.setGspgcInteRate(inData.getGspgcInteRate());
            data.setGspgcMemFlag(inData.getGspgcMemFlag());
            data.setGspgcProId(inData.getGspgcProId());
            this.sdPromGiftCondsMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromGiftSet(String clientId, String voucherId, List<PromGiftSetInData> promGiftSetInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promGiftSetInDataList.iterator();

        while (var6.hasNext()) {
            PromGiftSetInData inData = (PromGiftSetInData) var6.next();
            GaiaSdPromGiftSet data = new GaiaSdPromGiftSet();
            data.setClientId(clientId);
            data.setGspgsVoucherId(voucherId);
            data.setGspgsSerial(String.valueOf(index++));
            data.setGspgsSeriesProId(inData.getGspgsSeriesProId());
            data.setGspgsReachAmt1(inData.getGspgsReachAmt1());
            data.setGspgsReachAmt2(inData.getGspgsReachAmt2());
            data.setGspgsReachAmt3(inData.getGspgsReachAmt3());
            data.setGspgsReachQty1(inData.getGspgsReachQty1());
            data.setGspgsReachQty2(inData.getGspgsReachQty2());
            data.setGspgsReachQty3(inData.getGspgsReachQty3());
            data.setGspgsResultQty1(inData.getGspgsResultQty1());
            data.setGspgsResultQty2(inData.getGspgsResultQty2());
            data.setGspgsResultQty3(inData.getGspgsResultQty3());
            this.sdPromGiftSetMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromGiftResult(String clientId, String voucherId, List<PromGiftResultInData> promGiftResultInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promGiftResultInDataList.iterator();

        while (var6.hasNext()) {
            PromGiftResultInData inData = (PromGiftResultInData) var6.next();
            GaiaSdPromGiftResult data = new GaiaSdPromGiftResult();
            data.setClientId(clientId);
            data.setGspgrVoucherId(voucherId);
            data.setGspgrSerial(String.valueOf(index++));
            data.setGspgrSeriesId(inData.getGspgcSeriesId());
            data.setGspgrGiftPrc(inData.getGspgcGiftPrc());
            data.setGspgrGiftRebate(inData.getGspgcGiftRebate());
            data.setGspgrProId(inData.getGspgcProId());
            this.sdPromGiftResultMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromCouponSet(String clientId, String voucherId, List<PromCouponSetInData> promCouponSetInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promCouponSetInDataList.iterator();

        while (var6.hasNext()) {
            PromCouponSetInData inData = (PromCouponSetInData) var6.next();
            GaiaSdPromCouponSet data = new GaiaSdPromCouponSet();
            data.setClientId(clientId);
            data.setGspcsVoucherId(voucherId);
            data.setGspcsSerial(String.valueOf(index++));
            data.setGspcsActNo(inData.getGspcsActNo());
            data.setGspcsCouponType(inData.getGspcsCouponType());
            data.setGspcsCouponRemarks(inData.getGspcsCouponRemarks());
            if (ObjectUtil.isNotEmpty(inData.getGspcsBeginDate())) {
                data.setGspcsBeginDate(DateUtil.format(DateUtil.parse(inData.getGspcsBeginDate(), "yyyy-MM-dd"), "yyyyMMdd"));
            }

            if (ObjectUtil.isNotEmpty(inData.getGspcsEndDate())) {
                data.setGspcsEndDate(DateUtil.format(DateUtil.parse(inData.getGspcsEndDate(), "yyyy-MM-dd"), "yyyyMMdd"));
            }

            if (ObjectUtil.isNotEmpty(inData.getGspcsBeginTime())) {
                data.setGspcsBeginTime(DateUtil.format(DateUtil.parse(inData.getGspcsBeginTime(), "HH:mm:ss"), "HHmmss"));
            }

            if (ObjectUtil.isNotEmpty(inData.getGspcsEndTime())) {
                data.setGspcsEndTime(DateUtil.format(DateUtil.parse(inData.getGspcsEndTime(), "HH:mm:ss"), "HHmmss"));
            }

            data.setGspcsSinglePaycheckAmt(inData.getGspcsSinglePaycheckAmt());
            data.setGspcsSingleUseAmt(inData.getGspcsSingleUseAmt());
            data.setGspcsTotalPaycheckAmt(inData.getGspcsTotalPaycheckAmt());
            this.sdPromCouponSetMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromCouponGrant(String clientId, String voucherId, List<PromCouponGrantInData> promCouponGrantInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promCouponGrantInDataList.iterator();

        while (var6.hasNext()) {
            PromCouponGrantInData inData = (PromCouponGrantInData) var6.next();
            GaiaSdPromCouponGrant data = new GaiaSdPromCouponGrant();
            data.setClientId(clientId);
            data.setGspcgVoucherId(voucherId);
            data.setGspcgSerial(String.valueOf(index++));
            data.setGspcgProId(inData.getGspcgProId());
            data.setGspcgActNo(inData.getGspcgActNo());
            data.setGspcgInteFlag(inData.getGspcgInteFlag());
            data.setGspcgInteRate(inData.getGspcgInteRate());
            data.setGspcgMemFlag(inData.getGspcgMemFlag());
            data.setGspcgReachAmt1(inData.getGspcgReachAmt1());
            data.setGspcgReachAmt2(inData.getGspcgReachAmt2());
            data.setGspcgReachAmt3(inData.getGspcgReachAmt3());
            data.setGspcgReachQty1(inData.getGspcgReachQty1());
            data.setGspcgReachQty2(inData.getGspcgReachQty2());
            data.setGspcgReachQty3(inData.getGspcgReachQty3());
            data.setGspcgResultQty1(inData.getGspcgResultQty1());
            data.setGspcgResultQty2(inData.getGspcgResultQty2());
            data.setGspcgResultQty3(inData.getGspcgResultQty3());
            this.sdPromCouponGrantMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromCouponUse(String clientId, String voucherId, List<PromCouponUseInData> promCouponUseInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promCouponUseInDataList.iterator();
        while (var6.hasNext()) {
            PromCouponUseInData inData = (PromCouponUseInData) var6.next();
            GaiaSdPromCouponUse data = new GaiaSdPromCouponUse();
            data.setClientId(clientId);
            data.setGspcuVoucherId(voucherId);
            data.setGspcuSerial(String.valueOf(index++));
            data.setGspcuProId(inData.getGspcuProId());
            data.setGspcuActNo(inData.getGspcuActNo());
            data.setGspcuInteFlag(inData.getGspcuInteFlag());
            data.setGspcuInteRate(inData.getGspcuInteRate());
            data.setGspcuMemFlag(inData.getGspcuMemFlag());
            data.setGspcuReachAmt1(inData.getGspcuReachAmt1());
            data.setGspcuReachAmt2(inData.getGspcuReachAmt2());
            data.setGspcuReachAmt3(inData.getGspcuReachAmt3());
            data.setGspcuReachQty1(inData.getGspcuReachQty1());
            data.setGspcuReachQty2(inData.getGspcuReachQty2());
            data.setGspcuReachQty3(inData.getGspcuReachQty3());
            data.setGspcuResultQty1(inData.getGspcuResultQty1());
            data.setGspcuResultQty2(inData.getGspcuResultQty2());
            data.setGspcuResultQty3(inData.getGspcuResultQty3());
            this.sdPromCouponUseMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromHySet(String clientId, String voucherId, List<PromHySetInData> promHySetInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promHySetInDataList.iterator();
        while (var6.hasNext()) {
            PromHySetInData inData = (PromHySetInData) var6.next();
            GaiaSdPromHySet data = new GaiaSdPromHySet();
            data.setClientId(clientId);
            data.setGsphsVoucherId(voucherId);
            data.setGsphsSerial(String.valueOf(index++));
            data.setGsphsProId(inData.getGsphsProId());
            data.setGsphsInteFlag(inData.getGsphsInteFlag());
            data.setGsphsInteRate(inData.getGsphsInteRate());
            data.setGsphsPrice(inData.getGsphsPrice());
            data.setGsphsRebate(inData.getGsphsRebate());
            this.sdPromHySetMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromHyrDiscount(String clientId, String voucherId, List<PromHyrDiscountInData> promHyrDiscountInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promHyrDiscountInDataList.iterator();
        while (var6.hasNext()) {
            PromHyrDiscountInData inData = (PromHyrDiscountInData) var6.next();
            GaiaSdPromHyrDiscount data = new GaiaSdPromHyrDiscount();
            data.setClientId(clientId);
            data.setGsppVoucherId(voucherId);
            data.setGsppSerial(String.valueOf(index++));
            if (ObjectUtil.isNotEmpty(inData.getGsppBeginDate())) {
                data.setGsppBeginDate(DateUtil.format(DateUtil.parse(inData.getGsppBeginDate(), "yyyy-MM-dd"), "yyyyMMdd"));
            }

            if (ObjectUtil.isNotEmpty(inData.getGsppEndDate())) {
                data.setGsppEndDate(DateUtil.format(DateUtil.parse(inData.getGsppEndDate(), "yyyy-MM-dd"), "yyyyMMdd"));
            }

            if (ObjectUtil.isNotEmpty(inData.getGsppBeginTime())) {
                data.setGsppBeginTime(DateUtil.format(DateUtil.parse(inData.getGsppBeginTime(), "HH:mm:ss"), "HHmmss"));
            }

            if (ObjectUtil.isNotEmpty(inData.getGsppEndTime())) {
                data.setGsppEndTime(DateUtil.format(DateUtil.parse(inData.getGsppEndTime(), "HH:mm:ss"), "HHmmss"));
            }

            data.setGsppDateFrequency(inData.getGsppDateFrequency());
            data.setGsppInteFlag(inData.getGsppInteFlag());
            data.setGsppInteRate(inData.getGsppInteRate());
            data.setGsppRebate(inData.getGsppRebate());
            data.setGsppTimeFrequency(inData.getGsppTimeFrequency());
            this.sdPromHyrDiscountMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromHyrPrice(String clientId, String voucherId, List<PromHyrPriceInData> promHyrPriceInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promHyrPriceInDataList.iterator();
        while (var6.hasNext()) {
            PromHyrPriceInData inData = (PromHyrPriceInData) var6.next();
            GaiaSdPromHyrPrice data = new GaiaSdPromHyrPrice();
            data.setClientId(clientId);
            data.setGsphpVoucherId(voucherId);
            data.setGsphpSerial(String.valueOf(index++));
            data.setGsphpInteFlag(inData.getGsphpInteFlag());
            data.setGsphpInteRate(inData.getGsphpInteRate());
            data.setGsphpPrice(inData.getGsphpPrice());
            data.setGsphpProId(inData.getGsphpProId());
            data.setGsphpRebate(inData.getGsphpRebate());
            this.sdPromHyrPriceMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromAssoConds(String clientId, String voucherId, List<PromAssoCondsInData> promAssoCondsInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promAssoCondsInDataList.iterator();
        while (var6.hasNext()) {
            PromAssoCondsInData inData = (PromAssoCondsInData) var6.next();
            GaiaSdPromAssoConds data = new GaiaSdPromAssoConds();
            data.setClientId(clientId);
            data.setGspacVoucherId(voucherId);
            data.setGspacSerial(String.valueOf(index++));
            data.setGspacProId(inData.getGspacProId());
            data.setGspacSeriesId(inData.getGspacSeriesId());
            data.setGspacInteFlag(inData.getGspacInteFlag());
            data.setGspacInteRate(inData.getGspacInteRate());
            data.setGspacMemFlag(inData.getGspacMemFlag());
            data.setGspacQty(inData.getGspacQty());
            this.sdPromAssoCondsMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromAssoSet(String clientId, String voucherId, List<PromAssoSetInData> promAssoSetInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promAssoSetInDataList.iterator();
        while (var6.hasNext()) {
            PromAssoSetInData inData = (PromAssoSetInData) var6.next();
            GaiaSdPromAssoSet data = new GaiaSdPromAssoSet();
            data.setClientId(clientId);
            data.setGspasVoucherId(voucherId);
            data.setGspasSerial(String.valueOf(index++));
            data.setGspasSeriesId(inData.getGspasSeriesId());
            data.setGspasAmt(inData.getGspasAmt());
            data.setGspasQty(inData.getGspasQty());
            data.setGspasRebate(inData.getGspasRebate());
            this.sdPromAssoSetMapper.insert(data);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void savePromAssoResult(String clientId, String voucherId, List<PromAssoResultInData> promAssoResultInDataList, boolean isUpdate) {
        int index = 1;
        Iterator var6 = promAssoResultInDataList.iterator();
        while (var6.hasNext()) {
            PromAssoResultInData inData = (PromAssoResultInData) var6.next();
            GaiaSdPromAssoResult data = new GaiaSdPromAssoResult();
            data.setClientId(clientId);
            data.setGsparVoucherId(voucherId);
            data.setGsparSerial(String.valueOf(index++));
            data.setGsparSeriesId(inData.getGsparSeriesId());
            data.setGsparProId(inData.getGsparProId());
            data.setGsparGiftPrc(inData.getGsparGiftPrc());
            data.setGsparGiftRebate(inData.getGsparGiftRebate());
            this.sdPromAssoResultMapper.insert(data);
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<RebornData> reSummaryGiftPromotion(String client, String brId, ReSummaryPromoteIndata reSummaryPromoteIndata) {
        GetDisPriceOutData getDisPriceInData = reSummaryPromoteIndata.getGetDisPriceOutData();
        //根据单号查询 活动内容
        Example example = new Example(GaiaSdPromGiftSet.class);
        example.createCriteria().andEqualTo("clientId", client).andEqualTo("gspgsVoucherId", getDisPriceInData.getGspusVoucherId()).andEqualTo("gspgsSeriesProId", getDisPriceInData.getGspusSeriesId());
        GaiaSdPromGiftSet giftSet = sdPromGiftSetMapper.selectOneByExample(example);
        //根据单号查询 活动头内容
        example = new Example(GaiaSdPromHead.class);
        example.createCriteria().andEqualTo("clientId", client).andEqualTo("gsphBrId", brId).andEqualTo("gsphVoucherId", getDisPriceInData.getGspusVoucherId());
        GaiaSdPromHead head = promHeadMapper.selectOneByExample(example);

        if (ObjectUtil.isNotEmpty(giftSet) && ObjectUtil.isNotEmpty(head)) {
            //去除参加其他活动的 拆零的 积分换购的 改价的商品 并按单价排序
            Collections.reverse(reSummaryPromoteIndata.getRebornDataList());
            List<RebornData> rebornDataList = reSummaryPromoteIndata.getRebornDataList().stream()
                    .filter(item -> PromUtil.isJoinActivity(item.getStatus()) || (getDisPriceInData.getGspusVoucherId().equals(item.getGssdPmActivityId()) && "促销".equals(item.getStatus()) && "PROM_GIFT".equals(item.getGssdPmId())))
                    .sorted(Comparator.comparingDouble(ritm -> Double.parseDouble(ritm.getGssdPrc1())))
                    .collect(Collectors.toList());
            Collections.reverse(rebornDataList);
            rebornDataList.forEach(item -> {
                //状态
                item.setBillItemStatus("");
                item.setStatus("");
                //促销单号
                item.setVoucherId("");
                item.setGssdPmActivityId("");
                item.setGssdPmId("");
                //互斥条件
                item.setGsphExclusion("");
                item.setGssdPmActivityFlag("");
                item.setGssdAmt(new BigDecimal(item.getGssdPrc1()).multiply(new BigDecimal(item.getGssdQty())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                item.setBillItemRate("100");
                item.setGssdPrc2(item.getGssdPrc1());
            });

            if (CollectionUtil.isNotEmpty(rebornDataList)) {
                List<GaiaSdPromGiftConds> giftCondsList = null;
                //判断是否全场商品 若为全场商品 则条件列表置为空
                if (!"assign1".equals(head.getGsphPara3())) {
                    example = new Example(GaiaSdPromGiftConds.class);
                    example.createCriteria().andEqualTo("clientId", client).andEqualTo("gspgcSeriesId", getDisPriceInData.getGspusSeriesId()).andEqualTo("gspgcVoucherId", getDisPriceInData.getGspusVoucherId());
                    giftCondsList = sdPromGiftCondsMapper.selectByExample(example);//此活动系列的条件商品列表
                }

                example = new Example(GaiaSdPromGiftResult.class);
                example.createCriteria().andEqualTo("clientId", client).andEqualTo("gspgrSeriesId", getDisPriceInData.getGspusSeriesId()).andEqualTo("gspgrVoucherId", getDisPriceInData.getGspusVoucherId());
                List<GaiaSdPromGiftResult> giftResultList = sdPromGiftResultMapper.selectByExample(example);//此活动系列的赠送商品列表

                //判断活动阶梯
                if ("3".equals(head.getGsphPart())) { //第三阶梯
                    if (!StringUtils.isEmpty(giftSet.getGspgsReachQty3())) {//3阶梯数量条件
                        if (judgeGiftPromotion(giftSet.getGspgsReachQty3(), giftSet.getGspgsResultQty3(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion(giftSet.getGspgsReachQty3(), giftSet.getGspgsResultQty3(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        } else if (judgeGiftPromotion(giftSet.getGspgsReachQty2(), giftSet.getGspgsResultQty2(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion(giftSet.getGspgsReachQty2(), giftSet.getGspgsResultQty2(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        } else if (judgeGiftPromotion(giftSet.getGspgsReachQty1(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion(giftSet.getGspgsReachQty1(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        }
                    } else {//3阶梯金额条件
                        if (judgeGiftPromotion2(giftSet.getGspgsReachAmt3().toString(), giftSet.getGspgsResultQty3(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion2(giftSet.getGspgsReachAmt3().toString(), giftSet.getGspgsResultQty3(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        } else if (judgeGiftPromotion2(giftSet.getGspgsReachAmt2().toString(), giftSet.getGspgsResultQty2(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion2(giftSet.getGspgsReachAmt2().toString(), giftSet.getGspgsResultQty2(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        } else if (judgeGiftPromotion2(giftSet.getGspgsReachAmt1().toString(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion2(giftSet.getGspgsReachAmt1().toString(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        }
                    }
                } else if ("2".equals(head.getGsphPart())) {
                    if (!StringUtils.isEmpty(giftSet.getGspgsReachQty2())) {//2阶梯数量条件
                        if (judgeGiftPromotion(giftSet.getGspgsReachQty2(), giftSet.getGspgsResultQty2(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion(giftSet.getGspgsReachQty2(), giftSet.getGspgsResultQty2(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        } else if (judgeGiftPromotion(giftSet.getGspgsReachQty1(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion(giftSet.getGspgsReachQty1(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        }
                    } else {//2阶梯金额条件
                        if (judgeGiftPromotion2(giftSet.getGspgsReachAmt2().toString(), giftSet.getGspgsResultQty2(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion2(giftSet.getGspgsReachAmt2().toString(), giftSet.getGspgsResultQty2(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        } else if (judgeGiftPromotion2(giftSet.getGspgsReachAmt1().toString(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion2(giftSet.getGspgsReachAmt1().toString(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        }
                    }
                } else if ("1".equals(head.getGsphPart())) {
                    if (!StringUtils.isEmpty(giftSet.getGspgsReachQty1())) {//1阶梯数量条件
                        if (judgeGiftPromotion(giftSet.getGspgsReachQty1(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion(giftSet.getGspgsReachQty1(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        }
                    } else {//1阶梯金额条件
                        if (judgeGiftPromotion2(giftSet.getGspgsReachAmt1().toString(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, reSummaryPromoteIndata.getIsMember())) {
                            calculateGiftPromotion2(giftSet.getGspgsReachAmt1().toString(), giftSet.getGspgsResultQty1(), rebornDataList, giftCondsList, giftResultList, getDisPriceInData, head, reSummaryPromoteIndata.getIsMember());
                        }
                    }
                }

                reSummaryPromoteIndata.getRebornDataList().forEach(item -> {
                    rebornDataList.forEach(item2 -> {
                        if (item.getGssdSerial().equals(item2.getGssdSerial())) {
                            item.setBillItemStatus(item2.getBillItemStatus());
                            item.setStatus(item2.getStatus());
                            //促销单号
                            item.setVoucherId(item2.getVoucherId());
                            item.setGssdPmActivityId(item2.getGssdPmActivityId());
                            item.setGssdPmId(item2.getGssdPmId());
                            //互斥条件
                            item.setGsphExclusion(item2.getGsphExclusion());
                            //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                            item.setGssdPmActivityFlag(item2.getGssdPmActivityFlag());
                            item.setGssdPrc2(item2.getGssdPrc2());
                            item.setGssdAmt(item2.getGssdAmt());
                            item.setBillItemRate(item2.getBillItemRate());
                        }
                    });

                });
                return reSummaryPromoteIndata.getRebornDataList().stream().sorted(Comparator.comparingInt(item -> Integer.parseInt(item.getGssdSerial()))).collect(Collectors.toList());
            } else {
                return reSummaryPromoteIndata.getRebornDataList().stream().sorted(Comparator.comparingInt(item -> Integer.parseInt(item.getGssdSerial()))).collect(Collectors.toList());
            }
        } else {
            return reSummaryPromoteIndata.getRebornDataList();
        }
    }

    /**
     * 判断商品列表是否满足条件商品数量和赠送数量
     *
     * @param condTemp          条件数量
     * @param resultTemp        赠送数
     * @param rebornDataListArg 商品列表
     * @param giftCondsList     条件商品列表
     * @param giftResultList    赠品列表
     * @return 是否满足
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean judgeGiftPromotion(String condTemp, String resultTemp, List<RebornData> rebornDataListArg, List<GaiaSdPromGiftConds> giftCondsList, List<GaiaSdPromGiftResult> giftResultList, String isMember) {
        List<RebornData> rebornDataList = CopyUtil.copyList(rebornDataListArg, RebornData.class);
        //遍历统计计算赠送商品的数量是否满足数量条件
        BigDecimal temp = new BigDecimal("1"); //赠送数量缓存 用以计算
        for (int i = rebornDataList.size() - 1; i >= 0; i--) {
            RebornData reborn = rebornDataList.get(i);
            if (temp.compareTo(BigDecimal.ZERO) > 0) { //判断 赠送数量 是否存在
                for (GaiaSdPromGiftResult result : giftResultList) {
                    if (reborn.getGssdProId().equals(result.getGspgrProId())) {
                        if (ObjectUtil.isEmpty(reborn.getPromGiftQtyTemp())) {
                            reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
                        }
                        if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
                            reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                        }
                        //判断剩余可送商品数是否大于等于1
                        BigDecimal subQtyTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp());
                        if (subQtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                            reborn.setPromGiftSendQtyTemp(reborn.getPromGiftSendQtyTemp().add(temp));
                            temp = BigDecimal.ZERO;
                        }
                        break;
                    }
                }
            } else {
                break;
            }
        }

        if (temp.compareTo(BigDecimal.ZERO) == 0) {
            //遍历统计计算条件商品的数量是否满足数量条件
            temp = new BigDecimal(condTemp); //条件数量缓存 用以计算
            for (RebornData reborn : rebornDataList) {
                if (ObjectUtil.isNotEmpty(reborn.getPromGiftQtyTemp()) && reborn.getPromGiftQtyTemp().compareTo(BigDecimal.ZERO) > 0) {
                    temp = temp.add(reborn.getPromGiftQtyTemp());//将上一轮的条件数量累加 用以判断 当前促销是否满足（否则循环生效的情况 之前的条件会被清空）
                }
                reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
            }
            if (CollectionUtil.isNotEmpty(giftCondsList)) {
                for (RebornData reborn : rebornDataList) {
                    if (temp.compareTo(BigDecimal.ZERO) > 0) {
                        for (GaiaSdPromGiftConds conds : giftCondsList) {
                            if (reborn.getGssdProId().equals(conds.getGspgcProId()) && new BigDecimal(conds.getGspgcMemFlag()).compareTo(new BigDecimal(isMember)) < 1) {
                                if (ObjectUtil.isEmpty(reborn.getPromGiftQtyTemp())) {
                                    reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
                                }
                                if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
                                    reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                                }
                                //判断剩余商品数是否大于条件数 需减去已达成条件数和已赠送数
                                BigDecimal subQtyTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftQtyTemp()).subtract(reborn.getPromGiftSendQtyTemp());
                                if (subQtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                                    if (subQtyTemp.compareTo(temp) > -1) {
                                        reborn.setPromGiftQtyTemp(reborn.getPromGiftQtyTemp().add(temp));
                                        temp = BigDecimal.ZERO;
                                    } else if (subQtyTemp.compareTo(temp) < 0) {
                                        reborn.setPromGiftQtyTemp(reborn.getPromGiftQtyTemp().add(subQtyTemp));
                                        temp = temp.subtract(subQtyTemp);
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            } else {
                for (RebornData reborn : rebornDataList) {
                    if (temp.compareTo(BigDecimal.ZERO) > 0) {
                        if (ObjectUtil.isEmpty(reborn.getPromGiftQtyTemp())) {
                            reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
                        }
                        if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
                            reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                        }
                        //判断剩余商品数是否大于条件数 需减去已达成条件数和已赠送数
                        BigDecimal subQtyTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftQtyTemp()).subtract(reborn.getPromGiftSendQtyTemp());
                        if (subQtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                            if (subQtyTemp.compareTo(temp) > -1) {
                                reborn.setPromGiftQtyTemp(reborn.getPromGiftQtyTemp().add(temp));
                                temp = BigDecimal.ZERO;
                            } else if (subQtyTemp.compareTo(temp) < 0) {
                                reborn.setPromGiftQtyTemp(reborn.getPromGiftQtyTemp().add(subQtyTemp));
                                temp = temp.subtract(subQtyTemp);
                            }
                        }
                    }
                }
            }
            if (temp.compareTo(BigDecimal.ZERO) == 0) {
                return true;
            } else
                return false;
        } else {
            return false;
        }
    }

    /**
     * 判断商品列表是否满足条件商品金额和赠送数量
     *
     * @param condTemp          条件金额
     * @param resultTemp        赠送数
     * @param rebornDataListArg 商品列表
     * @param giftCondsList     条件商品列表
     * @param giftResultList    赠品列表
     * @return 是否满足
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean judgeGiftPromotion2(String condTemp, String resultTemp, List<RebornData> rebornDataListArg, List<GaiaSdPromGiftConds> giftCondsList, List<GaiaSdPromGiftResult> giftResultList,String isMember) {
        List<RebornData> rebornDataList = CopyUtil.copyList(rebornDataListArg, RebornData.class);
        //遍历统计计算赠送商品的数量是否满足数量条件
        BigDecimal temp = new BigDecimal("1"); //赠送数量缓存 用以计算
        for (int i = rebornDataList.size() - 1; i >= 0; i--) {
            RebornData reborn = rebornDataList.get(i);
            if (temp.compareTo(BigDecimal.ZERO) > 0) { //判断 赠送数量 是否存在
                for (GaiaSdPromGiftResult result : giftResultList) {
                    if (reborn.getGssdProId().equals(result.getGspgrProId())) {
                        if (ObjectUtil.isEmpty(reborn.getPromGiftQtyTemp())) {
                            reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
                        }
                        if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
                            reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                        }
                        //判断剩余可送商品数是否大于等于1
                        BigDecimal subQtyTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp());
                        if (subQtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                            reborn.setPromGiftSendQtyTemp(reborn.getPromGiftSendQtyTemp().add(temp));
                            temp = BigDecimal.ZERO;
                        }
                        break;
                    }
                }
            } else break;
        }

        if (temp.compareTo(BigDecimal.ZERO) == 0) {
            //遍历统计计算条件商品的数量是否满足数量条件
            temp = new BigDecimal(condTemp); //条件金额缓存 用以计算
            for (RebornData reborn : rebornDataList) {
                if (ObjectUtil.isNotEmpty(reborn.getPromGiftAmtTemp()) && reborn.getPromGiftAmtTemp().compareTo(BigDecimal.ZERO) > 0) {
                    temp = temp.add(reborn.getPromGiftAmtTemp());//将上一轮的条件金额累加 用以判断 当前促销是否满足（否则循环生效的情况 之前的条件会被清空）
                }
                reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
            }
            if (CollectionUtil.isNotEmpty(giftCondsList)) {
                for (RebornData reborn : rebornDataList) {
                    if (temp.compareTo(BigDecimal.ZERO) > 0) {
                        for (GaiaSdPromGiftConds conds : giftCondsList) {
                            if (reborn.getGssdProId().equals(conds.getGspgcProId()) && new BigDecimal(conds.getGspgcMemFlag()).compareTo(new BigDecimal(isMember)) < 1) {
                                if (ObjectUtil.isEmpty(reborn.getPromGiftAmtTemp())) {
                                    reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
                                }
                                if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
                                    reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                                }
                                //判断剩余商品数是否大于条件数 需减去已达成条件数和已赠送数
                                BigDecimal subAmtTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp()).multiply(new BigDecimal(reborn.getGssdPrc1())).subtract(reborn.getPromGiftAmtTemp());
                                if (subAmtTemp.compareTo(BigDecimal.ZERO) > 0) {
                                    if (subAmtTemp.compareTo(temp) > -1) {
                                        reborn.setPromGiftAmtTemp(reborn.getPromGiftAmtTemp().add(temp));
                                        temp = BigDecimal.ZERO;
                                    } else if (subAmtTemp.compareTo(temp) < 0) {
                                        reborn.setPromGiftAmtTemp(reborn.getPromGiftAmtTemp().add(subAmtTemp));
                                        temp = temp.subtract(subAmtTemp);
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            } else {
                for (RebornData reborn : rebornDataList) {
                    if (temp.compareTo(BigDecimal.ZERO) > 0) {
                        if (ObjectUtil.isEmpty(reborn.getPromGiftAmtTemp())) {
                            reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
                        }
                        if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
                            reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                        }
                        //判断剩余条件商品金额是否大于条件需求金额 需减去已达成条件数和已赠送数的金额
                        BigDecimal subAmtTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp()).multiply(new BigDecimal(reborn.getGssdPrc1())).subtract(reborn.getPromGiftAmtTemp());
                        if (subAmtTemp.compareTo(BigDecimal.ZERO) > 0) {
                            if (subAmtTemp.compareTo(temp) > -1) {
                                reborn.setPromGiftAmtTemp(reborn.getPromGiftAmtTemp().add(temp));
                                temp = BigDecimal.ZERO;
                            } else if (subAmtTemp.compareTo(temp) < 0) {
                                reborn.setPromGiftAmtTemp(reborn.getPromGiftAmtTemp().add(subAmtTemp));
                                temp = temp.subtract(subAmtTemp);
                            }
                        }
                    }
                }
            }
            if (temp.compareTo(BigDecimal.ZERO) == 0) {
                return true;
            } else
                return false;
        } else {
            return false;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void calculateGiftPromotion(String condTemp, String resultTemp, List<RebornData> rebornDataList, List<GaiaSdPromGiftConds> giftCondsList, List<GaiaSdPromGiftResult> giftResultList, GetDisPriceOutData priceOutDatum, GaiaSdPromHead head,String isMember) {

        // 遍历统计计算赠送商品的数量是否满足数量条件
        BigDecimal temp = new BigDecimal(resultTemp); // 赠送数量缓存 用以计算
        BigDecimal tTemp = new BigDecimal(condTemp); // 条件数量缓存 用以计算

        for (RebornData reborn : rebornDataList) {
            if (ObjectUtil.isNotEmpty(reborn.getPromGiftQtyTemp()) && reborn.getPromGiftQtyTemp().compareTo(BigDecimal.ZERO) > 0) {
                tTemp = tTemp.add(reborn.getPromGiftQtyTemp());
            }
            reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
        }
        List<RebornData> rebornDataListTemp = CopyUtil.copyList(rebornDataList, RebornData.class);
        int ii = temp.intValue();
        indexFor:
        for (int index = 0; index < ii; index++) {
            if (temp.compareTo(BigDecimal.ZERO) > 0) {
                for (RebornData reborn : rebornDataListTemp) {
                    if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp()) || reborn.getPromGiftSendQtyTemp().compareTo(BigDecimal.ZERO) == 0) {
                        reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
                        //状态
                        reborn.setBillItemStatus("");
                        reborn.setStatus("");
                        //促销单号
                        reborn.setVoucherId("");
                        reborn.setGssdPmActivityId("");
                        reborn.setGssdPmId("");
                        //互斥条件
                        reborn.setGsphExclusion("");
                        //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                        reborn.setGssdPmActivityFlag("");
                        reborn.setGssdAmt(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                        reborn.setBillItemRate("100");
                        reborn.setGssdPrc2(reborn.getGssdPrc1());
                    }
                }
                //取价格最低的一个赠品
                for (int i = rebornDataListTemp.size() - 1; i >= 0; i--) {
                    if (temp.compareTo(BigDecimal.ZERO) > 0) {
                        for (RebornData reborn : rebornDataListTemp) {
                            reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
                        }
                        RebornData reborn = rebornDataListTemp.get(i);
                        for (GaiaSdPromGiftResult result : giftResultList) {
                            if (reborn.getGssdProId().equals(result.getGspgrProId())) {
                                if (ObjectUtil.isEmpty(reborn.getPromGiftAmtTemp())) {
                                    reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
                                }
                                if (ObjectUtil.isEmpty(reborn.getPromGiftQtyTemp())) {
                                    reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
                                }
                                if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
                                    reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                                }
                                // 判断剩余可送商品数
                                BigDecimal subQtyTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp());
                                //有可送商品 则加入计算
                                if (subQtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                                    //赠送一个商品
                                    reborn.setPromGiftSendQtyTemp(reborn.getPromGiftSendQtyTemp().add(new BigDecimal("1")));
                                    //状态
                                    reborn.setBillItemStatus("促销");
                                    reborn.setStatus("促销");
                                    //促销单号
                                    reborn.setVoucherId(priceOutDatum.getGspusVoucherId());
                                    reborn.setGssdPmActivityId(priceOutDatum.getGspusVoucherId());
                                    reborn.setGssdPmId("PROM_GIFT");
                                    //互斥条件
                                    reborn.setGsphExclusion(priceOutDatum.getGsphExclusion());
                                    //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                                    reborn.setGssdPmActivityFlag(priceOutDatum.getGspusSeriesId());
                                    if (ObjectUtil.isNotEmpty(result.getGspgrGiftPrc())) { //促销价
                                        reborn.setGssdAmt(result.getGspgrGiftPrc().multiply(reborn.getPromGiftSendQtyTemp()).add(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp()))).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setGssdPrc2(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()), 2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setBillItemRate(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()).multiply(new BigDecimal(reborn.getGssdPrc1())), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                                    } else if (StringUtils.isNotEmpty(result.getGspgrGiftRebate())) { //折扣
                                        reborn.setGssdAmt(new BigDecimal(result.getGspgrGiftRebate()).divide(new BigDecimal("100")).multiply(new BigDecimal(reborn.getGssdPrc1())).multiply(reborn.getPromGiftSendQtyTemp()).add(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp()))).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setGssdPrc2(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()), 2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setBillItemRate(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()).multiply(new BigDecimal(reborn.getGssdPrc1())), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                                    } else { //0元
                                        reborn.setGssdAmt(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setGssdPrc2(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()), 2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setBillItemRate(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()).multiply(new BigDecimal(reborn.getGssdPrc1())), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                                    }
                                    //计算条件商品数是否满足
                                    BigDecimal temp2 = tTemp.add(BigDecimal.ZERO);
                                    if (CollectionUtil.isNotEmpty(giftCondsList)) { //条件商品
                                        for (RebornData reborn2 : rebornDataListTemp) {
                                            if (temp2.compareTo(BigDecimal.ZERO) > 0) {
                                                for (GaiaSdPromGiftConds conds : giftCondsList) {
                                                    if (reborn2.getGssdProId().equals(conds.getGspgcProId()) && new BigDecimal(conds.getGspgcMemFlag()).compareTo(new BigDecimal(isMember)) < 1) {
                                                        if (ObjectUtil.isEmpty(reborn2.getPromGiftAmtTemp())) {
                                                            reborn2.setPromGiftAmtTemp(BigDecimal.ZERO);
                                                        }
                                                        if (ObjectUtil.isEmpty(reborn2.getPromGiftQtyTemp())) {
                                                            reborn2.setPromGiftQtyTemp(BigDecimal.ZERO);
                                                        }
                                                        if (ObjectUtil.isEmpty(reborn2.getPromGiftSendQtyTemp())) {
                                                            reborn2.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                                                        }
                                                        //判断剩余商品数是否大于条件数
                                                        BigDecimal subQtyTemp2 = new BigDecimal(reborn2.getGssdQty()).subtract(reborn2.getPromGiftQtyTemp()).subtract(reborn2.getPromGiftSendQtyTemp());
                                                        if (subQtyTemp2.compareTo(BigDecimal.ZERO) > 0) {
                                                            if (subQtyTemp2.compareTo(temp2) > -1) {
                                                                reborn2.setPromGiftQtyTemp(reborn2.getPromGiftQtyTemp().add(temp2));
                                                                temp2 = BigDecimal.ZERO;
                                                            } else if (subQtyTemp2.compareTo(temp2) < 0) {
                                                                reborn2.setPromGiftQtyTemp(reborn2.getPromGiftQtyTemp().add(subQtyTemp2));
                                                                temp2 = temp2.subtract(subQtyTemp2);
                                                            }
                                                            //状态
                                                            reborn2.setBillItemStatus("促销");
                                                            reborn2.setStatus("促销");
                                                            //促销单号
                                                            reborn2.setVoucherId(priceOutDatum.getGspusVoucherId());
                                                            reborn2.setGssdPmActivityId(priceOutDatum.getGspusVoucherId());
                                                            reborn2.setGssdPmId("PROM_GIFT");
                                                            //互斥条件
                                                            reborn2.setGsphExclusion(priceOutDatum.getGsphExclusion());
                                                            //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                                                            reborn2.setGssdPmActivityFlag(priceOutDatum.getGspusSeriesId());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {//全场条件商品
                                        for (RebornData reborn2 : rebornDataListTemp) {
                                            if (temp2.compareTo(BigDecimal.ZERO) > 0) {
                                                if (ObjectUtil.isEmpty(reborn2.getPromGiftAmtTemp())) {
                                                    reborn2.setPromGiftAmtTemp(BigDecimal.ZERO);
                                                }
                                                if (ObjectUtil.isEmpty(reborn2.getPromGiftQtyTemp())) {
                                                    reborn2.setPromGiftQtyTemp(BigDecimal.ZERO);
                                                }
                                                if (ObjectUtil.isEmpty(reborn2.getPromGiftSendQtyTemp())) {
                                                    reborn2.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                                                }
                                                //判断剩余商品数是否大于条件数
                                                BigDecimal subQtyTemp2 = new BigDecimal(reborn2.getGssdQty()).subtract(reborn2.getPromGiftSendQtyTemp());
                                                if (subQtyTemp2.compareTo(BigDecimal.ZERO) > 0) {
                                                    if (subQtyTemp2.compareTo(temp2) > -1) {
                                                        reborn2.setPromGiftQtyTemp(reborn2.getPromGiftQtyTemp().add(temp2));
                                                        temp2 = BigDecimal.ZERO;
                                                    } else if (subQtyTemp2.compareTo(temp2) < 0) {
                                                        reborn2.setPromGiftQtyTemp(reborn2.getPromGiftQtyTemp().add(subQtyTemp2));
                                                        temp2 = temp2.subtract(subQtyTemp2);
                                                    }
                                                    //状态
                                                    reborn2.setBillItemStatus("促销");
                                                    reborn2.setStatus("促销");
                                                    //促销单号
                                                    reborn2.setVoucherId(priceOutDatum.getGspusVoucherId());
                                                    reborn2.setGssdPmActivityId(priceOutDatum.getGspusVoucherId());
                                                    reborn2.setGssdPmId("PROM_GIFT");
                                                    //互斥条件
                                                    reborn2.setGsphExclusion(priceOutDatum.getGsphExclusion());
                                                    //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                                                    reborn2.setGssdPmActivityFlag(priceOutDatum.getGspusSeriesId());
                                                }
                                            }
                                        }
                                    }
                                    // 若满足 则赠送成立 数据刷入 填充行内所有值
                                    if (temp2.compareTo(BigDecimal.ZERO) == 0) {
                                        temp = temp.subtract(new BigDecimal("1"));
                                        rebornDataList.forEach(item -> {
                                            rebornDataListTemp.forEach(item2 -> {
                                                if (item.getGssdSerial().equals(item2.getGssdSerial())) {
                                                    item.setBillItemStatus(item2.getBillItemStatus());
                                                    item.setStatus(item2.getStatus());
                                                    //促销单号
                                                    item.setVoucherId(item2.getVoucherId());
                                                    item.setGssdPmActivityId(item2.getGssdPmActivityId());
                                                    item.setGssdPmId(item2.getGssdPmId());
                                                    //互斥条件
                                                    item.setGsphExclusion(item2.getGsphExclusion());
                                                    //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                                                    item.setGssdPmActivityFlag(item2.getGssdPmActivityFlag());
                                                    item.setGssdPrc2(item2.getGssdPrc2());
                                                    item.setGssdAmt(item2.getGssdAmt());
                                                    item.setBillItemRate(item2.getBillItemRate());
                                                    item.setPromGiftSendQtyTemp(item2.getPromGiftSendQtyTemp());
                                                    item.setPromGiftQtyTemp(item2.getPromGiftQtyTemp());
                                                    item.setPromGiftAmtTemp(item2.getPromGiftAmtTemp());
                                                }
                                            });
                                        });
                                        continue indexFor;//跳转到外层 继续计算下一个赠送商品
                                    } else { // 不满足 则截止
                                        return;
                                    }
                                } else { //无可送商品则跳出
                                    break;
                                }
                            }
                        }
                    }
                }
            } else break;
        }
        if ("repeat1".equals(head.getGsphPara1())) {
            if (judgeGiftPromotion(condTemp, resultTemp, rebornDataList, giftCondsList, giftResultList, isMember)) {
                calculateGiftPromotion(condTemp, resultTemp, rebornDataList, giftCondsList, giftResultList, priceOutDatum, head, isMember);
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void calculateGiftPromotion2(String condTemp, String resultTemp, List<RebornData> rebornDataList, List<GaiaSdPromGiftConds> giftCondsList, List<GaiaSdPromGiftResult> giftResultList, GetDisPriceOutData priceOutDatum, GaiaSdPromHead head,String isMember) {
        // 遍历统计计算赠送商品的数量是否满足数量条件
        BigDecimal temp = new BigDecimal(resultTemp); // 赠送数量缓存 用以计算
        BigDecimal tTemp = new BigDecimal(condTemp); // 条件金额缓存 用以计算

        for (RebornData reborn : rebornDataList) {
            if (ObjectUtil.isNotEmpty(reborn.getPromGiftAmtTemp()) && reborn.getPromGiftAmtTemp().compareTo(BigDecimal.ZERO) > 0) {
                tTemp = tTemp.add(reborn.getPromGiftAmtTemp());
            }
            reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
        }
        List<RebornData> rebornDataListTemp = CopyUtil.copyList(rebornDataList, RebornData.class);
        int ii = temp.intValue();
        indexFor:
        for (int index = 0; index < ii; index++) {
            if (temp.compareTo(BigDecimal.ZERO) > 0) {
                for (RebornData reborn : rebornDataListTemp) {
                    if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp()) || reborn.getPromGiftSendQtyTemp().compareTo(BigDecimal.ZERO) == 0) {
                        reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
                        //状态
                        reborn.setBillItemStatus("");
                        reborn.setStatus("");
                        //促销单号
                        reborn.setVoucherId("");
                        reborn.setGssdPmActivityId("");
                        reborn.setGssdPmId("");
                        //互斥条件
                        reborn.setGsphExclusion("");
                        //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                        reborn.setGssdPmActivityFlag("");
                        reborn.setGssdAmt(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                        reborn.setBillItemRate("100");
                        reborn.setGssdPrc2(reborn.getGssdPrc1());
                    }
                }
                //取价格最低的一个赠品
                for (int i = rebornDataListTemp.size() - 1; i >= 0; i--) {
                    if (temp.compareTo(BigDecimal.ZERO) > 0) {
                        for (RebornData reborn : rebornDataListTemp) {
                            reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
                        }
                        RebornData reborn = rebornDataListTemp.get(i);
                        for (GaiaSdPromGiftResult result : giftResultList) {
                            if (reborn.getGssdProId().equals(result.getGspgrProId())) {
                                if (ObjectUtil.isEmpty(reborn.getPromGiftAmtTemp())) {
                                    reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
                                }
                                if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
                                    reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                                }
                                // 判断剩余可送商品数
                                BigDecimal subQtyTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp());
                                //有可送商品 则加入计算
                                if (subQtyTemp.compareTo(BigDecimal.ZERO) > 0) {
                                    //赠送一个商品
                                    reborn.setPromGiftSendQtyTemp(reborn.getPromGiftSendQtyTemp().add(new BigDecimal("1")));
                                    //状态
                                    reborn.setBillItemStatus("促销");
                                    reborn.setStatus("促销");
                                    //促销单号
                                    reborn.setVoucherId(priceOutDatum.getGspusVoucherId());
                                    reborn.setGssdPmActivityId(priceOutDatum.getGspusVoucherId());
                                    reborn.setGssdPmId("PROM_GIFT");
                                    //互斥条件
                                    reborn.setGsphExclusion(priceOutDatum.getGsphExclusion());
                                    //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                                    reborn.setGssdPmActivityFlag(priceOutDatum.getGspusSeriesId());
                                    if (ObjectUtil.isNotEmpty(result.getGspgrGiftPrc())) { //促销价
                                        reborn.setGssdAmt(result.getGspgrGiftPrc().multiply(reborn.getPromGiftSendQtyTemp()).add(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp()))).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setGssdPrc2(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()), 2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setBillItemRate(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()).multiply(new BigDecimal(reborn.getGssdPrc1())), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                                    } else if (StringUtils.isNotEmpty(result.getGspgrGiftRebate())) { //折扣
                                        reborn.setGssdAmt(new BigDecimal(result.getGspgrGiftRebate()).divide(new BigDecimal("100")).multiply(new BigDecimal(reborn.getGssdPrc1())).multiply(reborn.getPromGiftSendQtyTemp()).add(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp()))).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setGssdPrc2(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()), 2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setBillItemRate(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()).multiply(new BigDecimal(reborn.getGssdPrc1())), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                                    } else { //0元
                                        reborn.setGssdAmt(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setGssdPrc2(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()), 2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                                        reborn.setBillItemRate(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()).multiply(new BigDecimal(reborn.getGssdPrc1())), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                                    }
                                    //计算条件商品数是否满足
                                    BigDecimal temp2 = tTemp.add(BigDecimal.ZERO);
                                    if (CollectionUtil.isNotEmpty(giftCondsList)) { //条件商品
                                        for (RebornData reborn2 : rebornDataListTemp) {
                                            if (temp2.compareTo(BigDecimal.ZERO) > 0) {
                                                for (GaiaSdPromGiftConds conds : giftCondsList) {
                                                    if (reborn2.getGssdProId().equals(conds.getGspgcProId())) {
                                                        if (ObjectUtil.isEmpty(reborn2.getPromGiftAmtTemp())) {
                                                            reborn2.setPromGiftAmtTemp(BigDecimal.ZERO);
                                                        }
                                                        if (ObjectUtil.isEmpty(reborn2.getPromGiftAmtTemp())) {
                                                            reborn2.setPromGiftAmtTemp(BigDecimal.ZERO);
                                                        }
                                                        if (ObjectUtil.isEmpty(reborn2.getPromGiftSendQtyTemp())) {
                                                            reborn2.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                                                        }
                                                        //判断剩余商品数是否大于条件数
                                                        BigDecimal subAmtTemp2 = new BigDecimal(reborn2.getGssdQty()).subtract(reborn2.getPromGiftSendQtyTemp()).multiply(new BigDecimal(reborn2.getGssdPrc1())).subtract(reborn2.getPromGiftAmtTemp());
                                                        if (subAmtTemp2.compareTo(BigDecimal.ZERO) > 0) {
                                                            if (subAmtTemp2.compareTo(temp2) > -1) {
                                                                reborn2.setPromGiftAmtTemp(reborn2.getPromGiftAmtTemp().add(temp2));
                                                                temp2 = BigDecimal.ZERO;
                                                            } else if (subAmtTemp2.compareTo(temp2) < 0) {
                                                                reborn2.setPromGiftAmtTemp(reborn2.getPromGiftAmtTemp().add(subAmtTemp2));
                                                                temp2 = temp2.subtract(subAmtTemp2);
                                                            }
                                                            //状态
                                                            reborn2.setBillItemStatus("促销");
                                                            reborn2.setStatus("促销");
                                                            //促销单号
                                                            reborn2.setVoucherId(priceOutDatum.getGspusVoucherId());
                                                            reborn2.setGssdPmActivityId(priceOutDatum.getGspusVoucherId());
                                                            reborn2.setGssdPmId("PROM_GIFT");
                                                            //互斥条件
                                                            reborn2.setGsphExclusion(priceOutDatum.getGsphExclusion());
                                                            //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                                                            reborn2.setGssdPmActivityFlag(priceOutDatum.getGspusSeriesId());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {//全场条件商品
                                        for (RebornData reborn2 : rebornDataListTemp) {
                                            if (temp2.compareTo(BigDecimal.ZERO) > 0) {
                                                if (ObjectUtil.isEmpty(reborn2.getPromGiftAmtTemp())) {
                                                    reborn2.setPromGiftAmtTemp(BigDecimal.ZERO);
                                                }
                                                if (ObjectUtil.isEmpty(reborn2.getPromGiftSendQtyTemp())) {
                                                    reborn2.setPromGiftSendQtyTemp(BigDecimal.ZERO);
                                                }
                                                //判断剩余商品数是否大于条件数
                                                BigDecimal subAmtTemp2 = new BigDecimal(reborn2.getGssdQty()).subtract(reborn2.getPromGiftSendQtyTemp()).multiply(new BigDecimal(reborn2.getGssdPrc1())).subtract(reborn2.getPromGiftAmtTemp());
                                                if (subAmtTemp2.compareTo(BigDecimal.ZERO) > 0) {
                                                    if (subAmtTemp2.compareTo(temp2) > -1) {
                                                        reborn2.setPromGiftAmtTemp(reborn2.getPromGiftAmtTemp().add(temp2));
                                                        temp2 = BigDecimal.ZERO;
                                                    } else if (subAmtTemp2.compareTo(temp2) < 0) {
                                                        reborn2.setPromGiftAmtTemp(reborn2.getPromGiftAmtTemp().add(subAmtTemp2));
                                                        temp2 = temp2.subtract(subAmtTemp2);
                                                    }
                                                    //状态
                                                    reborn2.setBillItemStatus("促销");
                                                    reborn2.setStatus("促销");
                                                    //促销单号
                                                    reborn2.setVoucherId(priceOutDatum.getGspusVoucherId());
                                                    reborn2.setGssdPmActivityId(priceOutDatum.getGspusVoucherId());
                                                    reborn2.setGssdPmId("PROM_GIFT");
                                                    //互斥条件
                                                    reborn2.setGsphExclusion(priceOutDatum.getGsphExclusion());
                                                    //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                                                    reborn2.setGssdPmActivityFlag(priceOutDatum.getGspusSeriesId());
                                                }
                                            }
                                        }
                                    }
                                    // 若满足 则赠送成立 数据刷入 填充行内所有值
                                    if (temp2.compareTo(BigDecimal.ZERO) == 0) {
                                        temp = temp.subtract(new BigDecimal("1"));
                                        rebornDataList.forEach(item -> {
                                            rebornDataListTemp.forEach(item2 -> {
                                                if (item.getGssdSerial().equals(item2.getGssdSerial())) {
                                                    item.setBillItemStatus(item2.getBillItemStatus());
                                                    item.setStatus(item2.getStatus());
                                                    //促销单号
                                                    item.setVoucherId(item2.getVoucherId());
                                                    item.setGssdPmActivityId(item2.getGssdPmActivityId());
                                                    item.setGssdPmId(item2.getGssdPmId());
                                                    //互斥条件
                                                    item.setGsphExclusion(item2.getGsphExclusion());
                                                    //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
                                                    item.setGssdPmActivityFlag(item2.getGssdPmActivityFlag());
                                                    item.setGssdPrc2(item2.getGssdPrc2());
                                                    item.setGssdAmt(item2.getGssdAmt());
                                                    item.setBillItemRate(item2.getBillItemRate());
                                                    item.setPromGiftSendQtyTemp(item2.getPromGiftSendQtyTemp());
                                                    item.setPromGiftAmtTemp(item2.getPromGiftAmtTemp());
                                                }
                                            });
                                        });
                                        continue indexFor;//跳转到外层 继续计算下一个赠送商品
                                    } else { // 不满足 则截止
                                        return;
                                    }
                                } else { //无可送商品则跳出
                                    break;
                                }
                            }
                        }
                    }
                }
            } else break;
        }
        if ("repeat1".equals(head.getGsphPara1())) {
            if (judgeGiftPromotion2(condTemp, resultTemp, rebornDataList, giftCondsList, giftResultList, isMember)) {
                calculateGiftPromotion2(condTemp, resultTemp, rebornDataList, giftCondsList, giftResultList, priceOutDatum, head, isMember);
            }
        }
        //        // 遍历统计计算赠送商品的数量是否满足数量条件
//        BigDecimal temp = new BigDecimal(resultTemp); // 赠送数量缓存 用以计算
//        for (int i = rebornDataList.size() - 1; i >= 0; i--) {
//            RebornData reborn = rebornDataList.get(i);
//            if(temp.compareTo(BigDecimal.ZERO)>0) { //判断 赠送数量 是否存在
//                for (GaiaSdPromGiftResult result : giftResultList) {
//                    if (reborn.getGssdProId().equals(result.getGspgrProId())) {
//                        if (ObjectUtil.isEmpty(reborn.getPromGiftAmtTemp())) {
//                            reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
//                        }
//                        if (ObjectUtil.isEmpty(reborn.getPromGiftQtyTemp())) {
//                            reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
//                        }
//                        if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
//                            reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
//                        }
//                        //判断剩余商品数是否大于赠送数 需要减去条件商品数量后计算
//                        BigDecimal subQtyTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftQtyTemp()).subtract(reborn.getPromGiftSendQtyTemp()).subtract(reborn.getPromGiftAmtTemp().divide(new BigDecimal(reborn.getGssdPrc1()), 0, BigDecimal.ROUND_UP));
//                        if (subQtyTemp.compareTo(BigDecimal.ZERO) > 0) {
//                            if (subQtyTemp.compareTo(temp) > -1) {
//                                reborn.setPromGiftSendQtyTemp(reborn.getPromGiftSendQtyTemp().add(temp));
//                                temp = BigDecimal.ZERO;
//                            } else if (subQtyTemp.compareTo(temp) < 0) {
//                                reborn.setPromGiftSendQtyTemp(reborn.getPromGiftSendQtyTemp().add(subQtyTemp));
//                                temp = temp.subtract(subQtyTemp);
//                            }
//                            //状态
//                            reborn.setBillItemStatus("促销");
//                            reborn.setStatus("促销");
//                            //促销单号
//                            reborn.setVoucherId(priceOutDatum.getGspusVoucherId());
//                            reborn.setGssdPmActivityId(priceOutDatum.getGspusVoucherId());
//                            reborn.setGssdPmId("PROM_GIFT");
//                            //互斥条件
//                            reborn.setGsphExclusion(priceOutDatum.getGsphExclusion());
//                            //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
//                            reborn.setGssdPmActivityFlag(priceOutDatum.getGspusSeriesId());
//                            if (ObjectUtil.isNotEmpty(result.getGspgrGiftPrc())) { //促销价
//                                reborn.setGssdAmt(result.getGspgrGiftPrc().multiply(reborn.getPromGiftSendQtyTemp()).add(new BigDecimal(reborn.getGssdPrc1()).multiply((new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp())))).setScale(2,BigDecimal.ROUND_HALF_UP).toString());
//                                reborn.setGssdPrc2(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()),2,BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
//                                reborn.setBillItemRate(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()).multiply(new BigDecimal(reborn.getGssdPrc1())),2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
//                            } else if (StringUtils.isNotEmpty(result.getGspgrGiftRebate())) { //折扣
//                                reborn.setGssdAmt(new BigDecimal(result.getGspgrGiftRebate()).divide(new BigDecimal("100")).multiply(new BigDecimal(reborn.getGssdPrc1())).multiply(reborn.getPromGiftSendQtyTemp()).add(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp()))).setScale(2,BigDecimal.ROUND_HALF_UP).toString());
//                                reborn.setGssdPrc2(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()),2,BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
//                                reborn.setBillItemRate(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()).multiply(new BigDecimal(reborn.getGssdPrc1())),2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
//                            } else { //0元
//                                reborn.setGssdAmt(new BigDecimal(reborn.getGssdPrc1()).multiply(new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftSendQtyTemp())).setScale(2,BigDecimal.ROUND_HALF_UP).toString());
//                                reborn.setGssdPrc2(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()),2,BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
//                                reborn.setBillItemRate(new BigDecimal(reborn.getGssdAmt()).divide(new BigDecimal(reborn.getGssdQty()).multiply(new BigDecimal(reborn.getGssdPrc1())),2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
//                            }
//                        }
//                        break; //只要有匹配 就跳过内层循环
//                    }
//                }
//            }
//        }
//        //遍历统计计算条件商品的数量是否满足数量条件
//        temp = new BigDecimal(condTemp); //条件数量缓存 用以计算
//        if (CollectionUtil.isNotEmpty(giftCondsList)) {
//            for (RebornData reborn : rebornDataList) {
//                if(temp.compareTo(BigDecimal.ZERO)>0) { //判断 赠送数量 是否存在
//                    for (GaiaSdPromGiftConds conds : giftCondsList) {
//                        if (reborn.getGssdProId().equals(conds.getGspgcProId())) {
//                            if (ObjectUtil.isEmpty(reborn.getPromGiftAmtTemp())) {
//                                reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
//                            }
//                            if (ObjectUtil.isEmpty(reborn.getPromGiftQtyTemp())) {
//                                reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
//                            }
//                            if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
//                                reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
//                            }
//                            //判断剩余商品金额是否大于条件金额 但要减去其他赠送的数量后计算
//                            BigDecimal subAmtTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftQtyTemp()).subtract(reborn.getPromGiftSendQtyTemp()).multiply(new BigDecimal(reborn.getGssdPrc1())).subtract(reborn.getPromGiftAmtTemp());
//                            if (subAmtTemp.compareTo(BigDecimal.ZERO) > 0) {
//                                if (subAmtTemp.compareTo(temp) > -1) {
//                                    reborn.setPromGiftQtyTemp(reborn.getPromGiftQtyTemp().add(temp));
//                                    temp = BigDecimal.ZERO;
//                                } else if (subAmtTemp.compareTo(temp) < 0) {
//                                    reborn.setPromGiftQtyTemp(reborn.getPromGiftQtyTemp().add(subAmtTemp));
//                                    temp = temp.subtract(subAmtTemp);
//                                }
//                                //状态
//                                reborn.setBillItemStatus("促销");
//                                reborn.setStatus("促销");
//                                //促销单号
//                                reborn.setVoucherId(priceOutDatum.getGspusVoucherId());
//                                reborn.setGssdPmActivityId(priceOutDatum.getGspusVoucherId());
//                                reborn.setGssdPmId("PROM_GIFT");
//                                //互斥条件
//                                reborn.setGsphExclusion(priceOutDatum.getGsphExclusion());
//                                //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
//                                reborn.setGssdPmActivityFlag(priceOutDatum.getGspusSeriesId());
//                            }
//                            break;
//                        }
//                    }
//                }
//            }
//        } else {
//            for (RebornData reborn : rebornDataList) {
//                if(temp.compareTo(BigDecimal.ZERO)>0) { //判断 赠送数量 是否存在
//                    if (ObjectUtil.isEmpty(reborn.getPromGiftAmtTemp())) {
//                        reborn.setPromGiftAmtTemp(BigDecimal.ZERO);
//                    }
//                    if (ObjectUtil.isEmpty(reborn.getPromGiftQtyTemp())) {
//                        reborn.setPromGiftQtyTemp(BigDecimal.ZERO);
//                    }
//                    if (ObjectUtil.isEmpty(reborn.getPromGiftSendQtyTemp())) {
//                        reborn.setPromGiftSendQtyTemp(BigDecimal.ZERO);
//                    }
//                    //判断剩余商品金额是否大于条件金额 但要减去其他赠送的数量后计算
//                    BigDecimal subAmtTemp = new BigDecimal(reborn.getGssdQty()).subtract(reborn.getPromGiftQtyTemp()).subtract(reborn.getPromGiftSendQtyTemp()).multiply(new BigDecimal(reborn.getGssdPrc1())).subtract(reborn.getPromGiftAmtTemp());
//                    if (subAmtTemp.compareTo(BigDecimal.ZERO) > 0) {
//                        if (subAmtTemp.compareTo(temp) > -1) {
//                            reborn.setPromGiftQtyTemp(reborn.getPromGiftQtyTemp().add(temp));
//                            temp = BigDecimal.ZERO;
//                        } else if (subAmtTemp.compareTo(temp) < 0) {
//                            reborn.setPromGiftQtyTemp(reborn.getPromGiftQtyTemp().add(subAmtTemp));
//                            temp = temp.subtract(subAmtTemp);
//                        }
//                        //状态
//                        reborn.setBillItemStatus("促销");
//                        reborn.setStatus("促销");
//                        //促销单号
//                        reborn.setVoucherId(priceOutDatum.getGspusVoucherId());
//                        reborn.setGssdPmActivityId(priceOutDatum.getGspusVoucherId());
//                        reborn.setGssdPmId("PROM_GIFT");
//                        //互斥条件
//                        reborn.setGsphExclusion(priceOutDatum.getGsphExclusion());
//                        //促销参数  赠品促销取 GAIA_SD_PROM_GIFT_CONDS表SERIES_ID	系列编码
//                        reborn.setGssdPmActivityFlag(priceOutDatum.getGspusSeriesId());
//                    }
//                }
//            }
//        }
//
//        if ("repeat1".equals(head.getGsphPara1())) {
//            if (judgeGiftPromotion2(condTemp, resultTemp, rebornDataList, giftCondsList, giftResultList)) {
//                calculateGiftPromotion2(condTemp, resultTemp, rebornDataList, giftCondsList, giftResultList, priceOutDatum, head);
//            }
//        }
    }

    @Override
    public List<GiftPromOutData> queryGiftpromotion(PromotionMethodInData inData) {
        //查询当前赠品促销活动
        List<GiftPromOutData> result = new ArrayList<>();
        List<PromotionalConditionData> promotionalConditionDataList = promHeadMapper.findAllGiftPromotionByExclusion(inData);
        promotionalConditionDataList.forEach((item) -> {
            BigDecimal totalQty = BigDecimal.ZERO;//条件商品总数
            BigDecimal totalAmt = BigDecimal.ZERO;//条件商品总金额
            //遍历当前商品列表 查看是否满足条件
            if ("assign1".equals(item.getGsphPara3())) { //条件商品为全场商品
                totalQty = inData.getRebornDataList().stream().map(reborn -> new BigDecimal(reborn.getGssdQty())).reduce(BigDecimal.ZERO, BigDecimal::add); //当前商品总数
                totalAmt = inData.getRebornDataList().stream().map(reborn -> new BigDecimal(reborn.getGssdAmt())).reduce(BigDecimal.ZERO, BigDecimal::add); //当前商品总金额

            } else if ("assign2".equals(item.getGsphPara3())) { //条件商品指定商品
                // 查询条件商品
                List<GaiaSdPromGiftConds> giftCondsList = promHeadMapper.findAllGiftConditionProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId(), inData.getIsMember());
                for (RebornData reborn : inData.getRebornDataList()) {
                    for (GaiaSdPromGiftConds cond : giftCondsList) {
                        if (cond.getGspgcProId().equals(reborn.getGssdProId())) {
                            totalQty = totalQty.add(new BigDecimal(reborn.getGssdQty()));
                        }
                    }
                }
                for (RebornData reborn : inData.getRebornDataList()) {
                    for (GaiaSdPromGiftConds cond : giftCondsList) {
                        if (cond.getGspgcProId().equals(reborn.getGssdProId())) {
                            totalAmt = totalAmt.add(new BigDecimal(reborn.getGssdAmt()));
                        }
                    }
                }
            }
            if ("3".equals(item.getPart())) { //三阶段
                if (StringUtils.isNotEmpty(item.getQtyThreeStage())) { //三阶数量条件
                    BigDecimal qtyThreeStage = new BigDecimal(item.getQtyThreeStage());
                    BigDecimal qtyTwoStage = new BigDecimal(item.getQtyTwoStage());
                    BigDecimal qtyOneStage = new BigDecimal(item.getQtyOneStage());
                    if (totalQty.compareTo(qtyThreeStage) > -1) { //检查是否满足第三阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalQty.divideAndRemainder(qtyThreeStage)[0].multiply(new BigDecimal(item.getQtyThree()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtThreeStage() == null ? null : new BigDecimal(item.getAmtThreeStage()));
                                gift.setReachQty(item.getQtyThreeStage() == null ? null : new BigDecimal(item.getQtyThreeStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyThree());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtThreeStage() == null ? null : new BigDecimal(item.getAmtThreeStage()));
                                gift.setReachQty(item.getQtyThreeStage() == null ? null : new BigDecimal(item.getQtyThreeStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    } else if (totalQty.compareTo(qtyTwoStage) > -1) { //检查是否满足第二阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalQty.divideAndRemainder(qtyTwoStage)[0].multiply(new BigDecimal(item.getQtyTwo())); // 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtTwoStage() == null ? null : new BigDecimal(item.getAmtTwoStage()));
                                gift.setReachQty(item.getQtyTwoStage() == null ? null : new BigDecimal(item.getQtyTwoStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else { //不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyTwo());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtTwoStage() == null ? null : new BigDecimal(item.getAmtTwoStage()));
                                gift.setReachQty(item.getQtyTwoStage() == null ? null : new BigDecimal(item.getQtyTwoStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    } else if (totalQty.compareTo(qtyOneStage) > -1) {//检查是否满足第一阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalQty.divideAndRemainder(qtyOneStage)[0].multiply(new BigDecimal(item.getQtyOne()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyOne());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    }
                } else if (StringUtils.isNotEmpty(item.getAmtThreeStage())) { //三阶金额条件
                    BigDecimal amtThreeStage = new BigDecimal(item.getAmtThreeStage());
                    BigDecimal amtTwoStage = new BigDecimal(item.getAmtTwoStage());
                    BigDecimal amtOneStage = new BigDecimal(item.getAmtOneStage());
                    if (totalAmt.compareTo(amtThreeStage) > -1) { //检查是否满足第三阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalAmt.divideAndRemainder(amtThreeStage)[0].multiply(new BigDecimal(item.getQtyThree()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtThreeStage() == null ? null : new BigDecimal(item.getAmtThreeStage()));
                                gift.setReachQty(item.getQtyThreeStage() == null ? null : new BigDecimal(item.getQtyThreeStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyThree());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtThreeStage() == null ? null : new BigDecimal(item.getAmtThreeStage()));
                                gift.setReachQty(item.getQtyThreeStage() == null ? null : new BigDecimal(item.getQtyThreeStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    } else if (totalAmt.compareTo(amtTwoStage) > -1) {//检查是否满足第二阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalAmt.divideAndRemainder(amtTwoStage)[0].multiply(new BigDecimal(item.getQtyTwo()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtTwoStage() == null ? null : new BigDecimal(item.getAmtTwoStage()));
                                gift.setReachQty(item.getQtyTwoStage() == null ? null : new BigDecimal(item.getQtyTwoStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyTwo());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtTwoStage() == null ? null : new BigDecimal(item.getAmtTwoStage()));
                                gift.setReachQty(item.getQtyTwoStage() == null ? null : new BigDecimal(item.getQtyTwoStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    } else if (totalAmt.compareTo(amtOneStage) > -1) {//检查是否满足第一阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalAmt.divideAndRemainder(amtOneStage)[0].multiply(new BigDecimal(item.getQtyOne()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyOne());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    }
                }
            } else if ("2".equals(item.getPart())) { //2阶段
                if (StringUtils.isNotEmpty(item.getQtyTwoStage())) { //二阶数量条件
                    BigDecimal qtyTwoStage = new BigDecimal(item.getQtyTwoStage());
                    BigDecimal qtyOneStage = new BigDecimal(item.getQtyOneStage());
                    if (totalQty.compareTo(qtyTwoStage) > -1) { //检查是否满足第二阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalQty.divideAndRemainder(qtyTwoStage)[0].multiply(new BigDecimal(item.getQtyTwo())); // 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtTwoStage() == null ? null : new BigDecimal(item.getAmtTwoStage()));
                                gift.setReachQty(item.getQtyTwoStage() == null ? null : new BigDecimal(item.getQtyTwoStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else { //不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyTwo());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtTwoStage() == null ? null : new BigDecimal(item.getAmtTwoStage()));
                                gift.setReachQty(item.getQtyTwoStage() == null ? null : new BigDecimal(item.getQtyTwoStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    } else if (totalQty.compareTo(qtyOneStage) > -1) {//检查是否满足第一阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalQty.divideAndRemainder(qtyOneStage)[0].multiply(new BigDecimal(item.getQtyOne()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyOne());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    }
                } else if (StringUtils.isNotEmpty(item.getAmtTwoStage())) { //二阶金额条件
                    BigDecimal amtTwoStage = new BigDecimal(item.getAmtTwoStage());
                    BigDecimal amtOneStage = new BigDecimal(item.getAmtOneStage());
                    if (totalAmt.compareTo(amtTwoStage) > -1) {//检查是否满足第二阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalAmt.divideAndRemainder(amtTwoStage)[0].multiply(new BigDecimal(item.getQtyTwo()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtTwoStage() == null ? null : new BigDecimal(item.getAmtTwoStage()));
                                gift.setReachQty(item.getQtyTwoStage() == null ? null : new BigDecimal(item.getQtyTwoStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyTwo());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtTwoStage() == null ? null : new BigDecimal(item.getAmtTwoStage()));
                                gift.setReachQty(item.getQtyTwoStage() == null ? null : new BigDecimal(item.getQtyTwoStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    } else if (totalAmt.compareTo(amtOneStage) > -1) {//检查是否满足第一阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalAmt.divideAndRemainder(amtOneStage)[0].multiply(new BigDecimal(item.getQtyOne()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyOne());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    }
                }
            } else if ("1".equals(item.getPart())) { //1阶段
                if (StringUtils.isNotEmpty(item.getQtyOneStage())) { //一阶数量条件
                    BigDecimal qtyOneStage = new BigDecimal(item.getQtyOneStage());
                    if (totalQty.compareTo(qtyOneStage) > -1) {//检查是否满足第一阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalQty.divideAndRemainder(qtyOneStage)[0].multiply(new BigDecimal(item.getQtyOne()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyOne());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    }
                } else if (StringUtils.isNotEmpty(item.getAmtOneStage())) { //一阶金额条件
                    BigDecimal amtOneStage = new BigDecimal(item.getAmtOneStage());
                    if (totalAmt.compareTo(amtOneStage) > -1) {//检查是否满足第一阶梯
                        //计算最大赠送数
                        if ("repeat1".equals(item.getGsphPara1())) { //重复送
                            BigDecimal maxQty = totalAmt.divideAndRemainder(amtOneStage)[0].multiply(new BigDecimal(item.getQtyOne()));// 最大赠送数 = 取模*阶段赠送数
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        } else {//不重复送
                            BigDecimal maxQty = new BigDecimal(item.getQtyOne());
                            // 查询赠品
                            List<GiftPromOutData> giftPromOutDataList = promHeadMapper.findAllGiftSendProductByExclusion(inData.getClient(), inData.getBrId(), item.getVoucherId(), item.getSeriesId());
                            // 遍历商品列表
                            giftPromOutDataList.forEach(gift -> {
                                gift.setReachAmt(item.getAmtOneStage() == null ? null : new BigDecimal(item.getAmtOneStage()));
                                gift.setReachQty(item.getQtyOneStage() == null ? null : new BigDecimal(item.getQtyOneStage()));
                                gift.setMovTax(new BigDecimal(gift.getMovTax().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                                gift.setPara1(item.getGsphPara1());
                                gift.setPara2(item.getGsphPara2());
                                gift.setPara3(item.getGsphPara3());
                                gift.setPara4(item.getGsphPara4());
                                gift.setExclusion(item.getGsphExclusion());
                                gift.setMaxQty(maxQty);
                                result.add(gift);
                            });
                        }
                    }
                }
            }
        });
        //筛选符合条件的促销
        return result;
    }

    @Override
    @Transactional
    public boolean checkPromLimit(CheckPromLimitInData inData) {
//        if(inData==null || ObjectUtil.isEmpty(inData.getMemberCard())) {
//            throw new BusinessException("会员卡号为空！");
//        }
//        if(ObjectUtil.isEmpty(inData.getPromId())) {
//            throw new BusinessException("促销单号为空！");
//        }
//        if(ObjectUtil.isEmpty(inData.getProId())) {
//            throw new BusinessException("商品号为空！");
//        }
//        if(ObjectUtil.isEmpty(inData.getStartDate())) {
//            throw new BusinessException("促销开始时间为空！");
//        }
//        if(ObjectUtil.isEmpty(inData.getEndDate())) {
//            throw new BusinessException("促销结束时间为空！");
//        }
        Example example = new Example(GaiaSdPromHead.class);
        example.createCriteria()
                .andEqualTo("clientId",inData.getClient())
                .andEqualTo("gsphBrId",inData.getBrId())
                .andEqualTo("gsphVoucherId",inData.getPromId())
                .andEqualTo("gsphType",inData.getPromType());
        GaiaSdPromHead promHead = promHeadMapper.selectOneByExample(example);
        if("PROM_SINGLE".equals(inData.getPromType()) && ObjectUtil.isNotEmpty(promHead)) {
            //单品促销
            List<GaiaSdSaleH> saleHList;
            example = new Example(GaiaSdSaleH.class);
            if("1".equals(inData.getLimitType())) {
                example.createCriteria()
                        .andEqualTo("clientId",inData.getClient())
                        .andEqualTo("gsshHykNo",inData.getMemberCard())
                        .andGreaterThanOrEqualTo("gsshDate", promHead.getGsphBeginDate())
                        .andLessThanOrEqualTo("gsshDate", promHead.getGsphEndDate())
                        .andEqualTo("gsshHideFlag","0");
                saleHList = saleHMapper.selectByExample(example);
            }else {
                example.createCriteria()
                        .andEqualTo("clientId",inData.getClient())
                        .andEqualTo("gsshHykNo",inData.getMemberCard())
                        .andEqualTo("gsshDate", com.gys.util.DateUtil.getCurrentDateStr("yyyyMMdd"))
                        .andEqualTo("gsshHideFlag","0");
                saleHList = saleHMapper.selectByExample(example);
            }

            if(CollUtil.isNotEmpty(saleHList)) {
                example = new Example(GaiaSdSaleD.class);
                example.createCriteria()
                        .andEqualTo("clientId", inData.getClient())
                        .andEqualTo("gssdProId", inData.getProId())
                        .andEqualTo("gssdPmActivityId", inData.getPromId())
                        .andIn("gssdBillNo", saleHList.stream().map(GaiaSdSaleH::getGsshBillNo).collect(Collectors.toList()));
                List<GaiaSdSaleD> saleDList = saleDMapper.selectByExample(example);
                if(CollUtil.isNotEmpty(saleDList)) {
                    return false;
                }else {
                    return true;
                }
            }else {
                return true;
            }
        }else {
            return true;
        }
    }
}
