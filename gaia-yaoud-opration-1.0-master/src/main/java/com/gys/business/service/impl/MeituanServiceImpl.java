//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.MeituanService;
import com.gys.business.service.data.GetMeituanInData;
import com.gys.business.service.data.GetMeituanOutData;
import com.gys.common.exception.BusinessException;
import com.gys.common.webSocket.WebSocket;
//import com.sankuai.meituan.waimai.opensdk.exception.ApiOpException;
//import com.sankuai.meituan.waimai.opensdk.exception.ApiSysException;
//import com.sankuai.meituan.waimai.opensdk.factory.APIFactory;
//import com.sankuai.meituan.waimai.opensdk.vo.OrderDetailParam;
//import com.sankuai.meituan.waimai.opensdk.vo.SystemParam;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MeituanServiceImpl {
//    @Resource
//    private WebSocket webSocket;
//
//    public MeituanServiceImpl() {
//    }
//
//    @Transactional
//    public void orderPayedPush(String params) {
//        JSONObject object = JSONObject.parseObject(params);
//        String appId = object.getString("app_id");
//        GetMeituanOutData outData = new GetMeituanOutData();
//        outData.setParams(params);
//        outData.setSource("1");
//        outData.setType("orderPayedPush");
//        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
//    }
//
//    @Transactional
//    public void orderReminderPush(String params) {
//        JSONObject object = JSONObject.parseObject(params);
//        String appId = object.getString("app_id");
//        GetMeituanOutData outData = new GetMeituanOutData();
//        outData.setParams(params);
//        outData.setSource("1");
//        outData.setType("orderReminderPush");
//        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
//    }
//
//    @Transactional
//    public void orderCancelPush(String params) {
//        JSONObject object = JSONObject.parseObject(params);
//        String appId = object.getString("app_id");
//        GetMeituanOutData outData = new GetMeituanOutData();
//        outData.setParams(params);
//        outData.setSource("1");
//        outData.setType("orderCancelPush");
//        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
//    }
//
//    @Transactional
//    public void orderRefundAllPush(String params) {
//        JSONObject object = JSONObject.parseObject(params);
//        String appId = object.getString("app_id");
//        GetMeituanOutData outData = new GetMeituanOutData();
//        outData.setParams(params);
//        outData.setSource("1");
//        outData.setType("orderRefundAllPush");
//        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
//    }
//
//    @Transactional
//    public void orderRefundPush(String params) {
//        JSONObject object = JSONObject.parseObject(params);
//        String appId = object.getString("app_id");
//        GetMeituanOutData outData = new GetMeituanOutData();
//        outData.setParams(params);
//        outData.setSource("1");
//        outData.setType("orderRefundPush");
//        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
//    }
//
//    @Transactional
//    public void deliveryStatusPush(String params) {
//        JSONObject object = JSONObject.parseObject(params);
//        String appId = object.getString("app_id");
//        GetMeituanOutData outData = new GetMeituanOutData();
//        outData.setParams(params);
//        outData.setSource("1");
//        outData.setType("deliveryStatusPush");
//        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
//    }
//
//    @Transactional
//    public void orderStatusPush(String params) {
//        JSONObject object = JSONObject.parseObject(params);
//        String appId = object.getString("app_id");
//        GetMeituanOutData outData = new GetMeituanOutData();
//        outData.setParams(params);
//        outData.setSource("1");
//        outData.setType("orderStatusPush");
//        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
//    }
//
//    @Transactional
//    public void orderGetOrderDetail(GetMeituanInData inData) {
//        //chenhao TODO
////        int code;
////        String msg;
////        try {
////            SystemParam sysPram = new SystemParam("这里请填写app_id", "这里请填写门店secret");
////            OrderDetailParam orderDetailParam = APIFactory.getOrderAPI().orderGetOrderDetail(sysPram, inData.getOrderId().toString(), 0L);
////            System.out.println(orderDetailParam);
////            if (ObjectUtil.isEmpty(orderDetailParam)) {
////                throw new BusinessException("获取订单详情失败！");
////            }
////        } catch (ApiOpException var5) {
////            code = var5.getCode();
////            msg = var5.getMsg();
////            throw new BusinessException(msg);
////        } catch (ApiSysException var6) {
////            code = var6.getExceptionEnum().getCode();
////            msg = var6.getExceptionEnum().getMsg();
////            throw new BusinessException(msg);
////        }
//    }
//
//    @Transactional
//    public void orderConfirm(GetMeituanInData inData) {
//        int code;
//        String msg;
//        try {
//            SystemParam sysPram = new SystemParam("这里请填写app_id", "这里请填写门店secret");
//            String result = APIFactory.getOrderAPI().orderConfirm(sysPram, inData.getOrderId());
//            System.out.println(result);
//            JSONObject object = JSONObject.parseObject(result);
//            if (!"ok".equals(object.getString("data"))) {
//                throw new BusinessException(object.getString("msg"));
//            }
//        } catch (ApiOpException var5) {
//            code = var5.getCode();
//            msg = var5.getMsg();
//            throw new BusinessException(msg);
//        } catch (ApiSysException var6) {
//            code = var6.getExceptionEnum().getCode();
//            msg = var6.getExceptionEnum().getMsg();
//            throw new BusinessException(msg);
//        }
//    }
//
//    @Transactional
//    public void orderCancel(GetMeituanInData inData) {
//        int code;
//        String msg;
//        try {
//            SystemParam sysPram = new SystemParam("这里请填写app_id", "这里请填写门店secret");
//            String result = APIFactory.getOrderAPI().orderCancel(sysPram, inData.getOrderId(), inData.getReason(), inData.getReasonCode());
//            System.out.println(result);
//            JSONObject object = JSONObject.parseObject(result);
//            if (!"ok".equals(object.getString("data"))) {
//                throw new BusinessException(object.getString("msg"));
//            }
//        } catch (ApiOpException var5) {
//            code = var5.getCode();
//            msg = var5.getMsg();
//            throw new BusinessException(msg);
//        } catch (ApiSysException var6) {
//            code = var6.getExceptionEnum().getCode();
//            msg = var6.getExceptionEnum().getMsg();
//            throw new BusinessException(msg);
//        }
//    }
//
//    @Transactional
//    public void orderPreparationMealComplete(GetMeituanInData inData) {
//        int code;
//        String msg;
//        try {
//            SystemParam sysPram = new SystemParam("这里请填写app_id", "这里请填写门店secret");
//            String result = APIFactory.getOrderAPI().orderPreparationMealComplete(sysPram, inData.getOrderId().toString());
//            System.out.println(result);
//            JSONObject object = JSONObject.parseObject(result);
//            if (!"ok".equals(object.getString("data"))) {
//                throw new BusinessException(object.getString("msg"));
//            }
//        } catch (ApiOpException var5) {
//            code = var5.getCode();
//            msg = var5.getMsg();
//            throw new BusinessException(msg);
//        } catch (ApiSysException var6) {
//            code = var6.getExceptionEnum().getCode();
//            msg = var6.getExceptionEnum().getMsg();
//            throw new BusinessException(msg);
//        }
//    }
//
//    @Transactional
//    public void orderRefundAgree(GetMeituanInData inData) {
//        int code;
//        String msg;
//        try {
//            SystemParam sysPram = new SystemParam("这里请填写app_id", "这里请填写门店secret");
//            String result = APIFactory.getOrderAPI().orderRefundAgree(sysPram, inData.getOrderId(), inData.getReason());
//            System.out.println(result);
//            JSONObject object = JSONObject.parseObject(result);
//            if (!"ok".equals(object.getString("data"))) {
//                throw new BusinessException(object.getString("msg"));
//            }
//        } catch (ApiOpException var5) {
//            code = var5.getCode();
//            msg = var5.getMsg();
//            throw new BusinessException(msg);
//        } catch (ApiSysException var6) {
//            code = var6.getExceptionEnum().getCode();
//            msg = var6.getExceptionEnum().getMsg();
//            throw new BusinessException(msg);
//        }
//    }
//
//    @Transactional
//    public void orderRefundReject(GetMeituanInData inData) {
//        int code;
//        String msg;
//        try {
//            SystemParam sysPram = new SystemParam("这里请填写app_id", "这里请填写门店secret");
//            String result = APIFactory.getOrderAPI().orderRefundReject(sysPram, inData.getOrderId(), inData.getReason());
//            System.out.println(result);
//            JSONObject object = JSONObject.parseObject(result);
//            if (!"ok".equals(object.getString("data"))) {
//                throw new BusinessException(object.getString("msg"));
//            }
//        } catch (ApiOpException var5) {
//            code = var5.getCode();
//            msg = var5.getMsg();
//            throw new BusinessException(msg);
//        } catch (ApiSysException var6) {
//            code = var6.getExceptionEnum().getCode();
//            msg = var6.getExceptionEnum().getMsg();
//            throw new BusinessException(msg);
//        }
//    }
//
//    @Transactional
//    public void medicineStock(GetMeituanInData inData) {
//        int code;
//        String msg;
//        try {
//            SystemParam sysPram = new SystemParam("这里请填写app_id", "这里请填写门店secret");
//            String result = APIFactory.getMedicineAPI().medicineStock(sysPram, inData.getStoreCode(), inData.getReason());
//            System.out.println(result);
//            JSONObject object = JSONObject.parseObject(result);
//            if (!"ok".equals(object.getString("data"))) {
//                throw new BusinessException(object.getString("msg"));
//            }
//        } catch (ApiOpException var5) {
//            code = var5.getCode();
//            msg = var5.getMsg();
//            throw new BusinessException(msg);
//        } catch (ApiSysException var6) {
//            code = var6.getExceptionEnum().getCode();
//            msg = var6.getExceptionEnum().getMsg();
//            throw new BusinessException(msg);
//        }
//    }
}
