package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


@Data
public class CashboxOutData {
    private String clientId;    //
    private String gpdhBrId;

    private String roidId ; //钱箱号
    private String gsIeshBrId ; //门店编号
    private String counterId ; // 机台号
    private BigDecimal cashBoxMoney ; // 钱箱金额
    private BigDecimal handoverAmount ; // 交班金额
    private BigDecimal balance ; //剩余金额 (收款金额减去交班金额)
    private BigDecimal pettyCashB ; //备用金
    private String remarks ; // 备注
    private String cashierId ; //收银员
    private Date handoverTime ; //交班时间
    private Date lastShiftTime ; //上一班时间
    
    private BigDecimal lastShiftMoney; //上一班金额
    private BigDecimal chashBoxMoney;   //现金来源
    private BigDecimal cashSource;  //现金来源
}
