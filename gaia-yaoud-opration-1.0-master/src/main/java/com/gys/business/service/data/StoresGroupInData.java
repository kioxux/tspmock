package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 门店店型 入参
 *
 * @author XiaoZY on 2021/05/28
 */
@Data
public class StoresGroupInData implements Serializable {


    @ApiModelProperty(value = "加盟商编码")
    private String clientId;

    private String groupType;

}
