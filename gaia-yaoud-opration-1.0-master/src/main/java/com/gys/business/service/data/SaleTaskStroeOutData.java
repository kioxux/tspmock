package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class SaleTaskStroeOutData {
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "门店简称")
    private String stoName;
    @ApiModelProperty(value = "店型")
    private String state;
    @ApiModelProperty(value = "开业时间")
    private String openData;
}
