package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 返回实体
 *
 * @author xiaoyuan on 2020/8/3
 */
@Data
public class ReceiptOutData implements Serializable {
    private static final long serialVersionUID = 2388996582929656176L;

    /**
     * 加盟号
     */
    private String client;

    /**
     * 店号
     */
    private String gsshBrId;

    /**
     * 订单号
     */
    private String gsshBillNo;

    /**
     * 零售额总计
     */
    private String gsshNormalAmt;

    /**
     * 实收金额总计
     */
    private String gsshYsAmt;

    /**
     * 折让金额总计
     */
    private String gsshZkAmt;
}
