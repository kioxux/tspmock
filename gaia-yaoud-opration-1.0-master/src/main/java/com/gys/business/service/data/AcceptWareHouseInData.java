package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaColdTransportationInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AcceptWareHouseInData {
    @ApiModelProperty("加盟商")
    private String clientId;
    @ApiModelProperty("门店编码")
    private String stoCode;
    @ApiModelProperty("配送单号")
    private String psVoucherId;
    @ApiModelProperty("单据状态")
    private String psStatus;
    @ApiModelProperty("收货日期")
    private String gsahDate;
    @ApiModelProperty("拣货单号")
    private String pickProId;
    @ApiModelProperty("收货人")
    private String userId;
    @ApiModelProperty("起始日期")
    private String startDate;
    @ApiModelProperty("结束日期")
    private String endDate;
    @ApiModelProperty("批号")
    private String batchNo;
    @ApiModelProperty("商品编码、名称等模糊搜索")
    private String nameOrCode;
    @ApiModelProperty("商品编码、名称等模糊搜索")
    private GaiaColdTransportationInfo coldInfo;
    @ApiModelProperty("收货人姓名")
    private String userName;
    @ApiModelProperty("地点-配送中心编码")
    private String dcCode;
    //仓库收货明细
    private List<AcceptWareHouseProDetailData> acceptWareHouseProDetailList;
    //拒收入参
    private List<AcceptWareSaveWtpsRefuseInData> refuseInDataList;
}
