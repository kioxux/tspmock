//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromUnitarySetOutData implements Serializable {
    private static final long serialVersionUID = 1334278535052376272L;
    private String client;
    private String gspusVoucherId;
    private String gspusSerial;
    private String gspusProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private String gspusVaild;
    private String gspusQty1;
    private BigDecimal gspusPrc1;
    private String gspusRebate1;
    private String gspusQty2;
    private BigDecimal gspusPrc2;
    private String gspusRebate2;
    private String gspusQty3;
    private BigDecimal gspusPrc3;
    private String gspusRebate3;
    private String gspusMemFlag;
    private String gspusInteFlag;
    private String gspusInteRate;
}
