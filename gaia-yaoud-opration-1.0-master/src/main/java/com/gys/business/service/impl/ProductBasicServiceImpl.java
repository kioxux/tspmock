//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.WriteTable;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.gys.business.bosen.constant.UpLoadConstant;
import com.gys.business.bosen.dto.DownloadDataDTO;
import com.gys.business.mapper.GaiaProductBasicImageMapper;
import com.gys.business.mapper.GaiaProductBasicMapper;
import com.gys.business.mapper.GaiaSdNbMedicareMapper;
import com.gys.business.mapper.entity.GaiaProductBasicImage;
import com.gys.business.mapper.entity.GaiaSdNbMedicare;
import com.gys.business.service.CommonService;
import com.gys.business.service.ProductBasicService;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.productBasic.GetProductBasicInData;
import com.gys.business.service.data.productBasic.GetProductBasicOutData;
import com.gys.business.service.data.upload.UpLoadDto;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.*;

import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.pl.REGON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipOutputStream;

@Service
@Slf4j
public class ProductBasicServiceImpl implements ProductBasicService {
    @Autowired
    private GaiaProductBasicMapper gaiaProductBasicMapper;
    @Autowired
    private GaiaSdNbMedicareMapper gaiaSdNbMedicareMapper;
    @Autowired
    private CosUtils cosUtil;
    @Resource
    private DictionaryUtil dictionaryUtil;
    @Autowired
    private GaiaProductBasicImageMapper gaiaProductBasicImageMapper;

    //private static LinkedHashMap<String,String> fieldMap=new LinkedHashMap<>();
    public ProductBasicServiceImpl() {
    }

    public List<GetProductInfoQueryOutData> getProductBasicList(GetProductInfoQueryInData inData) {
        return this.gaiaProductBasicMapper.getProductBasicList(inData);
    }
    /*static {
        fieldMap.put()
    }*/

    public List<GetProductInfoQueryOutData> selectMedCheckProList(GetProductInfoQueryInData inData) {
        if (ObjectUtil.isNotNull(inData.getMonth())) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(2, inData.getMonth());
            inData.setVaildDate(DateUtil.format(calendar.getTime(), "yyyyMMdd"));
        }

        List<GetProductInfoQueryOutData> list = this.gaiaProductBasicMapper.selectMedCheckProList(inData);
        Iterator var3 = list.iterator();

        while(var3.hasNext()) {
            GetProductInfoQueryOutData product = (GetProductInfoQueryOutData)var3.next();
            if (!StrUtil.isBlank(product.getVaildDate())) {
                long days = DateUtil.betweenDay(new Date(), DateUtil.parse(product.getVaildDate(), "yyyyMMdd"), true);
                product.setVaild(StrUtil.toString(days));
            }
        }

        return list;
    }

    public List<GetProductInfoQueryOutData> selectPhysicalProList(GetProductInfoQueryInData inData) {
        List<GetProductInfoQueryOutData> list = this.gaiaProductBasicMapper.selectPhysicalProList(inData);
        Iterator var3 = list.iterator();

        while(var3.hasNext()) {
            GetProductInfoQueryOutData product = (GetProductInfoQueryOutData)var3.next();
            if (!StrUtil.isBlank(product.getVaildDate())) {
                long days = DateUtil.betweenDay(new Date(), DateUtil.parse(product.getVaildDate(), "yyyyMMdd"), true);
                product.setVaild(StrUtil.toString(days));
            }
        }

        return list;
    }

    @Override
    public PageInfo<GetProductBasicOutData> list(GetProductBasicInData inData) {
        if (-1 != inData.getPageSize()) {
            PageHelper.startPage(inData.getPageNum(),inData.getPageSize());
        }
        if (StrUtil.isNotEmpty(inData.getQueryString())) {
            inData.setQueryStringList(Arrays.stream(inData.getQueryString().split(",")).collect(Collectors.toList()));
        }
        List<GetProductBasicOutData> outData = this.gaiaProductBasicMapper.list(inData);
        outData=setDic(outData);
        PageInfo<GetProductBasicOutData> pageInfo;
        if (CollUtil.isNotEmpty(outData)) {
            return pageInfo = new PageInfo<>(outData);
        } else {
            return pageInfo =new PageInfo<>();
        }
    }

    private List<GetProductBasicOutData> setDic(List<GetProductBasicOutData> outData) {
        Map<String, String> inputMap =new HashMap<>(); //CollUtil.newHashMap();
        inputMap.put("taxFlag","1");
        Map<String, String> outputMap =new HashMap<>(); //CollUtil.newHashMap();
        outputMap.put("taxFlag","2");
        List<Map<String, String>> inputTaxList = this.getTaxList(inputMap);
        List<Map<String, String>> outputTaxList = this.getTaxList(outputMap);
        outData.stream().forEach(out->{
            Stream<Map<String, String>> intputTaxCodeStream = inputTaxList.stream().filter(inputTax -> {
                return inputTax.get("taxCode").equals(out.getProInputTax());
            });
            if (intputTaxCodeStream != null) {
                Map<String, String> intputTaxCode = intputTaxCodeStream.findFirst().orElse(null);
                if (intputTaxCode != null) {
                    out.setProInputTax(
                            intputTaxCode.get("taxCode")+"-"+intputTaxCode.get("taxCodeValue")
                    );
                }
            }
            Stream<Map<String, String>> outputTaxCodeStream = outputTaxList.stream().filter(outputTax -> {
                return outputTax.get("taxCode").equals(out.getProInputTax());
            });
            if (outputTaxCodeStream != null) {
                Map<String, String> outPutTaxCode = outputTaxCodeStream.findFirst().orElse(null);
                if (outPutTaxCode != null) {
                    out.setProInputTax(
                            outPutTaxCode.get("taxCode")+"-"+outPutTaxCode.get("taxCodeValue")
                    );
                }
            }

//            Map<String, String> intputTaxCode = inputTaxList.stream().filter(inputTax -> {
//                return inputTax.get("taxCode").equals(out.getProInputTax());
//            }).findFirst().get();
//            out.setProInputTax(
//                    intputTaxCode.get("taxCode")+"-"+intputTaxCode.get("taxCodeValue")
//            );
//            Map<String, String> outPutTaxCode = outputTaxList.stream().filter(outputTax -> {
//                return outputTax.get("taxCode").equals(out.getProOutputTax());
//            }).findFirst().get();
//            out.setProOutputTax(
//                    outPutTaxCode.get("taxCode")+"-"+outPutTaxCode.get("taxCodeValue")
//            );
        });
        return outData;
    }

    @Override
    public JsonResult edit(GetProductBasicOutData outData) {
        if (ObjectUtil.isNull(outData)||StrUtil.isEmpty(outData.getProCode())) {
            throw new BusinessException("修改对象不能为空");
        }
        this.gaiaProductBasicMapper.edit(outData);

        // 图片操作
        this.updateImage(outData.getImageList(), outData.getProCode());

        return JsonResult.success(null, "修改成功");
    }

    @Override
    public JsonResult getOne(Map<String,String> proCode) {
        if (StrUtil.isEmpty(proCode.get("proCode"))) {
            throw new BusinessException("商品编码不能为空");
        }

        GetProductBasicOutData getProductBasicOutData = this.gaiaProductBasicMapper.getOne(proCode.get("proCode"));
        // 插入图片列表
        if (getProductBasicOutData != null) {
            List<GaiaProductBasicImage> imageList = proImageList(proCode.get("proCode"));
            getProductBasicOutData.setImageList(imageList);
        }
        return JsonResult.success(getProductBasicOutData, "查询成功");
    }

    @Override
    public List<Map<String,String>> getUnitList() {
        return this.gaiaProductBasicMapper.getUnitList();
    }

    @Override
    public List<Map<String, String>> getTaxList(Map<String,String> map) {

        if (StrUtil.isEmpty(map.get("taxFlag")) || !CollUtil.newArrayList("1","2").contains(map.get("taxFlag"))) {
            throw new BusinessException("税率标记taxFlag参数错误,1查进项税率，2查销项税率");
        }
        return this.gaiaProductBasicMapper.getTaxList(map);
    }

    @Override
    public List<GaiaSdNbMedicare> getNBYBProCode(GetProductInfoQueryInData inData) {
        return this.gaiaSdNbMedicareMapper.getNBYBProCode(inData);
    }
    @Override
    public void export(HttpServletRequest request, HttpServletResponse response, GetProductBasicInData inData) {
        List<LinkedHashMap<String, String>> fieldMap = inData.getFieldMap();
        if (CollUtil.isEmpty(fieldMap)) {
            throw new BusinessException("导出字段必填");
        }
        try {
            inData.setPageSize(10000);
            this.listForExport(inData,response,fieldMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listForExport(GetProductBasicInData inData, HttpServletResponse response, List<LinkedHashMap<String, String>> fieldMap) throws IOException {
        inData.setFieldSet(fieldMap.stream().filter(o-> StringUtil.isNotEmpty(o.get("field"))).map(map -> map.get("field")).collect(Collectors.toSet()));
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("商品主数据", "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();
        Integer count=this.gaiaProductBasicMapper.listCount();
        Integer pages=count/inData.getPageSize()+1;
        while (inData.getPageNum()<=pages) {
            PageHelper.startPage(inData.getPageNum(),inData.getPageSize());
            List<LinkedHashMap<String,Object>> pageInfo = this.gaiaProductBasicMapper.listForExport(inData);
            inData.setPageNum(inData.getPageNum()+1);
            pageInfo= filter(pageInfo);
            List<List<Object>> data= dataList(pageInfo);
            WriteTable writeTable = EasyExcel.writerTable(0).needHead(Boolean.TRUE).head(head(fieldMap,pageInfo)).build();
            WriteSheet writeSheet = EasyExcel.writerSheet(0,0+"").needHead(Boolean.FALSE).build();

            try {
                excelWriter.write(data, writeSheet,writeTable);
            }catch (Exception e){
                log.error("写入sheet数据报错", e);
            }finally {
                log.info("写入sheet结束");
            }
        }
        excelWriter.finish();
        //CsvClient.endHandleMap(response,outData,fileInfo,fieldMap);
    }

    private List<LinkedHashMap<String, Object>> filter(List<LinkedHashMap<String, Object>> pageInfo) {
        pageInfo.forEach(linkedMap->{
            linkedMap.remove("1");
        });
        return pageInfo;
    }

    private List<List<Object>> dataList(List<LinkedHashMap<String, Object>> pageInfo) {
        List<List<Object>> lists = new ArrayList<>();
        pageInfo.stream().forEach(linkedMap->{
            List<Object> list = new ArrayList<>();
            linkedMap.forEach((k,v)->{
               list.add(v);
            });
            lists.add(list);
        });
        return lists;
    }

    private List<List<String>> head(List<LinkedHashMap<String, String>> fieldMap, List<LinkedHashMap<String, Object>> pageInfo) {
        List<List<String>> headTitles = CollUtil.newArrayList();

        pageInfo.stream().findFirst().ifPresent((map)->{
            map.keySet().forEach(key->{
                headTitles.add(CollUtil.newArrayList(
                        fieldMap.stream()
                        .filter(obj -> StringUtils.isNotEmpty(obj.get("field")) && obj.get("field").equals(key))
                        .findFirst().get().get("title")));
            });
        });

        return headTitles;
    }

    /**
     * 反射获取数据
     * @param outData
     * @param fieldSet
     * @return
     */
    private List<LinkedHashMap<String, String>> transferToLinkedMapList(List<GetProductBasicOutData> outData,Set<String> fieldSet) {
        List<LinkedHashMap<String, String>> mapList=new ArrayList<>();
        Field[] fields = GetProductBasicOutData.class.getDeclaredFields();
        for (GetProductBasicOutData out : outData) {
            LinkedHashMap<String, String> linkedHashMap=new LinkedHashMap<>();
            for (Field field : fields) {
                if (fieldSet.contains(field.getName())) {
                    try {
                        field.setAccessible(true);
                        Object value = field.get(out);
                        linkedHashMap.put(field.getName(),value.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            mapList.add(linkedHashMap);
        }
        return mapList;
    }


    /**
     * @Author jiht
     * @Description 批量上传校验
     * @Date 2022/1/16 21:17
     * @Param [multiFile]
     * @Return com.gys.common.data.JsonResult
     **/
    @Override
    public JsonResult batchUploadCheck(List<GaiaProductBasicImage> imageList) {
        log.info("[商品主数据查询及维护]批量上传校验开始，入参：{}", JSONObject.toJSONString(imageList));
        // 数据校验是否存在
        List<String> allProBaiscCodeList = gaiaProductBasicMapper.queryAllProBaiscCode();

        // 异常图片名
        List<String> errorFileNames = new ArrayList<>();
        // 循环校验入库
        for (GaiaProductBasicImage oneFile : imageList) {
            // 校验图片格式
            if (!checkImageFileName(oneFile.getGpbiFileName(), allProBaiscCodeList)) {
                errorFileNames.add(oneFile.getGpbiFileName());
            }
        }
        GetProductImageOutData getProductImageOutData = new GetProductImageOutData();
        // 异常报错
        if (errorFileNames.size() > 0) {
            log.info("图片命名有误或在编码库中不存在：{}", StringUtils.join(errorFileNames.toArray(), "、"));
            getProductImageOutData.setCheckFlag("2");
            getProductImageOutData.setErrorList(errorFileNames);
            return JsonResult.success(UtilConst.CODE_1001, errorFileNames, "提示：校验存在异常！");
        }else{
            getProductImageOutData.setCheckFlag("1");
            return JsonResult.success(getProductImageOutData, "提示：校验存在异常！");
        }
    }

    /**
     * @Author jiht
     * @Description 批量上传
     * @Date 2022/1/16 21:17
     * @Param [multiFile]
     * @Return com.gys.common.data.JsonResult
     **/
    @Override
    public JsonResult batchUpload(List<GaiaProductBasicImage> imageList) {
        log.info("[商品主数据查询及维护]批量上传开始，入参：{}", JSONObject.toJSONString(imageList));
        // 数据校验是否存在
        List<String> allProBaiscCodeList = gaiaProductBasicMapper.queryAllProBaiscCode();

        // 异常图片名
        List<String> errorFileNames = new ArrayList<>();
        // 循环校验入库
        for (GaiaProductBasicImage oneFile : imageList) {
            // 校验图片格式
            if (!checkImageFileName(oneFile.getGpbiFileName(), allProBaiscCodeList)) {
                errorFileNames.add(oneFile.getGpbiFileName());
            }
            // 图片url不能为空
            if (StringUtils.isEmpty(String.valueOf(oneFile.getGpbiUploadPath()))) {
                errorFileNames.add(oneFile.getGpbiFileName());
            }
            // 保存
            saveImage(oneFile.getGpbiFileName(), oneFile.getGpbiUploadPath());
        }
        // 异常报错
        if(errorFileNames.size()>0){
            throw new BusinessException("图片命名有误或在编码库中不存在：" + StringUtils.join(errorFileNames.toArray(),"、"));
        }
        return JsonResult.success("", "提示：上传成功！");
    }

    /**
     * @Author jiht
     * @Description 图片上传至腾讯云服务器
     * @Date 2022/1/17 15:32
     * @Param [fileBytes, OrifileName]
     * @Return com.gys.common.data.JsonResult
     **/
    private JsonResult uploadImage(byte[] fileBytes, String OrifileName) throws IOException {
        log.info("[商品主数据查询及维护]文件：{}上传至至云服务器开始", OrifileName);
        String path = UpLoadConstant.UPLOAD_CONFIG.get(UpLoadConstant.PRODUCT_BASIC_IMAGE) + "/" + OrifileName;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(fileBytes);
        JsonResult result = cosUtil.uploadFileNew((out), path);

        if (StringUtils.isEmpty(String.valueOf(result.getData()))) {
            throw new BusinessException("图片未成功上传至云服务器：" + OrifileName);
        }

        return result;
    }

    /**
     * @Author jiht
     * @Description 图片信息入库
     * @Date 2022/1/16 21:17
     * @Param [fileBytes, OrifileName]
     * @Return void
     **/
    private void saveImage(String OrifileName, String uploadPath) {
        log.info("[商品主数据查询及维护]文件：{}入库开始", OrifileName);
        String fileName = OrifileName.split("\\.")[0];
        String proCode = fileName.split("_")[0];
        int sort = Integer.valueOf(fileName.split("_")[1]);

        GaiaProductBasicImage queryImage = new GaiaProductBasicImage();
        queryImage.setGpbiProCode(proCode);
        queryImage.setGpbiSort(sort);
        GaiaProductBasicImage oneImage = gaiaProductBasicImageMapper.selectOne(queryImage);
        if (oneImage == null) {
            oneImage = new GaiaProductBasicImage();
            oneImage.setGpbiProCode(proCode);
            oneImage.setGpbiSort(sort);
            oneImage.setGpbiFileName(OrifileName);
            oneImage.setGpbiUploadPath(uploadPath);
            oneImage.setGpbiCreateTime(new Date());
            oneImage.setGpbiModifyTime(new Date());
            oneImage.setGpbiDeleted("0");
            gaiaProductBasicImageMapper.insert(oneImage);
        } else {
            oneImage.setGpbiFileName(OrifileName);
            oneImage.setGpbiUploadPath(uploadPath);
            oneImage.setGpbiModifyTime(new Date());
            oneImage.setGpbiDeleted("0");
            gaiaProductBasicImageMapper.updateByPrimaryKey(oneImage);
        }
    }

    /**
     * @Author jiht
     * @Description 校验文件名是否符合规范
     * @Date 2022/1/17 15:35
     * @Param [FileName, ProCodeList]
     * @Return boolean
     **/
    private boolean checkImageFileName(String fileName,List<String> allProBaiscCodeList) {
        log.info("[商品主数据查询及维护]图片{}校验开始", fileName);
        try {
            String fileNoSuffixName = fileName.split("\\.")[0];
            String suffix = fileName.split("\\.")[1];
            String proCode = fileNoSuffixName.split("_")[0];
            String sort = fileNoSuffixName.split("_")[1];
            if (!"JPG".equals(suffix.toUpperCase()) && !"JPEG".equals(suffix.toUpperCase()) && !"PNG".equals(suffix.toUpperCase())) {
                log.info("[商品主数据查询及维护]图片{}格式有误", fileName);
                return false;
            }
            if (sort.length() != 2 || Integer.parseInt(sort) <= 0) {
                log.info("[商品主数据查询及维护]图片{}序号有误，非2位数字", fileName);
                return false;
            }
            if (!allProBaiscCodeList.contains(proCode)) {
                log.info("[商品主数据查询及维护]图片{}对应编码在商品编码库中不存在", fileName);
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * @Author jiht
     * @Description 保存图片
     * @Date 2022/1/16 21:18
     * @Param [fileNames]
     * @Return com.gys.common.data.JsonResult
     **/
    public void updateImage(List<GaiaProductBasicImage> imageList, String proCode) {
        log.info("[商品主数据查询及维护]图片开始，入参：{}", JSONObject.toJSONString(imageList));

        // 删除老图片
        gaiaProductBasicMapper.deleteProductBasicImageByProCode(proCode);
        log.info("[商品主数据查询及维护]原图片删除完成");

        if (CollectionUtils.isEmpty(imageList)) {
            log.info("[商品主数据查询及维护]无图片信息无需处理");
            return;
        }
        List<String> allProBaiscCodeList= gaiaProductBasicMapper.queryAllProBaiscCode();
        // 保存新图片
        imageList.stream().forEach(o -> {
            if(!checkImageFileName(o.getGpbiFileName(),allProBaiscCodeList)){
                throw new BusinessException("图片命名有误或在编码库中不存在：" + o.getGpbiFileName());
            }
            if(StringUtils.isEmpty(o.getGpbiUploadPath())){
                throw new BusinessException("图片未成功上传至云服务器：" + o.getGpbiFileName());
            }
            this.saveImage(o.getGpbiFileName(), o.getGpbiUploadPath());
        });
    }

    /**
     * @Author jiht
     * @Description 下载图片
     * @Date 2022/1/16 21:18
     * @Param [request, response, proCodes]
     * @Return void
     **/
    public JsonResult downloadImage(List<String> proCodes) {
        log.info("[商品主数据查询及维护]文件下载开始，入参：{}", JSONObject.toJSONString(proCodes));

        // 查询文件上传目录
        List<GaiaProductBasicImage> imageList = this.gaiaProductBasicMapper.queryProductBasicImageList(proCodes);
        if (CollectionUtils.isEmpty(imageList)) {
            throw new BusinessException("没有满足条件的商品图片");
        }

        GetProductImageOutData getProductImageOutData = new GetProductImageOutData();
        getProductImageOutData.setImageList(imageList);

        return JsonResult.success(getProductImageOutData, "提示：查询成功！");
    }

    /**
     * @Author jiht
     * @Description 图片列表
     * @Date 2022/1/16 21:18
     * @Param [proCode]
     * @Return com.gys.common.data.JsonResult
     **/
    private List<GaiaProductBasicImage> proImageList(String proCode) {
        List<String> proCodes = new ArrayList<>();
        proCodes.add(proCode);
        List<GaiaProductBasicImage> imageList = this.gaiaProductBasicMapper.queryProductBasicImageList(proCodes);
        return imageList;
    }

    /**
     * @Author jiht
     * @Description 指定用户查询
     * @Date 2022/1/16 21:18
     * @Param []
     * @Return com.gys.common.data.JsonResult
     **/
    public JsonResult appointUserQuery() {
        // 查询库中配置的用户
        List<GaiaDictionary> dictionaryList = dictionaryUtil.geGaiaDictionaryByType("ProBasicAppointUser");
        List<String> appointUserList = new ArrayList<>();
        dictionaryList.stream().forEach(o -> appointUserList.add(o.getCode()));
        return JsonResult.success(appointUserList, "提示：查询成功！");
    }
}
