package com.gys.business.service.data;

import lombok.Data;

/**
 * @desc: 积分换购设置：查看（零售价）返回参数
 * @author: ZhangChi
 * @createTime: 2021/11/30 13:59
 */
@Data
public class ProductPriceInfo {
    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 门店
     */
    private String store;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 规格
     */
    private String productSpecs;

    /**
     * 厂家
     */
    private String proFactoryName;

    /**
     * 零售价
     */
    private String proCGJ;

    /**
     * 成本价
     */
    private String proLSJ;
}
