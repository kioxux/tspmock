//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetLoginStoreOutData implements Serializable {
    private String userId;
    private String stoCode;
    private String stoName;

    public GetLoginStoreOutData() {
    }

    public String getUserId() {
        return this.userId;
    }

    public String getStoCode() {
        return this.stoCode;
    }

    public String getStoName() {
        return this.stoName;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public void setStoCode(final String stoCode) {
        this.stoCode = stoCode;
    }

    public void setStoName(final String stoName) {
        this.stoName = stoName;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetLoginStoreOutData)) {
            return false;
        } else {
            GetLoginStoreOutData other = (GetLoginStoreOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$userId = this.getUserId();
                    Object other$userId = other.getUserId();
                    if (this$userId == null) {
                        if (other$userId == null) {
                            break label47;
                        }
                    } else if (this$userId.equals(other$userId)) {
                        break label47;
                    }

                    return false;
                }

                Object this$stoCode = this.getStoCode();
                Object other$stoCode = other.getStoCode();
                if (this$stoCode == null) {
                    if (other$stoCode != null) {
                        return false;
                    }
                } else if (!this$stoCode.equals(other$stoCode)) {
                    return false;
                }

                Object this$stoName = this.getStoName();
                Object other$stoName = other.getStoName();
                if (this$stoName == null) {
                    if (other$stoName != null) {
                        return false;
                    }
                } else if (!this$stoName.equals(other$stoName)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetLoginStoreOutData;
    }

    public int hashCode() {
        int result = 1;
        Object $userId = this.getUserId();
        result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        Object $stoCode = this.getStoCode();
        result = result * 59 + ($stoCode == null ? 43 : $stoCode.hashCode());
        Object $stoName = this.getStoName();
        result = result * 59 + ($stoName == null ? 43 : $stoName.hashCode());
        return result;
    }

    public String toString() {
        return "GetLoginStoreOutData(userId=" + this.getUserId() + ", stoCode=" + this.getStoCode() + ", stoName=" + this.getStoName() + ")";
    }
}
