package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetWfCreateLossDefecproInData {
    private int index = 0;
    private String lossDate = "";
    private String lossOrder = "";
    private String storeId = "";
    private String storeName = "";
    private String proCode = "";
    private String proName = "";
    private String proCommonName = "";
    private String proSpecs = "";
    private String proUnit = "";
    private String factoryName = "";
    private String batch = "";
    private String batchNo = "";
    private String validDate = "";
    private String qty = "";
    private String reason = "";
}
