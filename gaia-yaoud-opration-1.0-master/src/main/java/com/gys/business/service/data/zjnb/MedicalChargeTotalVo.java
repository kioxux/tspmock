package com.gys.business.service.data.zjnb;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author wuai
 * @description: 6)医疗费用累计结构
 * @date 2021/10/22 16:18
 */
@Data
public class MedicalChargeTotalVo {
    /**
     * 个人帐户当年(结算前)余额	9	保留2位小数
     */
    private BigDecimal psnAcctBalBef;
    /**
     * 个人帐户当年(结算后)余额	9	保留2位小数
     */
    private BigDecimal psnAcctBalAft;
    /**
     * 个人帐户历年(结算前)余额	9	保留2位小数
     */
    private BigDecimal psnAcctHisBalBef;
    /**
     * 个人帐户历年(结算后)余额	9	保留2位小数
     */
    private BigDecimal psnAcctHisBalAft;
    /**
     * 门诊自负段(结算前)累计	8	保留2位小数；大病特药起付段累计在此字段返回
     */
    private BigDecimal outPayTotBef;
    /**
     * 门诊自负段(结算后)累计	8	保留2位小数；大病特药起付段累计在此字段返回
     */
    private BigDecimal outPayTotAft;
    /**
     * 住院起付线(结算前)累计	8	保留2位小数
     */
    private BigDecimal hosStartTotBef;
    /**
     * 住院起付线(结算后)累计	8	保留2位小数
     */
    private BigDecimal hosStartTotAft;
    /**
     * 年度门诊费用(结算前)累计	10	保留2位小数；大病特药年度费用累计、苯丙酮尿症年度费用累计、丙肝年度费用累计在此字段返回
     */
    private BigDecimal outYearTotBef;
    /**
     * 年度门诊费用(结算后)累计	10	保留2位小数；大病特药年度费用累计、苯丙酮尿症年度费用累计、丙肝年度费用累计在此字段返回
     */
    private BigDecimal outYearTotAft;
    /**
     * 年度住院费用(结算前)累计	10	保留2位小数
     */
    private BigDecimal hosYearTotBef;
    /**
     * 年度住院费用(结算后)累计	10	保留2位小数
     */
    private BigDecimal hosYearTotAft;
    /**
     * 年度特病(结算前)累计	10	保留2位小数
     */
    private BigDecimal speYearTotBef;

    /**
     * 年度特病(结算后)累计	10	保留2位小数
     */
    private BigDecimal speYearTotAft;
    /**
     * 住院共负段自付[ 、是指住院类交易中“个人承担”部分发生数。](结算前)累计	10	保留2位小数
     */
    private BigDecimal hosTotBef;

    /**
     * 住院共负段自付(结算后)累计	10	保留2位小数
     */
    private BigDecimal hosTotAft;

    /**
     * 门诊封顶线下[ 、目前城镇居民有封顶线的概念，需要参考此项指标。](结算前)累计	10	保留2位小数
     */
    private BigDecimal outTopBef;
    /**
     * 门诊封顶线下(结算后)累计	10	保留2位小数
     */
    private BigDecimal outTopAft;
    /**
     * 住院封顶线下(结算前)累计	10	保留2位小数
     */
    private BigDecimal hosTopBef;
    /**
     * 住院封顶线下(结算后)累计	10	保留2位小数
     */
    private BigDecimal hosTopAft;
    /**
     * 特病封顶线下(结算前)累计	10	保留2位小数
     */
    private BigDecimal speTopBef;

    /**
     * 特病封顶线下(结算后)累计	10	保留2位小数
     */
    private BigDecimal speTopAft;

    /**
     * 家庭账户(结算前)余额	10	保留2位小数 原名“其他累计1(结算前)累计”
     */
    private BigDecimal familyAcctBef;
    /**
     * 家庭账户(结算后)余额	10	保留2位小数 原名“其他累计1(结算后)累计”
     */
    private BigDecimal familyAcctAft;
    /**
     * 其他累计2(结算前)累计	10	保留2位小数 其他累计2(结算后)累计	10	保留2位小数
     */
    private BigDecimal othTotBef;
    /**
     * 其他累计2(结算后)累计	10	保留2位小数 其他累计2(结算后)累计	10	保留2位小数
     */
    private BigDecimal othTotAft;
    /**
     * 医疗救（补）助支付(结算前)累计	10	保留2位小数 原名“其他累计3(结算前)累计”
     */
    private BigDecimal mafPayBef;
    /**
     * 医疗救（补）助支付(结算后)累计	10	保留2位小数 原名“其他累计3(结算后)累计”
     */
    private BigDecimal mafPayAft;

}
