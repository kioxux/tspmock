package com.gys.business.service.webServiceImpl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.service.data.WebService.Prescrip;
import com.gys.business.service.data.WebService.TlMaterial;
import com.gys.common.webService.ClientBean;
import com.gys.common.webService.ClientField;
import com.gys.common.webService.InterfaceVO;
import com.gys.common.webService.WebServiceUtils;
import com.gys.util.TlXmlParseUtil;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.*;

@Service
public class TlServiceImpl {
    private static final Logger logger = LoggerFactory.getLogger(TlServiceImpl.class);

    @Value("${zydj.tl}")
    private String tlIp;

    /**
     * 获取医院的唯一标识
     *
     * @param hidParam 账号
     * @param pwdParam 密码
     * @return 医院的唯一标识
     */
    @SuppressWarnings("unchecked")
    public InterfaceVO getUniqueID(String hidParam, String pwdParam) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField hid = new ClientField("hid", String.class, hidParam);
            clientFields.add(hid);
            // 设置参数2 密码
            ClientField pwd = new ClientField("pwd", String.class, pwdParam);
            clientFields.add(pwd);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Hospital.asmx",
                    "http://tempuri.org/", "http://tempuri.org/GetUniqueID",
                    "GetUniqueID", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【获取医院的唯一标识】【" + hidParam + "," + pwdParam + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                logger.info("【获取医院的唯一标识】【" + hidParam + "," + pwdParam + "】uniqueID: {}", resultMsg);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    logger.info("【获取医院的唯一标识】查询成功：" + resultMsg);
                } else {
                    resultMsg.failed();
                    logger.error("【获取医院的唯一标识】没有找到医院号为{" + hidParam + "," + pwdParam + "}失败!");
                }
            } else {
                resultMsg.failed();
                resultMsg.setMessage("【获取医院的唯一标识】医院号为{" + hidParam + "}，返回结果为空!");
                logger.error("【获取医院的唯一标识】医院号为{" + hidParam + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("【获取医院的唯一标识】请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 重置密码
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param newPwd   新密码
     * @return 空
     */
    @SuppressWarnings("unchecked")
    public InterfaceVO newPwd(String uniqueId, String newPwd) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField uniqueIdParam = new ClientField("uniqueId", String.class, uniqueId);
            clientFields.add(uniqueIdParam);
            // 设置参数2 新密码
            ClientField newPwdParam = new ClientField("newPwd", String.class, newPwd);
            clientFields.add(newPwdParam);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Hospital.asmx",
                    "http://tempuri.org/", "http://tempuri.org/NewPwd",
                    "NewPwd", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【重置密码】【" + uniqueId + "," + newPwd + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                logger.info("【重置密码】【" + uniqueId + "," + newPwd + "】uniqueID: {}", resultMsg);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    logger.info("【重置密码】成功：" + resultMsg);
                } else {
                    resultMsg.failed();
                    logger.error("【重置密码】医院号为{" + uniqueId + "," + newPwd + "}失败!");
                }
            } else {
                resultMsg.failed();
                resultMsg.setMessage("【重置密码】医院号为{" + uniqueId + "}，返回结果为空!");
                logger.error("【重置密码】医院号为{" + uniqueId + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("【重置密码】请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 获取中草药列表
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @return 中草药列表
     */
    @SuppressWarnings("unchecked")
    public InterfaceVO getItems(String uniqueId) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField uniqueIdParam = new ClientField("uniqueId", String.class, uniqueId);
            clientFields.add(uniqueIdParam);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Material.asmx",
                    "http://tempuri.org/", "http://tempuri.org/GetItems",
                    "GetItems", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【获取中草药列表】【" + uniqueId + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    //解析返回的XML数据
                    List<TlMaterial> materialList = (List<TlMaterial>) TlXmlParseUtil.parseXml2List(returnXml, TlMaterial.class);
                    if (CollectionUtil.isNotEmpty(materialList)) {
                        resultMsg.setResult(materialList.toString());
                    }
                }else{
                    resultMsg.failed();
                    logger.error("【获取中草药列表】医院号为{" + uniqueId + "}失败!");
                }
            } else {
                resultMsg.failed();
                resultMsg.setMessage("【获取中草药列表】医院号为{" + uniqueId + "}，返回结果为空!");
                logger.error("【获取中草药列表】医院号为{" + uniqueId + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("【获取中草药列表】请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 获取中草药库存
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param itmId 中草药编号
     * @return 库存
     */
    public InterfaceVO getStock(String uniqueId, String itmId) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField uniqueIdParam = new ClientField("uniqueId", String.class, uniqueId);
            clientFields.add(uniqueIdParam);
            // 设置参数2 密码
            ClientField itmIdParam = new ClientField("itmId", String.class, itmId);
            clientFields.add(itmIdParam);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Material.asmx",
                    "http://tempuri.org/", "http://tempuri.org/GetStock",
                    "GetStock", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【获取中草药库存】【" + uniqueIdParam + "," + itmIdParam + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                logger.info("【获取中草药库存】【" + uniqueIdParam + "," + itmIdParam + "】uniqueID: {}", resultMsg);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    logger.info("【获取中草药库存】查询成功：" + resultMsg);
                } else {
                    resultMsg.failed();
                    logger.error("【获取中草药库存】没有找到医院号,中草药编号为{" + uniqueIdParam + "," + itmIdParam + "}失败!");
                }
            } else {
                resultMsg.failed();
                resultMsg.setMessage("【获取中草药库存】医院号,中草药编号为{" + uniqueIdParam + "," + itmIdParam + "}，返回结果为空!");
                logger.error("【获取中草药库存】医院号,中草药编号为{" + uniqueIdParam +"," + itmIdParam + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("【获取中草药库存】请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 获取中草药价格
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param itmId    中草药编号
     * @return 单价
     */
    public InterfaceVO getPrice(String uniqueId, String itmId) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField uniqueIdParam = new ClientField("uniqueId", String.class, uniqueId);
            clientFields.add(uniqueIdParam);
            // 设置参数2 密码
            ClientField itmIdParam = new ClientField("itmId", String.class, itmId);
            clientFields.add(itmIdParam);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Material.asmx",
                    "http://tempuri.org/", "http://tempuri.org/GetPrice",
                    "GetPrice", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【获取中草药价格】【" + uniqueIdParam + "," + itmIdParam + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                logger.info("【获取中草药价格】【" + uniqueIdParam + "," + itmIdParam + "】uniqueID: {}", resultMsg);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    logger.info("【获取中草药价格】查询成功：" + resultMsg);
                } else {
                    resultMsg.failed();
                    logger.error("【获取中草药价格】医院号,中草药编号为{" + uniqueIdParam + "," + itmIdParam + "}失败!");
                }
            } else {
                resultMsg.failed();
                resultMsg.setMessage("【获取中草药价格】医院号,中草药编号为{" + uniqueIdParam + "," + itmIdParam + "}，返回结果为空!");
                logger.error("【获取中草药价格】医院号,中草药编号为{" + uniqueIdParam +"," + itmIdParam + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 创建处方
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param prescrip      处方示例
     * @return
     */
    public InterfaceVO create(String uniqueId, Prescrip prescrip) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField uniqueIdParam = new ClientField("uniqueId", String.class, uniqueId);
            clientFields.add(uniqueIdParam);
            // 设置参数2 处方示例
            ClientField pidParam = new ClientField("xml", String.class, prescrip.ParseXml());

            clientFields.add(pidParam);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Prescrip.asmx",
                    "http://tempuri.org/", "http://tempuri.org/Create",
                    "Create", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【创建处方】【" + uniqueIdParam + "," + prescrip.getPid() + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                logger.info("【创建处方】【" + uniqueIdParam + "," + pidParam + "】uniqueID: {}", resultMsg);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    logger.info("【创建处方】创建成功：" + resultMsg);
                } else {
                    resultMsg.failed();
                    logger.error("【创建处方】医院号,处方编号{" + uniqueIdParam + "," + prescrip.getPid() + "}失败!");
                }
            } else {
                resultMsg.failed();
                resultMsg.setMessage("【创建处方】医院号,处方编号{" + uniqueIdParam + "," + prescrip.getPid() + "}，返回结果为空!");
                logger.error("【创建处方】医院号,处方编号{" + uniqueIdParam +"," + prescrip.getPid() + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("【创建处方】请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 处方作废
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param pid      处方编号
     * @return 空
     */
    public InterfaceVO invalid(String uniqueId, String pid) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField uniqueIdParam = new ClientField("uniqueId", String.class, uniqueId);
            clientFields.add(uniqueIdParam);
            // 设置参数2 密码
            ClientField pidParam = new ClientField("pid", String.class, pid);
            clientFields.add(pidParam);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Prescrip.asmx",
                    "http://tempuri.org/", "http://tempuri.org/Invalid",
                    "Invalid", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【获取中草药价格】【" + uniqueIdParam + "," + pidParam + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                logger.info("【获取中草药价格】【" + uniqueIdParam + "," + pidParam + "】uniqueID: {}", resultMsg);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    logger.info("【获取中草药价格】查询成功：" + resultMsg);
                } else {
                    resultMsg.failed();
                    logger.error("【获取中草药价格】没有找到医院号,处方编号{" + uniqueIdParam + "," + pidParam + "}失败!");
                }
            } else {
                resultMsg.failed();
                resultMsg.setMessage("【获取中草药价格】医院号,处方编号{" + uniqueIdParam + "," + pidParam + "}，返回结果为空!");
                logger.error("【获取中草药价格】医院号,处方编号{" + uniqueIdParam +"," + pidParam + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("【获取中草药价格】请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 处方院方收货
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param pid      处方编号
     * @return
     */
    public InterfaceVO receipt(String uniqueId, String pid) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField uniqueIdParam = new ClientField("uniqueId", String.class, uniqueId);
            clientFields.add(uniqueIdParam);
            // 设置参数2 处方编号
            ClientField pidParam = new ClientField("pid", String.class, pid);
            clientFields.add(pidParam);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Prescrip.asmx",
                    "http://tempuri.org/", "http://tempuri.org/Receipt",
                    "Receipt", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【处方院方收货】【" + uniqueIdParam + "," + pidParam + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                logger.info("【处方院方收货】【" + uniqueIdParam + "," + pidParam + "】uniqueID: {}", resultMsg);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    logger.info("【处方院方收货】查询成功：" + resultMsg);
                } else {
                    resultMsg.failed();
                    logger.error("【处方院方收货】没有找到医院号,处方编号{" + uniqueIdParam + "," + pidParam + "}失败!");
                }
            } else {
                resultMsg.failed();
                logger.error("【处方院方收货】医院号,处方编号{" + uniqueIdParam +"," + pidParam + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("【处方院方收货】请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 获取处方状态
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param pid      处方编号
     * @return D 未处理/O 已确认/X 作废/C 完成
     */
    public InterfaceVO getStatus(String uniqueId, String pid) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField uniqueIdParam = new ClientField("uniqueId", String.class, uniqueId);
            clientFields.add(uniqueIdParam);
            // 设置参数2 处方编号
            ClientField pidParam = new ClientField("pid", String.class, pid);
            clientFields.add(pidParam);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Prescrip.asmx",
                    "http://tempuri.org/", "http://tempuri.org/GetStatus",
                    "GetStatus", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【获取处方状态】【" + uniqueIdParam + "," + pidParam + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                logger.info("【获取处方状态】【" + uniqueIdParam + "," + pidParam + "】uniqueID: {}", resultMsg);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    logger.info("【获取处方状态】查询成功：" + resultMsg);
                } else {
                    resultMsg.failed();
                    logger.error("【获取处方状态】没有找到医院号,处方编号{" + uniqueIdParam + "," + pidParam + "}失败!");
                }
            } else {
                resultMsg.failed();
                resultMsg.setMessage("【获取处方状态】医院号,处方编号{" + uniqueIdParam + "," + pidParam + "}，返回结果为空!");
                logger.error("【获取处方状态】医院号,处方编号{" + uniqueIdParam +"," + pidParam + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("【获取处方状态】请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 获取处方加工流程
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param pid      处方编号
     * @return 处方加工流程
     */
    public InterfaceVO getProcess(String uniqueId, String pid) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {
            List<ClientField> clientFields = new ArrayList<>();
            // 设置参数1 账户名
            ClientField uniqueIdParam = new ClientField("uniqueId", String.class, uniqueId);
            clientFields.add(uniqueIdParam);
            // 设置参数2 处方编号
            ClientField pidParam = new ClientField("pid", String.class, pid);
            clientFields.add(pidParam);
            //获取医院的唯一标识的接口
            ClientBean clientBean = new ClientBean(
                    tlIp+"/TlzyServices/Prescrip.asmx",
                    "http://tempuri.org/", "http://tempuri.org/GetProcess",
                    "GetProcess", clientFields);

            String returnXml = WebServiceUtils.sendWebServiceRequest(clientBean);
            logger.info("【获取处方加工流程】【" + uniqueIdParam + "," + pidParam + "】WebService接口返回XML: {}", returnXml);
            if (StringUtils.isNotBlank(returnXml)) {
                HashMap<String,Object> temp = (HashMap<String,Object>) TlXmlParseUtil.parseXml1Map(returnXml, InterfaceVO.class);
                resultMsg = JSON.parseObject(JSON.toJSONString(temp), InterfaceVO.class);
                logger.info("【获取处方加工流程】【" + uniqueIdParam + "," + pidParam + "】uniqueID: {}", resultMsg);
                if (ObjectUtil.isNotEmpty(resultMsg)&&"Y".equals(resultMsg.getSuccess())) {
                    logger.info("【获取处方加工流程】查询成功：" + resultMsg);
                } else {
                    resultMsg.failed();
                    logger.error("【获取处方加工流程】没有找到医院号,处方编号{" + uniqueIdParam + "," + pidParam + "}失败!");
                }
            } else {
                resultMsg.failed();
                resultMsg.setMessage("【获取处方加工流程】医院号,处方编号{" + uniqueIdParam + "," + pidParam + "}，返回结果为空!");
                logger.error("【获取处方加工流程】医院号,处方编号{" + uniqueIdParam +"," + pidParam + "}，返回结果为空!");
            }
        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("【获取处方加工流程】请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 获取处方
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param pid      处方编号
     * @return
     */
    public InterfaceVO getPrescrip(String uniqueId, String pid) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {

        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 获取处方材料明细
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param pid      处方编号
     * @return
     */
    public InterfaceVO getMaterials(String uniqueId, String pid) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {

        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 获取处方费用明细
     *
     * @param uniqueId 医院编号及密码信息的唯一标识
     * @param pid      处方编号
     * @return
     */
    public InterfaceVO getCharge(String uniqueId, String pid) {
        InterfaceVO resultMsg = new InterfaceVO();
        try {

        } catch (Exception e) {
            resultMsg.failed();
            resultMsg.setResult(e.getMessage());
            resultMsg.setMessage("请求出错，请与管理员联系! ");
            e.printStackTrace();
        }
        return resultMsg;
    }

    /**
     * 解析XML文本映射到实体类
     *
     * @param returnXml     XML文本
     * @param cls           解析生成后映射的Class类对象
     * @param fieldXmlPaths XMLPaths集合(XML中的标签路径) 如:
     * @return
     * @throws DocumentException
     */
    public static List<?> parserXmlToEntityList(String returnXml, Class cls, Map<String, String> fieldXmlPaths) throws DocumentException {
        Document document;
        document = DocumentHelper.parseText(returnXml);
        try {
            //单独用来存属性
            List<String> filedNames = new ArrayList<>();
            //存放<属性,values>
            Map<String, List<Node>> map = new HashMap<>();
            if (fieldXmlPaths != null && fieldXmlPaths.size() > 0) {
                Iterator it = fieldXmlPaths.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    List<Node> nodes = document.selectNodes(value.toString());
                    List<Node> list = new ArrayList<>();
                    for (Node node : nodes) {
                        list.add(node);
                    }
                    //循环放入属性-> 对应的节点
                    map.put(key.toString(), list);
                    filedNames.add(key.toString());
                }
            }

            List list = new ArrayList();
            //循环将节点的值放入到对象中，然后放入到list
            for (int i = 0; i < map.get(filedNames.get(0)).size(); i++) {
                Object instance = cls.newInstance();
                for (String fieldName : filedNames) {
                    String methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    Method method = cls.getMethod(methodName, String.class);
                    method.invoke(instance, map.get(fieldName).get(i).getText());
                }
                list.add(instance);
            }
            return list;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解析XML文本映射到实体类
     *
     * @param returnXml     XML文本
     * @param cls           解析生成后映射的Class类对象
     * @param fieldXmlPaths XMLPaths集合(XML中的标签路径) 如:
     * @return
     * @throws DocumentException
     */
    public static Object parserXmlToEntity(String returnXml, Class cls, Map<String, String> fieldXmlPaths) throws DocumentException {
        Document document;
        document = DocumentHelper.parseText(returnXml);
        try {
            //单独用来存属性
            List<String> filedNames = new ArrayList<>();
            //存放<属性,values>
            Map<String, List<Node>> map = new HashMap<>();
            if (fieldXmlPaths != null && fieldXmlPaths.size() > 0) {
                Iterator it = fieldXmlPaths.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    List<Node> nodes = document.selectNodes(value.toString());
                    List<Node> list = new ArrayList<>();
                    for (Node node : nodes) {
                        list.add(node);
                    }
                    //循环放入属性-> 对应的节点
                    map.put(key.toString(), list);
                    filedNames.add(key.toString());
                }
            }
            Object instance = cls.newInstance();
            //循环将节点的值放入到对象中，然后放入到list
            for (int i = 0; i < map.get(filedNames.get(0)).size(); i++) {
                for (String fieldName : filedNames) {
                    String methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                    Method method = cls.getMethod(methodName, String.class);
                    method.invoke(instance, map.get(fieldName).get(i).getText());
                }
            }
            return instance;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
