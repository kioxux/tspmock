package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ReplenishHOnWayOutData implements Serializable {

    @ApiModelProperty(value = "产品编码")
    private String productId;

    @ApiModelProperty(value = "配送单号")
    private String deliveryOrderId;

    @ApiModelProperty(value = "在途量")
    private BigDecimal onWayNumber;
}
