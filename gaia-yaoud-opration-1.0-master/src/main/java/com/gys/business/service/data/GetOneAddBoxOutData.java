//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdIndrawerH;
import java.util.List;

public class GetOneAddBoxOutData {
    private GaiaSdIndrawerH gaiaSdIndrawerH;
    private List<GaiaSdIndrawerDOutData> gaiaSdIndrawerDList;
    private String clientId;

    public GetOneAddBoxOutData() {
    }

    public GaiaSdIndrawerH getGaiaSdIndrawerH() {
        return this.gaiaSdIndrawerH;
    }

    public List<GaiaSdIndrawerDOutData> getGaiaSdIndrawerDList() {
        return this.gaiaSdIndrawerDList;
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setGaiaSdIndrawerH(final GaiaSdIndrawerH gaiaSdIndrawerH) {
        this.gaiaSdIndrawerH = gaiaSdIndrawerH;
    }

    public void setGaiaSdIndrawerDList(final List<GaiaSdIndrawerDOutData> gaiaSdIndrawerDList) {
        this.gaiaSdIndrawerDList = gaiaSdIndrawerDList;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetOneAddBoxOutData)) {
            return false;
        } else {
            GetOneAddBoxOutData other = (GetOneAddBoxOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$gaiaSdIndrawerH = this.getGaiaSdIndrawerH();
                    Object other$gaiaSdIndrawerH = other.getGaiaSdIndrawerH();
                    if (this$gaiaSdIndrawerH == null) {
                        if (other$gaiaSdIndrawerH == null) {
                            break label47;
                        }
                    } else if (this$gaiaSdIndrawerH.equals(other$gaiaSdIndrawerH)) {
                        break label47;
                    }

                    return false;
                }

                Object this$gaiaSdIndrawerDList = this.getGaiaSdIndrawerDList();
                Object other$gaiaSdIndrawerDList = other.getGaiaSdIndrawerDList();
                if (this$gaiaSdIndrawerDList == null) {
                    if (other$gaiaSdIndrawerDList != null) {
                        return false;
                    }
                } else if (!this$gaiaSdIndrawerDList.equals(other$gaiaSdIndrawerDList)) {
                    return false;
                }

                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetOneAddBoxOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gaiaSdIndrawerH = this.getGaiaSdIndrawerH();
        result = result * 59 + ($gaiaSdIndrawerH == null ? 43 : $gaiaSdIndrawerH.hashCode());
        Object $gaiaSdIndrawerDList = this.getGaiaSdIndrawerDList();
        result = result * 59 + ($gaiaSdIndrawerDList == null ? 43 : $gaiaSdIndrawerDList.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        return result;
    }

    public String toString() {
        return "GetOneAddBoxOutData(gaiaSdIndrawerH=" + this.getGaiaSdIndrawerH() + ", gaiaSdIndrawerDList=" + this.getGaiaSdIndrawerDList() + ", clientId=" + this.getClientId() + ")";
    }
}
