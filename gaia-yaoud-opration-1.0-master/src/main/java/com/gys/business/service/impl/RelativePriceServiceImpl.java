package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.RelativePriceService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.redis.RedisManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RelativePriceServiceImpl implements RelativePriceService {

    @Autowired
    private RedisManager redisManager;
    @Autowired
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;
    @Autowired
    private GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Autowired
    private GaiaPriceGetMapper gaiaPriceGetMapper;
    @Autowired
    private GaiaRepPriceMapper gaiaRepPriceMapper;
    @Autowired
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Override
    public List<PriceGetOutData> queryReplenishData(String client, String brId) {
//        Example example = new Example(GaiaSdReplenishH.class);
//        example.createCriteria().andEqualTo("clientId",client).andEqualTo("repBrId",brId).andEqualTo("repStatus","0");
        GaiaRepPrice gaiaRepPrice = new GaiaRepPrice();
        gaiaRepPrice.setClient(client);
        gaiaRepPrice.setRepBrId(brId);
        gaiaRepPrice.setRepStatus("0");
        List<PriceGetOutData> getReplishList = gaiaPriceGetMapper.getReplishList(gaiaRepPrice);
        if (CollectionUtil.isEmpty(getReplishList)) {
            return null;
        }
        List<String> pgId = getReplishList.stream().map(PriceGetOutData::getRepPgId).distinct().collect(Collectors.toList());

        pgId.forEach(item -> {
            Example example = new Example(GaiaRepPrice.class);
            example.createCriteria().andEqualTo("client", client).andEqualTo("repBrId", brId).andEqualTo("repBrType","0").andEqualTo("repStatus", "0");
            gaiaRepPrice.setRepStatus("1");
            gaiaRepPriceMapper.updateByExampleSelective(gaiaRepPrice, example);
        });
        return getReplishList;
    }

    @Override
    public List<PriceGetOutData> queryChainReplenishData(String client, String brId) {
//        Example example = new Example(GaiaSdReplenishH.class);
//        example.createCriteria().andEqualTo("clientId",client).andEqualTo("repBrId",brId).andEqualTo("repStatus","0");
        GaiaRepPrice gaiaRepPrice = new GaiaRepPrice();
        gaiaRepPrice.setClient(client);
        gaiaRepPrice.setRepBrId(brId);
        gaiaRepPrice.setRepStatus("0");
        List<PriceGetOutData> getReplishList = gaiaPriceGetMapper.getChainReplishList(gaiaRepPrice);
        if (CollectionUtil.isEmpty(getReplishList)) {
            return null;
        }
        List<String> pgId = getReplishList.stream().map(PriceGetOutData::getRepPgId).distinct().collect(Collectors.toList());

        Example example2 = new Example(GaiaStoreData.class);
        example2.createCriteria().andEqualTo("client", client).andEqualTo("stoCode", brId);
        GaiaStoreData store = gaiaStoreDataMapper.selectOneByExample(example2);

        pgId.forEach(item -> {
            Example example = new Example(GaiaRepPrice.class);
            example.createCriteria().andEqualTo("client", client).andEqualTo("repBrId", store.getStoDcCode()).andEqualTo("repBrType","1").andEqualTo("repStatus", "0");
            gaiaRepPrice.setRepStatus("1");
            gaiaRepPriceMapper.updateByExampleSelective(gaiaRepPrice, example);
        });
        return getReplishList;
    }

    @Override
    public int insertPriceGet(PriceGetInData indata) {
        indata.getList().forEach(item -> {
            item.setClient(indata.getClient());
//            item.setPrgSiteCode(indata.getBrId());
        });
        gaiaPriceGetMapper.insertBatch(indata.getList());
        return 0;
    }

    @Override
    public int updateReplenishHGetStatus(GaiaSdReplenishH gaiaSdReplenishH) {
        GaiaSdReplenishH inData = new GaiaSdReplenishH();
        inData.setClientId(gaiaSdReplenishH.getClientId());
        inData.setGsrhBrId(gaiaSdReplenishH.getGsrhBrId());
//        inData.setGsrhDate(gaiaSdReplenishH.getGsrhDate());
        inData.setGsrhVoucherId(gaiaSdReplenishH.getGsrhVoucherId());
        GaiaSdReplenishH data = gaiaSdReplenishHMapper.selectOne(inData);
        data.setGsrhGetStatus("2");
        gaiaSdReplenishHMapper.update(data);
        return 0;
    }

    @Override
    public int updateRepPriceStatus(String client, String brId, String pgId) {
        if (!StringUtils.isEmpty(pgId)) {
            Example example3 = new Example(GaiaRepPrice.class);
            example3.createCriteria().andEqualTo("client", client).andEqualTo("repPgId", pgId);
            List<GaiaRepPrice> repPrice = gaiaRepPriceMapper.selectByExample(example3);
            if(CollectionUtil.isNotEmpty(repPrice)){
                if("1".equals(repPrice.get(0).getRepBrType())){
                    Example example2 = new Example(GaiaStoreData.class);
                    example2.createCriteria().andEqualTo("client", client).andEqualTo("stoCode", brId);
                    GaiaStoreData store = gaiaStoreDataMapper.selectOneByExample(example2);

                    Example example = new Example(GaiaRepPrice.class);
                    example.createCriteria().andEqualTo("client", client).andEqualTo("repBrId", store.getStoDcCode()).andEqualTo("repPgId", pgId);
                    GaiaRepPrice temp = new GaiaRepPrice();
                    temp.setRepStatus("2");
                    gaiaRepPriceMapper.updateByExampleSelective(temp, example);
                }else {
                    Example example = new Example(GaiaRepPrice.class);
                    example.createCriteria().andEqualTo("client", client).andEqualTo("repBrId", brId).andEqualTo("repPgId", pgId);
                    GaiaRepPrice temp = new GaiaRepPrice();
                    temp.setRepStatus("2");
                    gaiaRepPriceMapper.updateByExampleSelective(temp, example);
                }
            }
        }
        return 0;
    }

    @Override
    public List<GaiaSupplierBusiness> querySupplierInfo(String client, String brId) {
        Example example = new Example(GaiaSupplierBusiness.class);
        example.createCriteria().andEqualTo("client", client).andEqualTo("supSite", brId).andEqualTo("supStatus", "0").andIsNotNull("supSysParm");
        return gaiaSupplierBusinessMapper.selectByExample(example);
    }

    @Override
    public List<GaiaSupplierBusiness> queryChainSupplierInfo(String client, String brId) {
        Example  example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", client).andEqualTo("stoCode", brId);
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectOneByExample(example);
        if(ObjectUtil.isNotEmpty(gaiaStoreData)){
            example = new Example(GaiaSupplierBusiness.class);
            example.createCriteria().andEqualTo("client", client).andEqualTo("supSite", gaiaStoreData.getStoDcCode()).andEqualTo("supStatus", "0").andIsNotNull("supSysParm");
            return gaiaSupplierBusinessMapper.selectByExample(example);
        }else
            return null;
    }

    @Override
    public int checkSupplierDataByPgId(String client, String brId, RepPriceSupplierCheckData inData) {
        int flag = 0;
        for (GaiaSupplierBusiness item : inData.getGaiaSupplierBusinessesList()) {
            Example example = new Example(GaiaPriceGet.class);
            example.createCriteria().andEqualTo("client", client).andEqualTo("gsrdVoucherId", inData.getPgId()).andEqualTo("prgSupplierCode", item.getSupSelfCode());

            if (gaiaPriceGetMapper.selectCountByExample(example) > 0) {
                flag = 1;
            } else {
                flag = 0;
                return flag;
            }
        }
        return flag;
    }

    @Override
    public QueryLoginInfoInData queryLoginInfoFromRedis(QueryLoginInfoInData inData){
        String loginInfo = "";
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client",inData.getClientId()).andEqualTo("stoCode",inData.getBrId());
        GaiaStoreData storeData = gaiaStoreDataMapper.selectOneByExample(example);
        if(ObjectUtil.isNotEmpty(storeData)){
            if(StringUtils.isEmpty(storeData.getStoChainHead())){
                loginInfo = (String)this.redisManager.get(inData.getClientId()+"/"+inData.getBrId()+"/"+inData.getClassName());
            }else{
                loginInfo = (String)this.redisManager.get(inData.getClientId()+"/"+storeData.getStoChainHead()+"/"+inData.getClassName());
            }
        }
        JSONObject jsonObject = JSON.parseObject(loginInfo);
        QueryLoginInfoInData data = JSONObject.toJavaObject(jsonObject, QueryLoginInfoInData.class);
        return data;
    }

    @Override
    public int insertLoginInfoToRedis(QueryLoginInfoInData inData) {
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client",inData.getClientId()).andEqualTo("stoCode",inData.getBrId());
        GaiaStoreData storeData = gaiaStoreDataMapper.selectOneByExample(example);
        if(ObjectUtil.isNotEmpty(storeData)){
            if(StringUtils.isEmpty(storeData.getStoChainHead())){
                this.redisManager.set(inData.getClientId()+"/"+inData.getBrId()+"/"+inData.getClassName(),inData.toString());
            }else{
                this.redisManager.set(inData.getClientId()+"/"+storeData.getStoChainHead()+"/"+inData.getClassName(),inData.toString());
            }
        }
        return 1;
    }
}
