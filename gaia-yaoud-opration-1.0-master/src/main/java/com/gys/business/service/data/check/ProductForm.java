package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/14 18:08
 **/
@Data
public class ProductForm {
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(value = "门店ID", example = "10004")
    private String storeId;
    @ApiModelProperty(value = "关键词", example = "感冒")
    private String keywords;
}
