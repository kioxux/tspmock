package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class NewStoreDistributionRecordDInData implements Serializable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
     * 门店编码
     */
    @ApiModelProperty(value="门店编码")
    private String storeId;

    /**
     * 创建日期
     */
    @ApiModelProperty(value="创建日期")
    private String createDate;

    /**
     * 单号
     */
    @ApiModelProperty(value="单号")
    private String orderId;
}
