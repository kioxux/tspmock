package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/11 11:34 下午
 */
@Data
public class OrderDetailCountVO {
    @ApiModelProperty(value = "要货数量汇总", example = "23")
    private BigDecimal requestQuantity;
    @ApiModelProperty(value = "配送金额汇总", example = "450")
    private BigDecimal sendAmount;
    @ApiModelProperty(value = "零售金额汇总", example = "700")
    private BigDecimal saleAmount;
}
