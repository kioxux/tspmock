package com.gys.business.service.impl;

import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.CommonMapper;
import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.service.CommonService;
import com.gys.business.service.ThirdParaService;
import com.gys.business.service.data.GetCosParaOutData;
import com.gys.business.service.data.GetFileOutData;
import com.gys.business.service.data.upload.UpLoadDto;
import com.gys.common.enums.SerialCodeTypeEnum;
import com.gys.common.redis.RedisManager;
import com.gys.util.CosUtil;
import com.gys.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;

@Service
@Slf4j
public class CommonServiceImpl implements CommonService {
    @Autowired
    private ThirdParaService thirdParaService;
    @Resource
    private RedisManager redisManager;
    @Autowired
    private CommonMapper commonMapper;

    public GetFileOutData fileUpload(MultipartFile file) {
        GetFileOutData outData = new GetFileOutData();
        try {
            GetCosParaOutData para = this.thirdParaService.getCosPara();
            String secretId = para.getSecretId();
            String secretKey = para.getSecretKey();
            String regionName = para.getRegionName();
            String bucketName = para.getBucketName();
            CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
            String path = cosUtil.upload(file);
            String url = cosUtil.getFileUrl(path,DateUtil.stringToDate("3000-12-31","yyyy-MM-dd"));
            outData.setPath(path);
            outData.setUrl(url);
            log.info("<上传图片><上传图片返回参数：{}>", JSON.toJSON(outData).toString());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("<上传图片><上传图片异常：{}>", e.getMessage(), e);
        }
        return outData;
    }

    /**
     * 服务器内部上传调用
     * 返回的文件路由  有效期为 3000-12-31
     * @param file
     * @return
     */
    @Override
    public GetFileOutData fileUploadLocal(File file) {
        GetFileOutData outData = new GetFileOutData();
        GetCosParaOutData para = this.thirdParaService.getCosPara();
        String secretId = para.getSecretId();
        String secretKey = para.getSecretKey();
        String regionName = para.getRegionName();
        String bucketName = para.getBucketName();
        CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
        String path = cosUtil.uploadLocal(file);
        String url = cosUtil.getFileUrl(path,DateUtil.stringToDate("3000-12-31","yyyy-MM-dd"));
        outData.setPath(path);
        outData.setUrl(url);
        return outData;
    }

    @Override
    public GetFileOutData getFileUrl(GetFileOutData file) {
        GetFileOutData outData = new GetFileOutData();
        GetCosParaOutData para = this.thirdParaService.getCosPara();
        String secretId = para.getSecretId();
        String secretKey = para.getSecretKey();
        String regionName = para.getRegionName();
        String bucketName = para.getBucketName();
        CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
        String url = cosUtil.getFileUrl("20211108141502526\\163.jpg", DateUtil.stringToDate("3000-12-31","yyyy-MM-dd"));
        System.out.println("url = " + url);
        return null;
    }

    @Override
    public String getSerialCode(String client, SerialCodeTypeEnum serialCodeTypeEnum) {
        String prefix = serialCodeTypeEnum.getCodePrefix();
        String key = String.format("%s:%s", client, prefix);
        Long value = redisManager.increment(key, 1);
        String fixedZero = "00000" + value;
        return String.format("%s%s", prefix, fixedZero.substring(fixedZero.length() - 5));
    }

    @Override
    public String getSdSerialCode(String client, String stoCode, SerialCodeTypeEnum serialCodeTypeEnum) {
        String prefix = serialCodeTypeEnum.getCodePrefix();
        String key = String.format("%s:%s:%s", client, stoCode, prefix);
        Long value = redisManager.increment(key, 1);
        String fixedZero = "00000" + value;
        return String.format("%s%s", prefix, fixedZero.substring(fixedZero.length() - 5));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String selectNextVoucherId(GaiaSdReplenishH inData) {
        String maxVoucherId = "";
        String voucherId = commonMapper.selectNextCodeByReplenish(inData.getClientId(),"PD");
//        inData.setGsrhBrId(null);
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(voucherId)){//存在则变更code_val表信息
            //计算补货表中最大补货单号
            String numStr =  voucherId.substring(6);
            int n = numStr.length();
            int newNum = Integer.parseInt(numStr) + 1;
            String newNumStr = String.valueOf(newNum);
            n = Math.min(n,newNumStr.length());
            maxVoucherId = voucherId.subSequence(0,voucherId.length() - n) + newNumStr;

            String codeVoucherId = commonMapper.selectNextCode(inData.getClientId(),"PD",6);
            String numStr1 =  codeVoucherId.substring(6);
            int newNum1 = Integer.parseInt(numStr1);
            if(newNum > newNum1){
                commonMapper.addBatchToCode(inData.getClientId(),maxVoucherId,"PD");
            }else {
                maxVoucherId = codeVoucherId;
            }
        }else {
            maxVoucherId = commonMapper.selectNextCode(inData.getClientId(),"PD",6);
        }
        return maxVoucherId;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String selectNextCancelId(String clientId) {
        String maxVoucherId = "";
        String voucherId = commonMapper.selectNextCodeByCancel(clientId,"KD");
//        inData.setGsrhBrId(null);
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(voucherId)){//存在则变更code_val表信息
            //计算补货表中最大补货单号
            String numStr =  voucherId.substring(6);
            int n = numStr.length();
            int newNum = Integer.parseInt(numStr) + 1;
            String newNumStr = String.valueOf(newNum);
            n = Math.min(n,newNumStr.length());
            maxVoucherId = voucherId.subSequence(0,voucherId.length() - n) + newNumStr;

            String codeVoucherId = commonMapper.selectNextCode(clientId,"KD",7);
            String numStr1 =  codeVoucherId.substring(6);
            int newNum1 = Integer.parseInt(numStr1);
            if(newNum > newNum1){
                commonMapper.addBatchToCode(clientId,maxVoucherId,"KD");
            }else {
                maxVoucherId = codeVoucherId;
            }
        }else {
            maxVoucherId = commonMapper.selectNextCode(clientId,"KD",7);
        }
        return maxVoucherId;
    }

    @Override
    public GetFileOutData fileUpload(UpLoadDto upLoadDto) {
        GetFileOutData outData = new GetFileOutData();
        GetCosParaOutData para = this.thirdParaService.getCosPara();
        String secretId = para.getSecretId();
        String secretKey = para.getSecretKey();
        String regionName = para.getRegionName();
        String bucketName = para.getBucketName();
        CosUtil cosUtil = CosUtil.getInstance(secretId, secretKey, regionName, bucketName);
        String path = cosUtil.upload(upLoadDto);
        if (StringUtils.isNotBlank(path)) {
            String url = cosUtil.getFileUrl(path, DateUtil.stringToDate("3000-12-31", "yyyy-MM-dd"));
            outData.setPath(path);
            outData.setUrl(url);
        }
        return outData;
    }
}
