package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdPrintSale;

public interface SaleBillSetService {
    void save(GaiaSdPrintSale inData);

    GaiaSdPrintSale get(GaiaSdPrintSale inData);
}
