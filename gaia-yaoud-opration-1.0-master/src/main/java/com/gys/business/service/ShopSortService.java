//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetGaiaDepInData;
import com.gys.business.service.data.GetShopDetailOutData;
import com.gys.business.service.data.GetShopInData;
import com.gys.business.service.data.GetShopSortOutData;
import com.gys.common.data.GetLoginOutData;
import java.util.List;

public interface ShopSortService {
    List<GetShopSortOutData> querySort(GetShopInData inData);

    List<GetShopDetailOutData> queryDetail(GetShopInData inData);

    void add(GetShopInData inData);

    void editSave(GetShopInData inData);

    void addAll(List<GetShopInData> inDataList);

    void delete(List<GetShopInData> inData);

    void update(List<GetShopDetailOutData> inData, GetLoginOutData userInfo);

    List<GetGaiaDepInData> queryStore(GetShopInData inData);
}
