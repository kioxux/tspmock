package com.gys.business.service.goodspurchase;

import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.entity.GaiaProductBasic;
import com.gys.business.service.data.GetProductNewOutData;
import com.gys.business.service.data.productBasic.GetProductBasicInData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.goodspurchase.AddYdProductCodeInfo;
import com.gys.common.data.goodspurchase.ConfirmYdProCodeDTO;
import com.gys.common.data.goodspurchase.ProductRelationMaintainListDTO;
import com.gys.common.data.goodspurchase.SearchSameProductDTO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author wu mao yin
 * @Title: 客户商品对应关系查询及维护
 * @date 2021/10/2717:07
 */
public interface ProductRelationMaintainService {

    /**
     * 查询客户 client 集合
     *
     * @return JsonResult
     */
    JsonResult selectCustomerClientList();

    /**
     * 根据客户获取地点集合
     *
     * @param client client
     * @return JsonResult
     */
    JsonResult selectCustomerProSiteList(String client);

    /**
     * 查询客户商品编码列表
     *
     * @param getProductBasicInData getProductBasicInData
     * @return JsonResult
     */
    JsonResult selectCustomerProCodeList(GetProductBasicInData getProductBasicInData);

    /**
     * 查询商品对应关系维护列表
     *
     * @param productRelationMaintainListDTO productRelationMaintainListDTO
     * @return JsonResult
     */
    JsonResult selectProductRelationMaintainList(ProductRelationMaintainListDTO productRelationMaintainListDTO);

    /**
     * 导出商品对应关系维护列表
     *
     * @param productRelationMaintainListDTO productRelationMaintainListDTO
     * @param response                       response
     */
    void exportProductRelationMaintainList(ProductRelationMaintainListDTO productRelationMaintainListDTO, HttpServletResponse response);

    /**
     * 查询商品编码详情信息
     *
     * @param addYdProductCodeInfo addYdProductCodeInfo
     * @return JsonResult
     */
    JsonResult selectProductCodeInfo(AddYdProductCodeInfo addYdProductCodeInfo);

    /**
     * 新建药德编码
     *
     * @param addYdProductCodeInfo addYdProductCodeInfo
     * @return JsonResult
     */
    JsonResult saveYdProCode(AddYdProductCodeInfo addYdProductCodeInfo);

    /** add by jinwencheng on 2021-12-30 13:50:13 新建药德商品编码页面，新建前先查询是否有符合条件的商品 **/
    JsonResult getSameProduct(SearchSameProductDTO searchSameProductDTO);
    /** add end **/

    JsonResult confirmYdProCode(ConfirmYdProCodeDTO confirmYdProCodeDTO);

}
