package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 单品请求参数
 *
 * @author xiaoyuan on 2020/8/19
 */
@Data
public class FindPromotionInData implements Serializable {
    private static final long serialVersionUID = 4300661491047297979L;

    /**
     * 加盟号
     */
    private String client ;

    /**
     * 店号
     */
    private String brId ;

    /**
     * 商品编码
     */
    private String gspusProId;

    /**
     * 促销类型编码
     */
    private String gssdPmId;

    /**
     * 促销id
     */
    private String gssdPmActivityId;
    /**
     * 促销参数
     */
    private String gssdPmActivityFlag;
}
