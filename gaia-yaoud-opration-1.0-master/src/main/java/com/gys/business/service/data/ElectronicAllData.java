package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 基本信息保存
 *
 * @author xiaoyuan on 2020/10/28
 */
@Data
public class ElectronicAllData implements Serializable {

    private static final long serialVersionUID = -5639471526312443883L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "当前用户id")
    private String userId;

    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    @ApiModelProperty(value = "是否审核 N为否，Y为是")
    private String gsebsStatus;

    @NotNull(message = "是否重复添加时间不可为空!")
    @ApiModelProperty(value = "是否重复添加时间")
    private boolean timeFlag;

    @Valid
    @NotNull(message = "主题设置信息不可为空")
    @ApiModelProperty(value = "主题设置信息")
    private ThemeSettingVO themeSettingVO;

    @ApiModelProperty(value = "选择电子券信息")
    private ThemeSettingToAutoSetVO toAutoSetVO;

    @ApiModelProperty(value = "送券信息设置")
    private ElectronCouponsInVO coupon;

    @Valid
    @NotNull(message = "用券信息不可为空")
    @ApiModelProperty(value = "用券信息设置")
    private ElectronVoucherInVO voucher;
}
