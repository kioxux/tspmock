
package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Data
public class SalesInquireInData implements Serializable {
    private static final long serialVersionUID = 7743366006928395784L;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "销售单号")
    private String billNo;

    @ApiModelProperty(value = "特殊药品分类   1:毒性药品 / 2:麻醉药品 / 3:一类精神药品 / 4:二类精神药品 / 5:易制毒药品（麻黄碱）/ 6:放射性药品 / " +
            "7:生物制品（含胰岛素） /8:兴奋剂（除胰岛素） / 9:第一类器械 / 10:第二类器械 /  11:第三类器械 / 12:其它管制")
    private String specialDrugClass;

    @ApiModelProperty(value = "退货  0 : 不选 / 1:选中退货")
    private String ifReturn;

    @ApiModelProperty(value = "收银员")
    private String cashier;

    @ApiModelProperty(value = "处方号")
    private String prescriptionNum;

    @ApiModelProperty(value = "是否拆零  0:否 / 1:是")
    private String ifSplit;

    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "会员卡号")
    private String memberCardNum;

    @ApiModelProperty(value = "整单金额")
    private String totalAmt;

    @ApiModelProperty(value = "支付方式  1:现金 / 2:银行卡 / 3:微信/支付宝 / 4:医保 /  5:储值卡")
    private String payMethod;

    @ApiModelProperty(value = "批号")
    private String batchNo;

    @ApiModelProperty(value = "营业员")
    private String salesPerson;

    @ApiModelProperty(value = "医生")
    private String doctor;

    @NotBlank(message = "起始日期不能为空")
    @ApiModelProperty(value = "起始日期")
    private String saleDate;

    @NotBlank(message = "结束日期不能为空")
    @ApiModelProperty(value = "结束日期")
    private String endDate;

    @ApiModelProperty(value = "当前页")
    private Integer pageNum;

    @ApiModelProperty(value = "总数")
    private Integer pageSize;

    @ApiModelProperty(value = "多店号")
    private String[] brIds;

    @ApiModelProperty(value = "连锁公司ID")
    private String stoChainHead;

    @ApiModelProperty(value = "多商品")
    private String[] prIds;

    @ApiModelProperty(value = "销售天数 7 30 90")
    private String type;

    /**
     * 临时备注
     */
    private String gsshDzqdyActno5;

    /**
     * 会员id
     */
    private String memberId;


    /**
     * 是否是处方药
     */
    @ApiModelProperty(value = "是否是处方药")
    private Boolean prescription;

    /**
     * 是否查询生成
     */
    @ApiModelProperty(value = "是否查询生成")
    private Boolean isGenerate;
    /**
     * 查询当前包含有什么类型的药物
     */
    @ApiModelProperty(value = "查询当前包含有什么类型的药物")
    private List<String> proPresclasss;

}
