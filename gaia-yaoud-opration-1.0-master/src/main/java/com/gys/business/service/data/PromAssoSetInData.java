package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromAssoSetInData implements Serializable {
    private static final long serialVersionUID = -4889658941585484546L;
    private String clientId;
    private String gspasVoucherId;
    private String gspasSerial;
    private String gspasSeriesId;
    private BigDecimal gspasAmt;
    private String gspasRebate;
    private String gspasQty;
}
