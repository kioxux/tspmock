package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "加盟商")
public class GetSupOutData {
    @ApiModelProperty(value = "门店")
    private String supBrId;
    @ApiModelProperty(value = "供应商自编码")
    private String supCode;
    @ApiModelProperty(value = "采购付款条件")
    private String payTerm;
    @ApiModelProperty(value = "供应商名称")
    private String supName;
    @ApiModelProperty(value = "助记码")
    private String supPym;
    @ApiModelProperty(value = "起订金额")
    private String supMixAmt;
}
