package com.gys.business.service.data;

import lombok.Data;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
public class PriceGetOutData {
    /**
     * 加盟商
     */
    private String clientId;

    /**
     * 比价单号
     */
    private String repPgId;

    /**
     * 比价日期
     */
    private String repDate;

    /**
     * 行号
     */
    private String repPgItem;

    /**
     * 比价门店
     */
    private String repBrId;

    /**
     * 商品编码
     */
    private String repProId;

    /**
     * 补货量
     */
    private String repNeedQty;

    /**
     * 状态
     */
    private String repStatus;

    /**
     * 创建日期
     */
    private String repCreateDate;
    /**
     * 创建时间
     */
    private String repCreateTime;
    /**
     * 创建人员
     */
    private String repCreateUser;

    /**
     * 通用名称
     */
    private String proCommonname;

    /**
     * 商品描述
     */
    private String proDepict;

    /**
     * 助记码
     */
    private String proPym;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 剂型
     */
    private String proForm;

    /**
     * 细分剂型
     */
    private String proPartform;

    /**
     * 国际条形码1
     */
    private String proBarcode;

    /**
     * 国际条形码2
     */
    private String proBarcode2;

    /**
     *  批准文号分类 1-国产药品，2-进口药品，3-国产器械，4-进口器械，5-中药饮片，6-特殊化妆品，7-消毒用品，8-保健食品，9-QS商品，10-其它
     */
    private String proRegisterClass;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 生产企业代码
     */
    private String proFactoryCode;

    /**
     * 生产企业
     */
    private String proFactoryName;

    /**
     * 中药规格
     */
    private String proTcmSpecs;

}
