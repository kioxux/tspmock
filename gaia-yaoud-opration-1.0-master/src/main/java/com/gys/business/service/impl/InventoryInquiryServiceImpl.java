package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaInventoryInquiryMapper;
import com.gys.business.mapper.GaiaSdStoresGroupMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.GaiaStoreCategoryType;
import com.gys.business.service.InventoryInquiryService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.ReportForms.InventoryInquiryByClientAndBatchNoOutData;
import com.gys.business.service.data.ReportForms.InventoryInquiryByClientAndSiteOutData;
import com.gys.business.service.data.ReportForms.InventoryInquiryByClientAndSiteOutDataByBatch;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.StoreAttributeEnum;
import com.gys.common.enums.StoreDTPEnum;
import com.gys.common.enums.StoreMedicalEnum;
import com.gys.common.enums.StoreTaxClassEnum;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.gys.util.CopyUtil;
import com.gys.util.FileUtil;
import com.gys.util.UtilMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@Slf4j
public class InventoryInquiryServiceImpl implements InventoryInquiryService {
    @Resource
    private GaiaInventoryInquiryMapper inventoryInquiryMapper;
    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Autowired
    private GaiaSdStoresGroupMapper gaiaSdStoresGroupMapper;

    @Override
    public List<InventoryInquiryOutData> getInventoryInquiryListWeb(InventoryInquiryInData inData) {
        List<InventoryInquiryOutData> outData = new ArrayList<>();

        //查询是否开启权限
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        if (ObjectUtil.isEmpty(flag)) {
            inData.setFlag("0");
        } else {
            inData.setFlag(flag);
        }
        if(ObjectUtil.isNotEmpty(inData.getClassArr())){
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        if (UtilMessage.ZERO.equals(inData.getType())) {
            outData = this.inventoryInquiryMapper.getInventoryInquiryList(inData);
        } else if (UtilMessage.ONE.equals(inData.getType())) {
            outData = this.inventoryInquiryMapper.getInventoryInquiryToStock(inData);
        }
        if (CollUtil.isNotEmpty(outData)){
            if ("1".equals(flag)) {
                List<Map<String, Object>> blackList = inventoryInquiryMapper.selectBlackListByBatchNo(inData.getClientId(), inData.getGssmBrId());
                if (blackList != null && blackList.size() > 0) {
                    if (UtilMessage.ZERO.equals(inData.getType())) {
                        List<InventoryInquiryOutData> removeData = new ArrayList<>();
                        for (int i = 0; i < outData.size();i++) {
                            InventoryInquiryOutData data = outData.get(i);
                            String proCode = data.getGssmProId();
                            String batchNo = data.getGssmBatchNo();
                            for (Map<String, Object> map : blackList) {
                                if (ObjectUtil.isNotEmpty(map) && proCode.equals(map.get("proCode").toString())
                                        && batchNo.equals(map.get("batchNo").toString())) {
                                    removeData.add(data);
                                }
                            }
                        }
                        if (ObjectUtil.isNotEmpty(removeData) && removeData.size() > 0){
                            outData.removeAll(removeData);
                        }
                    } else if (UtilMessage.ONE.equals(inData.getType())) {
                        for (InventoryInquiryOutData data : outData) {
                            String proCode = data.getGssmProId();
                            //                    String batchNo = data.getGssmBatchNo();
                            for (Map<String, Object> map : blackList) {
                                if (ObjectUtil.isNotEmpty(map) && proCode.equals(map.get("proCode").toString())) {
                                    String costAmount = map.get("costAmount").toString();
                                    String retailSales = map.get("retailSales").toString();
                                    String qty = map.get("stockQty").toString();
                                    data.setCostAmount(data.getCostAmount().subtract(new BigDecimal(costAmount)));
                                    data.setRetailSales(data.getRetailSales().subtract(new BigDecimal(retailSales)));
                                    data.setQty(data.getQty().subtract(new BigDecimal(qty)));
                                }
                            }
                        }
                    }
                }
            }
            List<InventoryInquiryOutData> finalOutData = outData;
            IntStream.range(0, outData.size()).forEach(i -> (finalOutData.get(i)).setIndex(i + 1));
        }
        return outData;
    }


    @Override
    public PageInfo<InventoryInquiryOutData> getEexpiryDateWarning(InventoryInquiryInData inData) {
        //先查询权限   flag  0：不开启  1：开启
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        if (ObjectUtil.isEmpty(flag)) {
            inData.setFlag("0");
        } else {
            inData.setFlag(flag);
        }
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
//        SimpleDateFormat format = new SimpleDateFormat(UtilMessage.YYYYMMDD);
        if (StrUtil.isNotBlank(inData.getExpiryData())) {
            boolean number = NumberUtil.isNumber(inData.getExpiryData());
            if (!number) {
                throw new BusinessException("请输入正确数字");
            }
//            DateTime dateTime = DateUtil.offsetDay(new Date(), Integer.valueOf(inData.getExpiryData()));
//            inData.setExpiryData(format.format(dateTime));
        }
        List<InventoryInquiryOutData> outData = this.inventoryInquiryMapper.getInventoryInquiryList(inData);
        if (CollUtil.isNotEmpty(outData)) {
            if ("1".equals(flag)) {
                List<Map<String, Object>> blackList = inventoryInquiryMapper.selectBlackListByBatchNo(inData.getClientId(), inData.getGssmBrId());
                if (blackList != null && blackList.size() > 0) {
                    List<InventoryInquiryOutData> removeData = new ArrayList<>();
                    for (int i = 0; i < outData.size(); i++) {
                        String proCode = outData.get(i).getGssmProId();
                        String batchNo = outData.get(i).getGssmBatchNo().trim();
                        for (Map<String, Object> map : blackList) {
                            if (ObjectUtil.isNotEmpty(map) && proCode.equals(map.get("proCode").toString())
                                    && batchNo.equals(map.get("batchNo").toString().trim())) {
                                removeData.add(outData.get(i));
                            }
                        }
                    }
                    if (ObjectUtil.isNotEmpty(removeData) && removeData.size() > 0){
                        outData.removeAll(removeData);
                    }
                }
            }
        }

        PageInfo pageInfo = null;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public PageInfo<InventoryInquiryOutData> getEexpiryDateWarningAll(InventoryInquiryInData inData) {
        //先查询权限   flag  0：不开启  1：开启
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        if (ObjectUtil.isEmpty(flag)) {
            inData.setFlag("0");
        } else {
            inData.setFlag(flag);
        }
        SimpleDateFormat format = new SimpleDateFormat(UtilMessage.YYYYMMDD);
        if (StrUtil.isNotBlank(inData.getExpiryData())) {
            boolean number = NumberUtil.isNumber(inData.getExpiryData());
            if (!number) {
                throw new BusinessException("请输入正确数字");
            }
            DateTime dateTime = DateUtil.offsetDay(new Date(), Integer.valueOf(inData.getExpiryData()));
            inData.setExpiryData(format.format(dateTime));
        }
        List<InventoryInquiryOutData> outData = this.inventoryInquiryMapper.getInventoryInquiryList(inData);
        if (CollUtil.isNotEmpty(outData)) {
            if ("1".equals(flag)) {
                List<Map<String, Object>> blackList = inventoryInquiryMapper.selectBlackListByBatchNo(inData.getClientId(), inData.getGssmBrId());
                if (blackList != null && blackList.size() > 0) {
                    for (int i = 0; i < outData.size(); i++) {
                        String proCode = outData.get(i).getGssmProId();
                        String batchNo = outData.get(i).getGssmBatchNo().trim();
                        for (Map<String, Object> map : blackList) {
                            if (ObjectUtil.isNotEmpty(map) && proCode.equals(map.get("proCode").toString())
                                    && batchNo.equals(map.get("batchNo").toString().trim())) {
                                outData.remove(outData.get(i));
                                i--;
                            }
                        }
                    }
                }
            }
        }

        PageInfo pageInfo = null;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public void getEexpiryDateWarningExport(InventoryInquiryInData inData, HttpServletResponse response)  throws Exception {
        //先查询权限   flag  0：不开启  1：开启
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        if (ObjectUtil.isEmpty(flag)) {
            inData.setFlag("0");
        } else {
            inData.setFlag(flag);
        }
        SimpleDateFormat format = new SimpleDateFormat(UtilMessage.YYYYMMDD);
        if (StrUtil.isNotBlank(inData.getExpiryData())) {
            boolean number = NumberUtil.isNumber(inData.getExpiryData());
            if (!number) {
                throw new BusinessException("请输入正确数字");
            }
            DateTime dateTime = DateUtil.offsetDay(new Date(), Integer.valueOf(inData.getExpiryData()));
            inData.setExpiryData(format.format(dateTime));
        }
        List<InventoryInquiryOutData> outData = this.inventoryInquiryMapper.getInventoryInquiryList(inData);
        if (CollUtil.isNotEmpty(outData)) {
            if ("1".equals(flag)) {
                List<Map<String, Object>> blackList = inventoryInquiryMapper.selectBlackListByBatchNo(inData.getClientId(), inData.getGssmBrId());
                if (blackList != null && blackList.size() > 0) {
                    for (int i = 0; i < outData.size(); i++) {
                        String proCode = outData.get(i).getGssmProId();
                        String batchNo = outData.get(i).getGssmBatchNo();
                        for (Map<String, Object> map : blackList) {
                            if (ObjectUtil.isNotEmpty(map) && proCode.equals(map.get("proCode").toString())
                                    && batchNo.equals(map.get("batchNo").toString())) {
                                outData.remove(outData.get(i));
                                i--;
                            }
                        }
                    }
                }
            }
        }
        String fileName = FileUtil.getPath() + "效期预警导出" + System.currentTimeMillis() + ".xlsx";
        List<InventoryInquiryOutDataExport> outExprot = CopyUtil.copyList(outData,InventoryInquiryOutDataExport.class);


        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), InventoryInquiryOutDataExport.class).sheet("sheet1").doWrite(outExprot);


//            // 这里 需要指定写用哪个class去写
//        ExcelWriter excelWriter = null;
//        OutputStream outputStream=null;
//        try {
//            excelWriter = EasyExcel.write(fileName, InventoryInquiryOutDataExport.class).build();
//            WriteSheet writeSheet = EasyExcel.writerSheet("sheet1").build();
////            response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
////             outputStream=response.getOutputStream();
//            excelWriter.write(outExprot, writeSheet);
////            outputStream.flush();
////            outputStream.close();
//        } finally {
//            excelWriter.finish();
//        }
    }


    /**
     * 不同的条件走不同的逻辑 不同的表
     * 不要问为什么
     * 按地点查询
     * @param inData
     * @return
     */
    @Override
    public PageInfo inventoryInquiryListByStoreAndDc(InventoryInquiryInData inData) {
        //匹配值
        Set<String> stoGssgTypeSet = new HashSet<>();
        boolean noChooseFlag = true;
        if (inData.getStoGssgType() != null) {
            List<GaiaStoreCategoryType> stoGssgTypes = new ArrayList<>();
            for (String s : inData.getStoGssgType().split(StrUtil.COMMA)) {
                String[] str = s.split(StrUtil.UNDERLINE);
                GaiaStoreCategoryType gaiaStoreCategoryType = new GaiaStoreCategoryType();
                gaiaStoreCategoryType.setGssgType(str[0]);
                gaiaStoreCategoryType.setGssgId(str[1]);
                stoGssgTypes.add(gaiaStoreCategoryType);
                stoGssgTypeSet.add(str[0]);
            }
            inData.setStoGssgTypes(stoGssgTypes);
            noChooseFlag = false;
        }
        if (inData.getStoAttribute() != null) {
            noChooseFlag = false;
            inData.setStoAttributes(Arrays.asList(inData.getStoAttribute().split(StrUtil.COMMA)));
        }
        if (inData.getStoIfMedical() != null) {
            noChooseFlag = false;
            inData.setStoIfMedicals(Arrays.asList(inData.getStoIfMedical().split(StrUtil.COMMA)));
        }
        if (inData.getStoTaxClass() != null) {
            noChooseFlag = false;
            inData.setStoTaxClasss(Arrays.asList(inData.getStoTaxClass().split(StrUtil.COMMA)));
        }
        if (inData.getStoIfDtp() != null) {
            noChooseFlag = false;
            inData.setStoIfDtps(Arrays.asList(inData.getStoIfDtp().split(StrUtil.COMMA)));
        }
        List<InventoryInquiryByClientAndSiteOutData> outData = new ArrayList<>();
        if (StringUtils.isNotEmpty(inData.getProCode())) {
            inData.setProArr(inData.getProCode().split("\\s+ |\\s+|,"));
        }
        if(ObjectUtil.isNotEmpty(inData.getClassArr())){
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        outData = this.inventoryInquiryMapper.inventoryInquiryListByAllStoreAndDc(inData);
//        if (UtilMessage.ZERO.equals(inData.getType())) {
//            //按批号查询各个地点下的库存
//            outData = this.inventoryInquiryMapper.inventoryInquiryListByStoreAndDcAndBatchNo(inData);
//        } else if (UtilMessage.ONE.equals(inData.getType())) {
//            if (StringUtils.isNotEmpty(inData.getProCode())) {
//                //按商品搜索的时候 没有商品信息的门店也要带出来
//                outData = this.inventoryInquiryMapper.inventoryInquiryListByAllStoreAndDc(inData);
//            } else {
//                //按地点查询
//                outData = this.inventoryInquiryMapper.inventoryInquiryListByStoreAndDc(inData);
//            }
//        }
        //是否过滤库存和库存成本额都为0的数据
        if (inData.getIsHide()!=null&&inData.getIsHide()) {
            if (outData.size() > 0) {
                outData=outData.stream()
                        .filter(out-> !(BigDecimal.ZERO.compareTo(out.getQty()) == 0 && BigDecimal.ZERO.compareTo(out.getAddAmount()) == 0))
                        .collect(Collectors.toList());
            }
        }

        List<GaiaStoreCategoryType> storeCategoryByClient = gaiaSdStoresGroupMapper.selectStoreCategoryByClient(inData.getClientId());

        for (InventoryInquiryByClientAndSiteOutData inventoryInquiry : outData){
            //转换
            if (inventoryInquiry.getStoAttribute() != null || noChooseFlag){
                inventoryInquiry.setStoAttribute(StoreAttributeEnum.getName(inventoryInquiry.getStoAttribute()));
            }
            if (inventoryInquiry.getStoIfMedical() != null || noChooseFlag){
                inventoryInquiry.setStoIfMedical(StoreMedicalEnum.getName(inventoryInquiry.getStoIfMedical()));
            }
            if (inventoryInquiry.getStoIfDtp() != null || noChooseFlag){
                inventoryInquiry.setStoIfDtp(StoreDTPEnum.getName(inventoryInquiry.getStoIfDtp()));
            }
            if (inventoryInquiry.getStoTaxClass() != null || noChooseFlag){
                inventoryInquiry.setStoTaxClass(StoreTaxClassEnum.getName(inventoryInquiry.getStoTaxClass()));
            }
            List<GaiaStoreCategoryType> collect = storeCategoryByClient.stream().filter(t -> t.getGssgBrId().equals(inventoryInquiry.getGssmBrId())).collect(Collectors.toList());
            Set<String> tmpStoGssgTypeSet = new HashSet<>(stoGssgTypeSet);
            for (GaiaStoreCategoryType storeCategoryType : collect){
                boolean flag = false;
                if (noChooseFlag){
                    if (storeCategoryType.getGssgType().contains("DX0001")){
                        inventoryInquiry.setShopType(storeCategoryType.getGssgIdName());
                    }else if (storeCategoryType.getGssgType().contains("DX0002")){
                        inventoryInquiry.setStoreEfficiencyLevel(storeCategoryType.getGssgIdName());
                    }else if (storeCategoryType.getGssgType().contains("DX0003")){
                        inventoryInquiry.setDirectManaged(storeCategoryType.getGssgIdName());
                    }else if (storeCategoryType.getGssgType().contains("DX0004")) {
                        inventoryInquiry.setManagementArea(storeCategoryType.getGssgIdName());
                    }
                }else {
                    if (!stoGssgTypeSet.isEmpty()){
                        if (tmpStoGssgTypeSet.contains("DX0001") && "DX0001".equals(storeCategoryType.getGssgType())) {
                            inventoryInquiry.setShopType(storeCategoryType.getGssgIdName());
                            tmpStoGssgTypeSet.remove("DX0001");
                        } else if (tmpStoGssgTypeSet.contains("DX0002") && "DX0002".equals(storeCategoryType.getGssgType())) {
                            inventoryInquiry.setStoreEfficiencyLevel(storeCategoryType.getGssgIdName());
                            tmpStoGssgTypeSet.remove("DX0002");
                        } else if (tmpStoGssgTypeSet.contains("DX0003") && "DX0003".equals(storeCategoryType.getGssgType())) {
                            inventoryInquiry.setDirectManaged(storeCategoryType.getGssgIdName());
                            tmpStoGssgTypeSet.remove("DX0003");
                        } else if (tmpStoGssgTypeSet.contains("DX0004") && "DX0004".equals(storeCategoryType.getGssgType())) {
                            inventoryInquiry.setManagementArea(storeCategoryType.getGssgIdName());
                            tmpStoGssgTypeSet.remove("DX0004");
                        }
                    }
                }
            }
        }


//        long start = 0;
//        long stop = 0;
        PageInfo pageInfo = null;
        if (ObjectUtil.isNotEmpty(outData)) {
            InventoryInquiryByClientAndSiteOutData outNum = new InventoryInquiryByClientAndSiteOutData();
            // 集合列的数据汇总
//            start = System.currentTimeMillis();
            for (InventoryInquiryByClientAndSiteOutData out : outData) {
                outNum.setRetailSales(CommonUtil.stripTrailingZeros(outNum.getRetailSales()).add(CommonUtil.stripTrailingZeros(out.getRetailSales())));
                outNum.setQty(CommonUtil.stripTrailingZeros(outNum.getQty()).add(CommonUtil.stripTrailingZeros(out.getQty())));
                outNum.setCostAmount(CommonUtil.stripTrailingZeros(outNum.getCostAmount()).add(CommonUtil.stripTrailingZeros(out.getCostAmount())));
                outNum.setCostRateAmount(CommonUtil.stripTrailingZeros(outNum.getCostRateAmount()).add(CommonUtil.stripTrailingZeros(out.getCostRateAmount())));
                outNum.setAddAmount(CommonUtil.stripTrailingZeros(outNum.getAddAmount()).add(CommonUtil.stripTrailingZeros(out.getAddAmount())));
            }
//            stop = System.currentTimeMillis();
//            log.info(String.valueOf(stop - start));
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(outNum);

        } else {
            pageInfo = new PageInfo();
        }


        return pageInfo;
    }

    /**
     * 按加盟商查询
     * @param inData
     * @return
     */
    @Override
    public List<InventoryInquiryOutData> getInventoryInquiryList(InventoryInquiryInData inData) {
        List<InventoryInquiryOutData> inventoryInquiryList;
        if (inData.getSpecifiedDate().equals((new SimpleDateFormat("yyyyMMdd")).format(new Date()))) {
            inventoryInquiryList = this.inventoryInquiryMapper.getInventoryInquiryList(inData);
        } else {
            inData.setStartDate(getFirstDate(StrToDate(inData.getSpecifiedDate())));
            inData.setEndDate(inData.getSpecifiedDate());
            inData.setSpecifiedDate(getBeforeDate(StrToDate(inData.getSpecifiedDate())));
            inventoryInquiryList = this.inventoryInquiryMapper.getInventoryInquiryList2(inData);
        }

        if (CollectionUtil.isNotEmpty(inventoryInquiryList)) {
            for (int i = 0; i < inventoryInquiryList.size(); ++i) {
                (inventoryInquiryList.get(i)).setIndex(i + 1);
            }
        }

        return inventoryInquiryList;
    }

    private static Date StrToDate(String str) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date date = null;

        try {
            date = format.parse(str);
        } catch (ParseException var4) {
            var4.printStackTrace();
        }

        return date;
    }

    private static String getBeforeDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(2, -1);
        return sdf.format(cal.getTime());
    }

    private static String getFirstDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(5, 1);
        return sdf.format(cal.getTime());
    }


    /**
     * 商品实时库存查询
     * ..................................................
     *
     * @param inData
     * @return
     */
    @Override
    public PageInfo inventoryInquiryListByClient(InventoryInquiryInData inData) {
        List<InventoryInquiryByClientAndBatchNoOutData> outData = new ArrayList<>();
        if (StringUtils.isNotEmpty(inData.getProCode())) {
            inData.setProArr(inData.getProCode().split("\\s+ |\\s+|,"));
        }
        if(ObjectUtil.isNotEmpty(inData.getClassArr())){
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        if (UtilMessage.ZERO.equals(inData.getType())) {
            outData = this.inventoryInquiryMapper.inventoryInquiryListByClientAndSiteAndBatchNo(inData);

        } else if (UtilMessage.ONE.equals(inData.getType())) {
            outData = this.inventoryInquiryMapper.inventoryInquiryListByClient(inData);
        }

        PageInfo pageInfo = null;
        if (ObjectUtil.isNotEmpty(outData)) {
            InventoryInquiryByClientAndBatchNoOutData outNum = new InventoryInquiryByClientAndBatchNoOutData();
            // 集合列的数据汇总
            for (InventoryInquiryByClientAndBatchNoOutData out : outData) {
                outNum.setQtySum(CommonUtil.stripTrailingZeros(outNum.getQtySum()).add(CommonUtil.stripTrailingZeros(out.getQtySum())));
                outNum.setCostSum(CommonUtil.stripTrailingZeros(outNum.getCostSum()).add(CommonUtil.stripTrailingZeros(out.getCostSum())));
                outNum.setCostRateSum(CommonUtil.stripTrailingZeros(outNum.getCostRateSum()).add(CommonUtil.stripTrailingZeros(out.getCostRateSum())));

                outNum.setStoreQty(CommonUtil.stripTrailingZeros(outNum.getStoreQty()).add(CommonUtil.stripTrailingZeros(out.getStoreQty())));
                outNum.setStoreCostAmount(CommonUtil.stripTrailingZeros(outNum.getStoreCostAmount()).add(CommonUtil.stripTrailingZeros(out.getStoreCostAmount())));
                outNum.setStoreCostRateAmount(CommonUtil.stripTrailingZeros(outNum.getStoreCostRateAmount()).add(CommonUtil.stripTrailingZeros(out.getStoreCostRateAmount())));

                outNum.setDcQty(CommonUtil.stripTrailingZeros(outNum.getDcQty()).add(CommonUtil.stripTrailingZeros(out.getDcQty())));
                outNum.setDcCostAmount(CommonUtil.stripTrailingZeros(outNum.getDcCostAmount()).add(CommonUtil.stripTrailingZeros(out.getDcCostAmount())));
                outNum.setDcCostRateAmount(CommonUtil.stripTrailingZeros(outNum.getDcCostRateAmount()).add(CommonUtil.stripTrailingZeros(out.getDcCostRateAmount())));
            }
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(outNum);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }


    //查询条件：销售等级
    @Override
    public List<MemberSaleClass> findSaleClass(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.findSaleClass(inData);
    }

    //查询条件：商品定位
    @Override
    public List<MemberSalePosition> findSalePosition(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.findSalePosition(inData);
    }

    //查询条件：商品自分类
    @Override
    public List<MemberProsClass> findProsClass(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.findProsClass(inData);
    }

    //查询条件：禁止销售
    @Override
    public List<MemberPurchase> findPurchase(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.findPurchase(inData);
    }

    //查询条件：自定义1
    @Override
    public List<MemberZDY1> findZDY1(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.findZDY1(inData);
    }

    //查询条件：自定义2
    @Override
    public List<MemberZDY2> findZDY2(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.findZDY2(inData);
    }

    //查询条件：自定义3
    @Override
    public List<MemberZDY3> findZDY3(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.findZDY3(inData);
    }

    //查询条件：自定义4
    @Override
    public List<MemberZDY4> findZDY4(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.findZDY4(inData);
    }

    //查询条件：自定义5
    @Override
    public List<MemberZDY5> findZDY5(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.findZDY5(inData);
    }

    //搜索条件
    @Override
    public JsonResult searchBox(InventoryInquiryInData inData){
        Map<String, Object> result = new HashMap<>(16);
        result.put("findSaleClass", findSaleClass(inData));
        result.put("findSalePosition", findSalePosition(inData));
        result.put("findProsClass", findProsClass(inData));
//        result.put("findPurchase",findPurchase(inData));
        result.put("findZDY1", findZDY1(inData));
        result.put("findZDY2", findZDY2(inData));
        result.put("findZDY3", findZDY3(inData));
        result.put("findZDY4", findZDY4(inData));
        result.put("findZDY5", findZDY5(inData));
        return JsonResult.success(result, "success");
    }

    @Override
    public JsonResult zdyBox(InventoryInquiryInData inData) {
        Map<String, Object> result = new HashMap<>(16);
        result.put("findZDY1", findZDY1(inData));
        result.put("findZDY2", findZDY2(inData));
        result.put("findZDY3", findZDY3(inData));
        result.put("findZDY4", findZDY4(inData));
        result.put("findZDY5", findZDY5(inData));
        return JsonResult.success(result, "success");
    }

    @Override
    public JsonResult search(InventoryInquiryInData inData) {
        Map<String, Object> result = new HashMap<>(16);
        result.put("findSaleClass", findSaleClass(inData));
        result.put("findSalePosition", findSalePosition(inData));
        result.put("findProsClass", findProsClass(inData));
//        result.put("findPurchase",findPurchase(inData));
        return JsonResult.success(result, "success");
    }
    //自定义名称匹配
    @Override
    public ZDYNameOutData zdyName(InventoryInquiryInData inData){
        return this.inventoryInquiryMapper.zdyName(inData);
    }

    @Override
    public void inventoryInquiryListByStoreExport(InventoryInquiryInData inData, HttpServletResponse response) throws IOException {
        List<InventoryInquiryByClientAndSiteOutData> list = this.inventoryInquiryListByStoreAndDc(inData).getList();
        if (CollUtil.isEmpty(list)) {
            throw new BusinessException("导出数据不能为空");
        }
        String fileName = null;
        try {
            fileName = URLEncoder.encode("地点实时库存导出", "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        Object export = null;
        if("1".equals(inData.getType())){
            export = new InventoryInquiryByClientAndSiteOutDataByBatch();
        }else{
            export = new InventoryInquiryByClientAndSiteOutData();
        }
        try {
            EasyExcel.write(response.getOutputStream(), export.getClass())
                    .sheet("0")
                    .doWrite(list);
        } catch (Exception e) {
            // 重置response
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            Map<String, String> map = new HashMap<>();
            map.put("status", "failure");
            map.put("message", "下载文件失败" + e.getMessage());
            response.getWriter().println(JSON.toJSONString(map));
        }
    }
}
