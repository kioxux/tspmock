package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "报损报溢请求实体")
public class IncomeStatementInData extends BaseVO{
    @ApiModelProperty(value = "损溢单号")
    private String gsishVoucherId;

    @ApiModelProperty(value = "盘点单号")
    private String gsishPcVoucherId;

    @ApiModelProperty(value = "损溢类型 1：盘点导入 2：批号养护 3：门店领用 4：门店报损 5：不合格品报损 6：门店报溢")
    private String gsishIsType;

    @ApiModelProperty(value = "审核状态：0未审核，1审核未批准，2已审核")
    private String gsishStatus;

    @ApiModelProperty(value = "生成日期")
    private String gsishDate;

    @ApiModelProperty(value = "行号")
    private String gsisdSerial;

    @ApiModelProperty(value = "商品编码")
    private String gsisdProId;

    @ApiModelProperty(value = "批号")
    private String gsisdBatchNo;

    @ApiModelProperty(value = "操作人员")
    private String gsishExamineEmp;

    @ApiModelProperty(value = "完成日期")
    private String gsishFinishDate;

    @ApiModelProperty(value = "备注")
    private String remark;
    private String gsishExamineDate;

    @ApiModelProperty(hidden = true)
    private String gsishTableType;

    @ApiModelProperty(value = "合计金额")
    private String gsishTotalAmt;

    @ApiModelProperty(value = "合计数量")
    private String gsishTotalQty;
    private String gsisdIsQty;

    private String gsishRemark;

    @ApiModelProperty(hidden = true)
    private String userId;

    @ApiModelProperty(hidden = true)
    private String token;

    @ApiModelProperty(value = "批号养护时新增数据List")
    private List<IncomeStatementDetailInData> detailOutDataList;


    private String gsishBrId;
    /*
      附件列表
     */
    private List<String> imgs;

    private  String gsishBranch;

}
