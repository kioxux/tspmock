package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

@Data
public class ProductSyncData {
    private String client;
    private String site;
    private List<String> proIds;
}
