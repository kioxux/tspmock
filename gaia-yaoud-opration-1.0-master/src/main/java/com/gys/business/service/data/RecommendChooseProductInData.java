package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class RecommendChooseProductInData implements Serializable {


    private static final long serialVersionUID = 2000025567208726686L;


    private String content;

    private Integer pageNum;

    private Integer pageSize;
}
