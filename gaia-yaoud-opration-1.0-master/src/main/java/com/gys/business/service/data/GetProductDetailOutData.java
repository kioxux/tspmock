//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetProductDetailOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    private String clientId;
    private String gspgIdDetail;
    private String gspgProId;
    private String gspgSourceProId;
    private String gspgProName;
    private Integer indexDetail;
    private String gspgSpecs;
    private String updateUser;

    public GetProductDetailOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspgIdDetail() {
        return this.gspgIdDetail;
    }

    public String getGspgProId() {
        return this.gspgProId;
    }

    public String getGspgSourceProId() {
        return this.gspgSourceProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public Integer getIndexDetail() {
        return this.indexDetail;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspgIdDetail(final String gspgIdDetail) {
        this.gspgIdDetail = gspgIdDetail;
    }

    public void setGspgProId(final String gspgProId) {
        this.gspgProId = gspgProId;
    }

    public void setGspgSourceProId(final String gspgSourceProId) {
        this.gspgSourceProId = gspgSourceProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setIndexDetail(final Integer indexDetail) {
        this.indexDetail = indexDetail;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setUpdateUser(final String updateUser) {
        this.updateUser = updateUser;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetProductDetailOutData)) {
            return false;
        } else {
            GetProductDetailOutData other = (GetProductDetailOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label107: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label107;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label107;
                    }

                    return false;
                }

                Object this$gspgIdDetail = this.getGspgIdDetail();
                Object other$gspgIdDetail = other.getGspgIdDetail();
                if (this$gspgIdDetail == null) {
                    if (other$gspgIdDetail != null) {
                        return false;
                    }
                } else if (!this$gspgIdDetail.equals(other$gspgIdDetail)) {
                    return false;
                }

                Object this$gspgProId = this.getGspgProId();
                Object other$gspgProId = other.getGspgProId();
                if (this$gspgProId == null) {
                    if (other$gspgProId != null) {
                        return false;
                    }
                } else if (!this$gspgProId.equals(other$gspgProId)) {
                    return false;
                }

                label86: {
                    Object this$gspgSourceProId = this.getGspgSourceProId();
                    Object other$gspgSourceProId = other.getGspgSourceProId();
                    if (this$gspgSourceProId == null) {
                        if (other$gspgSourceProId == null) {
                            break label86;
                        }
                    } else if (this$gspgSourceProId.equals(other$gspgSourceProId)) {
                        break label86;
                    }

                    return false;
                }

                label79: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label79;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label79;
                    }

                    return false;
                }

                label72: {
                    Object this$indexDetail = this.getIndexDetail();
                    Object other$indexDetail = other.getIndexDetail();
                    if (this$indexDetail == null) {
                        if (other$indexDetail == null) {
                            break label72;
                        }
                    } else if (this$indexDetail.equals(other$indexDetail)) {
                        break label72;
                    }

                    return false;
                }

                Object this$gspgSpecs = this.getGspgSpecs();
                Object other$gspgSpecs = other.getGspgSpecs();
                if (this$gspgSpecs == null) {
                    if (other$gspgSpecs != null) {
                        return false;
                    }
                } else if (!this$gspgSpecs.equals(other$gspgSpecs)) {
                    return false;
                }

                Object this$updateUser = this.getUpdateUser();
                Object other$updateUser = other.getUpdateUser();
                if (this$updateUser == null) {
                    if (other$updateUser != null) {
                        return false;
                    }
                } else if (!this$updateUser.equals(other$updateUser)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetProductDetailOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspgIdDetail = this.getGspgIdDetail();
        result = result * 59 + ($gspgIdDetail == null ? 43 : $gspgIdDetail.hashCode());
        Object $gspgProId = this.getGspgProId();
        result = result * 59 + ($gspgProId == null ? 43 : $gspgProId.hashCode());
        Object $gspgSourceProId = this.getGspgSourceProId();
        result = result * 59 + ($gspgSourceProId == null ? 43 : $gspgSourceProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $indexDetail = this.getIndexDetail();
        result = result * 59 + ($indexDetail == null ? 43 : $indexDetail.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $updateUser = this.getUpdateUser();
        result = result * 59 + ($updateUser == null ? 43 : $updateUser.hashCode());
        return result;
    }

    public String toString() {
        return "GetProductDetailOutData(clientId=" + this.getClientId() + ", gspgIdDetail=" + this.getGspgIdDetail() + ", gspgProId=" + this.getGspgProId() + ", gspgSourceProId=" + this.getGspgSourceProId() + ", gspgProName=" + this.getGspgProName() + ", indexDetail=" + this.getIndexDetail() + ", gspgSpecs=" + this.getGspgSpecs() + ", updateUser=" + this.getUpdateUser() + ")";
    }
}
