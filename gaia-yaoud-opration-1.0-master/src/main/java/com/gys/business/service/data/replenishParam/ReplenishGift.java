package com.gys.business.service.data.replenishParam;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReplenishGift {
    private String clientId;
    private String stoCode;
    private String gsrgVoucherId;
    private String gsrgProId;
    private String gsrgDproId;
    private BigDecimal gsrgQty;
}
