package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 库存审计平台-对比结果查询参数类
 */
@Data
public class StockDiffCondition implements Serializable {


    private static final long serialVersionUID = -7059441806166332983L;

    @ApiModelProperty(value = "比对任务号")
    private String taskNo;

    @ApiModelProperty(value = "比对任务行号")
    private String taskItem;

}
