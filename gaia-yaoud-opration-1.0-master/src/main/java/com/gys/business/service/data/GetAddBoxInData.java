//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetAddBoxInData {
    private String gschVoucherId;
    private String gsihVoucherId;
    private String clientId;
    private String brId;

    public GetAddBoxInData() {
    }

    public String getGschVoucherId() {
        return this.gschVoucherId;
    }

    public String getGsihVoucherId() {
        return this.gsihVoucherId;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public void setGschVoucherId(final String gschVoucherId) {
        this.gschVoucherId = gschVoucherId;
    }

    public void setGsihVoucherId(final String gsihVoucherId) {
        this.gsihVoucherId = gsihVoucherId;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetAddBoxInData)) {
            return false;
        } else {
            GetAddBoxInData other = (GetAddBoxInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$gschVoucherId = this.getGschVoucherId();
                    Object other$gschVoucherId = other.getGschVoucherId();
                    if (this$gschVoucherId == null) {
                        if (other$gschVoucherId == null) {
                            break label59;
                        }
                    } else if (this$gschVoucherId.equals(other$gschVoucherId)) {
                        break label59;
                    }

                    return false;
                }

                Object this$gsihVoucherId = this.getGsihVoucherId();
                Object other$gsihVoucherId = other.getGsihVoucherId();
                if (this$gsihVoucherId == null) {
                    if (other$gsihVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsihVoucherId.equals(other$gsihVoucherId)) {
                    return false;
                }

                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$brId = this.getBrId();
                Object other$brId = other.getBrId();
                if (this$brId == null) {
                    if (other$brId != null) {
                        return false;
                    }
                } else if (!this$brId.equals(other$brId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetAddBoxInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gschVoucherId = this.getGschVoucherId();
        result = result * 59 + ($gschVoucherId == null ? 43 : $gschVoucherId.hashCode());
        Object $gsihVoucherId = this.getGsihVoucherId();
        result = result * 59 + ($gsihVoucherId == null ? 43 : $gsihVoucherId.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        return result;
    }

    public String toString() {
        return "GetAddBoxInData(gschVoucherId=" + this.getGschVoucherId() + ", gsihVoucherId=" + this.getGsihVoucherId() + ", clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ")";
    }
}
