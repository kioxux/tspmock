package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ValidProductInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "门店编码")
    private String brId;

    @ApiModelProperty(value = "排序字段 1-效期数量 2-效期成本额")
    private Integer orderByFlag;

    @ApiModelProperty(value = "排序字段 1-前(从大到小) 2-后(从小到大)")
    private Integer topOrLast;

    @ApiModelProperty(value = "显示条数")
    private Integer pageSize;

    @ApiModelProperty(value = "商品成分大类 A-药品 B-中药 C-保健食品 D-医疗器械及护理 E-个人护理 F-家庭用品 G-赠品 N-其他")
    private String type;

    @ApiModelProperty(value = "配置天数")
    private String configDays;
}
