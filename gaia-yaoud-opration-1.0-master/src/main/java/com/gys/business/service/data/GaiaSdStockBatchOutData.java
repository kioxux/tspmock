package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 查询映射实体
 *
 * @author xiaoyuan on 2020/9/8
 */
@Data
public class GaiaSdStockBatchOutData implements Serializable {
    private static final long serialVersionUID = -2015570061669009917L;
    /**
     * 加盟号
     */
    private String clientId;

    /**
     * 店号
     */
    private String gssbBrId;

    /**
     * 商品编码
     */
    private String gssbProId;

    /**
     * 批号
     */
    private String gssbBatchNo;

    /**
     * 批次
     */
    private String gssbBatch;

    /**
     * 数量
     */
    private String gssbQty;

    /**
     * 有效期
     */
    private String gssbVaildDate;

    /**
     * 更新日期
     */
    private String gssbUpdateDate;

    /**
     * 更新人员编码
     */
    private String gssbUpdateEmp;

    /**
     * 销售单号
     */
    private String billNo;
}
