package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/10 16:02
 **/
@Data
public class RequestOrderVO {
    @ApiModelProperty(value = "序号", example = "1")
    private Integer index;
    @ApiModelProperty(value = "门店ID", example = "10001")
    private String storeId;
    @ApiModelProperty(value = "门店名称", example = "翠竹店")
    private String storeName;
    @ApiModelProperty(value = "补货单号", example = "PD20210098")
    private String replenishCode;
    @ApiModelProperty(value = "补货类型：0-正常请货、1-紧急请货、2-铺货", example = "0")
    private String replenishType;
    @ApiModelProperty(value = "要货日期", example = "2021-07-01")
    private String requestDate;
    @ApiModelProperty(value = "需求行数", example = "4")
    private String requestNum;
    @ApiModelProperty(value = "需求总数量", example = "12")
    private String requestQuantity;
    @ApiModelProperty(value = "需求总金额", example = "989")
    private String totalAmount;
    @ApiModelProperty(value = "审核状态：0-未审核、1-审核、2-已拒绝", example = "0")
    private String status;
}
