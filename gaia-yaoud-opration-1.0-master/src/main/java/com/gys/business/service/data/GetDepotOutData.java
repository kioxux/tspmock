//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

@ApiModel(value = "退库信息")
public class GetDepotOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "退库单号")
    private String gsrdhVoucherId;
    @ApiModelProperty(value = "退库日期")
    private String gsrdhDate;
    @ApiModelProperty(value = "完成日期")
    private String gsrdhFinishDate;
    @ApiModelProperty(value = "发货地点")
    private String gsrdhFrom;
    @ApiModelProperty(value = "收货地点")
    private String gsrdhTo;
    @ApiModelProperty(value = "单据类型")
    private String gsrdhType;
    @ApiModelProperty(value = "退库状态")
    private String gsrdhStatus;
    @ApiModelProperty(value = "退库金额")
    private BigDecimal gsrdhTotalAmt;
    @ApiModelProperty(value = "退库数量")
    private String gsrdhTotalQty;
    @ApiModelProperty(value = "退库人员")
    private String gsrdhEmp;
    @ApiModelProperty(value = "备注")
    private String gsrdhRemaks;
    @ApiModelProperty(value = "退库步骤")
    private String gsrdhProcedure;
    @ApiModelProperty(value = "召回单号")
    private String gsrdhRecallVoucherId;
    private List<GetDepotDetailOutData> depotDetailOutDataList;
    @ApiModelProperty(value = "商品编码")
    private String gsadProId;

    public GetDepotOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsrdhVoucherId() {
        return this.gsrdhVoucherId;
    }

    public String getGsrdhDate() {
        return this.gsrdhDate;
    }

    public String getGsrdhFinishDate() {
        return this.gsrdhFinishDate;
    }

    public String getGsrdhFrom() {
        return this.gsrdhFrom;
    }

    public String getGsrdhTo() {
        return this.gsrdhTo;
    }

    public String getGsrdhType() {
        return this.gsrdhType;
    }

    public String getGsrdhStatus() {
        return this.gsrdhStatus;
    }

    public BigDecimal getGsrdhTotalAmt() {
        return this.gsrdhTotalAmt;
    }

    public String getGsrdhTotalQty() {
        return this.gsrdhTotalQty;
    }

    public String getGsrdhEmp() {
        return this.gsrdhEmp;
    }

    public String getGsrdhRemaks() {
        return this.gsrdhRemaks;
    }

    public String getGsrdhProcedure() {
        return this.gsrdhProcedure;
    }

    public String getGsrdhRecallVoucherId() {
        return this.gsrdhRecallVoucherId;
    }

    public List<GetDepotDetailOutData> getDepotDetailOutDataList() {
        return depotDetailOutDataList;
    }

    public String getGsadProId() {
        return this.gsadProId;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsrdhVoucherId(final String gsrdhVoucherId) {
        this.gsrdhVoucherId = gsrdhVoucherId;
    }

    public void setGsrdhDate(final String gsrdhDate) {
        this.gsrdhDate = gsrdhDate;
    }

    public void setGsrdhFinishDate(final String gsrdhFinishDate) {
        this.gsrdhFinishDate = gsrdhFinishDate;
    }

    public void setGsrdhFrom(final String gsrdhFrom) {
        this.gsrdhFrom = gsrdhFrom;
    }

    public void setGsrdhTo(final String gsrdhTo) {
        this.gsrdhTo = gsrdhTo;
    }

    public void setGsrdhType(final String gsrdhType) {
        this.gsrdhType = gsrdhType;
    }

    public void setGsrdhStatus(final String gsrdhStatus) {
        this.gsrdhStatus = gsrdhStatus;
    }

    public void setGsrdhTotalAmt(final BigDecimal gsrdhTotalAmt) {
        this.gsrdhTotalAmt = gsrdhTotalAmt;
    }

    public void setGsrdhTotalQty(final String gsrdhTotalQty) {
        this.gsrdhTotalQty = gsrdhTotalQty;
    }

    public void setGsrdhEmp(final String gsrdhEmp) {
        this.gsrdhEmp = gsrdhEmp;
    }

    public void setGsrdhRemaks(final String gsrdhRemaks) {
        this.gsrdhRemaks = gsrdhRemaks;
    }

    public void setGsrdhProcedure(final String gsrdhProcedure) {
        this.gsrdhProcedure = gsrdhProcedure;
    }

    public void setGsrdhRecallVoucherId(final String gsrdhRecallVoucherId) {
        this.gsrdhRecallVoucherId = gsrdhRecallVoucherId;
    }

    public void setDepotDetailOutDataList(List<GetDepotDetailOutData> depotDetailOutDataList) {
        this.depotDetailOutDataList = depotDetailOutDataList;
    }

    public void setGsadProId(final String gsadProId) {
        this.gsadProId = gsadProId;
    }


}
