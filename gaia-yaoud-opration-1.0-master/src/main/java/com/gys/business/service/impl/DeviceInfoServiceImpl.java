//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaSdDeviceInfoMapper;
import com.gys.business.mapper.entity.GaiaSdDeviceInfo;
import com.gys.business.service.DeviceInfoService;
import com.gys.business.service.data.DeviceInfoInData;
import com.gys.business.service.data.DeviceInfoOutData;
import com.gys.business.service.data.device.DeviceForm;
import com.gys.business.service.data.device.UserVO;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class DeviceInfoServiceImpl implements DeviceInfoService {
    @Autowired
    private GaiaSdDeviceInfoMapper deviceInfoMapper;

    public DeviceInfoServiceImpl() {
    }

    public PageInfo<DeviceInfoOutData> getDeviceInfoList(DeviceInfoInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        new ArrayList();
        List<DeviceInfoOutData> deviceInfoList = this.deviceInfoMapper.getDeviceInfoList(inData);
        PageInfo pageInfo;
        if (CollectionUtils.isEmpty(deviceInfoList)) {
            pageInfo = new PageInfo(new ArrayList());
        } else {
            for(int i = 0; i < deviceInfoList.size(); ++i) {
                ((DeviceInfoOutData)deviceInfoList.get(i)).setIndex(i + 1);
            }

            pageInfo = new PageInfo(deviceInfoList);
        }

        return pageInfo;
    }

    public void insertDeviceInfo(DeviceInfoInData inData) {
        GaiaSdDeviceInfo deviceInfo = new GaiaSdDeviceInfo();
        inData.setGsdiId(this.deviceInfoMapper.selectNextGsdiId());
        BeanUtils.copyProperties(inData, deviceInfo);
        this.deviceInfoMapper.insert(deviceInfo);
    }

    public void editDeviceInfo(DeviceInfoInData inData) {
        GaiaSdDeviceInfo deviceInfo = new GaiaSdDeviceInfo();
        BeanUtils.copyProperties(inData, deviceInfo);
        this.deviceInfoMapper.updateByPrimaryKey(deviceInfo);
    }

    public DeviceInfoOutData getDeviceInfoById(DeviceInfoInData inData) {
        GaiaSdDeviceInfo deviceInfo = (GaiaSdDeviceInfo)this.deviceInfoMapper.selectByPrimaryKey(inData);
        DeviceInfoOutData outData = new DeviceInfoOutData();
        if (ObjectUtil.isNotEmpty(deviceInfo)) {
            outData.setClientId(deviceInfo.getClientId());
            outData.setGsdiCheck(deviceInfo.getGsdiCheck());
            outData.setGsdiFactory(deviceInfo.getGsdiFactory());
            outData.setGsdiId(deviceInfo.getGsdiId());
            outData.setGsdiInstruction(deviceInfo.getGsdiInstruction());
            outData.setGsdiModel(deviceInfo.getGsdiModel());
            outData.setGsdiName(deviceInfo.getGsdiName());
            outData.setGsdiQty(deviceInfo.getGsdiQty());
            outData.setGsdiQualify(deviceInfo.getGsdiQualify());
            outData.setGsdiStartDate(deviceInfo.getGsdiStartDate());
            outData.setGsdiUpdateDat(deviceInfo.getGsdiUpdateDat());
            outData.setGsthBrId(deviceInfo.getGsthBrId());
            outData.setGsthBrName(deviceInfo.getGsthBrName());
            outData.setGsthUpdateEmp(deviceInfo.getGsthUpdateEmp());
        }

        return outData;
    }

    public List<DeviceInfoOutData> selectBrDeviceList(DeviceInfoInData inData) {
        Example example = new Example(GaiaSdDeviceInfo.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsthBrId", inData.getGsthBrId());
        List<GaiaSdDeviceInfo> deviceInfoList = this.deviceInfoMapper.selectByExample(example);
        List<DeviceInfoOutData> deviceList = new ArrayList();
        Iterator var5 = deviceInfoList.iterator();

        while(var5.hasNext()) {
            GaiaSdDeviceInfo deviceInfo = (GaiaSdDeviceInfo)var5.next();
            DeviceInfoOutData device = new DeviceInfoOutData();
            device.setGsdiId(deviceInfo.getGsdiId());
            device.setGsdiName(deviceInfo.getGsdiName());
            device.setGsdiModel(deviceInfo.getGsdiModel());
            deviceList.add(device);
        }

        return deviceList;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(GaiaSdDeviceInfo deviceInfo) {
        int num = deviceInfoMapper.insert(deviceInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(GaiaSdDeviceInfo deviceInfo) {
        deviceInfoMapper.update(deviceInfo);
    }

    @Override
    public void save(List<DeviceInfoInData> list, GetLoginOutData loginUser) {
        for (DeviceInfoInData addDeviceForm : list) {
            if (StringUtils.isEmpty(addDeviceForm.getGsdiId())) {
                GaiaSdDeviceInfo deviceInfo = transformDeviceInfo(null, addDeviceForm, loginUser);
                add(deviceInfo);
            } else {
                GaiaSdDeviceInfo cond = new GaiaSdDeviceInfo();
                cond.setClientId(loginUser.getClient());
                cond.setGsthBrId(loginUser.getDepId());
                cond.setGsdiId(addDeviceForm.getGsdiId());
                GaiaSdDeviceInfo deviceInfo = deviceInfoMapper.selectOne(cond);
                if (deviceInfo == null) {
                    continue;
                }
                transformDeviceInfo(deviceInfo, addDeviceForm, loginUser);
                update(deviceInfo);
            }
        }
    }

    private GaiaSdDeviceInfo transformDeviceInfo(GaiaSdDeviceInfo deviceInfo, DeviceInfoInData addDeviceForm, GetLoginOutData loginUser) {
        if (StringUtils.isEmpty(addDeviceForm.getGsdiName())) {
            throw new BusinessException("设备名称不能为空");
        }
        if (deviceInfo == null) {
            deviceInfo = new GaiaSdDeviceInfo();
            deviceInfo.setGsdiId(deviceInfoMapper.selectNextGsdiId());
            deviceInfo.setClientId(loginUser.getClient());
            deviceInfo.setGsthBrId(loginUser.getDepId());
            deviceInfo.setGsthBrName(loginUser.getDepName());
        }
        String startDate = addDeviceForm.getGsdiStartDate();
        startDate=startDate.replace("-","");
        deviceInfo.setGsdiStartDate(startDate);
        deviceInfo.setGsdiFactory(addDeviceForm.getGsdiFactory());
        deviceInfo.setGsdiModel(addDeviceForm.getGsdiModel());
        deviceInfo.setGsdiInstruction(addDeviceForm.getGsdiInstruction() == null ? "0" : addDeviceForm.getGsdiInstruction());
        deviceInfo.setGsdiCheck(addDeviceForm.getGsdiCheck() == null ? "0" : addDeviceForm.getGsdiCheck());
        deviceInfo.setGsdiQualify(addDeviceForm.getGsdiQualify() == null ? "0" : addDeviceForm.getGsdiQualify());
        deviceInfo.setGsdiName(addDeviceForm.getGsdiName());
        deviceInfo.setGsdiQty(addDeviceForm.getGsdiQty());
        deviceInfo.setGsthUpdateEmp(loginUser.getLoginName() + "-" + loginUser.getUserId());
        deviceInfo.setGsdiUpdateDat(CommonUtil.getyyyyMMdd());
        return deviceInfo;
    }

    @Override
    public List<DeviceInfoOutData> findList(DeviceForm deviceForm) {
        return deviceInfoMapper.findList(deviceForm);
    }

    @Override
    public List<UserVO> getUserNameList(DeviceForm deviceForm) {
        return deviceInfoMapper.getUserNameList(deviceForm);
    }


}
