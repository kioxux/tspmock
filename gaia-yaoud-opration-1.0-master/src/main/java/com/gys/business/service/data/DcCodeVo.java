package com.gys.business.service.data;

import lombok.Data;

/**
 * @Auther: tzh
 * @Date: 2022/2/11 16:52
 * @Description: DcCodeVo
 * @Version 1.0.0
 */
@Data
public class DcCodeVo {
    /**
     * 最终code
     */
    private String finalCode;
    /**
     * 加盟商
     */
    private String client;
    /**
     *门店编码
     */
    private  String stoCode;
    /**
     * 配送中心
     */
    private  String stoDcCode;
    /**
     * 委托配送中心
     */
    private  String dcWtdc;
    /**
     * 主数据地点
     */
    private  String stoMdSite;
}
