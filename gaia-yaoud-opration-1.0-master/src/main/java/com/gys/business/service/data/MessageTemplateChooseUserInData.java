package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MessageTemplateChooseUserInData implements Serializable {
    private static final long serialVersionUID = 8701994257173124038L;

    private List<Item> chooseItems;

    @ApiModelProperty(value = "门店人员是否显示毛利数据 1表示打勾 0表示未打勾")
    private String gmtIfShowMao;

    private String gmtId;

    @Data
    public
    class Item{

        List<CommonVo> sites;

        private String positionName;

        private String positionId;

        private String receiveUserId;

        private String model;
    }
}
