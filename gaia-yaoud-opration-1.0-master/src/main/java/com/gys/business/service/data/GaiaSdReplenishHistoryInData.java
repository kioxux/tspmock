package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value="com-gys-business-service-data-GaiaSdReplenishHistory")
public class GaiaSdReplenishHistoryInData implements Serializable {
    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 门店
    */
    @ApiModelProperty(value="门店")
    private String gsrhhBrId;

    /**
    * 商品编号
    */
    @ApiModelProperty(value="商品编号")
    private String gsrhhProId;

    /**
    * 日期
    */
    @ApiModelProperty(value="日期")
    private String gsrhhDate;

    /**
    * 月均销量
    */
    @ApiModelProperty(value="月均销量")
    private BigDecimal gsrhhAvgQty;

    /**
    * 月均销售额
    */
    @ApiModelProperty(value="月均销售额")
    private BigDecimal gsrhhAvgAmt;

    /**
    * 月均毛利额
    */
    @ApiModelProperty(value="月均毛利额")
    private BigDecimal gsrhhAvgProfitAmt;

    private static final long serialVersionUID = 1L;

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getGsrhhBrId() {
        return gsrhhBrId;
    }

    public void setGsrhhBrId(String gsrhhBrId) {
        this.gsrhhBrId = gsrhhBrId;
    }

    public String getGsrhhProId() {
        return gsrhhProId;
    }

    public void setGsrhhProId(String gsrhhProId) {
        this.gsrhhProId = gsrhhProId;
    }

    public String getGsrhhDate() {
        return gsrhhDate;
    }

    public void setGsrhhDate(String gsrhhDate) {
        this.gsrhhDate = gsrhhDate;
    }

    public BigDecimal getGsrhhAvgQty() {
        return gsrhhAvgQty;
    }

    public void setGsrhhAvgQty(BigDecimal gsrhhAvgQty) {
        this.gsrhhAvgQty = gsrhhAvgQty;
    }

    public BigDecimal getGsrhhAvgAmt() {
        return gsrhhAvgAmt;
    }

    public void setGsrhhAvgAmt(BigDecimal gsrhhAvgAmt) {
        this.gsrhhAvgAmt = gsrhhAvgAmt;
    }

    public BigDecimal getGsrhhAvgProfitAmt() {
        return gsrhhAvgProfitAmt;
    }

    public void setGsrhhAvgProfitAmt(BigDecimal gsrhhAvgProfitAmt) {
        this.gsrhhAvgProfitAmt = gsrhhAvgProfitAmt;
    }
}