package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 会员积分明细查询传参
 *
 * @author xiaoyuan on 2020/7/28
 */
@Data
public class PointsDetailsQueryInData implements Serializable {
    private static final long serialVersionUID = 6626126976760329975L;

    /**
     * 加盟号
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 店地址
     */
    @ApiModelProperty(value = "店号")
    private String brId;

    /**
     * 会员卡号
     */
    @ApiModelProperty(value = "会员卡号")
    private String gsmbCardId;

    /**
     * 会员名称
     */
    @ApiModelProperty(value = "会员名称")
    private String gsmbName;

    @ApiModelProperty(value = "会员卡号")
    private String membersId;

    @ApiModelProperty(value = "会员名称")
    private String membersName;

    /**
     * 会员手机
     */
    private String memberPhone;

    /**
     * 类型 1增加积分 2积分抵现 3积分兑换 4积分升级 5手工修改
     */
    private String type;

    /**
     * 起始时间
     */
    @ApiModelProperty(value = "起始时间")
    private String startDate;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private String endDate;

    /**
     * 消费店号
     */
    private List<String> saleStoCode;

    private int pageNum = 1;

    private int pageSize=100;
}
