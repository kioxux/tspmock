
package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class GetMemberOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    private String clientId;
    private String gmbCardId;
    private String gmbName;
    private String gmbAddress;
    private String gmbMobile;
    private String gmbTel;
    private String gmbSex;
    private String gmbCredentials;
    private String gmbAge;
    private String gmbBirth;
    private String gmbBbName;
    private String gmbBbSex;
    private String gmbBbAge;
    private String gmbChronicDiseaseType;
    private String gmbContactAllowed;
    private String gmbChannel;
    private String gmbClassId;
    private String gmbIntegral;
    private String gmbIntegralLastdate;
    private String gmbZeroDate;
    private String gmbBrId;
    private String gmbBrName;
    private String gmbStatus;
    private String gmbUpdateBrId;
    private String gmbUpdateSaler;
    private String gmbUpdateDate;
    private String gsmbRemark;
    private Integer index;
    private String gsmcId;
    private String currCardAmt;
}
