//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class PromoteOutData {
    private String clientId;
    private String gsphBrId;
    private String gsphVoucherId;
    private String gsphTheme;
    private String gsphName;
    private String gsphRemarks;
    private String gsphStatus;
    private String gsphBeginDate;
    private String gsphEndDate;
    private String gsphDateFrequency;
    private String gsphWeekFrequency;
    private String gsphBeginTime;
    private String gsphEndTime;
    private String gsphType;
    private String gsphPart;
    private String gsphPara1;
    private String gsphPara2;
    private String gsphPara3;
    private String gsphPara4;
    private String gsphExclusion;
    private String type;
    private String storeStart;
    private String storeEnd;
    private String proCode;
    private String storeGroup;
    private List<ParamOutData> paramOneList;
    private List<ParamOutData> paramTwoList;
    private List<ParamOutData> paramThreeList;
    private List<ParamOutData> paramFourList;
    private List<ParamOutData> exclusionList;
    private List<String> storeIds;
    private List<String> storeList;
    private List<PromUnitarySetOutData> promUnitarySetInDataList;
    private List<PromSeriesCondsOutData> promSeriesCondsInDataList;
    private List<PromSeriesSetOutData> promSeriesSetInDataList;
    private List<PromGiftCondsOutData> promGiftCondsInDataList;
    private List<PromGiftSetOutData> promGiftSetInDataList;
    private List<PromGiftResultOutData> promGiftResultInDataList;
    private List<PromCouponSetOutData> promCouponSetInDataList;
    private List<PromCouponGrantOutData> promCouponGrantInDataList;
    private List<PromCouponUseOutData> promCouponUseInDataList;
    private List<PromHySetOutData> promHySetInDataList;
    private List<PromHyrDiscountOutData> promHyrDiscountInDataList;
    private List<PromHyrPriceOutData> promHyrPriceInDataList;
    private List<PromAssoCondsOutData> promAssoCondsInDataList;
    private List<PromAssoSetOutData> promAssoSetInDataList;
    private List<PromAssoResultOutData> promAssoResultInDataList;

    public PromoteOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsphBrId() {
        return this.gsphBrId;
    }

    public String getGsphVoucherId() {
        return this.gsphVoucherId;
    }

    public String getGsphTheme() {
        return this.gsphTheme;
    }

    public String getGsphName() {
        return this.gsphName;
    }

    public String getGsphRemarks() {
        return this.gsphRemarks;
    }

    public String getGsphStatus() {
        return this.gsphStatus;
    }

    public String getGsphBeginDate() {
        return this.gsphBeginDate;
    }

    public String getGsphEndDate() {
        return this.gsphEndDate;
    }

    public String getGsphDateFrequency() {
        return this.gsphDateFrequency;
    }

    public String getGsphWeekFrequency() {
        return this.gsphWeekFrequency;
    }

    public String getGsphBeginTime() {
        return this.gsphBeginTime;
    }

    public String getGsphEndTime() {
        return this.gsphEndTime;
    }

    public String getGsphType() {
        return this.gsphType;
    }

    public String getGsphPart() {
        return this.gsphPart;
    }

    public String getGsphPara1() {
        return this.gsphPara1;
    }

    public String getGsphPara2() {
        return this.gsphPara2;
    }

    public String getGsphPara3() {
        return this.gsphPara3;
    }

    public String getGsphPara4() {
        return this.gsphPara4;
    }

    public String getGsphExclusion() {
        return this.gsphExclusion;
    }

    public String getType() {
        return this.type;
    }

    public String getStoreStart() {
        return this.storeStart;
    }

    public String getStoreEnd() {
        return this.storeEnd;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getStoreGroup() {
        return this.storeGroup;
    }

    public List<ParamOutData> getParamOneList() {
        return this.paramOneList;
    }

    public List<ParamOutData> getParamTwoList() {
        return this.paramTwoList;
    }

    public List<ParamOutData> getParamThreeList() {
        return this.paramThreeList;
    }

    public List<ParamOutData> getParamFourList() {
        return this.paramFourList;
    }

    public List<ParamOutData> getExclusionList() {
        return this.exclusionList;
    }

    public List<String> getStoreIds() {
        return this.storeIds;
    }

    public List<String> getStoreList() {
        return this.storeList;
    }

    public List<PromUnitarySetOutData> getPromUnitarySetInDataList() {
        return this.promUnitarySetInDataList;
    }

    public List<PromSeriesCondsOutData> getPromSeriesCondsInDataList() {
        return this.promSeriesCondsInDataList;
    }

    public List<PromSeriesSetOutData> getPromSeriesSetInDataList() {
        return this.promSeriesSetInDataList;
    }

    public List<PromGiftCondsOutData> getPromGiftCondsInDataList() {
        return this.promGiftCondsInDataList;
    }

    public List<PromGiftSetOutData> getPromGiftSetInDataList() {
        return this.promGiftSetInDataList;
    }

    public List<PromGiftResultOutData> getPromGiftResultInDataList() {
        return this.promGiftResultInDataList;
    }

    public List<PromCouponSetOutData> getPromCouponSetInDataList() {
        return this.promCouponSetInDataList;
    }

    public List<PromCouponGrantOutData> getPromCouponGrantInDataList() {
        return this.promCouponGrantInDataList;
    }

    public List<PromCouponUseOutData> getPromCouponUseInDataList() {
        return this.promCouponUseInDataList;
    }

    public List<PromHySetOutData> getPromHySetInDataList() {
        return this.promHySetInDataList;
    }

    public List<PromHyrDiscountOutData> getPromHyrDiscountInDataList() {
        return this.promHyrDiscountInDataList;
    }

    public List<PromHyrPriceOutData> getPromHyrPriceInDataList() {
        return this.promHyrPriceInDataList;
    }

    public List<PromAssoCondsOutData> getPromAssoCondsInDataList() {
        return this.promAssoCondsInDataList;
    }

    public List<PromAssoSetOutData> getPromAssoSetInDataList() {
        return this.promAssoSetInDataList;
    }

    public List<PromAssoResultOutData> getPromAssoResultInDataList() {
        return this.promAssoResultInDataList;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsphBrId(final String gsphBrId) {
        this.gsphBrId = gsphBrId;
    }

    public void setGsphVoucherId(final String gsphVoucherId) {
        this.gsphVoucherId = gsphVoucherId;
    }

    public void setGsphTheme(final String gsphTheme) {
        this.gsphTheme = gsphTheme;
    }

    public void setGsphName(final String gsphName) {
        this.gsphName = gsphName;
    }

    public void setGsphRemarks(final String gsphRemarks) {
        this.gsphRemarks = gsphRemarks;
    }

    public void setGsphStatus(final String gsphStatus) {
        this.gsphStatus = gsphStatus;
    }

    public void setGsphBeginDate(final String gsphBeginDate) {
        this.gsphBeginDate = gsphBeginDate;
    }

    public void setGsphEndDate(final String gsphEndDate) {
        this.gsphEndDate = gsphEndDate;
    }

    public void setGsphDateFrequency(final String gsphDateFrequency) {
        this.gsphDateFrequency = gsphDateFrequency;
    }

    public void setGsphWeekFrequency(final String gsphWeekFrequency) {
        this.gsphWeekFrequency = gsphWeekFrequency;
    }

    public void setGsphBeginTime(final String gsphBeginTime) {
        this.gsphBeginTime = gsphBeginTime;
    }

    public void setGsphEndTime(final String gsphEndTime) {
        this.gsphEndTime = gsphEndTime;
    }

    public void setGsphType(final String gsphType) {
        this.gsphType = gsphType;
    }

    public void setGsphPart(final String gsphPart) {
        this.gsphPart = gsphPart;
    }

    public void setGsphPara1(final String gsphPara1) {
        this.gsphPara1 = gsphPara1;
    }

    public void setGsphPara2(final String gsphPara2) {
        this.gsphPara2 = gsphPara2;
    }

    public void setGsphPara3(final String gsphPara3) {
        this.gsphPara3 = gsphPara3;
    }

    public void setGsphPara4(final String gsphPara4) {
        this.gsphPara4 = gsphPara4;
    }

    public void setGsphExclusion(final String gsphExclusion) {
        this.gsphExclusion = gsphExclusion;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public void setStoreStart(final String storeStart) {
        this.storeStart = storeStart;
    }

    public void setStoreEnd(final String storeEnd) {
        this.storeEnd = storeEnd;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setStoreGroup(final String storeGroup) {
        this.storeGroup = storeGroup;
    }

    public void setParamOneList(final List<ParamOutData> paramOneList) {
        this.paramOneList = paramOneList;
    }

    public void setParamTwoList(final List<ParamOutData> paramTwoList) {
        this.paramTwoList = paramTwoList;
    }

    public void setParamThreeList(final List<ParamOutData> paramThreeList) {
        this.paramThreeList = paramThreeList;
    }

    public void setParamFourList(final List<ParamOutData> paramFourList) {
        this.paramFourList = paramFourList;
    }

    public void setExclusionList(final List<ParamOutData> exclusionList) {
        this.exclusionList = exclusionList;
    }

    public void setStoreIds(final List<String> storeIds) {
        this.storeIds = storeIds;
    }

    public void setStoreList(final List<String> storeList) {
        this.storeList = storeList;
    }

    public void setPromUnitarySetInDataList(final List<PromUnitarySetOutData> promUnitarySetInDataList) {
        this.promUnitarySetInDataList = promUnitarySetInDataList;
    }

    public void setPromSeriesCondsInDataList(final List<PromSeriesCondsOutData> promSeriesCondsInDataList) {
        this.promSeriesCondsInDataList = promSeriesCondsInDataList;
    }

    public void setPromSeriesSetInDataList(final List<PromSeriesSetOutData> promSeriesSetInDataList) {
        this.promSeriesSetInDataList = promSeriesSetInDataList;
    }

    public void setPromGiftCondsInDataList(final List<PromGiftCondsOutData> promGiftCondsInDataList) {
        this.promGiftCondsInDataList = promGiftCondsInDataList;
    }

    public void setPromGiftSetInDataList(final List<PromGiftSetOutData> promGiftSetInDataList) {
        this.promGiftSetInDataList = promGiftSetInDataList;
    }

    public void setPromGiftResultInDataList(final List<PromGiftResultOutData> promGiftResultInDataList) {
        this.promGiftResultInDataList = promGiftResultInDataList;
    }

    public void setPromCouponSetInDataList(final List<PromCouponSetOutData> promCouponSetInDataList) {
        this.promCouponSetInDataList = promCouponSetInDataList;
    }

    public void setPromCouponGrantInDataList(final List<PromCouponGrantOutData> promCouponGrantInDataList) {
        this.promCouponGrantInDataList = promCouponGrantInDataList;
    }

    public void setPromCouponUseInDataList(final List<PromCouponUseOutData> promCouponUseInDataList) {
        this.promCouponUseInDataList = promCouponUseInDataList;
    }

    public void setPromHySetInDataList(final List<PromHySetOutData> promHySetInDataList) {
        this.promHySetInDataList = promHySetInDataList;
    }

    public void setPromHyrDiscountInDataList(final List<PromHyrDiscountOutData> promHyrDiscountInDataList) {
        this.promHyrDiscountInDataList = promHyrDiscountInDataList;
    }

    public void setPromHyrPriceInDataList(final List<PromHyrPriceOutData> promHyrPriceInDataList) {
        this.promHyrPriceInDataList = promHyrPriceInDataList;
    }

    public void setPromAssoCondsInDataList(final List<PromAssoCondsOutData> promAssoCondsInDataList) {
        this.promAssoCondsInDataList = promAssoCondsInDataList;
    }

    public void setPromAssoSetInDataList(final List<PromAssoSetOutData> promAssoSetInDataList) {
        this.promAssoSetInDataList = promAssoSetInDataList;
    }

    public void setPromAssoResultInDataList(final List<PromAssoResultOutData> promAssoResultInDataList) {
        this.promAssoResultInDataList = promAssoResultInDataList;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromoteOutData)) {
            return false;
        } else {
            PromoteOutData other = (PromoteOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label575: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label575;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label575;
                    }

                    return false;
                }

                Object this$gsphBrId = this.getGsphBrId();
                Object other$gsphBrId = other.getGsphBrId();
                if (this$gsphBrId == null) {
                    if (other$gsphBrId != null) {
                        return false;
                    }
                } else if (!this$gsphBrId.equals(other$gsphBrId)) {
                    return false;
                }

                Object this$gsphVoucherId = this.getGsphVoucherId();
                Object other$gsphVoucherId = other.getGsphVoucherId();
                if (this$gsphVoucherId == null) {
                    if (other$gsphVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsphVoucherId.equals(other$gsphVoucherId)) {
                    return false;
                }

                label554: {
                    Object this$gsphTheme = this.getGsphTheme();
                    Object other$gsphTheme = other.getGsphTheme();
                    if (this$gsphTheme == null) {
                        if (other$gsphTheme == null) {
                            break label554;
                        }
                    } else if (this$gsphTheme.equals(other$gsphTheme)) {
                        break label554;
                    }

                    return false;
                }

                label547: {
                    Object this$gsphName = this.getGsphName();
                    Object other$gsphName = other.getGsphName();
                    if (this$gsphName == null) {
                        if (other$gsphName == null) {
                            break label547;
                        }
                    } else if (this$gsphName.equals(other$gsphName)) {
                        break label547;
                    }

                    return false;
                }

                Object this$gsphRemarks = this.getGsphRemarks();
                Object other$gsphRemarks = other.getGsphRemarks();
                if (this$gsphRemarks == null) {
                    if (other$gsphRemarks != null) {
                        return false;
                    }
                } else if (!this$gsphRemarks.equals(other$gsphRemarks)) {
                    return false;
                }

                Object this$gsphStatus = this.getGsphStatus();
                Object other$gsphStatus = other.getGsphStatus();
                if (this$gsphStatus == null) {
                    if (other$gsphStatus != null) {
                        return false;
                    }
                } else if (!this$gsphStatus.equals(other$gsphStatus)) {
                    return false;
                }

                label526: {
                    Object this$gsphBeginDate = this.getGsphBeginDate();
                    Object other$gsphBeginDate = other.getGsphBeginDate();
                    if (this$gsphBeginDate == null) {
                        if (other$gsphBeginDate == null) {
                            break label526;
                        }
                    } else if (this$gsphBeginDate.equals(other$gsphBeginDate)) {
                        break label526;
                    }

                    return false;
                }

                label519: {
                    Object this$gsphEndDate = this.getGsphEndDate();
                    Object other$gsphEndDate = other.getGsphEndDate();
                    if (this$gsphEndDate == null) {
                        if (other$gsphEndDate == null) {
                            break label519;
                        }
                    } else if (this$gsphEndDate.equals(other$gsphEndDate)) {
                        break label519;
                    }

                    return false;
                }

                Object this$gsphDateFrequency = this.getGsphDateFrequency();
                Object other$gsphDateFrequency = other.getGsphDateFrequency();
                if (this$gsphDateFrequency == null) {
                    if (other$gsphDateFrequency != null) {
                        return false;
                    }
                } else if (!this$gsphDateFrequency.equals(other$gsphDateFrequency)) {
                    return false;
                }

                label505: {
                    Object this$gsphWeekFrequency = this.getGsphWeekFrequency();
                    Object other$gsphWeekFrequency = other.getGsphWeekFrequency();
                    if (this$gsphWeekFrequency == null) {
                        if (other$gsphWeekFrequency == null) {
                            break label505;
                        }
                    } else if (this$gsphWeekFrequency.equals(other$gsphWeekFrequency)) {
                        break label505;
                    }

                    return false;
                }

                Object this$gsphBeginTime = this.getGsphBeginTime();
                Object other$gsphBeginTime = other.getGsphBeginTime();
                if (this$gsphBeginTime == null) {
                    if (other$gsphBeginTime != null) {
                        return false;
                    }
                } else if (!this$gsphBeginTime.equals(other$gsphBeginTime)) {
                    return false;
                }

                label491: {
                    Object this$gsphEndTime = this.getGsphEndTime();
                    Object other$gsphEndTime = other.getGsphEndTime();
                    if (this$gsphEndTime == null) {
                        if (other$gsphEndTime == null) {
                            break label491;
                        }
                    } else if (this$gsphEndTime.equals(other$gsphEndTime)) {
                        break label491;
                    }

                    return false;
                }

                Object this$gsphType = this.getGsphType();
                Object other$gsphType = other.getGsphType();
                if (this$gsphType == null) {
                    if (other$gsphType != null) {
                        return false;
                    }
                } else if (!this$gsphType.equals(other$gsphType)) {
                    return false;
                }

                Object this$gsphPart = this.getGsphPart();
                Object other$gsphPart = other.getGsphPart();
                if (this$gsphPart == null) {
                    if (other$gsphPart != null) {
                        return false;
                    }
                } else if (!this$gsphPart.equals(other$gsphPart)) {
                    return false;
                }

                label470: {
                    Object this$gsphPara1 = this.getGsphPara1();
                    Object other$gsphPara1 = other.getGsphPara1();
                    if (this$gsphPara1 == null) {
                        if (other$gsphPara1 == null) {
                            break label470;
                        }
                    } else if (this$gsphPara1.equals(other$gsphPara1)) {
                        break label470;
                    }

                    return false;
                }

                label463: {
                    Object this$gsphPara2 = this.getGsphPara2();
                    Object other$gsphPara2 = other.getGsphPara2();
                    if (this$gsphPara2 == null) {
                        if (other$gsphPara2 == null) {
                            break label463;
                        }
                    } else if (this$gsphPara2.equals(other$gsphPara2)) {
                        break label463;
                    }

                    return false;
                }

                Object this$gsphPara3 = this.getGsphPara3();
                Object other$gsphPara3 = other.getGsphPara3();
                if (this$gsphPara3 == null) {
                    if (other$gsphPara3 != null) {
                        return false;
                    }
                } else if (!this$gsphPara3.equals(other$gsphPara3)) {
                    return false;
                }

                Object this$gsphPara4 = this.getGsphPara4();
                Object other$gsphPara4 = other.getGsphPara4();
                if (this$gsphPara4 == null) {
                    if (other$gsphPara4 != null) {
                        return false;
                    }
                } else if (!this$gsphPara4.equals(other$gsphPara4)) {
                    return false;
                }

                label442: {
                    Object this$gsphExclusion = this.getGsphExclusion();
                    Object other$gsphExclusion = other.getGsphExclusion();
                    if (this$gsphExclusion == null) {
                        if (other$gsphExclusion == null) {
                            break label442;
                        }
                    } else if (this$gsphExclusion.equals(other$gsphExclusion)) {
                        break label442;
                    }

                    return false;
                }

                label435: {
                    Object this$type = this.getType();
                    Object other$type = other.getType();
                    if (this$type == null) {
                        if (other$type == null) {
                            break label435;
                        }
                    } else if (this$type.equals(other$type)) {
                        break label435;
                    }

                    return false;
                }

                Object this$storeStart = this.getStoreStart();
                Object other$storeStart = other.getStoreStart();
                if (this$storeStart == null) {
                    if (other$storeStart != null) {
                        return false;
                    }
                } else if (!this$storeStart.equals(other$storeStart)) {
                    return false;
                }

                Object this$storeEnd = this.getStoreEnd();
                Object other$storeEnd = other.getStoreEnd();
                if (this$storeEnd == null) {
                    if (other$storeEnd != null) {
                        return false;
                    }
                } else if (!this$storeEnd.equals(other$storeEnd)) {
                    return false;
                }

                label414: {
                    Object this$proCode = this.getProCode();
                    Object other$proCode = other.getProCode();
                    if (this$proCode == null) {
                        if (other$proCode == null) {
                            break label414;
                        }
                    } else if (this$proCode.equals(other$proCode)) {
                        break label414;
                    }

                    return false;
                }

                label407: {
                    Object this$storeGroup = this.getStoreGroup();
                    Object other$storeGroup = other.getStoreGroup();
                    if (this$storeGroup == null) {
                        if (other$storeGroup == null) {
                            break label407;
                        }
                    } else if (this$storeGroup.equals(other$storeGroup)) {
                        break label407;
                    }

                    return false;
                }

                Object this$paramOneList = this.getParamOneList();
                Object other$paramOneList = other.getParamOneList();
                if (this$paramOneList == null) {
                    if (other$paramOneList != null) {
                        return false;
                    }
                } else if (!this$paramOneList.equals(other$paramOneList)) {
                    return false;
                }

                label393: {
                    Object this$paramTwoList = this.getParamTwoList();
                    Object other$paramTwoList = other.getParamTwoList();
                    if (this$paramTwoList == null) {
                        if (other$paramTwoList == null) {
                            break label393;
                        }
                    } else if (this$paramTwoList.equals(other$paramTwoList)) {
                        break label393;
                    }

                    return false;
                }

                Object this$paramThreeList = this.getParamThreeList();
                Object other$paramThreeList = other.getParamThreeList();
                if (this$paramThreeList == null) {
                    if (other$paramThreeList != null) {
                        return false;
                    }
                } else if (!this$paramThreeList.equals(other$paramThreeList)) {
                    return false;
                }

                label379: {
                    Object this$paramFourList = this.getParamFourList();
                    Object other$paramFourList = other.getParamFourList();
                    if (this$paramFourList == null) {
                        if (other$paramFourList == null) {
                            break label379;
                        }
                    } else if (this$paramFourList.equals(other$paramFourList)) {
                        break label379;
                    }

                    return false;
                }

                Object this$exclusionList = this.getExclusionList();
                Object other$exclusionList = other.getExclusionList();
                if (this$exclusionList == null) {
                    if (other$exclusionList != null) {
                        return false;
                    }
                } else if (!this$exclusionList.equals(other$exclusionList)) {
                    return false;
                }

                Object this$storeIds = this.getStoreIds();
                Object other$storeIds = other.getStoreIds();
                if (this$storeIds == null) {
                    if (other$storeIds != null) {
                        return false;
                    }
                } else if (!this$storeIds.equals(other$storeIds)) {
                    return false;
                }

                label358: {
                    Object this$storeList = this.getStoreList();
                    Object other$storeList = other.getStoreList();
                    if (this$storeList == null) {
                        if (other$storeList == null) {
                            break label358;
                        }
                    } else if (this$storeList.equals(other$storeList)) {
                        break label358;
                    }

                    return false;
                }

                label351: {
                    Object this$promUnitarySetInDataList = this.getPromUnitarySetInDataList();
                    Object other$promUnitarySetInDataList = other.getPromUnitarySetInDataList();
                    if (this$promUnitarySetInDataList == null) {
                        if (other$promUnitarySetInDataList == null) {
                            break label351;
                        }
                    } else if (this$promUnitarySetInDataList.equals(other$promUnitarySetInDataList)) {
                        break label351;
                    }

                    return false;
                }

                Object this$promSeriesCondsInDataList = this.getPromSeriesCondsInDataList();
                Object other$promSeriesCondsInDataList = other.getPromSeriesCondsInDataList();
                if (this$promSeriesCondsInDataList == null) {
                    if (other$promSeriesCondsInDataList != null) {
                        return false;
                    }
                } else if (!this$promSeriesCondsInDataList.equals(other$promSeriesCondsInDataList)) {
                    return false;
                }

                Object this$promSeriesSetInDataList = this.getPromSeriesSetInDataList();
                Object other$promSeriesSetInDataList = other.getPromSeriesSetInDataList();
                if (this$promSeriesSetInDataList == null) {
                    if (other$promSeriesSetInDataList != null) {
                        return false;
                    }
                } else if (!this$promSeriesSetInDataList.equals(other$promSeriesSetInDataList)) {
                    return false;
                }

                label330: {
                    Object this$promGiftCondsInDataList = this.getPromGiftCondsInDataList();
                    Object other$promGiftCondsInDataList = other.getPromGiftCondsInDataList();
                    if (this$promGiftCondsInDataList == null) {
                        if (other$promGiftCondsInDataList == null) {
                            break label330;
                        }
                    } else if (this$promGiftCondsInDataList.equals(other$promGiftCondsInDataList)) {
                        break label330;
                    }

                    return false;
                }

                label323: {
                    Object this$promGiftSetInDataList = this.getPromGiftSetInDataList();
                    Object other$promGiftSetInDataList = other.getPromGiftSetInDataList();
                    if (this$promGiftSetInDataList == null) {
                        if (other$promGiftSetInDataList == null) {
                            break label323;
                        }
                    } else if (this$promGiftSetInDataList.equals(other$promGiftSetInDataList)) {
                        break label323;
                    }

                    return false;
                }

                Object this$promGiftResultInDataList = this.getPromGiftResultInDataList();
                Object other$promGiftResultInDataList = other.getPromGiftResultInDataList();
                if (this$promGiftResultInDataList == null) {
                    if (other$promGiftResultInDataList != null) {
                        return false;
                    }
                } else if (!this$promGiftResultInDataList.equals(other$promGiftResultInDataList)) {
                    return false;
                }

                Object this$promCouponSetInDataList = this.getPromCouponSetInDataList();
                Object other$promCouponSetInDataList = other.getPromCouponSetInDataList();
                if (this$promCouponSetInDataList == null) {
                    if (other$promCouponSetInDataList != null) {
                        return false;
                    }
                } else if (!this$promCouponSetInDataList.equals(other$promCouponSetInDataList)) {
                    return false;
                }

                label302: {
                    Object this$promCouponGrantInDataList = this.getPromCouponGrantInDataList();
                    Object other$promCouponGrantInDataList = other.getPromCouponGrantInDataList();
                    if (this$promCouponGrantInDataList == null) {
                        if (other$promCouponGrantInDataList == null) {
                            break label302;
                        }
                    } else if (this$promCouponGrantInDataList.equals(other$promCouponGrantInDataList)) {
                        break label302;
                    }

                    return false;
                }

                label295: {
                    Object this$promCouponUseInDataList = this.getPromCouponUseInDataList();
                    Object other$promCouponUseInDataList = other.getPromCouponUseInDataList();
                    if (this$promCouponUseInDataList == null) {
                        if (other$promCouponUseInDataList == null) {
                            break label295;
                        }
                    } else if (this$promCouponUseInDataList.equals(other$promCouponUseInDataList)) {
                        break label295;
                    }

                    return false;
                }

                Object this$promHySetInDataList = this.getPromHySetInDataList();
                Object other$promHySetInDataList = other.getPromHySetInDataList();
                if (this$promHySetInDataList == null) {
                    if (other$promHySetInDataList != null) {
                        return false;
                    }
                } else if (!this$promHySetInDataList.equals(other$promHySetInDataList)) {
                    return false;
                }

                label281: {
                    Object this$promHyrDiscountInDataList = this.getPromHyrDiscountInDataList();
                    Object other$promHyrDiscountInDataList = other.getPromHyrDiscountInDataList();
                    if (this$promHyrDiscountInDataList == null) {
                        if (other$promHyrDiscountInDataList == null) {
                            break label281;
                        }
                    } else if (this$promHyrDiscountInDataList.equals(other$promHyrDiscountInDataList)) {
                        break label281;
                    }

                    return false;
                }

                Object this$promHyrPriceInDataList = this.getPromHyrPriceInDataList();
                Object other$promHyrPriceInDataList = other.getPromHyrPriceInDataList();
                if (this$promHyrPriceInDataList == null) {
                    if (other$promHyrPriceInDataList != null) {
                        return false;
                    }
                } else if (!this$promHyrPriceInDataList.equals(other$promHyrPriceInDataList)) {
                    return false;
                }

                label267: {
                    Object this$promAssoCondsInDataList = this.getPromAssoCondsInDataList();
                    Object other$promAssoCondsInDataList = other.getPromAssoCondsInDataList();
                    if (this$promAssoCondsInDataList == null) {
                        if (other$promAssoCondsInDataList == null) {
                            break label267;
                        }
                    } else if (this$promAssoCondsInDataList.equals(other$promAssoCondsInDataList)) {
                        break label267;
                    }

                    return false;
                }

                Object this$promAssoSetInDataList = this.getPromAssoSetInDataList();
                Object other$promAssoSetInDataList = other.getPromAssoSetInDataList();
                if (this$promAssoSetInDataList == null) {
                    if (other$promAssoSetInDataList != null) {
                        return false;
                    }
                } else if (!this$promAssoSetInDataList.equals(other$promAssoSetInDataList)) {
                    return false;
                }

                Object this$promAssoResultInDataList = this.getPromAssoResultInDataList();
                Object other$promAssoResultInDataList = other.getPromAssoResultInDataList();
                if (this$promAssoResultInDataList == null) {
                    if (other$promAssoResultInDataList != null) {
                        return false;
                    }
                } else if (!this$promAssoResultInDataList.equals(other$promAssoResultInDataList)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromoteOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsphBrId = this.getGsphBrId();
        result = result * 59 + ($gsphBrId == null ? 43 : $gsphBrId.hashCode());
        Object $gsphVoucherId = this.getGsphVoucherId();
        result = result * 59 + ($gsphVoucherId == null ? 43 : $gsphVoucherId.hashCode());
        Object $gsphTheme = this.getGsphTheme();
        result = result * 59 + ($gsphTheme == null ? 43 : $gsphTheme.hashCode());
        Object $gsphName = this.getGsphName();
        result = result * 59 + ($gsphName == null ? 43 : $gsphName.hashCode());
        Object $gsphRemarks = this.getGsphRemarks();
        result = result * 59 + ($gsphRemarks == null ? 43 : $gsphRemarks.hashCode());
        Object $gsphStatus = this.getGsphStatus();
        result = result * 59 + ($gsphStatus == null ? 43 : $gsphStatus.hashCode());
        Object $gsphBeginDate = this.getGsphBeginDate();
        result = result * 59 + ($gsphBeginDate == null ? 43 : $gsphBeginDate.hashCode());
        Object $gsphEndDate = this.getGsphEndDate();
        result = result * 59 + ($gsphEndDate == null ? 43 : $gsphEndDate.hashCode());
        Object $gsphDateFrequency = this.getGsphDateFrequency();
        result = result * 59 + ($gsphDateFrequency == null ? 43 : $gsphDateFrequency.hashCode());
        Object $gsphWeekFrequency = this.getGsphWeekFrequency();
        result = result * 59 + ($gsphWeekFrequency == null ? 43 : $gsphWeekFrequency.hashCode());
        Object $gsphBeginTime = this.getGsphBeginTime();
        result = result * 59 + ($gsphBeginTime == null ? 43 : $gsphBeginTime.hashCode());
        Object $gsphEndTime = this.getGsphEndTime();
        result = result * 59 + ($gsphEndTime == null ? 43 : $gsphEndTime.hashCode());
        Object $gsphType = this.getGsphType();
        result = result * 59 + ($gsphType == null ? 43 : $gsphType.hashCode());
        Object $gsphPart = this.getGsphPart();
        result = result * 59 + ($gsphPart == null ? 43 : $gsphPart.hashCode());
        Object $gsphPara1 = this.getGsphPara1();
        result = result * 59 + ($gsphPara1 == null ? 43 : $gsphPara1.hashCode());
        Object $gsphPara2 = this.getGsphPara2();
        result = result * 59 + ($gsphPara2 == null ? 43 : $gsphPara2.hashCode());
        Object $gsphPara3 = this.getGsphPara3();
        result = result * 59 + ($gsphPara3 == null ? 43 : $gsphPara3.hashCode());
        Object $gsphPara4 = this.getGsphPara4();
        result = result * 59 + ($gsphPara4 == null ? 43 : $gsphPara4.hashCode());
        Object $gsphExclusion = this.getGsphExclusion();
        result = result * 59 + ($gsphExclusion == null ? 43 : $gsphExclusion.hashCode());
        Object $type = this.getType();
        result = result * 59 + ($type == null ? 43 : $type.hashCode());
        Object $storeStart = this.getStoreStart();
        result = result * 59 + ($storeStart == null ? 43 : $storeStart.hashCode());
        Object $storeEnd = this.getStoreEnd();
        result = result * 59 + ($storeEnd == null ? 43 : $storeEnd.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $storeGroup = this.getStoreGroup();
        result = result * 59 + ($storeGroup == null ? 43 : $storeGroup.hashCode());
        Object $paramOneList = this.getParamOneList();
        result = result * 59 + ($paramOneList == null ? 43 : $paramOneList.hashCode());
        Object $paramTwoList = this.getParamTwoList();
        result = result * 59 + ($paramTwoList == null ? 43 : $paramTwoList.hashCode());
        Object $paramThreeList = this.getParamThreeList();
        result = result * 59 + ($paramThreeList == null ? 43 : $paramThreeList.hashCode());
        Object $paramFourList = this.getParamFourList();
        result = result * 59 + ($paramFourList == null ? 43 : $paramFourList.hashCode());
        Object $exclusionList = this.getExclusionList();
        result = result * 59 + ($exclusionList == null ? 43 : $exclusionList.hashCode());
        Object $storeIds = this.getStoreIds();
        result = result * 59 + ($storeIds == null ? 43 : $storeIds.hashCode());
        Object $storeList = this.getStoreList();
        result = result * 59 + ($storeList == null ? 43 : $storeList.hashCode());
        Object $promUnitarySetInDataList = this.getPromUnitarySetInDataList();
        result = result * 59 + ($promUnitarySetInDataList == null ? 43 : $promUnitarySetInDataList.hashCode());
        Object $promSeriesCondsInDataList = this.getPromSeriesCondsInDataList();
        result = result * 59 + ($promSeriesCondsInDataList == null ? 43 : $promSeriesCondsInDataList.hashCode());
        Object $promSeriesSetInDataList = this.getPromSeriesSetInDataList();
        result = result * 59 + ($promSeriesSetInDataList == null ? 43 : $promSeriesSetInDataList.hashCode());
        Object $promGiftCondsInDataList = this.getPromGiftCondsInDataList();
        result = result * 59 + ($promGiftCondsInDataList == null ? 43 : $promGiftCondsInDataList.hashCode());
        Object $promGiftSetInDataList = this.getPromGiftSetInDataList();
        result = result * 59 + ($promGiftSetInDataList == null ? 43 : $promGiftSetInDataList.hashCode());
        Object $promGiftResultInDataList = this.getPromGiftResultInDataList();
        result = result * 59 + ($promGiftResultInDataList == null ? 43 : $promGiftResultInDataList.hashCode());
        Object $promCouponSetInDataList = this.getPromCouponSetInDataList();
        result = result * 59 + ($promCouponSetInDataList == null ? 43 : $promCouponSetInDataList.hashCode());
        Object $promCouponGrantInDataList = this.getPromCouponGrantInDataList();
        result = result * 59 + ($promCouponGrantInDataList == null ? 43 : $promCouponGrantInDataList.hashCode());
        Object $promCouponUseInDataList = this.getPromCouponUseInDataList();
        result = result * 59 + ($promCouponUseInDataList == null ? 43 : $promCouponUseInDataList.hashCode());
        Object $promHySetInDataList = this.getPromHySetInDataList();
        result = result * 59 + ($promHySetInDataList == null ? 43 : $promHySetInDataList.hashCode());
        Object $promHyrDiscountInDataList = this.getPromHyrDiscountInDataList();
        result = result * 59 + ($promHyrDiscountInDataList == null ? 43 : $promHyrDiscountInDataList.hashCode());
        Object $promHyrPriceInDataList = this.getPromHyrPriceInDataList();
        result = result * 59 + ($promHyrPriceInDataList == null ? 43 : $promHyrPriceInDataList.hashCode());
        Object $promAssoCondsInDataList = this.getPromAssoCondsInDataList();
        result = result * 59 + ($promAssoCondsInDataList == null ? 43 : $promAssoCondsInDataList.hashCode());
        Object $promAssoSetInDataList = this.getPromAssoSetInDataList();
        result = result * 59 + ($promAssoSetInDataList == null ? 43 : $promAssoSetInDataList.hashCode());
        Object $promAssoResultInDataList = this.getPromAssoResultInDataList();
        result = result * 59 + ($promAssoResultInDataList == null ? 43 : $promAssoResultInDataList.hashCode());
        return result;
    }

    public String toString() {
        return "PromoteOutData(clientId=" + this.getClientId() + ", gsphBrId=" + this.getGsphBrId() + ", gsphVoucherId=" + this.getGsphVoucherId() + ", gsphTheme=" + this.getGsphTheme() + ", gsphName=" + this.getGsphName() + ", gsphRemarks=" + this.getGsphRemarks() + ", gsphStatus=" + this.getGsphStatus() + ", gsphBeginDate=" + this.getGsphBeginDate() + ", gsphEndDate=" + this.getGsphEndDate() + ", gsphDateFrequency=" + this.getGsphDateFrequency() + ", gsphWeekFrequency=" + this.getGsphWeekFrequency() + ", gsphBeginTime=" + this.getGsphBeginTime() + ", gsphEndTime=" + this.getGsphEndTime() + ", gsphType=" + this.getGsphType() + ", gsphPart=" + this.getGsphPart() + ", gsphPara1=" + this.getGsphPara1() + ", gsphPara2=" + this.getGsphPara2() + ", gsphPara3=" + this.getGsphPara3() + ", gsphPara4=" + this.getGsphPara4() + ", gsphExclusion=" + this.getGsphExclusion() + ", type=" + this.getType() + ", storeStart=" + this.getStoreStart() + ", storeEnd=" + this.getStoreEnd() + ", proCode=" + this.getProCode() + ", storeGroup=" + this.getStoreGroup() + ", paramOneList=" + this.getParamOneList() + ", paramTwoList=" + this.getParamTwoList() + ", paramThreeList=" + this.getParamThreeList() + ", paramFourList=" + this.getParamFourList() + ", exclusionList=" + this.getExclusionList() + ", storeIds=" + this.getStoreIds() + ", storeList=" + this.getStoreList() + ", promUnitarySetInDataList=" + this.getPromUnitarySetInDataList() + ", promSeriesCondsInDataList=" + this.getPromSeriesCondsInDataList() + ", promSeriesSetInDataList=" + this.getPromSeriesSetInDataList() + ", promGiftCondsInDataList=" + this.getPromGiftCondsInDataList() + ", promGiftSetInDataList=" + this.getPromGiftSetInDataList() + ", promGiftResultInDataList=" + this.getPromGiftResultInDataList() + ", promCouponSetInDataList=" + this.getPromCouponSetInDataList() + ", promCouponGrantInDataList=" + this.getPromCouponGrantInDataList() + ", promCouponUseInDataList=" + this.getPromCouponUseInDataList() + ", promHySetInDataList=" + this.getPromHySetInDataList() + ", promHyrDiscountInDataList=" + this.getPromHyrDiscountInDataList() + ", promHyrPriceInDataList=" + this.getPromHyrPriceInDataList() + ", promAssoCondsInDataList=" + this.getPromAssoCondsInDataList() + ", promAssoSetInDataList=" + this.getPromAssoSetInDataList() + ", promAssoResultInDataList=" + this.getPromAssoResultInDataList() + ")";
    }
}
