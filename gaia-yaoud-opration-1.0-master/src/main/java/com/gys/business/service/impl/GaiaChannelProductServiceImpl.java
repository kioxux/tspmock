package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.GaiaChannelDataMapper;
import com.gys.business.mapper.GaiaChannelDefaultMapper;
import com.gys.business.mapper.GaiaChannelProductMapper;
import com.gys.business.service.GaiaChannelProductService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.util.ValidateUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

@Component
@Log4j2
public class GaiaChannelProductServiceImpl implements GaiaChannelProductService {

    @Autowired
    private GaiaChannelDefaultMapper gaiaChannelDefaultMapper;
    @Autowired
    private GaiaChannelDataMapper gaiaChannelDataMapper;
    @Autowired
    private GaiaChannelProductMapper gaiaChannelProductMapper;

    @Override
    public List<HashMap<String,Object>> listChannelDefault(GaiaChannelProductInData inData) {
        return gaiaChannelDefaultMapper.listChannelDefault(inData);
    }

    @Override
    @Transactional
    public boolean insertGaiaChannelData(GetLoginOutData loginUser, GaiaChannelData inData) {
        inData.setClient(loginUser.getClient());
        Date now = new Date();
        int count = gaiaChannelDataMapper.checkExsitData(inData);
        if (count > 0) {
            throw new BusinessException("该渠道编码已存在，请勿重复添加！");
        }
        /*if (StringUtils.isEmpty(inData.getGcdChannel())) {
            GaiaChannelData data = gaiaChannelDataMapper.selectChannelCode(inData);
            inData.setGcdChannel(data.getGcdChannel());
        }*/
        if (ObjectUtil.isEmpty(inData.getId())) {
            //新增
            inData.setClient(loginUser.getClient());
            inData.setGcdStatus("0");
            inData.setGcdCreateBy(loginUser.getUserId());
            inData.setGcdCreateDate(now);
            inData.setGcdUpdateBy(loginUser.getUserId());
            inData.setGcdUpdateDate(now);
            gaiaChannelDataMapper.insertSelective(inData);
        } else {
            inData.setGcdUpdateBy(loginUser.getUserId());
            inData.setGcdUpdateDate(now);
            gaiaChannelDataMapper.updateByPrimaryKeySelective(inData);
        }
        return true;
    }

    @Override
    public List<GaiaChannelDataOutData> listGaiaChannelData(GetLoginOutData loginUser, GaiaChannelData inData) {
        inData.setClient(loginUser.getClient());
        return gaiaChannelDataMapper.listGaiaChannelData(inData);
    }

    @Override
    @Transactional
    public Object deleteGaiaChannelData(GetLoginOutData loginUser, GaiaChannelData inData) {
        Date now = new Date();
        inData.setIsDelete(1);
        inData.setGcdUpdateBy(loginUser.getUserId());
        inData.setGcdUpdateDate(now);
        gaiaChannelDataMapper.updateByPrimaryKeySelective(inData);
        return true;
    }

    @Override
    public List<GaiaChannelProductOutData> listWaiteInsertData(GetLoginOutData loginUser, GaiaChannelProductInData inData) {
        inData.setClient(loginUser.getClient());
        if (!StringUtils.isEmpty(inData.getGcpProCode())) {
            String[] split = inData.getGcpProCode().split(",");
            inData.setProCodeList(Arrays.asList(split.clone()));
        }
        List<GaiaChannelProductOutData> outDataList = gaiaChannelDataMapper.listWaiteInsertData(inData);
        if (!CollectionUtils.isEmpty(outDataList)) {
            for (GaiaChannelProductOutData outData : outDataList) {
                outData.setGcpChannelPrice(checkNull(outData.getGsppPriceNormal()));
                outData.setGrossRate(divide(subtract(checkNull(outData.getGsppPriceNormal()),checkNull(outData.getPrice())), checkNull(outData.getGsppPriceNormal())));;
                outData.setGrossRateC(outData.getGrossRate().toString());;

            }
        }
        return outDataList;
    }
    private BigDecimal subtract(BigDecimal SalesAmt, BigDecimal lastSalesAmt) {
        SalesAmt = ObjectUtil.isEmpty(SalesAmt) ? BigDecimal.ZERO : SalesAmt;
        lastSalesAmt = ObjectUtil.isEmpty(lastSalesAmt) ? BigDecimal.ZERO : lastSalesAmt;
        BigDecimal subtract = SalesAmt.subtract(lastSalesAmt);
        return subtract;
    }
    private BigDecimal divide(BigDecimal lastYearSalesAmt, BigDecimal salesAmt2) {
        lastYearSalesAmt = ObjectUtil.isNotEmpty(lastYearSalesAmt) ? lastYearSalesAmt : BigDecimal.ZERO;
        salesAmt2 = ObjectUtil.isNotEmpty(salesAmt2) ? salesAmt2 : BigDecimal.ZERO;
        return salesAmt2.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : lastYearSalesAmt.divide(salesAmt2, 4, BigDecimal.ROUND_HALF_UP);
    }

    private BigDecimal checkNull(BigDecimal num) {
        BigDecimal bigDecimal = ObjectUtil.isNotEmpty(num) ? num : BigDecimal.ZERO;
        return bigDecimal;
    }

    private BigDecimal checkNull(Double num) {
        BigDecimal bigDecimal = ObjectUtil.isNotEmpty(num) ? new BigDecimal(num.toString()) : BigDecimal.ZERO;
        return bigDecimal;
    }
    @Override
    @Transactional
    public Object insertBatchChannelProduct(GetLoginOutData loginUser, GaiaChannelProductInData inData) {
        if (!CollectionUtils.isEmpty(inData.getDetailList())) {
            List<GaiaChannelProductOutData> detailList = inData.getDetailList();
            List<GaiaChannelProduct> inDataList = new ArrayList<>();
            Date now = new Date();
            for (GaiaChannelProductOutData gaiaChannelProductOutData : detailList) {
                int count = gaiaChannelDataMapper.checkProduct(gaiaChannelProductOutData);
                if (count > 0) {
                    throw new BusinessException("门店：" + gaiaChannelProductOutData.getStoName() + " 渠道：" + gaiaChannelProductOutData.getGcdChannelName() + " 药品：" + gaiaChannelProductOutData.getProCommonname() + "已存在！请勿重复添加！");
                }
                GaiaChannelProduct channelProduct = new GaiaChannelProduct();
                BeanUtils.copyProperties(gaiaChannelProductOutData, channelProduct);
                channelProduct.setGcpStatus("1");
                channelProduct.setGcpCreateBy(loginUser.getUserId());
                channelProduct.setGcpCreateDate(now);
                channelProduct.setGcpUpdateDate(now);
                channelProduct.setGcpUpdateBy(loginUser.getUserId());
                inDataList.add(channelProduct);
            }
            gaiaChannelProductMapper.batchInsert(inDataList);
        } else {
            throw new BusinessException("请选择需要保存的记录！");
        }
        return null;
    }

    /**
     * @author SunJiaNan
     * @Date 2021-07-08
     * @param file
     * @param userInfo
     * @return
     */
    @Override
    @Transactional
    public JsonResult importData(MultipartFile file, GetLoginOutData userInfo) {
        //检查文件以及数据完整性
        List<GaiaChannelProduct> channelProductDataList = checkFileData(file);

        //获取参数
        String client = userInfo.getClient();
        String userId = userInfo.getUserId();
        Date date = new Date();
        StringBuilder strBuilder = new StringBuilder("");
        //查询当前加盟商下所有 门店编码 商品编码 渠道 拼接为字符串
        List<String> concatStrList = gaiaChannelProductMapper.getConcatStrList(client);
        //查询当前加盟商下所有门店
        Set<String> storeSet = gaiaChannelProductMapper.getStoreByClient(client);
        //查询当前加盟商 门店下所有门店编码 + 商品编码拼接字符串
        Set<String> storeProductStrSet = gaiaChannelProductMapper.getConcatStrByClient(client);
        //查询所有渠道
        Set<String> channelSet = gaiaChannelProductMapper.getAllChannel(client);
        //批量新增list
        List<GaiaChannelProduct> batchInsertList = new ArrayList<>(channelProductDataList.size());
        for (GaiaChannelProduct channelProduct : channelProductDataList) {
            String storeCode = channelProduct.getGcpStoreCode();    //门店编码
            String channel = channelProduct.getGcdChannel();        //商品渠道
            String proCode = channelProduct.getGcpProCode();        //商品编码
            String channelPriceStr = channelProduct.getGcpChannelPriceStr();     //渠道价格
            String status = channelProduct.getGcpStatus();          //商品状态
            if (ValidateUtil.isEmpty(storeCode) || ValidateUtil.isEmpty(channel)
                    || ValidateUtil.isEmpty(proCode) || ValidateUtil.isEmpty(status)) {
                throw new BusinessException("提示：门店编码, 商品渠道, 商品编码, 商品状态不能为空, 请检查数据");
            }
            if (!storeSet.contains(storeCode)) {
                throw new BusinessException("提示：当前门店编码不存在：" + storeCode + ", 请检查数据");
            }
            if (!storeProductStrSet.contains(storeCode + proCode)) {
                throw new BusinessException("提示：" + storeCode + "门店下," + proCode + "商品编码,不存在, 请检查数据");
            }
            if (!channelSet.contains(channel)) {
                throw new BusinessException("提示：当前用户无此渠道：" + channel + ", 权限。 请添加渠道");
            }
            if (!CommonConstant.CHANNEL_MANAGER_STATUS_UP.equals(status) && !CommonConstant.CHANNEL_MANAGER_STATUS_LOWER.equals(status)) {
                throw new BusinessException("提示：商品状态错误, 请检查");
            }
            //将门店编码 商品编码 渠道进行拼接 与已经存在的渠道商品进行比较 如果已经存在 则不允许导入
            strBuilder.append(storeCode).append(proCode).append(channel);
            if (concatStrList.contains(strBuilder.toString())) {
                throw new BusinessException("提示：该门店编码：" + storeCode + ",商品：" + proCode + ",渠道已存在：" + channel + ", 无法导入！");
            }
            //每次比较后清空stringBuilder中的缓存
            strBuilder.delete(0, strBuilder.length());
            //获取零售价
            BigDecimal priceNormal = getPriceNormal(channelPriceStr, client, storeCode, proCode);
            channelProduct.setClient(client);
            channelProduct.setGcpChannelPrice(priceNormal.setScale(2, BigDecimal.ROUND_HALF_UP));
            channelProduct.setGcpStatus(CommonConstant.CHANNEL_MANAGER_STATUS_UP.equals(status) ? "1" : "2");
            channelProduct.setGcpCreateBy(userId);
            channelProduct.setGcpCreateDate(date);
            channelProduct.setGcpUpdateBy(userId);
            channelProduct.setGcpUpdateDate(date);
            batchInsertList.add(channelProduct);
        }
        //批量新增
        if (ValidateUtil.isNotEmpty(batchInsertList)) {
            gaiaChannelProductMapper.batchInsert(batchInsertList);
        }
        return JsonResult.success("", "提示：导入成功！");
    }

    @Override
    public List<GaiaChannelProductOutData> listChannelProduct(GetLoginOutData loginUser, GaiaChannelProductInData inData) {
        inData.setClient(loginUser.getClient());
        if (!StringUtils.isEmpty(inData.getGcpProCode())) {
            String[] split = inData.getGcpProCode().split(",");
            inData.setProCodeList(Arrays.asList(split.clone()));
        }
        return gaiaChannelProductMapper.listChannelProduct(inData);
    }

    @Override
    public Boolean updateChannelProduct(GetLoginOutData loginUser, GaiaChannelProduct inData) {
        inData.setGcpUpdateBy(loginUser.getUserId());
        inData.setGcpUpdateDate(new Date());
        gaiaChannelProductMapper.updateByPrimaryKeySelective(inData);
        return true;
    }

    @Override
    public Object deleteChannelProduct(GetLoginOutData loginUser, GaiaChannelProduct inData) {
        inData.setGcpUpdateBy(loginUser.getUserId());
        inData.setGcpUpdateDate(new Date());
        inData.setIsDelete(1);
        gaiaChannelProductMapper.updateByPrimaryKeySelective(inData);
        return true;
    }

    @Override
    public Object updateBatchChannelProduct(GetLoginOutData loginUser, List<GaiaChannelProduct> list) {
        if (!CollectionUtils.isEmpty(list)) {
            for (GaiaChannelProduct gaiaChannelProduct : list) {
                this.updateChannelProduct(loginUser,gaiaChannelProduct);
            }
        }
        return true;
    }

    @Override
    public Object deleteBatchChannelProduct(GetLoginOutData loginUser, List<GaiaChannelProduct> list) {
        if (!CollectionUtils.isEmpty(list)) {
            for (GaiaChannelProduct gaiaChannelProduct : list) {
                this.deleteChannelProduct(loginUser, gaiaChannelProduct);
            }
        } else {
            throw new BusinessException("请选择需要删除的记录！");
        }
        return true;
    }

    @Override
    public List<HashMap<String, Object>> listStores(GetLoginOutData loginUser, GaiaChannelProduct inData) {
        inData.setClient(loginUser.getClient());
        return gaiaChannelProductMapper.listStores(inData);
    }

    @Override
    public List<GaiaChannelProductOutData> listGoods(GetLoginOutData loginUser, GaiaChannelProduct inData) {
        return null;
    }


    /**
     * 检查文件以及数据完整性
     * @author SunJiaNan
     * @Date 2021-07-08
     * @param file
     */
    private List<GaiaChannelProduct> checkFileData(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (ValidateUtil.isEmpty(fileName)) {
            throw new BusinessException("提示：上传文件名不能为空");
        }
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        if (!".xlsx".equals(suffixName)) {
            throw new BusinessException("提示：请检查文件格式是否为xlsx格式");
        }

        //创建sheet  第一页，第一行
        Sheet sheet = new Sheet(1, 0, GaiaChannelProduct.class);
        //获取到 GaiaChannelProduct实体集合
        List<Object> excelDataList = null;
        try {
            excelDataList = EasyExcelFactory.read(file.getInputStream(), sheet);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("上传特殊商品,读取excel失败: {}",e.getMessage(), e);
            throw new BusinessException("提示：读取Excel失败");
        }
        if (ValidateUtil.isEmpty(excelDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //转换list泛型
        List<GaiaChannelProduct> channelProductList = new ArrayList<>(excelDataList.size());
        try {
            channelProductList = JSON.parseArray(JSON.toJSONString(excelDataList), GaiaChannelProduct.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据类型转换异常：{}",e.getMessage(), e);
            throw new BusinessException("提示：数据格式不正确, 请检查数据");
        }
        if (ValidateUtil.isEmpty(channelProductList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //校验表头
        GaiaChannelProduct dataHead = channelProductList.get(0);
        String[] channelManagerExcelHead = CommonConstant.CHANNEL_MANAGER_EXCEL_HEAD;
        if (!channelManagerExcelHead[0].equals(dataHead.getGcpStoreCode()) || !channelManagerExcelHead[1].equals(dataHead.getGcdChannel()) ||
                !channelManagerExcelHead[2].equals(dataHead.getGcpProCode()) || !channelManagerExcelHead[3].equals(dataHead.getGcpChannelPriceStr()) ||
                !channelManagerExcelHead[4].equals(dataHead.getGcpStatus())) {
            throw new BusinessException("提示：请填写正确的表头");
        }
        //截取除表头以外数据
        List<GaiaChannelProduct> channelProductDataList = channelProductList.subList(1, channelProductList.size());
        if (ValidateUtil.isEmpty(channelProductDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        return channelProductDataList;
    }

    /**
     * 获取零售价
     * @author SunJiaNan
     * @Date 2021-07-08
     * @param channelPriceStr
     * @param client
     * @param storeCode
     * @param proCode
     * @return
     */
    private BigDecimal getPriceNormal(String channelPriceStr, String client, String storeCode, String proCode) {
        //渠道价格
        BigDecimal normalPrice = null;
        if(ValidateUtil.isEmpty(channelPriceStr)) {
            //查询零售价
            normalPrice = gaiaChannelProductMapper.getPriceNormalByProCode(client, storeCode, proCode);
        } else {
            //使用前上传渠道价格
            try {
                normalPrice = new BigDecimal(channelPriceStr);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("上传渠道价格格式错误, 非数字格式!", e.getMessage());
                throw new BusinessException("提示：当前渠道价格格式错误：" + channelPriceStr + ", 请检查数据");
            }
        }
        return ValidateUtil.isNotEmpty(normalPrice) ? normalPrice : BigDecimal.ZERO;
    }


    /**
     * @author SunJiaNan
     * @Date 2021-07-09
     * @param file
     * @param userInfo
     * @return
     */
    @Override
    @Transactional
    public JsonResult batchImportUpdate(MultipartFile file, GetLoginOutData userInfo) {
        //检查文件以及数据完整性
        List<GaiaChannelProduct> channelProductList = checkFileData(file);
        //获取参数
        String client = userInfo.getClient();
        String userId = userInfo.getUserId();
        Date date = new Date();

        //批量更新list
        List<GaiaChannelProduct> batchUpdateList = new ArrayList<>(channelProductList.size());
        //查询当前加盟商下所有 门店编码 商品编码 渠道 拼接为字符串
        List<String> concatStrList = gaiaChannelProductMapper.getConcatStrList(client);
        StringBuilder strBuilder = new StringBuilder("");
        //校验数据是否存在
        for(GaiaChannelProduct channelProduct : channelProductList) {
            String storeCode = channelProduct.getGcpStoreCode();
            String channel = channelProduct.getGcdChannel();
            String proCode = channelProduct.getGcpProCode();
            String channelPriceStr = channelProduct.getGcpChannelPriceStr();
            String status = channelProduct.getGcpStatus();
            if (ValidateUtil.isEmpty(storeCode) || ValidateUtil.isEmpty(channel) || ValidateUtil.isEmpty(proCode)) {
                throw new BusinessException("提示：门店编码, 商品渠道, 商品编码不能为空, 请检查数据");
            }
            if(ValidateUtil.isNotEmpty(status)) {
                if (!CommonConstant.CHANNEL_MANAGER_STATUS_UP.equals(status) && !CommonConstant.CHANNEL_MANAGER_STATUS_LOWER.equals(status)) {
                    throw new BusinessException("提示：商品状态错误, 请检查");
                }
            }

            //将门店编码 商品编码 渠道进行拼接 与已经存在的渠道商品进行比较 如果不存在则无法修改
            String concatStr = strBuilder.append(storeCode).append(proCode).append(channel).toString();
            if(!concatStrList.contains(concatStr)) {
                throw new BusinessException("提示：该门店编码：" + storeCode + ",商品：" + proCode + ",渠道：" + channel + ",不存在, 请检查数据！");
            }
            strBuilder.delete(0, strBuilder.length());
            //如果渠道价格不为空 则更新此字段
            if(ValidateUtil.isNotEmpty(channelPriceStr)) {
                BigDecimal channelPrice = null;
                try {
                    channelPrice = new BigDecimal(channelPriceStr);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("上传渠道价格格式错误, 非数字格式!", e.getMessage());
                    throw new BusinessException("提示：当前渠道价格格式错误：" + channelPriceStr + ", 请检查数据");
                }
                channelProduct.setGcpChannelPrice(channelPrice.setScale(2, BigDecimal.ROUND_HALF_UP));
            }


            channelProduct.setClient(client);
            //如果商品状态不为空 则更新此字段
            if(ValidateUtil.isNotEmpty(status)) {
                channelProduct.setGcpStatus(CommonConstant.CHANNEL_MANAGER_STATUS_UP.equals(status) ? "1" : "2");
            }
            channelProduct.setGcpUpdateBy(userId);
            channelProduct.setGcpUpdateDate(date);
            batchUpdateList.add(channelProduct);
        }
        //批量更新
        if(ValidateUtil.isNotEmpty(batchUpdateList)) {
            gaiaChannelProductMapper.batchUpdate(batchUpdateList);
        }
        return JsonResult.success("", "提示：批量更新成功！");
    }

}
