package com.gys.business.service;

import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.CompadmDcOutData;
import com.gys.business.service.data.GetUserStoreOutData;
import com.gys.common.data.CommonData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

public interface StoreDataService {
    StoreOutData queryStoLeader(GetLoginOutData inDate);

    List<StoreOutData>queryStoInfoList(StoreOutData inData);

    List<CompadmDcOutData>selectAuthCompadmList(String clientId, String userId);

    List<GetUserStoreOutData> getStoreData(GetUserStoreOutData inData);

    List<StoreOutData> getStoreList(String client, CommonData data);

    StoreOutData getInventoryStore(InData inData);
}
