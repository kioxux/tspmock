//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetJingdongInData;

public interface JingdongService {
    void orderPayedPush(String params);

    void orderReminderPush(String params);

    void orderCancelPush(String params);

    void orderRefundAllPush(String params);

    void orderRefundPush(String params);

    void deliveryStatusPush(String params);

    void orderStatusPush(String params);

    void orderGetOrderDetail(GetJingdongInData inData);

    void orderConfirm(GetJingdongInData inData);

    void orderCancel(GetJingdongInData inData);

    void orderPreparationMealComplete(GetJingdongInData inData);

    void orderRefundAgree(GetJingdongInData inData);

    void orderRefundReject(GetJingdongInData inData);

    void medicineStock(GetJingdongInData inData);
}
