package com.gys.business.service.data.mib;

import lombok.Data;

import java.util.List;

/**
 * @Author ：gyx
 * @Date ：Created in 17:07 2021/12/30
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Data
public class MibSXSZHttpOutData {
    private MibSXSZJSFormData formData;

    private List<MibSXSZJSProListData> listData;

}
