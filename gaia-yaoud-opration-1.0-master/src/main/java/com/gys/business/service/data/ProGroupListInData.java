package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2020/9/30
 */
@Data
public class ProGroupListInData implements Serializable {
    private static final long serialVersionUID = -6549956784710780279L;

    @ApiModelProperty(value = "商品组名")
    private String gsecsProGroup;

    @ApiModelProperty(value = "商品组下的 商品id 集合方式")
    private List<String> gspgProIds;
}
