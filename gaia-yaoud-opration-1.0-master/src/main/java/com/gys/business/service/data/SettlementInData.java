package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 诊断信息返回集合
 */
@Data
public class SettlementInData implements Serializable {
    private static final long serialVersionUID = 7421614963224178949L;

    /**
     * 结算ID
     */
    private String setlId;

    /**
     * 就诊ID
     */
    private String mdtrtId;

    /**
     * 人员编号
     */
    private String psnNo;

    /**
     * 人员姓名
     */
    private String psnName;

    /**
     * 人员证件类型
     */
    private String psnCertType;

    /**
     * 证件号码
     */
    private String certno;

    /**
     * 性别
     */
    private String gend;

    /**
     * 民族
     */
    private String naty;

    /**
     * 出生日期
     */
    private String brdy;

    /**
     * 年龄
     */
    private String age;

    /**
     * 险种类型
     */
    private String insutype;

    /**
     * 人员类别
     */
    private String psnType;

    /**
     * 公务员标志
     */
    private String cvlservFlag;

    /**
     * 灵活就业标志
     */
    private String flxempeFlag;

    /**
     * 新生儿标志
     */
    private String newFlag;

    /**
     * 参保机构医保区划
     */
    private String insuOptins;

    /**
     * 单位名称
     */
    private String empName;

    /**
     * 支付地点类别
     */
    private String payLoc;

    /**
     * 定点医药机构编号
     */
    private String fixmedinsCode;

    /**
     * 定点医药机构名称
     */
    private String fixmedinsName;

    /**
     * 医院等级
     */
    private String hospLv;

    /**
     * 定点归属机构
     */
    private String fixmedinsPoolarea;

    /**
     * 限价医院等级
     */
    private String lmtpricHospLv;

    /**
     * 起付线医院等级
     */
    private String dedcHospLv;

    /**
     * 开始日期
     */
    private String begndate;

    /**
     * 结束日期
     */
    private String enddate;

    /**
     * 结算时间
     */
    private String setlTime;

    /**
     * 就诊凭证类型
     */
    private String mdtrtCertType;

    /**
     * 医疗类别
     */
    private String medType;

    /**
     * 清算类别
     */
    private String clrType;

    /**
     * 清算方式
     */
    private String clrWay;

    /**
     * 清算经办机构
     */
    private String clrOptins;

    /**
     * 医疗费总额
     */
    private String medfeeSumamt;

    /**
     * 全自费金额
     */
    private String fulamtOwnpayAmt;

    /**
     * 超限价自费费用
     */
    private String overlmtSelfpay;

    /**
     * 先行自付金额
     */
    private String preselfpayAmt;

    /**
     * 符合政策范围金额
     */
    private String inscpScpAmt;

    /**
     * 实际支付起付线
     */
    private String actPayDedc;

    /**
     * 基本医疗保险统筹基金支出
     */
    private String hifpPay;

    /**
     * 基本医疗保险统筹基金支付比例
     */
    private String poolPropSelfpay;

    /**
     * 公务员医疗补助资金支出
     */
    private String cvlservPay;

    /**
     * 企业补充医疗保险基金支出
     */
    private String hifesPay;

    /**
     * 居民大病保险资金支出
     */
    private String hifmiPay;

    /**
     * 职工大额医疗费用补助基金支出
     */
    private String hifobPay;

    /**
     * 医疗救助基金支出
     */
    private String mafPay;

    /**
     * 其他支出
     */
    private String othPay;

    /**
     * 基金支付总额
     */
    private String fundPaySumamt;

    /**
     * 个人支付金额
     */
    private String psnPay;

    /**
     * 个人账户支出
     */
    private String acctPay;

    /**
     * 现金支付金额
     */
    private String cashPayamt;

    /**
     * 余额
     */
    private String balc;

    /**
     * 个人账户共济支付金额
     */
    private String acctMulaidPay;

    /**
     * 医药机构结算ID
     */
    private String medinsSetlId;

    /**
     * 退费结算标志
     */
    private String refdSetlFlag;

    /**
     * 年度
     */
    private String year;

    /**
     * 病种编码
     */
    private String diseCodg;

    /**
     * 病种名称
     */
    private String diseName;

    /**
     * 发票号
     */
    private String invono;

    /**
     * 经办人ID
     */
    private String opterId;

    /**
     * 经办人姓名
     */
    private String opterName;

    /**
     * 经办时间
     */
    private String optTime;

}
