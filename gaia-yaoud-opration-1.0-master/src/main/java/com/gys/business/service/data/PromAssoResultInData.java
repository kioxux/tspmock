package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromAssoResultInData implements Serializable {
    private static final long serialVersionUID = -5273780234478734744L;
    private String clientId;
    private String gsparVoucherId;
    private String gsparSerial;
    private String gsparProId;
    private String gsparSeriesId;
    private BigDecimal gsparGiftPrc;
    private String gsparGiftRebate;

}
