//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class PromCouponUseOutData {
    private String clientId;
    private String gspcuVoucherId;
    private String gspcuSerial;
    private String gspcuProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private String gspcuActNo;
    private String gspcuReachQty1;
    private BigDecimal gspcuReachAmt1;
    private String gspcuResultQty1;
    private String gspcuReachQty2;
    private BigDecimal gspcuReachAmt2;
    private String gspcuResultQty2;
    private String gspcuReachQty3;
    private BigDecimal gspcuReachAmt3;
    private String gspcuResultQty3;
    private String gspcuMemFlag;
    private String gspcuInteFlag;
    private String gspcuInteRate;

    public PromCouponUseOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspcuVoucherId() {
        return this.gspcuVoucherId;
    }

    public String getGspcuSerial() {
        return this.gspcuSerial;
    }

    public String getGspcuProId() {
        return this.gspcuProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public String getGspcuActNo() {
        return this.gspcuActNo;
    }

    public String getGspcuReachQty1() {
        return this.gspcuReachQty1;
    }

    public BigDecimal getGspcuReachAmt1() {
        return this.gspcuReachAmt1;
    }

    public String getGspcuResultQty1() {
        return this.gspcuResultQty1;
    }

    public String getGspcuReachQty2() {
        return this.gspcuReachQty2;
    }

    public BigDecimal getGspcuReachAmt2() {
        return this.gspcuReachAmt2;
    }

    public String getGspcuResultQty2() {
        return this.gspcuResultQty2;
    }

    public String getGspcuReachQty3() {
        return this.gspcuReachQty3;
    }

    public BigDecimal getGspcuReachAmt3() {
        return this.gspcuReachAmt3;
    }

    public String getGspcuResultQty3() {
        return this.gspcuResultQty3;
    }

    public String getGspcuMemFlag() {
        return this.gspcuMemFlag;
    }

    public String getGspcuInteFlag() {
        return this.gspcuInteFlag;
    }

    public String getGspcuInteRate() {
        return this.gspcuInteRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspcuVoucherId(final String gspcuVoucherId) {
        this.gspcuVoucherId = gspcuVoucherId;
    }

    public void setGspcuSerial(final String gspcuSerial) {
        this.gspcuSerial = gspcuSerial;
    }

    public void setGspcuProId(final String gspcuProId) {
        this.gspcuProId = gspcuProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setGspcuActNo(final String gspcuActNo) {
        this.gspcuActNo = gspcuActNo;
    }

    public void setGspcuReachQty1(final String gspcuReachQty1) {
        this.gspcuReachQty1 = gspcuReachQty1;
    }

    public void setGspcuReachAmt1(final BigDecimal gspcuReachAmt1) {
        this.gspcuReachAmt1 = gspcuReachAmt1;
    }

    public void setGspcuResultQty1(final String gspcuResultQty1) {
        this.gspcuResultQty1 = gspcuResultQty1;
    }

    public void setGspcuReachQty2(final String gspcuReachQty2) {
        this.gspcuReachQty2 = gspcuReachQty2;
    }

    public void setGspcuReachAmt2(final BigDecimal gspcuReachAmt2) {
        this.gspcuReachAmt2 = gspcuReachAmt2;
    }

    public void setGspcuResultQty2(final String gspcuResultQty2) {
        this.gspcuResultQty2 = gspcuResultQty2;
    }

    public void setGspcuReachQty3(final String gspcuReachQty3) {
        this.gspcuReachQty3 = gspcuReachQty3;
    }

    public void setGspcuReachAmt3(final BigDecimal gspcuReachAmt3) {
        this.gspcuReachAmt3 = gspcuReachAmt3;
    }

    public void setGspcuResultQty3(final String gspcuResultQty3) {
        this.gspcuResultQty3 = gspcuResultQty3;
    }

    public void setGspcuMemFlag(final String gspcuMemFlag) {
        this.gspcuMemFlag = gspcuMemFlag;
    }

    public void setGspcuInteFlag(final String gspcuInteFlag) {
        this.gspcuInteFlag = gspcuInteFlag;
    }

    public void setGspcuInteRate(final String gspcuInteRate) {
        this.gspcuInteRate = gspcuInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromCouponUseOutData)) {
            return false;
        } else {
            PromCouponUseOutData other = (PromCouponUseOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label263: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label263;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label263;
                    }

                    return false;
                }

                Object this$gspcuVoucherId = this.getGspcuVoucherId();
                Object other$gspcuVoucherId = other.getGspcuVoucherId();
                if (this$gspcuVoucherId == null) {
                    if (other$gspcuVoucherId != null) {
                        return false;
                    }
                } else if (!this$gspcuVoucherId.equals(other$gspcuVoucherId)) {
                    return false;
                }

                label249: {
                    Object this$gspcuSerial = this.getGspcuSerial();
                    Object other$gspcuSerial = other.getGspcuSerial();
                    if (this$gspcuSerial == null) {
                        if (other$gspcuSerial == null) {
                            break label249;
                        }
                    } else if (this$gspcuSerial.equals(other$gspcuSerial)) {
                        break label249;
                    }

                    return false;
                }

                Object this$gspcuProId = this.getGspcuProId();
                Object other$gspcuProId = other.getGspcuProId();
                if (this$gspcuProId == null) {
                    if (other$gspcuProId != null) {
                        return false;
                    }
                } else if (!this$gspcuProId.equals(other$gspcuProId)) {
                    return false;
                }

                label235: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label235;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label235;
                    }

                    return false;
                }

                Object this$gspgSpecs = this.getGspgSpecs();
                Object other$gspgSpecs = other.getGspgSpecs();
                if (this$gspgSpecs == null) {
                    if (other$gspgSpecs != null) {
                        return false;
                    }
                } else if (!this$gspgSpecs.equals(other$gspgSpecs)) {
                    return false;
                }

                label221: {
                    Object this$proPlace = this.getProPlace();
                    Object other$proPlace = other.getProPlace();
                    if (this$proPlace == null) {
                        if (other$proPlace == null) {
                            break label221;
                        }
                    } else if (this$proPlace.equals(other$proPlace)) {
                        break label221;
                    }

                    return false;
                }

                label214: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label214;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label214;
                    }

                    return false;
                }

                Object this$gspcuActNo = this.getGspcuActNo();
                Object other$gspcuActNo = other.getGspcuActNo();
                if (this$gspcuActNo == null) {
                    if (other$gspcuActNo != null) {
                        return false;
                    }
                } else if (!this$gspcuActNo.equals(other$gspcuActNo)) {
                    return false;
                }

                label200: {
                    Object this$gspcuReachQty1 = this.getGspcuReachQty1();
                    Object other$gspcuReachQty1 = other.getGspcuReachQty1();
                    if (this$gspcuReachQty1 == null) {
                        if (other$gspcuReachQty1 == null) {
                            break label200;
                        }
                    } else if (this$gspcuReachQty1.equals(other$gspcuReachQty1)) {
                        break label200;
                    }

                    return false;
                }

                label193: {
                    Object this$gspcuReachAmt1 = this.getGspcuReachAmt1();
                    Object other$gspcuReachAmt1 = other.getGspcuReachAmt1();
                    if (this$gspcuReachAmt1 == null) {
                        if (other$gspcuReachAmt1 == null) {
                            break label193;
                        }
                    } else if (this$gspcuReachAmt1.equals(other$gspcuReachAmt1)) {
                        break label193;
                    }

                    return false;
                }

                Object this$gspcuResultQty1 = this.getGspcuResultQty1();
                Object other$gspcuResultQty1 = other.getGspcuResultQty1();
                if (this$gspcuResultQty1 == null) {
                    if (other$gspcuResultQty1 != null) {
                        return false;
                    }
                } else if (!this$gspcuResultQty1.equals(other$gspcuResultQty1)) {
                    return false;
                }

                Object this$gspcuReachQty2 = this.getGspcuReachQty2();
                Object other$gspcuReachQty2 = other.getGspcuReachQty2();
                if (this$gspcuReachQty2 == null) {
                    if (other$gspcuReachQty2 != null) {
                        return false;
                    }
                } else if (!this$gspcuReachQty2.equals(other$gspcuReachQty2)) {
                    return false;
                }

                label172: {
                    Object this$gspcuReachAmt2 = this.getGspcuReachAmt2();
                    Object other$gspcuReachAmt2 = other.getGspcuReachAmt2();
                    if (this$gspcuReachAmt2 == null) {
                        if (other$gspcuReachAmt2 == null) {
                            break label172;
                        }
                    } else if (this$gspcuReachAmt2.equals(other$gspcuReachAmt2)) {
                        break label172;
                    }

                    return false;
                }

                Object this$gspcuResultQty2 = this.getGspcuResultQty2();
                Object other$gspcuResultQty2 = other.getGspcuResultQty2();
                if (this$gspcuResultQty2 == null) {
                    if (other$gspcuResultQty2 != null) {
                        return false;
                    }
                } else if (!this$gspcuResultQty2.equals(other$gspcuResultQty2)) {
                    return false;
                }

                Object this$gspcuReachQty3 = this.getGspcuReachQty3();
                Object other$gspcuReachQty3 = other.getGspcuReachQty3();
                if (this$gspcuReachQty3 == null) {
                    if (other$gspcuReachQty3 != null) {
                        return false;
                    }
                } else if (!this$gspcuReachQty3.equals(other$gspcuReachQty3)) {
                    return false;
                }

                label151: {
                    Object this$gspcuReachAmt3 = this.getGspcuReachAmt3();
                    Object other$gspcuReachAmt3 = other.getGspcuReachAmt3();
                    if (this$gspcuReachAmt3 == null) {
                        if (other$gspcuReachAmt3 == null) {
                            break label151;
                        }
                    } else if (this$gspcuReachAmt3.equals(other$gspcuReachAmt3)) {
                        break label151;
                    }

                    return false;
                }

                Object this$gspcuResultQty3 = this.getGspcuResultQty3();
                Object other$gspcuResultQty3 = other.getGspcuResultQty3();
                if (this$gspcuResultQty3 == null) {
                    if (other$gspcuResultQty3 != null) {
                        return false;
                    }
                } else if (!this$gspcuResultQty3.equals(other$gspcuResultQty3)) {
                    return false;
                }

                label137: {
                    Object this$gspcuMemFlag = this.getGspcuMemFlag();
                    Object other$gspcuMemFlag = other.getGspcuMemFlag();
                    if (this$gspcuMemFlag == null) {
                        if (other$gspcuMemFlag == null) {
                            break label137;
                        }
                    } else if (this$gspcuMemFlag.equals(other$gspcuMemFlag)) {
                        break label137;
                    }

                    return false;
                }

                Object this$gspcuInteFlag = this.getGspcuInteFlag();
                Object other$gspcuInteFlag = other.getGspcuInteFlag();
                if (this$gspcuInteFlag == null) {
                    if (other$gspcuInteFlag != null) {
                        return false;
                    }
                } else if (!this$gspcuInteFlag.equals(other$gspcuInteFlag)) {
                    return false;
                }

                Object this$gspcuInteRate = this.getGspcuInteRate();
                Object other$gspcuInteRate = other.getGspcuInteRate();
                if (this$gspcuInteRate == null) {
                    if (other$gspcuInteRate == null) {
                        return true;
                    }
                } else if (this$gspcuInteRate.equals(other$gspcuInteRate)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromCouponUseOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspcuVoucherId = this.getGspcuVoucherId();
        result = result * 59 + ($gspcuVoucherId == null ? 43 : $gspcuVoucherId.hashCode());
        Object $gspcuSerial = this.getGspcuSerial();
        result = result * 59 + ($gspcuSerial == null ? 43 : $gspcuSerial.hashCode());
        Object $gspcuProId = this.getGspcuProId();
        result = result * 59 + ($gspcuProId == null ? 43 : $gspcuProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $gspcuActNo = this.getGspcuActNo();
        result = result * 59 + ($gspcuActNo == null ? 43 : $gspcuActNo.hashCode());
        Object $gspcuReachQty1 = this.getGspcuReachQty1();
        result = result * 59 + ($gspcuReachQty1 == null ? 43 : $gspcuReachQty1.hashCode());
        Object $gspcuReachAmt1 = this.getGspcuReachAmt1();
        result = result * 59 + ($gspcuReachAmt1 == null ? 43 : $gspcuReachAmt1.hashCode());
        Object $gspcuResultQty1 = this.getGspcuResultQty1();
        result = result * 59 + ($gspcuResultQty1 == null ? 43 : $gspcuResultQty1.hashCode());
        Object $gspcuReachQty2 = this.getGspcuReachQty2();
        result = result * 59 + ($gspcuReachQty2 == null ? 43 : $gspcuReachQty2.hashCode());
        Object $gspcuReachAmt2 = this.getGspcuReachAmt2();
        result = result * 59 + ($gspcuReachAmt2 == null ? 43 : $gspcuReachAmt2.hashCode());
        Object $gspcuResultQty2 = this.getGspcuResultQty2();
        result = result * 59 + ($gspcuResultQty2 == null ? 43 : $gspcuResultQty2.hashCode());
        Object $gspcuReachQty3 = this.getGspcuReachQty3();
        result = result * 59 + ($gspcuReachQty3 == null ? 43 : $gspcuReachQty3.hashCode());
        Object $gspcuReachAmt3 = this.getGspcuReachAmt3();
        result = result * 59 + ($gspcuReachAmt3 == null ? 43 : $gspcuReachAmt3.hashCode());
        Object $gspcuResultQty3 = this.getGspcuResultQty3();
        result = result * 59 + ($gspcuResultQty3 == null ? 43 : $gspcuResultQty3.hashCode());
        Object $gspcuMemFlag = this.getGspcuMemFlag();
        result = result * 59 + ($gspcuMemFlag == null ? 43 : $gspcuMemFlag.hashCode());
        Object $gspcuInteFlag = this.getGspcuInteFlag();
        result = result * 59 + ($gspcuInteFlag == null ? 43 : $gspcuInteFlag.hashCode());
        Object $gspcuInteRate = this.getGspcuInteRate();
        result = result * 59 + ($gspcuInteRate == null ? 43 : $gspcuInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "PromCouponUseOutData(clientId=" + this.getClientId() + ", gspcuVoucherId=" + this.getGspcuVoucherId() + ", gspcuSerial=" + this.getGspcuSerial() + ", gspcuProId=" + this.getGspcuProId() + ", gspgProName=" + this.getGspgProName() + ", gspgSpecs=" + this.getGspgSpecs() + ", proPlace=" + this.getProPlace() + ", proPrice=" + this.getProPrice() + ", gspcuActNo=" + this.getGspcuActNo() + ", gspcuReachQty1=" + this.getGspcuReachQty1() + ", gspcuReachAmt1=" + this.getGspcuReachAmt1() + ", gspcuResultQty1=" + this.getGspcuResultQty1() + ", gspcuReachQty2=" + this.getGspcuReachQty2() + ", gspcuReachAmt2=" + this.getGspcuReachAmt2() + ", gspcuResultQty2=" + this.getGspcuResultQty2() + ", gspcuReachQty3=" + this.getGspcuReachQty3() + ", gspcuReachAmt3=" + this.getGspcuReachAmt3() + ", gspcuResultQty3=" + this.getGspcuResultQty3() + ", gspcuMemFlag=" + this.getGspcuMemFlag() + ", gspcuInteFlag=" + this.getGspcuInteFlag() + ", gspcuInteRate=" + this.getGspcuInteRate() + ")";
    }
}
