package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 商品组,品牌组分类
 *
 * @author xiaoyuan on 2020/10/9
 */
@Data
public class Classification implements Serializable {
    private static final long serialVersionUID = -8527969625908132256L;


    @ApiModelProperty(value = "商品分类描述")
    private List<String> proClassName;

    @ApiModelProperty(value = "品牌标识名")
    private List<String> proBrand;
}
