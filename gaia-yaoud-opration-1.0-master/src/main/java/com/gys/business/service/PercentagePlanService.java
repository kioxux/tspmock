package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.service.data.percentageplan.PercentageInData;
import com.gys.business.service.data.percentageplan.PercentageOutData;
import com.gys.business.service.data.percentageplan.PercentageProInData;

import java.util.List;
import java.util.Map;

public interface PercentagePlanService {
    Integer insert(PercentageInData inData);

    PageInfo<PercentageOutData> list(PercentageInData inData);

    PercentageInData tichengDetail(Long planId);

    void deleteSalePlan(Long saleId);

    void deleteProPlan(Long proPlanId);

    void approve(Long planId,String planStatus);

    void deletePlan(Long planId);

    PercentageProInData selectProductByClient(String client,String proCode);

    List<PercentageProInData> getImportExcelDetailList(String clientId, List<Map<String,String>> importInDataList);

    List<Map<String,String>>selectStoList(Long planId);
}
