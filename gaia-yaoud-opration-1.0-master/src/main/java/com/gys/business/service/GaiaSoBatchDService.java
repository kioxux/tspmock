package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSoBatchD;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/10 18:13
 */
public interface GaiaSoBatchDService {

    List<GaiaSoBatchD> selectAll();

    void insertList(List<GaiaSoBatchD> list);

    List<GaiaSoBatchD> selectByOrderId(String soOrderid, String client);
}
