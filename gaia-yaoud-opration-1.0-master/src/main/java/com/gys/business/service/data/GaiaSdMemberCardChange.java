package com.gys.business.service.data;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@ApiModel("会员卡变动表")
public class GaiaSdMemberCardChange implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("加盟商")
    private String client;

    @ApiModelProperty("会员id")
    private String memberId;

    @ApiModelProperty("初始卡号")
    private String initCardId;

    @ApiModelProperty("原卡号")
    private String oldCardId;

    @ApiModelProperty("新卡号")
    private String newCardId;

    @ApiModelProperty("卡类型 1-挂失 2-解挂 3-换卡（注销）")
    private String cardType;

    @ApiModelProperty("操作原因")
    private String remark;

    @ApiModelProperty("操作人员编号")
    private String empId;

    @ApiModelProperty("操作时间")
    private Date updateTime;

    public GaiaSdMemberCardChange(String client) {}
}