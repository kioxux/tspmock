package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class SpecialClassifcationOutData implements Serializable {
    private static final long serialVersionUID = 1552533435654488352L;

    /**
     * 序号
     */
    @ApiModelProperty(value = "序号")
    private Integer index;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;
    /**
     * 销售单号
     */
    @ApiModelProperty(value = "销售单号")
    private String gsscBillNo;

    /**
     * 店号
     */
    @ApiModelProperty(value = "店号")
    private String brId;

    /**
     * 店名
     */
    @NotNull(message = "门店不可为空")
    @ApiModelProperty(value = "门店名称")
    private String gsscBrId;

    /**
     * 销售日期
     */
    @ApiModelProperty(value = "销售日期")
    private String gsscDate;

    @ApiModelProperty(value = "销售编码")
    private String gssdProId;
    /**
     *行号
     */
    @ApiModelProperty(value = "行号")
    private String gsscSerial;


    /**
     * 类别
     */
    @ApiModelProperty(value = "类别")
    private String gsscType;


    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证")
    private String gsscIdcard;

    /**
     * 顾客姓名
     */
    @ApiModelProperty(value = "顾客姓名")
    private String gsscName;

    /**
     * 顾客性别
     */
    @ApiModelProperty(value = "顾客性别")
    private String gsscSex;

    /**
     * 出生年月
     */
    @ApiModelProperty(value = "出生年月")
    private String gsscBirthday;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String gsscMobile;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String gsscAddress;

    /**
     * 购买人监管码
     */
    @ApiModelProperty(value = "购买人监管码")
    private String gsscEcode;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String proName;

    /**
     * 规格
     */
    @ApiModelProperty(value = "规格")
    private String proSpecs;

    /**
     * 销售数量
     */
    @ApiModelProperty(value = "销售数量")
    private String gssdQty;

    /**
     * 批号
     */
    @ApiModelProperty(value = "批号")
    private String gssdBatchNo;

    /**
     * 有效期
     */
    @ApiModelProperty(value = "有效期")
    private String gssdValidDate;

    /**
     * 厂家
     */
    @ApiModelProperty(value = "厂家")
    private String proFactoryName;

    /**
     * 门店编码
     */
    @ApiModelProperty(value = "门店编码")
    private String stoCode;




    @ApiModelProperty(value = "登记人员")
    private String userNam;

}
