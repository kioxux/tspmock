package com.gys.business.service;

import com.gys.business.mapper.entity.*;
import com.gys.business.service.data.*;
import com.gys.common.data.CommonData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import java.util.List;
import java.util.Map;

public interface SalesReceiptsService {
    GetQueryMemberOutData queryMember(GetQueryMemberInData inData);

    void givePoint(GetQueryMemberInData inData);

    List<GetQueryProductOutData> queryProduct(GetQueryProductInData inData);

    GetSalesReceiptsTableOutData queryProductDetail(GetQueryProductInData inData);

    List<GetPdAndExpOutData> queryBatchNoAndExp(GetQueryProductInData inData);

    List<GetPdAndExpOutData> queryStockAndExpGroupByBatchNo(GetQueryProductInData inData);

    List<GetPdAndExpOutData> queryStockAndExpGroupByThird(GetQueryProductInData inData);

    GetPointExchangeOutData pointExchange(GetQueryProductInData inData);

    List<GetEmpOutData> queryEmp(GetLoginOutData inData);

    List<GetEmpOutData> queryAllEmp(GetLoginOutData userInfo, CommonData content);

    List<GetDoctorOutData> queryDoctor(GetLoginOutData inData);

    List<GetDoctorOutData> queryDoctorByClient(Map map);

    List<GetDoctorOutData> queryDruggist(GetLoginOutData inData);

    List<GetPromByProCodeOutData> promByProCode(GetQueryProductInData inData);

    GetPromByProCodeOutData memByProCode(GetQueryProductInData inData);

    List<GetGiftCardOutData> giftCardByProCode(GetQueryProductInData inData);

    void createSale(GetCreateSaleOutData inData);

    GetDisPriceOutData getDisPrice(GetDisPriceInData inData);

    void saveRecipelInfo(RecipelInfoInputData recipelInfoInputData);

    void updateRecipelInfo(RecipelInfoInputData recipelInfoInputData);

    List<RestOrderInfoOutData> getRestOrder2(GetLoginOutData inData, Map<String, String> map);

    List<GetRestOrderInfoOutData> getRestOrder(GaiaSdSaleH inData);

    List<GetRestOrderDetailOutData> getRestOrderDetail(GaiaSdSaleD inData);

    String afterSplitePrice(GaiaRetailPriceInData inData);

    String getStoreDiscount(GaiaRetailPrice inData);

    String getStoreDyqMaxAmount(GaiaRetailPrice inData);

    void approveChangePrice(ApproveChangePriceInData inData, String token);

    void approveChangeRate(ApproveChangePriceInData inData, String token);

    void approveChangeAllRate(ApproveChangePriceInData inData, String token);

    int checkPrintBill(CheckParamData inData);

    List<GaiaStoreData> otherShops(GetLoginOutData userInfo);

    void giveGiftCard(List<GaiaSdPromCouponBasic> inData);

    List<GetGiftOutData> giftSelect(GetQueryProductInData inData);

    GaiaSdIntegralAddSet getPointSet(GetLoginOutData userInfo);

    GaiaSdMemberClass getMemberPointSet(GetQueryProductInData userInfo);


    /**
     * 查询单品的促销阶段数量
     *
     * @param inData
     * @return
     */
    NumberOfPromotionalMethodsData findSingleProductPromotionByCode(PromotionMethodInData inData);

    /**
     * 查询符合条件的活动
     *
     * @param inData
     * @return
     */
    List<PromotionalConditionData> findProductPromotionByCode(PromotionMethodInData inData);

    /**
     * 查询符合条件的活动（改版）
     *
     * @param inData
     * @return
     */
    List<PromotionalConditionData> findProductPromotionByCode2(PromotionMethodInData inData);

    /**
     * 根据促销单号查询单品信息
     *
     * @return
     */
    List<GetDisPriceOutData> findPromotionById(List<FindPromotionInData> inData);

    /**
     * 根据促销单号和参数查询所有符合条件的促销活动
     *
     * @return 所有符合条件的促销活动
     */
    List<PromotionalConditionData> findProductPromotionByIdAndArgs(List<FindPromotionInData> inData);

    /**
     * 根据商品列表，查询出其所有的系列商品对应编码
     *
     * @param inData
     * @return
     */
    List<GaiaSdPromSeriesConds> findSeriesCondsByRebornList(PromotionMethodInData inData);

    /**
     * 根据商品查询有无积分兑换活动
     *
     * @param inData
     * @return
     */
    List<GaiaSdIntegralExchangeSet> findIntegralExchangeByProCode(IntegralExchangeInData inData);

    /**
     * 查询会员类促销
     *
     * @param inData
     * @return
     */
    List<MemberPromotionOutData> findMemberPromotion(MemberPromotionInData inData);

    /**
     * 验证总库存表
     *
     * @param inData
     * @return
     */
    GaiaSdStock verifyInventory(MemberPromotionInData inData);

    /**
     * 查询当前会员信息
     *
     * @param inData
     * @return
     */
    CurrentMembersOutData checkCurrentMembers(GaiaSdSaleD inData);

    /**
     * 营业员报表
     *
     * @param inData
     * @return
     */
    HandoverReportOutData handoverReport(GaiaRetailPriceInData inData);


    /**
     * 商品编码集合验证库存
     *
     * @param inData
     * @return
     */
    List<GaiaSdStock> getAllByProIds(MemberPromotionInData inData);

    /**
     * 商品挂单
     *
     * @param inData
     */
    void createSalePendingOrder(GetCreateSaleOutData inData);

    /**
     * 获取储值卡信息
     *
     * @param inData
     * @return
     */
    GaiaSdRechargeCard getRechargeByCardId(MemberPromotionInData inData);

    List<GetEmpOutData> pharmacist(GetLoginOutData userInfo);

    /**
     * 删除挂单信息
     *
     * @param userInfo
     * @param map
     */
    void deletePendingOrder(GetLoginOutData userInfo, Map<String, String> map);

    /**
     * 获取商品成本价
     *
     * @param userInfo
     * @param map
     * @return
     */
    CommodityCostDto commodityCost(GetLoginOutData userInfo, Map<String, String> map);

    /**
     * 新增挂单请求
     *
     * @param userInfo
     * @param inData
     */
    void createSaleHand(GetLoginOutData userInfo, GetCreateSaleOutData inData);

    List<GetRestOrderInfoOutData> getSaleHand(GetLoginOutData userInfo, Map<String, String> map);

    List<RestOrderDetailOutData> getSaleHandDetail(GetLoginOutData userInfo, Map<String, String> map);

    /**
     * 查询加盟商下会员 所有的购买记录
     *
     * @param client
     * @param hykNo
     * @return
     */
    List<MemberPurchaseRecordOutData> getClientPurchaseRecordsByMember(String client, String hykNo);

    /**
     * 查询小票打印明细
     *
     * @param map
     * @return
     */
    List<RestOrderDetailDto> getPrintDetails(Map<String, String> map);


    /**
     * 查询小票打印明细
     *
     * @param map
     * @return
     */
    RestOrderChangeAll getPrintAll(Map<String, String> map);

    /**
     * 挂单作废
     *
     * @param userInfo
     * @param map
     */
    void updateHandSaleH(GetLoginOutData userInfo, Map<String, String> map);

    /**
     * 查询所有电子券信息
     *
     * @param map
     * @return
     */
    List<GaiaSdElectronChange> getGiveAndUseAll(Map<String, String> map);

    List<GetRestOrderInfoOutData> listSaleHand(GetLoginOutData userInfo, Map<String, String> map);

    List<RestOrderDetailOutData> listRestOrderDetail(GetLoginOutData userInfo, Map<String, String> map);

    void orderCommit(Map<String, String> map);

    /**
     * 获取His挂单数据
     * @param userInfo
     * @param map
     * @return
     */
    List<GetRestOrderInfoOutData> getHisHand(GetLoginOutData userInfo, Map<String, String> map);


    JsonResult updateSaleHandStatus(GetLoginOutData userInfo, Map<String, String> map);

    JsonResult getSite(GetLoginOutData userInfo);

    JsonResult updateAndReturnStatus(GetLoginOutData userInfo, Map<String, String> map);

    /**
     * 会员查询
     * @param map
     * @param userInfo
     * @return
     */
    List<GetQueryMemberOutData> memberOfTheQuery(Map<String, Object> map, GetLoginOutData userInfo);
}
