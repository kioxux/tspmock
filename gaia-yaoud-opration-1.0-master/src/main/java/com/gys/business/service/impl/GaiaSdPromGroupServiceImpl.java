package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdPromGroupMapper;
import com.gys.business.mapper.entity.GaiaSdPromGroup;
import com.gys.business.service.GaiaSdPromGroupService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author csm
 * @date 2021/12/15 - 13:38
 */
@Service
public class GaiaSdPromGroupServiceImpl implements GaiaSdPromGroupService {
    @Resource
    private GaiaSdPromGroupMapper gaiaSdPromGroupMapper;
    @Override
    public List<GaiaSdPromGroup> selectAll(String client) {
        List<GaiaSdPromGroup> gaiaSdPromGroups = gaiaSdPromGroupMapper.selectAll(client);
        if(CollectionUtils.isEmpty(gaiaSdPromGroups)){
            return new ArrayList<>();
        }
        return gaiaSdPromGroups;
    }
}
