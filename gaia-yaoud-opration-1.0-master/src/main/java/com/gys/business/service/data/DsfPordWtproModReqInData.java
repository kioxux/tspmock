package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
public class DsfPordWtproModReqInData {

    /**
     * 委托配送第三方商品编码对照表主键编号
     */
    private Long id;

    /**
     * 第三方商品名
     */
    private String dsfProName;

    /**
     * 第三方编号
     */
    @NotNull(message = "第三方编号不能为空")
    @NotEmpty(message = "第三方编号不能为空")
    private String idDsf;


    /**
     * 转换比率
     */
   @NotNull(message = "转换比例不能为空")
    private BigDecimal proRate;

    /**
     * 商品编码
     */
    @NotNull(message = "商品编码不能为空")
    @NotEmpty(message = "商品编码不能为空")
    private String proSelfCode;


}
