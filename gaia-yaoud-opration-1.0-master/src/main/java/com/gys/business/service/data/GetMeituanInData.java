//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetMeituanInData {
    private String storeCode;
    private Long orderId;
    private String reason;
    private String reasonCode;

    public GetMeituanInData() {
    }

    public String getStoreCode() {
        return this.storeCode;
    }

    public Long getOrderId() {
        return this.orderId;
    }

    public String getReason() {
        return this.reason;
    }

    public String getReasonCode() {
        return this.reasonCode;
    }

    public void setStoreCode(final String storeCode) {
        this.storeCode = storeCode;
    }

    public void setOrderId(final Long orderId) {
        this.orderId = orderId;
    }

    public void setReason(final String reason) {
        this.reason = reason;
    }

    public void setReasonCode(final String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetMeituanInData)) {
            return false;
        } else {
            GetMeituanInData other = (GetMeituanInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$storeCode = this.getStoreCode();
                    Object other$storeCode = other.getStoreCode();
                    if (this$storeCode == null) {
                        if (other$storeCode == null) {
                            break label59;
                        }
                    } else if (this$storeCode.equals(other$storeCode)) {
                        break label59;
                    }

                    return false;
                }

                Object this$orderId = this.getOrderId();
                Object other$orderId = other.getOrderId();
                if (this$orderId == null) {
                    if (other$orderId != null) {
                        return false;
                    }
                } else if (!this$orderId.equals(other$orderId)) {
                    return false;
                }

                Object this$reason = this.getReason();
                Object other$reason = other.getReason();
                if (this$reason == null) {
                    if (other$reason != null) {
                        return false;
                    }
                } else if (!this$reason.equals(other$reason)) {
                    return false;
                }

                Object this$reasonCode = this.getReasonCode();
                Object other$reasonCode = other.getReasonCode();
                if (this$reasonCode == null) {
                    if (other$reasonCode != null) {
                        return false;
                    }
                } else if (!this$reasonCode.equals(other$reasonCode)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetMeituanInData;
    }

    public int hashCode() {

        int result = 1;
        Object $storeCode = this.getStoreCode();
        result = result * 59 + ($storeCode == null ? 43 : $storeCode.hashCode());
        Object $orderId = this.getOrderId();
        result = result * 59 + ($orderId == null ? 43 : $orderId.hashCode());
        Object $reason = this.getReason();
        result = result * 59 + ($reason == null ? 43 : $reason.hashCode());
        Object $reasonCode = this.getReasonCode();
        result = result * 59 + ($reasonCode == null ? 43 : $reasonCode.hashCode());
        return result;
    }

    public String toString() {
        return "GetMeituanInData(storeCode=" + this.getStoreCode() + ", orderId=" + this.getOrderId() + ", reason=" + this.getReason() + ", reasonCode=" + this.getReasonCode() + ")";
    }
}
