package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年11月11日 下午4:55
 */
@Data
public class SaleSettingOutData implements Serializable {
    private static final long serialVersionUID = -273918284507394649L;

    @ApiModelProperty(value = "方案id")
    private Integer planId;

    @ApiModelProperty(value = "提成类型 1 销售提成 2 单品提成")
    private String planType;

    @ApiModelProperty(value = "每个设置的id，如果是销售提成那么值为null或者0")
    private Integer id;

    @ApiModelProperty(value = "提成方式: 0 按销售额提成 1 按毛利额提成")
    private String planAmtWay;

    @ApiModelProperty(value = "提成方式:0 按商品毛利率  1 按销售毛利率")
    private String planRateWay;

    @ApiModelProperty(value = "负毛利率商品是否不参与销售提成 0 是 1 否")
    private String planIfNegative;

    @ApiModelProperty(value = "销售提成明细列表")
    private List<PercentageSaleInData> saleInDataList;

    @ApiModelProperty(value = "剔除折扣率 操作符号 =、＞、＞＝、＜、＜＝")
    private String planRejectDiscountRateSymbol;

    @ApiModelProperty(value = "剔除折扣率 数值")
    private String planRejectDiscountRate;

    @ApiModelProperty(value = "剔除商品分类")
    private String[][] classArr;

    @ApiModelProperty(value = "剔除商品设置：0 否 1 是")
    private String planRejectPro;

    @ApiModelProperty(value = "剔除商品列表")
    private List<PercentageSalePlanRemoveProData> rejectProList;

    @ApiModelProperty(value = "子方案名称")
    private String cPlanName;


}

