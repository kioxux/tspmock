package com.gys.business.service.data.integralExchange;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class IntegralMessageOutData {
    private String clientId;//加盟商编码
    private String gsicVoucherId;//积分抵现活动单号
    private String gsicPlanId;//积分抵现规则代号
    private String gsicFrequencyType;//频率类型 1:日期，2:星期
    private String gsicProRange;//抵用商品范围 0-所有，1-部分
    private String gsicsSto;//门店
    private String gsicsType;//类型 0-单次固定，1-最优
    private String gsicsAmtLimitFlag;//抵用金额上限控制 0-无，1-有
    private String gsicsLimitRate;//抵扣金额比例
    private String gsicsAmtOffsetMax;//单笔最大抵现金额
    private String gsicsIntegralAddSet;//单笔交易是否积分 0-否，1-是
    private String gsicsCode;//规则内容代号
    private BigDecimal integral;//会员卡可用积分
}
