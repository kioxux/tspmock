package com.gys.business.service;

import com.gys.business.service.data.GetGspExpConfInData;
import com.gys.business.service.data.GspExpConfData;
import java.util.List;

public interface GspExpConfService {
    /**
     * 保存预警天数配置
     * @param inData
     * @return
     */
    Integer saveGspExpConf(GetGspExpConfInData inData);

    /**
     * 查询预警天数
     * @param inData
     * @return
     */
    List<GspExpConfData> selectGspExpConf(GspExpConfData inData);
}
