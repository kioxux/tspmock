//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.PageInfo;

import javax.validation.Valid;
import java.util.List;

public interface ProductSortService {
    List<GetProductSortOutData> querySort(GetProductInData inData);

    List<GetProductDetailOutData> queryDetail(GetProductInData inData);

    List<GetQueryProVoOutData> queryProVo(GetProductInData inData);

    PageInfo<GetQueryProVoOutData> queryPro(GetProductInData inData);

    List<GetQueryProVoOutData> queryPrice(GetProductInData inData);

    void add(GetProductInData inData);

    void update(GetProductInData inData);

    void importExcel(GetProImportExcelInData inData);

    void delete(List<GetProductInData> inData);

    List<GetProductSortOutData> selectProGroupByClient(GetProductInData inData);

    List<GetReplenishDetailOutData> queryProList(GetProductInData inData);

    List<GetProductNewOutData> queryProNew(GetProductNewInData inData);

    List<GetProductNewOutData> queryProConditionNew(GetProductNewInData inData);

    PageInfo queryProThirdly(GetProductThirdlyInData inData);

    PageInfo queryProFourthly(GetProductThirdlyInData inData);

    PageInfo queryProFourthlyForTC(GetProductThirdlyInForTCData inData);

    PageInfo queryProFourthlyS(GetProductThirdlyInData inData);//5.0

    PageInfo queryProForColdChain(GetProductThirdlyInData inData);

    List<ProductTitleSettings> productTitleSetting(@Valid GetProductThirdlyInData inData);

    PageInfo queryBasicProduct(GetProductThirdlyInData inData);
}
