package com.gys.business.service;

import com.gys.business.service.data.GetServerInData;
import com.gys.business.service.data.GetServerOutData;

public interface ServerService {
    GetServerOutData detail(GetServerInData inData);

    void save(GetServerInData inData);

}
