package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class StoreStockCountOutData implements Serializable {

    @ApiModelProperty(value = "门店编码")
    private String storeId;

    @ApiModelProperty(value = "门店名称")
    private String storeName;

    @ApiModelProperty(value = "店效级别")
    private Integer storeLevel;

    @ApiModelProperty(value = "店效级别描述")
    private String storeLevelDesc;

    @ApiModelProperty(value = "门店属性编码")
    private String storeAttributeCode;

    @ApiModelProperty(value = "门店属性")
    private String storeAttribute;

    @ApiModelProperty(value = "省市编码拼接字符串")
    private String provCityCode;
}
