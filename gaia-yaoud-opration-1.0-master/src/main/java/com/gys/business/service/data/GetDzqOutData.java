//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class GetDzqOutData {
    private String clientId;
    private String gspcbCouponId;
    private String gspcbActNo;
    private String gspcbName;
    private String gspcbDesc;
    private String gspcbEndDate;
    private String gspcbCount;
    private BigDecimal gspcbCouponAmt;
    private String proCode;
    private String reachAmount1;
    private String reachAmount2;
    private String reachAmount3;
    private String reachQty1;
    private String reachQty2;
    private String reachQty3;
    private String resultQty1;
    private String resultQty2;
    private String resultQty3;
    private String gspcbMemberId;
    private String gspcbGainFlag;
    private String gspcbGrantBillNo;
    private String gspcbGrantBrId;
    private String gspcbGrantDate;
    private String gspcbUseBillNo;
    private String gspcbUseBrId;
    private String gspcbUseDate;
    private String gspcbStatus;

    public GetDzqOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspcbCouponId() {
        return this.gspcbCouponId;
    }

    public String getGspcbActNo() {
        return this.gspcbActNo;
    }

    public String getGspcbName() {
        return this.gspcbName;
    }

    public String getGspcbDesc() {
        return this.gspcbDesc;
    }

    public String getGspcbEndDate() {
        return this.gspcbEndDate;
    }

    public String getGspcbCount() {
        return this.gspcbCount;
    }

    public BigDecimal getGspcbCouponAmt() {
        return this.gspcbCouponAmt;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getReachAmount1() {
        return this.reachAmount1;
    }

    public String getReachAmount2() {
        return this.reachAmount2;
    }

    public String getReachAmount3() {
        return this.reachAmount3;
    }

    public String getReachQty1() {
        return this.reachQty1;
    }

    public String getReachQty2() {
        return this.reachQty2;
    }

    public String getReachQty3() {
        return this.reachQty3;
    }

    public String getResultQty1() {
        return this.resultQty1;
    }

    public String getResultQty2() {
        return this.resultQty2;
    }

    public String getResultQty3() {
        return this.resultQty3;
    }

    public String getGspcbMemberId() {
        return this.gspcbMemberId;
    }

    public String getGspcbGainFlag() {
        return this.gspcbGainFlag;
    }

    public String getGspcbGrantBillNo() {
        return this.gspcbGrantBillNo;
    }

    public String getGspcbGrantBrId() {
        return this.gspcbGrantBrId;
    }

    public String getGspcbGrantDate() {
        return this.gspcbGrantDate;
    }

    public String getGspcbUseBillNo() {
        return this.gspcbUseBillNo;
    }

    public String getGspcbUseBrId() {
        return this.gspcbUseBrId;
    }

    public String getGspcbUseDate() {
        return this.gspcbUseDate;
    }

    public String getGspcbStatus() {
        return this.gspcbStatus;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspcbCouponId(final String gspcbCouponId) {
        this.gspcbCouponId = gspcbCouponId;
    }

    public void setGspcbActNo(final String gspcbActNo) {
        this.gspcbActNo = gspcbActNo;
    }

    public void setGspcbName(final String gspcbName) {
        this.gspcbName = gspcbName;
    }

    public void setGspcbDesc(final String gspcbDesc) {
        this.gspcbDesc = gspcbDesc;
    }

    public void setGspcbEndDate(final String gspcbEndDate) {
        this.gspcbEndDate = gspcbEndDate;
    }

    public void setGspcbCount(final String gspcbCount) {
        this.gspcbCount = gspcbCount;
    }

    public void setGspcbCouponAmt(final BigDecimal gspcbCouponAmt) {
        this.gspcbCouponAmt = gspcbCouponAmt;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setReachAmount1(final String reachAmount1) {
        this.reachAmount1 = reachAmount1;
    }

    public void setReachAmount2(final String reachAmount2) {
        this.reachAmount2 = reachAmount2;
    }

    public void setReachAmount3(final String reachAmount3) {
        this.reachAmount3 = reachAmount3;
    }

    public void setReachQty1(final String reachQty1) {
        this.reachQty1 = reachQty1;
    }

    public void setReachQty2(final String reachQty2) {
        this.reachQty2 = reachQty2;
    }

    public void setReachQty3(final String reachQty3) {
        this.reachQty3 = reachQty3;
    }

    public void setResultQty1(final String resultQty1) {
        this.resultQty1 = resultQty1;
    }

    public void setResultQty2(final String resultQty2) {
        this.resultQty2 = resultQty2;
    }

    public void setResultQty3(final String resultQty3) {
        this.resultQty3 = resultQty3;
    }

    public void setGspcbMemberId(final String gspcbMemberId) {
        this.gspcbMemberId = gspcbMemberId;
    }

    public void setGspcbGainFlag(final String gspcbGainFlag) {
        this.gspcbGainFlag = gspcbGainFlag;
    }

    public void setGspcbGrantBillNo(final String gspcbGrantBillNo) {
        this.gspcbGrantBillNo = gspcbGrantBillNo;
    }

    public void setGspcbGrantBrId(final String gspcbGrantBrId) {
        this.gspcbGrantBrId = gspcbGrantBrId;
    }

    public void setGspcbGrantDate(final String gspcbGrantDate) {
        this.gspcbGrantDate = gspcbGrantDate;
    }

    public void setGspcbUseBillNo(final String gspcbUseBillNo) {
        this.gspcbUseBillNo = gspcbUseBillNo;
    }

    public void setGspcbUseBrId(final String gspcbUseBrId) {
        this.gspcbUseBrId = gspcbUseBrId;
    }

    public void setGspcbUseDate(final String gspcbUseDate) {
        this.gspcbUseDate = gspcbUseDate;
    }

    public void setGspcbStatus(final String gspcbStatus) {
        this.gspcbStatus = gspcbStatus;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetDzqOutData)) {
            return false;
        } else {
            GetDzqOutData other = (GetDzqOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label335: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label335;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label335;
                    }

                    return false;
                }

                Object this$gspcbCouponId = this.getGspcbCouponId();
                Object other$gspcbCouponId = other.getGspcbCouponId();
                if (this$gspcbCouponId == null) {
                    if (other$gspcbCouponId != null) {
                        return false;
                    }
                } else if (!this$gspcbCouponId.equals(other$gspcbCouponId)) {
                    return false;
                }

                Object this$gspcbActNo = this.getGspcbActNo();
                Object other$gspcbActNo = other.getGspcbActNo();
                if (this$gspcbActNo == null) {
                    if (other$gspcbActNo != null) {
                        return false;
                    }
                } else if (!this$gspcbActNo.equals(other$gspcbActNo)) {
                    return false;
                }

                label314: {
                    Object this$gspcbName = this.getGspcbName();
                    Object other$gspcbName = other.getGspcbName();
                    if (this$gspcbName == null) {
                        if (other$gspcbName == null) {
                            break label314;
                        }
                    } else if (this$gspcbName.equals(other$gspcbName)) {
                        break label314;
                    }

                    return false;
                }

                label307: {
                    Object this$gspcbDesc = this.getGspcbDesc();
                    Object other$gspcbDesc = other.getGspcbDesc();
                    if (this$gspcbDesc == null) {
                        if (other$gspcbDesc == null) {
                            break label307;
                        }
                    } else if (this$gspcbDesc.equals(other$gspcbDesc)) {
                        break label307;
                    }

                    return false;
                }

                Object this$gspcbEndDate = this.getGspcbEndDate();
                Object other$gspcbEndDate = other.getGspcbEndDate();
                if (this$gspcbEndDate == null) {
                    if (other$gspcbEndDate != null) {
                        return false;
                    }
                } else if (!this$gspcbEndDate.equals(other$gspcbEndDate)) {
                    return false;
                }

                Object this$gspcbCount = this.getGspcbCount();
                Object other$gspcbCount = other.getGspcbCount();
                if (this$gspcbCount == null) {
                    if (other$gspcbCount != null) {
                        return false;
                    }
                } else if (!this$gspcbCount.equals(other$gspcbCount)) {
                    return false;
                }

                label286: {
                    Object this$gspcbCouponAmt = this.getGspcbCouponAmt();
                    Object other$gspcbCouponAmt = other.getGspcbCouponAmt();
                    if (this$gspcbCouponAmt == null) {
                        if (other$gspcbCouponAmt == null) {
                            break label286;
                        }
                    } else if (this$gspcbCouponAmt.equals(other$gspcbCouponAmt)) {
                        break label286;
                    }

                    return false;
                }

                label279: {
                    Object this$proCode = this.getProCode();
                    Object other$proCode = other.getProCode();
                    if (this$proCode == null) {
                        if (other$proCode == null) {
                            break label279;
                        }
                    } else if (this$proCode.equals(other$proCode)) {
                        break label279;
                    }

                    return false;
                }

                Object this$reachAmount1 = this.getReachAmount1();
                Object other$reachAmount1 = other.getReachAmount1();
                if (this$reachAmount1 == null) {
                    if (other$reachAmount1 != null) {
                        return false;
                    }
                } else if (!this$reachAmount1.equals(other$reachAmount1)) {
                    return false;
                }

                label265: {
                    Object this$reachAmount2 = this.getReachAmount2();
                    Object other$reachAmount2 = other.getReachAmount2();
                    if (this$reachAmount2 == null) {
                        if (other$reachAmount2 == null) {
                            break label265;
                        }
                    } else if (this$reachAmount2.equals(other$reachAmount2)) {
                        break label265;
                    }

                    return false;
                }

                Object this$reachAmount3 = this.getReachAmount3();
                Object other$reachAmount3 = other.getReachAmount3();
                if (this$reachAmount3 == null) {
                    if (other$reachAmount3 != null) {
                        return false;
                    }
                } else if (!this$reachAmount3.equals(other$reachAmount3)) {
                    return false;
                }

                label251: {
                    Object this$reachQty1 = this.getReachQty1();
                    Object other$reachQty1 = other.getReachQty1();
                    if (this$reachQty1 == null) {
                        if (other$reachQty1 == null) {
                            break label251;
                        }
                    } else if (this$reachQty1.equals(other$reachQty1)) {
                        break label251;
                    }

                    return false;
                }

                Object this$reachQty2 = this.getReachQty2();
                Object other$reachQty2 = other.getReachQty2();
                if (this$reachQty2 == null) {
                    if (other$reachQty2 != null) {
                        return false;
                    }
                } else if (!this$reachQty2.equals(other$reachQty2)) {
                    return false;
                }

                Object this$reachQty3 = this.getReachQty3();
                Object other$reachQty3 = other.getReachQty3();
                if (this$reachQty3 == null) {
                    if (other$reachQty3 != null) {
                        return false;
                    }
                } else if (!this$reachQty3.equals(other$reachQty3)) {
                    return false;
                }

                label230: {
                    Object this$resultQty1 = this.getResultQty1();
                    Object other$resultQty1 = other.getResultQty1();
                    if (this$resultQty1 == null) {
                        if (other$resultQty1 == null) {
                            break label230;
                        }
                    } else if (this$resultQty1.equals(other$resultQty1)) {
                        break label230;
                    }

                    return false;
                }

                label223: {
                    Object this$resultQty2 = this.getResultQty2();
                    Object other$resultQty2 = other.getResultQty2();
                    if (this$resultQty2 == null) {
                        if (other$resultQty2 == null) {
                            break label223;
                        }
                    } else if (this$resultQty2.equals(other$resultQty2)) {
                        break label223;
                    }

                    return false;
                }

                Object this$resultQty3 = this.getResultQty3();
                Object other$resultQty3 = other.getResultQty3();
                if (this$resultQty3 == null) {
                    if (other$resultQty3 != null) {
                        return false;
                    }
                } else if (!this$resultQty3.equals(other$resultQty3)) {
                    return false;
                }

                Object this$gspcbMemberId = this.getGspcbMemberId();
                Object other$gspcbMemberId = other.getGspcbMemberId();
                if (this$gspcbMemberId == null) {
                    if (other$gspcbMemberId != null) {
                        return false;
                    }
                } else if (!this$gspcbMemberId.equals(other$gspcbMemberId)) {
                    return false;
                }

                label202: {
                    Object this$gspcbGainFlag = this.getGspcbGainFlag();
                    Object other$gspcbGainFlag = other.getGspcbGainFlag();
                    if (this$gspcbGainFlag == null) {
                        if (other$gspcbGainFlag == null) {
                            break label202;
                        }
                    } else if (this$gspcbGainFlag.equals(other$gspcbGainFlag)) {
                        break label202;
                    }

                    return false;
                }

                label195: {
                    Object this$gspcbGrantBillNo = this.getGspcbGrantBillNo();
                    Object other$gspcbGrantBillNo = other.getGspcbGrantBillNo();
                    if (this$gspcbGrantBillNo == null) {
                        if (other$gspcbGrantBillNo == null) {
                            break label195;
                        }
                    } else if (this$gspcbGrantBillNo.equals(other$gspcbGrantBillNo)) {
                        break label195;
                    }

                    return false;
                }

                Object this$gspcbGrantBrId = this.getGspcbGrantBrId();
                Object other$gspcbGrantBrId = other.getGspcbGrantBrId();
                if (this$gspcbGrantBrId == null) {
                    if (other$gspcbGrantBrId != null) {
                        return false;
                    }
                } else if (!this$gspcbGrantBrId.equals(other$gspcbGrantBrId)) {
                    return false;
                }

                Object this$gspcbGrantDate = this.getGspcbGrantDate();
                Object other$gspcbGrantDate = other.getGspcbGrantDate();
                if (this$gspcbGrantDate == null) {
                    if (other$gspcbGrantDate != null) {
                        return false;
                    }
                } else if (!this$gspcbGrantDate.equals(other$gspcbGrantDate)) {
                    return false;
                }

                label174: {
                    Object this$gspcbUseBillNo = this.getGspcbUseBillNo();
                    Object other$gspcbUseBillNo = other.getGspcbUseBillNo();
                    if (this$gspcbUseBillNo == null) {
                        if (other$gspcbUseBillNo == null) {
                            break label174;
                        }
                    } else if (this$gspcbUseBillNo.equals(other$gspcbUseBillNo)) {
                        break label174;
                    }

                    return false;
                }

                label167: {
                    Object this$gspcbUseBrId = this.getGspcbUseBrId();
                    Object other$gspcbUseBrId = other.getGspcbUseBrId();
                    if (this$gspcbUseBrId == null) {
                        if (other$gspcbUseBrId == null) {
                            break label167;
                        }
                    } else if (this$gspcbUseBrId.equals(other$gspcbUseBrId)) {
                        break label167;
                    }

                    return false;
                }

                Object this$gspcbUseDate = this.getGspcbUseDate();
                Object other$gspcbUseDate = other.getGspcbUseDate();
                if (this$gspcbUseDate == null) {
                    if (other$gspcbUseDate != null) {
                        return false;
                    }
                } else if (!this$gspcbUseDate.equals(other$gspcbUseDate)) {
                    return false;
                }

                Object this$gspcbStatus = this.getGspcbStatus();
                Object other$gspcbStatus = other.getGspcbStatus();
                if (this$gspcbStatus == null) {
                    if (other$gspcbStatus != null) {
                        return false;
                    }
                } else if (!this$gspcbStatus.equals(other$gspcbStatus)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetDzqOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspcbCouponId = this.getGspcbCouponId();
        result = result * 59 + ($gspcbCouponId == null ? 43 : $gspcbCouponId.hashCode());
        Object $gspcbActNo = this.getGspcbActNo();
        result = result * 59 + ($gspcbActNo == null ? 43 : $gspcbActNo.hashCode());
        Object $gspcbName = this.getGspcbName();
        result = result * 59 + ($gspcbName == null ? 43 : $gspcbName.hashCode());
        Object $gspcbDesc = this.getGspcbDesc();
        result = result * 59 + ($gspcbDesc == null ? 43 : $gspcbDesc.hashCode());
        Object $gspcbEndDate = this.getGspcbEndDate();
        result = result * 59 + ($gspcbEndDate == null ? 43 : $gspcbEndDate.hashCode());
        Object $gspcbCount = this.getGspcbCount();
        result = result * 59 + ($gspcbCount == null ? 43 : $gspcbCount.hashCode());
        Object $gspcbCouponAmt = this.getGspcbCouponAmt();
        result = result * 59 + ($gspcbCouponAmt == null ? 43 : $gspcbCouponAmt.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $reachAmount1 = this.getReachAmount1();
        result = result * 59 + ($reachAmount1 == null ? 43 : $reachAmount1.hashCode());
        Object $reachAmount2 = this.getReachAmount2();
        result = result * 59 + ($reachAmount2 == null ? 43 : $reachAmount2.hashCode());
        Object $reachAmount3 = this.getReachAmount3();
        result = result * 59 + ($reachAmount3 == null ? 43 : $reachAmount3.hashCode());
        Object $reachQty1 = this.getReachQty1();
        result = result * 59 + ($reachQty1 == null ? 43 : $reachQty1.hashCode());
        Object $reachQty2 = this.getReachQty2();
        result = result * 59 + ($reachQty2 == null ? 43 : $reachQty2.hashCode());
        Object $reachQty3 = this.getReachQty3();
        result = result * 59 + ($reachQty3 == null ? 43 : $reachQty3.hashCode());
        Object $resultQty1 = this.getResultQty1();
        result = result * 59 + ($resultQty1 == null ? 43 : $resultQty1.hashCode());
        Object $resultQty2 = this.getResultQty2();
        result = result * 59 + ($resultQty2 == null ? 43 : $resultQty2.hashCode());
        Object $resultQty3 = this.getResultQty3();
        result = result * 59 + ($resultQty3 == null ? 43 : $resultQty3.hashCode());
        Object $gspcbMemberId = this.getGspcbMemberId();
        result = result * 59 + ($gspcbMemberId == null ? 43 : $gspcbMemberId.hashCode());
        Object $gspcbGainFlag = this.getGspcbGainFlag();
        result = result * 59 + ($gspcbGainFlag == null ? 43 : $gspcbGainFlag.hashCode());
        Object $gspcbGrantBillNo = this.getGspcbGrantBillNo();
        result = result * 59 + ($gspcbGrantBillNo == null ? 43 : $gspcbGrantBillNo.hashCode());
        Object $gspcbGrantBrId = this.getGspcbGrantBrId();
        result = result * 59 + ($gspcbGrantBrId == null ? 43 : $gspcbGrantBrId.hashCode());
        Object $gspcbGrantDate = this.getGspcbGrantDate();
        result = result * 59 + ($gspcbGrantDate == null ? 43 : $gspcbGrantDate.hashCode());
        Object $gspcbUseBillNo = this.getGspcbUseBillNo();
        result = result * 59 + ($gspcbUseBillNo == null ? 43 : $gspcbUseBillNo.hashCode());
        Object $gspcbUseBrId = this.getGspcbUseBrId();
        result = result * 59 + ($gspcbUseBrId == null ? 43 : $gspcbUseBrId.hashCode());
        Object $gspcbUseDate = this.getGspcbUseDate();
        result = result * 59 + ($gspcbUseDate == null ? 43 : $gspcbUseDate.hashCode());
        Object $gspcbStatus = this.getGspcbStatus();
        result = result * 59 + ($gspcbStatus == null ? 43 : $gspcbStatus.hashCode());
        return result;
    }

    public String toString() {
        return "GetDzqOutData(clientId=" + this.getClientId() + ", gspcbCouponId=" + this.getGspcbCouponId() + ", gspcbActNo=" + this.getGspcbActNo() + ", gspcbName=" + this.getGspcbName() + ", gspcbDesc=" + this.getGspcbDesc() + ", gspcbEndDate=" + this.getGspcbEndDate() + ", gspcbCount=" + this.getGspcbCount() + ", gspcbCouponAmt=" + this.getGspcbCouponAmt() + ", proCode=" + this.getProCode() + ", reachAmount1=" + this.getReachAmount1() + ", reachAmount2=" + this.getReachAmount2() + ", reachAmount3=" + this.getReachAmount3() + ", reachQty1=" + this.getReachQty1() + ", reachQty2=" + this.getReachQty2() + ", reachQty3=" + this.getReachQty3() + ", resultQty1=" + this.getResultQty1() + ", resultQty2=" + this.getResultQty2() + ", resultQty3=" + this.getResultQty3() + ", gspcbMemberId=" + this.getGspcbMemberId() + ", gspcbGainFlag=" + this.getGspcbGainFlag() + ", gspcbGrantBillNo=" + this.getGspcbGrantBillNo() + ", gspcbGrantBrId=" + this.getGspcbGrantBrId() + ", gspcbGrantDate=" + this.getGspcbGrantDate() + ", gspcbUseBillNo=" + this.getGspcbUseBillNo() + ", gspcbUseBrId=" + this.getGspcbUseBrId() + ", gspcbUseDate=" + this.getGspcbUseDate() + ", gspcbStatus=" + this.getGspcbStatus() + ")";
    }
}
