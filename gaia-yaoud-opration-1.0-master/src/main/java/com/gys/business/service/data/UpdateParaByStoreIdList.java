package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class UpdateParaByStoreIdList implements Serializable {

    @ApiModelProperty(value = "门店编号")
    private String brId;

    @ApiModelProperty(value = "月均销售额")
    private BigDecimal avgSaleAmt;

    @ApiModelProperty(value = "月均毛利额")
    private BigDecimal avgProfitAmt;

}
