package com.gys.business.service.data;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;



@Data
public class GaiaSdDefecproDvo {

    @ApiModelProperty(value = "序号")
    private Integer index;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "地点")
    private String brId;

    @ApiModelProperty(value = "商品编码")
    private String gsddProId;


}
