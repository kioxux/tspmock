package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 小票参数返回实体
 *
 * @author xiaoyuan on 2020/8/3
 */
@Data
public class ReceiptPayOutData implements Serializable {
    private static final long serialVersionUID = -3702405219028379070L;

    /**
     * 加盟号
     */
    private String client;

    /**
     * 店号
     */
    private String gsspmBrId;

    /**
     * 销售单号
     */
    private String gsspmBillNo;

    /**
     * 日期
     */
    private String gsspmDate;

    /**
     * 支付编码
     */
    private String gsspmId;

    /**
     * 支付类型
     */
    private String gsspmType;

    /**
     * 支付名称
     */
    private String gsspmName;

    /**
     * 支付金额
     */
    private String gsspmAmt;

    /**
     * 找零金额
     */
    private String gsspmZlAmt;

    /**
     * RMB收款金额
     */
    private String gsspmRmbAmt;

    /**
     * 支付卡号
     */
    private String gsspmCardNo;


    /**
     * 医保卡号
     */
    private String medicalCard;

    /**
     * 医保姓名
     */
    private String medicalName;

    /**
     * 账户余额
     */
    private String accountBalance;

    /**
     * 是否为接口 是否为接口  0为否，1为是
     */
    private String gspmFlag;

}
