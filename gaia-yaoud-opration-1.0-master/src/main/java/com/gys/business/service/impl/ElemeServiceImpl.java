//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.gys.business.service.ElemeService;
import com.gys.business.service.data.GetElemeInData;
import com.gys.common.exception.BusinessException;
import com.gys.common.http.HttpRequestUtil;
import com.gys.common.webSocket.WebSocket;
import com.gys.util.SignUtil;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ElemeServiceImpl implements ElemeService {
    private static final Logger log = LoggerFactory.getLogger(ElemeServiceImpl.class);
    @Resource
    private WebSocket webSocket;
    public static Gson gson = new Gson();
    String source = "60836";
    String secret = "e885dc3ff6f75105";
    String v = "3";
    String request_address = "https://api-be.ele.me";

    public ElemeServiceImpl() {
    }

    @Transactional
    public void orderGetOrderDetail(GetElemeInData inData) {
        Map<String, Object> params = new HashMap();
        params.put("order_id", inData.getOrderId());
        String ticket = UUID.randomUUID().toString().toUpperCase();
        String url = "order.get";
        String requestParams = this.getRequestparams(params, ticket, url);
        String result = HttpRequestUtil.sendPost(this.request_address, requestParams);
        log.info(result);
        JSONObject object = JSONObject.parseObject(result).getJSONObject("body");
        if (!"0".equals(object.getString("errno"))) {
            throw new BusinessException(object.getString("error"));
        }
    }

    @Transactional
    public void orderConfirm(GetElemeInData inData) {
        Map<String, Object> params = new HashMap();
        params.put("order_id", inData.getOrderId());
        String ticket = UUID.randomUUID().toString().toUpperCase();
        String url = "order.confirm";
        String requestParams = this.getRequestparams(params, ticket, url);
        String result = HttpRequestUtil.sendPost(this.request_address, requestParams);
        log.info(result);
        JSONObject object = JSONObject.parseObject(result).getJSONObject("body");
        if (!"0".equals(object.getString("errno"))) {
            throw new BusinessException(object.getString("error"));
        }
    }

    @Transactional
    public void orderCancel(GetElemeInData inData) {
        Map<String, Object> params = new HashMap();
        params.put("order_id", inData.getOrderId());
        params.put("type", inData.getReasonCode());
        params.put("reason", inData.getReason());
        String ticket = UUID.randomUUID().toString().toUpperCase();
        String url = "order.cancel";
        String requestParams = this.getRequestparams(params, ticket, url);
        String result = HttpRequestUtil.sendPost(this.request_address, requestParams);
        log.info(result);
        JSONObject object = JSONObject.parseObject(result).getJSONObject("body");
        if (!"0".equals(object.getString("errno"))) {
            throw new BusinessException(object.getString("error"));
        }
    }

    @Transactional
    public void orderPreparationMealComplete(GetElemeInData inData) {
        Map<String, Object> params = new HashMap();
        params.put("order_id", inData.getOrderId());
        String ticket = UUID.randomUUID().toString().toUpperCase();
        String url = "order.callDelivery";
        String requestParams = this.getRequestparams(params, ticket, url);
        String result = HttpRequestUtil.sendPost(this.request_address, requestParams);
        log.info(result);
        JSONObject object = JSONObject.parseObject(result).getJSONObject("body");
        if (!"0".equals(object.getString("errno"))) {
            throw new BusinessException(object.getString("error"));
        }
    }

    @Transactional
    public void orderRefundAgree(GetElemeInData inData) {
        Map<String, Object> params = new HashMap();
        params.put("order_id", inData.getOrderId());
        params.put("refund_order_id", inData.getServiceOrder());
        String ticket = UUID.randomUUID().toString().toUpperCase();
        String url = "order.agreerefund";
        String requestParams = this.getRequestparams(params, ticket, url);
        String result = HttpRequestUtil.sendPost(this.request_address, requestParams);
        log.info(result);
        JSONObject object = JSONObject.parseObject(result).getJSONObject("body");
        if (!"0".equals(object.getString("errno"))) {
            throw new BusinessException(object.getString("error"));
        }
    }

    @Transactional
    public void orderRefundReject(GetElemeInData inData) {
        Map<String, Object> params = new HashMap();
        params.put("order_id", inData.getOrderId());
        params.put("refund_order_id", inData.getServiceOrder());
        params.put("refuse_reason", inData.getReason());
        String ticket = UUID.randomUUID().toString().toUpperCase();
        String url = "order.disagreerefund";
        String requestParams = this.getRequestparams(params, ticket, url);
        String result = HttpRequestUtil.sendPost(this.request_address, requestParams);
        log.info(result);
        JSONObject object = JSONObject.parseObject(result).getJSONObject("body");
        if (!"0".equals(object.getString("errno"))) {
            throw new BusinessException(object.getString("error"));
        }
    }

    @Transactional
    public void medicineStock(GetElemeInData inData) {
        String skuidStocks = "1493930400:999;1493928499:999";
        Map<String, Object> params = new HashMap();
        params.put("order_id", inData.getOrderId());
        params.put("skuid_stocks", skuidStocks);
        String ticket = UUID.randomUUID().toString().toUpperCase();
        String url = "sku.stock.update.batch";
        String requestParams = this.getRequestparams(params, ticket, url);
        String result = HttpRequestUtil.sendPost(this.request_address, requestParams);
        log.info(result);
        JSONObject object = JSONObject.parseObject(result).getJSONObject("body");
        if (!"0".equals(object.getString("errno"))) {
            throw new BusinessException(object.getString("error"));
        }
    }

    private String getRequestparams(Map<String, Object> body, String ticket, String url) {
        Map<String, Object> params = new HashMap();
        params.put("cmd", url);
        params.put("encrypt", "");
        params.put("secret", this.secret);
        params.put("source", this.source);
        params.put("ticket", ticket);
        params.put("body", body);
        params.put("version", this.v);
        params.put("timestamp", System.currentTimeMillis() / 1000L);
        String sign = SignUtil.getSign(params);
        params.put("sign", sign);
        params.remove("secret");
        params.put("body", gson.toJson(params.get("body")));
        StringBuilder requestparams = new StringBuilder();
        Iterator var7 = params.entrySet().iterator();

        while(var7.hasNext()) {
            Entry<String, Object> map = (Entry)var7.next();
            requestparams.append((String)map.getKey() + "=" + map.getValue() + "&");
        }

        return requestparams.substring(0, requestparams.length() - 1).toString();
    }

    public static String gbEncoding(final String gbString) {
        char[] utfBytes = gbString.toCharArray();
        String unicodeBytes = "";

        for(int i = 0; i < utfBytes.length; ++i) {
            String hexB = Integer.toHexString(utfBytes[i]);
            if (hexB.length() <= 2) {
                hexB = "00" + hexB;
            }

            unicodeBytes = unicodeBytes + "\\u" + hexB;
        }

        return unicodeBytes;
    }
}
