//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class PromHySetOutData {
    private String clientId;
    private String gsphsVoucherId;
    private String gsphsSerial;
    private String gsphsProId;
    private String gspgProName;
    private String gspgSpecs;
    private String proPlace;
    private String proPrice;
    private BigDecimal gsphsPrice;
    private String gsphsRebate;
    private String gsphsInteFlag;
    private String gsphsInteRate;

    public PromHySetOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsphsVoucherId() {
        return this.gsphsVoucherId;
    }

    public String getGsphsSerial() {
        return this.gsphsSerial;
    }

    public String getGsphsProId() {
        return this.gsphsProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgSpecs() {
        return this.gspgSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public BigDecimal getGsphsPrice() {
        return this.gsphsPrice;
    }

    public String getGsphsRebate() {
        return this.gsphsRebate;
    }

    public String getGsphsInteFlag() {
        return this.gsphsInteFlag;
    }

    public String getGsphsInteRate() {
        return this.gsphsInteRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsphsVoucherId(final String gsphsVoucherId) {
        this.gsphsVoucherId = gsphsVoucherId;
    }

    public void setGsphsSerial(final String gsphsSerial) {
        this.gsphsSerial = gsphsSerial;
    }

    public void setGsphsProId(final String gsphsProId) {
        this.gsphsProId = gsphsProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgSpecs(final String gspgSpecs) {
        this.gspgSpecs = gspgSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setGsphsPrice(final BigDecimal gsphsPrice) {
        this.gsphsPrice = gsphsPrice;
    }

    public void setGsphsRebate(final String gsphsRebate) {
        this.gsphsRebate = gsphsRebate;
    }

    public void setGsphsInteFlag(final String gsphsInteFlag) {
        this.gsphsInteFlag = gsphsInteFlag;
    }

    public void setGsphsInteRate(final String gsphsInteRate) {
        this.gsphsInteRate = gsphsInteRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromHySetOutData)) {
            return false;
        } else {
            PromHySetOutData other = (PromHySetOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label155;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label155;
                    }

                    return false;
                }

                Object this$gsphsVoucherId = this.getGsphsVoucherId();
                Object other$gsphsVoucherId = other.getGsphsVoucherId();
                if (this$gsphsVoucherId == null) {
                    if (other$gsphsVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsphsVoucherId.equals(other$gsphsVoucherId)) {
                    return false;
                }

                Object this$gsphsSerial = this.getGsphsSerial();
                Object other$gsphsSerial = other.getGsphsSerial();
                if (this$gsphsSerial == null) {
                    if (other$gsphsSerial != null) {
                        return false;
                    }
                } else if (!this$gsphsSerial.equals(other$gsphsSerial)) {
                    return false;
                }

                label134: {
                    Object this$gsphsProId = this.getGsphsProId();
                    Object other$gsphsProId = other.getGsphsProId();
                    if (this$gsphsProId == null) {
                        if (other$gsphsProId == null) {
                            break label134;
                        }
                    } else if (this$gsphsProId.equals(other$gsphsProId)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label127;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$gspgSpecs = this.getGspgSpecs();
                    Object other$gspgSpecs = other.getGspgSpecs();
                    if (this$gspgSpecs == null) {
                        if (other$gspgSpecs == null) {
                            break label120;
                        }
                    } else if (this$gspgSpecs.equals(other$gspgSpecs)) {
                        break label120;
                    }

                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                label106: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label106;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label106;
                    }

                    return false;
                }

                Object this$gsphsPrice = this.getGsphsPrice();
                Object other$gsphsPrice = other.getGsphsPrice();
                if (this$gsphsPrice == null) {
                    if (other$gsphsPrice != null) {
                        return false;
                    }
                } else if (!this$gsphsPrice.equals(other$gsphsPrice)) {
                    return false;
                }

                label92: {
                    Object this$gsphsRebate = this.getGsphsRebate();
                    Object other$gsphsRebate = other.getGsphsRebate();
                    if (this$gsphsRebate == null) {
                        if (other$gsphsRebate == null) {
                            break label92;
                        }
                    } else if (this$gsphsRebate.equals(other$gsphsRebate)) {
                        break label92;
                    }

                    return false;
                }

                Object this$gsphsInteFlag = this.getGsphsInteFlag();
                Object other$gsphsInteFlag = other.getGsphsInteFlag();
                if (this$gsphsInteFlag == null) {
                    if (other$gsphsInteFlag != null) {
                        return false;
                    }
                } else if (!this$gsphsInteFlag.equals(other$gsphsInteFlag)) {
                    return false;
                }

                Object this$gsphsInteRate = this.getGsphsInteRate();
                Object other$gsphsInteRate = other.getGsphsInteRate();
                if (this$gsphsInteRate == null) {
                    if (other$gsphsInteRate != null) {
                        return false;
                    }
                } else if (!this$gsphsInteRate.equals(other$gsphsInteRate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromHySetOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsphsVoucherId = this.getGsphsVoucherId();
        result = result * 59 + ($gsphsVoucherId == null ? 43 : $gsphsVoucherId.hashCode());
        Object $gsphsSerial = this.getGsphsSerial();
        result = result * 59 + ($gsphsSerial == null ? 43 : $gsphsSerial.hashCode());
        Object $gsphsProId = this.getGsphsProId();
        result = result * 59 + ($gsphsProId == null ? 43 : $gsphsProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgSpecs = this.getGspgSpecs();
        result = result * 59 + ($gspgSpecs == null ? 43 : $gspgSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $gsphsPrice = this.getGsphsPrice();
        result = result * 59 + ($gsphsPrice == null ? 43 : $gsphsPrice.hashCode());
        Object $gsphsRebate = this.getGsphsRebate();
        result = result * 59 + ($gsphsRebate == null ? 43 : $gsphsRebate.hashCode());
        Object $gsphsInteFlag = this.getGsphsInteFlag();
        result = result * 59 + ($gsphsInteFlag == null ? 43 : $gsphsInteFlag.hashCode());
        Object $gsphsInteRate = this.getGsphsInteRate();
        result = result * 59 + ($gsphsInteRate == null ? 43 : $gsphsInteRate.hashCode());
        return result;
    }

    public String toString() {
        return "PromHySetOutData(clientId=" + this.getClientId() + ", gsphsVoucherId=" + this.getGsphsVoucherId() + ", gsphsSerial=" + this.getGsphsSerial() + ", gsphsProId=" + this.getGsphsProId() + ", gspgProName=" + this.getGspgProName() + ", gspgSpecs=" + this.getGspgSpecs() + ", proPlace=" + this.getProPlace() + ", proPrice=" + this.getProPrice() + ", gsphsPrice=" + this.getGsphsPrice() + ", gsphsRebate=" + this.getGsphsRebate() + ", gsphsInteFlag=" + this.getGsphsInteFlag() + ", gsphsInteRate=" + this.getGsphsInteRate() + ")";
    }
}
