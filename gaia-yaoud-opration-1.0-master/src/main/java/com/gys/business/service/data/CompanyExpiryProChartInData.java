package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class CompanyExpiryProChartInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "排序标志 1-按效期成本额降序  2-按店号升序")
    private String orderByFlag;

    @ApiModelProperty(value = "排序标志2 1-效期品项数 2-按效期成本额降序")
    private String orderByFlagExt;

    @ApiModelProperty(value = "排序字段 1-前(从大到小) 2-后(从小到大)")
    private Integer topOrLast;

    @ApiModelProperty(value = "显示条数")
    private Integer pageSize;


}
