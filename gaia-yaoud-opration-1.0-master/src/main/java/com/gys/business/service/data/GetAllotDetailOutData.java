//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetAllotDetailOutData {
    private String clientId;
    private String gsamdVoucherId;
    private String gsamdDate;
    private String gsamdSerial;
    private String gsamdProId;
    private String gsamdBatchNo;
    private String gsamdBatch;
    private String gsamdValidDate;
    private String gsamdStockQty;
    private String gsamdRecipientQty;
    private String gsamdStockStatus;
    private String proName;
    private String proFactoryName;
    private String proPlace;
    private String proForm;
    private String proPrice;
    private String proUnit;
    private String proSpecs;
    private String proRegisterNo;

    public GetAllotDetailOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsamdVoucherId() {
        return this.gsamdVoucherId;
    }

    public String getGsamdDate() {
        return this.gsamdDate;
    }

    public String getGsamdSerial() {
        return this.gsamdSerial;
    }

    public String getGsamdProId() {
        return this.gsamdProId;
    }

    public String getGsamdBatchNo() {
        return this.gsamdBatchNo;
    }

    public String getGsamdBatch() {
        return this.gsamdBatch;
    }

    public String getGsamdValidDate() {
        return this.gsamdValidDate;
    }

    public String getGsamdStockQty() {
        return this.gsamdStockQty;
    }

    public String getGsamdRecipientQty() {
        return this.gsamdRecipientQty;
    }

    public String getGsamdStockStatus() {
        return this.gsamdStockStatus;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProForm() {
        return this.proForm;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProRegisterNo() {
        return this.proRegisterNo;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsamdVoucherId(final String gsamdVoucherId) {
        this.gsamdVoucherId = gsamdVoucherId;
    }

    public void setGsamdDate(final String gsamdDate) {
        this.gsamdDate = gsamdDate;
    }

    public void setGsamdSerial(final String gsamdSerial) {
        this.gsamdSerial = gsamdSerial;
    }

    public void setGsamdProId(final String gsamdProId) {
        this.gsamdProId = gsamdProId;
    }

    public void setGsamdBatchNo(final String gsamdBatchNo) {
        this.gsamdBatchNo = gsamdBatchNo;
    }

    public void setGsamdBatch(final String gsamdBatch) {
        this.gsamdBatch = gsamdBatch;
    }

    public void setGsamdValidDate(final String gsamdValidDate) {
        this.gsamdValidDate = gsamdValidDate;
    }

    public void setGsamdStockQty(final String gsamdStockQty) {
        this.gsamdStockQty = gsamdStockQty;
    }

    public void setGsamdRecipientQty(final String gsamdRecipientQty) {
        this.gsamdRecipientQty = gsamdRecipientQty;
    }

    public void setGsamdStockStatus(final String gsamdStockStatus) {
        this.gsamdStockStatus = gsamdStockStatus;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProRegisterNo(final String proRegisterNo) {
        this.proRegisterNo = proRegisterNo;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetAllotDetailOutData)) {
            return false;
        } else {
            GetAllotDetailOutData other = (GetAllotDetailOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label239: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label239;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label239;
                    }

                    return false;
                }

                Object this$gsamdVoucherId = this.getGsamdVoucherId();
                Object other$gsamdVoucherId = other.getGsamdVoucherId();
                if (this$gsamdVoucherId == null) {
                    if (other$gsamdVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsamdVoucherId.equals(other$gsamdVoucherId)) {
                    return false;
                }

                Object this$gsamdDate = this.getGsamdDate();
                Object other$gsamdDate = other.getGsamdDate();
                if (this$gsamdDate == null) {
                    if (other$gsamdDate != null) {
                        return false;
                    }
                } else if (!this$gsamdDate.equals(other$gsamdDate)) {
                    return false;
                }

                label218: {
                    Object this$gsamdSerial = this.getGsamdSerial();
                    Object other$gsamdSerial = other.getGsamdSerial();
                    if (this$gsamdSerial == null) {
                        if (other$gsamdSerial == null) {
                            break label218;
                        }
                    } else if (this$gsamdSerial.equals(other$gsamdSerial)) {
                        break label218;
                    }

                    return false;
                }

                label211: {
                    Object this$gsamdProId = this.getGsamdProId();
                    Object other$gsamdProId = other.getGsamdProId();
                    if (this$gsamdProId == null) {
                        if (other$gsamdProId == null) {
                            break label211;
                        }
                    } else if (this$gsamdProId.equals(other$gsamdProId)) {
                        break label211;
                    }

                    return false;
                }

                Object this$gsamdBatchNo = this.getGsamdBatchNo();
                Object other$gsamdBatchNo = other.getGsamdBatchNo();
                if (this$gsamdBatchNo == null) {
                    if (other$gsamdBatchNo != null) {
                        return false;
                    }
                } else if (!this$gsamdBatchNo.equals(other$gsamdBatchNo)) {
                    return false;
                }

                Object this$gsamdBatch = this.getGsamdBatch();
                Object other$gsamdBatch = other.getGsamdBatch();
                if (this$gsamdBatch == null) {
                    if (other$gsamdBatch != null) {
                        return false;
                    }
                } else if (!this$gsamdBatch.equals(other$gsamdBatch)) {
                    return false;
                }

                label190: {
                    Object this$gsamdValidDate = this.getGsamdValidDate();
                    Object other$gsamdValidDate = other.getGsamdValidDate();
                    if (this$gsamdValidDate == null) {
                        if (other$gsamdValidDate == null) {
                            break label190;
                        }
                    } else if (this$gsamdValidDate.equals(other$gsamdValidDate)) {
                        break label190;
                    }

                    return false;
                }

                label183: {
                    Object this$gsamdStockQty = this.getGsamdStockQty();
                    Object other$gsamdStockQty = other.getGsamdStockQty();
                    if (this$gsamdStockQty == null) {
                        if (other$gsamdStockQty == null) {
                            break label183;
                        }
                    } else if (this$gsamdStockQty.equals(other$gsamdStockQty)) {
                        break label183;
                    }

                    return false;
                }

                Object this$gsamdRecipientQty = this.getGsamdRecipientQty();
                Object other$gsamdRecipientQty = other.getGsamdRecipientQty();
                if (this$gsamdRecipientQty == null) {
                    if (other$gsamdRecipientQty != null) {
                        return false;
                    }
                } else if (!this$gsamdRecipientQty.equals(other$gsamdRecipientQty)) {
                    return false;
                }

                label169: {
                    Object this$gsamdStockStatus = this.getGsamdStockStatus();
                    Object other$gsamdStockStatus = other.getGsamdStockStatus();
                    if (this$gsamdStockStatus == null) {
                        if (other$gsamdStockStatus == null) {
                            break label169;
                        }
                    } else if (this$gsamdStockStatus.equals(other$gsamdStockStatus)) {
                        break label169;
                    }

                    return false;
                }

                Object this$proName = this.getProName();
                Object other$proName = other.getProName();
                if (this$proName == null) {
                    if (other$proName != null) {
                        return false;
                    }
                } else if (!this$proName.equals(other$proName)) {
                    return false;
                }

                label155: {
                    Object this$proFactoryName = this.getProFactoryName();
                    Object other$proFactoryName = other.getProFactoryName();
                    if (this$proFactoryName == null) {
                        if (other$proFactoryName == null) {
                            break label155;
                        }
                    } else if (this$proFactoryName.equals(other$proFactoryName)) {
                        break label155;
                    }

                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                Object this$proForm = this.getProForm();
                Object other$proForm = other.getProForm();
                if (this$proForm == null) {
                    if (other$proForm != null) {
                        return false;
                    }
                } else if (!this$proForm.equals(other$proForm)) {
                    return false;
                }

                label134: {
                    Object this$proPrice = this.getProPrice();
                    Object other$proPrice = other.getProPrice();
                    if (this$proPrice == null) {
                        if (other$proPrice == null) {
                            break label134;
                        }
                    } else if (this$proPrice.equals(other$proPrice)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$proUnit = this.getProUnit();
                    Object other$proUnit = other.getProUnit();
                    if (this$proUnit == null) {
                        if (other$proUnit == null) {
                            break label127;
                        }
                    } else if (this$proUnit.equals(other$proUnit)) {
                        break label127;
                    }

                    return false;
                }

                Object this$proSpecs = this.getProSpecs();
                Object other$proSpecs = other.getProSpecs();
                if (this$proSpecs == null) {
                    if (other$proSpecs != null) {
                        return false;
                    }
                } else if (!this$proSpecs.equals(other$proSpecs)) {
                    return false;
                }

                Object this$proRegisterNo = this.getProRegisterNo();
                Object other$proRegisterNo = other.getProRegisterNo();
                if (this$proRegisterNo == null) {
                    if (other$proRegisterNo != null) {
                        return false;
                    }
                } else if (!this$proRegisterNo.equals(other$proRegisterNo)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetAllotDetailOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsamdVoucherId = this.getGsamdVoucherId();
        result = result * 59 + ($gsamdVoucherId == null ? 43 : $gsamdVoucherId.hashCode());
        Object $gsamdDate = this.getGsamdDate();
        result = result * 59 + ($gsamdDate == null ? 43 : $gsamdDate.hashCode());
        Object $gsamdSerial = this.getGsamdSerial();
        result = result * 59 + ($gsamdSerial == null ? 43 : $gsamdSerial.hashCode());
        Object $gsamdProId = this.getGsamdProId();
        result = result * 59 + ($gsamdProId == null ? 43 : $gsamdProId.hashCode());
        Object $gsamdBatchNo = this.getGsamdBatchNo();
        result = result * 59 + ($gsamdBatchNo == null ? 43 : $gsamdBatchNo.hashCode());
        Object $gsamdBatch = this.getGsamdBatch();
        result = result * 59 + ($gsamdBatch == null ? 43 : $gsamdBatch.hashCode());
        Object $gsamdValidDate = this.getGsamdValidDate();
        result = result * 59 + ($gsamdValidDate == null ? 43 : $gsamdValidDate.hashCode());
        Object $gsamdStockQty = this.getGsamdStockQty();
        result = result * 59 + ($gsamdStockQty == null ? 43 : $gsamdStockQty.hashCode());
        Object $gsamdRecipientQty = this.getGsamdRecipientQty();
        result = result * 59 + ($gsamdRecipientQty == null ? 43 : $gsamdRecipientQty.hashCode());
        Object $gsamdStockStatus = this.getGsamdStockStatus();
        result = result * 59 + ($gsamdStockStatus == null ? 43 : $gsamdStockStatus.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        Object $proPrice = this.getProPrice();
        result = result * 59 + ($proPrice == null ? 43 : $proPrice.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proRegisterNo = this.getProRegisterNo();
        result = result * 59 + ($proRegisterNo == null ? 43 : $proRegisterNo.hashCode());
        return result;
    }

    public String toString() {
        return "GetAllotDetailOutData(clientId=" + this.getClientId() + ", gsamdVoucherId=" + this.getGsamdVoucherId() + ", gsamdDate=" + this.getGsamdDate() + ", gsamdSerial=" + this.getGsamdSerial() + ", gsamdProId=" + this.getGsamdProId() + ", gsamdBatchNo=" + this.getGsamdBatchNo() + ", gsamdBatch=" + this.getGsamdBatch() + ", gsamdValidDate=" + this.getGsamdValidDate() + ", gsamdStockQty=" + this.getGsamdStockQty() + ", gsamdRecipientQty=" + this.getGsamdRecipientQty() + ", gsamdStockStatus=" + this.getGsamdStockStatus() + ", proName=" + this.getProName() + ", proFactoryName=" + this.getProFactoryName() + ", proPlace=" + this.getProPlace() + ", proForm=" + this.getProForm() + ", proPrice=" + this.getProPrice() + ", proUnit=" + this.getProUnit() + ", proSpecs=" + this.getProSpecs() + ", proRegisterNo=" + this.getProRegisterNo() + ")";
    }
}
