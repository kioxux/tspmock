package com.gys.business.service;

import com.gys.business.service.data.CommodityAllocationPushInData;

/**
 * 商品调库-明细表(GaiaCommodityInventoryD)表服务接口
 *
 * @author makejava
 * @since 2021-10-19 16:01:15
 */
public interface GaiaCommodityInventoryDService {

    void push(CommodityAllocationPushInData inData);
}
