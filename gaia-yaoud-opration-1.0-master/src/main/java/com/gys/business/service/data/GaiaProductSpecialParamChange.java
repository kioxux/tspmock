package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="com-gys-business-service-data-GaiaProductSpecialParamChange")
public class GaiaProductSpecialParamChange implements Serializable {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 省份
    */
    @ApiModelProperty(value="省份")
    private String proProv;

    /**
    * 地市
    */
    @ApiModelProperty(value="地市")
    private String proCity;

    /**
    * 参数名称-季节 1-春，2-夏，3-秋，4-冬
    */
    @ApiModelProperty(value="参数名称-季节 1-春，2-夏，3-秋，4-冬")
    private String proName;

    /**
    * 变更字段
    */
    @ApiModelProperty(value="变更字段")
    private String proChangeField;

    /**
    * 变更前值
    */
    @ApiModelProperty(value="变更前值")
    private String proChangeFrom;

    /**
    * 变更后值
    */
    @ApiModelProperty(value="变更后值")
    private String proChangeTo;

    /**
    * 变更人
    */
    @ApiModelProperty(value="变更人")
    private String proChangeUser;

    /**
    * 变更日期
    */
    @ApiModelProperty(value="变更日期")
    private String proChangeDate;

    /**
    * 变更时间
    */
    @ApiModelProperty(value="变更时间")
    private String proChangeTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProProv() {
        return proProv;
    }

    public void setProProv(String proProv) {
        this.proProv = proProv;
    }

    public String getProCity() {
        return proCity;
    }

    public void setProCity(String proCity) {
        this.proCity = proCity;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProChangeField() {
        return proChangeField;
    }

    public void setProChangeField(String proChangeField) {
        this.proChangeField = proChangeField;
    }

    public String getProChangeFrom() {
        return proChangeFrom;
    }

    public void setProChangeFrom(String proChangeFrom) {
        this.proChangeFrom = proChangeFrom;
    }

    public String getProChangeTo() {
        return proChangeTo;
    }

    public void setProChangeTo(String proChangeTo) {
        this.proChangeTo = proChangeTo;
    }

    public String getProChangeUser() {
        return proChangeUser;
    }

    public void setProChangeUser(String proChangeUser) {
        this.proChangeUser = proChangeUser;
    }

    public String getProChangeDate() {
        return proChangeDate;
    }

    public void setProChangeDate(String proChangeDate) {
        this.proChangeDate = proChangeDate;
    }

    public String getProChangeTime() {
        return proChangeTime;
    }

    public void setProChangeTime(String proChangeTime) {
        this.proChangeTime = proChangeTime;
    }
}