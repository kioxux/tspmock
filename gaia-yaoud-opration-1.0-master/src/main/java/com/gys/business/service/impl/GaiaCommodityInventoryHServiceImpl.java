package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaClSystemParaMapper;
import com.gys.business.mapper.GaiaCommodityInventoryDMapper;
import com.gys.business.mapper.GaiaCommodityInventoryHMapper;
import com.gys.business.mapper.GaiaSdMessageMapper;
import com.gys.business.mapper.entity.GaiaCommodityInventoryD;
import com.gys.business.mapper.entity.GaiaCommodityInventoryH;
import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.service.GaiaCommodityInventoryHService;
import com.gys.business.service.data.GaiaClSystemPara;
import com.gys.business.service.data.GaiaCommodityInventoryDOut;
import com.gys.business.service.data.GaiaCommodityInventoryHInData;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.SdMessageTypeEnum;
import com.gys.common.excel.ObjectUtil;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.DateUtil;
import com.gys.util.Util;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 商品调库-主表(GaiaCommodityInventoryH)表服务实现类
 *
 * @author makejava
 * @since 2021-10-19 16:00:52
 */
@Service("gaiaCommodityInventoryHService")
@Slf4j
public class GaiaCommodityInventoryHServiceImpl implements GaiaCommodityInventoryHService {

    @Resource
    private GaiaCommodityInventoryHMapper gaiaCommodityInventoryHMapper;

    @Resource
    private GaiaCommodityInventoryDMapper gaiaCommodityInventoryDMapper;

    @Resource
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;

    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;

    @Resource
    public CosUtils cosUtils;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object insertInventoryData(String param) {
        log.info(String.format("<商品调库><定时任务><开始时间：%s>", LocalDateTime.now()));
        //1.0查询有仓库
        List<HashMap<String, String>> clientList = gaiaCommodityInventoryHMapper.listClient();
        if (!CollectionUtils.isEmpty(clientList)) {
            Map<String, List<HashMap<String, String>>> clientMap = clientList.stream().collect(Collectors.groupingBy(item -> item.get("client")));
            Set<String> clientSet = clientMap.keySet();
            Date now = new Date();
            for (String client : clientSet) {
                List<HashMap<String, String>> storeList = clientMap.get(client);
                //查询自定义参数1
                GaiaClSystemPara para1 = gaiaClSystemParaMapper.selectByPrimaryKey(client, "COMMODITY_PARAM1");
                //是否开始插入数据
                Boolean enable = false;
                //参数2：商品调库销售计算时长，默认值：90
                String param1 = "month";
                int param2 = 90;
                enable = checkEnbaleFlag(now, para1, enable, param2, param1);
                if (enable|| "sender".equals(param)) {
                    List<GaiaCommodityInventoryD> insertDList = new ArrayList<>();
                    GaiaClSystemPara para4 = gaiaClSystemParaMapper.selectByPrimaryKey(client, "COMMODITY_PARAM4");
                    String billCode = gaiaCommodityInventoryHMapper.getBillCode(client);
                    GaiaClSystemPara para2 = gaiaClSystemParaMapper.selectByPrimaryKey(client, "COMMODITY_PARAM2");
                    GaiaClSystemPara para3 = gaiaClSystemParaMapper.selectByPrimaryKey(client, "COMMODITY_PARAM3");
                    for (HashMap<String, String> storeInfo : storeList) {
                        //查询该家门店，需要比较的商品
                        List<GaiaCommodityInventoryDOut> goods = gaiaCommodityInventoryHMapper.listWaitGoods(storeInfo.get("client"), storeInfo.get("stoCode"));
                        List<GaiaCommodityInventoryDOut> salesInfos = gaiaCommodityInventoryHMapper.listSalesInfo(storeInfo.get("client"), storeInfo.get("stoCode"),param2);
                        if (!CollectionUtils.isEmpty(goods)) {
                            for (GaiaCommodityInventoryDOut good : goods) {
                                //参数5：商品调库安全库存量（＜成本价界值），默认值：3
                                BigDecimal param5 = new BigDecimal("3");
                                //参数6：商品调库调库量取值（≥成本价界值），默认值1
                                BigDecimal param6 = new BigDecimal("1");
                                checkParam(para3, param5, param6);
                                BigDecimal param7 = new BigDecimal("1");
                                BigDecimal param8 = new BigDecimal("10");
                                checkParam(para4, param7, param8);
                                //参数3：商品调库调库量成本价界值，默认值：100
                                BigDecimal param3 = new BigDecimal("100");
                                //参数4：商品调库安全库存量（≥成本价界值），默认值：2
                                BigDecimal param4 = new BigDecimal("2");
                                checkParam(para2, param3, param4);
                                Boolean flag = false;
                                for (GaiaCommodityInventoryDOut salesInfo : salesInfos) {
                                    if (good.getClient().equals(salesInfo.getClient()) && good.getProSelfCode().equals(salesInfo.getProSelfCode()) && good.getStoCode().equals(salesInfo.getStoCode())) {
                                        //查询商品的 90天销量，90天销售额，90天毛利额，
                                        BigDecimal suggestionReturnQty = BigDecimal.ZERO;
                                        if (good.getCost().compareTo(param3) >= 0) {
                                            if (ObjectUtil.isNotEmpty(salesInfo) && param4.compareTo(salesInfo.getAverageSalesQty() == null ? BigDecimal.ZERO : salesInfo.getAverageSalesQty()) < 0) {
                                                param4 = new BigDecimal(salesInfo.getAverageSalesQty().toString());
                                            }
                                            //计算调库量
                                            //调库量=库存量-MAX（90天 （参数2）销量，2（参数4）），且调库量 ≥1（参数6）的整数(向下取整)
                                            suggestionReturnQty = new BigDecimal(good.getInventoryQty().toString()).subtract(param4).setScale(0, RoundingMode.DOWN);
                                            if (suggestionReturnQty.compareTo(param6) >= 0) {
                                                GaiaCommodityInventoryD gaiaCommodityInventoryD = new GaiaCommodityInventoryD();
                                                setDetail(now, client, param2, storeInfo, billCode, good, salesInfo, suggestionReturnQty, gaiaCommodityInventoryD);
                                                insertDList.add(gaiaCommodityInventoryD);
                                            }
                                        }//如果成本价 < 100
                                        else {
                                            if (ObjectUtil.isNotEmpty(salesInfo) && param5.compareTo(salesInfo.getAverageSalesQty() == null ? BigDecimal.ZERO : salesInfo.getAverageSalesQty()) < 0) {
                                                param5 = new BigDecimal(salesInfo.getAverageSalesQty().toString());
                                            }
                                            //计算调库量
                                            //计算调库量
                                            //调库量=库存量-MAX（90天 （参数2）销量，2（参数4）），且调库量 ≥1（参数6）的整数(向下取整)
                                            suggestionReturnQty = new BigDecimal(good.getInventoryQty().toString()).subtract(param5).setScale(0, RoundingMode.DOWN);
                                            if (suggestionReturnQty.compareTo(param7) > 0) {
                                                GaiaCommodityInventoryD gaiaCommodityInventoryD = new GaiaCommodityInventoryD();
                                                setDetail(now, client, param2, storeInfo, billCode, good, salesInfo, suggestionReturnQty, gaiaCommodityInventoryD);
                                                insertDList.add(gaiaCommodityInventoryD);
                                            }
                                        }
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag) {
                                    //查询商品的 90天销量，90天销售额，90天毛利额，
                                    BigDecimal suggestionReturnQty = BigDecimal.ZERO;
                                    GaiaCommodityInventoryDOut salesInfo = new GaiaCommodityInventoryDOut();
                                    if (good.getCost().compareTo(param3) >= 0) {
                                        //计算调库量
                                        //调库量=库存量-MAX（90天 （参数2）销量，2（参数4）），且调库量 ≥1（参数6）的整数(向下取整)
                                        suggestionReturnQty = new BigDecimal(good.getInventoryQty().toString()).subtract(param4).setScale(0, RoundingMode.DOWN);
                                        if (suggestionReturnQty.compareTo(param6) >= 0) {
                                            GaiaCommodityInventoryD gaiaCommodityInventoryD = new GaiaCommodityInventoryD();
                                            setDetail(now, client, param2, storeInfo, billCode, good, salesInfo, suggestionReturnQty, gaiaCommodityInventoryD);
                                            insertDList.add(gaiaCommodityInventoryD);
                                        }
                                    }//如果成本价 < 100
                                    else {
                                        //计算调库量
                                        //计算调库量
                                        //调库量=库存量-MAX（90天 （参数2）销量，2（参数4）），且调库量 ≥1（参数6）的整数(向下取整)
                                        suggestionReturnQty = new BigDecimal(good.getInventoryQty().toString()).subtract(param5).setScale(0, RoundingMode.DOWN);
                                        if (suggestionReturnQty.compareTo(param7) > 0) {
                                            GaiaCommodityInventoryD gaiaCommodityInventoryD = new GaiaCommodityInventoryD();
                                            setDetail(now, client, param2, storeInfo, billCode, good, salesInfo, suggestionReturnQty, gaiaCommodityInventoryD);
                                            insertDList.add(gaiaCommodityInventoryD);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!CollectionUtils.isEmpty(insertDList)) {
                        BigDecimal param7 = new BigDecimal("1");
                        BigDecimal param8 = new BigDecimal("10");
                        checkParam(para4, param7, param8);
                        GaiaCommodityInventoryH inventoryH = new GaiaCommodityInventoryH();
                        setHead(now, client, param1, insertDList, billCode, param8, inventoryH);
                        gaiaCommodityInventoryHMapper.insert(inventoryH);
                        gaiaCommodityInventoryDMapper.insertBatch(insertDList);
                        //插入首页消息
                        GaiaSdMessage sdMessage = new GaiaSdMessage();
                        sdMessage.setClient(inventoryH.getClient());
                        sdMessage.setGsmId("product");//针对所有加盟商
                        String voucherId = gaiaSdMessageMapper.selectNextVoucherId(inventoryH.getClient(), "company");
                        sdMessage.setGsmVoucherId(voucherId);//消息流水号
                        sdMessage.setGsmPage(SdMessageTypeEnum.COMMODITY_INVENTORY_ADJUSTMENT.page);
                        sdMessage.setGsmType(SdMessageTypeEnum.COMMODITY_INVENTORY_ADJUSTMENT.code);
                        sdMessage.setGsmRemark("您有<font color=\"#FF0000\">"+DateUtil.dateToString(inventoryH.getBillDate(),"yyyy年MM月")+"商品调库</font>需确认，请于" + DateUtil.dateToString(inventoryH.getInvalidDate(),"MM月dd日")+"前完成确认。");
                        sdMessage.setGsmFlag("N");//是否查看
                        sdMessage.setGsmPlatForm("WEB");//消息渠道
                        sdMessage.setGsmDeleteFlag("0");
                        sdMessage.setGsmArriveDate(com.gys.util.DateUtil.formatDate(now));
                        sdMessage.setGsmArriveTime(com.gys.util.DateUtil.dateToString(now, "HHmmss"));
                        gaiaSdMessageMapper.insertSelective(sdMessage);
                    }
                }
            }
        }
        log.info(String.format("<商品调库><定时任务><结束时间：%s>", LocalDateTime.now()));
        return true;
    }

    @Override
    public Object listBill(GaiaCommodityInventoryHInData param) {
        return gaiaCommodityInventoryHMapper.listBill(param);
    }

    @Override
    public Object listBillDetail(GaiaCommodityInventoryHInData param) {
        PageInfo<GaiaCommodityInventoryDOut> pageInfo = new PageInfo<>();
        List<GaiaCommodityInventoryDOut> outList = gaiaCommodityInventoryHMapper.listBillDetail(param);
        pageInfo.setList(outList);
        if (!CollectionUtils.isEmpty(outList)) {
            GaiaCommodityInventoryDOut total = gaiaCommodityInventoryHMapper.getTotal(param);
            total.setAverageSalesGrossRate(total.getAverageSalesGross().divide(total.getAverageSalesAmt(), 2, RoundingMode.HALF_UP));
            total.setStoCode("合计");
            pageInfo.setListNum(total);
        }
        return pageInfo;
    }

    @Override
    public Object listUnSendStore(GaiaCommodityInventoryHInData param) {
        return gaiaCommodityInventoryHMapper.listUnSendStore(param);
    }

    @Override
    public Object listStoreLine(GaiaCommodityInventoryHInData param) {
        return gaiaCommodityInventoryHMapper.listStoreLine(param);
    }

    @Override
    @Transactional
    public Result exportBillDetail(GaiaCommodityInventoryHInData param) {
        List<GaiaCommodityInventoryDOut> outList = gaiaCommodityInventoryHMapper.listBillDetail(param);
        String fileName = "调库单明细";
        gaiaCommodityInventoryHMapper.updateExportStatus(param.getClient(),param.getBillCode(),"1");
        if (outList.size() > 0) {
            CsvFileInfo csvInfo =null;
            // 导出
            // byte数据
            csvInfo = CsvClient.getCsvByte(outList,fileName, Collections.singletonList((short)1));

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据！");
        }
    }

    @Override
    @Transactional
    public void updateInventoryData(String param) {
        List<GaiaCommodityInventoryH> billList = gaiaCommodityInventoryHMapper.listUnSendBill();
        if (!CollectionUtils.isEmpty(billList)) {
            String currentDate = DateUtil.getCurrentDate();
            Date now = new Date();
            for (GaiaCommodityInventoryH gaiaCommodityInventoryH : billList) {
                if (currentDate.equals(DateUtil.formatDate(gaiaCommodityInventoryH.getInvalidDate())) || "sender".equals(param)) {
                    GaiaCommodityInventoryD param1 = new GaiaCommodityInventoryD();
                    param1.setBillCode(gaiaCommodityInventoryH.getBillCode());
                    param1.setClient(gaiaCommodityInventoryH.getClient());
                    param1.setStatus(1);
                    long count = gaiaCommodityInventoryDMapper.count(param1);
                    if (count > 0) {
                        gaiaCommodityInventoryH.setStatus(1);
                    } else {
                        gaiaCommodityInventoryH.setStatus(2);
                    }
                    gaiaCommodityInventoryH.setUpdateTime(now);
                    gaiaCommodityInventoryH.setUpdateUser("system");
                    gaiaCommodityInventoryH.setFinishTime(now);
                    gaiaCommodityInventoryHMapper.updateStatus(gaiaCommodityInventoryH);
                }
            }
        }
    }

    private void setHead(Date now, String client, String para1, List<GaiaCommodityInventoryD> insertDList, String billCode, BigDecimal param8, GaiaCommodityInventoryH inventoryH) {
        inventoryH.setClient(client);
        inventoryH.setBillCode(billCode);
        inventoryH.setBillDate(now);
        String invalidDay = "";
        if ("week".equals(para1)) {
            invalidDay = DateUtil.addDay(now, "yyyy-MM-dd", 5);
        } else {
            invalidDay = DateUtil.addDay(now, "yyyy-MM-dd", param8.intValue());
        }
        inventoryH.setInvalidDate(DateUtil.stringToDate(invalidDay,"yyyy-MM-dd"));
        inventoryH.setStatus(0);
        ArrayList<GaiaCommodityInventoryD> collect = insertDList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getStoCode() + ";" + o.getProSelfCode()))), ArrayList::new));
        inventoryH.setGoodsQty(collect.size());
        Map<String, List<GaiaCommodityInventoryD>> item = insertDList.stream().collect(Collectors.groupingBy(GaiaCommodityInventoryD::getProSelfCode));
        Set<String> proSelfCodeSet = item.keySet();
        inventoryH.setItemsQty(proSelfCodeSet.size());
        Map<String, List<GaiaCommodityInventoryD>> store = insertDList.stream().collect(Collectors.groupingBy(GaiaCommodityInventoryD::getStoCode));
        Set<String> storeCodeSet = store.keySet();
        inventoryH.setStoreQty(storeCodeSet.size());
        inventoryH.setIsDelete(0);
        inventoryH.setCreateTime(now);
        inventoryH.setCreateUser("system");
        inventoryH.setUpdateUser("system");
        inventoryH.setUpdateTime(now);
    }

    private void setDetail(Date now, String client, int param2, HashMap<String, String> storeInfo, String billCode, GaiaCommodityInventoryDOut good, GaiaCommodityInventoryDOut salesInfo, BigDecimal suggestionReturnQty, GaiaCommodityInventoryD gaiaCommodityInventoryD) {
        gaiaCommodityInventoryD.setClient(client);
        gaiaCommodityInventoryD.setStoCode(good.getStoCode());
        gaiaCommodityInventoryD.setStoName(storeInfo.get("stoName"));
        gaiaCommodityInventoryD.setBillCode(billCode);
        gaiaCommodityInventoryD.setProName(good.getProName());
        gaiaCommodityInventoryD.setProSelfCode(good.getProSelfCode());
        gaiaCommodityInventoryD.setProSpces(good.getProSpces());
        gaiaCommodityInventoryD.setProFactoryName(good.getProFactoryName());
        gaiaCommodityInventoryD.setProUnit(good.getProUnit());
        gaiaCommodityInventoryD.setAverageSalesQty(ObjectUtil.isNotEmpty(salesInfo) ? Util.divide(Util.checkNull(salesInfo.getAverageSalesQty()), new BigDecimal(param2)) : BigDecimal.ZERO);
        gaiaCommodityInventoryD.setInventoryQty(good.getInventoryQty());
        gaiaCommodityInventoryD.setSuggestionReturnQty(suggestionReturnQty);
        gaiaCommodityInventoryD.setSuggestionReturnCost(suggestionReturnQty.multiply(Util.checkNull(good.getCost().doubleValue())));
        gaiaCommodityInventoryD.setAverageSalesAmt(ObjectUtil.isNotEmpty(salesInfo) ? Util.divide(Util.checkNull(salesInfo.getAverageSalesAmt()), new BigDecimal(param2)): BigDecimal.ZERO);
        gaiaCommodityInventoryD.setAverageSalesGross(ObjectUtil.isNotEmpty(salesInfo) ? Util.divide(Util.checkNull(salesInfo.getAverageSalesGross()), new BigDecimal(param2)): BigDecimal.ZERO);
        gaiaCommodityInventoryD.setAverageSalesGrossRate(Util.divide(Util.checkNull(gaiaCommodityInventoryD.getAverageSalesGross()), Util.checkNull(gaiaCommodityInventoryD.getAverageSalesAmt())));
        gaiaCommodityInventoryD.setProCompclass(good.getProCompclass());
        gaiaCommodityInventoryD.setProClass(good.getProClass());
        gaiaCommodityInventoryD.setStatus(0);
        gaiaCommodityInventoryD.setIsDelete(0);
        gaiaCommodityInventoryD.setCreateTime(now);
        gaiaCommodityInventoryD.setCreateUser("system");
        gaiaCommodityInventoryD.setUpdateTime(now);
        gaiaCommodityInventoryD.setUpdateUser("system");
    }

    private void checkParam(GaiaClSystemPara para2,BigDecimal param3,BigDecimal param4) {
        if (ObjectUtil.isNotEmpty(para2) && ObjectUtil.isNotEmpty(para2.getGcspPara1())) {
            param3 = new BigDecimal(para2.getGcspPara1());
        }
        if (ObjectUtil.isNotEmpty(para2) && ObjectUtil.isNotEmpty(para2.getGcspPara2())) {
            param4 = new BigDecimal(para2.getGcspPara2());
        }
    }

    private Boolean checkEnbaleFlag(Date now, GaiaClSystemPara para1, Boolean enable ,int param2 ,String param1) {
        if (ObjectUtil.isNotEmpty(para1) && "week".equals(para1.getGcspPara1())) {
            param1 = "week";
            int whichDay = DateUtil.getWhichDay(now);
            if (1 == whichDay) {
                enable = true;
            }
            if (ObjectUtil.isNotEmpty(para1.getGcspPara2())) {
                param2 = Integer.parseInt(para1.getGcspPara2());
            }
        } else {
            String date = DateUtil.formatDate(now);
            String day = date.substring(6, 8);
            if ("11".equals(day)) {
                enable = true;
            }
        }
        return enable;
    }
}
