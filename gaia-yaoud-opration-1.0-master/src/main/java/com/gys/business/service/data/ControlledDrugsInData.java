package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("管制药品销售信息")
public class ControlledDrugsInData {
    private String client;
    private String brId;
    @ApiModelProperty("销售单号")
    private String gssdBillNo;
    @ApiModelProperty("销售时间")
    private String gssdDate;
    @ApiModelProperty("商品编码")
    private String gssdProId;
    @ApiModelProperty("登记人员")
    private String gsshEmp;
    @ApiModelProperty("特殊药品购买人姓名")
    private String gssdSpecialmedName;
    @ApiModelProperty("特殊药品购买人出生日期")
    private String gssdSpecialmedBirthday;
    @ApiModelProperty("特殊药品购买人身份证")
    private String gssdSpecialmedIdcard;
    @ApiModelProperty("特殊药品购买人手机号")
    private String gssdSpecialmedMobile;

}
