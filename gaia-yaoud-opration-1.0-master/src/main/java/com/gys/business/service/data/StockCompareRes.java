package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class StockCompareRes implements Serializable {

    private static final long serialVersionUID = 8604408268363153830L;

    @ApiModelProperty(value = "加盟商code")
    private String client;

    @ApiModelProperty(value = "加盟商名称")
    private String francName;

    @ApiModelProperty(value = "地点（门店/仓库）code")
    private String siteCode;

    @ApiModelProperty(value = "地点（门店/仓库）名称")
    private String siteName;

    @ApiModelProperty(value = "对比日期")
    private String gscCreateDate;

    @ApiModelProperty(value = "对比时间")
    private String gscCreateTime;

    @ApiModelProperty(value = "对比条目数")
    private BigDecimal gscCompareRow;

    @ApiModelProperty(value = "差异条目数")
    private BigDecimal gscDiffRow;

    @ApiModelProperty(value = "推算数量")
    private BigDecimal gscCountQty;

    @ApiModelProperty(value = "实时数量")
    private BigDecimal gscStockQty;

    @ApiModelProperty(value = "差异数量")
    private BigDecimal gscDiffQty;

    @ApiModelProperty(value = "实时金额")
    private BigDecimal gscStockAmt;

    @ApiModelProperty(value = "推算金额")
    private BigDecimal gscCountAmt;

    @ApiModelProperty(value = "差异金额")
    private BigDecimal gscDiffAmt;

    @ApiModelProperty(value = "比对任务号")
    private String taskNo;

    @ApiModelProperty(value = "比对任务行号")
    private String taskItem;

    @ApiModelProperty(value = "带序号的对比任务号")
    private String taskNoWithItem;

    @ApiModelProperty(value = "最后更新时间")
    private Date lastUpdateTime;

    public String getTaskNoWithItem() {
        return this.taskNo + this.taskItem;
    }
}
