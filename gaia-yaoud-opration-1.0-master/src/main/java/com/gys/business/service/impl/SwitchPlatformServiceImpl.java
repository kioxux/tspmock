package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaSdMemberBasicMapper;
import com.gys.business.mapper.GaiaSdRechargeCardChangeMapper;
import com.gys.business.mapper.entity.GaiaSdMemberBasic;
import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import com.gys.business.mapper.entity.GaiaSdRechargeCard;
import com.gys.business.service.SwitchPlatformService;
import com.gys.business.service.data.GaiaSdMemberData;
import com.gys.business.service.data.GaiaSdMemberVO;
import com.gys.common.data.GetLoginOutData;
import com.gys.util.Util;
import com.gys.util.UtilMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xiaoyuan on 2020/12/2
 */
@Service
public class SwitchPlatformServiceImpl implements SwitchPlatformService {

    @Autowired
    private GaiaSdRechargeCardChangeMapper gaiaSdRechargeCardChangeMapper;


    @Autowired
    private GaiaSdMemberBasicMapper gaiaSdMemberBasicMapper;


    @Override
    public List<String> getTableName(GetLoginOutData userInfo) {
        String depId = userInfo.getDepId();
        String format = DateUtil.format(DateUtil.date(), UtilMessage.YYYYMMDD);
        List<String> outDate = new ArrayList<>();
        List<String> tableNameOne = this.gaiaSdRechargeCardChangeMapper.determineWhetherTheTableExists("GAIA_SD_MEMBER_BASIC");
        List<String> numberOne = tableNameOne.stream().map(Util::getTheSuffix).filter(NumberUtil::isNumber).collect(Collectors.toList());
        String basic = this.createTableBasic(numberOne, depId, format, "GAIA_SD_MEMBER_BASIC");

        List<String> tableNameTwo = this.gaiaSdRechargeCardChangeMapper.determineWhetherTheTableExists("GAIA_SD_MEMBER_CARD");
        List<String> numberTwo = tableNameTwo.stream().map(Util::getTheSuffix).filter(NumberUtil::isNumber).collect(Collectors.toList());
        String memberCard = this.createTableMemberCard(numberTwo, depId, format, "GAIA_SD_MEMBER_CARD");

        List<String> tableNameThree = this.gaiaSdRechargeCardChangeMapper.determineWhetherTheTableExists("GAIA_SD_RECHARGE_CARD");
        List<String> numberThree = tableNameThree.stream().map(Util::getTheSuffix).filter(NumberUtil::isNumber).collect(Collectors.toList());
        String recharge = this.createTableRecharge(numberThree, depId, format, "GAIA_SD_RECHARGE_CARD");


        if (StrUtil.isNotBlank(basic) && StrUtil.isNotBlank(memberCard) && StrUtil.isNotBlank(recharge)) {
            outDate.add(basic);
            outDate.add(memberCard);
            outDate.add(recharge);
        }
        return outDate;

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createTableStructure(GetLoginOutData userInfo, GaiaSdMemberData vo) {
        List<GaiaSdMemberBasic> basics = new ArrayList<>();
        List<GaiaSdMemberCardData> cardDatas = new ArrayList<>();
        List<GaiaSdRechargeCard> rechargeCards = new ArrayList<>();
        for (String tableName : vo.getTableNames()) {
            if (tableName.contains("GAIA_SD_MEMBER_BASIC")) {
                for (GaiaSdMemberVO memberVO : vo.getBusinessVOOne()) {
                    GaiaSdMemberBasic basic = new GaiaSdMemberBasic();
                    basic.setClientId(userInfo.getClient());
                    basic.setGsmbMemberId(memberVO.getGsmbMemberId());
                    basic.setGsmbName(memberVO.getName());
                    basic.setGsmbAddress(memberVO.getAddress());
                    basic.setGsmbMobile(memberVO.getContactNumber());
                    basic.setGsmbSex("1");
                    basic.setGsmbBirth(StrUtil.isBlank(memberVO.getBirthday()) ? DateUtil.format(DateUtil.date(), UtilMessage.YYYYMMDD) : DateUtil.format(DateUtil.parse(memberVO.getBirthday()), UtilMessage.YYYYMMDD));
                    basics.add(basic);
                }
                if (CollUtil.isNotEmpty(basics)) {
                    this.gaiaSdMemberBasicMapper.insertBanch(basics,tableName);
                }
            }

            if (tableName.contains("GAIA_SD_MEMBER_CARD")) {
                for (GaiaSdMemberVO sdMemberVO : vo.getBusinessVOOne()) {
                    GaiaSdMemberCardData cardData = new GaiaSdMemberCardData();
                    cardData.setClient(userInfo.getClient());
                    cardData.setGsmbcMemberId(sdMemberVO.getGsmbMemberId());
                    cardData.setGsmbcCardId(sdMemberVO.getGsmbMemberId());
                    cardData.setGsmbcBrId(userInfo.getDepId());
                    cardData.setGsmbcChannel("0");
                    cardData.setGsmbcClassId("5");
                    cardData.setGsmbcIntegral(StrUtil.isBlank(sdMemberVO.getIntegral()) ? "0" : sdMemberVO.getIntegral());
                    cardData.setGsmbcIntegralLastdate(StrUtil.isBlank(sdMemberVO.getLastTimeSpent()) ? DateUtil.format(DateUtil.date(), UtilMessage.YYYYMMDD) : DateUtil.format(DateUtil.parse(sdMemberVO.getLastTimeSpent()), UtilMessage.YYYYMMDD));
                    cardData.setGsmbcCreateDate(StrUtil.isBlank(sdMemberVO.getOpeningTime()) ? DateUtil.format(DateUtil.date(), UtilMessage.YYYYMMDD) : DateUtil.format(DateUtil.parse(sdMemberVO.getBirthday()), UtilMessage.YYYYMMDD));
                    cardData.setGsmbcStatus("1");
                    cardData.setGsmbcCreateSaler(userInfo.getUserId());
                    cardDatas.add(cardData);
                }
                if (CollUtil.isNotEmpty(cardDatas)) {
                    this.gaiaSdMemberBasicMapper.insertToCardBanch(cardDatas,tableName);
                }
            }

            if (tableName.contains("GAIA_SD_RECHARGE_CARD")) {
                for (GaiaSdMemberVO sdMemberVO : vo.getBusinessVOOne()) {
                    GaiaSdRechargeCard card = new GaiaSdRechargeCard();
                    card.setClientId(userInfo.getClient());
                    card.setGsrcAccountId(sdMemberVO.getGsmbMemberId());
                    card.setGsrcId(sdMemberVO.getGsmbMemberId());
                    card.setGsrcBrId(userInfo.getDepId());
                    card.setGsrcDate(DateUtil.format(DateUtil.date(), UtilMessage.YYYYMMDD));
                    card.setGsrcEmp(userInfo.getUserId());
                    card.setGsrcStatus("1");
                    card.setGsrcName(sdMemberVO.getName());
                    card.setGsrcSex("1");
                    card.setGsrcMobile(sdMemberVO.getContactNumber());
                    card.setGsrcAddress(sdMemberVO.getAddress());
                    String balance = sdMemberVO.getBalance();
                    if (StrUtil.isBlank(balance) || !Util.isNumbers(balance)) {
                        card.setGsrcAmt(BigDecimal.ZERO);
                    } else {
                        card.setGsrcAmt(new BigDecimal(balance));
                    }

                    card.setGsrcMemberCardId(sdMemberVO.getGsmbMemberId());
                    rechargeCards.add(card);
                }
                if (CollUtil.isNotEmpty(rechargeCards)) {
                    this.gaiaSdRechargeCardChangeMapper.insertBanch(rechargeCards,tableName);
                }
            }
        }


    }

    public String createTableRecharge(List<String> numbers, String depId, String format, String table) {
        String flag = "";
        String tableName = "";
        if (CollUtil.isEmpty(numbers)) {
            tableName = depId + "_" + table + "_" + format + "_" + UtilMessage.One;
            int recharge = this.gaiaSdRechargeCardChangeMapper.createTableRecharge(tableName);
            if (recharge == 0) {
                flag = tableName;
            }
        } else {
            Long aLong = numbers.stream().map(e -> Long.parseLong(e)).sorted().max(Long::compareTo).get();
            long l = aLong + 1;
            tableName = depId + "_" + table + "_" + format + "_" + l;
            int recharge = this.gaiaSdRechargeCardChangeMapper.createTableRecharge(tableName);
            if (recharge == 0) {
                flag = tableName;
            }
        }
        return flag;
    }

    public String createTableMemberCard(List<String> numbers, String depId, String format, String table) {
        String flag = "";
        String tableName = "";
        if (CollUtil.isEmpty(numbers)) {
            tableName = depId + "_" + table + "_" + format + "_" + UtilMessage.One;
            int memberCard = this.gaiaSdRechargeCardChangeMapper.createTableMemberCard(tableName);
            if (memberCard == 0) {
                flag = tableName;
            }
        } else {
            Long aLong = numbers.stream().map(e -> Long.parseLong(e)).sorted().max(Long::compareTo).get();
            long l = aLong + 1;
            tableName = depId + "_" + table + "_" + format + "_" + l;
            int memberCard = this.gaiaSdRechargeCardChangeMapper.createTableMemberCard(tableName);
            if (memberCard == 0) {
                flag = tableName;
            }

        }
        return flag;
    }

    public String createTableBasic(List<String> numbers, String depId, String format, String table) {
        String flag = "";
        String tableName = "";
        if (CollUtil.isEmpty(numbers)) {
            tableName = depId + "_" + table + "_" + format + "_" + UtilMessage.One;
            int tableBasic = this.gaiaSdRechargeCardChangeMapper.createTableBasic(tableName);
            if (tableBasic == 0) {
                flag = tableName;
            }
        } else {
            Long aLong = numbers.stream().map(e -> Long.parseLong(e)).sorted().max(Long::compareTo).get();
            long l = aLong + 1;
            tableName = depId + "_" + table + "_" + format + "_" + l;
            int tableBasic = this.gaiaSdRechargeCardChangeMapper.createTableBasic(tableName);
            if (tableBasic == 0) {
                flag = tableName;
            }
        }
        return flag;
    }
}
