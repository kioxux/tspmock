//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class GetProImportExcelInData {
    private List<importProductExcelInData> importExcelInData;
    private GetUserOutData user;
    private String clientId;

    public GetProImportExcelInData() {
    }

    public List<importProductExcelInData> getImportExcelInData() {
        return this.importExcelInData;
    }

    public GetUserOutData getUser() {
        return this.user;
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setImportExcelInData(final List<importProductExcelInData> importExcelInData) {
        this.importExcelInData = importExcelInData;
    }

    public void setUser(final GetUserOutData user) {
        this.user = user;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetProImportExcelInData)) {
            return false;
        } else {
            GetProImportExcelInData other = (GetProImportExcelInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$importExcelInData = this.getImportExcelInData();
                    Object other$importExcelInData = other.getImportExcelInData();
                    if (this$importExcelInData == null) {
                        if (other$importExcelInData == null) {
                            break label47;
                        }
                    } else if (this$importExcelInData.equals(other$importExcelInData)) {
                        break label47;
                    }

                    return false;
                }

                Object this$user = this.getUser();
                Object other$user = other.getUser();
                if (this$user == null) {
                    if (other$user != null) {
                        return false;
                    }
                } else if (!this$user.equals(other$user)) {
                    return false;
                }

                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetProImportExcelInData;
    }

    public int hashCode() {

        int result = 1;
        Object $importExcelInData = this.getImportExcelInData();
        result = result * 59 + ($importExcelInData == null ? 43 : $importExcelInData.hashCode());
        Object $user = this.getUser();
        result = result * 59 + ($user == null ? 43 : $user.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        return result;
    }

    public String toString() {
        return "GetProImportExcelInData(importExcelInData=" + this.getImportExcelInData() + ", user=" + this.getUser() + ", clientId=" + this.getClientId() + ")";
    }
}
