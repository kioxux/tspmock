package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.mapper.entity.GaiaStoreInSuggestionH;
import com.gys.business.mapper.entity.GaiaStoreOutSuggestionH;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.SdMessageTypeEnum;

import java.util.List;

/**
* @author xiayuan
* @date  2020-12-23
*/
public interface GaiaSdMessageService {

    /**
     * 获取列表
     * @param userInfo
     * @return
     */
    List<GaiaSdMessage> getAll(GetLoginOutData userInfo,GaiaSdMessage inData);

    /**
     * 修改
     * @param userInfo
     * @param inData
     */
    void updateMessage(GetLoginOutData userInfo, GaiaSdMessage inData);

    void timerUpdateMessage();

    void addMessage(GaiaSdMessage gaiaSdMessage);

    /**
     * 发送门店调剂建议消息
     * @param storeInSuggestionConfirm 消息类型
     * @param inSuggestionHList 门店调剂建议调出对象列表
     * @param inSuggestionH 门店调剂建议调入对象
     */
    void sendStoreSuggestionMessage(SdMessageTypeEnum storeInSuggestionConfirm, List<GaiaStoreInSuggestionH> inSuggestionHList, GaiaStoreInSuggestionH inSuggestionH);

}
