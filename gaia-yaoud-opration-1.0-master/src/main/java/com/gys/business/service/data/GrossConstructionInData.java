package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 16:15 2021/9/18
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class GrossConstructionInData {
    /**
     * 月份
     */
    @NotBlank(message = "时间不能为空！")
    @Size(min = 6,max = 6,message = "时间只能包含年和月！")
    private String time;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 大类Code
     */
    private String bigClassCode;

    /**
     * 排序字段 1：定价毛利率、2：销售毛利率、3：毛利贡献率、4：折扣率、5：动销品项、6：销售额
     */
    private String orderCode;

    /**
     * 排序方式 0:降序 1:升序
     */
    private String orderType;

    /**
     * 是否开启毛利区间 1:开启 0:不开启
     */
    private String isGrossInterval;
}
