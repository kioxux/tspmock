package com.gys.business.service;


import com.gys.business.mapper.entity.GaiaStoreDiscountData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.response.Result;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface GaiaStoreDiscountService {
    List<String> getStoTaxRateList(String client);

    List<Map<String,Object>> getStoCodeList(Map<String,Object> inData);

    Map<String, Object> selectDiscouList(GaiaStoreDiscountData storeBudgetPlanData);

    Result exportDiscoun(GetLoginOutData userInfo, GaiaStoreDiscountData gaiaStoreDiscountData);

    Boolean insertStore(GetLoginOutData loginUser, List<GaiaStoreDiscountData> gaiaStoreDiscountDataList);

    Object importStoreDiscoun(MultipartFile file, GetLoginOutData userInfo);

    String updateStoRebate(GetLoginOutData userInfo, GaiaStoreDiscountData gaiaStoreDiscountData);
}
