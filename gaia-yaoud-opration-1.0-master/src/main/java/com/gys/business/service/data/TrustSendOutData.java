package com.gys.business.service.data;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Api(value = "委托配送单参数")
public class TrustSendOutData {
    @ApiModelProperty(value = "委托配送单号")
    private String gswhVoucherId;
    @ApiModelProperty(value = "配送日期")
    private String gswhDate;
    @ApiModelProperty(value = "收货日期")
    private String gswhAcceptDate;
    @ApiModelProperty(value = "供应商编码")
    private String gswhSupId;
    @ApiModelProperty(value = "发货地点")
    private String supName;
    @ApiModelProperty(value = "收货地点")
    private String stoName;
    @ApiModelProperty(value = "收货状态 0 未收货 1 已收货")
    private String gswhStatus;
    @ApiModelProperty(value = "收货状态名")
    private String gswhStatusName;
    @ApiModelProperty(value = "收货总金额")
    private BigDecimal gswhAmt;
    @ApiModelProperty(value = "收货总数量")
    private BigDecimal gswhQty;
    @ApiModelProperty(value = "拒收状态")
    private String gswhRefuseStatus;
}
