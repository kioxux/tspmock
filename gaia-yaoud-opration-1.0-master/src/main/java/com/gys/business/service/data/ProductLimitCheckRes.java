package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductLimitCheckRes implements Serializable {
    private static final long serialVersionUID = -5192070737528663933L;

    private Integer count;

    private String proId;

    private String proCommonName;
}
