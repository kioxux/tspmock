//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "药品养护商品信息")
public class GetMedCheckDOutData {
    @ApiModelProperty(value = "商品编码")
    private String gsmcdProId;
    @ApiModelProperty(value = "商品名称")
    private String proName;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "单位")
    private String proUnit;
    @ApiModelProperty(value = "批号")
    private String gsmcdBatchNo;
    @ApiModelProperty(value = "")
    private String vaild;
    @ApiModelProperty(value = "数量")
    private String stockQty;
    @ApiModelProperty(value = "效期")
    private String gssbVaildDate;
    @ApiModelProperty(value = "养护数量")
    private String gsmcdQty;
    @ApiModelProperty(value = "养护结论")
    private String gsmcdResult;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "剂型")
    private String proForm;

    public GetMedCheckDOutData() {
    }

    public String getGsmcdProId() {
        return this.gsmcdProId;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getGsmcdBatchNo() {
        return this.gsmcdBatchNo;
    }

    public String getVaild() {
        return this.vaild;
    }

    public String getStockQty() {
        return this.stockQty;
    }

    public String getGssbVaildDate() {
        return this.gssbVaildDate;
    }

    public String getGsmcdQty() {
        return this.gsmcdQty;
    }

    public String getGsmcdResult() {
        return this.gsmcdResult;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProForm() {
        return this.proForm;
    }

    public void setGsmcdProId(final String gsmcdProId) {
        this.gsmcdProId = gsmcdProId;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setGsmcdBatchNo(final String gsmcdBatchNo) {
        this.gsmcdBatchNo = gsmcdBatchNo;
    }

    public void setVaild(final String vaild) {
        this.vaild = vaild;
    }

    public void setStockQty(final String stockQty) {
        this.stockQty = stockQty;
    }

    public void setGssbVaildDate(final String gssbVaildDate) {
        this.gssbVaildDate = gssbVaildDate;
    }

    public void setGsmcdQty(final String gsmcdQty) {
        this.gsmcdQty = gsmcdQty;
    }

    public void setGsmcdResult(final String gsmcdResult) {
        this.gsmcdResult = gsmcdResult;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetMedCheckDOutData)) {
            return false;
        } else {
            GetMedCheckDOutData other = (GetMedCheckDOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label167: {
                    Object this$gsmcdProId = this.getGsmcdProId();
                    Object other$gsmcdProId = other.getGsmcdProId();
                    if (this$gsmcdProId == null) {
                        if (other$gsmcdProId == null) {
                            break label167;
                        }
                    } else if (this$gsmcdProId.equals(other$gsmcdProId)) {
                        break label167;
                    }

                    return false;
                }

                Object this$proName = this.getProName();
                Object other$proName = other.getProName();
                if (this$proName == null) {
                    if (other$proName != null) {
                        return false;
                    }
                } else if (!this$proName.equals(other$proName)) {
                    return false;
                }

                label153: {
                    Object this$proSpecs = this.getProSpecs();
                    Object other$proSpecs = other.getProSpecs();
                    if (this$proSpecs == null) {
                        if (other$proSpecs == null) {
                            break label153;
                        }
                    } else if (this$proSpecs.equals(other$proSpecs)) {
                        break label153;
                    }

                    return false;
                }

                Object this$proUnit = this.getProUnit();
                Object other$proUnit = other.getProUnit();
                if (this$proUnit == null) {
                    if (other$proUnit != null) {
                        return false;
                    }
                } else if (!this$proUnit.equals(other$proUnit)) {
                    return false;
                }

                label139: {
                    Object this$gsmcdBatchNo = this.getGsmcdBatchNo();
                    Object other$gsmcdBatchNo = other.getGsmcdBatchNo();
                    if (this$gsmcdBatchNo == null) {
                        if (other$gsmcdBatchNo == null) {
                            break label139;
                        }
                    } else if (this$gsmcdBatchNo.equals(other$gsmcdBatchNo)) {
                        break label139;
                    }

                    return false;
                }

                Object this$vaild = this.getVaild();
                Object other$vaild = other.getVaild();
                if (this$vaild == null) {
                    if (other$vaild != null) {
                        return false;
                    }
                } else if (!this$vaild.equals(other$vaild)) {
                    return false;
                }

                label125: {
                    Object this$stockQty = this.getStockQty();
                    Object other$stockQty = other.getStockQty();
                    if (this$stockQty == null) {
                        if (other$stockQty == null) {
                            break label125;
                        }
                    } else if (this$stockQty.equals(other$stockQty)) {
                        break label125;
                    }

                    return false;
                }

                label118: {
                    Object this$gssbVaildDate = this.getGssbVaildDate();
                    Object other$gssbVaildDate = other.getGssbVaildDate();
                    if (this$gssbVaildDate == null) {
                        if (other$gssbVaildDate == null) {
                            break label118;
                        }
                    } else if (this$gssbVaildDate.equals(other$gssbVaildDate)) {
                        break label118;
                    }

                    return false;
                }

                Object this$gsmcdQty = this.getGsmcdQty();
                Object other$gsmcdQty = other.getGsmcdQty();
                if (this$gsmcdQty == null) {
                    if (other$gsmcdQty != null) {
                        return false;
                    }
                } else if (!this$gsmcdQty.equals(other$gsmcdQty)) {
                    return false;
                }

                label104: {
                    Object this$gsmcdResult = this.getGsmcdResult();
                    Object other$gsmcdResult = other.getGsmcdResult();
                    if (this$gsmcdResult == null) {
                        if (other$gsmcdResult == null) {
                            break label104;
                        }
                    } else if (this$gsmcdResult.equals(other$gsmcdResult)) {
                        break label104;
                    }

                    return false;
                }

                label97: {
                    Object this$proFactoryName = this.getProFactoryName();
                    Object other$proFactoryName = other.getProFactoryName();
                    if (this$proFactoryName == null) {
                        if (other$proFactoryName == null) {
                            break label97;
                        }
                    } else if (this$proFactoryName.equals(other$proFactoryName)) {
                        break label97;
                    }

                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                Object this$proForm = this.getProForm();
                Object other$proForm = other.getProForm();
                if (this$proForm == null) {
                    if (other$proForm != null) {
                        return false;
                    }
                } else if (!this$proForm.equals(other$proForm)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetMedCheckDOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gsmcdProId = this.getGsmcdProId();
        result = result * 59 + ($gsmcdProId == null ? 43 : $gsmcdProId.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $gsmcdBatchNo = this.getGsmcdBatchNo();
        result = result * 59 + ($gsmcdBatchNo == null ? 43 : $gsmcdBatchNo.hashCode());
        Object $vaild = this.getVaild();
        result = result * 59 + ($vaild == null ? 43 : $vaild.hashCode());
        Object $stockQty = this.getStockQty();
        result = result * 59 + ($stockQty == null ? 43 : $stockQty.hashCode());
        Object $gssbVaildDate = this.getGssbVaildDate();
        result = result * 59 + ($gssbVaildDate == null ? 43 : $gssbVaildDate.hashCode());
        Object $gsmcdQty = this.getGsmcdQty();
        result = result * 59 + ($gsmcdQty == null ? 43 : $gsmcdQty.hashCode());
        Object $gsmcdResult = this.getGsmcdResult();
        result = result * 59 + ($gsmcdResult == null ? 43 : $gsmcdResult.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        return result;
    }

    public String toString() {
        return "GetMedCheckDOutData(gsmcdProId=" + this.getGsmcdProId() + ", proName=" + this.getProName() + ", proSpecs=" + this.getProSpecs() + ", proUnit=" + this.getProUnit() + ", gsmcdBatchNo=" + this.getGsmcdBatchNo() + ", vaild=" + this.getVaild() + ", stockQty=" + this.getStockQty() + ", gssbVaildDate=" + this.getGssbVaildDate() + ", gsmcdQty=" + this.getGsmcdQty() + ", gsmcdResult=" + this.getGsmcdResult() + ", proFactoryName=" + this.getProFactoryName() + ", proPlace=" + this.getProPlace() + ", proForm=" + this.getProForm() + ")";
    }
}
