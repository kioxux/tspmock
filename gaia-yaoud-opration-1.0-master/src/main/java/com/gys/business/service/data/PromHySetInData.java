package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromHySetInData implements Serializable {
    private static final long serialVersionUID = -2860889067540297570L;
    private String clientId;
    private String gsphsVoucherId;
    private String gsphsSerial;
    private String gsphsProId;
    private BigDecimal gsphsPrice;
    private String gsphsRebate;
    private String gsphsInteFlag;
    private String gsphsInteRate;
}
