package com.gys.business.service.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockSyncData {
    private String client;
    private String site;
    private List<String> params;
    //类型
    private String type;
}
