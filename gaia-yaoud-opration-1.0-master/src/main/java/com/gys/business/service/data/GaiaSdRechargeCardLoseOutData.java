//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GaiaSdRechargeCardLoseOutData {
    private String clientId;
    private String brName;
    private String gsrclVoucherId;
    private String gsrclStatus;
    private String gsrclBrId;
    private String gsrclDate;
    private String gsrclTime;
    private String gsrclEmp;
    private String gsrclCardId;
    private String gsrclReason;

    public GaiaSdRechargeCardLoseOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrName() {
        return this.brName;
    }

    public String getGsrclVoucherId() {
        return this.gsrclVoucherId;
    }

    public String getGsrclStatus() {
        return this.gsrclStatus;
    }

    public String getGsrclBrId() {
        return this.gsrclBrId;
    }

    public String getGsrclDate() {
        return this.gsrclDate;
    }

    public String getGsrclTime() {
        return this.gsrclTime;
    }

    public String getGsrclEmp() {
        return this.gsrclEmp;
    }

    public String getGsrclCardId() {
        return this.gsrclCardId;
    }

    public String getGsrclReason() {
        return this.gsrclReason;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrName(final String brName) {
        this.brName = brName;
    }

    public void setGsrclVoucherId(final String gsrclVoucherId) {
        this.gsrclVoucherId = gsrclVoucherId;
    }

    public void setGsrclStatus(final String gsrclStatus) {
        this.gsrclStatus = gsrclStatus;
    }

    public void setGsrclBrId(final String gsrclBrId) {
        this.gsrclBrId = gsrclBrId;
    }

    public void setGsrclDate(final String gsrclDate) {
        this.gsrclDate = gsrclDate;
    }

    public void setGsrclTime(final String gsrclTime) {
        this.gsrclTime = gsrclTime;
    }

    public void setGsrclEmp(final String gsrclEmp) {
        this.gsrclEmp = gsrclEmp;
    }

    public void setGsrclCardId(final String gsrclCardId) {
        this.gsrclCardId = gsrclCardId;
    }

    public void setGsrclReason(final String gsrclReason) {
        this.gsrclReason = gsrclReason;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GaiaSdRechargeCardLoseOutData)) {
            return false;
        } else {
            GaiaSdRechargeCardLoseOutData other = (GaiaSdRechargeCardLoseOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$brName = this.getBrName();
                Object other$brName = other.getBrName();
                if (this$brName == null) {
                    if (other$brName != null) {
                        return false;
                    }
                } else if (!this$brName.equals(other$brName)) {
                    return false;
                }

                Object this$gsrclVoucherId = this.getGsrclVoucherId();
                Object other$gsrclVoucherId = other.getGsrclVoucherId();
                if (this$gsrclVoucherId == null) {
                    if (other$gsrclVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsrclVoucherId.equals(other$gsrclVoucherId)) {
                    return false;
                }

                label110: {
                    Object this$gsrclStatus = this.getGsrclStatus();
                    Object other$gsrclStatus = other.getGsrclStatus();
                    if (this$gsrclStatus == null) {
                        if (other$gsrclStatus == null) {
                            break label110;
                        }
                    } else if (this$gsrclStatus.equals(other$gsrclStatus)) {
                        break label110;
                    }

                    return false;
                }

                label103: {
                    Object this$gsrclBrId = this.getGsrclBrId();
                    Object other$gsrclBrId = other.getGsrclBrId();
                    if (this$gsrclBrId == null) {
                        if (other$gsrclBrId == null) {
                            break label103;
                        }
                    } else if (this$gsrclBrId.equals(other$gsrclBrId)) {
                        break label103;
                    }

                    return false;
                }

                Object this$gsrclDate = this.getGsrclDate();
                Object other$gsrclDate = other.getGsrclDate();
                if (this$gsrclDate == null) {
                    if (other$gsrclDate != null) {
                        return false;
                    }
                } else if (!this$gsrclDate.equals(other$gsrclDate)) {
                    return false;
                }

                label89: {
                    Object this$gsrclTime = this.getGsrclTime();
                    Object other$gsrclTime = other.getGsrclTime();
                    if (this$gsrclTime == null) {
                        if (other$gsrclTime == null) {
                            break label89;
                        }
                    } else if (this$gsrclTime.equals(other$gsrclTime)) {
                        break label89;
                    }

                    return false;
                }

                label82: {
                    Object this$gsrclEmp = this.getGsrclEmp();
                    Object other$gsrclEmp = other.getGsrclEmp();
                    if (this$gsrclEmp == null) {
                        if (other$gsrclEmp == null) {
                            break label82;
                        }
                    } else if (this$gsrclEmp.equals(other$gsrclEmp)) {
                        break label82;
                    }

                    return false;
                }

                Object this$gsrclCardId = this.getGsrclCardId();
                Object other$gsrclCardId = other.getGsrclCardId();
                if (this$gsrclCardId == null) {
                    if (other$gsrclCardId != null) {
                        return false;
                    }
                } else if (!this$gsrclCardId.equals(other$gsrclCardId)) {
                    return false;
                }

                Object this$gsrclReason = this.getGsrclReason();
                Object other$gsrclReason = other.getGsrclReason();
                if (this$gsrclReason == null) {
                    if (other$gsrclReason != null) {
                        return false;
                    }
                } else if (!this$gsrclReason.equals(other$gsrclReason)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GaiaSdRechargeCardLoseOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brName = this.getBrName();
        result = result * 59 + ($brName == null ? 43 : $brName.hashCode());
        Object $gsrclVoucherId = this.getGsrclVoucherId();
        result = result * 59 + ($gsrclVoucherId == null ? 43 : $gsrclVoucherId.hashCode());
        Object $gsrclStatus = this.getGsrclStatus();
        result = result * 59 + ($gsrclStatus == null ? 43 : $gsrclStatus.hashCode());
        Object $gsrclBrId = this.getGsrclBrId();
        result = result * 59 + ($gsrclBrId == null ? 43 : $gsrclBrId.hashCode());
        Object $gsrclDate = this.getGsrclDate();
        result = result * 59 + ($gsrclDate == null ? 43 : $gsrclDate.hashCode());
        Object $gsrclTime = this.getGsrclTime();
        result = result * 59 + ($gsrclTime == null ? 43 : $gsrclTime.hashCode());
        Object $gsrclEmp = this.getGsrclEmp();
        result = result * 59 + ($gsrclEmp == null ? 43 : $gsrclEmp.hashCode());
        Object $gsrclCardId = this.getGsrclCardId();
        result = result * 59 + ($gsrclCardId == null ? 43 : $gsrclCardId.hashCode());
        Object $gsrclReason = this.getGsrclReason();
        result = result * 59 + ($gsrclReason == null ? 43 : $gsrclReason.hashCode());
        return result;
    }

    public String toString() {
        return "GaiaSdRechargeCardLoseOutData(clientId=" + this.getClientId() + ", brName=" + this.getBrName() + ", gsrclVoucherId=" + this.getGsrclVoucherId() + ", gsrclStatus=" + this.getGsrclStatus() + ", gsrclBrId=" + this.getGsrclBrId() + ", gsrclDate=" + this.getGsrclDate() + ", gsrclTime=" + this.getGsrclTime() + ", gsrclEmp=" + this.getGsrclEmp() + ", gsrclCardId=" + this.getGsrclCardId() + ", gsrclReason=" + this.getGsrclReason() + ")";
    }
}
