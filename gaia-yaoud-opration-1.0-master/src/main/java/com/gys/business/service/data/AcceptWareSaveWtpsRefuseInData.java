package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author ：gyx
 * @Date ：Created in 15:07 2021/10/29
 * @Description：门店收货(仓库)-委托配送收获单-明细数据保存拒收商品入参
 * @Modified By：gyx
 * @Version:
 */
@Data
public class AcceptWareSaveWtpsRefuseInData {

    private String client;

    private String stoCode;
    //配送单号
    @NotBlank(message = "配送单号不能为空")
    private String psVoucherId;
    //商品编码
    @NotBlank(message = "商品编码不能为空")
    private String proCode;
    //商品批次
    private String gswdBatch;
    //拒绝数量
    private String refuseQty;
    //拒绝理由
    private String refuseReason;


    @NotBlank(message = "到货数量不能为空")
    private String dhQty;
}
