package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "退库商品")
public class GetQueryProVoOutData implements Serializable {
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "门店编码")
    private String storeId;
    @ApiModelProperty(value = "门店名称")
    private String storeName;
    @ApiModelProperty(value = "商品名称")
    private String gspgProName;
    @ApiModelProperty(value = "商品编码")
    private String addGspgProName;
    @ApiModelProperty(value = "规格")
    private String gspgSpecs;
    @ApiModelProperty(value = "规格")
    private String addGspgSpecs;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "零售价")
    private String proPrice;
    @ApiModelProperty(value = "剂型")
    private String proForm;
    @ApiModelProperty(value = "计量单位")
    private String proUnit;
    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;
    @ApiModelProperty(value = "保质期")
    private String proLife;
    @ApiModelProperty(value = "中包装量")
    private String midPackage;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    @ApiModelProperty(value = "批次")
    private String batch;
    @ApiModelProperty(value = "库存数量")
    private String stockQty;
    @ApiModelProperty(value = "")
    private String allotQty;
    @ApiModelProperty(value = "有效期至")
    private String validDate;
    @ApiModelProperty(value = "采购凭证号")
    private String poId;
    @ApiModelProperty(value = "订单行号")
    private String poLineNo;
    @ApiModelProperty(value = "采购订单单价")
    private String poPrice;
    @ApiModelProperty(value = "税率")
    private String poRate;
    @ApiModelProperty(value = "助记码")
    private String pym;
}
