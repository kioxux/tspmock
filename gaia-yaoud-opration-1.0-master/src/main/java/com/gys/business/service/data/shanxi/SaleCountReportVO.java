package com.gys.business.service.data.shanxi;

import lombok.Data;

@Data
public class SaleCountReportVO {
    /**
     * 就诊类型
     */
    private String medType;
    /**
     * 人员类别
     */
    private String psnType;
    /**
     * 人员类别
     */
    private String psnType2;
    /**
     * 人次
     */
    private String personTime;
    /**
     * 医疗费用总金额
     */
    private String medfeeSumamt;
    /**
     * 人员账户支出
     */
    private String accPay;
    /**
     * 人员现金支出
     */
    private String psnCashPay;
    /**
     * 统筹基金支付
     */
    private String fundPaySumamt;
    /**
     * 大病支付
     */
    private String hifmiPay;
    /**
     * 公务员补助
     */
    private String cvlservPay;
}
