package com.gys.business.service.newProductsOrder;

import com.gys.business.service.data.ProductsOrderInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface ProductsOrderService {

    // 获取订单列表
    PageInfo getOrderList(GetLoginOutData userInfo, List<String> gsphBrIdList, String startDate, String endDate, Integer pageNum, Integer pageSize);

    void getOrderListExport(HttpServletResponse response, GetLoginOutData userInfo, ProductsOrderInData inData);
}