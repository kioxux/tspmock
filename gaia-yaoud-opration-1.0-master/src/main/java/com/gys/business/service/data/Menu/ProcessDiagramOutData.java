package com.gys.business.service.data.Menu;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("菜单导航")
public class ProcessDiagramOutData {
    private String flowName;

    private List<ProcessDiagramBO>  processDiagramBO;
}
