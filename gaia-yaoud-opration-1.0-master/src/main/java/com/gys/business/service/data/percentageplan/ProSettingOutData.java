package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: TODO
 * @author: flynn
 * @date: 2021年11月11日 下午4:55
 */
@Data
public class ProSettingOutData implements Serializable {
    private static final long serialVersionUID = -273918284507394649L;

    @ApiModelProperty(value = "方案id")
    private Integer planId;

    @ApiModelProperty(value = "提成设置id")
    private Long id;

    @ApiModelProperty(value = "提成类型 1 销售提成 2 单品提成")
    private String planType;

    @ApiModelProperty(value = "单品提成方式 0 不参与销售提成 1 参与销售提成")
    private String planProductWay;

    @ApiModelProperty(value = "提成方式 1 按零售额提成 2 按销售额提成 3按毛利额")
    private String planPercentageWay;

    @ApiModelProperty(value = "剔除折扣率 操作符号 =、＞、＞＝、＜、＜＝")
    private String planRejectDiscountRateSymbol;

    @ApiModelProperty(value = "负毛利率商品是否不参与销售提成 0 是 1 否")
    private String planIfNegative;

    @ApiModelProperty(value = "剔除折扣率 数值")
    private String planRejectDiscountRate;

    @ApiModelProperty(value = "单品提成明细列表")
    private List<PercentageProInData> proInDataList;

    @ApiModelProperty(value = "子方案名称")
    private String cPlanName;


}

