//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.salesReceiptsPriceUtil;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.entity.GaiaSdPromCouponGrant;
import com.gys.business.mapper.entity.GaiaSdPromCouponUse;
import java.math.BigDecimal;

public class PromCouponProType {
    public PromCouponProType() {
    }

    public static int gsphPart1Repeat1Use(int numTotal, BigDecimal moneyTotal, GaiaSdPromCouponUse gaiaSdPromCouponUse) {
        int count = 0;
        BigDecimal gspcuReachAmt1 = gaiaSdPromCouponUse.getGspcuReachAmt1();
        String gspcuReachQty1 = gaiaSdPromCouponUse.getGspcuReachQty1();
        String gspcuResultQty1 = gaiaSdPromCouponUse.getGspcuResultQty1();
        if (ObjectUtil.isNotEmpty(gspcuReachAmt1)) {
            if (moneyTotal.compareTo(gspcuReachAmt1) > -1) {
                while(moneyTotal.compareTo(gspcuReachAmt1) > -1) {
                    count += Integer.valueOf(gspcuResultQty1);
                    moneyTotal = moneyTotal.subtract(gspcuReachAmt1);
                }
            }
        } else if (ObjectUtil.isNotEmpty(gspcuReachQty1) && numTotal >= Integer.valueOf(gspcuReachQty1)) {
            while(numTotal >= Integer.valueOf(gspcuReachQty1)) {
                count += Integer.valueOf(gspcuResultQty1);
                numTotal -= Integer.valueOf(gspcuReachQty1);
            }
        }

        return count;
    }

    public static int gsphPart2Repeat1Use(int numTotal, BigDecimal moneyTotal, GaiaSdPromCouponUse gaiaSdPromCouponUse) {
        int count = 0;
        BigDecimal gspcuReachAmt1 = gaiaSdPromCouponUse.getGspcuReachAmt1();
        BigDecimal gspcuReachAmt2 = gaiaSdPromCouponUse.getGspcuReachAmt2();
        String gspcuReachQty1 = gaiaSdPromCouponUse.getGspcuReachQty1();
        String gspcuReachQty2 = gaiaSdPromCouponUse.getGspcuReachQty2();
        String gspcuResultQty1 = gaiaSdPromCouponUse.getGspcuResultQty1();
        String gspcuResultQty2 = gaiaSdPromCouponUse.getGspcuResultQty2();
        if (ObjectUtil.isNotEmpty(gspcuReachAmt2)) {
            if (moneyTotal.compareTo(gspcuReachAmt2) > -1) {
                while(moneyTotal.compareTo(gspcuReachAmt2) > -1) {
                    count += Integer.valueOf(gspcuResultQty2);
                    moneyTotal = moneyTotal.subtract(gspcuReachAmt2);
                    if (moneyTotal.compareTo(gspcuReachAmt2) == -1 && moneyTotal.compareTo(gspcuReachAmt2) > -1) {
                        count += Integer.valueOf(gspcuResultQty1);
                    }
                }
            } else if (moneyTotal.compareTo(gspcuReachAmt1) > -1) {
                count += Integer.valueOf(gspcuResultQty1);
            }
        } else if (ObjectUtil.isNotEmpty(gspcuReachQty2) && numTotal >= Integer.valueOf(gspcuReachQty2)) {
            while(numTotal >= Integer.valueOf(gspcuReachQty2)) {
                count += Integer.valueOf(gspcuResultQty2);
                numTotal -= Integer.valueOf(gspcuReachQty2);
                if (numTotal < Integer.valueOf(gspcuReachQty2) && numTotal >= Integer.valueOf(gspcuReachQty1)) {
                    count += Integer.valueOf(gspcuResultQty1);
                }
            }
        }

        return count;
    }

    public static int gsphPart3Repeat1Use(int numTotal, BigDecimal moneyTotal, GaiaSdPromCouponUse gaiaSdPromCouponUse) {
        int count = 0;
        BigDecimal gspcuReachAmt1 = gaiaSdPromCouponUse.getGspcuReachAmt1();
        BigDecimal gspcuReachAmt2 = gaiaSdPromCouponUse.getGspcuReachAmt2();
        BigDecimal gspcuReachAmt3 = gaiaSdPromCouponUse.getGspcuReachAmt3();
        String gspcuReachQty1 = gaiaSdPromCouponUse.getGspcuReachQty1();
        String gspcuReachQty2 = gaiaSdPromCouponUse.getGspcuReachQty2();
        String gspcuReachQty3 = gaiaSdPromCouponUse.getGspcuReachQty3();
        String gspcuResultQty1 = gaiaSdPromCouponUse.getGspcuResultQty1();
        String gspcuResultQty2 = gaiaSdPromCouponUse.getGspcuResultQty2();
        String gspcuResultQty3 = gaiaSdPromCouponUse.getGspcuResultQty3();
        if (ObjectUtil.isNotEmpty(gspcuReachAmt3)) {
            if (moneyTotal.compareTo(gspcuReachAmt3) > -1) {
                while(moneyTotal.compareTo(gspcuReachAmt3) > -1) {
                    count += Integer.valueOf(gspcuResultQty3);
                    moneyTotal = moneyTotal.subtract(gspcuReachAmt3);
                    if (moneyTotal.compareTo(gspcuReachAmt3) == -1) {
                        if (moneyTotal.compareTo(gspcuReachAmt2) > -1) {
                            count += Integer.valueOf(gspcuResultQty2);
                        } else if (moneyTotal.compareTo(gspcuReachAmt1) > -1) {
                            count += Integer.valueOf(gspcuResultQty1);
                        }
                    }
                }
            } else if (moneyTotal.compareTo(gspcuReachAmt2) > -1) {
                count += Integer.valueOf(gspcuResultQty2);
            } else if (moneyTotal.compareTo(gspcuReachAmt1) > -1) {
                count += Integer.valueOf(gspcuResultQty1);
            }
        } else if (ObjectUtil.isNotEmpty(gspcuReachQty3)) {
            if (numTotal >= Integer.valueOf(gspcuReachQty3)) {
                while(numTotal >= Integer.valueOf(gspcuReachQty3)) {
                    count += Integer.valueOf(gspcuResultQty3);
                    numTotal -= Integer.valueOf(gspcuReachQty3);
                    if (numTotal < Integer.valueOf(gspcuReachQty3)) {
                        if (numTotal >= Integer.valueOf(gspcuReachQty2)) {
                            count += Integer.valueOf(gspcuResultQty2);
                        } else if (numTotal >= Integer.valueOf(gspcuReachQty1)) {
                            count += Integer.valueOf(gspcuResultQty1);
                        }
                    }
                }
            } else if (numTotal >= Integer.valueOf(gspcuReachQty2)) {
                count += Integer.valueOf(gspcuResultQty2);
            } else if (numTotal >= Integer.valueOf(gspcuReachQty1)) {
                count += Integer.valueOf(gspcuResultQty1);
            }
        }

        return count;
    }

    public static int gsphPart1Repeat2Use(int numTotal, BigDecimal moneyTotal, GaiaSdPromCouponUse gaiaSdPromCouponUse) {
        int count = 0;
        BigDecimal gspcuReachAmt1 = gaiaSdPromCouponUse.getGspcuReachAmt1();
        String gspcuReachQty1 = gaiaSdPromCouponUse.getGspcuReachQty1();
        String gspcuResultQty1 = gaiaSdPromCouponUse.getGspcuResultQty1();
        if (ObjectUtil.isNotEmpty(gspcuReachAmt1)) {
            if (moneyTotal.compareTo(gspcuReachAmt1) > -1) {
                count += Integer.valueOf(gspcuResultQty1);
            }
        } else if (ObjectUtil.isNotEmpty(gspcuReachQty1) && numTotal >= Integer.valueOf(gspcuReachQty1)) {
            count += Integer.valueOf(gspcuResultQty1);
        }

        return count;
    }

    public static int gsphPart2Repeat2Use(int numTotal, BigDecimal moneyTotal, GaiaSdPromCouponUse gaiaSdPromCouponUse) {
        int count = 0;
        BigDecimal gspcuReachAmt1 = gaiaSdPromCouponUse.getGspcuReachAmt1();
        BigDecimal gspcuReachAmt2 = gaiaSdPromCouponUse.getGspcuReachAmt2();
        String gspcuReachQty1 = gaiaSdPromCouponUse.getGspcuReachQty1();
        String gspcuReachQty2 = gaiaSdPromCouponUse.getGspcuReachQty2();
        String gspcuResultQty1 = gaiaSdPromCouponUse.getGspcuResultQty1();
        String gspcuResultQty2 = gaiaSdPromCouponUse.getGspcuResultQty2();
        if (ObjectUtil.isNotEmpty(gspcuReachAmt2)) {
            if (moneyTotal.compareTo(gspcuReachAmt2) > -1) {
                count += Integer.valueOf(gspcuResultQty2);
            } else {
                count = gsphPart1Repeat2Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            }
        } else if (ObjectUtil.isNotEmpty(gspcuReachQty2)) {
            if (numTotal >= Integer.valueOf(gspcuReachQty2)) {
                count += Integer.valueOf(gspcuResultQty2);
            } else {
                count = gsphPart1Repeat2Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            }
        }

        return count;
    }

    public static int gsphPart3Repeat2Use(int numTotal, BigDecimal moneyTotal, GaiaSdPromCouponUse gaiaSdPromCouponUse) {
        int count = 0;
        BigDecimal gspcuReachAmt3 = gaiaSdPromCouponUse.getGspcuReachAmt3();
        String gspcuReachQty3 = gaiaSdPromCouponUse.getGspcuReachQty3();
        String gspcuResultQty3 = gaiaSdPromCouponUse.getGspcuResultQty3();
        if (ObjectUtil.isNotEmpty(gspcuReachAmt3)) {
            if (moneyTotal.compareTo(gspcuReachAmt3) > -1) {
                count += Integer.valueOf(gspcuResultQty3);
            } else {
                count = gsphPart2Repeat2Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            }
        } else if (ObjectUtil.isNotEmpty(gspcuReachQty3)) {
            if (numTotal >= Integer.valueOf(gspcuReachQty3)) {
                count += Integer.valueOf(gspcuResultQty3);
            } else {
                count = gsphPart2Repeat2Use(numTotal, moneyTotal, gaiaSdPromCouponUse);
            }
        }

        return count;
    }

    public static int gsphPart1Repeat1Grant(Float numTotal, BigDecimal moneyTotal, GaiaSdPromCouponGrant gaiaSdPromCouponGrant) {
        int count = 0;
        BigDecimal gspcgReachAmt1 = gaiaSdPromCouponGrant.getGspcgReachAmt1();
        String gspcgReachQty1 = gaiaSdPromCouponGrant.getGspcgReachQty1();
        String gspcgResultQty1 = gaiaSdPromCouponGrant.getGspcgResultQty1();
        if (ObjectUtil.isNotEmpty(gspcgReachAmt1)) {
            if (moneyTotal.compareTo(gspcgReachAmt1) > -1) {
                while(moneyTotal.compareTo(gspcgReachAmt1) > -1) {
                    count += Integer.valueOf(gspcgResultQty1);
                    moneyTotal = moneyTotal.subtract(gspcgReachAmt1);
                }
            }
        } else if (ObjectUtil.isNotEmpty(gspcgReachQty1) && numTotal >= (float)Integer.valueOf(gspcgReachQty1)) {
            while(numTotal >= (float)Integer.valueOf(gspcgReachQty1)) {
                count += Integer.valueOf(gspcgResultQty1);
                numTotal = numTotal - (float)Integer.valueOf(gspcgReachQty1);
            }
        }

        return count;
    }

    public static int gsphPart2Repeat1Grant(Float numTotal, BigDecimal moneyTotal, GaiaSdPromCouponGrant gaiaSdPromCouponGrant) {
        int count = 0;
        BigDecimal gspcgReachAmt1 = gaiaSdPromCouponGrant.getGspcgReachAmt1();
        BigDecimal gspcgReachAmt2 = gaiaSdPromCouponGrant.getGspcgReachAmt2();
        String gspcgReachQty1 = gaiaSdPromCouponGrant.getGspcgReachQty1();
        String gspcgReachQty2 = gaiaSdPromCouponGrant.getGspcgReachQty2();
        String gspcgResultQty1 = gaiaSdPromCouponGrant.getGspcgResultQty1();
        String gspcgResultQty2 = gaiaSdPromCouponGrant.getGspcgResultQty2();
        if (ObjectUtil.isNotEmpty(gspcgReachAmt2)) {
            if (moneyTotal.compareTo(gspcgReachAmt2) > -1) {
                while(moneyTotal.compareTo(gspcgReachAmt2) > -1) {
                    count += Integer.valueOf(gspcgResultQty2);
                    moneyTotal = moneyTotal.subtract(gspcgReachAmt2);
                    if (moneyTotal.compareTo(gspcgReachAmt2) == -1 && moneyTotal.compareTo(gspcgReachAmt2) > -1) {
                        count += Integer.valueOf(gspcgResultQty1);
                    }
                }
            } else if (moneyTotal.compareTo(gspcgReachAmt1) > -1) {
                count += Integer.valueOf(gspcgResultQty1);
            }
        } else if (ObjectUtil.isNotEmpty(gspcgReachQty2) && numTotal >= (float)Integer.valueOf(gspcgReachQty2)) {
            while(numTotal >= (float)Integer.valueOf(gspcgReachQty2)) {
                count += Integer.valueOf(gspcgResultQty2);
                numTotal = numTotal - (float)Integer.valueOf(gspcgReachQty2);
                if (numTotal < (float)Integer.valueOf(gspcgReachQty2) && numTotal >= (float)Integer.valueOf(gspcgReachQty1)) {
                    count += Integer.valueOf(gspcgResultQty1);
                }
            }
        }

        return count;
    }

    public static int gsphPart3Repeat1Grant(Float numTotal, BigDecimal moneyTotal, GaiaSdPromCouponGrant gaiaSdPromCouponGrant) {
        int count = 0;
        BigDecimal gspcuReachAmt1 = gaiaSdPromCouponGrant.getGspcgReachAmt1();
        BigDecimal gspcuReachAmt2 = gaiaSdPromCouponGrant.getGspcgReachAmt2();
        BigDecimal gspcuReachAmt3 = gaiaSdPromCouponGrant.getGspcgReachAmt3();
        String gspcgReachQty1 = gaiaSdPromCouponGrant.getGspcgReachQty1();
        String gspcgReachQty2 = gaiaSdPromCouponGrant.getGspcgReachQty2();
        String gspcgReachQty3 = gaiaSdPromCouponGrant.getGspcgReachQty3();
        String gspcgResultQty1 = gaiaSdPromCouponGrant.getGspcgResultQty1();
        String gspcgResultQty2 = gaiaSdPromCouponGrant.getGspcgResultQty2();
        String gspcgResultQty3 = gaiaSdPromCouponGrant.getGspcgResultQty3();
        if (ObjectUtil.isNotEmpty(gspcuReachAmt3)) {
            if (moneyTotal.compareTo(gspcuReachAmt3) > -1) {
                while(moneyTotal.compareTo(gspcuReachAmt3) > -1) {
                    count += Integer.valueOf(gspcgResultQty3);
                    moneyTotal = moneyTotal.subtract(gspcuReachAmt3);
                    if (moneyTotal.compareTo(gspcuReachAmt3) == -1) {
                        if (moneyTotal.compareTo(gspcuReachAmt2) > -1) {
                            count += Integer.valueOf(gspcgResultQty2);
                        } else if (moneyTotal.compareTo(gspcuReachAmt1) > -1) {
                            count += Integer.valueOf(gspcgResultQty1);
                        }
                    }
                }
            } else if (moneyTotal.compareTo(gspcuReachAmt2) > -1) {
                count += Integer.valueOf(gspcgResultQty2);
            } else if (moneyTotal.compareTo(gspcuReachAmt1) > -1) {
                count += Integer.valueOf(gspcgResultQty1);
            }
        } else if (ObjectUtil.isNotEmpty(gspcgReachQty3)) {
            if (numTotal >= (float)Integer.valueOf(gspcgReachQty3)) {
                while(numTotal >= (float)Integer.valueOf(gspcgReachQty3)) {
                    count += Integer.valueOf(gspcgResultQty3);
                    numTotal = numTotal - (float)Integer.valueOf(gspcgReachQty3);
                    if (numTotal < (float)Integer.valueOf(gspcgReachQty3)) {
                        if (numTotal >= (float)Integer.valueOf(gspcgReachQty2)) {
                            count += Integer.valueOf(gspcgResultQty2);
                        } else if (numTotal >= (float)Integer.valueOf(gspcgReachQty1)) {
                            count += Integer.valueOf(gspcgResultQty1);
                        }
                    }
                }
            } else if (numTotal >= (float)Integer.valueOf(gspcgReachQty2)) {
                count += Integer.valueOf(gspcgResultQty2);
            } else if (numTotal >= (float)Integer.valueOf(gspcgReachQty1)) {
                count += Integer.valueOf(gspcgResultQty1);
            }
        }

        return count;
    }

    public static int gsphPart1Repeat2Grant(Float numTotal, BigDecimal moneyTotal, GaiaSdPromCouponGrant gaiaSdPromCouponGrant) {
        int count = 0;
        BigDecimal gspcgReachAmt1 = gaiaSdPromCouponGrant.getGspcgReachAmt1();
        String gspcgReachQty1 = gaiaSdPromCouponGrant.getGspcgReachQty1();
        String gspcgResultQty1 = gaiaSdPromCouponGrant.getGspcgResultQty1();
        if (ObjectUtil.isNotEmpty(gspcgReachAmt1)) {
            if (moneyTotal.compareTo(gspcgReachAmt1) > -1) {
                count += Integer.valueOf(gspcgResultQty1);
            }
        } else if (ObjectUtil.isNotEmpty(gspcgReachQty1) && numTotal >= (float)Integer.valueOf(gspcgReachQty1)) {
            count += Integer.valueOf(gspcgResultQty1);
        }

        return count;
    }

    public static int gsphPart2Repeat2Grant(Float numTotal, BigDecimal moneyTotal, GaiaSdPromCouponGrant gaiaSdPromCouponGrant) {
        int count = 0;
        BigDecimal gspcgReachAmt1 = gaiaSdPromCouponGrant.getGspcgReachAmt1();
        BigDecimal gspcgReachAmt2 = gaiaSdPromCouponGrant.getGspcgReachAmt2();
        String gspcgReachQty1 = gaiaSdPromCouponGrant.getGspcgReachQty1();
        String gspcgReachQty2 = gaiaSdPromCouponGrant.getGspcgReachQty2();
        String gspcgResultQty1 = gaiaSdPromCouponGrant.getGspcgResultQty1();
        String gspcgResultQty2 = gaiaSdPromCouponGrant.getGspcgResultQty2();
        if (ObjectUtil.isNotEmpty(gspcgReachAmt2)) {
            if (moneyTotal.compareTo(gspcgReachAmt2) > -1) {
                count += Integer.valueOf(gspcgResultQty2);
            } else {
                count = gsphPart1Repeat2Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            }
        } else if (ObjectUtil.isNotEmpty(gspcgReachQty2)) {
            if (numTotal >= (float)Integer.valueOf(gspcgReachQty2)) {
                count += Integer.valueOf(gspcgResultQty2);
            } else {
                count = gsphPart1Repeat2Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            }
        }

        return count;
    }

    public static int gsphPart3Repeat2Grant(Float numTotal, BigDecimal moneyTotal, GaiaSdPromCouponGrant gaiaSdPromCouponGrant) {
        int count = 0;
        BigDecimal gspcgReachAmt3 = gaiaSdPromCouponGrant.getGspcgReachAmt3();
        String gspcgReachQty3 = gaiaSdPromCouponGrant.getGspcgReachQty3();
        String gspcgResultQty3 = gaiaSdPromCouponGrant.getGspcgResultQty3();
        if (ObjectUtil.isNotEmpty(gspcgReachAmt3)) {
            if (moneyTotal.compareTo(gspcgReachAmt3) > -1) {
                count += Integer.valueOf(gspcgResultQty3);
            } else {
                count = gsphPart2Repeat2Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            }
        } else if (ObjectUtil.isNotEmpty(gspcgReachQty3)) {
            if (numTotal >= (float)Integer.valueOf(gspcgReachQty3)) {
                count += Integer.valueOf(gspcgResultQty3);
            } else {
                count = gsphPart2Repeat2Grant(numTotal, moneyTotal, gaiaSdPromCouponGrant);
            }
        }

        return count;
    }
}
