package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.service.data.percentageplan.PercentageInData;
import com.gys.business.service.data.percentageplan.PercentageOutData;
import com.gys.business.service.data.percentageplan.PercentageProInData;

import java.util.List;
import java.util.Map;

public interface PercentagePlanV3Service {
    Integer insert(PercentageInData inData);

    PageInfo<PercentageOutData> list(PercentageInData inData);

    PercentageInData tichengDetail(Long planId,Long type);

    void approve(Long planId,String planStatus,String type);

    void deletePlan(Long planId,String type);

    PercentageProInData selectProductByClient(String client, String proCode);

    Map<String,PercentageProInData> selectProductByClientProCodes(String client, List<String> proCode);

    List<PercentageProInData> getImportExcelDetailList(String clientId, List<ImportPercentageData> importInDataList);

    List<Map<String,String>>selectStoList(Long planId,Long type);

    PercentageInData tichengDetailCopy(Long planId,Long type);

    void stopPlan(Long planId,String planType,String stopType,String type);

    void timerStopPlan();


}
