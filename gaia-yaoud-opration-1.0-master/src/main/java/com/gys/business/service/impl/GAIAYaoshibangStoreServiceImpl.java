package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.gys.business.mapper.GAIAYaoshibangStoreMapper;
import com.gys.business.mapper.entity.GAIAYaoshibangStore;
import com.gys.business.mapper.entity.GaiaSoBatchAccount;
import com.gys.business.service.GAIAYaoshibangStoreService;
import com.gys.business.service.GaiaSoBatchAccountService;
import com.gys.business.service.data.yaoshibang.CustormerQueryFrom;
import com.gys.util.ysb.YsbApiUtil;
import com.gys.util.ysb.YsbDrugstoreDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/12/22 13:41
 */
@Slf4j
@Service
public class GAIAYaoshibangStoreServiceImpl implements GAIAYaoshibangStoreService {
    @Resource
    private GAIAYaoshibangStoreMapper gaiaYaoshibangStoreMapper;
    @Autowired
    private GaiaSoBatchAccountService accountService;

    @Override
    public HashMap<String,String> getYaoshibangStoreByKey(CustormerQueryFrom queryFrom){
        List<HashMap<String,String>> list=gaiaYaoshibangStoreMapper.getYaoshibangStoreByKey(queryFrom);
        if(list.isEmpty()){
            HashMap<String,String> map=new HashMap<>();
            map.put("drugStoreName",queryFrom.getDrugstoreName());
            map.put("regAdress",queryFrom.getRegAdress());
            return map;
        }
        return list.get(0);
    }

    @Override
    public boolean synchYaoshibangStore(Integer isAll){
        log.info("药师帮客户同步开始==============");
        List<GaiaSoBatchAccount> allOpen = accountService.findAllOpen();
        if (CollUtil.isEmpty(allOpen)) {
            log.info("药师帮客户同步，没有开启的服务，return========");
            return true;
        }
        for (GaiaSoBatchAccount account : allOpen) {
            String appId = account.getAppId();
            String authCode = account.getAuthCode();
            String client = account.getClient();
            //获取药师帮客户数据
            List<YsbDrugstoreDto> ysbDrugstoreDtoList=YsbApiUtil.getDrugstoreInfo(appId, authCode, isAll);
            if(!ysbDrugstoreDtoList.isEmpty()){
                CustormerQueryFrom queryFrom=new CustormerQueryFrom();
                for (YsbDrugstoreDto dto: ysbDrugstoreDtoList) {
                    //查询药德库里是否存在
                    queryFrom.setDrugstoreName(dto.getDrugstoreName());
                    queryFrom.setRegAdress(dto.getRegAdress());
                    queryFrom.setClient(client);
                    List<HashMap<String,String>> list=gaiaYaoshibangStoreMapper.getYaoshibangStoreByKey(queryFrom);
                    GAIAYaoshibangStore model=new GAIAYaoshibangStore();
                    BeanUtils.copyProperties(dto,model);
                    model.setClient(client);
                    if(list.isEmpty()){
                        //新增
                        model.setStoreAdress(model.getRegAdress());
                        model.setReceiveAdress(model.getRegAdress());
                        gaiaYaoshibangStoreMapper.insert(model);
                    }else{
                        //修改
                        gaiaYaoshibangStoreMapper.updateByKey(model);
                    }
                }
            }
        }
        log.info("药师帮客户同步结束==============");
        return true;
    }
}
