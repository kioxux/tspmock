package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaFranchiseeMapper;
import com.gys.business.mapper.GaiaRegionalPlanningModelMapper;
import com.gys.business.mapper.GaiaRegionalStoreMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.GaiaRegionalPlanningModel;
import com.gys.business.mapper.entity.GaiaRegionalStore;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.RegionalPlanningService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.excel.ObjectUtil;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:55 2021/10/14
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RegionalPlanningServiceImpl implements RegionalPlanningService {

    @Autowired
    private GaiaRegionalPlanningModelMapper gaiaRegionalPlanningModelMapper;

    @Autowired
    private GaiaRegionalStoreMapper gaiaRegionalStoreMapper;

    @Autowired
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Override
    public void defaultDate(EditDefaultRegionalInData inData) {
        List<String> clients;
        if (ObjectUtil.isEmpty(inData.getClients())){
            clients = gaiaFranchiseeMapper.getAllClientId();
        }else {
            clients = inData.getClients();
        }
        //生成区域默认数据
        ArrayList<GaiaRegionalStore> gaiaRegionalStores = new ArrayList<>();
        for (String client : clients) {
            GaiaRegionalPlanningModel threeLevel = GaiaRegionalPlanningModel.getDefaultThreeLevel(client);
            gaiaRegionalPlanningModelMapper.saveRegionalPlanningModel(threeLevel);
            GaiaRegionalPlanningModel twoLevel = GaiaRegionalPlanningModel.getDefaultTwoLevel(client, threeLevel.getId());
            gaiaRegionalPlanningModelMapper.saveRegionalPlanningModel(twoLevel);
            GaiaRegionalPlanningModel oneLevel = GaiaRegionalPlanningModel.getDefaultOneLevel(client, twoLevel.getId());
            gaiaRegionalPlanningModelMapper.saveRegionalPlanningModel(oneLevel);

            //生成门店区域关系默认数据
            List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.getAllByClientAndBrId(client);
            for (int i = 0; i < storeDataList.size(); i++) {
                GaiaStoreData storeData = storeDataList.get(i);
                GaiaRegionalStore regionalStore = GaiaRegionalStore.getDefaultRegionalStore(client, oneLevel.getId(), storeData.getStoCode(), ObjectUtil.isNotEmpty(storeData.getStoShortName()) ? storeData.getStoShortName() : storeData.getStoName(), i);
                gaiaRegionalStores.add(regionalStore);
            }
        }
        if (ObjectUtil.isNotEmpty(gaiaRegionalStores)) {
            gaiaRegionalStoreMapper.batchInsert(gaiaRegionalStores);
        }
    }

    @Override
    public void editDefaultRegional(EditDefaultRegionalInData inData) {
        if (ObjectUtil.isEmpty(inData.getClients())) {
            List<String> clients = gaiaFranchiseeMapper.getAllClientId();
            inData.setClients(clients);
        }

        for (String client : inData.getClients()) {
            gaiaRegionalPlanningModelMapper.updateDefault(client,3,inData.getThreeLevelCode(),inData.getThreeLevelName());
            gaiaRegionalPlanningModelMapper.updateDefault(client,2,inData.getTwoLevelCode(),inData.getTwoLevelName());
            gaiaRegionalPlanningModelMapper.updateDefault(client,1,inData.getOneLevelCode(),inData.getOneLevelName());
        }
    }

    @Override
    public List<RegionalOutData> getRegional(GetLoginOutData userInfo, SelectRegionalInData inData) {
        List<RegionalOutData> regionalOutDataList = gaiaRegionalPlanningModelMapper.getRegionalByClientAndParentId(userInfo.getClient(), 0L);
        selectRegional(userInfo.getClient(), regionalOutDataList, inData.getType());
        return regionalOutDataList;
    }

    private void selectRegional(String client, List<RegionalOutData> list, String type) {
        for (RegionalOutData regionalOutData : list) {
            List<RegionalOutData> regionalOutDataList = gaiaRegionalPlanningModelMapper.getRegionalByClientAndParentId(client, regionalOutData.getId());
            regionalOutData.setRegionals(regionalOutDataList);
            if (ObjectUtil.isNotEmpty(regionalOutDataList)) {
                selectRegional(client, regionalOutDataList, type);
            }
            if ("1".equals(type)) {
                if (regionalOutData.getLevel() == 1) {
                    Integer storeNum = gaiaRegionalStoreMapper.getStoreNumByClientAndRegionalId(client, regionalOutData.getId());
                    regionalOutData.setStoreNum(storeNum);
                    List<GaiaRegionalStore> regionalStores = gaiaRegionalStoreMapper.selectByRegionalId(regionalOutData.getId());
                    regionalOutData.setStores(regionalStores);
                } else {
                    int sum = ObjectUtil.isNotEmpty(regionalOutData.getRegionals()) ? regionalOutData.getRegionals().stream().mapToInt(RegionalOutData::getStoreNum).sum() : 0;
                    regionalOutData.setStoreNum(sum);
                }
            }
        }
    }

    @Override
    public void saveRegional(SaveRegionalInData inData) {
        if (ObjectUtil.isNotEmpty(inData.getId())) {
            editRegional(inData);
        } else {
            insertRegional(inData);
        }
    }

    @Override
    public void removeRegional(RemoveRegionalInData inData) {
        GaiaRegionalPlanningModel model = gaiaRegionalPlanningModelMapper.selectById(inData.getId());
        if (ObjectUtil.isEmpty(model)) {
            throw new BusinessException("该区域不存在！");
        }
        if ("1".equals(model.getIsDefault())) {
            throw new BusinessException("默认区域不可移除！");
        }
        List<GaiaRegionalPlanningModel> models = new ArrayList<>();
        getAndLower(model.getId(), models);
        models.add(model);
        List<Long> ids = models.stream().map(o -> o.getId()).collect(Collectors.toList());
        if (ObjectUtil.isNotEmpty(ids)) {
            gaiaRegionalPlanningModelMapper.removeRegional(inData.getUpdateUser(), inData.getUpdateTime(), ids);
        }
        gaiaRegionalPlanningModelMapper.updateParentSort(inData.getClient(), model.getParentId(), model.getSortNum());
        GaiaRegionalPlanningModel defaultRegional = gaiaRegionalPlanningModelMapper.selectOneLevelDefaultRegionalByClient(inData.getClient());
        List<Long> regionalIds = models.stream().filter(o -> o.getLevel() == 1).map(o -> o.getId()).collect(Collectors.toList());
        if (ObjectUtil.isEmpty(regionalIds)) {
            return;
        }
        List<GaiaRegionalStore> regionalStores = gaiaRegionalStoreMapper.selectByRegionalIds(regionalIds);

        List<GaiaRegionalStore> defaultRegionalStore = gaiaRegionalStoreMapper.selectByRegionalId(defaultRegional.getId());
        Integer sortNum;
        if (ObjectUtil.isEmpty(defaultRegionalStore)) {
            sortNum = 0;
        } else {
            sortNum = defaultRegionalStore.get(defaultRegionalStore.size() - 1).getSortNum() + 1;
        }

        ArrayList<GaiaRegionalStore> stores = new ArrayList<>();
        for (int i = 0; i < regionalStores.size(); i++) {
            GaiaRegionalStore store = new GaiaRegionalStore();
            store.setId(regionalStores.get(i).getId());
            store.setRegionalId(defaultRegional.getId());
            store.setUpdateUser(inData.getUpdateUser());
            store.setUpdateTime(inData.getUpdateTime());
            store.setSortNum(sortNum + i);
            stores.add(store);
        }
        if (ObjectUtil.isNotEmpty(stores)) {
            gaiaRegionalStoreMapper.batchUpdateRegionalIdAndSortNum(stores);
        }
    }

    private void getAndLower(Long parentId, List<GaiaRegionalPlanningModel> list) {
        List<GaiaRegionalPlanningModel> models = gaiaRegionalPlanningModelMapper.selectByParentId(parentId);
        if (ObjectUtil.isNotEmpty(models)) {
            for (GaiaRegionalPlanningModel model : models) {
                getAndLower(model.getId(), list);
            }
        }
        list.addAll(models);
    }

    private void insertRegional(SaveRegionalInData inData) {
        //校验重复
        checkRepeat(inData);
        Integer level;
        if (inData.getParentId() == 0) {
            level = 3;
        } else {
            GaiaRegionalPlanningModel parentModel = gaiaRegionalPlanningModelMapper.selectById(inData.getParentId());
            if (ObjectUtil.isEmpty(parentModel)) {
                throw new BusinessException("上级区域ID有误！");
            }
            if (parentModel.getLevel() == 1) {
                throw new BusinessException("一级区域下不能新建区域！");
            }
            level = parentModel.getLevel() - 1;
        }

        List<GaiaRegionalPlanningModel> regionalPlanningModelList = gaiaRegionalPlanningModelMapper.selectByParentId(inData.getParentId());
        Integer sortNum;
        if (ObjectUtil.isEmpty(regionalPlanningModelList)) {
            sortNum = 0;
        } else {
            sortNum = regionalPlanningModelList.get(regionalPlanningModelList.size() - 1).getSortNum() + 1;
        }
        GaiaRegionalPlanningModel maxModel;
        if (inData.getParentId() == 0){
            maxModel = gaiaRegionalPlanningModelMapper.selectMaxRegionalCodeByClientAndLevel(inData.getClient(),3);
        }else {
            GaiaRegionalPlanningModel model = gaiaRegionalPlanningModelMapper.selectById(inData.getParentId());
            maxModel = gaiaRegionalPlanningModelMapper.selectMaxRegionalCodeByClientAndLevel(inData.getClient(),model.getLevel()-1);
        }
        if (ObjectUtil.isEmpty(maxModel)){
            throw new BusinessException("请联系工作人员生成默认数据！");
        }
        Integer maxCode = Integer.parseInt(maxModel.getRegionalCode());
        Integer length = 2;
        switch (maxModel.getLevel()){
            case 3:
                length = 2;
                break;
            case 2:
                length = 3;
                break;
            case 1:
                length = 4;
                break;
        }
        String newCode = String.format("%0" + length + "d", maxCode + 1);
        GaiaRegionalPlanningModel gaiaRegionalPlanningModel = new GaiaRegionalPlanningModel(
                null,
                inData.getClient(),
                newCode,
                inData.getRegionalName(),
                level,
                sortNum,
                inData.getParentId(),
                inData.getCreateUser(),
                new Date(),
                null,
                null,
                "0",
                "0"
        );
        gaiaRegionalPlanningModelMapper.saveRegionalPlanningModel(gaiaRegionalPlanningModel);
    }

    private void editRegional(SaveRegionalInData inData) {
        //判断是否为默认数据
        GaiaRegionalPlanningModel model = gaiaRegionalPlanningModelMapper.selectById(inData.getId());
        if (ObjectUtil.isEmpty(model)) {
            throw new BusinessException("该区域不存在！");
        }
        if ("1".equals(model.getIsDefault())) {
            throw new BusinessException("默认区域无法修改！");
        }
        //校验重复
        checkRepeat(inData);

        //判断是否修改上级区域
        if (inData.getParentId() != model.getParentId()) {
            //校验两个上级区域等级是否一致
            GaiaRegionalPlanningModel newModel = gaiaRegionalPlanningModelMapper.selectById(inData.getParentId());
            GaiaRegionalPlanningModel oldModel = gaiaRegionalPlanningModelMapper.selectById(model.getParentId());
            if (ObjectUtil.isEmpty(oldModel) || ObjectUtil.isEmpty(newModel) || newModel.getLevel() != oldModel.getLevel()) {
                throw new BusinessException("该区域只能移动到上一级区域！");
            }
            //修改排序等级
            gaiaRegionalPlanningModelMapper.updateParentSort(model.getClient(), model.getParentId(), model.getSortNum());
            List<GaiaRegionalPlanningModel> regionalPlanningModelList = gaiaRegionalPlanningModelMapper.selectByParentId(inData.getParentId());
            Integer sortNum;
            if (ObjectUtil.isEmpty(regionalPlanningModelList)) {
                sortNum = 0;
            } else {
                sortNum = regionalPlanningModelList.get(regionalPlanningModelList.size() - 1).getSortNum() + 1;
            }
            gaiaRegionalPlanningModelMapper.updateSortNum(inData.getId(), sortNum);
        }
        gaiaRegionalPlanningModelMapper.updateRegional(inData);
    }

    private void checkRepeat(SaveRegionalInData inData) {
        GaiaRegionalPlanningModel checkName = gaiaRegionalPlanningModelMapper.selectByClientAndNameExcludeId(inData.getClient(), inData.getRegionalName(), inData.getId());
        if (ObjectUtil.isNotEmpty(checkName)) {
            throw new BusinessException("区域名称已存在！");
        }
    }

    @Override
    public RegionalStoreOutData getRegionalStore(SelectRegionalStoreInData inData) {
        GaiaRegionalPlanningModel model = gaiaRegionalPlanningModelMapper.selectById(inData.getId());
        if (ObjectUtil.isEmpty(model)) {
            throw new BusinessException("该区域不存在！");
        }
        if (model.getLevel() != 1) {
            throw new BusinessException("一级区域才能查看所属门店！");
        }
        GaiaRegionalPlanningModel defaultRegional = gaiaRegionalPlanningModelMapper.selectOneLevelDefaultRegionalByClient(inData.getClient());
        List<GaiaRegionalStore> stores = gaiaRegionalStoreMapper.selectByRegionalId(model.getId());
        List<GaiaRegionalStore> defaultStores = gaiaRegionalStoreMapper.selectByRegionalId(defaultRegional.getId());
        RegionalStoreOutData outData = new RegionalStoreOutData();
        outData.setId(model.getId());
        outData.setRegionalCode(model.getRegionalCode());
        outData.setRegionalName(model.getRegionalName());
        outData.setStores(stores);
        outData.setDefaultId(defaultRegional.getId());
        outData.setDefaultRegionalCode(defaultRegional.getRegionalCode());
        outData.setDefaultRegionalName(defaultRegional.getRegionalName());
        outData.setDefaultStores(defaultStores);
        return outData;
    }

    @Override
    public void saveRegionalStore(SaveRegionalStoreInData inData) {
        GaiaRegionalPlanningModel model = gaiaRegionalPlanningModelMapper.selectById(inData.getId());
        if (ObjectUtil.isEmpty(model)) {
            throw new BusinessException("该区域不存在！");
        }
        if (model.getLevel() != 1) {
            throw new BusinessException("门店只能在一级区域下！");
        }
        if (ObjectUtil.isNotEmpty(inData.getRegionalStoreIds())) {
            List<GaiaRegionalStore> stores = new ArrayList<>();
            for (int i = 0; i < inData.getRegionalStoreIds().size(); i++) {
                GaiaRegionalStore store = new GaiaRegionalStore();
                store.setId(inData.getRegionalStoreIds().get(i));
                store.setSortNum(i);
                store.setRegionalId(inData.getId());
                store.setUpdateUser(inData.getUpdateUser());
                store.setUpdateTime(inData.getUpdateDate());
                stores.add(store);
            }
            if (ObjectUtil.isNotEmpty(stores)) {
                gaiaRegionalStoreMapper.batchUpdateRegionalIdAndSortNum(stores);
            }
        }

        GaiaRegionalPlanningModel defaultRegional = gaiaRegionalPlanningModelMapper.selectOneLevelDefaultRegionalByClient(inData.getClient());
        if (ObjectUtil.isNotEmpty(defaultRegional) && ObjectUtil.isNotEmpty(inData.getDefaultRegionalStoreIds())) {
            List<GaiaRegionalStore> stores = new ArrayList<>();
            for (int i = 0; i < inData.getDefaultRegionalStoreIds().size(); i++) {
                GaiaRegionalStore store = new GaiaRegionalStore();
                store.setId(inData.getDefaultRegionalStoreIds().get(i));
                store.setSortNum(i);
                store.setRegionalId(defaultRegional.getId());
                store.setUpdateUser(inData.getUpdateUser());
                store.setUpdateTime(inData.getUpdateDate());
                stores.add(store);
            }
            if (ObjectUtil.isNotEmpty(stores)) {
                gaiaRegionalStoreMapper.batchUpdateRegionalIdAndSortNum(stores);
            }
        }
    }
}
