package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author xiaoyuan on 2021/3/18
 */
@Data
public class AppAmountCalculationVo implements Serializable {
    private static final long serialVersionUID = -2859432973644367188L;

    @ApiModelProperty(value = "序号")
    private int index;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "商品通用名")
    private String proCommonName;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;

    @ApiModelProperty(value = "产地")
    private String proPlace;

    @ApiModelProperty(value = "剂型")
    private String proForm;

    @ApiModelProperty(value = "零售价")
    private String prcAmount;

    @ApiModelProperty(value = "处方类别 1-处方药，2-甲类OTC，3-乙类OTC，4-双跨处方")
    private String proPresclass;

    @ApiModelProperty(value = "管制特殊分类 1-毒性药品，2-麻醉药品，3-一类精神药品，4-二类精神药品，5-易制毒药品（麻黄碱），6-放射性药品，7-生物制品（含胰岛素），8-兴奋剂（除胰岛素），9-第一类器械，10-第二类器械，11-第三类器械，12-其它管制")
    private String proControlClass;

    @ApiModelProperty(value = "批号")
    private String gssbBatchNo;

    @ApiModelProperty(value = "批次")
    private String gssbBatch;

    @ApiModelProperty(value = "有效期")
    private String gssbValidDate;

    @ApiModelProperty(value = "商品仓储分区 1-内服药品，2-外用药品，3-中药饮片区，4-精装中药区，5-针剂，6-二类精神药品，7-含麻药品，8-冷藏商品，9-外用非药，10-医疗器械，11-食品，12-保健食品，13-易燃商品区")
    private String proStorageArea;

    @ApiModelProperty(value = "会员价格")
    private String memberPrice;

    @ApiModelProperty(value = "促销价格")
    private String promPrice;

    @ApiModelProperty(value = "实收单价")
    private String realPrice;

    @ApiModelProperty(value = "每个商品数量")
    private String proQty;

    @ApiModelProperty(value = "实收总价")
    private String totalAmt;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "税率值")
    private String gsrdInputTaxValue;

    @ApiModelProperty(value = "特殊药品购买人身份证")
    private String gssdSpecialmedIdcard;

    @ApiModelProperty(value = "特殊药品购买人姓名")
    private String gssdSpecialmedName;

    @ApiModelProperty(value = "特殊药品购买人性别 男 :1 / 女 :0")
    private String gssdSpecialmedSex;

    @ApiModelProperty(value = "特殊药品购买人出生日期")
    private String gssdSpecialmedBirthday;

    @ApiModelProperty(value = "特殊药品购买人手机")
    private String gssdSpecialmedMobile;

    @ApiModelProperty(value = "特殊药品购买人地址")
    private String gssdSpecialmedAddress;

    @ApiModelProperty(value = "特殊药品电子监管码")
    private String gssdSpecialmedEcode;

//    @ApiModelProperty(value = "促销信息")
//    private PromotionalConditionData conditionData;

    @ApiModelProperty(value = "商品会员类促销")
    private GetPromByProCodeOutData prom;

    @ApiModelProperty(value = "参加的促销活动")
    private AppPromData activityProm;

    @ApiModelProperty(value = "下一阶促销内容/当前促销内容")
    private List<AppPromData> prePromList;

}
