//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class ExchangeCashOutData {
    private String point;
    private BigDecimal dkamount;
    private String maxDkAmount;
    private String rate;
    private String exchangeRepeat;
    private String integralAddSet;

    public ExchangeCashOutData() {
    }

    public String getPoint() {
        return this.point;
    }

    public BigDecimal getDkamount() {
        return this.dkamount;
    }

    public String getMaxDkAmount() {
        return this.maxDkAmount;
    }

    public String getRate() {
        return this.rate;
    }

    public String getExchangeRepeat() {
        return this.exchangeRepeat;
    }

    public String getIntegralAddSet() {
        return this.integralAddSet;
    }

    public void setPoint(final String point) {
        this.point = point;
    }

    public void setDkamount(final BigDecimal dkamount) {
        this.dkamount = dkamount;
    }

    public void setMaxDkAmount(final String maxDkAmount) {
        this.maxDkAmount = maxDkAmount;
    }

    public void setRate(final String rate) {
        this.rate = rate;
    }

    public void setExchangeRepeat(final String exchangeRepeat) {
        this.exchangeRepeat = exchangeRepeat;
    }

    public void setIntegralAddSet(final String integralAddSet) {
        this.integralAddSet = integralAddSet;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ExchangeCashOutData)) {
            return false;
        } else {
            ExchangeCashOutData other = (ExchangeCashOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$point = this.getPoint();
                Object other$point = other.getPoint();
                if (this$point == null) {
                    if (other$point != null) {
                        return false;
                    }
                } else if (!this$point.equals(other$point)) {
                    return false;
                }

                Object this$dkamount = this.getDkamount();
                Object other$dkamount = other.getDkamount();
                if (this$dkamount == null) {
                    if (other$dkamount != null) {
                        return false;
                    }
                } else if (!this$dkamount.equals(other$dkamount)) {
                    return false;
                }

                Object this$maxDkAmount = this.getMaxDkAmount();
                Object other$maxDkAmount = other.getMaxDkAmount();
                if (this$maxDkAmount == null) {
                    if (other$maxDkAmount != null) {
                        return false;
                    }
                } else if (!this$maxDkAmount.equals(other$maxDkAmount)) {
                    return false;
                }

                label62: {
                    Object this$rate = this.getRate();
                    Object other$rate = other.getRate();
                    if (this$rate == null) {
                        if (other$rate == null) {
                            break label62;
                        }
                    } else if (this$rate.equals(other$rate)) {
                        break label62;
                    }

                    return false;
                }

                label55: {
                    Object this$exchangeRepeat = this.getExchangeRepeat();
                    Object other$exchangeRepeat = other.getExchangeRepeat();
                    if (this$exchangeRepeat == null) {
                        if (other$exchangeRepeat == null) {
                            break label55;
                        }
                    } else if (this$exchangeRepeat.equals(other$exchangeRepeat)) {
                        break label55;
                    }

                    return false;
                }

                Object this$integralAddSet = this.getIntegralAddSet();
                Object other$integralAddSet = other.getIntegralAddSet();
                if (this$integralAddSet == null) {
                    if (other$integralAddSet != null) {
                        return false;
                    }
                } else if (!this$integralAddSet.equals(other$integralAddSet)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ExchangeCashOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $point = this.getPoint();
        result = result * 59 + ($point == null ? 43 : $point.hashCode());
        Object $dkamount = this.getDkamount();
        result = result * 59 + ($dkamount == null ? 43 : $dkamount.hashCode());
        Object $maxDkAmount = this.getMaxDkAmount();
        result = result * 59 + ($maxDkAmount == null ? 43 : $maxDkAmount.hashCode());
        Object $rate = this.getRate();
        result = result * 59 + ($rate == null ? 43 : $rate.hashCode());
        Object $exchangeRepeat = this.getExchangeRepeat();
        result = result * 59 + ($exchangeRepeat == null ? 43 : $exchangeRepeat.hashCode());
        Object $integralAddSet = this.getIntegralAddSet();
        result = result * 59 + ($integralAddSet == null ? 43 : $integralAddSet.hashCode());
        return result;
    }

    public String toString() {
        return "ExchangeCashOutData(point=" + this.getPoint() + ", dkamount=" + this.getDkamount() + ", maxDkAmount=" + this.getMaxDkAmount() + ", rate=" + this.getRate() + ", exchangeRepeat=" + this.getExchangeRepeat() + ", integralAddSet=" + this.getIntegralAddSet() + ")";
    }
}
