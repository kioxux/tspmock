package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.GaiaBillOfArService;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.*;
import com.gys.common.excel.ObjectUtil;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.DateUtil;
import com.gys.util.ValidateUtil;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 明细清单表(GaiaBillOfAr)表服务实现类
 *
 * @author XIAOZY
 * @since 2021-09-18 15:13:23
 */
@Service("gaiaBillOfArService")
@Slf4j
public class GaiaBillOfArServiceImpl implements GaiaBillOfArService {
    @Resource
    private GaiaBillOfArMapper gaiaBillOfArMapper;
    @Resource
    private GaiaFiArBalanceMapper gaiaFiArBalanceMapper;
    @Resource
    private GaiaBillOfArCancelMapper gaiaBillOfArCancelMapper;
    @Resource
    private GaiaFiArPaymentMapper gaiaFiArPaymentMapper;
    @Resource
    public CosUtils cosUtils;
    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insertBill(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        GaiaBillOfAr insertOrUpdate = new GaiaBillOfAr();
        BeanUtil.copyProperties(inData, insertOrUpdate);
        Date now = new Date();
        if (ObjectUtil.isEmpty(inData.getId())) {
            //新增
            Integer count = gaiaBillOfArMapper.getBillCount(inData);
            GaiaBillOfArOutData cusInfo = checkParam(inData, count);
            String billCode = gaiaBillOfArMapper.getBillCode(inData);
            insertOrUpdate.setBillCode(billCode);
            insertOrUpdate.setBillDate(inData.getBillDate());
            insertOrUpdate.setCusName(cusInfo.getCusName());
            insertOrUpdate.setStatus(0);
            insertOrUpdate.setIsDelete(0);
            insertOrUpdate.setCreateTime(now);
            insertOrUpdate.setCreateUser(loginUser.getUserId());
            insertOrUpdate.setUpdateTime(now);
            insertOrUpdate.setUpdateUser(loginUser.getUserId());
            gaiaBillOfArMapper.insert(insertOrUpdate);
            if ("8888".equals(inData.getArMethodId())) {
                //如果是起初单据，自动补全发生日期 至 当前日期 的 单据
                String day = DateUtil.addDay(inData.getBillDate(), "yyyy-MM-dd", 1);
                inData.setStartTime(DateUtil.stringToDate(day, "yyyy-MM-dd"));
                inData.setEndTime(now);
                List<GaiaBillOfArDetailOutData> outDataList = gaiaBillOfArMapper.listUnReceivableDetail(inData);
                if (CollectionUtil.isNotEmpty(outDataList)) {
                    for (GaiaBillOfArDetailOutData gaiaBillOfArDetailOutData : outDataList) {
                        GaiaBillOfAr bill = new GaiaBillOfAr();
                        bill.setClient(loginUser.getClient());
                        String billCode1 = gaiaBillOfArMapper.getBillCode(inData);
                        bill.setBillCode(billCode1);
                        bill.setDcCode(inData.getDcCode());
                        bill.setArMethodId("9999");
                        bill.setArAmt(gaiaBillOfArDetailOutData.getAddTotalAmount());
                        bill.setHxAmt(BigDecimal.ZERO);
                        bill.setCusName(cusInfo.getCusName());
                        bill.setCusSelfCode(inData.getCusSelfCode());
                        bill.setBillDate(gaiaBillOfArDetailOutData.getBillDate());
                        bill.setStatus(0);
                        bill.setIsDelete(0);
                        bill.setCreateTime(now);
                        bill.setCreateUser(loginUser.getUserId());
                        bill.setUpdateTime(now);
                        bill.setUpdateUser(loginUser.getUserId());
                        gaiaBillOfArMapper.insert(bill);
                    }
                }
            }
            GaiaBillOfArOutData amt = gaiaBillOfArMapper.selectSumByArAmt(inData);
            if (!ObjectUtil.isEmpty(amt)){
                gaiaCustomerBusinessMapper.updateCusArAmt(inData,amt.getArAmt());
            }
        } else {
            //更新
            insertOrUpdate.setUpdateTime(now);
            insertOrUpdate.setUpdateUser(loginUser.getUserId());
            gaiaBillOfArMapper.update(insertOrUpdate);
        }
        //修改客户余额
        updatLastAmt(loginUser, inData, insertOrUpdate, now);
        return true;
    }

    private GaiaBillOfArOutData checkParam(GaiaBillOfArInData inData, Integer count) {
        if (count == 0 && !"8888".equals(inData.getArMethodId())) {
            throw new BusinessException("客户编码为：" + inData.getCusSelfCode() + "的客户，请先导入期初账款！");
        }
        if (count > 0 && "8888".equals(inData.getArMethodId())) {
            throw new BusinessException("该客户以存在期初账款，请勿重复导入！");
        }
        if ("9999".equals(inData.getArMethodId())) {
            throw new BusinessException("9999-应收增加为系统生成，请勿手动添加！");
        }
        GaiaBillOfArOutData cusInfo = gaiaBillOfArMapper.getCusInfo(inData);
        if (ObjectUtil.isEmpty(cusInfo)) {
            throw new BusinessException("系统中未查询到客户自编码为：" + inData.getCusSelfCode() + "的客户！");
        }
        if (ObjectUtil.isEmpty(inData.getBillDate())) {
            throw new BusinessException("请先选择发生日期！");
        }
        String billDate = DateUtil.formatDate(inData.getBillDate());
        boolean pastDate = DateUtil.isPastDate(billDate);
        if (pastDate) {
            throw new BusinessException("发生日期，不能大于当前日期！");
        }
        boolean flag = DateUtil.isThisMonth(inData.getBillDate(), "yyyyMM");
        if (!flag && !"8888".equals(inData.getArMethodId())) {
            throw new BusinessException("发生日期，只能选取本月日期！");
        }
        if ("6666".equals(inData.getArMethodId()) && BigDecimal.ZERO.compareTo(inData.getArAmt()) <= 0) {
            throw new BusinessException("应收回款金额，不能大于0！");
        }
        if(!Objects.equals("6666",inData.getArMethodId())){
            if(CollUtil.isNotEmpty(inData.getGwsCodeList())){
                throw new BusinessException("不是应收回款，不能选择销售员！");
            }
        }
        return cusInfo;
    }

    private void updatLastAmt(GetLoginOutData loginUser, GaiaBillOfArInData inData, GaiaBillOfAr insertOrUpdate, Date now) {
        inData.setArMethodId(null);
        GaiaFiArBalance balance = gaiaFiArBalanceMapper.getByDcCodeAndCusSelfCode(inData);
        inData.setStartTime(null);
        inData.setEndTime(null);
        Double lastAmt = gaiaBillOfArMapper.getLastAmt(inData);
        if (ObjectUtil.isEmpty(balance)) {
            GaiaFiArBalance insert = new GaiaFiArBalance();
            insert.setClient(loginUser.getClient());
            insert.setDcCode(insertOrUpdate.getDcCode());
            insert.setResidualAmt(new BigDecimal(lastAmt));
            insert.setCusSelfCode(insertOrUpdate.getCusSelfCode());
            insert.setCusName(insertOrUpdate.getCusName());
            insert.setIsDelete(0);
            insert.setCreateTime(now);
            insert.setCreateUser(loginUser.getUserId());
            insert.setUpdateTime(now);
            insert.setUpdateUser(loginUser.getUserId());
            gaiaFiArBalanceMapper.insert(insert);
        } else {
            balance.setResidualAmt(new BigDecimal(lastAmt));
            balance.setUpdateTime(now);
            balance.setUpdateUser(loginUser.getUserId());
            gaiaFiArBalanceMapper.update(balance);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insertBillList(GetLoginOutData loginUser, List<GaiaBillOfArInData> inData) {
        if (CollectionUtil.isEmpty(inData)) {
            throw new BusinessException("请填入需要保存的相关信息！");
        }
        for (GaiaBillOfArInData data : inData) {
            data.setClient(loginUser.getClient());
            this.insertBill(loginUser, data);
        }
        return true;
    }

    @Override
    public Object listBill(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        PageInfo pageInfo = new PageInfo();
        List<GaiaBillOfArOutData> outDataList = gaiaBillOfArMapper.listBill(inData);
        if (CollectionUtil.isNotEmpty(outDataList)) {
            GaiaBillOfArInData param = new GaiaBillOfArInData();
            List<GaiaBillOfArDetailOutData> cusList = gaiaBillOfArMapper.listCusInfo(param);
            for (GaiaBillOfArOutData outData : outDataList) {
                for (GaiaBillOfArDetailOutData cus : cusList) {
                    if (outData.getCusSelfCode().equals(cus.getCusSelfCode())) {
                        outData.setCusName(cus.getCusName());
                        break;
                    }
                }
            }
        }

        pageInfo.setList(outDataList);
        GaiaBillOfArOutData amt = gaiaBillOfArMapper.getBillTotal(inData);
        GaiaBillOfArOutData total = new GaiaBillOfArOutData();
        total.setDcCode("合计");
        if (ObjectUtil.isEmpty(amt)) {
            total.setArAmt(BigDecimal.ZERO);
        } else {
            total.setArAmt(amt.getArAmt());
        }
        pageInfo.setListNum(total);
        return pageInfo;
    }

    @Override
    public PageInfo listSalesBill(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        PageInfo pageInfo = new PageInfo();
        List<GaiaBillOfArDetailOutData> outDataList = gaiaBillOfArMapper.listSalesBills(inData);
        pageInfo.setList(outDataList);
        GaiaBillOfArDetailOutData total = new GaiaBillOfArDetailOutData();
        total.setDcCode("合计");
        if (CollectionUtil.isEmpty(outDataList)) {
            total.setArAmt(BigDecimal.ZERO);
        } else {
            BigDecimal addTotalAmount = outDataList.stream().map(GaiaBillOfArDetailOutData::getAddTotalAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal hasArAmt = outDataList.stream().map(GaiaBillOfArDetailOutData::getHasArAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
            total.setAddTotalAmount(addTotalAmount);
            total.setHasArAmt(hasArAmt);
        }
        pageInfo.setListNum(total);
        return pageInfo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateBillHXStatus(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        if (CollectionUtil.isEmpty(inData.getBillList())) {
            throw new BusinessException("请选择需要核销的单据！");
        }
        List<GaiaBillOfArDetailOutData> hxList = inData.getBillList();
        BigDecimal hxTotalAmt = hxList.stream().map(GaiaBillOfArDetailOutData::getArAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        GaiaBillOfAr billInfo = new GaiaBillOfAr();
        inData.setClient(loginUser.getClient());
        if (ObjectUtil.isNotEmpty(inData.getBillCode())) {
            if (BigDecimal.ZERO.compareTo(hxTotalAmt)>0) {
                throw new BusinessException("核销总金额，需要大于或等于0！");
            }
            billInfo = gaiaBillOfArMapper.getBillInfo(inData);
            // GAIA_BILL_OF_AR中的  flag = (-1*（发生金额 - 已核销金额）- 本次核销的总金额)
            // flag >= 0 才能核销
            BigDecimal flag = billInfo.getArAmt().multiply(new BigDecimal("-1")).subtract(billInfo.getHxAmt()).subtract(hxTotalAmt);
            if (BigDecimal.ZERO.compareTo(flag) > 0 && flag.abs().compareTo(new BigDecimal("0.01")) >= 0) {
                throw new BusinessException("总核销金额不能大于单据金额！");
            }
        }

        Date now = new Date();
        for (GaiaBillOfArDetailOutData bill : inData.getBillList()) {
            GaiaBillOfArCancel cancel = new GaiaBillOfArCancel();
            cancel.setClient(loginUser.getClient());
            cancel.setDcCode(bill.getDcCode());
            cancel.setArBillCode(inData.getBillCode());
            cancel.setArAmt(bill.getArAmt());
            cancel.setArMethodDate(now);
            cancel.setRemark(bill.getRemark());
            cancel.setCusSelfCode(bill.getCusSelfCode());
            inData.setDcCode(bill.getDcCode());
            inData.setCusSelfCode(bill.getCusSelfCode());
            GaiaBillOfArOutData cusInfo = gaiaBillOfArMapper.getCusInfo(inData);
            cancel.setCusName(cusInfo.getCusName());
            cancel.setMatDnId(bill.getMatDnId());
            cancel.setMatPoId(bill.getMatPoId());
            cancel.setInvoiceAmt(BigDecimal.ZERO);
            cancel.setSettlementAmt(BigDecimal.ZERO);
            cancel.setIsDelete(0);
            cancel.setCreateUser(loginUser.getUserId());
            cancel.setCreateTime(now);
            cancel.setUpdateTime(now);
            cancel.setUpdateUser(loginUser.getUserId());
            inData.setDcCode(bill.getDcCode());
            inData.setMatDnId(bill.getMatDnId());
            inData.setMatPoId(bill.getMatPoId());
            List<GaiaBillOfArDetailOutData> salesInfo = gaiaBillOfArMapper.listSalesBills(inData);
            if (CollectionUtil.isEmpty(salesInfo)) {
                throw new BusinessException("为查询到该单据，请重新核销！");
            }
            BigDecimal lastAmt = bill.getArAmt().add(salesInfo.get(0).getHasArAmt());
            if (BigDecimal.ZERO.compareTo(salesInfo.get(0).getAddTotalAmount()) > 0) {
                if (BigDecimal.ZERO.compareTo(bill.getArAmt()) <= 0) {
                    throw new BusinessException("单据:"+bill.getMatPoId()+"发生金额与核销金额的正负不一致！");
                }
            } else {
                if (BigDecimal.ZERO.compareTo(bill.getArAmt()) > 0) {
                    throw new BusinessException("单据:"+bill.getMatPoId()+"生金额与核销金额的正负不一致！");
                }
            }
            int status = 0;
            BigDecimal subtract = lastAmt.subtract(salesInfo.get(0).getAddTotalAmount());
            if (BigDecimal.ZERO.compareTo(salesInfo.get(0).getAddTotalAmount()) < 0) {
                if (subtract.abs().compareTo(new BigDecimal("0.01")) < 0) {
                    status = 2;
                } else if (subtract.compareTo(new BigDecimal("0.01")) > 0) {
                    throw new BusinessException("单据号：" + bill.getMatPoId() + ",的核销金额，不能超过未核销金额！");
                } else if (subtract.compareTo(new BigDecimal("-0.01")) <= 0) {
                    status = 1;
                }
            } else {
                if (subtract.abs().compareTo(new BigDecimal("0.01")) < 0) {
                    status = 2;
                } else if (subtract.compareTo(new BigDecimal("-0.01")) < 0) {
                    throw new BusinessException("单据号：" + bill.getMatPoId() + ",的核销金额，不能超过未核销金额！");
                } else if (subtract.compareTo(new BigDecimal("0.01")) >= 0) {
                    status = 1;
                }
            }

            bill.setHxStatus(status);
            bill.setClient(loginUser.getClient());
            //查询该单据的核销金额
            if ("XD".equals(bill.getMatType())) {
                //销售
                //更新调拨主表 核销状态
                gaiaBillOfArCancelMapper.updateDiaoBoZ(bill);
            }
            if ("ED".equals(bill.getMatType())) {
                //销退
                //更新退库单主表 核销状态
                gaiaBillOfArCancelMapper.updateTKZ(bill);
            }
            gaiaBillOfArCancelMapper.insert(cancel);
            gaiaBillOfArMapper.updateByHxAmt(cancel);
            GaiaCustomerBusiness gaiaCustomerBusiness = gaiaCustomerBusinessMapper.selectByCusArAmt(cancel);
            if (gaiaCustomerBusiness.getCusArAmt() == null){
                gaiaCustomerBusiness.setCusArAmt(BigDecimal.ZERO);
            }
            gaiaCustomerBusinessMapper.UpdateByCusArAmt(cancel,gaiaCustomerBusiness.getCusArAmt());
        }
        if (ObjectUtil.isNotEmpty(inData.getBillCode())) {
            //更新 GAIA_BILL_OF_AR 中的已核销金额
            GaiaBillOfAr gaiaBillOfAr = gaiaBillOfArMapper.getHxAmt(inData);
            BigDecimal hxAmt = BigDecimal.ZERO;
            if (ObjectUtil.isNotEmpty(gaiaBillOfAr.getHxAmt())) {
                hxAmt = hxAmt.add(gaiaBillOfAr.getHxAmt());
            }
            int status = 0;
            BigDecimal subtract = billInfo.getArAmt().abs().subtract(hxAmt);
            if (subtract.abs().compareTo(new BigDecimal("0.01")) < 0) {
                status = 2;
            } else if (subtract.compareTo(new BigDecimal("-0.01")) < 0) {
                throw new BusinessException("核销金额不能超过回款单未核销金额");
            } else if (subtract.compareTo(new BigDecimal("0.01")) >= 0) {
                status = 1;
            }
            billInfo.setUpdateTime(now);
            billInfo.setUpdateUser(loginUser.getUserId());
            billInfo.setHxAmt(hxAmt);
            billInfo.setStatus(status);
            gaiaBillOfArMapper.update(billInfo);
            GaiaCustomerBusiness gaiaCustomerBusiness = gaiaCustomerBusinessMapper.selectBillByCusArAmt(billInfo);
            if (gaiaCustomerBusiness.getCusArAmt() == null){
                gaiaCustomerBusiness.setCusArAmt(BigDecimal.ZERO);
            }
            gaiaCustomerBusinessMapper.UpdateBillByCusArAmt(billInfo,gaiaCustomerBusiness.getCusArAmt());
        }
        return true;
    }

    @Override
    public PageInfo listReceivableDetail(GetLoginOutData loginUser, GaiaBillOfArInData inDate) {
        PageInfo pageInfo = new PageInfo();
        List<GaiaBillOfArDetailOutData> outDataList = gaiaBillOfArMapper.listReceivableDetail(inDate);
        List<GaiaBillOfArDetailOutData> cusList = gaiaBillOfArMapper.listCusInfo(inDate);
        GaiaBillOfArDetailOutData total = new GaiaBillOfArDetailOutData();
        total.setDcCode("合计");
        if (CollectionUtil.isEmpty(outDataList)) {
            total.setHasArAmt(BigDecimal.ZERO);
            total.setAddTotalAmount(BigDecimal.ZERO);
        } else {
            for (GaiaBillOfArDetailOutData outData : outDataList) {
                for (GaiaBillOfArDetailOutData cus : cusList) {
                    if (outData.getCusSelfCode().equals(cus.getCusSelfCode())) {
                        outData.setCusName(cus.getCusName());
                        break;
                    }
                }

            }
            BigDecimal addTotalAmount = outDataList.stream().map(GaiaBillOfArDetailOutData::getAddTotalAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal hasArAmt = outDataList.stream().map(GaiaBillOfArDetailOutData::getHasArAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
            total.setAddTotalAmount(addTotalAmount);
            total.setHasArAmt(hasArAmt);
        }
        pageInfo.setList(outDataList);
        pageInfo.setListNum(total);
        return pageInfo;
    }

    @Override
    public com.gys.common.response.Result exportBill(GetLoginOutData userInfo, GaiaBillOfArInData inData) {
        List<GaiaBillOfArOutData> outData = gaiaBillOfArMapper.listBill(inData);
        GaiaBillOfArOutData total = new GaiaBillOfArOutData();
        total.setDcCode("合计");
        if (CollectionUtil.isEmpty(outData)) {
            total.setArAmt(BigDecimal.ZERO);
        } else {
            for (GaiaBillOfArOutData outDatum : outData) {
                String gssNameStr="";
                String gssCode = outDatum.getGssCode();
                String gssName = outDatum.getGssName();
                if(StrUtil.isNotBlank(gssCode) && StrUtil.isNotBlank(gssCode)){
                    gssNameStr=gssCode+"-"+gssName;
                }else if(StrUtil.isNotBlank(gssCode) && StrUtil.isBlank(gssName)){
                    gssNameStr=gssCode;
                }else if(StrUtil.isBlank(gssCode) && StrUtil.isNotBlank(gssName)){
                    gssNameStr=gssName;
                }
                outDatum.setGssNameStr(gssNameStr);
            }
            BigDecimal arAmt = outData.stream().map(GaiaBillOfArOutData::getArAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
            total.setArAmt(arAmt);
        }
        String fileName = "批发应收管理导出";
        outData.add(total);
        if (outData.size() > 0) {
            CsvFileInfo csvInfo = null;
            // 导出
            // byte数据
            csvInfo = CsvClient.getCsvByte(outData, fileName, Collections.singletonList((short) 1));

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据!");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object importBill(MultipartFile file, GetLoginOutData userInfo) {
        //检查文件以及数据完整性
        List<GaiaBillOfArInData> insertDataList = checkFileData(file);
        for (int i = 0; i < insertDataList.size(); i++) {
            GaiaBillOfArInData billOfArInData = insertDataList.get(i);
            billOfArInData.setClient(userInfo.getClient());
            billOfArInData.setArAmt(new BigDecimal(billOfArInData.getAmt()));
            billOfArInData.setBillDate(DateUtil.stringToDate(billOfArInData.getBillDateStr(), "yyyy-MM-dd"));
            if (StringUtils.isEmpty(billOfArInData.getDcCode())) {
                throw new BusinessException("第" + (i + 2) + "行数据，仓库编码不能为空！");
            }
            if (cn.hutool.core.util.ObjectUtil.isEmpty(billOfArInData.getCusSelfCode())) {
                throw new BusinessException("第" + (i + 2) + "行数据，客户编码不能为空！");
            }
            if (cn.hutool.core.util.ObjectUtil.isEmpty(billOfArInData.getArMethodId())) {
                throw new BusinessException("第" + (i + 2) + "行数据，结算金额不能为空！");
            }
            if (cn.hutool.core.util.ObjectUtil.isEmpty(billOfArInData.getBillDate())) {
                throw new BusinessException("第" + (i + 2) + "行数据，发生日期不能为空！");
            }
            this.insertBill(userInfo, billOfArInData);
        }
        return true;
    }

    @Override
    public Result exportReceivableDetail(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        List<GaiaBillOfArDetailOutData> outDataList = gaiaBillOfArMapper.listReceivableDetail(inData);
        GaiaBillOfArInData param = new GaiaBillOfArInData();
        param.setClient(loginUser.getClient());
        if (CollectionUtil.isNotEmpty(outDataList)) {
            List<GaiaBillOfArDetailOutData> cusList = gaiaBillOfArMapper.listCusInfo(param);
            for (GaiaBillOfArDetailOutData outData : outDataList) {
                setGssNameStr(outData);
                for (GaiaBillOfArDetailOutData cus : cusList) {
                    if (outData.getCusSelfCode().equals(cus.getCusSelfCode())) {
                        outData.setCusName(cus.getCusName());
                        break;
                    }
                }

            }
        }
        GaiaBillOfArDetailOutData total = new GaiaBillOfArDetailOutData();
        BigDecimal addTotalAmount = outDataList.stream().map(GaiaBillOfArDetailOutData::getAddTotalAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal hasArAmt = outDataList.stream().map(GaiaBillOfArDetailOutData::getHasArAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        total.setDcCode("合计");
        total.setAddTotalAmount(addTotalAmount);
        total.setHasArAmt(hasArAmt);
        String fileName = "批发应收明细导出";
        outDataList.add(total);
        if (outDataList.size() > 0) {
            CsvFileInfo csvInfo = null;
            // 导出
            // byte数据
            csvInfo = CsvClient.getCsvByte(outDataList, fileName, Collections.singletonList((short) 1));

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据!");
        }
    }
    private void setGssNameStr(GaiaBillOfArDetailOutData outData){
        String gssNameStr="";
        String gssCode = outData.getGssCode();
        String gssName = outData.getGssName();
        if(StrUtil.isNotBlank(gssCode) && StrUtil.isNotBlank(gssName)){
            gssNameStr=gssCode+"-"+gssName;
        }else if(StrUtil.isNotBlank(gssCode) && StrUtil.isBlank(gssName)){
            gssNameStr=gssCode;
        }else if(StrUtil.isBlank(gssCode) && StrUtil.isNotBlank(gssName)){
            gssNameStr=gssName;
        }
        outData.setGssNameStr(gssNameStr);
        String soDnByNameStr="";
        String soDnByCode = outData.getSoDnByCode();
        String soDnByName = outData.getSoDnByName();
        if(StrUtil.isNotBlank(soDnByCode) && StrUtil.isNotBlank(soDnByName)){
            soDnByNameStr=soDnByCode+"-"+soDnByName;
        }else if(StrUtil.isNotBlank(soDnByCode) && StrUtil.isBlank(soDnByName)){
            soDnByNameStr=soDnByCode;
        }else if(StrUtil.isBlank(soDnByCode) && StrUtil.isNotBlank(soDnByName)){
            soDnByNameStr=soDnByName;
        }
        outData.setSoDnByNameStr(soDnByNameStr);
    }
    @Override
    public PageInfo<GaiaBillOfArReportData> listReport(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        PageInfo pageInfo = new PageInfo();
        List<GaiaBillOfArReportData> outDataList = gaiaBillOfArMapper.listReport(inData);
        if (!CollectionUtil.isEmpty(outDataList)) {
            for (GaiaBillOfArReportData outData : outDataList) {
                /**
                 * 一月内：     c= 当前余额（a） - 9月的9999的发生额（b） （c>0 为 b ，c<0 为 a ）
                 * 三月内：     e =  a - 一月内 - 7月和8月的9999的发生额额 （d） (e>0 且 e>d 为d ,e>0 且 e<d 为 e   ，e<0 为 0 )
                 * 六月内：     g =  a - 一月内 - 三月内 - 4月5月6月的9999的发生额（f） (g>0 且 g>f  为f, g>0 且 g<f  为g，g<0为  0)
                 * 六月以上 :   h= 当前余额(a) - 一月内(c) - 三月内(e)- 六月内 (g)
                 */
                //当前余额
                BigDecimal a = outData.getDqye();
                //9月的9999的发生额（b）
                BigDecimal b = outData.getOne();
                //一月内
                BigDecimal c = a.subtract(b);
                if (BigDecimal.ZERO.compareTo(a) == 0) {
                    outData.setOneMonth(a);
                } else if (BigDecimal.ZERO.compareTo(c) <= 0) {
                    //c > 0
                    outData.setOneMonth(b);
                } else {
                    outData.setOneMonth(c);
                }
                //7月和8月的9999的发生额（d）
                BigDecimal d = outData.getThree();
                BigDecimal e = a.subtract(outData.getOneMonth()).subtract(d);
                //三月内
                if (e.compareTo(BigDecimal.ZERO) < 0) {
                        outData.setThreeMonth(d);
                } else {
                    outData.setThreeMonth(d);
                }
                //4月5月6月的9999的发生额（f）
                BigDecimal f = outData.getSix();
                BigDecimal g = a.subtract(outData.getOneMonth()).subtract(outData.getThreeMonth()).subtract(f);
                //六月内
                if (g.compareTo(BigDecimal.ZERO) >= 0) {

                        outData.setSixMonth(f);
                } else {
                    outData.setSixMonth(a.subtract(c).subtract(e));
                }
                //六月以上
                BigDecimal h = a.subtract(outData.getOneMonth()).subtract(outData.getThreeMonth()).subtract(outData.getSixMonth());
                if (BigDecimal.ZERO.compareTo(h) <= 0) {
                    outData.setOverSixMonth(h);
                } else {
                    outData.setOverSixMonth(BigDecimal.ZERO);
                }
                Date billDate = inData.getBillDate();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(billDate);
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                String last= format.format(calendar.getTime());
                outData.setEndTime(last);
                outData.setStartTime(format.format(billDate));
            }
        }
        pageInfo.setList(outDataList);
        GaiaBillOfArReportData total = new GaiaBillOfArReportData();
        total.setDcCode("合计");
        if (CollectionUtil.isEmpty(outDataList)) {
            total.setBqys(BigDecimal.ZERO);
            total.setBqhk(BigDecimal.ZERO);
            total.setDqye(BigDecimal.ZERO);
            total.setOneMonth(BigDecimal.ZERO);
            total.setThreeMonth(BigDecimal.ZERO);
            total.setSixMonth(BigDecimal.ZERO);
            total.setOverSixMonth(BigDecimal.ZERO);
            total.setOpeningAmount(BigDecimal.ZERO);
        } else {
            BigDecimal openingAmount = outDataList.stream().map(GaiaBillOfArReportData::getOpeningAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal bqys = outDataList.stream().map(GaiaBillOfArReportData::getBqys).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal bqhk = outDataList.stream().map(GaiaBillOfArReportData::getBqhk).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal dqye = outDataList.stream().map(GaiaBillOfArReportData::getDqye).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal oneMonth = outDataList.stream().map(GaiaBillOfArReportData::getOneMonth).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal threeMonth = outDataList.stream().map(GaiaBillOfArReportData::getThreeMonth).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal sixMonth = outDataList.stream().map(GaiaBillOfArReportData::getSixMonth).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal overSixMonth = outDataList.stream().map(GaiaBillOfArReportData::getOverSixMonth).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal adjustment = outDataList.stream().map(GaiaBillOfArReportData::getAdjustment).reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal other = outDataList.stream().map(GaiaBillOfArReportData::getOther).reduce(BigDecimal.ZERO, BigDecimal::add);
            total.setBqys(bqys);
            total.setBqhk(bqhk);
            total.setDqye(dqye);
            total.setOneMonth(oneMonth);
            total.setThreeMonth(threeMonth);
            total.setSixMonth(sixMonth);
            total.setOverSixMonth(overSixMonth);
            total.setOpeningAmount(openingAmount);
            total.setAdjustment(adjustment);
            total.setOther(other);
        }
        pageInfo.setListNum(total);

        return pageInfo;
    }

    @Override
    public void exportListReport(GetLoginOutData loginUser,HttpServletResponse response, GaiaBillOfArInData inData) {
        //调用查询接口 查询数据 然后导出
        PageInfo pageInfo = this.listReport(loginUser, inData);
        if (Objects.isNull(pageInfo) && CollUtil.isEmpty(pageInfo.getList())) {
            throw new BusinessException("提示：导出数据为空");
        }
        List<GaiaBillOfArReportData> outData = pageInfo.getList();
        GaiaBillOfArReportData listNum = (GaiaBillOfArReportData) pageInfo.getListNum();
        for (GaiaBillOfArReportData data : outData){
            StringBuilder stringBuilder = new StringBuilder();
            data.setCusSelfCodeByName(String.valueOf(stringBuilder.append(data.getCusSelfCode()).append("-").append(data.getCusName())));
            StringBuilder builder = new StringBuilder();
            data.setDcCodeByDcName(String.valueOf(builder.append(data.getDcCode()).append("-").append(data.getDcName())));
        }


        CsvFileInfo fileInfo = new CsvFileInfo(new byte[0], 0, "批发应收汇总导出");

        CsvClient.endHandle(response, outData, fileInfo, () -> {
            if (cn.hutool.core.util.ObjectUtil.isEmpty(fileInfo.getFileContent())) {
                throw new BusinessException("导出数据不能为空！");
            }
            byte[] bytes = ("\r\n合计,," + listNum.getOpeningAmount() + "," + listNum.getBqys() + "," + listNum.getBqhk() + "," + "," + listNum.getAdjustment() + "," + listNum.getOther() + ","+ listNum.getDqye() + "," + listNum.getOneMonth() + "," + listNum.getThreeMonth() + "," + listNum.getSixMonth() + "," + listNum.getOverSixMonth()).getBytes(StandardCharsets.UTF_8);
            byte[] all = ArrayUtils.addAll(fileInfo.getFileContent(), bytes);
            fileInfo.setFileContent(all);
            fileInfo.setFileSize(all.length);
        });
    }

    @Override
    public List<GaiaFiArPayment> listBillType(GetLoginOutData loginUser, GaiaFiArPayment inData) {
        inData.setIsDelete(0);
        List<GaiaFiArPayment> result = gaiaFiArPaymentMapper.queryAll(inData);
        //没有就新增 初始化 单据类型
        if (CollectionUtil.isEmpty(result)) {
            List<GaiaFiArPayment> insertList = new ArrayList<>();
            setBillType(new Date(), insertList, inData.getClient());
            gaiaFiArPaymentMapper.insertBatch(insertList);
            return insertList;
        } else {
            return result;
        }
    }

    @Override
    public Object insertGaiaFiArPayment(GetLoginOutData loginUser, GaiaFiArPayment inData) {
        List<String> clientList = gaiaFiArPaymentMapper.listFrance(inData);
        if (CollectionUtil.isNotEmpty(clientList)) {
            Date now = new Date();
            List<GaiaFiArPayment> insertList = new ArrayList<>();
            for (String client : clientList) {
                setBillType(now, insertList, client);
            }
            gaiaFiArPaymentMapper.insertBatch(insertList);
        }
        return true;
    }

    @Override
    public Object deleteArBill(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        //根据日期删除 生成的应收
        gaiaBillOfArMapper.deleteArBill(inData);
        return true;
    }


    @Override
    @Transactional
    public Object insertArBill(GetLoginOutData loginUser, GaiaBillOfArInData inData) {
        //查询出所有客户
        List<Map<String, Object>> mapList = gaiaBillOfArMapper.listHasCustomer(inData);
        Date now = new Date();
        for (Map<String, Object> map : mapList) {
            inData.setDcCode(map.get("dcCode").toString());
            inData.setCusSelfCode(map.get("cusSelfCode").toString());
            List<GaiaBillOfArDetailOutData> outDataList = gaiaBillOfArMapper.listUnReceivableDetail(inData);
            if (CollectionUtil.isNotEmpty(outDataList)) {
                for (GaiaBillOfArDetailOutData gaiaBillOfArDetailOutData : outDataList) {
                    GaiaBillOfAr bill = new GaiaBillOfAr();
                    bill.setClient(inData.getClient());
                    String billCode1 = gaiaBillOfArMapper.getBillCode(inData);
                    bill.setBillCode(billCode1);
                    bill.setDcCode(inData.getDcCode());
                    bill.setArMethodId("9999");
                    bill.setArAmt(gaiaBillOfArDetailOutData.getAddTotalAmount());
                    bill.setHxAmt(BigDecimal.ZERO);
                    bill.setCusName(map.get("cusName").toString());
                    bill.setCusSelfCode(map.get("cusSelfCode").toString());
                    bill.setBillDate(gaiaBillOfArDetailOutData.getBillDate());
                    bill.setStatus(0);
                    bill.setIsDelete(0);
                    bill.setCreateTime(now);
                    bill.setCreateUser(loginUser.getUserId());
                    bill.setUpdateTime(now);
                    bill.setUpdateUser(loginUser.getUserId());
                    gaiaBillOfArMapper.insert(bill);
                    //修改客户余额
                    updatLastAmt(loginUser, inData, bill, now);
                }
            }
        }

        return null;
    }



    private void setBillType(Date now, List<GaiaFiArPayment> insertList, String client) {
        GaiaFiArPayment a = new GaiaFiArPayment();
        a.setClient(client);
        a.setArMethodId("3333");
        a.setArMethodName("应收调整");
        a.setIsDelete(0);
        a.setCreateUser("system");
        a.setCreateTime(now);
        a.setUpdateTime(now);
        a.setUpdateUser("system");
        GaiaFiArPayment b = new GaiaFiArPayment();
        b.setClient(client);
        b.setArMethodId("5555");
        b.setArMethodName("其他");
        b.setIsDelete(0);
        b.setCreateUser("system");
        b.setCreateTime(now);
        b.setUpdateTime(now);
        b.setUpdateUser("system");
        GaiaFiArPayment c = new GaiaFiArPayment();
        c.setClient(client);
        c.setArMethodId("6666");
        c.setArMethodName("应收回款");
        c.setIsDelete(0);
        c.setCreateUser("system");
        c.setCreateTime(now);
        c.setUpdateTime(now);
        c.setUpdateUser("system");
        GaiaFiArPayment d = new GaiaFiArPayment();
        d.setClient(client);
        d.setArMethodId("8888");
        d.setArMethodName("期初导入");
        d.setIsDelete(0);
        d.setCreateUser("system");
        d.setCreateTime(now);
        d.setUpdateTime(now);
        d.setUpdateUser("system");
        GaiaFiArPayment e = new GaiaFiArPayment();
        e.setClient(client);
        e.setArMethodId("9999");
        e.setArMethodName("应收增加");
        e.setIsDelete(0);
        e.setCreateUser("system");
        e.setCreateTime(now);
        e.setUpdateTime(now);
        e.setUpdateUser("system");
        insertList.add(a);
        insertList.add(b);
        insertList.add(c);
        insertList.add(d);
        insertList.add(e);
    }

    private List<GaiaBillOfArInData> checkFileData(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (ValidateUtil.isEmpty(fileName)) {
            throw new BusinessException("提示：上传文件名不能为空");
        }
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        if (!".xlsx".equals(suffixName)) {
            throw new BusinessException("提示：请检查文件格式是否为xlsx格式");
        }

        //创建sheet  第一页，第一行
        Sheet sheet = new Sheet(1, 0, GaiaBillOfArInData.class);
        //获取到 GaiaChannelProduct实体集合
        List<Object> excelDataList = null;
        try {
            excelDataList = EasyExcelFactory.read(file.getInputStream(), sheet);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("上传商品目录,读取excel失败: ", e);
            throw new BusinessException("提示：读取Excel失败");
        }
        if (ValidateUtil.isEmpty(excelDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //转换list泛型
        List<GaiaBillOfArInData> relationList = new ArrayList<>(excelDataList.size());
        try {
            relationList = JSON.parseArray(JSON.toJSONString(excelDataList), GaiaBillOfArInData.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据类型转换异常：", e);
            throw new BusinessException("提示：数据格式不正确, 请检查数据");
        }
        if (ValidateUtil.isEmpty(relationList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //校验表头
        GaiaBillOfArInData dataHead = relationList.get(0);
        String[] channelManagerExcelHead = CommonConstant.GAIA_AR_BILL_IMPORT;
        if (!channelManagerExcelHead[0].equals(dataHead.getDcCode()) && !channelManagerExcelHead[1].equals(dataHead.getCusSelfCode())
                && !channelManagerExcelHead[2].equals(dataHead.getArMethodId()) && !channelManagerExcelHead[3].equals(dataHead.getArAmt())
                && !channelManagerExcelHead[4].equals(dataHead.getBillDate()) && !channelManagerExcelHead[5].equals(dataHead.getRemark())) {
            throw new BusinessException("提示：请填写正确的表头");
        }
        //截取除表头以外数据
        List<GaiaBillOfArInData> insertData = relationList.subList(1, relationList.size());
        if (ValidateUtil.isEmpty(insertData)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        return insertData;
    }

    /**
     * 获取开票员
     * @param loginUser
     * @return
     */
    @Override
    public List<Map<String, String>> selectSoDnByList(GetLoginOutData loginUser,String soDnByName) {
        List<Map<String, String>> list =  this.gaiaBillOfArMapper.selectSoDnByList(loginUser.getClient(),soDnByName);
        return list;
    }

}
