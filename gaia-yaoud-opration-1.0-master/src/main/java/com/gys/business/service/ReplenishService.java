
package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface ReplenishService {
    PageInfo<GetReplenishOutData> selectList(GetReplenishInData inData);

    List<GetReplenishDetailOutData> detailList(GetReplenishInData inData);

    GetReplenishOutData getDetail(GetReplenishInData inData);

    void insert(GetReplenishInData inData);

//    void update(GetReplenishInData inData);

    void approve(GetReplenishInData inData, GetLoginOutData userInfo);

    GetReplenishOutData getStock(GetReplenishInData inData);

    GetReplenishOutData getParam(GetReplenishInData inData);

    GetReplenishOutData getSales(GetReplenishInData inData);

    GetReplenishOutData getCost(GetReplenishInData inData);

    GetReplenishOutData getCount(GetReplenishInData inData);

    boolean isRepeat(GetReplenishInData inData);

    GetReplenishOutData getVoucherAmt(GetReplenishInData inData);

    String selectNextVoucherId(GaiaSdReplenishH inData);

    /**
     * 特殊药品
     * @param inData
     * @return
     */
    List<SpecialMedicineOutData> specialDrugsInquiry(SpecialMedicineInData inData);

//    String queryNextVoucherId(GaiaSdReplenishH inData);
}
