//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class RemoteAuditDInData {
    private String clientId;
    private String gsradVoucherId;
    private String gsradBrId;
    private String gsradSerial;
    private String gsradProId;
    private String gsradBatchNo;
    private String gsradQty;
    private Integer pageNum;
    private Integer pageSize;

    public RemoteAuditDInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsradVoucherId() {
        return this.gsradVoucherId;
    }

    public String getGsradBrId() {
        return this.gsradBrId;
    }

    public String getGsradSerial() {
        return this.gsradSerial;
    }

    public String getGsradProId() {
        return this.gsradProId;
    }

    public String getGsradBatchNo() {
        return this.gsradBatchNo;
    }

    public String getGsradQty() {
        return this.gsradQty;
    }

    public Integer getPageNum() {
        return this.pageNum;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsradVoucherId(final String gsradVoucherId) {
        this.gsradVoucherId = gsradVoucherId;
    }

    public void setGsradBrId(final String gsradBrId) {
        this.gsradBrId = gsradBrId;
    }

    public void setGsradSerial(final String gsradSerial) {
        this.gsradSerial = gsradSerial;
    }

    public void setGsradProId(final String gsradProId) {
        this.gsradProId = gsradProId;
    }

    public void setGsradBatchNo(final String gsradBatchNo) {
        this.gsradBatchNo = gsradBatchNo;
    }

    public void setGsradQty(final String gsradQty) {
        this.gsradQty = gsradQty;
    }

    public void setPageNum(final Integer pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final Integer pageSize) {
        this.pageSize = pageSize;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RemoteAuditDInData)) {
            return false;
        } else {
            RemoteAuditDInData other = (RemoteAuditDInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label119: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label119;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label119;
                    }

                    return false;
                }

                Object this$gsradVoucherId = this.getGsradVoucherId();
                Object other$gsradVoucherId = other.getGsradVoucherId();
                if (this$gsradVoucherId == null) {
                    if (other$gsradVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsradVoucherId.equals(other$gsradVoucherId)) {
                    return false;
                }

                label105: {
                    Object this$gsradBrId = this.getGsradBrId();
                    Object other$gsradBrId = other.getGsradBrId();
                    if (this$gsradBrId == null) {
                        if (other$gsradBrId == null) {
                            break label105;
                        }
                    } else if (this$gsradBrId.equals(other$gsradBrId)) {
                        break label105;
                    }

                    return false;
                }

                Object this$gsradSerial = this.getGsradSerial();
                Object other$gsradSerial = other.getGsradSerial();
                if (this$gsradSerial == null) {
                    if (other$gsradSerial != null) {
                        return false;
                    }
                } else if (!this$gsradSerial.equals(other$gsradSerial)) {
                    return false;
                }

                label91: {
                    Object this$gsradProId = this.getGsradProId();
                    Object other$gsradProId = other.getGsradProId();
                    if (this$gsradProId == null) {
                        if (other$gsradProId == null) {
                            break label91;
                        }
                    } else if (this$gsradProId.equals(other$gsradProId)) {
                        break label91;
                    }

                    return false;
                }

                Object this$gsradBatchNo = this.getGsradBatchNo();
                Object other$gsradBatchNo = other.getGsradBatchNo();
                if (this$gsradBatchNo == null) {
                    if (other$gsradBatchNo != null) {
                        return false;
                    }
                } else if (!this$gsradBatchNo.equals(other$gsradBatchNo)) {
                    return false;
                }

                label77: {
                    Object this$gsradQty = this.getGsradQty();
                    Object other$gsradQty = other.getGsradQty();
                    if (this$gsradQty == null) {
                        if (other$gsradQty == null) {
                            break label77;
                        }
                    } else if (this$gsradQty.equals(other$gsradQty)) {
                        break label77;
                    }

                    return false;
                }

                label70: {
                    Object this$pageNum = this.getPageNum();
                    Object other$pageNum = other.getPageNum();
                    if (this$pageNum == null) {
                        if (other$pageNum == null) {
                            break label70;
                        }
                    } else if (this$pageNum.equals(other$pageNum)) {
                        break label70;
                    }

                    return false;
                }

                Object this$pageSize = this.getPageSize();
                Object other$pageSize = other.getPageSize();
                if (this$pageSize == null) {
                    if (other$pageSize != null) {
                        return false;
                    }
                } else if (!this$pageSize.equals(other$pageSize)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RemoteAuditDInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsradVoucherId = this.getGsradVoucherId();
        result = result * 59 + ($gsradVoucherId == null ? 43 : $gsradVoucherId.hashCode());
        Object $gsradBrId = this.getGsradBrId();
        result = result * 59 + ($gsradBrId == null ? 43 : $gsradBrId.hashCode());
        Object $gsradSerial = this.getGsradSerial();
        result = result * 59 + ($gsradSerial == null ? 43 : $gsradSerial.hashCode());
        Object $gsradProId = this.getGsradProId();
        result = result * 59 + ($gsradProId == null ? 43 : $gsradProId.hashCode());
        Object $gsradBatchNo = this.getGsradBatchNo();
        result = result * 59 + ($gsradBatchNo == null ? 43 : $gsradBatchNo.hashCode());
        Object $gsradQty = this.getGsradQty();
        result = result * 59 + ($gsradQty == null ? 43 : $gsradQty.hashCode());
        Object $pageNum = this.getPageNum();
        result = result * 59 + ($pageNum == null ? 43 : $pageNum.hashCode());
        Object $pageSize = this.getPageSize();
        result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());
        return result;
    }

    public String toString() {
        return "RemoteAuditDInData(clientId=" + this.getClientId() + ", gsradVoucherId=" + this.getGsradVoucherId() + ", gsradBrId=" + this.getGsradBrId() + ", gsradSerial=" + this.getGsradSerial() + ", gsradProId=" + this.getGsradProId() + ", gsradBatchNo=" + this.getGsradBatchNo() + ", gsradQty=" + this.getGsradQty() + ", pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ")";
    }
}
