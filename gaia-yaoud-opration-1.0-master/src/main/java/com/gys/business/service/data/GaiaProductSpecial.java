package com.gys.business.service.data;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value="com-gys-business-service-data-GaiaProductSpecial")
public class GaiaProductSpecial extends BaseRowModel implements Serializable {
    /**
     * 商品编码  药德通用编码
     */
    @ApiModelProperty(value="商品编码  药德通用编码")
    @ExcelProperty(index = 0)
    private String proCode;

    /**
     * 省份
     */
    @ApiModelProperty(value="省份")
    @ExcelProperty(index = 1)
    private String proProv;

    /**
     * 地市
     */
    @ApiModelProperty(value="地市")
    @ExcelProperty(index = 2)
    private String proCity;

    /**
     * 类型  1-季节，2-品牌
     */
    @ApiModelProperty(value="类型  1-季节，2-品牌")
    @ExcelProperty(index = 3)
    private String proType;

    /**
     * 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌
     */
    @ApiModelProperty(value="1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌")
    @ExcelProperty(index = 4)
    private String proFlag;

    /**
     * 状态   0-正常，1-停用
     */
    @ApiModelProperty(value="状态   0-正常，1-停用")
    @ExcelProperty(index = 5)
    private String proStatus;

    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private String proCreateUser;

    /**
     * 创建日期
     */
    @ApiModelProperty(value="创建日期")
    private String proCreateDate;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private String proCreateTime;

    /**
     * 最后更新人
     */
    @ApiModelProperty(value="最后更新人")
    private String proLastUpdateUser;

    /**
     * 最后更新日期
     */
    @ApiModelProperty(value="最后更新日期")
    private String proLastUpdateDate;

    /**
     * 最后更新时间
     */
    @ApiModelProperty(value="最后更新时间")
    private String proLastUpdateTime;

    /**
     * 修改后标志  1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌
     */
    @ApiModelProperty(value="修改后标志  1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌")
    private String changeProFlag;

    /**
     * 修改后状态 状态   0-正常，1-停用
     */
    @ApiModelProperty(value="修改后状态 状态   0-正常，1-停用")
    private String changeProStatus;

}