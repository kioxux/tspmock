package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/10 14:04
 **/
@Data
public class OrderOperateForm {
    @ApiModelProperty(value = "加盟商", example = "10000001", hidden = true)
    private String client;
    @ApiModelProperty(value = "用户ID", example = "1116")
    private String userId;
    @ApiModelProperty(value = "审核日期",example = "")
    private String reviewDate;
    @ApiModelProperty(value = "委托配送方编码", example = "10000001")
    private String supplierCode;
    @ApiModelProperty(value = "补货单号", example = "PD091232001")
    private String replenishCode;
    @ApiModelProperty(value = "门店编码", example = "10001")
    private String storeId;
    @ApiModelProperty(value = "备注", example = "备注备注")
    private String remarks;
    @ApiModelProperty(value = "补货明细列表", example = "[]")
    private List<ReplenishDetail> detailList;

    @Data
    public static class ReplenishDetail {
        @ApiModelProperty(value = "商品编码", example = "4210003")
        private String proSelfCode;
        @ApiModelProperty(value = "要货数量", example = "11")
        private BigDecimal requestQuantity;
        @ApiModelProperty(value = "配送单价", example = "178")
        private BigDecimal sendPrice;
    }

}
