package com.gys.business.service;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ImportPercentageData {
    private String proCode;
    @ApiModelProperty(value = "提成级别 1 一级 2 二级 3 三级")
    private String tichengLevel;
    @ApiModelProperty(value = "达成数量1")
    private String seleQty;
    @ApiModelProperty(value = "提成金额1")
    private String tichengAmt;
    @ApiModelProperty(value = "提成比例1")
    private String tichengRate;
    @ApiModelProperty(value = "达成数量2")
    private String seleQty2;
    @ApiModelProperty(value = "提成金额2")
    private String tichengAmt2;
    @ApiModelProperty(value = "提成比例2")
    private String tichengRate2;
    @ApiModelProperty(value = "达成数量3")
    private String seleQty3;
    @ApiModelProperty(value = "提成金额3")
    private String tichengAmt3;
    @ApiModelProperty(value = "提成比例3")
    private String tichengRate3;
}
