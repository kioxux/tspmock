//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetMemberCardReportInData;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface MemberCardReportService {
    Map<String, Object> queryReport(@RequestBody GetMemberCardReportInData inData);
}
