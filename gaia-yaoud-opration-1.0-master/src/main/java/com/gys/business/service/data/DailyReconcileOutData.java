package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DailyReconcileOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "单号")
    private String gpdhVoucherId;
    @ApiModelProperty(value = "加盟商")
    private String gpdhBrId;
    @ApiModelProperty(value = "店号")
    private String gpdhSaleDate;
    @ApiModelProperty(value = "审核日期")
    private String gpdhCheckDate;
    @ApiModelProperty(value = "审核时间")
    private String gpdhCheckTime;
    @ApiModelProperty(value = "对账员工")
    private String gpdhEmp;
    @ApiModelProperty(value = "应收金额汇总")
    private String gpdhtotalSalesAmt;
    @ApiModelProperty(value = "对账金额汇总")
    private String gpdhtotalInputAmt;
    @ApiModelProperty(value = "审核状态")
    private String gpdhStatus;
    private List<DailyReconcileDetailOutData> detailOutDataList;
    @ApiModelProperty(value = "序号")
    private Integer index;
}
