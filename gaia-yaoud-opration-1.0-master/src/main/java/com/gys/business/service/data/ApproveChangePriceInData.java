//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class ApproveChangePriceInData {
    private String clientId;
    private String billNo;
    private String index;
    private String proCode;
    private String storeCode;
    private String changePrice;
    private String changeRate;

    public ApproveChangePriceInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBillNo() {
        return this.billNo;
    }

    public String getIndex() {
        return this.index;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getStoreCode() {
        return this.storeCode;
    }

    public String getChangePrice() {
        return this.changePrice;
    }

    public String getChangeRate() {
        return this.changeRate;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBillNo(final String billNo) {
        this.billNo = billNo;
    }

    public void setIndex(final String index) {
        this.index = index;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setStoreCode(final String storeCode) {
        this.storeCode = storeCode;
    }

    public void setChangePrice(final String changePrice) {
        this.changePrice = changePrice;
    }

    public void setChangeRate(final String changeRate) {
        this.changeRate = changeRate;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ApproveChangePriceInData)) {
            return false;
        } else {
            ApproveChangePriceInData other = (ApproveChangePriceInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label95;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label95;
                    }

                    return false;
                }

                Object this$billNo = this.getBillNo();
                Object other$billNo = other.getBillNo();
                if (this$billNo == null) {
                    if (other$billNo != null) {
                        return false;
                    }
                } else if (!this$billNo.equals(other$billNo)) {
                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                label74: {
                    Object this$proCode = this.getProCode();
                    Object other$proCode = other.getProCode();
                    if (this$proCode == null) {
                        if (other$proCode == null) {
                            break label74;
                        }
                    } else if (this$proCode.equals(other$proCode)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$storeCode = this.getStoreCode();
                    Object other$storeCode = other.getStoreCode();
                    if (this$storeCode == null) {
                        if (other$storeCode == null) {
                            break label67;
                        }
                    } else if (this$storeCode.equals(other$storeCode)) {
                        break label67;
                    }

                    return false;
                }

                Object this$changePrice = this.getChangePrice();
                Object other$changePrice = other.getChangePrice();
                if (this$changePrice == null) {
                    if (other$changePrice != null) {
                        return false;
                    }
                } else if (!this$changePrice.equals(other$changePrice)) {
                    return false;
                }

                Object this$changeRate = this.getChangeRate();
                Object other$changeRate = other.getChangeRate();
                if (this$changeRate == null) {
                    if (other$changeRate != null) {
                        return false;
                    }
                } else if (!this$changeRate.equals(other$changeRate)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ApproveChangePriceInData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $billNo = this.getBillNo();
        result = result * 59 + ($billNo == null ? 43 : $billNo.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $storeCode = this.getStoreCode();
        result = result * 59 + ($storeCode == null ? 43 : $storeCode.hashCode());
        Object $changePrice = this.getChangePrice();
        result = result * 59 + ($changePrice == null ? 43 : $changePrice.hashCode());
        Object $changeRate = this.getChangeRate();
        result = result * 59 + ($changeRate == null ? 43 : $changeRate.hashCode());
        return result;
    }

    public String toString() {
        return "ApproveChangePriceInData(clientId=" + this.getClientId() + ", billNo=" + this.getBillNo() + ", index=" + this.getIndex() + ", proCode=" + this.getProCode() + ", storeCode=" + this.getStoreCode() + ", changePrice=" + this.getChangePrice() + ", changeRate=" + this.getChangeRate() + ")";
    }
}
