package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.common.data.wechatTemplate.GaiaMouldMaintainDTO;
import com.gys.common.data.wechatTemplate.GaiaMouldMaintainVO;

import java.util.List;

public interface TemplateMaintainService {

    // 查询微信模板列表
    PageInfo<GaiaMouldMaintainVO> queryTemplateList(String mouldName, String status, Integer pageNum, Integer pageSize);

    // 新增微信模板
    void insertTemplate(String client, GaiaMouldMaintainVO gaiaMouldMaintainVO);

    // 删除微信模板
    void delTemplate(String mouldCode);

    // 启用微信模板
    void startTemplate(String mouldCode);

    // 停用微信模板
    void stopTemplate(String mouldCode);

    // 编辑微信模板
    void updateTemplate(GaiaMouldMaintainVO gaiaMouldMaintainVO);

    // 根据模板编码查询模板
    GaiaMouldMaintainVO queryTemplateByCode(String mouldCode);
}
