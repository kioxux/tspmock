package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
    * 积分异动
    */
@ApiModel(value="com-gys-business-service-data-GaiaSdIntegralChange")
@Data
public class GaiaSdIntegralChange {
    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 店号
    */
    @ApiModelProperty(value="店号")
    private String gsicBrId;

    /**
    * 异动业务单号
    */
    @ApiModelProperty(value="异动业务单号")
    private String gsicVoucherId;

    /**
    * 单据类型：1销售单 2手工修改单 3其他
    */
    @ApiModelProperty(value="单据类型：1销售单 2手工修改单 3其他")
    private String gsicFlag;

    /**
    * 异动日期
    */
    @ApiModelProperty(value="异动日期")
    private String gsicDate;

    /**
    * 异动类型：1增加积分 2积分抵现 3积分兑换 4积分升级 5手工修改
    */
    @ApiModelProperty(value="异动类型：1增加积分 2积分抵现 3积分兑换 4积分升级 5手工修改")
    private String gsicType;

    /**
    * 会员卡号
    */
    @ApiModelProperty(value="会员卡号")
    private String gsicCardId;

    /**
    * 初始积分
    */
    @ApiModelProperty(value="初始积分")
    private BigDecimal gsicInitialIntegral;

    /**
    * 异动积分
    */
    @ApiModelProperty(value="异动积分")
    private BigDecimal gsicChangeIntegral;

    /**
    * 结果积分
    */
    @ApiModelProperty(value="结果积分")
    private BigDecimal gsicResultIntegral;

    /**
    * 修改人员
    */
    @ApiModelProperty(value="修改人员")
    private String gsicEmp;

    /**
     * 操作原因
     */
    private String remark;

}