//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class GetSaleScheduleQueryInData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    private int pageNum;
    private int pageSize;
    private String bc;
    private String emp;
    private String clientId;
    private String brId;

    public GetSaleScheduleQueryInData() {
    }

    public int getPageNum() {
        return this.pageNum;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public String getBc() {
        return this.bc;
    }

    public String getEmp() {
        return this.emp;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public void setPageNum(final int pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final int pageSize) {
        this.pageSize = pageSize;
    }

    public void setBc(final String bc) {
        this.bc = bc;
    }

    public void setEmp(final String emp) {
        this.emp = emp;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSaleScheduleQueryInData)) {
            return false;
        } else {
            GetSaleScheduleQueryInData other = (GetSaleScheduleQueryInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getPageNum() != other.getPageNum()) {
                return false;
            } else if (this.getPageSize() != other.getPageSize()) {
                return false;
            } else {
                label64: {
                    Object this$bc = this.getBc();
                    Object other$bc = other.getBc();
                    if (this$bc == null) {
                        if (other$bc == null) {
                            break label64;
                        }
                    } else if (this$bc.equals(other$bc)) {
                        break label64;
                    }

                    return false;
                }

                label57: {
                    Object this$emp = this.getEmp();
                    Object other$emp = other.getEmp();
                    if (this$emp == null) {
                        if (other$emp == null) {
                            break label57;
                        }
                    } else if (this$emp.equals(other$emp)) {
                        break label57;
                    }

                    return false;
                }

                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$brId = this.getBrId();
                Object other$brId = other.getBrId();
                if (this$brId == null) {
                    if (other$brId != null) {
                        return false;
                    }
                } else if (!this$brId.equals(other$brId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSaleScheduleQueryInData;
    }

    public int hashCode() {
        int result = 1;
        result = result * 59 + this.getPageNum();
        result = result * 59 + this.getPageSize();
        Object $bc = this.getBc();
        result = result * 59 + ($bc == null ? 43 : $bc.hashCode());
        Object $emp = this.getEmp();
        result = result * 59 + ($emp == null ? 43 : $emp.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        return result;
    }

    public String toString() {
        return "GetSaleScheduleQueryInData(pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ", bc=" + this.getBc() + ", emp=" + this.getEmp() + ", clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ")";
    }
}
