package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaThirdPaymentTonglian;

/**
 * 通联支付接口
 * @author huxinxin
 */
public interface TongLianService {
    long insert(GaiaThirdPaymentTonglian inData);

    GaiaThirdPaymentTonglian selectById(GaiaThirdPaymentTonglian inData);
}
