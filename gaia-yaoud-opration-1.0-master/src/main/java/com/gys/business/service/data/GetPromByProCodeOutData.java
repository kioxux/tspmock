package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author
 */
@Data
public class GetPromByProCodeOutData implements Serializable {
    private static final long serialVersionUID = 8973460358760208039L;
    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "会员类促销")
    private String promotionType;

    @ApiModelProperty(value = "促销类型")
    private String promotionTypeId;

    @ApiModelProperty(value = "活动名称")
    private String promotionName;

    @ApiModelProperty(value = "活动说明")
    private String promotionContent;

    @ApiModelProperty(value = "促销单号")
    private String gsphVoucherId;


    private String proType;

    @ApiModelProperty(value = "行号")
    private String serial;


    private String seriesId;

    @ApiModelProperty(value = "起始日期")
    private String gsphBeginDate;

    @ApiModelProperty(value = "结束日期")
    private String gsphEndDate;

    @ApiModelProperty(value = "起始时间")
    private String gsphBeginTime;

    @ApiModelProperty(value = "结束时间")
    private String gsphEndTime;

    @ApiModelProperty(value = "日期频率")
    private String gsphDateFrequency;

    @ApiModelProperty(value = "星期频率")
    private String gsphWeekFrequency;


    private String gspscInteFlag;

    private String gspscInteRate;

    @ApiModelProperty(value = "互斥条件")
    private String gsphExclusion;

    @ApiModelProperty(value = "促销折扣")
    private String discMem;

    @ApiModelProperty(value = "价格")
    private String priceMem;

    private String memberFlag;

    private List<ActiveInfo> activeInfo;

    private List<GetSalesReceiptsTableOutData> numAmtDataList;
}
