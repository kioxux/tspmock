package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "供应商往来单据查询")
public class SelectSupplierDealData {
    @ApiModelProperty(value = "client")
    private String client;

    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "商品名称")
    private String proName;
    @ApiModelProperty(value = "规格")
    private String specs;
    @ApiModelProperty(value = "单位")
    private String unit;
    @ApiModelProperty(value = "生产厂家")
    private String factoryName;
    @ApiModelProperty(value = "业务类型")
    private String matType;
    @ApiModelProperty(value = "业务名称")
    private String matName;
    @ApiModelProperty(value = "发生时间")
    private String postDate;
    @ApiModelProperty(value = "业务单据号")
    private String dnId;
    @ApiModelProperty(value = "去税金额")
    private BigDecimal batAmt;
    @ApiModelProperty(value = "税金")
    private BigDecimal rateBat;
    @ApiModelProperty(value = "税率")
    private String taxCodeValue;
    @ApiModelProperty(value = "合计金额")
    private BigDecimal totalAmount;
    @ApiModelProperty(value = "供应商编码")
    private String supCode;
    @ApiModelProperty(value = "供应商名称")
    private String supName;
    @ApiModelProperty(value = "入库地点编码")
    private String siteCode;
    @ApiModelProperty(value = "入库地点")
    private String siteName;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "门店名称")
    private String stoName;
    @ApiModelProperty(value = "数量")
    private BigDecimal qty;
    @ApiModelProperty(value = "单价")
    private BigDecimal price;
    @ApiModelProperty(value = "开单金额")
    private BigDecimal billingAmount;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    @ApiModelProperty(value = "姓名")
    private String  userName;
    @ApiModelProperty(value = "编码")
    private String  userCode;
    @ApiModelProperty(value = "订单号")
    private String  poId;
    @ApiModelProperty(value = "营业员编码")
    private String  suppNameCode;
    @ApiModelProperty(value = "营业员名称")
    private String  suppName;
    @ApiModelProperty(value = "发票金额")
    private String  batFph;
    @ApiModelProperty(value = "批次号")
    private String  batch;



    private String  matBatAmt;
    private String  matRateBat;


    private String  amtSum;

     private String  dcOdClient;

     private String  dcOdStoCode;

     private String  dcOdProCode;
     private String  dcOdBatch;
     private String  dcOdDnId;
     private String  dcOdQty;

}
