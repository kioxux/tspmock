package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ImportAcceptInData {
//    @ApiModelProperty(value = "供应商")
//    private String supCode;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "条形码")
    private String proBarCode;
    @ApiModelProperty(value = "条形码2")
    private String proBarCode2;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "通用名")
    private String proCommonName;
    @ApiModelProperty(value = "商品数量")
    private String proQty;
    @ApiModelProperty(value = "生产日期")
    private String proMadeDate;
    @ApiModelProperty(value = "批号")
    private String proBatchNo;
    @ApiModelProperty(value = "有效期至")
    private String proValiedDate;
    @ApiModelProperty(value = "零售价")
    private String prcAmount;
    @ApiModelProperty(value = "单价")
    private String proPrice;
    @ApiModelProperty(value = "单位")
    private String proUnit;
    @ApiModelProperty(value = "税率")
    private String proRate;
    @ApiModelProperty(value = "发票号")
    private String txNo;
    @ApiModelProperty(value = "折扣额")
    private String remark;
    @ApiModelProperty(value = "行金额")
    private String proLineAmt;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "生产厂家")
    private String proFactoryName;
    @ApiModelProperty(value = "税率值")
    private String gsrdInputTaxValue;
    @ApiModelProperty(value = "税率编码")
    private String gsrdInputTax;

    @ApiModelProperty(value = "状态")
    private String excelStatus;

    @ApiModelProperty(value = "禁采")
    private String noPurchase;
}
