package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.PayService;
import com.gys.business.service.SyncService;
import com.gys.business.service.UnqualifiedService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.Unqualified.UnqualifiedDto;
import com.gys.business.service.data.Unqualified.UnqualifiedInDataParam;
import com.gys.business.service.data.Unqualified.UnqualifiedSum;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.enums.MaterialTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.feign.AuthService;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UnqualifiedServiceImpl implements UnqualifiedService {
    @Resource
    private UnqualifiedMapper unqualifiedMapper;
    @Resource
    private GaiaSdIncomeStatementHMapper gaiaSdIncomeStatementHMapper;

    @Resource
    private GaiaSdIncomeStatementDMapper gaiaSdIncomeStatementDMapper;

    @Resource
    private AuthService authService;

    @Resource
    private PayService payService;

    @Resource
    private GaiaSdIncomeStatementHMapper incomeStatementHMapper;

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Autowired
    private GaiaSdBatchChangeMapper batchChangeMapper;

    @Resource
    private SyncService syncService;

    @Override
    public List<UnqualifiedOutData> unqualifiedList(UnqualifiedInData inData) {
        return unqualifiedMapper.selectUnqualifiedList(inData);
    }



    @Override
    public List<GaiaSdDefecProDInData> checkSubstandard(GaiaSdDefecProDInData vo) {
        List<GaiaSdDefecProDInData> outData = this.unqualifiedMapper.checkSubstandard(vo);
        if (CollUtil.isNotEmpty(outData)) {
            return outData;
        } else {
            throw new BusinessException("没有该商品！");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String addSubstandard(GaiaSdDefecProHInData inData) {

        //判断备注说明是否为空
        /*if (StrUtil.isEmpty(inData.getGsdhRemaks())){
            throw new BusinessException("请输入备注说明！");
        }*/

        GaiaSdDefecproH gaiaSdDefecproH = new GaiaSdDefecproH();
        gaiaSdDefecproH.setClient(inData.getClientId());
        gaiaSdDefecproH.setGsdhBrId(inData.getBrId());
        gaiaSdDefecproH.setGsdhRemarks(inData.getGsdhRemaks());//h 表备注
        gaiaSdDefecproH.setGsdhStatus("N");//审核状态 N为否，Y为是


        if(StringUtils.isEmpty(inData.getGsdhVoucherId())){ //获取单号
            String voucherId = unqualifiedMapper.selectNextVoucherId(inData.getClientId());
            gaiaSdDefecproH.setGsdhDate(CommonUtil.getyyyyMMdd());//日期
            gaiaSdDefecproH.setGsdhVoucherId(voucherId);//单号
            gaiaSdDefecproH.setGsdhEmp(inData.getGsdhEmp());// 人员
            gaiaSdDefecproH.setGsdhRemarks(inData.getGsdhRemaks());//备注
            gaiaSdDefecproH.setGsdhFlag1("N");//是否报损 n 否
            gaiaSdDefecproH.setGsdhFlag2("N");//是否销毁 n否
            gaiaSdDefecproH.setGsdhFlag3("0");//工作流状态
            int count1 = unqualifiedMapper.selectDefecProHCount(inData.getClientId(),inData.getGsdhVoucherId());
            if(count1 > 0){
                throw new BusinessException("该不合格品单已审核，请勿修改！");
            }
            this.unqualifiedMapper.insert(gaiaSdDefecproH);//添加 主表

            List<GaiaSdDefecProDInData> list = inData.getGaiaSdDefecProDInData();
            GaiaSdDefecProDInData ou = new GaiaSdDefecProDInData();

            List<GaiaSdDefecproD> gaiaSdDefecproDList = new ArrayList<>();
            //GaiaSdDefecproD gaiaSdDefecproD = new GaiaSdDefecproD();
            int count = 1;
            /*for (GaiaSdDefecProDInData dInData : dataList) {
                //String id =unqualifiedMapper.selectNextVoucherId(userInfo.getClient());
                dInData.setClientId(inData.getClientId()); //加盟商
                dInData.setBrId(inData.getBrId());//地点
                dInData.setGsddVoucherId(voucherId);// 单号
                dInData.setGsddSerial(count+"");
                if (new BigDecimal(dInData.getStockQty()).subtract(new BigDecimal(dInData.getGsddQty())).compareTo(BigDecimal.ZERO) != -1 ){
                    ou.setStockQty(new BigDecimal(dInData.getStockQty()).subtract(new BigDecimal(dInData.getGsddQty())).toString());
                }else {
                    throw new BusinessException("第"+ou.getGsddSerial()+"行，商品编码："+ou.getGsddProId()+"现有库存数量不足，请核实");
                }
                count++;
            }*/
            for (GaiaSdDefecProDInData data : list) {
                GaiaSdDefecproD gaiaSdDefecproD = new GaiaSdDefecproD();
                gaiaSdDefecproD.setClient(gaiaSdDefecproH.getClient());
                gaiaSdDefecproD.setGsddBrId(gaiaSdDefecproH.getGsdhBrId());
                gaiaSdDefecproD.setGsddDate(CommonUtil.getyyyyMMdd());
                gaiaSdDefecproD.setGsddBatch(data.getGsddBatch());
                gaiaSdDefecproD.setGsddBatchNo(data.getGsddBatchNo());
                gaiaSdDefecproD.setGsddProId(data.getGsddProId());
                gaiaSdDefecproD.setGsddQty(data.getGsddQty());
                gaiaSdDefecproD.setGsddSerial(count + "");
                gaiaSdDefecproD.setGsddValidDate(data.getGsddVaildDate());
                gaiaSdDefecproD.setGsddVoucherId(gaiaSdDefecproH.getGsdhVoucherId());
                gaiaSdDefecproD.setGsddRemaks(data.getGsddRemaks());
                gaiaSdDefecproD.setGsddEmp(gaiaSdDefecproH.getGsdhEmp());
                if (new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).compareTo(BigDecimal.ZERO) != -1 ){
                    gaiaSdDefecproD.setStockQty(new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                }else {
                    throw new BusinessException("第"+gaiaSdDefecproD.getGsddSerial()+"行，商品编码："+gaiaSdDefecproD.getGsddProId()+"现有库存数量不足，请核实");
                }
                gaiaSdDefecproDList.add(gaiaSdDefecproD);
                count++;
            }
            this.unqualifiedMapper.batchDefecProDList(gaiaSdDefecproDList);


        }else {
            int count1 = unqualifiedMapper.selectDefecProHCount(inData.getClientId(),inData.getGsdhVoucherId());
            if(count1 > 0){
                throw new BusinessException("该不合格品单已审核，请勿修改！");
            }
            gaiaSdDefecproH.setGsdhVoucherId(inData.getGsdhVoucherId());//修改 获取对象里面的单号
            List<GaiaSdDefecProDInData> list = inData.getGaiaSdDefecProDInData();//获取 d 集合

            //List<GaiaSdDefecproD> gaiaSdDefecProDInDatalist = unqualifiedMapper.selectDefecProDList(inData.getClientId(),inData.getBrId(),inData.getGsdhVoucherId());// 获取 d 表 里面的数据根据当前加盟商 门店 单号
            List<GaiaSdDefecProDInData> gaiaSdDefecproInfoList = unqualifiedMapper.selectDefecProDetail(gaiaSdDefecproH.getClient(),gaiaSdDefecproH.getGsdhBrId(),gaiaSdDefecproH.getGsdhVoucherId());
            List<GaiaSdDefecproD> gaiaSdDefecproDList = new ArrayList();
            if (ObjectUtil.isNotEmpty(gaiaSdDefecproDList) && gaiaSdDefecproDList.size()>0){//有

                for (GaiaSdDefecProDInData data : list) {

                    if (StringUtils.isNotEmpty(data.getGsddVoucherId())){//获取为空

                        /*for (GaiaSdDefecproD d: gaiaSdDefecProDInDatalist ) {

                            if (d.getClient().equals(data.getClientId()) && d.getGsddBrId().equals(data.getBrId())
                                    && d.getGsddProId().equals(data.getGsddProId()) && d.getGsddBatch().equals(data.getGsddBatch())
                                    && d.getGsddBatchNo().equals(data.getGsddBatchNo())) {
                                d.setGsddQty(data.getGsddQty());
                                d.setGsddRemaks(data.getGsddRemaks());
                                d.setStockQty(new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                            }
                        }*/


                        for (GaiaSdDefecProDInData da : gaiaSdDefecproInfoList) {

                            if (da.getClientId().equals(data.getClientId()) && da.getBrId().equals(data.getBrId())
                                    && da.getGsddProId().equals(data.getGsddProId()) && da.getGsddBatch().equals(data.getGsddBatch())
                                    && da.getGsddBatchNo().equals(data.getGsddBatchNo())) {
                                GaiaSdDefecproD d = new GaiaSdDefecproD();
                                d.setGsddQty(data.getGsddQty());
                                d.setGsddVoucherId(da.getGsddVoucherId());
                                d.setGsddProId(da.getGsddProId());
                                d.setClient(da.getClientId());
                                d.setGsddBrId(da.getBrId());
                                d.setGsddDate(da.getGsddDate());
                                d.setGsddValidDate(da.getGsddVaildDate());
                                d.setGsddRemaks(da.getGsddRemaks());
                                d.setGsddBatch(da.getGsddBatch());
                                d.setGsddBatchNo(da.getGsddBatchNo());
                                d.setGsddQty(da.getGsddQty());
                                d.setGsddEmp(gaiaSdDefecproH.getGsdhEmp());
                                d.setGsddSerial((Integer.valueOf(gaiaSdDefecproInfoList.get(gaiaSdDefecproInfoList.size()-1).getGsddSerial()) + 1) +"");
                                if (new BigDecimal(da.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).compareTo(BigDecimal.ZERO) != -1 ){
                                    d.setStockQty(new BigDecimal(da.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                                }else {
                                    throw new BusinessException("第"+d.getGsddSerial()+"行，商品编码："+d.getGsddProId()+"现有库存数量不足，请核实");
                                }
                                gaiaSdDefecproDList.add(d);
                            }
                        }
                    }else {
                        /*GaiaSdDefecproD d = new GaiaSdDefecproD();
                        d.setGsddVoucherId(gaiaSdDefecproH.getGsdhVoucherId());
                        d.setGsddProId(data.getGsddProId());
                        d.setClient(gaiaSdDefecproH.getClient());
                        d.setGsddBrId(gaiaSdDefecproH.getGsdhBrId());
                        d.setGsddDate(CommonUtil.getyyyyMMdd());
                        d.setGsddValidDate(data.getGsddVaildDate());
                        d.setGsddRemaks(data.getGsddRemaks());
                        d.setGsddBatch(data.getGsddBatch());
                        d.setGsddBatchNo(data.getGsddBatchNo());
                        d.setGsddQty(data.getGsddQty());
                        d.setGsddSerial((Integer.valueOf(gaiaSdDefecProDInDatalist.get(gaiaSdDefecProDInDatalist.size()-1).getGsddSerial()) + 1) +"");
                        d.setStockQty(new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                        gaiaSdDefecProDInDatalist.add(d);*/
                        GaiaSdDefecproD d = new GaiaSdDefecproD();
                        d.setGsddVoucherId(gaiaSdDefecproH.getGsdhVoucherId());
                        d.setGsddProId(data.getGsddProId());
                        d.setClient(gaiaSdDefecproH.getClient());
                        d.setGsddBrId(gaiaSdDefecproH.getGsdhBrId());
                        d.setGsddDate(CommonUtil.getyyyyMMdd());
                        d.setGsddValidDate(data.getGsddVaildDate());
                        d.setGsddRemaks(data.getGsddRemaks());
                        d.setGsddBatch(data.getGsddBatch());
                        d.setGsddBatchNo(data.getGsddBatchNo());
                        d.setGsddQty(data.getGsddQty());
                        d.setGsddEmp(gaiaSdDefecproH.getGsdhEmp());
                        d.setGsddSerial((Integer.valueOf(gaiaSdDefecproDList.get(gaiaSdDefecproDList.size()-1).getGsddSerial()) + 1) +"");
                        if (new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).compareTo(BigDecimal.ZERO) != -1 ){
                            d.setStockQty(new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                        }else {
                            throw new BusinessException("第"+d.getGsddSerial()+"行，商品编码："+d.getGsddProId()+"现有库存数量不足，请核实");
                        }
                        gaiaSdDefecproDList.add(d);
                    }
                }
            } else {
                int count =1;
                for (GaiaSdDefecProDInData data : list) {
                   /* GaiaSdDefecproD gaiaSdDefecproD = new GaiaSdDefecproD();
                    gaiaSdDefecproD.setClient(gaiaSdDefecproH.getClient());
                    gaiaSdDefecproD.setGsddBrId(gaiaSdDefecproH.getGsdhBrId());
                    gaiaSdDefecproD.setGsddDate(CommonUtil.getyyyyMMdd());
                    gaiaSdDefecproD.setGsddBatch(data.getGsddBatch());
                    gaiaSdDefecproD.setGsddBatchNo(data.getGsddBatchNo());
                    gaiaSdDefecproD.setGsddProId(data.getGsddProId());
                    gaiaSdDefecproD.setGsddQty(data.getGsddQty());
                    gaiaSdDefecproD.setGsddSerial(count + "");
                    gaiaSdDefecproD.setGsddValidDate(data.getGsddVaildDate());
                    gaiaSdDefecproD.setGsddVoucherId(gaiaSdDefecproH.getGsdhVoucherId());
                    gaiaSdDefecproD.setGsddRemaks(data.getGsddRemaks());
                    gaiaSdDefecproD.setStockQty(new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                    gaiaSdDefecProDInDatalist.add(gaiaSdDefecproD);
                    count++;*/
                    GaiaSdDefecproD gaiaSdDefecproD = new GaiaSdDefecproD();
                    gaiaSdDefecproD.setClient(gaiaSdDefecproH.getClient());
                    gaiaSdDefecproD.setGsddBrId(gaiaSdDefecproH.getGsdhBrId());
                    gaiaSdDefecproD.setGsddDate(CommonUtil.getyyyyMMdd());
                    gaiaSdDefecproD.setGsddBatch(data.getGsddBatch());
                    gaiaSdDefecproD.setGsddBatchNo(data.getGsddBatchNo());
                    gaiaSdDefecproD.setGsddProId(data.getGsddProId());
                    gaiaSdDefecproD.setGsddQty(data.getGsddQty());
                    gaiaSdDefecproD.setGsddSerial(count + "");
                    gaiaSdDefecproD.setGsddValidDate(data.getGsddVaildDate());
                    gaiaSdDefecproD.setGsddVoucherId(gaiaSdDefecproH.getGsdhVoucherId());
                    gaiaSdDefecproD.setGsddRemaks(data.getGsddRemaks());
                    gaiaSdDefecproD.setGsddEmp(gaiaSdDefecproH.getGsdhEmp());
                    if (new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).compareTo(BigDecimal.ZERO) != -1 ){
                        gaiaSdDefecproD.setStockQty(new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                    }else {
                        throw new BusinessException("第"+gaiaSdDefecproD.getGsddSerial()+"行，商品编码："+gaiaSdDefecproD.getGsddProId()+"现有库存数量不足，请核实");
                    }
                    gaiaSdDefecproDList.add(gaiaSdDefecproD);
                    count++;

                }
                //主表修改
                Example example = new Example(GaiaSdDefecproH.class);
                example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("gsdhBrId", inData.getBrId()).andEqualTo("gsdhVoucherId", inData.getGsdhVoucherId());
                unqualifiedMapper.updateByExampleSelective(gaiaSdDefecproH,example);
            }
            unqualifiedMapper.batchDefecProDList(gaiaSdDefecproDList);
        }
        /**
         * 2021/2/26
         * 验收时调用接口增减库存
         */
//        log.info("库存新增/修改:{}", JSON.toJSONString(list));
        List<String> params = new ArrayList<>();
        params.add(gaiaSdDefecproH.getGsdhVoucherId());
        StockSyncData stockSyncData=new StockSyncData();
        stockSyncData.setClient(inData.getClientId());
        stockSyncData.setSite(inData.getBrId());
        stockSyncData.setType("SDP");
        stockSyncData.setParams(params);
        this.syncService.stockSync(stockSyncData);
        log.info("库存新增/修改:{}", JSON.toJSONString(stockSyncData));
        return gaiaSdDefecproH.getGsdhVoucherId();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void approve(GaiaSdDefecProHInData inData) {
        if (ObjectUtil.isEmpty(inData.getGaiaSdDefecProDInData())
                && inData.getGaiaSdDefecProDInData().size() <= 0){
            throw new BusinessException("未添加不合格品，无法审核");
        }
        GaiaSdDefecproH gaiaSdDefecproH = new GaiaSdDefecproH();
        gaiaSdDefecproH.setClient(inData.getClientId());
        gaiaSdDefecproH.setGsdhBrId(inData.getBrId());
        gaiaSdDefecproH.setGsdhRemarks(inData.getGsdhRemaks());
        gaiaSdDefecproH.setGsdhStatus("Y");
        if(StringUtils.isEmpty(inData.getGsdhVoucherId())){
            String voucherId = unqualifiedMapper.selectNextVoucherId(inData.getClientId());
            gaiaSdDefecproH.setGsdhDate(CommonUtil.getyyyyMMdd());
            gaiaSdDefecproH.setGsdhVoucherId(voucherId);
            gaiaSdDefecproH.setGsdhEmp(inData.getGsdhEmp());
            gaiaSdDefecproH.setGsdhFlag1("N");
            gaiaSdDefecproH.setGsdhFlag2("N");
            gaiaSdDefecproH.setGsdhFlag3("1");
            this.saveProDetail(gaiaSdDefecproH,inData.getGaiaSdDefecProDInData(),inData.getToken());
            unqualifiedMapper.insert(gaiaSdDefecproH);
        }else {
            gaiaSdDefecproH.setGsdhVoucherId(inData.getGsdhVoucherId());
            gaiaSdDefecproH.setGsdhEmp(inData.getGsdhEmp());
            Example example = new Example(GaiaSdDefecproH.class);
            example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("gsdhBrId", inData.getBrId()).andEqualTo("gsdhVoucherId", inData.getGsdhVoucherId());
            this.saveProDetail(gaiaSdDefecproH,inData.getGaiaSdDefecProDInData(),inData.getToken());
            unqualifiedMapper.updateByExampleSelective(gaiaSdDefecproH,example);
        }
    }

    @Override
    public void removeDefecProD(GaiaSdDefecProDInData inData) {
        int count = unqualifiedMapper.selectDefecProHCount(inData.getClientId(),inData.getGsddVoucherId());
        if(count > 0){
            throw new BusinessException("该不合格品单已审核，不允许删除商品！");
        }
        unqualifiedMapper.removeDefecProD(inData);
    }

    private void saveProDetail(GaiaSdDefecproH gaiaSdDefecproH,List<GaiaSdDefecProDInData> list,String token){
        int count1 = unqualifiedMapper.selectDefecProHCount(gaiaSdDefecproH.getClient(),gaiaSdDefecproH.getGsdhVoucherId());
        if(count1 > 0){
            throw new BusinessException("该不合格品单已审核，请勿重复操作！");
        }
        GetWfCreateInData wfInData = new GetWfCreateInData();
        wfInData.setToken(token);
        wfInData.setWfTitle("不合格品转入");
        wfInData.setWfDescription("不合格品扣减库存，发送工作流任务审批");
        wfInData.setWfOrder(gaiaSdDefecproH.getGsdhVoucherId());
        wfInData.setWfSite(gaiaSdDefecproH.getGsdhBrId());
        wfInData.setWfDefineCode("GAIA_WF_046");
        List<GetWfCreateLossDefecproInData> wfDetailList = Lists.newArrayList();
        List<GaiaSdDefecProDInData> gaiaSdDefecproInfoList = unqualifiedMapper.selectDefecProDetail(gaiaSdDefecproH.getClient(),gaiaSdDefecproH.getGsdhBrId(),gaiaSdDefecproH.getGsdhVoucherId());
        List<GaiaSdDefecproD> gaiaSdDefecproDList = new ArrayList();
        if (ObjectUtil.isNotEmpty(gaiaSdDefecproDList) && gaiaSdDefecproDList.size() > 0){
            for (GaiaSdDefecProDInData data : list) {
                if (StringUtils.isNotEmpty(data.getGsddVoucherId())) {
                    for (GaiaSdDefecProDInData da : gaiaSdDefecproInfoList) {
                        if (da.getClientId().equals(data.getClientId()) && da.getBrId().equals(data.getBrId())
                                && da.getGsddProId().equals(data.getGsddProId()) && da.getGsddBatch().equals(data.getGsddBatch())
                                && da.getGsddBatchNo().equals(data.getGsddBatchNo())) {
                            GaiaSdDefecproD d = new GaiaSdDefecproD();
                            d.setGsddQty(data.getGsddQty());
                            d.setGsddVoucherId(da.getGsddVoucherId());
                            d.setGsddProId(da.getGsddProId());
                            d.setClient(da.getClientId());
                            d.setGsddBrId(da.getBrId());
                            d.setGsddDate(da.getGsddDate());
                            d.setGsddValidDate(da.getGsddVaildDate());
                            d.setGsddRemaks(da.getGsddRemaks());
                            d.setGsddBatch(da.getGsddBatch());
                            d.setGsddBatchNo(da.getGsddBatchNo());
                            d.setGsddQty(da.getGsddQty());
                            d.setGsddEmp(gaiaSdDefecproH.getGsdhEmp());
                            d.setGsddSerial((Integer.valueOf(gaiaSdDefecproDList.get(gaiaSdDefecproDList.size()-1).getGsddSerial()) + 1) +"");
                            if (new BigDecimal(da.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).compareTo(BigDecimal.ZERO) != -1 ){
                                d.setStockQty(new BigDecimal(da.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                            }else {
                                throw new BusinessException("第"+d.getGsddSerial()+"行，商品编码："+d.getGsddProId()+"现有库存数量不足，请核实");
                            }
                            gaiaSdDefecproDList.add(d);
                        }
                    }
                }else {
                    GaiaSdDefecproD d = new GaiaSdDefecproD();
                    d.setGsddVoucherId(gaiaSdDefecproH.getGsdhVoucherId());
                    d.setGsddProId(data.getGsddProId());
                    d.setClient(gaiaSdDefecproH.getClient());
                    d.setGsddBrId(gaiaSdDefecproH.getGsdhBrId());
                    d.setGsddDate(CommonUtil.getyyyyMMdd());
                    d.setGsddValidDate(data.getGsddVaildDate());
                    d.setGsddRemaks(data.getGsddRemaks());
                    d.setGsddBatch(data.getGsddBatch());
                    d.setGsddBatchNo(data.getGsddBatchNo());
                    d.setGsddQty(data.getGsddQty());
                    d.setGsddEmp(gaiaSdDefecproH.getGsdhEmp());
                    d.setGsddSerial((Integer.valueOf(gaiaSdDefecproDList.get(gaiaSdDefecproDList.size()-1).getGsddSerial()) + 1) +"");
                    if (new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).compareTo(BigDecimal.ZERO) != -1 ){
                        d.setStockQty(new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                    }else {
                        throw new BusinessException("第"+d.getGsddSerial()+"行，商品编码："+d.getGsddProId()+"现有库存数量不足，请核实");
                    }
                    gaiaSdDefecproDList.add(d);
                }
            }
        }else {
            int count = 1;
            for (GaiaSdDefecProDInData data : list) {
                GaiaSdDefecproD gaiaSdDefecproD = new GaiaSdDefecproD();
                gaiaSdDefecproD.setClient(gaiaSdDefecproH.getClient());
                gaiaSdDefecproD.setGsddBrId(gaiaSdDefecproH.getGsdhBrId());
                gaiaSdDefecproD.setGsddDate(CommonUtil.getyyyyMMdd());
                gaiaSdDefecproD.setGsddBatch(data.getGsddBatch());
                gaiaSdDefecproD.setGsddBatchNo(data.getGsddBatchNo());
                gaiaSdDefecproD.setGsddProId(data.getGsddProId());
                gaiaSdDefecproD.setGsddQty(data.getGsddQty());
                gaiaSdDefecproD.setGsddSerial(count + "");
                gaiaSdDefecproD.setGsddValidDate(data.getGsddVaildDate());
                gaiaSdDefecproD.setGsddVoucherId(gaiaSdDefecproH.getGsdhVoucherId());
                gaiaSdDefecproD.setGsddRemaks(data.getGsddRemaks());
                gaiaSdDefecproD.setGsddEmp(gaiaSdDefecproH.getGsdhEmp());
                if (new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).compareTo(BigDecimal.ZERO) != -1 ){
                    gaiaSdDefecproD.setStockQty(new BigDecimal(data.getStockQty()).subtract(new BigDecimal(data.getGsddQty())).toString());
                }else {
                    throw new BusinessException("第"+gaiaSdDefecproD.getGsddSerial()+"行，商品编码："+gaiaSdDefecproD.getGsddProId()+"现有库存数量不足，请核实");
                }
                gaiaSdDefecproDList.add(gaiaSdDefecproD);
                count++;
            }
        }
        if (ObjectUtil.isNotEmpty(gaiaSdDefecproDList) && gaiaSdDefecproDList.size() > 0){
            GaiaStoreData storeData = gaiaStoreDataMapper.findByClientAndStoCode(gaiaSdDefecproH.getClient(),gaiaSdDefecproH.getGsdhBrId());
            for (GaiaSdDefecProDInData da : list) {
                for (GaiaSdDefecproD gaiaSdDefecproD:gaiaSdDefecproDList) {
                    if (gaiaSdDefecproD.getGsddProId().equals(da.getGsddProId())
                            && gaiaSdDefecproD.getGsddBrId().equals(da.getBrId())
                            && gaiaSdDefecproD.getGsddBatch().equals(da.getGsddBatch())
                            && gaiaSdDefecproD.getGsddBatchNo().equals(da.getGsddBatchNo())){
                        GetWfCreateLossDefecproInData lossDefecproInData = new GetWfCreateLossDefecproInData();
                        lossDefecproInData.setIndex(Integer.valueOf(gaiaSdDefecproD.getGsddSerial()));
                        lossDefecproInData.setValidDate(da.getGsddVaildDate());
                        lossDefecproInData.setLossDate(gaiaSdDefecproD.getGsddDate());
                        lossDefecproInData.setLossOrder(da.getGsddVoucherId());
                        lossDefecproInData.setStoreId(da.getBrId());
                        lossDefecproInData.setProCode(da.getGsddProId());
                        lossDefecproInData.setBatch(da.getGsddBatch());
                        lossDefecproInData.setBatchNo(da.getGsddBatchNo());
                        lossDefecproInData.setQty(da.getGsddQty());
                        lossDefecproInData.setReason(da.getGsddRemaks());
                        lossDefecproInData.setFactoryName(da.getGsddFactoryName());
                        lossDefecproInData.setProName(da.getGsddProName());
                        lossDefecproInData.setProCommonName(da.getGsddProCommonName());
                        lossDefecproInData.setProSpecs(da.getGsddProSpecs());
                        lossDefecproInData.setProUnit(da.getGsddProUnit());
                        lossDefecproInData.setStoreName(storeData.getStoShortName());
                        wfDetailList.add(lossDefecproInData);
                    }
                }
            }
        }
        //新增不合格品明细
        unqualifiedMapper.batchDefecProDList(gaiaSdDefecproDList);
        //扣减批次库存
        unqualifiedMapper.batchStockBatch(gaiaSdDefecproDList);
        //商品编码相同商品汇总处理
        List<GaiaSdDefecproD> gaiaSdDefecproDTotalList = new ArrayList();
        List<GaiaSdBatchChange> changeList = new ArrayList<>();
        int icount = 1;
        for (GaiaSdDefecproD gaiaSdDefecproD : gaiaSdDefecproDList) {
            //减库存为负数
            gaiaSdDefecproD.setGsddQty(new BigDecimal(gaiaSdDefecproD.getGsddQty()).multiply(new BigDecimal(-1)).toString());
            if(ObjectUtil.isEmpty(gaiaSdDefecproDTotalList)){
                gaiaSdDefecproDTotalList.add(gaiaSdDefecproD);
            }else {
                boolean flag = true;
                for (GaiaSdDefecproD a : gaiaSdDefecproDTotalList) {
                    if (gaiaSdDefecproD.getGsddProId().equals(a.getGsddProId())
                            && gaiaSdDefecproD.getGsddBrId().equals(a.getGsddBrId())){
                        a.setGsddQty(new BigDecimal(a.getGsddQty()).add(new BigDecimal(gaiaSdDefecproD.getGsddQty())).toString());
                        flag = false;
                    }
                }
                if (flag){
                    gaiaSdDefecproDTotalList.add(gaiaSdDefecproD);
                }
            }
            GaiaSdBatchChange change = new GaiaSdBatchChange();
            change.setClient(gaiaSdDefecproD.getClient());
            change.setGsbcBatch(gaiaSdDefecproD.getGsddBatch());
            change.setGsbcBatchNo(gaiaSdDefecproD.getGsddBatchNo());
            change.setGsbcProId(gaiaSdDefecproD.getGsddProId());
            change.setGsbcDate(CommonUtil.getyyyyMMdd());
            change.setGsbcBrId(gaiaSdDefecproD.getGsddBrId());
            change.setGsbcQty(new BigDecimal(gaiaSdDefecproD.getGsddQty()));
            change.setGsbcVoucherId(gaiaSdDefecproD.getGsddVoucherId());
            change.setGsbcSerial(icount+"");
            changeList.add(change);
            icount ++;
        }
        //扣减门店库存
        unqualifiedMapper.batchStock(gaiaSdDefecproDTotalList);
        //添加批次异动记录
        if(ObjectUtil.isNotEmpty(changeList) && changeList.size() > 0){
            batchChangeMapper.insertLists(changeList);
        }
        //添加工作流任务
        wfInData.setWfDetail(wfDetailList);
        String result = authService.createWorkflow(wfInData);
        log.info("请求创建工作流：{}", JSON.toJSONString(wfInData));
        JSONObject jsonObject = JSONObject.parseObject(result);
        log.info("工作流回调：{}", JSON.toJSONString(result));
        if (jsonObject.containsKey("code") && !"0".equals(jsonObject.getString("code"))) {
            throw new BusinessException(jsonObject.getString("message"));
        }
        /**
         * 2021/2/26
         * 验收时调用接口增减库存
         */
//        log.info("库存新增/修改:{}", JSON.toJSONString(list));
        List<String> params = new ArrayList<>();
        params.add(gaiaSdDefecproH.getGsdhVoucherId());
        StockSyncData stockSyncData=new StockSyncData();
        stockSyncData.setClient(gaiaSdDefecproH.getClient());
        stockSyncData.setSite(gaiaSdDefecproH.getGsdhBrId());
        stockSyncData.setType("SDP");
        stockSyncData.setParams(params);
        this.syncService.stockSync(stockSyncData);
        log.info("库存新增/修改:{}", JSON.toJSONString(stockSyncData));
    }
    @Override
    public UnqualifiedOutData listUnqualified(GetLoginOutData userInfo, UnqualifiedInDataParam inData) {
        List<UnqualifiedDto> list = unqualifiedMapper.listUnqualified(inData);
        UnqualifiedOutData outData = new UnqualifiedOutData();
        outData.setList(list);
        UnqualifiedSum sum = new UnqualifiedSum();
        if (CollectionUtil.isNotEmpty(list)) {
            sum.setProId(new BigDecimal(String.valueOf(list.size())));
            BigDecimal qty = list.stream().map(UnqualifiedDto::getQty).reduce(BigDecimal.ZERO, BigDecimal::add);
            sum.setQty(qty);
        }
        outData.setListNum(sum);
        return outData;
    }

    @Override
    public List<UnqualifiedDto> listUnqualifiedHead(GetLoginOutData userInfo, UnqualifiedInDataParam inData) {
        return unqualifiedMapper.listUnqualifiedHead(inData);
    }

    @Override
    public List<UnqualifiedDto> listUnqualifiedDetailByBillNo(GetLoginOutData userInfo, UnqualifiedInDataParam inData) {
        checkUnqualified(inData, "该单据状态已变化，请刷新页面！");
        return unqualifiedMapper.listUnqualifiedDetailByBillNo(inData);
    }

    @Override
    @Transactional
    public Boolean commitUnqualifiedBill(GetLoginOutData userInfo, UnqualifiedInDataParam inData) {
        List<UnqualifiedDto> unqualifiedDtos = unqualifiedMapper.listUnqualifiedHead(inData);
        if (CollectionUtil.isEmpty(unqualifiedDtos) && unqualifiedDtos.size() != 1) {
            throw new BusinessException("该单据状态已变化，请刷新页面！");
        }
        UnqualifiedDto unqualifiedDto = unqualifiedDtos.get(0);
        //1.0插入 损益主表 GAIA_SD_INCOME_STATEMENT_H
        GaiaSdIncomeStatementH statementH = new GaiaSdIncomeStatementH();
        statementH.setClientId(unqualifiedDto.getClientId());
        String billNo = unqualifiedMapper.selectNextSYDPVoucherId(inData.getClientId());
        statementH.setGsishVoucherId(billNo);
        statementH.setGsishBrId(unqualifiedDto.getStoCode());
        statementH.setGsishDate(CommonUtil.getyyyyMMdd());
        statementH.setGsishIsType("5");
        statementH.setGsishTableType("损溢单");
        BigDecimal sumQty = BigDecimal.ZERO;
        BigDecimal sumAmt = BigDecimal.ZERO;
        statementH.setGsishStatus("1");
        statementH.setGsishPcVoucherId(unqualifiedDto.getBillNo());

        List<UnqualifiedDto> detailList = unqualifiedMapper.listUnqualifiedDetailByBillNo(inData);
        if (CollectionUtil.isEmpty(detailList)) {
            throw new BusinessException("该单据没有明细，无法提交！");
        }
        List<GaiaSdIncomeStatementD> dList = new ArrayList<>();
        //2.0插入 损益明细表 GAIA_SD_INCOME_STATEMENT_D
        for (UnqualifiedDto detail : detailList) {
            GaiaSdIncomeStatementD statementD = new GaiaSdIncomeStatementD();
            statementD.setClient(statementH.getClientId());
            statementD.setGsisdVoucherId(statementH.getGsishVoucherId());
            statementD.setGsisdBrId(statementH.getGsishBrId());
            statementD.setGsisdDate(statementH.getGsishDate());
            statementD.setGsisdSerial(detail.getSerial());
            statementD.setGsisdProId(detail.getProId());
            statementD.setGsisdBatch(detail.getBatch());
            statementD.setGsisdBatchNo(detail.getBatchNo());
            statementD.setGsisdStockQty(detail.getStockQty().add(detail.getQty()));
            statementD.setGsisdRealQty(detail.getStockQty());
            statementD.setGsisdIsCause("不合格品报损");
            statementD.setGsisdIsQty(detail.getQty());
            statementD.setGsisdStockStatus("1000");
            statementD.setGsisdValidUntil(detail.getValidDate());
            sumQty = sumQty.add(detail.getQty());
            dList.add(statementD);
        }
        statementH.setGsishTotalQty(sumQty.toString());
        statementH.setGsishTotalAmt(sumAmt);

        List<GaiaSdIncomeStatementD> sortList = dList.stream().sorted(Comparator.comparing(GaiaSdIncomeStatementD::getGsisdSerial)).collect(Collectors.toList());
        gaiaSdIncomeStatementHMapper.insert(statementH);
        gaiaSdIncomeStatementDMapper.batchInsert(sortList);
        //update GsdhFlag1-》"O"
        GaiaSdDefecproH defecproH = new GaiaSdDefecproH();
        defecproH.setGsdhFlag1("O");
        defecproH.setClient(inData.getClientId());
        defecproH.setGsdhVoucherId(inData.getBillNo());
        unqualifiedMapper.updateGsdhFlag1(defecproH);

        //3.0 发起工作流
        GetWfCreateInData wfInData = new GetWfCreateInData();
        wfInData.setToken(userInfo.getToken());
        wfInData.setWfTitle("不合格品报损");
        wfInData.setWfDescription("不合格品报损");
        wfInData.setWfOrder(statementH.getGsishVoucherId());
        wfInData.setWfSite(statementH.getGsishBrId());

        wfInData.setWfDefineCode("GAIA_WF_047");
        wfInData.setWfDetail(detailList);
        String result = authService.createWorkflow(wfInData);
        log.info("不合格品损益单：请求创建工作流：{}", JSON.toJSONString(wfInData));
        JSONObject jsonObject = JSONObject.parseObject(result);
        log.info("不合格品损益单：工作流回调：{}", JSON.toJSONString(result));
        if (jsonObject.containsKey("code") && !"0".equals(jsonObject.getString("code"))) {
            throw new BusinessException(jsonObject.getString("message"));
        }
        return true;
    }

    @Override
    @Transactional
    public Boolean checkTodoList(UnqualifiedInDataParam inData) {
        inData.setBillNo(inData.getWfOrder());
        Example example = new Example(GaiaSdIncomeStatementH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsishVoucherId", inData.getBillNo());
        GaiaSdIncomeStatementH incomeStatement = incomeStatementHMapper.selectOneByExample(example);
        if (ObjectUtil.isNull(incomeStatement)) {
            throw new BusinessException("损溢单不存在");
        }
        //通过
//        if ("2".equals(inData.getWfStatus())) {
        //update GsdhFlag1-》"Y"
        GaiaSdDefecproH defecproH = new GaiaSdDefecproH();
        defecproH.setGsdhFlag1("Y");
        defecproH.setClient(inData.getClientId());
        defecproH.setGsdhVoucherId(incomeStatement.getGsishPcVoucherId());
        unqualifiedMapper.updateGsdhFlag1(defecproH);
        //调用物料凭证
        List<MaterialDocRequestDto> materialList = Lists.newArrayList();
        UnqualifiedInDataParam param = new UnqualifiedInDataParam();
        param.setBillNo(incomeStatement.getGsishPcVoucherId());
        param.setClientId(inData.getClientId());
        param.setStoCode(inData.getStoCode());
        List<UnqualifiedDto> detailList = unqualifiedMapper.listUnqualifiedDetailByBillNo(param);
        for (int i = 0; i < detailList.size(); i++) {
            MaterialDocRequestDto dto = new MaterialDocRequestDto();
            dto.setClient(inData.getClientId());
            dto.setGuid(UUID.randomUUID().toString());
            dto.setGuidNo(StrUtil.toString(i + 1));
            dto.setMatType(MaterialTypeEnum.BAO_SUN.getCode());
            dto.setMatPostDate(CommonUtil.getyyyyMMdd());
            dto.setMatHeadRemark("");
            dto.setMatProCode(detailList.get(i).getProId());
            dto.setMatSiteCode(detailList.get(i).getStoCode());
            dto.setMatLocationCode("1000");
            dto.setMatLocationTo("");
            dto.setMatBatch(detailList.get(i).getBatch());
            dto.setMatQty(detailList.get(i).getQty());
            dto.setMatPrice(BigDecimal.ZERO);
            dto.setMatUnit("盒");
            dto.setMatDebitCredit("H");
            dto.setMatPoId(detailList.get(i).getBillNo());
            dto.setMatPoLineno(detailList.get(i).getSerial());
            dto.setMatDnId(detailList.get(i).getBillNo());
            dto.setMatDnLineno(detailList.get(i).getSerial());
            dto.setMatLineRemark("");
            dto.setMatCreateBy(inData.getApproverUserId());
            dto.setMatCreateDate(CommonUtil.getyyyyMMdd());
            dto.setMatCreateTime(CommonUtil.getHHmmss());
            materialList.add(dto);
        }
        payService.insertMaterialDoc(materialList);
        //update  损益主表
        incomeStatement.setGsishStatus("2");
        /*} else if ("1".equals(inData.getWfStatus())) {
        //驳回
            incomeStatement.setGsishStatus(inData.getWfStatus());
        }*/
        incomeStatement.setGsishExamineDate(CommonUtil.getyyyyMMdd());
        incomeStatement.setGsishExamineEmp(inData.getApproverUserId());
        incomeStatementHMapper.updateByPrimaryKeySelective(incomeStatement);
        return true;
    }

    @Override
    public GaiaSdDefecProHInData selectDefecProDetail(GaiaSdDefecProDInData inData) {
        GaiaSdDefecProHInData gaiaSdDefecProH = unqualifiedMapper.selectDefecProH(inData.getClientId(),inData.getBrId(),inData.getGsddVoucherId());
        List<GaiaSdDefecProDInData> itemList = unqualifiedMapper.selectDefecProDetail(inData.getClientId(),inData.getBrId(),inData.getGsddVoucherId());
        gaiaSdDefecProH.setGaiaSdDefecProDInData(itemList);
        return gaiaSdDefecProH;
    }

    @Override
    public void sendLossDefecpro(GetWfApproveInData inData) {
        if("3".equals(inData.getWfStatus())){
            inData.setWfStatus("2");
        }else if ("1".equals(inData.getWfStatus())){
            inData.setWfStatus("3");
        }
        this.unqualifiedMapper.updateLossStatus(inData.getClientId(),inData.getWfStatus(),inData.getWfOrder());
        if("3".equals(inData.getWfStatus())){
            List<GaiaSdDefecProDInData> unqualifiedDetailList = unqualifiedMapper.selectDefecProDetail(inData.getClientId(),"",inData.getWfOrder());
            List<GaiaSdDefecproD> list = new ArrayList<>();
            for (GaiaSdDefecProDInData detail : unqualifiedDetailList) {
                GaiaSdDefecproD gaiaSdDefecproD = new GaiaSdDefecproD();
                inData.setSite(detail.getBrId());
                gaiaSdDefecproD.setClient(detail.getClientId());
                gaiaSdDefecproD.setGsddBatch(detail.getGsddBatch());
                gaiaSdDefecproD.setGsddVoucherId(inData.getWfOrder());
                gaiaSdDefecproD.setGsddBatchNo(detail.getGsddBatchNo());
                gaiaSdDefecproD.setGsddEmp(inData.getCreateUser());
                gaiaSdDefecproD.setStockQty(new BigDecimal(detail.getGsddQty()).add(new BigDecimal(detail.getStockQty())).toString());
                gaiaSdDefecproD.setGsddBrId(detail.getBrId());
                gaiaSdDefecproD.setGsddProId(detail.getGsddProId());
                gaiaSdDefecproD.setGsddQty(detail.getGsddQty());
                gaiaSdDefecproD.setGsddEmp(inData.getApproverUserId());
                list.add(gaiaSdDefecproD);
            }
            if (ObjectUtil.isNotEmpty(list) && list.size() > 0){
                this.unqualifiedMapper.batchStockBatch(list);
                //商品编码相同商品汇总处理
                List<GaiaSdDefecproD> gaiaSdDefecproDTotalList = new ArrayList();
                for (GaiaSdDefecproD gaiaSdDefecproD : list) {
                    if(ObjectUtil.isEmpty(gaiaSdDefecproDTotalList)){
                        gaiaSdDefecproDTotalList.add(gaiaSdDefecproD);
                    }else {
                        boolean flag = true;
                        for (GaiaSdDefecproD a : gaiaSdDefecproDTotalList) {
                            if (gaiaSdDefecproD.getGsddProId().equals(a.getGsddProId())
                                    && gaiaSdDefecproD.getGsddBrId().equals(a.getGsddBrId())){
                                a.setGsddQty(new BigDecimal(a.getGsddQty()).add(new BigDecimal(gaiaSdDefecproD.getGsddQty())).toString());
                                flag = false;
                            }
                        }
                        if (flag){
                            gaiaSdDefecproDTotalList.add(gaiaSdDefecproD);
                        }
                    }
                }
                //扣减门店库存
                unqualifiedMapper.batchStock(gaiaSdDefecproDTotalList);
                List<GaiaSdBatchChange> changeList = new ArrayList<>();
                int icount = 1;
                for (GaiaSdDefecproD gaiaSdDefecproD : gaiaSdDefecproDTotalList) {
                    GaiaSdBatchChange change = new GaiaSdBatchChange();
                    change.setClient(gaiaSdDefecproD.getClient());
                    change.setGsbcBatch(gaiaSdDefecproD.getGsddBatch());
                    change.setGsbcBatchNo(gaiaSdDefecproD.getGsddBatchNo());
                    change.setGsbcProId(gaiaSdDefecproD.getGsddProId());
                    change.setGsbcDate(CommonUtil.getyyyyMMdd());
                    change.setGsbcBrId(gaiaSdDefecproD.getGsddBrId());
                    change.setGsbcQty(new BigDecimal(gaiaSdDefecproD.getGsddQty()));
                    change.setGsbcVoucherId(gaiaSdDefecproD.getGsddVoucherId());
                    change.setGsbcSerial(icount+"");
                    changeList.add(change);
                }
                //添加批次异动记录
                if(ObjectUtil.isNotEmpty(changeList) && changeList.size() > 0){
                    for (GaiaSdBatchChange d : changeList) {
                        d.setGsbcVoucherId("BH"+ d.getGsbcVoucherId());
                    }
                    batchChangeMapper.insertLists(changeList);
                }
            }
            /**
             * 2021/2/26
             * 验收时调用接口增减库存
             */
//        log.info("库存新增/修改:{}", JSON.toJSONString(list));
            List<String> params = new ArrayList<>();
            params.add(inData.getWfOrder());
            StockSyncData stockSyncData=new StockSyncData();
            stockSyncData.setClient(inData.getClientId());
            stockSyncData.setSite(inData.getSite());
            stockSyncData.setType("SDP");
            stockSyncData.setParams(params);
            this.syncService.stockSync(stockSyncData);
            log.info("库存新增/修改:{}", JSON.toJSONString(stockSyncData));
        }
    }
    private void checkUnqualified(UnqualifiedInDataParam inData, String s) {
        List<UnqualifiedDto> headList = unqualifiedMapper.listUnqualifiedHead(inData);
        if (CollectionUtil.isEmpty(headList)) {
            throw new BusinessException(s);
        }
    }
}
