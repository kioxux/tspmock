package com.gys.business.service.impl;

import com.alibaba.excel.util.DateUtils;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.DistributionPlanDetailService;
import com.gys.business.service.data.DistributionPlanDetailVO;
import com.gys.common.enums.StoreAttributeEnum;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 14:54
 */
@Service("distributionPlanDetailService")
public class DistributionPlanDetailServiceImpl implements DistributionPlanDetailService {
    @Resource
    private DistributionPlanDetailMapper distributionPlanDetailMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private NewDistributionPlanMapper newDistributionPlanMapper;
    @Resource
    private GaiaSdStoresGroupMapper gaiaSdStoresGroupMapper;

    @Override
    public DistributionPlanDetail getById(Long id) {
        return distributionPlanDetailMapper.getById(id);
    }

    @Transactional
    @Override
    public DistributionPlanDetail add(DistributionPlanDetail distributionPlanDetail) {
        distributionPlanDetailMapper.add(distributionPlanDetail);
        return getById(distributionPlanDetail.getId());
    }

    @Transactional
    @Override
    public DistributionPlanDetail update(DistributionPlanDetail distributionPlanDetail) {
        int num;
        try {
            distributionPlanDetail.setUpdateTime(new Date());
            num = distributionPlanDetailMapper.update(distributionPlanDetail);
        } catch (Exception e) {
            throw new BusinessException("铺货计划详情更新异常", e);
        }
        if (num == 0) {
            throw new BusinessException("铺货计划详情更新失败");
        }
        return getById(distributionPlanDetail.getId());
    }

    @Override
    public List<DistributionPlanDetail> findList(DistributionPlanDetail cond) {
        return distributionPlanDetailMapper.findList(cond);
    }

    @Override
    public DistributionCountInfo getPlanCountInfo(NewDistributionPlan distributionPlan) {
        return distributionPlanDetailMapper.getPlanCountInfo(distributionPlan);
    }

    @Override
    public List<DistributionPlanDetailVO> getPlanDetail(DistributionPlanDetail cond) {
        List<DistributionPlanDetail> detailList = findList(cond);
        NewDistributionPlan planCond = new NewDistributionPlan();
        planCond.setPlanCode(cond.getPlanCode());
        planCond.setClient(cond.getClient());
        NewDistributionPlan distributionPlan = newDistributionPlanMapper.getUnique(planCond);
        return transformPlanDetailVO(distributionPlan, detailList);
    }

    private List<DistributionPlanDetailVO> transformPlanDetailVO(NewDistributionPlan distributionPlan, List<DistributionPlanDetail> detailList) {
        List<DistributionPlanDetailVO> detailVOList = new ArrayList<>();

        GaiaProductBusiness proCond = new GaiaProductBusiness();
        proCond.setClient(detailList.get(0).getClient());
        proCond.setProSelfCode(detailList.get(0).getProSelfCode());
        List<GaiaProductBusiness> productList = gaiaProductBusinessMapper.findList(proCond);
        GaiaProductBusiness productBusiness = productList.get(0);
        int index = 1;
        for (DistributionPlanDetail planDetail : detailList) {
            DistributionPlanDetailVO detailVO = new DistributionPlanDetailVO();
            BeanUtils.copyProperties(planDetail, detailVO);
            detailVO.setIndex(index++);
            detailVO.setPlanTime(DateUtils.format(distributionPlan.getCreateTime(),"yyyy/MM/dd HH:mm:ss"));
            String finishTime = DateUtils.format(distributionPlan.getFinishDate(), "yyyy/MM/dd HH:mm:ss");
            if (!finishTime.startsWith("1970")) {
                detailVO.setFinishTime(finishTime);
            }
            //商品信息
            detailVO.setProSelfName(productBusiness.getProCommonname());
            detailVO.setProFactoryName(productBusiness.getProFactoryName());
            detailVO.setProSpec(productBusiness.getProSpecs());
            detailVO.setProUnit(productBusiness.getProUnit());
            //门店信息
            StoreOutData cond = new StoreOutData();
            cond.setClient(planDetail.getClient());
            cond.setStoCode(planDetail.getStoreId());
            StoreOutData storeData = gaiaStoreDataMapper.getUnique(cond);
            if (storeData != null) {
                detailVO.setStoreProperty(StoreAttributeEnum.getName(storeData.getStoAttribute()));
                //填充店型
                Map<String, String> params = new HashMap<>();
                params.put("client", detailVO.getClient());
                params.put("storeId", detailVO.getStoreId());
                detailVO.setStoreType(gaiaSdStoresGroupMapper.getStoreTypeName(params));
            }
            detailVOList.add(detailVO);
        }
        return detailVOList;
    }

}
