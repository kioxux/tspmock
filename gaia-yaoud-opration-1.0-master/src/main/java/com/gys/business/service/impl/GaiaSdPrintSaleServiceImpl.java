package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdPrintSaleMapper;
import com.gys.business.mapper.entity.GaiaSdPrintSale;
import com.gys.business.service.GaiaSdPrintSaleService;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GaiaSdPrintSaleServiceImpl implements GaiaSdPrintSaleService {

    @Autowired
    private GaiaSdPrintSaleMapper gaiaSdPrintSaleMapper;

    @Override
    public JsonResult getPrintSaleByClientAndBrId(String clientId, String brId) {
        if(ObjectUtil.isEmpty(clientId)) {
            throw new BusinessException("提示：加盟商编号不能为空");
        }
        if(ObjectUtil.isEmpty(brId)) {
            throw new BusinessException("提示：门店编号不能为空");
        }
        GaiaSdPrintSale printSale = gaiaSdPrintSaleMapper.getPrintSaleByClientAndBrId(clientId, brId);
        return JsonResult.success(printSale, "success");
    }
}
