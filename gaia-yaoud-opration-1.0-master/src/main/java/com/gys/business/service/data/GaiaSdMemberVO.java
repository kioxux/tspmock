package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/12/2
 */
@Data
public class GaiaSdMemberVO implements Serializable {
    private static final long serialVersionUID = -7754305449935292023L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String brId;

    /**
     * 会员id
     */
    private String gsmbMemberId;

    /**
     * 卡类型
     */
    private String cardType;

    /**
     * 姓名
     */
    private String name;

    /**
     * 余额
     */
    private String balance;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 联系电话
     */
    private String contactNumber;

    /**
     * 地址
     */
    private String address;

    /**
     * 上次消费时间
     */
    private String LastTimeSpent;

    /**
     * 积分
     */
    private String integral;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 开卡来源
     */
    private String cardSource;

    /**
     * 开卡时间
     */
    private String openingTime;

    /**
     * 商务电话
     */
    private String businessPhone;

    /**
     * 家庭电话
     */
    private String homePhone;

    /**
     * QQ号
     */
    private String qq;

}
