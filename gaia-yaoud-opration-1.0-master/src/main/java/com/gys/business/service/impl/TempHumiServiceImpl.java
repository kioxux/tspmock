//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaSdTempHumiMapper;
import com.gys.business.mapper.entity.GaiaSdTempHumi;
import com.gys.business.service.TempHumiService;
import com.gys.business.service.data.TempHumiInData;
import com.gys.business.service.data.TempHumiOutData;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;

import java.math.BigDecimal;
import java.util.*;

import com.gys.util.CalendarUtil;
import com.gys.util.ValidateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

@Service
public class TempHumiServiceImpl implements TempHumiService {
    @Autowired
    private GaiaSdTempHumiMapper tempHumiMapper;

    public TempHumiServiceImpl() {
    }

    public PageInfo<TempHumiOutData> getTempHumiList(TempHumiInData inData) {
        if(ObjectUtil.isEmpty(inData.getBrIdList()) || inData.getBrIdList().length< 0) {
            throw new BusinessException("请输入门店");
        }
//        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<TempHumiOutData> tempHumiList = this.tempHumiMapper.getTempHumiList(inData);
        PageInfo pageInfo;
        if (CollectionUtils.isEmpty(tempHumiList)) {
            pageInfo = new PageInfo(new ArrayList());
        } else {
            for(int i = 0; i < tempHumiList.size(); ++i) {
                ((TempHumiOutData)tempHumiList.get(i)).setIndex(i + 1);
            }

            pageInfo = new PageInfo(tempHumiList);
        }

        return pageInfo;
    }

    @Transactional
    public void insertHumi(TempHumiInData inData, GetLoginOutData userInfo) {
        Date date = new Date();
        GaiaSdTempHumi tempHumi = new GaiaSdTempHumi();
        String tempDate = DateUtil.format(new Date(), "yyyy");
        String voucherIdTemp = this.tempHumiMapper.selectNextVoucherId(userInfo.getClient(), tempDate);
        inData.setGsthVoucherId(voucherIdTemp);
        BeanUtils.copyProperties(inData, tempHumi);
        tempHumi.setClientId(userInfo.getClient());
        tempHumi.setGsthBrId(userInfo.getDepId());
        tempHumi.setGsthBrName(userInfo.getDepName());
        tempHumi.setGsthDate(DateUtil.format(date, "yyyyMMdd"));
        tempHumi.setGsthTime(DateUtil.format(date, "HHmmss"));
        tempHumi.setGsthUpdateEmp(userInfo.getLoginName());
        tempHumi.setGsthUpdateDate(DateUtil.format(date, "yyyyMMdd"));
        tempHumi.setGsthUpdateTime(DateUtil.format(date, "HHmmss"));
        tempHumi.setGsthStatus("0");
        this.tempHumiMapper.insert(tempHumi);
    }

    @Transactional
    public void aduitTempHumi(TempHumiInData inData, GetLoginOutData userInfo) {
        Date date = new Date();
        GaiaSdTempHumi record = new GaiaSdTempHumi();
        record.setClientId(userInfo.getClient());
//        Iterator var5 = inData.iterator();

//        while(var5.hasNext()) {
//            TempHumiInData data = (TempHumiInData)var5.next();
            record.setGsthVoucherId(inData.getGsthVoucherId());
            GaiaSdTempHumi tempHumi = (GaiaSdTempHumi)this.tempHumiMapper.selectByPrimaryKey(record);
            if (ObjectUtil.isNull(tempHumi)) {
                throw new BusinessException(StrUtil.format("单号{}不存在", new Object[]{inData.getGsthVoucherId()}));
            }

            if ("1".equals(tempHumi.getGsthStatus())) {
                throw new BusinessException(StrUtil.format("单号{}已审核，请勿重复审核", new Object[]{inData.getGsthVoucherId()}));
            }

            tempHumi.setGsthUpdateEmp(userInfo.getLoginName());
            tempHumi.setGsthUpdateDate(DateUtil.format(date, "yyyyMMdd"));
            tempHumi.setGsthUpdateTime(DateUtil.format(date, "HHmmss"));
            tempHumi.setGsthStatus("1");
            this.tempHumiMapper.updateByPrimaryKey(tempHumi);
//        }

    }

    public void updateTempHumi(TempHumiInData inData, GetLoginOutData userInfo){
        GaiaSdTempHumi record = new GaiaSdTempHumi();
        record.setClientId(userInfo.getClient());
        record.setGsthVoucherId(inData.getGsthVoucherId());
        record.setGsthUpdateEmp(userInfo.getLoginName());
        record.setGsthCheckStep(inData.getGsthCheckStep());
        record.setGsthHumi(inData.getGsthHumi());
        record.setGsthTemp(inData.getGsthTemp());
        record.setGsthCheckHumi(inData.getGsthCheckHumi());
        record.setGsthCheckTemp(inData.getGsthCheckTemp());
        record.setGsthUpdateDate(DateUtil.format(new Date(), "yyyyMMdd"));
        record.setGsthUpdateTime(DateUtil.format(new Date(), "HHmmss"));
        this.tempHumiMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @author SunJiaNan
     * @Date 2021-4-20 15:49
     * @param userInfo
     * @return
     */
    @Override
    public JsonResult genTempHumi(GetLoginOutData userInfo) {
        //获取参数
        String clientId = userInfo.getClient();
        String brId = userInfo.getDepId();
        String brName = userInfo.getDepName();
        String loginName = userInfo.getLoginName();

        Date date = new Date();
        //获取当前加盟商最大单号
        String year = DateUtil.format(date, CommonConstant.FORMAT_YYYY);
        String voucherId = tempHumiMapper.selectCurrentVoucherId(clientId, year);
        if(ValidateUtil.isEmpty(voucherId)) {
            throw new BusinessException("提示：获取单号失败");
        }
        //获取本月第一天
        String thisMonthFirstDate = CalendarUtil.getThisMonthFirstDate();
        //获取当前日期
        String currentDate = com.gys.util.DateUtil.getCurrentDate();
        //获取本月已经过去的日期(包括当天)
        List<String> dayList = CalendarUtil.getDayList(thisMonthFirstDate, currentDate, CommonConstant.FORMAT_YYYYMMDD);
        //获取数据库中已经存在的日期
        List<String> dbDayList = tempHumiMapper.getDistinctDayList(clientId, brId, thisMonthFirstDate, currentDate);

        //将两个list中的重复的日期 进行删除  如果删除后list为空 表示无需生成数据
        Collection<String> dayCollection = new ArrayList<>(dayList);
        dayCollection.removeAll(dbDayList);
        if(ValidateUtil.isEmpty(dayCollection)) {
            throw new BusinessException("提示：无数据生成");
        }

        //截取掉前两位（截掉TH） 并转为BigDecimal类型
        BigDecimal genVoucherId = new BigDecimal(voucherId.substring(2));
        //每次循环累加
        BigDecimal one = new BigDecimal(CommonConstant.BIGDECIMAL_ONE);
        List<TempHumiInData> batchList = new ArrayList<>(dayCollection.size() * 3);   //精确指定List初始化长度 以免扩容降低效率
        //组装批量参数
        for(String day : dayCollection) {
            //区域1
            TempHumiInData areaOneTempHumi = new TempHumiInData();
            areaOneTempHumi.setClientId(clientId);                     //加盟商
            //生成单号
            BigDecimal firstAdd = genVoucherId.add(one);
            areaOneTempHumi.setGsthVoucherId(CommonConstant.PERFIX_TH + firstAdd.toString());
            areaOneTempHumi.setGsthBrId(brId);                         //门店编号
            areaOneTempHumi.setGsthBrName(brName);                     //门店名
            areaOneTempHumi.setGsthArea(CommonConstant.AREA_1);        //区域1
            areaOneTempHumi.setGsthDate(day);                          //检查日期(补录日期)
            areaOneTempHumi.setGsthTime(com.gys.util.DateUtil.getTimeRange(CommonConstant.MAX_CHECK_TIME, CommonConstant.MIN_CHECK_TIME));    //检查时间
            areaOneTempHumi.setGsthTemp(CommonConstant.INDOOR_TEMP);   //库内温度
            areaOneTempHumi.setGsthHumi(CommonConstant.LIBRARY_HUMI);  //库内湿度
            areaOneTempHumi.setGsthUpdateEmp(loginName);               //修改人员
            areaOneTempHumi.setGsthUpdateDate(DateUtil.format(date, CommonConstant.FORMAT_YYYYMMDD));    //修改日期
            areaOneTempHumi.setGsthUpdateTime(DateUtil.format(date, CommonConstant.FORMAT_HHMMSS));      //修改时间
            areaOneTempHumi.setGsthStatus(CommonConstant.STATE_1);     //审核状态
            batchList.add(areaOneTempHumi);

            //区域3
            TempHumiInData areaThreeTempHumi = new TempHumiInData();
            BeanUtil.copyProperties(areaOneTempHumi, areaThreeTempHumi);
            areaThreeTempHumi.setGsthArea(CommonConstant.AREA_3);
            BigDecimal secondAdd = firstAdd.add(one);
            areaThreeTempHumi.setGsthVoucherId(CommonConstant.PERFIX_TH + secondAdd.toString());   //生成单号
            batchList.add(areaThreeTempHumi);

            //区域5
            TempHumiInData areaFiveTempHumi = new TempHumiInData();
            BeanUtil.copyProperties(areaOneTempHumi, areaFiveTempHumi);
            areaFiveTempHumi.setGsthArea(CommonConstant.AREA_5);
            BigDecimal thirdAdd = secondAdd.add(one);
            areaFiveTempHumi.setGsthVoucherId(CommonConstant.PERFIX_TH + thirdAdd.toString());     //生成单号
            batchList.add(areaFiveTempHumi);

            //将单号重新赋值为当前最新
            genVoucherId = thirdAdd;
        }
        //批量插入
        tempHumiMapper.batchInsert(batchList);
        return JsonResult.success("", "保存成功");
    }
}
