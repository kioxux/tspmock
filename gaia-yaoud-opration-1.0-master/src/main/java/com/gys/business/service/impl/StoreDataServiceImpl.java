package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaAuthconfiDataMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.service.StoreDataService;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.CompadmDcOutData;
import com.gys.business.service.data.GetUserStoreOutData;
import com.gys.common.data.CommonData;
import com.gys.common.data.GetLoginOutData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class StoreDataServiceImpl implements StoreDataService {
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    private GaiaAuthconfiDataMapper authconfiDataMapper;

    @Override
    public StoreOutData queryStoLeader(GetLoginOutData inDate) {
        StoreOutData outData = this.gaiaStoreDataMapper.queryStoLeader(inDate);
        System.out.println("queryStoLeader : " + outData);
        return outData;
    }

    @Override
    public List<StoreOutData> queryStoInfoList(StoreOutData inData) {
        return this.gaiaStoreDataMapper.queryStoInfoList(inData);
    }

    @Override
    public List<CompadmDcOutData> selectAuthCompadmList(String clientId, String userId) {
        List<CompadmDcOutData> dcOutDataList = this.authconfiDataMapper.selectAuthCompadmList(clientId,userId);
        for (CompadmDcOutData dcOutData : dcOutDataList){
            dcOutData.setIsRatio("0");
            List<Map<String,String>> stoInfoList = gaiaStoreDataMapper.selectStoInfoList(clientId,dcOutData.getCompadmId());
            for (Map<String,String> map : stoInfoList){
                if (map.get("stoParam").equals("1")){
                    dcOutData.setIsRatio("1");
                    break;
                }
            }
        }
        return dcOutDataList;
    }

    @Override
    public List<GetUserStoreOutData> getStoreData(GetUserStoreOutData inData) {
        return this.gaiaStoreDataMapper.getStoreOutData(inData);
    }

    @Override
    public List<StoreOutData> getStoreList(String client, CommonData data) {
        List<StoreOutData> result = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(data)){
            result = this.gaiaStoreDataMapper.getStoreList(client,data.getContent());
        }else {
            result = this.gaiaStoreDataMapper.getStoreList(client,"");
        }
        return result;
    }

    @Override
    public StoreOutData getInventoryStore(InData inData) {
        return this.gaiaStoreDataMapper.getInventoryStore(inData);
    }
}
