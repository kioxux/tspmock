package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.service.data.percentageplan.PercentageBasicInData;
import com.gys.business.service.data.percentageplan.PercentageInData;
import com.gys.business.service.data.percentageplan.PercentageOutData;
import com.gys.business.service.data.percentageplan.PercentageProInData;
import com.gys.common.data.GetLoginOutData;

import java.util.List;
import java.util.Map;

public interface PercentagePlanNewStrategy {
    Integer insert(PercentageBasicInData inData);

    PageInfo<PercentageOutData> list(PercentageInData inData);

    Map<String, Object> tichengDetail(Long planId, String type, GetLoginOutData userInfo);

    void approve(Long planId, String planStatus, String type);

    void deletePlan(Long planId, String type);

    PercentageProInData selectProductByClient(String client, String proCode);

    List<PercentageProInData> getImportExcelDetailList(String clientId, List<ImportPercentageData> importInDataList);

    List<Map<String,String>>selectStoList(Long planId, Long type);

    Map<String, Object> tichengDetailCopy(Long planId, Long type);

    void stopPlan(Long planId, String planType, String stopType, String stopReason);

    void timerStopPlan();

}
