package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class GetPdAndExpOutData implements Serializable {
    private static final long serialVersionUID = 8948056649947205958L;
    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "通用名")
    private String proCommonName;

    @ApiModelProperty(value = "总库存")
    private String gssbQty;

    @ApiModelProperty(value = "批次库存")
    private String gssbQtyB;

    @ApiModelProperty(value = "批号")
    private String gssbBatchNo;

    @ApiModelProperty(value = "批次")
    private String gssbBatch;

    @ApiModelProperty(value = "有效期")
    private String gssbVaildDate;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "产地")
    private String proPlace;

    @ApiModelProperty(value = "货位")
    private String gsplSeat;

    @ApiModelProperty(value = "是否拆零 1是0否")
    private String proIfPart;

    @ApiModelProperty(value = "拆零单位")
    private String proPartUint;

    @ApiModelProperty(value = "拆零比例")
    private String proPartRate;

    @ApiModelProperty(value = "国家医保品种 0-否，1-是")
    private String proMedProdct;

    @ApiModelProperty(value = "批次库存加总")
    private String qtySour;

    @ApiModelProperty(value = "处方类别 1-处方药，2-甲类OTC，3-乙类OTC，4-双跨处方")
    private String proPresclass;

    @ApiModelProperty(value = "管制特殊分类 1-毒性药品，2-麻醉药品，3-一类精神药品，4-二类精神药品，5-易制毒药品（麻黄碱），6-放射性药品，7-生物制品（含胰岛素），8-兴奋剂（除胰岛素），9-第一类器械，10-第二类器械，11-第三类器械，12-其它管制")
    private String proControlClass;

    @ApiModelProperty(value = "销项税率")
    private String proOutputTax;

    @ApiModelProperty(value = "税率值")
    private String outputTaxValue;

    @ApiModelProperty(value = "商品仓储分区 1-内服药品，2-外用药品，3-中药饮片区，4-精装中药区，5-针剂，6-二类精神药品，7-含麻药品，8-冷藏商品，9-外用非药，10-医疗器械，11-食品，12-保健食品，13-易燃商品区")
    private String proStorageArea;

    @ApiModelProperty(value = "商品定位")
    private String proPosition;

    @ApiModelProperty(value = "禁止销售 0-否，1-是")
    private String proNoRetail;

    @ApiModelProperty(value = "图片路径")
    private String imageUrl = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201609%2F24%2F20160924025917_XKWzG.thumb.700_0.jpeg&refer=http%3A%2F%2Fb-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1617861328&t=5f4002e61120b4475898362f57380857";
    private String num;
    /**
     * 商品成分编码
     */
    private String proCompclass;

    /**
     * 国家贯标编码
     */
    private String proMedProdctcode;
    /**
     *  会员价
     */
    private String prcHy;

    /**
     * 当前拆零价格
     */
    private String currentDismantlementPrice;
}
