package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaClientBenefitLevelInfo;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author csm
 * @date 2021/12/18 - 17:04
 */
@Data
public class GaiaClientBenefitLevelInfoData {
    /**
     * 客户加盟ID
     */
    private String client;
    /**
     * 入参客户列表
     */
    private List<String> clientIds;
    @NotNull(message = "数据不能为空")
    private List<GaiaClientBenefitLevelInfo>  list;
}
