//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetGiftOutData {
    private String index;
    private String indexD;
    private String giveNum;
    private String giveNumD;
    private String giveNumHave;
    private String promotionName;
    private String giftCode;
    private String giftName;
    private String flag;
    private String promotionType;
    private String addPrice;
    private String addDisc;
    private String voucherId;
    private String promotionContent;
    private String proFactoryName;
    private String proSpecs;
    private String proPym;
    private String giftNormalPrice;
    private String barcode;
    private String giftStock;
    private String param4;

    public GetGiftOutData() {
    }

    public String getIndex() {
        return this.index;
    }

    public String getIndexD() {
        return this.indexD;
    }

    public String getGiveNum() {
        return this.giveNum;
    }

    public String getGiveNumD() {
        return this.giveNumD;
    }

    public String getGiveNumHave() {
        return this.giveNumHave;
    }

    public String getPromotionName() {
        return this.promotionName;
    }

    public String getGiftCode() {
        return this.giftCode;
    }

    public String getGiftName() {
        return this.giftName;
    }

    public String getFlag() {
        return this.flag;
    }

    public String getPromotionType() {
        return this.promotionType;
    }

    public String getAddPrice() {
        return this.addPrice;
    }

    public String getAddDisc() {
        return this.addDisc;
    }

    public String getVoucherId() {
        return this.voucherId;
    }

    public String getPromotionContent() {
        return this.promotionContent;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProPym() {
        return this.proPym;
    }

    public String getGiftNormalPrice() {
        return this.giftNormalPrice;
    }

    public String getBarcode() {
        return this.barcode;
    }

    public String getGiftStock() {
        return this.giftStock;
    }

    public String getParam4() {
        return this.param4;
    }

    public void setIndex(final String index) {
        this.index = index;
    }

    public void setIndexD(final String indexD) {
        this.indexD = indexD;
    }

    public void setGiveNum(final String giveNum) {
        this.giveNum = giveNum;
    }

    public void setGiveNumD(final String giveNumD) {
        this.giveNumD = giveNumD;
    }

    public void setGiveNumHave(final String giveNumHave) {
        this.giveNumHave = giveNumHave;
    }

    public void setPromotionName(final String promotionName) {
        this.promotionName = promotionName;
    }

    public void setGiftCode(final String giftCode) {
        this.giftCode = giftCode;
    }

    public void setGiftName(final String giftName) {
        this.giftName = giftName;
    }

    public void setFlag(final String flag) {
        this.flag = flag;
    }

    public void setPromotionType(final String promotionType) {
        this.promotionType = promotionType;
    }

    public void setAddPrice(final String addPrice) {
        this.addPrice = addPrice;
    }

    public void setAddDisc(final String addDisc) {
        this.addDisc = addDisc;
    }

    public void setVoucherId(final String voucherId) {
        this.voucherId = voucherId;
    }

    public void setPromotionContent(final String promotionContent) {
        this.promotionContent = promotionContent;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProPym(final String proPym) {
        this.proPym = proPym;
    }

    public void setGiftNormalPrice(final String giftNormalPrice) {
        this.giftNormalPrice = giftNormalPrice;
    }

    public void setBarcode(final String barcode) {
        this.barcode = barcode;
    }

    public void setGiftStock(final String giftStock) {
        this.giftStock = giftStock;
    }

    public void setParam4(final String param4) {
        this.param4 = param4;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetGiftOutData)) {
            return false;
        } else {
            GetGiftOutData other = (GetGiftOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label263: {
                    Object this$index = this.getIndex();
                    Object other$index = other.getIndex();
                    if (this$index == null) {
                        if (other$index == null) {
                            break label263;
                        }
                    } else if (this$index.equals(other$index)) {
                        break label263;
                    }

                    return false;
                }

                Object this$indexD = this.getIndexD();
                Object other$indexD = other.getIndexD();
                if (this$indexD == null) {
                    if (other$indexD != null) {
                        return false;
                    }
                } else if (!this$indexD.equals(other$indexD)) {
                    return false;
                }

                label249: {
                    Object this$giveNum = this.getGiveNum();
                    Object other$giveNum = other.getGiveNum();
                    if (this$giveNum == null) {
                        if (other$giveNum == null) {
                            break label249;
                        }
                    } else if (this$giveNum.equals(other$giveNum)) {
                        break label249;
                    }

                    return false;
                }

                Object this$giveNumD = this.getGiveNumD();
                Object other$giveNumD = other.getGiveNumD();
                if (this$giveNumD == null) {
                    if (other$giveNumD != null) {
                        return false;
                    }
                } else if (!this$giveNumD.equals(other$giveNumD)) {
                    return false;
                }

                label235: {
                    Object this$giveNumHave = this.getGiveNumHave();
                    Object other$giveNumHave = other.getGiveNumHave();
                    if (this$giveNumHave == null) {
                        if (other$giveNumHave == null) {
                            break label235;
                        }
                    } else if (this$giveNumHave.equals(other$giveNumHave)) {
                        break label235;
                    }

                    return false;
                }

                Object this$promotionName = this.getPromotionName();
                Object other$promotionName = other.getPromotionName();
                if (this$promotionName == null) {
                    if (other$promotionName != null) {
                        return false;
                    }
                } else if (!this$promotionName.equals(other$promotionName)) {
                    return false;
                }

                label221: {
                    Object this$giftCode = this.getGiftCode();
                    Object other$giftCode = other.getGiftCode();
                    if (this$giftCode == null) {
                        if (other$giftCode == null) {
                            break label221;
                        }
                    } else if (this$giftCode.equals(other$giftCode)) {
                        break label221;
                    }

                    return false;
                }

                label214: {
                    Object this$giftName = this.getGiftName();
                    Object other$giftName = other.getGiftName();
                    if (this$giftName == null) {
                        if (other$giftName == null) {
                            break label214;
                        }
                    } else if (this$giftName.equals(other$giftName)) {
                        break label214;
                    }

                    return false;
                }

                Object this$flag = this.getFlag();
                Object other$flag = other.getFlag();
                if (this$flag == null) {
                    if (other$flag != null) {
                        return false;
                    }
                } else if (!this$flag.equals(other$flag)) {
                    return false;
                }

                label200: {
                    Object this$promotionType = this.getPromotionType();
                    Object other$promotionType = other.getPromotionType();
                    if (this$promotionType == null) {
                        if (other$promotionType == null) {
                            break label200;
                        }
                    } else if (this$promotionType.equals(other$promotionType)) {
                        break label200;
                    }

                    return false;
                }

                label193: {
                    Object this$addPrice = this.getAddPrice();
                    Object other$addPrice = other.getAddPrice();
                    if (this$addPrice == null) {
                        if (other$addPrice == null) {
                            break label193;
                        }
                    } else if (this$addPrice.equals(other$addPrice)) {
                        break label193;
                    }

                    return false;
                }

                Object this$addDisc = this.getAddDisc();
                Object other$addDisc = other.getAddDisc();
                if (this$addDisc == null) {
                    if (other$addDisc != null) {
                        return false;
                    }
                } else if (!this$addDisc.equals(other$addDisc)) {
                    return false;
                }

                Object this$voucherId = this.getVoucherId();
                Object other$voucherId = other.getVoucherId();
                if (this$voucherId == null) {
                    if (other$voucherId != null) {
                        return false;
                    }
                } else if (!this$voucherId.equals(other$voucherId)) {
                    return false;
                }

                label172: {
                    Object this$promotionContent = this.getPromotionContent();
                    Object other$promotionContent = other.getPromotionContent();
                    if (this$promotionContent == null) {
                        if (other$promotionContent == null) {
                            break label172;
                        }
                    } else if (this$promotionContent.equals(other$promotionContent)) {
                        break label172;
                    }

                    return false;
                }

                Object this$proFactoryName = this.getProFactoryName();
                Object other$proFactoryName = other.getProFactoryName();
                if (this$proFactoryName == null) {
                    if (other$proFactoryName != null) {
                        return false;
                    }
                } else if (!this$proFactoryName.equals(other$proFactoryName)) {
                    return false;
                }

                Object this$proSpecs = this.getProSpecs();
                Object other$proSpecs = other.getProSpecs();
                if (this$proSpecs == null) {
                    if (other$proSpecs != null) {
                        return false;
                    }
                } else if (!this$proSpecs.equals(other$proSpecs)) {
                    return false;
                }

                label151: {
                    Object this$proPym = this.getProPym();
                    Object other$proPym = other.getProPym();
                    if (this$proPym == null) {
                        if (other$proPym == null) {
                            break label151;
                        }
                    } else if (this$proPym.equals(other$proPym)) {
                        break label151;
                    }

                    return false;
                }

                Object this$giftNormalPrice = this.getGiftNormalPrice();
                Object other$giftNormalPrice = other.getGiftNormalPrice();
                if (this$giftNormalPrice == null) {
                    if (other$giftNormalPrice != null) {
                        return false;
                    }
                } else if (!this$giftNormalPrice.equals(other$giftNormalPrice)) {
                    return false;
                }

                label137: {
                    Object this$barcode = this.getBarcode();
                    Object other$barcode = other.getBarcode();
                    if (this$barcode == null) {
                        if (other$barcode == null) {
                            break label137;
                        }
                    } else if (this$barcode.equals(other$barcode)) {
                        break label137;
                    }

                    return false;
                }

                Object this$giftStock = this.getGiftStock();
                Object other$giftStock = other.getGiftStock();
                if (this$giftStock == null) {
                    if (other$giftStock != null) {
                        return false;
                    }
                } else if (!this$giftStock.equals(other$giftStock)) {
                    return false;
                }

                Object this$param4 = this.getParam4();
                Object other$param4 = other.getParam4();
                if (this$param4 == null) {
                    if (other$param4 == null) {
                        return true;
                    }
                } else if (this$param4.equals(other$param4)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetGiftOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $indexD = this.getIndexD();
        result = result * 59 + ($indexD == null ? 43 : $indexD.hashCode());
        Object $giveNum = this.getGiveNum();
        result = result * 59 + ($giveNum == null ? 43 : $giveNum.hashCode());
        Object $giveNumD = this.getGiveNumD();
        result = result * 59 + ($giveNumD == null ? 43 : $giveNumD.hashCode());
        Object $giveNumHave = this.getGiveNumHave();
        result = result * 59 + ($giveNumHave == null ? 43 : $giveNumHave.hashCode());
        Object $promotionName = this.getPromotionName();
        result = result * 59 + ($promotionName == null ? 43 : $promotionName.hashCode());
        Object $giftCode = this.getGiftCode();
        result = result * 59 + ($giftCode == null ? 43 : $giftCode.hashCode());
        Object $giftName = this.getGiftName();
        result = result * 59 + ($giftName == null ? 43 : $giftName.hashCode());
        Object $flag = this.getFlag();
        result = result * 59 + ($flag == null ? 43 : $flag.hashCode());
        Object $promotionType = this.getPromotionType();
        result = result * 59 + ($promotionType == null ? 43 : $promotionType.hashCode());
        Object $addPrice = this.getAddPrice();
        result = result * 59 + ($addPrice == null ? 43 : $addPrice.hashCode());
        Object $addDisc = this.getAddDisc();
        result = result * 59 + ($addDisc == null ? 43 : $addDisc.hashCode());
        Object $voucherId = this.getVoucherId();
        result = result * 59 + ($voucherId == null ? 43 : $voucherId.hashCode());
        Object $promotionContent = this.getPromotionContent();
        result = result * 59 + ($promotionContent == null ? 43 : $promotionContent.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proPym = this.getProPym();
        result = result * 59 + ($proPym == null ? 43 : $proPym.hashCode());
        Object $giftNormalPrice = this.getGiftNormalPrice();
        result = result * 59 + ($giftNormalPrice == null ? 43 : $giftNormalPrice.hashCode());
        Object $barcode = this.getBarcode();
        result = result * 59 + ($barcode == null ? 43 : $barcode.hashCode());
        Object $giftStock = this.getGiftStock();
        result = result * 59 + ($giftStock == null ? 43 : $giftStock.hashCode());
        Object $param4 = this.getParam4();
        result = result * 59 + ($param4 == null ? 43 : $param4.hashCode());
        return result;
    }

    public String toString() {
        return "GetGiftOutData(index=" + this.getIndex() + ", indexD=" + this.getIndexD() + ", giveNum=" + this.getGiveNum() + ", giveNumD=" + this.getGiveNumD() + ", giveNumHave=" + this.getGiveNumHave() + ", promotionName=" + this.getPromotionName() + ", giftCode=" + this.getGiftCode() + ", giftName=" + this.getGiftName() + ", flag=" + this.getFlag() + ", promotionType=" + this.getPromotionType() + ", addPrice=" + this.getAddPrice() + ", addDisc=" + this.getAddDisc() + ", voucherId=" + this.getVoucherId() + ", promotionContent=" + this.getPromotionContent() + ", proFactoryName=" + this.getProFactoryName() + ", proSpecs=" + this.getProSpecs() + ", proPym=" + this.getProPym() + ", giftNormalPrice=" + this.getGiftNormalPrice() + ", barcode=" + this.getBarcode() + ", giftStock=" + this.getGiftStock() + ", param4=" + this.getParam4() + ")";
    }
}
