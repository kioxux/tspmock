package com.gys.business.service.takeAway;

import java.util.List;
import java.util.Map;

public interface BaseServiceWeb {
    Map<String, Object> getSwitchInfo(Map<String, Object> param);

    Map<String, Object> setSwitchInfo(Map<String, Object> param);

    String insertMyAsyncTask(Map<String, Object> param);

    List<Map<String,Object>> SwichSetTable(Map<String, Object> param);

    void updateMyAsyncTask(Map<String, Object> param);

    Map<String, Object> getMyAsyncTaskInfo(Map<String, Object> param);

    Map<String, Object> getMyAsyncTaskDetail(Map<String, Object> param);

    Map<String, Object> getMedicineInfo(Map<String, Object> param);

    Map<String, Object> setMedicineInfo(Map<String, Object> param);
}
