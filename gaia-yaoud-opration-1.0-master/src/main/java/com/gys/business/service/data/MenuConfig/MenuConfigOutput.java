package com.gys.business.service.data.MenuConfig;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/4/6 17:32
 * @Version 1.0.0
 **/
@Data
@ApiModel(value = "菜单分配")
public class MenuConfigOutput {
    /**
     * 菜单编码
     */
    @ApiModelProperty(name = "菜单编码")
    private Long id;

    /**
     * 菜单名称
     */
    @ApiModelProperty(name = "菜单名称")
    private String title;

    /**
     * 父菜单id
     */
    @ApiModelProperty(name = "父菜单id")
    private Long parentId;

    /**
     * 权限状态 1 选择 2 未选中
     */
    @ApiModelProperty(name = "权限状态")
    private Integer menuState;

    private List<MenuConfigOutput> lists;

    private List<MenuAuthConfig> auths;
}
