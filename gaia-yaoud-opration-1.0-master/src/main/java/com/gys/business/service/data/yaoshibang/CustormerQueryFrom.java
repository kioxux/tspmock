package com.gys.business.service.data.yaoshibang;

import lombok.Data;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/12/22 13:32
 */
@Data
public class CustormerQueryFrom {
    private String drugstoreName;
    private String regAdress;
    private String client;
}
