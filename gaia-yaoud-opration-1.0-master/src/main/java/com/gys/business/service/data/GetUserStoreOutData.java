package com.gys.business.service.data;

import lombok.Data;

@Data
public class GetUserStoreOutData {
    private String client;
    private String userId;
    private String stoCode;
    private String stoName;
    private String paramName;
    private String paramValue;
}
