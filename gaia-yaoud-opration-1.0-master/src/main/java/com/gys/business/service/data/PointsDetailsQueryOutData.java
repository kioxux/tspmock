package com.gys.business.service.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * 返回映射的查询实体
 *
 * @author xiaoyuan on 2020/7/28
 */
@Data
@CsvRow
public class PointsDetailsQueryOutData implements Serializable {
    private static final long serialVersionUID = 2447423288399526571L;

    /**
     * 序号
     */
    private Integer index;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    @CsvCell(title = "办卡门店编号", index = 1, fieldNo = 1)
    private String brId;

    /**
     * 门店名称
     */
    @CsvCell(title = "办卡门店名称", index = 2, fieldNo = 1)
    private String brName;

    /**
     * 会员卡号
     */
    @CsvCell(title = "会员卡号", index = 5, fieldNo = 1)
    private String gmbCardId;

    /**
     * 会员名称
     */
    @CsvCell(title = "会员姓名", index = 6, fieldNo = 1)
    private String memberName;

    /**
     * 本次积分
     */
    @CsvCell(title = "本次积分", index = 9, fieldNo = 1)
    private String pointsThisTime;

    /**
     * 异动单号
     */
    @CsvCell(title = "异动单号", index = 11, fieldNo = 1)
    private String voucherId;

    /**
     * 最后修改日期
     */
    @CsvCell(title = "异动日期", index = 12, fieldNo = 1)
    private String gsicDate;

    /**
     * 备注
     */
    @CsvCell(title = "异动类型" ,index = 13, fieldNo = 1)
    private String remarks;

    /**
     * 异动人员
     */
    @CsvCell(title = "修改人员", index = 14, fieldNo = 1)
    private String userNam;

    /**
     * 会员手机号
     */
    private String gsmbMobile;

    /**
     * 卡类型
     */
    @CsvCell(title = "会员卡类型", index = 7, fieldNo = 1)
    private String gsmcType;

    /**
     * 消费店号
     */
    @CsvCell(title = "消费门店编号", index = 3, fieldNo = 1)
    private String consStoCode;

    /**
     * 消费门店名称
     */
    @CsvCell(title = "消费门店名称", index = 4, fieldNo = 1)
    private String consStoName;

    /**
     * 初始积分
     */
    @CsvCell(title = "初始积分", index = 8, fieldNo = 1)
    private String initIntegral;

    /**
     * 结果积分
     */
    @CsvCell(title = "结果积分", index = 10, fieldNo = 1)
    private String resultIntegral;
}
