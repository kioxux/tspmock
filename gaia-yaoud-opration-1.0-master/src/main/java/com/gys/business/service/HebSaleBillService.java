package com.gys.business.service;

import java.util.List;
import java.util.Map;

/**
 * @author shentao
 */
public interface HebSaleBillService {
    /**
     * 销售订单上传
     */
    Map<String, Object> payCommit(HebBillData billList);
}
