package com.gys.business.service.data;

import com.gys.business.mapper.entity.RecommendFirstPalnSto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RecommendFirstPlanInData implements Serializable {

    private static final long serialVersionUID = 8462213338705948880L;

    private Long id;

    private String planName;

    private String effectStartDate;

    private String effectEndDate;

    private List<RecommendFirstPalnSto> palnStos;

    private List<String> stos;

    private String preferences;

    private List<String> proIds;

    @ApiModelProperty(value = "页面展示数量", required = false)
    public Integer pageSize;

    @ApiModelProperty(value = "页数", required = false)
    private Integer pageNum;

}
