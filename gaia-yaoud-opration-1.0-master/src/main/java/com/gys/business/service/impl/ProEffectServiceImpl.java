//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaSdProEffectMapper;
import com.gys.business.service.ProEffectService;
import com.gys.business.service.data.ProEffectInData;
import com.gys.business.service.data.ProEffectOutData;
import com.gys.common.data.PageInfo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class ProEffectServiceImpl implements ProEffectService {
    @Autowired
    private GaiaSdProEffectMapper proEffectMapper;

    public ProEffectServiceImpl() {
    }

    public PageInfo<ProEffectOutData> proEffectList(ProEffectInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<ProEffectOutData> proEffectList = this.proEffectMapper.proEffectList(inData);
        PageInfo pageInfo;
        if (CollectionUtils.isEmpty(proEffectList)) {
            pageInfo = new PageInfo(new ArrayList());
        } else {
            for(int i = 0; i < proEffectList.size(); ++i) {
                ((ProEffectOutData)proEffectList.get(i)).setIndex(i + 1);
            }

            pageInfo = new PageInfo(proEffectList);
        }

        return pageInfo;
    }
}
