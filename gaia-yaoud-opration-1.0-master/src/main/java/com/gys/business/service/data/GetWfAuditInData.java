package com.gys.business.service.data;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/20 12:11
 */
@Data
public class GetWfAuditInData {

    private String wfTitle;
    private String client;
    private int pageNum = 1;
    private int pageSize = 10;
    private String startDate = "";
    private String endDate = "";
    private List value2 = Lists.newArrayList();

}
