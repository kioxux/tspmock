package com.gys.business.service.newProductsOrder.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.excel.EasyExcel;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.ProductsOrderServiceMapper;
import com.gys.business.service.data.ProductsOrderInData;
import com.gys.business.service.newProductsOrder.ProductsOrderService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.data.newProductsOrder.NewProductsOrderInfoDTO;
import com.gys.common.exception.BusinessException;
import com.gys.util.ExcelStyleUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

@Service
public class ProductsOrderServiceImpl implements ProductsOrderService {

    @Autowired
    ProductsOrderServiceMapper productsOrderServiceMapper;


    @Override
    public PageInfo getOrderList(GetLoginOutData userInfo, List<String> gsphBrIdList, String startDate, String endDate, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<NewProductsOrderInfoDTO> orderInfoDTOS = productsOrderServiceMapper.queryOrderList(userInfo.getClient(), gsphBrIdList, startDate, endDate);
        return new PageInfo(orderInfoDTOS);
    }

    @Override
    public void getOrderListExport(HttpServletResponse response, GetLoginOutData userInfo, ProductsOrderInData inData) {
        List<NewProductsOrderInfoDTO> list = productsOrderServiceMapper.queryOrderList(userInfo.getClient(),
                inData.getGsphBrIdList(), inData.getStartDate(), inData.getEndDate());
        if(CollectionUtil.isEmpty(list)){
            throw new BusinessException("导出数据不能为空");
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = "新品订单查询-导出";
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

        try {
            EasyExcel.write(response.getOutputStream(),NewProductsOrderInfoDTO.class)
                    .registerWriteHandler(ExcelStyleUtil.getStyle())
                    .sheet(0).doWrite(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
