package com.gys.business.service.data;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.NumberFormat;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/4/7 15:32
 * @Version 1.0.0
 **/
@Data
public class InventoryInquiryOutDataExport {

    @ExcelProperty(value = "商品编号",index = 0)
    private String gssmProId;

    @ExcelProperty(value = "商品名称",index = 1)
    private String gssmProName;

    @ExcelProperty(value = "零售价",index = 2)
    private BigDecimal retailPrice;

    @ExcelProperty(value = "批号",index = 3)
    private String gssmBatchNo;

    @ExcelProperty(value = "有效期至",index = 4)
    private String validUntil;

    @NumberFormat(value = "0.00")
    @ExcelProperty(value = "库存",index = 5)
    private BigDecimal qty;

    @ExcelProperty(value = "厂家",index = 6)
    private String factory;

    @ExcelProperty(value = "产地",index = 7)
    private String origin;

    @ExcelProperty(value = "剂型",index = 8)
    private String dosageForm;

    @ExcelProperty(value = "单位",index = 9)
    private String unit;

    @ExcelProperty(value = "规格",index = 10)
    private String format;

    @ExcelProperty(value = "批准文号",index = 11)
    private String approvalNum;

    @ExcelProperty(value = "定位",index = 12)
    private String proPosition;

    @NumberFormat(value = "0.00")
    @ExcelProperty(value = "成本价",index = 13)
    private BigDecimal addPrice;

    @NumberFormat(value = "0.00")
    @ExcelProperty(value = "成本额",index = 14)
    private BigDecimal costAmount;



}
