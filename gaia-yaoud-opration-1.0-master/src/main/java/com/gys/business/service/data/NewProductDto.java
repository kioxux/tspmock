package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:14 2021/8/9
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class NewProductDto {
    /**
     * 当前页码
     */
    @NotNull(message = "分页参数不能为空！")
    private Integer pageNum;

    /**
     * 每页条数
     */
    @NotNull(message = "分页参数不能为空！")
    private Integer pageSize;

    /**
     * 查询内容
     */
    private String content;

    /**
     * 门店code
     */
    @NotEmpty(message = "门店code不能为空！")
    private String[] client;

    /**
     * 查询条件逗号切割
     */
    private String[] proArr;
}
