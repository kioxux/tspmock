//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class TempHumiOutData {
    private String clientId;
    private String gsthVoucherId;
    private String gsthBrId;
    private String gsthBrName;
    private String gsthArea;
    private String gsthDate;
    private String gsthTime;
    private String gsthTemp;
    private String gsthHumi;
    private String gsthCheckStep;
    private String gsthCheckTemp;
    private String gsthCheckHumi;
    private String gsthUpdateEmp;
    private String gsthUpdateDate;
    private String gsthUpdateTime;
    private String gsthStatus;
    private Integer index;

    public TempHumiOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsthVoucherId() {
        return this.gsthVoucherId;
    }

    public String getGsthBrId() {
        return this.gsthBrId;
    }

    public String getGsthBrName() {
        return this.gsthBrName;
    }

    public String getGsthArea() {
        return this.gsthArea;
    }

    public String getGsthDate() {
        return this.gsthDate;
    }

    public String getGsthTime() {
        return this.gsthTime;
    }

    public String getGsthTemp() {
        return this.gsthTemp;
    }

    public String getGsthHumi() {
        return this.gsthHumi;
    }

    public String getGsthCheckStep() {
        return this.gsthCheckStep;
    }

    public String getGsthCheckTemp() {
        return this.gsthCheckTemp;
    }

    public String getGsthCheckHumi() {
        return this.gsthCheckHumi;
    }

    public String getGsthUpdateEmp() {
        return this.gsthUpdateEmp;
    }

    public String getGsthUpdateDate() {
        return this.gsthUpdateDate;
    }

    public String getGsthUpdateTime() {
        return this.gsthUpdateTime;
    }

    public String getGsthStatus() {
        return this.gsthStatus;
    }

    public Integer getIndex() {
        return this.index;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsthVoucherId(final String gsthVoucherId) {
        this.gsthVoucherId = gsthVoucherId;
    }

    public void setGsthBrId(final String gsthBrId) {
        this.gsthBrId = gsthBrId;
    }

    public void setGsthBrName(final String gsthBrName) {
        this.gsthBrName = gsthBrName;
    }

    public void setGsthArea(final String gsthArea) {
        this.gsthArea = gsthArea;
    }

    public void setGsthDate(final String gsthDate) {
        this.gsthDate = gsthDate;
    }

    public void setGsthTime(final String gsthTime) {
        this.gsthTime = gsthTime;
    }

    public void setGsthTemp(final String gsthTemp) {
        this.gsthTemp = gsthTemp;
    }

    public void setGsthHumi(final String gsthHumi) {
        this.gsthHumi = gsthHumi;
    }

    public void setGsthCheckStep(final String gsthCheckStep) {
        this.gsthCheckStep = gsthCheckStep;
    }

    public void setGsthCheckTemp(final String gsthCheckTemp) {
        this.gsthCheckTemp = gsthCheckTemp;
    }

    public void setGsthCheckHumi(final String gsthCheckHumi) {
        this.gsthCheckHumi = gsthCheckHumi;
    }

    public void setGsthUpdateEmp(final String gsthUpdateEmp) {
        this.gsthUpdateEmp = gsthUpdateEmp;
    }

    public void setGsthUpdateDate(final String gsthUpdateDate) {
        this.gsthUpdateDate = gsthUpdateDate;
    }

    public void setGsthUpdateTime(final String gsthUpdateTime) {
        this.gsthUpdateTime = gsthUpdateTime;
    }

    public void setGsthStatus(final String gsthStatus) {
        this.gsthStatus = gsthStatus;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof TempHumiOutData)) {
            return false;
        } else {
            TempHumiOutData other = (TempHumiOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label215: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label215;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label215;
                    }

                    return false;
                }

                Object this$gsthVoucherId = this.getGsthVoucherId();
                Object other$gsthVoucherId = other.getGsthVoucherId();
                if (this$gsthVoucherId == null) {
                    if (other$gsthVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsthVoucherId.equals(other$gsthVoucherId)) {
                    return false;
                }

                label201: {
                    Object this$gsthBrId = this.getGsthBrId();
                    Object other$gsthBrId = other.getGsthBrId();
                    if (this$gsthBrId == null) {
                        if (other$gsthBrId == null) {
                            break label201;
                        }
                    } else if (this$gsthBrId.equals(other$gsthBrId)) {
                        break label201;
                    }

                    return false;
                }

                Object this$gsthBrName = this.getGsthBrName();
                Object other$gsthBrName = other.getGsthBrName();
                if (this$gsthBrName == null) {
                    if (other$gsthBrName != null) {
                        return false;
                    }
                } else if (!this$gsthBrName.equals(other$gsthBrName)) {
                    return false;
                }

                label187: {
                    Object this$gsthArea = this.getGsthArea();
                    Object other$gsthArea = other.getGsthArea();
                    if (this$gsthArea == null) {
                        if (other$gsthArea == null) {
                            break label187;
                        }
                    } else if (this$gsthArea.equals(other$gsthArea)) {
                        break label187;
                    }

                    return false;
                }

                Object this$gsthDate = this.getGsthDate();
                Object other$gsthDate = other.getGsthDate();
                if (this$gsthDate == null) {
                    if (other$gsthDate != null) {
                        return false;
                    }
                } else if (!this$gsthDate.equals(other$gsthDate)) {
                    return false;
                }

                label173: {
                    Object this$gsthTime = this.getGsthTime();
                    Object other$gsthTime = other.getGsthTime();
                    if (this$gsthTime == null) {
                        if (other$gsthTime == null) {
                            break label173;
                        }
                    } else if (this$gsthTime.equals(other$gsthTime)) {
                        break label173;
                    }

                    return false;
                }

                label166: {
                    Object this$gsthTemp = this.getGsthTemp();
                    Object other$gsthTemp = other.getGsthTemp();
                    if (this$gsthTemp == null) {
                        if (other$gsthTemp == null) {
                            break label166;
                        }
                    } else if (this$gsthTemp.equals(other$gsthTemp)) {
                        break label166;
                    }

                    return false;
                }

                Object this$gsthHumi = this.getGsthHumi();
                Object other$gsthHumi = other.getGsthHumi();
                if (this$gsthHumi == null) {
                    if (other$gsthHumi != null) {
                        return false;
                    }
                } else if (!this$gsthHumi.equals(other$gsthHumi)) {
                    return false;
                }

                label152: {
                    Object this$gsthCheckStep = this.getGsthCheckStep();
                    Object other$gsthCheckStep = other.getGsthCheckStep();
                    if (this$gsthCheckStep == null) {
                        if (other$gsthCheckStep == null) {
                            break label152;
                        }
                    } else if (this$gsthCheckStep.equals(other$gsthCheckStep)) {
                        break label152;
                    }

                    return false;
                }

                label145: {
                    Object this$gsthCheckTemp = this.getGsthCheckTemp();
                    Object other$gsthCheckTemp = other.getGsthCheckTemp();
                    if (this$gsthCheckTemp == null) {
                        if (other$gsthCheckTemp == null) {
                            break label145;
                        }
                    } else if (this$gsthCheckTemp.equals(other$gsthCheckTemp)) {
                        break label145;
                    }

                    return false;
                }

                Object this$gsthCheckHumi = this.getGsthCheckHumi();
                Object other$gsthCheckHumi = other.getGsthCheckHumi();
                if (this$gsthCheckHumi == null) {
                    if (other$gsthCheckHumi != null) {
                        return false;
                    }
                } else if (!this$gsthCheckHumi.equals(other$gsthCheckHumi)) {
                    return false;
                }

                Object this$gsthUpdateEmp = this.getGsthUpdateEmp();
                Object other$gsthUpdateEmp = other.getGsthUpdateEmp();
                if (this$gsthUpdateEmp == null) {
                    if (other$gsthUpdateEmp != null) {
                        return false;
                    }
                } else if (!this$gsthUpdateEmp.equals(other$gsthUpdateEmp)) {
                    return false;
                }

                label124: {
                    Object this$gsthUpdateDate = this.getGsthUpdateDate();
                    Object other$gsthUpdateDate = other.getGsthUpdateDate();
                    if (this$gsthUpdateDate == null) {
                        if (other$gsthUpdateDate == null) {
                            break label124;
                        }
                    } else if (this$gsthUpdateDate.equals(other$gsthUpdateDate)) {
                        break label124;
                    }

                    return false;
                }

                Object this$gsthUpdateTime = this.getGsthUpdateTime();
                Object other$gsthUpdateTime = other.getGsthUpdateTime();
                if (this$gsthUpdateTime == null) {
                    if (other$gsthUpdateTime != null) {
                        return false;
                    }
                } else if (!this$gsthUpdateTime.equals(other$gsthUpdateTime)) {
                    return false;
                }

                Object this$gsthStatus = this.getGsthStatus();
                Object other$gsthStatus = other.getGsthStatus();
                if (this$gsthStatus == null) {
                    if (other$gsthStatus != null) {
                        return false;
                    }
                } else if (!this$gsthStatus.equals(other$gsthStatus)) {
                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof TempHumiOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsthVoucherId = this.getGsthVoucherId();
        result = result * 59 + ($gsthVoucherId == null ? 43 : $gsthVoucherId.hashCode());
        Object $gsthBrId = this.getGsthBrId();
        result = result * 59 + ($gsthBrId == null ? 43 : $gsthBrId.hashCode());
        Object $gsthBrName = this.getGsthBrName();
        result = result * 59 + ($gsthBrName == null ? 43 : $gsthBrName.hashCode());
        Object $gsthArea = this.getGsthArea();
        result = result * 59 + ($gsthArea == null ? 43 : $gsthArea.hashCode());
        Object $gsthDate = this.getGsthDate();
        result = result * 59 + ($gsthDate == null ? 43 : $gsthDate.hashCode());
        Object $gsthTime = this.getGsthTime();
        result = result * 59 + ($gsthTime == null ? 43 : $gsthTime.hashCode());
        Object $gsthTemp = this.getGsthTemp();
        result = result * 59 + ($gsthTemp == null ? 43 : $gsthTemp.hashCode());
        Object $gsthHumi = this.getGsthHumi();
        result = result * 59 + ($gsthHumi == null ? 43 : $gsthHumi.hashCode());
        Object $gsthCheckStep = this.getGsthCheckStep();
        result = result * 59 + ($gsthCheckStep == null ? 43 : $gsthCheckStep.hashCode());
        Object $gsthCheckTemp = this.getGsthCheckTemp();
        result = result * 59 + ($gsthCheckTemp == null ? 43 : $gsthCheckTemp.hashCode());
        Object $gsthCheckHumi = this.getGsthCheckHumi();
        result = result * 59 + ($gsthCheckHumi == null ? 43 : $gsthCheckHumi.hashCode());
        Object $gsthUpdateEmp = this.getGsthUpdateEmp();
        result = result * 59 + ($gsthUpdateEmp == null ? 43 : $gsthUpdateEmp.hashCode());
        Object $gsthUpdateDate = this.getGsthUpdateDate();
        result = result * 59 + ($gsthUpdateDate == null ? 43 : $gsthUpdateDate.hashCode());
        Object $gsthUpdateTime = this.getGsthUpdateTime();
        result = result * 59 + ($gsthUpdateTime == null ? 43 : $gsthUpdateTime.hashCode());
        Object $gsthStatus = this.getGsthStatus();
        result = result * 59 + ($gsthStatus == null ? 43 : $gsthStatus.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        return result;
    }

    public String toString() {
        return "TempHumiOutData(clientId=" + this.getClientId() + ", gsthVoucherId=" + this.getGsthVoucherId() + ", gsthBrId=" + this.getGsthBrId() + ", gsthBrName=" + this.getGsthBrName() + ", gsthArea=" + this.getGsthArea() + ", gsthDate=" + this.getGsthDate() + ", gsthTime=" + this.getGsthTime() + ", gsthTemp=" + this.getGsthTemp() + ", gsthHumi=" + this.getGsthHumi() + ", gsthCheckStep=" + this.getGsthCheckStep() + ", gsthCheckTemp=" + this.getGsthCheckTemp() + ", gsthCheckHumi=" + this.getGsthCheckHumi() + ", gsthUpdateEmp=" + this.getGsthUpdateEmp() + ", gsthUpdateDate=" + this.getGsthUpdateDate() + ", gsthUpdateTime=" + this.getGsthUpdateTime() + ", gsthStatus=" + this.getGsthStatus() + ", index=" + this.getIndex() + ")";
    }
}
