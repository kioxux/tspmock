package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaFranchiseeMapper;
import com.gys.business.mapper.GaiaProductSpecialMapper;
import com.gys.business.service.CommonAssemblyService;
import com.gys.business.service.data.AreaOutData;
import com.gys.business.service.data.ProvCityInData;
import com.gys.business.service.data.ProvinceCityOutData;
import com.gys.common.data.JsonResult;
import com.gys.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *  @author SunJiaNan
 *  @version 1.0
 *  @date 2021/11/16 16:05
 *  @description 公用数据相关接口
 */
@Service
@Slf4j
public class CommonAssemblyServiceImpl implements CommonAssemblyService {

    @Autowired
    private GaiaProductSpecialMapper productSpecialMapper;
    @Autowired
    private GaiaFranchiseeMapper franchiseeMapper;


    @Override
    public JsonResult getProvCityList() {
        List<AreaOutData> areaList = productSpecialMapper.getAreaList();
        List<ProvinceCityOutData> provinceCityList = getAreInfo(areaList);
        return JsonResult.success(provinceCityList, "success");
    }


    /**
     * 获取省市下拉框数据
     * @param areaList
     * @return
     */
    private List<ProvinceCityOutData> getAreInfo(List<AreaOutData> areaList) {
        List<ProvinceCityOutData> provinceCityList = new ArrayList<>(areaList.size());
        if(ValidateUtil.isNotEmpty(areaList)) {
            //将list根据 level转成map  key为level value为对应的list
            Map<String, List<AreaOutData>> areaListMap = areaList.stream().collect(Collectors.groupingBy(item -> item.getLevel()));
            List<AreaOutData> provinceList = areaListMap.get("1");   //获取省列表
            List<AreaOutData> cityList = areaListMap.get("2");       //获取市列表
            //遍历省集合
            for(AreaOutData province : provinceList) {
                ProvinceCityOutData provinceCity = new ProvinceCityOutData();
                //获取省集合中每个元素的地区编号
                String areaId = province.getAreaId();
                //对应的省下市集合
                List<AreaOutData> cityChildList = cityList.stream().filter(item -> areaId.equals(item.getParentId())).collect(Collectors.toList());

                //组装参数
                provinceCity.setAreaId(areaId);
                provinceCity.setAreaName(province.getAreaName());
                provinceCity.setLevel(province.getLevel());
                provinceCity.setParentId(province.getParentId());
                provinceCity.setCityChildList(cityChildList);
                provinceCityList.add(provinceCity);
            }
        }
        return provinceCityList;
    }


    @Override
    public JsonResult getClientListByProvCity(ProvCityInData inData) {
        List<Map<String, String>> clientList = franchiseeMapper.getClientListByProvCity(inData);
        return JsonResult.success(clientList, "success");
    }


}
