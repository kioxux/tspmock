//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ClearBoxService;
import com.gys.business.service.data.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
public class ClearBoxServiceImpl implements ClearBoxService {
    @Autowired
    private GaiaSdCleandrawerHMapper gaiaSdCleandrawerHMapper;
    @Autowired
    private GaiaSdCleandrawerDMapper gaiaSdCleandrawerDMapper;
    @Autowired
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;
    @Autowired
    private GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Autowired
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    public ClearBoxServiceImpl() {
    }

    public GetOneClearBoxOutData getOneClearBox(ClearBoxInfoOutData inData) {
        GaiaSdCleandrawerH sdCleandrawerH = new GaiaSdCleandrawerH();
        sdCleandrawerH.setClientId(inData.getClientId());
        sdCleandrawerH.setGschVoucherId(inData.getGschVoucherId());
        sdCleandrawerH = (GaiaSdCleandrawerH)this.gaiaSdCleandrawerHMapper.selectOne(sdCleandrawerH);
        if ("1".equals(sdCleandrawerH.getGschStatus())) {
            sdCleandrawerH.setGschStatus("已清斗");
        } else if ("0".equals(sdCleandrawerH.getGschStatus())) {
            sdCleandrawerH.setGschStatus("未清斗");
        }

        GetOneClearBoxOutData getOneClearBoxOutData = new GetOneClearBoxOutData();
        getOneClearBoxOutData.setGaiaSdCleandrawerH(sdCleandrawerH);
        GaiaSdCleandrawerD sdCleandrawerD = new GaiaSdCleandrawerD();
        sdCleandrawerD.setClientId(inData.getClientId());
        sdCleandrawerD.setGscdVoucherId(inData.getGschVoucherId());
        List<GaiaSdCleandrawerD> sdCleandrawerDList = this.gaiaSdCleandrawerDMapper.select(sdCleandrawerD);
        List<GaiaSdCleandrawerDOutData> gaiaSdCleandrawerDOutDataList = new ArrayList();

        for(int i = 0; i < sdCleandrawerDList.size(); ++i) {
            GaiaSdCleandrawerD item = sdCleandrawerDList.get(i);
            if ("未清斗".equals(sdCleandrawerH.getGschStatus())) {
                GaiaSdStockBatch gaiaSdStockBatch = new GaiaSdStockBatch();
                gaiaSdStockBatch.setClientId(inData.getClientId());
                gaiaSdStockBatch.setGssbBatchNo(item.getGscdBatchNo());
                gaiaSdStockBatch.setGssbBatch(item.getGscdBatch());
                gaiaSdStockBatch.setGssbBrId(inData.getBrId());
                gaiaSdStockBatch.setGssbProId(item.getGscdProId());
                gaiaSdStockBatch = this.gaiaSdStockBatchMapper.selectOne(gaiaSdStockBatch);
                if (ObjectUtil.isNotEmpty(gaiaSdStockBatch)) {
                    item.setGscdQty(gaiaSdStockBatch.getGssbQty());
                } else {
                    item.setGscdQty("");
                }
            }

            GaiaSdCleandrawerDOutData outData = new GaiaSdCleandrawerDOutData();
            BeanUtils.copyProperties(item, outData);
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            gaiaProductBusiness.setClient(inData.getClientId());
            gaiaProductBusiness.setProSite(inData.getBrId());
            gaiaProductBusiness.setProSelfCode(item.getGscdProId());
            gaiaProductBusiness.setProStatus("0");
            gaiaProductBusiness = (GaiaProductBusiness)this.gaiaProductBusinessMapper.selectOne(gaiaProductBusiness);
            outData.setGscdProName(gaiaProductBusiness.getProName());
            outData.setProPlace(gaiaProductBusiness.getProPlace());
            outData.setProForm(gaiaProductBusiness.getProForm());
            outData.setProFactoryName(gaiaProductBusiness.getProFactoryName());
            outData.setProSpecs(gaiaProductBusiness.getProSpecs());
            outData.setProUnit(gaiaProductBusiness.getProUnit());
            GaiaSdProductPrice gaiaSdProductPrice = new GaiaSdProductPrice();
            gaiaSdProductPrice.setClientId(inData.getClientId());
            gaiaSdProductPrice.setGsppProId(item.getGscdProId());
            gaiaSdProductPrice.setGsppBrId(inData.getBrId());
            gaiaSdProductPrice = (GaiaSdProductPrice)this.gaiaSdProductPriceMapper.selectOne(gaiaSdProductPrice);
            DecimalFormat decimalFormat = new DecimalFormat("##########.##########");
            outData.setNormalPrice(decimalFormat.format(gaiaSdProductPrice.getGsppPriceNormal()));
            outData.setGsadStockStatus("正常");
            outData.setProRegisterNo(gaiaProductBusiness.getProRegisterNo());
            outData.setQtySour(outData.getGscdQty());
            gaiaSdCleandrawerDOutDataList.add(outData);
        }

        getOneClearBoxOutData.setGaiaSdCleandrawerDList(gaiaSdCleandrawerDOutDataList);
        return getOneClearBoxOutData;
    }

    public List<ClearBoxInfoOutData> getClearBox(ClearBoxInfoOutData inData) {
        List<ClearBoxInfoOutData> outData = this.gaiaSdCleandrawerHMapper.getClearBox(inData);

        for(int i = 0; i < outData.size(); ++i) {
            ((ClearBoxInfoOutData)outData.get(i)).setIndex(String.valueOf(i + 1));
        }

        return outData;
    }

    @Transactional
    public void examine(GetOneClearBoxOutData inData) {
        GaiaSdCleandrawerH gaiaSdCleandrawerH = inData.getGaiaSdCleandrawerH();
        gaiaSdCleandrawerH.setClientId(inData.getClientId());
        GaiaSdCleandrawerH gaiaSdCleandrawerHOld = (GaiaSdCleandrawerH)this.gaiaSdCleandrawerHMapper.selectByPrimaryKey(gaiaSdCleandrawerH);
        gaiaSdCleandrawerHOld.setGschEmp(gaiaSdCleandrawerH.getGschEmp());
        gaiaSdCleandrawerHOld.setGschStatus("1");
        gaiaSdCleandrawerHOld.setGschDate(DateUtil.format(new Date(), "yyyyMMdd"));
        this.gaiaSdCleandrawerHMapper.updateByPrimaryKeySelective(gaiaSdCleandrawerHOld);
        List<GaiaSdCleandrawerDOutData> gaiaSdCleandrawerDList = inData.getGaiaSdCleandrawerDList();
        Iterator var5 = gaiaSdCleandrawerDList.iterator();

        while(var5.hasNext()) {
            GaiaSdCleandrawerDOutData item = (GaiaSdCleandrawerDOutData)var5.next();
            item.setClientId(inData.getClientId());
            item.setGscdVoucherId(gaiaSdCleandrawerHOld.getGschVoucherId());
            item.setGscdStatus("1");
            item.setGscdDate(DateUtil.format(new Date(), "yyyyMMdd"));
            this.gaiaSdCleandrawerDMapper.updateExa(item);
        }

    }

    @Transactional
    public void addClearBox(AddClearBoxOutData inData) {
        GaiaSdCleandrawerH gaiaSdCleandrawerH = new GaiaSdCleandrawerH();
        gaiaSdCleandrawerH.setClientId(inData.getClientId());
        String gschVoucherId = this.gaiaSdCleandrawerHMapper.selectMaxgschVoucherId();
        gaiaSdCleandrawerH.setGschVoucherId(gschVoucherId);
        gaiaSdCleandrawerH.setGschBrId(inData.getBrId());
        gaiaSdCleandrawerH.setGschDate(DateUtil.format(new Date(), "yyyyMMdd"));
        gaiaSdCleandrawerH.setGschStatus("1");
        gaiaSdCleandrawerH.setGschEmp(inData.getEmp());
        gaiaSdCleandrawerH.setGschRemaks("人工增加");
        this.gaiaSdCleandrawerHMapper.insert(gaiaSdCleandrawerH);

        for(int i = 0; i < inData.getClearBoxDetailOutDataList().size(); ++i) {
            ClearBoxDetailOutData item = (ClearBoxDetailOutData)inData.getClearBoxDetailOutDataList().get(i);
            GaiaSdCleandrawerD gaiaSdCleandrawerD = new GaiaSdCleandrawerD();
            BeanUtils.copyProperties(item, gaiaSdCleandrawerD);
            gaiaSdCleandrawerD.setClientId(inData.getClientId());
            gaiaSdCleandrawerD.setGscdVoucherId(gschVoucherId);
            gaiaSdCleandrawerD.setGscdDate(DateUtil.format(new Date(), "yyyyMMdd"));
            gaiaSdCleandrawerD.setGscdStatus("1");
            gaiaSdCleandrawerD.setGscdSerial(String.valueOf(i + 1));
            this.gaiaSdCleandrawerDMapper.insert(gaiaSdCleandrawerD);
        }

    }
}
