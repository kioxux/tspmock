package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel
public class MonthPushMoneyByStoreOutData {
    private String client;
    @ApiModelProperty(value = "提成月份")
    private String suitMonth;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "门店名称")
    private String stoName;
    @ApiModelProperty(value = "销售天数")
    private BigDecimal days;
    @ApiModelProperty(value = "实收金额")
    private BigDecimal amt;
    @ApiModelProperty(value = "销售提成")
    private BigDecimal deductionWageSales;
    @ApiModelProperty(value = "单品提成")
    private BigDecimal deductionWagePro;
    @ApiModelProperty(value = "提成合计")
    private BigDecimal deductionWage;
    @ApiModelProperty(value = "销售提成占比")
    private BigDecimal deductionWageSalesRate;
    @ApiModelProperty(value = "单品提成占比")
    private BigDecimal deductionWageProRade;
    @ApiModelProperty(value = "提成占比")
    private BigDecimal deductionWageRate;

}
