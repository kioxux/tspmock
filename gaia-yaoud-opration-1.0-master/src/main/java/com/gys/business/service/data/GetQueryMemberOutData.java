package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdMemberBasic;
import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class GetQueryMemberOutData implements Serializable {
    private static final long serialVersionUID = 7172488398704557418L;

    /**
     * 唯一id
     */
    private String memberId;

    @ApiModelProperty(value = "会员卡号")
    private String cardNum;

    @ApiModelProperty(value = "会员姓名")
    private String Name;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "卡类型折率")
    private String dis;

    @ApiModelProperty(value = "卡类型折率2")
    private String dis2;

    @ApiModelProperty(value = "消费金额")
    private String amtSale;

    @ApiModelProperty(value = "积分数值")
    private String integerSale;

    @ApiModelProperty(value = "卡类型编号 : 1 :钻石 / 2 :白金/ 3 :金 / 4 :银/5 :普通")
    private String typeId;

    @ApiModelProperty(value = "卡类型名称")
    private String Type;

    @ApiModelProperty(value = "当前积分")
    private String point;

    @ApiModelProperty(value = "药店名称")
    private String company;

    @ApiModelProperty(value = "年纪")
    private String age;

    @ApiModelProperty(value = "男  :1 / 女 : 0")
    private String sex;

    @ApiModelProperty(value = "身份证")
    private String idCard;

    @ApiModelProperty(value = "电子券")
    private String elNum;

    @ApiModelProperty(value = "出生日期")
    private String dateOfBirth;

    private String mobile;

    private String clientId;

    private String brId;

    private String cardIdOrPhoneOrMobile;

    private String nameOrPinYin;

    private String identityId;

    private String cardBalance;//储值余额

    private String store;//办卡门店

    private String universal;
    /**
     * 地址
     */
    private String address;

    /**
     * 激活状态
     */
    private String gsmbcState;
    /**
     * 激活人工号
     */
    private String gsmbcJhUser;

    /**
     * 激活日期
     */
    private String gsmbcJhDate;

    /**
     * 激活时间
     */
    private String gsmbcJhTime;


    private GaiaSdMemberBasic basic;

    private GaiaSdMemberCardData cardData;
}
