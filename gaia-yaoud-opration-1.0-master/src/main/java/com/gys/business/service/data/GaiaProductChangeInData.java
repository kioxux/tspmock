package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value="com-gys-business-service-data-GaiaProductChangeInData")
@Data
public class GaiaProductChangeInData implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 唯一值
    */
    @ApiModelProperty(value="唯一值")
    private Integer id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 地点
    */
    @ApiModelProperty(value="地点")
    private String proSite;

    /**
    * 商品自编码
    */
    @ApiModelProperty(value="商品自编码")
    private String proSelfCode;

    /**
    * 通用名称
    */
    @ApiModelProperty(value="通用名称")
    private String proCommonname;

    /**
    * 规格
    */
    @ApiModelProperty(value="规格")
    private String proSpecs;

    /**
    * 生产厂家
    */
    @ApiModelProperty(value="生产厂家")
    private String proFactoryName;

    /**
    * 批准文号
    */
    @ApiModelProperty(value="批准文号")
    private String proRegisterNo;

    /**
    * 变更字段
    */
    @ApiModelProperty(value="变更字段")
    private String proChangeField;

    /**
    * 变更前值
    */
    @ApiModelProperty(value="变更前值")
    private String proChangeFrom;

    /**
    * 变更后值
    */
    @ApiModelProperty(value="变更后值")
    private String proChangeTo;

    /**
    * 变更人
    */
    @ApiModelProperty(value="变更人")
    private String proChangeUser;

    /**
    * 变更日期
    */
    @ApiModelProperty(value="变更日期")
    private String proChangeDate;

    /**
    * 变更时间
    */
    @ApiModelProperty(value="变更时间")
    private String proChangeTime;

    /**
    * 工作流编号
    */
    @ApiModelProperty(value="工作流编号")
    private String proFlowNo;

    /**
    * 是否审批结束
    */
    @ApiModelProperty(value="是否审批结束")
    private String proSfsp;

    /**
    * 审批状态
    */
    @ApiModelProperty(value="审批状态")
    private String proSfty;

    /**
    * 备注
    */
    @ApiModelProperty(value="备注")
    private String proRemarks;



}