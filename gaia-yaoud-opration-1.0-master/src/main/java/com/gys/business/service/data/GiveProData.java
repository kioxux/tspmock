//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GiveProData {
    private String giftName;
    private String proFactoryName;
    private String proSpecs;
    private String proPym;
    private String giftNormalPrice;
    private String barcode;
    private String giftStock;

    public GiveProData() {
    }

    public String getGiftName() {
        return this.giftName;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProPym() {
        return this.proPym;
    }

    public String getGiftNormalPrice() {
        return this.giftNormalPrice;
    }

    public String getBarcode() {
        return this.barcode;
    }

    public String getGiftStock() {
        return this.giftStock;
    }

    public void setGiftName(final String giftName) {
        this.giftName = giftName;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProPym(final String proPym) {
        this.proPym = proPym;
    }

    public void setGiftNormalPrice(final String giftNormalPrice) {
        this.giftNormalPrice = giftNormalPrice;
    }

    public void setBarcode(final String barcode) {
        this.barcode = barcode;
    }

    public void setGiftStock(final String giftStock) {
        this.giftStock = giftStock;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GiveProData)) {
            return false;
        } else {
            GiveProData other = (GiveProData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$giftName = this.getGiftName();
                    Object other$giftName = other.getGiftName();
                    if (this$giftName == null) {
                        if (other$giftName == null) {
                            break label95;
                        }
                    } else if (this$giftName.equals(other$giftName)) {
                        break label95;
                    }

                    return false;
                }

                Object this$proFactoryName = this.getProFactoryName();
                Object other$proFactoryName = other.getProFactoryName();
                if (this$proFactoryName == null) {
                    if (other$proFactoryName != null) {
                        return false;
                    }
                } else if (!this$proFactoryName.equals(other$proFactoryName)) {
                    return false;
                }

                Object this$proSpecs = this.getProSpecs();
                Object other$proSpecs = other.getProSpecs();
                if (this$proSpecs == null) {
                    if (other$proSpecs != null) {
                        return false;
                    }
                } else if (!this$proSpecs.equals(other$proSpecs)) {
                    return false;
                }

                label74: {
                    Object this$proPym = this.getProPym();
                    Object other$proPym = other.getProPym();
                    if (this$proPym == null) {
                        if (other$proPym == null) {
                            break label74;
                        }
                    } else if (this$proPym.equals(other$proPym)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$giftNormalPrice = this.getGiftNormalPrice();
                    Object other$giftNormalPrice = other.getGiftNormalPrice();
                    if (this$giftNormalPrice == null) {
                        if (other$giftNormalPrice == null) {
                            break label67;
                        }
                    } else if (this$giftNormalPrice.equals(other$giftNormalPrice)) {
                        break label67;
                    }

                    return false;
                }

                Object this$barcode = this.getBarcode();
                Object other$barcode = other.getBarcode();
                if (this$barcode == null) {
                    if (other$barcode != null) {
                        return false;
                    }
                } else if (!this$barcode.equals(other$barcode)) {
                    return false;
                }

                Object this$giftStock = this.getGiftStock();
                Object other$giftStock = other.getGiftStock();
                if (this$giftStock == null) {
                    if (other$giftStock != null) {
                        return false;
                    }
                } else if (!this$giftStock.equals(other$giftStock)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GiveProData;
    }

    public int hashCode() {

        int result = 1;
        Object $giftName = this.getGiftName();
        result = result * 59 + ($giftName == null ? 43 : $giftName.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proPym = this.getProPym();
        result = result * 59 + ($proPym == null ? 43 : $proPym.hashCode());
        Object $giftNormalPrice = this.getGiftNormalPrice();
        result = result * 59 + ($giftNormalPrice == null ? 43 : $giftNormalPrice.hashCode());
        Object $barcode = this.getBarcode();
        result = result * 59 + ($barcode == null ? 43 : $barcode.hashCode());
        Object $giftStock = this.getGiftStock();
        result = result * 59 + ($giftStock == null ? 43 : $giftStock.hashCode());
        return result;
    }

    public String toString() {
        return "GiveProData(giftName=" + this.getGiftName() + ", proFactoryName=" + this.getProFactoryName() + ", proSpecs=" + this.getProSpecs() + ", proPym=" + this.getProPym() + ", giftNormalPrice=" + this.getGiftNormalPrice() + ", barcode=" + this.getBarcode() + ", giftStock=" + this.getGiftStock() + ")";
    }
}
