package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaUserTrainingRecordMapper;
import com.gys.business.mapper.entity.GaiaUserTrainingRecord;
import com.gys.business.service.UserTrainingRecordService;
import com.gys.business.service.data.UserTrainingRecordInData;
import com.gys.business.service.data.UserTrainingRecordOutData;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2022/1/3 20:45
 */
@Slf4j
@Service
public class UserTrainingRecordServiceImpl implements UserTrainingRecordService {
    @Resource
    private GaiaUserTrainingRecordMapper userTrainingRecordMapper;
    @Resource
    public CosUtils cosUtils;

    @Override
    public void saveUserTrainingRecord(List<GaiaUserTrainingRecord> recordList, String client, String user) {
        for (GaiaUserTrainingRecord userTrainingRecord : recordList){
            GaiaUserTrainingRecord trainingRecord = new GaiaUserTrainingRecord();
            if (StringUtils.isBlank(userTrainingRecord.getGutrVoucherId())){
                String voucherId = userTrainingRecordMapper.getNextGutrVoucherId(client);
                BeanUtils.copyProperties(userTrainingRecord,trainingRecord);
                trainingRecord.setClient(client);
                trainingRecord.setGutrVoucherId(voucherId);
                trainingRecord.setGutrCreate(user);
                trainingRecord.setGutrCreateDate(new Date());
                userTrainingRecordMapper.insertUserTrainingRecord(trainingRecord);
            }else {
                trainingRecord.setGutrVoucherId(userTrainingRecord.getGutrVoucherId());
                Long id = userTrainingRecordMapper.getIdByClientAndGutrVoucherId(client,userTrainingRecord.getGutrVoucherId());
                BeanUtils.copyProperties(userTrainingRecord,trainingRecord);
                trainingRecord.setId(id);
                trainingRecord.setGutrUpdate(user);
                trainingRecord.setGutrUpdateDate(new Date());
                userTrainingRecordMapper.updateUserTrainingRecord(trainingRecord);
            }
        }
    }

    @Override
    public List<UserTrainingRecordOutData> getUserTrainingRecordList(UserTrainingRecordInData inData) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            if (StringUtils.isNotBlank(inData.getGutrDateStart())){
                Date startDate;
                String str = inData.getGutrDateStart()+"000000";
                startDate = simpleDateFormat.parse(str);
                inData.setStartDate(startDate);
            }
            if (StringUtils.isNotBlank(inData.getGutrDateEnd())){
                Date endDate;
                String str = inData.getGutrDateEnd()+"235959";
                endDate = simpleDateFormat.parse(str);
                inData.setEndDate(endDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return userTrainingRecordMapper.getUserTrainingRecordList(inData);
    }

    @Override
    public void deleteUserTrainingRecord(List<GaiaUserTrainingRecord> recordList, String client) {
        for (GaiaUserTrainingRecord record : recordList){
            Long id = userTrainingRecordMapper.getIdByClientAndGutrVoucherId(client,record.getGutrVoucherId());
            userTrainingRecordMapper.deleteUserTrainingRecord(id);
        }
    }

    @Override
    public Result exportUserTrainingRecordList(UserTrainingRecordInData inData) {
        Result result;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            if (StringUtils.isNotBlank(inData.getGutrDateStart())){
                Date startDate;
                String str = inData.getGutrDateStart()+"000000";
                startDate = simpleDateFormat.parse(str);
                inData.setStartDate(startDate);
            }
            if (StringUtils.isNotBlank(inData.getGutrDateEnd())){
                Date endDate;
                String str = inData.getGutrDateEnd()+"235959";
                endDate = simpleDateFormat.parse(str);
                inData.setEndDate(endDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<UserTrainingRecordOutData> list = userTrainingRecordMapper.getUserTrainingRecordList(inData);
        String name = "员工培训计划导出";
        if (list.size() > 0){
            CsvFileInfo csvFileInfo = CsvClient.getCsvByte(list,name, Collections.singletonList((short) 1));
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                assert csvFileInfo != null;
                outputStream.write(csvFileInfo.getFileContent());
                result = cosUtils.uploadFile(outputStream, csvFileInfo.getFileName());
            } catch (IOException e){
                log.error("导出文件失败:{}",e.getMessage(), e);
                throw new BusinessException("导出文件失败！");
            } finally {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    log.error("关闭流异常：{}",e.getMessage(),e);
                    throw new BusinessException("关闭流异常！");
                }
            }
            return result;
        }else{
            throw new BusinessException("提示：数据为空！");
        }
    }
}
