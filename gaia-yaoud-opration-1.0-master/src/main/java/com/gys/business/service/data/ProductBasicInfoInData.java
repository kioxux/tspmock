package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProductBasicInfoInData implements Serializable {

    @ApiModelProperty(value = "查询内容")
    private String content;

    @ApiModelProperty(value = "匹配类型 0 模糊匹配 1 精确匹配")
    private String mateType;

    @ApiModelProperty(value = "药德商品编码数组")
    private String[] proCodeArr;

    @ApiModelProperty(value = "起始页")
    private int pageNum;

    @ApiModelProperty(value = "显示条数")
    private int pageSize;
}
