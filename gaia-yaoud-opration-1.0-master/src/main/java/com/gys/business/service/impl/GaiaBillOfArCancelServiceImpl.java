package com.gys.business.service.impl;

import com.gys.business.mapper.entity.GaiaBillOfArCancel;
import com.gys.business.mapper.GaiaBillOfArCancelMapper;
import com.gys.business.service.GaiaBillOfArCancelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 应收核销清单表(GaiaBillOfArCancel)表服务实现类
 *
 * @author makejava
 * @since 2021-09-18 15:20:50
 */
@Service("gaiaBillOfArCancelService")
public class GaiaBillOfArCancelServiceImpl implements GaiaBillOfArCancelService {
    @Resource
    private GaiaBillOfArCancelMapper gaiaBillOfArCancelDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public GaiaBillOfArCancel queryById(Long id) {
        return this.gaiaBillOfArCancelDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<GaiaBillOfArCancel> queryAllByLimit(int offset, int limit) {
        return this.gaiaBillOfArCancelDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param gaiaBillOfArCancel 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaBillOfArCancel insert(GaiaBillOfArCancel gaiaBillOfArCancel) {
        this.gaiaBillOfArCancelDao.insert(gaiaBillOfArCancel);
        return gaiaBillOfArCancel;
    }

    /**
     * 修改数据
     *
     * @param gaiaBillOfArCancel 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaBillOfArCancel update(GaiaBillOfArCancel gaiaBillOfArCancel) {
        this.gaiaBillOfArCancelDao.update(gaiaBillOfArCancel);
        return this.queryById(gaiaBillOfArCancel.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.gaiaBillOfArCancelDao.deleteById(id) > 0;
    }
}
