package com.gys.business.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.gys.business.controller.app.form.DeleteForm;
import com.gys.business.controller.app.form.OperateForm;
import com.gys.business.controller.app.form.OutStockForm;
import com.gys.business.controller.app.form.PushForm;
import com.gys.business.controller.app.vo.CompanyOutStockVO;
import com.gys.business.controller.app.vo.DcPushVO;
import com.gys.business.controller.app.vo.ProductInfoVO;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CompanyOutOfStockService;
import com.gys.business.service.GaiaDcReplenishHService;
import com.gys.business.service.OutOfStockPushRecordService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.CategoryModelForm;
import com.gys.common.data.JsonResult;
import com.gys.common.enums.PushTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.feign.ReportService;
import com.gys.util.CommonUtil;
import com.gys.util.DateUtil;
import com.gys.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/9 18:16
 **/
@Slf4j
@Service("companyOutOfStockService")
public class CompanyOutOfStockServiceImpl implements CompanyOutOfStockService {
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Autowired
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;
    @Autowired
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Autowired
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;
    @Autowired
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;
    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Autowired
    private GaiaSdOutOfStockPushRecordMapper outOfStockPushRecordMapper;
    @Resource
    private GaiaDcReplenishHService gaiaDcReplenishHService;
    @Resource
    private ReportService reportService;
    @Resource
    private GaiaWmKuCunMapper gaiaWmKuCunMapper;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private OutOfStockPushRecordService outOfStockPushRecordService;
    @Autowired
    private GaiaAuthconfiDataMapper authconfiDataMapper;
    @Autowired
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Resource
    private DcOutStockDMapper dcOutStockDMapper;
    @Resource
    private DcOutStockHMapper dcOutStockHMapper;
    @Resource
    private DcOutStockStoreMapper dcOutStockStoreMapper;
    @Resource
    private DcOutStockStoreHMapper dcOutStockStoreHMapper;
    @Autowired
    private GaiaSdStoresGroupMapper storesGroupMapper;

    @Override
    public CompanyOutStockVO getOutOfStockList(OutStockForm outStockForm) {
        CompanyOutStockVO companyOutStockVO = null;
        try {
            companyOutStockVO = new CompanyOutStockVO();
            //获取缺断货商品列表
            DcOutStockH dcOutStockH = dcOutStockHMapper.getLatestH(outStockForm.getClient());
            if (dcOutStockH == null) {
                return companyOutStockVO;
            }
            companyOutStockVO.setVoucherId(dcOutStockH.getVoucherId());
            companyOutStockVO.setProductNum(dcOutStockH.getProNum());
            companyOutStockVO.setMonthSaleAmount(dcOutStockH.getMonthSaleAmount().setScale(2, RoundingMode.HALF_UP));
            companyOutStockVO.setMonthProfitAmount(dcOutStockH.getMonthProfitAmount().setScale(2, RoundingMode.HALF_UP));
            DcOutStockD cond = new DcOutStockD();
            cond.setClient(outStockForm.getClient());
            cond.setVoucherId(dcOutStockH.getVoucherId());
            //排序条件处理
            cond.setOrderBy(getOrderByCond(outStockForm));
            cond.setQueryNum(outStockForm.getQueryNum());
            List<DcOutStockD> list = dcOutStockDMapper.findList(cond);
            transformProductList(companyOutStockVO, list);
            threadPoolTaskExecutor.execute(() -> outOfStockPushRecordService.updatePushRecord(outStockForm.getClient(), outStockForm.getUserId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return companyOutStockVO;
    }

    private String getOrderByCond(OutStockForm outStockForm) {
        StringBuilder sb = new StringBuilder();
        sb.append(" order by ");
        switch (outStockForm.getSortCond()) {
            case 2:
                sb.append("MONTH_PROFIT_AMOUNT ");
                if (outStockForm.getSortType() == 1) {
                    sb.append(" desc ");
                } else {
                    sb.append(" asc ");
                }
                break;
            case 3:
                sb.append(" MONTH_SALE_NUM ");
                if (outStockForm.getSortType() == 1) {
                    sb.append(" desc ");
                } else {
                    sb.append(" asc ");
                }
                break;
            case 4:
                sb.append(" MODEL_SORT ");
                if (outStockForm.getSortType() == 2) {
                    sb.append(" desc ");
                } else {
                    sb.append(" asc ");
                }
                break;
            default:
                sb.append(" MONTH_SALE_AMOUNT ");
                if (outStockForm.getSortType() == 1) {
                    sb.append(" desc ");
                } else {
                    sb.append(" asc ");
                }
                break;
        }

        return sb.toString();
    }

    private void transformProductList(CompanyOutStockVO companyOutStockVO, List<DcOutStockD> list) {
        List<ProductInfoVO> productList = new ArrayList<>();
        for (DcOutStockD stockD : list) {
            ProductInfoVO productInfoVO = new ProductInfoVO();
            productInfoVO.setProSelfCode(stockD.getProSelfCode());
            productInfoVO.setProSelfName(stockD.getProCommonName());
            productInfoVO.setProFactoryName(stockD.getFactoryName());
            productInfoVO.setProSpec(stockD.getProSpecs());
            productInfoVO.setSortNum(stockD.getModelSort());
            productInfoVO.setMonthSaleNum(stockD.getMonthSaleNum().setScale(0, RoundingMode.HALF_UP));
            productInfoVO.setMonthProfitAmount(stockD.getMonthProfitAmount().setScale(2, RoundingMode.HALF_UP));
            productInfoVO.setMonthSaleAmount(stockD.getMonthSaleAmount().setScale(2, RoundingMode.HALF_UP));
            productInfoVO.setStatus(stockD.getStatus());
            productInfoVO.setVoucherId(stockD.getVoucherId());
            productList.add(productInfoVO);
        }
        companyOutStockVO.setList(productList);
    }

    @Override
    public List<ProductRank> getCategoryModelList(String client, Integer analyseDay) {
        CategoryModelForm categoryModelForm = new CategoryModelForm();
        categoryModelForm.setEndDate(DateUtil.getCurrentDate());
        categoryModelForm.setAnalyseDay(String.valueOf(analyseDay));
        categoryModelForm.setIsProfit("1");
        categoryModelForm.setDimension("3");
        categoryModelForm.setLimitNum(1000);
        categoryModelForm.setClientIds(Lists.newArrayList(client));
        String response = reportService.getCategoryModelRank(categoryModelForm);
        JSONObject result = JSON.parseObject(response);
        List<ProductRank> list = new ArrayList<>();
        Map<String, Integer> proMap = Maps.newHashMap();
        if (result.getInteger("code") == 0) {
            JSONArray data = result.getJSONArray("data");
            for (int i = 0; i < data.size(); i++) {
                JSONObject jsonObject = data.getJSONObject(i);
                String proCompClass = jsonObject.getString("proCompClass");
                if (StringUtil.isNotEmpty(proCompClass) && proCompClass.startsWith("A")) {
                    proMap.put(jsonObject.getString("proCode"), jsonObject.getInteger("comprehensiveRank"));
                }
            }
        }
        if (proMap.size() == 0) {
            return list;
        }
        //药德编码转为商品自编码
        List<GaiaProductBusiness> productList = gaiaProductBusinessMapper.getListByProCode(client, proMap);
        for (GaiaProductBusiness productBusiness : productList) {
            ProductRank rank = new ProductRank();
            rank.setClient(productBusiness.getClient());
            rank.setProSelfCode(productBusiness.getProSelfCode());
            rank.setRankNum(proMap.get(productBusiness.getProCode()));
            list.add(rank);
        }
        return list;
    }

    @Override
    public void deleteProduct(DeleteForm deleteForm) {
        //更新缺断货记录
        DcOutStockD cond = new DcOutStockD();
        cond.setClient(deleteForm.getClient());
        cond.setVoucherId(deleteForm.getVoucherId());
        cond.setProSelfCode(deleteForm.getProSelfCode());
        DcOutStockD dcOutStockD = dcOutStockDMapper.getUnique(cond);
        if (dcOutStockD == null) {
            return;
        }
        if (dcOutStockD.getStatus() > 0) {
            throw new BusinessException("当前状态不可淘汰");
        }
        dcOutStockD.setStatus(2);
        dcOutStockD.setUpdateTime(new Date());
        dcOutStockD.setUpdateUser(String.format("%s-%s", deleteForm.getLoginName(), deleteForm.getUserId()));
        dcOutStockDMapper.update(dcOutStockD);
        //更新商品定位
        GaiaProductBusiness productCond = new GaiaProductBusiness();
        productCond.setProSelfCode(deleteForm.getProSelfCode());
        productCond.setClient(deleteForm.getClient());
        List<GaiaProductBusiness> list = gaiaProductBusinessMapper.findList(productCond);
        for (GaiaProductBusiness productBusiness : list) {
            productBusiness.setProPosition("T");
            gaiaProductBusinessMapper.updatePosition(productBusiness);
        }
    }

    @Override
    public CompanyOutOfStockInfoOutData getDcStoreDetailList(OperateForm operateForm) {
        CompanyOutOfStockInfoOutData outData = new CompanyOutOfStockInfoOutData();
        //门店汇总
        DcOutStockStoreH cond = new DcOutStockStoreH();
        cond.setClient(operateForm.getClient());
        cond.setVoucherId(operateForm.getVoucherId());
        DcOutStockStoreH stockStoreH = dcOutStockStoreHMapper.getUnique(cond);
        if (stockStoreH == null) {
            log.warn("<公司级缺断货><门店明细><未获取门店明细主数据:{}{}>", operateForm.getClient(), operateForm.getVoucherId());
            return outData;
        }
        outData.setStoreCount(stockStoreH.getStoreNum());
        outData.setOutOfStockCount(stockStoreH.getProductNum());
        outData.setAvgSaleAmt(stockStoreH.getMonthSaleAmount().setScale(2, RoundingMode.HALF_UP));
        outData.setAvgProfitAmt(stockStoreH.getMonthProfitAmount().setScale(2, RoundingMode.HALF_UP));

        //门店明细
        List<CompanyOutOfStockDetailOutData> detailList = transformOutStockStoreDetail(operateForm);
        outData.setOutOfStockDetail(detailList);
        return outData;
    }

    private List<CompanyOutOfStockDetailOutData> transformOutStockStoreDetail(OperateForm operateForm) {
        List<CompanyOutOfStockDetailOutData> detailList = new ArrayList<>();
        DcOutStockStore cond = new DcOutStockStore();
        cond.setClient(operateForm.getClient());
        cond.setVoucherId(operateForm.getVoucherId());
        cond.setQueryNum(operateForm.getQueryNum());
        cond.setSortMode(operateForm.getSortMode());
        cond.setOrderBy(getStoreDetailSort(operateForm));
        List<DcOutStockStore> storeList = dcOutStockStoreMapper.findList(cond);
        if (storeList == null || storeList.size() == 0) {
            return detailList;
        }
        for (DcOutStockStore stockStore : storeList) {
            CompanyOutOfStockDetailOutData detailOutData = new CompanyOutOfStockDetailOutData();
            detailOutData.setBrId(stockStore.getStoreId());
            detailOutData.setBrName(stockStore.getStoreName());
            detailOutData.setOutOfStockCount(stockStore.getProNum());
            detailOutData.setAvgSaleAmt(stockStore.getMonthSaleAmount().setScale(2, RoundingMode.HALF_UP));
            detailOutData.setAvgProfitAmt(stockStore.getMonthProfitAmount().setScale(2, RoundingMode.HALF_UP));
            detailList.add(detailOutData);
        }
        return detailList;
    }

    private String getStoreDetailSort(OperateForm operateForm) {
        StringBuilder sb = new StringBuilder();
        if (operateForm.getSortCond() == 2) {
            sb.append(" order by MONTH_PROFIT_AMOUNT");
            //按毛利额排序
            if (operateForm.getSortType() == 1) {
                //倒序
                sb.append(" desc ");
            }
        } else if (operateForm.getSortCond() == 1) {
            //按照销售额排序
            sb.append(" order by MONTH_SALE_AMOUNT");
            if (operateForm.getSortType() == 1) {
                //倒序
                sb.append(" desc ");
            }
        }
        return sb.toString();
    }

    @Override
    public DcPushVO push(PushForm pushForm) {
        try {
            //保存仓库紧急补货需求
            gaiaDcReplenishHService.saveDcReplenish(pushForm);
            //更新缺断货记录状态
            updateDcOutStockStatus(pushForm);
            //推送消息
            savePushMessage(pushForm);
            //推送web端消息
            savePushWebMessage(pushForm);
            DcPushVO outOfStockAmt = dcOutStockDMapper.getPushAmount(pushForm);
            outOfStockAmt.setNum(pushForm.getProSelfCodeList().size());
            return outOfStockAmt;
        } catch (Exception e) {
            throw new BusinessException("推送缺断货信息异常", e);
        }
    }

    private void updateDcOutStockStatus(PushForm pushForm) {
        for (String proSelfCode : pushForm.getProSelfCodeList()) {
            DcOutStockD cond = new DcOutStockD();
            cond.setClient(pushForm.getClient());
            cond.setVoucherId(pushForm.getVoucherId());
            cond.setProSelfCode(proSelfCode);
            DcOutStockD unique = dcOutStockDMapper.getUnique(cond);
            unique.setStatus(1);
            unique.setUpdateTime(new Date());
            unique.setUpdateUser(String.format("%s-%s", pushForm.getLoginName(), pushForm.getUserId()));
            dcOutStockDMapper.update(unique);
        }
    }

    @Override
    public List<DcReplenishVO> replenishList(DcReplenishForm replenishForm) {
        List<DcReplenishVO> dcReplenishVOS = gaiaDcReplenishHService.replenishList(replenishForm);
        for (DcReplenishVO dcReplenishVO : dcReplenishVOS) {
            try {
                if (StringUtil.isNotEmpty(dcReplenishVO.getProcessTime()) && dcReplenishVO.getProcessTime().startsWith("1970")) {
                    dcReplenishVO.setProcessTime("");
                }
                Date createTime = DateUtils.parseDate(dcReplenishVO.getCreateTime(), "yyyy-MM-dd HH:mm:ss");
                Date before5Day = DateUtils.addDays(new Date(), -5);
                if (dcReplenishVO.getStatus() == 0 && createTime.compareTo(before5Day) >= 0) {
                    dcReplenishVO.setColorType(1);
                } else {
                    dcReplenishVO.setColorType(0);
                }
            } catch (ParseException e) {
                log.error("日期格式化错误：{}", e.getMessage(), e);
            }
        }
        return dcReplenishVOS;
    }

    @Override
    public List<DcReplenishDetailVO> replenishDetail(DcReplenishForm replenishForm) {
        GaiaDcReplenishH cond = new GaiaDcReplenishH();
        cond.setClient(replenishForm.getClient());
        cond.setVoucherId(replenishForm.getVoucherId());
        GaiaDcReplenishH dcReplenishH = gaiaDcReplenishHService.getUnique(cond);
        if (dcReplenishH == null) {
            throw new BusinessException(String.format("未获取到仓库紧急补货需求[%s]", replenishForm.getVoucherId()));
        }
        return gaiaDcReplenishHService.getReplenishDetail(replenishForm);
    }

    private void savePushMessage(PushForm pushForm) {
        List<GaiaAuthconfiData> dcInfoRoleList = null;
        //获取商采负责人
        if (PushTypeEnum.BUSINESS_MASTER.type.equals(pushForm.getPushType()))
            dcInfoRoleList = authconfiDataMapper.getDcInfoRole(pushForm.getClient(), "GAIA_MM_SCZG");
        else if (PushTypeEnum.OPERATION_MASTER.type.equals(pushForm.getPushType())) {
            //获取营运负责人
            dcInfoRoleList = authconfiDataMapper.getDcInfoRole(pushForm.getClient(), "GAIA_SD_YYGL");
        }

        //插入推送记录表
        if(ValidateUtil.isNotEmpty(dcInfoRoleList)) {
            for(GaiaAuthconfiData dcInfoRole : dcInfoRoleList) {
                String authConfigUser = dcInfoRole.getAuthconfiUser();
                GaiaSdOutOfStockPushRecord pushRecord = new GaiaSdOutOfStockPushRecord();
                pushRecord.setClient(dcInfoRole.getClient());
                pushRecord.setBrId("out_of_stock");     //门店，存入常量out_of_stock
                pushRecord.setType(2);
                pushRecord.setStatus(0);
                pushRecord.setPushEmp(pushForm.getUserId());
                pushRecord.setPushTime(new Date());
                pushRecord.setReadEmp(pushForm.getUserId());
                pushRecord.setReadTime(new Date(0));
                pushRecord.setReceiveEmp(authConfigUser);
                outOfStockPushRecordMapper.insert(pushRecord);
            }
        }
    }


    private void savePushWebMessage(PushForm pushForm) {
        String client = pushForm.getClient();

        //获取仓库列表
        List<String> dcCodeList = gaiaWmKuCunMapper.getDcCodeList(client);
        if (dcCodeList == null || dcCodeList.size() == 0) {
            throw new BusinessException("未获取到仓库信息");
        }
        GaiaSdMessage message = new GaiaSdMessage();
        message.setClient(client);
        message.setGsmId(dcCodeList.get(0));
        message.setGsmType("GAIA_MM_010314");
        message.setGsmPage("warehouseEmergencyReplenishment");
        message.setGsmArriveDate(CommonUtil.getyyyyMMdd());
        //检查当天是否已经有消息插入  如果已经存在消息 则不插入
        List<GaiaSdMessage> messageList = gaiaSdMessageMapper.selectListByNow(message);
        if(ValidateUtil.isNotEmpty(messageList)) {
            return;
        }
        message.setGsmVoucherId(gaiaSdMessageMapper.selectNextVoucherId(client, "company"));
        message.setGsmRemark("您有<font color=\"#FF0000\">仓库紧急需求</font>未处理，请处理。");
        message.setGsmFlag(CommonConstant.MESSAGE_FLAG_N);
        message.setGsmWarningDay(CommonConstant.MESSAGE_WARNING_DAY_180);
        message.setGsmArriveTime(CommonUtil.getHHmmss());
        message.setGsmPlatForm("WEB");
        message.setGsmDeleteFlag(CommonConstant.MESSAGE_DELETE_FLAG_0);
        gaiaSdMessageMapper.insertSelective(message);
    }


    @Override
    public JsonResult getValidProduct(ValidProductInData inData) {
        //效期商品信息 表头
        CompanyOutOfStockOutData expiryProInfo = gaiaSdStockBatchMapper.getCompanyExpiryProInfo(inData.getClientId());

        if(ValidateUtil.isEmpty(expiryProInfo)) {
            return JsonResult.success(null, "success");
        }
        //效期商品列表
        List<ValidProductListOutData> expiryProList = gaiaSdStockBatchMapper.getCompanyExpiryProList(inData);
        if(ValidateUtil.isNotEmpty(expiryProList)) {
            expiryProInfo.setProductList(expiryProList);
        }
        return JsonResult.success(expiryProInfo, "success");
    }


    @Override
    public JsonResult getValidProductChart(CompanyExpiryProChartInData inData) {
        //查询库存图表数据
        List<CompanyExpiryProChartOutData> companyExpiryProChartList = gaiaSdStockBatchMapper.getCompanyExpiryProChart(inData);

        List<CompanyExpiryProChartOutData> resultList = new ArrayList<>();
        if(ValidateUtil.isNotEmpty(companyExpiryProChartList)) {
            //用type进行分组 将仓库与门店分开
            Map<String, List<CompanyExpiryProChartOutData>> typeMap = companyExpiryProChartList.stream()
                    .collect(Collectors.groupingBy(item -> item.getType()));
            //判断是否有仓库存在
            if(typeMap.containsKey("dc")) {
                //将仓库对象放到list首个元素
                resultList.addAll(typeMap.get("dc"));
                resultList.addAll(typeMap.get("store"));
            } else {
                resultList.addAll(companyExpiryProChartList);
            }
        }
        return JsonResult.success(resultList, "success");
    }


    @Override
    public JsonResult getStockReport(CompanyStoreReportInData inData) {
        String type = inData.getType();
        if(ValidateUtil.isEmpty(type)) {
            type = "1";
            inData.setType(type);
        }
        //返回参数
        Map<String, Object> result = new HashMap<>(16);

        //开启线程1
        CompletableFuture<List<StoreStockReportOutData>> resultFir = CompletableFuture.supplyAsync(() ->{
            log.info("线程1开始");
            long start = System.currentTimeMillis();
            List<StoreStockReportOutData> clientStockList = gaiaSdStockBatchMapper.getClientStockList(inData.getClient(), inData.getType());
            log.info("线程1结束，共耗时：" + (System.currentTimeMillis() - start) + "毫秒");
            return clientStockList;
        });
        //开启线程2
        CompletableFuture<StockReportHeadOutData> resultSed = CompletableFuture.supplyAsync(() ->{
            log.info("线程2开始");
            long start = System.currentTimeMillis();
            StockReportHeadOutData totalHeadInfo = gaiaSdStockBatchMapper.getTotalHeadInfo(inData.getClient());
            log.info("线程2结束，共耗时：" + (System.currentTimeMillis() - start) + "毫秒");
            return totalHeadInfo;
        });
        //开启线程3
        CompletableFuture<List<SaleAmtByBigCodeOutData>> resultThr = CompletableFuture.supplyAsync(() ->{
            log.info("线程3开始");
            long start = System.currentTimeMillis();
            List<SaleAmtByBigCodeOutData> saleAmtList = gaiaSdSaleDMapper.getSaleAmtByCompClassList(inData.getClient());   //查询商品大类编码对应销售额
            log.info("线程3结束，共耗时：" + (System.currentTimeMillis() - start) + "毫秒");
            return saleAmtList;
        });

        CompletableFuture.allOf(resultFir, resultSed, resultThr).join();   //等待后执行完的线程结束

        List<StoreStockReportOutData> clientStockList = null;
        StockReportHeadOutData totalHeadInfo = null;
        List<SaleAmtByBigCodeOutData> saleAmtList = null;
        try {
            clientStockList = resultFir.get();
            totalHeadInfo = resultSed.get();
            saleAmtList = resultThr.get();
        } catch (Exception e) {
            log.error("库存报表查询异常：", e.getMessage());
            throw new BusinessException("提示：查询库存报表失败！");
        }
        //组装列表数据
        List<StoreStockReportOutData> resultList = new ArrayList<>();
        //计算列表中的每个周转天数
        if(ValidateUtil.isNotEmpty(clientStockList)) {
            //将List转为Map key为大类编码 value为销售额
            Map<String, BigDecimal> saleAmtMap = saleAmtList.stream().collect(Collectors.toMap(SaleAmtByBigCodeOutData::getBigTypeCode, SaleAmtByBigCodeOutData::getSaleAmt));
            for(StoreStockReportOutData storeStockReport : clientStockList) {
                String bigTypeCode = storeStockReport.getBigTypeCode();
                BigDecimal costAmt = storeStockReport.getCostAmt();
                BigDecimal saleAmtByBigCode = saleAmtMap.get(bigTypeCode);
                if (ValidateUtil.isNotEmpty(costAmt) && ValidateUtil.isNotEmpty(saleAmtByBigCode)) {
                    if (BigDecimal.ZERO.compareTo(saleAmtByBigCode) != 0) {
                        BigDecimal turnoverDays = costAmt.divide(saleAmtByBigCode, 5, BigDecimal.ROUND_HALF_UP)
                                .multiply(new BigDecimal(30)).setScale(0, BigDecimal.ROUND_HALF_UP);
                        storeStockReport.setTurnoverDays(turnoverDays);
                    }
                }
            }
            //计算合计
            BigDecimal totalStockCount = clientStockList.stream().map(StoreStockReportOutData :: getStockCount).reduce(BigDecimal.ZERO, BigDecimal :: add);
            BigDecimal totalCostAmt = clientStockList.stream().map(StoreStockReportOutData :: getCostAmt).reduce(BigDecimal.ZERO, BigDecimal :: add);
            //计算30天内销售额
            BigDecimal saleAmt = saleAmtList.stream().map(SaleAmtByBigCodeOutData::getSaleAmt).reduce(BigDecimal.ZERO, BigDecimal::add);

            BigDecimal totalTurnoverDays = null;
            if(ValidateUtil.isNotEmpty(saleAmt) && BigDecimal.ZERO.compareTo(saleAmt) != 0) {
                //计算合计周转天数
                totalTurnoverDays = totalCostAmt.divide(saleAmt, 5, BigDecimal.ROUND_HALF_UP)
                        .multiply(new BigDecimal(30)).setScale(0, BigDecimal.ROUND_HALF_UP);
            }

            //合计数据
            StoreStockReportOutData totalStoreStock = new StoreStockReportOutData();
            totalStoreStock.setBigType("合计");
            totalStoreStock.setStockCount(totalStockCount);
            totalStoreStock.setCostAmt(totalCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
            totalStoreStock.setTurnoverDays(totalTurnoverDays);
            resultList.add(totalStoreStock);
            resultList.addAll(clientStockList);

            //获取库存品项数图表数据
            Map<String, BigDecimal> stockCountChartInfo = getStockCountChartInfo(clientStockList);
            //获取库存成本额
            Map<String, BigDecimal> costAmtChartInfo = getCostAmtChartInfo(clientStockList, totalCostAmt);
            //获取周转天数图表数据
            Map<String, BigDecimal> turnoverDaysChartInfo = getTurnoverDaysChartInfo(clientStockList, saleAmtList);
            result.put("stockCountChart", stockCountChartInfo);
            result.put("costAmtChart", costAmtChartInfo);
            result.put("turnoverDaysChart", turnoverDaysChartInfo);
        }

        result.put("typeList", resultList);
        result.put("totalHeadInfo", totalHeadInfo);
        return JsonResult.success(result, "success");
    }

    /**
     * 获取库存品项数图表数据
     * @return
     */
    private Map<String, BigDecimal> getStockCountChartInfo(List<StoreStockReportOutData> storeStockReportList) {
        Map<String, BigDecimal> resultMap = new HashMap<>(3);
        BigDecimal totalNonDrug = BigDecimal.ZERO;
        for(StoreStockReportOutData storeStock : storeStockReportList) {
            if (ValidateUtil.isNotEmpty(storeStock)) {
                String bigType = storeStock.getBigType();
                BigDecimal stockCount = storeStock.getStockCount();
                switch (bigType) {
                    case "A药品":
                        resultMap.put("stockCountA", stockCount);
                        break;
                    case "B中药":
                        resultMap.put("stockCountB", stockCount);
                        break;
                    default:
                        //将库存品项数汇总（不包括成分大类A药品 以及 B中药）
                        totalNonDrug = totalNonDrug.add(stockCount);
                        break;
                }
            }
        }
        resultMap.put("nonDrug", totalNonDrug);
        return resultMap;
    }


    /**
     * 获取库存成本额图表数据
     * @param storeStockReportList
     * @return
     */
    private Map<String, BigDecimal> getCostAmtChartInfo(List<StoreStockReportOutData> storeStockReportList, BigDecimal totalCostAmt) {
        Map<String, BigDecimal> resultMap = new HashMap<>(3);
        BigDecimal totalNonDrug = BigDecimal.ZERO;
        BigDecimal oneHundred = new BigDecimal(100);
        for(StoreStockReportOutData storeStock : storeStockReportList) {
            if(ValidateUtil.isNotEmpty(storeStock)) {
                String bigType = storeStock.getBigType();
                BigDecimal costAmt = storeStock.getCostAmt();
                switch (bigType) { 
                    case "A药品":
                        if(ValidateUtil.isNotEmpty(totalCostAmt) && BigDecimal.ZERO.compareTo(totalCostAmt) != 0) {
                            BigDecimal costAmtA = costAmt.divide(totalCostAmt, 5, BigDecimal.ROUND_HALF_UP).multiply(oneHundred)
                                    .setScale(2, BigDecimal.ROUND_HALF_UP);
                            resultMap.put("costAmtA", costAmtA);
                        }
                        break;
                    case "B中药":
                        if(ValidateUtil.isNotEmpty(totalCostAmt) && BigDecimal.ZERO.compareTo(totalCostAmt) != 0) {
                            BigDecimal costAmtB = costAmt.divide(totalCostAmt, 5, BigDecimal.ROUND_HALF_UP).multiply(oneHundred)
                                    .setScale(2, BigDecimal.ROUND_HALF_UP);
                            resultMap.put("costAmtB", costAmtB);
                        }
                        break;
                    default:
                        //将销售成本额汇总（不包括成分大类A药品 以及 B中药）
                        totalNonDrug = totalNonDrug.add(costAmt);
                        break;
                }
            }
        }
        if(ValidateUtil.isNotEmpty(totalCostAmt) && BigDecimal.ZERO.compareTo(totalCostAmt) != 0) {
            BigDecimal nonDrugProp = totalNonDrug.divide(totalCostAmt, 5, BigDecimal.ROUND_HALF_UP).multiply(oneHundred)
                    .setScale(2, BigDecimal.ROUND_HALF_UP);;
            resultMap.put("nonDrug", nonDrugProp);
        }
        return resultMap;
    }


    /**
     * 获取周转天数图表数据
     * @param storeStockReportList
     * @param saleAmtList
     * @return
     */
    private Map<String, BigDecimal> getTurnoverDaysChartInfo(List<StoreStockReportOutData> storeStockReportList, List<SaleAmtByBigCodeOutData> saleAmtList) {
        Map<String, BigDecimal> resultMap = new HashMap<>(3);
        //非药品的库存成本额
        BigDecimal totalNonDrug = new BigDecimal("0");

        for(StoreStockReportOutData storeStock : storeStockReportList) {
            String bigType = storeStock.getBigType();
            BigDecimal turnoverDays = storeStock.getTurnoverDays();
            switch (bigType) {
                case "A药品":
                    resultMap.put("turnoverDaysA", turnoverDays);
                    break;
                case "B中药":
                    resultMap.put("turnoverDaysB", turnoverDays);
                    break;
                default:
                    //将销售成本额汇总（不包括成分大类A药品 以及 B中药）
                    totalNonDrug = totalNonDrug.add(storeStock.getCostAmt());
                    break;
            }
        }
        //查询除去药品成分分类 A 以及 B商品的销售成本额
        BigDecimal saleAmtByDateRange = new BigDecimal(0);
        for(SaleAmtByBigCodeOutData saleAmtByBigCode : saleAmtList) {
            String bigTypeCode = saleAmtByBigCode.getBigTypeCode();
            BigDecimal saleAmt = saleAmtByBigCode.getSaleAmt();
            if(!"A".equals(bigTypeCode) && !"B".equals(bigTypeCode)) {
                saleAmtByDateRange = saleAmtByDateRange.add(ValidateUtil.isNotEmpty(saleAmt) ? saleAmt : new BigDecimal(0));
            }
        }
        if(ValidateUtil.isEmpty(saleAmtByDateRange) || ValidateUtil.isEmpty(totalNonDrug)) {
            resultMap.put("nonDrug", null);
            return resultMap;
        }
        if(BigDecimal.ZERO.compareTo(saleAmtByDateRange) != 0) {
            BigDecimal nonDrugTurnoverDays = totalNonDrug.divide(saleAmtByDateRange, 5, BigDecimal.ROUND_HALF_UP)
                    .multiply(new BigDecimal(30)).setScale(0, BigDecimal.ROUND_HALF_UP);
            resultMap.put("nonDrug", nonDrugTurnoverDays);
        } else {
            resultMap.put("nonDrug", null);
        }
        return resultMap;
    }


    @Override
    @Transactional
    public JsonResult getCompanyOutOfStock(OutOfStockInData inData) {
        String clientId = inData.getClientId();
        String queryDate = inData.getQueryDate();
        if(ValidateUtil.isEmpty(queryDate)) {
            inData.setQueryDate(CommonUtil.getyyyyMMdd());
        }

        //获取所有直营店
        List<String> directStoreList = storesGroupMapper.getListByClientAndType(clientId, "DX0003");
        inData.setDirectStoreList(directStoreList);
        //查询公司级缺断货信息
        CompanyOutOfStockInfoOutData companyOutOfStockInfo = gaiaProductBusinessMapper.getCompanyOutOfStockInfo(inData);
        if(ValidateUtil.isEmpty(companyOutOfStockInfo)) {
            return JsonResult.success(null, "success");
        }

        //查询缺断货明细列表
        List<CompanyOutOfStockDetailOutData> companyOutOfStockList = gaiaProductBusinessMapper.getCompanyOutOfStockList(inData);
        if(ValidateUtil.isNotEmpty(companyOutOfStockList)) {
            companyOutOfStockInfo.setOutOfStockDetail(companyOutOfStockList);
        }
        //先去查询是否存在记录
        GaiaClSystemPara clientAuth = gaiaClSystemParaMapper.getAuthByClient(clientId, "APP_AUTO_PUSH_FLAG");
        //初始化门店权限   如果不存在 则表示为第一次进入此页面  则插入自动推送的权限
        if(ValidateUtil.isEmpty(clientAuth)) {
            GaiaClSystemPara clientSystemPara = new GaiaClSystemPara();
            clientSystemPara.setClient(clientId);
            clientSystemPara.setGcspId("APP_AUTO_PUSH_FLAG");
            clientSystemPara.setGcspName("APP端自动是否自动推送标志");
            clientSystemPara.setGcspParaRemark("0：手动推送 1：自动推送");
            clientSystemPara.setGcspPara1("1");   //自动推送 勾选
            clientSystemPara.setGcspPara2("2");   //默认推送给直营店
            clientSystemPara.setGcspUpdateDate(CommonUtil.getyyyyMMdd());
            clientSystemPara.setGcspUpdateTime(CommonUtil.getHHmmss());
            gaiaClSystemParaMapper.insert(clientSystemPara);

            //推送直营店权限
            List<GaiaSdSystemPara> batchInsertList = new ArrayList<>();
            List<StoreOutData> storeList = gaiaStoreDataMapper.getListByClientAndAttribute(clientId, "2");
            for(StoreOutData store : storeList) {
                GaiaSdSystemPara storeSystemPara = new GaiaSdSystemPara();
                storeSystemPara.setClientId(clientId);
                storeSystemPara.setGsspBrId(store.getStoCode());
                storeSystemPara.setGsspId("APP_STORE_SHOW_OUT_STOCK_FLAG");
                storeSystemPara.setGsspName("APP端是否显示缺断货标识");
                storeSystemPara.setGsspParaRemark("0：不显示 1：显示");
                storeSystemPara.setGsspPara("1");
                storeSystemPara.setGsspUpdateEmp(inData.getUserId());
                storeSystemPara.setGsspUpdateDate(CommonUtil.getyyyyMMdd());
                batchInsertList.add(storeSystemPara);
            }
            if(ValidateUtil.isNotEmpty(batchInsertList)) {
                gaiaSdSystemParaMapper.batchReplaceInsert(batchInsertList);
            }
            companyOutOfStockInfo.setCheckFlag("1");
            companyOutOfStockInfo.setPushFlag("2");
            //插入推送记录表
            insertOutOfStockRecord(clientId, inData.getUserId(), storeList.stream().map(StoreOutData::getStoCode).collect(Collectors.toList()));
        } else {
            companyOutOfStockInfo.setCheckFlag(clientAuth.getGcspPara1());
            companyOutOfStockInfo.setPushFlag(clientAuth.getGcspPara2());
        }
        return JsonResult.success(companyOutOfStockInfo, "success");
    }


    @Override
    @Transactional
    public JsonResult pushStoreOutOfStock(UpdateParaByStoreListInData inData) {
        //勾选状态   是否勾选 0-取消 1-勾选
        String checkFlag = inData.getCheckFlag();
        //推送状态   推送标志 null-所有 2-直营店 3-加盟店 4-其他  选择所有时此字段传null
        String pushFlag = inData.getPushFlag();

        //修改勾选 修改推送状态
        if(ValidateUtil.isNotEmpty(checkFlag)) {
            GaiaClSystemPara clSystemPara = new GaiaClSystemPara();
            clSystemPara.setClient(inData.getClient());
            clSystemPara.setGcspName("APP端自动是否自动推送标志");
            clSystemPara.setGcspParaRemark("0：手动推送 1：自动推送");
            clSystemPara.setGcspId("APP_AUTO_PUSH_FLAG");
            clSystemPara.setGcspPara1(checkFlag);
            clSystemPara.setGcspPara2(pushFlag);
            clSystemPara.setGcspUpdateEmp(inData.getUserId());
            clSystemPara.setGcspUpdateDate(CommonUtil.getyyyyMMdd());
            clSystemPara.setGcspUpdateTime(CommonUtil.getHHmmss());
            gaiaClSystemParaMapper.updateByPrimaryKey(clSystemPara);
        }

        inData.setPara("0");
        inData.setParaId("APP_STORE_SHOW_OUT_STOCK_FLAG");
        inData.setUpdateDate(CommonUtil.getyyyyMMdd());
        //先将所有门店改为不显示状态  然后再修改为显示（推送）状态
        gaiaSdSystemParaMapper.updateParaByStoreList(inData);

        List<GaiaSdSystemPara> batchInsertList = new ArrayList<>();
        List<StoreOutData> storeList = gaiaStoreDataMapper.getListByClientAndAttributeExt(inData.getClient(), pushFlag);
        if(ValidateUtil.isEmpty(storeList)) {
            throw new BusinessException("该类型下无门店");
        }
        for(StoreOutData store : storeList) {
            String stoCode = store.getStoCode();
            GaiaSdSystemPara storeSystemPara = new GaiaSdSystemPara();
            storeSystemPara.setClientId(inData.getClient());
            storeSystemPara.setGsspBrId(stoCode);
            storeSystemPara.setGsspId("APP_STORE_SHOW_OUT_STOCK_FLAG");
            storeSystemPara.setGsspName("APP端是否显示缺断货标识");
            storeSystemPara.setGsspParaRemark("0：不显示 1：显示");
            storeSystemPara.setGsspPara("1");
            storeSystemPara.setGsspUpdateEmp(inData.getUserId());
            storeSystemPara.setGsspUpdateDate(CommonUtil.getyyyyMMdd());
            batchInsertList.add(storeSystemPara);
        }
        //插入权限表
        if(ValidateUtil.isNotEmpty(batchInsertList)) {
            gaiaSdSystemParaMapper.batchReplaceInsert(batchInsertList);
        }
        //插入推送记录表
        insertOutOfStockRecord(inData.getClient(), inData.getUserId(), storeList.stream().map(StoreOutData::getStoCode).collect(Collectors.toList()));
        return JsonResult.success("", "success");
    }


    @Override
    @Transactional
    public JsonResult choicePushStoreOutOfStock(UpdateParaByStoreListInData inData) {
        List<UpdateParaByStoreIdList> brIdList = inData.getBrIdList();
        if(ValidateUtil.isEmpty(brIdList)) {
            throw new BusinessException("至少选择一家推送门店");
        }

        //修改勾选 修改推送状态
        if(ValidateUtil.isNotEmpty(inData.getCheckFlag())) {
            GaiaClSystemPara clSystemPara = new GaiaClSystemPara();
            clSystemPara.setClient(inData.getClient());
            clSystemPara.setGcspId("APP_AUTO_PUSH_FLAG");
            clSystemPara.setGcspPara1(inData.getCheckFlag());
            clSystemPara.setGcspPara2(inData.getPushFlag());
            clSystemPara.setGcspUpdateEmp(inData.getUserId());
            clSystemPara.setGcspUpdateDate(CommonUtil.getyyyyMMdd());
            clSystemPara.setGcspUpdateTime(CommonUtil.getHHmmss());
            gaiaClSystemParaMapper.updateByPrimaryKeySelective(clSystemPara);
        }

        List<UpdateParaByStoreIdList> copyBrIdList = new ArrayList<>();
        //深拷贝list  先将所有数据改为不推送状态  然后将选择的门店修改为推送状态
        CollectionUtils.addAll(copyBrIdList, new Object[brIdList.size()]);
        Collections.copy(copyBrIdList, brIdList);
        inData.setPara("0");
        inData.setParaId("APP_STORE_SHOW_OUT_STOCK_FLAG");
        inData.setUpdateDate(CommonUtil.getyyyyMMdd());
        inData.setBrIdList(null);
        gaiaSdSystemParaMapper.updateParaByStoreList(inData);

        List<GaiaSdSystemPara> batchInsertList = new ArrayList<>();
        for(UpdateParaByStoreIdList store : copyBrIdList) {
            GaiaSdSystemPara storeSystemPara = new GaiaSdSystemPara();
            storeSystemPara.setClientId(inData.getClient());
            storeSystemPara.setGsspBrId(store.getBrId());
            storeSystemPara.setGsspId("APP_STORE_SHOW_OUT_STOCK_FLAG");
            storeSystemPara.setGsspName("APP端是否显示缺断货标识");
            storeSystemPara.setGsspParaRemark("0：不显示 1：显示");
            storeSystemPara.setGsspPara("1");
            storeSystemPara.setGsspUpdateEmp(inData.getUserId());
            storeSystemPara.setGsspUpdateDate(CommonUtil.getyyyyMMdd());
            batchInsertList.add(storeSystemPara);
        }
        if(ValidateUtil.isNotEmpty(batchInsertList)) {
            gaiaSdSystemParaMapper.batchReplaceInsert(batchInsertList);
        }

        //插入推送记录表
        insertOutOfStockRecord(inData.getClient(), inData.getUserId(), brIdList.stream().map(UpdateParaByStoreIdList::getBrId).collect(Collectors.toList()));
        //计算汇总
        Map<String, Object> result = new HashMap(3);
        result.put("pushStoreCount", batchInsertList.size());
        result.put("avgSaleAmt", copyBrIdList.stream().map(UpdateParaByStoreIdList :: getAvgSaleAmt).reduce(BigDecimal.ZERO, BigDecimal :: add));
        result.put("avgProfitAmt", copyBrIdList.stream().map(UpdateParaByStoreIdList :: getAvgProfitAmt).reduce(BigDecimal.ZERO, BigDecimal :: add));
        return JsonResult.success(result, "success");
    }


    /**
     * 插入推送记录表
     * @param client
     * @param brIdList
     */
    private void insertOutOfStockRecord(String client, String emp, List<String> brIdList) {
        if(ValidateUtil.isNotEmpty(brIdList)) {
            List<GaiaSdOutOfStockPushRecord> pushRecordList = new ArrayList<>(brIdList.size());
            Date date = new Date();
            for(String brId : brIdList) {
                GaiaSdOutOfStockPushRecord pushRecord = new GaiaSdOutOfStockPushRecord();
                pushRecord.setClient(client);
                pushRecord.setBrId(brId);
                pushRecord.setType(1);
                pushRecord.setStatus(0);
                pushRecord.setPushTime(date);
                pushRecord.setPushEmp(emp);
                pushRecord.setReadTime(new Date(0));    //推送时 设置读取时间为1970年1月1日 8点
                pushRecord.setReadEmp(emp);             //推送时 设置读取员工为当前推送员工工号
                pushRecordList.add(pushRecord);
            }
            outOfStockPushRecordMapper.batchInsert(pushRecordList);
        }
    }


    @Override
    public JsonResult getPushStoreHistory(PushStoreHistoryInData inData) {
        String weekOrMonth = inData.getWeekOrMonth();
        if(ValidateUtil.isEmpty(weekOrMonth)) {
            weekOrMonth = "1";
        }
        //查询门店历史数据
        List<PushStoreHistoryData> pushStoreHistoryList = outOfStockPushRecordMapper.getPushStoreHistory(inData.getClient(), weekOrMonth);

        List<PushStoreHistoryOutData> resultList = new ArrayList<>();
        if(ValidateUtil.isNotEmpty(pushStoreHistoryList)) {
            //将历史数据根据日期分组 转为map  key为日期  value为对应的list
            Map<String, List<PushStoreHistoryData>> pushStoreHistoryMap =
                    pushStoreHistoryList.stream().collect(Collectors.groupingBy(item -> item.getWeeksOrMonths()));
            //遍历map组装参数
            for(String date : pushStoreHistoryMap.keySet()) {
                //获取到日期对应的list
                List<PushStoreHistoryData> tempList = pushStoreHistoryMap.get(date);
                PushStoreHistoryOutData pushStoreHistoryOutData = new PushStoreHistoryOutData();
                pushStoreHistoryOutData.setWeeksOrMonths(date);      //日期
                pushStoreHistoryOutData.setHistoryList(tempList);    //日期对应List
                resultList.add(pushStoreHistoryOutData);
            }
        }
        return JsonResult.success(resultList, "success");
    }


    @Override
    public JsonResult getPushWmsHistory(PushStoreHistoryInData inData) {
        String weekOrMonth = inData.getWeekOrMonth();
        if(ValidateUtil.isEmpty(weekOrMonth)) {
            weekOrMonth = "1";
        }
        //查询仓库历史数据
        List<PushStoreHistoryData> wmsHistoryList = outOfStockPushRecordMapper.getPushWmsHistory(inData.getClient(), weekOrMonth);
        return JsonResult.success(wmsHistoryList, "success");
    }



    @Override
    public JsonResult getProductMidTypeReport(ProductMidTypeSaleInData inData) {
        String type = inData.getType();
        String bigCode = inData.getBigCode();
        if(ValidateUtil.isEmpty(type)) {
            inData.setType("1");
        }
        if(ValidateUtil.isEmpty(bigCode)) {
            inData.setBigCode("1");
        }

        //开启线程1
        CompletableFuture<List<ProductMidTypeOutData>> resultFir = CompletableFuture.supplyAsync(() ->{
            List<ProductMidTypeOutData> productMidTypeList = null;
            //查询中类列表
            log.info("库存报表(中类)-线程1开始");
            long startTime = System.currentTimeMillis();
            if("1".equals(type)) {
                //公司
                productMidTypeList = gaiaSdStockBatchMapper.getProductMidTypeReportByClient(inData);
            } else if("2".equals(type)) {
                //仓库
                productMidTypeList = gaiaSdStockBatchMapper.getProductMidTypeReportByDc(inData);
            } else if("3".equals(type)) {
                //门店
                productMidTypeList = gaiaSdStockBatchMapper.getProductMidTypeReportByStore(inData);
            }
            log.info("库存报表(中类)-线程1结束，耗时：" + (System.currentTimeMillis() - startTime));
            return productMidTypeList;
        });
        //开启线程2
        CompletableFuture<List<SaleAmtByMidCodeOutData>> resultSec = CompletableFuture.supplyAsync(() ->{
            log.info("库存报表(中类)-线程2开始");
            long startTime = System.currentTimeMillis();
            List<SaleAmtByMidCodeOutData> saleAmtByCompMidClassList = gaiaSdSaleDMapper.getSaleAmtByCompMidClassList(inData);
            log.info("库存报表(中类)-线程2结束，耗时：" + (System.currentTimeMillis() - startTime));
            return saleAmtByCompMidClassList;
        });
        //开启线程3
        CompletableFuture<BigDecimal> resultThr = CompletableFuture.supplyAsync(() ->{
            log.info("库存报表(中类)-线程3开始");
            long startTime = System.currentTimeMillis();
            BigDecimal saleAmt = gaiaSdSaleDMapper.getSaleAmtByCompClass(inData.getClientId(), inData.getBigCode());   //查询30天内销售额
            log.info("库存报表(中类)-线程3结束，耗时：" + (System.currentTimeMillis() - startTime));
            return saleAmt;
        });

        CompletableFuture.allOf(resultFir, resultSec, resultThr).join();   //等待后执行完的线程结束

        List<ProductMidTypeOutData> productMidTypeList = null;
        List<SaleAmtByMidCodeOutData> saleAmtByCompMidClassList = null;
        BigDecimal saleAmt = null;
        try {
            productMidTypeList = resultFir.get();
            saleAmtByCompMidClassList = resultSec.get();
            saleAmt = resultThr.get();
        } catch (Exception e) {
            log.error("查询库存报表(中类)异常：", e.getMessage());
            throw new BusinessException("提示：查询中类销售情况失败！");
        }

        if(ValidateUtil.isNotEmpty(productMidTypeList)) {
            Map<String, BigDecimal> saleAmtByCompMidMap = saleAmtByCompMidClassList.stream().collect(Collectors.toMap(SaleAmtByMidCodeOutData::getMidTypeCode, SaleAmtByMidCodeOutData::getSaleAmt));
            for(ProductMidTypeOutData productMid : productMidTypeList) {
                BigDecimal costAmt = productMid.getCostAmt();
                //计算每种中类的周转天数
                BigDecimal saleAmtByMidClass = saleAmtByCompMidMap.get(productMid.getMidCode());
                if(ValidateUtil.isNotEmpty(saleAmtByMidClass) && BigDecimal.ZERO.compareTo(saleAmtByMidClass) != 0) {
                    BigDecimal turnoverDaysByMidClass = costAmt.divide(saleAmtByMidClass, 5, BigDecimal.ROUND_HALF_UP)
                            .multiply(new BigDecimal(30)).setScale(0, BigDecimal.ROUND_HALF_UP);
                    productMid.setTurnoverDays(turnoverDaysByMidClass);
                } else {
                    productMid.setTurnoverDays(BigDecimal.ZERO);
                }
                //库存成本额 保留0位小数
                productMid.setCostAmt(costAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
            }
        }
        //合计库存品项数
        BigDecimal totalStockCount = productMidTypeList.stream().map(ProductMidTypeOutData::getStockCount).reduce(BigDecimal.ZERO, BigDecimal::add);
        //合计库存成本额
        BigDecimal totalCostAmt = productMidTypeList.stream().map(ProductMidTypeOutData::getCostAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        //计算合计
        ProductMidTypeOutData total = new ProductMidTypeOutData();
        total.setMidName("合计");
        total.setStockCount(totalStockCount);
        total.setCostAmt(totalCostAmt.setScale(0, BigDecimal.ROUND_HALF_UP));

        //计算周转天数
        BigDecimal totalTurnoverDays = BigDecimal.ZERO;
        if(ValidateUtil.isNotEmpty(saleAmt) && BigDecimal.ZERO.compareTo(saleAmt) != 0) {
            //计算合计周转天数
            totalTurnoverDays = totalCostAmt.divide(saleAmt, 5, BigDecimal.ROUND_HALF_UP)
                    .multiply(new BigDecimal(30)).setScale(0, BigDecimal.ROUND_HALF_UP);
        }
        total.setTurnoverDays(totalTurnoverDays);

        //返回列表 合计排在第一个元素
        List<ProductMidTypeOutData> resultList = new ArrayList<>(productMidTypeList.size() + 1);
        resultList.add(total);
        resultList.addAll(productMidTypeList);
        return JsonResult.success(resultList, "success");
    }


    @Override
    public JsonResult getStoreStockListReport(AppStoreSaleInData inData) {
        String orderByFlag = inData.getOrderByFlag();               //排序标志 1-升序 2-降序 传空默认为降序
        String topOrLast = inData.getTopOrLast();                   //排序前后 0-全部 1-前(从大到小) 2-后(从小到大)
        String orderByCondition = inData.getOrderByCondition();     //排序字段 1-动销品项 2-销售额 3-毛利率 传空默认为销售额
        Integer pageSize = inData.getPageSize();                    //传空默认10条
        if(ValidateUtil.isEmpty(orderByFlag)) {
            inData.setOrderByFlag("2");
        }
        if(ValidateUtil.isEmpty(orderByCondition)) {
            inData.setOrderByCondition("2");
        }
        if(ValidateUtil.isEmpty(topOrLast)) {
            inData.setTopOrLast("0");
        }
        if(ValidateUtil.isEmpty(pageSize)) {
            inData.setPageSize(10);
        }

        List<StoreStockOutData> storeStockList = gaiaSdStockBatchMapper.getStoreStockList(inData);
        return JsonResult.success(storeStockList, "success");
    }

}
