package com.gys.business.service.data.replenishParam;

import lombok.Data;

@Data
public class ReplenishPart {
    private String clientId;
    private String stoCode;
    private String flag1;
    private String flag2;
    private String flag3;
    private String flag4;
    private String flag5;
    private String flag6;
}
