package com.gys.business.service.data.billOfAp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 供应商应付明细清单表
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Data
@Accessors(chain = true)
public class GaiaBillOfApResponse implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "仓库地点")
    private String depId;

    @ApiModelProperty(value = "仓库地门店编码点")
    private String stoCode;

    @ApiModelProperty(value = "应付编码")
    private String paymentId;

    @ApiModelProperty(value = "供应商自编码")
    private String supSelfCode;

    @NotNull(message = "开始时间不能为空")
    private LocalDate beginTime;

    @NotNull(message = "结束时间不能为空")
    private LocalDate endTime;

    private Integer pageNum;

    private Integer pageSize;

    @ApiModelProperty(value = "业务员名称")
    private String salesmanName;

    @ApiModelProperty(value = "业务员编码")
    private String salesmanId;

    @ApiModelProperty(value = "业务员名称")
    private String[] salesmanNameArray;

    private String beginTimeStr;


    private String endTimeStr;
}
