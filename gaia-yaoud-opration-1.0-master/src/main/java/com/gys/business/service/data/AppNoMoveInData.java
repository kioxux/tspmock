package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AppNoMoveInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "不动销天数  默认45天")
    private Integer noMoveDays;

    @ApiModelProperty(value = "是否包含仓库 1-未勾选仓库 2-勾选仓库  默认1")
    private String wmsFlag;

    @ApiModelProperty(value = "中类类型代码  1-处方药 2-OTC 3-中药 888-药品小计 999-非药品 传空默认为药品小计")
    private String typeCode;

    @ApiModelProperty(value = "直营管理门店列表")
    List<String> directStoreList;

}
