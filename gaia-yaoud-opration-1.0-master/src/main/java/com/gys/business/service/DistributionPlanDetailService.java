package com.gys.business.service;

import com.gys.business.mapper.entity.DistributionCountInfo;
import com.gys.business.mapper.entity.DistributionPlanDetail;
import com.gys.business.mapper.entity.NewDistributionPlan;
import com.gys.business.service.data.DistributionPlanDetailVO;

import java.util.List;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 14:52
 */
public interface DistributionPlanDetailService {

    DistributionPlanDetail getById(Long id);

    DistributionPlanDetail add(DistributionPlanDetail distributionPlanDetail);

    DistributionPlanDetail update(DistributionPlanDetail distributionPlanDetail);

    List<DistributionPlanDetail> findList(DistributionPlanDetail cond);

    DistributionCountInfo getPlanCountInfo(NewDistributionPlan distributionPlan);

    List<DistributionPlanDetailVO> getPlanDetail(DistributionPlanDetail cond);
}
