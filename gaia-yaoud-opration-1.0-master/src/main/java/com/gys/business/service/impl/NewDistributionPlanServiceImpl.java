package com.gys.business.service.impl;

import com.alibaba.excel.util.DateUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.DistributionPlanDetailService;
import com.gys.business.service.GaiaSdMessageService;
import com.gys.business.service.NewDistributionPlanService;
import com.gys.business.service.data.DistributionPlanForm;
import com.gys.business.service.data.DistributionPlanVO;
import com.gys.business.service.data.PreReplenishVO;
import com.gys.common.enums.DistributionPlanEnum;
import com.gys.common.enums.DistributionTypeEnum;
import com.gys.common.enums.LoginMessageEnum;
import com.gys.common.exception.BusinessException;
import com.gys.feign.PurchaseService;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 13:06
 */
@Slf4j
@Service("newDistributionPlanService")
public class NewDistributionPlanServiceImpl implements NewDistributionPlanService {
    @Resource
    private NewDistributionPlanMapper newDistributionPlanMapper;
    @Resource
    private DistributionPlanDetailService distributionPlanDetailService;
    @Resource
    private GaiaWmsRkysMapper gaiaWmsRkysMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaWmKuCunMapper gaiaWmKuCunMapper;
    @Resource
    private GaiaSdMessageService gaiaSdMessageService;
    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Resource
    private PurchaseService purchaseService;

    @Override
    public NewDistributionPlan getById(Long id) {
        return newDistributionPlanMapper.getById(id);
    }

    @Override
    public NewDistributionPlan getUnique(NewDistributionPlan cond) {
        return newDistributionPlanMapper.getUnique(cond);
    }

    @Override
    public List<NewDistributionPlan> findList(NewDistributionPlan cond) {
        return newDistributionPlanMapper.findList(cond);
    }

    @Transactional
    @Override
    public NewDistributionPlan add(NewDistributionPlan newDistributionPlan) {
        try {
            newDistributionPlan.setPlanCode(getNextPlanCode(newDistributionPlan.getClient()));
            newDistributionPlanMapper.add(newDistributionPlan);
        } catch (Exception e) {
            throw new BusinessException("新增铺货计划异常", e);
        }
        return getById(newDistributionPlan.getId());
    }

    private String getNextPlanCode(String client) {
        String prefix = CommonUtil.getyyyyMMdd();
        NewDistributionPlan cond = new NewDistributionPlan();
        cond.setClient(client);
        cond.setCreateTime(new Date());
        List<NewDistributionPlan> list = findList(cond);
        int num = list == null ? 1 : list.size() + 1;
        String suffix = String.format("%04d", num);
        return prefix + suffix;
    }

    @Transactional
    @Override
    public NewDistributionPlan update(NewDistributionPlan newDistributionPlan) {
        int num;
        try {
            num = newDistributionPlanMapper.update(newDistributionPlan);
        } catch (Exception e) {
            throw new BusinessException("铺货计划更新异常", e);
        }
        if (num == 0) {
            throw new BusinessException("铺货计划更新失败");
        }
        return getById(newDistributionPlan.getId());
    }

    @Override
    public List<DistributionPlanVO> findPage(DistributionPlanForm distributionPlanForm) {
        if (StringUtil.isNotEmpty(distributionPlanForm.getProSelfCode())) {
            distributionPlanForm.setProSelfCodeList(getProSelfCodeList(distributionPlanForm.getProSelfCode()));
        }
        List<NewDistributionPlan> distributionPlanList = newDistributionPlanMapper.findPage(distributionPlanForm);
        return transformDistributionPlanVO(distributionPlanList);
    }

    private List<String> getProSelfCodeList(String proSelfCode) {
        String[] proCodes = proSelfCode.replaceAll("，", ",").split(",");
        return Lists.newArrayList(proCodes);
    }

    @Override
    public void confirmReplenish(NewDistributionPlan distributionPlan) {
        //校验库存是否充足
        WmKuCun cond = new WmKuCun();
        cond.setClient(distributionPlan.getClient());
        cond.setWmSpBm(distributionPlan.getProSelfCode());
        WmKuCun wmKuCun = gaiaWmKuCunMapper.getKuCunInfo(cond);
        if (wmKuCun == null || BigDecimal.ZERO.compareTo(wmKuCun.getWmKysl()) == 0) {
            //库存不足，补铺失败、计划直接完成
            Date date = new Date();
            distributionPlan.setStatus(DistributionPlanEnum.COMPLETED.status);
            distributionPlan.setFinishDate(date);
            distributionPlan.setUpdateTime(date);
            update(distributionPlan);
        } else {
            log.info(String.format("<新品铺货><确认补铺><调用微服务请求参数：%s>", JSON.toJSONString(distributionPlan)));
            String result = purchaseService.confirmReplenish(distributionPlan);
            log.info(String.format("<新品铺货><确认补铺><调用微服务返回参数：%s>", result));
        }
    }

    @Override
    public List<PreReplenishVO> preReplenish(NewDistributionPlan distributionPlan) {
        DistributionPlanDetail cond = new DistributionPlanDetail();
        cond.setClient(distributionPlan.getClient());
        cond.setPlanCode(distributionPlan.getPlanCode());
        cond.setStatus(0);
        List<DistributionPlanDetail> list = distributionPlanDetailService.findList(cond);
        //获取库存信息
        WmKuCun wmKuCunCond = new WmKuCun();
        wmKuCunCond.setClient(distributionPlan.getClient());
        wmKuCunCond.setWmSpBm(distributionPlan.getProSelfCode());
        WmKuCun wmKuCun = gaiaWmKuCunMapper.getKuCunInfo(wmKuCunCond);
        GaiaProductBusiness proCond = new GaiaProductBusiness();
        proCond.setClient(distributionPlan.getClient());
        proCond.setProSelfCode(distributionPlan.getProSelfCode());
        List<GaiaProductBusiness> productList = gaiaProductBusinessMapper.findList(proCond);
        return transformReplenishVO(list, productList, wmKuCun);
    }

    @Override
    public void confirmCall(NewDistributionPlan distributionPlan) {
        BigDecimal rkysQuantity = getWmsRkysQuantity(distributionPlan);
        if (BigDecimal.ZERO.compareTo(rkysQuantity) >= 0 && DistributionTypeEnum.ONCE_CALL.type.equals(distributionPlan.getDistributionType())) {
            distributionPlan.setStatus(DistributionPlanEnum.COMPLETED.status);
            distributionPlan.setFinishDate(new Date());
            update(distributionPlan);
        } else{
            purchaseService.confirmCall(distributionPlan);
        }
    }

    @Override
    public List<WmsRkys> getWmsRkysList(NewDistributionPlan distributionPlan) {
        WmsRkys cond = new WmsRkys();
        cond.setClient(distributionPlan.getClient());
        cond.setWmSpBm(distributionPlan.getProSelfCode());
        cond.setState(2);
        return gaiaWmsRkysMapper.findList(cond);
    }

    @Override
    public BigDecimal getWmsRkysQuantity(NewDistributionPlan distributionPlan) {
        BigDecimal rkysQuantity = BigDecimal.ZERO;
        List<WmsRkys> list = getWmsRkysList(distributionPlan);
        if (list != null && list.size() > 0) {
            for (WmsRkys wmsRkys : list) {
                if (StringUtil.isNotEmpty(distributionPlan.getWmRkdh()) && distributionPlan.getWmRkdh().contains(wmsRkys.getWmRkdh())) {
                    continue;
                }
                rkysQuantity = rkysQuantity.add(wmsRkys.getWmShsl());
            }
        }
        return rkysQuantity;
    }

    @Override
    public boolean validStock(NewDistributionPlan distributionPlan) {
        WmKuCun cond = new WmKuCun();
        cond.setClient(distributionPlan.getClient());
        cond.setWmSpBm(distributionPlan.getProSelfCode());
        WmKuCun wmsKuCun = gaiaWmKuCunMapper.getKuCunInfo(cond);
        return wmsKuCun != null && BigDecimal.ZERO.compareTo(wmsKuCun.getWmKysl()) < 0;
    }

    @Override
    public void execPlanExpireJob() {
        List<NewDistributionPlan> list = newDistributionPlanMapper.getExpireList(DateUtils.format(new Date(), DateUtils.DATE_FORMAT_19));
        if (list != null && list.size() > 0) {
            for (NewDistributionPlan distributionPlan : list) {
                distributionPlan.setStatus(DistributionPlanEnum.EXPIRED.status);
                update(distributionPlan);
            }
        }
    }

    @Override
    public void execCheckDistribution() {
        //获取已过期的铺货计划
        NewDistributionPlan cond = new NewDistributionPlan();
        cond.setStatus(DistributionPlanEnum.EXPIRED.status);
        List<NewDistributionPlan> planList = findList(cond);
        Map<String, Integer> remindMap = new HashMap<>();
        for (NewDistributionPlan distributionPlan : planList) {
            if (remindMap.containsKey(distributionPlan.getClient())) {
                Integer num = remindMap.get(distributionPlan.getClient());
                remindMap.put(distributionPlan.getClient(), num + 1);
            } else {
                remindMap.put(distributionPlan.getClient(), 1);
            }
        }
        if (remindMap.size() > 0) {
            for (Map.Entry<String, Integer> entry : remindMap.entrySet()) {
                //发送铺货差异提醒
                generateDistributionDiffMessage(entry);
            }
        }
    }

    /**
     * 产生铺货差异消息提醒
     */
    private void generateDistributionDiffMessage(Map.Entry<String, Integer> entry) {
        GaiaSdMessage sdMessage = new GaiaSdMessage();
        sdMessage.setClient(entry.getKey());
        sdMessage.setGsmId("product");//门店、配送中心
        sdMessage.setGsmType(LoginMessageEnum.NEW_DISTRIBUTION_PLAN_CODE.getCode());//消息类型
        String voucherId = gaiaSdMessageMapper.selectNextVoucherId(entry.getKey(), "company");
        sdMessage.setGsmVoucherId(voucherId);//消息流水号
        sdMessage.setGsmValue("");//消息值
        sdMessage.setGsmRemark("您有<font color=\"#FF0000\">" + entry.getValue() + "</font>个新品铺货计划有差异，请查看。");
        sdMessage.setGsmFlag("N");//是否查看
        sdMessage.setGsmPage(LoginMessageEnum.NEW_DISTRIBUTION_PLAN_PAGE.getCode());//跳转页面
        sdMessage.setGsmWarningDay("6");//有效期天数
        sdMessage.setGsmPlatForm("WEB");//消息渠道
        sdMessage.setGsmDeleteFlag("0");
        Date date = new Date();
        sdMessage.setGsmArriveDate(DateUtils.format(date, "yyyyMMdd"));
        sdMessage.setGsmArriveTime(DateUtils.format(date, "HHmmss"));
        gaiaSdMessageService.addMessage(sdMessage);
    }

    private List<PreReplenishVO> transformReplenishVO(List<DistributionPlanDetail> list, List<GaiaProductBusiness> productList, WmKuCun wmKuCun) {
        BigDecimal useQuantity = BigDecimal.ZERO;
        List<PreReplenishVO> preReplenishVOList = new ArrayList<>();
        GaiaProductBusiness productBusiness = productList.get(0);
        int i = 1;
        for (DistributionPlanDetail distributionPlanDetail : list) {
            BigDecimal leftQuantity = wmKuCun.getWmKysl().subtract(useQuantity);
            PreReplenishVO preReplenishVO = new PreReplenishVO();
            BeanUtils.copyProperties(distributionPlanDetail, preReplenishVO);
            preReplenishVO.setIndex(i++);
            preReplenishVO.setProductSpec(productBusiness.getProSpecs());
            preReplenishVO.setProductUnit(productBusiness.getProUnit());
            if (BigDecimal.ZERO.compareTo(leftQuantity) >= 0) {
                preReplenishVO.setNeedQuantity(BigDecimal.ZERO);
            } else {
                BigDecimal needQuantity = preReplenishVO.getPlanQuantity().subtract(preReplenishVO.getActualQuantity());
                if (leftQuantity.compareTo(needQuantity) >= 0) {
                    preReplenishVO.setNeedQuantity(needQuantity);
                    useQuantity = useQuantity.add(needQuantity);
                } else {
                    preReplenishVO.setNeedQuantity(leftQuantity);
                    useQuantity = useQuantity.add(leftQuantity);
                }
            }
            preReplenishVOList.add(preReplenishVO);
        }
        return preReplenishVOList;
    }

    private List<DistributionPlanVO> transformDistributionPlanVO(List<NewDistributionPlan> distributionPlanList) {
        List<DistributionPlanVO> list = new ArrayList<>();
        int index = 1;
        for (NewDistributionPlan newDistributionPlan : distributionPlanList) {
            DistributionPlanVO distributionPlanVO = new DistributionPlanVO();
            BeanUtils.copyProperties(newDistributionPlan, distributionPlanVO);
            distributionPlanVO.setIndex(index++);
            distributionPlanVO.setCreateTime(DateUtils.format(newDistributionPlan.getCreateTime(), "yyyy/MM/dd HH:mm:ss"));
            String finishDate = DateUtils.format(newDistributionPlan.getFinishDate(), "yyyy/MM/dd HH:mm:ss");
            distributionPlanVO.setFinishTime(finishDate.startsWith("1970") ? "" : finishDate);
            list.add(distributionPlanVO);
        }
        return list;
    }

}
