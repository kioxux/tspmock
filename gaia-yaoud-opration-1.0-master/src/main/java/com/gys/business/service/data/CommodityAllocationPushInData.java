package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:24 2021/10/21
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class CommodityAllocationPushInData {
    /**
     * 加盟商
     */
    private String client;

    /**
     * token
     */
    private String token;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 调拨方式 1:按门店 2:按线路
     */
    @NotBlank(message = "调拨方式不能为空！")
    @Pattern(regexp = "[1,2]",message = "调拨方式 1:按门店 2:按线路")
    private String allotType;

    /**
     * 线路星期
     */
    private List<String> xlxqs;

    /**
     * 调库单号
     */
    @NotBlank(message = "调库单号不能为空！")
    private String billNo;

    /**
     * 门店code
     */
    private List<String> stores;
}
