package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 传参实体
 *
 * @author xiaoyuan on 2020/9/11
 */
@Data
public class ReplenishmentRecordInData implements Serializable {
    private static final long serialVersionUID = 6029128233513721086L;


    @ApiModelProperty(value = "加盟商")
    private String client;


    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "商品编码")
    private String proSelfCode;

    @ApiModelProperty(value = "补货单号")
    private String gsrdVoucherId;

    @ApiModelProperty(value = "日期")
    private String gsrdDate;

}
