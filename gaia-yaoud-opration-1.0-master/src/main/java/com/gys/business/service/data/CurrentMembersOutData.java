package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/12/31
 */
@Data
public class CurrentMembersOutData implements Serializable {
    private static final long serialVersionUID = -6865509865310627621L;

    /**
     * 会员卡号
     */
    private String salesMemberCardNum;

    /**
     * 会员名称
     */
    private String salesMemberName;

    /**
     * 当前积分
     */
    private String pointsThisTime;

    /**
     * 总积分
     */
    private String gsmbcIntegral;
}
