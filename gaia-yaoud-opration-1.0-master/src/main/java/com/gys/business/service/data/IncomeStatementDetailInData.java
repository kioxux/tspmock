package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "批号养护时新增数据实体")
public class IncomeStatementDetailInData {
    private String gsisdVoucherId;
    @ApiModelProperty(value = "商品编码")
    private String gsisdProId;

    @ApiModelProperty(value = "商品名称", hidden = true)
    private String gsisdProName;

    @ApiModelProperty(value = "零售价", hidden = true)
    private String gsisdRetailPrice;

    @ApiModelProperty(value = "批号")
    private String gsisdBatchNo;

    @ApiModelProperty(hidden = true)
    private String gsisdBatch;

    @ApiModelProperty(hidden = true)
    private String gsisdDate;

    @ApiModelProperty(value = "有效期至", hidden = true)
    private String gsisdValidUntil;

    @ApiModelProperty(value = "库存数量", hidden = true)
    private String gsisdQty;

    @ApiModelProperty(value = "损益原因", hidden = true)
    private String gsisdIsCause;
    private String gsisdIsQty;

    @ApiModelProperty(value = "实际数量")
    private String gsisdRealQty;

    @ApiModelProperty(value = "损溢量")
    private String difference;

    @ApiModelProperty(value = "厂家", hidden = true)
    private String gsisdFactory;

    private int gsisdSerial;

    @ApiModelProperty(hidden = true)
    private String gsisdOrigin;

    @ApiModelProperty(hidden = true)
    private String gsisdDosageForm;

    @ApiModelProperty(hidden = true)
    private String gsisdUnit;

    @ApiModelProperty(hidden = true)
    private String gsisdFormat;

    @ApiModelProperty(hidden = true)
    private String gsisdApprovalNum;

    @ApiModelProperty(hidden = true)
    private String gsisdStockStatus;

    @ApiModelProperty(hidden = true)
    private Integer indexDetail;

    @ApiModelProperty(hidden = true)
    private String clientId;
    @ApiModelProperty(hidden = true)
    private String gsisdBrId;
}
