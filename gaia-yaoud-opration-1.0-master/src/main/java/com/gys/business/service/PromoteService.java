//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import java.util.List;
import java.util.Map;

public interface PromoteService {
    PageInfo<PromoteOutData> getList(PromoteInData inData);

    void update(PromoteInData inData, GetLoginOutData userInfo);

    void insert(PromoteInData inData, GetLoginOutData userInfo);

    PromoteOutData detail(PromoteInData inData);

    void approve(PromoteInData inData, GetLoginOutData userInfo);

    PromoteOutData getParam(PromoteInData inData, GetLoginOutData userInfo);

    List<RebornData> reSummaryGiftPromotion(String client, String brId, ReSummaryPromoteIndata reSummaryPromoteIndata);

    List<GiftPromOutData> queryGiftpromotion(PromotionMethodInData inData);

    /**
     * 检查促销限购
     * @param inData
     */
    boolean checkPromLimit(CheckPromLimitInData inData);
}
