package com.gys.business.service.data;

import com.gys.business.mapper.entity.*;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MQSaleOrderInData implements Serializable {
    private static final long serialVersionUID = -3402394137726844825L;
    private String client;
    private String brId;
    private GaiaSdSaleH saleH;
    private List<GaiaSdSaleD> saleDList;
    private GaiaIntoMemberCard IntoMemberCardInfo;
    private List<GaiaSdSalePayMsg> payMsgList;//支付明细
    private List<GaiaSdBatchChange> batchChangeList;//库存异动
    private List<ElectronDetailOutData> selectElectronList; //电子券列表
    private List<GaiaSdRechargeCard> selectRechargeCardList; //储值卡列表
    private List<GaiaSdSaleRecipel> saleRecipelList; //处方列表
    private List<RelatedSaleEject> saleEjectList;//关联推荐相关商品
    private List<GaiaSdSrSaleRecord> saleRInsertList;//销售成功关联推荐
    private List<GaiaSdSrRecord> srRecordList;//关联商品
    private List<SpecialClassOutData> specialClassOutDataList;//特殊分类商品
    private List<GaiaSdSendElectron> sendElectronList;//选择赠送的电子券
}
