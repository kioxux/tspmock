package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdStoreEssentialProductD;
import com.gys.business.service.data.EssentialProductCalculationInData;
import com.gys.business.service.data.SaveEssentialProductInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;

import java.util.List;

public interface StoreEssentialProductService {

    JsonResult getCalculationList(EssentialProductCalculationInData inData);

    JsonResult saveOrUpdate(SaveEssentialProductInData inData);

    JsonResult confirm(SaveEssentialProductInData inData);

    JsonResult getOrderList(EssentialProductCalculationInData inData);

    JsonResult getSourceDateList(GetLoginOutData userInfo);

    JsonResult getStoreList(EssentialProductCalculationInData inData);

    JsonResult getOrderDetailList(EssentialProductCalculationInData inData);

    Result export(EssentialProductCalculationInData inData);

    JsonResult getDisableFlag(String client);
}
