package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.gys.business.mapper.GaiaFranchiseeMapper;
import com.gys.business.mapper.GaiaGrossMarginIntervalMapper;
import com.gys.business.mapper.GaiaSdSaleDMapper;
import com.gys.business.mapper.GrossConstructionMapper;
import com.gys.business.mapper.entity.GaiaGrossMarginInterval;
import com.gys.business.mapper.entity.GrossConstruction;
import com.gys.business.service.GrossConstructionService;
import com.gys.business.service.data.GrossConstructionInData;
import com.gys.business.service.data.GrossConstructionOutData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:45 2021/9/16
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Service
@Slf4j
public class GrossConstructionServiceImpl implements GrossConstructionService {

    @Autowired
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Autowired
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;

    @Autowired
    private GaiaGrossMarginIntervalMapper gaiaGrossMarginIntervalMapper;

    @Autowired
    private GrossConstructionMapper grossConstructionMapper;

    @Override
    public List<Map<String, Object>> getTotalList(GrossConstructionInData inData) {
        List<GrossConstruction> grossConstructionList = grossConstructionMapper.bigClassTotalList(inData.getClient(), inData.getTime());
        if (ObjectUtil.isEmpty(grossConstructionList)) {
            return new ArrayList<>();
        }
        List<GrossConstructionOutData> resultList = new ArrayList<>();
        for (GrossConstruction grossConstruction : grossConstructionList) {
            if ("1".equals(grossConstruction.getProBigClassCode())) {
                grossConstruction.setProBigClassName("药品-" + grossConstruction.getProBigClassName());
            } else if ("2".equals(grossConstruction.getProBigClassCode())) {
                grossConstruction.setProBigClassName("药品-" + grossConstruction.getProBigClassName());
            }
            GrossConstructionOutData outData = dataCompute(grossConstruction);
            resultList.add(outData);
        }
        GrossConstruction ypTotal = getYpTotal(grossConstructionList, inData.getClient(), inData.getTime());
        GrossConstructionOutData ypTotalOutData = dataCompute(ypTotal);

        int index = 0;
        for (int i = 0; i < resultList.size(); i++) {
            if ("1".equals(resultList.get(i).getClassCode()) || "2".equals(resultList.get(i).getClassCode())) {
                index = i + 1;
            }
        }

        if (index != 0) {
            resultList.add(index, ypTotalOutData);
        }

        switch (inData.getOrderCode()) {
            case "1":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getPricingGrossRate().compareTo(o2.getPricingGrossRate()) : o2.getPricingGrossRate().compareTo(o1.getPricingGrossRate())));
                break;
            case "2":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getSaleGrossRate().compareTo(o2.getSaleGrossRate()) : o2.getSaleGrossRate().compareTo(o1.getSaleGrossRate())));
                break;
            case "3":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getGrossContributionRate().compareTo(o2.getGrossContributionRate()) : o2.getGrossContributionRate().compareTo(o1.getGrossContributionRate())));
                break;
            case "4":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getZkRate().compareTo(o2.getZkRate()) : o2.getZkRate().compareTo(o1.getZkRate())));
                break;
            case "5":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getProCount().compareTo(o2.getProCount()) : o2.getProCount().compareTo(o1.getProCount())));
                break;
            case "6":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getAmt().compareTo(o2.getAmt()) : o2.getAmt().compareTo(o1.getAmt())));
                break;
        }
        Map<String, List<GrossConstructionOutData>> map = new LinkedHashMap<>();
        for (GrossConstructionOutData outData : resultList) {
            map.put(outData.getClassCode() + ":" + outData.getClassName(), Arrays.asList(outData));
        }
        LinkedHashMap<String, List<GrossConstructionOutData>> resultMap = new LinkedHashMap<>();
        if ("1".equals(inData.getIsGrossInterval())) {
            for (Map.Entry<String, List<GrossConstructionOutData>> entry : map.entrySet()) {
                String[] split = entry.getKey().split(":");
                GrossConstructionInData data = new GrossConstructionInData();
                data.setClient(inData.getClient());
                data.setBigClassCode(split[0]);
                data.setTime(inData.getTime());
                List<GrossConstructionOutData> grossConstructions = getGrossConstruction(data);
                resultMap.put(entry.getKey(), grossConstructions);
            }
        } else {
            for (Map.Entry<String, List<GrossConstructionOutData>> entry : map.entrySet()) {
                resultMap.put(entry.getKey(), entry.getValue());
            }
        }
        List<Map<String, Object>> list = new ArrayList<>();
        for (Map.Entry<String, List<GrossConstructionOutData>> entry : resultMap.entrySet()) {
            LinkedHashMap<String, Object> hashMap = new LinkedHashMap<>();
            String[] split = entry.getKey().split(":");
            hashMap.put("name", split[1]);
            hashMap.put("list", entry.getValue());
            list.add(hashMap);
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> getMidGross(GrossConstructionInData inData) {
        ArrayList<GrossConstructionOutData> resultList = new ArrayList<>();
        ArrayList<String> bigClassCodes = new ArrayList<>();
        if (inData.getBigClassCode().contains(",")) {
            List<String> strings = Arrays.asList(inData.getBigClassCode().split(","));
            bigClassCodes.addAll(strings);
        } else {
            bigClassCodes.add(inData.getBigClassCode());
        }
        List<GrossConstruction> grossConstructionList = inData.getBigClassCode().contains(",") ? grossConstructionMapper.selectMidGroupByBigClassCode(inData.getClient(), inData.getTime(), bigClassCodes) : grossConstructionMapper.selectMidByBigClassCode(inData.getClient(), inData.getTime(), bigClassCodes);
        for (GrossConstruction grossConstruction : grossConstructionList) {
            GrossConstructionOutData outData = dataCompute(grossConstruction);
            resultList.add(outData);
        }
        switch (inData.getOrderCode()) {
            case "1":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getPricingGrossRate().compareTo(o2.getPricingGrossRate()) : o2.getPricingGrossRate().compareTo(o1.getPricingGrossRate())));
                break;
            case "2":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getSaleGrossRate().compareTo(o2.getSaleGrossRate()) : o2.getSaleGrossRate().compareTo(o1.getSaleGrossRate())));
                break;
            case "3":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getGrossContributionRate().compareTo(o2.getGrossContributionRate()) : o2.getGrossContributionRate().compareTo(o1.getGrossContributionRate())));
                break;
            case "4":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getZkRate().compareTo(o2.getZkRate()) : o2.getZkRate().compareTo(o1.getZkRate())));
                break;
            case "5":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getProCount().compareTo(o2.getProCount()) : o2.getProCount().compareTo(o1.getProCount())));
                break;
            case "6":
                resultList.sort(((o1, o2) -> "1".equals(inData.getOrderType()) ? o1.getAmt().compareTo(o2.getAmt()) : o2.getAmt().compareTo(o1.getAmt())));
                break;
        }

        Map<String, List<GrossConstructionOutData>> map = new LinkedHashMap<>();
        for (GrossConstructionOutData outData : resultList) {
            map.put(outData.getClassCode() + ":" + outData.getClassName(), Arrays.asList(outData));
        }
        LinkedHashMap<String, List<GrossConstructionOutData>> resultMap = new LinkedHashMap<>();
        if ("1".equals(inData.getIsGrossInterval())) {
            for (Map.Entry<String, List<GrossConstructionOutData>> entry : map.entrySet()) {
                String[] split = entry.getKey().split(":");
                ArrayList<GrossConstructionOutData> outDataList = new ArrayList<>();
                List<GrossConstruction> grossConstructions = inData.getBigClassCode().contains(",") ? grossConstructionMapper.selectMidGroupByMidClassCode(inData.getClient(), inData.getTime(), bigClassCodes, split[0].substring(1)) : grossConstructionMapper.selectMidByMidClassCode(inData.getClient(), inData.getTime(), split[0]);
                for (GrossConstruction grossConstruction : grossConstructions) {
                    GrossConstructionOutData outData = dataCompute(grossConstruction);
                    outDataList.add(outData);
                }
                for (GrossConstructionOutData outData : entry.getValue()) {
                    outData.setInterval("小计");
                    outDataList.add(outData);
                }
                resultMap.put(entry.getKey(), outDataList);
            }
        } else {
            for (Map.Entry<String, List<GrossConstructionOutData>> entry : map.entrySet()) {
                resultMap.put(entry.getKey(), entry.getValue());
            }
        }
        List<Map<String, Object>> list = new ArrayList<>();
        for (Map.Entry<String, List<GrossConstructionOutData>> entry : resultMap.entrySet()) {
            String[] split = entry.getKey().split(":");
            LinkedHashMap<String, Object> hashMap = new LinkedHashMap<>();
            hashMap.put("name", split[1]);
            hashMap.put("list", entry.getValue());
            list.add(hashMap);
        }
        return list;
    }

    @Override
    public List<GrossConstructionOutData> getGrossConstruction(GrossConstructionInData inData) {
        ArrayList<GrossConstructionOutData> resultList = new ArrayList<>();
        List<GrossConstruction> grossConstructions;
        if (inData.getBigClassCode().contains(",")) {
            String[] split = inData.getBigClassCode().split(",");
            List<String> classCodes = Arrays.asList(split);
            grossConstructions = grossConstructionMapper.selectBigGroupByBigClassCodes(inData.getClient(), inData.getTime(), classCodes);
            for (GrossConstruction grossConstruction : grossConstructions) {
                grossConstruction.setClassType(1);
                grossConstruction.setProBigClassCode(inData.getBigClassCode());
                grossConstruction.setProBigClassName("合计");
                if ("1,2".equals(inData.getBigClassCode())) {
                    grossConstruction.setProBigClassName("药品-小计");
                }
                GrossConstructionOutData outData = dataCompute(grossConstruction);
                resultList.add(outData);
            }

        } else {
            grossConstructions = grossConstructionMapper.selectByBigClassCode(inData.getClient(), inData.getTime(), inData.getBigClassCode(), 1);
            for (GrossConstruction grossConstruction : grossConstructions) {
                GrossConstructionOutData outData = dataCompute(grossConstruction);
                resultList.add(outData);
            }
        }
        BigDecimal pricingTotal = grossConstructions.stream().map(gro -> gro.getGgcPricing()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal amtTotal = grossConstructions.stream().map(gro -> gro.getGgcAmt()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal movTotal = grossConstructions.stream().map(gro -> gro.getGgcMov()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal zkAmtTotal = grossConstructions.stream().map(gro -> gro.getGgcZkAmt()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal saleProportionTotal = grossConstructions.stream().map(gro -> gro.getGgcSaleProportion()).reduce(BigDecimal.ZERO, BigDecimal::add);
        Integer proCountTotal = grossConstructions.stream().mapToInt(o -> o.getGgcProCount() == null || new Integer(0).equals(o.getGgcProCount()) ? new Integer(0) : o.getGgcProCount()).sum();
        GrossConstruction total = new GrossConstruction(null, inData.getClient(), null, null, null, inData.getBigClassCode(), "合计", null, null, null, null, 1, inData.getTime(), amtTotal, zkAmtTotal, pricingTotal, saleProportionTotal, movTotal, proCountTotal);
        GrossConstructionOutData outData = dataCompute(total);
        outData.setInterval("合计");
        resultList.add(outData);
        return resultList;
    }

    @Override
    public List<GrossConstructionOutData> bigClassList(GrossConstructionInData inData) {
        List<GrossConstruction> grossConstructionList = grossConstructionMapper.bigClassTotalList(inData.getClient(), inData.getTime());
        if (ObjectUtil.isEmpty(grossConstructionList)) {
            return new ArrayList<>();
        }
        BigDecimal pricingTotal = grossConstructionList.stream().map(gro -> gro.getGgcPricing()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal amtTotal = grossConstructionList.stream().map(gro -> gro.getGgcAmt()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal movTotal = grossConstructionList.stream().map(gro -> gro.getGgcMov()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal zkAmtTotal = grossConstructionList.stream().map(gro -> gro.getGgcZkAmt()).reduce(BigDecimal.ZERO, BigDecimal::add);
        Integer proCountTotal = grossConstructionList.stream().mapToInt(o -> o.getGgcProCount() == null || new Integer(0).equals(o.getGgcProCount()) ? new Integer(0) : o.getGgcProCount()).sum();
        String classCodeTotal = "";
        for (GrossConstruction grossConstruction : grossConstructionList) {
            classCodeTotal += grossConstruction.getProBigClassCode();
            classCodeTotal += ",";
        }
        classCodeTotal = classCodeTotal.substring(0, classCodeTotal.length() - 1);
        GrossConstruction total = new GrossConstruction(null, inData.getClient(), null, null, null, classCodeTotal, "合计", null, null, null, null, 1, inData.getTime(), amtTotal, zkAmtTotal, pricingTotal, BigDecimal.ONE, movTotal, proCountTotal);
        ArrayList<GrossConstructionOutData> resultList = new ArrayList<>();
        GrossConstructionOutData totalOutData = dataCompute(total);
        resultList.add(totalOutData);
        for (GrossConstruction grossConstruction : grossConstructionList) {
            if ("1".equals(grossConstruction.getProBigClassCode())) {
                String proBigClassName = "药品-" + grossConstruction.getProBigClassName();
                grossConstruction.setProBigClassName(proBigClassName);
            } else if ("2".equals(grossConstruction.getProBigClassCode())) {
                String proBigClassName = "药品-" + grossConstruction.getProBigClassName();
                grossConstruction.setProBigClassName(proBigClassName);
            }
            GrossConstructionOutData outData = dataCompute(grossConstruction);
            resultList.add(outData);
        }
        GrossConstruction ypTotal = getYpTotal(grossConstructionList, inData.getClient(), inData.getTime());
        GrossConstructionOutData ypTotalOutData = dataCompute(ypTotal);

        int index = 0;
        for (int i = 0; i < resultList.size(); i++) {
            if ("1".equals(resultList.get(i).getClassCode()) || "2".equals(resultList.get(i).getClassCode())) {
                index = i + 1;
            }
        }

        if (index != 0) {
            resultList.add(index, ypTotalOutData);
        }
        return resultList;
    }

    private GrossConstruction getYpTotal(List<GrossConstruction> AllGroList, String client, String time) {
        //BigDecimal amtTotal = AllGroList.stream().map(gro -> gro.getGgcAmt()).reduce(BigDecimal.ZERO, BigDecimal::add);
        List<GrossConstruction> collect = AllGroList.stream().filter(gro -> "1".equals(gro.getProBigClassCode()) || "2".equals(gro.getProBigClassCode())).collect(Collectors.toList());
        BigDecimal ypPricingTotal = collect.stream().map(gro -> gro.getGgcPricing()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal ypAmtTotal = collect.stream().map(gro -> gro.getGgcAmt()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal ypMovTotal = collect.stream().map(gro -> gro.getGgcMov()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal ypZkAmtTotal = collect.stream().map(gro -> gro.getGgcZkAmt()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal ypSaleProportionTotal = collect.stream().map(gro -> gro.getGgcSaleProportion()).reduce(BigDecimal.ZERO, BigDecimal::add);
        Integer ypProCountTotal = collect.stream().mapToInt(o -> o.getGgcProCount() == null || new Integer(0).equals(o.getGgcProCount()) ? new Integer(0) : o.getGgcProCount()).sum();
        String ypClassCodeTotal = "";
        for (GrossConstruction grossConstruction : collect) {
            ypClassCodeTotal += grossConstruction.getProBigClassCode();
            ypClassCodeTotal += ",";
        }
        ypClassCodeTotal = ypClassCodeTotal.substring(0, ypClassCodeTotal.length() - 1);
        GrossConstruction ypTotal = new GrossConstruction(null, client, null, null, null, ypClassCodeTotal, "药品-小计", null, null, null, null, 1, time, ypAmtTotal, ypZkAmtTotal, ypPricingTotal, ypSaleProportionTotal, ypMovTotal, ypProCountTotal);
        return ypTotal;
    }

    private GrossConstructionOutData dataCompute(GrossConstruction grossConstruction) {
        GrossConstructionOutData outData = new GrossConstructionOutData();
        outData.setPricingGrossRate(BigDecimal.ZERO.compareTo(grossConstruction.getGgcPricing()) == 0 ? BigDecimal.ZERO : grossConstruction.getGgcPricing().subtract(grossConstruction.getGgcMov()).divide(grossConstruction.getGgcPricing(), 3, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP));
        outData.setSaleGrossRate(BigDecimal.ZERO.compareTo(grossConstruction.getGgcAmt()) == 0 ? BigDecimal.ZERO : grossConstruction.getGgcAmt().subtract(grossConstruction.getGgcMov()).divide(grossConstruction.getGgcAmt(), 3, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP));
        outData.setGrossContributionRate(BigDecimal.ZERO.compareTo(grossConstruction.getGgcAmt()) == 0 ? BigDecimal.ZERO : grossConstruction.getGgcAmt().subtract(grossConstruction.getGgcMov()).divide(grossConstruction.getGgcAmt(), 3, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).multiply(grossConstruction.getGgcSaleProportion()).setScale(1, BigDecimal.ROUND_HALF_UP));
        outData.setZkRate(BigDecimal.ZERO.compareTo(grossConstruction.getGgcPricing()) == 0 ? BigDecimal.ZERO : grossConstruction.getGgcZkAmt().divide(grossConstruction.getGgcPricing(), 3, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP));
        outData.setProCount(grossConstruction.getGgcProCount());
        outData.setSaleProportion(grossConstruction.getGgcSaleProportion().multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP));
        outData.setAmt(grossConstruction.getGgcAmt().setScale(0, BigDecimal.ROUND_HALF_UP));
        outData.setClassCode(grossConstruction.getClassType() == 1 ? grossConstruction.getProBigClassCode() : grossConstruction.getProMidClassCode());
        outData.setClassName(grossConstruction.getClassType() == 1 ? grossConstruction.getProBigClassName() : grossConstruction.getProMidClassName());
        if (ObjectUtil.isNotEmpty(grossConstruction.getEndValue()) && ObjectUtil.isNotEmpty(grossConstruction.getStartValue())) {
            if (grossConstruction.getStartValue().compareTo(new BigDecimal(-999999)) <= 0) {
                outData.setInterval("X≤" + grossConstruction.getEndValue().setScale(0, BigDecimal.ROUND_HALF_UP) + "%");
            } else if (grossConstruction.getEndValue().compareTo(new BigDecimal(999999)) >= 0) {
                outData.setInterval(grossConstruction.getStartValue().setScale(0, BigDecimal.ROUND_HALF_UP) + "%以上");
            } else {
                outData.setInterval(grossConstruction.getStartValue().setScale(0, BigDecimal.ROUND_HALF_UP) + "-" + grossConstruction.getEndValue().setScale(0, BigDecimal.ROUND_HALF_UP) + "%(含)");
            }
        }
        return outData;
    }

    @Override
    @Transactional
    public void grossConstructionJob(String xxlData) {
        Map<String, List<String>> xxlParamMap = new HashMap<>();
        List<String> clients;
        List<String> dates;
        if (StringUtils.isNotEmpty(xxlData)) {
            xxlParamMap = JSON.parseObject(xxlData, (Type) Map.class);
        }
        if (ObjectUtil.isNotEmpty(xxlParamMap.get("clients"))) {
            clients = xxlParamMap.get("clients");
        } else {
            clients = gaiaFranchiseeMapper.getAllClientId();
        }
        if (ObjectUtil.isNotEmpty(xxlParamMap.get("dates"))) {
            dates = xxlParamMap.get("dates");
        } else {
            Calendar instance = Calendar.getInstance();
            instance.setTime(new Date());
            instance.add(Calendar.MONTH, -1);
            dates = Lists.newArrayList(DateUtil.format(instance.getTime(), "yyyyMM"));
        }
        log.info("<毛利结构定时任务><执行参数>clients:{},dates:{}", clients, dates);
        for (String date : dates) {
            saveGrossConstruction(clients, date, "1");
            log.info("<毛利结构定时任务><大类执行完成>clients:{},date:{}", clients, date);
            saveGrossConstruction(clients, date, "2");
            log.info("<毛利结构定时任务><中类执行完成>clients:{},date:{}", clients, date);
        }
    }

    private void saveGrossConstruction(List<String> clients, String date, String type) {
        String startDate = date + "01";
        String endDate = date + "31";
        List<GrossConstruction> grossConstructionList = "1".equals(type) ? gaiaSdSaleDMapper.selectProBigClassByClientAndMonth(clients, startDate, endDate) : gaiaSdSaleDMapper.selectProMidClassByClientAndMonth(clients, startDate, endDate);
        List<GaiaGrossMarginInterval> grossMarginIntervalList = gaiaGrossMarginIntervalMapper.getByClients(clients);
        Map<String, List<GaiaGrossMarginInterval>> clientMap = grossMarginIntervalList.stream().collect(Collectors.groupingBy(GaiaGrossMarginInterval::getClient));
        //client销售额总计
        Map<String, List<GrossConstruction>> totalAmtCollect = grossConstructionList.stream().collect(Collectors.groupingBy(GrossConstruction::getClient));
        Map<String, BigDecimal> totalAmtMap = new HashMap<>();
        for (Map.Entry<String, List<GrossConstruction>> entry : totalAmtCollect.entrySet()) {
            BigDecimal reduce = entry.getValue().stream().map(GrossConstruction::getGgcAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
            totalAmtMap.put(entry.getKey(), reduce);
        }
        //client对应商品分类
        Map<String, List<GaiaGrossMarginInterval>> defaultGroMap = new HashMap<>();
        for (Map.Entry<String, List<GrossConstruction>> entry : totalAmtCollect.entrySet()) {
            Set<String> collect = entry.getValue().stream().map(
                    gro -> gro.getClient()
                            + "-" + gro.getProBigClassCode()
                            + "-" + gro.getProBigClassName()
                            + "-" + gro.getProMidClassCode()
                            + "-" + gro.getProMidClassName()
                            + "-" + gro.getProClassCode()
                            + "-" + gro.getProClassName()
            ).collect(Collectors.toSet());
            for (String key : collect) {
                defaultGroMap.put(key, clientMap.get(entry.getKey()));
            }
        }
        //client、商品分类销售额合计
        Map<String, List<GrossConstruction>> proClassAmtMap = "1".equals(type) ?
                grossConstructionList.stream().collect(Collectors.groupingBy(grossConstruction -> grossConstruction.getClient() + "-" + grossConstruction.getProBigClassCode())) :
                grossConstructionList.stream().collect(Collectors.groupingBy(grossConstruction -> grossConstruction.getClient() + "-" + grossConstruction.getProMidClassCode()));
        Map<String, BigDecimal> proTotalMap = new HashMap<>();
        for (Map.Entry<String, List<GrossConstruction>> entry : proClassAmtMap.entrySet()) {
            BigDecimal reduce = entry.getValue().stream().map(GrossConstruction::getGgcAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
            proTotalMap.put(entry.getKey(), reduce);
        }
        List<GrossConstruction> insertList = new ArrayList<>();
        for (GrossConstruction grossConstruction : grossConstructionList) {
            String defaultKey = grossConstruction.getClient()
                    + "-" + grossConstruction.getProBigClassCode()
                    + "-" + grossConstruction.getProBigClassName()
                    + "-" + grossConstruction.getProMidClassCode()
                    + "-" + grossConstruction.getProMidClassName()
                    + "-" + grossConstruction.getProClassCode()
                    + "-" + grossConstruction.getProClassName();
            List<GaiaGrossMarginInterval> intervals = defaultGroMap.get(defaultKey);
            if (ObjectUtil.isNotEmpty(intervals)) {
                List<GaiaGrossMarginInterval> collect = intervals.stream().filter(gro -> grossConstruction.getIntervalType() == null || !grossConstruction.getIntervalType().equals(gro.getIntervalType())).collect(Collectors.toList());
                defaultGroMap.put(defaultKey, collect);
            }
            BigDecimal totalAmt = totalAmtMap.get(grossConstruction.getClient());
            grossConstruction.setGgcSaleProportion(totalAmt.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : grossConstruction.getGgcAmt().divide(totalAmt, 4, BigDecimal.ROUND_HALF_UP));
            grossConstruction.setSaleMonth(date);
            grossConstruction.setClassType(Integer.parseInt(type));
            insertList.add(grossConstruction);
        }
        for (Map.Entry<String, List<GaiaGrossMarginInterval>> entry : defaultGroMap.entrySet()) {
            String[] split = entry.getKey().split("-");
            String client = "null".equals(split[0]) ? null : split[0];
            String proBigClassCode = "null".equals(split[1]) ? null : split[1];
            String proBigClassName = "null".equals(split[2]) ? null : split[2];
            String proMidClassCode = "null".equals(split[3]) ? null : split[3];
            String proMidClassName = "null".equals(split[4]) ? null : split[4];
            String proClassCode = "null".equals(split[5]) ? null : split[5];
            String proClassName = "null".equals(split[6]) ? null : split[6];

            if (ObjectUtil.isEmpty(entry.getValue())) {
                continue;
            }
            for (GaiaGrossMarginInterval gaiaGrossMarginInterval : entry.getValue()) {
                GrossConstruction grossConstruction = new GrossConstruction();
                grossConstruction.setClient(client);
                grossConstruction.setIntervalType(gaiaGrossMarginInterval.getIntervalType());
                grossConstruction.setStartValue(gaiaGrossMarginInterval.getStartValue());
                grossConstruction.setEndValue(gaiaGrossMarginInterval.getEndValue());
                grossConstruction.setProBigClassCode(proBigClassCode);
                grossConstruction.setProBigClassName(proBigClassName);
                grossConstruction.setProMidClassCode(proMidClassCode);
                grossConstruction.setProMidClassName(proMidClassName);
                grossConstruction.setProClassCode(proClassCode);
                grossConstruction.setProClassName(proClassName);
                grossConstruction.setClassType(Integer.parseInt(type));
                grossConstruction.setSaleMonth(date);
                grossConstruction.setGgcAmt(BigDecimal.ZERO);
                grossConstruction.setGgcZkAmt(BigDecimal.ZERO);
                grossConstruction.setGgcPricing(BigDecimal.ZERO);
                BigDecimal totalAmt = totalAmtMap.get(grossConstruction.getClient());
                grossConstruction.setGgcSaleProportion(totalAmt.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : BigDecimal.ZERO.divide(totalAmt, 4, BigDecimal.ROUND_HALF_UP));
                grossConstruction.setGgcMov(BigDecimal.ZERO);
                grossConstruction.setGgcProCount(0);
                insertList.add(grossConstruction);
            }
        }
        if (ObjectUtil.isNotEmpty(insertList)) {
            grossConstructionMapper.deleteByClientAndMonth(clients, date, type);
            grossConstructionMapper.batchInsert(insertList);
        }
    }
}