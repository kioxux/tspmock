package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2020/12/2
 */
@Data
public class GaiaSdMemberData implements Serializable {
    private static final long serialVersionUID = -7754305449935292023L;

    private List<GaiaSdMemberVO> businessVOOne;

    private List<String> tableNames;
}
