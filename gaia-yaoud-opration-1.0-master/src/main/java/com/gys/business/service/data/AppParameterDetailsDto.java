package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2021/3/23
 */
@Data
public class AppParameterDetailsDto implements Serializable {
    private static final long serialVersionUID = -6693499947662212631L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 销售单号
     */
    private String gsshBillNo;

    /**
     * 店号
     */
    private String gsshBrId;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 销售时间
     */
    private String gsshTime;

    /**
     * 收银工号
     */
    private String gsshEmp;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 厂家
     */
    private String proFactoryName;


    /**
     * 单位
     */
    private String proUnit;

    /**
     * 规格
     */
    private String proSpecs;


    /**
     * 行号
     */
    private String gssdSerial;

    /**
     * 商品编码
     */
    private String gssdProId;

    /**
     * 商品批号
     */
    private String gssdBatchNo;

    /**
     * 商品有效期
     */
    private String gssdValidDate;


    /**
     * 单品应收价
     */
    private String gssdPrc2;

    /**
     * 数量
     */
    private String gssdQty;

    /**
     * 行金额
     */
    private String gssdAmt;


}
