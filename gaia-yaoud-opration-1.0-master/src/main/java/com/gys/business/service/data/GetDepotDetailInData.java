//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "退库明细")
public class GetDepotDetailInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "退库单号")
    private String gsrddVoucherId;
    @ApiModelProperty(value = "退库日期")
    private String gsrddDate;
    @ApiModelProperty(value = "行号")
    private String gsrddSerial;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "批号")
    private String BatchNo;
    @ApiModelProperty(value = "批次")
    private String Batch;
    @ApiModelProperty(value = "有效期")
    private String ValidDate;
    @ApiModelProperty(value = "批号库存数量")
    private String stockQty;
    @ApiModelProperty(value = "退库数量")
    private String gsrddRetdepQty;
    @ApiModelProperty(value = "退库原因")
    private String gsrddRetdepCause;
    @ApiModelProperty(value = "发起数量")
    private String gsrddStartQty;
    @ApiModelProperty(value = "实退数量")
    private String gsrddReviseQty;
    @ApiModelProperty(value = "库存状态")
    private String gsrddStockStatus;
    @ApiModelProperty(value = "商品名称")
    private String proName;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "剂型")
    private String proForm;
    @ApiModelProperty(value = "价格")
    private String proPrice;
    @ApiModelProperty(value = "计量单位")
    private String proUnit;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "采购单号")
    private String poId;
    @ApiModelProperty(value = "订单行号")
    private String poLineNo;
    @ApiModelProperty(value = "采购单价")
    private String poPrice;
    @ApiModelProperty(value = "税率")
    private String poRate;
    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;

    public GetDepotDetailInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsrddVoucherId() {
        return this.gsrddVoucherId;
    }

    public String getGsrddDate() {
        return this.gsrddDate;
    }

    public String getGsrddSerial() {
        return this.gsrddSerial;
    }

    public String getProCode() {
        return proCode;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public String getBatch() {
        return Batch;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public String getStockQty() {
        return stockQty;
    }

    public String getGsrddRetdepQty() {
        return this.gsrddRetdepQty;
    }

    public String getGsrddRetdepCause() {
        return this.gsrddRetdepCause;
    }

    public String getGsrddStartQty() {
        return this.gsrddStartQty;
    }

    public String getGsrddReviseQty() {
        return this.gsrddReviseQty;
    }

    public String getGsrddStockStatus() {
        return this.gsrddStockStatus;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProForm() {
        return this.proForm;
    }

    public String getProPrice() {
        return this.proPrice;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getPoId() {
        return this.poId;
    }

    public String getPoLineNo() {
        return this.poLineNo;
    }

    public String getPoPrice() {
        return this.poPrice;
    }

    public String getPoRate() {
        return this.poRate;
    }

    public String getProRegisterNo() {
        return this.proRegisterNo;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsrddVoucherId(final String gsrddVoucherId) {
        this.gsrddVoucherId = gsrddVoucherId;
    }

    public void setGsrddDate(final String gsrddDate) {
        this.gsrddDate = gsrddDate;
    }

    public void setGsrddSerial(final String gsrddSerial) {
        this.gsrddSerial = gsrddSerial;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public void setBatchNo(String batchNo) {
        BatchNo = batchNo;
    }

    public void setBatch(String batch) {
        Batch = batch;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    public void setStockQty(String stockQty) {
        this.stockQty = stockQty;
    }

    public void setGsrddRetdepQty(final String gsrddRetdepQty) {
        this.gsrddRetdepQty = gsrddRetdepQty;
    }

    public void setGsrddRetdepCause(final String gsrddRetdepCause) {
        this.gsrddRetdepCause = gsrddRetdepCause;
    }

    public void setGsrddStartQty(final String gsrddStartQty) {
        this.gsrddStartQty = gsrddStartQty;
    }

    public void setGsrddReviseQty(final String gsrddReviseQty) {
        this.gsrddReviseQty = gsrddReviseQty;
    }

    public void setGsrddStockStatus(final String gsrddStockStatus) {
        this.gsrddStockStatus = gsrddStockStatus;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public void setProPrice(final String proPrice) {
        this.proPrice = proPrice;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setPoId(final String poId) {
        this.poId = poId;
    }

    public void setPoLineNo(final String poLineNo) {
        this.poLineNo = poLineNo;
    }

    public void setPoPrice(final String poPrice) {
        this.poPrice = poPrice;
    }

    public void setPoRate(final String poRate) {
        this.poRate = poRate;
    }

    public void setProRegisterNo(final String proRegisterNo) {
        this.proRegisterNo = proRegisterNo;
    }

}
