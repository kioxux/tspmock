package com.gys.business.service.data.DtYb.Physical;

import lombok.Data;

/**
 * @Description 大同易联众 盘点商品
 * @Author huxinxin
 * @Date 2021/5/11 13:38
 * @Version 1.0.0
 **/
@Data
public class PhysicalProVO {
    // 药品编号
    private String proCode;
    // 药品名称
    private String proName;
    // 药品通用名称
    private String proCommonName;
    // 单位
    private String unit;
    // 剂型
    private String form;
    // 规格
    private String specs;
    // 实际数量
    private String realQty;
    // 药品批号
    private String batchNo;
    // 生产日期
    private String productionDate;
    // 有效日期
    private String expiryDate;
    // 进价
    private String price;
    // 入库编号
    private String stockcode;
}
