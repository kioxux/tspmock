package com.gys.business.service.data;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class MessageParams {
    /**
     * 消息模板
     */
    private String id;

    /**
     * 标题参数
     */
    private List<String> titleParams;

    /**
     * 内容参数
     */
    private List<String> contentParmas;

    /**
     * 自定义参数
     */
    private Map<String, String> params;

    /**
     * 接收人加盟商
     */
    private String client;

    /**
     * 接收用户
     */
    private String userId;

    /**
     * 接收消息用户
     */
    private List<String> userIdList;
}
