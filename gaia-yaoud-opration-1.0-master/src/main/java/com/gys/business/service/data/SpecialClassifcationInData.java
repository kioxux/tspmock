package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel("特殊分类药品实体类")
@Data
public class SpecialClassifcationInData implements Serializable {
    private static final long serialVersionUID = 1552533435876488352L;
    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "商品")
    private String proId = "";


    /**
     * 店号
     */
    @ApiModelProperty(value = "店名")
    private String brId;
    /**
     * 销售单号
     */
    @ApiModelProperty(value = "销售单号")
    private String gsscBillNo;

    /**
     * 店名
     */
    @NotNull(message = "门店不可为空")
    @ApiModelProperty(value = "门店名称")
    private String[] brIdList;

    /**
     * 销售日期
     */
    @ApiModelProperty(value = "商品编码")
    private String gsscProId;

    /**
     * 销售日期
     */
    @ApiModelProperty(value = "销售日期")
    private String gsscDate;



    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证")
    private String gsscIdcard;

    /**
     * 顾客姓名
     */
    @ApiModelProperty(value = "顾客姓名")
    private String gsscName;


    /**
     * 出生年月
     */
    @ApiModelProperty(value = "出生年月")
    private String gsscBirthday;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String gsscMobile;

    @ApiModelProperty(value = "特殊分类")
    private String  gsscType;
}
