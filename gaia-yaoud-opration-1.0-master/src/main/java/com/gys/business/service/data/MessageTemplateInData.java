package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Data
public class MessageTemplateInData implements Serializable {
    private static final long serialVersionUID = -6639629442336052021L;

    @ApiModelProperty(value = "GMT_ID")
    private String gmtId;

    @NotBlank(message = "消息名称不能为空")
    @ApiModelProperty(value = "消息名称")
    private String gmtName;

    @ApiModelProperty(value = "消息标题")
    private String gmtTitle;

//    @NotBlank(message = "消息内容不能为空")
    @ApiModelProperty(value = "消息内容")
    private String gmtContent;

    @NotBlank(message = "业务类型不能为空")
    @ApiModelProperty(value = "业务类型")
    private Integer gmtBusinessType;

    @NotBlank(message = "是否跳转不能为空")
    @ApiModelProperty(value = "是否跳转")
    private Integer gmtGoPage;

    @NotBlank(message = "消息类型不能为空")
    @ApiModelProperty(value = "消息类型")
    private Integer gmtType;

    @NotBlank(message = "表示形式不能为空")
    @ApiModelProperty(value = "表示形式")
    private Integer gmtShowType;

    @ApiModelProperty(value = "是否启用 0停用 1启用 2已保存")
    private Integer gmtFlag;

    @ApiModelProperty(value = "按日期循环1勾选0未勾选")
    private String gmtSendDate;

    @ApiModelProperty(value = "按日期循环设置值")
    private Integer gmtSendDateSettingNum;

    @ApiModelProperty(value = "按星期循环 1勾选0未勾选")
    private String gmtSendWeek;

    @ApiModelProperty(value = "按星期循环 设置值")
    private Integer gmtSendWeekSettingNum;

    @ApiModelProperty(value = "按活动设置 1勾选0未勾选")
    private String gmtSendActivity;

    @ApiModelProperty(value = "按活动设置值")
    private Integer gmtSendActivitySettingNum;

    @NotBlank(message = "推送时间不能为空")
    @ApiModelProperty(value = "推送时间")
    private String gmtSendTime;

    @NotBlank(message = "门店人员是否显示毛利数据不能为空")
    @ApiModelProperty(value = "门店人员是否显示毛利数据 1表示打勾 0表示未打勾")
    private String gmtIfShowMao;

    @ApiModelProperty(value = "启用或者停用 1立即启用 0立即停用 2定时启用 3 定时停用 ")
    private String effectMode;

    @ApiModelProperty(value = "启用生效日期")
    private String effectDate;

    @ApiModelProperty(value = "启用生效时间")
    private String effectTime;

    @ApiModelProperty(value = "true:选择所有加盟商， false|null:选择指定加盟商")
    private Boolean chooseAllClient;

    @ApiModelProperty(value = "选择的加盟商")
    private List<String> clients;

    @ApiModelProperty(value = "true:选择所有岗位， false|null:选择指定岗位")
    private Boolean chooseAllPosition;

    @ApiModelProperty(value = "选择的职位")
    private List<String> positions;

    @ApiModelProperty(value = "开始时间")
    private String startTime;

    @ApiModelProperty(value = "结束时间")
    private String endTime;

    private Integer pageSize;

    private Integer pageNum;

}
