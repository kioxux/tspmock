//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;

public class AddProTableOutData {
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "规格")
    private String specs;

    public AddProTableOutData() {
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getProName() {
        return this.proName;
    }

    public String getSpecs() {
        return this.specs;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setSpecs(final String specs) {
        this.specs = specs;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof AddProTableOutData)) {
            return false;
        } else {
            AddProTableOutData other = (AddProTableOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$proCode = this.getProCode();
                    Object other$proCode = other.getProCode();
                    if (this$proCode == null) {
                        if (other$proCode == null) {
                            break label47;
                        }
                    } else if (this$proCode.equals(other$proCode)) {
                        break label47;
                    }

                    return false;
                }

                Object this$proName = this.getProName();
                Object other$proName = other.getProName();
                if (this$proName == null) {
                    if (other$proName != null) {
                        return false;
                    }
                } else if (!this$proName.equals(other$proName)) {
                    return false;
                }

                Object this$specs = this.getSpecs();
                Object other$specs = other.getSpecs();
                if (this$specs == null) {
                    if (other$specs != null) {
                        return false;
                    }
                } else if (!this$specs.equals(other$specs)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof AddProTableOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $specs = this.getSpecs();
        result = result * 59 + ($specs == null ? 43 : $specs.hashCode());
        return result;
    }

    public String toString() {
        return "AddProTableOutData(proCode=" + this.getProCode() + ", proName=" + this.getProName() + ", specs=" + this.getSpecs() + ")";
    }
}
