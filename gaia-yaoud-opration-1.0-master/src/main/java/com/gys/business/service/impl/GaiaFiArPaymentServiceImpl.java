package com.gys.business.service.impl;

import com.gys.business.mapper.entity.GaiaFiArPayment;
import com.gys.business.mapper.GaiaFiArPaymentMapper;
import com.gys.business.service.GaiaFiArPaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 应收方式维护表(GaiaFiArPayment)表服务实现类
 *
 * @author makejava
 * @since 2021-09-18 15:29:32
 */
@Service("gaiaFiArPaymentService")
public class GaiaFiArPaymentServiceImpl implements GaiaFiArPaymentService {
    @Resource
    private GaiaFiArPaymentMapper gaiaFiArPaymentDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public GaiaFiArPayment queryById(Long id) {
        return this.gaiaFiArPaymentDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<GaiaFiArPayment> queryAllByLimit(int offset, int limit) {
        return this.gaiaFiArPaymentDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param gaiaFiArPayment 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaFiArPayment insert(GaiaFiArPayment gaiaFiArPayment) {
        this.gaiaFiArPaymentDao.insert(gaiaFiArPayment);
        return gaiaFiArPayment;
    }

    /**
     * 修改数据
     *
     * @param gaiaFiArPayment 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaFiArPayment update(GaiaFiArPayment gaiaFiArPayment) {
        this.gaiaFiArPaymentDao.update(gaiaFiArPayment);
        return this.queryById(gaiaFiArPayment.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.gaiaFiArPaymentDao.deleteById(id) > 0;
    }
}
