//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GaiaSdRechargeCardChangeOutData {
    private String clientId;
    private String gsrccVoucherId;
    private String brName;
    private String gsrccStatus;
    private String gsrccBrId;
    private String gsrccDate;
    private String gsrccTime;
    private String gsrccEmp;
    private String gsrccOldCardId;
    private String gsrccNewCardId;
    private String gsrccRemark;

    public GaiaSdRechargeCardChangeOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsrccVoucherId() {
        return this.gsrccVoucherId;
    }

    public String getBrName() {
        return this.brName;
    }

    public String getGsrccStatus() {
        return this.gsrccStatus;
    }

    public String getGsrccBrId() {
        return this.gsrccBrId;
    }

    public String getGsrccDate() {
        return this.gsrccDate;
    }

    public String getGsrccTime() {
        return this.gsrccTime;
    }

    public String getGsrccEmp() {
        return this.gsrccEmp;
    }

    public String getGsrccOldCardId() {
        return this.gsrccOldCardId;
    }

    public String getGsrccNewCardId() {
        return this.gsrccNewCardId;
    }

    public String getGsrccRemark() {
        return this.gsrccRemark;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsrccVoucherId(final String gsrccVoucherId) {
        this.gsrccVoucherId = gsrccVoucherId;
    }

    public void setBrName(final String brName) {
        this.brName = brName;
    }

    public void setGsrccStatus(final String gsrccStatus) {
        this.gsrccStatus = gsrccStatus;
    }

    public void setGsrccBrId(final String gsrccBrId) {
        this.gsrccBrId = gsrccBrId;
    }

    public void setGsrccDate(final String gsrccDate) {
        this.gsrccDate = gsrccDate;
    }

    public void setGsrccTime(final String gsrccTime) {
        this.gsrccTime = gsrccTime;
    }

    public void setGsrccEmp(final String gsrccEmp) {
        this.gsrccEmp = gsrccEmp;
    }

    public void setGsrccOldCardId(final String gsrccOldCardId) {
        this.gsrccOldCardId = gsrccOldCardId;
    }

    public void setGsrccNewCardId(final String gsrccNewCardId) {
        this.gsrccNewCardId = gsrccNewCardId;
    }

    public void setGsrccRemark(final String gsrccRemark) {
        this.gsrccRemark = gsrccRemark;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GaiaSdRechargeCardChangeOutData)) {
            return false;
        } else {
            GaiaSdRechargeCardChangeOutData other = (GaiaSdRechargeCardChangeOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label143: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label143;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label143;
                    }

                    return false;
                }

                Object this$gsrccVoucherId = this.getGsrccVoucherId();
                Object other$gsrccVoucherId = other.getGsrccVoucherId();
                if (this$gsrccVoucherId == null) {
                    if (other$gsrccVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsrccVoucherId.equals(other$gsrccVoucherId)) {
                    return false;
                }

                Object this$brName = this.getBrName();
                Object other$brName = other.getBrName();
                if (this$brName == null) {
                    if (other$brName != null) {
                        return false;
                    }
                } else if (!this$brName.equals(other$brName)) {
                    return false;
                }

                label122: {
                    Object this$gsrccStatus = this.getGsrccStatus();
                    Object other$gsrccStatus = other.getGsrccStatus();
                    if (this$gsrccStatus == null) {
                        if (other$gsrccStatus == null) {
                            break label122;
                        }
                    } else if (this$gsrccStatus.equals(other$gsrccStatus)) {
                        break label122;
                    }

                    return false;
                }

                label115: {
                    Object this$gsrccBrId = this.getGsrccBrId();
                    Object other$gsrccBrId = other.getGsrccBrId();
                    if (this$gsrccBrId == null) {
                        if (other$gsrccBrId == null) {
                            break label115;
                        }
                    } else if (this$gsrccBrId.equals(other$gsrccBrId)) {
                        break label115;
                    }

                    return false;
                }

                Object this$gsrccDate = this.getGsrccDate();
                Object other$gsrccDate = other.getGsrccDate();
                if (this$gsrccDate == null) {
                    if (other$gsrccDate != null) {
                        return false;
                    }
                } else if (!this$gsrccDate.equals(other$gsrccDate)) {
                    return false;
                }

                Object this$gsrccTime = this.getGsrccTime();
                Object other$gsrccTime = other.getGsrccTime();
                if (this$gsrccTime == null) {
                    if (other$gsrccTime != null) {
                        return false;
                    }
                } else if (!this$gsrccTime.equals(other$gsrccTime)) {
                    return false;
                }

                label94: {
                    Object this$gsrccEmp = this.getGsrccEmp();
                    Object other$gsrccEmp = other.getGsrccEmp();
                    if (this$gsrccEmp == null) {
                        if (other$gsrccEmp == null) {
                            break label94;
                        }
                    } else if (this$gsrccEmp.equals(other$gsrccEmp)) {
                        break label94;
                    }

                    return false;
                }

                label87: {
                    Object this$gsrccOldCardId = this.getGsrccOldCardId();
                    Object other$gsrccOldCardId = other.getGsrccOldCardId();
                    if (this$gsrccOldCardId == null) {
                        if (other$gsrccOldCardId == null) {
                            break label87;
                        }
                    } else if (this$gsrccOldCardId.equals(other$gsrccOldCardId)) {
                        break label87;
                    }

                    return false;
                }

                Object this$gsrccNewCardId = this.getGsrccNewCardId();
                Object other$gsrccNewCardId = other.getGsrccNewCardId();
                if (this$gsrccNewCardId == null) {
                    if (other$gsrccNewCardId != null) {
                        return false;
                    }
                } else if (!this$gsrccNewCardId.equals(other$gsrccNewCardId)) {
                    return false;
                }

                Object this$gsrccRemark = this.getGsrccRemark();
                Object other$gsrccRemark = other.getGsrccRemark();
                if (this$gsrccRemark == null) {
                    if (other$gsrccRemark != null) {
                        return false;
                    }
                } else if (!this$gsrccRemark.equals(other$gsrccRemark)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GaiaSdRechargeCardChangeOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsrccVoucherId = this.getGsrccVoucherId();
        result = result * 59 + ($gsrccVoucherId == null ? 43 : $gsrccVoucherId.hashCode());
        Object $brName = this.getBrName();
        result = result * 59 + ($brName == null ? 43 : $brName.hashCode());
        Object $gsrccStatus = this.getGsrccStatus();
        result = result * 59 + ($gsrccStatus == null ? 43 : $gsrccStatus.hashCode());
        Object $gsrccBrId = this.getGsrccBrId();
        result = result * 59 + ($gsrccBrId == null ? 43 : $gsrccBrId.hashCode());
        Object $gsrccDate = this.getGsrccDate();
        result = result * 59 + ($gsrccDate == null ? 43 : $gsrccDate.hashCode());
        Object $gsrccTime = this.getGsrccTime();
        result = result * 59 + ($gsrccTime == null ? 43 : $gsrccTime.hashCode());
        Object $gsrccEmp = this.getGsrccEmp();
        result = result * 59 + ($gsrccEmp == null ? 43 : $gsrccEmp.hashCode());
        Object $gsrccOldCardId = this.getGsrccOldCardId();
        result = result * 59 + ($gsrccOldCardId == null ? 43 : $gsrccOldCardId.hashCode());
        Object $gsrccNewCardId = this.getGsrccNewCardId();
        result = result * 59 + ($gsrccNewCardId == null ? 43 : $gsrccNewCardId.hashCode());
        Object $gsrccRemark = this.getGsrccRemark();
        result = result * 59 + ($gsrccRemark == null ? 43 : $gsrccRemark.hashCode());
        return result;
    }

    public String toString() {
        return "GaiaSdRechargeCardChangeOutData(clientId=" + this.getClientId() + ", gsrccVoucherId=" + this.getGsrccVoucherId() + ", brName=" + this.getBrName() + ", gsrccStatus=" + this.getGsrccStatus() + ", gsrccBrId=" + this.getGsrccBrId() + ", gsrccDate=" + this.getGsrccDate() + ", gsrccTime=" + this.getGsrccTime() + ", gsrccEmp=" + this.getGsrccEmp() + ", gsrccOldCardId=" + this.getGsrccOldCardId() + ", gsrccNewCardId=" + this.getGsrccNewCardId() + ", gsrccRemark=" + this.getGsrccRemark() + ")";
    }
}
