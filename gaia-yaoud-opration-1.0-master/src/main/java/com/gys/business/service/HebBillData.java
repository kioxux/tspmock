package com.gys.business.service;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.gys.business.mapper.entity.PayMsg;
import com.gys.business.mapper.entity.SaleD;
import com.gys.business.mapper.entity.SaleH;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@JSONType(orders={"payMsgList","saleDList","saleH"})
public class HebBillData {
    @NotNull
    @JSONField(ordinal=3)
    private SaleH saleH;
    @NotNull
    @JSONField(ordinal=2)
    private List<SaleD> saleDList;
    @NotNull
    @JSONField(ordinal=1)
    private List<PayMsg> payMsgList;

//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder("{");
//        sb.append(",\"payMsgList\":")
//                .append(payMsgList);
//        sb.append(",\"saleDList\":")
//                .append(saleDList);
//        sb.append("\"saleH\":")
//                .append(saleH);
//        sb.append('}');
//        return sb.toString();
//    }
}
