package com.gys.business.service.data;

import lombok.Data;

@Data
public class ColdChainInData {
    private String client;
    private String brId;
    private String startTime;
    private String endTime;
    private String goodsownid;


}
