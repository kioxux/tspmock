package com.gys.business.service.data;

import lombok.Data;

/**
 * 基础字段
 * @author chenhao
 */
@Data
public class BaseVO {
    private String clientId;
    private String gsstBrId;
}
