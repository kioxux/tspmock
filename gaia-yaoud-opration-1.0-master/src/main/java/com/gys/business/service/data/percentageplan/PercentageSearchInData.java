package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class PercentageSearchInData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "方案名称")
    private String planName;
    @ApiModelProperty(value = "页数")
    private Integer pageNum;
    @ApiModelProperty(value = "每页个数")
    private Integer pageSize;
    @ApiModelProperty(value = "方案起始日期")
    private String planStartDate;
    @ApiModelProperty(value = "方案结束日期")
    private String planEndDate;
    @ApiModelProperty(value = "提成类型 1 销售 2 单品")
    private String planType;
    @ApiModelProperty(value = "审核状态 0 已保存 1 已审核 2 已停用 3 反审核")
    private String planStatus;
    @ApiModelProperty(value = "用户具备的门店code集合")
    private List<String> stos;

}
