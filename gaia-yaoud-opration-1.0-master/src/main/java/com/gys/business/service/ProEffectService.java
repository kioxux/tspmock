//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.ProEffectInData;
import com.gys.business.service.data.ProEffectOutData;
import com.gys.common.data.PageInfo;

public interface ProEffectService {
    PageInfo<ProEffectOutData> proEffectList(ProEffectInData inData);
}
