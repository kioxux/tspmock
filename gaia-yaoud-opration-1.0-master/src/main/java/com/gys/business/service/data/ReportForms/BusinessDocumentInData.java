package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "业务单据查询")
public class BusinessDocumentInData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "公司编码")
    private String siteCode;
    @ApiModelProperty(value = "公司编码")
    private String[] siteArr;
    @ApiModelProperty(value = "开始日期")
    private String startDate;
    @ApiModelProperty(value = "截至日期")
    private String endDate;
    @ApiModelProperty(value = "发生门店")
    private String store;
    @ApiModelProperty(value = "物流中心")
    private String dc;
    @ApiModelProperty(value = "业务类型")
    private String matType;
    @ApiModelProperty(value = "业务单据号")
    private String dnId;
    @ApiModelProperty(value = "合计金额")
    private String totalAmount;
    @ApiModelProperty(value = "入库地点")
    private String locationCode;
    @ApiModelProperty(value = "生产厂家")
    private String factory;
    @ApiModelProperty(value = "商品编号")
    private String proCodes;
    private String[] proArr;
    private Integer pageNum;
    private Integer pageSize;
}
