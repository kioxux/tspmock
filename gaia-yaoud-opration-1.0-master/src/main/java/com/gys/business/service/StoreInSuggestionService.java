package com.gys.business.service;

import com.gys.common.data.PageInfo;
import com.gys.common.data.suggestion.InDetailSaveForm;
import com.gys.common.data.suggestion.SuggestionInForm;
import com.gys.common.data.suggestion.SuggestionInVO;

import java.util.Map;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/11/1 16:09
 */
public interface StoreInSuggestionService {

    PageInfo<SuggestionInVO> list(SuggestionInForm suggestionInForm);

    Map<String,Object> detail(SuggestionInForm suggestionInForm);

    void saveDetail(InDetailSaveForm detailSaveForm);

    void confirm(InDetailSaveForm detailSaveForm);
}
