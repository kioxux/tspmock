package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdPromUnitarySet;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromSingleInData implements Serializable {
    private static final long serialVersionUID = 8482064273684250075L;
    private GaiaSdPromUnitarySet gaiaSdPromUnitarySet;
    private float num;
    private BigDecimal normalPrice;
    private BigDecimal money;
}
