package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/12/15
 */
@Data
public class ReturnCacheData implements Serializable {
    private static final long serialVersionUID = 7040034961907680735L;

    /**
     * 新销售单号
     */
    private String newBillNo;

    /**
     * 旧销售单号
     */
    private String oldBillNo;

    /**
     * 退货单日期
     */
    private String returnOrderTime;

    /**
     * 时间
     */
    private String gsshTime;

    /**
     * 医保订单流水号
     */
    private String ishDealSerial;

    /**
     * 就诊流水号（住院（或门诊）号）
     */
    private String ishJzbh;

}
