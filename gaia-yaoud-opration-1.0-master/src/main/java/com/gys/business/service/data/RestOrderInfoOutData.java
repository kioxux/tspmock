package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class RestOrderInfoOutData implements Serializable {
    private static final long serialVersionUID = 6663370052055216349L;
    /**
     * 日期
     */
    private String gsshDate;

    /**
     * 时间
     */
    private String gsshTime;

    /**
     * 销售单号
     */
    private String gsshBillNo;

    /**
     * 会员卡号
     */
    private String gsshHykNo;

    /**
     * 会员姓名
     */
    private String gsshHykName;

    /**
     * 应收金额
     */
    private String gsshYsAmt;

    /**
     * 折让金额
     */
    private String gsshZkAmt;

    /**
     * 零售金额
     */
    private String gsshNormalAmt;
}


