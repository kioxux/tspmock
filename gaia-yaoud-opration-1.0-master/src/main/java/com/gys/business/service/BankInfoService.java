//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.BankInfoInData;
import com.gys.business.service.data.BankInfoOutData;
import java.util.List;

public interface BankInfoService {
    List<BankInfoOutData> getBankInfoList(BankInfoInData inData);

    List<String> getBankNames(BankInfoInData inData);

    List<String> getAllBankIdByName(BankInfoInData inData);

    List<String> getAllBankAccountByName(BankInfoInData inData);

    List<String> getBankIdByBankAccount(BankInfoInData inData);

    void insertBankInfo(BankInfoInData inData);

    BankInfoOutData getBankInfoById(BankInfoInData inData);

    void editBankInfo(BankInfoInData inData);
}
