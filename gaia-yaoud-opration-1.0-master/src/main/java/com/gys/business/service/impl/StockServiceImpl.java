package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.CacheService;
import com.gys.business.service.StockService;
import com.gys.business.service.data.ConsumeStockResponse;
import com.gys.business.service.data.StockInData;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.util.CommonUtil;
import com.gys.util.takeAway.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 库存ServiceImpl
 *
 * @author chenhao
 */
@Service
@Slf4j
public class StockServiceImpl implements StockService {
    @Resource
    private GaiaSdStockMapper stockMapper;

    @Resource
    private GaiaSdStockBatchMapper stockBatchMapper;

    @Resource
    private GaiaSdBatchChangeMapper batchChangeMapper;

    @Resource
    private CacheService synchronizedService;

    @Resource
    private GaiaTAccountMapper gaiaTAccountMapper;

    @Resource
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private TakeAwayService takeAwayService;
    @Autowired
    private BaseService baseService;
    private static final ExecutorService EXECUTOR = new ThreadPoolExecutor(4, 8, 30, TimeUnit.SECONDS, new LinkedBlockingDeque<>(10),
            r -> {
                Thread thread = new Thread(r, "stock sync worker");
                thread.setDaemon(true);
                return thread;
            }, new ThreadPoolExecutor.CallerRunsPolicy());

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object[] addShopStock(StockInData stockInData) {
        log.info("addShopStock:{}", JSON.toJSONString(stockInData));

        Object[] resultObj = new Object[2];
        List<ConsumeStockResponse> consumeBatchList = Lists.newArrayList();
        Example stockBatchExample = new Example(GaiaSdStockBatch.class);
        //将库存添加到最大批次
        stockBatchExample.createCriteria().andEqualTo("clientId", stockInData.getClientId()).andEqualTo("gssbBrId", stockInData.getGssBrId())
                .andEqualTo("gssbProId", stockInData.getGssProId()).andEqualTo("gssbBatchNo", stockInData.getBatchNo());
        stockBatchExample.setOrderByClause("GSSB_BATCH DESC");
        List<GaiaSdStockBatch> stockBatchList = stockBatchMapper.selectByExample(stockBatchExample);
        if (CollectionUtils.isNotEmpty(stockBatchList)) {
            BigDecimal qty = stockInData.getNum();
            GaiaSdStockBatch stockBatch = CollectionUtil.getFirst(stockBatchList);
            stockBatch.setGssbQty(new BigDecimal(stockBatch.getGssbQty()).add(qty).toString());
            stockBatch.setGssbUpdateDate(CommonUtil.getyyyyMMdd());
            stockBatch.setGssbUpdateEmp(stockInData.getEmpId());
            stockBatchMapper.updateByPrimaryKeySelective(stockBatch);

            GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
            batchChange.setClient(stockInData.getClientId());
            batchChange.setGsbcBrId(stockInData.getGssBrId());
            batchChange.setGsbcVoucherId(stockInData.getVoucherId());
            batchChange.setGsbcSerial(stockInData.getSerial());
            batchChange.setGsbcDate(CommonUtil.getyyyyMMdd());
            batchChange.setGsbcProId(stockInData.getGssProId());
            batchChange.setGsbcBatch(stockBatch.getGssbBatch());
            batchChange.setGsbcBatchNo(stockInData.getBatchNo());
            batchChange.setGsbcQty(qty);
            batchChangeMapper.insert(batchChange);

            stockInData.setUpdateDate(CommonUtil.getyyyyMMdd());
            stockMapper.addShopStock(stockInData);

            ConsumeStockResponse consumeStockResponse = new ConsumeStockResponse();
            consumeStockResponse.setBatch(stockBatch.getGssbBatch());
            consumeStockResponse.setNum(qty);
            consumeBatchList.add(consumeStockResponse);
        }

        resultObj[1] = consumeBatchList;
        return resultObj;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object[] consumeShopStock(StockInData stockInData, BigDecimal owingStock, boolean hasNext) {
        log.info("stockInData:{}", JSON.toJSONString(stockInData));
        //是否为中药代煎
        boolean crStatusFlag = StringUtils.isNotEmpty(stockInData.getCrFlag());
        //虚拟中药代煎商品的批号
        String crBatchNo = "P00000000000";
        Object[] resultObj = new Object[2];
        //所有消耗过的批次数据
        List<ConsumeStockResponse> consumeBatchList = Lists.newArrayList();
        //消耗的库存
        BigDecimal consumeNum = stockInData.getNum();
        Example stockBatchExample = new Example(GaiaSdStockBatch.class);
        String batchNo = stockInData.getBatchNo();
        //有批号
        if (StringUtils.isNotEmpty(batchNo)) {
            stockBatchExample.createCriteria().andEqualTo("clientId", stockInData.getClientId()).andEqualTo("gssbBrId", stockInData.getGssBrId())
                    .andEqualTo("gssbProId", stockInData.getGssProId()).andEqualTo("gssbBatchNo", stockInData.getBatchNo())
                    .andGreaterThan("gssbQty", "0");
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            Date date = new Date();
            String now = formatter.format(date);
            stockBatchExample.createCriteria().andEqualTo("clientId", stockInData.getClientId()).andEqualTo("gssbBrId", stockInData.getGssBrId())
                    .andEqualTo("gssbProId", stockInData.getGssProId()).andGreaterThan("gssbQty", "0").andGreaterThan("gssbVaildDate", now);
        }

        stockBatchExample.setOrderByClause("GSSB_BATCH ASC");
        List<GaiaSdStockBatch> stockBatchList = stockBatchMapper.selectByExample(stockBatchExample);
        Iterator<GaiaSdStockBatch> stockBatchIterator = stockBatchList.iterator();
        //剩余库存数
        BigDecimal surplus;
        boolean breakFlag = false;

        ConsumeStockResponse consumeStockResponse = new ConsumeStockResponse();
        while (stockBatchIterator.hasNext()) {
            consumeStockResponse = new ConsumeStockResponse();
            GaiaSdStockBatch stockBatch = stockBatchIterator.next();
            BigDecimal existQty = new BigDecimal(stockBatch.getGssbQty());
            surplus = existQty.subtract(consumeNum);
            consumeStockResponse.setBatch(stockBatch.getGssbBatch());

            //当前批次的库存足够扣减
            if (existQty.compareTo(consumeNum) >= 0) {
                stockBatch.setGssbQty(StrUtil.toString(surplus));
                consumeStockResponse.setNum(consumeNum);
                breakFlag = true;
            } else {
                owingStock = surplus.add(owingStock);
                //当前批次库存不够，依次递减
                surplus = surplus.abs();
                consumeNum = surplus;
                consumeStockResponse.setNum(existQty);
                //库存被消耗完
                stockBatch.setGssbQty("0");
            }

            stockBatch.setGssbUpdateDate(CommonUtil.getyyyyMMdd());
            stockBatch.setGssbUpdateEmp(stockInData.getEmpId());
            stockBatchMapper.updateByPrimaryKeySelective(stockBatch);

            Map<String, Object> map = new HashMap<>(16);
            map.put("CLIENT", stockInData.getClientId());
            map.put("GSSB_BR_ID", stockInData.getGssBrId());
            map.put("GSSB_PRO_ID", stockInData.getGssProId());
            map.put("GSSB_BATCH_NO", stockInData.getBatchNo());
            map.put("GSSB_BATCH", stockBatch.getGssbBatch());
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(stockInData.getClientId());
            aSynchronized.setSite(stockInData.getGssBrId());
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_SD_STOCK_BATCH");
            aSynchronized.setCommand("update");
            aSynchronized.setSourceNo("销售");
            aSynchronized.setDatetime(String.valueOf(Calendar.getInstance().getTimeInMillis()));
            aSynchronized.setArg(JSON.toJSONString(map));
            this.synchronizedService.addOne(aSynchronized);

            //更新库存异动表
            GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
            batchChange.setClient(stockInData.getClientId());
            batchChange.setGsbcBrId(stockInData.getGssBrId());
            batchChange.setGsbcVoucherId(stockInData.getVoucherId());
            batchChange.setGsbcSerial(stockInData.getSerial());
            batchChange.setGsbcDate(CommonUtil.getyyyyMMdd());
            batchChange.setGsbcProId(stockInData.getGssProId());
            batchChange.setGsbcBatchNo(stockBatch.getGssbBatchNo());
            batchChange.setGsbcBatch(StrUtil.isBlank(stockBatch.getGssbBatch()) ? "" : stockBatch.getGssbBatch());
            batchChange.setGsbcQty(breakFlag ? consumeNum : existQty);
            batchChangeMapper.insert(batchChange);

            Map<String, Object> map2 = new HashMap<>(16);
            map2.put("CLIENT", batchChange.getClient());
            map2.put("GSBC_BR_ID", batchChange.getGsbcBrId());
            map2.put("GSBC_VOUCHER_ID", batchChange.getGsbcVoucherId());
            map2.put("GSBC_DATE", batchChange.getGsbcDate());
            map2.put("GSBC_SERIAL", batchChange.getGsbcSerial());
            map2.put("GSBC_PRO_ID", batchChange.getGsbcProId());
            map2.put("GSBC_BATCH", batchChange.getGsbcBatch());
            GaiaDataSynchronized aSynchronized2 = new GaiaDataSynchronized();
            aSynchronized2.setClient(stockInData.getClientId());
            aSynchronized2.setSite(stockInData.getGssBrId());
            aSynchronized2.setVersion(IdUtil.randomUUID());
            aSynchronized2.setTableName("GAIA_SD_BATCH_CHANGE");
            aSynchronized2.setCommand("insert");
            aSynchronized2.setSourceNo("销售");
            aSynchronized2.setArg(JSON.toJSONString(map2));
            this.synchronizedService.addOne(aSynchronized2);

            consumeBatchList.add(consumeStockResponse);
            //足够扣减，跳出循环
            if (breakFlag) {
                break;
            }
        }

        stockInData.setNum(stockInData.getNum());
        stockInData.setUpdateDate(CommonUtil.getyyyyMMdd());
        if (crStatusFlag) {
            GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
            batchChange.setClient(stockInData.getClientId());
            batchChange.setGsbcBrId(stockInData.getGssBrId());
            batchChange.setGsbcVoucherId(stockInData.getVoucherId());
            batchChange.setGsbcSerial(stockInData.getSerial());
            batchChange.setGsbcDate(CommonUtil.getyyyyMMdd());
            batchChange.setGsbcProId(stockInData.getGssProId());
            batchChange.setGsbcBatch(crBatchNo);
            batchChange.setGsbcBatchNo(stockInData.getBatchNo());
            batchChange.setGsbcQty(consumeNum);
            batchChangeMapper.insert(batchChange);


            Map<String, Object> map2 = new HashMap<>(16);
            map2.put("CLIENT", batchChange.getClient());
            map2.put("GSBC_BR_ID", batchChange.getGsbcBrId());
            map2.put("GSBC_VOUCHER_ID", batchChange.getGsbcVoucherId());
            map2.put("GSBC_DATE", batchChange.getGsbcDate());
            map2.put("GSBC_SERIAL", batchChange.getGsbcSerial());
            map2.put("GSBC_PRO_ID", batchChange.getGsbcProId());
            map2.put("GSBC_BATCH", batchChange.getGsbcBatch());
            GaiaDataSynchronized aSynchronized2 = new GaiaDataSynchronized();
            aSynchronized2.setClient(stockInData.getClientId());
            aSynchronized2.setSite(stockInData.getGssBrId());
            aSynchronized2.setVersion(IdUtil.randomUUID());
            aSynchronized2.setTableName("GAIA_SD_BATCH_CHANGE");
            aSynchronized2.setCommand("insert");
            aSynchronized2.setSourceNo("销售");
            aSynchronized2.setArg(JSON.toJSONString(map2));
            this.synchronizedService.addOne(aSynchronized2);

            consumeStockResponse = new ConsumeStockResponse();
            consumeStockResponse.setBatch(crBatchNo);
            consumeStockResponse.setNum(consumeNum);
            consumeBatchList.add(consumeStockResponse);
        } else {
            stockMapper.consumeShopStockQty(stockInData);

            Map<String, Object> map = new HashMap<>(16);
            map.put("CLIENT", stockInData.getClientId());
            map.put("GSS_BR_ID", stockInData.getGssBrId());
            map.put("GSS_PRO_ID", stockInData.getGssProId());
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(stockInData.getClientId());
            aSynchronized.setSite(stockInData.getGssBrId());
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_SD_STOCK");
            aSynchronized.setCommand("update");
            aSynchronized.setSourceNo("销售");
            aSynchronized.setArg(JSON.toJSONString(map));
            this.synchronizedService.addOne(aSynchronized);
        }

        resultObj[0] = owingStock;
        resultObj[1] = consumeBatchList;
        return resultObj;
    }

    /**
     * 库存同步
     *
     * @param stockList 需要同步的库存
     */
    public void syncStock(List<GaiaSdStock> stockList) {
        EXECUTOR.execute(() -> {
            log.info("库存部分更新，调外卖平台库存更新接口 begin======");
            try {
                List<Map<String, Object>> list = getPlatformParamMap(stockList);
                if (list != null) {
                    for (Map<String, Object> paramMap : list) {
                        String accessToken = null;
                        if (Constants.PLATFORMS_MT.equalsIgnoreCase((String) paramMap.get("platform"))) {
                            GaiaTAccount account = (GaiaTAccount) paramMap.get("account");
                            accessToken = takeAwayService.getAccessToken(account);
                        }
                        if (Constants.PLATFORMS_JD.equalsIgnoreCase((String)paramMap.get("platform"))) {
                            GaiaTAccount account = (GaiaTAccount) paramMap.get("account");
                            accessToken = account.getToken();
                        }
                        String result = baseService.updateStockWithMap(paramMap, accessToken);
                        log.info("库存部分更新，调外卖平台库存更新接口, paramMap:{}, result:{}", JSON.toJSONString(paramMap), result);
                    }
                }
            } catch (Exception e) {
                log.info("库存部分更新，调外卖平台库存更新接口 error:{}", e.getMessage());
            }
            log.info("库存部分更新，调外卖平台库存更新接口 end======");
        });
    }

    /**
     * 获取调用外卖平台更新库存接口所需参数
     * return  null: 此门店无外卖功能 or 库存开关关闭，无需调接口
     */
    @Override
    public List<Map<String, Object>> getPlatformParamMap(List<GaiaSdStock> list) {
        List<Map<String, Object>> resultList = Lists.newArrayList();
        //门店号改用GAIA_STORE_DATA表 具有全局唯一性的public_code字段替代gssBrId
        String clientId = list.get(0).getClientId();
        String gssBrId = list.get(0).getGssBrId();
        List<Map<String, Object>> medicineList = com.google.common.collect.Lists.newArrayList();
        for (GaiaSdStock gaiaSdStock : list) {
            Map<String, Object> medicineMap = new HashMap<>();
            medicineMap.put("medicine_code", gaiaSdStock.getGssProId());
            medicineMap.put("stock", gaiaSdStock.getGssQty());
            medicineList.add(medicineMap);
        }
        //查店铺对接的外卖平台
        List<GaiaTAccount> accountList = gaiaTAccountMapper.findByClientAndStoCode(clientId, gssBrId);
        accountList = accountList.stream().filter(r -> "Y".equalsIgnoreCase(r.getStockOnService())).collect(Collectors.toList());
        if (accountList.size() <= 0) {
            return null;
        }
        for (GaiaTAccount account : accountList) {
            List<Map<String, Object>> shopList = Lists.newArrayList();
            Map<String, Object> shopMap = new HashMap<>();
            shopMap.put("medicine", medicineList);
            shopMap.put("shop_no", account.getPublicCode());
            shopList.add(shopMap);
            Map<String, Object> tempMap = new HashMap<>();
            tempMap.put("source", account.getAppId());
            tempMap.put("secret", account.getAppSecret());
            tempMap.put("platform", account.getPlatform());
            tempMap.put("shop", shopList);
            tempMap.put("account", account);//给方法临时传参用
            resultList.add(tempMap);
        }
        return resultList;
    }
}
