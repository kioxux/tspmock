package com.gys.business.service.response;

import com.gys.common.data.PageInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SelectLargeOasSicknessPageInfoResponse {


    @ApiModelProperty(value = "疾病编码")
    private String largeSicknessCoding;

    @ApiModelProperty(value = "疾病名称")
    private String largeSicknessName;

    @ApiModelProperty(value = "疾病编码")
    private String middleSicknessCoding;

    @ApiModelProperty(value = "疾病名称")
    private String middleSicknessCodingName;


    @ApiModelProperty(value = "疾病名称")
    private PageInfo<List<SelectMiddleOasSicknessPageInfoResponse>> pageInfo;

    /**
     * 中类分类
     */

    @Data
    public static class SelectMiddleOasSicknessPageInfoResponse {

        @ApiModelProperty(value = "疾病编码")
        private String sicknessCoding;

        @ApiModelProperty(value = "疾病名称")
        private String sicknessName;
    }
}
