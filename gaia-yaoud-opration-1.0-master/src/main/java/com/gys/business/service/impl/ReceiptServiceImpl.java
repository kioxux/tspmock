package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaReceiptMapper;
import com.gys.business.service.ReceiptService;
import com.gys.business.service.data.ReceiptInData;
import com.gys.business.service.data.ReceiptOutData;
import com.gys.business.service.data.ReceiptPayOutData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 业务层
 *
 * @author xiaoyuan on 2020/8/3
 */
@Service
public class ReceiptServiceImpl implements ReceiptService {

    @Autowired
    private GaiaReceiptMapper receiptMapper;


    @Override
    public ReceiptOutData findReceipt(ReceiptInData inData) {
        ReceiptOutData outData = null;
        if (ObjectUtil.isNotEmpty(inData.getSaleBillNo())){
            outData = receiptMapper.findReceipt(inData);
        }
        return outData;
    }

    @Override
    public List<ReceiptPayOutData> findTypeByOrderNumber(ReceiptInData inData) {
        List<ReceiptPayOutData> payOutData = null;
        if(ObjectUtil.isNotEmpty(inData.getSaleBillNo())){
            payOutData = receiptMapper.findTypeByOrderNumber(inData);
        }
        return payOutData;
    }
}
