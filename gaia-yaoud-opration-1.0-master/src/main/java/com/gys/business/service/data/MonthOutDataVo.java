package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import java.math.BigDecimal;

//月销售返回值
@ApiModel(value = "用户月销售返回值", description = "")
@Data
public class MonthOutDataVo {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店编码")
    private String brId;
    @ApiModelProperty(value = "店名简称")
    private String brName;
    @ApiModelProperty(value = "任务天数")
    private int missionDays;
    //营业额 turnover
    @ApiModelProperty(value = "营业额同期日均")
    private String turnoverTSP;
    @ApiModelProperty(value = "营业额上期日均")
    private String turnoverOP;
    @ApiModelProperty(value = "营业额 同比增长日均计划")
    private String turnoverYOYDAPlan;
    @ApiModelProperty(value = "营业额 环比增长日均计划")
    private String turnoverCRDAPlan;
    @ApiModelProperty(value = "营业额 本期日均计划")
    private String turnoverDailyPlan;
    @ApiModelProperty(value = "营业额 本期月计划")
    private String turnoverDailyMonth;
    //毛利额 grossProfit
    @ApiModelProperty(value = "毛利额 同期日均")
    private String grossProfitTSP;
    @ApiModelProperty(value = "毛利额 上期日均")
    private String grossProfitOP;
    @ApiModelProperty(value = "毛利额 同比增长日均计划")
    private String grossProfitYOYDAPlan;
    @ApiModelProperty(value = "毛利额 环比增长日均计划")
    private String grossProfitCRDAPlan;
    @ApiModelProperty(value = "毛利额 本期日均计划")
    private String grossProfitDailyPlan;
    @ApiModelProperty(value = "毛利额 本期月计划")
    private String grossProfitDailyMonth;
    //毛利率 grossMargin
    @ApiModelProperty(value = "毛利率 同期")
    private String grossMarginTSP;
    @ApiModelProperty(value = "毛利率 上期")
    private String grossMarginOP;
    @ApiModelProperty(value = "毛利率 本期计划")
    private String grossMarginThisPeriod;
    //会员卡
    @ApiModelProperty(value = "会员卡 同期")
    private String mcardTSP;
    @ApiModelProperty(value = "会员卡 上期")
    private String mcardOP;
    @ApiModelProperty(value = "会员卡 同比增长日均计划")
    private String mcardYOYDAPlan;
    @ApiModelProperty(value = "会员卡 环比增长日均计划")
    private String mcardCRDAPlan;
    @ApiModelProperty(value = "会员卡 本期日均计划")
    private String mcardDailyPlan;
    @ApiModelProperty(value = "会员卡 本期月均计划")
    private String mcardDailyMonth;


    @ApiModelProperty(value = "营业额同比增长率")
    private BigDecimal yoySaleAmt;
    @ApiModelProperty(value = "营业额环比增长率")
    private BigDecimal momSaleAmt;
    @ApiModelProperty(value = "毛利额同比增长率")
    private BigDecimal yoySaleGross;
    @ApiModelProperty(value = "毛利额环比增长率")
    private BigDecimal momSaleGross;
    @ApiModelProperty(value = "会员卡同比增长率")
    private BigDecimal yoyMcardQty;
    @ApiModelProperty(value = "会员卡环比增长率")
    private BigDecimal momMcardQty;
    @ApiModelProperty(value = "任务月份")
    //@NotEmpty(message = "monthPlan 任务月份不可为空!")
    private String monthPlan;
    @ApiModelProperty(value = "任务id")
    private String staskTaskId;
    @ApiModelProperty(value = "任务名称")
    private String staskTaskName;

    /**
     * 同比月份
     */
    private String yoyMonth;

    /**
     * 环比月份
     */
    private String momMonth;

}
