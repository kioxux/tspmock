//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.gys.business.mapper.GaiaSdRemoteAuditDMapper;
import com.gys.business.mapper.GaiaSdRemoteAuditHMapper;
import com.gys.business.mapper.entity.GaiaSdRemoteAuditD;
import com.gys.business.mapper.entity.GaiaSdRemoteAuditH;
import com.gys.business.service.RemoteAuditService;
import com.gys.business.service.data.RemoteAuditDInData;
import com.gys.business.service.data.RemoteAuditHInData;
import com.gys.common.data.GetLoginOutData;
import java.util.Date;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RemoteAuditServiceImpl implements RemoteAuditService {
    @Autowired
    private GaiaSdRemoteAuditHMapper gaiaSdRemoteAuditHMapper;
    @Autowired
    private GaiaSdRemoteAuditDMapper gaiaSdRemoteAuditDMapper;

    public RemoteAuditServiceImpl() {
    }

    @Transactional
    public void insertRemoteAudit(RemoteAuditHInData inData, GetLoginOutData userInfo) {
        String client = userInfo.getClient();
        GaiaSdRemoteAuditH gaiaSdRemoteAuditH = new GaiaSdRemoteAuditH();
        BeanUtils.copyProperties(inData, gaiaSdRemoteAuditH);
        String codePre = DateUtil.format(new Date(), "yyMMdd");
        String voucherId = this.gaiaSdRemoteAuditHMapper.selectNextVoucherId(client, codePre);
        gaiaSdRemoteAuditH.setClientId(client);
        gaiaSdRemoteAuditH.setGsrahVoucherId(voucherId);
        gaiaSdRemoteAuditH.setGsrahBrId(userInfo.getDepId());
        gaiaSdRemoteAuditH.setGsrahBrName(userInfo.getDepName());
        gaiaSdRemoteAuditH.setGsrahUploadEmp(userInfo.getUserId());
        gaiaSdRemoteAuditH.setGsrahUploadFlag("0");
        gaiaSdRemoteAuditH.setGsrahStatus("0");
        gaiaSdRemoteAuditH.setGsrahUploadDate(DateUtil.format(new Date(), "yyyyMMdd"));
        gaiaSdRemoteAuditH.setGsrahUploadTime(DateUtil.format(new Date(), "HHmmss"));
        gaiaSdRemoteAuditH.setGsrahRecipelPicAddress(inData.getGsrahRecipelPicAddress());
        this.gaiaSdRemoteAuditHMapper.insert(gaiaSdRemoteAuditH);
        if (CollUtil.isNotEmpty(inData.getAuditDs())) {
            for(int i = 0; i < inData.getAuditDs().size(); ++i) {
                RemoteAuditDInData auditD = (RemoteAuditDInData)inData.getAuditDs().get(i);
                GaiaSdRemoteAuditD gaiaSdRemoteAuditD = new GaiaSdRemoteAuditD();
                BeanUtils.copyProperties(auditD, gaiaSdRemoteAuditD);
                gaiaSdRemoteAuditD.setClientId(client);
                gaiaSdRemoteAuditD.setGsradVoucherId(voucherId);
                gaiaSdRemoteAuditD.setGsradBrId(userInfo.getDepId());
                gaiaSdRemoteAuditD.setGsradSerial(String.valueOf(i + 1));
                this.gaiaSdRemoteAuditDMapper.insert(gaiaSdRemoteAuditD);
            }
        }

    }
}
