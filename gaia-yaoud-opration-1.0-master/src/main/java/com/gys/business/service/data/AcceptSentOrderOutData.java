package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AcceptSentOrderOutData {
    @ApiModelProperty("配送单号")
    private String psVoucherId;
    @ApiModelProperty("总数量")
    private String psTotalCount;
    @ApiModelProperty("配送总金额")
    private String psTotalAmt;
    @ApiModelProperty("零售总金额")
    private String lsTotalAmt;
}
