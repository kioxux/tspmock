
package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Ordering;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ReplenishService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("all")
@Service
@Slf4j
public class ReplenishServiceImpl implements ReplenishService {
    @Autowired
    private GaiaSdReplenishHMapper replenishHMapper;
    @Autowired
    private GaiaSdReplenishDMapper replenishDMapper;
    @Autowired
    private GaiaSdStockMapper stockMapper;
    @Autowired
    private GaiaSdSystemParaMapper sdSystemParaMapper;
    @Autowired
    private GaiaSdSystemParaDefaultMapper sdSystemParaDefaultMapper;
    @Autowired
    private GaiaPoHeaderMapper poHeaderMapper;
    @Autowired
    private GaiaPoItemMapper poItemMapper;
    @Autowired
    private GaiaProductBusinessMapper productBusinessMapper;
    @Autowired
    private GaiaProductBasicMapper productBasicMapper;
    @Autowired
    private GaiaSoItemMapper soItemMapper;
    @Autowired
    private GaiaBatchStockMapper batchStockMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdStoreDataMapper sdStoreDataMapper;
    @Autowired
    private GaiaSupplierBusinessMapper supplierBusinessMapper;
    @Autowired
    private GaiaSdReplenishParaMapper replenishParaMapper;
    @Autowired
    private GaiaMaterialAssessMapper materialAssessMapper;


    @Override
    public PageInfo<GetReplenishOutData> selectList(GetReplenishInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GetReplenishOutData> outData = this.replenishHMapper.selectList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public List<GetReplenishDetailOutData> detailList(GetReplenishInData inData) {
        List<GetReplenishDetailOutData> outDataList = productBasicMapper.queryProFromReplenish(inData);
        inData.setParamCode("REPLENISH_MINQTY");
        Map<String, GetReplenishOutData> saleMap = this.getSalesMap(inData);
        Map<String, String> stockMap = this.getStockMap(inData);
        Map<String, GetReplenishOutData> costMap = this.getCostMap(inData);
        Map<String, String> countMap = this.getCountMap(inData);
        Map<String, String> amtMap = this.getVoucherAmtMap(inData);
        GetReplenishOutData param = this.getParam(inData);
//        Map<String, QuerySupplierData> supplierNameMap = this.getSupplierNameMap(inData.getClientId());
        Iterator var9 = outDataList.iterator();
        while (var9.hasNext()) {
            GetReplenishDetailOutData detailOutData = (GetReplenishDetailOutData) var9.next();
            inData.setProductCode(detailOutData.getGsrdProId());
            GetReplenishOutData replenishOutData = saleMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNull(replenishOutData)) {
                detailOutData.setGsrdSalesDays1("0");
                detailOutData.setGsrdSalesDays2("0");
                detailOutData.setGsrdSalesDays3("0");
            } else {
                detailOutData.setGsrdSalesDays1(replenishOutData.getSalesCountOne());
                detailOutData.setGsrdSalesDays2(replenishOutData.getSalesCountTwo());
                detailOutData.setGsrdSalesDays3(replenishOutData.getSalesCountThree());
            }

            detailOutData.setGsrdStockStore(stockMap.get(detailOutData.getGsrdProId()));
            replenishOutData = costMap.get(detailOutData.getGsrdProId());
            if (ObjectUtil.isNotNull(replenishOutData)) {
                detailOutData.setGsrdAverageCost(replenishOutData.getAverageCost());
                detailOutData.setGsrdGrossMargin(replenishOutData.getBatchCost());
            }

//            QuerySupplierData supplierData = supplierNameMap.get(detailOutData.getGsrdProId());
//            if (ObjectUtil.isNotEmpty(supplierData)) {
//                detailOutData.setGsrdLastSupid(supplierData.getSupName());
//            }
            detailOutData.setGsrdDisplayMin(param.getParam());
            String count = countMap.get(detailOutData.getGsrdProId());
            // 建议补货量为0的不显示
            if (StrUtil.isNotBlank(count) && !"0".equals(count)) {
                detailOutData.setGsrdNeedQty(count);
                detailOutData.setGsrdProposeQty(count);
            } else {
                var9.remove();
                continue;
            }

            String amt = amtMap.get(inData.getProductCode());
            if (StrUtil.isNotBlank(amt)) {
                detailOutData.setVoucherAmt(amt);
            } else {
                detailOutData.setVoucherAmt("0");
            }

            // 格式化销售量
            detailOutData.setGsrdSalesDays1(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays1(), ".0000"));
            detailOutData.setGsrdSalesDays2(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays2(), ".0000"));
            detailOutData.setGsrdSalesDays3(StrUtil.removeSuffix(detailOutData.getGsrdSalesDays3(), ".0000"));
            detailOutData.setGsrdProposeQty(StrUtil.removeSuffix(detailOutData.getGsrdProposeQty(), ".0000"));
            detailOutData.setGsrdNeedQty(StrUtil.removeSuffix(detailOutData.getGsrdNeedQty(), ".0000"));
            detailOutData.setGsrdStockStore(StrUtil.isEmpty(StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000")) ? "0" : StrUtil.removeSuffix(detailOutData.getGsrdStockStore(), ".0000"));

        }

        outDataList = new Ordering<GetReplenishDetailOutData>() {
            @Override
            public int compare(GetReplenishDetailOutData left, GetReplenishDetailOutData right) {
                int leftQty = NumberUtil.parseInt(left.getGsrdNeedQty());
                int rightQty = NumberUtil.parseInt(right.getGsrdNeedQty());
                return rightQty - leftQty;
            }
        }.immutableSortedCopy(outDataList);

        return outDataList;
    }

    @Override
    @Transactional
    public GetReplenishOutData getDetail(GetReplenishInData inData) {
        GetReplenishOutData outData = new GetReplenishOutData();
        Example example = new Example(GaiaSdReplenishH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId());
        GaiaSdReplenishH replenishH = (GaiaSdReplenishH) this.replenishHMapper.selectOneByExample(example);
        outData.setGsrhType(replenishH.getGsrhType());
        outData.setGsrhDate(replenishH.getGsrhDate());
        outData.setGsrhStatus(replenishH.getGsrhStatus());
        outData.setGsrhTotalAmt(replenishH.getGsrhTotalAmt());
        outData.setGsrhTotalQty(replenishH.getGsrhTotalQty());
        outData.setGsrhEmp(replenishH.getGsrhEmp());
        List<GetReplenishDetailOutData> detailList = this.replenishDMapper.queryDetailList(inData);
        outData.setDetailList(detailList);
        return outData;
    }


    @Override
    public String selectNextVoucherId(GaiaSdReplenishH inData) {
        String voucherId = null;
        if (ObjectUtil.isNotEmpty(inData.getClientId())) {
            voucherId = this.replenishHMapper.selectNextVoucherId(inData);
        }
        return voucherId;
    }

    @Override
    public List<SpecialMedicineOutData> specialDrugsInquiry(SpecialMedicineInData inData) {
        List<SpecialMedicineOutData> outData = this.replenishDMapper.specialDrugsInquiry(inData);
        if (CollUtil.isNotEmpty(outData)){
            IntStream.range(0, outData.size()).forEach(i -> ((SpecialMedicineOutData) outData.get(i)).setIndex(i + 1));
        }
        return outData;
    }

//    @Override
//    public String queryNextVoucherId(GaiaSdReplenishH inData) {
//        return this.replenishHMapper.queryNextVoucherId(inData);
//    }


    @Override
    @Transactional
    public void insert(GetReplenishInData inData) {
        Example example = new Example(GaiaSdReplenishH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId());
        GaiaSdReplenishH replenish = (GaiaSdReplenishH) this.replenishHMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(replenish)) {
            GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
            replenishH.setClientId(inData.getClientId());
            replenishH.setGsrhVoucherId(inData.getGsrhVoucherId());
            replenishH.setGsrhBrId(inData.getStoCode());
            replenishH.setGsrhStatus("0");
            replenishH.setGsrhFlag("0");
            replenishH.setGsrhPattern(inData.getGsrhPattern());
            replenishH.setGsrhType(inData.getGsrhType());
            replenishH.setGsrhDate(inData.getGsrhDate());
            saveDetails(inData, replenishH);
            this.replenishHMapper.insert(replenishH);
        } else {
            example.createCriteria().andEqualTo("clientId", inData.getClientId())
                    .andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId())
                    .andEqualTo("gsrhBrId", inData.getStoCode());
            GaiaSdReplenishH replenishH = replenishHMapper.selectOneByExample(example);
            saveDetails(inData, replenishH);
            this.replenishHMapper.update(replenishH);
        }

    }


    @Transactional
    public void saveDetails(GetReplenishInData inData, GaiaSdReplenishH replenishH) {
        if (ObjectUtil.isNotEmpty(replenishH)) {
            replenishH.setGsrhStatus("0");
            replenishH.setGsrhPattern(inData.getGsrhPattern());
            replenishH.setGsrhType(inData.getGsrhType());
            replenishH.setGsrhDate(inData.getGsrhDate());
            BigDecimal amount = BigDecimal.ZERO;
            BigDecimal count = BigDecimal.ZERO;
            GetReplenishOutData outData = new GetReplenishOutData();
            outData.setGsrhType(replenishH.getGsrhType());
            outData.setGsrhDate(replenishH.getGsrhDate());
            outData.setGsrhStatus(replenishH.getGsrhStatus());
            outData.setGsrhTotalAmt(replenishH.getGsrhTotalAmt());
            outData.setGsrhTotalQty(replenishH.getGsrhTotalQty());
            outData.setGsrhEmp(replenishH.getGsrhEmp());
            List<GetReplenishDetailOutData> detailList = this.replenishDMapper.queryDetailList(inData);
            if (CollUtil.isNotEmpty(detailList)) {
                for (GetReplenishDetailOutData detail : detailList) {
                    Example exampleStore = new Example(GaiaSdReplenishD.class);
                    exampleStore.createCriteria().andEqualTo("clientId", inData.getClientId())
                            .andEqualTo("gsrdVoucherId", inData.getGsrhVoucherId())
                            .andEqualTo("gsrdBrId", inData.getStoCode());
                    this.replenishDMapper.deleteByExample(exampleStore);

                }
            }

            for (GetReplenishDetailInData detail : inData.getDetailList()) {
                if (StrUtil.isEmpty(detail.getGsrdNeedQty())) {
                    count = count.add(new BigDecimal("0"));
                } else {
                    count = count.add(new BigDecimal(detail.getGsrdNeedQty()));
                }
                GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
                replenishD.setClientId(inData.getClientId());
                replenishD.setGsrdVoucherId(inData.getGsrhVoucherId());
                replenishD.setGsrdBrId(inData.getStoCode());
                replenishD.setGsrdDate(inData.getGsrhDate());
                replenishD.setGsrdSerial(detail.getGsrdSerial());
                replenishD.setGsrdProId(detail.getGsrdProId());
                replenishD.setGsrdAverageCost(detail.getGsrdAverageCost());
                replenishD.setGsrdDisplayMin(detail.getGsrdDisplayMin());
                replenishD.setGsrdGrossMargin(detail.getGsrdGrossMargin());
                replenishD.setGsrdNeedQty(detail.getGsrdNeedQty().replaceFirst("^0*", "").length() == 0 ? "0" : detail.getGsrdNeedQty().replaceFirst("^0*", ""));
                replenishD.setGsrdPackMidsize(detail.getGsrdPackMidsize());
                replenishD.setGsrdProposeQty(detail.getGsrdProposeQty());
                replenishD.setGsrdSalesDays1(detail.getGsrdSalesDays1());
                replenishD.setGsrdSalesDays2(detail.getGsrdSalesDays2());
                replenishD.setGsrdSalesDays3(detail.getGsrdSalesDays3());
                replenishD.setGsrdStockDepot(detail.getGsrdStockDepot());
                replenishD.setGsrdStockStore(detail.getGsrdStockStore());
                replenishD.setGsrdLastSupid(detail.getGsrdLastSupid());
                replenishD.setGsrdVoucherAmt(new BigDecimal(StrUtil.isEmpty(detail.getVoucherAmt()) ? "0" : detail.getVoucherAmt()));
                this.replenishDMapper.insert(replenishD);

                if (ObjectUtil.isNotEmpty(detail.getGsrdProPrice())) {
                    if (StrUtil.isEmpty(detail.getGsrdNeedQty())) {
                        amount = amount.add(detail.getGsrdProPrice().multiply(new BigDecimal("0")));
                    } else {
                        amount = amount.add(detail.getGsrdProPrice().multiply(new BigDecimal(detail.getGsrdNeedQty())));
                    }
                }
            }
            replenishH.setGsrhTotalAmt(amount);
            replenishH.setGsrhTotalQty(count.toString());
        }
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void approve(GetReplenishInData inData, GetLoginOutData userInfo) {
        if (!ObjectUtil.isEmpty(inData.getGsrhVoucherId())) {
            Example exampleStore = new Example(GaiaStoreData.class);
            exampleStore.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("stoCode", inData.getStoCode());
            GaiaStoreData storeData = storeDataMapper.selectOneByExample(exampleStore);
            List<GaiaSdReplenishD> list = new ArrayList<>();
            List<GetReplenishDetailOutData> detailList = this.replenishDMapper.queryDetailList(inData);
            for (GetReplenishDetailOutData data : detailList){
                GetReplenishInData movData = new GetReplenishInData();
                movData.setClientId(data.getClientId());
                movData.setProductCode(data.getGsrdProId());
                movData.setStoCode(inData.getStoCode());
                String mov = replenishDMapper.getMovPrice(movData);
                BigDecimal movPrice = new BigDecimal(mov);
                GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
                replenishD.setClientId(data.getClientId());
                replenishD.setGsrdAverageCost(movPrice);
                replenishD.setGsrdProId(data.getGsrdProId());
                replenishD.setGsrdVoucherId(data.getGsrdVoucherId());
                replenishD.setGsrdVoucherAmt(movPrice.multiply(new BigDecimal(data.getGsrdNeedQty())));
                list.add(replenishD);
            }
            if (list.size() > 0){
                replenishDMapper.updatePrice(list);
            }
            Example example = new Example(GaiaSdReplenishH.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId());
            GaiaSdReplenishH replenishH = replenishHMapper.selectOneByExample(example);
            replenishH.setGsrhStatus("1");
            replenishH.setGsrhType(storeData.getStoAttribute());
            replenishH.setGsrhEmp(inData.getGsrhEmp());
            replenishHMapper.updateByPrimaryKeySelective(replenishH);
        }
    }

    @Override
    public GetReplenishOutData getStock(GetReplenishInData inData) {
        Map<String, String> stockMap = this.getStockMap(inData);
        GetReplenishOutData outData = new GetReplenishOutData();
        outData.setStock(StrUtil.isEmpty(StrUtil.removeSuffix(stockMap.get(inData.getProductCode()), ".0000")) ? "0" : StrUtil.removeSuffix(stockMap.get(inData.getProductCode()), ".0000"));
        return outData;
    }

    private Map<String, String> getStockMap(GetReplenishInData inData) {
        Map<String, String> map;
        Example example = new Example(GaiaSdStock.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", inData.getStoCode());
        List<GaiaSdStock> stockList = this.stockMapper.selectByExample(example);
        map = stockList.stream().collect(Collectors.toMap(GaiaSdStock::getGssProId, GaiaSdStock::getGssQty, (a, b) -> b));
        return map;
    }

    @Override
    public GetReplenishOutData getParam(GetReplenishInData inData) {
        Example example = new Example(GaiaSdSystemPara.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsspBrId", inData.getStoCode()).andEqualTo("gsspId", inData.getParamCode());
        GaiaSdSystemPara systemPara = (GaiaSdSystemPara) this.sdSystemParaMapper.selectOneByExample(example);
        GetReplenishOutData outData = new GetReplenishOutData();
        if (ObjectUtil.isNotEmpty(systemPara)) {
            outData.setParam(systemPara.getGsspPara());
        } else {
            Example exampleD = new Example(GaiaSdSystemParaDefault.class);
            exampleD.createCriteria().andEqualTo("gsspId", inData.getParamCode());
            GaiaSdSystemParaDefault systemParaDefault = (GaiaSdSystemParaDefault) this.sdSystemParaDefaultMapper.selectOneByExample(exampleD);
            if (ObjectUtil.isNotEmpty(systemParaDefault)) {
                outData.setParam(systemParaDefault.getGsspPara());
            }
        }

        return outData;
    }

    @Override
    public GetReplenishOutData getSales(GetReplenishInData inData) {
        Map<String, GetReplenishOutData> saleMap = this.getSalesMap(inData);
        GetReplenishOutData outData = (GetReplenishOutData) saleMap.get(inData.getProductCode());
        if (ObjectUtil.isNull(outData)) {
            outData = new GetReplenishOutData();
            outData.setSalesCountOne("0");
            outData.setSalesCountTwo("0");
            outData.setSalesCountThree("0");
        }
        outData.setSalesCountOne(StrUtil.removeSuffix(outData.getSalesCountOne(), ".0000"));
        outData.setSalesCountTwo(StrUtil.removeSuffix(outData.getSalesCountTwo(), ".0000"));
        outData.setSalesCountThree(StrUtil.removeSuffix(outData.getSalesCountThree(), ".0000"));

        return outData;
    }

    private Map<String, GetReplenishOutData> getSalesMap(GetReplenishInData inData) {
        Map<String, GetReplenishOutData> map;
        List<GetReplenishOutData> list = this.soItemMapper.selectCountByDate(inData);
        map = list.stream().collect(Collectors.toMap(GetReplenishOutData::getProId, x -> x, (a, b) -> b));
        return map;
    }

//    private Map<String, QuerySupplierData> getSupplierNameMap(String clientId) {
//        Map<String, QuerySupplierData> map;
//        List<QuerySupplierData> list = this.replenishDMapper.querySupplierName(clientId);
//        map = list.stream().collect(Collectors.toMap(QuerySupplierData::getPoProCode, x -> x, (a, b) -> b));
//        return map;
//    }

    @Override
    public GetReplenishOutData getCost(GetReplenishInData inData) {
        Map<String, GetReplenishOutData> costMap = this.getCostMap(inData);
        GetReplenishOutData outData = (GetReplenishOutData) costMap.get(inData.getProductCode());
        if (ObjectUtil.isNull(outData)) {
            outData = new GetReplenishOutData();
        }

        return outData;
    }

    private Map<String, GetReplenishOutData> getCostMap(GetReplenishInData inData) {
        Map<String, GetReplenishOutData> map = new HashMap();
        Example example = new Example(GaiaBatchStock.class);
        example.setOrderByClause("BAT_CREATE_DATE DESC");
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("batLocationCode", inData.getStoCode()).andEqualTo("batProCode", inData.getProductCode());
        List<GaiaBatchStock> batchStockList = this.batchStockMapper.selectByExample(example);
        Iterator var5 = batchStockList.iterator();

        while (var5.hasNext()) {
            GaiaBatchStock batchStock = (GaiaBatchStock) var5.next();
            if (!ObjectUtil.isNotNull(map.get(batchStock.getBatProCode()))) {
                GetReplenishOutData outData = new GetReplenishOutData();
                outData.setAverageCost(batchStock.getBatMovPrice());
                outData.setBatchCost(batchStock.getBatBatchCost());
                map.put(batchStock.getBatProCode(), outData);
            }
        }

        return map;
    }

    @Override
    public GetReplenishOutData getCount(GetReplenishInData inData) {
        GetReplenishOutData outData = new GetReplenishOutData();
        Map<String, String> countMap = this.getCountMap(inData);
        String count = (String) countMap.get(inData.getProductCode());
        if (StrUtil.isBlank(count)) {
            count = "0";
        }

        outData.setGsrhTotalQty(count);
        return outData;
    }

    private Map<String, String> getCountMap(GetReplenishInData inData) {
        Map<String, String> map = new HashMap();
        Map<String, String> stockMap = this.getStockMap(inData);
        Example exampleStore = new Example(GaiaStoreData.class);
        exampleStore.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("stoCode", inData.getStoCode());
        GaiaStoreData storeData = (GaiaStoreData) this.storeDataMapper.selectOneByExample(exampleStore);
        int days = 7;
        List list;
        Example examplePara;
        if (StrUtil.isBlank(storeData.getStoChainHead())) {
            GetAcceptInData acceptInData = new GetAcceptInData();
            acceptInData.setClientId(inData.getClientId());
            acceptInData.setGsadProId(inData.getProductCode());
            list = this.poHeaderMapper.poList(acceptInData);
            if (ObjectUtil.isNotEmpty(list)) {
                GetPoOutData poOutData = (GetPoOutData) list.get(0);
                examplePara = new Example(GaiaSupplierBusiness.class);
                examplePara.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("supSelfCode", poOutData.getPoSupplierId()).andEqualTo("supSite", inData.getStoCode());
                GaiaSupplierBusiness supplierBusiness = (GaiaSupplierBusiness) this.supplierBusinessMapper.selectOneByExample(examplePara);
                if (ObjectUtil.isNotEmpty(supplierBusiness)) {
                    days = Integer.parseInt(supplierBusiness.getSupLeadTime());
                }
            }
        } else {
            Example exampleSD = new Example(GaiaSdStoreData.class);
            exampleSD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsstBrId", inData.getStoCode());
            GaiaSdStoreData sdStoreData = (GaiaSdStoreData) this.sdStoreDataMapper.selectOneByExample(exampleSD);
            if (ObjectUtil.isNotEmpty(sdStoreData)) {
                if (ObjectUtil.isNotEmpty(sdStoreData.getGsstPsDate())) {
                    days = Integer.parseInt(sdStoreData.getGsstPsDate());
                }
            }
        }

        Map<String, GetReplenishOutData> saleMap = this.getSalesMap(inData);
        list = this.soItemMapper.selectCountList(inData);
        Map<String, List<Integer>> saleDayMap = new HashMap();
        Iterator var40 = list.iterator();

        while (var40.hasNext()) {
            GetReplenishOutData sale = (GetReplenishOutData) var40.next();
            List<Integer> saleDayList = (List) saleDayMap.get(sale.getProId());
            if (CollUtil.isEmpty((Collection) saleDayList)) {
                saleDayList = new ArrayList();
            }

            if (sale.getSaleQty() != null) {
                (saleDayList).add((new BigDecimal(sale.getSaleQty())).intValue());
            }

            saleDayMap.put(sale.getProId(), saleDayList);
        }

        examplePara = new Example(GaiaSdReplenishPara.class);
        examplePara.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsepBrId", inData.getStoCode());
        List<GaiaSdReplenishPara> replenishParaList = this.replenishParaMapper.selectByExample(examplePara);
        Map<String, GaiaSdReplenishPara> replenishParaMap = new HashMap();
        replenishParaList.forEach((x) -> {
            GaiaSdReplenishPara var10000 = (GaiaSdReplenishPara) replenishParaMap.put(x.getGsepId(), x);
        });
        BigDecimal d = BigDecimal.ZERO;
        Example exampleP = new Example(GaiaSdSystemPara.class);
        exampleP.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsspBrId", inData.getStoCode()).andEqualTo("gsspId", inData.getParamCode());
        GaiaSdSystemPara systemPara = (GaiaSdSystemPara) this.sdSystemParaMapper.selectOneByExample(exampleP);
        if (ObjectUtil.isNotEmpty(systemPara)) {
            d = new BigDecimal(systemPara.getGsspPara());
        } else {
            Example exampleD = new Example(GaiaSdSystemParaDefault.class);
            exampleD.createCriteria().andEqualTo("gsspId", inData.getParamCode());
            GaiaSdSystemParaDefault systemParaDefault = (GaiaSdSystemParaDefault) this.sdSystemParaDefaultMapper.selectOneByExample(exampleD);
            if (ObjectUtil.isNotEmpty(systemParaDefault)) {
                d = new BigDecimal(systemParaDefault.getGsspPara());
            }
        }

        List<GetReplenishDetailOutData> proList = this.productBasicMapper.queryProFromReplenish(inData);
        Iterator var45 = proList.iterator();

        while (true) {
            while (var45.hasNext()) {
                GetReplenishDetailOutData pro = (GetReplenishDetailOutData) var45.next();
                BigDecimal Q = BigDecimal.ZERO;
                BigDecimal X = BigDecimal.ZERO;
                BigDecimal S = BigDecimal.ZERO;
                BigDecimal T = BigDecimal.ZERO;
                new GetReplenishOutData();
                if (StrUtil.isNotBlank((CharSequence) stockMap.get(pro.getGsrdProId()))) {
                    T = new BigDecimal((String) stockMap.get(pro.getGsrdProId()));
                }

                GetReplenishOutData saleData = (GetReplenishOutData) saleMap.get(pro.getGsrdProId());
                if (ObjectUtil.isNotNull(saleData) && (StrUtil.isBlank(saleData.getSalesCountTwo()) || "0".equals(saleData.getSalesCountTwo())) && StrUtil.isNotBlank(saleData.getSalesCountOne()) && !"0".equals(saleData.getSalesCountOne())) {
                    map.put(pro.getGsrdProId(), "2");
                } else {
                    String paramId = "";
                    int sumQty = 0;
                    List<Integer> salesCountList = (List) saleDayMap.get(pro.getGsrdProId());
                    if (salesCountList == null) {
                        salesCountList = new ArrayList();
                    }

                    Integer qty;
                    for (Iterator var28 = ((List) salesCountList).iterator(); var28.hasNext(); sumQty += qty) {
                        qty = (Integer) var28.next();
                    }

                    if (((List) salesCountList).size() <= 15) {
                        X = new BigDecimal(sumQty);
                        if (sumQty < 5) {
                            paramId = "1";
                        }

                        if (sumQty >= 5 && sumQty < 10) {
                            paramId = "2";
                        }

                        if (sumQty >= 10 && sumQty < 15) {
                            paramId = "3";
                        }

                        if (sumQty >= 15 && sumQty < 30) {
                            paramId = "4";
                        }

                        if (sumQty >= 30) {
                            paramId = "5";
                        }
                    }

                    BigDecimal c;
                    if (((List) salesCountList).size() > 15) {
                        BigDecimal aveg = (new BigDecimal(sumQty)).divide(new BigDecimal(((List) salesCountList).size()), 2, 4);
                        c = BigDecimal.ZERO;

                        for (Iterator var30 = ((List) salesCountList).iterator(); var30.hasNext(); c = c.add((new BigDecimal(qty)).subtract(aveg).multiply((new BigDecimal(qty)).subtract(aveg)))) {
                            qty = (Integer) var30.next();
                        }

                        BigDecimal a = new BigDecimal(Math.sqrt(c.doubleValue()));
                        BigDecimal diff = a.divide(new BigDecimal(((List) salesCountList).size()), 2, 4);
                        Iterator it = ((List) salesCountList).iterator();

                        while (it.hasNext()) {
                            BigDecimal b = (new BigDecimal((Integer) it.next())).subtract(aveg).divide(diff, 2, 4);
                            if (b.compareTo(new BigDecimal(3)) > 0) {
                                it.remove();
                            }
                        }

                        Iterator var52 = ((List) salesCountList).iterator();

                        while (var52.hasNext()) {
                            qty = (Integer) var52.next();
                            BigDecimal b = (new BigDecimal(qty)).subtract(aveg).divide(diff, 2, 4);
                            if (b.compareTo(new BigDecimal(3)) > 0) {
                                ((List) salesCountList).remove(qty);
                            }
                        }

                        int sumQty1 = 0;

                        for (Iterator var54 = ((List) salesCountList).iterator(); var54.hasNext(); sumQty1 += qty) {
                            qty = (Integer) var54.next();
                        }

                        if (((List) salesCountList).size() <= 15) {
                            X = new BigDecimal(sumQty1);
                            if (sumQty < 5) {
                                paramId = "1";
                            }

                            if (sumQty >= 5 && sumQty < 10) {
                                paramId = "2";
                            }

                            if (sumQty >= 10 && sumQty < 15) {
                                paramId = "3";
                            }

                            if (sumQty >= 15 && sumQty < 30) {
                                paramId = "4";
                            }

                            if (sumQty >= 30) {
                                paramId = "5";
                            }
                        }
                    }

                    GaiaSdReplenishPara replenishPara = (GaiaSdReplenishPara) replenishParaMap.get(paramId);
                    if (replenishPara != null) {
                        if (days <= 7) {
                            Q = replenishPara.getGsepPara1();
                        } else if (days > 7 && days <= 15) {
                            Q = replenishPara.getGsepPara2();
                        } else if (days > 15) {
                            Q = replenishPara.getGsepPara3();
                        }

                        S = X.multiply(Q).subtract(T);
                        if (S.compareTo(new BigDecimal(0)) <= 0) {
                            map.put(pro.getGsrdProId(), "0");
                        } else {
                            if ("3".equals(pro.getProStorageArea())) {
                                c = S.divide(new BigDecimal(pro.getProMidPackage())).setScale(1, 4);
                                String[] a = c.toString().split("\\.");
                                if (Integer.parseInt(a[1]) >= 8) {
                                    S = new BigDecimal(Integer.parseInt(a[0]) + 1);
                                } else {
                                    S = c;
                                }
                            }

                            if (d.subtract(T).compareTo(S) > 0) {
                                map.put(pro.getGsrdProId(), d.subtract(T).toString());
                            } else {
                                map.put(pro.getGsrdProId(), S.setScale(0, 4).toString());
                            }
                        }
                    }
                }
            }

            return map;
        }
    }

    @Override
    @Transactional
    public boolean isRepeat(GetReplenishInData inData) {
        Example example = new Example(GaiaSdReplenishH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrhDate", inData.getGsrhDate()).andEqualTo("gsrhPattern", "0");
        List<GaiaSdReplenishH> replenishHList = this.replenishHMapper.selectByExample(example);
        System.out.println("replenishHList 查询:" + replenishHList);
        return ObjectUtil.isNotEmpty(replenishHList);
    }

    @Override
    public GetReplenishOutData getVoucherAmt(GetReplenishInData inData) {
        GetReplenishOutData outData = new GetReplenishOutData();
        Map<String, String> amtMap = this.getVoucherAmtMap(inData);
        String amt = (String) amtMap.get(inData.getProductCode());
        if (StrUtil.isBlank(amt)) {
            amt = "0";
        }

        outData.setVoucherAmt(amt);
        return outData;
    }


    private Map<String, String> getVoucherAmtMap(GetReplenishInData inData) {
        Map<String, String> map = new HashMap();
        Example example = new Example(GaiaMaterialAssess.class);
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("matAssessSite", inData.getStoCode());
        List<GaiaMaterialAssess> materialAssessList = this.materialAssessMapper.selectByExample(example);
        materialAssessList.forEach((x) -> {
            String var10000 = (String) map.put(x.getMatProCode(), x.getMatMovPrice().toString());
        });
        return map;
    }
}
