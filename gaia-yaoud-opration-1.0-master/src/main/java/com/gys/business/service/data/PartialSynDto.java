package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2021/5/7
 */
@Data
public class PartialSynDto implements Serializable {
    private static final long serialVersionUID = 8556735751273137239L;

    /**
     * 加盟商
     */
    private List<PartialSynDto> clientList;

    /**
     * 门店
     */
    private List<String> tableNames;

    /**
     * 店号
     */
    private String brId;

    /**
     * 店名
     */
    private String brName;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 加盟商名字
     */
    private String francName;
}

