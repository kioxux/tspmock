package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdWtbhdD;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/13 18:45
 **/
public interface GaiaSdWtbhdDService {

    GaiaSdWtbhdD add(GaiaSdWtbhdD gaiaSdWtbhdD);

    GaiaSdWtbhdD update(GaiaSdWtbhdD gaiaSdWtbhdD);
}
