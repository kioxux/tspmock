package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromCouponUseInData implements Serializable {
    private static final long serialVersionUID = -9129747046146957533L;
    private String clientId;
    private String gspcuVoucherId;
    private String gspcuSerial;
    private String gspcuProId;
    private String gspcuActNo;
    private String gspcuReachQty1;
    private BigDecimal gspcuReachAmt1;
    private String gspcuResultQty1;
    private String gspcuReachQty2;
    private BigDecimal gspcuReachAmt2;
    private String gspcuResultQty2;
    private String gspcuReachQty3;
    private BigDecimal gspcuReachAmt3;
    private String gspcuResultQty3;
    private String gspcuMemFlag;
    private String gspcuInteFlag;
    private String gspcuInteRate;
}
