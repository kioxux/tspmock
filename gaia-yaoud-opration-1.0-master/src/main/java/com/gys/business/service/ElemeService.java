//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetElemeInData;

public interface ElemeService {
    void orderGetOrderDetail(GetElemeInData inData);

    void orderConfirm(GetElemeInData inData);

    void orderCancel(GetElemeInData inData);

    void orderPreparationMealComplete(GetElemeInData inData);

    void orderRefundAgree(GetElemeInData inData);

    void orderRefundReject(GetElemeInData inData);

    void medicineStock(GetElemeInData inData);
}
