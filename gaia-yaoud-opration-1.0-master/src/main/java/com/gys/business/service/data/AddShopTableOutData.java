//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class AddShopTableOutData {
    private String index;
    private String shopName;
    private String shopId;

    public AddShopTableOutData() {
    }

    public String getIndex() {
        return this.index;
    }

    public String getShopName() {
        return this.shopName;
    }

    public String getShopId() {
        return this.shopId;
    }

    public void setIndex(final String index) {
        this.index = index;
    }

    public void setShopName(final String shopName) {
        this.shopName = shopName;
    }

    public void setShopId(final String shopId) {
        this.shopId = shopId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof AddShopTableOutData)) {
            return false;
        } else {
            AddShopTableOutData other = (AddShopTableOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$index = this.getIndex();
                    Object other$index = other.getIndex();
                    if (this$index == null) {
                        if (other$index == null) {
                            break label47;
                        }
                    } else if (this$index.equals(other$index)) {
                        break label47;
                    }

                    return false;
                }

                Object this$shopName = this.getShopName();
                Object other$shopName = other.getShopName();
                if (this$shopName == null) {
                    if (other$shopName != null) {
                        return false;
                    }
                } else if (!this$shopName.equals(other$shopName)) {
                    return false;
                }

                Object this$shopId = this.getShopId();
                Object other$shopId = other.getShopId();
                if (this$shopId == null) {
                    if (other$shopId != null) {
                        return false;
                    }
                } else if (!this$shopId.equals(other$shopId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof AddShopTableOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $shopName = this.getShopName();
        result = result * 59 + ($shopName == null ? 43 : $shopName.hashCode());
        Object $shopId = this.getShopId();
        result = result * 59 + ($shopId == null ? 43 : $shopId.hashCode());
        return result;
    }

    public String toString() {
        return "AddShopTableOutData(index=" + this.getIndex() + ", shopName=" + this.getShopName() + ", shopId=" + this.getShopId() + ")";
    }
}
