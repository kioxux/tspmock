package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProductSpecialDateInData implements Serializable {

    /**
     * 季节 1-春，2-夏，3-秋，4-冬
     */
    @ApiModelProperty(value="季节 1-春，2-夏，3-秋，4-冬")
    private String proSeason;

    /**
     * 起始日期
     */
    @ApiModelProperty(value="起始日期")
    private String startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty(value="结束日期")
    private String endDate;
}
