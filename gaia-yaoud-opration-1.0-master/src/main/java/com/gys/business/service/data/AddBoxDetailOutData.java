//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class AddBoxDetailOutData {
    private String gsidBatch;
    private String proForm;
    private String proSpecs;
    private String proPlace;
    private String proUnit;
    private String gsadStockStatus;
    private String gsidValidDate;
    private String gsidProId;
    private String gsidProName;
    private String proRegisterNo;
    private String proFactoryName;
    private String gsidQty;
    private String indexDetail;
    private String gsidBatchNo;
    private String normalPrice;
    private String gsidSerial;

    public AddBoxDetailOutData() {
    }

    public String getGsidBatch() {
        return this.gsidBatch;
    }

    public String getProForm() {
        return this.proForm;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getGsadStockStatus() {
        return this.gsadStockStatus;
    }

    public String getGsidValidDate() {
        return this.gsidValidDate;
    }

    public String getGsidProId() {
        return this.gsidProId;
    }

    public String getGsidProName() {
        return this.gsidProName;
    }

    public String getProRegisterNo() {
        return this.proRegisterNo;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getGsidQty() {
        return this.gsidQty;
    }

    public String getIndexDetail() {
        return this.indexDetail;
    }

    public String getGsidBatchNo() {
        return this.gsidBatchNo;
    }

    public String getNormalPrice() {
        return this.normalPrice;
    }

    public String getGsidSerial() {
        return this.gsidSerial;
    }

    public void setGsidBatch(final String gsidBatch) {
        this.gsidBatch = gsidBatch;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setGsadStockStatus(final String gsadStockStatus) {
        this.gsadStockStatus = gsadStockStatus;
    }

    public void setGsidValidDate(final String gsidValidDate) {
        this.gsidValidDate = gsidValidDate;
    }

    public void setGsidProId(final String gsidProId) {
        this.gsidProId = gsidProId;
    }

    public void setGsidProName(final String gsidProName) {
        this.gsidProName = gsidProName;
    }

    public void setProRegisterNo(final String proRegisterNo) {
        this.proRegisterNo = proRegisterNo;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setGsidQty(final String gsidQty) {
        this.gsidQty = gsidQty;
    }

    public void setIndexDetail(final String indexDetail) {
        this.indexDetail = indexDetail;
    }

    public void setGsidBatchNo(final String gsidBatchNo) {
        this.gsidBatchNo = gsidBatchNo;
    }

    public void setNormalPrice(final String normalPrice) {
        this.normalPrice = normalPrice;
    }

    public void setGsidSerial(final String gsidSerial) {
        this.gsidSerial = gsidSerial;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof AddBoxDetailOutData)) {
            return false;
        } else {
            AddBoxDetailOutData other = (AddBoxDetailOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label203: {
                    Object this$gsidBatch = this.getGsidBatch();
                    Object other$gsidBatch = other.getGsidBatch();
                    if (this$gsidBatch == null) {
                        if (other$gsidBatch == null) {
                            break label203;
                        }
                    } else if (this$gsidBatch.equals(other$gsidBatch)) {
                        break label203;
                    }

                    return false;
                }

                Object this$proForm = this.getProForm();
                Object other$proForm = other.getProForm();
                if (this$proForm == null) {
                    if (other$proForm != null) {
                        return false;
                    }
                } else if (!this$proForm.equals(other$proForm)) {
                    return false;
                }

                Object this$proSpecs = this.getProSpecs();
                Object other$proSpecs = other.getProSpecs();
                if (this$proSpecs == null) {
                    if (other$proSpecs != null) {
                        return false;
                    }
                } else if (!this$proSpecs.equals(other$proSpecs)) {
                    return false;
                }

                label182: {
                    Object this$proPlace = this.getProPlace();
                    Object other$proPlace = other.getProPlace();
                    if (this$proPlace == null) {
                        if (other$proPlace == null) {
                            break label182;
                        }
                    } else if (this$proPlace.equals(other$proPlace)) {
                        break label182;
                    }

                    return false;
                }

                label175: {
                    Object this$proUnit = this.getProUnit();
                    Object other$proUnit = other.getProUnit();
                    if (this$proUnit == null) {
                        if (other$proUnit == null) {
                            break label175;
                        }
                    } else if (this$proUnit.equals(other$proUnit)) {
                        break label175;
                    }

                    return false;
                }

                label168: {
                    Object this$gsadStockStatus = this.getGsadStockStatus();
                    Object other$gsadStockStatus = other.getGsadStockStatus();
                    if (this$gsadStockStatus == null) {
                        if (other$gsadStockStatus == null) {
                            break label168;
                        }
                    } else if (this$gsadStockStatus.equals(other$gsadStockStatus)) {
                        break label168;
                    }

                    return false;
                }

                Object this$gsidValidDate = this.getGsidValidDate();
                Object other$gsidValidDate = other.getGsidValidDate();
                if (this$gsidValidDate == null) {
                    if (other$gsidValidDate != null) {
                        return false;
                    }
                } else if (!this$gsidValidDate.equals(other$gsidValidDate)) {
                    return false;
                }

                label154: {
                    Object this$gsidProId = this.getGsidProId();
                    Object other$gsidProId = other.getGsidProId();
                    if (this$gsidProId == null) {
                        if (other$gsidProId == null) {
                            break label154;
                        }
                    } else if (this$gsidProId.equals(other$gsidProId)) {
                        break label154;
                    }

                    return false;
                }

                Object this$gsidProName = this.getGsidProName();
                Object other$gsidProName = other.getGsidProName();
                if (this$gsidProName == null) {
                    if (other$gsidProName != null) {
                        return false;
                    }
                } else if (!this$gsidProName.equals(other$gsidProName)) {
                    return false;
                }

                label140: {
                    Object this$proRegisterNo = this.getProRegisterNo();
                    Object other$proRegisterNo = other.getProRegisterNo();
                    if (this$proRegisterNo == null) {
                        if (other$proRegisterNo == null) {
                            break label140;
                        }
                    } else if (this$proRegisterNo.equals(other$proRegisterNo)) {
                        break label140;
                    }

                    return false;
                }

                Object this$proFactoryName = this.getProFactoryName();
                Object other$proFactoryName = other.getProFactoryName();
                if (this$proFactoryName == null) {
                    if (other$proFactoryName != null) {
                        return false;
                    }
                } else if (!this$proFactoryName.equals(other$proFactoryName)) {
                    return false;
                }

                Object this$gsidQty = this.getGsidQty();
                Object other$gsidQty = other.getGsidQty();
                if (this$gsidQty == null) {
                    if (other$gsidQty != null) {
                        return false;
                    }
                } else if (!this$gsidQty.equals(other$gsidQty)) {
                    return false;
                }

                label119: {
                    Object this$indexDetail = this.getIndexDetail();
                    Object other$indexDetail = other.getIndexDetail();
                    if (this$indexDetail == null) {
                        if (other$indexDetail == null) {
                            break label119;
                        }
                    } else if (this$indexDetail.equals(other$indexDetail)) {
                        break label119;
                    }

                    return false;
                }

                label112: {
                    Object this$gsidBatchNo = this.getGsidBatchNo();
                    Object other$gsidBatchNo = other.getGsidBatchNo();
                    if (this$gsidBatchNo == null) {
                        if (other$gsidBatchNo == null) {
                            break label112;
                        }
                    } else if (this$gsidBatchNo.equals(other$gsidBatchNo)) {
                        break label112;
                    }

                    return false;
                }

                Object this$normalPrice = this.getNormalPrice();
                Object other$normalPrice = other.getNormalPrice();
                if (this$normalPrice == null) {
                    if (other$normalPrice != null) {
                        return false;
                    }
                } else if (!this$normalPrice.equals(other$normalPrice)) {
                    return false;
                }

                Object this$gsidSerial = this.getGsidSerial();
                Object other$gsidSerial = other.getGsidSerial();
                if (this$gsidSerial == null) {
                    if (other$gsidSerial != null) {
                        return false;
                    }
                } else if (!this$gsidSerial.equals(other$gsidSerial)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof AddBoxDetailOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $gsidBatch = this.getGsidBatch();
        result = result * 59 + ($gsidBatch == null ? 43 : $gsidBatch.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $gsadStockStatus = this.getGsadStockStatus();
        result = result * 59 + ($gsadStockStatus == null ? 43 : $gsadStockStatus.hashCode());
        Object $gsidValidDate = this.getGsidValidDate();
        result = result * 59 + ($gsidValidDate == null ? 43 : $gsidValidDate.hashCode());
        Object $gsidProId = this.getGsidProId();
        result = result * 59 + ($gsidProId == null ? 43 : $gsidProId.hashCode());
        Object $gsidProName = this.getGsidProName();
        result = result * 59 + ($gsidProName == null ? 43 : $gsidProName.hashCode());
        Object $proRegisterNo = this.getProRegisterNo();
        result = result * 59 + ($proRegisterNo == null ? 43 : $proRegisterNo.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $gsidQty = this.getGsidQty();
        result = result * 59 + ($gsidQty == null ? 43 : $gsidQty.hashCode());
        Object $indexDetail = this.getIndexDetail();
        result = result * 59 + ($indexDetail == null ? 43 : $indexDetail.hashCode());
        Object $gsidBatchNo = this.getGsidBatchNo();
        result = result * 59 + ($gsidBatchNo == null ? 43 : $gsidBatchNo.hashCode());
        Object $normalPrice = this.getNormalPrice();
        result = result * 59 + ($normalPrice == null ? 43 : $normalPrice.hashCode());
        Object $gsidSerial = this.getGsidSerial();
        result = result * 59 + ($gsidSerial == null ? 43 : $gsidSerial.hashCode());
        return result;
    }

    public String toString() {
        return "AddBoxDetailOutData(gsidBatch=" + this.getGsidBatch() + ", proForm=" + this.getProForm() + ", proSpecs=" + this.getProSpecs() + ", proPlace=" + this.getProPlace() + ", proUnit=" + this.getProUnit() + ", gsadStockStatus=" + this.getGsadStockStatus() + ", gsidValidDate=" + this.getGsidValidDate() + ", gsidProId=" + this.getGsidProId() + ", gsidProName=" + this.getGsidProName() + ", proRegisterNo=" + this.getProRegisterNo() + ", proFactoryName=" + this.getProFactoryName() + ", gsidQty=" + this.getGsidQty() + ", indexDetail=" + this.getIndexDetail() + ", gsidBatchNo=" + this.getGsidBatchNo() + ", normalPrice=" + this.getNormalPrice() + ", gsidSerial=" + this.getGsidSerial() + ")";
    }
}
