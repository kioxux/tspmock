//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value = "",description = "销售处方查询表")
public class GaiaSdSaleRecipelRecordOutData {
    @ApiModelProperty(value = "序号")
    private Integer index;
    @ApiModelProperty(value = "审方单号")
    private String gssrVoucherId;
    @ApiModelProperty(value = "销售单号")
    private String gsshBillNo;
    @ApiModelProperty(value = "是否登记处方信息 0否1是")
    private String gssdRecipelFlag;
    @ApiModelProperty(value = "审方类型")
    private String gssrType;
    @ApiModelProperty(value = "登记店名")
    private String gssrBrName;
    @ApiModelProperty(value = "登记日期")
    private String gssrDate;
    @ApiModelProperty(value = "登记时间")
    private String gssrTime;
    @ApiModelProperty(value = "登记人员")
    private String gssrEmp;
    @ApiModelProperty(value = "店名")
    private String gssdBrId;
    @ApiModelProperty(value = "登记店名")
    private String gssdBrName;
    @ApiModelProperty(value = "销售日期")
    private String gsshDate;
    @ApiModelProperty(value = "销售时间")
    private String gsshTime;
    @ApiModelProperty(value = "销售单号")
    private String gssdBillNo;
    @ApiModelProperty(value = "顾客姓名")
    private String gssrCustName;
    @ApiModelProperty(value = "顾客性别")
    private String gssrCustSex;
    @ApiModelProperty(value = "顾客年龄")
    private String gssrCustAge;
    @ApiModelProperty(value = "顾客身份证")
    private String gssrCustIdcard;
    @ApiModelProperty(value = "顾客手机")
    private String gssrCustMobile;
    @ApiModelProperty(value = "审方药师编号")
    private String gssrPharmacistId;
    @ApiModelProperty(value = "审方药师姓名")
    private String gssrPharmacistName;
    @ApiModelProperty(value = "审方日期")
    private String gssrCheckDate;
    @ApiModelProperty(value = "审方时间")
    private String gssrCheckTime;
    @ApiModelProperty(value = "审方状态")
    private String gssrCheckStatus;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "通用名称")
    private String proCommonname;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "计量单位")
    private String proUnit;
    @ApiModelProperty(value = "商品批次")
    private String gssdBatch;
    @ApiModelProperty(value = "数量")
    private String gssdQty;
    @ApiModelProperty(value = "单品零售价")
    private String gssdPrc1;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "剂型")
    private String proForm;
    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;
    @ApiModelProperty(value = "处方编号")
    private String gssrRecipelId;
    @ApiModelProperty(value = "处方医院")
    private String gssrRecipelHospital;
    @ApiModelProperty(value = "处方科室")
    private String gssrRecipelDepartment;
    @ApiModelProperty(value = "处方医生")
    private String gssrRecipelDoctor;
    @ApiModelProperty(value = "症状")
    private String gssrSymptom;
    @ApiModelProperty(value = "诊断")
    private String gssrDiagnose;
    @ApiModelProperty(value = "行号")
    private String serial;

    public GaiaSdSaleRecipelRecordOutData() {
    }

    public Integer getIndex() {
        return this.index;
    }

    public String getGssrVoucherId() {
        return this.gssrVoucherId;
    }

    public String getGsshBillNo() {
        return this.gsshBillNo;
    }

    public String getGssdRecipelFlag() {
        return this.gssdRecipelFlag;
    }

    public String getGssrType() {
        return this.gssrType;
    }

    public String getGssrBrName() {
        return this.gssrBrName;
    }

    public String getGssrDate() {
        return this.gssrDate;
    }

    public String getGssrTime() {
        return this.gssrTime;
    }

    public String getGssrEmp() {
        return this.gssrEmp;
    }

    public String getGssdBrId() {
        return this.gssdBrId;
    }

    public String getGssdBrName() {
        return this.gssdBrName;
    }

    public String getGsshDate() {
        return this.gsshDate;
    }

    public String getGsshTime() {
        return this.gsshTime;
    }

    public String getGssdBillNo() {
        return this.gssdBillNo;
    }

    public String getGssrCustName() {
        return this.gssrCustName;
    }

    public String getGssrCustSex() {
        return this.gssrCustSex;
    }

    public String getGssrCustAge() {
        return this.gssrCustAge;
    }

    public String getGssrCustIdcard() {
        return this.gssrCustIdcard;
    }

    public String getGssrCustMobile() {
        return this.gssrCustMobile;
    }

    public String getGssrPharmacistId() {
        return this.gssrPharmacistId;
    }

    public String getGssrPharmacistName() {
        return this.gssrPharmacistName;
    }

    public String getGssrCheckDate() {
        return this.gssrCheckDate;
    }

    public String getGssrCheckTime() {
        return this.gssrCheckTime;
    }

    public String getGssrCheckStatus() {
        return this.gssrCheckStatus;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getProCommonname() {
        return this.proCommonname;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getProUnit() {
        return this.proUnit;
    }

    public String getGssdBatch() {
        return this.gssdBatch;
    }

    public String getGssdQty() {
        return this.gssdQty;
    }

    public String getGssdPrc1() {
        return this.gssdPrc1;
    }

    public String getProFactoryName() {
        return this.proFactoryName;
    }

    public String getProPlace() {
        return this.proPlace;
    }

    public String getProForm() {
        return this.proForm;
    }

    public String getProRegisterNo() {
        return this.proRegisterNo;
    }

    public String getGssrRecipelId() {
        return this.gssrRecipelId;
    }

    public String getGssrRecipelHospital() {
        return this.gssrRecipelHospital;
    }

    public String getGssrRecipelDepartment() {
        return this.gssrRecipelDepartment;
    }

    public String getGssrRecipelDoctor() {
        return this.gssrRecipelDoctor;
    }

    public String getGssrSymptom() {
        return this.gssrSymptom;
    }

    public String getGssrDiagnose() {
        return this.gssrDiagnose;
    }

    public String getSerial() {
        return this.serial;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public void setGssrVoucherId(final String gssrVoucherId) {
        this.gssrVoucherId = gssrVoucherId;
    }

    public void setGsshBillNo(final String gsshBillNo) {
        this.gsshBillNo = gsshBillNo;
    }

    public void setGssdRecipelFlag(final String gssdRecipelFlag) {
        this.gssdRecipelFlag = gssdRecipelFlag;
    }

    public void setGssrType(final String gssrType) {
        this.gssrType = gssrType;
    }

    public void setGssrBrName(final String gssrBrName) {
        this.gssrBrName = gssrBrName;
    }

    public void setGssrDate(final String gssrDate) {
        this.gssrDate = gssrDate;
    }

    public void setGssrTime(final String gssrTime) {
        this.gssrTime = gssrTime;
    }

    public void setGssrEmp(final String gssrEmp) {
        this.gssrEmp = gssrEmp;
    }

    public void setGssdBrId(final String gssdBrId) {
        this.gssdBrId = gssdBrId;
    }

    public void setGssdBrName(final String gssdBrName) {
        this.gssdBrName = gssdBrName;
    }

    public void setGsshDate(final String gsshDate) {
        this.gsshDate = gsshDate;
    }

    public void setGsshTime(final String gsshTime) {
        this.gsshTime = gsshTime;
    }

    public void setGssdBillNo(final String gssdBillNo) {
        this.gssdBillNo = gssdBillNo;
    }

    public void setGssrCustName(final String gssrCustName) {
        this.gssrCustName = gssrCustName;
    }

    public void setGssrCustSex(final String gssrCustSex) {
        this.gssrCustSex = gssrCustSex;
    }

    public void setGssrCustAge(final String gssrCustAge) {
        this.gssrCustAge = gssrCustAge;
    }

    public void setGssrCustIdcard(final String gssrCustIdcard) {
        this.gssrCustIdcard = gssrCustIdcard;
    }

    public void setGssrCustMobile(final String gssrCustMobile) {
        this.gssrCustMobile = gssrCustMobile;
    }

    public void setGssrPharmacistId(final String gssrPharmacistId) {
        this.gssrPharmacistId = gssrPharmacistId;
    }

    public void setGssrPharmacistName(final String gssrPharmacistName) {
        this.gssrPharmacistName = gssrPharmacistName;
    }

    public void setGssrCheckDate(final String gssrCheckDate) {
        this.gssrCheckDate = gssrCheckDate;
    }

    public void setGssrCheckTime(final String gssrCheckTime) {
        this.gssrCheckTime = gssrCheckTime;
    }

    public void setGssrCheckStatus(final String gssrCheckStatus) {
        this.gssrCheckStatus = gssrCheckStatus;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setProCommonname(final String proCommonname) {
        this.proCommonname = proCommonname;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setProUnit(final String proUnit) {
        this.proUnit = proUnit;
    }

    public void setGssdBatch(final String gssdBatch) {
        this.gssdBatch = gssdBatch;
    }

    public void setGssdQty(final String gssdQty) {
        this.gssdQty = gssdQty;
    }

    public void setGssdPrc1(final String gssdPrc1) {
        this.gssdPrc1 = gssdPrc1;
    }

    public void setProFactoryName(final String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public void setProPlace(final String proPlace) {
        this.proPlace = proPlace;
    }

    public void setProForm(final String proForm) {
        this.proForm = proForm;
    }

    public void setProRegisterNo(final String proRegisterNo) {
        this.proRegisterNo = proRegisterNo;
    }

    public void setGssrRecipelId(final String gssrRecipelId) {
        this.gssrRecipelId = gssrRecipelId;
    }

    public void setGssrRecipelHospital(final String gssrRecipelHospital) {
        this.gssrRecipelHospital = gssrRecipelHospital;
    }

    public void setGssrRecipelDepartment(final String gssrRecipelDepartment) {
        this.gssrRecipelDepartment = gssrRecipelDepartment;
    }

    public void setGssrRecipelDoctor(final String gssrRecipelDoctor) {
        this.gssrRecipelDoctor = gssrRecipelDoctor;
    }

    public void setGssrSymptom(final String gssrSymptom) {
        this.gssrSymptom = gssrSymptom;
    }

    public void setGssrDiagnose(final String gssrDiagnose) {
        this.gssrDiagnose = gssrDiagnose;
    }

    public void setSerial(final String serial) {
        this.serial = serial;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GaiaSdSaleRecipelRecordOutData)) {
            return false;
        } else {
            GaiaSdSaleRecipelRecordOutData other = (GaiaSdSaleRecipelRecordOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                Object this$gssrVoucherId = this.getGssrVoucherId();
                Object other$gssrVoucherId = other.getGssrVoucherId();
                if (this$gssrVoucherId == null) {
                    if (other$gssrVoucherId != null) {
                        return false;
                    }
                } else if (!this$gssrVoucherId.equals(other$gssrVoucherId)) {
                    return false;
                }

                Object this$gsshBillNo = this.getGsshBillNo();
                Object other$gsshBillNo = other.getGsshBillNo();
                if (this$gsshBillNo == null) {
                    if (other$gsshBillNo != null) {
                        return false;
                    }
                } else if (!this$gsshBillNo.equals(other$gsshBillNo)) {
                    return false;
                }

                label494: {
                    Object this$gssdRecipelFlag = this.getGssdRecipelFlag();
                    Object other$gssdRecipelFlag = other.getGssdRecipelFlag();
                    if (this$gssdRecipelFlag == null) {
                        if (other$gssdRecipelFlag == null) {
                            break label494;
                        }
                    } else if (this$gssdRecipelFlag.equals(other$gssdRecipelFlag)) {
                        break label494;
                    }

                    return false;
                }

                label487: {
                    Object this$gssrType = this.getGssrType();
                    Object other$gssrType = other.getGssrType();
                    if (this$gssrType == null) {
                        if (other$gssrType == null) {
                            break label487;
                        }
                    } else if (this$gssrType.equals(other$gssrType)) {
                        break label487;
                    }

                    return false;
                }

                Object this$gssrBrName = this.getGssrBrName();
                Object other$gssrBrName = other.getGssrBrName();
                if (this$gssrBrName == null) {
                    if (other$gssrBrName != null) {
                        return false;
                    }
                } else if (!this$gssrBrName.equals(other$gssrBrName)) {
                    return false;
                }

                label473: {
                    Object this$gssrDate = this.getGssrDate();
                    Object other$gssrDate = other.getGssrDate();
                    if (this$gssrDate == null) {
                        if (other$gssrDate == null) {
                            break label473;
                        }
                    } else if (this$gssrDate.equals(other$gssrDate)) {
                        break label473;
                    }

                    return false;
                }

                label466: {
                    Object this$gssrTime = this.getGssrTime();
                    Object other$gssrTime = other.getGssrTime();
                    if (this$gssrTime == null) {
                        if (other$gssrTime == null) {
                            break label466;
                        }
                    } else if (this$gssrTime.equals(other$gssrTime)) {
                        break label466;
                    }

                    return false;
                }

                Object this$gssrEmp = this.getGssrEmp();
                Object other$gssrEmp = other.getGssrEmp();
                if (this$gssrEmp == null) {
                    if (other$gssrEmp != null) {
                        return false;
                    }
                } else if (!this$gssrEmp.equals(other$gssrEmp)) {
                    return false;
                }

                Object this$gssdBrId = this.getGssdBrId();
                Object other$gssdBrId = other.getGssdBrId();
                if (this$gssdBrId == null) {
                    if (other$gssdBrId != null) {
                        return false;
                    }
                } else if (!this$gssdBrId.equals(other$gssdBrId)) {
                    return false;
                }

                label445: {
                    Object this$gssdBrName = this.getGssdBrName();
                    Object other$gssdBrName = other.getGssdBrName();
                    if (this$gssdBrName == null) {
                        if (other$gssdBrName == null) {
                            break label445;
                        }
                    } else if (this$gssdBrName.equals(other$gssdBrName)) {
                        break label445;
                    }

                    return false;
                }

                label438: {
                    Object this$gsshDate = this.getGsshDate();
                    Object other$gsshDate = other.getGsshDate();
                    if (this$gsshDate == null) {
                        if (other$gsshDate == null) {
                            break label438;
                        }
                    } else if (this$gsshDate.equals(other$gsshDate)) {
                        break label438;
                    }

                    return false;
                }

                Object this$gsshTime = this.getGsshTime();
                Object other$gsshTime = other.getGsshTime();
                if (this$gsshTime == null) {
                    if (other$gsshTime != null) {
                        return false;
                    }
                } else if (!this$gsshTime.equals(other$gsshTime)) {
                    return false;
                }

                label424: {
                    Object this$gssdBillNo = this.getGssdBillNo();
                    Object other$gssdBillNo = other.getGssdBillNo();
                    if (this$gssdBillNo == null) {
                        if (other$gssdBillNo == null) {
                            break label424;
                        }
                    } else if (this$gssdBillNo.equals(other$gssdBillNo)) {
                        break label424;
                    }

                    return false;
                }

                Object this$gssrCustName = this.getGssrCustName();
                Object other$gssrCustName = other.getGssrCustName();
                if (this$gssrCustName == null) {
                    if (other$gssrCustName != null) {
                        return false;
                    }
                } else if (!this$gssrCustName.equals(other$gssrCustName)) {
                    return false;
                }

                label410: {
                    Object this$gssrCustSex = this.getGssrCustSex();
                    Object other$gssrCustSex = other.getGssrCustSex();
                    if (this$gssrCustSex == null) {
                        if (other$gssrCustSex == null) {
                            break label410;
                        }
                    } else if (this$gssrCustSex.equals(other$gssrCustSex)) {
                        break label410;
                    }

                    return false;
                }

                Object this$gssrCustAge = this.getGssrCustAge();
                Object other$gssrCustAge = other.getGssrCustAge();
                if (this$gssrCustAge == null) {
                    if (other$gssrCustAge != null) {
                        return false;
                    }
                } else if (!this$gssrCustAge.equals(other$gssrCustAge)) {
                    return false;
                }

                Object this$gssrCustIdcard = this.getGssrCustIdcard();
                Object other$gssrCustIdcard = other.getGssrCustIdcard();
                if (this$gssrCustIdcard == null) {
                    if (other$gssrCustIdcard != null) {
                        return false;
                    }
                } else if (!this$gssrCustIdcard.equals(other$gssrCustIdcard)) {
                    return false;
                }

                Object this$gssrCustMobile = this.getGssrCustMobile();
                Object other$gssrCustMobile = other.getGssrCustMobile();
                if (this$gssrCustMobile == null) {
                    if (other$gssrCustMobile != null) {
                        return false;
                    }
                } else if (!this$gssrCustMobile.equals(other$gssrCustMobile)) {
                    return false;
                }

                label382: {
                    Object this$gssrPharmacistId = this.getGssrPharmacistId();
                    Object other$gssrPharmacistId = other.getGssrPharmacistId();
                    if (this$gssrPharmacistId == null) {
                        if (other$gssrPharmacistId == null) {
                            break label382;
                        }
                    } else if (this$gssrPharmacistId.equals(other$gssrPharmacistId)) {
                        break label382;
                    }

                    return false;
                }

                label375: {
                    Object this$gssrPharmacistName = this.getGssrPharmacistName();
                    Object other$gssrPharmacistName = other.getGssrPharmacistName();
                    if (this$gssrPharmacistName == null) {
                        if (other$gssrPharmacistName == null) {
                            break label375;
                        }
                    } else if (this$gssrPharmacistName.equals(other$gssrPharmacistName)) {
                        break label375;
                    }

                    return false;
                }

                Object this$gssrCheckDate = this.getGssrCheckDate();
                Object other$gssrCheckDate = other.getGssrCheckDate();
                if (this$gssrCheckDate == null) {
                    if (other$gssrCheckDate != null) {
                        return false;
                    }
                } else if (!this$gssrCheckDate.equals(other$gssrCheckDate)) {
                    return false;
                }

                label361: {
                    Object this$gssrCheckTime = this.getGssrCheckTime();
                    Object other$gssrCheckTime = other.getGssrCheckTime();
                    if (this$gssrCheckTime == null) {
                        if (other$gssrCheckTime == null) {
                            break label361;
                        }
                    } else if (this$gssrCheckTime.equals(other$gssrCheckTime)) {
                        break label361;
                    }

                    return false;
                }

                label354: {
                    Object this$gssrCheckStatus = this.getGssrCheckStatus();
                    Object other$gssrCheckStatus = other.getGssrCheckStatus();
                    if (this$gssrCheckStatus == null) {
                        if (other$gssrCheckStatus == null) {
                            break label354;
                        }
                    } else if (this$gssrCheckStatus.equals(other$gssrCheckStatus)) {
                        break label354;
                    }

                    return false;
                }

                Object this$proCode = this.getProCode();
                Object other$proCode = other.getProCode();
                if (this$proCode == null) {
                    if (other$proCode != null) {
                        return false;
                    }
                } else if (!this$proCode.equals(other$proCode)) {
                    return false;
                }

                Object this$proCommonname = this.getProCommonname();
                Object other$proCommonname = other.getProCommonname();
                if (this$proCommonname == null) {
                    if (other$proCommonname != null) {
                        return false;
                    }
                } else if (!this$proCommonname.equals(other$proCommonname)) {
                    return false;
                }

                label333: {
                    Object this$proSpecs = this.getProSpecs();
                    Object other$proSpecs = other.getProSpecs();
                    if (this$proSpecs == null) {
                        if (other$proSpecs == null) {
                            break label333;
                        }
                    } else if (this$proSpecs.equals(other$proSpecs)) {
                        break label333;
                    }

                    return false;
                }

                label326: {
                    Object this$proUnit = this.getProUnit();
                    Object other$proUnit = other.getProUnit();
                    if (this$proUnit == null) {
                        if (other$proUnit == null) {
                            break label326;
                        }
                    } else if (this$proUnit.equals(other$proUnit)) {
                        break label326;
                    }

                    return false;
                }

                Object this$gssdBatch = this.getGssdBatch();
                Object other$gssdBatch = other.getGssdBatch();
                if (this$gssdBatch == null) {
                    if (other$gssdBatch != null) {
                        return false;
                    }
                } else if (!this$gssdBatch.equals(other$gssdBatch)) {
                    return false;
                }

                label312: {
                    Object this$gssdQty = this.getGssdQty();
                    Object other$gssdQty = other.getGssdQty();
                    if (this$gssdQty == null) {
                        if (other$gssdQty == null) {
                            break label312;
                        }
                    } else if (this$gssdQty.equals(other$gssdQty)) {
                        break label312;
                    }

                    return false;
                }

                Object this$gssdPrc1 = this.getGssdPrc1();
                Object other$gssdPrc1 = other.getGssdPrc1();
                if (this$gssdPrc1 == null) {
                    if (other$gssdPrc1 != null) {
                        return false;
                    }
                } else if (!this$gssdPrc1.equals(other$gssdPrc1)) {
                    return false;
                }

                label298: {
                    Object this$proFactoryName = this.getProFactoryName();
                    Object other$proFactoryName = other.getProFactoryName();
                    if (this$proFactoryName == null) {
                        if (other$proFactoryName == null) {
                            break label298;
                        }
                    } else if (this$proFactoryName.equals(other$proFactoryName)) {
                        break label298;
                    }

                    return false;
                }

                Object this$proPlace = this.getProPlace();
                Object other$proPlace = other.getProPlace();
                if (this$proPlace == null) {
                    if (other$proPlace != null) {
                        return false;
                    }
                } else if (!this$proPlace.equals(other$proPlace)) {
                    return false;
                }

                Object this$proForm = this.getProForm();
                Object other$proForm = other.getProForm();
                if (this$proForm == null) {
                    if (other$proForm != null) {
                        return false;
                    }
                } else if (!this$proForm.equals(other$proForm)) {
                    return false;
                }

                Object this$proRegisterNo = this.getProRegisterNo();
                Object other$proRegisterNo = other.getProRegisterNo();
                if (this$proRegisterNo == null) {
                    if (other$proRegisterNo != null) {
                        return false;
                    }
                } else if (!this$proRegisterNo.equals(other$proRegisterNo)) {
                    return false;
                }

                label270: {
                    Object this$gssrRecipelId = this.getGssrRecipelId();
                    Object other$gssrRecipelId = other.getGssrRecipelId();
                    if (this$gssrRecipelId == null) {
                        if (other$gssrRecipelId == null) {
                            break label270;
                        }
                    } else if (this$gssrRecipelId.equals(other$gssrRecipelId)) {
                        break label270;
                    }

                    return false;
                }

                label263: {
                    Object this$gssrRecipelHospital = this.getGssrRecipelHospital();
                    Object other$gssrRecipelHospital = other.getGssrRecipelHospital();
                    if (this$gssrRecipelHospital == null) {
                        if (other$gssrRecipelHospital == null) {
                            break label263;
                        }
                    } else if (this$gssrRecipelHospital.equals(other$gssrRecipelHospital)) {
                        break label263;
                    }

                    return false;
                }

                Object this$gssrRecipelDepartment = this.getGssrRecipelDepartment();
                Object other$gssrRecipelDepartment = other.getGssrRecipelDepartment();
                if (this$gssrRecipelDepartment == null) {
                    if (other$gssrRecipelDepartment != null) {
                        return false;
                    }
                } else if (!this$gssrRecipelDepartment.equals(other$gssrRecipelDepartment)) {
                    return false;
                }

                label249: {
                    Object this$gssrRecipelDoctor = this.getGssrRecipelDoctor();
                    Object other$gssrRecipelDoctor = other.getGssrRecipelDoctor();
                    if (this$gssrRecipelDoctor == null) {
                        if (other$gssrRecipelDoctor == null) {
                            break label249;
                        }
                    } else if (this$gssrRecipelDoctor.equals(other$gssrRecipelDoctor)) {
                        break label249;
                    }

                    return false;
                }

                label242: {
                    Object this$gssrSymptom = this.getGssrSymptom();
                    Object other$gssrSymptom = other.getGssrSymptom();
                    if (this$gssrSymptom == null) {
                        if (other$gssrSymptom == null) {
                            break label242;
                        }
                    } else if (this$gssrSymptom.equals(other$gssrSymptom)) {
                        break label242;
                    }

                    return false;
                }

                Object this$gssrDiagnose = this.getGssrDiagnose();
                Object other$gssrDiagnose = other.getGssrDiagnose();
                if (this$gssrDiagnose == null) {
                    if (other$gssrDiagnose != null) {
                        return false;
                    }
                } else if (!this$gssrDiagnose.equals(other$gssrDiagnose)) {
                    return false;
                }

                Object this$serial = this.getSerial();
                Object other$serial = other.getSerial();
                if (this$serial == null) {
                    if (other$serial != null) {
                        return false;
                    }
                } else if (!this$serial.equals(other$serial)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GaiaSdSaleRecipelRecordOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $gssrVoucherId = this.getGssrVoucherId();
        result = result * 59 + ($gssrVoucherId == null ? 43 : $gssrVoucherId.hashCode());
        Object $gsshBillNo = this.getGsshBillNo();
        result = result * 59 + ($gsshBillNo == null ? 43 : $gsshBillNo.hashCode());
        Object $gssdRecipelFlag = this.getGssdRecipelFlag();
        result = result * 59 + ($gssdRecipelFlag == null ? 43 : $gssdRecipelFlag.hashCode());
        Object $gssrType = this.getGssrType();
        result = result * 59 + ($gssrType == null ? 43 : $gssrType.hashCode());
        Object $gssrBrName = this.getGssrBrName();
        result = result * 59 + ($gssrBrName == null ? 43 : $gssrBrName.hashCode());
        Object $gssrDate = this.getGssrDate();
        result = result * 59 + ($gssrDate == null ? 43 : $gssrDate.hashCode());
        Object $gssrTime = this.getGssrTime();
        result = result * 59 + ($gssrTime == null ? 43 : $gssrTime.hashCode());
        Object $gssrEmp = this.getGssrEmp();
        result = result * 59 + ($gssrEmp == null ? 43 : $gssrEmp.hashCode());
        Object $gssdBrId = this.getGssdBrId();
        result = result * 59 + ($gssdBrId == null ? 43 : $gssdBrId.hashCode());
        Object $gssdBrName = this.getGssdBrName();
        result = result * 59 + ($gssdBrName == null ? 43 : $gssdBrName.hashCode());
        Object $gsshDate = this.getGsshDate();
        result = result * 59 + ($gsshDate == null ? 43 : $gsshDate.hashCode());
        Object $gsshTime = this.getGsshTime();
        result = result * 59 + ($gsshTime == null ? 43 : $gsshTime.hashCode());
        Object $gssdBillNo = this.getGssdBillNo();
        result = result * 59 + ($gssdBillNo == null ? 43 : $gssdBillNo.hashCode());
        Object $gssrCustName = this.getGssrCustName();
        result = result * 59 + ($gssrCustName == null ? 43 : $gssrCustName.hashCode());
        Object $gssrCustSex = this.getGssrCustSex();
        result = result * 59 + ($gssrCustSex == null ? 43 : $gssrCustSex.hashCode());
        Object $gssrCustAge = this.getGssrCustAge();
        result = result * 59 + ($gssrCustAge == null ? 43 : $gssrCustAge.hashCode());
        Object $gssrCustIdcard = this.getGssrCustIdcard();
        result = result * 59 + ($gssrCustIdcard == null ? 43 : $gssrCustIdcard.hashCode());
        Object $gssrCustMobile = this.getGssrCustMobile();
        result = result * 59 + ($gssrCustMobile == null ? 43 : $gssrCustMobile.hashCode());
        Object $gssrPharmacistId = this.getGssrPharmacistId();
        result = result * 59 + ($gssrPharmacistId == null ? 43 : $gssrPharmacistId.hashCode());
        Object $gssrPharmacistName = this.getGssrPharmacistName();
        result = result * 59 + ($gssrPharmacistName == null ? 43 : $gssrPharmacistName.hashCode());
        Object $gssrCheckDate = this.getGssrCheckDate();
        result = result * 59 + ($gssrCheckDate == null ? 43 : $gssrCheckDate.hashCode());
        Object $gssrCheckTime = this.getGssrCheckTime();
        result = result * 59 + ($gssrCheckTime == null ? 43 : $gssrCheckTime.hashCode());
        Object $gssrCheckStatus = this.getGssrCheckStatus();
        result = result * 59 + ($gssrCheckStatus == null ? 43 : $gssrCheckStatus.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $proCommonname = this.getProCommonname();
        result = result * 59 + ($proCommonname == null ? 43 : $proCommonname.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $proUnit = this.getProUnit();
        result = result * 59 + ($proUnit == null ? 43 : $proUnit.hashCode());
        Object $gssdBatch = this.getGssdBatch();
        result = result * 59 + ($gssdBatch == null ? 43 : $gssdBatch.hashCode());
        Object $gssdQty = this.getGssdQty();
        result = result * 59 + ($gssdQty == null ? 43 : $gssdQty.hashCode());
        Object $gssdPrc1 = this.getGssdPrc1();
        result = result * 59 + ($gssdPrc1 == null ? 43 : $gssdPrc1.hashCode());
        Object $proFactoryName = this.getProFactoryName();
        result = result * 59 + ($proFactoryName == null ? 43 : $proFactoryName.hashCode());
        Object $proPlace = this.getProPlace();
        result = result * 59 + ($proPlace == null ? 43 : $proPlace.hashCode());
        Object $proForm = this.getProForm();
        result = result * 59 + ($proForm == null ? 43 : $proForm.hashCode());
        Object $proRegisterNo = this.getProRegisterNo();
        result = result * 59 + ($proRegisterNo == null ? 43 : $proRegisterNo.hashCode());
        Object $gssrRecipelId = this.getGssrRecipelId();
        result = result * 59 + ($gssrRecipelId == null ? 43 : $gssrRecipelId.hashCode());
        Object $gssrRecipelHospital = this.getGssrRecipelHospital();
        result = result * 59 + ($gssrRecipelHospital == null ? 43 : $gssrRecipelHospital.hashCode());
        Object $gssrRecipelDepartment = this.getGssrRecipelDepartment();
        result = result * 59 + ($gssrRecipelDepartment == null ? 43 : $gssrRecipelDepartment.hashCode());
        Object $gssrRecipelDoctor = this.getGssrRecipelDoctor();
        result = result * 59 + ($gssrRecipelDoctor == null ? 43 : $gssrRecipelDoctor.hashCode());
        Object $gssrSymptom = this.getGssrSymptom();
        result = result * 59 + ($gssrSymptom == null ? 43 : $gssrSymptom.hashCode());
        Object $gssrDiagnose = this.getGssrDiagnose();
        result = result * 59 + ($gssrDiagnose == null ? 43 : $gssrDiagnose.hashCode());
        Object $serial = this.getSerial();
        result = result * 59 + ($serial == null ? 43 : $serial.hashCode());
        return result;
    }

    public String toString() {
        return "GaiaSdSaleRecipelRecordOutData(index=" + this.getIndex() + ", gssrVoucherId=" + this.getGssrVoucherId() + ", gsshBillNo=" + this.getGsshBillNo() + ", gssdRecipelFlag=" + this.getGssdRecipelFlag() + ", gssrType=" + this.getGssrType() + ", gssrBrName=" + this.getGssrBrName() + ", gssrDate=" + this.getGssrDate() + ", gssrTime=" + this.getGssrTime() + ", gssrEmp=" + this.getGssrEmp() + ", gssdBrId=" + this.getGssdBrId() + ", gssdBrName=" + this.getGssdBrName() + ", gsshDate=" + this.getGsshDate() + ", gsshTime=" + this.getGsshTime() + ", gssdBillNo=" + this.getGssdBillNo() + ", gssrCustName=" + this.getGssrCustName() + ", gssrCustSex=" + this.getGssrCustSex() + ", gssrCustAge=" + this.getGssrCustAge() + ", gssrCustIdcard=" + this.getGssrCustIdcard() + ", gssrCustMobile=" + this.getGssrCustMobile() + ", gssrPharmacistId=" + this.getGssrPharmacistId() + ", gssrPharmacistName=" + this.getGssrPharmacistName() + ", gssrCheckDate=" + this.getGssrCheckDate() + ", gssrCheckTime=" + this.getGssrCheckTime() + ", gssrCheckStatus=" + this.getGssrCheckStatus() + ", proCode=" + this.getProCode() + ", proCommonname=" + this.getProCommonname() + ", proSpecs=" + this.getProSpecs() + ", proUnit=" + this.getProUnit() + ", gssdBatch=" + this.getGssdBatch() + ", gssdQty=" + this.getGssdQty() + ", gssdPrc1=" + this.getGssdPrc1() + ", proFactoryName=" + this.getProFactoryName() + ", proPlace=" + this.getProPlace() + ", proForm=" + this.getProForm() + ", proRegisterNo=" + this.getProRegisterNo() + ", gssrRecipelId=" + this.getGssrRecipelId() + ", gssrRecipelHospital=" + this.getGssrRecipelHospital() + ", gssrRecipelDepartment=" + this.getGssrRecipelDepartment() + ", gssrRecipelDoctor=" + this.getGssrRecipelDoctor() + ", gssrSymptom=" + this.getGssrSymptom() + ", gssrDiagnose=" + this.getGssrDiagnose() + ", serial=" + this.getSerial() + ")";
    }
}
