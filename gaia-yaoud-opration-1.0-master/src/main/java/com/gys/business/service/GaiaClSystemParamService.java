package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdEnv;
import com.gys.business.service.data.GaiaClSystemPara;
import com.gys.business.service.data.UserInData;

/**
 * @desc:
 * @author: RULAISZ
 * @createTime: 2021/12/30 18:47
 */
public interface GaiaClSystemParamService {
    GaiaSdEnv getEnv(UserInData inData);
}
