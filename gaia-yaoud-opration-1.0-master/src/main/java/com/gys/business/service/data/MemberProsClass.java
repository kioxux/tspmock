package com.gys.business.service.data;

import lombok.Data;

/**
 * 商品自分类
 */
@Data
public class MemberProsClass {
    private String proClass;
}
