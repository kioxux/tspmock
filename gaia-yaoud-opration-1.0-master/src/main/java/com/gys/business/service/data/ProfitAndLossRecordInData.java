package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 传参实体
 *
 * @author xiaoyuan on 2020/9/9
 */
@Data
public class ProfitAndLossRecordInData implements Serializable {
    private static final long serialVersionUID = -3614598684807777526L;

    @ApiModelProperty(value = "加盟号")
    private String client;

    @ApiModelProperty(value = "店号")
    private String brId;

    @NotBlank(message = "起始日期不能为空")
    @ApiModelProperty(value = "起始日期")
    private String startDate;

    @NotBlank(message = "结束日期不能为空")
    @ApiModelProperty(value = "结束日期")
    private String endDate;

    @ApiModelProperty(value = "损益类型 1盘点转入 2批号养护 3门店领用 4门店报损 ")
    private String gsishIsType;

    @ApiModelProperty(value = "损益单号")
    private String gsisdVoucherId;

    @ApiModelProperty(value = "商品编码")
    private String gsisdProId;

    @ApiModelProperty(value = "批号")
    private String gsisdBatchNo;

    private String userId;

    private Integer pageNum;
    private Integer pageSize;
}
