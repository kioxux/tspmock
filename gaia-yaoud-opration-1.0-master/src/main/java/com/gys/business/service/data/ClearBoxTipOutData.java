//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class ClearBoxTipOutData {
    private String gschDate;
    private String gsehDate;
    private String gschVoucherId;
    private String gschExaVoucherId;
    private String gsehType;

    public ClearBoxTipOutData() {
    }

    public String getGschDate() {
        return this.gschDate;
    }

    public String getGsehDate() {
        return this.gsehDate;
    }

    public String getGschVoucherId() {
        return this.gschVoucherId;
    }

    public String getGschExaVoucherId() {
        return this.gschExaVoucherId;
    }

    public String getGsehType() {
        return this.gsehType;
    }

    public void setGschDate(final String gschDate) {
        this.gschDate = gschDate;
    }

    public void setGsehDate(final String gsehDate) {
        this.gsehDate = gsehDate;
    }

    public void setGschVoucherId(final String gschVoucherId) {
        this.gschVoucherId = gschVoucherId;
    }

    public void setGschExaVoucherId(final String gschExaVoucherId) {
        this.gschExaVoucherId = gschExaVoucherId;
    }

    public void setGsehType(final String gsehType) {
        this.gsehType = gsehType;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ClearBoxTipOutData)) {
            return false;
        } else {
            ClearBoxTipOutData other = (ClearBoxTipOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$gschDate = this.getGschDate();
                    Object other$gschDate = other.getGschDate();
                    if (this$gschDate == null) {
                        if (other$gschDate == null) {
                            break label71;
                        }
                    } else if (this$gschDate.equals(other$gschDate)) {
                        break label71;
                    }

                    return false;
                }

                Object this$gsehDate = this.getGsehDate();
                Object other$gsehDate = other.getGsehDate();
                if (this$gsehDate == null) {
                    if (other$gsehDate != null) {
                        return false;
                    }
                } else if (!this$gsehDate.equals(other$gsehDate)) {
                    return false;
                }

                label57: {
                    Object this$gschVoucherId = this.getGschVoucherId();
                    Object other$gschVoucherId = other.getGschVoucherId();
                    if (this$gschVoucherId == null) {
                        if (other$gschVoucherId == null) {
                            break label57;
                        }
                    } else if (this$gschVoucherId.equals(other$gschVoucherId)) {
                        break label57;
                    }

                    return false;
                }

                Object this$gschExaVoucherId = this.getGschExaVoucherId();
                Object other$gschExaVoucherId = other.getGschExaVoucherId();
                if (this$gschExaVoucherId == null) {
                    if (other$gschExaVoucherId != null) {
                        return false;
                    }
                } else if (!this$gschExaVoucherId.equals(other$gschExaVoucherId)) {
                    return false;
                }

                Object this$gsehType = this.getGsehType();
                Object other$gsehType = other.getGsehType();
                if (this$gsehType == null) {
                    if (other$gsehType == null) {
                        return true;
                    }
                } else if (this$gsehType.equals(other$gsehType)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ClearBoxTipOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $gschDate = this.getGschDate();
        result = result * 59 + ($gschDate == null ? 43 : $gschDate.hashCode());
        Object $gsehDate = this.getGsehDate();
        result = result * 59 + ($gsehDate == null ? 43 : $gsehDate.hashCode());
        Object $gschVoucherId = this.getGschVoucherId();
        result = result * 59 + ($gschVoucherId == null ? 43 : $gschVoucherId.hashCode());
        Object $gschExaVoucherId = this.getGschExaVoucherId();
        result = result * 59 + ($gschExaVoucherId == null ? 43 : $gschExaVoucherId.hashCode());
        Object $gsehType = this.getGsehType();
        result = result * 59 + ($gsehType == null ? 43 : $gsehType.hashCode());
        return result;
    }

    public String toString() {
        return "ClearBoxTipOutData(gschDate=" + this.getGschDate() + ", gsehDate=" + this.getGsehDate() + ", gschVoucherId=" + this.getGschVoucherId() + ", gschExaVoucherId=" + this.getGschExaVoucherId() + ", gsehType=" + this.getGsehType() + ")";
    }
}
