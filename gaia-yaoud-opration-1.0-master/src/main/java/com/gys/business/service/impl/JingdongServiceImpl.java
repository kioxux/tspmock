//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.JingdongService;
import com.gys.business.service.data.GetJingdongInData;
import com.gys.business.service.data.GetJingdongOutData;
import com.gys.business.service.data.SkuStock;
import com.gys.common.exception.BusinessException;
import com.gys.common.webSocket.WebSocket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
//import o2o.openplatform.sdk.dto.WebRequestDTO;
//import o2o.openplatform.sdk.util.HttpUtil;
//import o2o.openplatform.sdk.util.SignUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JingdongServiceImpl implements JingdongService {
    @Resource
    private WebSocket webSocket;
    String appKey = "7fd1c34598924181b3ba295b41c63507";
    String appSecret = "a7182e7f06274e4ebcbb0c64213fcfa7";
    String token = "2f3da4db-a0d4-40a8-bf4e-22007b5603d5";
    String format = "json";
    String v = "1.0";

    public JingdongServiceImpl() {
    }

    @Transactional
    public void orderPayedPush(String params) {
        JSONObject object = JSONObject.parseObject(params);
        String appId = object.getString("app_key");
        GetJingdongOutData outData = new GetJingdongOutData();
        outData.setParams(params);
        outData.setSource("3");
        outData.setType("orderPayedPush");
        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
    }

    @Transactional
    public void orderReminderPush(String params) {
        JSONObject object = JSONObject.parseObject(params);
        String appId = object.getString("app_key");
        GetJingdongOutData outData = new GetJingdongOutData();
        outData.setParams(params);
        outData.setSource("3");
        outData.setType("orderReminderPush");
        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
    }

    @Transactional
    public void orderCancelPush(String params) {
        JSONObject object = JSONObject.parseObject(params);
        String appId = object.getString("app_key");
        GetJingdongOutData outData = new GetJingdongOutData();
        outData.setParams(params);
        outData.setSource("3");
        outData.setType("orderCancelPush");
        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
    }

    @Transactional
    public void orderRefundAllPush(String params) {
        JSONObject object = JSONObject.parseObject(params);
        String appId = object.getString("app_key");
        GetJingdongOutData outData = new GetJingdongOutData();
        outData.setParams(params);
        outData.setSource("3");
        outData.setType("orderRefundAllPush");
        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
    }

    @Transactional
    public void orderRefundPush(String params) {
        JSONObject object = JSONObject.parseObject(params);
        String appId = object.getString("app_key");
        GetJingdongOutData outData = new GetJingdongOutData();
        outData.setParams(params);
        outData.setSource("3");
        outData.setType("orderRefundPush");
        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
    }

    @Transactional
    public void deliveryStatusPush(String params) {
        JSONObject object = JSONObject.parseObject(params);
        String appId = object.getString("app_key");
        GetJingdongOutData outData = new GetJingdongOutData();
        outData.setParams(params);
        outData.setSource("3");
        outData.setType("deliveryStatusPush");
        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
    }

    @Transactional
    public void orderStatusPush(String params) {
        JSONObject object = JSONObject.parseObject(params);
        String appId = object.getString("app_key");
        GetJingdongOutData outData = new GetJingdongOutData();
        outData.setParams(params);
        outData.setSource("3");
        outData.setType("orderStatusPush");
        this.webSocket.AppointSending(appId, JSONObject.toJSONString(outData));
    }

    @Transactional
    public void orderGetOrderDetail(GetJingdongInData inData) {
        String timestamp = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        Map<String, Object> params = new HashMap();
        params.put("orderId", inData.getOrderId());
        String paramsString = JSONObject.toJSONString(params);
        String url = "https://openo2o.jd.com/djapi/order/es/query";

        try {
            String sign = this.getSign(paramsString, timestamp);
            String result = this.request(sign, url, paramsString, timestamp);
            System.out.println("result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (!"0".equals(object.getString("code"))) {
                throw new BusinessException(object.getString("msg"));
            }
        } catch (Exception var9) {
            throw new BusinessException(var9.getMessage());
        }
    }

    @Transactional
    public void orderConfirm(GetJingdongInData inData) {
        String timestamp = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        Map<String, Object> params = new HashMap();
        params.put("orderId", inData.getOrderId());
        params.put("isAgreed", true);
        params.put("operator", inData.getAccount());
        String paramsString = JSONObject.toJSONString(params);
        String url = "https://openapi.jddj.com/djapi/ocs/orderAcceptOperate";

        try {
            String sign = this.getSign(paramsString, timestamp);
            String result = this.request(sign, url, paramsString, timestamp);
            System.out.println("result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (!"0".equals(object.getString("code"))) {
                throw new BusinessException(object.getString("msg"));
            }
        } catch (Exception var9) {
            throw new BusinessException(var9.getMessage());
        }
    }

    @Transactional
    public void orderCancel(GetJingdongInData inData) {
        String timestamp = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        Map<String, Object> params = new HashMap();
        params.put("orderId", inData.getOrderId());
        params.put("isAgreed", false);
        params.put("operator", inData.getAccount());
        String paramsString = JSONObject.toJSONString(params);
        String url = "https://openapi.jddj.com/djapi/ocs/orderAcceptOperate";

        try {
            String sign = this.getSign(paramsString, timestamp);
            String result = this.request(sign, url, paramsString, timestamp);
            System.out.println("result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (!"0".equals(object.getString("code"))) {
                throw new BusinessException(object.getString("msg"));
            }
        } catch (Exception var9) {
            throw new BusinessException(var9.getMessage());
        }
    }

    @Transactional
    public void orderPreparationMealComplete(GetJingdongInData inData) {
        String timestamp = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        Map<String, Object> params = new HashMap();
        params.put("orderId", inData.getOrderId());
        params.put("operator", inData.getAccount());
        String paramsString = JSONObject.toJSONString(params);
        String url = "https://openapi.jddj.com/djapi/bm/open/api/order/OrderJDZBDelivery";

        try {
            String sign = this.getSign(paramsString, timestamp);
            String result = this.request(sign, url, paramsString, timestamp);
            System.out.println("result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (!"0".equals(object.getString("code"))) {
                throw new BusinessException(object.getString("msg"));
            }
        } catch (Exception var9) {
            throw new BusinessException(var9.getMessage());
        }
    }

    @Transactional
    public void orderRefundAgree(GetJingdongInData inData) {
        String timestamp = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        Map<String, Object> params = new HashMap();
        params.put("serviceOrder", inData.getServiceOrder());
        params.put("approveType", "2");
        params.put("optPin", inData.getAccount());
        String paramsString = JSONObject.toJSONString(params);
        String url = "https://openapi.jddj.com/djapi/afs/afsOpenApprove";

        try {
            String sign = this.getSign(paramsString, timestamp);
            String result = this.request(sign, url, paramsString, timestamp);
            System.out.println("result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (!"0".equals(object.getString("code"))) {
                throw new BusinessException(object.getString("msg"));
            }
        } catch (Exception var9) {
            throw new BusinessException(var9.getMessage());
        }
    }

    @Transactional
    public void orderRefundReject(GetJingdongInData inData) {
        String timestamp = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        Map<String, Object> params = new HashMap();
        params.put("serviceOrder", inData.getServiceOrder());
        params.put("approveType", "3");
        params.put("rejectReason", inData.getReason());
        params.put("optPin", inData.getAccount());
        String paramsString = JSONObject.toJSONString(params);
        String url = "https://openapi.jddj.com/djapi/afs/afsOpenApprove";

        try {
            String sign = this.getSign(paramsString, timestamp);
            String result = this.request(sign, url, paramsString, timestamp);
            System.out.println("result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (!"0".equals(object.getString("code"))) {
                throw new BusinessException(object.getString("msg"));
            }
        } catch (Exception var9) {
            throw new BusinessException(var9.getMessage());
        }
    }

    @Transactional
    public void medicineStock(GetJingdongInData inData) {
        List<SkuStock> list = new ArrayList();
        String timestamp = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        Map<String, Object> params = new HashMap();
        params.put("outStationNo", inData.getStoreCode());
        params.put("userPin", inData.getAccount());
        params.put("skuStockList", list);
        String paramsString = JSONObject.toJSONString(params);
        String url = "https://openapi.jddj.com/djapi/stock/batchUpdateCurrentQtys";

        try {
            String sign = this.getSign(paramsString, timestamp);
            String result = this.request(sign, url, paramsString, timestamp);
            System.out.println("result:" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (!"0".equals(object.getString("code"))) {
                throw new BusinessException(object.getString("msg"));
            }
        } catch (Exception var10) {
            throw new BusinessException(var10.getMessage());
        }
    }

    private String getSign(String params, String timestamp) {
        //chenhao TODO
//        WebRequestDTO webReqeustDTO = new WebRequestDTO();
//        webReqeustDTO.setApp_key(this.appKey);
//        webReqeustDTO.setFormat(this.format);
//        webReqeustDTO.setJd_param_json(params);
//        webReqeustDTO.setTimestamp(timestamp);
//        webReqeustDTO.setToken(this.token);
//        webReqeustDTO.setV(this.v);
//        String sign = null;
//
//        try {
//            sign = SignUtils.getSignByMD5(webReqeustDTO, this.appSecret);
//        } catch (Exception var6) {
//            var6.printStackTrace();
//        }
//
//        System.out.println("md5 sign:" + sign);
//        return sign;
        return null;
    }

    private String request(String sign, String url, String paramsString, String timestamp) {
        //chenhao TODO
//        Map<String, Object> param = new HashMap();
//        param.put("token", this.token);
//        param.put("app_key", this.appKey);
//        param.put("timestamp", timestamp);
//        param.put("sign", sign);
//        param.put("format", this.format);
//        param.put("v", this.v);
//        param.put("jd_param_json", paramsString);
//        String result = null;
//
//        try {
//            result = HttpUtil.sendSimplePostRequest(url, param);
//        } catch (Exception var8) {
//            var8.printStackTrace();
//        }
//
//        return result;
        return null;
    }
}
