package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.business.service.data.Unqualified.UnqualifiedDto;
import com.gys.business.service.data.Unqualified.UnqualifiedInDataParam;
import com.gys.common.data.GetLoginOutData;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface UnqualifiedService {
    List<UnqualifiedOutData> unqualifiedList(UnqualifiedInData inData);

    List<GaiaSdDefecProDInData> checkSubstandard(@RequestBody GaiaSdDefecProDInData vo);

    String addSubstandard(GaiaSdDefecProHInData gaiaSdDefecProHInData);


    void approve(GaiaSdDefecProHInData inData);

    void removeDefecProD(GaiaSdDefecProDInData inData);

    UnqualifiedOutData listUnqualified(GetLoginOutData userInfo, UnqualifiedInDataParam inData);

    List<UnqualifiedDto> listUnqualifiedHead(GetLoginOutData userInfo, UnqualifiedInDataParam inData);

    List<UnqualifiedDto> listUnqualifiedDetailByBillNo(GetLoginOutData userInfo, UnqualifiedInDataParam inData);

    Boolean commitUnqualifiedBill(GetLoginOutData userInfo, UnqualifiedInDataParam inData);

    Boolean checkTodoList(UnqualifiedInDataParam inData);

    GaiaSdDefecProHInData selectDefecProDetail(GaiaSdDefecProDInData inData);

    void sendLossDefecpro(GetWfApproveInData inData);

}
