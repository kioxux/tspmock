package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaDcDataMapper;
import com.gys.business.mapper.entity.GaiaDcData;
import com.gys.business.service.DcOutStockHService;
import com.gys.business.service.DcOutStockJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/23 00:18
 */
@Slf4j
@Service("dcOutStockJobService")
public class DcOutStockJobServiceImpl implements DcOutStockJobService {
    @Resource
    private DcOutStockHService dcOutStockHService;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;

    @Override
    public void execJob() {
        //加盟商列表
        List<GaiaDcData> dcList = gaiaDcDataMapper.getDcList();
        //统计缺断货情况
        if (dcList == null || dcList.size() == 0) {
            return;
        }
        for (GaiaDcData gaiaDcData : dcList) {
            try {
                dcOutStockHService.generateOutStock(gaiaDcData);
            } catch (Exception e) {
                log.error("<定时任务><公司级缺断货><任务执行异常：{}>", e.getMessage(), e);
            }
        }
        log.info("<定时任务><公司级缺断货><任务执行结束>");
    }
}
