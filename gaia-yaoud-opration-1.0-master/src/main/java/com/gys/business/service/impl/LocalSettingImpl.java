//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdStoreDataMapper;
import com.gys.business.mapper.entity.GaiaSdStoreData;
import com.gys.business.service.LocalSettingService;
import com.gys.business.service.data.LocalSettingInData;
import com.gys.business.service.data.LocalSettingOutData;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class LocalSettingImpl implements LocalSettingService {
    @Autowired
    private GaiaSdStoreDataMapper gaiaSdStoreDataMapper;

    public LocalSettingImpl() {
    }

    public LocalSettingOutData selectOne(LocalSettingInData inData) {
        LocalSettingOutData outData = new LocalSettingOutData();
        GaiaSdStoreData sdStoreData = this.gaiaSdStoreDataMapper.selectByPrimaryKey(inData);
        if (ObjectUtil.isNotEmpty(sdStoreData)) {
            BeanUtils.copyProperties(sdStoreData, outData);
        }

        return outData;
    }

    public void saveOrUpDate(LocalSettingInData inData) {
        GaiaSdStoreData sdStoreData = (GaiaSdStoreData)this.gaiaSdStoreDataMapper.selectByPrimaryKey(inData);
        GaiaSdStoreData gaiaSdStoreData = new GaiaSdStoreData();
        BeanUtils.copyProperties(inData, gaiaSdStoreData);
        if (ObjectUtils.isEmpty(sdStoreData)) {
            this.gaiaSdStoreDataMapper.insert(gaiaSdStoreData);
        } else {
            this.gaiaSdStoreDataMapper.updateByPrimaryKey(gaiaSdStoreData);
        }

    }
}
