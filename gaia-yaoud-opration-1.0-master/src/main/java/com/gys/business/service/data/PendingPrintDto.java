package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PendingPrintDto implements Serializable {
    private static final long serialVersionUID = -6202387550469671365L;

    /**
     * 会员卡号
     */
    private String memberId;

    /**
     * 会员姓名
     */
    private String memberName;

    /**
     * 小票抬头
     */
    private String module;

    /**
     * 销售时间
     */
    private String saleDate;

    /**
     * H表 emp
     */
    private String hEmp;

    /**
     * D表 emp
     */
    private String dEmp;

    /**
     * 销售实收
     */
    private String saleYsAmt;

    /**
     * 销售折扣
     */
    private String saleZkAmt;

    /**
     * 销售原价
     */
    private String saleYjAmt;

    /**
     * 销售明细集合
     */
    private List<RestOrderDetailOutData> detail;
}
