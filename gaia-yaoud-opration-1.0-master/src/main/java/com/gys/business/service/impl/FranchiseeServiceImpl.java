package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaFranchiseeMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaDataSynchronized;
import com.gys.business.mapper.entity.GaiaFranchisee;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.CacheService;
import com.gys.business.service.FranchiseeService;
import com.gys.business.service.data.FranchiseeInData;
import com.gys.business.service.data.FranchiseeOutData;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.MQReceiveData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
public class FranchiseeServiceImpl implements FranchiseeService {
    @Autowired
    private GaiaFranchiseeMapper franchiseeMapper;

    @Autowired
    private GaiaUserDataMapper userDataMapper;

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;

    @Autowired
    private RabbitTemplateHelper rabbitTemplate;

    @Autowired
    private CacheService synchronizedService;

    @Override
    public FranchiseeOutData getFranchiseeById(FranchiseeInData inData) {
        GaiaFranchisee franchisee = (GaiaFranchisee) this.franchiseeMapper.selectByPrimaryKey(inData.getClient());
        FranchiseeOutData outData = new FranchiseeOutData();
        if (ObjectUtil.isNotEmpty(franchisee)) {
            outData.setClient(franchisee.getClient());
            outData.setFrancAddr(franchisee.getFrancAddr());
            outData.setFrancLegalPersonName(franchisee.getFrancLegalPerson());
            outData.setFrancLogo(franchisee.getFrancLogo());
            outData.setFrancName(franchisee.getFrancName());
            outData.setFrancNo(franchisee.getFrancNo());
            outData.setFrancQua(franchisee.getFrancQua());
            outData.setFrancAssName(franchisee.getFrancAss());
            outData.setFrancType1(franchisee.getFrancType1());
            outData.setFrancType2(franchisee.getFrancType2());
            outData.setFrancType3(franchisee.getFrancType3());
        }

        return outData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertFranchisee(GetLoginOutData userInfo, FranchiseeInData inData) {
        Example example = new Example(GaiaFranchisee.class);
        example.createCriteria().andEqualTo("francName", inData.getFrancName());
        List<GaiaFranchisee> list = this.franchiseeMapper.selectByExample(example);
        if (ObjectUtil.isNotEmpty(list)) {
            throw new BusinessException("加盟商名称已存在！");
        } else {
            int max = this.franchiseeMapper.selectMaxId();
            GaiaFranchisee franchisee = new GaiaFranchisee();
            franchisee.setClient(String.valueOf(max + 1));
            franchisee.setFrancAddr(inData.getFrancAddr());
            franchisee.setFrancLogo(inData.getFrancLogo());
            franchisee.setFrancName(inData.getFrancName());
            franchisee.setFrancNo(inData.getFrancNo());
            franchisee.setFrancQua(inData.getFrancQua());
            franchisee.setFrancAss(inData.getFrancAss());
            franchisee.setFrancType1(inData.getFrancType1());
            franchisee.setFrancType2(inData.getFrancType2());
            franchisee.setFrancType3(inData.getFrancType3());
            franchisee.setFrancCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
            franchisee.setFrancCreTime(DateUtil.format(new Date(), "HHmmss"));
            franchisee.setFrancModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
            franchisee.setFrancModiTime(DateUtil.format(new Date(), "HHmmss"));
            String password = RandomUtil.randomNumbers(6);
//            inData.setUserPassword(CodecUtil.encryptMD5(password));
            GaiaUserData userData = this.saveAuthConfi(franchisee.getClient(), inData, userInfo);
            franchisee.setFrancLegalPerson(userData.getUserId());
            this.franchiseeMapper.insert(franchisee);
            List<String> groupList = new ArrayList();
            groupList.add("GAIA_AL_ADMIN");
            groupList.add("GAIA_AL_MANAGER");
            groupList.add("GAIA_WM_ZG");
            groupList.add("GAIA_MM_ZLZG");
            groupList.add("GAIA_MM_SCZG");
            groupList.add("SD_01");
            groupList.add("GAIA_FI_ZG");
        }
    }

    @Override
    public PageInfo<FranchiseeOutData> getFranchisee(FranchiseeInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        List<FranchiseeOutData> franchisee = this.franchiseeMapper.getFranchisee();
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(franchisee)) {
            pageInfo = new PageInfo(franchisee);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public List<FranchiseeOutData> getFranchiseeByName(FranchiseeInData inData) {
        return franchiseeMapper.getFranchiseeByName(inData.getFrancName());
    }

    @Override
    public String selectStoPriceComparison(String clientId, String stoCode, String value) {
        return storeDataMapper.selectStoPriceComparison(clientId, stoCode,value);
    }

    @Transactional(rollbackFor = Exception.class)
    public GaiaUserData saveAuthConfi(String client, FranchiseeInData inData, GetLoginOutData userInfo) {
        Example example = new Example(GaiaUserData.class);
        example.createCriteria().andEqualTo("client", client).andEqualTo("userTel", inData.getFrancPhone());
        List<GaiaUserData> list = this.userDataMapper.selectByExample(example);
        if (ObjectUtil.isNotEmpty(list)) {
            throw new BusinessException("该手机号已存在，不要重复创建！");
        } else {
            int userMax = this.userDataMapper.selectMaxId();
            GaiaUserData userData = new GaiaUserData();
            userData.setUserId(String.valueOf(userMax + 1));
            userData.setClient(client);
            userData.setUserNam(inData.getFrancLegalPersonName());
            userData.setUserTel(inData.getFrancPhone());
            userData.setUserSta("0");
            userData.setUserLoginSta("0");
            userData.setUserCreDate(DateUtil.format(new Date(), "yyyyMMdd"));
            userData.setUserCreTime(DateUtil.format(new Date(), "HHmmss"));
            userData.setUserModiDate(DateUtil.format(new Date(), "yyyyMMdd"));
            userData.setUserModiTime(DateUtil.format(new Date(), "HHmmss"));
            this.userDataMapper.insert(userData);
            Map<String, Object> map = new HashMap<>(16);
            map.put("USER_ID", String.valueOf(userMax + 1));
            map.put("CLIENT", client);
            GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
            aSynchronized.setClient(client);
            aSynchronized.setSite(userInfo.getDepId());
            aSynchronized.setVersion(IdUtil.randomUUID());
            aSynchronized.setTableName("GAIA_USER_DATA");
            aSynchronized.setCommand("insert");
            aSynchronized.setArg(JSON.toJSONString(map));
            this.synchronizedService.addOne(aSynchronized);
            MQReceiveData mqReceiveData = new MQReceiveData();
            mqReceiveData.setType("DataSync");
            mqReceiveData.setCmd("saveAuthConfi");
            mqReceiveData.setData(inData.getFrancLegalPersonName());
            this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, client + "." + userInfo.getDepId(), JSON.toJSON(mqReceiveData));
            return userData;
        }
    }
}
