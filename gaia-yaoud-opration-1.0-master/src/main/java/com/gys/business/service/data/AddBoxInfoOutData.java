//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class AddBoxInfoOutData {
    private String gsihDate;
    private String gsihStatus;
    private String gsihVoucherId;
    private String index;
    private String gsihEmp;
    private String gsihRemaks;
    private String clientId;
    private String brId;
    private String proId;
    private String gscdBatchNo;

    public AddBoxInfoOutData() {
    }

    public String getGsihDate() {
        return this.gsihDate;
    }

    public String getGsihStatus() {
        return this.gsihStatus;
    }

    public String getGsihVoucherId() {
        return this.gsihVoucherId;
    }

    public String getIndex() {
        return this.index;
    }

    public String getGsihEmp() {
        return this.gsihEmp;
    }

    public String getGsihRemaks() {
        return this.gsihRemaks;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getBrId() {
        return this.brId;
    }

    public String getProId() {
        return this.proId;
    }

    public String getGscdBatchNo() {
        return this.gscdBatchNo;
    }

    public void setGsihDate(final String gsihDate) {
        this.gsihDate = gsihDate;
    }

    public void setGsihStatus(final String gsihStatus) {
        this.gsihStatus = gsihStatus;
    }

    public void setGsihVoucherId(final String gsihVoucherId) {
        this.gsihVoucherId = gsihVoucherId;
    }

    public void setIndex(final String index) {
        this.index = index;
    }

    public void setGsihEmp(final String gsihEmp) {
        this.gsihEmp = gsihEmp;
    }

    public void setGsihRemaks(final String gsihRemaks) {
        this.gsihRemaks = gsihRemaks;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setBrId(final String brId) {
        this.brId = brId;
    }

    public void setProId(final String proId) {
        this.proId = proId;
    }

    public void setGscdBatchNo(final String gscdBatchNo) {
        this.gscdBatchNo = gscdBatchNo;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof AddBoxInfoOutData)) {
            return false;
        } else {
            AddBoxInfoOutData other = (AddBoxInfoOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$gsihDate = this.getGsihDate();
                Object other$gsihDate = other.getGsihDate();
                if (this$gsihDate == null) {
                    if (other$gsihDate != null) {
                        return false;
                    }
                } else if (!this$gsihDate.equals(other$gsihDate)) {
                    return false;
                }

                Object this$gsihStatus = this.getGsihStatus();
                Object other$gsihStatus = other.getGsihStatus();
                if (this$gsihStatus == null) {
                    if (other$gsihStatus != null) {
                        return false;
                    }
                } else if (!this$gsihStatus.equals(other$gsihStatus)) {
                    return false;
                }

                Object this$gsihVoucherId = this.getGsihVoucherId();
                Object other$gsihVoucherId = other.getGsihVoucherId();
                if (this$gsihVoucherId == null) {
                    if (other$gsihVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsihVoucherId.equals(other$gsihVoucherId)) {
                    return false;
                }

                label110: {
                    Object this$index = this.getIndex();
                    Object other$index = other.getIndex();
                    if (this$index == null) {
                        if (other$index == null) {
                            break label110;
                        }
                    } else if (this$index.equals(other$index)) {
                        break label110;
                    }

                    return false;
                }

                label103: {
                    Object this$gsihEmp = this.getGsihEmp();
                    Object other$gsihEmp = other.getGsihEmp();
                    if (this$gsihEmp == null) {
                        if (other$gsihEmp == null) {
                            break label103;
                        }
                    } else if (this$gsihEmp.equals(other$gsihEmp)) {
                        break label103;
                    }

                    return false;
                }

                Object this$gsihRemaks = this.getGsihRemaks();
                Object other$gsihRemaks = other.getGsihRemaks();
                if (this$gsihRemaks == null) {
                    if (other$gsihRemaks != null) {
                        return false;
                    }
                } else if (!this$gsihRemaks.equals(other$gsihRemaks)) {
                    return false;
                }

                label89: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label89;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label89;
                    }

                    return false;
                }

                label82: {
                    Object this$brId = this.getBrId();
                    Object other$brId = other.getBrId();
                    if (this$brId == null) {
                        if (other$brId == null) {
                            break label82;
                        }
                    } else if (this$brId.equals(other$brId)) {
                        break label82;
                    }

                    return false;
                }

                Object this$proId = this.getProId();
                Object other$proId = other.getProId();
                if (this$proId == null) {
                    if (other$proId != null) {
                        return false;
                    }
                } else if (!this$proId.equals(other$proId)) {
                    return false;
                }

                Object this$gscdBatchNo = this.getGscdBatchNo();
                Object other$gscdBatchNo = other.getGscdBatchNo();
                if (this$gscdBatchNo == null) {
                    if (other$gscdBatchNo != null) {
                        return false;
                    }
                } else if (!this$gscdBatchNo.equals(other$gscdBatchNo)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof AddBoxInfoOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gsihDate = this.getGsihDate();
        result = result * 59 + ($gsihDate == null ? 43 : $gsihDate.hashCode());
        Object $gsihStatus = this.getGsihStatus();
        result = result * 59 + ($gsihStatus == null ? 43 : $gsihStatus.hashCode());
        Object $gsihVoucherId = this.getGsihVoucherId();
        result = result * 59 + ($gsihVoucherId == null ? 43 : $gsihVoucherId.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        Object $gsihEmp = this.getGsihEmp();
        result = result * 59 + ($gsihEmp == null ? 43 : $gsihEmp.hashCode());
        Object $gsihRemaks = this.getGsihRemaks();
        result = result * 59 + ($gsihRemaks == null ? 43 : $gsihRemaks.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $brId = this.getBrId();
        result = result * 59 + ($brId == null ? 43 : $brId.hashCode());
        Object $proId = this.getProId();
        result = result * 59 + ($proId == null ? 43 : $proId.hashCode());
        Object $gscdBatchNo = this.getGscdBatchNo();
        result = result * 59 + ($gscdBatchNo == null ? 43 : $gscdBatchNo.hashCode());
        return result;
    }

    public String toString() {
        return "AddBoxInfoOutData(gsihDate=" + this.getGsihDate() + ", gsihStatus=" + this.getGsihStatus() + ", gsihVoucherId=" + this.getGsihVoucherId() + ", index=" + this.getIndex() + ", gsihEmp=" + this.getGsihEmp() + ", gsihRemaks=" + this.getGsihRemaks() + ", clientId=" + this.getClientId() + ", brId=" + this.getBrId() + ", proId=" + this.getProId() + ", gscdBatchNo=" + this.getGscdBatchNo() + ")";
    }
}
