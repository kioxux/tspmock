package com.gys.business.service.data.DtYb;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Rkmxlb {
//    @NotNull(message = "入库验收日期不能为空")
    private String rkysrq;
    //验收单据号
//    @NotNull(message = "验收单据号不能为空")
    private String ysdjh0;
    //供应商编码
//    @NotNull(message = "供应商编码不能为空")
    private String gysbh0;
    //供应商名称
//    @NotNull(message = "供应商名称不能为空")
    private String gysmc0;
    //药品编号
    @NotNull(message = "药品编号不能为空")
    private String ypbh00;
    //药品名称
    @NotNull(message = "药品名称不能为空")
    private String ypmc00;
    //通用名称
    @NotNull(message = "通用名称不能为空")
    private String bzmc00;
    //剂型
//    @NotNull(message = "剂型不能为空")
    private String xh0000;
    //规格
//    @NotNull(message = "规格不能为空")
    private String ypgg00;
    //生产厂家
//    @NotNull(message = "生产厂家不能为空")
    private String ypcjmc;
    //单位
//    @NotNull(message = "单位不能为空")
    private String dwmc00;
    //验收合格数量
    @NotNull(message = "验收合格数量不能为空")
    private String hgsl00;
    //药品批号
    @NotNull(message = "药品批号不能为空")
    private String ypph00;
    //生产日期
//    @NotNull(message = "生产日期不能为空")
    private String scrq00;
    //有效日期
    @NotNull(message = "有效日期不能为空")
    private String yxrq00;
    //进价
    @NotNull(message = "进价不能为空")
    private String ypjj00;
    //入库批次号
    @NotNull(message = "入库批次号不能为空")
    private String rkpch0;
    //入库成功后由服务器生
    //成，药店端必须将此字
    //段存储起来，在销售的
    //时候，出库明细等操作
    //时要与该字段对应，用
    //于区分销售哪条库存
    private String stockcode;

}
