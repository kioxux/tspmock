//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetLoginStoreInData {
    private String clientId;
    private String userId;
    private String stoCode;
    private String stoName;

    public GetLoginStoreInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getStoCode() {
        return this.stoCode;
    }

    public String getStoName() {
        return this.stoName;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public void setStoCode(final String stoCode) {
        this.stoCode = stoCode;
    }

    public void setStoName(final String stoName) {
        this.stoName = stoName;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetLoginStoreInData)) {
            return false;
        } else {
            GetLoginStoreInData other = (GetLoginStoreInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label59;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label59;
                    }

                    return false;
                }

                Object this$userId = this.getUserId();
                Object other$userId = other.getUserId();
                if (this$userId == null) {
                    if (other$userId != null) {
                        return false;
                    }
                } else if (!this$userId.equals(other$userId)) {
                    return false;
                }

                Object this$stoCode = this.getStoCode();
                Object other$stoCode = other.getStoCode();
                if (this$stoCode == null) {
                    if (other$stoCode != null) {
                        return false;
                    }
                } else if (!this$stoCode.equals(other$stoCode)) {
                    return false;
                }

                Object this$stoName = this.getStoName();
                Object other$stoName = other.getStoName();
                if (this$stoName == null) {
                    if (other$stoName != null) {
                        return false;
                    }
                } else if (!this$stoName.equals(other$stoName)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetLoginStoreInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $userId = this.getUserId();
        result = result * 59 + ($userId == null ? 43 : $userId.hashCode());
        Object $stoCode = this.getStoCode();
        result = result * 59 + ($stoCode == null ? 43 : $stoCode.hashCode());
        Object $stoName = this.getStoName();
        result = result * 59 + ($stoName == null ? 43 : $stoName.hashCode());
        return result;
    }

    public String toString() {
        return "GetLoginStoreInData(clientId=" + this.getClientId() + ", userId=" + this.getUserId() + ", stoCode=" + this.getStoCode() + ", stoName=" + this.getStoName() + ")";
    }
}
