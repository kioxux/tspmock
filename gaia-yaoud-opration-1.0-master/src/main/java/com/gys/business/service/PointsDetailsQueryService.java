package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.data.PointsDetailsQueryInData;
import com.gys.business.service.data.PointsDetailsQueryOutData;
import com.gys.common.response.Result;

import java.util.List;


/**
 * @author xiaoyuan
 */
public interface PointsDetailsQueryService {

    /**
     * 根据会员id 会员名称 时间查询
     * @param queryInData
     * @return
     */
    PageInfo<PointsDetailsQueryOutData> selectPointsDetailsQueryByNameAndId(PointsDetailsQueryInData queryInData);

    Result selectPointsDetailsQueryExport(PointsDetailsQueryInData queryInData);

    List<GaiaStoreData> getSaleStore(String client);
}
