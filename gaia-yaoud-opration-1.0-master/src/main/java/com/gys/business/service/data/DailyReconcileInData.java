package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DailyReconcileInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "店号")
    private String gpdhBrId;
    @ApiModelProperty(value = "单号")
    private String gpdhVoucherId;
    @ApiModelProperty(value = "销售日期")
    private String gpdhSaleDate;
    @ApiModelProperty(value = "销售人员")
    private String gpdhEmp;
}
