package com.gys.business.service.data;

import lombok.Data;

@Data
public class QueryParam {
    private String startDate;
    private String endDate;
    private String orderNO;
    private String clientId;
    private String brId;
    private String status;
    private String uniqueId;
}
