package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GetProductInData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    private String gspgName;
    @ApiModelProperty(value = "商品编码 通用名称 国际条形码1 国际条形码2 助记码 商品名（模糊匹配）")
    private String gspgProId;
    @ApiModelProperty(value = "商品名")
    private String gspgProName;
    private String gspgId;
    private String gspgUpdateEmp;
    @ApiModelProperty(value = "规格")
    private String gspgSpecs;
    @ApiModelProperty(value = "地点")
    private String storeCode;
    @ApiModelProperty(value = "批次")
    private String batch;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    private List<AddProTableOutData> proTableOutDataList;
    public int pageSize;
    private int pageNum;

}
