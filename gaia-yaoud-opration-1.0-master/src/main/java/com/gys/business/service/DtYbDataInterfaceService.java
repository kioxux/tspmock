package com.gys.business.service;

import com.gys.business.service.data.DtYb.Inventory.InventoryVO;
import com.gys.business.service.data.DtYb.Physical.PhysicalVO;
import com.gys.business.service.data.DtYb.ProfitLoss.ProfitlossVO;
import com.gys.business.service.data.DtYb.ReturnExamime.ReturnExamimeVO;
import com.gys.business.service.data.DtYb.RkmxlbEntity;
import com.gys.business.service.data.DtYb.RkmxlbParam;
import com.gys.business.service.data.YbInterfaceQueryData;
import com.gys.common.YiLianDaModel.GYSXXGL.Gysxxlb;
import com.gys.common.YiLianDaModel.YPXXGL.Ypxxlb;

import java.util.List;

public interface DtYbDataInterfaceService {
    List<RkmxlbParam> queryExamime(YbInterfaceQueryData inData);
    int insertExamime(String client, String brId, RkmxlbEntity inData);
    List<Ypxxlb> selectPushProduct(String client, String brId,String startDate,String endDate);
    int insertPushProduct(String client, String brId,List<Ypxxlb> inData);
    List<Gysxxlb> selectPushSupplier(String client, String brId,String startDate,String endDate);
    int insertPushSupplier(String client, String brId,List<Gysxxlb> inData);

    List<ReturnExamimeVO> queryReturnExamime(YbInterfaceQueryData inData);

    List<PhysicalVO> queryPhysical(YbInterfaceQueryData inData);

    List<ProfitlossVO> queryProfitloss(YbInterfaceQueryData inData);

    List<InventoryVO> queryInventory(YbInterfaceQueryData inData);
}
