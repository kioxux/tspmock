package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Maps;
import com.gys.business.mapper.GaiaInvoicingMapper;
import com.gys.business.mapper.GaiaSdBatchChangeMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.service.InvoicingOutDataObj;
import com.gys.business.service.InvoicingService;
import com.gys.business.service.data.InvoicingInData;
import com.gys.business.service.data.InvoicingOutData;
import com.gys.common.constant.CommonConstant;
import com.gys.common.enums.OrderRuleEnum;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.gys.util.CosUtils;
import com.gys.util.ExportStatusUtil;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class InvoicingServiceImpl implements InvoicingService {
    @Resource
    private GaiaInvoicingMapper invoicingMapper;

    @Resource
    private GaiaSdBatchChangeMapper batchChangeMapper;

    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Autowired
    private CosUtils cosUtils;

    @Override
    public List<InvoicingOutData> getIncomeStatementList(InvoicingInData inData) {
        List<InvoicingOutData> invoicingOutDataList = invoicingMapper.getIncomeStatementList(inData);
        return invoicingOutDataList;
    }

    @Override
    public void invoicingExport(InvoicingInData inData, HttpServletResponse response) {
        ExportStatusUtil.checkExportAuthority(inData.getClientId(),inData.getGssbBrId());
        InvoicingOutDataObj dataObj = this.getInvoicingList(inData);
        List<InvoicingOutData> invoicingOutDataList = dataObj.getResultList();
        if (ObjectUtil.isEmpty(invoicingOutDataList)){
            throw new BusinessException("导出数据为空！");
        }
        String totalDec = dataObj.getTotalDec();
        for (int i = 0; i < invoicingOutDataList.size(); i++) {
            invoicingOutDataList.get(i).setIndex(i+1);
        }
        CsvFileInfo csvInfo = CsvClient.getCsvByte(invoicingOutDataList, "进销存明细", Collections.singletonList((short) 1));
        if(ObjectUtil.isEmpty(csvInfo)){
            throw new BusinessException("导出数据为空！（或超过50万条）");
        }
        byte[] bytes = ("\r\n备注," + totalDec).getBytes(StandardCharsets.UTF_8);
        byte[] all = ArrayUtils.addAll(csvInfo.getFileContent(), bytes);
        CsvClient.writeCSV(response,"进销存明细",all);
    }

    @Override
    public InvoicingOutDataObj getInvoicingList(InvoicingInData inData) {

        String startStr = inData.getGssbStartDate();
        String endStr = inData.getGssbFinishDate();
        //先查询权限   flag  0：开启  1：不开启
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        if (ObjectUtil.isEmpty(flag)){
            inData.setFlag("0");
        }else {
            inData.setFlag(flag);
        }
        if(StrUtil.isBlank(startStr) || StrUtil.isBlank(endStr)){
            throw new BusinessException("日期不可为空");
        }else {
            inData.setGssbStartDate(startStr);
            inData.setGssbFinishDate(endStr);
        }
        if (StringUtils.isNotEmpty(inData.getGssbProId())) {
            inData.setProArr(inData.getGssbProId().split("\\s+ |\\s+|,"));

//            System.out.println(inData.getBillNoArr());
        }
        InvoicingOutDataObj invoicingOutDataObj = new InvoicingOutDataObj();
        Map<String, BigDecimal> countMap = Maps.newLinkedHashMap();
        List<InvoicingOutData> resultList = batchChangeMapper.getInventoryList(inData);
        //查询门店手工流数据表
        List<InvoicingOutData> resultBrList = batchChangeMapper.getBrIdInventoryList(inData);
        resultList.addAll(resultBrList);
        Map<String, InvoicingOutData> sumMap = Maps.newLinkedHashMap();
        BigDecimal sum = BigDecimal.ZERO;
        List<InvoicingOutData> realResultList = Lists.newArrayList();
        for (InvoicingOutData outData : resultList) {
            outData.setGssbValidUntil(CommonUtil.parseyyyyMMdd(outData.getGssbValidUntil()));
            outData.setGssbDate(CommonUtil.parseyyyyMMdd(outData.getGssbDate()));
            outData.setGssbType(OrderRuleEnum.getOrderRule(outData.getGssbVoucherId()));
            outData.setGssbQty(CommonUtil.stripTrailingZeros(outData.getGssbQty()));
            outData.setPickingDate(CommonUtil.parseyyyyMMdd(outData.getPickingDate()));
            if(StringUtils.isNotEmpty(outData.getBillReturnNo())){
                outData.setGssbQty(new BigDecimal(outData.getGssbQty()).negate().toString());
            }

            StringBuffer sameProStr = new StringBuffer();
            sameProStr.append(outData.getGssbVoucherId())
                    .append("#")
                    .append(outData.getGssbProId())
                    .append("#")
                    .append(outData.getGssbBatchNo());

            String type = outData.getGssbType();
            BigDecimal qty = new BigDecimal(outData.getGssbQty());
            if(sumMap.get(sameProStr.toString()) != null){
                sum = qty.add(new BigDecimal(sumMap.get(sameProStr.toString()).getGssbQty()));
                outData.setGssbQty(sum.toString());
                sumMap.put(sameProStr.toString(), outData);
            }else{
                sumMap.put(sameProStr.toString(), outData);
            }

            if(countMap.get(type) != null){
                qty = qty.add(countMap.get(type));
            }
            countMap.put(type, qty);
        }

        StringBuffer totalDec = new StringBuffer();
        for(String key : countMap.keySet()){
            totalDec.append(key).append("数量:").append(countMap.get(key)).append("个 ");
        }

        for(String key : sumMap.keySet()){
            realResultList.add(sumMap.get(key));
        }

        invoicingOutDataObj.setResultList(realResultList);
        invoicingOutDataObj.setTotalDec(totalDec.toString());
        return invoicingOutDataObj;
    }

    private static Date StrToDate(String str) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date date = null;

        try {
            date = format.parse(str);
        } catch (ParseException var4) {
            var4.printStackTrace();
        }

        return date;
    }

    private static String getBeforeDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(2, -1);
        return sdf.format(cal.getTime());
    }

    private static String getFirstDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(5, 1);
        return sdf.format(cal.getTime());
    }
}
