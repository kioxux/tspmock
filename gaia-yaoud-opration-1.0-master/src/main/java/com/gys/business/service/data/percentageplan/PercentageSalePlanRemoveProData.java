package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PercentageSalePlanRemoveProData {
    @ApiModelProperty(value = "主键ID")
    private Long id;
//    @ApiModelProperty(value = "加盟商")
//    private String clientId;
    @ApiModelProperty(value = "提成方案主表主键ID")
    private Integer pid;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;
    @ApiModelProperty(value = "成本价")
    private BigDecimal proCostPrice;
    @ApiModelProperty(value = "零售价")
    private BigDecimal proPrice;
}
