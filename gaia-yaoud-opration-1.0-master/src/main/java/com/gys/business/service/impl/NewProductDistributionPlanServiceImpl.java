package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.NewProductDistributionPlanMapper;
import com.gys.business.service.NewProductDistributionPlanService;
import com.gys.business.service.data.NewProductDistributionPlanDto;
import com.gys.business.service.data.NewProductDistributionPlanVo;
import com.gys.business.service.data.NewProductDto;
import com.gys.common.data.*;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.JsonUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author ：liuzhiwen.guoyuxi
 * @Date ：Created in 10:28 2021/8/4
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Service
@Slf4j
public class NewProductDistributionPlanServiceImpl implements NewProductDistributionPlanService {

    private static final Logger logger = LoggerFactory.getLogger(NewProductDistributionPlanServiceImpl.class);

    @Autowired
    private CosUtils cosUtils;
    @Autowired
    private NewProductDistributionPlanMapper newProductDistributionPlanMapper;

    /**
     * 有计划-新品铺货查询参数获取
     * @return
     */
    @Override
    public Map<String, Object> getQueryCondition(NewProductDistributionPlanDto planDto) {
        Map<String, Object> resultMap = new HashMap<>();
        //查询所有省
        List<Map<String, String>> provAndCity = newProductDistributionPlanMapper.getByProvAndCity(null);
        Set<String> provs = provAndCity.stream().filter(map -> map != null && StringUtils.isNotBlank(map.get("prov"))).map(map -> map.get("prov")).collect(Collectors.toSet());
        ArrayList<Map<String, String>> provList = new ArrayList<>();
        for (String prov : provs) {
            HashMap<String, String> provMap = new HashMap<>();
            provMap.put("provCode",prov);
            provMap.put("provName","");
            provList.add(provMap);
        }
        resultMap.put("prov",provList);
        List<Map<String, String>> cityList = new ArrayList<>();
        //根据省来查询市
   /*     if (StringUtils.isNotBlank(planDto.getProv())){
            Set<String> citys = provAndCity.stream().filter(map -> map != null && StringUtils.isNotBlank(map.get("prov")) && StringUtils.isNotBlank(map.get("city"))&&planDto.getProv().equals(map.get("prov"))).map(map->map.get("city")).collect(Collectors.toSet());
            for (String city : citys) {
                HashMap<String, String> cityMap = new HashMap<>();
                cityMap.put("cityCode",city);
                cityMap.put("cityName","");
                cityList.add(cityMap);
            }*/

        if (CollectionUtil.isNotEmpty(planDto.getProv())) {
            Set<String> citys = provAndCity.stream().filter(map -> map != null && StringUtils.isNotBlank(map.get("prov")) && StringUtils.isNotBlank(map.get("city")) && planDto.getProv().contains(map.get("prov"))).map(map -> map.get("city")).collect(Collectors.toSet());
            for (String city : citys) {
                HashMap<String, String> cityMap = new HashMap<>();
                cityMap.put("cityCode", city);
                cityMap.put("cityName", "");
                cityList.add(cityMap);
            }
        }
        resultMap.put("city",cityList);
        List<Map<String, String>> clientsAndClientNames = newProductDistributionPlanMapper.getByProvAndCity(planDto);
        List<Map<String, String>> clientAndClientName = new ArrayList<>();
        for (Map<String, String> map : clientsAndClientNames) {
            HashMap<String, String> clientAndClientNameMap = new HashMap<>();
            clientAndClientNameMap.put("clientCode",map.get("client"));
            clientAndClientNameMap.put("clientName",map.get("clientName"));
            clientAndClientName.add(clientAndClientNameMap);
        }
        resultMap.put("client",clientAndClientName);
        return resultMap;
    }

    /**
     * 有计划-获取查询商品详情
     * @param dto
     * @return
     */
    @Override
    public PageInfo getProduct(NewProductDto dto) {
        long startTime = System.currentTimeMillis();
        String content = dto.getContent();
        if (ObjectUtil.isNotEmpty(dto)&&StringUtils.isNotBlank(dto.getContent())){
            dto.setProArr(content.split(","));
        }
        PageHelper.startPage(dto.getPageNum(),dto.getPageSize());
        List<Map<String,Object>> result = newProductDistributionPlanMapper.getProSelfCodeByCondition(dto);
        PageInfo pageInfo = new PageInfo();
        if (ObjectUtil.isNotEmpty(result)){
            pageInfo = new PageInfo(result);
        }
        long endTime = System.currentTimeMillis();
        logger.info("<系统级新品铺货计划><有新品铺货计划明细><查询产品详情时间：{}ms>",endTime-startTime);
        return pageInfo;
    }

    /**
     * 有计划-新品铺货计划详情获取
     * @param planDto
     * @return
     */
    @Override
    public List<NewProductDistributionPlanVo> plan(NewProductDistributionPlanDto planDto) {
        long startTime = System.currentTimeMillis();
        logger.info("<系统级新品铺货计划><有新品铺货计划明细><查询参数：{}>", JsonUtils.beanToJson(planDto));
        //查询新品铺货计划详情（除商品主数据业务信息表数据）
        List<NewProductDistributionPlanVo> plans = newProductDistributionPlanMapper.plan(planDto);

        //补充商品主数据业务信息表数据
        if (ObjectUtil.isNotEmpty(plans)){
            Set<String> clients = plans.stream().map(plan -> plan.getClient()).collect(Collectors.toSet());
            Set<String> porSelfCodes = plans.stream().map(plan -> plan.getProSelfCode()).collect(Collectors.toSet());
            List<NewProductDistributionPlanVo> productDetails = newProductDistributionPlanMapper.getProductDetailByClientAndProSelfCode(clients,porSelfCodes);
            for (NewProductDistributionPlanVo plan : plans) {
                NewProductDistributionPlanVo productDetail= productDetails.stream().filter(product -> plan.getClient().equals(product.getClient()) && product.getProSelfCode().equals(plan.getProSelfCode())).findAny().orElse(null);
                if (ObjectUtil.isNotEmpty(productDetail)){
                    plan.setProDepict(productDetail.getProDepict());
                    plan.setProSpecs(productDetail.getProSpecs());
                    plan.setProFactoryName(productDetail.getProFactoryName());
                    plan.setProUnit(productDetail.getProUnit());
                }
            }
        }
        long endTime = System.currentTimeMillis();
        logger.info("<系统级新品铺货计划><有新品铺货计划明细><查询时间：{}ms>",endTime-startTime);
        return plans;
    }

    /**
     * 有计划-新品铺货详情导出
     * @param planDto
     * @return
     */
    @Override
    public Result export(NewProductDistributionPlanDto planDto) {
        List<NewProductDistributionPlanVo> plan = plan(planDto);
        Result result = null;
        for (int i = 0; i < plan.size(); i++) {
            plan.get(i).setIndex(i+1);
            plan.get(i).setProSelfCode("\t" + (plan.get(i).getProSelfCode()==null?"":plan.get(i).getProSelfCode()));
        }
        CsvFileInfo csvInfo = CsvClient.getCsvByte(plan, "有新品铺货计划明细", Collections.singletonList((short) 1));
        if(ObjectUtil.isEmpty(csvInfo)){
            throw new BusinessException("导出数据为空！（或超过50万条）");
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            bos.write(csvInfo.getFileContent());
            result = this.cosUtils.uploadFile(bos, csvInfo.getFileName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bos != null) {
                    bos.flush();
                    bos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    @Override
    public JsonResult noPlan(NewProductDistributionNoPlanDto noPlanDto) {
        List<GaiaNewProductDistributionNoPlanVo> noPlanResult = newProductDistributionPlanMapper.findNoPlanResult(noPlanDto);
        List<GaiaNewProductDistributionNoPlanVo> voList=new ArrayList<>();
        noPlanResult.forEach(result->{
            if (result.getCusCode() != null) {
                voList.add(result);
            }
        });
        return JsonResult.success(voList,"查询成功");
    }

    @Override
    public JsonResult getNoPlanClientList(NewProductDistributionNoPlanClientDto noPlanClientDto) {
        List<Map<String,String>> clientList= newProductDistributionPlanMapper.findNoPlanClientList(noPlanClientDto);
        return JsonResult.success(clientList,"查询成功");
    }

    @Override
    public JsonResult getProductCodeList(NewProductDistributionNoPlanProductCodeDto noPlanProductCodeDto) {
        List<Map<String,String>> productCodeStringList=newProductDistributionPlanMapper.findNoPlanProductCodeList(noPlanProductCodeDto);
        if (StringUtils.isNotEmpty(noPlanProductCodeDto.getContent())) {
            noPlanProductCodeDto.setQueryStringList(Arrays.stream(noPlanProductCodeDto.getContent().split(",")).collect(Collectors.toList()));
        }
        Map<String,Set<String>> productCodeList=new HashMap<>();
        Set<String> clientSet=new HashSet<>();
        Set<String> productCode=new HashSet<>();
        productCodeStringList.forEach(clientMap->{
            if (clientMap.get("client") != null) {
                productCodeList.put(clientMap.get("client"), Arrays.stream(clientMap.get("proStr").split(",")).collect(Collectors.toSet()));
                clientSet.add(clientMap.get("client"));
                productCode.addAll(Arrays.stream(clientMap.get("proStr").split(",")).collect(Collectors.toSet()));
            }
        });
        PageHelper.startPage(noPlanProductCodeDto.getPageNum(),noPlanProductCodeDto.getPageSize());

        List<Map<String,String>> proDetail=newProductDistributionPlanMapper.findProDetailByProCode(clientSet,productCode,noPlanProductCodeDto);
        PageInfo<Map<String, String>> page = new PageInfo<>(proDetail);
        return JsonResult.success(page,"查询成功");
    }

    @Override
    public JsonResult getProv(NewProductDistributionNoPlanProvAndCityDto noPlanProvAndCityDto) {
        List<Map<String,String>> provList=newProductDistributionPlanMapper.findProvList(noPlanProvAndCityDto);
        List<Map<String,String>> voList=new ArrayList<>();
        provList.forEach(prov->{
            if (prov.get("provCode") != null) {
                voList.add(prov);
            }
        });
        return JsonResult.success(voList,"查询成功");
    }

    @Override
    public JsonResult getCityList(NewProductDistributionNoPlanProvAndCityDto noPlanProvAndCityDto) {
        List<Map<String,String>> cityList= newProductDistributionPlanMapper.findCityList(noPlanProvAndCityDto);
        List<Map<String,String>> voList=new ArrayList<>();
        cityList.forEach(city->{
            if (city.get("cityCode") != null) {
                voList.add(city);
            }
        });
        return JsonResult.success(voList,"查询成功");
    }
}
