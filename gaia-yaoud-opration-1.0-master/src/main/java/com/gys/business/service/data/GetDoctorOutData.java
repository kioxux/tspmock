package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class GetDoctorOutData implements Serializable {
    private static final long serialVersionUID = 5577225574466160812L;
    private String storName;
    private String no;
    private String orgName;
    private String name;
    private String id;

}
