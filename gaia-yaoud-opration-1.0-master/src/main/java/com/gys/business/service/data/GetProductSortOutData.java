//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "商品分类")
public class GetProductSortOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "分类编号")
    private String gspgIdSort;
    @ApiModelProperty(value = "分类名称")
    private String gspgNameSort;
    @ApiModelProperty(value = "商品编码")
    private String gspgProId;
    @ApiModelProperty(value = "商品通用名")
    private String gspgProName;
    @ApiModelProperty(value = "修改人")
    private String gspgUpdateEmp;
    @ApiModelProperty(value = "")
    private Integer indexSort;

    public GetProductSortOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspgIdSort() {
        return this.gspgIdSort;
    }

    public String getGspgNameSort() {
        return this.gspgNameSort;
    }

    public String getGspgProId() {
        return this.gspgProId;
    }

    public String getGspgProName() {
        return this.gspgProName;
    }

    public String getGspgUpdateEmp() {
        return this.gspgUpdateEmp;
    }

    public Integer getIndexSort() {
        return this.indexSort;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspgIdSort(final String gspgIdSort) {
        this.gspgIdSort = gspgIdSort;
    }

    public void setGspgNameSort(final String gspgNameSort) {
        this.gspgNameSort = gspgNameSort;
    }

    public void setGspgProId(final String gspgProId) {
        this.gspgProId = gspgProId;
    }

    public void setGspgProName(final String gspgProName) {
        this.gspgProName = gspgProName;
    }

    public void setGspgUpdateEmp(final String gspgUpdateEmp) {
        this.gspgUpdateEmp = gspgUpdateEmp;
    }

    public void setIndexSort(final Integer indexSort) {
        this.indexSort = indexSort;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetProductSortOutData)) {
            return false;
        } else {
            GetProductSortOutData other = (GetProductSortOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label95;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label95;
                    }

                    return false;
                }

                Object this$gspgIdSort = this.getGspgIdSort();
                Object other$gspgIdSort = other.getGspgIdSort();
                if (this$gspgIdSort == null) {
                    if (other$gspgIdSort != null) {
                        return false;
                    }
                } else if (!this$gspgIdSort.equals(other$gspgIdSort)) {
                    return false;
                }

                Object this$gspgNameSort = this.getGspgNameSort();
                Object other$gspgNameSort = other.getGspgNameSort();
                if (this$gspgNameSort == null) {
                    if (other$gspgNameSort != null) {
                        return false;
                    }
                } else if (!this$gspgNameSort.equals(other$gspgNameSort)) {
                    return false;
                }

                label74: {
                    Object this$gspgProId = this.getGspgProId();
                    Object other$gspgProId = other.getGspgProId();
                    if (this$gspgProId == null) {
                        if (other$gspgProId == null) {
                            break label74;
                        }
                    } else if (this$gspgProId.equals(other$gspgProId)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$gspgProName = this.getGspgProName();
                    Object other$gspgProName = other.getGspgProName();
                    if (this$gspgProName == null) {
                        if (other$gspgProName == null) {
                            break label67;
                        }
                    } else if (this$gspgProName.equals(other$gspgProName)) {
                        break label67;
                    }

                    return false;
                }

                Object this$gspgUpdateEmp = this.getGspgUpdateEmp();
                Object other$gspgUpdateEmp = other.getGspgUpdateEmp();
                if (this$gspgUpdateEmp == null) {
                    if (other$gspgUpdateEmp != null) {
                        return false;
                    }
                } else if (!this$gspgUpdateEmp.equals(other$gspgUpdateEmp)) {
                    return false;
                }

                Object this$indexSort = this.getIndexSort();
                Object other$indexSort = other.getIndexSort();
                if (this$indexSort == null) {
                    if (other$indexSort != null) {
                        return false;
                    }
                } else if (!this$indexSort.equals(other$indexSort)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetProductSortOutData;
    }

    public int hashCode() {
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspgIdSort = this.getGspgIdSort();
        result = result * 59 + ($gspgIdSort == null ? 43 : $gspgIdSort.hashCode());
        Object $gspgNameSort = this.getGspgNameSort();
        result = result * 59 + ($gspgNameSort == null ? 43 : $gspgNameSort.hashCode());
        Object $gspgProId = this.getGspgProId();
        result = result * 59 + ($gspgProId == null ? 43 : $gspgProId.hashCode());
        Object $gspgProName = this.getGspgProName();
        result = result * 59 + ($gspgProName == null ? 43 : $gspgProName.hashCode());
        Object $gspgUpdateEmp = this.getGspgUpdateEmp();
        result = result * 59 + ($gspgUpdateEmp == null ? 43 : $gspgUpdateEmp.hashCode());
        Object $indexSort = this.getIndexSort();
        result = result * 59 + ($indexSort == null ? 43 : $indexSort.hashCode());
        return result;
    }

    public String toString() {
        return "GetProductSortOutData(clientId=" + this.getClientId() + ", gspgIdSort=" + this.getGspgIdSort() + ", gspgNameSort=" + this.getGspgNameSort() + ", gspgProId=" + this.getGspgProId() + ", gspgProName=" + this.getGspgProName() + ", gspgUpdateEmp=" + this.getGspgUpdateEmp() + ", indexSort=" + this.getIndexSort() + ")";
    }
}
