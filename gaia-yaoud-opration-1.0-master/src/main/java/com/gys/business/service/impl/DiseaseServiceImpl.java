package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaSdSystemPara;
import com.gys.business.mapper.entity.RecommendFirstPlan;
import com.gys.business.mapper.entity.RecommendFirstPlanProduct;
import com.gys.business.service.DiseaseService;
import com.gys.business.service.data.disease.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.xxl.job.core.context.XxlJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/8/6 13:41
 * @Version 1.0.0
 **/
@Service
public class DiseaseServiceImpl implements DiseaseService {
    @Autowired
    private GaiaDiseaseMedicationMapper diseaseMedicationMapper;
    @Autowired
    private GaiaDiseaseClassifcationMapper diseaseClassifcationMapper;
    @Autowired
    private RecommendFirstPlanMapper recommendFirstPlanMapper;
    @Autowired
    private RecommendFirstPlanProductMapper recommendFirstPlanProductMapper;
    @Autowired
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;

    @Override
    public List<RelevanceProductOutData> getRelevanceProduct(GetLoginOutData userInfo, RelevanceProductInData inData) {
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
//        inData.setClient("10000005");
//        inData.setStoCode("10000");
        List<RelevanceProductOutData> outDatas = diseaseMedicationMapper.selectDisease(inData);
        if (ObjectUtil.isNotEmpty(outDatas)) {
            /****************** START 查询有效方案(两个的那玩意)  *******************/
            Map<String, Object> map = new HashMap<>();
            map.put("client", inData.getClient());
            map.put("stoCode", inData.getStoCode());
            map.put("currentDate", CommonUtil.getyyyyMMdd());
            RecommendFirstPlan planId = recommendFirstPlanMapper.getRecentlyPlan(map);
            List<RecommendFirstPlanProduct> planProducts = null;
            //  查询有效商品 没有就没有那俩玩意
            List<SchemeBO> firstRecommendList = new ArrayList<>();
            if (ObjectUtil.isNotEmpty(planId)) {
                Example exampleProduct = new Example(RecommendFirstPlanProduct.class);
                exampleProduct.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("planId", planId.getId());
                planProducts = recommendFirstPlanProductMapper.selectByExample(exampleProduct);
                inData.setPlanProducts(planProducts);
                inData.setPreferences(planId.getPreferences());
                firstRecommendList = diseaseMedicationMapper.getFirstRecommendProduct(inData);
            }
            /****************** END 查询有效方案  *******************/

            /****************** START 配置优先级(六个的那玩意)  *******************/
            GaiaSdSystemPara systemPara = gaiaSdSystemParaMapper.getAuthByClientAndBrId(inData.getClient(), inData.getStoCode(), "RELEVANCE_RECOMMEND");
            /******************  END  配置优先级(六个的那玩意)  *******************/
            if (ObjectUtil.isEmpty(systemPara)) {
                systemPara = new GaiaSdSystemPara();
                systemPara.setGsspPara("LSJ");
            }
            inData.setPreferences2(systemPara.getGsspPara());
            List<SchemeBO> relevanceRecommendList = diseaseMedicationMapper.getRelevanceRecommendProduct(inData);

            /******************  END  查询有效方案(两个的那玩意)  *******************/

            Set<String> firstProCodeArr = new HashSet<>();

            for (RelevanceProductOutData relevanceProduct : outDatas) {
                /****************** START 首推商品(两个的那玩意)  *******************/
                if (ObjectUtil.isNotEmpty(firstRecommendList)) {
                    List<SchemeBO> firstRecommendByDis = firstRecommendList.stream().filter(s -> s.getDiseaseCode().equals(relevanceProduct.getDiseaseCode())).collect(Collectors.toList());
                    /**
                     *  查询轮回
                     *  第一次查询 根据疾病编码和方案编码取正序第一个
                     *  后面的查询如过是同一下疾病编码 方案编码下移一位 最后一个跳转到第一个
                     **/
                    int i = 0;
                    if (CollUtil.isNotEmpty(firstRecommendByDis) && firstRecommendByDis.size() > 1) {
                        for (i = 0; i < firstRecommendByDis.size(); i++) {
                            SchemeBO firstRecommend = firstRecommendByDis.get(i);
                            if (firstRecommend.getDiseaseCode().equals(inData.getDiseaseCode())
                                    && firstRecommend.getSchemeCode().equals(inData.getSchemeCode())) {
                                i++;
                                break;
                            }
                        }
                    }

                    //  如果已经是最后一个  跳转到第一个
                    if (firstRecommendList.size() == i) {
                        i = 0;
                    }
                    //  这是首推需要的疾病
                    SchemeBO firstRecommendPro = firstRecommendList.get(i);
                    relevanceProduct.setSchemeCode(firstRecommendPro.getSchemeCode());
                    List<RecommendProductBO> auxiliaryMedications = firstRecommendPro.getAuxiliaryMedications();

                    List<RecommendProductBO> firstRecommendOuts = new LinkedList<>();

                    if (ObjectUtil.isNotEmpty(auxiliaryMedications)) {
                        //  判断关联组方 是否超过 1个  大于1 从每一组里面取一个  等于1 从1里取两个
                        Set<String> num = auxiliaryMedications.stream().map(RecommendProductBO::getAuxiliaryMedication).collect(Collectors.toSet());
                        int size1 = num.size();
                        if (size1 > 1) {
                            for (int j = 1; j <= num.size(); j++) {
                                String index = String.valueOf(j);
                                List<RecommendProductBO> auxiliaryMedication = auxiliaryMedications.stream().filter(s -> index.equals(s.getAuxiliaryMedication())).collect(Collectors.toList());
                                firstRecommendOuts.add(auxiliaryMedication.get(0));
                                firstProCodeArr.add(auxiliaryMedication.get(0).getProCode());
                            }
                        } else if (size1 == 1) {
                            firstRecommendOuts = auxiliaryMedications.size() > 2 ? auxiliaryMedications.subList(0,2) : auxiliaryMedications ;
                            firstProCodeArr = firstRecommendOuts.stream().map(RecommendProductBO::getProCode).collect(Collectors.toSet());
                        }
                        firstRecommendOuts.forEach(item -> {
                            item.setInProCode(inData.getProCode());
                        });
                        relevanceProduct.setFirstRecommend(firstRecommendOuts);

                    }
                }

                /******************  END  首推商品(两个的那玩意)  *******************/

                /****************** START 推荐商品(六个的那玩意)  *******************/
                if (ObjectUtil.isNotEmpty(relevanceRecommendList)) {
                    List<SchemeBO> relevanceRecommendDis = relevanceRecommendList.stream().filter(s -> s.getDiseaseCode().equals(relevanceProduct.getDiseaseCode())).collect(Collectors.toList());

                    int x = 0;
                    if (CollUtil.isNotEmpty(relevanceRecommendDis) && relevanceRecommendDis.size() > 1) {
                        for (x = 0; x < relevanceRecommendDis.size(); x++) {
                            SchemeBO relevanceRecommend = relevanceRecommendDis.get(x);
                            if (relevanceRecommend.getDiseaseCode().equals(inData.getDiseaseCode())
                                    && relevanceRecommend.getSchemeCode().equals(inData.getSchemeCode())) {
                                x++;
                                break;
                            }
                        }
                    }
                    //  如果已经是最后一个  跳转到第一个
                    if (relevanceRecommendDis.size() == x) {
                        x = 0;
                    }
                    //  这是关联推荐需要的疾病
                    SchemeBO relevanceRecommendPro = relevanceRecommendList.get(x);
                    relevanceProduct.setSchemeCode(relevanceRecommendPro.getSchemeCode());
                    List<RecommendProductBO> auxiliaryMedications2 = relevanceRecommendPro.getAuxiliaryMedications();
                    List<RecommendProductBO> relevanceRecommendOuts = new LinkedList<>();

                    /******************  END  推荐商品(六个的那玩意)  *******************/
                    if (ObjectUtil.isNotEmpty(auxiliaryMedications2)) {
                        //  判断关联组方 是否超过 1个  大于1 从每一组里面取一个  等于1 从1里取两个
                        Set<String> num = auxiliaryMedications2.stream().map(RecommendProductBO::getAuxiliaryMedication).collect(Collectors.toSet());
                        for (int j = 1; j <= num.size(); j++) {
                            String index = String.valueOf(j);
                            //  首推商品编码 firstProCodeArr 关联推荐商品剔除首推商品
                            Set<String> finalFirstProCodeArr = firstProCodeArr;
                            List<RecommendProductBO> auxiliaryMedicationPro2 = auxiliaryMedications2.stream()
                                    .filter(s -> index.equals(s.getAuxiliaryMedication()) && (ObjectUtil.isEmpty(finalFirstProCodeArr) ? true : !finalFirstProCodeArr.contains(s.getProCode())))
                                    .collect(Collectors.toList());
                            if (ObjectUtil.isNotEmpty(auxiliaryMedicationPro2)) {
                                int size3 = auxiliaryMedicationPro2.size();
                                LinkedList<RecommendProductBO> recommendProductPros = new LinkedList<>(auxiliaryMedicationPro2);

                                //  效期优先直接取效期最近的三个
                                if ("XQ".equals(systemPara.getGsspPara())) {
                                    recommendProductPros = new LinkedList(recommendProductPros.subList(0, size3 > 3 ? 3 : size3));
                                    int y = 1;
                                    for (RecommendProductBO item : recommendProductPros) {
                                        item.setSort(String.valueOf(y));
                                        relevanceRecommendOuts.add(item);
                                        y++;
                                    }
                                } else {
                                    if (size3 == 1) {
                                        RecommendProductBO item1 = recommendProductPros.get(0);
                                        item1.setSort(String.valueOf(1));
                                        relevanceRecommendOuts.add(item1);
                                    } else if (size3 == 2) {
                                        RecommendProductBO item1 = recommendProductPros.get(0);
                                        item1.setSort(String.valueOf(1));
                                        relevanceRecommendOuts.add(item1);
                                        RecommendProductBO item2 = recommendProductPros.get(1);
                                        item2.setSort(String.valueOf(2));
                                        relevanceRecommendOuts.add(item2);
                                    } else if (size3 > 2) {
                                        //  取高  零售价或毛利最大的
                                        RecommendProductBO item1 = recommendProductPros.removeFirst();
                                        item1.setSort(String.valueOf(1));
                                        relevanceRecommendOuts.add(item1);
                                        //  取低  零售价或毛利最小的
                                        RecommendProductBO item3 = recommendProductPros.removeLast();
                                        item3.setSort(String.valueOf(3));
                                        relevanceRecommendOuts.add(item3);

                                        //  取种  getAvgNormalPrice和getAvgGrossprofit是  零售价-平均零售价再取绝对值   最小的那个就是离平均价最近的
                                        if ("LSJ".equals(systemPara.getGsspPara())) {
                                            recommendProductPros = new LinkedList(recommendProductPros.stream().sorted(Comparator.comparing(RecommendProductBO::getAvgNormalPrice)).collect(Collectors.toList()));
                                        } else if ("MLL".equals(systemPara.getGsspPara())) {
                                            recommendProductPros = new LinkedList(recommendProductPros.stream().sorted(Comparator.comparing(RecommendProductBO::getAvgGrossprofit)).collect(Collectors.toList()));
                                        }
                                        RecommendProductBO item2 = recommendProductPros.get(0);
                                        item2.setSort(String.valueOf(2));
                                        relevanceRecommendOuts.add(item2);

                                    }
                                }
                            }
                        }
                        relevanceRecommendOuts.forEach(item -> {
                            item.setInProCode(inData.getProCode());
                        });
                        relevanceProduct.setRelevanceRecommend(relevanceRecommendOuts);

                    }
                }
            }
        }
        return outDatas;
    }

    /**
     * 会员贴标签
     */
    @Override
    public void setMemberLabel(String startDate, String endDate, Boolean delete) {

        /**falg
         * 1.获取前一天的日期
         * 2.从sale_h表拿到所有的会员信息 关联到子订单表 用成分表关联关联GAIA_DISEASE_CLASSIFCATION
         * 3.true 全量删除  false 不删除累加
         */
        if(Objects.isNull(delete)){
            delete = false;
        }else {
            if(delete){
                //全量删除
                diseaseMedicationMapper.deleteMemberTag();
            }
        }
        List<String> dates = new ArrayList<>();
        if (StrUtil.isNotBlank(startDate) && StrUtil.isNotBlank(endDate)) {
            try {
                dates = com.gys.util.DateUtil.getDateList(startDate, endDate);
            } catch (ParseException e) {
                XxlJobHelper.log("<会员卡贴标签日期获取失败> {}", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
            }
        } else {
            dates.add(DateUtil.format(DateUtil.parse(DateUtil.yesterday().toString(), "yyyy-MM-dd"), "yyyyMMdd"));
        }
        if (CollUtil.isNotEmpty(dates)) {
            List<Map<String, String>> getClientStore = diseaseMedicationMapper.getClientStore();
            if (CollUtil.isNotEmpty(getClientStore)) {
                for (String date : dates) {
                    getClientStore.forEach(clientSt -> {
                        List<MemberLabelResponse> list = diseaseMedicationMapper.getMemberLabel(date, clientSt.get("client"), clientSt.get("store"));
                        //List<MemberLabelResponse> list = diseaseMedicationMapper.getMemberLabel(date, "10000001", "1000000001");
                        if (CollUtil.isNotEmpty(list)) {
                            //分组
                            Map<String, List<MemberLabelResponse>> proCompClassMap = list.stream().collect(Collectors.groupingBy(s -> s.getCardId() + "-" + s.getProCompClass()));
                            for (Map.Entry<String, List<MemberLabelResponse>> entry : proCompClassMap.entrySet()) {
                                List<MemberLabelResponse> value = entry.getValue();
                                List<MemberLabelResponse> responseList = new ArrayList<>();
                                MemberLabelResponse mber = new MemberLabelResponse();
                                if (CollUtil.isNotEmpty(value)) {
                                    MemberLabelResponse labelResponse = value.get(0);
                                    //成分标签
                                    BeanUtil.copyProperties(labelResponse, mber);
                                    mber.setType("0");
                                    //成分标签给疾病
                                    mber.setDiseaseCode(labelResponse.getProCompClass());
                                    value.forEach(v -> {
                                        //疾病
                                        v.setType("1");
                                    });
                                    value.add(mber);
                                    responseList.addAll(value);
                                }
                                if (CollUtil.isNotEmpty(responseList)) {
                                    for (MemberLabelResponse memberLabel : responseList) {
                                        try {
                                            //查询会员标签统计列表是否存在  维度：client加盟商 GSMTL_CARD_ID会员卡号 GSMTL_TAG_ID会员标签 GSMTL_CREATE_DATE创建日期
                                            List<MemberTagListResponse> memberTagList = diseaseMedicationMapper.getMemberTagList(memberLabel.getCardId(), memberLabel.getDiseaseCode(), memberLabel.getClient(), date);
                                            if (CollUtil.isNotEmpty(memberTagList)) {
                                                MemberTagListResponse memberTag = memberTagList.get(0);
                                                //消费次数+1
                                                memberTag.setSaleTimes(Convert.toStr((Convert.toLong(memberTag.getSaleTimes()) + 1)));
                                                //金额相加
                                                memberTag.setAmt(Convert.toStr(new BigDecimal(memberLabel.getAmt()).add(new BigDecimal(memberTag.getAmt()))));
                                                //更新日期
                                                memberTag.setUpdateDate(DateUtil.format(DateUtil.parse(DateUtil.yesterday().toString(), "yyyy-MM-dd"), "yyyyMMdd"));
                                                XxlJobHelper.log("<会员卡贴标签修改数量,金额> {},{}", memberTag, DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
                                                //修改会员数量 金额
                                                diseaseMedicationMapper.update(memberTag);
                                            } else {
                                                //新增会员
                                                MemberTagListResponse memberTag = new MemberTagListResponse();
                                                BeanUtil.copyProperties(memberLabel, memberTag);
                                                //标签
                                                memberTag.setTagId(memberLabel.getDiseaseCode());
                                                //消费金额
                                                memberTag.setAmt(memberLabel.getAmt());
                                                //消费次数
                                                memberTag.setSaleTimes("1");
                                                //标签类型
                                                memberTag.setType(memberLabel.getType());
                                                //计算月份
                                                memberTag.setCalcMonth(DateUtil.format(DateUtil.parse(memberLabel.getGssdDate(), "yyyyMMdd"), "yyyyMM"));
                                                //更新时间
                                                memberTag.setUpdateDate(memberLabel.getGssdDate());
                                                XxlJobHelper.log("<会员卡贴标签新增> {},{}", memberTag, DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
                                                diseaseMedicationMapper.insertMemberTag(memberTag);
                                            }
                                        } catch (Exception e) {
                                            XxlJobHelper.log("<会员卡贴标签同步数据失败> {},{}", e, DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }
    }

    /**
     * 通过加盟商会员卡获取会员标签
     *
     * @param request
     * @return
     */
    @Override
    public List<SelectCardTagResponse> getCardTag(SelectCardTagRequest request) {
        if(StrUtil.isBlank(request.getMemberCard())){
            throw new BusinessException("会员卡号不可为空！");
        }
        List<SelectCardTagResponse> list = diseaseMedicationMapper.getCardTag(request);
        return list;
    }
}
