package com.gys.business.service.data.his;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/25 17:21
 */
@Data
public class OrderCouponForm {
    @ApiModelProperty(value = "会员手机号", example = "15009872456")
    private String memberPhone;
    @ApiModelProperty(value = "优惠券编码", example = "Sd008jde333001")
    private String couponNo;
}
