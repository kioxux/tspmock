//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetMedCheckInData;
import com.gys.business.service.data.GetMedCheckOutData;
import com.gys.business.service.data.UserInData;
import com.gys.business.service.data.UserOutData;
import com.gys.common.data.GetLoginOutData;
import java.util.List;

public interface MedCheckService {
    List<GetMedCheckOutData> selectMedCheckList(GetMedCheckInData inData);

    void saveMedCheck(GetMedCheckInData inData, GetLoginOutData userInfo);

    GetMedCheckOutData selectMedCheckOne(GetMedCheckInData inData);

    void approveMedCheck(GetMedCheckInData inData, GetLoginOutData userInfo);

    List<UserOutData> selectPharmacistByBrId(UserInData inData);

}
