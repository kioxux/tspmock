package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaMaterialDocMapper;
import com.gys.business.mapper.StockMouthMapper;
import com.gys.business.mapper.entity.GaiaMaterialDoc;
import com.gys.business.mapper.entity.StockMouth;
import com.gys.business.service.StockMouthCaculateStrategy;
import com.gys.business.service.data.StockMouthCaculateCondition;
import com.gys.common.exception.BusinessException;
import com.gys.util.DateUtil;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 选择从期上线月份更新至当月份实现推算的计算类
 */
@Transactional
public class StockMouthCaculateQCStrategy implements StockMouthCaculateStrategy {

    private StockMouthMapper stockMouthMapper;

    private GaiaMaterialDocMapper materialDocMapper;

    private StockMouthCaculateCondition condition;

    private boolean stopFlag = true;//标识，默认为true标识如果出现没有初期情况，跑出异常，但是定时任务模块不抛出异常


    public StockMouthCaculateQCStrategy(StockMouthMapper stockMouthMapper, GaiaMaterialDocMapper materialDocMapper, StockMouthCaculateCondition condition, boolean stopFlag) {
        this.stockMouthMapper = stockMouthMapper;
        this.materialDocMapper = materialDocMapper;
        this.condition = condition;
        this.stopFlag = stopFlag;
    }

    @Override
    public void caculate() {
        //先进行验证，用户输入的加盟商是否需要是没有QC
        Map<String, List<String>> clientSiteMap = this.condition.getClientSiteMap();
        List<String> clients = new ArrayList<>();
        clientSiteMap.forEach((x, y) -> {
            clients.add(x);
        });
        //此处扩展多客户运算的逻辑
        for (String x : clients) {


//        clients.forEach(x -> {
            List<String> sites = clientSiteMap.get(x);
            Example clientSiteExample = new Example(GaiaMaterialDoc.class);
            Example.Criteria clientSitecriteria = clientSiteExample.createCriteria().andEqualTo("matType", "QC")
                    .andEqualTo("client", x);
            if (CollectionUtil.isNotEmpty(sites)) {
                clientSitecriteria.andIn("matSiteCode", sites);
            }
            Integer count = materialDocMapper.selectCountByExample(clientSiteExample);
//            Integer count = this.materialDocMapper.getClientGcCount(x);
            if (count <= 0 && this.stopFlag) {
                throw new BusinessException("客户无期初数据，无法推算,客户编号：" + x);
            } else if (count <= 0 && !this.stopFlag) {
                //
                continue;
            }
            String client = x;
            //查询出对应的初期日期，直接做多个客户的模式，避免麻烦
            //线上操作现在目前选定的是单个客户，定时器不在意性能，所以直接进行初步循环
            String matPostDate = getMatDocDateInfo(client, materialDocMapper);
            if (StrUtil.isNotBlank(matPostDate)) {
                //此处不进行对凭证类型的查询，因为后期直接对查询数据进行过滤,此处查询的最低维度是月数据
                //设置查询条件加盟商+地点+年月组合
                Example example = new Example(GaiaMaterialDoc.class);
                Example.Criteria criteria = example.createCriteria()
                        .andEqualTo("client", client)
                        .andLike("matPostDate", matPostDate.substring(0, matPostDate.length() - 2) + "%");
                if (CollectionUtil.isNotEmpty(sites)) {
                    criteria.andIn("matSiteCode", sites);
                }
                List<GaiaMaterialDoc> inTimeDocList = materialDocMapper.selectByExample(example);
                //1.定义基数集合
                List<GaiaMaterialDoc> basicList = new ArrayList<>();
                //2.定义变动量集合
                List<GaiaMaterialDoc> changeList = new ArrayList<>();
                //为基数以及变动量进行初始化
                this.handleBasicAndChangeList(basicList, changeList, inTimeDocList);

                //分别将数据进行按维度分组--分组维度为地点+商品+批次
                Map<String, GaiaMaterialDoc> basicGroupMap = new HashMap<>();
//                    Map<String, GaiaMaterialDoc> changeGroupMap = new HashMap<>();
                basicList.forEach(b -> {
//                    if(b.getMatProCode().equals("22320")){
//                        System.out.println(111);
//                    }
                    String key = b.getClient() + b.getMatSiteCode() + b.getMatProCode() + b.getMatBatch();
                    //根据吴建的需求，在以上维度下可能存在多个数据的情况，那么在处理基础数据时，即对以上key相同情况下，需要进行合并累加
                    if (basicGroupMap.containsKey(key)) {
                        GaiaMaterialDoc qCDoc = basicGroupMap.get(key);
                        String matDebitCredit = b.getMatDebitCredit();//H-减，S-加
                        BigDecimal matQtyFinal = null;
                        BigDecimal matBatAmtFinal = null;
                        BigDecimal matRateBatFinal = null;
                        BigDecimal matQtyQC = qCDoc.getMatQty();//QC的物料凭证数量
                        BigDecimal matMatBatAmtQC = qCDoc.getMatBatAmt();//QC的总金额（批次）
                        BigDecimal matRateBatQC = qCDoc.getMatRateBat();//QC的税金（批次）
                        if (b.getMatDebitCredit().equals(matDebitCredit)) {
                            matQtyFinal = matQtyQC.add(b.getMatQty());
                            matBatAmtFinal = matMatBatAmtQC.add(b.getMatBatAmt());
                            matRateBatFinal = matRateBatQC.add(b.getMatRateBat());
                        } else {
                            matQtyFinal = matQtyQC.subtract(b.getMatQty());
                            matBatAmtFinal = matMatBatAmtQC.subtract(b.getMatBatAmt());
                            matRateBatFinal = matRateBatQC.subtract(b.getMatRateBat());
                        }
                        qCDoc.setMatQty(matQtyFinal);
                        qCDoc.setMatBatAmt(matBatAmtFinal);
                        qCDoc.setMatRateBat(matRateBatFinal);
                        basicGroupMap.put(key, qCDoc);//将单条的计算结果汇总到qc的计算结果中
                    } else {
                        basicGroupMap.put(key, b);
                    }

                });
                //test
//                GaiaMaterialDoc doc1 = basicGroupMap.get("100000121000009848SUP201114000122379");
//                System.out.println(doc1);
                //此处处理结束之后basicGroupMap中有就是QC当月的最初库存
                if (CollectionUtil.isNotEmpty(changeList)) {
                    changeList.forEach(c -> {
                        String key = c.getClient() + c.getMatSiteCode() + c.getMatProCode() + c.getMatBatch();
//                            changeGroupMap.put(key,c);
                        if (basicGroupMap.containsKey(key)) {
                            //QC物料凭证中存在，那么需要将本月的进出情况进行加减
//                                String matType = c.getMatType();
                            String matDebitCredit = c.getMatDebitCredit();//H-减，S-加
                            if (StrUtil.isNotBlank(matDebitCredit)) {
                                GaiaMaterialDoc qCDoc = basicGroupMap.get(key);
                                BigDecimal matQtyFinal = null;
                                BigDecimal matBatAmtFinal = null;
                                BigDecimal matRateBatFinal = null;
                                BigDecimal matQtyQC = qCDoc.getMatQty();//QC的物料凭证数量
                                BigDecimal matMatBatAmtQC = qCDoc.getMatBatAmt();//QC的总金额（批次）
                                BigDecimal matRateBatQC = qCDoc.getMatRateBat();//QC的税金（批次）
                                if (c.getMatDebitCredit().equals(matDebitCredit)) {
                                    matQtyFinal = matQtyQC.add(c.getMatQty());
                                    matBatAmtFinal = matMatBatAmtQC.add(c.getMatBatAmt());
                                    matRateBatFinal = matRateBatQC.add(c.getMatRateBat());
                                } else {
                                    matQtyFinal = matQtyQC.subtract(c.getMatQty());
                                    matBatAmtFinal = matMatBatAmtQC.subtract(c.getMatBatAmt());
                                    matRateBatFinal = matRateBatQC.subtract(c.getMatRateBat());
                                }
                                qCDoc.setMatQty(matQtyFinal);
                                qCDoc.setMatBatAmt(matBatAmtFinal);
                                qCDoc.setMatRateBat(matRateBatFinal);
                                basicGroupMap.put(key, qCDoc);//将单条的计算结果汇总到qc的计算结果中
                            }
                        } else {
                            //因为取不到对应的key，这意味着，可能是新的单据，而且理论上只能是新增种类
                            basicGroupMap.put(key, c);
                        }
                    });
                }

                //将basicGroupMap 中处理的当月最终库存处理入库，存储到月度库存表中
                List<StockMouth> insertQCList = new ArrayList<>();
                for (String key : basicGroupMap.keySet()) {
                    GaiaMaterialDoc doc = basicGroupMap.get(key);
                    //构建月度库存对象
                    StockMouth stockMouth = new StockMouth();
                    stockMouth.setClient(client);
                    stockMouth.setGsmProCode(doc.getMatProCode());
                    stockMouth.setGsmSiteCode(doc.getMatSiteCode());
                    stockMouth.setGsmYearMonth(doc.getMatPostDate().substring(0, matPostDate.length() - 2));
                    stockMouth.setGsmBatch(doc.getMatBatch());
                    stockMouth.setGsmStockQty(doc.getMatQty());
                    stockMouth.setGsmStockAmt(doc.getMatBatAmt());
                    stockMouth.setGsmTaxAmt(doc.getMatRateBat());
                    stockMouth.setLastUpdateTime(new Date());
                    insertQCList.add(stockMouth);
                }

                //因为推算数据是进行覆盖更新的，所以在插入数据之前必须要做删除
                Example deleteStockMonthExample = new Example(StockMouth.class);
                Example.Criteria criteria1 = deleteStockMonthExample.createCriteria().andEqualTo("client", x).andEqualTo("gsmYearMonth", matPostDate.substring(0, matPostDate.length() - 2));
                if (CollectionUtil.isNotEmpty(sites)) {
                    criteria1.andIn("gsmSiteCode", sites);
                }
                stockMouthMapper.deleteByExample(deleteStockMonthExample);
                //插入新的推算数据
                stockMouthMapper.insertList(insertQCList);
                //以上过程为QC的月份的实际库存数量，其实我们已经推算出QC月份的库存数量
                //接下来要根据选择月份进行到截止月份的所有月份的数据进行月度汇总，这个逻辑跟第二种根据月度推算逻辑相同，抽取公共方法，避免二次计算QC库存
                caculateStockMouthFromQCToChooseTime(insertQCList, matPostDate.substring(0, matPostDate.length() - 2), this.condition.getChooseDate(), client, sites, stockMouthMapper, materialDocMapper);
            }
        }
//        });
    }


    /**
     * 计算一段时间内的月度库存数据
     *
     * @param qCStockMouthList  Qc的月度库存,直接传递，避免二次计算
     * @param endYearMonth      结束时间，此处是用户选择的月份
     * @param stockMouthMapper
     * @param materialDocMapper
     */
    private void caculateStockMouthFromQCToChooseTime(List<StockMouth> qCStockMouthList, String qCYearMonth, String endYearMonth, String client, List<String> sites, StockMouthMapper stockMouthMapper, GaiaMaterialDocMapper materialDocMapper) {

        List<String> excludeMatTypeInChange = new ArrayList<>();
        excludeMatTypeInChange.add("LX");
        excludeMatTypeInChange.add("CX");
        excludeMatTypeInChange.add("PX");
        excludeMatTypeInChange.add("QC");
        //如果QC的库存都是空的，就不会进行下一步的计算
        if (CollectionUtil.isNotEmpty(qCStockMouthList)) {
            //计算出中间所有的月份
            List<String> yearMonthList = caculateAllYearMonths(qCYearMonth, endYearMonth);
            //如果中间不存在差值，无需计算
            //定义每一步计算的上一月度库存容器,初始值是QC库存
            List<StockMouth> basicList = qCStockMouthList;
            if (CollectionUtil.isNotEmpty(yearMonthList)) {
                for (String x : yearMonthList) {
                    //循环每个月份进行分别计算
                    //处理基数
                    Map<String, StockMouth> basicGroupMap = new HashMap<>();
                    basicList.forEach(y -> {
                        String key = y.getClient() + y.getGsmSiteCode() + y.getGsmProCode() + y.getGsmBatch();
                        basicGroupMap.put(key, y);
                    });
                    //处理变动量
                    Example example = new Example(GaiaMaterialDoc.class);
                    Example.Criteria criteria = example.createCriteria()
                            .andEqualTo("client", client)
                            .andLike("matPostDate", x + "%");
                    if (CollectionUtil.isNotEmpty(sites)) {
                        criteria.andIn("matSiteCode", sites);
                    }
                    example.orderBy("matCreateTime");
                    List<GaiaMaterialDoc> changeList = materialDocMapper.selectByExample(example);

                    if (CollectionUtil.isNotEmpty(changeList)) {
                        //过滤一些类型不进行处理
                        changeList = changeList.stream().filter(change -> !excludeMatTypeInChange.contains(change.getMatType())).collect(Collectors.toList());
                        changeList.forEach(ch -> {
//                            if(ch.getMatProCode().equals("21115") && ch.getMatBatch().equals("P20210001387")){
//                                System.out.println(111);
//                            }
                            //99999999
                            String key = ch.getClient() + ch.getMatSiteCode() + ch.getMatProCode() + ch.getMatBatch();
//                            changeGroupMap.put(key,c);
                            String matDebitCredit = ch.getMatDebitCredit();//H-减，S-加
                            if (basicGroupMap.containsKey(key)) {
                                //QC物料凭证中存在，那么需要将本月的进出情况进行加减
//                                String matType = c.getMatType();
                                if (StrUtil.isNotBlank(matDebitCredit)) {
                                    StockMouth qCStockMouth = basicGroupMap.get(key);
                                    BigDecimal matQtyFinal = null;
                                    BigDecimal matBatAmtFinal = null;
                                    BigDecimal matRateBatFinal = null;
                                    BigDecimal matQtySY = qCStockMouth.getGsmStockQty();//上月物料凭证数量
                                    BigDecimal matBatAmtSY = qCStockMouth.getGsmStockAmt();//上月的总金额（批次）
                                    BigDecimal matRateBatSY = qCStockMouth.getGsmTaxAmt();//上月的税金（批次）
                                    if ("S".equals(matDebitCredit)) {
                                        matQtyFinal = matQtySY.add(ch.getMatQty());
                                        matBatAmtFinal = matBatAmtSY.add(ch.getMatBatAmt());
                                        matRateBatFinal = matRateBatSY.add(ch.getMatRateBat());
                                    } else if ("H".equals(matDebitCredit)) {
                                        matQtyFinal = matQtySY.subtract(ch.getMatQty());
                                        matBatAmtFinal = matBatAmtSY.subtract(ch.getMatBatAmt());
                                        matRateBatFinal = matRateBatSY.subtract(ch.getMatRateBat());
                                    }
                                    qCStockMouth.setGsmStockQty(matQtyFinal);
                                    qCStockMouth.setGsmStockAmt(matBatAmtFinal);
                                    qCStockMouth.setGsmTaxAmt(matRateBatFinal);
                                    basicGroupMap.put(key, qCStockMouth);//将单条的计算结果汇总到qc的计算结果中
                                }
                            } else {
                                //因为取不到对应的key，这意味着，可能是新的单据，而且理论上只能是新增种类
                                //构建月度库存对象
                                StockMouth stockMouth = new StockMouth();
                                stockMouth.setClient(ch.getClient());
                                stockMouth.setGsmProCode(ch.getMatProCode());
                                stockMouth.setGsmSiteCode(ch.getMatSiteCode());
                                stockMouth.setGsmYearMonth(ch.getMatPostDate().substring(0, ch.getMatPostDate().length() - 2));
                                stockMouth.setGsmBatch(ch.getMatBatch());
                                stockMouth.setGsmStockQty(ch.getMatQty());
                                stockMouth.setGsmStockAmt(ch.getMatBatAmt());
                                stockMouth.setGsmTaxAmt(ch.getMatRateBat());
                                stockMouth.setLastUpdateTime(new Date());
                                basicGroupMap.put(key, stockMouth);


                            }
                        });
                    }
                    //将basicGroupMap 中处理的当月最终库存处理入库，存储到月度库存表中
                    List<StockMouth> insertQCList = new ArrayList<>();
                    for (String key : basicGroupMap.keySet()) {
                        StockMouth doc = basicGroupMap.get(key);
                        //构建月度库存对象
                        doc.setGsmYearMonth(x);
                        doc.setLastUpdateTime(new Date());
                        insertQCList.add(doc);
                    }
                    //因为推算数据是进行覆盖更新的，所以在插入数据之前必须要做删除
                    Example deleteStockMonthExample = new Example(StockMouth.class);
                    Example.Criteria criteria1 = deleteStockMonthExample.createCriteria().andEqualTo("client", client).andEqualTo("gsmYearMonth", x);
                    if (CollectionUtil.isNotEmpty(sites)) {
                        criteria1.andIn("gsmSiteCode", sites);
                    }
                    stockMouthMapper.deleteByExample(deleteStockMonthExample);

                    //插入推算数据
                    stockMouthMapper.insertList(insertQCList);
                    //给basicList即初始化的上月库存付值
                    basicList = insertQCList;
                }
            }
        }
    }

    private List<String> caculateAllYearMonths(String qCYearMonth, String endYearMonth) {
        List<String> resList = new ArrayList<>();
        if (Integer.parseInt(qCYearMonth) >= Integer.parseInt(endYearMonth)) {
            return resList;
        } else {
//            String startYearMonth = DateUtil.getNextMonth(qCYearMonth);
            while (!DateUtil.getNextMonth(qCYearMonth).equals(endYearMonth)) {
                resList.add(DateUtil.getNextMonth(qCYearMonth));
                qCYearMonth = DateUtil.getNextMonth(qCYearMonth);
//                startYearMonth = DateUtil.getNextMonth(qCYearMonth);
            }
            resList.add(endYearMonth);//添加上最后一个月，就是客户选择的月份，这个也是需要进行计算的
        }
        return resList;
    }


    private String getMatDocDateInfo(String client, GaiaMaterialDocMapper materialDocMapper) {
        Example example = new Example(GaiaMaterialDoc.class);
        example.createCriteria().andEqualTo("client", client).andEqualTo("matType", "QC");
        example.setOrderByClause("MAT_POST_DATE limit 1");
        GaiaMaterialDoc doc = materialDocMapper.selectOneByExample(example);
        if (doc != null) {
            return doc.getMatPostDate();
        } else {
            return "";
        }
    }

    private void handleBasicAndChangeList(List<GaiaMaterialDoc> basicList, List<GaiaMaterialDoc> changeList, List<GaiaMaterialDoc> inTimeDocList) {
        //初始化需要过滤的无靠凭证，给变动量使用
        List<String> excludeMatTypeInChange = new ArrayList<>();
        excludeMatTypeInChange.add("LX");
        excludeMatTypeInChange.add("CX");
        excludeMatTypeInChange.add("PX");
        excludeMatTypeInChange.add("QC");

        //筛选出基数以及变动量对应的结果
        if (CollectionUtil.isNotEmpty(inTimeDocList)) {
            inTimeDocList.forEach(x -> {
                if (StrUtil.isNotBlank(x.getMatType()) && (x.getMatType().equals(StockMouthCaculateCondition.QC))) {
                    basicList.add(x);
                }
                if (StrUtil.isNotBlank(x.getMatType()) && !excludeMatTypeInChange.contains(x.getMatType())) {
                    changeList.add(x);
                }else{
//                    System.out.println(x);
                }
            });
        }
    }


}
