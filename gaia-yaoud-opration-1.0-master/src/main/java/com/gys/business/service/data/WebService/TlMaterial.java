package com.gys.business.service.data.WebService;

import lombok.Data;

@Data
public class TlMaterial {
    //物料编号
    private String ItmID;
    //物料名称
    private String ItmName;
    //规格
    private String ItmSpec;
    //包装规格
    private String PackSpec;
    //产地
    private String Place;
    //计量单位
    private String Unit;
    //库存
    private String Stock;
    //单价
    private String Price;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"ItmID\":\"")
                .append(ItmID).append('\"');
        sb.append(",\"ItmName\":\"")
                .append(ItmName).append('\"');
        sb.append(",\"ItmSpec\":\"")
                .append(ItmSpec).append('\"');
        sb.append(",\"PackSpec\":\"")
                .append(PackSpec).append('\"');
        sb.append(",\"Place\":\"")
                .append(Place).append('\"');
        sb.append(",\"Unit\":\"")
                .append(Unit).append('\"');
        sb.append(",\"Stock\":\"")
                .append(Stock).append('\"');
        sb.append(",\"Price\":\"")
                .append(Price).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
