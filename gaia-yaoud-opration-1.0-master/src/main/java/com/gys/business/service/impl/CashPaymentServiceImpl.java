//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaCashPaymentDetailMapper;
import com.gys.business.mapper.GaiaCashPaymentMapper;
import com.gys.business.mapper.GaiaSdEmpGroupMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.entity.GaiaSdCashPayment;
import com.gys.business.mapper.entity.GaiaSdCashPaymentDetail;
import com.gys.business.service.CashPaymentService;
import com.gys.business.service.data.CashPaymentDetailOutData;
import com.gys.business.service.data.CashPaymentInData;
import com.gys.business.service.data.CashPaymentOutData;
import com.gys.business.service.data.GetEmpOutData;
import com.gys.business.service.data.GetSaleScheduleOutData;
import com.gys.business.service.data.GetSaleScheduleQueryInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CashPaymentServiceImpl implements CashPaymentService {
    @Autowired
    private GaiaCashPaymentMapper cashPaymentMapper;
    @Autowired
    private GaiaCashPaymentDetailMapper cashPaymentDetailMapper;
    @Autowired
    private GaiaSdEmpGroupMapper gaiaSdEmpGroupMapper;
    @Autowired
    private GaiaUserDataMapper gaiaUserDataMapper;

    public CashPaymentServiceImpl() {
    }

    public List<CashPaymentOutData> getCashPaymentList(CashPaymentInData inData) {
        new ArrayList();
        Map<String, Object> params = new HashMap();
        if (StrUtil.isNotEmpty(inData.getClientId())) {
            params.put("clientId", inData.getClientId());
        }

        if (StrUtil.isNotEmpty(inData.getGsdhBrId())) {
            params.put("gsdhBrId", inData.getGsdhBrId());
        }

        if (StrUtil.isNotEmpty(inData.getGsdhVoucherId())) {
            params.put("gsdhVoucherId", inData.getGsdhVoucherId());
        }

        if (StrUtil.isNotEmpty(inData.getGsdhDepositAmt())) {
            params.put("gsdhDepositAmt", inData.getGsdhDepositAmt());
        }

        if (StrUtil.isNotEmpty(inData.getGsdhBankId())) {
            params.put("gsdhBankId", inData.getGsdhBankId().split(","));
        }

        if (StrUtil.isNotEmpty(inData.getGsdhDepositDate())) {
            params.put("gsdhDepositDate", inData.getGsdhDepositDate());
        }

        if (StrUtil.isNotEmpty(inData.getGsddEmpGroup())) {
            params.put("gsddEmpGroup", inData.getGsddEmpGroup());
        }

        if (StrUtil.isNotEmpty(inData.getGsddEmp())) {
            params.put("gsddEmp", inData.getGsddEmp());
        }

        if (StrUtil.isNotEmpty(inData.getGsdhStatus())) {
            params.put("gsdhStatus", inData.getGsdhStatus());
        }

        if (StrUtil.isNotEmpty(inData.getGsdhCheckDate())) {
            params.put("gsdhCheckDate", inData.getGsdhCheckDate());
        }

        List<CashPaymentOutData> cashPaymentOutDataList = this.cashPaymentMapper.getCashPaymentList(params);
        if (cashPaymentOutDataList.size() > 0) {
            for(int i = 0; i < cashPaymentOutDataList.size(); ++i) {
                ((CashPaymentOutData)cashPaymentOutDataList.get(i)).setIndex(i + 1);
            }
        }

        return cashPaymentOutDataList;
    }

    public List<CashPaymentDetailOutData> getCashPaymentDetailList(CashPaymentOutData inData) {
        new ArrayList();
        List<CashPaymentDetailOutData> cashPaymentDetailOutDataList = this.cashPaymentDetailMapper.getCashPaymentDetailList(inData);
        if (cashPaymentDetailOutDataList.size() > 0) {
            for(int i = 0; i < cashPaymentDetailOutDataList.size(); ++i) {
                ((CashPaymentDetailOutData)cashPaymentDetailOutDataList.get(i)).setIndexDetail(i + 1);
            }
        }

        return cashPaymentDetailOutDataList;
    }

    @Transactional
    public void insert(CashPaymentOutData inData) {
        GaiaSdCashPayment cashPayment = new GaiaSdCashPayment();
        cashPayment.setClientId(inData.getClientId());
        cashPayment.setGsdhBrId(inData.getGsdhBrId());
        String voucherId = this.cashPaymentMapper.selectNextVoucherId(inData.getClientId());
        cashPayment.setGsdhVoucherId(voucherId);
        cashPayment.setGsdhDepositId(inData.getGsdhDepositId());
        cashPayment.setGsdhBankId(inData.getGsdhBankId());
        cashPayment.setGsdhBankAccount(inData.getGsdhBankAccount());
        cashPayment.setGsdhDepositDate(inData.getGsdhDepositDate());
        cashPayment.setGsdhDepositAmt(new BigDecimal(inData.getGsdhDepositAmt()));
        cashPayment.setGsdhStatus("0");
        this.cashPaymentMapper.insert(cashPayment);
        List<CashPaymentDetailOutData> details = inData.getDetailOutDataList();

        for(int i = 0; i < details.size(); ++i) {
            GaiaSdCashPaymentDetail detail = new GaiaSdCashPaymentDetail();
            detail.setGsddSerial(String.valueOf(i + 1));
            detail.setClientId(inData.getClientId());
            detail.setGsddVoucherId(voucherId);
            detail.setGsddBrId(inData.getGsdhBrId());
            detail.setGsddSaleDate(((CashPaymentDetailOutData)details.get(i)).getGsddSaleDate());
            detail.setGsddEmpGroup(((CashPaymentDetailOutData)details.get(i)).getGsddEmpGroup());
            detail.setGsddEmp(((CashPaymentDetailOutData)details.get(i)).getGsddEmp());
            detail.setGsddRmbAmt(((CashPaymentDetailOutData)details.get(i)).getGsddRmbAmt());
            this.cashPaymentDetailMapper.insert(detail);
        }

    }

    @Transactional
    public void approve(CashPaymentOutData inData) {
        GaiaSdCashPayment h = (GaiaSdCashPayment)this.cashPaymentMapper.selectByPrimaryKey(inData);
        if (ObjectUtil.isNull(h)) {
            throw new BusinessException("缴款信息不存在");
        } else if ("1".equals(h.getGsdhStatus())) {
            throw new BusinessException("该缴款信息已审核");
        } else {
            h.setGsdhCheckDate(DateUtil.format(new Date(), "yyyyMMdd"));
            h.setGsdhStatus("1");
            this.cashPaymentMapper.updateByPrimaryKey(h);
        }
    }

    public List<GetSaleScheduleOutData> getEmpGroupList(GetSaleScheduleQueryInData inData) {
        List<GetSaleScheduleOutData> saleScheduleOutData = this.gaiaSdEmpGroupMapper.selectAllEmpGroup(inData);
        return saleScheduleOutData;
    }

    public List<GetEmpOutData> getEmpList(GetLoginOutData inData) {
        return this.gaiaUserDataMapper.queryEmp(inData);
    }
}
