package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class IncomeStatementStoreDeficitOutDataResult {
    @ApiModelProperty(value = "批号")
    @JSONField(serialize = false)
    private String batchNo;

    @ApiModelProperty(value = "有效期至")
    private String validUntil;

    @ApiModelProperty(value = "库存数量")
    private BigDecimal qty;
}
