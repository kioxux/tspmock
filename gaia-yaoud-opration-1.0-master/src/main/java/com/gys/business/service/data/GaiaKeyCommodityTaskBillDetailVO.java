package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GaiaKeyCommodityTaskBillDetailVO {
    private String levelCode;
    private String stoCode;
    private String client;
    private String billCode;
    private String stoName;
    private BigDecimal avaSalesAmt;

}
