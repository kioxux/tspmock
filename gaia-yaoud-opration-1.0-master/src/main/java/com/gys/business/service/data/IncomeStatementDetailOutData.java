package com.gys.business.service.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "损溢单明细")
public class IncomeStatementDetailOutData {
    @ApiModelProperty(value = "损溢单号")
    private String gsisdVoucherId;

    @ApiModelProperty(value = "盘点单号")
    private String voucherId;

    @ApiModelProperty(value = "商品编码")
    private String gsisdProId;

    @ApiModelProperty(value = "商品名称")
    private String gsisdProName;

    @ApiModelProperty(value = "零售价")
    private String gsisdRetailPrice;

    @ApiModelProperty(value = "批号")
    private String gsisdBatchNo;

    @ApiModelProperty(value = "批次")
    private String gsisdBatch;

    private String gsisdDate;

    @ApiModelProperty(value = "有效期至")
    private String gsisdValidUntil;

    @ApiModelProperty(value = "库存数量")
    private String gsisdQty;

    @ApiModelProperty(value = "损溢原因")
    private String gsisdIsCause;

    @ApiModelProperty(value = "损溢数")
    private String gsisdIsQty;

    @ApiModelProperty(value = "实际数量")
    private String gsisdRealQty;

    @ApiModelProperty(value = "厂家")
    private String gsisdFactory;

    @ApiModelProperty(value = "产地")
    private String gsisdOrigin;

    @ApiModelProperty(value = "剂型")
    private String gsisdDosageForm;

    @ApiModelProperty(value = "单位")
    private String gsisdUnit;

    @ApiModelProperty(value = "商品规格")
    private String gsisdFormat;

    @ApiModelProperty(value = "批准文号")
    private String gsisdApprovalNum;

    @ApiModelProperty(value = "库存状态")
    private String gsisdStockStatus;

    @ApiModelProperty(value = "行号")
    private String gsisdSerial;

    @ApiModelProperty(value = "审核状态：0未审核，1审核未批准，2已审核")
    private String gsishStatus;

    private Integer indexDetail;

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private String clientId;

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private String gsisdBrId;

    @ApiModelProperty(value = "税率编码")
    private String proRate;
    @ApiModelProperty(value = "税率值")
    private String gsrdInputTaxValue;
    @ApiModelProperty(value = "是否可移除：0-否 1-是", example = "0")
    private Integer canDelete;


}
