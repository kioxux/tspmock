//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "养护药品信息")
public class GetMedCheckDInData {
    @ApiModelProperty(value = "药品编码")
    private String gsmcdProId;
    @ApiModelProperty(value = "药品批号")
    private String gsmcdBatchNo;
    @ApiModelProperty(value = "药品数量")
    private String gsmcdQty;
    @ApiModelProperty(value = "药品结论(1.合格  2.不合格  3.异常  4.锁定)")
    private String gsmcdResult;

    public GetMedCheckDInData() {
    }

    public String getGsmcdProId() {
        return this.gsmcdProId;
    }

    public String getGsmcdBatchNo() {
        return this.gsmcdBatchNo;
    }

    public String getGsmcdQty() {
        return this.gsmcdQty;
    }

    public String getGsmcdResult() {
        return this.gsmcdResult;
    }

    public void setGsmcdProId(final String gsmcdProId) {
        this.gsmcdProId = gsmcdProId;
    }

    public void setGsmcdBatchNo(final String gsmcdBatchNo) {
        this.gsmcdBatchNo = gsmcdBatchNo;
    }

    public void setGsmcdQty(final String gsmcdQty) {
        this.gsmcdQty = gsmcdQty;
    }

    public void setGsmcdResult(final String gsmcdResult) {
        this.gsmcdResult = gsmcdResult;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetMedCheckDInData)) {
            return false;
        } else {
            GetMedCheckDInData other = (GetMedCheckDInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label59: {
                    Object this$gsmcdProId = this.getGsmcdProId();
                    Object other$gsmcdProId = other.getGsmcdProId();
                    if (this$gsmcdProId == null) {
                        if (other$gsmcdProId == null) {
                            break label59;
                        }
                    } else if (this$gsmcdProId.equals(other$gsmcdProId)) {
                        break label59;
                    }

                    return false;
                }

                Object this$gsmcdBatchNo = this.getGsmcdBatchNo();
                Object other$gsmcdBatchNo = other.getGsmcdBatchNo();
                if (this$gsmcdBatchNo == null) {
                    if (other$gsmcdBatchNo != null) {
                        return false;
                    }
                } else if (!this$gsmcdBatchNo.equals(other$gsmcdBatchNo)) {
                    return false;
                }

                Object this$gsmcdQty = this.getGsmcdQty();
                Object other$gsmcdQty = other.getGsmcdQty();
                if (this$gsmcdQty == null) {
                    if (other$gsmcdQty != null) {
                        return false;
                    }
                } else if (!this$gsmcdQty.equals(other$gsmcdQty)) {
                    return false;
                }

                Object this$gsmcdResult = this.getGsmcdResult();
                Object other$gsmcdResult = other.getGsmcdResult();
                if (this$gsmcdResult == null) {
                    if (other$gsmcdResult != null) {
                        return false;
                    }
                } else if (!this$gsmcdResult.equals(other$gsmcdResult)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetMedCheckDInData;
    }

    public int hashCode() {

        int result = 1;
        Object $gsmcdProId = this.getGsmcdProId();
        result = result * 59 + ($gsmcdProId == null ? 43 : $gsmcdProId.hashCode());
        Object $gsmcdBatchNo = this.getGsmcdBatchNo();
        result = result * 59 + ($gsmcdBatchNo == null ? 43 : $gsmcdBatchNo.hashCode());
        Object $gsmcdQty = this.getGsmcdQty();
        result = result * 59 + ($gsmcdQty == null ? 43 : $gsmcdQty.hashCode());
        Object $gsmcdResult = this.getGsmcdResult();
        result = result * 59 + ($gsmcdResult == null ? 43 : $gsmcdResult.hashCode());
        return result;
    }

    public String toString() {
        return "GetMedCheckDInData(gsmcdProId=" + this.getGsmcdProId() + ", gsmcdBatchNo=" + this.getGsmcdBatchNo() + ", gsmcdQty=" + this.getGsmcdQty() + ", gsmcdResult=" + this.getGsmcdResult() + ")";
    }
}
