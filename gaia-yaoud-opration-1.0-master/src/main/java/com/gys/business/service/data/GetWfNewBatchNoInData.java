package com.gys.business.service.data;

import lombok.Data;

/**
 * 门店盘点新增批号审批
 * @author 陈浩
 */
@Data
public class GetWfNewBatchNoInData {
    private String index;

    private String wsdPddh;

    private String proSite;

    private String wsdMdMc;

    private String wsdSpBm;

    private String wsdSpMc;

    private String wsdSpGg;

    private String wsdSpCj;

    private String wsdPh;

    private String wsdScrq;

    private String wsdYxq;

    private String wsdGysBh;

    private String wsdGysMc;

    private String wsdPcj;

    private String wsdSqyy;
}
