//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaProductBasicImage;
import com.gys.business.mapper.entity.GaiaSdNbMedicare;
import com.gys.business.service.data.GetProductInfoQueryInData;
import com.gys.business.service.data.GetProductInfoQueryOutData;
import com.gys.business.service.data.productBasic.GetProductBasicInData;
import com.gys.business.service.data.productBasic.GetProductBasicOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ProductBasicService {
    List<GetProductInfoQueryOutData> getProductBasicList(GetProductInfoQueryInData inData);

    List<GetProductInfoQueryOutData> selectMedCheckProList(GetProductInfoQueryInData inData);

    List<GetProductInfoQueryOutData> selectPhysicalProList(GetProductInfoQueryInData inData);

    PageInfo<GetProductBasicOutData> list(GetProductBasicInData inData);

    @Transactional
    JsonResult edit(GetProductBasicOutData outData);

    void export(HttpServletRequest request, HttpServletResponse response, GetProductBasicInData inData);

    JsonResult getOne(Map<String,String> proCode);

    List<Map<String,String>> getUnitList();

    List<Map<String,String>> getTaxList(Map<String,String> map);


    List<GaiaSdNbMedicare> getNBYBProCode(GetProductInfoQueryInData inData);

    JsonResult batchUploadCheck(List<GaiaProductBasicImage> imageList);

    @Transactional(rollbackFor = Exception.class)
    JsonResult batchUpload(List<GaiaProductBasicImage> imageList);

    JsonResult downloadImage(List<String> proCodes);

    JsonResult appointUserQuery();
}
