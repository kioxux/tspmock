package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 药盟商品信息(GaiaProductYm)实体类
 *
 * @author XIAOZY
 * @since 2021-11-11 20:26:20
 */
@Data
public class GaiaProductYmInData implements Serializable {
    private static final long serialVersionUID = 363645518106968473L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 商品名称
     */
    private String drugName;
    private String client;
    private String stoCode;
    private String keywords;
    /**
     * 单位
     */
    private String unit;
    /**
     * 商品编码
     */
    private String drugId;
    /**
     * 国药准字
     */
    private String approvalNumber;
    private String proSelfCode;
    private String ymProCode;
    private String proCode;
    /**
     * 规格
     */
    private String spec;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;
    private String proCommonname;
    private String status;

}

