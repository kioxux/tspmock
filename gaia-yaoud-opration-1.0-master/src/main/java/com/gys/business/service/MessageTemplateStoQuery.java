package com.gys.business.service;

import lombok.Data;

import java.io.Serializable;

@Data
public class MessageTemplateStoQuery implements Serializable {

    private static final long serialVersionUID = -2088679527400816270L;

    private String client;

    private String gmtId;

}
