package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaBillOfArCancel;

import java.util.List;

/**
 * 应收核销清单表(GaiaBillOfArCancel)表服务接口
 *
 * @author makejava
 * @since 2021-09-18 15:20:50
 */
public interface GaiaBillOfArCancelService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaBillOfArCancel queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaBillOfArCancel> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param gaiaBillOfArCancel 实例对象
     * @return 实例对象
     */
    GaiaBillOfArCancel insert(GaiaBillOfArCancel gaiaBillOfArCancel);

    /**
     * 修改数据
     *
     * @param gaiaBillOfArCancel 实例对象
     * @return 实例对象
     */
    GaiaBillOfArCancel update(GaiaBillOfArCancel gaiaBillOfArCancel);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
