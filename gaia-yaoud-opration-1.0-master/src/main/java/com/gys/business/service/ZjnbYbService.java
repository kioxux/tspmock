package com.gys.business.service;


import com.gys.business.mapper.entity.GaiaNbybProCatalog;
import com.gys.business.mapper.entity.MibSettlementZjnb;
import com.gys.business.service.data.MibSettlementZjnbDto;
import com.gys.business.service.data.zjnb.MibProductMatchDto;
import com.gys.common.data.JsonResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ZjnbYbService {

    void insertSaleInfo(Map<String, Object> inData);

    List<MibSettlementZjnb> selectSaleInfo(Map<String, Object> inData);

    MibSettlementZjnb selectSaleInfoSum(Map<String, Object> inData);

    List<MibProductMatchDto> getNBMibCodeByCode(Map<String,Object> inData);

    int submitMibMatchPro(String site, List<MibProductMatchDto> matchList);
    int submitMibMatchPro2(String site ,List<MibProductMatchDto> matchList);

    void updateDetail(MibSettlementZjnb inData);

    List<GaiaNbybProCatalog> getNbybProCatalog(Map inData);
    GaiaNbybProCatalog getNbybProCatalogOne(Map inData);
    List<MibProductMatchDto> getNBMibCodeByWzbmCode(Map<String,Object> inData);

    void saveNBMibStockCode(Map<String,Object> inData);

    /**
     * 重打小票
     * @param inData
     * @return
     */
    MibSettlementZjnb accessToHealthCareInformation(Map<String, String> inData);

    /**
     * 重打发票
     * @param inData
     * @return
     */
    MibSettlementZjnbDto retypeItInvoice(Map<String, String> inData);

    void insert(MibSettlementZjnb zjnb);
}
