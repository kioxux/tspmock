package com.gys.business.service.data;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import lombok.Data;
import java.math.BigDecimal;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2022/1/11 13:58
 */
@CsvRow
@Data
public class AnnualTrainingPlanOutData {
    /**
     * 单号
     */
    private String gatpVoucherId;

    /**
     * 年份
     */
    @CsvCell(title = "年份",index = 1, fieldNo = 1)
    private String gatpYear;

    /**
     * 计划编号
     */
    @CsvCell(title = "计划编号",index = 2, fieldNo = 1)
    private String gatpPlanId;

    /**
     * 培训目标
     */
    @CsvCell(title = "培训目标",index = 3, fieldNo = 1)
    private String gatpGoals;

    /**
     * 培训时间
     */
    @CsvCell(title = "培训时间",index = 4, fieldNo = 1)
    private String gatpTrainingDate;

    /**
     * 培训内容
     */
    @CsvCell(title = "培训内容",index = 5, fieldNo = 1)
    private String gatpContent;

    /**
     * 听课部门
     */
    @CsvCell(title = "听课部门",index = 7, fieldNo = 1)
    private String gatpAttendanceDep;

    /**
     * 听课人员
     */
    @CsvCell(title = "听课人员",index = 8, fieldNo = 1)
    private String gatpAttendanceStaff;

    /**
     * 听课费用
     */
    @CsvCell(title = "预算费用",index = 9, fieldNo = 1)
    private BigDecimal gatpBudgetExpenses;

    /**
     * 授课人
     */
    @CsvCell(title = "授课人",index = 6, fieldNo = 1)
    private String gatpLecturer;

    /**
     * 负责人
     */
    @CsvCell(title = "负责人",index = 14, fieldNo = 1)
    private String gatpDirector;

    /**
     * 审核人
     */
    @CsvCell(title = "审核人",index = 12, fieldNo = 1)
    private String gatpReviewer;

    /**
     * 审核时间
     */
    @CsvCell(title = "审核时间",index = 13, fieldNo = 1)
    private String gatpAuditDate;

    /**
     * 效果反馈
     */
    @CsvCell(title = "效果反馈",index = 15, fieldNo = 1)
    private String gatpEffectFeedback;

    /**
     * 实施总结
     */
    @CsvCell(title = "实施总结",index = 16, fieldNo = 1)
    private String gatpExecutiveSummar;

    /**
     * 创建人
     */
    @CsvCell(title = "制订人",index = 10, fieldNo = 1)
    private String gatpMaker;

    /**
     * 创建时间
     */
    @CsvCell(title = "制订时间",index = 11, fieldNo = 1)
    private String gatpMakingDate;

    /**
     * 备注
     */
    @CsvCell(title = "备注",index = 17, fieldNo = 1)
    private String gatpRemarks;

}
