package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class InvalidStockInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "门店编码")
    private String brId;

    @ApiModelProperty(value = "员工编号")
    private String userId;

    @ApiModelProperty(value = "商品编码列表")
    private List<String> productIdList;
}
