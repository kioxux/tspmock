package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaStoreCategoryType;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.PayDayReportDetailOutData;
import com.gys.business.service.PayService;
import com.gys.business.service.ReportFormsService;
import com.gys.business.service.data.GetPayInData;
import com.gys.business.service.data.GetPayTypeOutData;
import com.gys.business.service.data.ReportForms.*;
import com.gys.business.service.data.SalespersonsSalesDetailsOutData;
import com.gys.business.service.data.StoClassOutData;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.FuzzyQueryForConditionInData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.StoreAttributeEnum;
import com.gys.common.enums.StoreDTPEnum;
import com.gys.common.enums.StoreMedicalEnum;
import com.gys.common.enums.StoreTaxClassEnum;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.util.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ReportFormsServiceImpl implements ReportFormsService {
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private GaiaMaterialDocMapper materialDocMapper;
    @Autowired
    private GaiaSdSaleDMapper saleDMapper;
    @Autowired
    private GaiaSdPayDayreportHMapper payDayreportH;
    @Resource
    private PayService payService;
    @Autowired
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Autowired
    private GaiaSdBatchChanegMapper sdBatchChanegMapper;
    @Autowired
    private PercentagePlanMapper percentagePlanMapper;
    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Autowired
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaSdStoresGroupMapper gaiaSdStoresGroupMapper;

    @Autowired
    private GaiaSoHeaderMapper gaiaSoHeaderMapper;
    @Autowired
    private GaiaUserDataMapper userDataMapper;

    @Autowired
    private GaiaSdPayDayreportDMapper payDayreportDMapper;

    @Autowired
    private GaiaSdPaymentMethodMapper paymentMethodMapper;

    @Override
    public PageInfo<SupplierDealOutData> selectSupplierDealPage(SupplierDealInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum() - 1, inData.getPageSize());
        }
        if ((ObjectUtil.isEmpty(inData.getStartDate()) || ObjectUtil.isEmpty(inData.getEndDate())) && StringUtils.isEmpty(inData.getDnId())) {
            throw new BusinessException("日期或单号不能为空！");
        }
        List<SupplierDealOutData> outData = this.materialDocMapper.selectSupplierDealPageBySiteCode(inData);
        List<SupplierDealOutData> list = new ArrayList<>();
        if (CollUtil.isNotEmpty(outData)) {
            List<Map<String, Object>> selectSupplierName = this.materialDocMapper.selectSupplierName(inData);
            if (StrUtil.isNotBlank(inData.getPoSupplierSalesman())) {
                //查询业务员
                if (CollUtil.isNotEmpty(selectSupplierName)) {
                    for (Map<String, Object> map : selectSupplierName) {
                        for (SupplierDealOutData outDatum : outData) {
                            if (Objects.equals(Convert.toStr(map.get("client")), outDatum.getCLIENT()) && Objects.equals(Convert.toStr(map.get("supSite")), outDatum.getSiteCode()) && Objects.equals(Convert.toStr(map.get("supSelfCode")), outDatum.getSupCode()) && Objects.equals(Convert.toStr(map.get("gssCode")), outDatum.getPoSupplierSalesman())) {
                                outDatum.setPoSupplierSalesmanName(Convert.toStr(map.get("gssName")));
                                list.add(outDatum);
                            }
                        }
                    }
                }
            } else {
                if (CollUtil.isNotEmpty(selectSupplierName)) {
                    for (Map<String, Object> map : selectSupplierName) {
                        for (SupplierDealOutData outDatum : outData) {
                            if (Objects.equals(Convert.toStr(map.get("client")), outDatum.getCLIENT()) && Objects.equals(Convert.toStr(map.get("supSite")), outDatum.getSiteCode()) && Objects.equals(Convert.toStr(map.get("supSelfCode")), outDatum.getSupCode()) && Objects.equals(Convert.toStr(map.get("gssCode")), outDatum.getPoSupplierSalesman())) {
                                outDatum.setPoSupplierSalesmanName(Convert.toStr(map.get("gssName")));
                            }
                        }
                    }
                }
                list = outData;
            }
        }

//        if(StringUtils.isEmpty(inData.getSiteCode()) &&  StringUtils.isEmpty(inData.getStoCode())){
//            throw new BusinessException("请输入入库地点或门店！");
//        }
//        if(StringUtils.isNotEmpty(inData.getSiteCode()) &&  StringUtils.isNotEmpty(inData.getStoCode())){
//            throw new BusinessException("不能同时输入入库地点和门店！");
//        }


        PageInfo pageInfo;

        if (ObjectUtil.isNotEmpty(list)) {
            //开单金额
            BigDecimal billingAmount =BigDecimal.ZERO;
            //税金
            BigDecimal rateBat =BigDecimal.ZERO;
            //含税成本额
            BigDecimal totalAmount =BigDecimal.ZERO;
            //去税成本额
            BigDecimal batAmt =BigDecimal.ZERO;

            for (SupplierDealOutData supplierDealOutData : list) {
                billingAmount = billingAmount.add(supplierDealOutData.getBillingAmount().setScale(4,BigDecimal.ROUND_HALF_UP));
                rateBat = rateBat.add(supplierDealOutData.getRateBat().setScale(4,BigDecimal.ROUND_HALF_UP));
                totalAmount = totalAmount.add(supplierDealOutData.getTotalAmount().setScale(4,BigDecimal.ROUND_HALF_UP));
                batAmt = batAmt.add(supplierDealOutData.getBatAmt().setScale(4,BigDecimal.ROUND_HALF_UP));

            }
            SupplierDealOutTotal outTotal = new SupplierDealOutTotal();
            outTotal.setBillingAmount(billingAmount);
            outTotal.setRateBat(rateBat);
            outTotal.setTotalAmount(totalAmount);
            outTotal.setBatAmt(batAmt);

            pageInfo = new PageInfo(list, outTotal);
        } else {
            pageInfo = new PageInfo(list);
        }

        return pageInfo;
    }


//    @Override
//    public void selectSupplierDealExcel(HttpServletResponse response,SupplierDealInData inData) {
//        List<SupplierDealOutData> outData = this.materialDocMapper.selectSupplierDealPageBySiteCode(inData);
//        if(ObjectUtil.isNotEmpty(outData)) {
////                response.setContentType("application/vnd.ms-excel");
////                response.setCharacterEncoding("utf-8");
//            EasyExcelUtil.simpleWrite("供应商单据查询", SupplierDealOutData.class, outData);
//
//        }

//    }


    @Override
    public PageInfo<SupplierDealOutData> selectSupplierDealDetailPage(SupplierDealInData inData) {

        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if ((ObjectUtil.isEmpty(inData.getStartDate()) || ObjectUtil.isEmpty(inData.getEndDate())) && StringUtils.isEmpty(inData.getDnId())) {
            throw new BusinessException("日期或单号不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getProCode())) {
            inData.setProArr(inData.getProCode().split("\\s+ |\\s+|,"));
        }
        List<SupplierDealOutData> outData = this.materialDocMapper.selectSupplierDealDetailPage(inData);
//        List<SupplierDealOutData> outData = this.materialDocMapper.selectSupplierDealDetailPageBySiteCode(inData);
//        if(StringUtils.isEmpty(inData.getSiteCode()) &&  StringUtils.isEmpty(inData.getStoCode())){
//            throw new BusinessException("请输入入库地点或门店！");
//        }
//        if(StringUtils.isNotEmpty(inData.getSiteCode()) &&  StringUtils.isNotEmpty(inData.getStoCode())){
//            throw new BusinessException("不能同时输入入库地点和门店！");
//        }
        List<SupplierDealOutData> list = new ArrayList<>();
        if (CollUtil.isNotEmpty(outData)) {
            //查询业务员
            List<Map<String, Object>> selectSupplierName = this.materialDocMapper.selectSupplierName(inData);
            if (StrUtil.isNotBlank(inData.getPoSupplierSalesman())) {

                if (CollUtil.isNotEmpty(selectSupplierName)) {
                    for (Map<String, Object> map : selectSupplierName) {
                        for (SupplierDealOutData outDatum : outData) {
                            if (Objects.equals(Convert.toStr(map.get("client")), outDatum.getCLIENT()) && Objects.equals(Convert.toStr(map.get("supSite")), outDatum.getSiteCode()) && Objects.equals(Convert.toStr(map.get("supSelfCode")), outDatum.getSupCode()) && Objects.equals(Convert.toStr(map.get("gssCode")), outDatum.getPoSupplierSalesman())) {
                                outDatum.setPoSupplierSalesmanName(Convert.toStr(map.get("gssName")));
                                list.add(outDatum);
                            }
                        }
                    }
                }
            } else {
                if (CollUtil.isNotEmpty(selectSupplierName)) {
                    for (Map<String, Object> map : selectSupplierName) {
                        for (SupplierDealOutData outDatum : outData) {
                            if (Objects.equals(Convert.toStr(map.get("client")), outDatum.getCLIENT()) && Objects.equals(Convert.toStr(map.get("supSite")), outDatum.getSiteCode()) && Objects.equals(Convert.toStr(map.get("supSelfCode")), outDatum.getSupCode()) && Objects.equals(Convert.toStr(map.get("gssCode")), outDatum.getPoSupplierSalesman())) {
                                outDatum.setPoSupplierSalesmanName(Convert.toStr(map.get("gssName")));
                            }
                        }
                    }
                }
                list = outData;
            }
        }
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(list)) {
            //开单金额
            BigDecimal billingAmount =BigDecimal.ZERO;
            //税金
            BigDecimal rateBat =BigDecimal.ZERO;
            //含税成本额
            BigDecimal totalAmount =BigDecimal.ZERO;
            //去税成本额
            BigDecimal batAmt =BigDecimal.ZERO;
            //数量
            BigDecimal qty =BigDecimal.ZERO;
            for (SupplierDealOutData supplierDealOutData : list) {
                billingAmount = billingAmount.add(supplierDealOutData.getBillingAmount().setScale(4,BigDecimal.ROUND_HALF_UP));
                rateBat = rateBat.add(supplierDealOutData.getRateBat().setScale(4,BigDecimal.ROUND_HALF_UP));
                totalAmount = totalAmount.add(supplierDealOutData.getTotalAmount().setScale(4,BigDecimal.ROUND_HALF_UP));
                batAmt = batAmt.add(supplierDealOutData.getBatAmt().setScale(4,BigDecimal.ROUND_HALF_UP));
                qty = qty.add(supplierDealOutData.getQty());
            }
            SupplierDealOutTotal outTotal = new SupplierDealOutTotal();
            outTotal.setBillingAmount(billingAmount);
            outTotal.setRateBat(rateBat);
            outTotal.setTotalAmount(totalAmount);
            outTotal.setBatAmt(batAmt);
            outTotal.setQty(qty);
            //SupplierDealOutTotal outTotal = this.materialDocMapper.selectSupplierDealDetailByTotal(inData);
            pageInfo = new PageInfo(list, outTotal);
        } else {
            pageInfo = new PageInfo(list);
        }
        return pageInfo;
    }

    @Override
    public PageInfo<WholesaleSaleOutData> selectWholesaleSalePage(WholesaleSaleInData inData) {
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("单据号或起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("单据号或结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getProCodes())) {
            inData.setProArr(inData.getProCodes().split("\\s+ |\\s+|,"));
        }
        /*if (StringUtils.isNotEmpty(inData.getMatType())) {
            if (inData.getMatType().equals("XD")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD")));
            } else if (inData.getMatType().equals("ED")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("ED")));
            } else if (inData.getMatType().equals("PD")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("PD", "ND", "PX")));
            } else if (inData.getMatType().equals("TD")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("TD", "MD")));
            } else if (inData.getMatType().equals("PX")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("PX")));
            }
        } else {
            inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD", "PD", "ND", "ED", "TD", "MD", "PX")));
        }*/
        if (CollUtil.isNotEmpty(inData.getMatTypes())) {
            List<String> matTypes = inData.getMatTypes();
            for (int i = 0; i < matTypes.size(); i++) {
                if (matTypes.get(i).equals("PD")) {
                    matTypes.add("PX");
                }
            }
        }else {
            inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD", "PD", "ND", "ED", "TD", "MD", "PX")));
        }
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        List<WholesaleSaleOutData> outData = this.materialDocMapper.selectWholesaleSalePage(inData);

        //新增销售员、抬头备注字段
        //销售员取值:GAIA_WHOLESALE_SALESMAN表GSS_NAME字段 用CLIENT+GSS_CODE关联
        //GSS_CODE取值:GAIA_SO_HEADER表GWS_CODE字段
        List<WholesaleSaleOutData> salemanList=this.materialDocMapper.selectWholesaleSalemanList(inData);
        for (WholesaleSaleOutData out : outData) {
            for (WholesaleSaleOutData saleOutData : salemanList) {
                if (out.getDnId().equals(saleOutData.getDnId())
                        && out.getMatType().equals(saleOutData.getMatType())) {
                    out.setGwoOwnerSaleMan(saleOutData.getGwoOwnerSaleMan());
                    out.setGwoOwnerSaleManUserId(saleOutData.getGwoOwnerSaleManUserId());
                    out.setSoHeaderRemark(saleOutData.getSoHeaderRemark());
                }
            }
        }
        if (StrUtil.isNotEmpty(inData.getGwoOwnerSaleMan())) {
            outData=outData.stream().filter(out->{
                return org.apache.commons.lang3.StringUtils.isNotEmpty(out.getGwoOwnerSaleManUserId())
                        && out.getGwoOwnerSaleManUserId().equals(inData.getGwoOwnerSaleMan());
            }).collect(Collectors.toList());
        }
        if (StrUtil.isNotEmpty(inData.getSoHeaderRemark())) {
            outData=outData.stream().filter(out->{
                return org.apache.commons.lang3.StringUtils.isNotEmpty(out.getSoHeaderRemark())
                        && out.getSoHeaderRemark().equals(inData.getSoHeaderRemark());
            }).collect(Collectors.toList());
        }

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            WholesaleSaleOutTotal outNum = new WholesaleSaleOutTotal();
            for (WholesaleSaleOutData out : outData) {
                outNum.setQty(CommonUtil.stripTrailingZeros(outNum.getQty()).add(CommonUtil.stripTrailingZeros(out.getQty())));
                outNum.setBatAmt(CommonUtil.stripTrailingZeros(outNum.getBatAmt()).add(CommonUtil.stripTrailingZeros(out.getBatAmt())));
                outNum.setRateBat(CommonUtil.stripTrailingZeros(outNum.getRateBat()).add(CommonUtil.stripTrailingZeros(out.getRateBat())));
                outNum.setTotalAmount(CommonUtil.stripTrailingZeros(outNum.getTotalAmount()).add(CommonUtil.stripTrailingZeros(out.getTotalAmount())));
                outNum.setAddAmt(CommonUtil.stripTrailingZeros(outNum.getAddAmt()).add(CommonUtil.stripTrailingZeros(out.getAddAmt())));
                outNum.setAddTax(CommonUtil.stripTrailingZeros(outNum.getAddTax()).add(CommonUtil.stripTrailingZeros(out.getAddTax())));
                outNum.setAddtotalAmount(CommonUtil.stripTrailingZeros(outNum.getAddtotalAmount()).add(CommonUtil.stripTrailingZeros(out.getAddtotalAmount())));
                outNum.setMovAmt(CommonUtil.stripTrailingZeros(outNum.getMovAmt()).add(CommonUtil.stripTrailingZeros(out.getMovAmt())));
                outNum.setRateMov(CommonUtil.stripTrailingZeros(outNum.getRateMov()).add(CommonUtil.stripTrailingZeros(out.getRateMov())));
                outNum.setMovTotalAmount(CommonUtil.stripTrailingZeros(outNum.getMovTotalAmount()).add(CommonUtil.stripTrailingZeros(out.getMovTotalAmount())));

            }
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(outNum);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }


    @Override
    public void selectWholesaleSalePageExport(HttpServletResponse response, WholesaleSaleInData data) {
        PageInfo pageInfo = this.selectWholesaleSalePage(data);
        List<WholesaleSaleOutData> outDataList = pageInfo.getList();
        WholesaleSaleOutTotal total = (WholesaleSaleOutTotal) pageInfo.getListNum();
        if (CollUtil.isEmpty(outDataList)) {
            throw new BusinessException("导出数据不能为空");
        }
        List<WholesaleSaleSummaryExportOutData> dataList = outDataList.stream().map(outData -> {
            WholesaleSaleSummaryExportOutData exportOutData = new WholesaleSaleSummaryExportOutData();
            BeanUtil.copyProperties(outData, exportOutData);
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(outData.getGwoOwnerSaleMan())) {
                exportOutData.setGwoOwnerSaleMan(outData.getGwoOwnerSaleManUserId()+"-"+outData.getGwoOwnerSaleMan());
            }
            exportOutData.setAddAmt(outData.getAddAmt() == null?BigDecimal.ZERO.setScale(4):outData.getAddAmt().setScale(4,RoundingMode.HALF_UP));
            exportOutData.setAddTax(outData.getAddTax() == null?BigDecimal.ZERO.setScale(4):outData.getAddTax().setScale(4,RoundingMode.HALF_UP));
            exportOutData.setAddtotalAmount(outData.getAddtotalAmount() == null?BigDecimal.ZERO.setScale(2):outData.getAddtotalAmount().setScale(2,RoundingMode.HALF_UP));
            return exportOutData;
        }).collect(Collectors.toList());
        WholesaleSaleSummaryExportOutData totalData = new WholesaleSaleSummaryExportOutData();
        BeanUtil.copyProperties(total, totalData);
        dataList.add(totalData);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = "配送单据导出";
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

        try {
            EasyExcel.write(response.getOutputStream(),WholesaleSaleSummaryExportOutData.class)
                    .registerWriteHandler(ExcelStyleUtil.getStyle())
                    .sheet(0).doWrite(dataList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<SupplierDealOutDataCSV> exportSupplierDealDetailPage(SupplierDealInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if ((ObjectUtil.isEmpty(inData.getStartDate()) || ObjectUtil.isEmpty(inData.getEndDate())) && StringUtils.isEmpty(inData.getDnId())) {
            throw new BusinessException("日期或单号不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getProCode())) {
            inData.setProArr(inData.getProCode().split("\\s+ |\\s+|,"));
        }
        List<SupplierDealOutData> outData = this.materialDocMapper.selectSupplierDealDetailPage(inData);
        List<SupplierDealOutData> list = new ArrayList<>();
        if (CollUtil.isNotEmpty(outData)) {
            //查询业务员
            List<Map<String, Object>> selectSupplierName = this.materialDocMapper.selectSupplierName(inData);
            if (StrUtil.isNotBlank(inData.getPoSupplierSalesman())) {

                if (CollUtil.isNotEmpty(selectSupplierName)) {
                    for (Map<String, Object> map : selectSupplierName) {
                        for (SupplierDealOutData outDatum : outData) {
                            if (Objects.equals(Convert.toStr(map.get("client")), outDatum.getCLIENT()) && Objects.equals(Convert.toStr(map.get("supSite")), outDatum.getSiteCode()) && Objects.equals(Convert.toStr(map.get("supSelfCode")), outDatum.getSupCode()) && Objects.equals(Convert.toStr(map.get("gssCode")), outDatum.getPoSupplierSalesman())) {
                                outDatum.setPoSupplierSalesmanName(Convert.toStr(map.get("gssName")));
                                list.add(outDatum);
                            }
                        }
                    }
                }
            } else {
                if (CollUtil.isNotEmpty(selectSupplierName)) {
                    for (Map<String, Object> map : selectSupplierName) {
                        for (SupplierDealOutData outDatum : outData) {
                            if (Objects.equals(Convert.toStr(map.get("client")), outDatum.getCLIENT()) && Objects.equals(Convert.toStr(map.get("supSite")), outDatum.getSiteCode()) && Objects.equals(Convert.toStr(map.get("supSelfCode")), outDatum.getSupCode()) && Objects.equals(Convert.toStr(map.get("gssCode")), outDatum.getPoSupplierSalesman())) {
                                outDatum.setPoSupplierSalesmanName(Convert.toStr(map.get("gssName")));
                            }
                        }
                    }
                }
                list = outData;
            }
        }
        ArrayList<SupplierDealOutDataCSV> supplierDealOutDataCSVS = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(list)) {
            //开单金额
            BigDecimal billingAmount =BigDecimal.ZERO;
            //税金
            BigDecimal rateBat =BigDecimal.ZERO;
            //含税成本额
            BigDecimal totalAmount =BigDecimal.ZERO;
            //去税成本额
            BigDecimal batAmt =BigDecimal.ZERO;
            //数量
            BigDecimal qty =BigDecimal.ZERO;
            Integer index = 1;
            for (SupplierDealOutData supplierDealOutData : list) {
                billingAmount = billingAmount.add(supplierDealOutData.getBillingAmount().setScale(4,BigDecimal.ROUND_HALF_UP));
                rateBat = rateBat.add(supplierDealOutData.getRateBat().setScale(4,BigDecimal.ROUND_HALF_UP));
                totalAmount = totalAmount.add(supplierDealOutData.getTotalAmount().setScale(4,BigDecimal.ROUND_HALF_UP));
                batAmt = batAmt.add(supplierDealOutData.getBatAmt().setScale(4,BigDecimal.ROUND_HALF_UP));
                qty = qty.add(supplierDealOutData.getQty());
                SupplierDealOutDataCSV supplierDealOutDataCSV = new SupplierDealOutDataCSV();
                BeanUtils.copyProperties(supplierDealOutData,supplierDealOutDataCSV);
                String userCode = StringUtils.isEmpty(supplierDealOutData.getUserCode()) ? "" : supplierDealOutData.getUserCode();
                String userName = StringUtils.isEmpty(supplierDealOutData.getUserName()) ? "" : supplierDealOutData.getUserName();
                String manCode = StringUtils.isEmpty(supplierDealOutData.getPoSupplierSalesman()) ? "" : supplierDealOutData.getPoSupplierSalesman();
                String manName = StringUtils.isEmpty(supplierDealOutData.getPoSupplierSalesmanName()) ? "" : supplierDealOutData.getPoSupplierSalesmanName();
                supplierDealOutDataCSV.setUeserInfo(userCode + userName);
                supplierDealOutDataCSV.setPoSupplierSalesmanInfo(manCode + manName);
                supplierDealOutDataCSV.setIndex(index.toString());
                index++;
                supplierDealOutDataCSVS.add(supplierDealOutDataCSV);
            }

            SupplierDealOutTotal outTotal = new SupplierDealOutTotal();
            outTotal.setBillingAmount(billingAmount);
            outTotal.setRateBat(rateBat);
            outTotal.setTotalAmount(totalAmount);
            outTotal.setBatAmt(batAmt);
            outTotal.setQty(qty);
            SupplierDealOutDataCSV supplierDealOutDataCSV = new SupplierDealOutDataCSV();
            BeanUtils.copyProperties(outTotal,supplierDealOutDataCSV);
            supplierDealOutDataCSV.setIndex("合计");
            supplierDealOutDataCSVS.add(supplierDealOutDataCSV);
        }
        return supplierDealOutDataCSVS;
    }

    @Override
    public List<GetPayTypeOutData> payTypeListByClient(GetPayInData inData) {
        List<GetPayTypeOutData> getPayTypeOutData = paymentMethodMapper.payTypeListByClient(inData);
        ArrayList<GetPayTypeOutData> getPayTypeOutData1 = new ArrayList<>();
        for (GetPayTypeOutData getPayTypeOutDatum : getPayTypeOutData) {
            getPayTypeOutDatum.setIsUpdate("1");
            getPayTypeOutData1.add(getPayTypeOutDatum);
            getPayTypeOutData1.add(new GetPayTypeOutData("sys"+ getPayTypeOutDatum.getGspmKey(),getPayTypeOutDatum.getGspmName() + "系统金额","0"));
            getPayTypeOutData1.add(new GetPayTypeOutData("diff" + getPayTypeOutDatum.getGspmKey(),getPayTypeOutDatum.getGspmName() +  "差异","0"));
        }
        return getPayTypeOutData1;
    }

    @Override
    @Transactional
    public void updatePayTypeListValue(String client, List<HashMap<String, String>> updateList) {
        if (ObjectUtil.isEmpty(updateList)){
            throw new BusinessException("不存在修改内容！");
        }
        // 查出所有支付方式，挨个修改内容
        GetPayInData getPayInData = new GetPayInData();
        getPayInData.setClientId(client);
        getPayInData.setType("1");
        List<GetPayTypeOutData> getPayTypeOutData = paymentMethodMapper.payTypeListByClient(getPayInData);
        for (HashMap<String, String> updateMap : updateList) {
            if (!updateMap.containsKey("voucherId")){
                throw new BusinessException("修改内容必须存在单号！");
            }
            for (GetPayTypeOutData getPayTypeOutDatum : getPayTypeOutData) {
                String gspmKey = getPayTypeOutDatum.getGspmKey().substring(1);          // 返回的支付方式在开头拼接了 "s"，修改的时候去掉
                BigDecimal changeValue = new BigDecimal(updateMap.get(getPayTypeOutDatum.getGspmKey()));
                changeValue.setScale(4,BigDecimal.ROUND_HALF_UP);
                payDayreportDMapper.updatePayTypeValue(client,updateMap.get("voucherId"),updateMap.get("stoCode"),gspmKey,changeValue);
            }
        }
    }

    @Override
    public PageInfo<WholesaleSaleOutData> selectWholesaleSaleDetailPage(WholesaleSaleInData inData) {
        //输入单号可以不输入时间
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("单据号或起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("单据号或结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getProCodes())) {
            inData.setProArr(inData.getProCodes().split("\\s+ |\\s+|,"));
        }
        /*if (StringUtils.isNotEmpty(inData.getMatType())) {
            if (inData.getMatType().equals("XD")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD")));
            } else if (inData.getMatType().equals("ED")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("ED")));
            } else if (inData.getMatType().equals("PD")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("PD", "ND")));
            } else if (inData.getMatType().equals("TD")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("TD", "MD")));
            } else if (inData.getMatType().equals("PX")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("PX")));
            }
        } else {
            inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD", "PD", "ND", "ED", "TD", "MD", "PX")));
        }*/
        if (CollUtil.isEmpty(inData.getMatTypes())) {
            inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD", "PD", "ND", "ED", "TD", "MD", "PX")));
        }
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        List<WholesaleSaleOutData> outData = this.materialDocMapper.selectWholesaleSaleDetailPage(inData);
        //新增销售员、抬头备注字段
        //销售员取值:GAIA_WHOLESALE_SALESMAN表GSS_NAME字段 用CLIENT+GSS_CODE关联
        //GSS_CODE取值:GAIA_SO_HEADER表GWS_CODE字段
        List<WholesaleSaleOutData> salemanList=this.materialDocMapper.selectWholesaleDetailSalemanList(inData);
        for (WholesaleSaleOutData out : outData) {
            for (WholesaleSaleOutData saleOutData : salemanList) {
                if (out.getPostDate().equals(saleOutData.getPostDate())
                        && out.getProCode().equals(saleOutData.getProCode())
                        && out.getDnId().equals(saleOutData.getDnId())
                        && out.getMatType().equals(saleOutData.getMatType())) {
                    out.setGwoOwnerSaleMan(saleOutData.getGwoOwnerSaleMan());
                    out.setGwoOwnerSaleManUserId(saleOutData.getGwoOwnerSaleManUserId());
                    out.setSoHeaderRemark(saleOutData.getSoHeaderRemark());
                }
            }
        }
        if (StrUtil.isNotEmpty(inData.getGwoOwnerSaleMan())) {
            outData=outData.stream().filter(out->{
                return org.apache.commons.lang3.StringUtils.isNotEmpty(out.getGwoOwnerSaleManUserId())
                        && out.getGwoOwnerSaleManUserId().equals(inData.getGwoOwnerSaleMan());
            }).collect(Collectors.toList());
        }
        if (StrUtil.isNotEmpty(inData.getSoHeaderRemark())) {
            outData=outData.stream().filter(out->{
                return org.apache.commons.lang3.StringUtils.isNotEmpty(out.getSoHeaderRemark())
                       && out.getSoHeaderRemark().equals(inData.getSoHeaderRemark());
            }).collect(Collectors.toList());
        }
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            WholesaleSaleOutTotal outNum = new WholesaleSaleOutTotal();
            for (WholesaleSaleOutData out : outData) {
                outNum.setQty(CommonUtil.stripTrailingZeros(outNum.getQty()).add(CommonUtil.stripTrailingZeros(out.getQty())));
                outNum.setBatAmt(CommonUtil.stripTrailingZeros(outNum.getBatAmt()).add(CommonUtil.stripTrailingZeros(out.getBatAmt())));
                outNum.setRateBat(CommonUtil.stripTrailingZeros(outNum.getRateBat()).add(CommonUtil.stripTrailingZeros(out.getRateBat())));
                outNum.setTotalAmount(CommonUtil.stripTrailingZeros(outNum.getTotalAmount()).add(CommonUtil.stripTrailingZeros(out.getTotalAmount())));
                outNum.setAddAmt(CommonUtil.stripTrailingZeros(outNum.getAddAmt()).add(CommonUtil.stripTrailingZeros(out.getAddAmt())));
                outNum.setAddTax(CommonUtil.stripTrailingZeros(outNum.getAddTax()).add(CommonUtil.stripTrailingZeros(out.getAddTax())));
                outNum.setAddtotalAmount(CommonUtil.stripTrailingZeros(outNum.getAddtotalAmount()).add(CommonUtil.stripTrailingZeros(out.getAddtotalAmount())));
                outNum.setMovAmt(CommonUtil.stripTrailingZeros(outNum.getMovAmt()).add(CommonUtil.stripTrailingZeros(out.getMovAmt())));
                outNum.setRateMov(CommonUtil.stripTrailingZeros(outNum.getRateMov()).add(CommonUtil.stripTrailingZeros(out.getRateMov())));
                outNum.setMovTotalAmount(CommonUtil.stripTrailingZeros(outNum.getMovTotalAmount()).add(CommonUtil.stripTrailingZeros(out.getMovTotalAmount())));

            }
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(outNum);
        } else {
            pageInfo = new PageInfo();
        }


        return pageInfo;
    }


    @Override
    public void selectWholesaleSaleDetailPageExport(HttpServletResponse response, WholesaleSaleInData data) {
        PageInfo pageInfo = this.selectWholesaleSaleDetailPage(data);
        List<WholesaleSaleOutData> outDataList = pageInfo.getList();
        WholesaleSaleOutTotal total = (WholesaleSaleOutTotal) pageInfo.getListNum();
        if (CollUtil.isEmpty(outDataList)) {
            throw new BusinessException("导出数据不能为空");
        }
        List<WholesaleSaleDetailExportOutData> dataList = outDataList.stream().map(outData -> {
            WholesaleSaleDetailExportOutData exportOutData = new WholesaleSaleDetailExportOutData();
            BeanUtil.copyProperties(outData, exportOutData);
            if (org.apache.commons.lang3.StringUtils.isNotEmpty(outData.getGwoOwnerSaleMan())) {
                exportOutData.setGwoOwnerSaleMan(outData.getGwoOwnerSaleManUserId()+"-"+outData.getGwoOwnerSaleMan());
            }
            exportOutData.setAddAmt(outData.getAddAmt() != null ? outData.getAddAmt().setScale(4, RoundingMode.HALF_UP) : BigDecimal.ZERO);
            exportOutData.setAddTax(outData.getAddTax() != null ? outData.getAddTax().setScale(4, RoundingMode.HALF_UP) : BigDecimal.ZERO);
            exportOutData.setAddtotalAmount(outData.getAddtotalAmount() != null ? outData.getAddtotalAmount().setScale(2, RoundingMode.HALF_UP) : BigDecimal.ZERO);
            return exportOutData;
        }).collect(Collectors.toList());
        WholesaleSaleDetailExportOutData totalData = new WholesaleSaleDetailExportOutData();
        BeanUtil.copyProperties(total, totalData);
        dataList.add(totalData);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = "配送单据明细导出";
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        try {
            EasyExcel.write(response.getOutputStream(), WholesaleSaleDetailExportOutData.class)
                    .registerWriteHandler(ExcelStyleUtil.getStyle())
                    .sheet(0).doWrite(dataList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public PageInfo<BusinessDocumentOutData> selectBusinessDocumentPage(BusinessDocumentInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("单据号或起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("单据号或结束日期不能为空！");
        }

        List<BusinessDocumentOutData> outData = this.materialDocMapper.selectBusinessDocumentPage(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            BusinessDocumentOutTotal outTotal = this.materialDocMapper.selectBusinessDocumentTotal(inData);

            pageInfo = new PageInfo(outData, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo<BusinessDocumentOutData> selectBusinessDocumentDetailPage(BusinessDocumentInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("单据号或起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("单据号或结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getProCodes())) {
            inData.setProArr(inData.getProCodes().split("\\s+ |\\s+|,"));
        }
        List<BusinessDocumentOutData> outData = new ArrayList<>();
//            if(inData.getMatType().equals("BD") || inData.getMatType().equals("ZD")){
        //报损
        List<BusinessDocumentOutData> bdStore = new ArrayList<>();
        List<BusinessDocumentOutData> bdDc = new ArrayList<>();
        //当同时为空或者有值的时候  都查
//                if((StringUtils.isNotEmpty(inData.getStore()) && StringUtils.isNotEmpty(inData.getDc())) ||
//                        (StringUtils.isEmpty(inData.getStore()) && StringUtils.isEmpty(inData.getDc()))){
//                }else if(StringUtils.isNotEmpty(inData.getStore())){ //报损  门店
//
//                }else if(StringUtils.isNotEmpty(inData.getDc())){  //报损  配送中心
//                }
        outData = this.materialDocMapper.selectBusinessDocumentDetailPage(inData);
//            }
//            if(inData.getMatType().equals("ZD")){
//                //自用
//                List<BusinessDocumentOutData> zdStore = new ArrayList<>();
//                List<BusinessDocumentOutData> zdDc = new ArrayList<>();
//                //当同时为空或者有值的时候  都查
//                if((StringUtils.isNotEmpty(inData.getStore()) && StringUtils.isNotEmpty(inData.getDc())) ||
//                        (StringUtils.isEmpty(inData.getStore()) && StringUtils.isEmpty(inData.getDc()))){
//                    zdStore = this.materialDocMapper.selectByZdStore(inData);
//                    zdDc = this.materialDocMapper.selectByZdDc(inData);
//                }else if(StringUtils.isNotEmpty(inData.getStore())){  //自用  门店
//                    zdStore = this.materialDocMapper.selectByZdStore(inData);
//                }else if(StringUtils.isNotEmpty(inData.getDc())){      //自用  配送中心
//                    zdDc = this.materialDocMapper.selectByZdDc(inData);
//                }
//                outData.addAll(zdStore);
//                outData.addAll(zdDc);
//            }
//           else if(inData.getMatType() .equals("DD") ){
//                //盘点 remark 为空
//                List<BusinessDocumentOutData>  dd = this.materialDocMapper.selectByDd(inData);
//                outData.addAll(dd);
//            }
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            BusinessDocumentOutTotal outTotal = this.materialDocMapper.selectBusinessDocumentDetailTotal(inData);

            pageInfo = new PageInfo(outData, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo<StoreRateSellOutData> selectStoreRateSellDetailPage(StoreRateSellInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        List<StoreRateSellOutData> outData = this.saleDMapper.selectStoreRateSellDetailPage(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            StoreRateSellOutData outNum = new StoreRateSellOutData();
            for (StoreRateSellOutData out : outData) {
                outNum.setQty(CommonUtil.stripTrailingZeros(outNum.getQty()).add(CommonUtil.stripTrailingZeros(out.getQty())));
                outNum.setAmountReceivable(CommonUtil.stripTrailingZeros(outNum.getAmountReceivable()).add(CommonUtil.stripTrailingZeros(out.getAmountReceivable())));
                outNum.setAmt(CommonUtil.stripTrailingZeros(outNum.getAmt()).add(CommonUtil.stripTrailingZeros(out.getAmt())));
                outNum.setDeduction(CommonUtil.stripTrailingZeros(outNum.getDeduction()).add(CommonUtil.stripTrailingZeros(out.getDeduction())));
                outNum.setRemoveTaxSale(CommonUtil.stripTrailingZeros(outNum.getRemoveTaxSale()).add(CommonUtil.stripTrailingZeros(out.getRemoveTaxSale())));
                outNum.setMovPrices(CommonUtil.stripTrailingZeros(outNum.getMovPrices()).add(CommonUtil.stripTrailingZeros(out.getMovPrices())));
                outNum.setIncludeTaxSale(CommonUtil.stripTrailingZeros(outNum.getIncludeTaxSale()).add(CommonUtil.stripTrailingZeros(out.getIncludeTaxSale())));
                outNum.setGrossProfitMargin(CommonUtil.stripTrailingZeros(outNum.getGrossProfitMargin()).add(CommonUtil.stripTrailingZeros(out.getGrossProfitMargin())));
                outNum.setAddMovPrices(CommonUtil.stripTrailingZeros(outNum.getAddMovPrices()).add(CommonUtil.stripTrailingZeros(out.getAddMovPrices())));
                outNum.setAddIncludeTaxSale(CommonUtil.stripTrailingZeros(outNum.getAddIncludeTaxSale()).add(CommonUtil.stripTrailingZeros(out.getAddIncludeTaxSale())));
                outNum.setAddGrossProfitMargin(CommonUtil.stripTrailingZeros(outNum.getAddGrossProfitMargin()).add(CommonUtil.stripTrailingZeros(out.getAddGrossProfitMargin())));
            }
            DecimalFormat df = new DecimalFormat("0.00%");
            outNum.setGrossProfitRate(df.format(outNum.getGrossProfitMargin().divide(outNum.getAmt(), BigDecimal.ROUND_HALF_EVEN)));
            outNum.setAddGrossProfitRate(df.format(outNum.getAddGrossProfitMargin().divide(outNum.getAmt(), BigDecimal.ROUND_HALF_EVEN)));
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(outNum);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo<StoreRateSellOutData> selectStoreRateSellPage(StoreRateSellInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        List<StoreRateSellOutData> outData = this.saleDMapper.selectStoreRateSellPage(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            StoreRateSellOutData outNum = new StoreRateSellOutData();
            for (StoreRateSellOutData out : outData) {
                outNum.setQty(CommonUtil.stripTrailingZeros(outNum.getQty()).add(CommonUtil.stripTrailingZeros(out.getQty())));
                outNum.setAmountReceivable(CommonUtil.stripTrailingZeros(outNum.getAmountReceivable()).add(CommonUtil.stripTrailingZeros(out.getAmountReceivable())));
                outNum.setAmt(CommonUtil.stripTrailingZeros(outNum.getAmt()).add(CommonUtil.stripTrailingZeros(out.getAmt())));
                outNum.setDeduction(CommonUtil.stripTrailingZeros(outNum.getDeduction()).add(CommonUtil.stripTrailingZeros(out.getDeduction())));
                outNum.setRemoveTaxSale(CommonUtil.stripTrailingZeros(outNum.getRemoveTaxSale()).add(CommonUtil.stripTrailingZeros(out.getRemoveTaxSale())));
                outNum.setMovPrices(CommonUtil.stripTrailingZeros(outNum.getMovPrices()).add(CommonUtil.stripTrailingZeros(out.getMovPrices())));
                outNum.setIncludeTaxSale(CommonUtil.stripTrailingZeros(outNum.getIncludeTaxSale()).add(CommonUtil.stripTrailingZeros(out.getIncludeTaxSale())));
                outNum.setGrossProfitMargin(CommonUtil.stripTrailingZeros(outNum.getGrossProfitMargin()).add(CommonUtil.stripTrailingZeros(out.getGrossProfitMargin())));
                outNum.setAddMovPrices(CommonUtil.stripTrailingZeros(outNum.getAddMovPrices()).add(CommonUtil.stripTrailingZeros(out.getAddMovPrices())));
                outNum.setAddIncludeTaxSale(CommonUtil.stripTrailingZeros(outNum.getAddIncludeTaxSale()).add(CommonUtil.stripTrailingZeros(out.getAddIncludeTaxSale())));
                outNum.setAddGrossProfitMargin(CommonUtil.stripTrailingZeros(outNum.getAddGrossProfitMargin()).add(CommonUtil.stripTrailingZeros(out.getAddGrossProfitMargin())));
            }
            DecimalFormat df = new DecimalFormat("0.00%");
            outNum.setGrossProfitRate(df.format(outNum.getGrossProfitMargin().divide(outNum.getAmt(), BigDecimal.ROUND_HALF_EVEN)));
            outNum.setAddGrossProfitRate(df.format(outNum.getAddGrossProfitMargin().divide(outNum.getAmt(), BigDecimal.ROUND_HALF_EVEN)));
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(outNum);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo selectStoreSaleByDay(StoreSaleDayInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        GetPayInData pay = new GetPayInData();
        pay.setClientId(inData.getClient());
        pay.setType("1");

        //获取支付类型
        List<GetPayTypeOutData> payTypeOutData = payService.payTypeListByClient(pay);
        if (payTypeOutData != null && payTypeOutData.size() > 0) {
            inData.setPayTypeOutData(payTypeOutData);
        }
        //先查询权限   flag  0：不开启  1：开启
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClient(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        inData.setFlag(flag);
        List<Map<String, Object>> outData = this.saleDMapper.selectStoreSaleByDay(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            // 集合列的数据汇总
            Map<String, Object> mapTotal = this.saleDMapper.selectStoreSaleByDayTotal(inData);
            if (ValidateUtil.isNotEmpty(flag)) {
                mapTotal.put("flag", flag);
            } else {
                mapTotal.put("flag", "0");
            }

            pageInfo = new PageInfo(outData, mapTotal);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public PageInfo selectStoreSaleByDayAndClient(StoreSaleDayInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        if(StrUtil.isNotBlank(inData.getStatDatePart())){
            inData.setStatDatePart(inData.getStatDatePart()+"00");
        }
        if(StrUtil.isNotBlank(inData.getEndDatePart())){
            inData.setEndDatePart(inData.getEndDatePart()+"59");
        }
        GetPayInData pay = new GetPayInData();
        pay.setClientId(inData.getClient());
        pay.setType("1");
//        pay.setPayName(inData.getPayName());

        //获取支付类型
        List<GetPayTypeOutData> payTypeOutData = payService.payTypeListByClient(pay);
        if (payTypeOutData != null && payTypeOutData.size() > 0) {
            inData.setPayTypeOutData(payTypeOutData);
        }
        List<Map<String, Object>> outData = this.saleDMapper.selectStoreSaleByDay(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            for (Map<String, Object> item : outData) {
                if(StrUtil.isNotBlank(Convert.toStr(item.get("datePart")))){
                    String hm =  cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.parse(Convert.toStr(item.get("datePart")), "HHmmss"), "HH:mm");
                    item.put("datePart",hm);
                }
            }
            // 集合列的数据汇总
            Map<String, Object> mapTotal = this.saleDMapper.selectStoreSaleByDayTotal(inData);

            pageInfo = new PageInfo(outData, mapTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo selectInventoryChangeSummary(InventoryChangeSummaryInData inData) {

        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getProCode())) {
            inData.setProArr(inData.getProCode().split("\\s+ |\\s+|,"));
        }

        //查询期初数据
        List<InventoryChangeSummaryOutData> startData=getStartData(inData);

        //查询期间数据
        List<InventoryChangeSummaryOutData> distributionAndAdjustAndSaleData = this.materialDocMapper.selectDistributionAndAdjustSummaryData(inData);

        //合并期初、期间数据
        for (InventoryChangeSummaryOutData out : startData) {
            for (InventoryChangeSummaryOutData bet : distributionAndAdjustAndSaleData) {
                if (out.getProCode().equals(bet.getProCode())) {
                    out.setPurchaseAmount(bet.getPurchaseAmount()==null?BigDecimal.ZERO:bet.getPurchaseAmount());
                    out.setReturnAmount(bet.getReturnAmount()==null?BigDecimal.ZERO:bet.getReturnAmount());
                    out.setSaleAmount(bet.getSaleAmount()==null?BigDecimal.ZERO:bet.getSaleAmount());
                    out.setAdjustAmt(bet.getAdjustAmt()==null?BigDecimal.ZERO:bet.getAdjustAmt());
                    out.setDistributionAmt(bet.getDistributionAmt()==null?BigDecimal.ZERO:bet.getDistributionAmt());
                    out.setProfitLossAmount(bet.getProfitLossAmount()==null?BigDecimal.ZERO:bet.getProfitLossAmount());
                    out.setQcAmt(bet.getQcAmt()==null?BigDecimal.ZERO:bet.getQcAmt());

                    out.setPurchaseQty(bet.getPurchaseQty()==null?BigDecimal.ZERO:bet.getPurchaseQty());
                    out.setReturnQty(bet.getReturnQty()==null?BigDecimal.ZERO:bet.getReturnQty());
                    out.setSaleQty(bet.getSaleQty()==null?BigDecimal.ZERO:bet.getSaleQty());
                    out.setAdjustQty(bet.getAdjustQty()==null?BigDecimal.ZERO:bet.getAdjustQty());
                    out.setDistributionQty(bet.getDistributionQty()==null?BigDecimal.ZERO:bet.getDistributionQty());
                    out.setProfitLossQty(bet.getProfitLossQty()==null?BigDecimal.ZERO:bet.getProfitLossQty());
                    out.setQcQty(bet.getQcQty()==null?BigDecimal.ZERO:bet.getQcQty());

                    out.setEndAmt(
                            out.getStartAmt()==null?BigDecimal.ZERO:out.getStartAmt()
                            .add(bet.getPurchaseAmount()==null?BigDecimal.ZERO:bet.getPurchaseAmount())
                            .add(bet.getReturnAmount()==null?BigDecimal.ZERO:bet.getReturnAmount())
                            .add(bet.getSaleAmount()==null?BigDecimal.ZERO:bet.getSaleAmount())
                            .add(bet.getAdjustAmt()==null?BigDecimal.ZERO:bet.getAdjustAmt())
                            .add(bet.getDistributionAmt()==null?BigDecimal.ZERO:bet.getDistributionAmt())
                            .add(bet.getProfitLossAmount()==null?BigDecimal.ZERO:bet.getProfitLossAmount())
                            .add(bet.getQcAmt()==null?BigDecimal.ZERO:bet.getQcAmt())
                            .setScale(2,BigDecimal.ROUND_HALF_UP));
                    out.setEndQty(
                            out.getStartQty()==null?BigDecimal.ZERO:out.getStartQty()
                            .add(bet.getPurchaseQty()==null?BigDecimal.ZERO:bet.getPurchaseQty())
                            .add(bet.getReturnQty()==null?BigDecimal.ZERO:bet.getReturnQty())
                            .add(bet.getSaleQty()==null?BigDecimal.ZERO:bet.getSaleQty())
                            .add(bet.getAdjustQty()==null?BigDecimal.ZERO:bet.getAdjustQty())
                            .add(bet.getDistributionQty()==null?BigDecimal.ZERO:bet.getDistributionQty())
                            .add(bet.getProfitLossQty()==null?BigDecimal.ZERO:bet.getProfitLossQty())
                            .add(bet.getQcQty()==null?BigDecimal.ZERO:bet.getQcQty())
                            .setScale(2,BigDecimal.ROUND_HALF_UP));
                }
            }
        }
        //没有期初，但是有期间
        List<String> proCode = startData.stream().map(InventoryChangeSummaryOutData::getProCode).collect(Collectors.toList());
        distributionAndAdjustAndSaleData.removeIf(bet->{
            return proCode.contains(bet.getProCode());
        });
        for (InventoryChangeSummaryOutData bet : distributionAndAdjustAndSaleData) {
            bet.setStartAmt(BigDecimal.ZERO);
            bet.setStartQty(BigDecimal.ZERO);
            bet.setEndAmt(
                    BigDecimal.ZERO
                            .add(bet.getPurchaseAmount()==null?BigDecimal.ZERO:bet.getPurchaseAmount())
                            .add(bet.getReturnAmount()==null?BigDecimal.ZERO:bet.getReturnAmount())
                            .add(bet.getSaleAmount()==null?BigDecimal.ZERO:bet.getSaleAmount())
                            .add(bet.getAdjustAmt()==null?BigDecimal.ZERO:bet.getAdjustAmt())
                            .add(bet.getDistributionAmt()==null?BigDecimal.ZERO:bet.getDistributionAmt())
                            .add(bet.getProfitLossAmount()==null?BigDecimal.ZERO:bet.getProfitLossAmount())
                            .add(bet.getQcAmt()==null?BigDecimal.ZERO:bet.getQcAmt())
                            .setScale(2,BigDecimal.ROUND_HALF_UP));
            bet.setEndQty(
                    BigDecimal.ZERO
                            .add(bet.getPurchaseQty()==null?BigDecimal.ZERO:bet.getPurchaseQty())
                            .add(bet.getReturnQty()==null?BigDecimal.ZERO:bet.getReturnQty())
                            .add(bet.getSaleQty()==null?BigDecimal.ZERO:bet.getSaleQty())
                            .add(bet.getAdjustQty()==null?BigDecimal.ZERO:bet.getAdjustQty())
                            .add(bet.getDistributionQty()==null?BigDecimal.ZERO:bet.getDistributionQty())
                            .add(bet.getProfitLossQty()==null?BigDecimal.ZERO:bet.getProfitLossQty())
                            .add(bet.getQcQty()==null?BigDecimal.ZERO:bet.getQcQty())
                            .setScale(2,BigDecimal.ROUND_HALF_UP));
        }
        startData.addAll(distributionAndAdjustAndSaleData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(startData)) {
            // 集合列的数据汇总
            InventoryChangeSummaryOutTotal outTotal = new InventoryChangeSummaryOutTotal();
            //计算合计
            outTotal=computeInventoryChangeSummaryTotal(startData,outTotal);
            pageInfo = new PageInfo(startData, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    private List<InventoryChangeSummaryOutData> mergeLastMonthDataAndRestData(List<InventoryChangeSummaryOutData> startData, List<InventoryChangeSummaryOutData> restData) {

        List<String> startCodeList = startData.stream().map(InventoryChangeSummaryOutData::getProCode).collect(Collectors.toList());

        for (InventoryChangeSummaryOutData lastMonth : startData) {
            for (InventoryChangeSummaryOutData rest : restData) {
                if (lastMonth.getProCode().equals(rest.getProCode())) {
                    BeanUtil.copyProperties(rest,lastMonth,"startAmt","startQty");
                }
            }
        }
        //如果没有上个月库存数据但是有月初到开始日期的剩余数据
        for (InventoryChangeSummaryOutData rest : restData) {
            if (!startCodeList.contains(rest.getProCode())) {

                rest.setStartAmt(BigDecimal.ZERO
                    .add(rest.getQcAmt()==null?BigDecimal.ZERO:rest.getQcAmt())
                    .add(rest.getPurchaseAmount()==null?BigDecimal.ZERO:rest.getPurchaseAmount())
                    .add(rest.getReturnAmount()==null?BigDecimal.ZERO:rest.getReturnAmount())
                    .add(rest.getSaleAmount()==null?BigDecimal.ZERO:rest.getSaleAmount())
                    .add(rest.getAdjustAmt()==null?BigDecimal.ZERO:rest.getAdjustAmt())
                    .add(rest.getDistributionAmt()==null?BigDecimal.ZERO:rest.getDistributionAmt())
                    .add(rest.getProfitLossAmount()==null?BigDecimal.ZERO:rest.getProfitLossAmount())
                );
                rest.setStartQty(BigDecimal.ZERO
                        .add(rest.getQcQty()==null?BigDecimal.ZERO:rest.getQcQty())
                        .add(rest.getPurchaseQty()==null?BigDecimal.ZERO:rest.getPurchaseQty())
                        .add(rest.getReturnQty()==null?BigDecimal.ZERO:rest.getReturnQty())
                        .add(rest.getSaleQty()==null?BigDecimal.ZERO:rest.getSaleQty())
                        .add(rest.getAdjustQty()==null?BigDecimal.ZERO:rest.getAdjustQty())
                        .add(rest.getDistributionQty()==null?BigDecimal.ZERO:rest.getDistributionQty())
                        .add(rest.getProfitLossQty()==null?BigDecimal.ZERO:rest.getProfitLossQty())
                );
                startData.add(rest);
            }
        }
        return startData;
    }

    private InventoryChangeSummaryOutTotal computeInventoryChangeSummaryTotal(List<InventoryChangeSummaryOutData> outData, InventoryChangeSummaryOutTotal outTotal) {
        if (ObjectUtil.isNull(outTotal)) {
            outTotal=new InventoryChangeSummaryOutTotal();
        }
        BigDecimal startAmt=BigDecimal.ZERO;
        BigDecimal startQty=BigDecimal.ZERO;
        BigDecimal endAmt=BigDecimal.ZERO;
        BigDecimal endQty=BigDecimal.ZERO;
        BigDecimal adjustAmt=BigDecimal.ZERO;
        BigDecimal adjustQty=BigDecimal.ZERO;
        BigDecimal distributionAmt=BigDecimal.ZERO;
        BigDecimal distributionQty=BigDecimal.ZERO;
        BigDecimal saleAmount=BigDecimal.ZERO;
        BigDecimal saleQty=BigDecimal.ZERO;
        BigDecimal qcAmt=BigDecimal.ZERO;
        BigDecimal qcQty=BigDecimal.ZERO;
        BigDecimal lossAmt=BigDecimal.ZERO;
        BigDecimal lossQty=BigDecimal.ZERO;
        BigDecimal purchaseAmt=BigDecimal.ZERO;
        BigDecimal purchaseQty=BigDecimal.ZERO;
        BigDecimal returnAmt=BigDecimal.ZERO;
        BigDecimal returnQty=BigDecimal.ZERO;
        startAmt = outData.stream().map(InventoryChangeSummaryOutData::getStartAmt).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        startQty = outData.stream().map(InventoryChangeSummaryOutData::getStartQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        adjustAmt = outData.stream().map(InventoryChangeSummaryOutData::getAdjustAmt).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        adjustQty = outData.stream().map(InventoryChangeSummaryOutData::getAdjustQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        distributionAmt = outData.stream().map(InventoryChangeSummaryOutData::getDistributionAmt).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        distributionQty = outData.stream().map(InventoryChangeSummaryOutData::getDistributionQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        saleAmount = outData.stream().map(InventoryChangeSummaryOutData::getSaleAmount).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        saleQty = outData.stream().map(InventoryChangeSummaryOutData::getSaleQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        qcAmt = outData.stream().map(InventoryChangeSummaryOutData::getQcAmt).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        qcQty = outData.stream().map(InventoryChangeSummaryOutData::getQcQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2, BigDecimal.ROUND_HALF_UP);
        lossAmt = outData.stream().map(InventoryChangeSummaryOutData::getProfitLossAmount).filter(Objects::nonNull).reduce(BigDecimal.ZERO,BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
        lossQty = outData.stream().map(InventoryChangeSummaryOutData::getProfitLossQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO,BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
        purchaseAmt = outData.stream().map(InventoryChangeSummaryOutData::getPurchaseAmount).filter(Objects::nonNull).reduce(BigDecimal.ZERO,BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
        purchaseQty = outData.stream().map(InventoryChangeSummaryOutData::getPurchaseQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO,BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
        returnAmt = outData.stream().map(InventoryChangeSummaryOutData::getReturnAmount).filter(Objects::nonNull).reduce(BigDecimal.ZERO,BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
        returnQty = outData.stream().map(InventoryChangeSummaryOutData::getReturnQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO,BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
        endAmt = startAmt
                .add(qcAmt)
                .add(adjustAmt)
                .add(distributionAmt)
                .add(saleAmount)
                .add(lossAmt)
                .add(returnAmt)
                .add(purchaseAmt)
                .setScale(2,BigDecimal.ROUND_HALF_UP);
        endQty = startQty
                .add(qcQty)
                .add(adjustQty)
                .add(distributionQty)
                .add(saleQty)
                .add(lossQty)
                .add(returnQty)
                .add(purchaseQty)
                .setScale(2,BigDecimal.ROUND_HALF_UP);
        outTotal.setStartAmt(startAmt);
        outTotal.setStartQty(startQty);
        outTotal.setPurchaseAmount(purchaseAmt);
        outTotal.setPurchaseQty(purchaseQty);
        outTotal.setReturnAmount(returnAmt);
        outTotal.setReturnQty(returnQty);
        outTotal.setProfitLossAmount(lossAmt);
        outTotal.setProfitLossQty(lossQty);
        outTotal.setEndAmt(endAmt);
        outTotal.setEndQty(endQty);
        outTotal.setAdjustAmt(adjustAmt);
        outTotal.setAdjustQty(adjustQty);
        outTotal.setDistributionAmt(distributionAmt);
        outTotal.setDistributionQty(distributionQty);
        outTotal.setSaleAmount(saleAmount);
        outTotal.setSaleQty(saleQty);
        outTotal.setQcAmt(qcAmt);
        outTotal.setQcQty(qcQty);
        return outTotal;
    }

    /**
     * 查询期初数据
     * @param inData
     * @return
     */
    private List<InventoryChangeSummaryOutData> getStartData(InventoryChangeSummaryInData inData) {

        //门店按月同步 仓库按天同步
        List<InventoryChangeSummaryOutData> stoQcData=new ArrayList<>(); //门店期初数据
        List<InventoryChangeSummaryOutData> dcQcData=new ArrayList<>();  //仓库期初数据
        List<InventoryChangeSummaryOutData> allData=new ArrayList<>();   //所有门店+仓库期初数据
        if (StrUtil.isEmpty(inData.getSiteCode()) && ArrayUtil.isNotEmpty(inData.getSiteArr())) {
            //门店 (期初:物料表的期初加上上个月的库存加上这个月一号到开始日期的期间 合计等于期初 )
            //1.先获取用户输入的开始日期的上一个月的库存数据
            stoQcData= this.materialDocMapper.getStartDataBySto(inData);
            InventoryChangeSummaryInData stoInData=new InventoryChangeSummaryInData();
            //2.计算剩余的日期的中间发生额
            List<InventoryChangeSummaryOutData> restStoData=new ArrayList<>();
            BeanUtil.copyProperties(inData,stoInData);
            //剩余日期开始日期: 开始日期对应月份的一号
            String startDate = cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.beginOfMonth(cn.hutool.core.date.DateUtil.parse(inData.getStartDate(), "yyyyMMdd").toCalendar()).getTime(), "yyyyMMdd");
            //3.剩余日期的发生额加上月末库存等于期初
            if (!startDate.equals(inData.getStartDate())) { //如果剩余日期的开始日期就是1号那么就没有剩余日期
                stoInData.setStartDate(startDate);
                //结束日期 : 开始日期的前一天
                stoInData.setEndDate(cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.offsetDay(cn.hutool.core.date.DateUtil.parse(inData.getStartDate(),"yyyyMMdd"),-1),"yyyyMMdd"));
                restStoData = this.materialDocMapper.selectInventoryChangeSummary(stoInData);
                stoQcData=mergeLastMonthDataAndRestData(stoQcData,restStoData);
            }
            for (InventoryChangeSummaryOutData startData : stoQcData) {
                startData.setEndAmt(startData.getStartAmt()==null?BigDecimal.ZERO:startData.getStartAmt());
                startData.setEndQty(startData.getStartQty()==null?BigDecimal.ZERO:startData.getStartQty());
            }
        }
        //选仓库
        //2.直接取前一天的库存数据作为期初 (期初:物料表的期初加上仓库表的库存乘以单价)
        if (ArrayUtil.isEmpty(inData.getSiteArr()) && StrUtil.isNotEmpty(inData.getSiteCode())) {
            dcQcData= this.materialDocMapper.getStartDataByDc(inData);
            for (InventoryChangeSummaryOutData dcQcAmt : dcQcData) {
                dcQcAmt.setPurchaseQty(BigDecimal.ZERO);
                dcQcAmt.setReturnQty(BigDecimal.ZERO);
                dcQcAmt.setSaleQty(BigDecimal.ZERO);
                dcQcAmt.setProfitLossQty(BigDecimal.ZERO);
                dcQcAmt.setAdjustQty(BigDecimal.ZERO);
                dcQcAmt.setDistributionQty(BigDecimal.ZERO);
                dcQcAmt.setPurchaseAmount(BigDecimal.ZERO);
                dcQcAmt.setReturnAmount(BigDecimal.ZERO);
                dcQcAmt.setSaleAmount(BigDecimal.ZERO);
                dcQcAmt.setProfitLossAmount(BigDecimal.ZERO);
                dcQcAmt.setAdjustAmt(BigDecimal.ZERO);
                dcQcAmt.setDistributionAmt(BigDecimal.ZERO);
                dcQcAmt.setEndAmt(dcQcAmt.getStartAmt());
                dcQcAmt.setEndQty(dcQcAmt.getStartQty());
            }
        }
        //3.门店和仓库都不选的情况
        if (ArrayUtil.isEmpty(inData.getSiteArr()) && StrUtil.isEmpty(inData.getSiteCode())) {
            allData= this.materialDocMapper.getStartDataAll(inData);
            for (InventoryChangeSummaryOutData all : allData) {
                all.setPurchaseQty(BigDecimal.ZERO);
                all.setReturnQty(BigDecimal.ZERO);
                all.setSaleQty(BigDecimal.ZERO);
                all.setProfitLossQty(BigDecimal.ZERO);
                all.setAdjustQty(BigDecimal.ZERO);
                all.setDistributionQty(BigDecimal.ZERO);
                all.setPurchaseAmount(BigDecimal.ZERO);
                all.setReturnAmount(BigDecimal.ZERO);
                all.setSaleAmount(BigDecimal.ZERO);
                all.setProfitLossAmount(BigDecimal.ZERO);
                all.setAdjustAmt(BigDecimal.ZERO);
                all.setDistributionAmt(BigDecimal.ZERO);
                all.setEndAmt(all.getStartAmt());
                all.setEndQty(all.getStartQty());
            }
        }
        stoQcData.addAll(dcQcData);
        stoQcData.addAll(allData);
        return stoQcData;
    }

    @Override
    public PageInfo selectInventoryChangeSummaryDetail(InventoryChangeSummaryInData inData) {

        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        if (StringUtils.isEmpty(inData.getProCode())) {
            throw new BusinessException("请选择商品！");
        }
        if (StringUtils.isNotEmpty(inData.getProCode())) {
            inData.setProArr(inData.getProCode().split("\\s+ |\\s+|,"));
        }
        List<InventoryChangeSummaryDetailOutData> outData = this.materialDocMapper.selectInventoryChangeSummaryDetail(inData);
        InventoryChangeSummaryDetailOutData outNum = new InventoryChangeSummaryDetailOutData();
        BigDecimal startCount=BigDecimal.ZERO;
        //期间数量
        BigDecimal betweenCount=outData.stream().map(InventoryChangeSummaryDetailOutData::getQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO,BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
        if (StringUtils.isNotEmpty(inData.getStartDate())) {
            List<InventoryChangeSummaryOutData> startData= this.materialDocMapper.getStartDataAll(inData);
            //期初数量
            startCount=startData.stream().map(InventoryChangeSummaryOutData::getStartQty).filter(Objects::nonNull).reduce(BigDecimal.ZERO,BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
        }
        //期末数量
        BigDecimal endCount = startCount.add(betweenCount).setScale(2,BigDecimal.ROUND_HALF_UP);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            // 集合列的数据汇总
            outNum.setStartCount(startCount.toString());
            outNum.setBetweenCount(betweenCount.toString());
            outNum.setEndCount(endCount.toString());
            for (InventoryChangeSummaryDetailOutData out : outData) {
                outNum.setAmount(CommonUtil.stripTrailingZeros(outNum.getAmount()).add(CommonUtil.stripTrailingZeros(out.getAmount())));
                outNum.setAddAmount(CommonUtil.stripTrailingZeros(outNum.getAddAmount()).add(CommonUtil.stripTrailingZeros(out.getAddAmount())));
                outNum.setQty(CommonUtil.stripTrailingZeros(outNum.getQty()).add(CommonUtil.stripTrailingZeros(out.getQty())));
            }
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(outNum);
        } else {
            outNum.setStartCount(startCount.toString());
            outNum.setBetweenCount(betweenCount.toString());
            outNum.setEndCount(endCount.toString());
            pageInfo = new PageInfo();
            pageInfo.setListNum(outNum);
        }

        return pageInfo;
    }

    @Override
    public PageInfo selectPayDayreport(PayDayreportInData inData) {
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        GetPayInData pay = new GetPayInData();
        pay.setClientId(inData.getClient());
        pay.setType("1");
        // 转换审核时间格式

        //获取支付类型
        List<GetPayTypeOutData> payTypeOutData = payService.payTypeListByClient(pay);
        if (payTypeOutData != null && payTypeOutData.size() > 0) {
            inData.setPayTypeOutData(payTypeOutData);
        }
        List<Map<String, Object>> outData = this.payDayreportH.selectPayDayreport(inData);
        if(CollUtil.isNotEmpty(outData)){
            for (Map<String, Object> data : outData) {
                // 如果单号不为空，查出销售应收(系统金额saleAmount)，储值卡充值金额(cardAmount)，对账金额(saleinputAmt)，差异(saleDifference)
                if (ObjectUtil.isNotEmpty(data.get("voucherId"))){
                    List<PayDayReportDetailOutData> payDayReportDetailOutData = payDayreportDMapper.queryPayDayReportDetail(inData.getClient(),(String)data.get("voucherId"));  // 日结对账明细
                    BigDecimal inputSale = BigDecimal.ZERO; // 对账金额
                    BigDecimal sysAmt = BigDecimal.ZERO; // 系统金额
                    BigDecimal difference = BigDecimal.ZERO; // 差异金额
                    for (PayDayReportDetailOutData detail : payDayReportDetailOutData) {
                        for (GetPayTypeOutData payType : payTypeOutData) {
                            if (StringUtils.equals(payType.getGspmId(),detail.getGspddPayType())){
                                data.put("sys"+ payType.getGspmKey(),detail.getSaleAmount().setScale(2,BigDecimal.ROUND_HALF_UP));     // 系统金额
                                data.put(payType.getGspmKey(),detail.getSaleinputAmt().setScale(2,BigDecimal.ROUND_HALF_UP));    // 对账金额
                                data.put("diff"+ payType.getGspmKey(),detail.getSaleAmount().add(detail.getCardAmount()).subtract(detail.getSaleinputAmt()).setScale(2,BigDecimal.ROUND_HALF_UP)); // 差异
                                inputSale = inputSale.add(detail.getSaleinputAmt().setScale(2,BigDecimal.ROUND_HALF_UP));
                                sysAmt = sysAmt.add(detail.getSaleAmount().setScale(2,BigDecimal.ROUND_HALF_UP));
                                difference = difference.add(detail.getSaleAmount().add(detail.getCardAmount()).subtract(detail.getSaleinputAmt()).setScale(2,BigDecimal.ROUND_HALF_UP));
                            }
                        }
                    }
                    data.put("sysAmt",sysAmt);     // 系统金额
                    data.put("inputAmt",inputSale); // 对账金额
                    data.put("difference",difference); // 差异金额
                }
                //差异
//                data.put("difference",new BigDecimal(Convert.toStr(data.get("salesAmt"))).add(new BigDecimal(Convert.toStr(data.get("afterAmt")))).subtract(new BigDecimal(Convert.toStr(data.get("saleInputAmt")))).setScale(2,BigDecimal.ROUND_HALF_UP));
            }
        }

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            // 集合列的数据汇总
            //初始化需要汇总的列
            Map<String, BigDecimal> mapSum = new HashMap<>();

            mapSum.put("salesAmt", BigDecimal.ZERO);        // 实收金额
            mapSum.put("inputAmt", BigDecimal.ZERO);
            //储值卡充值
            mapSum.put("afterAmt", BigDecimal.ZERO);        // 储值卡充值
            mapSum.put("saleInputAmt", BigDecimal.ZERO);    // 对账金额
            mapSum.put("sysAmt", BigDecimal.ZERO);    // 系统金额
            mapSum.put("difference", BigDecimal.ZERO);    // 差异和
            ArrayList<GetPayTypeOutData> getPayTypeOutData = new ArrayList<>();         // 支付方式加入新增的字段，用于合计
            for (GetPayTypeOutData payTypeOutDatum : payTypeOutData) {
                getPayTypeOutData.add(new GetPayTypeOutData("sys" + payTypeOutDatum.getGspmKey()));   // 系统金额
                getPayTypeOutData.add(new GetPayTypeOutData("record" + payTypeOutDatum.getGspmKey()));   // 对账金额
                getPayTypeOutData.add(new GetPayTypeOutData("diff" + payTypeOutDatum.getGspmKey()));    // 差异
            }
            payTypeOutData.addAll(getPayTypeOutData);
            for (GetPayTypeOutData paytppe : payTypeOutData) {
                mapSum.put(paytppe.getGspmKey(), BigDecimal.ZERO);
            }
            for (Map<String, Object> outMap : outData) {
                //调用方法相加
                mapSum = CommonUtil.addLeftJoinMap(mapSum, outMap);
//                if(CollUtil.isNotEmpty(mapSum)){
//                    //差异和
//                    mapSum.put("difference",mapSum.get("salesAmt").add(mapSum.get("afterAmt")).subtract(mapSum.get("saleInputAmt")).setScale(2,BigDecimal.ROUND_HALF_UP));
//                }
            }
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(mapSum);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    /**
     * 门店用
     * 供应商商品销售合计
     * 陈老板和李老板设计要求的
     *
     * @param inData
     * @return
     */
    @Override
    public PageInfo selectProductSalesBySupplierByStore(ProductSalesBySupplierInData inData) {
        List<ProductSalesBySupplierOutData> productSupplierData = new ArrayList<>();
        PageInfo pageInfo;
//        BigDecimal ss = BigDecimal.ZERO;
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        List<SalespersonsSalesDetailsOutData> saleList = gaiaSdSaleDMapper.getProductSalesBySupplier(inData);
        if (saleList.size() > 0) {
            productSupplierData = sdBatchChanegMapper.getProductSalesBySupplier(inData);
            if (productSupplierData.size() > 0) {
                for (SalespersonsSalesDetailsOutData saleSum : saleList) {
                    //根据商品编码汇总 获取该商品所有的销售数量
                    BigDecimal sumQty = CommonUtil.stripTrailingZeros(saleSum.getQty());
                    for (ProductSalesBySupplierOutData supplierOutData : productSupplierData) {
                        if (saleSum.getSelfCode().equals(supplierOutData.getProCode()) && saleSum.getStoCode().equals(supplierOutData.getStoCode())) {
                            //根据批次汇总 获取该商品所有的异动数量
                            // 批次==供应商
                            BigDecimal supQty = CommonUtil.stripTrailingZeros(supplierOutData.getQty());
                            //算出该加盟商销售数量占总销量的百分比
                            BigDecimal rateQty = BigDecimal.ONE;
                            if (sumQty.compareTo(BigDecimal.ZERO) != 0) {
                                rateQty = supQty.divide(sumQty, 10, BigDecimal.ROUND_HALF_EVEN);
                            }
                            //用百分比乘各个数据
                            supplierOutData.setAmountReceivable(CommonUtil.stripTrailingZeros(saleSum.getAmountReceivable()).multiply(rateQty));//应收金额
//                            ss = ss.add(supplierOutData.getAmountReceivable());
                            supplierOutData.setAmt(CommonUtil.stripTrailingZeros(saleSum.getAmt()).multiply(rateQty));//实收金额
                            supplierOutData.setIncludeTaxSale(CommonUtil.stripTrailingZeros(saleSum.getIncludeTaxSale()).multiply(rateQty));//成本额
                            supplierOutData.setGrossProfitMargin(CommonUtil.stripTrailingZeros(saleSum.getGrossProfitMargin()).multiply(rateQty));//毛利额
//                        try {
//                            System.out.println("异常商品:"+supplierOutData.getBrId()+","+supplierOutData.getProCode()+","+supplierOutData.getBatch()+","+supplierOutData.getAmt());
                            //在错误的情况下   实收金额可能是0  NND
                            if (supplierOutData.getAmt().compareTo(BigDecimal.ZERO) != 0) {
                                supplierOutData.setGrossProfitRate(supplierOutData.getGrossProfitMargin().
                                        multiply(new BigDecimal(100)).
                                        divide(supplierOutData.getAmt(), 2, BigDecimal.ROUND_HALF_EVEN)
                                );//毛利率=毛利额/实收金额
                            } else {
                                supplierOutData.setGrossProfitRate(BigDecimal.ZERO);
                            }


                        }
                    }
                }
            } else {
                return new PageInfo();
            }
        } else {
            return new PageInfo();
        }
//        System.out.println(ss);
        ProductSalesBySupplierOutTotal supplierOutTotal = new ProductSalesBySupplierOutTotal();
        // 集合列的数据汇总
        for (ProductSalesBySupplierOutData supplierOutData : productSupplierData) {
            supplierOutTotal.setAmountReceivable(CommonUtil.stripTrailingZeros(supplierOutData.getAmountReceivable()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getAmountReceivable())));
            supplierOutTotal.setAmt(CommonUtil.stripTrailingZeros(supplierOutData.getAmt()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getAmt())));
            supplierOutTotal.setIncludeTaxSale(CommonUtil.stripTrailingZeros(supplierOutData.getIncludeTaxSale()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getIncludeTaxSale())));
            supplierOutTotal.setGrossProfitMargin(CommonUtil.stripTrailingZeros(supplierOutData.getGrossProfitMargin()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getGrossProfitMargin())));
            supplierOutTotal.setQty(CommonUtil.stripTrailingZeros(supplierOutData.getQty()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getQty())));

        }
        if (supplierOutTotal.getAmt().compareTo(BigDecimal.ZERO) != 0) {
            DecimalFormat df = new DecimalFormat("0.00%");
            supplierOutTotal.setGrossProfitRate(df.format(CommonUtil.stripTrailingZeros(supplierOutTotal.getGrossProfitMargin()).divide(CommonUtil.stripTrailingZeros(supplierOutTotal.getAmt()), BigDecimal.ROUND_HALF_EVEN)));
        }

        pageInfo = new PageInfo(productSupplierData, supplierOutTotal);
        return pageInfo;
    }

    /**
     * 供应商用
     * 供应商商品销售合计
     * 陈老板和李老板设计要求的
     *
     * @param inData
     * @return
     */
    @Override
    public PageInfo selectProductSalesBySupplier(ProductSalesBySupplierInData inData) {
        List<ProductSalesBySupplierOutData> productSupplierData = new ArrayList<>();
        PageInfo pageInfo;
//        BigDecimal ss = BigDecimal.ZERO;

        List<SalespersonsSalesDetailsOutData> saleList = gaiaSdSaleDMapper.getProductSalesBySupplier(inData);
        if (saleList.size() > 0) {
            productSupplierData = sdBatchChanegMapper.getProductSalesBySupplier(inData);
            if (productSupplierData.size() > 0) {
                for (SalespersonsSalesDetailsOutData saleSum : saleList) {
                    //根据商品编码汇总 获取该商品所有的销售数量
                    BigDecimal sumQty = CommonUtil.stripTrailingZeros(saleSum.getQty());
                    for (ProductSalesBySupplierOutData supplierOutData : productSupplierData) {
                        if (saleSum.getSelfCode().equals(supplierOutData.getProCode()) && saleSum.getStoCode().equals(supplierOutData.getStoCode())) {
                            //根据批次汇总 获取该商品所有的异动数量
                            // 批次==供应商
                            BigDecimal supQty = CommonUtil.stripTrailingZeros(supplierOutData.getQty());
                            //算出该加盟商销售数量占总销量的百分比
                            BigDecimal rateQty = BigDecimal.ONE;
                            if (sumQty.compareTo(BigDecimal.ZERO) != 0) {
                                rateQty = supQty.divide(sumQty, 10, BigDecimal.ROUND_HALF_EVEN);
                            }
                            //用百分比乘各个数据
                            supplierOutData.setAmountReceivable(CommonUtil.stripTrailingZeros(saleSum.getAmountReceivable()).multiply(rateQty));//应收金额
//                            ss = ss.add(supplierOutData.getAmountReceivable());
                            supplierOutData.setAmt(CommonUtil.stripTrailingZeros(saleSum.getAmt()).multiply(rateQty));//实收金额
                            supplierOutData.setIncludeTaxSale(CommonUtil.stripTrailingZeros(saleSum.getIncludeTaxSale()).multiply(rateQty));//成本额
                            supplierOutData.setGrossProfitMargin(CommonUtil.stripTrailingZeros(saleSum.getGrossProfitMargin()).multiply(rateQty));//毛利额
//                        try {
//                            System.out.println("异常商品:"+supplierOutData.getBrId()+","+supplierOutData.getProCode()+","+supplierOutData.getBatch()+","+supplierOutData.getAmt());
                            //在错误的情况下   实收金额可能是0  NND
                            if (supplierOutData.getAmt().compareTo(BigDecimal.ZERO) != 0) {
                                supplierOutData.setGrossProfitRate(supplierOutData.getGrossProfitMargin().
                                        multiply(new BigDecimal(100)).
                                        divide(supplierOutData.getAmt(), 2, BigDecimal.ROUND_HALF_EVEN)
                                );//毛利率=毛利额/实收金额
                            } else {
                                supplierOutData.setGrossProfitRate(BigDecimal.ZERO);
                            }


                        }
                    }
                }
            } else {
                return new PageInfo();
            }
        } else {
            return new PageInfo();
        }
//        System.out.println(ss);
        ProductSalesBySupplierOutTotal supplierOutTotal = new ProductSalesBySupplierOutTotal();
        // 集合列的数据汇总
        for (ProductSalesBySupplierOutData supplierOutData : productSupplierData) {
            supplierOutTotal.setAmountReceivable(CommonUtil.stripTrailingZeros(supplierOutData.getAmountReceivable()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getAmountReceivable())));
            supplierOutTotal.setAmt(CommonUtil.stripTrailingZeros(supplierOutData.getAmt()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getAmt())));
            supplierOutTotal.setIncludeTaxSale(CommonUtil.stripTrailingZeros(supplierOutData.getIncludeTaxSale()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getIncludeTaxSale())));
            supplierOutTotal.setGrossProfitMargin(CommonUtil.stripTrailingZeros(supplierOutData.getGrossProfitMargin()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getGrossProfitMargin())));
            supplierOutTotal.setQty(CommonUtil.stripTrailingZeros(supplierOutData.getQty()).add(CommonUtil.stripTrailingZeros(supplierOutTotal.getQty())));

        }
        if (supplierOutTotal.getAmt().compareTo(BigDecimal.ZERO) != 0) {
            DecimalFormat df = new DecimalFormat("0.00%");
            supplierOutTotal.setGrossProfitRate(df.format(CommonUtil.stripTrailingZeros(supplierOutTotal.getGrossProfitMargin()).divide(CommonUtil.stripTrailingZeros(supplierOutTotal.getAmt()), BigDecimal.ROUND_HALF_EVEN)));
        }

        pageInfo = new PageInfo(productSupplierData, supplierOutTotal);
        return pageInfo;
    }


    @Override
    public PageInfo selectStoreSaleByDate(StoreSaleDateInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        if (inData.getDateType().equals("1")) {
            inData.setStartDate(DateUtil.getYearFirst(Integer.parseInt(inData.getStartDate())));
            inData.setEndDate(DateUtil.getYearLast(Integer.parseInt(inData.getEndDate())));
        }
        if (inData.getDateType().equals("2")) {
            inData.setStartDate(DateUtil.getYearMonthFirst(inData.getStartDate()));
            inData.setEndDate(DateUtil.getYearMonthLast(inData.getEndDate()));
        }
        if(StrUtil.isNotBlank(inData.getStatDatePart())){
            inData.setStatDatePart(inData.getStatDatePart()+"00");
        }
        if(StrUtil.isNotBlank(inData.getEndDatePart())){
            inData.setEndDatePart(inData.getEndDatePart()+"59");
        }

        GetPayInData pay = new GetPayInData();
        pay.setClientId(inData.getClient());
        pay.setType("1");
        //获取支付类型
        List<GetPayTypeOutData> payTypeOutData = payService.payTypeListByClient(pay);
        if (payTypeOutData != null && payTypeOutData.size() > 0) {
            inData.setPayTypeOutData(payTypeOutData);
        }

        List<Map<String, Object>> outData = this.saleDMapper.selectStoreSaleByDate(inData);

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData) && outData.get(0).size() > 1) {
            // 集合列的数据汇总
            Map<String, Object> outTotal = this.saleDMapper.selectStoreSaleByTatol(inData);
            pageInfo = new PageInfo(outData, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo selectProductSaleByClient(StoreProductSaleClientInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        List<StoreProductSaleClientOutData> outData = this.saleDMapper.selectProductSaleByClient(inData);

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {// 集合列的数据汇总
            StoreProductSaleClientOutTotal outTotal = this.saleDMapper.selectProductSaleByClientTatol(inData);
            BigDecimal dcTotal=BigDecimal.ZERO;
            BigDecimal stoTotal=BigDecimal.ZERO;
            BigDecimal onlineSaleQty=BigDecimal.ZERO;
            BigDecimal onlineAmt=BigDecimal.ZERO;
            BigDecimal onlineGrossAmt=BigDecimal.ZERO;
            BigDecimal onlineMov=BigDecimal.ZERO;
            String onlineGrossRate="0.00%";

            onlineSaleQty = outData.stream().collect(CollectorsUtil.summingBigDecimal(out->out.getOnlineSaleQty()==null?BigDecimal.ZERO:out.getOnlineSaleQty()));
            onlineAmt = outData.stream().collect(CollectorsUtil.summingBigDecimal(out->out.getOnlineAmt()==null?BigDecimal.ZERO:out.getOnlineAmt()));
            onlineGrossAmt = outData.stream().collect(CollectorsUtil.summingBigDecimal(out->out.getOnlineGrossAmt()==null?BigDecimal.ZERO:out.getOnlineGrossAmt()));
            onlineMov = outData.stream().collect(CollectorsUtil.summingBigDecimal(out->out.getOnlineMov()==null?BigDecimal.ZERO:out.getOnlineMov()));
            onlineGrossRate=onlineGrossAmt.divide(onlineAmt.compareTo(BigDecimal.ZERO)==0?BigDecimal.ONE:onlineAmt,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP)+"%";
            for (StoreProductSaleClientOutData out : outData) {
                if (out.getDcQty() != null) {
                    dcTotal=dcTotal.add(new BigDecimal(out.getDcQty()));
                }
                if (out.getStoQty() != null) {
                    stoTotal=stoTotal.add(new BigDecimal(out.getStoQty()));
                }
            }
            outTotal.setDcQty(dcTotal);
            outTotal.setStoQty(stoTotal);
            outTotal.setOnlineSaleQty(onlineSaleQty);
            outTotal.setOnlineAmt(onlineAmt);
            outTotal.setOnlineGrossAmt(onlineGrossAmt);
            outTotal.setOnlineMov(onlineMov);
            outTotal.setOnlineGrossRate(onlineGrossRate);
            pageInfo = new PageInfo(outData, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo selectProductSaleByStore(StoreProductSaleStoreInData inData) {
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        List<StoreProductSaleStoreOutData> outData;
        StoreProductSaleStoreOutTotal outTotal;
        //选择商品的时候要把没有销售的门店也带出来
        if (ObjectUtil.isEmpty(inData.getProArr())) {
            outData = this.saleDMapper.selectProductSaleByStore(inData);
            outTotal = this.saleDMapper.selectProductSaleByStoreTatol(inData);
        } else {
            outData = this.saleDMapper.selectProductSaleByProAllStore(inData);
            outTotal = this.saleDMapper.selectProductSaleByProAllStoreTatol(inData);
        }
        BigDecimal onlineSaleQty=BigDecimal.ZERO;
        BigDecimal onlineAmt=BigDecimal.ZERO;
        BigDecimal onlineGrossAmt=BigDecimal.ZERO;
        BigDecimal onlineMov=BigDecimal.ZERO;
        String onlineGrossRate="0.00%";
        onlineSaleQty = outData.stream().collect(CollectorsUtil.summingBigDecimal(out->out.getOnlineSaleQty()==null?BigDecimal.ZERO:out.getOnlineSaleQty()));
        onlineAmt = outData.stream().collect(CollectorsUtil.summingBigDecimal(out->out.getOnlineAmt()==null?BigDecimal.ZERO:out.getOnlineAmt()));
        onlineGrossAmt = outData.stream().collect(CollectorsUtil.summingBigDecimal(out->out.getOnlineGrossAmt()==null?BigDecimal.ZERO:out.getOnlineGrossAmt()));
        onlineMov = outData.stream().collect(CollectorsUtil.summingBigDecimal(out->out.getOnlineMov()==null?BigDecimal.ZERO:out.getOnlineMov()));
        onlineGrossRate=onlineGrossAmt.divide(onlineAmt.compareTo(BigDecimal.ZERO)==0?BigDecimal.ONE:onlineAmt,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP)+"%";
        if (ObjectUtil.isNotNull(outTotal)) {
            outTotal.setOnlineSaleQty(onlineSaleQty);
            outTotal.setOnlineAmt(onlineAmt);
            outTotal.setOnlineGrossAmt(onlineGrossAmt);
            outTotal.setOnlineMov(onlineMov);
            outTotal.setOnlineGrossRate(onlineGrossRate);
        }
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            // 集合列的数据汇总
            pageInfo = new PageInfo(outData, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public JsonResult selectDistributionData(StoreDistributaionDataInData inData) {
        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getDcCode())) {
            throw new BusinessException("仓库地不能为空");
        }

        Map<String, Object> outData = new HashMap<>();

        List<StoreDistributaionDataOutData> inSideData = new ArrayList<>();
        List<StoreDistributaionDataOutData> outSideData = new ArrayList<>();
        if (inData.getCustomerFlag() != null && inData.getCustomerFlag().equals("1")) {
            //内部客户
            inSideData = this.saleDMapper.getDistributionDataByInSide(inData);
        } else if (inData.getCustomerFlag() != null && inData.getCustomerFlag().equals("2")) {
            //外部客户
            outSideData = this.saleDMapper.getDistributionDataByOutSide(inData);
        } else {
            //全部
            inSideData = this.saleDMapper.getDistributionDataByInSide(inData);
            outSideData = this.saleDMapper.getDistributionDataByOutSide(inData);

        }
        Map<String, Object> inSideTotal = new HashMap<>();

        for (StoreDistributaionDataOutData inSide : inSideData) {
            if (inSideTotal.get("outStockDBE") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("outStockDBE").toString());
                BigDecimal add = inSide.getOutStockDBE();
                inSideTotal.put("outStockDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("outStockDBE", inSide.getOutStockDBE());
            }
            if (inSideTotal.get("refoundDBE") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("refoundDBE").toString());
                BigDecimal add = inSide.getRefoundDBE();
                inSideTotal.put("refoundDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("refoundDBE", inSide.getRefoundDBE());
            }
            if (inSideTotal.get("dbeRightTotal") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("dbeRightTotal").toString());
                BigDecimal add = inSide.getDbeRightTotal();
                inSideTotal.put("dbeRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("dbeRightTotal", inSide.getDbeRightTotal());
            }

            if (inSideTotal.get("outStockMOV") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("outStockMOV").toString());
                BigDecimal add = inSide.getOutStockMOV();
                inSideTotal.put("outStockMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("outStockMOV", inSide.getOutStockMOV());
            }
            if (inSideTotal.get("refoundMOV") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("refoundMOV").toString());
                BigDecimal add = inSide.getRefoundMOV();
                inSideTotal.put("refoundMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("refoundMOV", inSide.getRefoundMOV());
            }
            if (inSideTotal.get("movRightTotal") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("movRightTotal").toString());
                BigDecimal add = inSide.getMovRightTotal();
                inSideTotal.put("movRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("movRightTotal", inSide.getMovRightTotal());
            }

            if (inSideTotal.get("outStockGrossAmt") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("outStockGrossAmt").toString());
                BigDecimal add = inSide.getOutStockGrossAmt();
                inSideTotal.put("outStockGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("outStockGrossAmt", inSide.getOutStockGrossAmt());
            }
            if (inSideTotal.get("refoundGrossAmt") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("refoundGrossAmt").toString());
                BigDecimal add = inSide.getRefoundGrossAmt();
                inSideTotal.put("refoundGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("refoundGrossAmt", inSide.getRefoundGrossAmt());
            }
            if (inSideTotal.get("grossAmtRightTotal") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("grossAmtRightTotal").toString());
                BigDecimal add = inSide.getGrossAmtRightTotal();
                inSideTotal.put("grossAmtRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("grossAmtRightTotal", inSide.getGrossAmtRightTotal());
            }

        }
        if (!inSideTotal.isEmpty()) {
            if (inSideTotal.get("outStockGrossAmt") != null && inSideTotal.get("outStockDBE") != null) {
                BigDecimal grossAmtOutStockTotal = new BigDecimal(inSideTotal.get("outStockGrossAmt").toString());
                BigDecimal dbeOutStockTotal = new BigDecimal(inSideTotal.get("outStockDBE").toString());
                if (dbeOutStockTotal.compareTo(BigDecimal.ZERO) != 0) {
                    String grossOutStockRate = grossAmtOutStockTotal.divide(dbeOutStockTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    inSideTotal.put("outStockGrossRate", grossOutStockRate);
                }
            }
            if (inSideTotal.get("refoundGrossAmt") != null && inSideTotal.get("refoundDBE") != null) {
                BigDecimal grossAmtRefoundTotal = new BigDecimal(inSideTotal.get("refoundGrossAmt").toString());
                BigDecimal dbeRefoundTotal = new BigDecimal(inSideTotal.get("refoundDBE").toString());
                if (dbeRefoundTotal.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRefoundRate = grossAmtRefoundTotal.divide(dbeRefoundTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    inSideTotal.put("refoundGrossRate", grossRefoundRate);
                }
            }
            if (inSideTotal.get("grossAmtRightTotal") != null && inSideTotal.get("dbeRightTotal") != null) {
                BigDecimal grossAmtRightTotal = new BigDecimal(inSideTotal.get("grossAmtRightTotal").toString());
                BigDecimal dbeRightTotal = new BigDecimal(inSideTotal.get("dbeRightTotal").toString());
                if (dbeRightTotal.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRightRate = grossAmtRightTotal.divide(dbeRightTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    inSideTotal.put("grossRateRightTotal", grossRightRate);
                }
            }
        }

        Map<String, Object> outSideTotal = new HashMap<>();
        for (StoreDistributaionDataOutData outSide : outSideData) {
            if (outSideTotal.get("outStockDBE") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("outStockDBE").toString());
                BigDecimal add = outSide.getOutStockDBE();
                outSideTotal.put("outStockDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("outStockDBE", outSide.getOutStockDBE());
            }
            if (outSideTotal.get("refoundDBE") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("refoundDBE").toString());
                BigDecimal add = outSide.getRefoundDBE();
                outSideTotal.put("refoundDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("refoundDBE", outSide.getRefoundDBE());
            }
            if (outSideTotal.get("dbeRightTotal") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("dbeRightTotal").toString());
                BigDecimal add = outSide.getDbeRightTotal();
                outSideTotal.put("dbeRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("dbeRightTotal", outSide.getDbeRightTotal());
            }
            if (outSideTotal.get("outStockMOV") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("outStockMOV").toString());
                BigDecimal add = outSide.getOutStockMOV();
                outSideTotal.put("outStockMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("outStockMOV", outSide.getOutStockMOV());
            }
            if (outSideTotal.get("refoundMOV") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("refoundMOV").toString());
                BigDecimal add = outSide.getRefoundMOV();
                outSideTotal.put("refoundMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("refoundMOV", outSide.getRefoundMOV());
            }
            if (outSideTotal.get("movRightTotal") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("movRightTotal").toString());
                BigDecimal add = outSide.getMovRightTotal();
                outSideTotal.put("movRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("movRightTotal", outSide.getMovRightTotal());
            }
            if (outSideTotal.get("outStockGrossAmt") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("outStockGrossAmt").toString());
                BigDecimal add = outSide.getOutStockGrossAmt();
                outSideTotal.put("outStockGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("outStockGrossAmt", outSide.getOutStockGrossAmt());
            }
            if (outSideTotal.get("refoundGrossAmt") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("refoundGrossAmt").toString());
                BigDecimal add = outSide.getRefoundGrossAmt();
                outSideTotal.put("refoundGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("refoundGrossAmt", outSide.getRefoundGrossAmt());
            }
            if (outSideTotal.get("grossAmtRightTotal") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("grossAmtRightTotal").toString());
                BigDecimal add = outSide.getGrossAmtRightTotal();
                outSideTotal.put("grossAmtRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("grossAmtRightTotal", outSide.getGrossAmtRightTotal());
            }

        }
        if (!outSideTotal.isEmpty()) {
            if (outSideTotal.get("outStockGrossAmt") != null && outSideTotal.get("outStockDBE") != null) {
                BigDecimal grossAmtOutStockTotalOutSide = new BigDecimal(outSideTotal.get("outStockGrossAmt").toString());
                BigDecimal dbeOutStockTotalOutSide = new BigDecimal(outSideTotal.get("outStockDBE").toString());
                if (dbeOutStockTotalOutSide.compareTo(BigDecimal.ZERO) != 0) {
                    String grossOutStockRateOutSide = grossAmtOutStockTotalOutSide.divide(dbeOutStockTotalOutSide, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    outSideTotal.put("outStockGrossRate", grossOutStockRateOutSide);
                }
            }
            if (outSideTotal.get("refoundGrossAmt") != null && outSideTotal.get("refoundDBE") != null) {
                BigDecimal grossAmtRefoundTotalOutSide = new BigDecimal(outSideTotal.get("refoundGrossAmt").toString());
                BigDecimal dbeRefoundTotalOutSide = new BigDecimal(outSideTotal.get("refoundDBE").toString());
                if (dbeRefoundTotalOutSide.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRefoundRateOutSide = grossAmtRefoundTotalOutSide.divide(dbeRefoundTotalOutSide, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    outSideTotal.put("refoundGrossRate", grossRefoundRateOutSide);
                }
            }
            if (outSideTotal.get("grossAmtRightTotal") != null && outSideTotal.get("dbeRightTotal") != null) {
                BigDecimal grossAmtRightTotalOutSide = new BigDecimal(outSideTotal.get("grossAmtRightTotal").toString());
                BigDecimal dbeRightTotalOutSide = new BigDecimal(outSideTotal.get("dbeRightTotal").toString());
                if (dbeRightTotalOutSide.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRightRateOutSide = grossAmtRightTotalOutSide.divide(dbeRightTotalOutSide, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    outSideTotal.put("grossRateRightTotal", grossRightRateOutSide);
                }
            }
        }

        List<StoreDistributaionDataOutData> allData = new ArrayList<>();
        allData.addAll(inSideData);
        allData.addAll(outSideData);
        Map<String, Object> allDataTotal = new HashMap<>();
        for (StoreDistributaionDataOutData all : allData) {
            if (allDataTotal.get("outStockDBE") != null) {
                BigDecimal count = new BigDecimal(allDataTotal.get("outStockDBE").toString());
                BigDecimal add = all.getOutStockDBE();
                allDataTotal.put("outStockDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                allDataTotal.put("outStockDBE", all.getOutStockDBE());
            }
            if (allDataTotal.get("refoundDBE") != null) {
                BigDecimal count = new BigDecimal(allDataTotal.get("refoundDBE").toString());
                BigDecimal add = all.getRefoundDBE();
                allDataTotal.put("refoundDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                allDataTotal.put("refoundDBE", all.getRefoundDBE());
            }
            if (allDataTotal.get("dbeRightTotal") != null) {
                BigDecimal count = new BigDecimal(allDataTotal.get("dbeRightTotal").toString());
                BigDecimal add = all.getDbeRightTotal();
                allDataTotal.put("dbeRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                allDataTotal.put("dbeRightTotal", all.getDbeRightTotal());
            }
            if (allDataTotal.get("outStockMOV") != null) {
                BigDecimal count = new BigDecimal(allDataTotal.get("outStockMOV").toString());
                BigDecimal add = all.getOutStockMOV();
                allDataTotal.put("outStockMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                allDataTotal.put("outStockMOV", all.getOutStockMOV());
            }
            if (allDataTotal.get("refoundMOV") != null) {
                BigDecimal count = new BigDecimal(allDataTotal.get("refoundMOV").toString());
                BigDecimal add = all.getRefoundMOV();
                allDataTotal.put("refoundMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                allDataTotal.put("refoundMOV", all.getRefoundMOV());
            }
            if (allDataTotal.get("movRightTotal") != null) {
                BigDecimal count = new BigDecimal(allDataTotal.get("movRightTotal").toString());
                BigDecimal add = all.getMovRightTotal();
                allDataTotal.put("movRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                allDataTotal.put("movRightTotal", all.getMovRightTotal());
            }
            if (allDataTotal.get("outStockGrossAmt") != null) {
                BigDecimal count = new BigDecimal(allDataTotal.get("outStockGrossAmt").toString());
                BigDecimal add = all.getOutStockGrossAmt();
                allDataTotal.put("outStockGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                allDataTotal.put("outStockGrossAmt", all.getOutStockGrossAmt());
            }
            if (allDataTotal.get("refoundGrossAmt") != null) {
                BigDecimal count = new BigDecimal(allDataTotal.get("refoundGrossAmt").toString());
                BigDecimal add = all.getRefoundGrossAmt();
                allDataTotal.put("refoundGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                allDataTotal.put("refoundGrossAmt", all.getRefoundGrossAmt());
            }
            if (allDataTotal.get("grossAmtRightTotal") != null) {
                BigDecimal count = new BigDecimal(allDataTotal.get("grossAmtRightTotal").toString());
                BigDecimal add = all.getGrossAmtRightTotal();
                allDataTotal.put("grossAmtRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                allDataTotal.put("grossAmtRightTotal", all.getGrossAmtRightTotal());
            }
            //TODO 内部客户合计毛利率

        }
        if (!allDataTotal.isEmpty()) {
            if (allDataTotal.get("outStockGrossAmt") != null && allDataTotal.get("outStockDBE") != null) {
                BigDecimal grossAmtOutStockTotalAll = new BigDecimal(allDataTotal.get("outStockGrossAmt").toString());
                BigDecimal dbeOutStockTotalAll = new BigDecimal(allDataTotal.get("outStockDBE").toString());
                if (dbeOutStockTotalAll.compareTo(BigDecimal.ZERO) != 0) {
                    String grossOutStockRateAll = grossAmtOutStockTotalAll.divide(dbeOutStockTotalAll, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    allDataTotal.put("outStockGrossRate", grossOutStockRateAll);
                }
            }
            if (allDataTotal.get("refoundGrossAmt") != null && allDataTotal.get("refoundDBE") != null) {
                BigDecimal grossAmtRefoundTotalAll = new BigDecimal(allDataTotal.get("refoundGrossAmt").toString());
                BigDecimal dbeRefoundTotalAll = new BigDecimal(allDataTotal.get("refoundDBE").toString());
                if (dbeRefoundTotalAll.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRefoundRateAll = grossAmtRefoundTotalAll.divide(dbeRefoundTotalAll, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    allDataTotal.put("refoundGrossRate", grossRefoundRateAll);
                }
            }
            if (allDataTotal.get("grossAmtRightTotal") != null && allDataTotal.get("dbeRightTotal") != null) {
                BigDecimal grossAmtRightTotalAll = new BigDecimal(allDataTotal.get("grossAmtRightTotal").toString());
                BigDecimal dbeRightTotalAll = new BigDecimal(allDataTotal.get("dbeRightTotal").toString());
                if (dbeRightTotalAll.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRightRateAll = grossAmtRightTotalAll.divide(dbeRightTotalAll, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    allDataTotal.put("grossRateRightTotal", grossRightRateAll);
                }
            }
        }

        outData.put("inSideData", inSideData);
        outData.put("outSideData", outSideData);
        outData.put("inSideDataTotal", inSideTotal);
        outData.put("outSideDataTotal", outSideTotal);
        outData.put("allDataTotal", allDataTotal);
        return JsonResult.success(outData, "查询成功");
    }

    @Override
    public JsonResult getDcCode(String client) {
        List<Map<String, Object>> dcCodeList = this.saleDMapper.getDcCode(client);
        return JsonResult.success(dcCodeList, "查询成功");
    }

    @Override
    public JsonResult getCusList(String client, Map<String, String> dcCode) {

        List<Map<String, Object>> dcCodeList = this.saleDMapper.getCusList(client, dcCode.get("dcCode"));
        return JsonResult.success(dcCodeList, "查询成功");
    }

    /*public static void main(String[] args) {
        BigDecimal grossAmtRightTotalAll=new BigDecimal("-26828.81");
        BigDecimal dbeRightTotalAll=new BigDecimal("145559.19");
        String grossRightRateAll = grossAmtRightTotalAll.divide(dbeRightTotalAll, 4, BigDecimal.ROUND_HALF_UP)
                .multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
        System.out.println(grossRightRateAll);
    }*/

    @Override
    public List<StoreDistributaionDataOutData> exportCSV(StoreDistributaionDataInData inData) {

        if (ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getDcCode())) {
            throw new BusinessException("仓库地不能为空");
        }
        Map<String, Object> outData = new HashMap<>();

        List<StoreDistributaionDataOutData> inSideData = new ArrayList<>();
        List<StoreDistributaionDataOutData> outSideData = new ArrayList<>();
        if (inData.getCustomerFlag() != null && inData.getCustomerFlag().equals("1")) {
            //内部客户
            inSideData = this.saleDMapper.getDistributionDataByInSide(inData);

        } else if (inData.getCustomerFlag() != null && inData.getCustomerFlag().equals("2")) {
            //外部客户
            outSideData = this.saleDMapper.getDistributionDataByOutSide(inData);
        } else {
            //全部
            inSideData = this.saleDMapper.getDistributionDataByInSide(inData);
            outSideData = this.saleDMapper.getDistributionDataByOutSide(inData);
        }
        List<StoreDistributaionDataOutData> allData=new ArrayList<>();
        /*StoreDistributaionDataOutData inSideFlag = new StoreDistributaionDataOutData();
        inSideFlag.setCusCode("内部客户");
        allData.add(inSideFlag);*/
        allData.addAll(inSideData);
        Map<String, Object> inSideTotal = new HashMap<>();
        inSideTotal.put("outStockDBE",0);
        inSideTotal.put("refoundDBE",0);
        inSideTotal.put("dbeRightTotal",0);
        inSideTotal.put("outStockMOV",0);
        inSideTotal.put("refoundMOV",0);
        inSideTotal.put("movRightTotal",0);
        inSideTotal.put("outStockGrossAmt",0);
        inSideTotal.put("refoundGrossAmt",0);
        inSideTotal.put("grossAmtRightTotal",0);
        inSideTotal.put("outStockGrossRate","0.00%");
        inSideTotal.put("refoundGrossRate","0.00%");
        inSideTotal.put("grossRateRightTotal","0.00%");
        for (StoreDistributaionDataOutData inSide : inSideData) {
            if (inSideTotal.get("outStockDBE") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("outStockDBE").toString());
                BigDecimal add = inSide.getOutStockDBE();
                inSideTotal.put("outStockDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("outStockDBE", inSide.getOutStockDBE());
            }
            if (inSideTotal.get("refoundDBE") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("refoundDBE").toString());
                BigDecimal add = inSide.getRefoundDBE();
                inSideTotal.put("refoundDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("refoundDBE", inSide.getRefoundDBE());
            }
            if (inSideTotal.get("dbeRightTotal") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("dbeRightTotal").toString());
                BigDecimal add = inSide.getDbeRightTotal();
                inSideTotal.put("dbeRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("dbeRightTotal", inSide.getDbeRightTotal());
            }

            if (inSideTotal.get("outStockMOV") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("outStockMOV").toString());
                BigDecimal add = inSide.getOutStockMOV();
                inSideTotal.put("outStockMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("outStockMOV", inSide.getOutStockMOV());
            }
            if (inSideTotal.get("refoundMOV") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("refoundMOV").toString());
                BigDecimal add = inSide.getRefoundMOV();
                inSideTotal.put("refoundMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("refoundMOV", inSide.getRefoundMOV());
            }
            if (inSideTotal.get("movRightTotal") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("movRightTotal").toString());
                BigDecimal add = inSide.getMovRightTotal();
                inSideTotal.put("movRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("movRightTotal", inSide.getMovRightTotal());
            }

            if (inSideTotal.get("outStockGrossAmt") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("outStockGrossAmt").toString());
                BigDecimal add = inSide.getOutStockGrossAmt();
                inSideTotal.put("outStockGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("outStockGrossAmt", inSide.getOutStockGrossAmt());
            }
            if (inSideTotal.get("refoundGrossAmt") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("refoundGrossAmt").toString());
                BigDecimal add = inSide.getRefoundGrossAmt();
                inSideTotal.put("refoundGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("refoundGrossAmt", inSide.getRefoundGrossAmt());
            }
            if (inSideTotal.get("grossAmtRightTotal") != null) {
                BigDecimal count = new BigDecimal(inSideTotal.get("grossAmtRightTotal").toString());
                BigDecimal add = inSide.getGrossAmtRightTotal();
                inSideTotal.put("grossAmtRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                inSideTotal.put("grossAmtRightTotal", inSide.getGrossAmtRightTotal());
            }

        }
        if (!inSideTotal.isEmpty()) {
            if (inSideTotal.get("outStockGrossAmt") != null && inSideTotal.get("outStockDBE") != null) {
                BigDecimal grossAmtOutStockTotal = new BigDecimal(inSideTotal.get("outStockGrossAmt").toString());
                BigDecimal dbeOutStockTotal = new BigDecimal(inSideTotal.get("outStockDBE").toString());
                if (dbeOutStockTotal.compareTo(BigDecimal.ZERO) != 0) {
                    String grossOutStockRate = grossAmtOutStockTotal.divide(dbeOutStockTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    inSideTotal.put("outStockGrossRate", grossOutStockRate);
                }
            }
            if (inSideTotal.get("refoundGrossAmt") != null && inSideTotal.get("refoundDBE") != null) {
                BigDecimal grossAmtRefoundTotal = new BigDecimal(inSideTotal.get("refoundGrossAmt").toString());
                BigDecimal dbeRefoundTotal = new BigDecimal(inSideTotal.get("refoundDBE").toString());
                if (dbeRefoundTotal.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRefoundRate = grossAmtRefoundTotal.divide(dbeRefoundTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    inSideTotal.put("refoundGrossRate", grossRefoundRate);
                }
            }
            if (inSideTotal.get("grossAmtRightTotal") != null && inSideTotal.get("dbeRightTotal") != null) {
                BigDecimal grossAmtRightTotal = new BigDecimal(inSideTotal.get("grossAmtRightTotal").toString());
                BigDecimal dbeRightTotal = new BigDecimal(inSideTotal.get("dbeRightTotal").toString());
                if (dbeRightTotal.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRightRate = grossAmtRightTotal.divide(dbeRightTotal, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    inSideTotal.put("grossRateRightTotal", grossRightRate);
                }
            }
        }
            StoreDistributaionDataOutData inSideDataTotal=new StoreDistributaionDataOutData();
        if (!inSideData.isEmpty()) {
            inSideDataTotal.setCusType("内部小计");
            inSideDataTotal.setOutStockDBE(new BigDecimal(inSideTotal.get("outStockDBE").toString()));
            inSideDataTotal.setRefoundDBE(new BigDecimal(inSideTotal.get("refoundDBE").toString()));
            inSideDataTotal.setDbeRightTotal(new BigDecimal(inSideTotal.get("dbeRightTotal").toString()));
            inSideDataTotal.setOutStockMOV(new BigDecimal(inSideTotal.get("outStockMOV").toString()));
            inSideDataTotal.setRefoundMOV(new BigDecimal(inSideTotal.get("refoundMOV").toString()));
            inSideDataTotal.setMovRightTotal(new BigDecimal(inSideTotal.get("movRightTotal").toString()));
            inSideDataTotal.setOutStockGrossAmt(new BigDecimal(inSideTotal.get("outStockGrossAmt").toString()));
            inSideDataTotal.setRefoundGrossAmt(new BigDecimal(inSideTotal.get("refoundGrossAmt").toString()));
            inSideDataTotal.setGrossAmtRightTotal(new BigDecimal(inSideTotal.get("grossAmtRightTotal").toString()));
            inSideDataTotal.setOutStockGrossRate(inSideTotal.get("outStockGrossRate").toString());
            inSideDataTotal.setRefoundGrossRate(inSideTotal.get("refoundGrossRate").toString());
            inSideDataTotal.setGrossRateRightTotal(inSideTotal.get("grossRateRightTotal").toString());
            allData.add(inSideDataTotal);
        }
        /*StoreDistributaionDataOutData outSideFlag = new StoreDistributaionDataOutData();
        outSideFlag.setCusCode("外部客户");
        allData.add(outSideFlag);*/
        allData.addAll(outSideData);
        Map<String, Object> outSideTotal = new HashMap<>();
        outSideTotal.put("outStockDBE",0);
        outSideTotal.put("refoundDBE",0);
        outSideTotal.put("dbeRightTotal",0);
        outSideTotal.put("outStockMOV",0);
        outSideTotal.put("refoundMOV",0);
        outSideTotal.put("movRightTotal",0);
        outSideTotal.put("outStockGrossAmt",0);
        outSideTotal.put("refoundGrossAmt",0);
        outSideTotal.put("grossAmtRightTotal",0);
        outSideTotal.put("outStockGrossRate","0.00%");
        outSideTotal.put("refoundGrossRate","0.00%");
        outSideTotal.put("grossRateRightTotal","0.00%");
        for (StoreDistributaionDataOutData outSide : outSideData) {
            if (outSideTotal.get("outStockDBE") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("outStockDBE").toString());
                BigDecimal add = outSide.getOutStockDBE();
                outSideTotal.put("outStockDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("outStockDBE", outSide.getOutStockDBE());
            }
            if (outSideTotal.get("refoundDBE") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("refoundDBE").toString());
                BigDecimal add = outSide.getRefoundDBE();
                outSideTotal.put("refoundDBE", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("refoundDBE", outSide.getRefoundDBE());
            }
            if (outSideTotal.get("dbeRightTotal") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("dbeRightTotal").toString());
                BigDecimal add = outSide.getDbeRightTotal();
                outSideTotal.put("dbeRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("dbeRightTotal", outSide.getDbeRightTotal());
            }
            if (outSideTotal.get("outStockMOV") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("outStockMOV").toString());
                BigDecimal add = outSide.getOutStockMOV();
                outSideTotal.put("outStockMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("outStockMOV", outSide.getOutStockMOV());
            }
            if (outSideTotal.get("refoundMOV") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("refoundMOV").toString());
                BigDecimal add = outSide.getRefoundMOV();
                outSideTotal.put("refoundMOV", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("refoundMOV", outSide.getRefoundMOV());
            }
            if (outSideTotal.get("movRightTotal") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("movRightTotal").toString());
                BigDecimal add = outSide.getMovRightTotal();
                outSideTotal.put("movRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("movRightTotal", outSide.getMovRightTotal());
            }
            if (outSideTotal.get("outStockGrossAmt") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("outStockGrossAmt").toString());
                BigDecimal add = outSide.getOutStockGrossAmt();
                outSideTotal.put("outStockGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("outStockGrossAmt", outSide.getOutStockGrossAmt());
            }
            if (outSideTotal.get("refoundGrossAmt") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("refoundGrossAmt").toString());
                BigDecimal add = outSide.getRefoundGrossAmt();
                outSideTotal.put("refoundGrossAmt", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("refoundGrossAmt", outSide.getRefoundGrossAmt());
            }
            if (outSideTotal.get("grossAmtRightTotal") != null) {
                BigDecimal count = new BigDecimal(outSideTotal.get("grossAmtRightTotal").toString());
                BigDecimal add = outSide.getGrossAmtRightTotal();
                outSideTotal.put("grossAmtRightTotal", count.add(add).setScale(2, BigDecimal.ROUND_HALF_UP));
            } else {
                outSideTotal.put("grossAmtRightTotal", outSide.getGrossAmtRightTotal());
            }

        }
        if (!outSideTotal.isEmpty()) {
            if (outSideTotal.get("outStockGrossAmt") != null && outSideTotal.get("outStockDBE") != null) {
                BigDecimal grossAmtOutStockTotalOutSide = new BigDecimal(outSideTotal.get("outStockGrossAmt").toString());
                BigDecimal dbeOutStockTotalOutSide = new BigDecimal(outSideTotal.get("outStockDBE").toString());
                if (dbeOutStockTotalOutSide.compareTo(BigDecimal.ZERO) != 0) {
                    String grossOutStockRateOutSide = grossAmtOutStockTotalOutSide.divide(dbeOutStockTotalOutSide, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    outSideTotal.put("outStockGrossRate", grossOutStockRateOutSide);
                }
            }
            if (outSideTotal.get("refoundGrossAmt") != null && outSideTotal.get("refoundDBE") != null) {
                BigDecimal grossAmtRefoundTotalOutSide = new BigDecimal(outSideTotal.get("refoundGrossAmt").toString());
                BigDecimal dbeRefoundTotalOutSide = new BigDecimal(outSideTotal.get("refoundDBE").toString());
                if (dbeRefoundTotalOutSide.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRefoundRateOutSide = grossAmtRefoundTotalOutSide.divide(dbeRefoundTotalOutSide, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    outSideTotal.put("refoundGrossRate", grossRefoundRateOutSide);
                }
            }
            if (outSideTotal.get("grossAmtRightTotal") != null && outSideTotal.get("dbeRightTotal") != null) {
                BigDecimal grossAmtRightTotalOutSide = new BigDecimal(outSideTotal.get("grossAmtRightTotal").toString());
                BigDecimal dbeRightTotalOutSide = new BigDecimal(outSideTotal.get("dbeRightTotal").toString());
                if (dbeRightTotalOutSide.compareTo(BigDecimal.ZERO) != 0) {
                    String grossRightRateOutSide = grossAmtRightTotalOutSide.divide(dbeRightTotalOutSide, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP) + "%";
                    outSideTotal.put("grossRateRightTotal", grossRightRateOutSide);
                }
            }
        }
            StoreDistributaionDataOutData outSideDataTotal=new StoreDistributaionDataOutData();
        if (!outSideData.isEmpty()) {
            outSideDataTotal.setCusType("外部小计");
            outSideDataTotal.setOutStockDBE(new BigDecimal(outSideTotal.get("outStockDBE").toString()));
            outSideDataTotal.setRefoundDBE(new BigDecimal(outSideTotal.get("refoundDBE").toString()));
            outSideDataTotal.setDbeRightTotal(new BigDecimal(outSideTotal.get("dbeRightTotal").toString()));
            outSideDataTotal.setOutStockMOV(new BigDecimal(outSideTotal.get("outStockMOV").toString()));
            outSideDataTotal.setRefoundMOV(new BigDecimal(outSideTotal.get("refoundMOV").toString()));
            outSideDataTotal.setMovRightTotal(new BigDecimal(outSideTotal.get("movRightTotal").toString()));
            outSideDataTotal.setOutStockGrossAmt(new BigDecimal(outSideTotal.get("outStockGrossAmt").toString()));
            outSideDataTotal.setRefoundGrossAmt(new BigDecimal(outSideTotal.get("refoundGrossAmt").toString()));
            outSideDataTotal.setGrossAmtRightTotal(new BigDecimal(outSideTotal.get("grossAmtRightTotal").toString()));
            outSideDataTotal.setOutStockGrossRate(outSideTotal.get("outStockGrossRate").toString());
            outSideDataTotal.setRefoundGrossRate(outSideTotal.get("refoundGrossRate").toString());
            outSideDataTotal.setGrossRateRightTotal(outSideTotal.get("grossRateRightTotal").toString());
            allData.add(outSideDataTotal);
        }

        Map<String, Object> allDataTotal = new HashMap<>();
        allDataTotal.put("outStockDBE",0);
        allDataTotal.put("refoundDBE",0);
        allDataTotal.put("dbeRightTotal",0);
        allDataTotal.put("outStockMOV",0);
        allDataTotal.put("refoundMOV",0);
        allDataTotal.put("movRightTotal",0);
        allDataTotal.put("outStockGrossAmt",0);
        allDataTotal.put("refoundGrossAmt",0);
        allDataTotal.put("grossAmtRightTotal",0);
        allDataTotal.put("outStockGrossRate","0.00%");
        allDataTotal.put("refoundGrossRate","0.00%");
        allDataTotal.put("grossRateRightTotal","0.00%");
        if (inSideDataTotal.getCusType() != null && outSideDataTotal.getCusType()  != null) {
            StoreDistributaionDataOutData allTotal = new StoreDistributaionDataOutData();
            allTotal.setCusType("合计");
            allTotal.setOutStockDBE(inSideDataTotal.getOutStockDBE().add(outSideDataTotal.getOutStockDBE()));
            allTotal.setRefoundDBE(inSideDataTotal.getRefoundDBE().add(outSideDataTotal.getRefoundDBE()));
            allTotal.setDbeRightTotal(inSideDataTotal.getDbeRightTotal().add(outSideDataTotal.getDbeRightTotal()));
            allTotal.setOutStockMOV(inSideDataTotal.getOutStockMOV().add(outSideDataTotal.getOutStockMOV()));
            allTotal.setRefoundMOV(inSideDataTotal.getRefoundMOV().add(outSideDataTotal.getRefoundMOV()));
            allTotal.setMovRightTotal(inSideDataTotal.getMovRightTotal().add(outSideDataTotal.getMovRightTotal()));
            allTotal.setOutStockGrossAmt(inSideDataTotal.getOutStockGrossAmt().add(outSideDataTotal.getOutStockGrossAmt()));
            allTotal.setRefoundGrossAmt(inSideDataTotal.getRefoundGrossAmt().add(outSideDataTotal.getRefoundGrossAmt()));
            allTotal.setGrossAmtRightTotal(inSideDataTotal.getGrossAmtRightTotal().add(outSideDataTotal.getGrossAmtRightTotal()));
            allTotal.setOutStockGrossRate(allTotal.getOutStockGrossAmt().divide(allTotal.getOutStockDBE(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP) + "%");
            allTotal.setRefoundGrossRate(allTotal.getRefoundGrossAmt().divide(allTotal.getRefoundDBE(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP) + "%");
            allTotal.setGrossRateRightTotal(allTotal.getGrossAmtRightTotal().divide(allTotal.getDbeRightTotal(), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP) + "%");
            allData.add(allTotal);
        }
        return allData;
    }
    @Autowired
    private ReportFormsService reportFormsService;

    /**
     * 供应商单据明细列表
     *
     * @param
     * @param data 请求参数
     * @return
     */
    @Override
    public PageInfo<SelectSupplierDetailPageListResponse> selectSupplierDetailPageList(SelectSupplierDetailPageListData data) {
        //校验逻辑
        this.checkTheFunction(data);
        PageInfo<SelectSupplierDetailPageListResponse> pageInfo = new PageInfo<>();
        SupplierDealInData supplierDealInData = new SupplierDealInData();
        supplierDealInData.setClient(data.getClient());
        supplierDealInData.setStoCode(data.getStoCode());
        supplierDealInData.setStartDate(data.getBeginTime());
        supplierDealInData.setEndDate(data.getEndTime());
        supplierDealInData.setSupCode(data.getSupSelfCode());
        supplierDealInData.setMatType(data.getMatType());


        PageInfo page = new PageInfo();
        List<SelectSupplierDetailPageListResponse> list = new ArrayList<>();
        List<SelectSupplierDealData> dataList = this.materialDocMapper.selectList(supplierDealInData);
        if (CollUtil.isEmpty(dataList)) {
            return pageInfo;
        }

        //包含退场单
        List<SelectSupplierDealData> gd = dataList.stream().filter(s -> Objects.equals(s.getMatType(), "GD")).collect(Collectors.toList());
        if(CollUtil.isNotEmpty(gd)){
            //根据商品id获取退场单
            List<SelectSupplierDealData> getGdList =  materialDocMapper.getGdList(supplierDealInData);
            if(CollUtil.isNotEmpty(getGdList)){
                for (SelectSupplierDealData gds : getGdList) {
                    for (SelectSupplierDealData gdList : dataList) {
                        if(Objects.equals(gds.getClient(),gdList.getDcOdClient())&& Objects.equals(gds.getStoCode(),gdList.getDcOdStoCode())
                                && Objects.equals(gds.getDnId(),gdList.getDcOdDnId()) && Objects.equals(gds.getProCode(),gdList.getDcOdProCode()) && Objects.equals(gds.getBatch(),gdList.getDcOdBatch()) && Objects.equals(Convert.toStr(gds.getQty()),gdList.getDcOdQty())){
                            gdList.setBillingAmount(gds.getBillingAmount());
                            gdList.setMatRateBat("0");
                            gdList.setMatBatAmt(Convert.toStr(gds.getBillingAmount()));
                            gdList.setDcOdQty(Convert.toStr(new BigDecimal(gdList.getDcOdQty()).multiply(new BigDecimal("-1"))));
                            //break;
                        }
                    }
                }
            }
        }

        //根据加盟商 业务单号 名称分组
        Map<String, List<SelectSupplierDealData>> map = dataList.stream().collect(
                Collectors.groupingBy(
                        s -> s.getClient() + '-' + s.getDnId() + '-' + s.getPoId()
                )
        );
        for (Map.Entry<String, List<SelectSupplierDealData>> entry : map.entrySet()) {
            List<SelectSupplierDealData> dealData = entry.getValue();
            //按照业务员再次分组
            if (CollUtil.isNotEmpty(dealData)) {
                BigDecimal amtSum = BigDecimal.ZERO;

                BigDecimal amtPrice = BigDecimal.ZERO;
                BigDecimal invoiceAmount = BigDecimal.ZERO;
                BigDecimal invoiceAmountSum = BigDecimal.ZERO;
                BigDecimal qty = BigDecimal.ZERO;
                BigDecimal qtyAmt = BigDecimal.ZERO;
                for (SelectSupplierDealData supplierDealData : dealData) {
                    qtyAmt = qtyAmt.add(new BigDecimal(supplierDealData.getDcOdQty()));
                    amtSum = amtSum.add(new BigDecimal(supplierDealData.getMatBatAmt()).add(new BigDecimal(supplierDealData.getMatRateBat())));
                    if (Objects.isNull(supplierDealData.getBatFph()) || StrUtil.isBlank(supplierDealData.getBatFph())) {
                        supplierDealData.setBatFph("0.00");
                    }

                    invoiceAmountSum = invoiceAmountSum.add(new BigDecimal(supplierDealData.getBatFph()));
                    SelectSupplierDetailPageListResponse response = new SelectSupplierDetailPageListResponse();
                    response.setDepId(supplierDealData.getStoCode() + "-" + supplierDealData.getStoName());
                    response.setDepIdName(supplierDealData.getStoCode());
                    response.setClient(supplierDealData.getClient());
                    response.setSupSelfCode(supplierDealData.getSupCode());
                    response.setSupSelfName(supplierDealData.getSupName());
                    response.setSalesmanCode(supplierDealData.getSuppNameCode());
                    response.setSalesmanName(supplierDealData.getSuppName());
                    response.setMatType(supplierDealData.getMatType());
                    response.setPostDate(supplierDealData.getPostDate());
                    response.setPoId(supplierDealData.getDnId());
                    response.setBillingAmount(new BigDecimal(supplierDealData.getMatBatAmt()).add(new BigDecimal(supplierDealData.getMatRateBat())).setScale(2, BigDecimal.ROUND_HALF_UP));
                    response.setInvoiceAmount(new BigDecimal(supplierDealData.getBatFph()).multiply(new BigDecimal(supplierDealData.getDcOdQty())));
                    list.add(response);
                }
            }
        }
        if (StrUtil.isNotBlank(data.getSalesmanName())) {
                if(StrUtil.isNotBlank(data.getSupSelfCode()) && StrUtil.isNotBlank(data.getSalesmanName())  && StrUtil.isNotBlank(data.getMatType())){
                    list = list.stream().
                                        filter(k ->Objects.equals(data.getSupSelfCode(),k.getSupSelfCode()) &&
                                                        Objects.equals(data.getMatType(),k.getMatType()) &&
                                                    Objects.equals(k.getSalesmanName(), data.getSalesmanName())).collect(Collectors.toList());
                }else if(StrUtil.isNotBlank(data.getSalesmanName())  && StrUtil.isNotBlank(data.getMatType())){
                    list = list.stream().
                            filter(k -> Objects.equals(data.getMatType(),k.getMatType()) && Objects.equals(k.getSalesmanName(), data.getSalesmanName())).collect(Collectors.toList());
                }else if(StrUtil.isNotBlank(data.getMatType())){
                    list = list.stream().filter(k -> Objects.equals(data.getMatType(),k.getMatType())).collect(Collectors.toList());
                }else if(StrUtil.isNotBlank(data.getSupSelfCode())){
                    list = list.stream().filter(k ->Objects.equals(data.getSupSelfCode(),k.getSupSelfCode())).collect(Collectors.toList());
                }else if(StrUtil.isNotBlank(data.getSalesmanName())){
                    list = list.stream().filter(k -> Objects.equals(k.getSalesmanName(), data.getSalesmanName())).collect(Collectors.toList());
                }
        }
        page.setList(list);
        page.setListNum(this.total(list));
        return page;

    }


    /**
     * 计算合计金额
     *
     * @param list
     * @return
     */
    private Map<String, BigDecimal> total(List<SelectSupplierDetailPageListResponse> list) {
        Map<String, BigDecimal> map = new HashMap<>();
        if (CollUtil.isNotEmpty(list)) {
            BigDecimal billingAmount = BigDecimal.ZERO;
            BigDecimal invoiceAmount = BigDecimal.ZERO;
            for (SelectSupplierDetailPageListResponse response : list) {
                if(Objects.equals(response.getMatType(),"CD")){
                    response.setMatType("采购");
                }else if(Objects.equals(response.getMatType(),"GD")){
                    response.setMatType("退厂");
                    //response.setBillingAmount(BigDecimal.ZERO.subtract(response.getBillingAmount()));
                }else if(Objects.equals(response.getMatType(),"CX")){
                    response.setMatType("代煎采购");
                }
                billingAmount = billingAmount.add(response.getBillingAmount());
                invoiceAmount = invoiceAmount.add(response.getInvoiceAmount());
            }
            map.put("billingAmount", billingAmount);
            map.put("invoiceAmount", invoiceAmount);
        }
        return map;
    }

    /**
     * 校验逻辑
     *
     * @param data
     */
    private void checkTheFunction(SelectSupplierDetailPageListData data) {
        if (StrUtil.isBlank(data.getStoCode())) {
            throw new BusinessException("查询仓库地点或门店编码必选其一");
        }
        if (StrUtil.isBlank(data.getBeginTime())) {
            throw new BusinessException("查询开始时间不能为空");
        }
        if (StrUtil.isBlank(data.getEndTime())) {
            throw new BusinessException("查询结束时间不能为空");
        }
        data.setBeginTime(cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.parse(data.getBeginTime()), "yyyyMMdd"));
        data.setEndTime(cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.parse(data.getEndTime()), "yyyyMMdd"));
    }

    @Override
    public SXSSFWorkbook export(SelectSupplierDetailPageListData datas) {
        if(StrUtil.isNotBlank(datas.getDepId())){
            datas.setStoCode(datas.getDepId());
        }
        PageInfo<SelectSupplierDetailPageListResponse> pageInfo = selectSupplierDetailPageList(datas);
        if (Objects.isNull(pageInfo) || CollUtil.isEmpty(pageInfo.getList())) {
            throw new BusinessException("提示：导出数据为空");
        }
        /*if (CollUtil.isEmpty(pageInfo.getList())) {
            throw new BusinessException("提示：导出数据为空");
        }*/
        List<SelectSupplierDetailPageListResponse> data = pageInfo.getList();
        Map<String, BigDecimal> listNum = (Map<String, BigDecimal>) pageInfo.getListNum();
        SelectSupplierDetailPageListResponse resp = new SelectSupplierDetailPageListResponse();
        resp.setDepId("合计");
        resp.setBillingAmount(listNum.get("billingAmount").setScale(2,BigDecimal.ROUND_HALF_UP));
        resp.setInvoiceAmount(listNum.get("invoiceAmount").setScale(2,BigDecimal.ROUND_HALF_UP));
        data.add(resp);
        // 获取数据
        SXSSFWorkbook workbook = new SXSSFWorkbook(data.size() + 100);
        org.apache.poi.ss.usermodel.Sheet sheet = workbook.createSheet("供应商单据明细列表");
        //设置标题
        String[] keys = CommonConstant.GAIA_OAS_SICKNESS_SUPPLIER;
        // 设置表头
        Row titleRow = sheet.createRow(0);
        int i = 0;
        for (String key : keys) {
            Cell cell = titleRow.createCell(i);
            cell.setCellValue(key);
            i++;
        }
        for (int m = 1; m <= data.size(); m++) {
            Row dataRow = sheet.createRow(m);
            SelectSupplierDetailPageListResponse response = data.get(m - 1);
            // 列数
            int increase = 0;
            Cell cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getDepId()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSupSelfCode()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSupSelfName()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSalesmanCode()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSalesmanName()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getMatType()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getPostDate()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getPoId()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(Double.valueOf(ExcelUtils.tranColumn(response.getBillingAmount().setScale(2,BigDecimal.ROUND_HALF_UP))));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(Double.valueOf(ExcelUtils.tranColumn(response.getInvoiceAmount().setScale(2,BigDecimal.ROUND_HALF_UP))));
        }
        //自适应列宽
        ExcelUtils.setSizeColumnLength(sheet, keys.length);
        return workbook;
    }

    /**
     * 供应商单据汇总列表
     * @param
     * @param data
     * @return
     */
    @Override
    public PageInfo<SelectSupplierDetailPageListResponse> selectSupplierSummaryPageList(SelectSupplierSummaryPageList data) {
        SelectSupplierDetailPageListData selectSupplier = new SelectSupplierDetailPageListData();
        BeanUtil.copyProperties(data,selectSupplier);
        if(StrUtil.isNotBlank(data.getDepId())){
            data.setStoCode(data.getDepId());
        }
        //校验
        this.checkTheFunction(selectSupplier);

        PageInfo<SelectSupplierDetailPageListResponse> info = selectSupplierDetailPageList(selectSupplier);
        if(Objects.isNull(info) || CollUtil.isEmpty(info.getList())){
            return info;
        }
        List<SelectSupplierDetailPageListResponse> infoList = info.getList();
        //根据加盟商 地点 供应商编码 维度分组
        Map<String, List<SelectSupplierDetailPageListResponse>> map = infoList.stream().collect(
                Collectors.groupingBy(
                        gaiaBillOfAp -> gaiaBillOfAp.getClient() + '-' + gaiaBillOfAp.getDepIdName() + '-' + gaiaBillOfAp.getSupSelfCode()
                )
        );
        List<SelectSupplierDetailPageListResponse> list = new ArrayList<>();
        Map<String,Object> amtMap = new HashMap<>();
        for (Map.Entry<String, List<SelectSupplierDetailPageListResponse>> listEntry : map.entrySet()) {
            List<SelectSupplierDetailPageListResponse> responses = listEntry.getValue();
            // //根据加盟商 地点 供应商编码 业务员 维度分组
            Map<String, List<SelectSupplierDetailPageListResponse>> stringListMap = responses.stream().collect(
                    Collectors.groupingBy(
                            gaiaBillOfAp -> gaiaBillOfAp.getClient() + '-' + gaiaBillOfAp.getDepIdName() + '-' + gaiaBillOfAp.getSupSelfCode()+ gaiaBillOfAp.getSalesmanCode()
                    )
            );
            for (Map.Entry<String, List<SelectSupplierDetailPageListResponse>> entry : stringListMap.entrySet()) {
                List<SelectSupplierDetailPageListResponse> value = entry.getValue();

                SelectSupplierDetailPageListResponse response = value.get(0);
                BigDecimal billingAmt = value.stream().map(s -> s.getBillingAmount()).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal reduce = value.stream().map(s -> s.getInvoiceAmount()).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
                response.setBillingAmount(billingAmt);
                response.setInvoiceAmount(reduce);
                list.add(response);
            }
            if (CollUtil.isNotEmpty(list)) {
                BigDecimal billingAmounts = BigDecimal.ZERO;
                BigDecimal invoiceAmounts = BigDecimal.ZERO;
                for (SelectSupplierDetailPageListResponse response : list) {
                    billingAmounts = billingAmounts.add(response.getBillingAmount());
                    invoiceAmounts = invoiceAmounts.add(response.getInvoiceAmount());
                }
                amtMap.put("billingAmount", billingAmounts);
                amtMap.put("invoiceAmount", invoiceAmounts);
                if (StrUtil.isNotBlank(data.getSalesmanName())) {
                    if(StrUtil.isNotBlank(data.getSupSelfCode()) && StrUtil.isNotBlank(data.getSalesmanName())){
                        list = list.stream().filter(k ->Objects.equals(data.getSupSelfCode(),k.getSupSelfCode()) &&
                                                        Objects.equals(k.getSalesmanName(), data.getSalesmanName())).collect(Collectors.toList());
                    }else if(StrUtil.isNotBlank(data.getSalesmanName())){
                        list = list.stream().filter(k -> Objects.equals(k.getSalesmanName(), data.getSalesmanName())).collect(Collectors.toList());
                    }else if(StrUtil.isNotBlank(data.getSupSelfCode())){
                        list = list.stream().filter(k ->Objects.equals(data.getSupSelfCode(),k.getSupSelfCode())).collect(Collectors.toList());
                    }
                }
            }

        }
        PageInfo pageInfo = new PageInfo<>();
        pageInfo.setList(list);
        pageInfo.setListNum(amtMap);
        return pageInfo;
    }

    /**
     * 供应商单据汇总列表
     * @param
     * @param data
     * @return
     */
    @Override
    public SXSSFWorkbook supplierExport(SelectSupplierSummaryPageList request) {

        if(StrUtil.isNotBlank(request.getDepId())){
            request.setStoCode(request.getDepId());
        }
        PageInfo<SelectSupplierDetailPageListResponse> pageInfo = selectSupplierSummaryPageList(request);
        if(Objects.isNull(pageInfo)){
            throw new BusinessException("提示：导出数据为空");
        }
        if(CollUtil.isEmpty(pageInfo.getList())){
            throw new BusinessException("提示：导出数据为空");
        }
        List<SelectSupplierDetailPageListResponse> data = pageInfo.getList();
        Map<String, BigDecimal> listNum = (Map<String, BigDecimal>) pageInfo.getListNum();
        SelectSupplierDetailPageListResponse resp = new SelectSupplierDetailPageListResponse();
        resp.setDepId("合计");
        resp.setBillingAmount(listNum.get("billingAmount").setScale(2,BigDecimal.ROUND_HALF_UP));
        resp.setInvoiceAmount(listNum.get("invoiceAmount").setScale(2,BigDecimal.ROUND_HALF_UP));
        data.add(resp);
        // 获取数据
        SXSSFWorkbook workbook = new SXSSFWorkbook(data.size() + 100);
        org.apache.poi.ss.usermodel.Sheet sheet = workbook.createSheet("供应商汇总查询");
        //设置标题
        String[] keys = CommonConstant.GAIA_OAS_SICKNESS;
        // 设置表头
        Row titleRow = sheet.createRow(0);
        int i = 0;
        for (String key : keys) {
            Cell cell = titleRow.createCell(i);
            cell.setCellValue(key);
            i++;
        }
        for (int m = 1; m <= data.size(); m++) {
            Row dataRow = sheet.createRow(m);
            SelectSupplierDetailPageListResponse response = data.get(m - 1);
            // 列数
            int increase = 0;
            Cell cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getDepId()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSupSelfCode()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSupSelfName()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSalesmanCode()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSalesmanName()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(Double.valueOf(ExcelUtils.tranColumn(response.getBillingAmount().setScale(2,BigDecimal.ROUND_HALF_UP))));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(Double.valueOf(ExcelUtils.tranColumn(response.getInvoiceAmount().setScale(2,BigDecimal.ROUND_HALF_UP))));
        }
        //自适应列宽
        ExcelUtils.setSizeColumnLength(sheet, keys.length);
        return workbook;
    }

    @Override
    public PageInfo<InventoryChangeCheckOutData> selectInventoryStockCheckListBySto(InventoryChangeCheckInData data) {
            if (CollUtil.isEmpty(data.getSiteCodeList())) {
                throw new BusinessException("门店必填");
            }
            List<StoClassOutData> stoList = setValue(data);
            //返回对象
            List<InventoryChangeCheckOutData> outData=new ArrayList<>();
            //上个月库存金额
            List<InventoryChangeCheckOutData> startAmtList=this.materialDocMapper.selectInventoryStockCheckAmtStoStartList(data);
            //求这个月初到开始日期的剩余期间
            List<InventoryChangeCheckOutData> restStartAmtList=getRestStartDataList(data);
            //合并上个月库存和剩余期间得到期初
            startAmtList=mergeStartData(startAmtList,restStartAmtList);
            //求开始日期到结束日期的期间发生
            List<InventoryChangeCheckOutData> betweenAmt=this.materialDocMapper.selectInventoryStockCheckStoBetweenAmt(data);
            //合并期初、期间
            for (InventoryChangeCheckOutData start : startAmtList) {
                for (InventoryChangeCheckOutData bet : betweenAmt) {
                    if (start.getSiteCode().equals(bet.getSiteCode())) {

                        start.setBetweenQc(bet.getBetweenQc());
                        start.setBetweenProductIn(bet.getBetweenProductIn());
                        start.setBetweenProductRefound(bet.getBetweenProductRefound());
                        start.setBetweenDistribution(bet.getBetweenDistribution());
                        start.setBetweenStockRefound(bet.getBetweenStockRefound());
                        start.setBetweenSale(bet.getBetweenSale());
                        start.setBetweenLoss(bet.getBetweenLoss());
                        start.setEndAmt(
                                BigDecimal.ZERO
                                .add(start.getStartAmt()==null?BigDecimal.ZERO:start.getStartAmt())
                                .add(bet.getBetweenProductIn()==null?BigDecimal.ZERO:bet.getBetweenProductIn())
                                .add(bet.getBetweenProductRefound()==null?BigDecimal.ZERO:bet.getBetweenProductRefound())
                                .add(bet.getBetweenDistribution()==null?BigDecimal.ZERO:bet.getBetweenDistribution())
                                .add(bet.getBetweenStockRefound()==null?BigDecimal.ZERO:bet.getBetweenStockRefound())
                                .add(bet.getBetweenSale()==null?BigDecimal.ZERO:bet.getBetweenSale())
                                .add(bet.getBetweenLoss()==null?BigDecimal.ZERO:bet.getBetweenLoss())
                                .add(bet.getBetweenQc()==null?BigDecimal.ZERO:bet.getBetweenQc())
                        );
                        outData.add(start);
                    }
                }
            }
            //如果没有期初为空
            if (CollUtil.isEmpty(startAmtList)) {
                for (InventoryChangeCheckOutData bet : betweenAmt) {
                    bet.setStartAmt(BigDecimal.ZERO);
                    bet.setEndAmt(
                            bet.getBetweenProductIn()==null?BigDecimal.ZERO:bet.getBetweenProductIn()
                            .add(bet.getBetweenProductRefound()==null?BigDecimal.ZERO:bet.getBetweenProductRefound())
                            .add(bet.getBetweenDistribution()==null?BigDecimal.ZERO:bet.getBetweenDistribution())
                            .add(bet.getBetweenStockRefound()==null?BigDecimal.ZERO:bet.getBetweenStockRefound())
                            .add(bet.getBetweenSale()==null?BigDecimal.ZERO:bet.getBetweenSale())
                            .add(bet.getBetweenLoss()==null?BigDecimal.ZERO:bet.getBetweenLoss())
                            .add(bet.getBetweenQc()==null?BigDecimal.ZERO:bet.getBetweenQc())
                    );
                }
                outData.addAll(betweenAmt);
            }
            List<InventoryChangeCheckOutData> newOutData = new ArrayList<>();
            for (InventoryChangeCheckOutData changeCheckOutData : outData){
                for (StoClassOutData stoClassOutData : stoList){
                    if (changeCheckOutData.getSiteCode().equals(stoClassOutData.getStoCode())){
                        changeCheckOutData.setStoAttribute(stoClassOutData.getStoAttribute());
                        changeCheckOutData.setDirectManaged(stoClassOutData.getDirectManaged());
                        changeCheckOutData.setShopType(stoClassOutData.getShopType());
                        changeCheckOutData.setStoIfDtp(stoClassOutData.getStoIfDtp());
                        changeCheckOutData.setStoIfMedical(stoClassOutData.getStoIfMedical());
                        changeCheckOutData.setStoreEfficiencyLevel(stoClassOutData.getStoreEfficiencyLevel());
                        changeCheckOutData.setStoTaxClass(stoClassOutData.getStoTaxClass());
                        changeCheckOutData.setManagementArea(stoClassOutData.getManagementArea());
                        newOutData.add(changeCheckOutData);
                    }
                }
            }
        InventoryChangeCheckOutData total=computeTotal(newOutData);
        return new PageInfo<>(newOutData,total);
    }

    private List<StoClassOutData> setValue(InventoryChangeCheckInData data) {
        //匹配值
        Set<String> stoGssgTypeSet = new HashSet<>();
        boolean noChooseFlag = true;
        if (data.getStoGssgType() != null) {
            List<GaiaStoreCategoryType> stoGssgTypes = new ArrayList<>();
            for (String s : data.getStoGssgType().split(StrUtil.COMMA)) {
                String[] str = s.split(StrUtil.UNDERLINE);
                GaiaStoreCategoryType gaiaStoreCategoryType = new GaiaStoreCategoryType();
                gaiaStoreCategoryType.setGssgType(str[0]);
                gaiaStoreCategoryType.setGssgId(str[1]);
                stoGssgTypes.add(gaiaStoreCategoryType);
                stoGssgTypeSet.add(str[0]);
            }
            data.setStoGssgTypes(stoGssgTypes);
            noChooseFlag = false;
        }
        if (data.getStoAttribute() != null) {
            noChooseFlag = false;
            data.setStoAttributes(Arrays.asList(data.getStoAttribute().split(StrUtil.COMMA)));
        }
        if (data.getStoIfMedical() != null) {
            noChooseFlag = false;
            data.setStoIfMedicals(Arrays.asList(data.getStoIfMedical().split(StrUtil.COMMA)));
        }
        if (data.getStoTaxClass() != null) {
            noChooseFlag = false;
            data.setStoTaxClasss(Arrays.asList(data.getStoTaxClass().split(StrUtil.COMMA)));
        }
        if (data.getStoIfDtp() != null) {
            noChooseFlag = false;
            data.setStoIfDtps(Arrays.asList(data.getStoIfDtp().split(StrUtil.COMMA)));
        }
        List<StoClassOutData> stoList = gaiaStoreDataMapper.getStoreCodeByStoClass(data);
        List<GaiaStoreCategoryType> storeCategoryByClient = gaiaSdStoresGroupMapper.selectStoreCategoryByClient(data.getClient());

        for (StoClassOutData sto : stoList) {
            //转换
            if (sto.getStoAttribute() != null || noChooseFlag) {
                sto.setStoAttribute(StoreAttributeEnum.getName(sto.getStoAttribute()));
            }
            if (sto.getStoIfMedical() != null || noChooseFlag) {
                sto.setStoIfMedical(StoreMedicalEnum.getName(sto.getStoIfMedical()));
            }
            if (sto.getStoIfDtp() != null || noChooseFlag) {
                sto.setStoIfDtp(StoreDTPEnum.getName(sto.getStoIfDtp()));
            }
            if (sto.getStoTaxClass() != null || noChooseFlag) {
                sto.setStoTaxClass(StoreTaxClassEnum.getName(sto.getStoTaxClass()));
            }
            List<GaiaStoreCategoryType> collect = storeCategoryByClient.stream().filter(t -> t.getGssgBrId().equals(sto.getStoCode())).collect(Collectors.toList());
            Set<String> tmpStoGssgTypeSet = new HashSet<>(stoGssgTypeSet);
            for (GaiaStoreCategoryType storeCategoryType : collect) {
                boolean flag = false;
                if (noChooseFlag) {
                    if (storeCategoryType.getGssgType().contains("DX0001")) {
                        sto.setShopType(storeCategoryType.getGssgIdName());
                    } else if (storeCategoryType.getGssgType().contains("DX0002")) {
                        sto.setStoreEfficiencyLevel(storeCategoryType.getGssgIdName());
                    } else if (storeCategoryType.getGssgType().contains("DX0003")) {
                        sto.setDirectManaged(storeCategoryType.getGssgIdName());
                    } else if (storeCategoryType.getGssgType().contains("DX0004")) {
                        sto.setManagementArea(storeCategoryType.getGssgIdName());
                    }
                } else {
                    if (!stoGssgTypeSet.isEmpty()) {
                        if (tmpStoGssgTypeSet.contains("DX0001") && "DX0001".equals(storeCategoryType.getGssgType())) {
                            sto.setShopType(storeCategoryType.getGssgIdName());
                            tmpStoGssgTypeSet.remove("DX0001");
                        } else if (tmpStoGssgTypeSet.contains("DX0002") && "DX0002".equals(storeCategoryType.getGssgType())) {
                            sto.setStoreEfficiencyLevel(storeCategoryType.getGssgIdName());
                            tmpStoGssgTypeSet.remove("DX0002");
                        } else if (tmpStoGssgTypeSet.contains("DX0003") && "DX0003".equals(storeCategoryType.getGssgType())) {
                            sto.setDirectManaged(storeCategoryType.getGssgIdName());
                            tmpStoGssgTypeSet.remove("DX0003");
                        } else if (tmpStoGssgTypeSet.contains("DX0004") && "DX0004".equals(storeCategoryType.getGssgType())) {
                            sto.setManagementArea(storeCategoryType.getGssgIdName());
                            tmpStoGssgTypeSet.remove("DX0004");
                        }
                    }
                }
            }
        }
        return stoList;
    }


    //门店:合并上个月库存期初加这个月月初到开始日期前一天的期间
    private List<InventoryChangeCheckOutData> mergeStartData(List<InventoryChangeCheckOutData> startAmtList, List<InventoryChangeCheckOutData> restStartAmtList) {
        for (InventoryChangeCheckOutData startQc : startAmtList) {

            for (InventoryChangeCheckOutData restAmt : restStartAmtList) {
                if (startQc.getSiteCode().equals(restAmt.getSiteCode())) {
                    startQc.setStartAmt(
                            startQc.getStartAmt()==null?BigDecimal.ZERO:startQc.getStartAmt()
                            .add(restAmt.getBetweenProductIn()==null?BigDecimal.ZERO:restAmt.getBetweenProductIn())
                            .add(restAmt.getBetweenProductRefound()==null?BigDecimal.ZERO:restAmt.getBetweenProductRefound())
                            .add(restAmt.getBetweenDistribution()==null?BigDecimal.ZERO:restAmt.getBetweenDistribution())
                            .add(restAmt.getBetweenStockRefound()==null?BigDecimal.ZERO:restAmt.getBetweenStockRefound())
                            .add(restAmt.getBetweenSale()==null?BigDecimal.ZERO:restAmt.getBetweenSale())
                            .add(restAmt.getBetweenLoss()==null?BigDecimal.ZERO:restAmt.getBetweenLoss())
                            .add(restAmt.getBetweenQc()==null?BigDecimal.ZERO:restAmt.getBetweenQc())
                    );
                }
            }
        }
        List<String> siteCodeList = startAmtList.stream().map(InventoryChangeCheckOutData::getSiteCode).collect(Collectors.toList());
        restStartAmtList.removeIf(rest->{
            return siteCodeList.contains(rest.getSiteCode());
        });
        //给默认值
        for (InventoryChangeCheckOutData rest : restStartAmtList) {
            rest.setStartAmt(rest.getBetweenQc()==null?BigDecimal.ZERO:rest.getBetweenQc());
            rest.setEndAmt(
                    rest.getBetweenQc()==null?BigDecimal.ZERO:rest.getBetweenQc()
                    .add(rest.getBetweenProductIn()==null?BigDecimal.ZERO:rest.getBetweenProductIn())
                    .add(rest.getBetweenProductRefound()==null?BigDecimal.ZERO:rest.getBetweenProductRefound())
                    .add(rest.getBetweenDistribution()==null?BigDecimal.ZERO:rest.getBetweenDistribution())
                    .add(rest.getBetweenStockRefound()==null?BigDecimal.ZERO:rest.getBetweenStockRefound())
                    .add(rest.getBetweenSale()==null?BigDecimal.ZERO:rest.getBetweenSale())
                    .add(rest.getBetweenLoss()==null?BigDecimal.ZERO:rest.getBetweenLoss())
            );
        }
        startAmtList.addAll(restStartAmtList);
        return startAmtList;
    }

    /**
     * 求门店剩余期初数据
     * @param data
     * @return
     */
    private List<InventoryChangeCheckOutData> getRestStartDataList(InventoryChangeCheckInData data) {
        List<InventoryChangeCheckOutData> restStartAmtList=new ArrayList<>();
        InventoryChangeCheckInData restInData=new InventoryChangeCheckInData();
        BeanUtil.copyProperties(data,restInData);
        String startDate = data.getStartDate();
        //求开始日期对应月份的1号
        String beginOfMonth = cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.beginOfMonth(cn.hutool.core.date.DateUtil.parse(startDate, "yyyyMMdd")), "yyyyMMdd");
        //求开始日期的前一天
        String theDayBeforeStartDate = cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.offsetDay(cn.hutool.core.date.DateUtil.parse(startDate), -1), "yyyyMMdd");
        if (startDate.equals(beginOfMonth)) {//如果开始日期就是1号那么就没有门店剩余期初数据
            return restStartAmtList;
        }
        restInData.setStartDate(beginOfMonth);
        restInData.setEndDate(theDayBeforeStartDate);
        restStartAmtList = this.materialDocMapper.selectInventoryStockCheckStoBetweenAmt(restInData);
        return restStartAmtList;
    }

    @Override
    public PageInfo<InventoryChangeCheckOutData> selectInventoryStockCheckListByDc(InventoryChangeCheckInData data) {
            if (CollUtil.isEmpty(data.getSiteCodeList())) {
                throw new BusinessException("仓库编码必填");
            }
            List<InventoryChangeCheckOutData> outData=new ArrayList<>();
            InventoryChangeCheckOutData startAmt = this.materialDocMapper.selectInventoryStockCheckAmtDcStartList(data);
            InventoryChangeCheckOutData betweenAmt=this.materialDocMapper.selectInventoryStockCheckDcBetweenAmt(data);
            if (ObjectUtil.isNull(betweenAmt)) {
                if (ObjectUtil.isNull(startAmt)) {
                    startAmt=new InventoryChangeCheckOutData();
                    startAmt.setSiteCode(data.getSiteCodeList().get(0));
                    String dcName=this.materialDocMapper.findDcNameByDcCode(data.getClient(),data.getSiteCodeList().get(0));
                    startAmt.setSiteName(dcName);
                    startAmt.setStartAmt(BigDecimal.ZERO);
                }
                startAmt.setBetweenProductIn(BigDecimal.ZERO);
                startAmt.setBetweenProductRefound(BigDecimal.ZERO);
                startAmt.setBetweenDistribution(BigDecimal.ZERO);
                startAmt.setBetweenStockRefound(BigDecimal.ZERO);
                startAmt.setBetweenSale(BigDecimal.ZERO);
                startAmt.setBetweenLoss(BigDecimal.ZERO);
                startAmt.setBetweenQc(BigDecimal.ZERO);
            } else {
                if (ObjectUtil.isNull(startAmt)) {
                    startAmt=new InventoryChangeCheckOutData();
                    startAmt.setSiteCode(betweenAmt.getSiteCode());
                    startAmt.setSiteName(betweenAmt.getSiteName());
                    startAmt.setStartAmt(BigDecimal.ZERO);
                }
                startAmt.setBetweenProductIn(betweenAmt.getBetweenProductIn()==null?BigDecimal.ZERO:betweenAmt.getBetweenProductIn());
                startAmt.setBetweenProductRefound(betweenAmt.getBetweenProductRefound()==null?BigDecimal.ZERO:betweenAmt.getBetweenProductRefound());
                startAmt.setBetweenDistribution(betweenAmt.getBetweenDistribution()==null?BigDecimal.ZERO:betweenAmt.getBetweenDistribution());
                startAmt.setBetweenStockRefound(betweenAmt.getBetweenStockRefound()==null?BigDecimal.ZERO:betweenAmt.getBetweenStockRefound());
                startAmt.setBetweenSale(betweenAmt.getBetweenSale()==null?BigDecimal.ZERO:betweenAmt.getBetweenSale());
                startAmt.setBetweenLoss(betweenAmt.getBetweenLoss()==null?BigDecimal.ZERO:betweenAmt.getBetweenLoss());
                startAmt.setBetweenQc(betweenAmt.getBetweenQc()==null?BigDecimal.ZERO:betweenAmt.getBetweenQc());
            }

            if (null != betweenAmt) {
                startAmt.setEndAmt(
                        startAmt.getStartAmt() == null ? BigDecimal.ZERO : startAmt.getStartAmt()
                                .add(betweenAmt.getBetweenProductIn() == null ? BigDecimal.ZERO : betweenAmt.getBetweenProductIn())
                                .add(betweenAmt.getBetweenProductRefound() == null ? BigDecimal.ZERO : betweenAmt.getBetweenProductRefound())
                                .add(betweenAmt.getBetweenDistribution() == null ? BigDecimal.ZERO : betweenAmt.getBetweenDistribution())
                                .add(betweenAmt.getBetweenStockRefound() == null ? BigDecimal.ZERO : betweenAmt.getBetweenStockRefound())
                                .add(betweenAmt.getBetweenSale() == null ? BigDecimal.ZERO : betweenAmt.getBetweenSale())
                                .add(betweenAmt.getBetweenLoss() == null ? BigDecimal.ZERO : betweenAmt.getBetweenLoss())
                                .add(betweenAmt.getBetweenQc() == null ? BigDecimal.ZERO : betweenAmt.getBetweenQc())
                );
            } else {
                startAmt.setEndAmt(startAmt.getStartAmt());
                startAmt.setBetweenProductIn(BigDecimal.ZERO);
                startAmt.setBetweenProductRefound(BigDecimal.ZERO);
                startAmt.setBetweenDistribution(BigDecimal.ZERO);
                startAmt.setBetweenStockRefound(BigDecimal.ZERO);
                startAmt.setBetweenSale(BigDecimal.ZERO);
                startAmt.setBetweenLoss(BigDecimal.ZERO);
                startAmt.setBetweenQc(BigDecimal.ZERO);
            }
            startAmt=formatBigDecimal(startAmt);
            outData.add(startAmt);

        InventoryChangeCheckOutData total=computeTotal(outData);
        return new PageInfo<>(outData,total);
    }

    private InventoryChangeCheckOutData formatBigDecimal(InventoryChangeCheckOutData startAmt) {
        startAmt.setStartAmt(NumberUtil.round(startAmt.getStartAmt(),2, RoundingMode.HALF_UP));
        startAmt.setBetweenProductIn(NumberUtil.round(startAmt.getBetweenProductIn(),2, RoundingMode.HALF_UP));
        startAmt.setBetweenProductRefound(NumberUtil.round(startAmt.getBetweenProductRefound(),2, RoundingMode.HALF_UP));
        startAmt.setBetweenDistribution(NumberUtil.round(startAmt.getBetweenDistribution(),2, RoundingMode.HALF_UP));
        startAmt.setBetweenStockRefound(NumberUtil.round(startAmt.getBetweenStockRefound(),2, RoundingMode.HALF_UP));
        startAmt.setBetweenSale(NumberUtil.round(startAmt.getBetweenSale(),2, RoundingMode.HALF_UP));
        startAmt.setBetweenLoss(NumberUtil.round(startAmt.getBetweenLoss(),2, RoundingMode.HALF_UP));
        startAmt.setBetweenQc(NumberUtil.round(startAmt.getBetweenQc(),2, RoundingMode.HALF_UP));
        startAmt.setEndAmt(NumberUtil.round(startAmt.getEndAmt(),2, RoundingMode.HALF_UP));
        return startAmt;
    }

    @Override
    public void checkListDcExport(InventoryChangeCheckInData data, HttpServletResponse response) {
        PageInfo<InventoryChangeCheckOutData> pageInfo = this.selectInventoryStockCheckListByDc(data);
        InventoryChangeCheckOutData total = pageInfo.getListNum();
        total.setSiteCode("合计");
        total.setSiteName("");
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = null;
        try {
            String fileName = URLEncoder.encode("期末仓库库存导出", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            excelWriter = EasyExcel.write(response.getOutputStream(),InventoryChangeCheckOutData.class).build();
            WriteSheet writeSheet = EasyExcel.writerSheet(0, 0 + "").build();
            excelWriter.write(pageInfo.getList(),writeSheet);
            excelWriter.write(CollUtil.newArrayList(total),writeSheet);
            excelWriter.finish();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void checkListStoExport(InventoryChangeCheckInData data, HttpServletResponse response) {
        PageInfo<InventoryChangeCheckOutData> pageInfo = this.selectInventoryStockCheckListBySto(data);
        InventoryChangeCheckOutData total = pageInfo.getListNum();
        total.setSiteCode("合计");
        total.setSiteName("");
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        ExcelWriter excelWriter = null;
        try {
            String fileName = URLEncoder.encode("期末门店库存导出", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            excelWriter = EasyExcel.write(response.getOutputStream(),InventoryChangeCheckOutData.class).build();
            WriteSheet writeSheet = EasyExcel.writerSheet(0, 0 + "").build();
            excelWriter.write(pageInfo.getList(),writeSheet);
            excelWriter.write(CollUtil.newArrayList(total),writeSheet);
            excelWriter.finish();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private InventoryChangeCheckOutData computeTotal(List<InventoryChangeCheckOutData> outData) {
        InventoryChangeCheckOutData total=new InventoryChangeCheckOutData();
        BigDecimal startAmt=BigDecimal.ZERO;
        BigDecimal betweenProductIn=BigDecimal.ZERO;
        BigDecimal betweenProductRefound=BigDecimal.ZERO;
        BigDecimal betweenDistribution=BigDecimal.ZERO;
        BigDecimal betweenStockRefound=BigDecimal.ZERO;
        BigDecimal betweenSale=BigDecimal.ZERO;
        BigDecimal betweenLoss=BigDecimal.ZERO;
        BigDecimal betweenQc=BigDecimal.ZERO;
        BigDecimal endAmt=BigDecimal.ZERO;
        if (CollUtil.isNotEmpty(outData)) {
            startAmt = outData.stream().map(InventoryChangeCheckOutData::getStartAmt).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
            betweenProductIn = outData.stream().map(InventoryChangeCheckOutData::getBetweenProductIn).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
            betweenProductRefound = outData.stream().map(InventoryChangeCheckOutData::getBetweenProductRefound).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
            betweenDistribution = outData.stream().map(InventoryChangeCheckOutData::getBetweenDistribution).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
            betweenStockRefound = outData.stream().map(InventoryChangeCheckOutData::getBetweenStockRefound).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
            betweenSale = outData.stream().map(InventoryChangeCheckOutData::getBetweenSale).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
            betweenLoss = outData.stream().map(InventoryChangeCheckOutData::getBetweenLoss).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
            betweenQc = outData.stream().map(InventoryChangeCheckOutData::getBetweenQc).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
            endAmt = outData.stream().map(InventoryChangeCheckOutData::getEndAmt).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_UP);
        }
        total.setStartAmt(startAmt);
        total.setBetweenProductIn(betweenProductIn);
        total.setBetweenProductRefound(betweenProductRefound);
        total.setBetweenDistribution(betweenDistribution);
        total.setBetweenStockRefound(betweenStockRefound);
        total.setBetweenSale(betweenSale);
        total.setBetweenLoss(betweenLoss);
        total.setBetweenQc(betweenQc);
        total.setEndAmt(endAmt);
        return total;
    }

    @Override
    public List<String> selectSoHeadRemark(FuzzyQueryForConditionInData conditionQuery) {
        return this.materialDocMapper.selectSoHeadRemark(conditionQuery);
    }


    @Override
    public PageInfo<WholesaleSaleOutData> selectDistributionInvoiceByBatch(WholesaleSaleInData inData) {
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("单据号或起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("单据号或结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getProCodes())) {
            inData.setProArr(inData.getProCodes().split("\\s+ |\\s+|,"));
        }
        if (CollUtil.isNotEmpty(inData.getMatTypes())) {
            List<String> matTypes = inData.getMatTypes();
            for (int i = 0; i < matTypes.size(); i++) {
                if (matTypes.get(i).equals("PD")) {
                    matTypes.add("PX");
                }
            }
        } else {
            inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD", "PD", "ND", "ED", "TD", "MD", "PX")));
        }
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        List<WholesaleSaleOutData> outData = this.materialDocMapper.selectDistributionInvoiceByBatch(inData);

        //新增销售员、抬头备注字段
        //销售员取值:GAIA_WHOLESALE_SALESMAN表GSS_NAME字段 用CLIENT+GSS_CODE关联
        //GSS_CODE取值:GAIA_SO_HEADER表GWS_CODE字段
        List<WholesaleSaleOutData> salemanList = this.materialDocMapper.selectWholesaleSalemanList(inData);
        for (WholesaleSaleOutData out : outData) {
            for (WholesaleSaleOutData saleOutData : salemanList) {
                if (out.getDnId().equals(saleOutData.getDnId())
                        && out.getMatType().equals(saleOutData.getMatType())) {
                    out.setGwoOwnerSaleMan(saleOutData.getGwoOwnerSaleMan());
                    out.setGwoOwnerSaleManUserId(saleOutData.getGwoOwnerSaleManUserId());
                    out.setSoHeaderRemark(saleOutData.getSoHeaderRemark());
                }
            }
        }
        if (StrUtil.isNotEmpty(inData.getGwoOwnerSaleMan())) {
            outData = outData.stream().filter(out -> {
                return StringUtils.isNotEmpty(out.getGwoOwnerSaleManUserId())
                        && out.getGwoOwnerSaleManUserId().equals(inData.getGwoOwnerSaleMan());
            }).collect(Collectors.toList());
        }
        if (StrUtil.isNotEmpty(inData.getSoHeaderRemark())) {
            outData = outData.stream().filter(out -> {
                return StringUtils.isNotEmpty(out.getSoHeaderRemark())
                        && out.getSoHeaderRemark().equals(inData.getSoHeaderRemark());
            }).collect(Collectors.toList());
        }

        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            WholesaleSaleOutTotal outNum = new WholesaleSaleOutTotal();
            for (WholesaleSaleOutData out : outData) {
                outNum.setQty(CommonUtil.stripTrailingZeros(outNum.getQty()).add(CommonUtil.stripTrailingZeros(out.getQty())));
                outNum.setBatAmt(CommonUtil.stripTrailingZeros(outNum.getBatAmt()).add(CommonUtil.stripTrailingZeros(out.getBatAmt())));
                outNum.setRateBat(CommonUtil.stripTrailingZeros(outNum.getRateBat()).add(CommonUtil.stripTrailingZeros(out.getRateBat())));
                outNum.setTotalAmount(CommonUtil.stripTrailingZeros(outNum.getTotalAmount()).add(CommonUtil.stripTrailingZeros(out.getTotalAmount())));
                outNum.setAddAmt(CommonUtil.stripTrailingZeros(outNum.getAddAmt()).add(CommonUtil.stripTrailingZeros(out.getAddAmt())));
                outNum.setAddTax(CommonUtil.stripTrailingZeros(outNum.getAddTax()).add(CommonUtil.stripTrailingZeros(out.getAddTax())));
                outNum.setAddtotalAmount(CommonUtil.stripTrailingZeros(outNum.getAddtotalAmount()).add(CommonUtil.stripTrailingZeros(out.getAddtotalAmount())));
                outNum.setMovAmt(CommonUtil.stripTrailingZeros(outNum.getMovAmt()).add(CommonUtil.stripTrailingZeros(out.getMovAmt())));
                outNum.setRateMov(CommonUtil.stripTrailingZeros(outNum.getRateMov()).add(CommonUtil.stripTrailingZeros(out.getRateMov())));
                outNum.setMovTotalAmount(CommonUtil.stripTrailingZeros(outNum.getMovTotalAmount()).add(CommonUtil.stripTrailingZeros(out.getMovTotalAmount())));

            }
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(outNum);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public PageInfo<WholesaleSaleOutData> selectDistributionInvoiceDetailByBatch(WholesaleSaleInData inData) {
        //输入单号可以不输入时间
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getStartDate())) {
            throw new BusinessException("单据号或起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getDnId()) && ObjectUtil.isEmpty(inData.getEndDate())) {
            throw new BusinessException("单据号或结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getProCodes())) {
            inData.setProArr(inData.getProCodes().split("\\s+ |\\s+|,"));
        }
         /*if (StringUtils.isNotEmpty(inData.getMatType())) {
            if (inData.getMatType().equals("XD")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD")));
            } else if (inData.getMatType().equals("ED")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("ED")));
            } else if (inData.getMatType().equals("PD")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("PD", "ND")));
            } else if (inData.getMatType().equals("TD")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("TD", "MD")));
            } else if (inData.getMatType().equals("PX")) {
                inData.setMatTypes(new ArrayList<String>(Arrays.asList("PX")));
            }
        } else {
            inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD", "PD", "ND", "ED", "TD", "MD", "PX")));
        }*/
        if (CollUtil.isEmpty(inData.getMatTypes())) {
            inData.setMatTypes(new ArrayList<String>(Arrays.asList("XD", "PD", "ND", "ED", "TD", "MD", "PX")));
        }
        if (ObjectUtil.isNotNull(inData.getPageNum()) && ObjectUtil.isNotNull(inData.getPageSize())) {
            PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        List<WholesaleSaleOutData> outData = this.materialDocMapper.selectDistributionInvoiceDetailByBatch(inData);
        //新增销售员、抬头备注字段
        //销售员取值:GAIA_WHOLESALE_SALESMAN表GSS_NAME字段 用CLIENT+GSS_CODE关联
        //GSS_CODE取值:GAIA_SO_HEADER表GWS_CODE字段
        List<WholesaleSaleOutData> salemanList = this.materialDocMapper.selectWholesaleDetailSalemanList(inData);
        for (WholesaleSaleOutData out : outData) {
            for (WholesaleSaleOutData saleOutData : salemanList) {
                if (out.getPostDate().equals(saleOutData.getPostDate())
                        && out.getProCode().equals(saleOutData.getProCode())
                        && out.getDnId().equals(saleOutData.getDnId())
                        && out.getMatType().equals(saleOutData.getMatType())) {
                    out.setGwoOwnerSaleMan(saleOutData.getGwoOwnerSaleMan());
                    out.setGwoOwnerSaleManUserId(saleOutData.getGwoOwnerSaleManUserId());
                    out.setSoHeaderRemark(saleOutData.getSoHeaderRemark());
                }
            }
        }
        if (StrUtil.isNotEmpty(inData.getGwoOwnerSaleMan())) {
            outData = outData.stream().filter(out -> {
                return StringUtils.isNotEmpty(out.getGwoOwnerSaleManUserId())
                        && out.getGwoOwnerSaleManUserId().equals(inData.getGwoOwnerSaleMan());
            }).collect(Collectors.toList());
        }
        if (StrUtil.isNotEmpty(inData.getSoHeaderRemark())) {
            outData = outData.stream().filter(out -> {
                return StringUtils.isNotEmpty(out.getSoHeaderRemark())
                        && out.getSoHeaderRemark().equals(inData.getSoHeaderRemark());
            }).collect(Collectors.toList());
        }
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            WholesaleSaleOutTotal outNum = new WholesaleSaleOutTotal();
            for (WholesaleSaleOutData out : outData) {
                outNum.setQty(CommonUtil.stripTrailingZeros(outNum.getQty()).add(CommonUtil.stripTrailingZeros(out.getQty())));
                outNum.setBatAmt(CommonUtil.stripTrailingZeros(outNum.getBatAmt()).add(CommonUtil.stripTrailingZeros(out.getBatAmt())));
                outNum.setRateBat(CommonUtil.stripTrailingZeros(outNum.getRateBat()).add(CommonUtil.stripTrailingZeros(out.getRateBat())));
                outNum.setTotalAmount(CommonUtil.stripTrailingZeros(outNum.getTotalAmount()).add(CommonUtil.stripTrailingZeros(out.getTotalAmount())));
                outNum.setAddAmt(CommonUtil.stripTrailingZeros(outNum.getAddAmt()).add(CommonUtil.stripTrailingZeros(out.getAddAmt())));
                outNum.setAddTax(CommonUtil.stripTrailingZeros(outNum.getAddTax()).add(CommonUtil.stripTrailingZeros(out.getAddTax())));
                outNum.setAddtotalAmount(CommonUtil.stripTrailingZeros(outNum.getAddtotalAmount()).add(CommonUtil.stripTrailingZeros(out.getAddtotalAmount())));
                outNum.setMovAmt(CommonUtil.stripTrailingZeros(outNum.getMovAmt()).add(CommonUtil.stripTrailingZeros(out.getMovAmt())));
                outNum.setRateMov(CommonUtil.stripTrailingZeros(outNum.getRateMov()).add(CommonUtil.stripTrailingZeros(out.getRateMov())));
                outNum.setMovTotalAmount(CommonUtil.stripTrailingZeros(outNum.getMovTotalAmount()).add(CommonUtil.stripTrailingZeros(out.getMovTotalAmount())));

            }
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(outNum);
        } else {
            pageInfo = new PageInfo();
        }


        return pageInfo;
    }

    @Override
    public void exportDistributionInvoiceByBatch(HttpServletResponse response, WholesaleSaleInData data) {
        PageInfo pageInfo = this.selectDistributionInvoiceByBatch(data);
        List<WholesaleSaleOutData> outDataList = pageInfo.getList();
        WholesaleSaleOutTotal total = (WholesaleSaleOutTotal) pageInfo.getListNum();
        if (CollUtil.isEmpty(outDataList)) {
            throw new BusinessException("导出数据不能为空");
        }
        List<WholesaleSaleSummaryExportOutData> dataList = outDataList.stream().map(outData -> {
            WholesaleSaleSummaryExportOutData exportOutData = new WholesaleSaleSummaryExportOutData();
            BeanUtil.copyProperties(outData, exportOutData);
            if (StrUtil.isNotEmpty(outData.getGwoOwnerSaleMan())) {
                exportOutData.setGwoOwnerSaleMan(outData.getGwoOwnerSaleManUserId()+"-"+outData.getGwoOwnerSaleMan());
            }
            exportOutData.setAddAmt(outData.getAddAmt() == null?BigDecimal.ZERO.setScale(4):outData.getAddAmt().setScale(4,RoundingMode.HALF_UP));
            exportOutData.setAddTax(outData.getAddTax() == null?BigDecimal.ZERO.setScale(4):outData.getAddTax().setScale(4,RoundingMode.HALF_UP));
            exportOutData.setAddtotalAmount(outData.getAddtotalAmount() == null?BigDecimal.ZERO.setScale(2):outData.getAddtotalAmount().setScale(2,RoundingMode.HALF_UP));
            return exportOutData;
        }).collect(Collectors.toList());
        WholesaleSaleSummaryExportOutData totalData = new WholesaleSaleSummaryExportOutData();
        BeanUtil.copyProperties(total, totalData);
        dataList.add(totalData);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = "配送单据(批次)导出";
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

        try {
            EasyExcel.write(response.getOutputStream(),WholesaleSaleSummaryExportOutData.class)
                    .registerWriteHandler(ExcelStyleUtil.getStyle())
                    .sheet(0).doWrite(dataList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exportDistributionInvoiceDetailByBatch(HttpServletResponse response, WholesaleSaleInData data) {
        PageInfo pageInfo = this.selectDistributionInvoiceDetailByBatch(data);
        List<WholesaleSaleOutData> outDataList = pageInfo.getList();
        WholesaleSaleOutTotal total = (WholesaleSaleOutTotal) pageInfo.getListNum();
        if (CollUtil.isEmpty(outDataList)) {
            throw new BusinessException("导出数据不能为空");
        }
        List<WholesaleSaleDetailExportOutData> dataList = outDataList.stream().map(outData -> {
            WholesaleSaleDetailExportOutData exportOutData = new WholesaleSaleDetailExportOutData();
            BeanUtil.copyProperties(outData, exportOutData);
            if (StrUtil.isNotEmpty(outData.getGwoOwnerSaleMan())) {
                exportOutData.setGwoOwnerSaleMan(outData.getGwoOwnerSaleManUserId()+"-"+outData.getGwoOwnerSaleMan());
            }
            exportOutData.setAddAmt(outData.getAddAmt() != null ? outData.getAddAmt().setScale(4, RoundingMode.HALF_UP) : BigDecimal.ZERO);
            exportOutData.setAddTax(outData.getAddTax() != null ? outData.getAddTax().setScale(4, RoundingMode.HALF_UP) : BigDecimal.ZERO);
            exportOutData.setAddtotalAmount(outData.getAddtotalAmount() != null ? outData.getAddtotalAmount().setScale(2, RoundingMode.HALF_UP) : BigDecimal.ZERO);
            return exportOutData;
        }).collect(Collectors.toList());
        WholesaleSaleDetailExportOutData totalData = new WholesaleSaleDetailExportOutData();
        BeanUtil.copyProperties(total, totalData);
        dataList.add(totalData);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = "配送单据(批次)明细导出";
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        try {
            EasyExcel.write(response.getOutputStream(), WholesaleSaleDetailExportOutData.class)
                    .registerWriteHandler(ExcelStyleUtil.getStyle())
                    .sheet(0).doWrite(dataList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
