package com.gys.business.service.data.disease;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MemberTagListResponse {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "会员卡号")
    private String cardId;

    @ApiModelProperty(value = "消费金额")
    private String amt;

    @ApiModelProperty(value = "消费的次数")
    private String saleTimes;

    @ApiModelProperty(value = "类型0疾病标签1成分标签")
    private String type;

    @ApiModelProperty(value = "计算月份")
    private String calcMonth;

    @ApiModelProperty(value = "更新日期")
    private String updateDate;

    @ApiModelProperty(value = "标签")
    private String tagId;
}
