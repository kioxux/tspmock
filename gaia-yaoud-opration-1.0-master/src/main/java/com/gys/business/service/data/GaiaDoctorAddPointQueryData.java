package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 10:21 2021/7/26
 * @Description：FX医生加点查询参数封装类
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GaiaDoctorAddPointQueryData {

    @ApiModelProperty(value = "用户")
    private String client;

    @ApiModelProperty(value = "开始日期")
    private String startDate;

    @ApiModelProperty(value = "结束日期")
    private String endDate;

    @ApiModelProperty(value = "医生编码数组集合")
    private List<String> gsdaDrIdList;
}
