package com.gys.business.service;

import com.gys.business.controller.app.form.PushForm;
import com.gys.business.mapper.entity.GaiaDcReplenishH;
import com.gys.business.service.data.DcReplenishDetailVO;
import com.gys.business.service.data.DcReplenishForm;
import com.gys.business.service.data.DcReplenishVO;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/21 17:10
 */
public interface GaiaDcReplenishHService {

    void saveDcReplenish(PushForm pushForm);

    List<DcReplenishVO> replenishList(DcReplenishForm replenishForm);

    GaiaDcReplenishH getUnique(GaiaDcReplenishH cond);

    List<DcReplenishDetailVO> getReplenishDetail(DcReplenishForm replenishForm);
}
