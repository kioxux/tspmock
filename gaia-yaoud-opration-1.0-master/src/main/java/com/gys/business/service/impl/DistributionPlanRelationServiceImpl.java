package com.gys.business.service.impl;

import com.gys.business.mapper.DistributionPlanRelationMapper;
import com.gys.business.mapper.entity.DistributionPlanRelation;
import com.gys.business.service.DistributionPlanRelationService;
import com.gys.common.exception.BusinessException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 15:11
 */
@Service("distributionPlanRelationService")
public class DistributionPlanRelationServiceImpl implements DistributionPlanRelationService {
    @Resource
    private DistributionPlanRelationMapper distributionPlanRelationMapper;

    @Override
    public DistributionPlanRelation getById(Long id) {
        return distributionPlanRelationMapper.getById(id);
    }

    @Override
    public DistributionPlanRelation add(DistributionPlanRelation distributionPlanRelation) {
        try {
            distributionPlanRelationMapper.add(distributionPlanRelation);
        } catch (Exception e) {
            throw new BusinessException("新增铺货计划详情关联关系异常", e);
        }
        return getById(distributionPlanRelation.getPlanDetailId());
    }
}
