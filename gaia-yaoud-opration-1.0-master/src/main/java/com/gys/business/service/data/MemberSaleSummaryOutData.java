package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xiaoyuan
 */
@Data
public class MemberSaleSummaryOutData implements Serializable {
    private static final long serialVersionUID = 4089783244442439959L;
    /**
     * 序号
     */
    @ApiModelProperty(value = "序号")
    private Integer index;

    /**
     * 销售日期
     */
    @ApiModelProperty(value = "销售日期")
    private String gpdhSaleDate;

    /**
     * 实收金额
     */
    @ApiModelProperty(value = "应收金额")
    private BigDecimal ssAmount;

    /**
     * 来客数
     */
    @ApiModelProperty(value = "来客数")
    private BigDecimal visitNum;

    /**
     * 毛利额
     */
    @ApiModelProperty(value = "毛利额")
    private BigDecimal grossProfitAmt;

    /**
     * 毛利率
     */
    @ApiModelProperty(value = "毛利率")
    private String grossProfitRate;

    /**
     * 客单价
     */
    @ApiModelProperty(value = "客单价")
    private BigDecimal visitUnitPrice;

    /**
     * 会员应收金额
     */
    @ApiModelProperty(value = "会员应收金额")
    private BigDecimal proSpecs;

    /**
     * 会员来客数
     */
    @ApiModelProperty(value = "会员来客数")
    private BigDecimal membersVisitNum;

    /**
     * 会员客单价
     */
    @ApiModelProperty(value = "会员客单价")
    private BigDecimal membersUnitPrice;

    /**
     * 会员毛利额
     */
    @ApiModelProperty(value = "会员毛利额")
    private BigDecimal gssdAmt;

    /**
     * 会员毛利率
     */
    @ApiModelProperty(value = "会员毛利率")
    private String discountAmt;

    /**
     * 会员消费额度比例
     */
    @ApiModelProperty(value = "会员消费额度比例")
    private String discountRate;
}
