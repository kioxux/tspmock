package com.gys.business.service.data;

import lombok.Data;

import java.util.List;

@Data
public class SupplierSyncData {
    private String client;
    private String site;//门店号/配送中心
    private String attribute; //1 门店 ，2 配送中心
    private List<String> supplierCodes;
}
