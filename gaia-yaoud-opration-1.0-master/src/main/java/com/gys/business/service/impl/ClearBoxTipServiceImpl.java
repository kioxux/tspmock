//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ClearBoxTipService;
import com.gys.business.service.data.AddBoxInfoOutData;
import com.gys.business.service.data.ClearBoxTipOutData;
import com.gys.business.service.data.GetAddBoxInData;
import com.gys.common.data.GetLoginOutData;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClearBoxTipServiceImpl implements ClearBoxTipService {
    @Autowired
    private GaiaSdCleandrawerHMapper gaiaSdCleandrawerHMapper;
    @Autowired
    private GaiaSdCleandrawerDMapper gaiaSdCleandrawerDMapper;
    @Autowired
    private GaiaSdIndrawerHMapper gaiaSdIndrawerHMapper;
    @Autowired
    private GaiaSdIndrawerDMapper gaiaSdIndrawerDMapper;
    @Autowired
    private GaiaSdExamineHMapper gaiaSdExamineHMapper;

    public ClearBoxTipServiceImpl() {
    }

    public List<ClearBoxTipOutData> selectList(GetLoginOutData userInfo) {
        GaiaSdCleandrawerH sdCleandrawerH = new GaiaSdCleandrawerH();
        sdCleandrawerH.setClientId(userInfo.getClient());
        sdCleandrawerH.setGschBrId(userInfo.getDepId());
        List<GaiaSdCleandrawerH> outData = this.gaiaSdCleandrawerHMapper.select(sdCleandrawerH);
        List<ClearBoxTipOutData> out = new ArrayList();

        for(int i = 0; i < outData.size(); ++i) {
            GaiaSdCleandrawerH item = (GaiaSdCleandrawerH)outData.get(i);
            GaiaSdIndrawerH sdIndrawerH = new GaiaSdIndrawerH();
            sdIndrawerH.setClientId(userInfo.getClient());
            sdIndrawerH.setGsihCleVoucherId(item.getGschVoucherId());
            sdIndrawerH = (GaiaSdIndrawerH)this.gaiaSdIndrawerHMapper.selectOne(sdIndrawerH);
            if (!ObjectUtil.isNotEmpty(sdIndrawerH)) {
                ClearBoxTipOutData temp = new ClearBoxTipOutData();
                BeanUtils.copyProperties(item, temp);
                if (ObjectUtil.isNotEmpty(item.getGschExaVoucherId())) {
                    GaiaSdExamineH gaiaSdExamineH = new GaiaSdExamineH();
                    gaiaSdExamineH.setClientId(userInfo.getClient());
                    gaiaSdExamineH.setGsehVoucherId(item.getGschExaVoucherId());
                    gaiaSdExamineH = (GaiaSdExamineH)this.gaiaSdExamineHMapper.selectOne(gaiaSdExamineH);
                    if (ObjectUtil.isNotEmpty(gaiaSdExamineH)) {
                        temp.setGsehDate(gaiaSdExamineH.getGsehDate());
                        temp.setGsehType(gaiaSdExamineH.getGsehType());
                    }
                }

                out.add(temp);
            }
        }

        return out;
    }

    public AddBoxInfoOutData getAddBoxInfo(GetAddBoxInData inData) {
        AddBoxInfoOutData outData = new AddBoxInfoOutData();
        String gsihVoucherId = this.gaiaSdIndrawerHMapper.selectMaxgsihVoucherId();
        inData.setGsihVoucherId(gsihVoucherId);
        inData.setBrId(inData.getBrId());
        this.gaiaSdIndrawerHMapper.insertData(inData);
        GaiaSdCleandrawerD sdCleandrawerD = new GaiaSdCleandrawerD();
        sdCleandrawerD.setClientId(inData.getClientId());
        sdCleandrawerD.setGscdVoucherId(inData.getGschVoucherId());
        List<GaiaSdCleandrawerD> sdCleandrawerDList = this.gaiaSdCleandrawerDMapper.select(sdCleandrawerD);

        for(int i = 0; i < sdCleandrawerDList.size(); ++i) {
            GaiaSdCleandrawerD item = (GaiaSdCleandrawerD)sdCleandrawerDList.get(i);
            GaiaSdIndrawerD date = new GaiaSdIndrawerD();
            date.setClientId(inData.getClientId());
            date.setGsidVoucherId(inData.getGsihVoucherId());
            date.setGsidSerial(item.getGscdSerial());
            date.setGsidProId(item.getGscdProId());
            date.setGsidBatchNo(item.getGscdBatchNo());
            date.setGsidBatch(item.getGscdBatch());
            date.setGsidValidDate(item.getGscdValidDate());
            date.setGsidQty("1000");
            date.setGsidStatus("0");
            date.setGsidDate("");
            this.gaiaSdIndrawerDMapper.insert(date);
        }

        outData.setGsihVoucherId(inData.getGsihVoucherId());
        return outData;
    }
}
