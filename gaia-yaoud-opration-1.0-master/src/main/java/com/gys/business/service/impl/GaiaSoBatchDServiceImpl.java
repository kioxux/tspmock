package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSoBatchDMapper;
import com.gys.business.mapper.entity.GaiaSoBatchD;
import com.gys.business.service.GaiaSoBatchDService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/11 10:20
 */
@Service
@Slf4j
public class GaiaSoBatchDServiceImpl implements GaiaSoBatchDService {

    @Autowired
    private GaiaSoBatchDMapper gaiaSoBatchDMapper;

    @Override
    public List<GaiaSoBatchD> selectAll() {
        return gaiaSoBatchDMapper.selectAll();
    }

    @Override
    public void insertList(List<GaiaSoBatchD> list) {
        gaiaSoBatchDMapper.insertList(list);
    }

    @Override
    public List<GaiaSoBatchD> selectByOrderId(String soOrderid, String client) {
        return gaiaSoBatchDMapper.selectByOrderId(soOrderid, client);
    }
}
