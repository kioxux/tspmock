package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "损益信息列表返回实体")
public class IncomeStatementOutData implements Serializable {
    @ApiModelProperty(value = "损益单号")
    private String gsishVoucherId;

    @ApiModelProperty(value = "生成日期")
    private String gsishDate;

    @ApiModelProperty(value = "审核日期")
    private String gsishExamineDate;

    @ApiModelProperty(value = "生成日期")
    private String gsishIsType;

    @ApiModelProperty(value = "损益类型 1：盘点差异 2：批号调整 3：低值易耗品领用 4：门店领用")
    private String gsishTableType;

    @ApiModelProperty(value = "合计数量")
    private String gsishTotalAmt;

    @ApiModelProperty(value = "合计金额")
    private String gsishTotalQty;
    private String gsisdIsQty;

    @ApiModelProperty(value = "审核状态 0：未审核 1：已审核 2：已退回")
    private String gsishStatus;

    @ApiModelProperty(value = "操作人员姓名")
    private String gsishExamineEmp;

    @ApiModelProperty(value = "备注")
    private String gsishRemark;
    private Integer index;
    private List<IncomeStatementDetailOutData> detailOutDataList;

    @ApiModelProperty(hidden = true)
    private String clientId;

    @ApiModelProperty(hidden = true)
    private String gsishBrId;
}
