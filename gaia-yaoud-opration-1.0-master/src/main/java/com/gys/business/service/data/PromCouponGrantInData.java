package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PromCouponGrantInData implements Serializable {
    private static final long serialVersionUID = -2122014788219690909L;
    private String clientId;
    private String gspcgVoucherId;
    private String gspcgSerial;
    private String gspcgProId;
    private String gspcgActNo;
    private String gspcgReachQty1;
    private BigDecimal gspcgReachAmt1;
    private String gspcgResultQty1;
    private String gspcgReachQty2;
    private BigDecimal gspcgReachAmt2;
    private String gspcgResultQty2;
    private String gspcgReachQty3;
    private BigDecimal gspcgReachAmt3;
    private String gspcgResultQty3;
    private String gspcgMemFlag;
    private String gspcgInteFlag;
    private String gspcgInteRate;
}
