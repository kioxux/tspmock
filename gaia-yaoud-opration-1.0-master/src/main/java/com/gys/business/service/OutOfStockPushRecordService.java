package com.gys.business.service;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/16 15:59
 */
public interface OutOfStockPushRecordService {

    void updatePushRecord(String client, String userId);

}
