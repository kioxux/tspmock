package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ServerService;
import com.gys.business.service.data.*;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static cn.hutool.core.date.DateUtil.between;
import static cn.hutool.core.date.DateUtil.parse;

@SuppressWarnings("all")
@Service
public class ServerServiceImpl implements ServerService {

    @Autowired
    private GaiaSdServerRelevancyMapper serverRelevancyMapper;
    @Autowired
    private GaiaSdServerDiffMapper serverDiffMapper;
    @Autowired
    private GaiaSdServerContraMapper serverContraMapper;
    @Autowired
    private GaiaSdServerTipsMapper serverTipsMapper;
    @Autowired
    private GaiaProductBasicMapper productBasicMapper;
    @Autowired
    private GaiaSdSrRecordMapper recordMapper;

    @Override
    public GetServerOutData detail(GetServerInData inData) {
        GetServerOutData outData = new GetServerOutData();
        //主要成分
        List<String> compclassList = new ArrayList<>();
        //商品id
        List<String> proIdList = new ArrayList<>();
        //剂型 集合
        List<String> partformList = new ArrayList<>();
        List<GetProductInfoQueryOutData> proFromList = new ArrayList<>();

        //1.遍历商品列表 将所有成分和剂型分别保存到list
        for (GetServerProductInData proInData : inData.getProductList()) {
            if (!StringUtils.isEmpty(proInData.getProCode())) {
                proIdList.add(proInData.getProCode());
            }
            GetProductInfoQueryInData getProductInfoQueryInData = new GetProductInfoQueryInData();
            getProductInfoQueryInData.setClient(inData.getClientId());
            getProductInfoQueryInData.setGspp_br_id(inData.getStoreCode());
            getProductInfoQueryInData.setGspgProId(proInData.getProCode());
            List<GetProductInfoQueryOutData> getProductInfoQueryOutDataList = productBasicMapper.getProductBasicList(getProductInfoQueryInData);
            if (getProductInfoQueryOutDataList != null) {
                for (GetProductInfoQueryOutData item : getProductInfoQueryOutDataList) {
                    partformList.add(Optional.ofNullable(item.getProForm()).orElse(""));
                    compclassList.add(Optional.ofNullable(item.getProCompclass()).orElse(""));
                    proFromList.add(item);
                }
            }
        }
        if (compclassList.size() == 0) {
            compclassList.add(null);
        }
        if (proIdList.size() == 0) {
            proIdList.add(null);
        }
        if (partformList.size() == 0) {
            partformList.add(null);
        }

        //2.按所有成分 按主成分优先级 单店优先级和关联成分优先级 查询出所有不包含主成分的所有关联成分
        Example example = new Example(GaiaSdServerRelevancy.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssrBrId", inData.getStoreCode()).andIn("gssrCompclass", compclassList).andNotIn("gssrReleCompclass", compclassList);
        example.setOrderByClause("`GSSR_PRIORITY1` ASC,`GSSR_PRIORITY2` DESC,`GSSR_PRIORITY3` ASC");
        List<GaiaSdServerRelevancy> list = this.serverRelevancyMapper.selectByExample(example);
        Map<String, Object> map = new HashMap<>();

        //3.遍历所有关联成分 查询出有库存的商品 筛选出前两个
        List<GetServerProduct1OutData> tipsList1;
        List<GetServerProduct1OutData> tipsList1One = new ArrayList<>();
        List<GetServerProduct1OutData> tipsList1Two = new ArrayList<>();
        List<GetServerProduct1OutData> tipsList1Three = new ArrayList<>();

        List<GetServerProduct2OutData> tipsList2;
        List<GetServerProduct2OutData> tipsList2One = new ArrayList<>();
        List<GetServerProduct2OutData> tipsList2Two = new ArrayList<>();
        List<GetServerProduct2OutData> tipsList2Three = new ArrayList<>();
        int j = 0;
        for (GaiaSdServerRelevancy serverRelevancy : list) {
            String compClass = serverRelevancy.getGssrCompclass();
            String releCompclass = serverRelevancy.getGssrReleCompclass();

            if (ObjectUtil.isNull(map.get(releCompclass)) && j < 2) {
                if (outData.getProductList1() == null || outData.getProductList1().size() == 0) {
                    tipsList1 = this.productBasicMapper.getProductListByCompclass1(releCompclass, inData.getStoreCode(), inData.getClientId());
                    if (CollUtil.isNotEmpty(tipsList1)) {
                        for (GetServerProduct1OutData getServerProduct1OutData : tipsList1) {
                            if (StrUtil.isBlank(getServerProduct1OutData.getVaildDate1())) {
                                tipsList1Three.add(getServerProduct1OutData);
                                continue;
                            }
                            String vaildDate1 = getServerProduct1OutData.getVaildDate1();
                            long between = between(new Date(), parse(vaildDate1), DateUnit.DAY);
                            if (between < 180) {
                                tipsList1One.add(getServerProduct1OutData);
                            } else {
                                tipsList1Two.add(getServerProduct1OutData);
                            }
                            getServerProduct1OutData.setPriority1(serverRelevancy.getGssrPriority1().toString());
                            getServerProduct1OutData.setPriority3(serverRelevancy.getGssrPriority3().toString());
                            getServerProduct1OutData.setMovTax1(new BigDecimal(getServerProduct1OutData.getMovTax1().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());

                        }
                        if (CollUtil.isNotEmpty(tipsList1One)) {
                            if (tipsList1One.size() > 2) {
                                List<GetServerProduct1OutData> outData1 = tipsList1One.stream().sorted(Comparator.comparing(GetServerProduct1OutData::getGrossProfit1).reversed()).limit(2).collect(Collectors.toList());
                                outData.setProductList1(outData1);
                            } else {
                                if (CollUtil.isNotEmpty(tipsList1Two)) {
                                    tipsList1Two.forEach(tipsList1One::add);
                                    List<GetServerProduct1OutData> collect = tipsList1One.stream()
                                            .sorted(Comparator.comparing(GetServerProduct1OutData::getVaildDate1).reversed())
                                            .sorted(Comparator.comparing(GetServerProduct1OutData::getGrossProfit1).reversed())
                                            .limit(2).collect(Collectors.toList());
                                    if (CollUtil.isNotEmpty(collect)) {
                                        outData.setProductList1(collect);
                                    } else {
                                        List<GetServerProduct1OutData> dataList = tipsList1Three.stream()
                                                .sorted(Comparator.comparing(GetServerProduct1OutData::getBatch1))
                                                .limit(2).collect(Collectors.toList());
                                        outData.setProductList1(dataList);
                                    }
                                }
                            }
                        } else {
                            if (CollUtil.isNotEmpty(tipsList1Two)) {
                                List<GetServerProduct1OutData> collect = tipsList1Two.stream()
                                        .sorted(Comparator.comparing(GetServerProduct1OutData::getVaildDate1).reversed())
                                        .sorted(Comparator.comparing(GetServerProduct1OutData::getGrossProfit1).reversed())
                                        .limit(2).collect(Collectors.toList());
                                if (CollUtil.isNotEmpty(collect)) {
                                    outData.setProductList1(collect);
                                } else {
                                    List<GetServerProduct1OutData> dataList = tipsList1Three.stream()
                                            .sorted(Comparator.comparing(GetServerProduct1OutData::getBatch1))
                                            .limit(2).collect(Collectors.toList());
                                    outData.setProductList1(dataList);
                                }
                            }
                        }


                        outData.setRelevancyExplain1(serverRelevancy.getGssrExplain1());
                        outData.setRelevancyExplain2(serverRelevancy.getGssrExplain2());
                        outData.setRelevancyExplain3(serverRelevancy.getGssrExplain3());
                        outData.setRelevancyExplain4(serverRelevancy.getGssrExplain4());
                        outData.setRelevancyExplain5(serverRelevancy.getGssrExplain5());
                        outData.setProductFormList1(proFromList.stream().filter(item -> ObjectUtil.isNotEmpty(item.getProCompclass()) && item.getProCompclass().equals(compClass)).collect(Collectors.toList()));
                        outData.getProductFormList1().forEach(item -> item.setProCompclassPriority(serverRelevancy.getGssrPriority1().toString()));

                        outData.setRelevancy1(true);
                        j++;
                    }
                } else {
                    tipsList2 = this.productBasicMapper.getProductListByCompclass2(releCompclass, inData.getStoreCode(), inData.getClientId());
                    if (CollUtil.isNotEmpty(tipsList2)) {
                        for (GetServerProduct2OutData getServerProduct2OutData : tipsList2) {
                            if (StrUtil.isBlank(getServerProduct2OutData.getVaildDate2())) {
                                tipsList2Three.add(getServerProduct2OutData);
                                continue;
                            }
                            String vaildDate2 = getServerProduct2OutData.getVaildDate2();
                            long between = between(new Date(), parse(vaildDate2), DateUnit.DAY);
                            if (between < 180) {
                                tipsList2One.add(getServerProduct2OutData);
                            } else {
                                tipsList2Two.add(getServerProduct2OutData);
                            }

                            getServerProduct2OutData.setPriority1(serverRelevancy.getGssrPriority1().toString());
                            getServerProduct2OutData.setPriority3(serverRelevancy.getGssrPriority3().toString());
                            getServerProduct2OutData.setMovTax2(new BigDecimal(getServerProduct2OutData.getMovTax2().replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP).toString());
                        }

                        if (CollUtil.isNotEmpty(tipsList2One)) {
                            if (tipsList2One.size() > 2) {
                                List<GetServerProduct2OutData> outData2 = tipsList2One.stream().sorted(Comparator.comparing(GetServerProduct2OutData::getGrossProfit2).reversed()).limit(2).collect(Collectors.toList());
                                outData.setProductList2(outData2);
                            } else {
                                if (CollUtil.isNotEmpty(tipsList2Two)) {
                                    tipsList2Two.forEach(tipsList2One::add);
                                    List<GetServerProduct2OutData> collect = tipsList2One.stream()
                                            .sorted(Comparator.comparing(GetServerProduct2OutData::getVaildDate2).reversed())
                                            .sorted(Comparator.comparing(GetServerProduct2OutData::getGrossProfit2).reversed())
                                            .limit(2).collect(Collectors.toList());

                                    if (CollUtil.isNotEmpty(collect)) {
                                        outData.setProductList2(collect);
                                    }else {
                                        List<GetServerProduct2OutData> dataList = tipsList2Three.stream()
                                                .sorted(Comparator.comparing(GetServerProduct2OutData::getBatch2))
                                                .limit(2).collect(Collectors.toList());
                                        outData.setProductList2(dataList);
                                    }
                                }
                            }
                        } else {
                            if (CollUtil.isNotEmpty(tipsList2Two)) {
                                List<GetServerProduct2OutData> collect = tipsList2Two.stream()
                                        .sorted(Comparator.comparing(GetServerProduct2OutData::getVaildDate2).reversed())
                                        .sorted(Comparator.comparing(GetServerProduct2OutData::getGrossProfit2).reversed())
                                        .limit(2).collect(Collectors.toList());
                                if (CollUtil.isNotEmpty(collect)) {
                                    outData.setProductList2(collect);
                                } else {
                                    List<GetServerProduct2OutData> dataList = tipsList2Three.stream()
                                            .sorted(Comparator.comparing(GetServerProduct2OutData::getBatch2))
                                            .limit(2).collect(Collectors.toList());
                                    outData.setProductList2(dataList);
                                }
                            }
                        }

                        outData.setProductFormList2(proFromList.stream().filter(item -> ObjectUtil.isNotEmpty(item.getProCompclass()) && item.getProCompclass().equals(compClass)).collect(Collectors.toList()));
                        outData.getProductFormList2().forEach(item -> item.setProCompclassPriority(serverRelevancy.getGssrPriority1().toString()));
                        outData.setRelevancyExplain6(serverRelevancy.getGssrExplain1());
                        outData.setRelevancyExplain7(serverRelevancy.getGssrExplain2());
                        outData.setRelevancyExplain8(serverRelevancy.getGssrExplain3());
                        outData.setRelevancyExplain9(serverRelevancy.getGssrExplain4());
                        outData.setRelevancyExplain10(serverRelevancy.getGssrExplain5());
                        outData.setRelevancy2(true);
                        j++;
                    }
                }
            }
            map.put(releCompclass, serverRelevancy);
        }

        //4.根据成分列表 查询出所有冲突
        Example exampleDiff = new Example(GaiaSdServerDiff.class);
        exampleDiff.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssdBrId", inData.getStoreCode()).andIn("gssdCompclass", compclassList);
        example.setOrderByClause("gssdPriority ASC");
        List<GaiaSdServerDiff> diffList = this.serverDiffMapper.selectByExample(exampleDiff);
        if (ObjectUtil.isNotEmpty(diffList)) {
            Iterator var17 = diffList.iterator();

            while (var17.hasNext()) {
                GaiaSdServerDiff serverDiff = (GaiaSdServerDiff) var17.next();
                if (compclassList.contains(serverDiff.getGssdReleCompclass())) {
                    outData.setDiffExplain1(serverDiff.getGssdExplain1());
                    outData.setDiffExplain2(serverDiff.getGssdExplain2());
                    outData.setDiffExplain3(serverDiff.getGssdExplain3());
                    outData.setDiffExplain4(serverDiff.getGssdExplain4());
                    outData.setDiffExplain5(serverDiff.getGssdExplain5());
                    outData.setDiff(true);
                    break;
                }
            }
        }

        //5.根据成分列表 查询出所有禁忌
        Example exampleContra = new Example(GaiaSdServerContra.class);
        exampleContra.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsscBrId", inData.getStoreCode()).andIn("gsscCompclass", compclassList);
        example.setOrderByClause("gsscPriority ASC");
        List<GaiaSdServerContra> contraList = this.serverContraMapper.selectByExample(exampleContra);
        if (CollUtil.isNotEmpty(contraList)) {
            GaiaSdServerContra serverContra = contraList.get(0);
            outData.setContraExplain1(serverContra.getGsscExplain1());
            outData.setContraExplain2(serverContra.getGsscExplain2());
            outData.setContraExplain3(serverContra.getGsscExplain3());
            outData.setContraExplain4(serverContra.getGsscExplain4());
            outData.setContraExplain5(serverContra.getGsscExplain5());
            outData.setContra(true);
        }

        //6.根据成分，商品和剂型 查询出所有提示内容
        List<GaiaSdServerTips> tipsList = this.serverTipsMapper.selectTipList(inData.getClientId(), inData.getStoreCode(), compclassList, proIdList, partformList);
        if (CollUtil.isNotEmpty(tipsList)) {
            GaiaSdServerTips serverTips = tipsList.get(0);
            outData.setTipsExplain1(serverTips.getGsstExplain1());
            outData.setTipsExplain2(serverTips.getGsstExplain2());
            outData.setTipsExplain3(serverTips.getGsstExplain3());
            outData.setTipsExplain4(serverTips.getGsstExplain4());
            outData.setTipsExplain5(serverTips.getGsstExplain5());
            outData.setTips(true);
        }

        if (CollUtil.isEmpty(list) && CollUtil.isEmpty(diffList) && CollUtil.isEmpty(contraList) && CollUtil.isEmpty(tipsList)) {
            throw new BusinessException("无相关药事服务推荐");
        } else {
            return outData;
        }
    }

    @Override
    @Transactional
    public void save(GetServerInData inData) {
        GaiaSdSrRecord sdSrRecord = new GaiaSdSrRecord();
        sdSrRecord.setClientId(inData.getClientId());
        sdSrRecord.setGssrBillNo(inData.getBillNo());
        sdSrRecord.setGssrBrId(inData.getStoreCode());
        int index = recordMapper.selectCount(sdSrRecord) + 1;
        Example example = new Example(GaiaSdSrRecord.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssrBrId", inData.getStoreCode()).andEqualTo("gssrBillNo", inData.getBillNo()).andEqualTo("gssrReleProType", inData.getType()).andLike("gssrFlag", inData.getCallMethod() + "%");
        List<GaiaSdSrRecord> srRecord = this.recordMapper.selectByExample(example);
        String flag = inData.getCallMethod() + "0";
        if (srRecord != null && srRecord.size() > 0) {
            int size = 0;
            for (GaiaSdSrRecord item : srRecord) {
                if (Integer.parseInt(item.getGssrFlag().substring(1)) > size) {
                    size = Integer.parseInt(item.getGssrFlag().substring(1));
                }
            }
            flag = inData.getCallMethod() + size;
        }

        Iterator var3 = inData.getProductList().iterator();

        while (var3.hasNext()) {
            GetServerProductInData product = (GetServerProductInData) var3.next();
            GaiaSdSrRecord record = new GaiaSdSrRecord();
            record.setClientId(inData.getClientId());
            record.setGssrBillNo(inData.getBillNo());
            record.setGssrBrId(inData.getStoreCode());
            record.setGssrDate(DateUtil.format(new Date(), "yyyyMMdd"));
            record.setGssrTime(DateUtil.format(new Date(), "HHmmss"));
            record.setGssrProId(product.getProCode());
            record.setGssrCompclass(product.getProCompclass());
            record.setGssrReleProType(inData.getType());
            record.setGssrSerial(String.valueOf(index++));
            record.setGssrStockQty(StringUtils.isEmpty(product.getProStock()) ? null : product.getProStock());
            record.setGssrFlag(inData.getCallMethod() + (Integer.parseInt(flag.substring(1)) + 1));
            record.setGssrPtiority1(product.getPriority1());
            record.setGssrPtiority3(product.getPriority3());
            record.setGssrStatus("1");
            this.recordMapper.insert(record);
        }
    }
}
