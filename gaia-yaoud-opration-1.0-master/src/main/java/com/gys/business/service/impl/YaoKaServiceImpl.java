package com.gys.business.service.impl;


import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.gys.business.mapper.GaiaStoreThirdPaymentMapper;
import com.gys.business.mapper.entity.GaiaStoreThirdPayment;
import com.gys.business.service.YaoKaService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.yaoka.CommonResponse;
import com.gys.common.data.yaoka.YaokaApiType;
import com.gys.common.data.yaoka.YaokaResponse;
import com.gys.common.data.yaoka.pollReadyPlan.PaymentDecision;
import com.gys.common.data.yaoka.preCalc.PreCalcResp;
import com.gys.common.exception.BusinessException;
import com.gys.util.yaoka.OpenAPIClient;
import com.gys.util.yaoka.QueryEnv;
import com.pajk.CashCollectionOpen.model.pay.PayReq;
import com.pajk.CashCollectionOpen.model.pollReadyPlan.PollReadyPlanReq;
import com.pajk.CashCollectionOpen.model.preCalc.PreCalcReq;
import com.pajk.CashCollectionOpen.model.undoPay.UndoPayReq;
import lombok.extern.slf4j.Slf4j;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 胡鑫鑫
 */
@Service
@Slf4j
public class YaoKaServiceImpl implements YaoKaService {
    @Autowired
    private GaiaStoreThirdPaymentMapper gaiaStoreThirdPaymentMapper;

    /**
     * 预结算
     */
    @Override
    @SneakyThrows
    public CommonResponse<PreCalcResp> preCalcUtil(GetLoginOutData userInfo, PreCalcReq preCalc){
        Map<String, Object> config = getYaokaParaMap("YaoKa",userInfo);
        preCalc.setSource(String.valueOf(config.get("yaoka_source")));
        preCalc.setStoreId(Long.parseLong(String.valueOf(config.get("yaoka_sto_code"))));
        preCalc.setStoreName(String.valueOf(config.get("yaoka_sto_name")));
        OpenAPIClient openAPIClient = new OpenAPIClient(String.valueOf(config.get("yaoka_source")),
                String.valueOf(config.get("yaoka_partner_id")),
                String.valueOf(config.get("yaoka_partner_key")),
                String.valueOf(config.get("yaoka_api_group")),
                YaokaApiType.PRE_CALC.getApiName(),
                YaokaApiType.PRE_CALC.getApiId(),
                QueryEnv.TEST);
        log.error("预支付 preCalc 输入=" + JSON.toJSONString(preCalc));
        YaokaResponse response = openAPIClient.get(preCalc);
        if (response.getStatus()) {
//            commonResponse = new CommonResponse<PreCalcResp>();
            CommonResponse<PreCalcResp> commonResponse = JSONObject.parseObject(response.getObj(), new TypeReference<CommonResponse<PreCalcResp>>() {});
            log.error("预支付 preCalc 输出" + JSON.toJSONString(commonResponse));
            return commonResponse;
        } else {
            log.error("预支付 preCalc 输出" + JSON.toJSONString(response));
            return null;
        }
    }
    /**
     * 结算
     */
    @SneakyThrows
    @Override
    public CommonResponse payUtil(GetLoginOutData userInfo,PayReq pay){
        Map<String, Object> config = getYaokaParaMap("YaoKa",userInfo);
        pay.setSource(String.valueOf(config.get("yaoka_source")));
        OpenAPIClient openAPIClient = new OpenAPIClient(String.valueOf(config.get("yaoka_source")),
                String.valueOf(config.get("yaoka_partner_id")),
                String.valueOf(config.get("yaoka_partner_key")),
                String.valueOf(config.get("yaoka_api_group")),
                YaokaApiType.PAY.getApiName(),
                YaokaApiType.PAY.getApiId(),
                QueryEnv.TEST);
        log.error("支付 pay 输入=" + JSON.toJSONString(pay));
        YaokaResponse response = openAPIClient.get(pay);
        if (response.getStatus()) {
            CommonResponse commonResponse = JSONObject.parseObject(response.getObj(), CommonResponse.class);
            log.error("支付 pay 输出=" + JSON.toJSONString(commonResponse));
            return commonResponse;
        } else {
            log.error("支付 pay 输出=" + JSON.toJSONString(response));
            return null;
        }
    }
    @SneakyThrows
    @Override
    public CommonResponse pollReadyPlanUtil(GetLoginOutData userInfo,PollReadyPlanReq preCalc){
        Map<String, Object> config = getYaokaParaMap("YaoKa",userInfo);
        preCalc.setSource(String.valueOf(config.get("yaoka_source")));
        OpenAPIClient openAPIClient = new OpenAPIClient(String.valueOf(config.get("yaoka_source")),
                String.valueOf(config.get("yaoka_partner_id")),
                String.valueOf(config.get("yaoka_partner_key")),
                String.valueOf(config.get("yaoka_api_group")),
                YaokaApiType.POLL_READY_PLAN.getApiName(),
                YaokaApiType.POLL_READY_PLAN.getApiId(),
                QueryEnv.TEST);
        log.error("获取用户支付状态 pollReadyPlanUtil 输入=" + JSON.toJSONString(preCalc));
        YaokaResponse response = openAPIClient.get(preCalc);
        if (response.getStatus()) {
            CommonResponse<PaymentDecision> commonResponse = JSONObject.parseObject(response.getObj(), new TypeReference<CommonResponse<PaymentDecision>>() {});
            log.error("获取用户支付状态 pollReadyPlanUtil 输出" + JSON.toJSONString(commonResponse));
            return commonResponse;
        } else {
            log.error("获取用户支付状态 pollReadyPlanUtil 输出" + JSON.toJSONString(response));
            return null;
        }
    }


    @SneakyThrows
    @Override
    public CommonResponse pingAnRefund(GetLoginOutData userInfo, UndoPayReq undoPayReq){
        Map<String, Object> config = getYaokaParaMap("YaoKa",userInfo);
        undoPayReq.setSource(String.valueOf(config.get("yaoka_source")));
        undoPayReq.setStoreId(Long.parseLong(String.valueOf(config.get("yaoka_sto_code"))));
        undoPayReq.setStoreName(String.valueOf(config.get("yaoka_sto_name")));
        OpenAPIClient openAPIClient = new OpenAPIClient(String.valueOf(config.get("yaoka_source")),
                String.valueOf(config.get("yaoka_partner_id")),
                String.valueOf(config.get("yaoka_partner_key")),
                String.valueOf(config.get("yaoka_api_group")),
                YaokaApiType.UNDO_PAY.getApiName(),
                YaokaApiType.UNDO_PAY.getApiId(),
                QueryEnv.TEST);
        log.error("药卡退货 pingAnRefund 输入=" + JSON.toJSONString(undoPayReq));
        YaokaResponse response = openAPIClient.get(undoPayReq);
        if (response.getStatus()) {
            CommonResponse commonResponse = JSONObject.parseObject(response.getObj(), CommonResponse.class);
            log.error("药卡退货 pingAnRefund  输出=  " + JSON.toJSONString(response));
            return commonResponse;
        } else {
            log.error("药卡退货 pingAnRefund  输出=  " + JSON.toJSONString(response));
            return null;
        }
    }

    public Map<String, Object> getYaokaParaMap(String mibArea,GetLoginOutData userInfo) {
        Map<String,Object> map = new HashMap<>(4);
        map.put("client", userInfo.getClient());
        map.put("stoCode", userInfo.getDepId());
        map.put("payType", 1);
        map.put("payValue", mibArea);
        List<GaiaStoreThirdPayment> thirdPaymentsList = gaiaStoreThirdPaymentMapper.selectValue(map);
        return checkYaoka(thirdPaymentsList);
    }

    public Map<String, Object> checkYaoka(List<GaiaStoreThirdPayment> thirdPaymentsList){
        Map<String,Object> resMap = new HashMap<>();
        if(ObjectUtil.isEmpty(thirdPaymentsList)){               throw new BusinessException("该门店未配置平安药卡信息！");}
        resMap = thirdPaymentsList.stream().collect(Collectors.toMap(GaiaStoreThirdPayment::getStoKey, GaiaStoreThirdPayment::getStoValue, (key1, key2) -> key2));
        if(ObjectUtil.isEmpty(resMap.get("yaoka_sto_code"))){    throw new BusinessException("该门店未配置平安药卡门店编码！");}
        if(ObjectUtil.isEmpty(resMap.get("yaoka_sto_name"))){    throw new BusinessException("该门店未配置平安药卡门店名称！");}
        if(ObjectUtil.isEmpty(resMap.get("yaoka_source"))){      throw new BusinessException("该门店未配置平安药卡来源！");}
        if(ObjectUtil.isEmpty(resMap.get("yaoka_api_group"))){   throw new BusinessException("该门店未配置平安药卡apiGroup！");}
        if(ObjectUtil.isEmpty(resMap.get("yaoka_partner_id"))){  throw new BusinessException("该门店未配置平安药卡partnerId！");}
        if(ObjectUtil.isEmpty(resMap.get("yaoka_partner_key"))){ throw new BusinessException("该门店未配置平安药卡partnerKey！");}
        return resMap;
    }
}