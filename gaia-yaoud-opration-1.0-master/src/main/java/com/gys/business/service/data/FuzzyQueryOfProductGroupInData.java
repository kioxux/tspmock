package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 模糊查询传参实体
 *
 * @author xiaoyuan on 2020/9/29
 */
@Data
public class FuzzyQueryOfProductGroupInData implements Serializable {
    private static final long serialVersionUID = -1049039184315509155L;

    private String client;

    private String brId;

    @ApiModelProperty(value = "上传集合商品编码")
    private List<String> proCode;

    @ApiModelProperty(value = "商品编码 通用名称 国际条形码1 国际条形码2 助记码 商品名（模糊匹配）")
    private String nameOrCode;

    @ApiModelProperty(value = "品牌分类")
    private String proBrand;

    @ApiModelProperty(value = "商品分类")
    private String proClassName;

    @ApiModelProperty(value = "毛利区间 : 开始")
    private String grossProfitStart;

    @ApiModelProperty(value = "毛利区间 : 结束")
    private String grossProfitEnd;

    @ApiModelProperty(value = "价格区间 : 开始")
    private String priceRangeStart;

    @ApiModelProperty(value = "价格区间 : 结束")
    private String priceRangeEnd;

    private Integer pageNum;
    private Integer pageSize;

    // 大类 中类 商品分类
    private String [][] classArr;
    private List<String> classArrs;
    //商品组
    private String[] gsyGroups;
    //自定义1
    private String[] zdy1;
    //自定义2
    private String[] zdy2;
    //自定义3
    private String[] zdy3;
    //自定义4
    private String[] zdy4;
    //自定义5
    private String[] zdy5;
}
