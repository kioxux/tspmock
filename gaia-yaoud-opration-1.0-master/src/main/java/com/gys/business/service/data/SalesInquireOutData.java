
package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class SalesInquireOutData implements Serializable {
    private static final long serialVersionUID = -2657875823116981691L;

    private String clientId;
    private String salesBrId;
    private String salesBrName;
    private String salesBillNo;
    private String salesDate;
    private String salesTime;
    private String salesMemberCardNum;
    private String salesMemberName;
    private String salesRetailPrice;
    private String salesAmountReceived;
    private String salesPayMethod;
    private String salesCashier;
    private String pointsThisTime;
    private Integer index;
    private String gsshBillNoReturn;
    private String gsshCrFlag;
    private String memberId;

    /**
     * 会员手机号
     */
    private String salesMemberPhone;
    /**
     * 会员身份证
     */
    private String salesMemberId;
    /**
     * 会员年龄
     */
    private String salesMemberAge;
    /**
     * 会员性别
     */
    private String salesMemberSex;


    /**
     * 折让金额
     */
    private String salesDiscount;

    /**
     * 零售金额
     */
    private String salesRetail;

    /**
     * 营业员
     */
    private String salesSalesperson;

    /**
     * 支付方式
     */
    private String salesPaymentMethod;

    /**
     * 支付名称
     */
    private String gsspmName;

    /**
     * 支付种类
     */
    private String countPay;

    /**
     * 临时备注
     */
    private String gsshDzqdyActno5;

    /**
     * 支付编码
     */
    private String gsspmId;
    /**
     * 帖数
     */
    private String gssdDose;

    /**
     * 当前登陆时间
     */
    private String nowDate;

    /**
     * 是否有处方单
     */
    private Boolean isprescription;

    /**
     * 订单类型， 0-本地pos销售单；1-中药代煎单；2-HIS门诊单 默认为空
     */
    private String gsshOrderSource;
    /**
     * 关联销售单号
     */
    private String linkSalesBillNo;
    /**
     * 外卖平台单号
     */
    private String platformsOrderId;
    /**
     * 是否开票
     */
    private String markInvoicing;

}
