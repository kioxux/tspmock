//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class RechargeCardSetInData {
    private String gsrcsCardPrefix;
    private String gsrcsPassword;
    private String gsrcsPaymentMethod;
    private String gsrcsPwNeedOpt;
    private String gsrcsMinAmtOpt;
    private String gsrcsMaxAmtOpt;
    private String gsrcsFirstPwOpt;
    private String gsrcsRecoverpwOpt;
    private String gsrcsProportion1;
    private BigDecimal gsrcsMinAmt1;
    private String gsrcsProportion2;
    private BigDecimal gsrcsMinAmt2;
    private String gsrcsPromotion2;
    private String gsrcsRepeat2;
    private String gsrcsProportion3;
    private BigDecimal gsrcsMinAmt3;
    private String gsrcsPromotion3;
    private String gsrcsRepeat3;

    public RechargeCardSetInData() {
    }

    public String getGsrcsCardPrefix() {
        return this.gsrcsCardPrefix;
    }

    public String getGsrcsPassword() {
        return this.gsrcsPassword;
    }

    public String getGsrcsPaymentMethod() {
        return this.gsrcsPaymentMethod;
    }

    public String getGsrcsPwNeedOpt() {
        return this.gsrcsPwNeedOpt;
    }

    public String getGsrcsMinAmtOpt() {
        return this.gsrcsMinAmtOpt;
    }

    public String getGsrcsMaxAmtOpt() {
        return this.gsrcsMaxAmtOpt;
    }

    public String getGsrcsFirstPwOpt() {
        return this.gsrcsFirstPwOpt;
    }

    public String getGsrcsRecoverpwOpt() {
        return this.gsrcsRecoverpwOpt;
    }

    public String getGsrcsProportion1() {
        return this.gsrcsProportion1;
    }

    public BigDecimal getGsrcsMinAmt1() {
        return this.gsrcsMinAmt1;
    }

    public String getGsrcsProportion2() {
        return this.gsrcsProportion2;
    }

    public BigDecimal getGsrcsMinAmt2() {
        return this.gsrcsMinAmt2;
    }

    public String getGsrcsPromotion2() {
        return this.gsrcsPromotion2;
    }

    public String getGsrcsRepeat2() {
        return this.gsrcsRepeat2;
    }

    public String getGsrcsProportion3() {
        return this.gsrcsProportion3;
    }

    public BigDecimal getGsrcsMinAmt3() {
        return this.gsrcsMinAmt3;
    }

    public String getGsrcsPromotion3() {
        return this.gsrcsPromotion3;
    }

    public String getGsrcsRepeat3() {
        return this.gsrcsRepeat3;
    }

    public void setGsrcsCardPrefix(final String gsrcsCardPrefix) {
        this.gsrcsCardPrefix = gsrcsCardPrefix;
    }

    public void setGsrcsPassword(final String gsrcsPassword) {
        this.gsrcsPassword = gsrcsPassword;
    }

    public void setGsrcsPaymentMethod(final String gsrcsPaymentMethod) {
        this.gsrcsPaymentMethod = gsrcsPaymentMethod;
    }

    public void setGsrcsPwNeedOpt(final String gsrcsPwNeedOpt) {
        this.gsrcsPwNeedOpt = gsrcsPwNeedOpt;
    }

    public void setGsrcsMinAmtOpt(final String gsrcsMinAmtOpt) {
        this.gsrcsMinAmtOpt = gsrcsMinAmtOpt;
    }

    public void setGsrcsMaxAmtOpt(final String gsrcsMaxAmtOpt) {
        this.gsrcsMaxAmtOpt = gsrcsMaxAmtOpt;
    }

    public void setGsrcsFirstPwOpt(final String gsrcsFirstPwOpt) {
        this.gsrcsFirstPwOpt = gsrcsFirstPwOpt;
    }

    public void setGsrcsRecoverpwOpt(final String gsrcsRecoverpwOpt) {
        this.gsrcsRecoverpwOpt = gsrcsRecoverpwOpt;
    }

    public void setGsrcsProportion1(final String gsrcsProportion1) {
        this.gsrcsProportion1 = gsrcsProportion1;
    }

    public void setGsrcsMinAmt1(final BigDecimal gsrcsMinAmt1) {
        this.gsrcsMinAmt1 = gsrcsMinAmt1;
    }

    public void setGsrcsProportion2(final String gsrcsProportion2) {
        this.gsrcsProportion2 = gsrcsProportion2;
    }

    public void setGsrcsMinAmt2(final BigDecimal gsrcsMinAmt2) {
        this.gsrcsMinAmt2 = gsrcsMinAmt2;
    }

    public void setGsrcsPromotion2(final String gsrcsPromotion2) {
        this.gsrcsPromotion2 = gsrcsPromotion2;
    }

    public void setGsrcsRepeat2(final String gsrcsRepeat2) {
        this.gsrcsRepeat2 = gsrcsRepeat2;
    }

    public void setGsrcsProportion3(final String gsrcsProportion3) {
        this.gsrcsProportion3 = gsrcsProportion3;
    }

    public void setGsrcsMinAmt3(final BigDecimal gsrcsMinAmt3) {
        this.gsrcsMinAmt3 = gsrcsMinAmt3;
    }

    public void setGsrcsPromotion3(final String gsrcsPromotion3) {
        this.gsrcsPromotion3 = gsrcsPromotion3;
    }

    public void setGsrcsRepeat3(final String gsrcsRepeat3) {
        this.gsrcsRepeat3 = gsrcsRepeat3;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RechargeCardSetInData)) {
            return false;
        } else {
            RechargeCardSetInData other = (RechargeCardSetInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$gsrcsCardPrefix = this.getGsrcsCardPrefix();
                Object other$gsrcsCardPrefix = other.getGsrcsCardPrefix();
                if (this$gsrcsCardPrefix == null) {
                    if (other$gsrcsCardPrefix != null) {
                        return false;
                    }
                } else if (!this$gsrcsCardPrefix.equals(other$gsrcsCardPrefix)) {
                    return false;
                }

                Object this$gsrcsPassword = this.getGsrcsPassword();
                Object other$gsrcsPassword = other.getGsrcsPassword();
                if (this$gsrcsPassword == null) {
                    if (other$gsrcsPassword != null) {
                        return false;
                    }
                } else if (!this$gsrcsPassword.equals(other$gsrcsPassword)) {
                    return false;
                }

                Object this$gsrcsPaymentMethod = this.getGsrcsPaymentMethod();
                Object other$gsrcsPaymentMethod = other.getGsrcsPaymentMethod();
                if (this$gsrcsPaymentMethod == null) {
                    if (other$gsrcsPaymentMethod != null) {
                        return false;
                    }
                } else if (!this$gsrcsPaymentMethod.equals(other$gsrcsPaymentMethod)) {
                    return false;
                }

                label206: {
                    Object this$gsrcsPwNeedOpt = this.getGsrcsPwNeedOpt();
                    Object other$gsrcsPwNeedOpt = other.getGsrcsPwNeedOpt();
                    if (this$gsrcsPwNeedOpt == null) {
                        if (other$gsrcsPwNeedOpt == null) {
                            break label206;
                        }
                    } else if (this$gsrcsPwNeedOpt.equals(other$gsrcsPwNeedOpt)) {
                        break label206;
                    }

                    return false;
                }

                label199: {
                    Object this$gsrcsMinAmtOpt = this.getGsrcsMinAmtOpt();
                    Object other$gsrcsMinAmtOpt = other.getGsrcsMinAmtOpt();
                    if (this$gsrcsMinAmtOpt == null) {
                        if (other$gsrcsMinAmtOpt == null) {
                            break label199;
                        }
                    } else if (this$gsrcsMinAmtOpt.equals(other$gsrcsMinAmtOpt)) {
                        break label199;
                    }

                    return false;
                }

                Object this$gsrcsMaxAmtOpt = this.getGsrcsMaxAmtOpt();
                Object other$gsrcsMaxAmtOpt = other.getGsrcsMaxAmtOpt();
                if (this$gsrcsMaxAmtOpt == null) {
                    if (other$gsrcsMaxAmtOpt != null) {
                        return false;
                    }
                } else if (!this$gsrcsMaxAmtOpt.equals(other$gsrcsMaxAmtOpt)) {
                    return false;
                }

                label185: {
                    Object this$gsrcsFirstPwOpt = this.getGsrcsFirstPwOpt();
                    Object other$gsrcsFirstPwOpt = other.getGsrcsFirstPwOpt();
                    if (this$gsrcsFirstPwOpt == null) {
                        if (other$gsrcsFirstPwOpt == null) {
                            break label185;
                        }
                    } else if (this$gsrcsFirstPwOpt.equals(other$gsrcsFirstPwOpt)) {
                        break label185;
                    }

                    return false;
                }

                label178: {
                    Object this$gsrcsRecoverpwOpt = this.getGsrcsRecoverpwOpt();
                    Object other$gsrcsRecoverpwOpt = other.getGsrcsRecoverpwOpt();
                    if (this$gsrcsRecoverpwOpt == null) {
                        if (other$gsrcsRecoverpwOpt == null) {
                            break label178;
                        }
                    } else if (this$gsrcsRecoverpwOpt.equals(other$gsrcsRecoverpwOpt)) {
                        break label178;
                    }

                    return false;
                }

                Object this$gsrcsProportion1 = this.getGsrcsProportion1();
                Object other$gsrcsProportion1 = other.getGsrcsProportion1();
                if (this$gsrcsProportion1 == null) {
                    if (other$gsrcsProportion1 != null) {
                        return false;
                    }
                } else if (!this$gsrcsProportion1.equals(other$gsrcsProportion1)) {
                    return false;
                }

                Object this$gsrcsMinAmt1 = this.getGsrcsMinAmt1();
                Object other$gsrcsMinAmt1 = other.getGsrcsMinAmt1();
                if (this$gsrcsMinAmt1 == null) {
                    if (other$gsrcsMinAmt1 != null) {
                        return false;
                    }
                } else if (!this$gsrcsMinAmt1.equals(other$gsrcsMinAmt1)) {
                    return false;
                }

                label157: {
                    Object this$gsrcsProportion2 = this.getGsrcsProportion2();
                    Object other$gsrcsProportion2 = other.getGsrcsProportion2();
                    if (this$gsrcsProportion2 == null) {
                        if (other$gsrcsProportion2 == null) {
                            break label157;
                        }
                    } else if (this$gsrcsProportion2.equals(other$gsrcsProportion2)) {
                        break label157;
                    }

                    return false;
                }

                label150: {
                    Object this$gsrcsMinAmt2 = this.getGsrcsMinAmt2();
                    Object other$gsrcsMinAmt2 = other.getGsrcsMinAmt2();
                    if (this$gsrcsMinAmt2 == null) {
                        if (other$gsrcsMinAmt2 == null) {
                            break label150;
                        }
                    } else if (this$gsrcsMinAmt2.equals(other$gsrcsMinAmt2)) {
                        break label150;
                    }

                    return false;
                }

                Object this$gsrcsPromotion2 = this.getGsrcsPromotion2();
                Object other$gsrcsPromotion2 = other.getGsrcsPromotion2();
                if (this$gsrcsPromotion2 == null) {
                    if (other$gsrcsPromotion2 != null) {
                        return false;
                    }
                } else if (!this$gsrcsPromotion2.equals(other$gsrcsPromotion2)) {
                    return false;
                }

                label136: {
                    Object this$gsrcsRepeat2 = this.getGsrcsRepeat2();
                    Object other$gsrcsRepeat2 = other.getGsrcsRepeat2();
                    if (this$gsrcsRepeat2 == null) {
                        if (other$gsrcsRepeat2 == null) {
                            break label136;
                        }
                    } else if (this$gsrcsRepeat2.equals(other$gsrcsRepeat2)) {
                        break label136;
                    }

                    return false;
                }

                Object this$gsrcsProportion3 = this.getGsrcsProportion3();
                Object other$gsrcsProportion3 = other.getGsrcsProportion3();
                if (this$gsrcsProportion3 == null) {
                    if (other$gsrcsProportion3 != null) {
                        return false;
                    }
                } else if (!this$gsrcsProportion3.equals(other$gsrcsProportion3)) {
                    return false;
                }

                label122: {
                    Object this$gsrcsMinAmt3 = this.getGsrcsMinAmt3();
                    Object other$gsrcsMinAmt3 = other.getGsrcsMinAmt3();
                    if (this$gsrcsMinAmt3 == null) {
                        if (other$gsrcsMinAmt3 == null) {
                            break label122;
                        }
                    } else if (this$gsrcsMinAmt3.equals(other$gsrcsMinAmt3)) {
                        break label122;
                    }

                    return false;
                }

                Object this$gsrcsPromotion3 = this.getGsrcsPromotion3();
                Object other$gsrcsPromotion3 = other.getGsrcsPromotion3();
                if (this$gsrcsPromotion3 == null) {
                    if (other$gsrcsPromotion3 != null) {
                        return false;
                    }
                } else if (!this$gsrcsPromotion3.equals(other$gsrcsPromotion3)) {
                    return false;
                }

                Object this$gsrcsRepeat3 = this.getGsrcsRepeat3();
                Object other$gsrcsRepeat3 = other.getGsrcsRepeat3();
                if (this$gsrcsRepeat3 == null) {
                    if (other$gsrcsRepeat3 != null) {
                        return false;
                    }
                } else if (!this$gsrcsRepeat3.equals(other$gsrcsRepeat3)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RechargeCardSetInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gsrcsCardPrefix = this.getGsrcsCardPrefix();
        result = result * 59 + ($gsrcsCardPrefix == null ? 43 : $gsrcsCardPrefix.hashCode());
        Object $gsrcsPassword = this.getGsrcsPassword();
        result = result * 59 + ($gsrcsPassword == null ? 43 : $gsrcsPassword.hashCode());
        Object $gsrcsPaymentMethod = this.getGsrcsPaymentMethod();
        result = result * 59 + ($gsrcsPaymentMethod == null ? 43 : $gsrcsPaymentMethod.hashCode());
        Object $gsrcsPwNeedOpt = this.getGsrcsPwNeedOpt();
        result = result * 59 + ($gsrcsPwNeedOpt == null ? 43 : $gsrcsPwNeedOpt.hashCode());
        Object $gsrcsMinAmtOpt = this.getGsrcsMinAmtOpt();
        result = result * 59 + ($gsrcsMinAmtOpt == null ? 43 : $gsrcsMinAmtOpt.hashCode());
        Object $gsrcsMaxAmtOpt = this.getGsrcsMaxAmtOpt();
        result = result * 59 + ($gsrcsMaxAmtOpt == null ? 43 : $gsrcsMaxAmtOpt.hashCode());
        Object $gsrcsFirstPwOpt = this.getGsrcsFirstPwOpt();
        result = result * 59 + ($gsrcsFirstPwOpt == null ? 43 : $gsrcsFirstPwOpt.hashCode());
        Object $gsrcsRecoverpwOpt = this.getGsrcsRecoverpwOpt();
        result = result * 59 + ($gsrcsRecoverpwOpt == null ? 43 : $gsrcsRecoverpwOpt.hashCode());
        Object $gsrcsProportion1 = this.getGsrcsProportion1();
        result = result * 59 + ($gsrcsProportion1 == null ? 43 : $gsrcsProportion1.hashCode());
        Object $gsrcsMinAmt1 = this.getGsrcsMinAmt1();
        result = result * 59 + ($gsrcsMinAmt1 == null ? 43 : $gsrcsMinAmt1.hashCode());
        Object $gsrcsProportion2 = this.getGsrcsProportion2();
        result = result * 59 + ($gsrcsProportion2 == null ? 43 : $gsrcsProportion2.hashCode());
        Object $gsrcsMinAmt2 = this.getGsrcsMinAmt2();
        result = result * 59 + ($gsrcsMinAmt2 == null ? 43 : $gsrcsMinAmt2.hashCode());
        Object $gsrcsPromotion2 = this.getGsrcsPromotion2();
        result = result * 59 + ($gsrcsPromotion2 == null ? 43 : $gsrcsPromotion2.hashCode());
        Object $gsrcsRepeat2 = this.getGsrcsRepeat2();
        result = result * 59 + ($gsrcsRepeat2 == null ? 43 : $gsrcsRepeat2.hashCode());
        Object $gsrcsProportion3 = this.getGsrcsProportion3();
        result = result * 59 + ($gsrcsProportion3 == null ? 43 : $gsrcsProportion3.hashCode());
        Object $gsrcsMinAmt3 = this.getGsrcsMinAmt3();
        result = result * 59 + ($gsrcsMinAmt3 == null ? 43 : $gsrcsMinAmt3.hashCode());
        Object $gsrcsPromotion3 = this.getGsrcsPromotion3();
        result = result * 59 + ($gsrcsPromotion3 == null ? 43 : $gsrcsPromotion3.hashCode());
        Object $gsrcsRepeat3 = this.getGsrcsRepeat3();
        result = result * 59 + ($gsrcsRepeat3 == null ? 43 : $gsrcsRepeat3.hashCode());
        return result;
    }

    public String toString() {
        return "RechargeCardSetInData(gsrcsCardPrefix=" + this.getGsrcsCardPrefix() + ", gsrcsPassword=" + this.getGsrcsPassword() + ", gsrcsPaymentMethod=" + this.getGsrcsPaymentMethod() + ", gsrcsPwNeedOpt=" + this.getGsrcsPwNeedOpt() + ", gsrcsMinAmtOpt=" + this.getGsrcsMinAmtOpt() + ", gsrcsMaxAmtOpt=" + this.getGsrcsMaxAmtOpt() + ", gsrcsFirstPwOpt=" + this.getGsrcsFirstPwOpt() + ", gsrcsRecoverpwOpt=" + this.getGsrcsRecoverpwOpt() + ", gsrcsProportion1=" + this.getGsrcsProportion1() + ", gsrcsMinAmt1=" + this.getGsrcsMinAmt1() + ", gsrcsProportion2=" + this.getGsrcsProportion2() + ", gsrcsMinAmt2=" + this.getGsrcsMinAmt2() + ", gsrcsPromotion2=" + this.getGsrcsPromotion2() + ", gsrcsRepeat2=" + this.getGsrcsRepeat2() + ", gsrcsProportion3=" + this.getGsrcsProportion3() + ", gsrcsMinAmt3=" + this.getGsrcsMinAmt3() + ", gsrcsPromotion3=" + this.getGsrcsPromotion3() + ", gsrcsRepeat3=" + this.getGsrcsRepeat3() + ")";
    }
}
