package com.gys.business.service.storeorder.impl;

import cn.hutool.core.util.CharUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.google.common.base.Splitter;
import com.gys.business.mapper.GaiaBatchInfoMapper;
import com.gys.business.mapper.GaiaCompAdmMapper;
import com.gys.business.mapper.GaiaSdReplenishHMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.GaiaCompAdm;
import com.gys.business.service.storeorder.StoreOrderService;
import com.gys.common.data.BaseListSelect;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.data.storeorder.LastSupplierPrice;
import com.gys.common.data.storeorder.ProSummaryVO;
import com.gys.common.data.storeorder.StoreDetailedVO;
import com.gys.common.data.storeorder.StoreOrderDTO;
import com.gys.common.exception.BusinessException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wu mao yin
 * @title: 门店请货数据查询
 * @date 2021/12/315:12
 */
@Service
public class StoreOrderServiceImpl implements StoreOrderService {

    @Resource
    private GaiaBatchInfoMapper gaiaBatchInfoMapper;

    @Resource
    private GaiaCompAdmMapper gaiaCompAdmMapper;

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    private GaiaSdReplenishHMapper gaiaSdReplenishHMapper;

    @Override
    public JsonResult selectChainCompList(String client) {
        GaiaCompAdm gaiaCompAdm = new GaiaCompAdm();
        gaiaCompAdm.setClient(client);
        List<GaiaCompAdm> gaiaCompAdms = gaiaCompAdmMapper.select(gaiaCompAdm);
        List<BaseListSelect> res = new ArrayList<>();
        for (GaiaCompAdm compAdm : gaiaCompAdms) {
            BaseListSelect baseListSelect = new BaseListSelect();
            baseListSelect.setId(compAdm.getCompadmId());
            baseListSelect.setName(compAdm.getCompadmName());
            res.add(baseListSelect);
        }
        return JsonResult.success(res);
    }

    @Override
    public JsonResult selectStoListByChainComp(String client, String chainCompId) {
        if (StringUtils.isBlank(chainCompId)) {
            throw new BusinessException("请选择连锁公司!");
        }
        List<BaseListSelect> stoListByChainComp = gaiaStoreDataMapper.selectStoListByChainComp(client, chainCompId);
        return JsonResult.success(stoListByChainComp);
    }

    @Override
    public JsonResult selectStoreDetailedList(StoreOrderDTO storeOrderDTO) {
        if (StringUtils.isBlank(storeOrderDTO.getChainCompId())) {
            throw new BusinessException("请选择连锁公司!");
        }
        String client = storeOrderDTO.getClient();
        transferParamType(storeOrderDTO);
        PageHelper.startPage(storeOrderDTO.getPageNum(), storeOrderDTO.getPageSize());
        List<StoreDetailedVO> storeDetailedList = gaiaSdReplenishHMapper.selectStoreDetailedList(storeOrderDTO);
        if (CollectionUtils.isNotEmpty(storeDetailedList)) {
            // 获取商品末次供应商信息
            List<LastSupplierPrice> lastSupplierPrices = gaiaBatchInfoMapper.selectLastSupplier(client, storeOrderDTO.getProCodeList());
            Map<String, LastSupplierPrice> lastSupplierPricesMap = lastSupplierPrices.stream()
                    .collect(Collectors.toMap(LastSupplierPrice::getProCode, s -> s));
            for (StoreDetailedVO storeDetailedVO : storeDetailedList) {
                // 末次供应商
                LastSupplierPrice lastSupplierPrice = lastSupplierPricesMap.get(storeDetailedVO.getProCode());
                if (lastSupplierPrice != null) {
                    storeDetailedVO.setLastSupplier(lastSupplierPrice.getLastSupplier());
                    storeDetailedVO.setLastPurchasePrice(lastSupplierPrice.getLastPurchasePrice());
                }
                BigDecimal outgoingPrice = storeDetailedVO.getOutgoingPrice();
                if (outgoingPrice != null) {
                    storeDetailedVO.setOutgoingPrice(outgoingPrice.setScale(2, RoundingMode.HALF_UP));
                }
                BigDecimal amount = storeDetailedVO.getAmount();
                if (amount != null) {
                    storeDetailedVO.setAmount(amount.setScale(2, RoundingMode.HALF_UP));
                }
                BigDecimal retailPrice = storeDetailedVO.getRetailPrice();
                if (retailPrice != null) {
                    storeDetailedVO.setRetailPrice(retailPrice.setScale(2, RoundingMode.HALF_UP));
                }
                BigDecimal lastPurchasePrice = storeDetailedVO.getLastPurchasePrice();
                if (lastPurchasePrice != null) {
                    storeDetailedVO.setLastPurchasePrice(lastPurchasePrice.setScale(2, RoundingMode.HALF_UP));
                }
            }
        }
        return JsonResult.success(new PageInfo<>(storeDetailedList));
    }

    @Override
    public void exportExcel(StoreOrderDTO storeOrderDTO, HttpServletResponse response) {
        if (StringUtils.isBlank(storeOrderDTO.getChainCompId())) {
            throw new BusinessException("请选择连锁公司!");
        }
        String client = storeOrderDTO.getClient();
        transferParamType(storeOrderDTO);
        List<StoreDetailedVO> storeDetailedList = gaiaSdReplenishHMapper.selectStoreDetailedList(storeOrderDTO);
        if (CollectionUtils.isNotEmpty(storeDetailedList)) {
            // 获取商品末次供应商信息
            List<LastSupplierPrice> lastSupplierPrices = gaiaBatchInfoMapper.selectLastSupplier(client, storeOrderDTO.getProCodeList());
            Map<String, LastSupplierPrice> lastSupplierPricesMap = lastSupplierPrices.stream()
                    .collect(Collectors.toMap(LastSupplierPrice::getProCode, s -> s));
            for (StoreDetailedVO storeDetailedVO : storeDetailedList) {
                // 末次供应商
                LastSupplierPrice lastSupplierPrice = lastSupplierPricesMap.get(storeDetailedVO.getProCode());
                if (lastSupplierPrice != null) {
                    storeDetailedVO.setLastSupplier(lastSupplierPrice.getLastSupplier());
                    storeDetailedVO.setLastPurchasePrice(lastSupplierPrice.getLastPurchasePrice());
                }
                BigDecimal outgoingPrice = storeDetailedVO.getOutgoingPrice();
                if (outgoingPrice != null) {
                    storeDetailedVO.setOutgoingPrice(outgoingPrice.setScale(2, RoundingMode.HALF_UP));
                }
                BigDecimal amount = storeDetailedVO.getAmount();
                if (amount != null) {
                    storeDetailedVO.setAmount(amount.setScale(2, RoundingMode.HALF_UP));
                }
                BigDecimal retailPrice = storeDetailedVO.getRetailPrice();
                if (retailPrice != null) {
                    storeDetailedVO.setRetailPrice(retailPrice.setScale(2, RoundingMode.HALF_UP));
                }
                BigDecimal lastPurchasePrice = storeDetailedVO.getLastPurchasePrice();
                if (lastPurchasePrice != null) {
                    storeDetailedVO.setLastPurchasePrice(lastPurchasePrice.setScale(2, RoundingMode.HALF_UP));
                }
            }
        }
        if (CollectionUtils.isEmpty(storeDetailedList)){
            throw new BusinessException("提示：导出数据为空！");
        }
        String fileName = null;
        try{
            fileName = URLEncoder.encode("门店请货明细导出", "UTF-8").replaceAll("\\+", "%20");
        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        try {
            EasyExcel.write(response.getOutputStream(), StoreDetailedVO.class)
                    .sheet("0")
                    .doWrite(storeDetailedList);
        } catch (Exception e) {
            // 重置response
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            Map<String, String> map = new HashMap<>();
            map.put("status", "failure");
            map.put("message", "下载文件失败" + e.getMessage());
            try {
                response.getWriter().println(JSON.toJSONString(map));
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    /**
     * 参数类型转换
     *
     * @param storeOrderDTO storeOrderDTO
     */
    private void transferParamType(StoreOrderDTO storeOrderDTO) {
        // 请货单号
        String orderNos = storeOrderDTO.getOrderNos();
        if (StringUtils.isNotBlank(orderNos)) {
            storeOrderDTO.setOrderNoList(Splitter.on(CharUtil.COMMA).omitEmptyStrings().splitToList(orderNos));
        }
        // 商品编码
        String proCodes = storeOrderDTO.getProCodes();
        if (StringUtils.isNotBlank(proCodes)) {
            storeOrderDTO.setProCodeList(Splitter.on(CharUtil.COMMA).omitEmptyStrings().splitToList(proCodes));
        }
    }

    @Override
    public JsonResult selectProSummaryList(StoreOrderDTO storeOrderDTO) {
        if (StringUtils.isBlank(storeOrderDTO.getChainCompId())) {
            throw new BusinessException("请选择连锁公司!");
        }
        transferParamType(storeOrderDTO);

        PageHelper.startPage(storeOrderDTO.getPageNum(), storeOrderDTO.getPageSize());
        // 获取商品明细汇总
        List<ProSummaryVO> proSummaryList = gaiaSdReplenishHMapper.selectProSummaryList(storeOrderDTO);
        if (CollectionUtils.isNotEmpty(proSummaryList)) {
            String client = storeOrderDTO.getClient();
            // 获取商品末次供应商信息
            List<LastSupplierPrice> lastSupplierPrices = gaiaBatchInfoMapper.selectLastSupplier(client, storeOrderDTO.getProCodeList());
            Map<String, LastSupplierPrice> lastSupplierPricesMap = lastSupplierPrices.stream()
                    .collect(Collectors.toMap(LastSupplierPrice::getProCode, s -> s));
            for (ProSummaryVO proSummaryVO : proSummaryList) {
                String proCode = proSummaryVO.getProCode();
                // 末次供应商
                LastSupplierPrice lastSupplierPrice = lastSupplierPricesMap.get(proCode);
                if (lastSupplierPrice != null) {
                    proSummaryVO.setLastSupplier(lastSupplierPrice.getLastSupplier());
                    proSummaryVO.setLastPurchasePrice(lastSupplierPrice.getLastPurchasePrice());
                }
                // 差异量
                Integer demandNum = proSummaryVO.getDemandNum();
                Integer billingNum = proSummaryVO.getBillingNum();
                proSummaryVO.setDiffNum(demandNum - billingNum);
                BigDecimal retailPrice = proSummaryVO.getRetailPrice();
                if (retailPrice != null) {
                    proSummaryVO.setRetailPrice(retailPrice.setScale(2, RoundingMode.HALF_UP));
                }
                BigDecimal lastPurchasePrice = proSummaryVO.getLastPurchasePrice();
                if (lastPurchasePrice != null) {
                    proSummaryVO.setLastPurchasePrice(lastPurchasePrice.setScale(2, RoundingMode.HALF_UP));
                }
            }
        }
        return JsonResult.success(new PageInfo<>(proSummaryList));
    }

    @Override
    public void exportProSummaryList(StoreOrderDTO storeOrderDTO, HttpServletResponse response){
        if (StringUtils.isBlank(storeOrderDTO.getChainCompId())) {
            throw new BusinessException("请选择连锁公司!");
        }
        transferParamType(storeOrderDTO);

        // 获取商品明细汇总
        List<ProSummaryVO> proSummaryList = gaiaSdReplenishHMapper.selectProSummaryList(storeOrderDTO);
        if (CollectionUtils.isNotEmpty(proSummaryList)) {
            String client = storeOrderDTO.getClient();
            // 获取商品末次供应商信息
            List<LastSupplierPrice> lastSupplierPrices = gaiaBatchInfoMapper.selectLastSupplier(client, storeOrderDTO.getProCodeList());
            Map<String, LastSupplierPrice> lastSupplierPricesMap = lastSupplierPrices.stream()
                    .collect(Collectors.toMap(LastSupplierPrice::getProCode, s -> s));
            for (ProSummaryVO proSummaryVO : proSummaryList) {
                String proCode = proSummaryVO.getProCode();
                // 末次供应商
                LastSupplierPrice lastSupplierPrice = lastSupplierPricesMap.get(proCode);
                if (lastSupplierPrice != null) {
                    proSummaryVO.setLastSupplier(lastSupplierPrice.getLastSupplier());
                    proSummaryVO.setLastPurchasePrice(lastSupplierPrice.getLastPurchasePrice());
                }
                // 差异量
                Integer demandNum = proSummaryVO.getDemandNum();
                Integer billingNum = proSummaryVO.getBillingNum();
                proSummaryVO.setDiffNum(demandNum - billingNum);
                BigDecimal retailPrice = proSummaryVO.getRetailPrice();
                if (retailPrice != null) {
                    proSummaryVO.setRetailPrice(retailPrice.setScale(2, RoundingMode.HALF_UP));
                }
                BigDecimal lastPurchasePrice = proSummaryVO.getLastPurchasePrice();
                if (lastPurchasePrice != null) {
                    proSummaryVO.setLastPurchasePrice(lastPurchasePrice.setScale(2, RoundingMode.HALF_UP));
                }
            }
        }
        if (CollectionUtils.isEmpty(proSummaryList)){
            throw new BusinessException("提示：导出数据为空！");
        }
        String fileName = null;
        try{
            fileName = URLEncoder.encode("商品请货明细导出", "UTF-8").replaceAll("\\+", "%20");
        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        try {
            EasyExcel.write(response.getOutputStream(), ProSummaryVO.class)
                    .sheet("0")
                    .doWrite(proSummaryList);
        } catch (Exception e) {
            // 重置response
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            Map<String, String> map = new HashMap<>();
            map.put("status", "failure");
            map.put("message", "下载文件失败" + e.getMessage());
            try {
                response.getWriter().println(JSON.toJSONString(map));
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }

    }

}
