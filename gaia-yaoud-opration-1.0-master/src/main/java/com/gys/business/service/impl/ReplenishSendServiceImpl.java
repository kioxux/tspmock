package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.service.ReplenishSendService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.SupInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.vo.BaseResponse;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
public class ReplenishSendServiceImpl implements ReplenishSendService {
    @Resource
    private GaiaPoHeaderMapper poHeaderMapper;

    @Resource
    private GaiaPoItemMapper poItemMapper;
    @Resource
    private GaiaSdSaleDMapper saleDMapper;
    @Autowired
    private GaiaSdReplenishDMapper replenishDMapper;
    @Autowired
    private GaiaSdStockMapper stockMapper;
    @Resource
    private GaiaAuthconfiDataMapper authconfiDataMapper;
    @Autowired
    private GaiaSdReplenishHMapper replenishHMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Resource
    private GaiaProductBusinessMapper productBusinessMapper;
    @Resource
    private GaiaSupplierBusinessMapper supplierBusinessMapper;
    @Autowired
    private GaiaSoItemMapper soItemMapper;
    @Autowired
    private GaiaBatchStockMapper batchStockMapper;
    @Autowired
    private GaiaProductBasicMapper productBasicMapper;
    @Override
    public Map<String, Object> getProDetailList(GetReplenishInData inData,GetLoginOutData userInfo) {
//        List<CompadmDcOutData> compadmInfo = authconfiDataMapper.selectAuthCompadmList(inData.getClientId(),userInfo.getUserId());
//        for (CompadmDcOutData dc : compadmInfo) {
//            if (dc.getCompadmId().equals(inData.getStoChainHead())){
//                inData.setDcCode(dc.getDcCode());
//            }
//        }
        CompadmDcOutData compadmInfo = authconfiDataMapper.selectCompadmById(inData.getClientId(),inData.getStoChainHead());
        if (compadmInfo != null){
            inData.setDcCode(compadmInfo.getDcCode());
        }
        Map<String, Object> map1 = new HashMap<>();
        GetAcceptInData inData1 = new GetAcceptInData();
        inData1.setClientId(inData.getClientId());
        inData1.setStoreCode(inData.getStoChainHead());
        List<GetReplenishDetailOutData> results = new ArrayList<>();
        List<GetReplenishDetailOutData> detailOutData = this.replenishDMapper.getReplenishDetailList(inData);
        System.out.println(""+detailOutData);
        if (detailOutData != null && detailOutData.size() > 0) {
            GetReplenishCountInData countInData = new GetReplenishCountInData();
            countInData.setClientId(inData.getClientId());
            countInData.setDcCode(inData.getStoChainHead());
            List<GetReplenishInData> proList = new ArrayList<>();
            List<String> proIds = new ArrayList<>();
            //移动平均价列表查询
            GetPlaceOrderInData movInData = new GetPlaceOrderInData();
            movInData.setClientId(inData.getClientId());
            for (GetReplenishDetailOutData OutData : detailOutData) {
                GetReplenishInData replenishInData = new GetReplenishInData();
                replenishInData.setGspgProId(OutData.getGsrdProId());
                replenishInData.setStoCode(OutData.getGsrdBrId());
                proList.add(replenishInData);
                proIds.add(OutData.getGsrdProId());
            }
            List<String> proIdList = proIds.stream().distinct().collect(Collectors.toList());
            countInData.setProIds(proIdList);
            //销量列表查询
            Map<String, GetReplenishOutData> saleMap = this.getSalesDcCodeMap(countInData);

            movInData.setDcCode(inData.getDcCode());
            movInData.setProIds(proIdList);
//            movInData.setImportInDataList(movItemList);
            List<Map<String, Object>> movPriceList = replenishDMapper.getMovPriceList(movInData);
            results = detailOutData.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GetReplenishDetailOutData::getGsrdProId))), ArrayList::new)
            );
            StockInData stockInData = new StockInData();
            stockInData.setClientId(inData.getClientId());
            stockInData.setStoChainHead(inData.getStoChainHead());
            //门店库存列表
            Map<String, String> stockMap = this.getTotleStockMap(stockInData);
            //供应商列表查询
//            List<String> mapList = new ArrayList<>();
            Map<String, Object> supMap = new HashMap<>();
            supMap.put("clientId", inData.getClientId());
            supMap.put("dcCode", inData.getDcCode());
            supMap.put("proIds",proIdList);
            //门店库存列表
//            List<Map<String, Object>> supOutData =  this.replenishDMapper.getSupMsgList(supMap);
            for (GetReplenishDetailOutData result : results) {
//                if (supOutData != null && supOutData.size() > 0) {
//                    for (Map<String, Object> supMsg : supOutData) {
//                        if (supMsg.get("proCode").toString().equals(result.getGsrdProId())) {
//                            result.setGsrdSupid(supMsg.get("supId").toString());
//                            result.setGsrdSupName(supMsg.get("supName").toString());
//                        } else {
//                            result.setGsrdSupid(result.getGsrdLastSupid());
//                            result.setGsrdSupName(result.getGsrdLastSupName());
//                        }
//                    }
//                }else {
//                    result.setGsrdSupid(result.getGsrdLastSupid());
//                    result.setGsrdSupName(result.getGsrdLastSupName());
//                }
//                result.setGsrdSupid(result.getGsrdLastSupid());
//                result.setGsrdSupName(result.getGsrdLastSupName());
                GetReplenishOutData replenishOutData = saleMap.get(result.getGsrdProId());
                if (ObjectUtil.isNull(replenishOutData)) {
                    result.setGsrdSalesDays1("0");
                    result.setGsrdSalesDays2("0");
                    result.setGsrdSalesDays3("0");
                } else {
                    result.setGsrdSalesDays1(replenishOutData.getSalesCountOne());
                    result.setGsrdSalesDays2(replenishOutData.getSalesCountTwo());
                    result.setGsrdSalesDays3(replenishOutData.getSalesCountThree());
                }
                //查门店库存
                if (stockMap.size() > 0) {
                    if (stockMap.containsKey(result.getGsrdProId())) {
                        result.setGsrdStockStore(stockMap.get(result.getGsrdProId()));
                    }else{
                        result.setGsrdStockStore("0.00");
                    }
                }else {
                    result.setGsrdStockStore("0.00");
                }
                for (Map<String, Object> movMap : movPriceList) {
                    if (movMap.get("productCode").toString().equals(result.getGsrdProId())) {
                        if (ObjectUtil.isNotEmpty(movMap.get("movPrice").toString())) {
                            result.setGsrdAverageCost(new BigDecimal(movMap.get("movPrice").toString()).setScale(2,BigDecimal.ROUND_HALF_UP));
                        } else {
                            result.setGsrdAverageCost(BigDecimal.ZERO.setScale(2,BigDecimal.ROUND_HALF_UP));
                        }
                    }
                }
                String proId = result.getGsrdProId();
                BigDecimal needTotalQty = BigDecimal.ZERO;
                BigDecimal totalQty = BigDecimal.ZERO;
                BigDecimal totalPrice = BigDecimal.ZERO;
                int count = 0;
                List<String> prices = new ArrayList<>();
                for (GetReplenishDetailOutData data : detailOutData) {
                    String itemProId = data.getGsrdProId();
                    if (itemProId.equals(proId)) {
                        if (ObjectUtil.isNotEmpty(data.getGsrdProposeQty())){
                            if (("0.0000").equals(data.getGsrdProposeQty())){
                                data.setGsrdProposeQty(data.getGsrdNeedQty());
                            }
                        }else {
                            data.setGsrdProposeQty(data.getGsrdNeedQty());
                        }
                        needTotalQty = needTotalQty.add(new BigDecimal(data.getGsrdProposeQty()));
                        totalQty = totalQty.add(new BigDecimal(data.getGsrdNeedQty()).subtract(new BigDecimal(data.getGsrdDnQty()))).setScale(2,BigDecimal.ROUND_HALF_UP);
                        if (ObjectUtil.isNull(data.getGsrdProPrice())) {
                            data.setGsrdProPrice("0.00");
                        }
                        int a = new BigDecimal(data.getGsrdProPrice()).compareTo(BigDecimal.ZERO);
                        if (a == 0){
                            data.setGsrdProPrice("0.00");
                        }
                        prices.add(data.getGsrdProPrice());
                        if (ObjectUtil.isNotNull(data.getGsrdAverageCost())) {
                            totalPrice = totalPrice.add(data.getGsrdAverageCost()).setScale(2,BigDecimal.ROUND_HALF_UP);
                        }
                        count++;
                    }
                }
                // map的key存放数组中存在的数字，value存放该数字在数组中出现的次数
                HashMap<String, Integer> map = new HashMap<String, Integer>();
                for (int i = 0; i < prices.size(); i++) {
                    if (map.containsKey(prices.get(i))) {
                        int temp = map.get(prices.get(i));
                        map.put(prices.get(i), temp + 1);
                    } else {
                        map.put(prices.get(i), 1);
                    }
                }

                Collection<Integer> values = map.values();
                // 找出map的value中最大的数字，也就是数组中数字出现最多的次数
                int maxCount = Collections.max(values);
                String maxNumber = "";
                for (Map.Entry<String, Integer> entry : map.entrySet()) {
                    // 得到value为maxCount的key，也就是数组中出现次数最多的数字
                    if (maxCount == entry.getValue()) {
                        maxNumber = entry.getKey();
                    }
                }
                BigDecimal avgPrice = totalPrice.divide(new BigDecimal(count), 4, BigDecimal.ROUND_UP);
                result.setGsrdAverageCost(avgPrice);
                result.setGsrdProPrice(maxNumber);
                if(!((new BigDecimal(maxNumber).compareTo(BigDecimal.ZERO)) == 0)) {
                    BigDecimal gsrdGrossMargin = (new BigDecimal(maxNumber).subtract(avgPrice)).divide(new BigDecimal(maxNumber), 2, BigDecimal.ROUND_UP);
                    result.setGsrdGrossMargin(gsrdGrossMargin);
                }else {
                    result.setGsrdGrossMargin(new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                result.setGsrdNeedQty(totalQty.setScale(2,BigDecimal.ROUND_HALF_UP).toString());
                result.setGsrdProposeQty(totalQty.setScale(2,BigDecimal.ROUND_HALF_UP).toString());
                result.setStoNum(count + "");
            }
        }
        //放置可选供应商
        List<SupPriceRatioOutData> supPrice =  this.supplierBusinessMapper.getRatioSupPrice(inData1);
        for (GetReplenishDetailOutData data : results) {
//            data.setSupCode1("1010011");
//            data.setSupName1("兵兵杭州公司");
//            data.setSupPrice1("5.5");
//            data.setSupUpdateDate1("20201223");
//            data.setSupCode2("1010012");
//            data.setSupName2("苏州通和生物科技有限公司");
//            data.setSupPrice2("5.7");
//            data.setSupUpdateDate2("20201223");
//            data.setSupCode3("1010013");
//            data.setSupName3("中澳生物");
//            data.setSupPrice3("6.3");
//            data.setSupUpdateDate3("20201223");
            data.setSupCode1("");
            data.setSupName1("");
            data.setSupPrice1("");
            data.setSupUpdateDate1("");
            data.setSupCode2("");
            data.setSupName2("");
            data.setSupPrice2("");
            data.setSupUpdateDate2("");
            data.setSupCode3("");
            data.setSupName3("");
            data.setSupPrice3("");
            data.setSupUpdateDate3("");
            if (ObjectUtil.isNotNull(supPrice) && supPrice.size() > 0){
                List<SupPriceRatioOutData> result = supPrice.stream().filter(u->u.getProCode().equals(data.getGsrdProId())).collect(Collectors.toList());
                if (ObjectUtil.isNotNull(result) && result.size() > 0) {
                    for (int i = 0; i < result.size(); i++) {
                        String dateString = "";
                        if (ObjectUtil.isNotEmpty(result.get(i).getSupUpdateDate())
                                && ObjectUtil.isNotEmpty(result.get(i).getSupUpdateTime())) {
                            try {
                                DateFormat format = new SimpleDateFormat("yyyyMMdd");
                                Date dateTime1 = format.parse(result.get(i).getSupUpdateDate());
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String date1 = formatter.format(dateTime1);
                                DateFormat format1 = new SimpleDateFormat("HHmmss");
                                Date dateTime2 = format1.parse(result.get(i).getSupUpdateTime());
                                SimpleDateFormat formatter1 = new SimpleDateFormat("HH:mm:ss");
                                String time = formatter1.format(dateTime2);
                                dateString = date1 + " " + time;
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if (i == 0){
                            data.setSupCode1(result.get(0).getSupCode());
                            data.setSupName1(result.get(0).getSupName());
                            data.setSupPrice1(result.get(0).getProPrice());
                            data.setSupUpdateDate1(dateString);
                        }else if (i == 1){
                            data.setSupCode2(result.get(1).getSupCode());
                            data.setSupName2(result.get(1).getSupName());
                            data.setSupPrice2(result.get(1).getProPrice());
                            data.setSupUpdateDate2(dateString);
                        }else if (i == 2){
                            data.setSupCode3(result.get(2).getSupCode());
                            data.setSupName3(result.get(2).getSupName());
                            data.setSupPrice3(result.get(2).getProPrice());
                            data.setSupUpdateDate3(dateString);
                        }
                    }
                }
            }
        }
        List<GetReplenishDetailOutData> result = addSupInfo(results,inData);
        //汇总
        BigDecimal proCount = new BigDecimal(result.size());//商品品类数
        BigDecimal proTotalCount = BigDecimal.ZERO;//补货总数
        BigDecimal proTotalAmt = BigDecimal.ZERO;
        for (GetReplenishDetailOutData item : result){
            if (ObjectUtil.isEmpty(item.getGsrdNeedQty())){
                item.setGsrdNeedQty("0");
            }
            proTotalCount = proTotalCount.add(new BigDecimal(item.getGsrdNeedQty()));
            if (ObjectUtil.isEmpty(item.getGsrdProPrice())){
                item.setGsrdProPrice("0");
            }
            proTotalAmt = proTotalAmt.add(new BigDecimal(item.getGsrdNeedQty()).multiply(new BigDecimal(item.getGsrdProPrice())));
        }
        map1.put("outDataList",result);
        map1.put("proCount",proCount);
        map1.put("proTotalCount",proTotalCount);
        map1.put("proTotalAmt",proTotalAmt);
        return map1;
    }

    @Override
    public List<GetReplenishDetailOutData> getReplenishDetailList(GetReplenishInData inData,GetLoginOutData userInfo) {
        CompadmDcOutData compadmInfo = authconfiDataMapper.selectCompadmById(inData.getClientId(),inData.getStoChainHead());
        if (compadmInfo != null){
            inData.setDcCode(compadmInfo.getDcCode());
        }
        List<GetReplenishDetailOutData> result = new ArrayList<>();
        List<GetReplenishDetailOutData> detailList = this.replenishDMapper.getReplenishDetailList(inData);
//        List<GetReplenishDetailOutData> detailList = addSupInfo(detailList1,inData);
        if (detailList != null && detailList.size() > 0) {
            GetReplenishCountInData countInData = new GetReplenishCountInData();
            countInData.setClientId(inData.getClientId());
            List<GetReplenishInData> proList = new ArrayList<>();
            List<StockInData> stockItemList = new ArrayList<>();
            //供应商列表查询
            List<String> proIdMap = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            map.put("clientId", inData.getClientId());
            map.put("dcCode", inData.getDcCode());
            GetPlaceOrderInData movInData = new GetPlaceOrderInData();
            movInData.setClientId(inData.getClientId());
            List<ImportInData> importInDataList = new ArrayList<>();
            for (GetReplenishDetailOutData detailOutData : detailList) {
                GetReplenishInData replenishInData = new GetReplenishInData();
                replenishInData.setGspgProId(detailOutData.getGsrdProId());
                replenishInData.setStoCode(detailOutData.getGsrdBrId());
                proList.add(replenishInData);
                StockInData stockInData = new StockInData();
                stockInData.setClientId(inData.getClientId());
                stockInData.setGssBrId(detailOutData.getGsrdBrId());
                stockInData.setGssProId(detailOutData.getGsrdProId());
                stockItemList.add(stockInData);
                proIdMap.add(detailOutData.getGsrdProId());
                ImportInData importInData = new ImportInData();
                importInData.setGsrdBrId(detailOutData.getGsrdBrId());
                importInData.setGsrdProId(detailOutData.getGsrdProId());
                importInDataList.add(importInData);
            }
            List<GetReplenishInData> itemList = proList.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGspgProId() + f.getStoCode() + f.getClientId()))), ArrayList::new)
            );
            countInData.setItemList(itemList);
            List<GetReplenishOutData> list = this.soItemMapper.selectCountByProIds(countInData);
            //门店库存列表
            List<StockInData> stockInData = stockItemList.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGssBrId() + f.getClientId() + f.getGssProId()))), ArrayList::new)
            );
            Map<String, String> stockMap = this.getStockMap(stockInData);

            //门店库存列表
            List<String> proIds = proIdMap.stream().distinct().collect(Collectors.toList());
            map.put("proIds",proIds);
            List<Map<String, Object>> supOutData = this.replenishDMapper.getSupMsgList(map);
            //移动平均价
            List<ImportInData> imInData = importInDataList.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGsrdBrId() + f.getGsrdProId()))), ArrayList::new)
            );
            movInData.setImportInDataList(imInData);
            List<Map<String, Object>> movPriceList = replenishDMapper.getMovPriceListByBrIds(movInData);
            for (GetReplenishDetailOutData detail : detailList) {
                if (supOutData != null && supOutData.size() > 0) {
                    for (Map<String, Object> supMsg : supOutData) {
                        if (supMsg.get("proCode").toString().equals(detail.getGsrdProId())) {
                            detail.setGsrdSupid(supMsg.get("supId").toString());
                            detail.setGsrdSupName(supMsg.get("supName").toString());
                            if (ObjectUtil.isNotEmpty(supMsg.get("payTerm").toString())) {
                                detail.setGsrdPayTerm(supMsg.get("payTerm").toString());
                            } else {
                                detail.setGsrdPayTerm("");
                            }
                        } else {
                            detail.setGsrdSupid(detail.getGsrdLastSupid());
                            detail.setGsrdSupName(detail.getGsrdLastSupName());
                        }
                    }
                }else {
                    detail.setGsrdSupid(detail.getGsrdLastSupid());
                    detail.setGsrdSupName(detail.getGsrdLastSupName());
                }
                if (list.size() > 0) {
                    for (GetReplenishOutData outData : list) {
                        if (detail.getGsrdBrId().equals(outData.getBrId())
                                && detail.getGsrdProId().equals(outData.getProId())) {
                            detail.setGsrdSalesDays1("0");
                            detail.setGsrdSalesDays2(outData.getSalesCountTwo());
                            detail.setGsrdSalesDays3("0");
                        }
                    }
                }else{
                    detail.setGsrdSalesDays1("0");
                    detail.setGsrdSalesDays2("0");
                    detail.setGsrdSalesDays3("0");
                }
                if (ObjectUtil.isNull(detail.getGsrdAverageCost())) {
                    detail.setGsrdAverageCost(BigDecimal.ZERO);
                }
                if (stockMap.size() > 0) {
                    if (stockMap.containsKey(detail.getGsrdProId())) {
                        detail.setGsrdStockStore(stockMap.get(detail.getGsrdProId()));
                    }else{
                        detail.setGsrdStockStore("0");
                    }
                }else {
                    detail.setGsrdStockStore("0");
                }
                for (Map<String,Object> mov :movPriceList) {
                    if (mov.get("broCode").toString().equals(detail.getGsrdBrId())
                            && mov.get("productCode").toString().equals(detail.getGsrdProId())){
                        detail.setGsrdAverageCost(new BigDecimal(mov.get("movPrice").toString()));
                    }
                }
                detail.setGsrdProposeQty(detail.getGsrdNeedQty());
                if (ObjectUtil.isNotEmpty(detail.getGsrdProPrice())){
                    if (new BigDecimal(detail.getGsrdProPrice()).compareTo(new BigDecimal(0)) == 0){
                        detail.setGsrdGrossMargin(BigDecimal.ZERO);
                    }else{
                        BigDecimal gsrdGrossMargin = (new BigDecimal(detail.getGsrdProPrice()).subtract(detail.getGsrdAverageCost())).divide(new BigDecimal(detail.getGsrdProPrice()), 4, BigDecimal.ROUND_UP).multiply(new BigDecimal(100));
                        detail.setGsrdGrossMargin(gsrdGrossMargin);
                    }
                }else{
                    detail.setGsrdGrossMargin(BigDecimal.ZERO);
                }
            }
            for (GetReplenishDetailOutData outData : inData.getDetailInList()){
                Integer endNeedQty = 0;
                int count = 1;
                for (GetReplenishDetailOutData detail : detailList) {
                    if (outData.getGsrdProId().equals(detail.getGsrdProId())) {
                        Integer num = (int)Math.round(Double.valueOf(detail.getGsrdProposeQty())/Double.valueOf(outData.getGsrdProposeQty())*Double.valueOf(outData.getGsrdNeedQty()));
                        detail.setPoPrice(outData.getPoPrice());
                        detail.setGsrdSupid(outData.getGsrdSupid());
                        detail.setGsrdSupName(outData.getGsrdSupName());
                        detail.setGsrdInputTax(outData.getGsrdInputTax());
                        detail.setGsrdInputTaxValue(outData.getGsrdInputTaxValue());
                        detail.setGsrdNeedQty(num+"");
                        if (outData.getStoNum().equals(count+"")){
//                            detail.setGsrdNeedQty(Integer.valueOf(outData.getGsrdNeedQty())-endNeedQty+"");
                            BigDecimal b = new BigDecimal(outData.getGsrdNeedQty()).subtract(new BigDecimal(endNeedQty));
                            detail.setGsrdNeedQty(b.toString());
                        }else{
                            endNeedQty += num;
                        }
                        count ++;
                    }
                }
            }
            result = addSupInfo(detailList,inData);
        }
        return result;
    }

    @Transactional
    @Override
    public List<GetReplenishInData> placeOrder(GetPlaceOrderInData inData, GetLoginOutData userInfo) {
        List<CompadmDcOutData> compadmInfo = authconfiDataMapper.selectAuthCompadmList(inData.getClientId(),userInfo.getUserId());
//        inData.setDcCode(compadmInfo.get(0).getDcCode());
        List<GetReplenishInData> replenishInDataList = new ArrayList<>();
        List<GetReplenishDetailOutData> results = new ArrayList<>();
        List<GetReplenishDetailOutData> orderList = inData.getOrderList();
        if (inData.getType().equals("1")) {
            results = inData.getOrderList().stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGsrdSupid() + f.getGsrdBrId() + f.getGsrdVoucherId()))), ArrayList::new)
            );
            for (GetReplenishDetailOutData detail : results) {
                detail.setClientId(inData.getClientId());
                GetReplenishInData replenishInData = new GetReplenishInData();
                replenishInData.setClientId(inData.getClientId());
                replenishInData.setGsrhVoucherId(detail.getGsrdVoucherId());
                replenishInData.setStoCode(detail.getGsrdBrId());
                replenishInData.setGsrhType("3");
                replenishInData.setGsrhPattern("0");
                if (compadmInfo.size() > 0) {
                    replenishInData.setDcCode(compadmInfo.get(0).getDcCode());
                }
                List<GetReplenishDetailInData> detailList = new ArrayList<>();
                for (GetReplenishDetailOutData item : orderList){
                    if (detail.getGsrdSupid().equals(item.getGsrdSupid() )
                            && detail.getGsrdBrId().equals(item.getGsrdBrId())
                            && detail.getGsrdVoucherId().equals(item.getGsrdVoucherId())){
                        GetReplenishDetailInData detailInData = new GetReplenishDetailInData();
                        detailInData.setClientId(inData.getClientId());
                        detailInData.setGsrdDate(item.getGsrdDate());
                        detailInData.setGsrdProUnit(item.getGsrdProUnit());
                        detailInData.setGsrdProId(item.getGsrdProId());
                        detailInData.setGsrdProName(item.getGsrdProName());
                        detailInData.setGsrdBrId(item.getGsrdBrId());
                        detailInData.setGsrdBrName(item.getGsrdBrName());
                        detailInData.setGsrdAverageCost(item.getGsrdAverageCost());
                        detailInData.setGsrdNeedQty(item.getGsrdNeedQty());
                        detailInData.setGsrdProposeQty(item.getGsrdProposeQty());
                        detailInData.setGsrdLastSupid(item.getGsrdSupid());
                        detailInData.setGsrdLastSupName(item.getGsrdSupName());
                        detailInData.setGsrdProPrice(new BigDecimal(item.getGsrdProPrice()));
                        detailInData.setVoucherAmt(item.getGsrdAverageCost().multiply(new BigDecimal(item.getGsrdNeedQty())).toString());
                        detailInData.setGsrdDisplayMin(item.getGsrdDisplayMin());
                        detailInData.setGsrdGrossMargin(item.getGsrdGrossMargin().toString());
                        detailInData.setGsrdSalesDays1(item.getGsrdSalesDays1());
                        detailInData.setGsrdSalesDays2(item.getGsrdSalesDays2());
                        detailInData.setGsrdSalesDays3(item.getGsrdSalesDays3());
                        detailInData.setGsrdSerial(item.getGsrdSerial());
                        detailInData.setGsrdVoucherId(item.getGsrdVoucherId());
                        detailInData.setGsrdPackMidsize(item.getGsrdPackMidsize());
                        detailInData.setGsrdStockDepot(item.getGsrdStockDepot());
                        detailInData.setGsrdStockStore(item.getGsrdStockStore());
                        detailInData.setGsrdProRate(item.getGsrdInputTaxValue());
                        detailInData.setGsrdProRateCode(item.getGsrdInputTax());
                        detailInData.setGsrdTaxTate(item.getGsrdTaxTate());
                        detailInData.setGsrdLastPrice(item.getGsrdLastPrice());
                        if (ObjectUtil.isEmpty(item.getPoPrice())){
                            item.setPoPrice("0");
                        }
                        detailInData.setAllStoreStock(item.getAllStoreStock());
                        detailInData.setPoPrice(item.getPoPrice());
                        detailInData.setGsrdProCompany(item.getGsrdProCompany());
                        detailInData.setGsrdProPec(item.getGsrdProPec());
                        detailInData.setGsrdProCommonName(item.getGsrdProCommonName());
                        detailList.add(detailInData);
                    }
                }
                replenishInData.setDetailList(detailList);
                this.update(replenishInData);
                String vv = this.insertPo(replenishInData,userInfo);
                replenishInData.setGsrhVoucherId(vv);
                replenishInDataList.add(replenishInData);
            }
        }else {
            results = inData.getOrderList().stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGsrdSupid() + f.getGsrdBrId()))), ArrayList::new)
            );
            for (GetReplenishDetailOutData data : results) {
                GaiaSdReplenishH vData = new GaiaSdReplenishH();
                vData.setClientId(userInfo.getClient());
                vData.setGsrhBrId(data.getGsrdBrId());
                GetReplenishInData replenishInData = new GetReplenishInData();
                replenishInData.setClientId(inData.getClientId());
                replenishInData.setStoCode(data.getGsrdBrId());
                replenishInData.setGsrhType("3");
                replenishInData.setGsrhPattern("2");
                replenishInData.setDcCode(compadmInfo.get(0).getDcCode());
                replenishInData.setGsrhEmp(userInfo.getUserId());
                List<GetReplenishDetailInData> detailList = new ArrayList<>();
                orderList = inData.getOrderList().stream().sorted(
                        Comparator.comparing(GetReplenishDetailOutData :: getGsrdProId)).collect(Collectors.toList());
                int count = 1;
                for (GetReplenishDetailOutData item : orderList){
                    if (ObjectUtil.isNotNull(item.getGsrdSupid())){
                        item.setGsrdLastSupid(item.getGsrdSupid());
                    }
                    if (data.getClientId().equals(item.getClientId())
                            && data.getGsrdLastSupid().equals(item.getGsrdLastSupid())
                            && data.getGsrdBrId().equals(item.getGsrdBrId())){
                        GetReplenishDetailInData detailInData = new GetReplenishDetailInData();
                        detailInData.setClientId(inData.getClientId());
                        detailInData.setGsrdDate(item.getGsrdDate());
                        detailInData.setGsrdProName(item.getGsrdProName());
                        detailInData.setGsrdBrId(item.getGsrdBrId());
                        detailInData.setGsrdBrName(item.getGsrdBrName());
                        detailInData.setGsrdProId(item.getGsrdProId());
                        detailInData.setGsrdAverageCost(item.getGsrdAverageCost());
                        detailInData.setGsrdNeedQty(item.getGsrdNeedQty());
                        detailInData.setGsrdProposeQty(item.getGsrdProposeQty());
                        detailInData.setGsrdLastSupid(item.getGsrdSupid());
                        detailInData.setGsrdLastSupName(item.getGsrdSupName());
                        if (ObjectUtil.isEmpty(item.getGsrdProPrice())){
                            item.setGsrdProPrice("0");
                        }
                        detailInData.setGsrdProPrice(new BigDecimal(item.getGsrdProPrice()));
                        detailInData.setVoucherAmt(item.getGsrdAverageCost().multiply(new BigDecimal(item.getGsrdNeedQty())).toString());
                        detailInData.setGsrdDisplayMin(item.getGsrdDisplayMin());
                        detailInData.setGsrdGrossMargin(item.getGsrdGrossMargin().toString());
                        detailInData.setGsrdSalesDays1(item.getGsrdSalesDays1());
                        detailInData.setGsrdSalesDays2(item.getGsrdSalesDays2());
                        detailInData.setGsrdSalesDays3(item.getGsrdSalesDays3());
                        detailInData.setGsrdSerial(count+"");
//                        detailInData.setGsrdVoucherId(item.getGsrdVoucherId());
                        detailInData.setGsrdPackMidsize(item.getGsrdPackMidsize());
                        detailInData.setGsrdStockDepot(item.getGsrdStockDepot());
                        detailInData.setGsrdStockStore(item.getGsrdStockStore());
                        detailInData.setGsrdProUnit(item.getGsrdProUnit());
                        detailInData.setGsrdProRate(item.getGsrdInputTaxValue());
                        detailInData.setGsrdProRateCode(item.getGsrdInputTax());
                        detailInData.setGsrdTaxTate(item.getGsrdInputTax());
                        detailInData.setGsrdLastPrice(item.getGsrdLastPrice());
                        detailInData.setPoPrice(item.getPoPrice());
                        detailInData.setGsrdProCompany(item.getGsrdProCompany());
                        detailInData.setGsrdProPec(item.getGsrdProPec());
                        detailInData.setGsrdProCommonName(item.getGsrdProCommonName());
                        detailList.add(detailInData);
                        count ++;
                    }
                }
                replenishInData.setDetailList(detailList);
                this.insert(replenishInData);
                String vv = this.insertPo(replenishInData,userInfo);
                replenishInData.setGsrhVoucherId(vv);
                replenishInDataList.add(replenishInData);
            }
        }
        for (GetReplenishInData replenishInData : replenishInDataList){
            String voucherId = replenishInData.getGsrhVoucherId();
            List<GetReplenishDetailInData> detailInDataList = replenishInData.getDetailList();
            for (GetReplenishDetailInData detailInData : detailInDataList){
                detailInData.setGsrdVoucherId(voucherId);
            }
        }
        List<GetReplenishDetailInData> outItemDataList = new ArrayList<>();
        for (GetReplenishInData replenishInData : replenishInDataList){
            outItemDataList.addAll(replenishInData.getDetailList());
        }
        List<GetReplenishDetailInData> sortList = outItemDataList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGsrdLastSupid()))), ArrayList::new)
        );
        List<GetReplenishInData> outDataList = new ArrayList<>();
        for (GetReplenishDetailInData sort : sortList){
            GetReplenishInData outData = new GetReplenishInData();
            outData.setGspgProId(sort.getGsrdLastSupid());
            List<GetReplenishDetailInData> item = new ArrayList<>();
            for (GetReplenishDetailInData replenishInData : outItemDataList) {
                if (replenishInData.getGsrdLastSupid().equals(sort.getGsrdLastSupid())){
                    item.add(replenishInData);
                }
            }
            outData.setDetailList(item);
            outDataList.add(outData);
        }
        return outDataList;
    }

    @Override
    public List<GetReplenishDetailOutData> getImportExcelDetailList(GetPlaceOrderInData inData) {
        List<GetReplenishDetailOutData> outData = new ArrayList<>();
        List<Map<String,Object>> movPriceList = replenishDMapper.getMovPriceListByBrIds(inData);
        List<GetSupOutData> supList = supplierBusinessMapper.getSupByList(inData);
        List<GetQueryProductOutData> productOutDataList = productBusinessMapper.queryProductDetailByList(inData);
        GetReplenishCountInData countInData = new GetReplenishCountInData();
        countInData.setClientId(inData.getClientId());
        List<GetReplenishInData> itemList = new ArrayList<>();
        List<StockInData> stockItemList = new ArrayList<>();
        BigDecimal allStoreStock = BigDecimal.ZERO;
        for (GetQueryProductOutData proDetail : productOutDataList){
            GetReplenishInData replenishInData = new GetReplenishInData();
            replenishInData.setGspgProId(proDetail.getProCode());
            replenishInData.setStoCode(proDetail.getBrId());
            itemList.add(replenishInData);
            StockInData stockInData = new StockInData();
            stockInData.setClientId(inData.getClientId());
            stockInData.setGssBrId(proDetail.getBrId());
            stockInData.setGssProId(proDetail.getProCode());
            stockItemList.add(stockInData);
        }
        countInData.setItemList(itemList);
        GetReplenishCountInData saleData = new GetReplenishCountInData();
        saleData.setClientId(inData.getClientId());
        List<GetReplenishOutData> list = this.soItemMapper.selectCountByProIds(saleData);
        //门店库存列表
        List<StockInData> stockInData = stockItemList.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getGssBrId() + f.getClientId() + f.getGssProId()))), ArrayList::new)
        );
        Map<String, String> stockMap = this.getStockMap(stockInData);
        for (GetQueryProductOutData data : productOutDataList) {
            GetReplenishDetailOutData replenishDetailOutData = new GetReplenishDetailOutData();
            replenishDetailOutData.setClientId(inData.getClientId());
            replenishDetailOutData.setGsrdBrId(data.getBrId());
            replenishDetailOutData.setGsrdBrName(data.getBrName());
            replenishDetailOutData.setGsrdProPrice(data.getPrcAmount());
            replenishDetailOutData.setGsrdProName(data.getProName());
            replenishDetailOutData.setGsrdProUnit(data.getProUnit());
            replenishDetailOutData.setGsrdDate(CommonUtil.getyyyyMMdd());
            replenishDetailOutData.setGsrdPackMidsize(data.getProMidPackage());
            replenishDetailOutData.setGsrdProPec(data.getProSpecs());
            replenishDetailOutData.setGsrdpProForm(data.getProForm());
            replenishDetailOutData.setGsrdProCompany(data.getProFactoryName());
            replenishDetailOutData.setGsrdProLife(data.getProLife());
            replenishDetailOutData.setGsrdProPlace(data.getProPlace());
            replenishDetailOutData.setGsrdProId(data.getProCode());
            replenishDetailOutData.setGsrdStockStore(data.getProGssQty());
            replenishDetailOutData.setGsrdInputTaxValue(data.getGsrdInputTaxValue());
            replenishDetailOutData.setGsrdInputTax(data.getGsrdInputTax());
            replenishDetailOutData.setGsrdProCommonName(data.getProCommonName());
            if (ObjectUtil.isNotEmpty(data.getBigClass())){
                replenishDetailOutData.setBigClass(data.getBigClass());
            }
            if (ObjectUtil.isNotEmpty(data.getMidClass())){
                replenishDetailOutData.setMidClass(data.getMidClass());
            }
            if (ObjectUtil.isNotEmpty(data.getMinClass())){
                replenishDetailOutData.setMinClass(data.getMinClass());
            }
            for (ImportInData importInData : inData.getImportInDataList()) {
                if (importInData.getGsrdBrId().equals(data.getBrId())
                        && importInData.getGsrdProId().equals(data.getProCode())){
                    replenishDetailOutData.setGsrdNeedQty(importInData.getGsrdNeedQty());
                    replenishDetailOutData.setGsrdProposeQty(importInData.getGsrdNeedQty());
                    BigDecimal grossMargin = BigDecimal.ZERO;
                    if (ObjectUtil.isEmpty(data.getPrcAmount())){
                        data.setPrcAmount("0");
                    }
                    int a = new BigDecimal(data.getPrcAmount()).compareTo(BigDecimal.ZERO);
                    if (a != 0) {
                        grossMargin = ((new BigDecimal(data.getPrcAmount()).subtract(
                                new BigDecimal(importInData.getPoPrice()))).divide(new BigDecimal(data.getPrcAmount()), 2).multiply(new BigDecimal(100)));
                    }
                    replenishDetailOutData.setGsrdGrossMargin(grossMargin);
                    replenishDetailOutData.setGsrdLastPrice(importInData.getPoPrice());
                    for (GetSupOutData sup : supList) {
                        if (sup.getSupCode().equals(importInData.getGsrdSupid())){
                            replenishDetailOutData.setGsrdLastSupid(sup.getSupCode());
                            replenishDetailOutData.setGsrdLastSupName(sup.getSupName());
                            replenishDetailOutData.setGsrdSupName(sup.getSupName());
                            replenishDetailOutData.setGsrdSupid(sup.getSupCode());
                        }
                    }
                }
            }
            GetReplenishInData replenishInData = new GetReplenishInData();
            replenishInData.setClientId(inData.getClientId());
            replenishInData.setStoCode(data.getBrId());
            replenishDetailOutData.setGsrdSalesDays1("0");
            replenishDetailOutData.setGsrdSalesDays2("0");
            replenishDetailOutData.setGsrdSalesDays3("0");
            for (GetReplenishOutData replenishOutData : list) {
                if (replenishOutData.getBrId().equals(data.getBrId())
                        && replenishOutData.getProId().equals(data.getProCode())) {
                    replenishDetailOutData.setGsrdSalesDays1(replenishOutData.getSalesCountOne());
                    replenishDetailOutData.setGsrdSalesDays2(replenishOutData.getSalesCountTwo());
                    replenishDetailOutData.setGsrdSalesDays3(replenishOutData.getSalesCountThree());
                }
            }
            replenishDetailOutData.setGsrdStockStore(stockMap.get(data.getProCode()));
            replenishInData.setProductCode(data.getProCode());
//            Map<String, GetReplenishOutData> costMap = this.getCostMap(replenishInData);
//            replenishOutData = costMap.get(data.getProCode());
//            if (ObjectUtil.isNotNull(replenishOutData)) {
//                replenishDetailOutData.setGsrdAverageCost(replenishOutData.getAverageCost());
//                replenishDetailOutData.setGsrdGrossMargin(replenishOutData.getBatchCost());
//            }
            for (ImportInData im : inData.getImportInDataList()) {
                if (im.getGsrdProId().equals(replenishDetailOutData.getGsrdProId())
                        && im.getGsrdSupid().equals(replenishDetailOutData.getGsrdSupid())
                        && im.getGsrdBrId().equals(replenishDetailOutData.getGsrdBrId())){
                    replenishDetailOutData.setPoPrice(im.getPoPrice());
                    if (ObjectUtil.isEmpty(im.getPoPrice())){
                        im.setPoPrice("0");
                    }
                    replenishDetailOutData.setGsrdLastPrice(im.getPoPrice());
                    replenishDetailOutData.setGsrdNeedQty(im.getGsrdNeedQty());
                }
            }
            for (Map<String,Object> mov :movPriceList) {
                if (mov.get("broCode").toString().equals(data.getBrId())
                        && mov.get("productCode").toString().equals(data.getProCode())){
                    replenishDetailOutData.setGsrdAverageCost(new BigDecimal(mov.get("movPrice").toString()));
                }else{
                    replenishDetailOutData.setGsrdAverageCost(BigDecimal.ZERO);
                }
            }
            if (ObjectUtil.isNull(replenishDetailOutData.getGsrdAverageCost())){
                replenishDetailOutData.setGsrdAverageCost(BigDecimal.ZERO);
            }
            if (ObjectUtil.isEmpty(replenishDetailOutData.getGsrdStockStore())){
                replenishDetailOutData.setGsrdStockStore("0");
            }
            outData.add(replenishDetailOutData);
        }
        return outData;
    }

    @Override
    public String addSendRepPrice(RatioProInData inData) {
//        if (ObjectUtil.isEmpty(inData.getStoChinaHead())) {
////        Map<String,String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(),inData.getBrId());
//            List<CompadmDcOutData> dcOutDataList = this.authconfiDataMapper.selectAuthCompadmList(inData.getClientId(), inData.getUserId());
//            if (dcOutDataList != null && dcOutDataList.size() > 0) {
//                inData.setBrId(dcOutDataList.get(0).getCompadmId());
//            }
//        }else{
//            inData.setBrId(inData.getStoChinaHead());
//        }

        List<CompadmDcOutData> dcOutDataList = this.authconfiDataMapper.selectAuthCompadmList(inData.getClientId(), inData.getUserId());
        if (dcOutDataList != null && dcOutDataList.size() > 0) {
            inData.setBrId(dcOutDataList.get(0).getCompadmId());
            if (ObjectUtil.isNotEmpty(inData.getStoChinaHead())){
                for (CompadmDcOutData dcOutData : dcOutDataList) {
                    if (inData.getStoChinaHead().equals(dcOutData.getCompadmId())){
                        inData.setBrId(dcOutData.getDcCode());
                        break;
                    }
                }
            }
        }

        String pgId = productBasicMapper.selectNextPoId(inData.getClientId());
        List<RatioProDetail> list = this.productBasicMapper.selectRatioStatus(inData);
        if (list.size() > 0) {
            inData.setPgId(list.get(0).getPgId());
            if (!list.get(0).getRepStatus().equals("2")) {
                productBasicMapper.updateStatus(inData);
            }
        }
        List<RatioProDetail> proDetail = inData.getProDetail();
        if (proDetail.size() > 0) {
            int i = 1;
            for (RatioProDetail detail : proDetail) {
                detail.setClientId(inData.getClientId());
                detail.setPgId(pgId);
                detail.setPgItem(String.valueOf(i));
//                detail.setRepType("0");
                detail.setBrId(inData.getBrId());
//                if (stoInfo.get("stoAttribute").equals("2")) {
                    detail.setRepType("1");
                    detail.setBrId(inData.getBrId());
//                }
                detail.setRepDate(CommonUtil.getyyyyMMdd());
                detail.setCreateDate(CommonUtil.getyyyyMMdd());
                detail.setCreateTime(CommonUtil.getHHmmss());
                detail.setCreateUser(inData.getUserId());
                detail.setRepStatus("0");
                if (ObjectUtil.isEmpty(detail.getGsrdFlag())){
                    detail.setGsrdFlag("");
                }
                i++;
            }
            this.productBasicMapper.addRepPrice(proDetail);
        }
        return pgId;
    }

    @Override
    public Map<String, Object> selectSendRatioStatus(RatioProInData inData) {
//        Map<String,String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(),inData.getBrId());
        List<CompadmDcOutData> dcOutDataList = this.authconfiDataMapper.selectAuthCompadmList(inData.getClientId(), inData.getUserId());
        Map<String,String> map = new HashMap<>();
        map.put("client",inData.getClientId());
        if (dcOutDataList != null && dcOutDataList.size() > 0) {
            inData.setBrId(dcOutDataList.get(0).getCompadmId());
            if (ObjectUtil.isNotEmpty(inData.getStoChinaHead())){
                for (CompadmDcOutData dcOutData : dcOutDataList) {
                    if (inData.getStoChinaHead().equals(dcOutData.getCompadmId())){
                        inData.setBrId(dcOutData.getCompadmId());
                        map.put("supSite",dcOutData.getDcCode());
                        inData.setBrId(dcOutData.getDcCode());
                        break;
                    }
                }
            }
        }
        List<Map<String,String>> supList = productBasicMapper.selectSupplierList(map);
        Map<String,Object> result = new HashMap<>();
        if (ObjectUtil.isNotEmpty(inData.getPgId())) {
            map.put("pgId",inData.getPgId());
            List<Map<String,String>> supGetList = productBasicMapper.selectGetPriceBySup(map);
            List<RatioProDetail> list = this.productBasicMapper.selectRatioStatus(inData);
            if (list.size() > 0){
                if ("1".equals(list.get(0).getRepStatus())){
                    if (supList.size() > 0) {
                        BigDecimal a = (new BigDecimal(supGetList.size()).divide(new BigDecimal(supList.size()),2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100"))).setScale(2, RoundingMode.HALF_UP);
                        result.put("repStatus", "1");
                        result.put("speed",a);
                        result.put("msg", "比价中");
                    }
                }else if("0".equals(list.get(0).getRepStatus())){
                    result.put("repStatus", "0");
                    result.put("speed",0);
                    result.put("msg", "比价中");
                }else if("2".equals(list.get(0).getRepStatus())){
                    result.put("repStatus", "2");
                    result.put("speed",100);
                    result.put("msg", "比价已完成");
                }else {
                    result.put("repStatus", "3");
                    result.put("speed",100);
                    result.put("msg", "由于其他人员正在比价，当前比价已中断，您可以刷新页面查看比价结果");
                }
            }
        }else{
            result.put("repStatus", "0");
            result.put("speed",0);
            result.put("msg", "比价中");
        }
        return result;
    }

    private Map<String, GetReplenishOutData> getSalesDcCodeMap(GetReplenishCountInData inData) {
        Map<String, GetReplenishOutData> map;
        List<GetReplenishOutData> list = this.soItemMapper.selectCountByDcCode(inData);
        map = list.stream().collect(Collectors.toMap(GetReplenishOutData::getProId, x -> x, (a, b) -> b));
        return map;
    }

    private Map<String, String> getStockMap(List<StockInData> inData) {
        Map<String, String> map;
        List<GaiaSdStock> stockList = this.stockMapper.selectStocks(inData);
        map = stockList.stream().collect(Collectors.toMap(GaiaSdStock::getGssProId, GaiaSdStock::getGssQty, (a, b) -> b));
        return map;
    }
    private Map<String, String> getTotleStockMap(StockInData inData) {
        Map<String, String> map;
        List<GaiaSdStock> stockList = this.stockMapper.selectTitleStock(inData);
        map = stockList.stream().collect(Collectors.toMap(GaiaSdStock::getGssProId, GaiaSdStock::getGssQty, (a, b) -> b));
        return map;
    }
    private Map<String, GetReplenishOutData> getCostMap(GetReplenishInData inData) {
        Map<String, GetReplenishOutData> map = new HashMap();
        Example example = new Example(GaiaBatchStock.class);
        example.setOrderByClause("BAT_CREATE_DATE DESC");
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("batLocationCode", inData.getStoCode()).andEqualTo("batProCode", inData.getProductCode());
        List<GaiaBatchStock> batchStockList = this.batchStockMapper.selectByExample(example);
        Iterator var5 = batchStockList.iterator();

        while (var5.hasNext()) {
            GaiaBatchStock batchStock = (GaiaBatchStock) var5.next();
            if (!ObjectUtil.isNotNull(map.get(batchStock.getBatProCode()))) {
                GetReplenishOutData outData = new GetReplenishOutData();
                outData.setAverageCost(batchStock.getBatMovPrice());
                outData.setBatchCost(batchStock.getBatBatchCost());
                map.put(batchStock.getBatProCode(), outData);
            }
        }

        return map;
    }

    @Transactional
    public void update(GetReplenishInData inData) {
        GetLoginOutData loginOutData = new GetLoginOutData();
        loginOutData.setDepId(inData.getStoCode());
        loginOutData.setClient(inData.getClientId());
        StoreOutData storeData = storeDataMapper.queryStoLeader(loginOutData);
        inData.setGsrhType(storeData.getStoDeliveryMode());
        Example example = new Example(GaiaSdReplenishH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId())
                .andEqualTo("gsrhVoucherId", inData.getGsrhVoucherId())
                .andEqualTo("gsrhBrId", inData.getStoCode());
        GaiaSdReplenishH replenishH = replenishHMapper.selectOneByExample(example);
        inData.setGsrhDate(replenishH.getGsrhDate());
        replenishH.setGsrhFlag("1");
//        saveDetails(inData, replenishH);
        this.replenishHMapper.update(replenishH);
    }

    @Transactional
    public void insert(GetReplenishInData inData) {
        GetLoginOutData loginOutData = new GetLoginOutData();
        loginOutData.setDepId(inData.getStoCode());
        loginOutData.setClient(inData.getClientId());
        StoreOutData storeData = storeDataMapper.queryStoLeader(loginOutData);
        inData.setGsrhType(storeData.getStoDeliveryMode());
        GaiaSdReplenishH loginData = new GaiaSdReplenishH();
        loginData.setGsrhBrId(inData.getStoCode());
        loginData.setClientId(inData.getClientId());
        String voucherId = this.selectNextVoucherId(loginData);
        inData.setGsrhVoucherId(voucherId);
        inData.setGsrhDate(CommonUtil.getyyyyMMdd());
        GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
        replenishH.setClientId(inData.getClientId());
        replenishH.setGsrhVoucherId(inData.getGsrhVoucherId());
        replenishH.setGsrhBrId(inData.getStoCode());
        replenishH.setGsrhStatus("1");
        replenishH.setGsrhFlag("1");
        replenishH.setGsrhPattern(inData.getGsrhPattern());
        replenishH.setGsrhType(inData.getGsrhType());
        replenishH.setGsrhDate(inData.getGsrhDate());
        replenishH.setGsrhEmp(inData.getGsrhEmp());
        saveDetails(inData, replenishH);
        this.replenishHMapper.insert(replenishH);
    }

    @Transactional
    public void saveDetails(GetReplenishInData inData, GaiaSdReplenishH replenishH) {
        if (ObjectUtil.isNotEmpty(replenishH)) {
//            replenishH.setGsrhStatus("0");
            replenishH.setGsrhPattern(inData.getGsrhPattern());
            replenishH.setGsrhType(inData.getGsrhType());
            replenishH.setGsrhDate(inData.getGsrhDate());
            BigDecimal amount = BigDecimal.ZERO;
            BigDecimal count = BigDecimal.ZERO;
            GetReplenishOutData outData = new GetReplenishOutData();
            outData.setGsrhType(replenishH.getGsrhType());
            outData.setGsrhDate(replenishH.getGsrhDate());
            outData.setGsrhStatus(replenishH.getGsrhStatus());
            outData.setGsrhTotalAmt(replenishH.getGsrhTotalAmt());
            outData.setGsrhTotalQty(replenishH.getGsrhTotalQty());
            outData.setGsrhEmp(replenishH.getGsrhEmp());
            List<GetReplenishDetailOutData> detailList = this.replenishDMapper.queryDetailList(inData);
            if (CollUtil.isNotEmpty(detailList)) {
                for (GetReplenishDetailOutData detail : detailList) {
                    Example exampleStore = new Example(GaiaSdReplenishD.class);
                    exampleStore.createCriteria().andEqualTo("clientId", inData.getClientId())
                            .andEqualTo("gsrdVoucherId", inData.getGsrhVoucherId())
                            .andEqualTo("gsrdBrId", inData.getStoCode())
                            .andEqualTo("gsrdProId",detail.getGsrdProId());
                    this.replenishDMapper.deleteByExample(exampleStore);
                }
            }
            int index = 1;
            for (GetReplenishDetailInData detail : inData.getDetailList()) {
                if (StrUtil.isEmpty(detail.getGsrdNeedQty())) {
                    count = count.add(new BigDecimal("0"));
                } else {
                    count = count.add(new BigDecimal(detail.getGsrdNeedQty()));
                }
                GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
                replenishD.setClientId(inData.getClientId());
                replenishD.setGsrdVoucherId(inData.getGsrhVoucherId());
                replenishD.setGsrdBrId(inData.getStoCode());
                replenishD.setGsrdDate(inData.getGsrhDate().trim());
                replenishD.setGsrdSerial(index+"");
                replenishD.setGsrdProId(detail.getGsrdProId());
                replenishD.setGsrdAverageCost(detail.getGsrdAverageCost());
                replenishD.setGsrdDisplayMin(detail.getGsrdDisplayMin());
                replenishD.setGsrdGrossMargin(detail.getGsrdGrossMargin());
                replenishD.setGsrdNeedQty(detail.getGsrdNeedQty().replaceFirst("^0*", "").length() == 0 ? "0" : detail.getGsrdNeedQty().replaceFirst("^0*", ""));
                replenishD.setGsrdPackMidsize(detail.getGsrdPackMidsize());
                replenishD.setGsrdProposeQty(detail.getGsrdProposeQty());
                replenishD.setGsrdSalesDays1(detail.getGsrdSalesDays1());
                replenishD.setGsrdSalesDays2(detail.getGsrdSalesDays2());
                replenishD.setGsrdSalesDays3(detail.getGsrdSalesDays3());
                replenishD.setGsrdStockDepot(detail.getGsrdStockDepot());
                replenishD.setGsrdStockStore(detail.getGsrdStockStore());
                replenishD.setGsrdLastSupid(detail.getGsrdLastSupid());
                replenishD.setGsrdVoucherAmt(new BigDecimal(StrUtil.isEmpty(detail.getVoucherAmt()) ? "0" : detail.getVoucherAmt()));
                replenishD.setGsrdTaxRate(detail.getGsrdProRate());
                this.replenishDMapper.insert(replenishD);

                if (ObjectUtil.isNotEmpty(detail.getGsrdProPrice())) {
                    if (StrUtil.isEmpty(detail.getGsrdNeedQty())) {
                        amount = amount.add(detail.getGsrdProPrice().multiply(new BigDecimal("0")));
                    } else {
                        amount = amount.add(detail.getGsrdProPrice().multiply(new BigDecimal(detail.getGsrdNeedQty())));
                    }
                }
                index ++;
            }
            replenishH.setGsrhTotalAmt(amount);
            replenishH.setGsrhTotalQty(count.toString());
        }
    }

    @Transactional
    public String insertPo(GetReplenishInData inData, GetLoginOutData userInfo) {
        GaiaPoHeader poHeader = new GaiaPoHeader();
        poHeader.setClient(inData.getClientId());
        poHeader.setPoCompanyCode(inData.getDcCode());
        poHeader.setPoDeliveryTypeStore(inData.getStoCode());
        if (ObjectUtil.isNotNull(inData.getDetailList().get(0).getGsrdLastSupid())){
            poHeader.setPoSupplierId(inData.getDetailList().get(0).getGsrdLastSupid());
        }else {
            poHeader.setPoSupplierId("");
        }
        String voucherId = poHeaderMapper.selectNextVoucherId(inData.getClientId());
        poHeader.setPoId(voucherId);
        poHeader.setPoType("Z001");
        poHeader.setPoSubjectType("1");
        poHeader.setPoDate(CommonUtil.getyyyyMMdd());
        poHeader.setPoHeadRemark("");
        if (ObjectUtil.isNotEmpty(inData.getDetailList().get(0).getPayTerm())) {
            poHeader.setPoPaymentId(inData.getDetailList().get(0).getPayTerm());
        }else{
            poHeader.setPoPaymentId("");
        }
        poHeader.setPoCreateBy(userInfo.getUserId());
        poHeader.setPoCreateDate(CommonUtil.getyyyyMMdd());
        poHeader.setPoCreateTime(CommonUtil.getHHmmss());
        if (ObjectUtil.isNotEmpty(inData.getGsrhVoucherId())) {
            poHeader.setPoReqId(inData.getGsrhVoucherId());
        }
        poHeader.setPoApproveStatus("0");
        this.poHeaderMapper.insert(poHeader);
        int i = 1;
        for (GetReplenishDetailInData detail : inData.getDetailList()) {
//            BatchInfoInData batchInfoInData = new BatchInfoInData();
//            batchInfoInData.setClient(inData.getClientId());
//            batchInfoInData.setProSite(userInfo.getDepId());
//            batchInfoInData.setBatProCode(detail.getGsrdProId());
//            batchInfoInData.setBatPoId(voucherId);
//            batchInfoInData.setBatPoLineno(StrUtil.toString(i));
//            batchInfoInData.setBatProductDate(detail.getProMadeDate());
//            batchInfoInData.setBatExpiryDate(detail.get);
//            batchInfoInData.setBatSupplierCode(detail.get);
//            batchInfoInData.setBatPoPrice(detail.getGsrdProPrice().toString());
//            batchInfoInData.setBatReceiveQty(detail.getGsrdNeedQty());
//            batchInfoInData.setBatBatchNo(detail.getProBatchNo());
//            batchInfoInData.setBatCreateDate(CommonUtil.getyyyyMMdd());
//            batchInfoInData.setBatCreateTime(CommonUtil.getHHmmss());
//            batchInfoInData.setBatCreateUser(userInfo.getUserId());
//            batchInfoInData.setBatSourceNo(voucherId);
//            log.info("saveBatchInfo传入参数:{}", JSON.toJSONString(batchInfoInData));
//            String wmsResult = wmsService.saveBatchInfo(batchInfoInData);
//            log.info("saveBatchInfo返回:{}", JSON.toJSONString(wmsResult));
//            BaseResponse batchInfoRes =  JSONObject.parseObject(wmsResult, BaseResponse.class);
//            if(batchInfoRes.getCode() != HttpStatus.SC_OK){
//                throw new BusinessException("调用生成批次接口失败");
//            }

            GaiaPoItem poItem = new GaiaPoItem();
            poItem.setClient(inData.getClientId());
            poItem.setPoId(voucherId);
            poItem.setPoLineNo(StrUtil.toString(i));
            detail.setGsrdSerial(StrUtil.toString(i));
            poItem.setPoProCode(detail.getGsrdProId());
            poItem.setPoQty(new BigDecimal(detail.getGsrdNeedQty()));
            poItem.setPoUnit(detail.getGsrdProUnit());
            poItem.setPoPrice(new BigDecimal(detail.getPoPrice()));
            poItem.setPoSiteCode(inData.getDcCode());
            poItem.setPoLocationCode("1000");
            poItem.setPoLineAmt(new BigDecimal(detail.getGsrdNeedQty()).multiply(poItem.getPoPrice()));
            //批号新增
//            poItem.setPoBatch(batchInfoRes.getData().getBatBatch());
            if (ObjectUtil.isNotEmpty(detail.getGsrdProRateCode())) {
                poItem.setPoRate(detail.getGsrdProRateCode());
            }else {
                poItem.setPoRate("X0");
            }
            poItem.setPoDeliveryDate(DateUtil.format(new Date(), "yyyyMMdd"));
            poItem.setPoLineRemark("");
            poItem.setPoCompleteFlag("0");
            poItem.setPoLineDelete("0");
            poItem.setPoCompleteInvoice("0");
            this.poItemMapper.insert(poItem);
            i++;
        }
        return voucherId;
    }

    public String selectNextVoucherId(GaiaSdReplenishH inData) {
        String voucherId = null;
        inData.setGsrhBrId(null);
        if (ObjectUtil.isNotEmpty(inData.getClientId())) {
            voucherId = this.replenishHMapper.selectNextVoucherId(inData);
        }
        return voucherId;
    }

    public List<GetReplenishDetailOutData> addSupInfo(List<GetReplenishDetailOutData> detail,GetReplenishInData inData){
        List<SupInfo> lastSupList = replenishDMapper.nearDateSupList(inData);
        List<SupInfo> minPriceSupList = replenishDMapper.minPriceSupList(inData);
        for (GetReplenishDetailOutData a : detail) {
            //String gsrdBrId = a.getGsrdBrId();
            String proCode = a.getGsrdProId();
            for (SupInfo info : lastSupList){
                String proCode1 = info.getProCode();
                if (proCode.equals(proCode1)){
                    String supName = info.getSupName();
                    String supCode = info.getSupCode();
                    String poPrice = info.getPoPrice();
                    a.setGsrdLastPrice(poPrice);
                    a.setGsrdLastSupid(supCode);
                    a.setGsrdLastSupName(supName);
                    a.setGsrdSupid(supCode);
                    a.setGsrdSupName(supName);
                    a.setPoPrice(poPrice);
                }
            }
            for (SupInfo info : minPriceSupList){
                String proCode1 = info.getProCode();
                if (proCode.equals(proCode1)){
                    String supName = info.getSupName();
                    String supCode = info.getSupCode();
                    String poPrice = info.getPoPrice();
                    a.setGsrdMinPrice(poPrice);
                    a.setGsrdMinSupid(supCode);
                    a.setGsrdMinSupName(supName);
                }
            }
        }
        return detail;
    }
}
