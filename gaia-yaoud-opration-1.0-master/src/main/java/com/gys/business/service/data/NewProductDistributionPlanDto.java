package com.gys.business.service.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 10:40 2021/8/4
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class NewProductDistributionPlanDto {
    /**
     * 省
     */
    private List<String> prov;

    /**
     * 市
     */
    private List<String> city;

    /**
     * 客户
     */
    private String[] client;

    /**
     * 商品编码
     */
    private String[] productCode;

    /**
     * 是否补铺 0:否  1:是
     */
    private Integer[] isSupplementDistribution;

    /**
     * 实际铺货模式 1:单次提取 2:多次提取
     */
    private Integer[] distributionType;

    /**
     * 实际铺货方式 1:门店属性 2:门店类型 3:门店
     */
    private Integer[] planType;

    /**
     * 实际调取方式 1:自动 2:手动
     */
    private Integer[] useType;

    /**
     * 实际有效期天数 7 15 30 60 90
     */
    private Integer[] planDays;

    /**
     * 计划开始日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date startDate;

    /**
     * 计划结束日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date endDate;

    /**
     * 入库开始日期yyyyMMdd
     */
    private String startEntryDate;

    /**
     * 入库结束日期yyyyMMdd
     */
    private String endEntryDate;

    /**
     * 是否勾选加盟商 0代表否  1代表是
     */
    private  Integer isIncludeClient;
}
