package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 销售目录维护表(GaiaSalesCatalog)实体类
 *
 * @author makejava
 * @since 2021-08-25 10:51:47
 */
@Data
public class GaiaSalesCatalogInData implements Serializable {
    private static final long serialVersionUID = 643579019570930620L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店
     */
    private String storeCode;
    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 商品通用名
     */
    private String proCommonname;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 厂家
     */
    private String proFactoryName;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 状态 1:正常隐藏 2:彻底隐藏
     */
    private Integer status;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 创建日期
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String updateUser;
    /**
     * 更新日期
     */
    private Date updateTime;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 功能开关 0-关 1-开
     */
    private String gcspPara1;
    /**
     * 参数字典
     */
    private String gcspId;

    private String type;

    private List<Long> idList;



}
