package com.gys.business.service.data.zjnb;


import lombok.Data;

/**
 * @author wuai
 * @description:
 * @date 2021/10/24 21:11
 */
@Data
public class FeesSettleConfirmVo {
   /* 卡数据格式2	——	详见“卡数据格式2”
    个人基本信息	——	详见“个人基本信息”
    医疗费用支付结构	——	详见“医疗费用支付结构”
    医疗费用累计结构	——	详见“医疗费用累计结构”
    就诊编号	20
    明细账单号	20
    结算交易流水号	20*/
    /**
     * 卡数据格式2	——	详见“卡数据格式2”
     */
    private CardData1Dto cardData1Dto;
    /**
     * 个人基本信息	——	详见“个人基本信息”（工伤相关交易此结构为空）
     */
    private PersonalInfoVo personalInfoVo;
    /**
     * 原医疗费用支付结构	——	详见“医疗费用支付结构”
     */
    private MedicalChargePayDto medicalChargePayDto;
    /**
     * 医疗费用累计结构	——	详见“医疗费用累计结构”（工伤相关交易此结构为空）
     */
    private MedicalChargeTotalVo medicalChargeTotalVo;
    /**
     * 就诊编号	20
     */
    private String  clinicNum;
    /**
     * 明细账单号	20
     */
    private String detailBillCode;
    /**
     * 明细处理流水号	20
     */
    private String  detailSerialCode;

}
