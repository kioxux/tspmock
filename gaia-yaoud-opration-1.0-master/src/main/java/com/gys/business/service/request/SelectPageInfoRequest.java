package com.gys.business.service.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectPageInfoRequest {

    @ApiModelProperty(value = "当前页码")
    private Integer pageNum;
    @ApiModelProperty(value = "当前页码数量")
    private Integer pageSize;
    @ApiModelProperty(value = "疾病分类编码或名称")
    private String sicknessCodingName;

    @ApiModelProperty(value = "成分名称")
    private String smallSicknessCodingName;

    @ApiModelProperty(value = "维护开始日期")
    private String startTime;

    @ApiModelProperty(value = "维护结束日期")
    private String endTime;

    @ApiModelProperty(value = "维护人")
    private String maintenanceUser;

    @ApiModelProperty(value = "状态")
    private String status;
}
