package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:20 2021/8/13
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class EditWtpsDetailDto {
    /**
     * 操作类型 U：修改 D：删除
     */
    @NotBlank(message = "操作类型不能为空！")
    private String type;

    /**
     *加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 配送单号
     */
    @NotBlank(message = "配送单号不能为空！")
    private String gswdVoucherId;

    /**
     * 第三方商品编码
     */
    @NotBlank(message = "第三方商品编码不能为空！")
    private String gswdProDsfId;

    /**
     * 数量
     */
    private BigDecimal gswdQty;

    /**
     * 单价
     */
    private BigDecimal gswdPrc;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 修改日期
     */
    private String updateDate;

    /**
     * 修改时间
     */
    private String updateTime;
}
