package com.gys.business.service;

import com.gys.business.service.data.AppStoreSaleInData;
import com.gys.business.service.data.ProductBigTypeSaleInData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.app.salereport.ProPositionDTO;

public interface AppSaleReportService {

    JsonResult getChartInfo(ProductBigTypeSaleInData inData);

    JsonResult getChartInfoByBigCode(ProductBigTypeSaleInData inData);

    JsonResult getBigTypeSaleList(ProductBigTypeSaleInData inData);

    JsonResult getMidTypeSaleList(ProductBigTypeSaleInData inData);

    JsonResult getStoreList(AppStoreSaleInData inData);

    void initSaleData();

    JsonResult getGroupOrScManagerList(String client, String userId);

    /**
     * 商品定位统计分类
     *
     * @return JsonResult
     */
    JsonResult selectProPositionSummaryCategory();

    /**
     * 商品定位统计
     *
     * @param proPositionDTO proPositionDTO
     * @return JsonResult
     */
    JsonResult selectProPositionSummary(ProPositionDTO proPositionDTO);

}
