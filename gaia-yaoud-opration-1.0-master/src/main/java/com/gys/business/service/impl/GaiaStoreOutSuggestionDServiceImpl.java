package com.gys.business.service.impl;

import com.gys.business.mapper.entity.GaiaStoreOutSuggestionD;
import com.gys.business.mapper.GaiaStoreOutSuggestionDMapper;
import com.gys.business.service.GaiaStoreOutSuggestionDService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionD)表服务实现类
 *
 * @author makejava
 * @since 2021-10-28 10:53:43
 */
@Service("gaiaStoreOutSuggestionDService")
public class GaiaStoreOutSuggestionDServiceImpl implements GaiaStoreOutSuggestionDService {
    @Resource
    private GaiaStoreOutSuggestionDMapper gaiaStoreOutSuggestionDDao;

}
