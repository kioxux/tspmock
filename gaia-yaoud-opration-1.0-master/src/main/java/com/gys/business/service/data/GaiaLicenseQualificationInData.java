package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class GaiaLicenseQualificationInData {
    private String client;

    @NotBlank(message = "地点编码必填")
    private String siteCode;
    @NotBlank(message = "查询类型必填")
    private String queryType;

    private List<String> supCodeList;

    private List<String> cusCodeList;

    private String proCode;
    private List<String> proCodeList;

    private String stoCode;

    private List<String> stoCodeList;

    private String days;
}
