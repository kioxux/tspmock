package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ProductBigTypeSaleOutData implements Serializable {

    @ApiModelProperty(value = "大类编码")
    private String bigCode;

    @ApiModelProperty(value = "大类名称")
    private String bigName;

    @ApiModelProperty(value = "动销品项")
    private Integer productCount;

    @ApiModelProperty(value = "销售额")
    private BigDecimal saleAmt;

    @ApiModelProperty(value = "销售占比")
    private BigDecimal saleAmtProp;

    @ApiModelProperty(value = "毛利额")
    private BigDecimal profitAmt;

    @ApiModelProperty(value = "毛利率")
    private BigDecimal profitRate;

}
