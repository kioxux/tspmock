package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.RemoteReviewService;
import com.gys.business.service.YunNanZYYSSuperviseService;
import com.gys.business.service.data.StoreListOutData;
import com.gys.business.service.data.YunNanPrescriptionDrugInData;
import com.gys.business.service.data.YunNanPrescriptionInData;
import com.gys.business.service.yaomeng.YaomengService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.data.remoteReview.GaiaSdRemoteAuditD;
import com.gys.common.data.remoteReview.GaiaSdRemoteAuditH;
import com.gys.common.data.remoteReview.dto.QueryReviewDetailsOutDataDTO;
import com.gys.common.data.remoteReview.dto.QueryReviewOutDataDTO;
import com.gys.common.data.remoteReview.dto.QueryReviewParamDTO;
import com.gys.common.data.remoteReview.dto.ReviewCheckStatus;
import com.gys.common.enums.ReviewCheckStatusEnum;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import com.gys.util.YunnanSuperviseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static com.gys.common.enums.ReviewCheckStatusEnum.STATUS_REJECTED;
import static com.gys.common.enums.ReviewCheckStatusEnum.STATUS_REVIEWED;
import static com.gys.util.yaomeng.YaomengApiUtil.readInputStream;

@Service
@Slf4j
public class RemoteReviewServiceImpl implements RemoteReviewService {

    @Autowired
    RemoteReviewMapper remoteReviewMapper;

    @Autowired
    GaiaSdSaleRecipelMapper gaiaSdSaleRecipelMapper;

    @Autowired
    private YunNanZYYSSuperviseService yunNanZYYSSuperviseService;

    @Autowired
    private GaiaUserDataMapper gaiaUserDataMapper;

    @Autowired
    private YaomengService yaomengService;

    @Autowired
    private GaiaSdSaleHMapper gaiaSdSaleHMapper;


    @Override
    public PageInfo<QueryReviewOutDataDTO> queryReviewList(GetLoginOutData userInfo, QueryReviewParamDTO queryReviewParamDTO, Integer pageNum, Integer pageSize) {
        ArrayList<QueryReviewOutDataDTO> queryReviewOutDataDTOS = new ArrayList<>();
        // 分页获取处方列表
        PageHelper.startPage(pageNum, pageSize);
        ArrayList<GaiaSdSaleRecipel> gaiaSdSaleRecipels = gaiaSdSaleRecipelMapper.queryReviewList(userInfo.getClient(), queryReviewParamDTO);
        PageInfo pageInfo = new PageInfo(gaiaSdSaleRecipels);
        // 获取处方对应明细
        if (gaiaSdSaleRecipels.size() == 0) {
            return pageInfo;
        }
        for (GaiaSdSaleRecipel gaiaSdSaleRecipel : gaiaSdSaleRecipels) {
            QueryReviewOutDataDTO queryReviewOutDataDTO = new QueryReviewOutDataDTO();
            BeanUtils.copyProperties(gaiaSdSaleRecipel, queryReviewOutDataDTO);
            switch (queryReviewOutDataDTO.getGssrCheckStatus()) {
                case "1":
                    queryReviewOutDataDTO.setCheckResult("合格");
                    break;
                case "2":
                    queryReviewOutDataDTO.setCheckResult("不合格");
                    break;
                default:
                    queryReviewOutDataDTO.setCheckResult("");
                    queryReviewOutDataDTO.setGssrCheckStatus("0");
            }
            // 添加明细
            ArrayList<QueryReviewDetailsOutDataDTO> details = remoteReviewMapper.queryReviewDetailList(gaiaSdSaleRecipel.getClientId(), gaiaSdSaleRecipel.getGssrSaleBillNo());
            queryReviewOutDataDTO.setDetailsList(details);
            queryReviewOutDataDTOS.add(queryReviewOutDataDTO);
        }
        pageInfo.setList(queryReviewOutDataDTOS);
        return pageInfo;
    }


    @Override
    public ArrayList<ReviewCheckStatus> queryReviewCheckStatus() {
        ArrayList<ReviewCheckStatus> reviewCheckStatuses = new ArrayList<>();
        for (ReviewCheckStatusEnum reviewCheckStatus : ReviewCheckStatusEnum.values()) {
            ReviewCheckStatus reviewCheckStatus1 = new ReviewCheckStatus();
            reviewCheckStatus1.setCode(reviewCheckStatus.getCode());
            reviewCheckStatus1.setDesc(reviewCheckStatus.getDesc());
            reviewCheckStatuses.add(reviewCheckStatus1);
        }
        return reviewCheckStatuses;
    }

    @Override
    @Transactional
    public void approveReview(GetLoginOutData userInfo, List<String> gssrSaleBillNoList) {
        if (gssrSaleBillNoList.size()==0){
            throw new BusinessException("审核失败,未选中任何处方");
        }
        for (String gssrSaleBillNo : gssrSaleBillNoList) {
            ArrayList<GaiaSdSaleRecipel> gaiaSdSaleRecipels = gaiaSdSaleRecipelMapper.queryReviewBySaleBillNo(userInfo.getClient(), gssrSaleBillNo);
            if (gaiaSdSaleRecipels.size() == 0){
                throw new BusinessException("审核失败,选中处方不存在");
            }
            GaiaSdSaleRecipel gaiaSdSaleRecipel = gaiaSdSaleRecipels.get(0);        // 处方登记记录表
            // 判断该处方是否是已经审核过的状态
            if (org.apache.commons.lang.StringUtils.equals(gaiaSdSaleRecipel.getGssrCheckStatus(),STATUS_REJECTED.getCode()) | org.apache.commons.lang.StringUtils.equals(gaiaSdSaleRecipel.getGssrCheckStatus(),STATUS_REVIEWED.getCode())){
                throw new BusinessException("审核失败,存在已被审核处方");
            }
            String newUrl = "";
            //过滤出药盟的处方图片
            List<GaiaSdSaleRecipel> ymList = gaiaSdSaleRecipels.stream().filter(item -> "1".equals(item.getGssrYmflag()) && !ObjectUtils.isEmpty(item.getGssrRecipelPicAddress())).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(ymList)) {
                List<GaiaUserData> userDataList = gaiaUserDataMapper.getAllByClientById(userInfo.getClient(), userInfo.getUserId());
                GaiaUserData signUrl = userDataList.get(0);
                if (ObjectUtils.isEmpty(signUrl.getUserSignAdds())) {
                    throw new BusinessException("该药师没有维护签名照，请先维护签名照才能审核！");
                }
                JsonResult jsonResult = yaomengService.getPrescriptionFileUrl(gaiaSdSaleRecipel.getGssrRecipelId(), signUrl.getUserSignAdds());
                if (jsonResult.getCode()!=0) {
                    throw new BusinessException("药盟审核处方失败！");
                }
                newUrl = yaomengService.downLoadAndUploadFile(jsonResult.getData().toString());

            }
            Date date = new Date();
            String formatDate = DateUtil.format(date, "yyyyMMdd");
            String formatTime = DateUtil.format(date, "HH:mm:ss");

            GaiaSdSaleH gaiaSdSaleH = gaiaSdSaleHMapper.selectByBillNo(gssrSaleBillNo, userInfo.getClient(), userInfo.getDepId());
            //如果 远程审方在 审核之前已经 有销售单了 上传日期 ，审核日期和 上传时间，审核时间 取sale_h表的日期和时间减1分钟
            if (!ObjectUtils.isEmpty(gaiaSdSaleH)) {
                String gsshDate = gaiaSdSaleH.getGsshDate();
                Date dateFormat = gaiaSdSaleH.getDateFormat();
                dateFormat.setTime(dateFormat.getTime() - 1*60*1000);
                String gsshTime = DateUtil.format(dateFormat, "HH:mm:ss");
                // 修改处方登记记录表数据
                if (!updateReviewStatus(userInfo.getClient(),gssrSaleBillNo,STATUS_REVIEWED.getCode(),userInfo.getUserId(),userInfo.getLoginName(),gsshDate,gsshTime,newUrl,gsshTime,gsshDate)){
                    throw new BusinessException("审核失败");
                }
            }else {
                // 修改处方登记记录表数据
                if (!updateReviewStatus(userInfo.getClient(),gssrSaleBillNo,STATUS_REVIEWED.getCode(),userInfo.getUserId(),userInfo.getLoginName(),formatDate,formatTime,newUrl, null, null)){
                    throw new BusinessException("审核失败");
                }
            }
            // 添加远程审方主表(GAIA_SD_REMOTE_AUDIT_H)数据
            String checkResult = "合格";
            GaiaSdRemoteAuditH gaiaSdRemoteAuditH = new GaiaSdRemoteAuditH(){{
                setClient(gaiaSdSaleRecipel.getClientId());
                setGsrahVoucherId(gaiaSdSaleRecipel.getGssrVoucherId());
                setGsrahBrId(gaiaSdSaleRecipel.getGssrBrId());
                setGsrahBrName(gaiaSdSaleRecipel.getGssrBrName());
                setGsrahBillNo(gaiaSdSaleRecipel.getGssrSaleBillNo());
                setGsrahUploadDate(gaiaSdSaleRecipel.getGssrDate());
                setGsrahUploadTime(gaiaSdSaleRecipel.getGssrTime());
                setGsrahUploadEmp(gaiaSdSaleRecipel.getGssrEmp());
                setGsrahUploadFlag(gaiaSdSaleRecipel.getGssrUploadFlag());
                setGsrahDate(formatDate);
                setGsrahTime(formatTime);
                setGsrahResult(checkResult);
                setGsrahPharmacistId(gaiaSdSaleRecipel.getGssrPharmacistId());
                setGsrahPharmacistName(gaiaSdSaleRecipel.getGssrPharmacistName());
                setGsrahCustName(gaiaSdSaleRecipel.getGssrCustName());
                setGsrahCustSex(gaiaSdSaleRecipel.getGssrCustSex());
                setGsrahCustAge(gaiaSdSaleRecipel.getGssrCustAge());
                setGsrahCustIdcard(gaiaSdSaleRecipel.getGssrCustIdcard());
                setGsrahCustMobile(gaiaSdSaleRecipel.getGssrCustMobile());
                setGsrahStatus(gaiaSdSaleRecipel.getGssrCheckStatus());
                setGsrahRecipelPicAddress(gaiaSdSaleRecipel.getGssrRecipelPicAddress());
                gaiaSdSaleRecipel.setGssrUploadFlag(" ");
                if (!StringUtils.isEmpty(gaiaSdSaleRecipel.getGssrUploadFlag())){
                    setGsrahUploadFlag(gaiaSdSaleRecipel.getGssrUploadFlag()); // 上传状态可能为空,但是数据库为必传字段
                }
            }};
            remoteReviewMapper.insertH(gaiaSdRemoteAuditH);
            // 添加远程审方明细(GAIA_SD_REMOTE_AUDIT_D)数据
            Integer i = 1;
            for (GaiaSdSaleRecipel sdSaleRecipel : gaiaSdSaleRecipels) {
                Integer j = i;
                GaiaSdRemoteAuditD gaiaSdRemoteAuditD = new GaiaSdRemoteAuditD(){{
                    setClient(sdSaleRecipel.getClientId());
                    setGsradVoucherId(sdSaleRecipel.getGssrVoucherId());
                    setGsradBrId(sdSaleRecipel.getGssrBrId());
                    setGsradSerial(j.toString()); // 行号从一开始往后推
                    setGsradProId(sdSaleRecipel.getGssrProId());
                    setGsradBatchNo(" ");
                    if (!StringUtils.isEmpty(sdSaleRecipel.getGssrBatchNo())){
                        setGsradBatchNo(sdSaleRecipel.getGssrBatchNo());; // 批号不可能为空,但是之前有脏数据
                    }
                    setGsradQty(new BigDecimal(sdSaleRecipel.getGssrQty()));
                }};
                remoteReviewMapper.insertD(gaiaSdRemoteAuditD);
                i++;
            }
            // 审核结束后，修改挂单表状态GAIA_SD_SALE_HAND_H（2：未审核，1：已审核，3：已驳回）
            remoteReviewMapper.updateHandStatus("1",gaiaSdSaleRecipel.getClientId(),gaiaSdSaleRecipel.getGssrSaleBillNo(),gaiaSdSaleRecipel.getGssrBrId(),gaiaSdSaleRecipel.getGssrSaleDate());
        }
    }

    //药盟下载处方图片
    public static Map<String, Object> downLoadFile(String httpUrl) {
        log.info("药盟开始下载处方图片, url:" + httpUrl);
        Map<String, Object> map = new HashMap<>(2);
        URL url = null;
        InputStream inputStream = null;
        FileOutputStream fos = null;
        int random = ThreadLocalRandom.current().nextInt(10000, 99999);
        String fileTempName = "yaomeng" + random + ".jpg";
        map.put("fileName", fileTempName);
        try {
            url = new URL(httpUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //设置超时间为3秒
            conn.setConnectTimeout(3 * 1000);
            //防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

            //得到输入流
            inputStream = conn.getInputStream();
            //获取自己数组
            byte[] getData = readInputStream(inputStream);

            //文件保存位置
            File saveDir = new File(fileTempName);
            if (!saveDir.exists()) {
                saveDir.createNewFile();
            }
            fos = new FileOutputStream(saveDir);
            fos.write(getData);
            map.put("file", saveDir);
        } catch (IOException e) {
            log.error("药盟下载处方图片失败, url:" + httpUrl + "，error:" + e);
            return null;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        log.info("药盟结束下载处方图片, url:" + httpUrl + "，保存的图片名:" + fileTempName);
        return map;
    }

    @Override
    @Transactional
    public void rejectReview(GetLoginOutData userInfo, List<String> gssrSaleBillNoList) {
        if (gssrSaleBillNoList.size()==0){
            throw new BusinessException("审核失败,未选中任何处方");
        }
        for (String gssrSaleBillNo : gssrSaleBillNoList) {
            ArrayList<GaiaSdSaleRecipel> gaiaSdSaleRecipels = gaiaSdSaleRecipelMapper.queryReviewBySaleBillNo(userInfo.getClient(), gssrSaleBillNo);
            if (gaiaSdSaleRecipels.size() == 0){
                throw new BusinessException("驳回失败,不存在该处方");
            }
            GaiaSdSaleRecipel gaiaSdSaleRecipel = gaiaSdSaleRecipels.get(0);        // 处方登记记录表
            // 判断该处方是否是已经审核过的状态
            if (org.apache.commons.lang.StringUtils.equals(gaiaSdSaleRecipel.getGssrCheckStatus(),STATUS_REJECTED.getCode()) | org.apache.commons.lang.StringUtils.equals(gaiaSdSaleRecipel.getGssrCheckStatus(),STATUS_REVIEWED.getCode())){
                throw new BusinessException("审核失败,该处方已被审核");
            }
            Date date = new Date();
            String formatDate = DateUtil.format(date, "yyyyMMdd");
            String formatTime = DateUtil.format(date, "HH:mm:ss");
            // 修改处方登记记录表状态
            GaiaSdSaleH gaiaSdSaleH = gaiaSdSaleHMapper.selectByBillNo(gssrSaleBillNo, userInfo.getClient(), userInfo.getDepId());
            //如果 远程审方在 审核之前已经 有销售单了 上传日期 ，审核日期和 上传时间，审核时间 取sale_h表的日期和时间减1分钟
            if (!ObjectUtils.isEmpty(gaiaSdSaleH)) {
                String gsshDate = gaiaSdSaleH.getGsshDate();
                Date dateFormat = gaiaSdSaleH.getDateFormat();
                dateFormat.setTime(dateFormat.getTime() - 1*60*1000);
                String gsshTime = DateUtil.format(dateFormat, "HH:mm:ss");
                // 修改处方登记记录表数据
                if (!updateReviewStatus(userInfo.getClient(),gssrSaleBillNo,STATUS_REJECTED.getCode(),userInfo.getUserId(),userInfo.getLoginName(),formatDate,formatTime, "", gsshTime, gsshDate)){
                    throw new BusinessException("驳回失败");
                }
            }else {
                // 修改处方登记记录表数据
                if (!updateReviewStatus(userInfo.getClient(),gssrSaleBillNo,STATUS_REJECTED.getCode(),userInfo.getUserId(),userInfo.getLoginName(),formatDate,formatTime, "", null, null)){
                    throw new BusinessException("驳回失败");
                }
            }
            // 添加远程审方主表(GAIA_SD_REMOTE_AUDIT_H)数据
            String checkResult = "不合格";
            GaiaSdRemoteAuditH gaiaSdRemoteAuditH = new GaiaSdRemoteAuditH(){{
                setClient(gaiaSdSaleRecipel.getClientId());
                setGsrahVoucherId(gaiaSdSaleRecipel.getGssrVoucherId());
                setGsrahBrId(gaiaSdSaleRecipel.getGssrBrId());
                setGsrahBrName(gaiaSdSaleRecipel.getGssrBrName());
                setGsrahBillNo(gaiaSdSaleRecipel.getGssrSaleBillNo());
                setGsrahUploadDate(gaiaSdSaleRecipel.getGssrDate());
                setGsrahUploadTime(gaiaSdSaleRecipel.getGssrTime());
                setGsrahUploadEmp(gaiaSdSaleRecipel.getGssrEmp());
                gaiaSdSaleRecipel.setGssrUploadFlag(" ");
                if (!StringUtils.isEmpty(gaiaSdSaleRecipel.getGssrUploadFlag())){
                    setGsrahUploadFlag(gaiaSdSaleRecipel.getGssrUploadFlag()); // 上传状态可能为空,但是数据库为必传字段
                }
                setGsrahDate(formatDate);
                setGsrahTime(formatTime);
                setGsrahResult(checkResult);
                setGsrahPharmacistId(gaiaSdSaleRecipel.getGssrPharmacistId());
                setGsrahPharmacistName(gaiaSdSaleRecipel.getGssrPharmacistName());
                setGsrahCustName(gaiaSdSaleRecipel.getGssrCustName());
                setGsrahCustSex(gaiaSdSaleRecipel.getGssrCustSex());
                setGsrahCustAge(gaiaSdSaleRecipel.getGssrCustAge());
                setGsrahCustIdcard(gaiaSdSaleRecipel.getGssrCustIdcard());
                setGsrahCustMobile(gaiaSdSaleRecipel.getGssrCustMobile());
                setGsrahStatus(gaiaSdSaleRecipel.getGssrCheckStatus());
                setGsrahRecipelPicAddress(gaiaSdSaleRecipel.getGssrRecipelPicAddress());
            }};
            remoteReviewMapper.insertH(gaiaSdRemoteAuditH);
            // 添加远程审方明细(GAIA_SD_REMOTE_AUDIT_D)数据
            Integer i = 1;
            for (GaiaSdSaleRecipel sdSaleRecipel : gaiaSdSaleRecipels) {
                Integer j = i;
                GaiaSdRemoteAuditD gaiaSdRemoteAuditD = new GaiaSdRemoteAuditD(){{
                    setClient(sdSaleRecipel.getClientId());
                    setGsradVoucherId(sdSaleRecipel.getGssrVoucherId());
                    setGsradBrId(sdSaleRecipel.getGssrBrId());
                    setGsradSerial(j.toString()); // 行号从一开始往后推
                    setGsradProId(sdSaleRecipel.getGssrProId());
                    setGsradBatchNo(" ");
                    if (!StringUtils.isEmpty(sdSaleRecipel.getGssrBatchNo())){
                        setGsradBatchNo(sdSaleRecipel.getGssrBatchNo());; // 批号不可能为空,但是之前有脏数据
                    }
                    setGsradQty(new BigDecimal(sdSaleRecipel.getGssrQty()));
                }};
                remoteReviewMapper.insertD(gaiaSdRemoteAuditD);
                i++;
            }
            // 审核结束后，修改挂单表状态GAIA_SD_SALE_HAND_H（2：未审核，1：已审核，3：已驳回）
            remoteReviewMapper.updateHandStatus("3",gaiaSdSaleRecipel.getClientId(),gaiaSdSaleRecipel.getGssrSaleBillNo(),gaiaSdSaleRecipel.getGssrBrId(),gaiaSdSaleRecipel.getGssrSaleDate());
        }
    }

    @Override
    public Integer queryAuthenticationType(GetLoginOutData userInfo) {
        Integer type = remoteReviewMapper.queryAuthenticationType(userInfo.getClient());
        if (ObjectUtils.isEmpty(type)){
            throw new BusinessException("机构尚未配置审核方式");
        }
        return type;
    }

    @Override
    public void checkRegisterByPassword(GetLoginOutData userInfo, String password) {
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        Integer integer = remoteReviewMapper.checkRegisterByPassword(userInfo.getClient(), userInfo.getUserId(), password);
        if (integer == 0){
            throw new BusinessException("用户名或密码错误");
        }
    }

    @Override
    public String checkRegisterByFingerMark(GetLoginOutData userInfo, String password) {
        return null;
    }

    @Override
    public String checkRegisterByPasswordAndFingerMark(GetLoginOutData userInfo, String password, Object fingerMark) {
        return null;
    }

    @Override
    public void remoteCallYunNan(GetLoginOutData userInfo, List<String> gssrSaleBillNoList) {
        if (gssrSaleBillNoList.size()==0){
            return;
        }
        for (String gssrSaleBillNo : gssrSaleBillNoList) {
            String client = userInfo.getClient();
            // 根据销售单号查出列表
            ArrayList<GaiaSdSaleRecipel> gaiaSdSaleRecipels = gaiaSdSaleRecipelMapper.queryReviewBySaleBillNo(client, gssrSaleBillNo);   // GSSR_TYPE = '2'
            if (gaiaSdSaleRecipels.size() == 0){
                throw new BusinessException("推送,不存在该处方");
            }
            GaiaSdSaleRecipel gaiaSdSaleRecipel = gaiaSdSaleRecipels.get(0);
            GaiaStoreData gaiaStoreData = remoteReviewMapper.queryStoreData(client, gaiaSdSaleRecipel.getGssrBrId());// 对应门店主数据
            YunNanPrescriptionInData inData = new YunNanPrescriptionInData();
            inData.setInstNo(YunnanSuperviseUtil.instNo);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(DateUtil.format(new Date(), "yyyyMMdd"))
                    .append(YunnanSuperviseUtil.instNo)
                    .append(System.currentTimeMillis());
            inData.setSeqNo(stringBuilder.toString());      // 唯一编号 日期 + 机构编号 + 序列号（机构自定义）yyyyMMddYN001000001
            inData.setPrescriptionNo(gaiaSdSaleRecipel.getGssrRecipelId());  // 处方编号
            inData.setHospitalName(gaiaSdSaleRecipel.getGssrRecipelHospital());     // 开方医院
            inData.setDoctorName(gaiaSdSaleRecipel.getGssrRecipelDoctor());   // 开方医生姓名
            inData.setPrescribeDate(gaiaSdSaleRecipel.getGssrDate());    // 开方时间yyyyMMdd
            inData.setStoreNo(gaiaStoreData.getStoNo());  // 药店编号 统一社会信用代码
            inData.setStoreName(gaiaSdSaleRecipel.getGssrBrName());        // 药店名称
            inData.setAreaNo("88604");   // 药店所在区域 88604
            inData.setPatientName(gaiaSdSaleRecipel.getGssrCustName());      // 病人姓名
            if (StringUtil.equals(gaiaSdSaleRecipel.getGssrCustSex(),"0")){
                inData.setPatientGender(1);
            }else {
                inData.setPatientGender(0);
            }
            inData.setPatientAge(Integer.valueOf(gaiaSdSaleRecipel.getGssrCustAge()));    // 病人年龄
            inData.setPatientPhone(gaiaSdSaleRecipel.getGssrCustMobile());  // 病人联系方式
            inData.setDiagnosis(gaiaSdSaleRecipel.getGssrDiagnose());     // 临床诊断
            inData.setImgUrl(gaiaSdSaleRecipel.getGssrRecipelPicAddress());    // 处方图片网络地址
            inData.setRegistrationNo(userInfo.getUserYsId());    // 审方药师注册证编号   药师注册证编号取值为：USER_YS_ID
            inData.setVerificationTime(gaiaSdSaleRecipel.getGssrCheckDate() + DateUtil.format(DateUtil.parse(gaiaSdSaleRecipel.getGssrCheckTime(), "HH:mm:ss"), "HHmmss"));  // 审方时间yyyyMMddHHmmss
            List<YunNanPrescriptionDrugInData> drugList = new ArrayList<>();// 药品信息说明
            for (GaiaSdSaleRecipel sdSaleRecipel : gaiaSdSaleRecipels) {
                GaiaProductBusiness productBusiness = remoteReviewMapper.queryProductBySelfCode(sdSaleRecipel.getClientId(),sdSaleRecipel.getGssrBrId(),sdSaleRecipel.getGssrProId());    // 查出对应商品信息
                YunNanPrescriptionDrugInData yunNanPrescriptionDrugInData = new YunNanPrescriptionDrugInData();
                yunNanPrescriptionDrugInData.setDrugNo(productBusiness.getProSelfCode()); // 药品编号 国药准字编号
                yunNanPrescriptionDrugInData.setDrugName(productBusiness.getProName());   // 药品名称 商品名
                yunNanPrescriptionDrugInData.setSpec(productBusiness.getProSpecs());   // 药品规格
                yunNanPrescriptionDrugInData.setAmount(Double.valueOf(sdSaleRecipel.getGssrQty()).intValue());   // 药品数量
                yunNanPrescriptionDrugInData.setUnit(productBusiness.getProUnit()); // 药品单位
                yunNanPrescriptionDrugInData.setInstructions(productBusiness.getProUsage()); // 药品用法用量
                drugList.add(yunNanPrescriptionDrugInData);
            }
            inData.setDrugList(drugList);
            yunNanZYYSSuperviseService.prescription(inData);
        }
    }

    @Override
    public List<StoreListOutData> storeListQry(String client) {
        return gaiaSdSaleRecipelMapper.storeListQry(client);
    }

    // 修改处方登记记录表状态
    public Boolean updateReviewStatus(String client, String gssrSaleBillNo, String code, String userId, String loginName, String formatDate, String formatTime, String newUrl, String gssrTime, String gssrDate) {
        Integer affectedRows = gaiaSdSaleRecipelMapper.updateReviewStatus(client, gssrSaleBillNo, code,userId,loginName,formatDate,formatTime,newUrl,gssrTime,gssrDate);
        if (affectedRows>0){
            return true;
        }
        return false;
    }

}
