package com.gys.business.service;

import com.gys.business.service.data.YunNanPrescriptionInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;

/**
 * @author li_haixia@gov-info.cn
 * @desc 云南省执业药师远程药事服务及审方业务监管系统
 * @date 2021/11/12 11:59
 */
public interface YunNanZYYSSuperviseService {
    JsonResult login(GetLoginOutData userInfo);
    JsonResult logout(GetLoginOutData userInfo);
    JsonResult prescription(YunNanPrescriptionInData inData);
    JsonResult salesDetail();

}
