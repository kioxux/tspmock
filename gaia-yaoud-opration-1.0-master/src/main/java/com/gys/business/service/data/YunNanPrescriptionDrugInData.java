package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/12 14:24
 */
@Data
public class YunNanPrescriptionDrugInData {
    @ApiModelProperty("药品编号 国药准字编号")
    private String drugNo;
    @ApiModelProperty("药品名称")
    private String drugName;
    @ApiModelProperty("药品规格")
    private String spec;
    @ApiModelProperty("药品数量")
    private Integer amount;
    @ApiModelProperty("药品单位")
    private String unit;
    @ApiModelProperty("药品用法用量")
    private String instructions;

}
