package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "配送信息")
public class GetDiaoboOutData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "配送单号")
    private String wmPsdh;
    @ApiModelProperty(value = "订单号")
    private String wmDdh;
    @ApiModelProperty(value = "")
    private String wmKhBh;
    private String wmXqlx;
    @ApiModelProperty(value = "单证金额")
    private String totalAmount;
    @ApiModelProperty(value = "合计数量")
    private String totalQty;
    @ApiModelProperty(value = "配送日期")
    private String wmCjrq;
    private String wmCjsj;
    private String wmCjr;
    private String wmSfsq;
    private String wmSfsf;
    private String wmXgrq;
    private String wmXgsj;
    private String wmXgr;
    @ApiModelProperty(value = "备注")
    private String remark;
}
