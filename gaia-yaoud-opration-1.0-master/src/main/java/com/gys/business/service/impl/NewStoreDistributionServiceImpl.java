package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdNewStoreDistributionRecordDMapper;
import com.gys.business.mapper.GaiaSdNewStoreDistributionRecordHMapper;
import com.gys.business.mapper.GaiaSdStoresGroupMapper;
import com.gys.business.mapper.entity.GaiaSdNewStoreDistributionRecordD;
import com.gys.business.mapper.entity.GaiaSdNewStoreDistributionRecordH;
import com.gys.business.mapper.entity.GaiaSdStoresGroup;
import com.gys.business.service.NewStoreDistributionService;
import com.gys.business.service.data.NewStoreDistributionBaseInData;
import com.gys.business.service.data.NewStoreDistributionOutData;
import com.gys.business.service.data.NewStoreDistributionRecordDInData;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.util.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author SunJiaNan
 * @version 1.0
 * @date 2021/10/30 13:54
 * @description 门店必备商品业务层
 */
@Service
@Slf4j
public class NewStoreDistributionServiceImpl implements NewStoreDistributionService {

    @Autowired
    private GaiaSdStoresGroupMapper storesGroupMapper;
    @Autowired
    private GaiaSdNewStoreDistributionRecordHMapper newStoreDistributionRecordHMapper;
    @Autowired
    private GaiaSdNewStoreDistributionRecordDMapper newStoreDistributionRecordDMapper;
    @Autowired
    private DictionaryUtil dictionaryUtil;
    @Autowired
    private CosUtils cosUtils;

    @Override
    public JsonResult getBaseInfo(NewStoreDistributionBaseInData inData) {
        String client = inData.getClient();
        String type = inData.getType();
        if(ValidateUtil.isEmpty(type)) {
            throw new BusinessException("提示：类型不可为空");
        }

        //返回参数
        Map<String, Object> result = new HashMap<>(16);
        switch (type) {
            case "1" :   //查询类型 1-获取数据来源以及现状(药品占比)
                String lastMonthDesc = newStoreDistributionRecordHMapper.getLastMonth();    //获取数据来源
                BigDecimal drugsProportion = newStoreDistributionRecordHMapper.getDrugsProportion(client);   //获取药品占比
                result.put("lastMonthDesc", lastMonthDesc);
                result.put("drugsProportion", drugsProportion);
                break;
            case "2" :   //2-使用门店号获取店效级别
                String storeId = inData.getStoreId();
                if(ValidateUtil.isEmpty(storeId)) {
                    throw new BusinessException("提示：门店编号不可为空");
                }
                GaiaSdStoresGroup storesGroup = storesGroupMapper.getByStoreAndType(client, storeId, "DX0002");
                String levelName = storesGroup.getGssgName();
                result.put("levelName", levelName);
                break;
            case "3" :   //3-使用新店面积以及预估药品销售占比获取计划药品铺货品项数
                String newStoreSize = inData.getNewStoreSize();   //新店面积
                String expectDrugSaleProp = inData.getExpectDrugSaleProp();   //预估药品销售占比
                if(ValidateUtil.isEmpty(newStoreSize)) {
                    throw new BusinessException("提示：新店面积不可为空");
                }
                if(ValidateUtil.isEmpty(expectDrugSaleProp)) {
                    throw new BusinessException("提示：预估药品销售占比不可为空");
                }

                //获取码值对应铺货面积
                String storeSize = dictionaryUtil.getValueByTypeCode("NewStoreDistributionSize", newStoreSize);
                //获取码值对应预估药品销售占比
                String drugsSaleProp = dictionaryUtil.getValueByTypeCode("NewStoreDistributionDrugsSaleProp", expectDrugSaleProp);
                //校验数据是否存在
                if(ValidateUtil.isEmpty(storeSize)) {
                    throw new BusinessException("提示：铺货面积查询失败！");
                }
                if(ValidateUtil.isEmpty(drugsSaleProp)) {
                    throw new BusinessException("提示：预估药品销售占比查询失败！");
                }
                //转换 估药品销售占比 类型
                BigDecimal saleProp = new BigDecimal(drugsSaleProp);

                //新店面积
                String[] storeSizeArr = storeSize.split("-");   // - 分割字符串
                BigDecimal minValue = new BigDecimal(storeSizeArr[0]);    //最小面积
                BigDecimal maxValue = new BigDecimal(storeSizeArr[1]);    //最大面积

                //计算公式  最小（大）面积 ÷ 2 * 面积数量 * 预计药品销售占比
                BigDecimal minValueRange = minValue.divide(new BigDecimal(2)).multiply(new BigDecimal(76)).multiply(saleProp).setScale(0, BigDecimal.ROUND_HALF_UP);
                BigDecimal maxValueRange = maxValue.divide(new BigDecimal(2)).multiply(new BigDecimal(76)).multiply(saleProp).setScale(0, BigDecimal.ROUND_HALF_UP);

                String adviseDrugsDistributionRange = "";
                //判断面积是否为120米以上
                if(!"4".equals(newStoreSize)) {
                    adviseDrugsDistributionRange = minValueRange.toString() + "-" + maxValueRange.toString() + "品";
                } else {
                    adviseDrugsDistributionRange = maxValueRange.toString() + "品以上";
                }

                //计算计划药品铺货品项数
                BigDecimal drugsDistributionCount = (minValueRange.add(maxValueRange)).divide(new BigDecimal(2), 0, BigDecimal.ROUND_HALF_UP);
                result.put("drugsDistributionCount", drugsDistributionCount);
                result.put("adviseDrugsDistributionRange", adviseDrugsDistributionRange);
                break;
        }
        return JsonResult.success(result, "success");
    }


    @Override
    @Transactional
    public JsonResult getCalculationList(NewStoreDistributionBaseInData inData) {
        String storeId = inData.getStoreId();            //门店编码
        Integer drugsDistributionCount = inData.getDrugsDistributionCount();   //计划药品铺货品项数
        List<String> storeIdList = inData.getStoreIdList();      //参考门店

        //校验数据
        if(ValidateUtil.isEmpty(storeId)) {
            throw new BusinessException("提示：门店编号不可为空");
        }
        //计划药品铺货品项数
        if(ValidateUtil.isEmpty(drugsDistributionCount)) {
            throw new BusinessException("提示：计划药品铺货品项数不可为空");
        }
        //参考门店列表
        Integer storeSize = 0;
        if(ValidateUtil.isNotEmpty(storeIdList)) {
            storeSize = storeIdList.size();
            if(storeSize > 3) {
                throw new BusinessException("提示：参考门店最多选择3家");
            }
            inData.setStoreIdCount(storeSize);
        }

        BigDecimal totalCostAmt = BigDecimal.ZERO;    //参考成本额 =  参考成本价 * 建议铺货量
        BigDecimal totalSaleAmt = BigDecimal.ZERO;    //参考参考零售额
        //查询新店铺货列表
        List<NewStoreDistributionOutData> distributionList = newStoreDistributionRecordHMapper.getCalculationList(inData);
        if(ValidateUtil.isNotEmpty(distributionList)) {
            for(NewStoreDistributionOutData newStoreDistribution : distributionList) {
                //计算建议铺货量
                getAdviseNumber(newStoreDistribution);

                BigDecimal adviseNumber = newStoreDistribution.getAdviseNumber();             //建议铺货量
                BigDecimal referenceRetailPrice = newStoreDistribution.getReferenceRetailPrice();     //参考零售价
                BigDecimal referenceCostAmt = newStoreDistribution.getReferenceCostAmt();     //参考成本价
                totalCostAmt = totalCostAmt.add(referenceCostAmt.multiply(adviseNumber));     //计算参考成本额
                totalSaleAmt = totalSaleAmt.add(referenceRetailPrice.multiply(adviseNumber));    //累加参考零售额
            }
        }
        //出参
        Map<String, Object> result = new HashMap<>(4);
        result.put("totalCount", distributionList.size());      //实际铺货品项数
        result.put("totalCostAmt", totalCostAmt.divide(new BigDecimal(10000), 1, BigDecimal.ROUND_HALF_UP));    //参考成本额
        result.put("totalSaleAmt", totalSaleAmt.divide(new BigDecimal(10000), 1, BigDecimal.ROUND_HALF_UP));    //参考零售额
        result.put("distributionList", distributionList);       //铺货列表

        //插入计算结果
        insertCalculationResult(inData, result);
        /*try {
            insertCalculationResult(inData, result);
        } catch (Exception e) {
            log.error("插入计算记录异常：", e);
            return JsonResult.success(result, "success");
        }*/
        return JsonResult.success(result, "success");
    }


    /**
     * 计算建议铺货量
     * A.则使用参考门店月均销量为建议铺货量
     * B.否则使用公司店均月销量
     * C.继续判断是否小于3  如果小于3   则处理为最小铺货量 铺货量：成本价 ≥ 100 铺货量=2    成本价＜100 铺货量=3
     * D.优化建议铺货量
     * @param newStoreDistribution
     * @return
     */
    private void getAdviseNumber(NewStoreDistributionOutData newStoreDistribution) {
        BigDecimal adviseNumber = null;
        BigDecimal storeStockNumber = newStoreDistribution.getStoreStockNumber();     //本店库存量
        BigDecimal storeAvgSaleQty = newStoreDistribution.getStoreAvgSaleQty();       //参考门店月均销量
        BigDecimal companyAvgSaleQty = newStoreDistribution.getCompanyAvgSaleQty();   //公司店均月销量
        BigDecimal midPackage = newStoreDistribution.getMidPackage();                 //中包装数量

        //如果本店库量 大于 0 则直接建议铺货量为0  无需进行下列计算
        if(BigDecimal.ZERO.compareTo(storeStockNumber) == -1) {
            newStoreDistribution.setAdviseNumber(BigDecimal.ZERO);
            return;
        }
        //如果参考门店月均销量为大于0  A.则使用参考门店月均销量为建议铺货量   B.否则使用公司店均月销量
        if(BigDecimal.ZERO.compareTo(storeAvgSaleQty) == -1) {
            adviseNumber = storeAvgSaleQty;
        } else {
            adviseNumber = companyAvgSaleQty;
        }

        //赋值后建议铺货量后 C.继续判断是否小于3  如果小于3   则处理为最小铺货量 铺货量：成本价 ≥ 100 铺货量=2    成本价＜100 铺货量=3
        if(new BigDecimal(3).compareTo(adviseNumber) == 1) {     //3 > 参考门店月均销量 或者  公司店均月销量
            BigDecimal referenceCostAmt = newStoreDistribution.getReferenceCostAmt();   //参考成本价
            if(referenceCostAmt.compareTo(new BigDecimal(100)) != -1) {    // 参考成本价 >= 100
                adviseNumber = new BigDecimal(2);
            } else {
                adviseNumber = new BigDecimal(3);
            }
        }

        //D.优化建议铺货量
        //使用建议铺货量 ÷ 中包装数量 四舍五入保留1位小数
        BigDecimal oldMidPackageCount = adviseNumber.divide(midPackage, 1, BigDecimal.ROUND_HALF_UP);     //原中包装个数
        BigDecimal decimalPlaces = oldMidPackageCount.remainder(BigDecimal.ONE);                //获取小数位  demo：如果为5.9 则获取后为0.9

        //判断小数位是否 ≥ 0.8  如果大于等于0.8就要原中包装个数进位保留整数位 * 中包装数量  否的话直接取 ABC计算数量
        if(decimalPlaces.compareTo(new BigDecimal(0.8)) != -1) {
            BigDecimal integer = oldMidPackageCount.setScale(0, BigDecimal.ROUND_HALF_UP);    //原中包装个数进位保留整数位
            adviseNumber = integer.multiply(midPackage);              //原中包装个数进位保留整数位 * 中包装数量
        }
        newStoreDistribution.setAdviseNumber(adviseNumber);
    }


    /**
     * @param inData
     * @param result
     */
    private void insertCalculationResult(NewStoreDistributionBaseInData inData, Map<String, Object> result) {
        String client = inData.getClient();
        String storeId = inData.getStoreId();
        String createDate = CommonUtil.getyyyyMMdd();
        String userId = inData.getUserId();

        //获取列表
        List<NewStoreDistributionOutData> distributionList = (List<NewStoreDistributionOutData>) result.get("distributionList");
        //插入数据
        String nextOrderId = String.valueOf(SnowflakeIdUtil.getNextId());
        Date dateTime = new Date();
        GaiaSdNewStoreDistributionRecordH recordH = new GaiaSdNewStoreDistributionRecordH();
        recordH.setClient(client);
        recordH.setStoreId(storeId);
        recordH.setCreateDate(createDate);
        recordH.setOrderId(nextOrderId);             //单号
        recordH.setResultNumber(new BigDecimal(distributionList.size()));       //实际铺货品项数
        recordH.setReferenceCostAmt((BigDecimal) result.get("totalCostAmt"));   //参考成本额
        recordH.setReferenceSaleAmt((BigDecimal) result.get("totalSaleAmt"));   //参考零售额
        recordH.setCreateUser(userId);
        recordH.setCreateTime(dateTime);
        newStoreDistributionRecordHMapper.deleteByCondition(recordH);   //删除当日最新数据
        newStoreDistributionRecordDMapper.deleteByCondition(client, storeId, createDate);    //删除明细表当日最新数据

        List<GaiaSdNewStoreDistributionRecordD> batchInsertList = new ArrayList<>(distributionList.size());
        //组装参数
        for(NewStoreDistributionOutData newStoreDistribution : distributionList) {
            GaiaSdNewStoreDistributionRecordD recordD = new GaiaSdNewStoreDistributionRecordD();
            recordD.setClient(client);
            recordD.setStoreId(storeId);
            recordD.setCreateDate(createDate);
            recordD.setOrderId(nextOrderId);         //单号
            recordD.setProSelfCode(newStoreDistribution.getProSelfCode());     //商品自编码
            recordD.setStoreAttribute(newStoreDistribution.getStoreAttributeCode());     //门店属性
            recordD.setStoreLevel(newStoreDistribution.getStoreLevelCode());             //店效级别
            recordD.setReferenceCostAmt(newStoreDistribution.getReferenceCostAmt());     //参考成本价
            recordD.setMidPackage(newStoreDistribution.getMidPackage());                 //中包装
            recordD.setCompanyAvgSaleQty(newStoreDistribution.getCompanyAvgSaleQty());   //公司店均月销量
            recordD.setStoreAvgSaleQty(newStoreDistribution.getStoreAvgSaleQty());       //门店店均月销量
            recordD.setWmsNumber(newStoreDistribution.getWmsNumber());                   //仓库量
            recordD.setStoreStockNumber(newStoreDistribution.getStoreStockNumber());     //本店库存
            recordD.setAdviseNumber(newStoreDistribution.getAdviseNumber());             //建议铺货量
            recordD.setCreateUser(userId);
            recordD.setCreateTime(dateTime);
            batchInsertList.add(recordD);
        }

        //如果铺货列表不为空则执行插入  否则只删除数据 不插入数据
        if(ValidateUtil.isNotEmpty(distributionList)) {
            newStoreDistributionRecordHMapper.insert(recordH);              //查询数据为当日最新数据
            newStoreDistributionRecordDMapper.batchInsert(batchInsertList);     //批量插入明细表数据
        }
    }

    @Override
    public JsonResult export(NewStoreDistributionRecordDInData inData) {
        String storeId = inData.getStoreId();
        if(ValidateUtil.isEmpty(storeId)) {
            throw new BusinessException("提示：请选择门店!");
        }
        //查询该加盟商门店 当天生成最新结果
        inData.setCreateDate(CommonUtil.getyyyyMMdd());    //指定日期为当天最新计算数据
        List<NewStoreDistributionOutData> newStoreDistributionList = newStoreDistributionRecordDMapper.getNewStoreDistributionList(inData);
        if(ValidateUtil.isEmpty(newStoreDistributionList)) {
            return JsonResult.success("导出数据为空", "success");
        }

        // 行号 (同一单号下顺序排列)
        AtomicInteger index = new AtomicInteger(1);
        //组装导出内容
        List<List<Object>> dataList = new ArrayList<>(newStoreDistributionList.size());
        for(NewStoreDistributionOutData newStoreDistribution : newStoreDistributionList) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //序号
            lineList.add(index.getAndIncrement());
            //商品自编码
            lineList.add(newStoreDistribution.getProSelfCode());
            //大类编码
            lineList.add(newStoreDistribution.getBigTypeCode());
            //大类名称
            lineList.add(newStoreDistribution.getBigTypeName());
            //中类编码
            lineList.add(newStoreDistribution.getMidTypeCode());
            //中类名称
            lineList.add(newStoreDistribution.getMidTypeName());
            //商品分类编码
            lineList.add(newStoreDistribution.getTypeCode());
            //商品分类名称
            lineList.add(newStoreDistribution.getTypeName());
            //商品描述
            lineList.add(newStoreDistribution.getDesc());
            //商品规格
            lineList.add(newStoreDistribution.getSpecs());
            //生产厂家
            lineList.add(newStoreDistribution.getFactoryName());
            //单位
            lineList.add(newStoreDistribution.getUnit());
            //门店编号
            lineList.add(newStoreDistribution.getStoreId());
            //门店名称
            lineList.add(newStoreDistribution.getStoreName());
            //门店属性
            lineList.add(newStoreDistribution.getStoreAttribute());
            //店效级别
            lineList.add(newStoreDistribution.getStoreLevel());
            //参考成本价
            lineList.add(newStoreDistribution.getReferenceCostAmt());
            //中包装
            lineList.add(newStoreDistribution.getMidPackage());
            //公司店均月销量
            lineList.add(newStoreDistribution.getCompanyAvgSaleQty());
            //门店店均月销量
            lineList.add(newStoreDistribution.getStoreAvgSaleQty());
            //仓库量
            lineList.add(newStoreDistribution.getWmsNumber());
            //本店库存
            lineList.add(newStoreDistribution.getStoreStockNumber());
            //建议铺货量
            lineList.add(newStoreDistribution.getAdviseNumber());
            dataList.add(lineList);
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.NEW_STORE_DISTRIBUTION_EXPORT_HEAD);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add(CommonConstant.NEW_STORE_DISTRIBUTION_EXPORT_SHEET_NAME);
                }});

        JsonResult uploadResult = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.NEW_STORE_DISTRIBUTION_EXPORT_SHEET_NAME + "-" + CommonUtil.getyyyyMMdd() + ".xls";
            uploadResult = cosUtils.uploadFileNew(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}",e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}",e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;
    }
}
