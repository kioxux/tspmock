package com.gys.business.service.data.Unqualified;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class UnqualifiedDto {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "门店名称")
    private String stoName;
    @ApiModelProperty(value = "商品编码")
    private String proId;
    @ApiModelProperty(value = "商品名称")
    private String proName;
    @ApiModelProperty(value = "规格")
    private String specs;
    @ApiModelProperty(value = "厂家")
    private String factoryName;
    @ApiModelProperty(value = "单位")
    private String unit;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    @ApiModelProperty(value = "有效期")
    private String validDate;
    @ApiModelProperty(value = "批次")
    private String batch;
    @ApiModelProperty(value = "不合格品数量")
    private BigDecimal qty;
    @ApiModelProperty(value = "单号")
    private String billNo;
    @ApiModelProperty(value = "单据日期")
    private String billDate;
    @ApiModelProperty(value = "状态")
    private String status;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "创建人")
    private String userName;
    @ApiModelProperty(value = "行号")
    private String serial;
    @ApiModelProperty(value = "通用名")
    private String commonName;
    @ApiModelProperty(value = "实时库存量")
    private BigDecimal stockQty;


}
