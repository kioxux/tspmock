package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.entity.GaiaSaletaskDplan;
import com.gys.business.mapper.entity.GaiaSaletaskHplan;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

public interface SaleTaskService {
    SaleTaskHplan insertH(GetLoginOutData userInfo, SaleTaskInData saletask);

    void insertD(GetLoginOutData userInfo, SaleTaskInData saletask,String type);

    SaleTaskInData selectById( SaleTaskInData saletask);

    List<SaleTaskOutData> selectPage(SaleTaskInData saletask);

    void edit(GetLoginOutData userInfo,SaleTaskInData saletask);

    void delect(GetLoginOutData userInfo, SaleTaskInData saletask);

    List<SaleTaskStroeOutData> getStoreBySaleTask(GaiaStoreData storeData);

    void getImportExcelSaleTask(SaleTaskInData inData,GetLoginOutData userInfo);

    PageInfo<Saletask> selectSaleTaskReachPage(Saletask saletask);

    void update(GaiaSaletaskHplan saletask);
}
