package com.gys.business.service.data.percentageplan;

import lombok.Data;

import java.util.List;

@Data
public class ImportProInData {
    private String clientId;
    private String dcCode;
    private List<String> proCodes;
}
