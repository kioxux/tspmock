package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/10 17:37
 */
@Data
public class RemoveIncomeForm {
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(hidden = true)
    private String depId;
    @ApiModelProperty(hidden = true)
    private String userId;
    @ApiModelProperty(value = "损益单号", example = "SYS2021000162")
    private String gsisdVoucherId;
    @ApiModelProperty(value = "商品编码", example = "A01001232")
    private String gsisdProId;
    @ApiModelProperty(value = "行号", example = "2")
    private String gsisdSerial;
}
