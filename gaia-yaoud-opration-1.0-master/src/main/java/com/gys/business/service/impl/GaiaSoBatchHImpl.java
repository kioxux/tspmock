package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSoBatchHMapper;
import com.gys.business.mapper.entity.GaiaSoBatchH;
import com.gys.business.service.GaiaSoBatchHService;
import com.gys.business.service.data.yaoshibang.CustomerDto;
import com.gys.business.service.data.yaoshibang.OrderDetailDto;
import com.gys.business.service.data.yaoshibang.OrderQueryBean;
import com.gys.common.enums.YaoshibangDeliverTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhangdong
 * @date 2021/6/10 18:13
 */
@Service
@Slf4j
public class GaiaSoBatchHImpl implements GaiaSoBatchHService {

    @Autowired
    private GaiaSoBatchHMapper hMapper;

    public List<GaiaSoBatchH> selectAll() {
        return hMapper.findAll();
    }

    public void insertList(List<GaiaSoBatchH> list) {
        hMapper.insertList(list);
    }

    @Override
    public void updateStatus(List<GaiaSoBatchH> batchHList, String status) {
        hMapper.updateStatus(batchHList.stream().map(r -> r.getId()).collect(Collectors.toList()), status);
    }

    @Override
    public void updateCustomerId(Long id, String customerId) {
        hMapper.updateCustomerId(id, customerId);
    }

    @Override
    public void updateStatusByClientAndCustomerName(String client, String customerName, String status) {
        hMapper.updateStatusByClientAndCustomerName(client, customerName, status);
    }

    @Override
    public void updateCustomerIdClientAndCustomerName(String client, String customerName, String customerId) {
        hMapper.updateCustomerIdClientAndCustomerName(client, customerName, customerId);
    }

    @Override
    public List<GaiaSoBatchH> orderQuery(OrderQueryBean bean) {
        List<GaiaSoBatchH> list = hMapper.orderQuery(bean);
        return list;
    }

    @Override
    public List<OrderDetailDto> orderDetail(Long id, String client, String depId) {
        List<OrderDetailDto> list = hMapper.orderDetail(id, client, depId);
        for (OrderDetailDto dto : list) {
            dto.setSoDelivertype(YaoshibangDeliverTypeEnum.getType(dto.getSoDelivertype()));
        }
        return list;
    }

    @Override
    public List<CustomerDto> customerQuery(String name, String client) {
        return hMapper.customerQuery(name, client);
    }

    @Override
    public GaiaSoBatchH selectByPrimaryKey(Long id) {
        return hMapper.selectByPrimaryKey(id);
    }

    @Override
    public void updateBatchOrderId(Long id, String batchOrderId) {
        hMapper.updateBatchOrderId(id, batchOrderId);
    }

    @Override
    public Map<String, GaiaSoBatchH> selectAllCustomerId(String client) {
        return hMapper.selectAllCustomerId(client);
    }

    @Override
    public void compensate(String customerName, String customerId, String client) {
        hMapper.updateCustomerIdClientAndCustomerName(client, customerName, customerId);
    }

    @Override
    public List<String> queryOrderIdExist(String client, List<String> orderIdQueryList) {
        return hMapper.queryOrderIdExist(client, orderIdQueryList);
    }

}
