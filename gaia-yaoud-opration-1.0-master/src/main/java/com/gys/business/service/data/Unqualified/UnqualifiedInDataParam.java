package com.gys.business.service.data.Unqualified;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UnqualifiedInDataParam {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店")
    private String stoCode;
    @ApiModelProperty(value = "关键字查询")
    private String keyword;
    @ApiModelProperty(value = "单号")
    private String billNo;
    @ApiModelProperty(value = "单据日期")
    private String billDate;
    @ApiModelProperty(value = "审核状态 1：驳回 2：通过")
    private String wfStatus;
    @ApiModelProperty(value = "单号")
    private String wfOrder;
    @ApiModelProperty(value = "审核人id")
    private String approverUserId;

}
