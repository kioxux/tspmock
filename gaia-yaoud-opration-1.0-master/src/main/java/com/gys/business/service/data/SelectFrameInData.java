package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SelectFrameInData implements Serializable {

    /**
     * 查询类型 all-所有 1-地区列表 2-标志列表 3-商品成分分类列表
     */
    @ApiModelProperty(value="查询类型 all-所有 1-地区列表 2-标志列表 3-商品成分分类列表")
    private String frameType;

    /**
     * 查询类型2 1-季节 2-品牌 当查询类型为2时 此字段生效 默认查询季节
     */
    @ApiModelProperty(value="查询类型2 1-季节 2-品牌 当frameType为2或者all时 此字段生效 默认查询季节")
    private String queryType;
}
