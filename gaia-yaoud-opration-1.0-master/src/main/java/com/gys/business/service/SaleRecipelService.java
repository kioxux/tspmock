//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface SaleRecipelService {
    PageInfo<GaiaSdSaleRecipelOutData> getAuditedSaleRecipelList(GaiaSdSaleRecipelInData inData);

    PageInfo<GaiaSdSaleRecipelRecordOutData> getSaleRecipelRecordList(GaiaSdSaleRecipelRecordInData inData);

    void addOrEditRecipelRecord(List<GaiaSdSaleRecipelRecordInData> inData);

    void auditRecipelRecord(List<GaiaSdSaleRecipelRecordInData> inData);

    List<ControlledDrugsOutData> getControlledDrugsManage(ControlledDrugsInData inData );

    List<SalePecipelProductOutData> getProductInfo(SalePecipelProductInData inData);

    int updatePicture(SaleRecipelPictureInData inData);
}
