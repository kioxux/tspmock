//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class GetSyncSdProductPriceOutData {
    private String clientId;
    private String gsppBrId;
    private String gsppProId;
    private BigDecimal gsppPriceNormal;
    private BigDecimal gsppPriceHy;
    private BigDecimal gsppPriceYb;
    private BigDecimal gsppPriceCl;
    private BigDecimal gsppPriceHyr;
    private BigDecimal gsppPriceOnlineNormal;
    private BigDecimal gsppPriceOnlineHy;

    public GetSyncSdProductPriceOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsppBrId() {
        return this.gsppBrId;
    }

    public String getGsppProId() {
        return this.gsppProId;
    }

    public BigDecimal getGsppPriceNormal() {
        return this.gsppPriceNormal;
    }

    public BigDecimal getGsppPriceHy() {
        return this.gsppPriceHy;
    }

    public BigDecimal getGsppPriceYb() {
        return this.gsppPriceYb;
    }

    public BigDecimal getGsppPriceCl() {
        return this.gsppPriceCl;
    }

    public BigDecimal getGsppPriceHyr() {
        return this.gsppPriceHyr;
    }

    public BigDecimal getGsppPriceOnlineNormal() {
        return this.gsppPriceOnlineNormal;
    }

    public BigDecimal getGsppPriceOnlineHy() {
        return this.gsppPriceOnlineHy;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsppBrId(final String gsppBrId) {
        this.gsppBrId = gsppBrId;
    }

    public void setGsppProId(final String gsppProId) {
        this.gsppProId = gsppProId;
    }

    public void setGsppPriceNormal(final BigDecimal gsppPriceNormal) {
        this.gsppPriceNormal = gsppPriceNormal;
    }

    public void setGsppPriceHy(final BigDecimal gsppPriceHy) {
        this.gsppPriceHy = gsppPriceHy;
    }

    public void setGsppPriceYb(final BigDecimal gsppPriceYb) {
        this.gsppPriceYb = gsppPriceYb;
    }

    public void setGsppPriceCl(final BigDecimal gsppPriceCl) {
        this.gsppPriceCl = gsppPriceCl;
    }

    public void setGsppPriceHyr(final BigDecimal gsppPriceHyr) {
        this.gsppPriceHyr = gsppPriceHyr;
    }

    public void setGsppPriceOnlineNormal(final BigDecimal gsppPriceOnlineNormal) {
        this.gsppPriceOnlineNormal = gsppPriceOnlineNormal;
    }

    public void setGsppPriceOnlineHy(final BigDecimal gsppPriceOnlineHy) {
        this.gsppPriceOnlineHy = gsppPriceOnlineHy;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSyncSdProductPriceOutData)) {
            return false;
        } else {
            GetSyncSdProductPriceOutData other = (GetSyncSdProductPriceOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gsppBrId = this.getGsppBrId();
                Object other$gsppBrId = other.getGsppBrId();
                if (this$gsppBrId == null) {
                    if (other$gsppBrId != null) {
                        return false;
                    }
                } else if (!this$gsppBrId.equals(other$gsppBrId)) {
                    return false;
                }

                Object this$gsppProId = this.getGsppProId();
                Object other$gsppProId = other.getGsppProId();
                if (this$gsppProId == null) {
                    if (other$gsppProId != null) {
                        return false;
                    }
                } else if (!this$gsppProId.equals(other$gsppProId)) {
                    return false;
                }

                label110: {
                    Object this$gsppPriceNormal = this.getGsppPriceNormal();
                    Object other$gsppPriceNormal = other.getGsppPriceNormal();
                    if (this$gsppPriceNormal == null) {
                        if (other$gsppPriceNormal == null) {
                            break label110;
                        }
                    } else if (this$gsppPriceNormal.equals(other$gsppPriceNormal)) {
                        break label110;
                    }

                    return false;
                }

                label103: {
                    Object this$gsppPriceHy = this.getGsppPriceHy();
                    Object other$gsppPriceHy = other.getGsppPriceHy();
                    if (this$gsppPriceHy == null) {
                        if (other$gsppPriceHy == null) {
                            break label103;
                        }
                    } else if (this$gsppPriceHy.equals(other$gsppPriceHy)) {
                        break label103;
                    }

                    return false;
                }

                Object this$gsppPriceYb = this.getGsppPriceYb();
                Object other$gsppPriceYb = other.getGsppPriceYb();
                if (this$gsppPriceYb == null) {
                    if (other$gsppPriceYb != null) {
                        return false;
                    }
                } else if (!this$gsppPriceYb.equals(other$gsppPriceYb)) {
                    return false;
                }

                label89: {
                    Object this$gsppPriceCl = this.getGsppPriceCl();
                    Object other$gsppPriceCl = other.getGsppPriceCl();
                    if (this$gsppPriceCl == null) {
                        if (other$gsppPriceCl == null) {
                            break label89;
                        }
                    } else if (this$gsppPriceCl.equals(other$gsppPriceCl)) {
                        break label89;
                    }

                    return false;
                }

                label82: {
                    Object this$gsppPriceHyr = this.getGsppPriceHyr();
                    Object other$gsppPriceHyr = other.getGsppPriceHyr();
                    if (this$gsppPriceHyr == null) {
                        if (other$gsppPriceHyr == null) {
                            break label82;
                        }
                    } else if (this$gsppPriceHyr.equals(other$gsppPriceHyr)) {
                        break label82;
                    }

                    return false;
                }

                Object this$gsppPriceOnlineNormal = this.getGsppPriceOnlineNormal();
                Object other$gsppPriceOnlineNormal = other.getGsppPriceOnlineNormal();
                if (this$gsppPriceOnlineNormal == null) {
                    if (other$gsppPriceOnlineNormal != null) {
                        return false;
                    }
                } else if (!this$gsppPriceOnlineNormal.equals(other$gsppPriceOnlineNormal)) {
                    return false;
                }

                Object this$gsppPriceOnlineHy = this.getGsppPriceOnlineHy();
                Object other$gsppPriceOnlineHy = other.getGsppPriceOnlineHy();
                if (this$gsppPriceOnlineHy == null) {
                    if (other$gsppPriceOnlineHy != null) {
                        return false;
                    }
                } else if (!this$gsppPriceOnlineHy.equals(other$gsppPriceOnlineHy)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSyncSdProductPriceOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsppBrId = this.getGsppBrId();
        result = result * 59 + ($gsppBrId == null ? 43 : $gsppBrId.hashCode());
        Object $gsppProId = this.getGsppProId();
        result = result * 59 + ($gsppProId == null ? 43 : $gsppProId.hashCode());
        Object $gsppPriceNormal = this.getGsppPriceNormal();
        result = result * 59 + ($gsppPriceNormal == null ? 43 : $gsppPriceNormal.hashCode());
        Object $gsppPriceHy = this.getGsppPriceHy();
        result = result * 59 + ($gsppPriceHy == null ? 43 : $gsppPriceHy.hashCode());
        Object $gsppPriceYb = this.getGsppPriceYb();
        result = result * 59 + ($gsppPriceYb == null ? 43 : $gsppPriceYb.hashCode());
        Object $gsppPriceCl = this.getGsppPriceCl();
        result = result * 59 + ($gsppPriceCl == null ? 43 : $gsppPriceCl.hashCode());
        Object $gsppPriceHyr = this.getGsppPriceHyr();
        result = result * 59 + ($gsppPriceHyr == null ? 43 : $gsppPriceHyr.hashCode());
        Object $gsppPriceOnlineNormal = this.getGsppPriceOnlineNormal();
        result = result * 59 + ($gsppPriceOnlineNormal == null ? 43 : $gsppPriceOnlineNormal.hashCode());
        Object $gsppPriceOnlineHy = this.getGsppPriceOnlineHy();
        result = result * 59 + ($gsppPriceOnlineHy == null ? 43 : $gsppPriceOnlineHy.hashCode());
        return result;
    }

    public String toString() {
        return "GetSyncSdProductPriceOutData(clientId=" + this.getClientId() + ", gsppBrId=" + this.getGsppBrId() + ", gsppProId=" + this.getGsppProId() + ", gsppPriceNormal=" + this.getGsppPriceNormal() + ", gsppPriceHy=" + this.getGsppPriceHy() + ", gsppPriceYb=" + this.getGsppPriceYb() + ", gsppPriceCl=" + this.getGsppPriceCl() + ", gsppPriceHyr=" + this.getGsppPriceHyr() + ", gsppPriceOnlineNormal=" + this.getGsppPriceOnlineNormal() + ", gsppPriceOnlineHy=" + this.getGsppPriceOnlineHy() + ")";
    }
}
