package com.gys.business.service.takeAway;

import java.util.List;
import java.util.Map;

public interface BaseServicePTS {
    //查询门店基础信息
    List<Map<String, Object>> selectShopInfoByMap(String buid);

    //查询所有门店药品信息
    List<Map<String, Object>> queryMedicine(String shop_no);

    //更新药品信息
    Map<String,Object> updateMedicineInfo(String shop_no,Map<String,Object> param);

    List<Map<String, Object>> queryMedicineStock(Map<String, Object> param);

    //查询有变化的商品基础信息
    List<Map<String,Object>> queryModifyMedicine(Map<String, Object> param);

    List<Map<String,Object>> queryMedicineStockByUpc(Map<String, Object> param);

    //更新PTS内数据的读写标记，标识数据已经被处理了
    String updateMedicineO2OStatus(Map<String, Object> param);
}
