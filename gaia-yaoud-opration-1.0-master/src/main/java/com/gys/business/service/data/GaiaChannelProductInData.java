package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@ApiModel(value="com-gys-business-service-data-GaiaChannelProduct")
@Data
public class GaiaChannelProductInData {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 渠道
    */
    @ApiModelProperty(value="渠道")
    private String gcdChannel;

    /**
    * 门店
    */
    @ApiModelProperty(value="门店")
    private String gcpStoreCode;

    /**
    * 商品编码
    */
    @ApiModelProperty(value="商品编码")
    private String gcpProCode;

    /**
    * 渠道价格
    */
    @ApiModelProperty(value="渠道价格")
    private BigDecimal gcpChannelPrice;

    /**
    * 状态（1-上架，2-下架）
    */
    @ApiModelProperty(value="状态（1-上架，2-下架）")
    private String gcpStatus;

    /**
    * 创建人
    */
    @ApiModelProperty(value="创建人")
    private String gcpCreateBy;

    /**
    * 创建日期
    */
    @ApiModelProperty(value="创建日期")
    private String gcpCreateDate;



    /**
    * 更新人
    */
    @ApiModelProperty(value="更新人")
    private String gcpUpdateBy;

    /**
    * 更新日期
    */
    @ApiModelProperty(value="更新日期")
    private String gcpUpdateDate;

    private String startTime;

    private String endTime;



    private List<String> stoIdList;

    private List<String> channelList;

    private List<String> proCodeList;

    private List<GaiaChannelProductOutData> detailList;

}