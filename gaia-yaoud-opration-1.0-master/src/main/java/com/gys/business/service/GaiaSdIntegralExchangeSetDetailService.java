package com.gys.business.service;

import com.gys.business.service.data.IntegralExchangeSetDetail;
import com.gys.business.service.data.IntegralInData;
import com.gys.common.response.Result;

import java.util.List;

/**
* @author RULAISZ
* @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_DETAIL】的数据库操作Service
* @createDate 2021-11-29 09:46:42
*/
public interface GaiaSdIntegralExchangeSetDetailService {
    List<IntegralExchangeSetDetail> detailList(IntegralInData inData);

    Result exportDetailList(IntegralInData inData);

}
