package com.gys.business.service.data.cancelStock;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CancelStockDetail {
    private String clientId;
    private String stoCode;
    private String cancelStockCode;
    private String cancelSerial;
    private String proCode;
    private String proBatch;
    private String proBatchNo;
    private String cancelQty;
    private String cancelReason;
    private BigDecimal taxPrice;
}
