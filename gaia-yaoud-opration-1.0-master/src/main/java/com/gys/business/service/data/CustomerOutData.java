package com.gys.business.service.data;

import lombok.Data;

/**
 * 查询条件：客户ID-客户名称
 *
 * @author Zhangchi
 * @since 2021/09/16/9:55
 */
@Data
public class CustomerOutData {
    private String client;
    private String customerName;
}
