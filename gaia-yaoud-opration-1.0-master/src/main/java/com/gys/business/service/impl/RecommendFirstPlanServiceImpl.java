package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.IRecommendFirstPlanService;
import com.gys.business.service.data.RecommendChooseProductInData;
import com.gys.business.service.data.RecommendFirstPlanInData;
import com.gys.business.service.data.RecommendFirstPlanRes;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.gys.util.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.*;
import java.util.stream.Collectors;


/**
 * <p>
 * 首推商品方案表 服务实现类
 * </p>
 *
 * @author flynn
 * @since 2021-08-04
 */
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class RecommendFirstPlanServiceImpl implements IRecommendFirstPlanService {

    private static final List<String> preferencesList = new ArrayList();

    static {
        preferencesList.add("LSJ");
        preferencesList.add("MLL");
        preferencesList.add("XQ");
    }

    @Autowired
    private RecommendFirstPlanMapper recommendFirstPlanMapper;
    @Autowired
    private RecommendFirstPalnStoMapper recommendFirstPalnStoMapper;
    @Autowired
    private RecommendFirstPlanProductMapper recommendFirstPlanProductMapper;

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;


    //============================  新增初始化  ==========================

    @Override
    public Map<String, Object> buildInit(GetLoginOutData userInfo) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        Map<String, Object> resultMap = new HashMap<>();
        //业务逻辑
        Map<String, String> stores = new HashMap<>();
        String client = userInfo.getClient();//获取客户编号
        //初始化门店
        Example exampleStore = new Example(GaiaStoreData.class);
        exampleStore.createCriteria().andEqualTo("stoStatus", "0").andEqualTo("client", client);
        List<GaiaStoreData> storeDataList = storeDataMapper.selectByExample(exampleStore);
        if (CollectionUtil.isNotEmpty(storeDataList)) {
            for (GaiaStoreData storeData : storeDataList) {
                stores.put(storeData.getStoCode(), StrUtil.isBlank(storeData.getStoShortName())?storeData.getStoName():storeData.getStoShortName());
            }
        }
        resultMap.put("stores", stores);
        //LSJ 表示零售价优先 MLL表示毛利额优先 XQ表示效期优先
        Map<String, String> preferences = new HashMap<>();
        preferences.put("LSJ", "零售价优先");
        preferences.put("MLL", "毛利额优先");
        preferences.put("XQ", "效期优先");
        resultMap.put("preferences", preferences);
        return resultMap;
    }

    //============================  新增初始化  ==========================

    private boolean checkLegal(RecommendFirstPlanInData inData) {
        boolean res = true;
        //数据验证
        if (StrUtil.isBlank(inData.getPlanName())) {
            throw new BusinessException("请输入方案名称！");
        }
        if(inData.getPlanName().length()>100){
            throw new BusinessException("方案名称长度不能超过100");
        }
        if (StrUtil.isBlank(inData.getEffectStartDate())) {
            throw new BusinessException("请输入生效时间！");
        }
        if (CollectionUtil.isEmpty(inData.getStos())) {
            throw new BusinessException("请选择适用门店！");
        }

        if (StrUtil.isBlank(inData.getPreferences())) {
            throw new BusinessException("请选择偏好设置");
        } else {
            if (!preferencesList.contains(inData.getPreferences())) {
                throw new BusinessException("请传递合法的偏好设置参数");
            }
        }

        if (CollectionUtil.isEmpty(inData.getProIds())) {
            throw new BusinessException("请添加商品！");
        } else {
            Map<String, String> repeatProIdMap = new HashMap<>();
            String message = "";
            for (String proId : inData.getProIds()) {
                if (repeatProIdMap.get(proId) != null) {
                    message = message + proId + ";";
                } else {
                    repeatProIdMap.put(proId, proId);
                }
            }
            if (StrUtil.isNotBlank(message)) {
                throw new BusinessException("下列商品编号的商品" + message + "存在重复添加！");
            }
        }
        return res;
    }


    //============================  新增  ==========================

    @Override
    public Object build(GetLoginOutData userInfo, RecommendFirstPlanInData inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (checkLegal(inData)) {
            //封装入库实体
            RecommendFirstPlan recommendFirstPlan = new RecommendFirstPlan();
            BeanUtils.copyProperties(inData, recommendFirstPlan);
            recommendFirstPlan.setClient(userInfo.getClient());
            recommendFirstPlan.setEffectStartDate(inData.getEffectStartDate().replaceAll("-",""));
            recommendFirstPlan.setCreateBy(userInfo.getUserId());
            recommendFirstPlan.setCreateName(userInfo.getLoginName());
            recommendFirstPlan.setCreateDate(CommonUtil.getyyyyMMdd());
            recommendFirstPlan.setCreateTime(CommonUtil.getHHmmss());
            recommendFirstPlan.setStatus("0");//设置状态为已保存
            recommendFirstPlanMapper.insertUseGeneratedKeys(recommendFirstPlan);
            //获取主键id
            Long planId = recommendFirstPlan.getId();
            //处理逻辑

            //处理店铺
            Map<String, Object> initMap = this.buildInit(userInfo);
            Map<String, String> stores = (Map<String, String>) initMap.get("stores");

            List<RecommendFirstPalnSto> palnStos = new ArrayList<>();
            for (String sto : inData.getStos()) {
                RecommendFirstPalnSto palnSto = new RecommendFirstPalnSto();
                palnSto.setBrId(sto);
                palnSto.setBrName(stores.get(sto));
                palnSto.setClient(userInfo.getClient());
                palnSto.setPlanId(planId);
                palnStos.add(palnSto);
            }
            if (CollectionUtil.isNotEmpty(palnStos)) {
                recommendFirstPalnStoMapper.insertList(palnStos);
            }


            //处理商品
            List<RecommendFirstPlanProduct> planProducts = new ArrayList<>();
            List<String> proIds = inData.getProIds();
            List<GaiaProductBusiness> businessList = getProductBusinessByProIds(userInfo.getClient(), proIds);
            Map<String, String> proIdProCompclassMap = new HashMap<>();
            if(CollectionUtil.isNotEmpty(businessList)){
                for(GaiaProductBusiness business: businessList){
                    proIdProCompclassMap.put(business.getProSelfCode(),business.getProCompclass());
                }
//                proIdProCompclassMap =  businessList.stream().collect(Collectors.toMap(GaiaProductBusiness::getProSelfCode, GaiaProductBusiness::getProCompclass));
            }
            for (String proId : proIds) {
                if (StrUtil.isNotBlank(proId)) {
                    RecommendFirstPlanProduct planProduct = new RecommendFirstPlanProduct();
                    planProduct.setClient(userInfo.getClient());
                    planProduct.setPlanId(planId);
                    planProduct.setProId(proId);
                    planProduct.setProCompClass(proIdProCompclassMap.get(proId));
                    planProducts.add(planProduct);

                }
            }
            if (CollectionUtil.isNotEmpty(palnStos)) {
                recommendFirstPlanProductMapper.insertList(planProducts);
            }
        }

        //处理逻辑
//        recommendFirstPlanMapper.updateByPrimaryKey(recommendFirstPlan);
        return null;
    }

    private List<GaiaProductBusiness> getProductBusinessByProIds(String client, List<String> proIds) {
        List<GaiaProductBusiness> resList = new ArrayList<>();
        Example exampleStore = new Example(GaiaProductBusiness.class);
        exampleStore.createCriteria().andEqualTo("client", client).andIn("proSelfCode", proIds);
        List<GaiaProductBusiness> businessList = gaiaProductBusinessMapper.selectByExample(exampleStore);
        if(CollectionUtil.isNotEmpty(businessList)){
            resList = businessList;
        }
        return resList;
    }


    //============================  新增  ==========================


    //============================  修改初始化  ==========================
    @Override
    public Map<String, Object> updateInit(GetLoginOutData userInfo, Integer id) {

        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (id == null) {
            throw new BusinessException("请传入id");
        }
        Map<String, Object> res = new HashMap<>();
        res = this.buildInit(userInfo);

        RecommendFirstPlanRes planInfo = new RecommendFirstPlanRes();
        List<RecommendFirstPalnSto> palnStos = new ArrayList<>();
        List<RecommendFirstPlanProductRes> planProducts = new ArrayList<>();
        //查询出db对象
        RecommendFirstPlan RecommendFirstPlanDb = recommendFirstPlanMapper.selectByPrimaryKey(Long.parseLong(id + ""));
        if (RecommendFirstPlanDb == null) {
            throw new BusinessException("查询无此数据");
        }

        BeanUtils.copyProperties(RecommendFirstPlanDb, planInfo);

        Example exampleSto = new Example(RecommendFirstPalnSto.class);
        exampleSto.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("planId", Long.parseLong(RecommendFirstPlanDb.getId() + ""));
        List<RecommendFirstPalnSto> palnStosDb = recommendFirstPalnStoMapper.selectByExample(exampleSto);
        if (CollectionUtil.isNotEmpty(palnStosDb)) {
            palnStos = palnStosDb;
        }
        planInfo.setPalnStos(palnStos);

        Example exampleProduct = new Example(RecommendFirstPlanProduct.class);
        exampleProduct.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("planId", Long.parseLong(RecommendFirstPlanDb.getId() + ""));
        List<RecommendFirstPlanProduct> planProductsDb = recommendFirstPlanProductMapper.selectByExample(exampleSto);
        if (CollectionUtil.isNotEmpty(planProductsDb)) {
            Map<String, RecommendFirstPlanProduct> planProductMap = new HashMap<>();
            List<String> proIds = new ArrayList<>();
            for (RecommendFirstPlanProduct planProduct : planProductsDb) {
                proIds.add(planProduct.getProId());
                planProductMap.put(planProduct.getProId(), planProduct);
            }
            List<RecommendFirstPlanProductRes> productResListDb = recommendFirstPlanProductMapper.selectProductInfo(userInfo.getClient(), "", proIds);
            if (CollectionUtil.isNotEmpty(productResListDb)) {
                productResListDb.forEach(x -> {
//                    x.setId(x.getId());
                    x.setPlanProductId(planProductMap.get(x.getProId()) == null ? null : planProductMap.get(x.getProId()).getId());
                });
                planProducts = productResListDb;
            }
        }
        planInfo.setProInfos(planProducts);
        //业务逻辑
        res.put("planInfo", planInfo);
        return res;
    }

    //============================  修改初始化  ==========================


    //============================  修改  ==========================
    @Override
    public Object update(GetLoginOutData userInfo, RecommendFirstPlanInData inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }

        if (checkLegal(inData)) {
            if (inData.getClass() == null) {
                throw new BusinessException("请传入id");
            }
            //封装入库实体
            RecommendFirstPlan recommendFirstPlan = recommendFirstPlanMapper.selectByPrimaryKey(inData.getId());
            if (recommendFirstPlan == null) {
                throw new BusinessException("查无数据！");
            }
            if (StrUtil.isNotBlank(recommendFirstPlan.getStatus()) && recommendFirstPlan.getStatus().equals("1")) {
                throw new BusinessException("已审核数据不能进行编辑");
            }
            BeanUtils.copyProperties(inData, recommendFirstPlan);
            recommendFirstPlan.setClient(userInfo.getClient());
            recommendFirstPlan.setEffectStartDate(inData.getEffectStartDate().replaceAll("-",""));
            recommendFirstPlan.setCreateBy(userInfo.getUserId());
            recommendFirstPlan.setCreateName(userInfo.getLoginName());
            recommendFirstPlan.setCreateDate(CommonUtil.getyyyyMMdd());
            recommendFirstPlan.setCreateTime(CommonUtil.getHHmmss());
            recommendFirstPlan.setStatus("0");//设置状态为已保存
            recommendFirstPlanMapper.updateByPrimaryKey(recommendFirstPlan);
            //获取主键id
            Long planId = recommendFirstPlan.getId();
            //处理逻辑


            //处理店铺
            //先删除再新增
            Example exampleSto = new Example(RecommendFirstPalnSto.class);
            exampleSto.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("planId", Long.parseLong(inData.getId() + ""));
            recommendFirstPalnStoMapper.deleteByExample(exampleSto);

            Map<String, Object> initMap = this.buildInit(userInfo);
            Map<String, String> stores = (Map<String, String>) initMap.get("stores");
            List<RecommendFirstPalnSto> palnStos = new ArrayList<>();
            for (String sto : inData.getStos()) {
                RecommendFirstPalnSto palnSto = new RecommendFirstPalnSto();
                palnSto.setBrId(sto);
                palnSto.setBrName(stores.get(sto));
                palnSto.setClient(userInfo.getClient());
                palnSto.setPlanId(planId);
                palnStos.add(palnSto);
            }
            if (CollectionUtil.isNotEmpty(palnStos)) {
                recommendFirstPalnStoMapper.insertList(palnStos);
            }


            //处理商品
            //先删除再新增
            Example examplePlanProduct = new Example(RecommendFirstPalnSto.class);
            examplePlanProduct.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("planId", Long.parseLong(inData.getId() + ""));
            recommendFirstPlanProductMapper.deleteByExample(exampleSto);

            List<RecommendFirstPlanProduct> planProducts = new ArrayList<>();
            List<String> proIds = inData.getProIds();
            List<GaiaProductBusiness> businessList = getProductBusinessByProIds(userInfo.getClient(), proIds);
            Map<String, String> proIdProCompclassMap = new HashMap<>();
            if(CollectionUtil.isNotEmpty(businessList)){
                for(GaiaProductBusiness business: businessList){
                    proIdProCompclassMap.put(business.getProSelfCode(),business.getProCompclass());
                }
//                proIdProCompclassMap =  businessList.stream().collect(Collectors.toMap(GaiaProductBusiness::getProSelfCode, GaiaProductBusiness::getProCompclass));
            }
            for (String proId : proIds) {
                if (StrUtil.isNotBlank(proId)) {
                    RecommendFirstPlanProduct planProduct = new RecommendFirstPlanProduct();
                    planProduct.setClient(userInfo.getClient());
                    planProduct.setPlanId(planId);
                    planProduct.setProId(proId);
                    planProduct.setProCompClass(proIdProCompclassMap.get(proId));
                    planProducts.add(planProduct);
                }
            }
            if (CollectionUtil.isNotEmpty(palnStos)) {
                recommendFirstPlanProductMapper.insertList(planProducts);
            }
        }
        return null;
    }


    //============================  修改  ==========================


    //============================  删除  ==========================
    @Override
    public int delete(GetLoginOutData userInfo, Integer id) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (id == null) {
            throw new BusinessException("请传入合法数据！");
        }

        RecommendFirstPlan recommendFirstPlanDb = recommendFirstPlanMapper.selectByPrimaryKey(Long.parseLong(id + ""));
        if (recommendFirstPlanDb == null) {
            throw new BusinessException("查无此数据！");
        }
        if (StrUtil.isNotBlank(recommendFirstPlanDb.getStatus()) && !recommendFirstPlanDb.getStatus().equals("0")) {
            throw new BusinessException("只能删除已保存的方案");
        }
        Long planId = recommendFirstPlanDb.getId();
        Example exampleSto = new Example(RecommendFirstPalnSto.class);
        exampleSto.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("planId", planId);
        recommendFirstPalnStoMapper.deleteByExample(exampleSto);


        Example examplePlanProduct = new Example(RecommendFirstPalnSto.class);
        examplePlanProduct.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("planId", planId);
        recommendFirstPlanProductMapper.deleteByExample(exampleSto);

        return recommendFirstPlanMapper.deleteByPrimaryKey(Long.parseLong(id + ""));
    }

    //============================  删除  ==========================


    //============================  详情  ==========================
    @Override
    public Object getDetailById(GetLoginOutData userInfo, Integer id) {
        Object res = new Object();
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        //查询出db对象
        RecommendFirstPlan RecommendFirstPlanDb = recommendFirstPlanMapper.selectByPrimaryKey(id);
        if (RecommendFirstPlanDb == null) {
            throw new BusinessException("查询无此数据");
        }

        //业务逻辑处理最终返回值

        return res;
    }


    //============================  详情  ==========================


    //============================  获取列表（分页）  ==========================

    @Override
    public PageInfo<RecommendFirstPlanRes> getListPage(GetLoginOutData userInfo, RecommendFirstPlanInData inData) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (inData.getPageSize() == null) {
            inData.setPageSize(100);
        }
        if (inData.getPageNum() == null) {
            inData.setPageNum(1);
        }
        List<RecommendFirstPlanRes> resList = new ArrayList<>();
        PageInfo pageInfo;
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        //实际执行的sql，执行定制任务
        Example example = new Example(RecommendFirstPlan.class);
        Example.Criteria criteria = example.createCriteria().andEqualTo("client", userInfo.getClient());
        if (StrUtil.isNotBlank(inData.getPlanName())) {
            criteria.andLike("planName", "%" + inData.getPlanName() + "%");
        }
        if (StrUtil.isNotBlank(inData.getEffectStartDate())) {
            criteria.andGreaterThanOrEqualTo("effectStartDate", inData.getEffectStartDate().replaceAll("-",""));
        }
        if (StrUtil.isNotBlank(inData.getEffectEndDate())) {
            criteria.andLessThanOrEqualTo("effectStartDate", inData.getEffectEndDate().replaceAll("-",""));
        }
        example.setOrderByClause(" EFFECT_START_DATE DESC,CREATE_DATE DESC,CREATE_TIME DESC");
        List<RecommendFirstPlan> dbList = recommendFirstPlanMapper.selectByExample(example);
        Integer count = recommendFirstPlanMapper.selectCountByExample(example);
        Set<Long> planIdSet = new HashSet<>();
        Map<Long, List<RecommendFirstPalnSto>> palnStoMap = new HashMap<>();
        if (ObjectUtil.isNotEmpty(dbList)) {
            //处理返回时可能要处理的转换工作
            dbList.forEach(x -> {
                planIdSet.add(x.getId());
            });
            List<Long> planIds = new ArrayList<>(planIdSet);
            if (CollectionUtil.isNotEmpty(planIds)) {
                Example exampleSto = new Example(RecommendFirstPalnSto.class);
                exampleSto.createCriteria().andEqualTo("client", userInfo.getClient()).andIn("planId", planIds);
                List<RecommendFirstPalnSto> palnStos = recommendFirstPalnStoMapper.selectByExample(exampleSto);
                if (CollectionUtil.isNotEmpty(palnStos)) {
                    palnStos.forEach(x -> {
                        if (CollectionUtil.isNotEmpty(palnStoMap.get(x.getPlanId()))) {
                            List<RecommendFirstPalnSto> stos = palnStoMap.get(x.getPlanId());
                            stos.add(x);
                            palnStoMap.put(x.getPlanId(), stos);
                        } else {
                            List<RecommendFirstPalnSto> stos = new ArrayList<>();
                            stos.add(x);
                            palnStoMap.put(x.getPlanId(), stos);
                        }
                    });
                }
            }

            dbList.forEach(x -> {
                RecommendFirstPlanRes res = new RecommendFirstPlanRes();
                BeanUtils.copyProperties(x, res);
                res.setPalnStos(palnStoMap.get(x.getId()));
                resList.add(res);
            });

            pageInfo = new PageInfo(resList);
            pageInfo.setTotal(count);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public Object audit(GetLoginOutData userInfo, Integer id) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        RecommendFirstPlan recommendFirstPlan = recommendFirstPlanMapper.selectByPrimaryKey(Long.parseLong(id + ""));
        if (recommendFirstPlan == null) {
            throw new BusinessException("查无此数据");
        }
        if (!recommendFirstPlan.getStatus().equals("0")) {
            throw new BusinessException("只能对处于已保存状态的数据进行审核！");
        }
        //1表示已审核
        recommendFirstPlan.setStatus("1");
        recommendFirstPlan.setAuditBy(userInfo.getUserId());
        recommendFirstPlan.setAuditDate(CommonUtil.getyyyyMMdd());
        recommendFirstPlan.setAuditTime(CommonUtil.getHHmmss());
        recommendFirstPlanMapper.updateByPrimaryKey(recommendFirstPlan);
        return null;
    }

    @Override
    public Object stop(GetLoginOutData userInfo, Integer id) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        RecommendFirstPlan recommendFirstPlan = recommendFirstPlanMapper.selectByPrimaryKey(Long.parseLong(id + ""));
        if (recommendFirstPlan == null) {
            throw new BusinessException("查无此数据");
        }
        if (!recommendFirstPlan.getStatus().equals("1")) {
            throw new BusinessException("只能对处于已审核状态的数据进行停用！");
        }
        //2表示已停用
        recommendFirstPlan.setStatus("2");
        recommendFirstPlanMapper.updateByPrimaryKey(recommendFirstPlan);
        return null;
    }

    @Override
    public Object copy(GetLoginOutData userInfo, Integer id) {
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        //查询原始数据，进行拷贝元数据准备
        RecommendFirstPlan recommendFirstPlanDb = recommendFirstPlanMapper.selectByPrimaryKey(Long.parseLong(id + ""));
        if (recommendFirstPlanDb == null) {
            throw new BusinessException("查无此数据");
        }
        Long planId = recommendFirstPlanDb.getId();
        Example exampleSto = new Example(RecommendFirstPalnSto.class);
        exampleSto.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("planId", planId);
        List<RecommendFirstPalnSto> palnStosDb = recommendFirstPalnStoMapper.selectByExample(exampleSto);

        Example exampleProduct = new Example(RecommendFirstPlanProduct.class);
        exampleProduct.createCriteria().andEqualTo("client", userInfo.getClient()).andEqualTo("planId", planId);
        List<RecommendFirstPlanProduct> planProductsDb = recommendFirstPlanProductMapper.selectByExample(exampleSto);

        RecommendFirstPlan recommendFirstPlan = new RecommendFirstPlan();
        BeanUtils.copyProperties(recommendFirstPlanDb, recommendFirstPlan, "id", "auditDate", "auditTime", "auditBy");
        recommendFirstPlan.setPlanName(recommendFirstPlanDb.getPlanName() + "-副本");
        recommendFirstPlan.setCreateBy(userInfo.getUserId());
        recommendFirstPlan.setCreateName(userInfo.getLoginName());
        recommendFirstPlan.setCreateDate(CommonUtil.getyyyyMMdd());
        recommendFirstPlan.setCreateTime(CommonUtil.getHHmmss());
        recommendFirstPlan.setStatus("0");//设置状态为已保存
        recommendFirstPlanMapper.insertUseGeneratedKeys(recommendFirstPlan);
        //获取新的主键id
        Long planIdNew = recommendFirstPlan.getId();

        if (CollectionUtil.isNotEmpty(palnStosDb)) {
            List<RecommendFirstPalnSto> palnStos = new ArrayList<>();
            palnStosDb.forEach(sto -> {
                RecommendFirstPalnSto target = new RecommendFirstPalnSto();
                BeanUtils.copyProperties(sto, target, "id");
                target.setPlanId(planIdNew);
                palnStos.add(target);
            });
            recommendFirstPalnStoMapper.insertList(palnStos);
        }

        if (CollectionUtil.isNotEmpty(planProductsDb)) {
            List<RecommendFirstPlanProduct> planProducts = new ArrayList<>();
            planProductsDb.forEach(product -> {
                RecommendFirstPlanProduct target = new RecommendFirstPlanProduct();
                BeanUtils.copyProperties(product, target, "id");
                target.setPlanId(planIdNew);
                planProducts.add(target);
            });
            recommendFirstPlanProductMapper.insertList(planProducts);
        }
        return null;
    }

    @Override
    public PageInfo<RecommendFirstPlanProductRes> chooseProduct(GetLoginOutData userInfo, RecommendChooseProductInData inData) {
        List<RecommendFirstPlanProductRes> resList = new ArrayList<>();
        if (userInfo == null) {
            throw new BusinessException("请重新登陆");
        }
        if (StrUtil.isBlank(inData.getContent())) {
            throw new BusinessException("请输入查询条件");
        }
        if (inData.getPageSize() == null) {
            inData.setPageSize(100);
        }
        if (inData.getPageNum() == null) {
            inData.setPageNum(1);
        }
        PageInfo pageInfo;
        List<RecommendFirstPlanProductRes> recommendFirstPlanProductResDb = recommendFirstPlanProductMapper.selectProductInfo(userInfo.getClient(), inData.getContent(), null);
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        if (ObjectUtil.isNotEmpty(recommendFirstPlanProductResDb)) {
            resList = recommendFirstPlanProductResDb;
            //处理返回时可能要处理的转换工作
            pageInfo = new PageInfo(resList);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }


    //============================  获取列表（分页）  ==========================
}
