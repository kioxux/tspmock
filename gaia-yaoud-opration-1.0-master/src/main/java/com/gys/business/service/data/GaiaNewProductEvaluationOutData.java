package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class GaiaNewProductEvaluationOutData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "门店")
    private String storeId;
    @ApiModelProperty(value = "品项数")
    private int qty;
    @ApiModelProperty(value = "销售额")
    private BigDecimal salesAmt;
    @ApiModelProperty(value = "毛利额")
    private BigDecimal gross;
    @ApiModelProperty(value = "单据号")
    private String billCode;
    @ApiModelProperty(value = "单据日期")
    private Date billDate;
    @ApiModelProperty(value = "建议转正常数量")
    private int nQty;
    @ApiModelProperty(value = "建议淘汰数量")
    private int eQty;
    @ApiModelProperty(value = "确认转正常数量")
    private int nCQty;
    @ApiModelProperty(value = "确认淘汰数量")
    private int eCQty;
    @ApiModelProperty(value = "页码")
    private int pageNum;
    @ApiModelProperty(value = "每页大小")
    private int pageSize;
    @ApiModelProperty(value = "最后日期")
    private String lastDate;
    @ApiModelProperty(value = "门店类型 0：直营 1 是非直营")
    private String type;
    private String id;
    private int sendRange;
    private int ttQty;
    private int zcQty;
    @ApiModelProperty(value = "明细")
    private List<GaiaProductEvaluationDetailOutData> detailList;

}
