package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Maps;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.IGaiaBillOfApService;
import com.gys.business.service.ReportFormsService;
import com.gys.business.service.data.ReportForms.SupplierDealInData;
import com.gys.business.service.data.ReportForms.SupplierDealOutData;
import com.gys.business.service.data.billOfAp.GaiaBillOfApDto;
import com.gys.business.service.data.billOfAp.GaiaBillOfApExcelDto;
import com.gys.business.service.data.billOfAp.GaiaBillOfApParam;
import com.gys.business.service.data.billOfAp.GaiaBillOfApResponse;
import com.gys.common.data.GaiaBillOfApData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.FiApPaymentEnum;
import com.gys.common.exception.BusinessException;
import com.gys.util.DateUtil;
import com.gys.util.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;


/**
 * <p>
 * 供应商应付明细清单表 服务实现类
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class GaiaBillOfApServiceImpl implements IGaiaBillOfApService {
    @Autowired
    private GaiaBillOfApMapper billOfApMapper;
    @Autowired
    private GaiaPaymentApplicationsMapper gaiaPaymentApplicationsMapper;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    private GaiaDepDataMapper gaiaDepDataMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaFiApPaymentMapper gaiaFiApPaymentMapper;
    @Resource
    private GaiaFiApBalanceMapper gaiaFiApBalanceMapper;

    @Autowired
    private ReportFormsService reportFormsService;

    @Autowired
    private ComeAndGoManageMapper comeAndGoManageMapper;
    @Autowired
    private GaiaMaterialDocMapper materialDocMapper;

    /**
     * 功能描述: 供应商应付明细列表
     *
     * @param gaiaBillOfApParam
     * @return com.gys.common.data.PageInfo
     * @author wangQc
     * @date 2021/9/6 9:44 上午
     */
    @Override
    public PageInfo<GaiaBillOfAp> detailList(GaiaBillOfApParam gaiaBillOfApParam, GetLoginOutData userInfo) {
        if (StringUtils.isEmpty(gaiaBillOfApParam.getDepId()) && StringUtils.isEmpty(gaiaBillOfApParam.getStoCode())) {
            throw new BusinessException("仓库地点和门店编码必选其一");
        }

        if (Objects.nonNull(gaiaBillOfApParam.getPageNum()) && Objects.nonNull(gaiaBillOfApParam.getPageSize())) {
            PageHelper.startPage(gaiaBillOfApParam.getPageNum(), gaiaBillOfApParam.getPageSize());
        }


        Map<String, String> siteNameMap = new HashMap<>();

        if (StrUtil.isNotBlank(gaiaBillOfApParam.getDepId())) {
            List<Map<String, String>> dcList = comeAndGoManageMapper.getDcListByClient(userInfo.getClient());
            if (CollectionUtil.isNotEmpty(dcList)) {
                dcList.forEach(x -> {
                    Map<String, String> resource = x;
                    siteNameMap.put(resource.get("DC_CODE"), resource.get("DC_NAME"));
                });
            }
        }

        if (StrUtil.isNotBlank(gaiaBillOfApParam.getStoCode())) {
            List<StoreOutData> storeList = this.gaiaStoreDataMapper.getStoreList(userInfo.getClient(), "");
            if (CollectionUtil.isNotEmpty(storeList)) {
                storeList.forEach(x -> {
                    siteNameMap.put(x.getStoCode(), StrUtil.isBlank(x.getStoShortName()) ? x.getStoName() : x.getStoShortName());
                });
            }
        }


        List<GaiaBillOfAp> gaiaBillOfApList = this.getBillList(gaiaBillOfApParam);

        Map<String, GaiaFiApPayment> paymentMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(gaiaBillOfApList)) {
            List<String> paymentIds = new ArrayList<>();
            gaiaBillOfApList.forEach(x -> {
                paymentIds.add(x.getPaymentId());
            });
            Example fiApPaymentExample = Example.builder(GaiaFiApPayment.class).build();
            Example.Criteria criteria = fiApPaymentExample.createCriteria();
            criteria.andEqualTo("client", userInfo.getClient())
                    .andIn("paymentId", paymentIds);
            fiApPaymentExample.setOrderByClause(" PAYMENT_CRE_DATE DESC");
            List<GaiaFiApPayment> gaiaFiApPayments = gaiaFiApPaymentMapper.selectByExample(fiApPaymentExample);
            if (CollectionUtil.isNotEmpty(gaiaFiApPayments)) {
                gaiaFiApPayments.forEach(x -> {
                    paymentMap.put(x.getPaymentId(), x);
                });
            }
            gaiaBillOfApList.forEach(x -> {
                if (paymentMap.get(x.getPaymentId()) != null) {
                    x.setPaymentId(x.getPaymentId() + "-" + paymentMap.get(x.getPaymentId()).getPaymentName());
                }
                if (siteNameMap.get(x.getDcCode()) != null) {
                    x.setDcCode(x.getDcCode() + "-" + siteNameMap.get(x.getDcCode()));
                }
                if (Objects.equals(x.getPaymentId(), "9999")) {
                    x.setPaymentId(x.getPaymentId() + "-" + "期间发生额");
                } else if (Objects.equals(x.getPaymentId(), "8888")) {
                    x.setPaymentId(x.getPaymentId() + "-" + "期初金额");
                } else if (Objects.equals(x.getPaymentId(), "6666")) {
                    x.setPaymentId(x.getPaymentId() + "-" + "期间付款");
                }
            });
        }
        GaiaBillOfAp total = handleTotalBillOfAp(gaiaBillOfApList);
        if (ObjectUtil.isEmpty(gaiaBillOfApList)) {
            return new PageInfo<>();
        }
        PageInfo pageInfo = new PageInfo<>(gaiaBillOfApList);
        pageInfo.setListNum(total);
        return pageInfo;
    }

    private GaiaBillOfAp handleTotalBillOfAp(List<GaiaBillOfAp> gaiaBillOfApList) {
        GaiaBillOfAp res = new GaiaBillOfAp();

        BigDecimal amountOfPaymentTotal = BigDecimal.ZERO;
        if (CollectionUtil.isNotEmpty(gaiaBillOfApList)) {
            for (GaiaBillOfAp ap : gaiaBillOfApList) {
                amountOfPaymentTotal = amountOfPaymentTotal.add(ap.getAmountOfPayment().setScale(2,BigDecimal.ROUND_HALF_UP));
            }
        }
        res.setAmountOfPayment(amountOfPaymentTotal);
        return res;
    }


    /**
     * 功能描述: 供应商应付明细列表
     *
     * @param gaiaBillOfApParam
     * @return org.apache.poi.xssf.streaming.SXSSFWorkbook
     * @author wangQc
     * @date 2021/9/6 2:22 下午
     */
    @Override
    public SXSSFWorkbook detailListExport(GaiaBillOfApParam gaiaBillOfApParam, GetLoginOutData userInfo) {
        PageInfo<GaiaBillOfAp> pageInfo = this.detailList(gaiaBillOfApParam, userInfo);
        List<GaiaBillOfAp> data = pageInfo.getList();
        GaiaBillOfAp total = pageInfo.getListNum();
        data.add(total);
        if (CollectionUtils.isEmpty(data)) {
            throw new BusinessException("提示：导出失败, 无数据");
        }

        // 获取数据
        SXSSFWorkbook workbook = new SXSSFWorkbook(data.size() + 100);
        Sheet sheet = workbook.createSheet("供应商应付明细列表");


        String[] title = new String[]{"序号", "发生地", "供应商编码","业务员","应付方式编码","结算金额", "发生日期", "备注"};
        //设置标题
        // 设置表头
        Row titleRow = sheet.createRow(0);
        int i = 0;
        for (String key : title) {
            Cell cell = titleRow.createCell(i);
            cell.setCellValue(key);
            i++;
        }

        for (int m = 1; m <= data.size(); m++) {
            Row dataRow = sheet.createRow(m);
            // 列数
            int increase = 0;

            GaiaBillOfAp gaiaBillOfAp = data.get(m - 1);
            if (m == data.size()) {
                //序号
                Cell cell = dataRow.createCell(increase++);
                cell.setCellValue("合计");

                //发生地
                cell = dataRow.createCell(increase++);
                cell.setCellValue("");
                //供应商编码
                cell = dataRow.createCell(increase++);
                cell.setCellValue("");
                //业务员
                cell = dataRow.createCell(increase++);
                cell.setCellValue("");
                //应付方式编码
                cell = dataRow.createCell(increase++);
                cell.setCellValue("");
                //结算金额
                cell = dataRow.createCell(increase++);
                //cell.setCellValue(ExcelUtils.tran(gaiaBillOfAp.getAmountOfPaymentStr()));
                cell.setCellValue(new BigDecimal(gaiaBillOfAp.getAmountOfPaymentStr()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
                //发生日期
                cell = dataRow.createCell(increase++);
                cell.setCellValue("");
                //备注
                cell = dataRow.createCell(increase++);
                cell.setCellValue("");
            } else {
                //序号
                Cell cell = dataRow.createCell(increase++);
                cell.setCellValue(m);

                //发生地
                cell = dataRow.createCell(increase++);
                cell.setCellValue(ExcelUtils.tran(gaiaBillOfAp.getDcCode()));
                //供应商编码
                cell = dataRow.createCell(increase++);
                //cell.setCellValue(ExcelUtils.tran(gaiaBillOfAp.getSupSelfCode()));
                cell.setCellValue(ExcelUtils.tran(gaiaBillOfAp.getSupSelfCode()+"-"+gaiaBillOfAp.getSupName()));
                //业务员
                cell = dataRow.createCell(increase++);
                cell.setCellValue(Objects.isNull(gaiaBillOfAp.getSalesmanId())?"" : gaiaBillOfAp.getSalesmanId()+"-"+gaiaBillOfAp.getSalesmanName());
                //应付方式编码
                cell = dataRow.createCell(increase++);
                cell.setCellValue(ExcelUtils.tran(gaiaBillOfAp.getPaymentId()));
                //结算金额
                cell = dataRow.createCell(increase++);
                //cell.setCellValue(ExcelUtils.tran(gaiaBillOfAp.getAmountOfPaymentStr()));
                cell.setCellValue(new BigDecimal(gaiaBillOfAp.getAmountOfPaymentStr()).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());

                //发生日期
                cell = dataRow.createCell(increase++);
                //cell.setCellValue(ExcelUtils.tran(gaiaBillOfAp.getPaymentDate()));
                cell.setCellValue(Double.valueOf(gaiaBillOfAp.getPaymentDate()));
                //备注
                cell = dataRow.createCell(increase++);
                cell.setCellValue(ExcelUtils.tran(gaiaBillOfAp.getRemarks()));
            }

        }
        //自适应列宽
        ExcelUtils.setSizeColumn(sheet, title.length);
        return workbook;
    }


    /**
     * 功能描述: 供应商应付明细清单表查询（分页）
     *
     * @param gaiaBillOfApParam
     * @return com.gys.common.data.PageInfo<java.lang.Object>
     * @author wangQc
     * @date 2021/8/31 10:14 上午
     */
    @Override
    public PageInfo<LinkedHashMap<String, Object>> getListPage(GaiaBillOfApParam gaiaBillOfApParam, GetLoginOutData userInfo) {
        //定义回参
        List<Map<String, Object>> resList = new ArrayList<>();

        List<GaiaBillOfAp> gaiaBillOfApList = this.getBillList(gaiaBillOfApParam);

        Map<String, String> gaiaBillOfApMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(gaiaBillOfApList)) {
            gaiaBillOfApList.forEach(x -> {
                gaiaBillOfApMap.put(x.getSupSelfCode(), x.getSupName());
            });
        }

        Map<String, String> siteNameMap = new HashMap<>();

        if (StrUtil.isNotBlank(gaiaBillOfApParam.getDepId())) {
            List<Map<String, String>> dcList = comeAndGoManageMapper.getDcListByClient(userInfo.getClient());
            if (CollectionUtil.isNotEmpty(dcList)) {
                dcList.forEach(x -> {
                    Map<String, String> resource = x;
                    siteNameMap.put(resource.get("DC_CODE"), resource.get("DC_NAME"));
                });
            }
        }

        if (StrUtil.isNotBlank(gaiaBillOfApParam.getStoCode())) {
            List<StoreOutData> storeList = this.gaiaStoreDataMapper.getStoreList(userInfo.getClient(), "");
            if (CollectionUtil.isNotEmpty(storeList)) {
                storeList.forEach(x -> {
                    siteNameMap.put(x.getStoCode(), StrUtil.isBlank(x.getStoShortName()) ? x.getStoName() : x.getStoShortName());
                });
            }
        }

        /*LocalDate localDate = LocalDate.now();
        System.out.println("LocalDate = " + localDate);*/

        Calendar c = Calendar.getInstance();
        Date date = Date.from(gaiaBillOfApParam.getBeginTime().atStartOfDay(ZoneId.systemDefault()).toInstant());
        //date = new SimpleDateFormat("yy-MM-dd")
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day - 1);
        //获取前一天
        String endTime = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        String startTime = "";
        if (StrUtil.isNotBlank(endTime)) {
            String[] split = endTime.split("-");
            if (ArrayUtil.isNotEmpty(split)) {
                startTime = split[0] + split[1] + "01";
            }
        }


        List<GaiaBillOfAp> ofApList = billOfApMapper.getSelectBillOfList(gaiaBillOfApParam, startTime, Convert.toStr(endTime));
        if (ObjectUtil.isEmpty(gaiaBillOfApList)) {
            if (CollUtil.isEmpty(ofApList)) {
                return new PageInfo<>();
            }

        }

        //查询出所有的应付类型
        Map<String, String> gaiaFiApPaymentMap = getFiApPayment(gaiaBillOfApParam);

        //组装数据
        //根据client sup de_code分组
        Map<String, List<GaiaBillOfAp>> map = gaiaBillOfApList.stream().collect(
                Collectors.groupingBy(
                        gaiaBillOfAp -> gaiaBillOfAp.getClient() + '-' + gaiaBillOfAp.getSupSelfCode() + '-' + gaiaBillOfAp.getDcCode()
                )
        );


        //计算当月第一天 和  beginTime的前一天 的总金额
        //例：beginTime = 2021-07-15  则查询6月的余额 + 2021-07-01~2021-07-14 期间的金额sum
        Map<String, List<GaiaBillOfAp>> beginPeriodBillOfApMap = calculateBeforeBeginTimePeriod(gaiaBillOfApParam);

        //拿到beginTime的上一个月
        LocalDate preMonth = DateUtil.getPreMonth(gaiaBillOfApParam.getBeginTime());
        //获取上个月最后一天
        String lastDay = DateUtil.getLastDay(preMonth.getYear(), preMonth.getMonthValue());
       /* String.valueOf(preMonth.getYear())
        String.valueOf(preMonth.getMonthValue())*/
        //获取期初
        Map<String, GaiaFiApBalance> balanceMap = new HashMap<>();
        String client = gaiaBillOfApParam.getClient();
        List<GaiaFiApBalance> balanceList = this.gaiaFiApBalanceMapper.getbeginPeriodBillOfAp(client,String.valueOf(preMonth.getYear()), String.valueOf(preMonth.getMonthValue()));
        if (CollUtil.isNotEmpty(balanceList)) {
            balanceMap = balanceList.stream().collect(Collectors.toMap(s -> s.getClient() + s.getDcCode() + s.getSupSelfCode(), balan -> balan, (k, v) -> v));
        }
        //获取所有供应商期初
        Map<String, GaiaBillOfAp> ofApMap = new HashMap<>();
        List<GaiaBillOfAp> billOfApList =  this.billOfApMapper.getbeginPeriodAmt(client);
        if(CollUtil.isNotEmpty(billOfApList)){
             ofApMap = billOfApList.stream().collect(Collectors.toMap(s -> s.getClient() + s.getDcCode() + s.getSupSelfCode(), balan -> balan, (k, v) -> v));
        }
        List<GaiaBillOfAp> gaiaBillOfAps  = billOfApMapper.getList(client, lastDay);
        Map<String, List<GaiaBillOfAp>> billOfApGroupBy = new HashMap<>();
        if(CollUtil.isNotEmpty(gaiaBillOfAps)){
            billOfApGroupBy = gaiaBillOfAps.stream().collect(Collectors.groupingBy(s -> s.getClient() + s.getDcCode() + s.getSupSelfCode()));

        }
        //遍历外层维度 map
        for (Map.Entry<String, List<GaiaBillOfAp>> entry : map.entrySet()) {
            LinkedHashMap<String, Object> resultMap = Maps.newLinkedHashMap();
            //client 供应商 发生地
            List<GaiaBillOfAp> value = entry.getValue();
            //获取第一条
            GaiaBillOfAp gaiaBillOfAp = value.get(0);
            resultMap.put("client", gaiaBillOfAp.getClient());
            resultMap.put("supSelfCode", gaiaBillOfAp.getSupSelfCode());
            if (siteNameMap.get(gaiaBillOfAp.getDcCode()) != null) {
                resultMap.put("门店编码", gaiaBillOfAp.getDcCode() + "-" + siteNameMap.get(gaiaBillOfAp.getDcCode()));
            }
            if (gaiaBillOfApMap.get(gaiaBillOfAp.getSupSelfCode()) != null) {
                resultMap.put("供应商编码", gaiaBillOfAp.getSupSelfCode() + "-" + gaiaBillOfApMap.get(gaiaBillOfAp.getSupSelfCode()));
            }


            //计算期初
            BigDecimal beginPeriodAmount = this.calculateBeginPeriod(balanceMap,ofApMap,billOfApGroupBy,gaiaBillOfApParam, gaiaBillOfAp.getDcCode(), gaiaBillOfAp.getSupSelfCode(), beginPeriodBillOfApMap);

            //resultMap.put("期初金额", Convert.toStr(beginPeriodAmount));
            resultMap.put("期初金额", beginPeriodAmount);

            //组装其他应付方式
            this.differentPayment(gaiaFiApPaymentMap, resultMap, value, beginPeriodAmount);

            resList.add(resultMap);
        }
        List<GaiaBillOfAp> ofAps = new ArrayList<>();
        if (CollUtil.isNotEmpty(ofApList)) {
            if (CollUtil.isNotEmpty(resList)) {
                for (GaiaBillOfAp gaiaBillOfAp : ofApList) {
                    resList.forEach(res -> {
                        if (Objects.equals(gaiaBillOfAp.getClient(), res.get("client")) && Objects.equals(gaiaBillOfAp.getSupSelfCode(), res.get("supSelfCode"))) {
                            ofAps.add(gaiaBillOfAp);
                        }
                    });
                }
            }
        }
        ofApList.removeAll(ofAps);
        Map<String, GaiaBillOfApData> periodAmount = getBeginPeriodAmount(gaiaBillOfApParam);
        BigDecimal bigDecimal = new BigDecimal("0.00");
        for (GaiaBillOfAp gaiaBillOfAp : ofApList) {
            //LinkedHashMap<String, Object> resultMap = Maps.newLinkedHashMap();
            LinkedHashMap<String, Object> lsMap = new LinkedHashMap<>();
            //BigDecimal beginPeriodAmount = this.calculateBeginPeriod(gaiaBillOfApParam, gaiaBillOfAp.getDcCode(), gaiaBillOfAp.getSupSelfCode(), calculateBeforeBeginTimePeriod(gaiaBillOfApParam));
            //获取期初金额
            BigDecimal beginPeriodAmount = BigDecimal.ZERO;
            lsMap.put("门店编码", gaiaBillOfAp.getDcCode() + "-" + siteNameMap.get(gaiaBillOfAp.getDcCode()));
            lsMap.put("供应商编码", gaiaBillOfAp.getSupSelfCode() + "-" + gaiaBillOfAp.getSupName());
            if(periodAmount.containsKey(gaiaBillOfAp.getClient()+gaiaBillOfAp.getDcCode()+gaiaBillOfAp.getSupSelfCode())){
                GaiaBillOfApData ofApData = periodAmount.get(gaiaBillOfAp.getClient() + gaiaBillOfAp.getDcCode() + gaiaBillOfAp.getSupSelfCode());
                if(Objects.nonNull(ofApData)){
                    beginPeriodAmount = beginPeriodAmount.add(new BigDecimal(ofApData.getAmountOfPayment()));
                }
            }
            lsMap.put("期初金额", beginPeriodAmount);
            lsMap.put("期间发生额",bigDecimal);
            lsMap.put("去税成本额",bigDecimal);
            lsMap.put("税额",bigDecimal);
            lsMap.put("期间付款",bigDecimal);
            Map<String, String> paymentMap = getPay(gaiaBillOfApParam);
            if(CollUtil.isNotEmpty(paymentMap)){
                for (Map.Entry<String, String> entry : paymentMap.entrySet()) {
                    lsMap.put(entry.getValue(),bigDecimal);
                }
            }
            lsMap.put("期末余额",beginPeriodAmount);
            resList.add(lsMap);
        }
        PageInfo pageInfo = new PageInfo(resList);
        LinkedHashMap<String, Object> total = getTotal(resList);
        pageInfo.setListNum(total);
        return pageInfo;

    }

    /**
     * 获取起初金额
     * @param gaiaBillOfAp
     * @return
     */
    private Map<String, GaiaBillOfApData> getBeginPeriodAmount(GaiaBillOfApParam gaiaBillOfApParam){
        String beginTime = DateUtil.formatDate(gaiaBillOfApParam.getBeginTime());
        List<GaiaBillOfApData> beginAmount = this.billOfApMapper.getBeginAmount(gaiaBillOfApParam, beginTime,StrUtil.isNotBlank(gaiaBillOfApParam.getDepId())?gaiaBillOfApParam.getDepId():gaiaBillOfApParam.getStoCode());
        Map<String, GaiaBillOfApData> dataMap = new HashMap<>();
        if(CollUtil.isNotEmpty(beginAmount)){
             dataMap = beginAmount.stream().collect(Collectors.toMap(s -> s.getClient() + s.getDcCode() + s.getSupSelfCode(), begin -> begin, (k, v) -> v));
        }
        return dataMap;
    }

    private LinkedHashMap<String, Object> getTotal(List<Map<String, Object>> resList) {
        LinkedHashMap<String, Object> total = new LinkedHashMap<>();
        total.put("合计","合计");
        if (CollectionUtil.isNotEmpty(resList)) {
            for (Map<String, Object> single : resList) {

                for (String key : single.keySet()) {


               /* for (Map.Entry<String, Object> entry : single.entrySet()) {
                    if (Objects.equals(entry.getKey(), "期初金额")) {
                        if (total.containsKey("期初金额")) {
                            total.put("期初金额", new BigDecimal(Convert.toStr(total.get("期初金额"))).add(new BigDecimal(Convert.toStr(entry.getValue()))));
                        } else {
                            total.put("期初金额", StrUtil.isBlank(Convert.toStr(entry.getValue()))?"0.00":new BigDecimal(Convert.toStr(entry.getValue())));
                        }
                    }
                    if (Objects.equals(entry.getKey(), "去税成本额")) {
                        if (total.containsKey("去税成本额")) {
                            total.put("去税成本额", new BigDecimal(Convert.toStr(total.get("去税成本额"))).add(new BigDecimal(Convert.toStr(entry.getValue()))));
                        } else {
                            total.put("去税成本额", StrUtil.isBlank(Convert.toStr(entry.getValue()))?"0.00":new BigDecimal(Convert.toStr(entry.getValue())));
                        }
                    }
                    if (Objects.equals(entry.getKey(), "期末余额")) {
                        if (total.containsKey("期末余额")) {
                            total.put("期末余额", new BigDecimal(Convert.toStr(total.get("期末余额"))).add(new BigDecimal(Convert.toStr(entry.getValue()))));
                        } else {
                            total.put("期末余额", StrUtil.isBlank(Convert.toStr(entry.getValue()))?"0.00":new BigDecimal(Convert.toStr(entry.getValue())));
                        }
                    }

                    if (Objects.equals(entry.getKey(), "期间付款")) {
                        if (total.containsKey("期间付款")) {
                            total.put("期间付款", new BigDecimal(Convert.toStr(total.get("期间付款"))).add(new BigDecimal(Convert.toStr(entry.getValue()))));
                        } else {
                            total.put("期间付款", StrUtil.isBlank(Convert.toStr(entry.getValue()))?"0.00":new BigDecimal(Convert.toStr(entry.getValue())));
                        }
                    }

                    if (Objects.equals(entry.getKey(), "期间发生额")) {
                        if (total.containsKey("期间发生额")) {
                            total.put("期间发生额", new BigDecimal(Convert.toStr(total.get("期间发生额"))).add(new BigDecimal(Convert.toStr(entry.getValue()))));
                        } else {
                            total.put("期间发生额", StrUtil.isBlank(Convert.toStr(entry.getValue()))?"0.00":new BigDecimal(Convert.toStr(entry.getValue())));
                        }
                    }

                    if (Objects.equals(entry.getKey(), "税额")) {
                        if (total.containsKey("税额")) {
                            total.put("税额", new BigDecimal(Convert.toStr(total.get("税额"))).add(new BigDecimal(Convert.toStr(entry.getValue()))));
                        } else {
                            total.put("税额", StrUtil.isBlank(Convert.toStr(entry.getValue()))?"0.00":new BigDecimal(Convert.toStr(entry.getValue())));
                        }
                    }
                }
            }*/
                    Object value = single.get(key);
                    String reg = "-?[0-9]+.?[0-9]{0,}";
                    boolean matches = Convert.toStr(value).matches(reg);
                    if ("门店编码".equals(key) || "供应商编码".equals(key)) {
                        total.put(key, "");
                    } else {
                        if (total.get(key) != null) {
                            if (value instanceof BigDecimal) {
                                if (matches) {
                                    BigDecimal oldValue = new BigDecimal(Convert.toStr(total.get(key)));
                                    oldValue = oldValue.add((BigDecimal) value);
                                    total.put(key, oldValue);
                                }
                            } else if (value instanceof String) {
                                if (matches) {
                                    BigDecimal temp = BigDecimal.ZERO;
                                    if (StrUtil.isNotBlank(Convert.toStr( value))) {
                                        temp = new BigDecimal(Convert.toStr(value));
                                    } else {
                                        temp = BigDecimal.ZERO;
                                    }
                                    BigDecimal oldValue = BigDecimal.ZERO;
                                    if (StrUtil.isNotBlank(Convert.toStr(total.get(key)))) {
                                        oldValue = new BigDecimal(Convert.toStr(total.get(key)));
                                    }
                                    temp = temp.add(oldValue);
                                    total.put(key, Convert.toStr(temp));
                                }
                            }
                        } else {
                            total.put(key, value);
                        }
                    }
                }


            }
        }
        return total;
    }

    /**
     * 正则表达式   校验 小数 负数 小数
     *
     * @param args
     */
    public static void main(String[] args) {
        boolean numeric = StringUtils.isNumeric("1");
        String str = "-1.0999";
        // boolean matches = number.matches("-?[0-9]+(\\\\.[0-9]+)?");

     /*   Pattern pattern = Pattern.compile("[0-9]*");
        boolean matches = pattern.matcher(str).matches();*/
        // System.out.println(matches);
        String reg = "-?[0-9]+.?[0-9]{0,}";
        boolean matches = str.matches(reg);
        System.out.println(matches);
    }


    /**
     * 功能描述: 查询出所有的应付类型
     *
     * @param gaiaBillOfApParam
     * @return java.util.Map<java.lang.String, java.lang.String>
     * @author wangQc
     * @date 2021/8/31 5:04 下午
     */
    private Map<String, String> getFiApPayment(GaiaBillOfApParam gaiaBillOfApParam) {
        Example paymentIdExample = Example.builder(GaiaBillOfAp.class).build();
        Example.Criteria paymentIdCriteria = paymentIdExample.createCriteria();
        paymentIdCriteria.andEqualTo("client", gaiaBillOfApParam.getClient());
        paymentIdExample.setOrderByClause("PAYMENT_CRE_DATE desc");
        List<GaiaFiApPayment> gaiaFiApPaymentList = gaiaFiApPaymentMapper.selectByExample(paymentIdExample);

        Map<String, String> gaiaFiApPaymentMap = Maps.newLinkedHashMap();
        if (CollectionUtils.isNotEmpty(gaiaFiApPaymentList)) {
            gaiaFiApPaymentMap = gaiaFiApPaymentList.stream()
                    .collect(Collectors.toMap(GaiaFiApPayment::getPaymentId, GaiaFiApPayment::getPaymentName, (k1, k2) -> k1));
        }
        return gaiaFiApPaymentMap;
    }

    private Map<String, String> getPay(GaiaBillOfApParam gaiaBillOfApParam) {
        Example paymentIdExample = Example.builder(GaiaBillOfAp.class).build();
        Example.Criteria paymentIdCriteria = paymentIdExample.createCriteria();
        paymentIdCriteria.andEqualTo("client", gaiaBillOfApParam.getClient());
        List<GaiaFiApPayment> gaiaFiApPaymentList = gaiaFiApPaymentMapper.selectByExample(paymentIdExample);

        Map<String, String> gaiaFiApPaymentMap =new LinkedHashMap<>();
        if (CollectionUtils.isNotEmpty(gaiaFiApPaymentList)) {
            for (GaiaFiApPayment payment : gaiaFiApPaymentList) {
                gaiaFiApPaymentMap.put(payment.getPaymentId(),payment.getPaymentName());
            }
        }
        return gaiaFiApPaymentMap;
    }


    /**
     * 功能描述: 组装其他应付方式
     *
     * @param gaiaFiApPaymentMap
     * @param resultMap
     * @param value
     * @param endAmount
     * @return void
     * @author wangQc
     * @date 2021/8/31 5:03 下午
     */
    private void differentPayment(Map<String, String> gaiaFiApPaymentMap, Map<String, Object> resultMap, List<GaiaBillOfAp> value, BigDecimal endAmount) {
        //拿 client 供应商 发生地 维度的集合 根据应付方式分组
        Map<String, List<GaiaBillOfAp>> paymentIdMap = value.stream().filter(s->!Objects.equals("8888",s.getPaymentId())).collect(Collectors.groupingBy(GaiaBillOfAp::getPaymentId));
        BigDecimal bigDecimal = new BigDecimal("0.00");
        //先遍历应付方式 占位
        resultMap.put("期间发生额",bigDecimal);
        resultMap.put("去税成本额",bigDecimal);
        resultMap.put("税额",bigDecimal);
        resultMap.put("期间付款",bigDecimal);


        for (String paymentName : gaiaFiApPaymentMap.values()) {
            resultMap.put(paymentName,bigDecimal);
        }

        //遍历 应付方式分组后的map
        for (Map.Entry<String, List<GaiaBillOfAp>> paymentIdMapEntry : paymentIdMap.entrySet()) {
            List<GaiaBillOfAp> sonValue = paymentIdMapEntry.getValue();

            //计算出所有的 AMOUNT_OF_PAYMENT 字段的总和
            BigDecimal sumAmount = sonValue.stream()
                    .map(s->s.getAmountOfPayment().setScale(2,BigDecimal.ROUND_HALF_UP))
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            //如果是 期间发生额（9999）
            if (FiApPaymentEnum.BILL.getCode().equals(paymentIdMapEntry.getKey())) {
                //计算 去税成本 税额
                BigDecimal sumAmountExcludingTax = sonValue.stream()
                        .map(GaiaBillOfAp::getAmountExcludingTax)
                        .filter(Objects::nonNull)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                BigDecimal sumAmountOfTax = sonValue.stream()
                        .map(GaiaBillOfAp::getAmountOfTax)
                        .filter(Objects::nonNull)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                resultMap.put("期间发生额", sumAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
                resultMap.put("去税成本额", sumAmountExcludingTax);
                resultMap.put("税额", sumAmountOfTax);

                //加上期间发生
                endAmount = endAmount.add(sumAmount);
            }
            //期间付款
            else if (FiApPaymentEnum.PAYED.getCode().equals(paymentIdMapEntry.getKey())) {
                resultMap.put("期间付款", sumAmount);
                //减去
                endAmount = endAmount.add(sumAmount);
            } else {
                //减去其他
                endAmount = endAmount.add(sumAmount);
            }

            String paymentName = gaiaFiApPaymentMap.get(paymentIdMapEntry.getKey());
            if (StringUtils.isNotEmpty(paymentName)) {
                resultMap.put(paymentName, sumAmount);
            }
        }

        resultMap.put("期末余额", endAmount.setScale(2,BigDecimal.ROUND_HALF_UP));
    }


    /**
     * 功能描述: 根据条件查询bill主表
     *
     * @param gaiaBillOfApParam
     * @return java.util.List<com.gys.business.mapper.entity.GaiaBillOfAp>
     * @author wangQc
     * @date 2021/8/31 4:58 下午
     */
    private List<GaiaBillOfAp> getBillList(GaiaBillOfApParam gaiaBillOfApParam) {
        if (StringUtils.isEmpty(gaiaBillOfApParam.getDepId()) && StringUtils.isEmpty(gaiaBillOfApParam.getStoCode())) {
            throw new BusinessException("仓库地点和门店编码必选其一");
        }

        Example billExample = Example.builder(GaiaBillOfAp.class).build();
        Example.Criteria criteria = billExample.createCriteria();
        if (StringUtils.isNotEmpty(gaiaBillOfApParam.getClient())) {
            criteria.andEqualTo("client", gaiaBillOfApParam.getClient());
        }
        //仓库地点
        if (StringUtils.isNotEmpty(gaiaBillOfApParam.getDepId())) {
            criteria.andEqualTo("dcCode", gaiaBillOfApParam.getDepId());
        }
        //门店编码
        if (StringUtils.isNotEmpty(gaiaBillOfApParam.getStoCode())) {
            criteria.andEqualTo("dcCode", gaiaBillOfApParam.getStoCode());
        }
        //门店编码
        if (StringUtils.isNotEmpty(gaiaBillOfApParam.getPaymentId())) {
            criteria.andEqualTo("paymentId", gaiaBillOfApParam.getPaymentId());
        }

        //营业员
        if (gaiaBillOfApParam.getSalesmanNameArray()!=null && gaiaBillOfApParam.getSalesmanNameArray().length>0) {
            List<String> integers = Arrays.asList(gaiaBillOfApParam.getSalesmanNameArray());
            criteria.andIn("salesmanId", integers.stream().map(s->Convert.toStr(s)).collect(Collectors.toList()));
        }
        //供应商编码
        if (StringUtils.isNotEmpty(gaiaBillOfApParam.getSupSelfCode())) {
            criteria.andEqualTo("supSelfCode", gaiaBillOfApParam.getSupSelfCode());
        }
        criteria.andBetween("paymentDate",
                DateUtil.formatDate(gaiaBillOfApParam.getBeginTime()),
                DateUtil.formatDate(gaiaBillOfApParam.getEndTime())
        );
//        criteria.andNotEqualTo("paymentId", FiApPaymentEnum.PERIOD_BEGIN.getCode());

        billExample.setOrderByClause("CLIENT,PAYMENT_DATE desc");
        return billOfApMapper.selectByExample(billExample);
    }

    private BigDecimal calculateBeginPeriod
            (Map<String, GaiaFiApBalance> balanceMap,Map<String, GaiaBillOfAp> ofApMap,Map<String, List<GaiaBillOfAp>> billOfApGroupBy,GaiaBillOfApParam gaiaBillOfApParam, String dcCode, String supSelfCode, Map<String, List<GaiaBillOfAp>> beginPeriodBillOfApMap) {
        //拿到beginTime的上一个月
        LocalDate preMonth = DateUtil.getPreMonth(gaiaBillOfApParam.getBeginTime());
        //获取上个月最后一天
        String lastDay = DateUtil.getLastDay(preMonth.getYear(), preMonth.getMonthValue());

        List<GaiaBillOfAp> gaiaBillOfAps = beginPeriodBillOfApMap.get(supSelfCode + "-" + dcCode);


        //查询上一个月的结算余额
       /* Example fiApBalanceExample = Example.builder(GaiaFiApBalance.class).build();
        Example.Criteria fiApBalanceCriteria = fiApBalanceExample.createCriteria();
        fiApBalanceCriteria.andEqualTo("client", gaiaBillOfApParam.getClient());
        fiApBalanceCriteria.andEqualTo("dcCode", dcCode);
        fiApBalanceCriteria.andEqualTo("supSelfCode", supSelfCode);
        fiApBalanceCriteria.andEqualTo("balanceYear", String.valueOf(preMonth.getYear()));
        fiApBalanceCriteria.andEqualTo("balanceMonth", String.valueOf(preMonth.getMonthValue()));
        fiApBalanceExample.setOrderByClause("CLIENT limit 1");
        GaiaFiApBalance fiApBalance = gaiaFiApBalanceMapper.selectOneByExample(fiApBalanceExample);*/

        GaiaFiApBalance fiApBalance = null;
        if(balanceMap.containsKey(gaiaBillOfApParam.getClient()+dcCode+supSelfCode)){
            fiApBalance= balanceMap.get(gaiaBillOfApParam.getClient() + dcCode + supSelfCode);
        }

        BigDecimal preMonthBalance = BigDecimal.ZERO;

        BigDecimal sumAmount = BigDecimal.ZERO;
        //期初
        BigDecimal beginning = BigDecimal.ZERO;
        //期间发生
        BigDecimal period = BigDecimal.ZERO;
        //期间付款
        BigDecimal payment = BigDecimal.ZERO;
        //其他消费
        BigDecimal rest = BigDecimal.ZERO;
        //上个月的余额
        if (Objects.nonNull(fiApBalance)) {
            preMonthBalance = fiApBalance.getResidualAmount();
        } else {
           /* //查询明细表上个月之前的所有数据
            Example oaAp = new Example(GaiaBillOfAp.class);
            Example.Criteria oaApExa = oaAp.createCriteria();
            oaApExa.andEqualTo("client", gaiaBillOfApParam.getClient());
            oaApExa.andEqualTo("dcCode", dcCode);
            oaApExa.andEqualTo("supSelfCode", supSelfCode);
            oaApExa.andEqualTo("paymentId","8888");*/
            GaiaBillOfAp billOfAp = ofApMap.get(gaiaBillOfApParam.getClient() + dcCode + supSelfCode);
            //List<GaiaBillOfAp> billOfApList = billOfApMapper.selectByExample(oaAp);
            if(Objects.nonNull(billOfAp)){
                //GaiaBillOfAp billOfAp = billOfApList.get(0);
                if (Convert.toLong(billOfAp.getPaymentDate())>=Convert.toLong(DateUtil.formatDate(gaiaBillOfApParam.getBeginTime()))) {
                    billOfAp.setAmountOfPayment(billOfAp.getAmountOfPayment().setScale(2,BigDecimal.ROUND_HALF_UP));
                    beginning = beginning.add(billOfAp.getAmountOfPayment());
                }
            }
            List<GaiaBillOfAp> ofAps = billOfApGroupBy.get(gaiaBillOfApParam.getClient() + dcCode + supSelfCode);
            //List<GaiaBillOfAp> ofAps = billOfApMapper.getList(gaiaBillOfApParam.getClient(), dcCode, supSelfCode, lastDay);
            if (CollUtil.isNotEmpty(ofAps)) {
                for (GaiaBillOfAp gaiaBillOfAp : ofAps) {
                    gaiaBillOfAp.setAmountOfPayment(gaiaBillOfAp.getAmountOfPayment().setScale(2,BigDecimal.ROUND_HALF_UP));
                    if (gaiaBillOfAp.getPaymentId().equals("8888")) {
                        beginning = beginning.add(gaiaBillOfAp.getAmountOfPayment());
                    }
                    if (gaiaBillOfAp.getPaymentId().equals("9999")) {
                        period = period.add(gaiaBillOfAp.getAmountOfPayment());
                    }
                    if (gaiaBillOfAp.getPaymentId().equals("6666")) {
                        payment = payment.add(gaiaBillOfAp.getAmountOfPayment());
                    }
                    if (!(gaiaBillOfAp.getPaymentId().equals("9999")) && !(gaiaBillOfAp.getPaymentId().equals("8888")) && !(gaiaBillOfAp.getPaymentId().equals("6666"))) {
                        rest = rest.add(gaiaBillOfAp.getAmountOfPayment());
                    }
                }
            }
        }
        if (CollUtil.isNotEmpty(gaiaBillOfAps)) {
            for (GaiaBillOfAp gaiaBillOfAp : gaiaBillOfAps) {
                LocalDate firstDay = LocalDate.of(gaiaBillOfApParam.getBeginTime().getYear(), gaiaBillOfApParam.getBeginTime().getMonthValue(), 1);
                if (!(gaiaBillOfApParam.getBeginTime().isEqual(firstDay))) {
                    gaiaBillOfAp.setAmountOfPayment(gaiaBillOfAp.getAmountOfPayment().setScale(2,BigDecimal.ROUND_HALF_UP));


                    if (gaiaBillOfAp.getPaymentId().equals("8888")) {
                        beginning = beginning.add(gaiaBillOfAp.getAmountOfPayment());
                    }
                    if (gaiaBillOfAp.getPaymentId().equals("9999")) {
                        period = period.add(gaiaBillOfAp.getAmountOfPayment());
                    }
                    if (gaiaBillOfAp.getPaymentId().equals("6666")) {
                        payment = payment.add(gaiaBillOfAp.getAmountOfPayment());
                    }
                    if (!(gaiaBillOfAp.getPaymentId().equals("9999")) && !(gaiaBillOfAp.getPaymentId().equals("8888")) && !(gaiaBillOfAp.getPaymentId().equals("6666"))) {
                        rest = rest.add(gaiaBillOfAp.getAmountOfPayment());
                    }

                }
            }
        }
        BigDecimal residualAmount = beginning.add(period).add(payment).add(rest);
       // sumAmount = residualAmount;
        //加上当月 beginTime前 几天的金额总和
        return preMonthBalance.add(residualAmount);
    }


    /**
     * 功能描述: 计算当月第一天 和  beginTime的前一天 的单据记录
     * 例：beginTime = 2021-07-15  则查询6月的余额 + 2021-07-01~2021-07-14 期间的金额sum
     *
     * @param gaiaBillOfApParam
     * @return java.util.Map<java.lang.String, java.util.List < com.gys.business.mapper.entity.GaiaBillOfAp>>
     * @author wangQc
     * @date 2021/9/1 4:26 下午
     */
    private Map<String, List<GaiaBillOfAp>> calculateBeforeBeginTimePeriod(GaiaBillOfApParam gaiaBillOfApParam) {

        Calendar c = Calendar.getInstance();
        Date date = Date.from(gaiaBillOfApParam.getBeginTime().atStartOfDay(ZoneId.systemDefault()).toInstant());
        //date = new SimpleDateFormat("yy-MM-dd")
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day - 1);
        //获取前一天
        String endTime = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());

        LocalDate startDate = LocalDate.parse(endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        //拿到当月的第一天 beginTime的前一天
        LocalDate firstDay = LocalDate.of(startDate.getYear(), startDate.getMonthValue(), 1);
       /* LocalDate lastDay = firstDay;
        if (firstDay.isAfter(gaiaBillOfApParam.getBeginTime())) {
            lastDay = gaiaBillOfApParam.getBeginTime().minusDays(1);
        }*/

        Example beginPeriodExample = Example.builder(GaiaBillOfAp.class).build();
        Example.Criteria beginPeriodCriteria = beginPeriodExample.createCriteria();
        beginPeriodCriteria.andEqualTo("client", gaiaBillOfApParam.getClient());
        beginPeriodCriteria.andBetween("paymentDate",
                DateUtil.formatDate(firstDay),
                DateUtil.formatDate(startDate)
        );
        List<GaiaBillOfAp> beginPeriodBillOfApList = billOfApMapper.selectByExample(beginPeriodExample);
        if(CollUtil.isNotEmpty(beginPeriodBillOfApList)){
            for (GaiaBillOfAp billOfAp : beginPeriodBillOfApList) {
                billOfAp.setAmountOfPayment(billOfAp.getAmountOfPayment().setScale(2,BigDecimal.ROUND_HALF_UP));
            }
        }
        return beginPeriodBillOfApList.stream().collect(
                Collectors.groupingBy(
                        beginPeriodBillOfAp -> beginPeriodBillOfAp.getSupSelfCode() + '-' + beginPeriodBillOfAp.getDcCode()
                )
        );
    }

    @Override
    public void dailyInsert(LocalDate day) throws ParseException {
        String stringDay = DateUtil.formatDate(day);

        //供应商单据数据插入 9999
        this.supBillInsert(stringDay);

        //付款申请表 数据插入 6666
        this.paymentApplicationInsert(stringDay);

    }


    /**
     * 功能描述: 手动录入
     *
     * @param userInfo
     * @param inDataList
     * @return java.lang.Object
     * @author wangQc
     * @date 2021/8/26 4:01 下午
     */
    @Override
    public Object save(GetLoginOutData userInfo, List<GaiaBillOfApDto> inDataList) {

        for (GaiaBillOfApDto inData : inDataList) {

            if (StringUtils.isEmpty(inData.getDcCode())) {
                throw new BusinessException("DC编码不能为空");
            }

            if (StringUtils.isEmpty(inData.getPaymentId())) {
                throw new BusinessException("应付方式编码不能为空");
            }

            if (StringUtils.isEmpty(inData.getSupSelfCode())) {
                throw new BusinessException("供应商编码不能为空");
            }

            if (Objects.isNull(inData.getAmountOfPayment())) {
                throw new BusinessException("发生金额不能为空");
            }
            if (inData.getAmountOfPayment().compareTo(new BigDecimal("999999999")) > 0) {
                throw new BusinessException("发生金额不能超过999999999");
            }
            /*//如果期初未导入  则continue
            Example billExample = Example.builder(GaiaBillOfAp.class).build();
            Example.Criteria exampleCriteria = billExample.createCriteria();
            exampleCriteria
                    .andEqualTo("client", userInfo.getClient())
                    .andEqualTo("dcCode", inData.getDcCode())
                    .andEqualTo("supSelfCode", inData.getSupSelfCode())
                    .andEqualTo("paymentId", FiApPaymentEnum.PERIOD_BEGIN.getCode());

                exampleCriteria.andEqualTo("salesmanId",inData.getSalesmanName());

            billExample.setOrderByClause("CLIENT limit 1");
            GaiaBillOfAp billOfAp = billOfApMapper.selectOneByExample(billExample);
            //如果没有期初 或者期初的日期>当前日期 则直接返回
            if(StrUtil.isNotBlank(inData.getSalesmanName())){
                if (Objects.isNull(billOfAp)) {
                    if(!(Objects.equals(inData.getPaymentId(),"8888"))){
                        throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】"+"，业务员【"+inData.getSalesmanName()+"】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                    }else {
                        if (DateUtil.formatDate(inData.getPaymentDate()).compareTo(DateUtil.formatDate(LocalDate.now())) > 0) {
                            throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                        }
                    }
                }else {
                    if(Objects.equals(inData.getPaymentId(),"8888")){
                        throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】"+"，业务员【"+inData.getSalesmanName()+"】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                    }
                }
            }else {
                if (Objects.isNull(billOfAp)) {
                    if(!(Objects.equals(inData.getPaymentId(),"8888"))){
                        throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                    }else {
                        if (DateUtil.formatDate(inData.getPaymentDate()).compareTo(DateUtil.formatDate(LocalDate.now())) > 0) {
                            throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                        }
                    }
                }else {
                    if(Objects.equals(inData.getPaymentId(),"8888")){
                        throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                    }
                }
            }*/
            beginningCheck(userInfo,inData);
            //校验发生地
            this.checkDcCode(userInfo, inData.getDcCode());

            //校验供应商编码
            GaiaSupplierBusiness gaiaSupplierBusiness = this.checkSup(userInfo, inData.getSupSelfCode());

            //校验编码方式 是否存在
            this.checkPaymentId(userInfo, inData.getPaymentId());

            //封装入库实体
            GaiaBillOfAp gaiaBillOfAp = new GaiaBillOfAp();
            BeanUtils.copyProperties(inData, gaiaBillOfAp);
            gaiaBillOfAp.setSalesmanName(null);
            gaiaBillOfAp.setClient(userInfo.getClient());
            gaiaBillOfAp.setPaymentDate(DateUtil.formatDate(inData.getPaymentDate()));
            gaiaBillOfAp.setSupName(gaiaSupplierBusiness.getSupName());
            gaiaBillOfAp.setSupCode(gaiaSupplierBusiness.getSupCode());
            gaiaBillOfAp.setAmountExcludingTax(BigDecimal.ZERO);
            gaiaBillOfAp.setAmountOfTax(BigDecimal.ZERO);
            if(StrUtil.isNotBlank(inData.getSalesmanName())){
                gaiaBillOfAp.setSalesmanId(inData.getSalesmanName());
                List<String> salesman = getName(userInfo.getClient(), gaiaBillOfAp.getDcCode(), gaiaBillOfAp.getSupSelfCode(), inData.getSalesmanName());
                if(CollUtil.isNotEmpty(salesman)){
                    gaiaBillOfAp.setSalesmanName(salesman.get(0));
                }
            }else {
                gaiaBillOfAp.setSalesmanId(null);
                gaiaBillOfAp.setSalesmanName(null);
            }
            billOfApMapper.insert(gaiaBillOfAp);
            //updateSupplierBusiness(userInfo.getClient(),inData.getSupSelfCode(),inData.getDcCode(),DateUtil.formatDate(inData.getPaymentDate()));
        }
        return null;
    }

    private void beginningCheck(GetLoginOutData userInfo,GaiaBillOfApDto inData){
        //如果期初未导入  则continue
        Example billExample = Example.builder(GaiaBillOfAp.class).build();
        Example.Criteria exampleCriteria = billExample.createCriteria();
        exampleCriteria
                .andEqualTo("client", userInfo.getClient())
                .andEqualTo("dcCode", inData.getDcCode())
                .andEqualTo("supSelfCode", inData.getSupSelfCode())
                .andEqualTo("paymentId", FiApPaymentEnum.PERIOD_BEGIN.getCode());

        //exampleCriteria.andIsNull("salesmanId");
        if(StrUtil.isNotBlank(inData.getSalesmanName())){
            exampleCriteria.andEqualTo("salesmanId",StrUtil.isBlank(inData.getSalesmanName())?null:inData.getSalesmanName());
        }else {
            exampleCriteria.andIsNull("salesmanId");
        }

        billExample.setOrderByClause("CLIENT limit 1");
        GaiaBillOfAp billOfAp = billOfApMapper.selectOneByExample(billExample);
        //如果没有期初 或者期初的日期>当前日期 则直接返回
        if(StrUtil.isNotBlank(inData.getSalesmanName())){
            if (Objects.isNull(billOfAp)) {
                if(!(Objects.equals(inData.getPaymentId(),"8888"))){
                    throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】"+"，业务员【"+inData.getSalesmanName()+"】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                }else {
                    if (DateUtil.formatDate(inData.getPaymentDate()).compareTo(DateUtil.formatDate(LocalDate.now())) > 0) {
                        throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                    }
                }
            }else {
                if(Objects.equals(inData.getPaymentId(),"8888")){
                    throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】"+"，业务员【"+inData.getSalesmanName()+"】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                }
            }
        }else {
            if (Objects.isNull(billOfAp)) {
                if(!(Objects.equals(inData.getPaymentId(),"8888"))){
                    throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                }else {
                    if (DateUtil.formatDate(inData.getPaymentDate()).compareTo(DateUtil.formatDate(LocalDate.now())) > 0) {
                        throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                    }
                }
            }else {
                if(Objects.equals(inData.getPaymentId(),"8888")){
                    throw new BusinessException("发生地编码【" + inData.getDcCode() + "】，供应商编码【" + inData.getSupSelfCode() + "】无期初或期初日期大于导入日期或已存在期初，请先核实。");
                }
            }
        }
    }

    /**
     * 功能描述: 校验编码方式 是否存在
     *
     * @param userInfo
     * @param paymentId
     * @return void
     * @author wangQc
     * @date 2021/8/27 10:08 上午
     */
    private void checkPaymentId(GetLoginOutData userInfo, String paymentId) {
        if (StringUtils.isNotEmpty(FiApPaymentEnum.getValueByCode(paymentId))) {
            throw new BusinessException("应付方式编码有误，不能为系统默认应付方式");
        }

      /*  Example paymentIdExample = Example.builder(GaiaFiApPayment.class).build();
        paymentIdExample.createCriteria()
                .andEqualTo("client", userInfo.getClient())
                .andEqualTo("paymentId", paymentId);
        paymentIdExample.setOrderByClause("CLIENT limit 1");
        GaiaFiApPayment gaiaFiApPayment = gaiaFiApPaymentMapper.selectOneByExample(paymentIdExample);

        if (Objects.isNull(gaiaFiApPayment)) {
            throw new BusinessException("应付方式编码有误");
        }*/
    }

    /**
     * 功能描述: 校验供应商
     *
     * @param userInfo
     * @param supSelfCode
     * @return com.gys.business.mapper.entity.GaiaSupplierBusiness
     * @author wangQc
     * @date 2021/8/27 10:08 上午
     */
    private GaiaSupplierBusiness checkSup(GetLoginOutData userInfo, String supSelfCode) {

        Example supplierExample = Example.builder(GaiaSupplierBusiness.class).build();
        supplierExample.createCriteria()
                .andEqualTo("client", userInfo.getClient())
                .andEqualTo("supSelfCode", supSelfCode);
        supplierExample.setOrderByClause("CLIENT limit 1");
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectOneByExample(supplierExample);

        if (Objects.isNull(gaiaSupplierBusiness)) {
            throw new BusinessException("供应商编码有误");
        }
        return gaiaSupplierBusiness;
    }

    /**
     * 功能描述: 校验发生地
     *
     * @param userInfo
     * @param dcCode
     * @return void
     * @author wangQc
     * @date 2021/8/27 10:06 上午
     */
    private void checkDcCode(GetLoginOutData userInfo, String dcCode) {
        //校验仓库
        Example depExample = Example.builder(GaiaDepData.class).build();
        depExample.createCriteria()
                .andEqualTo("client", userInfo.getClient())
                .andEqualTo("depCls", "10")
                .andEqualTo("depId", dcCode);
        depExample.setOrderByClause("CLIENT limit 1");
        GaiaDepData gaiaDepData = gaiaDepDataMapper.selectOneByExample(depExample);

        // 校验门店
        Example storeExample = Example.builder(GaiaStoreData.class).build();
        storeExample.createCriteria()
                .andEqualTo("client", userInfo.getClient())
                .andEqualTo("stoCode", dcCode);
        storeExample.setOrderByClause("CLIENT limit 1");
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectOneByExample(storeExample);

        if (Objects.isNull(gaiaDepData) && Objects.isNull(gaiaStoreData)) {
            throw new BusinessException("发生地编码有误");
        }
    }


    /**
     * 功能描述: 批量导入
     *
     * @param userInfo
     * @param dataList
     * @return void
     * @author wangQc
     * @date 2021/8/27 9:52 上午
     */
    @Override
    public void batchImport(GetLoginOutData userInfo, List<GaiaBillOfApExcelDto> dataList) {
        //根据 供应商 发生日期 发生地 应付编码 分组
        Map<String, List<GaiaBillOfApExcelDto>> map = dataList.stream().collect(
                Collectors.groupingBy(
                        excelDto -> excelDto.getSupSelfCode() + '-' + excelDto.getPaymentDate() + '-' + excelDto.getDcCode() + '-' + excelDto.getPaymentId()+"-"+excelDto.getSalesmanName()
                )
        );
        //List<Map<String, Object>> salesman = supplierSalesman(userInfo, null);
        for (Map.Entry<String, List<GaiaBillOfApExcelDto>> entry : map.entrySet()) {
            //供应商 发生日期 发生地 应付编码
            String[] key = entry.getKey().split("-");
            String supSelfCode = key[0];
            String stringDay = key[1];
            String dcCode = key[2];
            String paymentId = key[3];
            //String salesmanName = key[4];
            //校验发生地
            this.checkDcCode(userInfo, dcCode);
            //校验供应商编码
            GaiaSupplierBusiness gaiaSupplierBusiness = this.checkSup(userInfo, supSelfCode);
            //校验编码方式 是否存在
            this.checkPaymentId(userInfo, paymentId);
            /*//校验期初
            if (!checkBeginPeriod(userInfo.getClient(), dcCode, supSelfCode)) {
                throw new BusinessException("发生地编码【" + dcCode + "】，供应商编码【" + supSelfCode + "】无期初或期初日期大于导入日期，请先核实。");
            }*/
            //拿到分组后的集合
            List<GaiaBillOfApExcelDto> value = entry.getValue();
            GaiaBillOfApExcelDto gaiaBillOfApExcelDto = value.get(0);

            GaiaBillOfApDto inData = new GaiaBillOfApDto();
            BeanUtil.copyProperties(gaiaBillOfApExcelDto,inData);
            beginningCheck(userInfo,inData);



            //校验业务员是否存在业务员表
            List<Map<String, Object>> mapList = new ArrayList<>();
            if(Objects.nonNull(gaiaBillOfApExcelDto)){
                if(StrUtil.isNotBlank(gaiaBillOfApExcelDto.getSalesmanName())){
                    mapList = this.checkSalesman(userInfo.getClient(), supSelfCode, dcCode, gaiaBillOfApExcelDto.getSalesmanName());
                }

            }
           /* //校验业务员是否存在业务员表
            String salesmanName = value.stream().findFirst().get().getSalesmanName();*/
            List<GaiaBillOfAp> ofApList = new ArrayList<>();
            for (GaiaBillOfApExcelDto bill : value) {
               /* //计算付款金额
                BigDecimal sumPay = value.stream()
                        .map(GaiaBillOfApExcelDto::getAmountOfPayment)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);*/

                //入库
                GaiaBillOfAp gaiaBillOfAp = new GaiaBillOfAp();
                gaiaBillOfAp.setClient(userInfo.getClient());
                gaiaBillOfAp.setDcCode(dcCode);
                gaiaBillOfAp.setPaymentId(paymentId);
                gaiaBillOfAp.setSupSelfCode(supSelfCode);
                gaiaBillOfAp.setSupCode(gaiaSupplierBusiness.getSupCode());
                gaiaBillOfAp.setSupName(gaiaSupplierBusiness.getSupName());
                gaiaBillOfAp.setPaymentDate(stringDay);
                gaiaBillOfAp.setAmountOfPayment(bill.getAmountOfPayment());
                gaiaBillOfAp.setAmountExcludingTax(BigDecimal.ZERO);
                gaiaBillOfAp.setAmountOfTax(BigDecimal.ZERO);
                gaiaBillOfAp.setRemarks(Objects.isNull(gaiaBillOfApExcelDto) ? null : gaiaBillOfApExcelDto.getRemarks());
                if (CollUtil.isNotEmpty(mapList)) {
                    gaiaBillOfAp.setSalesmanName(Convert.toStr(mapList.get(0).get("gssName")));
                    gaiaBillOfAp.setSalesmanId(Convert.toStr(mapList.get(0).get("gssCode")));
                }
                ofApList.add(gaiaBillOfAp);
                //updateSupplierBusiness(userInfo.getClient(),supSelfCode,dcCode,stringDay);
            }
            billOfApMapper.insertList(ofApList);
        }


    }

    /**
     * 校验营业员
     * @param client 加盟商
     * @param supSelfCode 供应商编码
     * @param dcCode 地点
     */
    private List<Map<String,Object>> checkSalesman(String client, String supSelfCode, String dcCode,String name){
        List<Map<String,Object>> count  =  billOfApMapper.checkSalesman(client,supSelfCode,dcCode,name);
        if(CollUtil.isEmpty(count)){
            throw new BusinessException("营业员【"+name+"】不存在或营业员不在加盟商或地点或供应商编码下");
        }
        return count;
    }

    /**
     * 功能描述: 期初导入
     *
     * @param userInfo
     * @param dataList
     * @return void
     * @author wangQc
     * @date 2021/8/27 10:38 上午
     */
    @Override
    public void beginPeriodImport(GetLoginOutData userInfo, List<GaiaBillOfApExcelDto> dataList) {

        List<GaiaBillOfApExcelDto> paymentId = dataList.stream().filter(s -> !Objects.equals(FiApPaymentEnum.PERIOD_BEGIN.getCode(), s.getPaymentId())).collect(Collectors.toList());
        if(CollUtil.isNotEmpty(paymentId)){
            throw new BusinessException("提示："+StringUtils.join(dataList,",")+"只能导入期初数据！");
        }

        //根据 供应商 发生地 分组
        Map<String, List<GaiaBillOfApExcelDto>> map = dataList.stream().collect(
                Collectors.groupingBy(
                        excelDto -> excelDto.getSupSelfCode() + '-' + excelDto.getDcCode()+"-"+excelDto.getSalesmanName()
                )
        );

        for (Map.Entry<String, List<GaiaBillOfApExcelDto>> entry : map.entrySet()) {
            //供应商 发生日期 发生地 应付编码
            String[] key = entry.getKey().split("-");
            String supSelfCode = key[0];
            String dcCode = key[1];

            List<GaiaBillOfApExcelDto> values= entry.getValue();
            GaiaBillOfApExcelDto gaiaBillOfApExcelDto = values.get(0);
            GaiaBillOfApDto inData = new GaiaBillOfApDto();
            BeanUtil.copyProperties(gaiaBillOfApExcelDto,inData);
            beginningCheck(userInfo,inData);


            //校验发生地
            this.checkDcCode(userInfo, dcCode);

            //校验供应商编码
            GaiaSupplierBusiness gaiaSupplierBusiness = this.checkSup(userInfo, supSelfCode);
            GaiaBillOfApExcelDto value = new GaiaBillOfApExcelDto();

            //期初只有一个
            if(CollectionUtil.isNotEmpty(values)){
                value = values.get(0);
            }
            //校验业务员
            List<Map<String, Object>> mapList = new ArrayList<>();
            if(StrUtil.isNotBlank(value.getSalesmanName())){
                mapList = this.checkSalesman(userInfo.getClient(), value.getSupSelfCode(), value.getDcCode(), value.getSalesmanName());
            }
            //拿到分组后的集合
            //计算付款金额
           /* BigDecimal sumPay = entry.getValue().stream()
                    .map(GaiaBillOfApExcelDto::getAmountOfPayment)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);*/

            //入库

            GaiaBillOfAp gaiaBillOfAp = new GaiaBillOfAp();
            gaiaBillOfAp.setClient(userInfo.getClient());
            gaiaBillOfAp.setDcCode(dcCode);
            gaiaBillOfAp.setPaymentId(FiApPaymentEnum.PERIOD_BEGIN.getCode());
            gaiaBillOfAp.setSupSelfCode(supSelfCode);
            gaiaBillOfAp.setSupCode(gaiaSupplierBusiness.getSupCode());
            gaiaBillOfAp.setSupName(gaiaSupplierBusiness.getSupName());
            gaiaBillOfAp.setRemarks(value.getRemarks());
            if(StrUtil.isNotBlank(value.getPaymentDate())){
                gaiaBillOfAp.setPaymentDate(value.getPaymentDate());
            }
            gaiaBillOfAp.setAmountOfPayment(value.getAmountOfPayment());
            gaiaBillOfAp.setAmountExcludingTax(BigDecimal.ZERO);
            gaiaBillOfAp.setAmountOfTax(BigDecimal.ZERO);
            if(CollUtil.isNotEmpty(mapList)){
                gaiaBillOfAp.setSalesmanName(Convert.toStr(mapList.get(0).get("gssName")));
                gaiaBillOfAp.setSalesmanId(Convert.toStr(mapList.get(0).get("gssCode")));
            }


            billOfApMapper.insert(gaiaBillOfAp);

            //updateSupplierBusiness(userInfo.getClient(),supSelfCode,dcCode,value.getPaymentDate());
        }
    }

    /**
     * 获取期末余额
     * @param client 加盟商
     * @param dcCode 地点
     * @param supSelfCode 供应商
     */
    private void updateSupplierBusiness(String client, String supSelfCode, String dcCode, String date){
        List<GaiaBillOfAp> billOfApVOList = this.billOfApMapper.getAmt(client, supSelfCode, dcCode, date);
        //期初是否存在
        if (CollUtil.isNotEmpty(billOfApVOList)) {
            GaiaBillOfAp ofApVO = billOfApVOList.get(0);
            List<GaiaBillOfAp> billOfList = this.billOfApMapper.getBillOfAp(client, supSelfCode, dcCode, date);
            //为空说明期初在时间后面
            if (CollUtil.isEmpty(billOfList)) {
                //修改
                this.billOfApMapper.updateSupplierBusiness(client, supSelfCode, dcCode, ofApVO.getAmountOfPayment());
            } else {
                //昨天
                String yestDay = cn.hutool.core.date.DateUtil.format(cn.hutool.core.date.DateUtil.parse(cn.hutool.core.date.DateUtil.yesterday().toString(), "yyyy-MM-dd"), "yyyyMMdd");
                GaiaBillOfAp billOf = this.billOfApMapper.getBillOf(client, supSelfCode, dcCode, yestDay);
                BigDecimal amountOfPayment = BigDecimal.ZERO;
                if(Objects.nonNull(billOf)){
                      amountOfPayment = billOfList.stream().map(GaiaBillOfAp::getAmountOfPayment).reduce(BigDecimal.ZERO, BigDecimal::add).add(billOf.getAmountOfPayment());
                }else {
                      amountOfPayment = billOfList.stream().map(GaiaBillOfAp::getAmountOfPayment).reduce(BigDecimal.ZERO, BigDecimal::add);
                }
                this.billOfApMapper.updateSupplierBusiness(client, supSelfCode, dcCode, amountOfPayment);
            }
        }
    }

    @Override
    public SXSSFWorkbook listExport(GaiaBillOfApParam gaiaBillOfApParam, GetLoginOutData userInfo) {

        PageInfo<LinkedHashMap<String, Object>> listPage = getListPage(gaiaBillOfApParam, userInfo);
        List<LinkedHashMap<String, Object>> datas = listPage.getList();

        if (CollectionUtils.isEmpty(datas)) {
            throw new BusinessException("提示：导出失败, 无数据");
        }
        LinkedHashMap<String, Object> total = listPage.getListNum();
        datas.add(total);
        //转换
        List<LinkedHashMap<String, Object>> data = new Gson().fromJson(new Gson().toJson(datas), new TypeToken<List<LinkedHashMap<String, Object>>>() {
        }.getType());
        //获取
        // 获取数据
        SXSSFWorkbook workbook = new SXSSFWorkbook(data.size() + 100);
        Sheet sheet = workbook.createSheet("供应商应付列表");

      /*  LinkedHashMap<String, Object> hashMap = data.get(0);
        for (Map.Entry<String, Object> stringObjectEntry : hashMap.entrySet()) {
            if(stringObjectEntry.getKey().equals("供应商编码")){
                hashMap.put("业务员")
            }
        }*/

        if(CollUtil.isNotEmpty(data)){
            data.get(data.size()-1).remove("门店编码");
            /*for (LinkedHashMap<String, Object> next : data) {
                next.remove("client");
                next.remove("supSelfCode");
                next.remove("salesmanName");
            }*/
            Iterator<LinkedHashMap<String, Object>> it = data.iterator();
            if(it.hasNext()){
                while (it.hasNext()) {
                    LinkedHashMap<String, Object> next = it.next();
                    next.remove("client");
                    next.remove("supSelfCode");
                    next.remove("salesmanName");
                }
            }
            for (LinkedHashMap<String, Object> datum : data) {
                for (Map.Entry<String, Object> entry : datum.entrySet()) {
                    if(StrUtil.isBlank(Convert.toStr(entry.getValue())) && !(Convert.toStr(entry.getKey()).equals("供应商编码")) && !(Convert.toStr(entry.getKey()).equals("业务员"))){
                        datum.put(Convert.toStr(entry.getKey()),"0.00");
                    }
                }
               /* if(datum.containsKey("合计")){
                    datum.put("业务员","");
                }*/
            }
        }
        Set<String> keys = data.get(0).keySet();

        //过滤
        if(CollUtil.isNotEmpty(keys)) {
            Iterator it = keys.iterator();
            while (it.hasNext()) {
                String str = Convert.toStr(it.next());
                if ("client".equals(str)) {
                    it.remove();
                }else if("supSelfCode".equals(str)){
                    it.remove();
                }else if("salesmanName".equals(str)){
                    it.remove();
                }
            }
            /*for (String key : keys) {
                if(key.equals("供应商编码")){

                }
            }*/
        }
        //设置标题
        // 设置表头
        Row titleRow = sheet.createRow(0);
        int i = 0;
        for (String key : keys) {
            Cell cell = titleRow.createCell(i);
            cell.setCellValue(key);
            i++;
        }

        for (int m = 1; m <= data.size(); m++) {
            Row dataRow = sheet.createRow(m);
            LinkedHashMap<String, Object> map = data.get(m - 1);
            // 列数
            int increase = 0;

            //遍历map
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                Cell cell = dataRow.createCell(increase++);
                String reg = "-?[0-9]+.?[0-9]{0,}";
                boolean fal = ExcelUtils.tran(entry.getValue()).matches(reg);
                if(fal){
                    cell.setCellValue(new BigDecimal(ExcelUtils.tran(entry.getValue())).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
                }else {
                    cell.setCellValue(ExcelUtils.tran(entry.getValue()));
                }
            }
        }
        //自适应列宽
        ExcelUtils.setSizeColumn(sheet, keys.size());
        return workbook;
    }

    /**
     * 功能描述:
     * <p>
     * 根据GAIA_PAYMENT_APPLICATIONS付款申请表
     * APPLICATION_STATUS状态0，
     * APPROVAL_STATUS状态0、1查询日期、地点、供应商
     * 按照6666（是否应付方式）应付发生金额GAIA_BILL_OF_AP 明细清单表
     *
     * @param stringDay
     * @return void
     * @author wangQc
     * @date 2021/8/25 9:04 下午
     */
    public void paymentApplicationInsert(String stringDay) {

        List<GaiaBillOfAp> beginPeriodBillOf = new ArrayList<>();
        List<GaiaBillOfAp> beginPeriodBillOfApList = billOfApMapper.selectGroupBy("8888");
        beginPeriodBillOf.addAll(beginPeriodBillOfApList);
        if (CollUtil.isEmpty(beginPeriodBillOfApList)) {
            return;
        }
        List<GaiaBillOfAp> ofAp = new ArrayList<>();
        List<GaiaBillOfAp> ofApList = billOfApMapper.selectGroupBy("6666");
        if (CollUtil.isNotEmpty(ofApList)) {
            for (GaiaBillOfAp billOfAp : beginPeriodBillOfApList) {
                for (GaiaBillOfAp gaiaBillOfAp : ofApList) {
                    if (billOfAp.getClient().equals(gaiaBillOfAp.getClient()) && billOfAp.getDcCode().equals(gaiaBillOfAp.getDcCode()) && billOfAp.getSupSelfCode().equals(gaiaBillOfAp.getSupSelfCode())) {
                        ofAp.add(billOfAp);
                    }
                }
            }
            beginPeriodBillOfApList.removeAll(ofAp);
        }
        String startDay = "";
        if (CollUtil.isNotEmpty(beginPeriodBillOfApList)) {
            Collections.sort(beginPeriodBillOfApList, new Comparator<GaiaBillOfAp>() {
                @Override
                public int compare(GaiaBillOfAp o1, GaiaBillOfAp o2) {
                    return o1.getPaymentDate().compareTo(o2.getPaymentDate());
                }
            });
            startDay = beginPeriodBillOfApList.get(0).getPaymentDate();
        } else {
            //那就是查询昨天的
            startDay = stringDay;
        }
        //单据付款
        Example example = Example.builder(GaiaPaymentApplications.class).build();
        example.createCriteria().andGreaterThanOrEqualTo("gpaApprovalDate", startDay)
                .andLessThanOrEqualTo("gpaApprovalDate", stringDay)
                .andEqualTo("approvalStatus",1);
        //找出当天有付款的
        List<GaiaPaymentApplications> applicationsList = gaiaPaymentApplicationsMapper.selectByExample(example);
        if(CollUtil.isNotEmpty(applicationsList)){
            for (GaiaPaymentApplications paymentApplications : applicationsList) {
                paymentApplications.setType(1);
            }
        }
        //类型为3的供应商预付
        for (GaiaBillOfAp billOfAp : beginPeriodBillOf) {
            List<Map<String,Object>> paymentApplicationsList = this.gaiaPaymentApplicationsMapper.selectprepaidDocuments(startDay, stringDay, billOfAp.getClient(), billOfAp.getSupSelfCode());
            if (CollUtil.isNotEmpty(paymentApplicationsList)) {
                for (Map<String,Object> gaiaPaymentApplications : paymentApplicationsList) {
                    if (Objects.equals(Convert.toStr(gaiaPaymentApplications.get("approvalStatus")), "2")) {
                        GaiaPaymentApplications paymentApplications = new GaiaPaymentApplications();
                        gaiaPaymentApplications.put("invoiceAmountOfThisPayment",new BigDecimal(Convert.toStr(gaiaPaymentApplications.get("invoiceAmountOfThisPayment"))).subtract(new BigDecimal(Convert.toStr(gaiaPaymentApplications.get("writeOffAmount")))));
                        paymentApplications.setClient(billOfAp.getClient());
                        paymentApplications.setSupCode(billOfAp.getSupSelfCode());
                        String proSite = Convert.toStr(gaiaPaymentApplications.get("proSite"));
                        paymentApplications.setProSite(proSite);
                        paymentApplications.setType(3);
                        paymentApplications.setPaymentOrderDate(Convert.toStr(gaiaPaymentApplications.get("paymentOrderDate")));
                        paymentApplications.setGpaApprovalDate(Convert.toStr(gaiaPaymentApplications.get("gpaApprovalDate")));
                        paymentApplications.setInvoiceAmountOfThisPayment(new BigDecimal(Convert.toStr(gaiaPaymentApplications.get("invoiceAmountOfThisPayment"))));
                        applicationsList.add(paymentApplications);
                    }
                }
            }
        }


        if (CollectionUtils.isEmpty(applicationsList)) {
            return;
        }

        //根据 加盟商 发生地  供应商 分组
        Map<String, List<GaiaPaymentApplications>> map = applicationsList.stream().collect(
                Collectors.groupingBy(
                        paymentApplication -> paymentApplication.getClient() + '-' + paymentApplication.getSupCode() + '-' + paymentApplication.getProSite()
                )
        );


        //遍历map 计算开单金额 去税成本额 税金
        for (Map.Entry<String, List<GaiaPaymentApplications>> entry : map.entrySet()) {
            //clientId-供应商-地点
            String[] key = entry.getKey().split("-");
            String client = key[0];
            String supSelfCode = key[1];
            String place = key[2];

            //校验期初
            if (!checkBeginPeriod(client, place, supSelfCode)) continue;

            Example billExamples = Example.builder(GaiaBillOfAp.class).build();
            billExamples.createCriteria()
                    .andEqualTo("client", client)
                    .andEqualTo("dcCode", place)
                    .andEqualTo("supSelfCode", supSelfCode)
                    .andEqualTo("paymentId", FiApPaymentEnum.PAYED.getCode());
            billExamples.setOrderByClause("CLIENT limit 1");
            GaiaBillOfAp gaiaBillOfAps = billOfApMapper.selectOneByExample(billExamples);
            if (Objects.nonNull(gaiaBillOfAps)) {
                //有6666
                //拿到分组后的集合
                List<GaiaPaymentApplications> value = entry.getValue();
                if (CollUtil.isNotEmpty(value)) {
                    Example supplierExample = Example.builder(GaiaSupplierBusiness.class).build();
                    supplierExample.createCriteria()
                            .andEqualTo("client", client)
                            .andEqualTo("supSelfCode", supSelfCode);
                    supplierExample.setOrderByClause("CLIENT limit 1");
                    GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectOneByExample(supplierExample);
                    if (Objects.isNull(gaiaSupplierBusiness)) {
                        log.info("client======={},supSelfCode==={}找到不对应的供应商", client, supSelfCode);
                        continue;
                    }
                    List<GaiaPaymentApplications> collect = value.stream().filter(va -> stringDay.equals(va.getGpaApprovalDate())).collect(Collectors.toList());
                    for (GaiaPaymentApplications datum : collect) {
                        if (Objects.nonNull(datum.getInvoiceAmountOfThisPayment())) {
                            //获取业务员记录
                            if (datum.getType() == 1) {
                                billOfApMapper.insert(
                                        new GaiaBillOfAp()
                                                .setClient(key[0])
                                                .setDcCode(place)
                                                .setAmountOfPayment(new BigDecimal("0").subtract(datum.getInvoiceAmountOfThisPayment()))
                                                .setAmountExcludingTax(BigDecimal.ZERO)
                                                .setAmountOfTax(BigDecimal.ZERO)
                                                .setPaymentId(FiApPaymentEnum.PAYED.getCode())
                                                .setPaymentDate(datum.getGpaApprovalDate())
                                                .setSupCode(gaiaSupplierBusiness.getSupCode())
                                                .setSupSelfCode(gaiaSupplierBusiness.getSupSelfCode())
                                                .setSupName(gaiaSupplierBusiness.getSupName())
                                );

                                /*//单据付款
                                List<Map<String, Object>> applications = billOfApMapper.selectAmt(datum);
                                if (CollUtil.isNotEmpty(applications)) {
                                    BigDecimal sum = BigDecimal.ZERO;
                                    for (Map<String, Object> application : applications) {
                                        if (Objects.nonNull(Convert.toStr(application.get("name")))) {
                                            BigDecimal bigDecimal = new BigDecimal(Convert.toStr(application.get("billingAmount")));
                                            List<String> name = getName(client, place, supSelfCode, Convert.toStr(application.get("name")));
                                            billOfApMapper.insert(
                                                    new GaiaBillOfAp()
                                                            .setClient(key[0])
                                                            .setDcCode(place)
                                                            .setAmountOfPayment(new BigDecimal("0").subtract(bigDecimal))
                                                            .setAmountExcludingTax(BigDecimal.ZERO)
                                                            .setAmountOfTax(BigDecimal.ZERO)
                                                            .setPaymentId(FiApPaymentEnum.PAYED.getCode())
                                                            .setPaymentDate(datum.getPaymentOrderDate())
                                                            .setSupCode(gaiaSupplierBusiness.getSupCode())
                                                            .setSupSelfCode(gaiaSupplierBusiness.getSupSelfCode())
                                                            .setSupName(gaiaSupplierBusiness.getSupName())
                                                            .setSalesmanId(Convert.toStr(application.get("name")))
                                                            .setSalesmanName(CollUtil.isNotEmpty(name)?name.get(0):null)
                                            );
                                            sum = sum.add(bigDecimal);
                                        }
                                    }
                                    //查看金额
                                    if (!(sum.compareTo(datum.getInvoiceAmountOfThisPayment()) == 0)) {
                                        BigDecimal subtract = datum.getInvoiceAmountOfThisPayment().subtract(sum);
                                        billOfApMapper.insert(
                                                new GaiaBillOfAp()
                                                        .setClient(key[0])
                                                        .setDcCode(place)
                                                        .setAmountOfPayment(new BigDecimal("0").subtract(subtract))
                                                        .setAmountExcludingTax(BigDecimal.ZERO)
                                                        .setAmountOfTax(BigDecimal.ZERO)
                                                        .setPaymentId(FiApPaymentEnum.PAYED.getCode())
                                                        .setPaymentDate(datum.getPaymentOrderDate())
                                                        .setSupCode(gaiaSupplierBusiness.getSupCode())
                                                        .setSupSelfCode(gaiaSupplierBusiness.getSupSelfCode())
                                                        .setSupName(gaiaSupplierBusiness.getSupName())
                                        );
                                    }
                                }*/
                            } else if (datum.getType() == 3) {
                                billOfApMapper.insert(
                                        new GaiaBillOfAp()
                                                .setClient(key[0])
                                                .setDcCode(place)
                                                .setAmountOfPayment(new BigDecimal("0").subtract(datum.getInvoiceAmountOfThisPayment()))
                                                .setAmountExcludingTax(BigDecimal.ZERO)
                                                .setAmountOfTax(BigDecimal.ZERO)
                                                .setPaymentId(FiApPaymentEnum.PAYED.getCode())
                                                .setPaymentDate(datum.getGpaApprovalDate())
                                                .setSupCode(gaiaSupplierBusiness.getSupCode())
                                                .setSupSelfCode(gaiaSupplierBusiness.getSupSelfCode())
                                                .setSupName(gaiaSupplierBusiness.getSupName())
                                );
                            }
                        }
                    }
                }
            } else {
                //过滤有效
                //如果期初未导入  则continue
                Example billExample = Example.builder(GaiaBillOfAp.class).build();
                billExample.createCriteria()
                        .andEqualTo("client", client)
                        .andEqualTo("dcCode", place)
                        .andEqualTo("supSelfCode", supSelfCode)
                        .andEqualTo("paymentId", FiApPaymentEnum.PERIOD_BEGIN.getCode());
                billExample.setOrderByClause("CLIENT limit 1");
                GaiaBillOfAp gaiaBillOfAp = billOfApMapper.selectOneByExample(billExample);
                //不需要判空 上面已经判过

                LocalDate startDate = LocalDate.parse(gaiaBillOfAp.getPaymentDate(), DateTimeFormatter.ofPattern("yyyyMMdd"));
                Calendar c = Calendar.getInstance();
                Date date = Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                c.setTime(date);
                int day = c.get(Calendar.DATE);
                c.set(Calendar.DATE, day + 1);
                //初期金额的后一天
                String startTime = new SimpleDateFormat("yyyyMMdd").format(c.getTime());
                //拿到分组后的集合
                List<GaiaPaymentApplications> value = entry.getValue();

                if (CollUtil.isNotEmpty(value)) {
                    List<GaiaPaymentApplications> data = value.stream().filter(va -> Convert.toLong(startTime) <= Convert.toLong(va.getGpaApprovalDate()) && Convert.toLong(stringDay) >= Convert.toLong(va.getGpaApprovalDate())).collect(Collectors.toList());
                    if (CollUtil.isNotEmpty(data)) {
                        Example supplierExample = Example.builder(GaiaSupplierBusiness.class).build();
                        supplierExample.createCriteria()
                                .andEqualTo("client", client)
                                .andEqualTo("supSelfCode", supSelfCode);
                        supplierExample.setOrderByClause("CLIENT limit 1");
                        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectOneByExample(supplierExample);
                        if (Objects.isNull(gaiaSupplierBusiness)) {
                            log.info("client======={},supSelfCode==={}找到不对应的供应商", client, supSelfCode);
                            continue;
                        }
                        for (GaiaPaymentApplications datum : data) {
                            if (Objects.nonNull(datum.getInvoiceAmountOfThisPayment())) {
                                //获取业务员记录
                                if (datum.getType() == 1) {
                                    /*//单据付款
                                    List<Map<String, Object>> applications = billOfApMapper.selectAmt(datum);
                                    if (CollUtil.isNotEmpty(applications)) {
                                        BigDecimal sum = BigDecimal.ZERO;
                                        BigDecimal sumAmt = BigDecimal.ZERO;
                                        for (Map<String, Object> application : applications) {

                                            sumAmt = sumAmt.add(new BigDecimal(Convert.toStr(application.get("billingAmount"))));
                                            if (Objects.nonNull(Convert.toStr(application.get("name")))) {
                                                BigDecimal bigDecimal = new BigDecimal(Convert.toStr(application.get("billingAmount")));
                                                List<String> nameList = getName(client, place, supSelfCode, Convert.toStr(application.get("name")));
                                                billOfApMapper.insert(
                                                        new GaiaBillOfAp()
                                                                .setClient(key[0])
                                                                .setDcCode(place)
                                                                .setAmountOfPayment(new BigDecimal("0").subtract(bigDecimal))
                                                                .setAmountExcludingTax(BigDecimal.ZERO)
                                                                .setAmountOfTax(BigDecimal.ZERO)
                                                                .setPaymentId(FiApPaymentEnum.PAYED.getCode())
                                                                .setPaymentDate(datum.getPaymentOrderDate())
                                                                .setSupCode(gaiaSupplierBusiness.getSupCode())
                                                                .setSupSelfCode(gaiaSupplierBusiness.getSupSelfCode())
                                                                .setSupName(gaiaSupplierBusiness.getSupName())
                                                                .setSalesmanId(Convert.toStr(application.get("name")))
                                                                .setSalesmanName(CollUtil.isNotEmpty(nameList) ? nameList.get(0) : null)
                                                );
                                                sum = sum.add(bigDecimal);
                                            }
                                        }
                                        //查看金额
                                        if (!(sum.compareTo(sumAmt) == 0)) {
                                            BigDecimal subtract = datum.getInvoiceAmountOfThisPayment().subtract(sum);
                                            billOfApMapper.insert(
                                                    new GaiaBillOfAp()
                                                            .setClient(key[0])
                                                            .setDcCode(place)
                                                            .setAmountOfPayment(new BigDecimal("0").subtract(subtract))
                                                            .setAmountExcludingTax(BigDecimal.ZERO)
                                                            .setAmountOfTax(BigDecimal.ZERO)
                                                            .setPaymentId(FiApPaymentEnum.PAYED.getCode())
                                                            .setPaymentDate(datum.getPaymentOrderDate())
                                                            .setSupCode(gaiaSupplierBusiness.getSupCode())
                                                            .setSupSelfCode(gaiaSupplierBusiness.getSupSelfCode())
                                                            .setSupName(gaiaSupplierBusiness.getSupName())
                                            );
                                        }
                                    }*/
                                    billOfApMapper.insert(
                                            new GaiaBillOfAp()
                                                    .setClient(key[0])
                                                    .setDcCode(place)
                                                    .setAmountOfPayment(new BigDecimal("0").subtract(datum.getInvoiceAmountOfThisPayment()))
                                                    .setAmountExcludingTax(BigDecimal.ZERO)
                                                    .setAmountOfTax(BigDecimal.ZERO)
                                                    .setPaymentId(FiApPaymentEnum.PAYED.getCode())
                                                    .setPaymentDate(datum.getGpaApprovalDate())
                                                    .setSupCode(gaiaSupplierBusiness.getSupCode())
                                                    .setSupSelfCode(gaiaSupplierBusiness.getSupSelfCode())
                                                    .setSupName(gaiaSupplierBusiness.getSupName())
                                    );
                                } else if (datum.getType() == 3) {
                                    billOfApMapper.insert(
                                            new GaiaBillOfAp()
                                                    .setClient(key[0])
                                                    .setDcCode(place)
                                                    .setAmountOfPayment(new BigDecimal("0").subtract(datum.getInvoiceAmountOfThisPayment()))
                                                    .setAmountExcludingTax(BigDecimal.ZERO)
                                                    .setAmountOfTax(BigDecimal.ZERO)
                                                    .setPaymentId(FiApPaymentEnum.PAYED.getCode())
                                                    .setPaymentDate(datum.getGpaApprovalDate())
                                                    .setSupCode(gaiaSupplierBusiness.getSupCode())
                                                    .setSupSelfCode(gaiaSupplierBusiness.getSupSelfCode())
                                                    .setSupName(gaiaSupplierBusiness.getSupName())
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    private List<String> getName(String client,String supSite,String supSelfCode,String salesmanId){
        List<String>  name =  this.billOfApMapper.getName(client,supSite,supSelfCode,salesmanId);
        return name;
    }

    /**
     * 功能描述: 校验期初
     *
     * @param client
     * @return boolean
     * @author wangQc
     * @date 2021/8/27 10:13 上午
     */
    private boolean checkBeginPeriod(String client, String dcCode, String supSelfCode) {
        //如果期初未导入  则continue
        Example billExample = Example.builder(GaiaBillOfAp.class).build();
        billExample.createCriteria()
                .andEqualTo("client", client)
                .andEqualTo("dcCode", dcCode)
                .andEqualTo("supSelfCode", supSelfCode)
                .andEqualTo("paymentId", FiApPaymentEnum.PERIOD_BEGIN.getCode());
        billExample.setOrderByClause("CLIENT limit 1");
        GaiaBillOfAp gaiaBillOfAp = billOfApMapper.selectOneByExample(billExample);
        //如果没有期初 或者期初的日期>当前日期 则直接返回
        if (Objects.isNull(gaiaBillOfAp) || gaiaBillOfAp.getPaymentDate().compareTo(DateUtil.formatDate(LocalDate.now())) > 0) {
            return false;
        }
        return true;
    }

    /**
     * 功能描述: 供应商单据数据插入 9999
     *
     * @param stringDay
     * @return void
     * @author wangQc
     * @date 2021/8/25 9:03 下午
     */
    public void supBillInsert(String stringDay) throws ParseException {

        List<GaiaBillOfAp> ofList = new ArrayList<>();
        //查询期初日期
        Example beginPeriodExample = Example.builder(GaiaBillOfAp.class).build();
        Example.Criteria beginPeriodCriteria = beginPeriodExample.createCriteria();
        beginPeriodCriteria.andEqualTo("paymentId", FiApPaymentEnum.PERIOD_BEGIN.getCode());
        List<GaiaBillOfAp> beginPeriodBillOfApList = billOfApMapper.selectByExample(beginPeriodExample);
        ofList.addAll(beginPeriodBillOfApList);
        if (CollUtil.isEmpty(beginPeriodBillOfApList)) {
            return;
        }

        List<GaiaBillOfAp> ofAp = new ArrayList<>();
        //找出已经存在9999的
        List<GaiaBillOfAp> ofApList = billOfApMapper.selectGroupBy("9999");
        if (CollUtil.isNotEmpty(ofApList)) {
            for (GaiaBillOfAp billOfAp : beginPeriodBillOfApList) {
                for (GaiaBillOfAp gaiaBillOfAp : ofApList) {
                    if (billOfAp.getClient().equals(gaiaBillOfAp.getClient()) && billOfAp.getDcCode().equals(gaiaBillOfAp.getDcCode()) && billOfAp.getSupSelfCode().equals(gaiaBillOfAp.getSupSelfCode())) {
                        ofAp.add(billOfAp);
                    }
                }
            }
            beginPeriodBillOfApList.removeAll(ofAp);
        }
        String startDay = "";
        if (CollUtil.isNotEmpty(beginPeriodBillOfApList)) {
            Collections.sort(beginPeriodBillOfApList, new Comparator<GaiaBillOfAp>() {
                @Override
                public int compare(GaiaBillOfAp o1, GaiaBillOfAp o2) {
                    return o1.getPaymentDate().compareTo(o2.getPaymentDate());
                }
            });
            startDay = beginPeriodBillOfApList.get(0).getPaymentDate();
        } else {
            //那就是查询昨天的
            startDay = stringDay;
        }
        String start = DateUtil.dateConvert(startDay);
        String end = DateUtil.dateConvert(stringDay);
        List<String> dateList = DateUtil.getDateList(start, end);
        PageInfo<SupplierDealOutData> pageInfo = new PageInfo<>();
        List<SupplierDealOutData> list = new ArrayList<>();
        if (CollUtil.isNotEmpty(ofList)) {
            //for (Map<String, Object> map : getClientStoreMap) {
            Map<String, List<GaiaBillOfAp>> listMap = ofList.stream().collect(Collectors.groupingBy(ofAps -> ofAps.getClient() + "-" + ofAps.getDcCode()));
            if (CollUtil.isNotEmpty(listMap)) {
                for (Map.Entry<String, List<GaiaBillOfAp>> stringListEntry : listMap.entrySet()) {
                    String[] split = stringListEntry.getKey().split("-");
                    SupplierDealInData data = new SupplierDealInData();
                    data.setClient(split[0]);
                    data.setStoCode(split[1]);
                   if(CollUtil.isNotEmpty(dateList)){
                       for (String s : dateList) {
                           data.setStartDate(s);
                           data.setEndDate(s);
                           pageInfo = reportFormsService.selectSupplierDealPage(data);
                           if (Objects.nonNull(pageInfo)) {
                               if (CollUtil.isNotEmpty(pageInfo.getList())) {
                                   list.addAll(pageInfo.getList());
                               }
                           }
                       }
                   }
                }
            }

        }
        //根据 加盟商 发生地  供应商 分组
        Map<String, List<SupplierDealOutData>> map = list.stream()
                .filter(supplierDealOutData -> StringUtils.isNotEmpty(supplierDealOutData.getSupCode()))
                .filter(supplierDealOutData -> StringUtils.isNotEmpty(supplierDealOutData.getCLIENT()))
                .filter(supplierDealOutData -> StringUtils.isNotEmpty(supplierDealOutData.getSiteCode()))
                .collect(
                        Collectors.groupingBy(
                                supplierDealOutData -> supplierDealOutData.getCLIENT() + '-' + supplierDealOutData.getSupCode() + '-' + supplierDealOutData.getSiteCode()
                        )
                );

        //遍历map 计算开单金额 去税成本额 税金
        for (Map.Entry<String, List<SupplierDealOutData>> entry : map.entrySet()) {
            //拿到分组后的集合
            List<SupplierDealOutData> value = entry.getValue();
            //获取第一条
            SupplierDealOutData supplierDealOutData = value.get(0);

            //判断如果没有期初数据  直接下一个
            if (!checkBeginPeriod(supplierDealOutData.getCLIENT(), supplierDealOutData.getSiteCode(), supplierDealOutData.getSupCode()))
                continue;
            Example billExample = Example.builder(GaiaBillOfAp.class).build();
            billExample.createCriteria()
                    .andEqualTo("client", supplierDealOutData.getCLIENT())
                    .andEqualTo("dcCode", supplierDealOutData.getSiteCode())
                    .andEqualTo("supSelfCode", supplierDealOutData.getSupCode())
                    .andEqualTo("paymentId", FiApPaymentEnum.BILL.getCode());
            billExample.setOrderByClause("CLIENT limit 1");
            GaiaBillOfAp gaiaBillOfAp = billOfApMapper.selectOneByExample(billExample);
            if (Objects.nonNull(gaiaBillOfAp)) {
                //已经有9999
                if (CollUtil.isNotEmpty(value)) {
                    List<SupplierDealOutData> data = value.stream().filter(va -> stringDay.equals(va.getPostDate())).collect(Collectors.toList());
                    if (CollUtil.isNotEmpty(data)) {
                        //插入数据库记录每一条  而不是汇总
                        for (SupplierDealOutData outData : data) {
                            //昨天有没有营业员销售数据
                            List<SupplierDealOutData> suppliers = materialDocMapper.getBatSupplier(outData);
                            if (CollUtil.isNotEmpty(suppliers)) {
                                //获取到批次号
                                BigDecimal decimal = BigDecimal.ZERO;
                                //去税额度
                                BigDecimal batAmt = BigDecimal.ZERO;
                                //税额度
                                BigDecimal rateBat = BigDecimal.ZERO;
                                for (SupplierDealOutData supp : suppliers) {
                                    if (StrUtil.isNotBlank(supp.getSuppName())) {
                                        List<String> name = getName(supplierDealOutData.getCLIENT(), supplierDealOutData.getSiteCode(), supplierDealOutData.getSupCode(), supp.getSuppName());
                                        billOfApMapper.insert(
                                                new GaiaBillOfAp()
                                                        .setClient(supplierDealOutData.getCLIENT())
                                                        .setDcCode(supplierDealOutData.getSiteCode())
                                                        .setAmountOfPayment(supp.getBillingAmount())
                                                        .setAmountExcludingTax(supp.getBatAmt())
                                                        .setAmountOfTax(supp.getRateBat())
                                                        .setPaymentId(FiApPaymentEnum.BILL.getCode())
                                                        .setPaymentDate(outData.getPostDate())
                                                        .setSupSelfCode(supplierDealOutData.getSupCode())
                                                        .setSupName(supplierDealOutData.getSupName())
                                                        .setSalesmanId(supp.getSuppName())
                                                        .setSalesmanName(CollUtil.isNotEmpty(name)?name.get(0):null)
                                        );
                                        decimal = decimal.add(supp.getBillingAmount());
                                        batAmt = batAmt.add(supp.getBatAmt());
                                        rateBat = rateBat.add(supp.getRateBat());
                                    }
                                }
                                //查看金额   返回1表示大于0，返回-1表示小于0
                                if (!(decimal.compareTo(outData.getBillingAmount()) == 0)) {
                                    billOfApMapper.insert(
                                            new GaiaBillOfAp()
                                                    .setClient(supplierDealOutData.getCLIENT())
                                                    .setDcCode(supplierDealOutData.getSiteCode())
                                                    .setAmountOfPayment(outData.getBillingAmount().subtract(decimal))
                                                    .setAmountExcludingTax(outData.getBatAmt().subtract(batAmt))
                                                    .setAmountOfTax(outData.getRateBat().subtract(rateBat))
                                                    .setPaymentId(FiApPaymentEnum.BILL.getCode())
                                                    .setPaymentDate(outData.getPostDate())
                                                    .setSupSelfCode(supplierDealOutData.getSupCode())
                                                    .setSupName(supplierDealOutData.getSupName())
                                    );
                                }
                            }
                        }
                    }
                }
            } else {
                //过滤有效
                //如果期初未导入  则continue
                Example billExamples = Example.builder(GaiaBillOfAp.class).build();
                billExamples.createCriteria()
                        .andEqualTo("client", supplierDealOutData.getCLIENT())
                        .andEqualTo("dcCode", supplierDealOutData.getSiteCode())
                        .andEqualTo("supSelfCode", supplierDealOutData.getSupCode())
                        .andEqualTo("paymentId", FiApPaymentEnum.PERIOD_BEGIN.getCode());
                billExamples.setOrderByClause("CLIENT limit 1");
                GaiaBillOfAp gaiaBillOfAps = billOfApMapper.selectOneByExample(billExamples);
                //不需要判空 上面已经判过

                LocalDate startDate = LocalDate.parse(gaiaBillOfAps.getPaymentDate(), DateTimeFormatter.ofPattern("yyyyMMdd"));
                Calendar c = Calendar.getInstance();
                Date date = Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                c.setTime(date);
                int day = c.get(Calendar.DATE);
                c.set(Calendar.DATE, day + 1);
                //初期金额的后一天
                String startTime = new SimpleDateFormat("yyyyMMdd").format(c.getTime());

                //当前日期的前一天
                if (CollUtil.isNotEmpty(value)) {
                    List<SupplierDealOutData> data = value.stream().filter(va -> Convert.toLong(startTime) <= Convert.toLong(va.getPostDate()) && Convert.toLong(stringDay) >= Convert.toLong(va.getPostDate())).collect(Collectors.toList());
                    if (CollUtil.isNotEmpty(data)) {
                        for (SupplierDealOutData outData : data) {
                            //昨天有没有营业员销售数据
                            List<SupplierDealOutData> suppliers = materialDocMapper.getBatSupplier(outData);
                            if (CollUtil.isNotEmpty(suppliers)) {
                                //获取到批次号
                                BigDecimal decimal = BigDecimal.ZERO;
                                //去税额度
                                BigDecimal batAmt = BigDecimal.ZERO;
                                //税额度
                                BigDecimal rateBat = BigDecimal.ZERO;
                                for (SupplierDealOutData supp : suppliers) {
                                    if (StrUtil.isNotBlank(supp.getSuppName())) {
                                        List<String> name = getName(supplierDealOutData.getCLIENT(), supplierDealOutData.getSiteCode(), supplierDealOutData.getSupCode(), supp.getSuppName());

                                        billOfApMapper.insert(
                                                new GaiaBillOfAp()
                                                        .setClient(supplierDealOutData.getCLIENT())
                                                        .setDcCode(supplierDealOutData.getSiteCode())
                                                        .setAmountOfPayment(supp.getBillingAmount())
                                                        .setAmountExcludingTax(supp.getBatAmt())
                                                        .setAmountOfTax(supp.getRateBat())
                                                        .setPaymentId(FiApPaymentEnum.BILL.getCode())
                                                        .setPaymentDate(outData.getPostDate())
                                                        .setSupSelfCode(supplierDealOutData.getSupCode())
                                                        .setSupName(supplierDealOutData.getSupName())
                                                        .setSalesmanId(supp.getSuppName())
                                                        .setSalesmanName(CollUtil.isNotEmpty(name)?name.get(0):null)
                                        );
                                        decimal = decimal.add(supp.getBillingAmount());
                                        batAmt = batAmt.add(supp.getBatAmt());
                                        rateBat = rateBat.add(supp.getRateBat());
                                    }
                                }
                                //查看金额   返回1表示大于0，返回-1表示小于0
                                if (!(decimal.compareTo(outData.getBillingAmount()) == 0)) {
                                        billOfApMapper.insert(
                                                new GaiaBillOfAp()
                                                        .setClient(supplierDealOutData.getCLIENT())
                                                        .setDcCode(supplierDealOutData.getSiteCode())
                                                        .setAmountOfPayment(outData.getBillingAmount().subtract(decimal))
                                                        .setAmountExcludingTax(outData.getBatAmt().subtract(batAmt))
                                                        .setAmountOfTax(outData.getRateBat().subtract(rateBat))
                                                        .setPaymentId(FiApPaymentEnum.BILL.getCode())
                                                        .setPaymentDate(outData.getPostDate())
                                                        .setSupSelfCode(supplierDealOutData.getSupCode())
                                                        .setSupName(supplierDealOutData.getSupName())
                                        );
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 根据
     *
     * @param userInfo
     * @return
     */

    @Override
    public List<Map<String, Object>> supplierSalesman(GetLoginOutData userInfo,String depId,String stoCode,String supSelfCode, String name) {
        List<Map<String, Object>> getSupplierSalesman = billOfApMapper.supplierSalesman(userInfo,depId,stoCode,supSelfCode,name);
        return getSupplierSalesman;
    }

    /**
     * 供应商业务员汇总列表
     *
     * @param response 请求参数
     * @return
     */
    @Override
    public PageInfo assistantList(GaiaBillOfApResponse response) {
        //校验入参
        this.checkout(response);
        List<GaiaBillOfAp> billOfApList = this.billOfApMapper.assistantList(response);
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String, String> resource =new HashMap<>();
        //查询加盟商下的所有支付方式
        Map<String, String> fiApPayment = this.getPay(new GaiaBillOfApParam().setClient(response.getClient()));

        //地点
        if (StrUtil.isNotBlank(response.getDepId())) {
            List<Map<String, String>> dcList = comeAndGoManageMapper.getDcListByClient(response.getClient());
            if (CollectionUtil.isNotEmpty(dcList)) {
                dcList.forEach(x -> {
                    Map<String, String> resourceMap = x;
                    resource.put(resourceMap.get("DC_CODE"), resourceMap.get("DC_NAME"));
                });
            }
        }
        //门店
        if (StrUtil.isNotBlank(response.getStoCode())) {
            List<StoreOutData> storeList = this.gaiaStoreDataMapper.getStoreList(response.getClient(), "");
            if (CollectionUtil.isNotEmpty(storeList)) {
                storeList.forEach(x -> {
                    resource.put(x.getStoCode(), StrUtil.isBlank(x.getStoShortName()) ? x.getStoName() : x.getStoShortName());
                });
            }
        }

        if(CollUtil.isNotEmpty(billOfApList)){
            //分组
            Map<String, List<GaiaBillOfAp>> map = billOfApList.stream()
                    .collect(
                            Collectors.groupingBy(
                                    supplierDealOutData -> supplierDealOutData.getClient() +'-' + supplierDealOutData.getDcCode()  +'-' + supplierDealOutData.getSupSelfCode() + '-' + supplierDealOutData.getSalesmanId()
                            )
                    );

            //遍历map
            for (Map.Entry<String, List<GaiaBillOfAp>> ofAp : map.entrySet()) {
                LinkedHashMap<String,Object> responseMap = new LinkedHashMap<>();
                List<GaiaBillOfAp> value = ofAp.getValue();
                GaiaBillOfAp billOfAp = value.get(0);
                //地点
                responseMap.put("发生地点",billOfAp.getDcCode()+"-"+resource.get(billOfAp.getDcCode()));
                //供应商编码
                responseMap.put("供应商编码",billOfAp.getSupSelfCode()+"-"+billOfAp.getSupName());
                //业务员
                responseMap.put("业务员",billOfAp.getSalesmanId()+"-"+billOfAp.getSalesmanName());
                /**
                 * 期初金额
                 * 获取这个营业员的期初时间后一天  到 选择时间的前一天
                 */
                //期初金额
                BigDecimal beginningAmt = calculateBeginPeriodAssistant(response, billOfAp.getDcCode(), billOfAp.getSupSelfCode(),billOfAp.getSalesmanId());
                responseMap.put("期初金额",beginningAmt);
                Map<String, BigDecimal> decimalMap = amt(value);
                //期间发生金额
                responseMap.put("期间发生额",decimalMap.get("duringAmt"));
                //去税成本额
                responseMap.put("去税成本额",decimalMap.get("quTaxAmt"));
                //税成本额
                responseMap.put("税额",decimalMap.get("taxAmt"));
                //期间付款
                responseMap.put("期间付款",decimalMap.get("duringPayment"));
                //其他支付方式
                BigDecimal paySumAmt = this.otherPayment(responseMap, value, fiApPayment);
                //其他方式默认为0
                //期末金额
                responseMap.put("期末余额",beginningAmt.add(decimalMap.get("duringAmt")).add(paySumAmt).add(decimalMap.get("duringPayment")));
                list.add(responseMap);
            }
        }
        List<Map<String, Object>> responseMap = getResponseMap(response, resource, fiApPayment, billOfApList);
        list.addAll(responseMap);

        PageInfo pageInfo = new PageInfo();
        pageInfo.setList(list);
        pageInfo.setListNum(getTotal(list));
        return pageInfo;
    }


    /**
     * 获取不在时间范围内的供应商业务员
     * @param response
     * @return
     */
    private List<Map<String,Object>> getResponseMap(GaiaBillOfApResponse response,Map<String, String> resource,Map<String, String> fiApPayment,List<GaiaBillOfAp> billOfApList){
        response.setBeginTimeStr(null);
        response.setEndTimeStr(null);
        List<GaiaBillOfAp> ofApList = this.billOfApMapper.assistantList(response);

        List<Map<String,Object>> mapList = new ArrayList<>();
        if(CollUtil.isNotEmpty(ofApList)){
            if(CollUtil.isNotEmpty(billOfApList)){
                List<GaiaBillOfAp> ofAps = new ArrayList<>();
                for (GaiaBillOfAp billOfAp : billOfApList) {
                    ofApList.forEach(ofAp -> {
                        if(Objects.equals(billOfAp.getClient(),ofAp.getClient()) && Objects.equals(billOfAp.getDcCode(),ofAp.getDcCode()) && Objects.equals(billOfAp.getSupSelfCode(),ofAp.getSupSelfCode()) && Objects.equals(billOfAp.getSalesmanId(),ofAp.getSalesmanId())){
                            ofAps.add(ofAp);
                        }
                    });
                }
                ofApList.removeAll(ofAps);
            }
            Map<String, List<GaiaBillOfAp>> map = ofApList.stream()
                    .collect(
                            Collectors.groupingBy(
                                    supplierDealOutData -> supplierDealOutData.getClient() +'-' + supplierDealOutData.getDcCode()  +'-' + supplierDealOutData.getSupSelfCode() + '-' + supplierDealOutData.getSalesmanId()
                            )
                    );
            for (Map.Entry<String, List<GaiaBillOfAp>> entry : map.entrySet()) {
                LinkedHashMap<String,Object> responseMap = new LinkedHashMap<>();

                List<GaiaBillOfAp> value = entry.getValue();
                GaiaBillOfAp billOfAp = value.get(0);
                //地点
                responseMap.put("发生地点",billOfAp.getDcCode()+"-"+resource.get(billOfAp.getDcCode()));
                //供应商编码
                responseMap.put("供应商编码",billOfAp.getSupSelfCode()+"-"+billOfAp.getSupName());
                //业务员
                responseMap.put("业务员",billOfAp.getSalesmanId()+"-"+billOfAp.getSalesmanName());
                //期初金额
                BigDecimal beginningAmt = calculateBeginPeriodAssistant(response, billOfAp.getDcCode(), billOfAp.getSupSelfCode(),billOfAp.getSalesmanId());
                responseMap.put("期初金额",beginningAmt);
                //Map<String, BigDecimal> decimalMap = amt(value);
                //期间发生金额
                responseMap.put("期间发生额",BigDecimal.ZERO);
                //去税成本额
                responseMap.put("去税成本额",BigDecimal.ZERO);
                //税成本额
                responseMap.put("税额",BigDecimal.ZERO);
                //期间付款
                responseMap.put("期间付款",BigDecimal.ZERO);
                //其他支付方式
                save(responseMap,fiApPayment);
                //其他方式默认为0
                //期末金额
                responseMap.put("期末余额",beginningAmt);
                mapList.add(responseMap);
            }
        }
        return mapList;
    }

    private void save(LinkedHashMap<String,Object> responseMap,Map<String, String> fiApPayment){
        for (Map.Entry<String, String> entry : fiApPayment.entrySet()) {
            responseMap.put(entry.getValue(),BigDecimal.ZERO);
        }
    }


    /**
     * 获取其他的支付方式
     * @param responseMap
     * @param value
     * @param fiApPayment
     */
    private BigDecimal otherPayment(Map<String,Object> responseMap,List<GaiaBillOfAp> value,Map<String, String> fiApPayment){
        //根据支付方式分组
        Map<String, List<GaiaBillOfAp>> map = value.stream()
                .collect(Collectors.groupingBy(supplierDealOutData -> supplierDealOutData.getPaymentId()));
        BigDecimal sum = BigDecimal.ZERO;
        Map<String,Object> resp = new LinkedHashMap<>();
        for (Map.Entry<String, List<GaiaBillOfAp>> pay : map.entrySet()) {
            List<GaiaBillOfAp> payValue = pay.getValue();
            GaiaBillOfAp billOfAp = payValue.get(0);
            for (Map.Entry<String, String> payAmt : fiApPayment.entrySet()) {
                if(Objects.equals(billOfAp.getPaymentId(),payAmt.getKey())){
                    BigDecimal decimal = payValue.stream().map(amt -> amt.getAmountOfPayment()).reduce(BigDecimal.ZERO, BigDecimal::add);
                    resp.put(payAmt.getValue(),decimal);
                    sum = sum.add(decimal);
                }
            }
        }
        for (Map.Entry<String, String> entry : fiApPayment.entrySet()) {
            if(resp.containsKey(entry.getValue())){
                responseMap.put(entry.getValue(),resp.get(entry.getValue()));
            }else {
                responseMap.put(entry.getValue(),BigDecimal.ZERO);
            }
        }
        return sum;
    }



    /**
     * 获取期间发生金额 去税成本额
     * @param value
     * @return
     */
    private Map<String,BigDecimal> amt(List<GaiaBillOfAp> value){
        BigDecimal duringAmt = BigDecimal.ZERO;
        BigDecimal taxAmt = BigDecimal.ZERO;
        BigDecimal quTaxAmt = BigDecimal.ZERO;
        BigDecimal duringPayment = BigDecimal.ZERO;
        BigDecimal beginningAmt = BigDecimal.ZERO;
        Map<String,BigDecimal> map = new HashMap<>();
        if(CollUtil.isNotEmpty(value)){
            for (GaiaBillOfAp billOfAp : value) {
                if(Objects.equals(billOfAp.getPaymentId(),"9999")){
                    duringAmt = duringAmt.add(billOfAp.getAmountOfPayment());
                }else if(Objects.equals(billOfAp.getPaymentId(),"6666")){
                    duringPayment = duringPayment.add(billOfAp.getAmountOfPayment());
                }else if(Objects.equals(billOfAp.getPaymentId(),"8888")){
                    beginningAmt = beginningAmt.add(billOfAp.getAmountOfPayment());
                }
                taxAmt = taxAmt.add(billOfAp.getAmountOfTax());
                quTaxAmt = quTaxAmt.add(billOfAp.getAmountExcludingTax());
            }
        }
        map.put("duringAmt",duringAmt);
        map.put("taxAmt",taxAmt);
        map.put("quTaxAmt",quTaxAmt);
        map.put("duringPayment",duringPayment);
        map.put("beginningAmt",beginningAmt);
        return map;
    }



    /**
     * 获取期初
     *
     * @param
     * @param dcCode
     * @param supSelfCode
     * @param beginPeriodBillOfApMap
     * @return
     */
    private BigDecimal calculateBeginPeriodAssistant(GaiaBillOfApResponse response, String dcCode, String supSelfCode,String salesmanId) {
        //期初
        BigDecimal beginning = BigDecimal.ZERO;
        //期间发生
        BigDecimal period = BigDecimal.ZERO;
        //期间付款
        BigDecimal payment = BigDecimal.ZERO;
        //其他消费
        BigDecimal rest = BigDecimal.ZERO;
        //获取开始时间的前一天
        String startTime = DateUtil.formatDate(response.getBeginTime().minusDays(1));
        //查询明细表上个月之前的所有数据
        Example oaAp = new Example(GaiaBillOfAp.class);
        Example.Criteria oaApExa = oaAp.createCriteria();
        oaApExa.andEqualTo("client", response.getClient());
        oaApExa.andEqualTo("dcCode", dcCode);
        oaApExa.andEqualTo("supSelfCode", supSelfCode);
        oaApExa.andEqualTo("paymentId","8888");
        oaApExa.andIsNotNull("salesmanId");
        List<GaiaBillOfAp> billOfApList = billOfApMapper.selectByExample(oaAp);
        if(CollUtil.isNotEmpty(billOfApList)){
            GaiaBillOfAp billOfAp = billOfApList.get(0);
            if (Convert.toLong(billOfAp.getPaymentDate())>=Convert.toLong(DateUtil.formatDate(response.getBeginTime()))) {
                beginning = beginning.add(billOfAp.getAmountOfPayment());
            }
        }
        //计算这个营业员的期初
        List<GaiaBillOfAp> ofAps = billOfApMapper.getListOfAp(response.getClient(), dcCode, supSelfCode, startTime,salesmanId);
        if (CollUtil.isNotEmpty(ofAps)) {
            for (GaiaBillOfAp gaiaBillOfAp : ofAps) {
                if (gaiaBillOfAp.getPaymentId().equals("8888")) {
                    beginning = beginning.add(gaiaBillOfAp.getAmountOfPayment());
                }
                if (gaiaBillOfAp.getPaymentId().equals("9999")) {
                    period = period.add(gaiaBillOfAp.getAmountOfPayment());
                }
                if (gaiaBillOfAp.getPaymentId().equals("6666")) {
                    payment = payment.add(gaiaBillOfAp.getAmountOfPayment());
                }
                if (!(gaiaBillOfAp.getPaymentId().equals("9999")) && !(gaiaBillOfAp.getPaymentId().equals("8888")) && !(gaiaBillOfAp.getPaymentId().equals("6666"))) {
                    rest = rest.add(gaiaBillOfAp.getAmountOfPayment());
                }
            }
        }
        BigDecimal residualAmount = beginning.add(period).add(payment).add(rest);
        return residualAmount;
    }


    private void checkout(GaiaBillOfApResponse response) {
        if (StrUtil.isBlank(response.getDepId()) && StrUtil.isBlank(response.getStoCode())) {
            throw new BusinessException("仓库地点或门店编码必选其一");
        }
        if (Objects.isNull(response.getBeginTime())) {
            throw new BusinessException("开始日期不能为空");
        }
        if (Objects.isNull(response.getEndTime())) {
            throw new BusinessException("结束始日期不能为空");
        }
        response.setBeginTimeStr(DateUtil.formatDate(response.getBeginTime()));
        response.setEndTimeStr(DateUtil.formatDate(response.getEndTime()));
    }
    @Override
    public SXSSFWorkbook exportExcel(GaiaBillOfApResponse data, GetLoginOutData userInfo) {
        PageInfo pageInfo = assistantList(data);
        if(Objects.isNull(pageInfo) || CollUtil.isEmpty(pageInfo.getList())){
           throw new BusinessException("提示：导出失败, 无数据");
        }
        List<Map<String,Object>> list = (List<Map<String,Object>>)pageInfo.getList();
        LinkedHashMap<String, Object> listNum = (LinkedHashMap<String, Object>)pageInfo.getListNum();
        listNum.put("业务员",null);
        listNum.remove("发生地点");

        list.add(listNum);

        // 获取数据
        SXSSFWorkbook workbook = new SXSSFWorkbook(list.size() + 100);
        Sheet sheet = workbook.createSheet("供应商应付列表");

        Set<String> keys = list.get(0).keySet();
        //设置标题
        // 设置表头
        Row titleRow = sheet.createRow(0);
        int i = 0;
        for (String key : keys) {
            Cell cell = titleRow.createCell(i);
            cell.setCellValue(key);
            i++;
        }

        for (int m = 1; m <= list.size(); m++) {
            Row dataRow = sheet.createRow(m);
            Map<String, Object> map = list.get(m - 1);
            // 列数
            int increase = 0;

            //遍历map
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                Cell cell = dataRow.createCell(increase++);
                String reg = "-?[0-9]+.?[0-9]{0,}";
                boolean fal = ExcelUtils.tran(entry.getValue()).matches(reg);
                if(fal){
                    cell.setCellValue(new BigDecimal(ExcelUtils.tran(entry.getValue())).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
                }else {
                    cell.setCellValue(ExcelUtils.tran(entry.getValue()));
                }
            }
        }
        //自适应列宽
        ExcelUtils.setSizeColumn(sheet, keys.size());
        return workbook;
    }

    /**
     * 修改供应商期末余额
     */
    @Override
    public void updateEndingBalance() {

        List<GaiaBillOfAp> list = this.billOfApMapper.selectEndingBalance(DateUtil.formatDate(new Date()));
        if (CollUtil.isNotEmpty(list)) {
            for (GaiaBillOfAp billOfAp : list) {
                //修改金额
                this.billOfApMapper.updateEndingBalance(billOfAp);
            }
        }
    }
}

