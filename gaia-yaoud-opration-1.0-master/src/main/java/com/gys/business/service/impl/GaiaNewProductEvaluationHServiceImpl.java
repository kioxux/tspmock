package com.gys.business.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.GaiaEntNewProductEvaluationHMapper;
import com.gys.business.mapper.GaiaNewProductEvaluationDMapper;
import com.gys.business.mapper.GaiaNewProductEvaluationHMapper;
import com.gys.business.mapper.GaiaProductBusinessMapper;
import com.gys.business.mapper.entity.GaiaNewProductEvaluationD;
import com.gys.business.mapper.entity.GaiaNewProductEvaluationH;
import com.gys.business.mapper.entity.GaiaProductBusiness;
import com.gys.business.mapper.entity.GaiaSdPhysicalCounting;
import com.gys.business.service.GaiaNewProductEvaluationHService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.util.DateUtil;
import com.gys.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GaiaNewProductEvaluationHServiceImpl implements GaiaNewProductEvaluationHService {

    @Resource
    private GaiaNewProductEvaluationHMapper gaiaNewProductEvaluationHMapper;
    @Resource
    private GaiaNewProductEvaluationDMapper gaiaNewProductEvaluationDMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaEntNewProductEvaluationHMapper gaiaEntNewProductEvaluationHMapper;


    @Override
    @Transactional
    public void InsertGaiaNewProductEvaluation() {
        GaiaNewProductEvaluationInData inData = new GaiaNewProductEvaluationInData();
        //1.0 查询所有 需要创建新品评估单的商品
        List<GaiaNewProductEvaluationD> list = gaiaNewProductEvaluationHMapper.listProductionInfo(inData);
        //2.0 按加盟商+门店 分类
        if (!CollectionUtils.isEmpty(list)) {
            //按加盟商 分类
            Map<String, List<GaiaNewProductEvaluationD>> productions = list.stream().collect(Collectors.groupingBy(GaiaNewProductEvaluationD::getClient));
            Set<String> clientList = productions.keySet();
            Date now = new Date();
            for (String client : clientList) {
                List<GaiaNewProductEvaluationD> clientProduction = productions.get(client);
                //按门店分类
                Map<String, List<GaiaNewProductEvaluationD>> stoList = clientProduction.stream().collect(Collectors.groupingBy(GaiaNewProductEvaluationD::getStoreId));
                Set<String> keySet = stoList.keySet();
                for (String stoId : keySet) {
                    String clientId = "";
                    String stoCode = "";
                    List<GaiaNewProductEvaluationD> stos = stoList.get(stoId);
                    //查询单据号
                    String billCode = gaiaNewProductEvaluationHMapper.getBillCode(stos.get(0).getClient());
                    for (GaiaNewProductEvaluationD evaluationD : stos) {
                        clientId = evaluationD.getClient();
                        stoCode = evaluationD.getStoreId();
                        evaluationD.setBillCode(billCode);
                        evaluationD.setIsDelete(0);
                        evaluationD.setDealStatus(0);
                        evaluationD.setStatus(evaluationD.getSuggestedStatus());
                        evaluationD.setUpdateTime(now);
                        evaluationD.setCreateTime(now);
                        evaluationD.setUpdateUser("system");
                        evaluationD.setCreateUser("system");
                    }
                    GaiaNewProductEvaluationH evaluationH = new GaiaNewProductEvaluationH();
                    evaluationH.setClient(clientId);
                    evaluationH.setStoreId(stoCode);
                    evaluationH.setBillCode(billCode);
                    evaluationH.setBillDate(now);
                    evaluationH.setStatus(0);
                    evaluationH.setIsDelete(0);
                    evaluationH.setUpdateTime(now);
                    evaluationH.setUpdateUser("");
                    evaluationH.setCreateTime(now);
                    evaluationH.setCreateUser("");
                    gaiaNewProductEvaluationHMapper.insertInfo(evaluationH);
                    gaiaNewProductEvaluationDMapper.insertDetailList(stos);
                }
            }
        }

    }

    @Override
    public GaiaNewProductEvaluationOutData getBillByClientAndStorId(GaiaNewProductEvaluationInData inData) {
        if ("ENT".equals(inData.getType())) {
            //公司级新品评估
            GaiaNewProductEvaluationOutData outData = gaiaEntNewProductEvaluationHMapper.getBillByClientAndStorId(inData);
            return outData;
        } else {
            //门店级新品评估
            GaiaNewProductEvaluationOutData outData = gaiaNewProductEvaluationHMapper.getBillByClientAndStorId(inData);
            return outData;
        }
    }

    @Override
    public GaiaNewProductEvaluationOutData getBillInfoByBillCode(GaiaNewProductEvaluationInData inData) {
        GaiaNewProductEvaluationOutData outData = new GaiaNewProductEvaluationOutData();
        GaiaNewProductEvaluationInData param = new GaiaNewProductEvaluationInData();
        param.setClientId(inData.getClientId());
        param.setBillCode(inData.getBillCode());
        param.setStoreId(inData.getStoreId());
        List<GaiaProductEvaluationDetailOutData> detailList = gaiaNewProductEvaluationHMapper.listBillDetail(param);
        if (!CollectionUtils.isEmpty(detailList)) {
            outData.setDetailList(detailList);
            outData.setQty(detailList.size());
            outData.setLastDate(DateUtil.addDay(detailList.get(0).getBillDate(), "yyyy-MM-dd", 5));
            //设置建议数量
            Map<Integer, List<GaiaProductEvaluationDetailOutData>> map = detailList.stream().collect(Collectors.groupingBy(GaiaProductEvaluationDetailOutData::getSuggestedStatus));
            Set<Integer> keySet = map.keySet();
            for (Integer key : keySet) {
                List<GaiaProductEvaluationDetailOutData> gaiaProductEvaluationDetailOutData = map.get(key);
                if (key == 0) {
                    outData.setNQty(gaiaProductEvaluationDetailOutData.size());
                }
                if (key == 1) {
                    outData.setEQty(gaiaProductEvaluationDetailOutData.size());
                }
            }
            //设置以确定数量
            Map<Integer, List<GaiaProductEvaluationDetailOutData>> hMap = detailList.stream().collect(Collectors.groupingBy(GaiaProductEvaluationDetailOutData::getStatus));
            Set<Integer> hKeySet = hMap.keySet();
            for (Integer key : hKeySet) {
                List<GaiaProductEvaluationDetailOutData> gaiaProductEvaluationDetailOutData = hMap.get(key);
                if (key == 0) {
                    outData.setNCQty(gaiaProductEvaluationDetailOutData.size());
                }
                if (key == 1) {
                    outData.setECQty(gaiaProductEvaluationDetailOutData.size());
                }
            }
        }
        List<GaiaProductEvaluationDetailOutData> list = gaiaNewProductEvaluationHMapper.listBillDetail(inData);
        outData.setDetailList(list);
        return outData;
    }

    @Override
    @Transactional
    public GaiaNewProductEvaluationH updateDetailList(GaiaNewProductEvaluationOutData param, GetLoginOutData userInfo) {
        Date now = new Date();
        List<GaiaProductEvaluationDetailOutData> detailList = param.getDetailList();
        if (!CollectionUtils.isEmpty(detailList)) {
            param.setBillCode(detailList.get(0).getBillCode());
            List<GaiaNewProductEvaluationD> details = new ArrayList<>();
            //子表状态设为 用户所选择状态
            for (GaiaProductEvaluationDetailOutData detail : detailList) {
                GaiaNewProductEvaluationD evaluationD = new GaiaNewProductEvaluationD();
                evaluationD.setClient(detail.getClientId());
                evaluationD.setStoreId(detail.getStoreId());
                evaluationD.setBillCode(detail.getBillCode());
                evaluationD.setProSelfCode(detail.getProSelfCode());
                evaluationD.setStatus(detail.getStatus());
                evaluationD.setDealStatus(1);
                evaluationD.setUpdateUser(userInfo.getLoginName() + "|" + userInfo.getUserId());
                evaluationD.setUpdateTime(now);
                details.add(evaluationD);
                // 更新 GAIA_PRODUCT_BUSINESS  PRO_POSITION  Status 为0-》T 1-》H
                updateProductionBussness(detail);
            }
            gaiaNewProductEvaluationDMapper.updateList(details);
        } else {
            throw new BusinessException("请选择需要评估的明细！");
        }
        param.setStoreId(detailList.get(0).getStoreId());
        Integer count = gaiaNewProductEvaluationDMapper.getUnDealCount(param);
        GaiaNewProductEvaluationH head = new GaiaNewProductEvaluationH();
        head.setClient(userInfo.getClient());
        head.setStoreId(detailList.get(0).getStoreId());
        head.setBillCode(param.getBillCode());
        if (count == 0) {
            //主表状态设为已确认
            head.setUpdateUser(userInfo.getLoginName() + "|" + userInfo.getUserId());
            head.setUpdateTime(now);
            head.setStatus(1);
            gaiaNewProductEvaluationHMapper.updateInfo(head);
        } else {
            head.setStatus(0);
        }

        return head;
    }

    @Override
    public PageInfo listBillInfo(GaiaNewProductEvaluationOutData param, GetLoginOutData userInfo) {
        //获取参数
        int pageNum = param.getPageNum();
        int pageSize = param.getPageSize();
        //非空校验 如果为空则赋值默认值
        if(pageNum == 0) {
            pageNum = CommonConstant.PAGE_NUM;
        }
        if(pageSize == 0) {
            pageSize = CommonConstant.PAGE_SIZE;
        }
        //分页
        PageHelper.startPage(pageNum, pageSize);
        List<GaiaNewProductEvaluationOutData> dataList = gaiaNewProductEvaluationHMapper.listBillInfo(param);
        com.github.pagehelper.PageInfo memberPageList = new com.github.pagehelper.PageInfo(dataList);
        return memberPageList;
    }

    @Override
    public List<HashMap<String, String>> listBigClass(GaiaNewProductEvaluationOutData param) {
        return gaiaNewProductEvaluationHMapper.listBigClass(param);
    }

    @Override
    public void updateBillStatus() {
        List<GaiaProductEvaluationDetailOutData> detailList = gaiaNewProductEvaluationHMapper.listUnCheckBill();
        if (!CollectionUtils.isEmpty(detailList)) {

            //按加盟商 分类
            Map<String, List<GaiaProductEvaluationDetailOutData>> productions = detailList.stream().collect(Collectors.groupingBy(GaiaProductEvaluationDetailOutData::getClientId));
            Set<String> clientList = productions.keySet();
            Date now = new Date();
            for (String client : clientList) {
                List<GaiaProductEvaluationDetailOutData> clientProduction = productions.get(client);
                //按门店分类
                Map<String, List<GaiaProductEvaluationDetailOutData>> stoList = clientProduction.stream().collect(Collectors.groupingBy(GaiaProductEvaluationDetailOutData::getStoreId));
                Set<String> keySet = stoList.keySet();
                for (String stoId : keySet) {
                    String clientId = "";
                    String stoCode = "";
                    String billCode = "";
                    List<GaiaProductEvaluationDetailOutData> stos = stoList.get(stoId);
                    List<GaiaNewProductEvaluationD> list = new ArrayList<>();
                    for (GaiaProductEvaluationDetailOutData evaluationD : stos) {
                        GaiaNewProductEvaluationD d = new GaiaNewProductEvaluationD();
                        clientId = evaluationD.getClientId();
                        stoCode = evaluationD.getStoreId();
                        billCode = evaluationD.getBillCode();
                        d.setClient(clientId);
                        d.setStoreId(stoCode);
                        d.setBillCode(evaluationD.getBillCode());
                        d.setProSelfCode(evaluationD.getProSelfCode());
                        d.setDealStatus(1);
                        d.setUpdateTime(now);
                        d.setUpdateUser("system");
                        list.add(d);

                        // 更新 GAIA_PRODUCT_BUSINESS  PRO_POSITION  Status 为0-》T 1-》H
                        updateProductionBussness1(evaluationD);
                    }
                    GaiaNewProductEvaluationH evaluationH = new GaiaNewProductEvaluationH();
                    evaluationH.setClient(clientId);
                    evaluationH.setStoreId(stoCode);
                    evaluationH.setBillCode(billCode);
                    evaluationH.setStatus(1);
                    evaluationH.setUpdateTime(now);
                    evaluationH.setUpdateUser("system");
                    gaiaNewProductEvaluationHMapper.updateInfo(evaluationH);
                    gaiaNewProductEvaluationDMapper.updateList(list);
                }
            }
        }
    }

    private void updateProductionBussness(GaiaProductEvaluationDetailOutData evaluationD) {
        InvalidOutOfStockInData gaiaProductBusiness = new InvalidOutOfStockInData();
        gaiaProductBusiness.setClientId(evaluationD.getClientId());
        gaiaProductBusiness.setBrId(evaluationD.getStoreId());
        gaiaProductBusiness.setProductId(evaluationD.getProSelfCode());
        gaiaProductBusiness.setPosition(1 == evaluationD.getStatus() ? "Z" : "T");
        gaiaProductBusinessMapper.updateByProductId(gaiaProductBusiness);
    }

    private void updateProductionBussness1(GaiaProductEvaluationDetailOutData evaluationD) {
        InvalidOutOfStockInData gaiaProductBusiness = new InvalidOutOfStockInData();
        gaiaProductBusiness.setClientId(evaluationD.getClientId());
        gaiaProductBusiness.setBrId(evaluationD.getStoreId());
        gaiaProductBusiness.setProductId(evaluationD.getProSelfCode());
        gaiaProductBusiness.setPosition("Z");
        gaiaProductBusinessMapper.updateByProductId(gaiaProductBusiness);
    }


}
