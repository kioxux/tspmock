package com.gys.business.service.data;

import lombok.Data;

@Data
public class LoginParam {
    private String userName;
    private String password;
}
