package com.gys.business.service.data.saleTask;

import lombok.Data;

@Data
public class TaskSystemPushData {
    private String pushType;
    private String pushStatus;
    private String pushDate;
    private String taskId;
    private String planStatus;
}
