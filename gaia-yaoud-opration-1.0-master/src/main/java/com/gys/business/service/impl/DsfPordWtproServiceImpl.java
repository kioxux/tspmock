package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.DsfPordWtproMapper;
import com.gys.business.mapper.GaiaClSystemParaMapper;
import com.gys.business.mapper.GaiaSdMessageMapper;
import com.gys.business.mapper.GaiaSdWtproMapper;
import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.mapper.entity.GaiaSdWtpro;
import com.gys.business.service.DsfPordWtproService;
import com.gys.business.service.data.*;
import com.gys.common.enums.ClientParamsEnum;
import com.gys.common.enums.SdMessageTypeEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service("dsfPordWtproService")
public class DsfPordWtproServiceImpl implements DsfPordWtproService {

    @Resource
    private DsfPordWtproMapper dsfPordWtproMapper;

    @Resource
    private GaiaSdWtproMapper gaiaSdWtproMapper;

    @Resource
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;

    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;

    @Override
    public PageInfo<DsfPordWtproOutData> selectDsfPordWtproList(DsfPordWtproInData inData) {
        if(null != inData.getProSelfCode()&&inData.getProSelfCode().size() > 0){
            String [] proSelfCode = inData.getProSelfCode().get(0).split(",");
            List<String >  proSelfCodes = Arrays.stream(proSelfCode).map(String::trim).collect(Collectors.toList());
            inData.setProSelfCode(proSelfCodes);
        }
        PageHelper.startPage(inData.getPageNum(),inData.getPageSize());
        List<DsfPordWtproOutData> list = dsfPordWtproMapper.selectDsfPordWtproList(inData);
        PageInfo<DsfPordWtproOutData> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Transactional
    public void updateWtpro(DsfPordWtproModInData inData){
        if(null == inData.getList() || inData.getList().size() == 0){
            return;
        }
        for(DsfPordWtproModReqInData dsfPordWtproModReqInData : inData.getList()){
            GaiaSdWtpro gaiaSdWtpro = new GaiaSdWtpro();
            gaiaSdWtpro.setProDsfCode(dsfPordWtproModReqInData.getIdDsf());
            gaiaSdWtpro.setClient(inData.getClient());
            if(null == dsfPordWtproModReqInData.getId()){
                List<GaiaSdWtpro> gaiaSdWtproList = gaiaSdWtproMapper.getGaiaSdWtproByClientAndProDsfCode(gaiaSdWtpro);
                if(CollectionUtils.isNotEmpty(gaiaSdWtproList)){
                    dsfPordWtproModReqInData.setId(gaiaSdWtproList.get(0).getId());
                }
            }
            gaiaSdWtpro.setProDsfName(dsfPordWtproModReqInData.getDsfProName());
            gaiaSdWtpro.setProSelfCode(dsfPordWtproModReqInData.getProSelfCode());
            gaiaSdWtpro.setProRate(dsfPordWtproModReqInData.getProRate());
            if(null == dsfPordWtproModReqInData.getId()){
                //新增
                gaiaSdWtpro.setProSyncflag(0);
                gaiaSdWtpro.setStatus(1);
                gaiaSdWtpro.setProUpdateDate("");
                gaiaSdWtpro.setProUpdateTime("");
                dsfPordWtproMapper.saveSdWtsto(gaiaSdWtpro);
            }else{
                gaiaSdWtpro.setId(dsfPordWtproModReqInData.getId());
                GaiaSdWtpro sdWtsto = dsfPordWtproMapper.selectSdWtstoById(gaiaSdWtpro.getId());
                if(sdWtsto == null || !inData.getClient().equals(sdWtsto.getClient())){
                    continue;
                }
                if(gaiaSdWtpro.getProRate().equals(sdWtsto.getProRate())&&gaiaSdWtpro.getProSelfCode().equals(sdWtsto.getProSelfCode())){
                    continue;
                }
                gaiaSdWtpro.setProSyncflag(0);
                dsfPordWtproMapper.updateSdWtsto(gaiaSdWtpro);
            }
        }
    }

    @Override
    public PageInfo<DsfPordWtproOutData> selectDsfPordWtpsdList(DsfPordWtproInData inData) {
        PageHelper.startPage(inData.getPageNum(),inData.getPageSize());
        List<DsfPordWtproOutData> list = dsfPordWtproMapper.selectDsfPordWtpsdList(inData);
        return new PageInfo<>(list);
    }

    @Override
    public void autoMatchCodeNotice() {
        //查询配置未对码自动提醒的加盟商列表
        //1.0删除之前的消息
        gaiaSdMessageMapper.deleteMessageByType(SdMessageTypeEnum.RECEIPT_ARE_NOT_MATCHED.page);
        List<GaiaClSystemPara> clSystemParaList = gaiaClSystemParaMapper.getParaByGcspIdAndParaId(ClientParamsEnum.WTPSDD_NOT_MATCH_CODE_NOTICE.id,"1");
        for(GaiaClSystemPara gaiaClSystemPara : clSystemParaList){
            String client = gaiaClSystemPara.getClient();
            //查询此加盟商单据未对码的数量
            long num    = dsfPordWtproMapper.countDsfPordWtpsdList(client);
           if(num > 0 ){
               insertMessage(client,num);
           }
        }
    }

    private void insertMessage(String client, long num) {
        String voucherIdWeb = gaiaSdMessageMapper.selectNextVoucherId(client, "company");
        GaiaSdMessage sdMessageWeb= new GaiaSdMessage();
        sdMessageWeb.setGsmVoucherId(voucherIdWeb);
        sdMessageWeb.setClient(client);
        sdMessageWeb.setGsmId("product");
        sdMessageWeb.setGsmType(SdMessageTypeEnum.RECEIPT_ARE_NOT_MATCHED.code);
        sdMessageWeb.setGsmPage(SdMessageTypeEnum.RECEIPT_ARE_NOT_MATCHED.page);
        sdMessageWeb.setGsmPlatForm("WEB");
        sdMessageWeb.setGsmDeleteFlag("0");
        sdMessageWeb.setGsmArriveDate(DateUtil.format(DateUtil.date(),"yyyyMMdd"));
        sdMessageWeb.setGsmRemark("截至目前，在委托配送来货单中，共有"+"<font color='red'>"+num+"</font>"+"条未对码的商品信息，请尽快处理。");
        sdMessageWeb.setGsmArriveTime(DateUtil.format(DateUtil.date(),"HHmmss"));
        sdMessageWeb.setGsmFlag("N");
       sdMessageWeb.setGsmWarningDay("Y");
        gaiaSdMessageMapper.insertSelective(sdMessageWeb);
    }
}
