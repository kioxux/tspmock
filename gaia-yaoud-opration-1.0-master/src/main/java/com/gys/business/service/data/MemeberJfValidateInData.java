package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MemeberJfValidateInData {
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店
     */
    private String brId;
    /**
     * 订单号
     */
    private String billNo;
    /**
     * 会员卡号
     */
    private String cardNo;
    /**
     * 积分抵用金额
     */
    private BigDecimal integralAmt;
    /**
     * 积分换购的积分统计
     */
    private BigDecimal integralDhPoint;
}
