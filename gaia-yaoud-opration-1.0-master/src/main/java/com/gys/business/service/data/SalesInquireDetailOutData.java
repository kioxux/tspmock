package com.gys.business.service.data;

import lombok.Data;

@Data
public class SalesInquireDetailOutData {

    /**
     * 序号
     */
    private Integer indexDetail;

    /**
     * 加盟商
     */
    private String clientId;

    /**
     * 店号
     */
    private String salesDetailBrId;

    /**
     * 销售单号
     */
    private String salesDetailBillNo;

    /**
     * 商品编码
     */
    private String salesDetailProCode;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 剂型
     */
    private String proForm;

    /**
     * 行号
     */
    private String lineNum;

    /**
     * 批号
     */
    private String salesDetailBatchNo;

    /**
     * 有效期
     */
    private String validUntil;

    /**
     * 数量
     */
    private String qty;

    /**
     * 零售价
     */
    private String salesDetailRetailPrice;

    /**
     * 实收金额
     */
    private String salesDetailAmountReceived;

    /**
     * 厂家
     */
    private String factory;

    /**
     * 产地
     */
    private String origin;

    /**
     * 营业员
     */
    private String salesDetailPerson;

    /**
     * 医生
     */
    private String salesDetailDoctor;

    private String gssdMovTax;
}
