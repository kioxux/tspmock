package com.gys.business.service.data.mib;

import lombok.Data;

/**
 * @Author ：gyx
 * @Date ：Created in 17:09 2021/12/30
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Data
public class MibSXSZJSFormData {
    //姓名
    private String name;
    //个人编号
    private String psnNo;
    //证件号码
    private String credentialsNo;
    //人员类别
    private String personalCategory;
    //就诊号
    private String seekMedicalAdviceId;
    //结算号
    private String accountNo;
    //现账户余额
    private String personalAccountBalance;
    //原账户余额
    private String beforePersonalAccountBalance;
    //账户支付额
    private String personalAccountExpenditure;
    //现金支付额
    private String personalCashExpenditure;
    //合计支付（小写）
    private String totalPaySmall;
    //合计支付（大写）
    private String totalPayMax;
    //收款单位
    private String recAmtUnit;
    //收款人
    private String recEmp;
    //收款日期
    private String recAmtDate;
}
