package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/10/19
 */
@Data
public class GsppPriceNormalOutData implements Serializable {
    private static final long serialVersionUID = -3156201314695351794L;

    /**
     * 商品id
     */
    private String proId;

    /**
     * 零售价
     */
    private String gsppPriceNormal;
}
