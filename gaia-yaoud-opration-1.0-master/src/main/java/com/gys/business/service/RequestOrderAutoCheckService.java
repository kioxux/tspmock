package com.gys.business.service;

import com.gys.business.service.data.check.*;

import java.util.List;

/***
 * @desc:
 * @author: wangchuan
 * @createTime:
 **/
public interface RequestOrderAutoCheckService {

    void autoAuditRequestOrder();
}
