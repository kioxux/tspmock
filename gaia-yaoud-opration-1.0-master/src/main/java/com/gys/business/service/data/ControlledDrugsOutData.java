package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("管制药品销售信息")
public class ControlledDrugsOutData {
    @ApiModelProperty("销售单号")
    private String gssdBillNo;
    @ApiModelProperty("销售时间")
    private String gssdDate;
    @ApiModelProperty("商品编码")
    private String gssdProId;
    @ApiModelProperty("商品名")
    private String proName;
    @ApiModelProperty("规格")
    private String proSprce;
    @ApiModelProperty("数量")
    private String proQty;
    @ApiModelProperty("特殊药品购买人姓名")
    private String gssdSpecialmedName;
    @ApiModelProperty("特殊药品购买人性别")
    private String gssdSpecialmedSex;
    @ApiModelProperty("特殊药品购买人出生日期")
    private String gssdSpecialmedBirthday;
    @ApiModelProperty("特殊药品购买人身份证")
    private String gssdSpecialmedIdcard;
    @ApiModelProperty("特殊药品购买人手机号")
    private String gssdSpecialmedMobile;
    @ApiModelProperty("特殊药品购买人地址")
    private String gssdSpecialmedAddress;
    @ApiModelProperty("登记人员")
    private String gsshEmp;
}
