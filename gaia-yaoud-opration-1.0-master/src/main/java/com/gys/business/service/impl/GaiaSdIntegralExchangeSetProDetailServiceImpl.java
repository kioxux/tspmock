package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdIntegralExchangeSetProDetailMapper;
import com.gys.business.service.GaiaSdIntegralExchangeSetProDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author RULAISZ
* @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_PRO_DETAIL】的数据库操作Service实现
* @createDate 2021-11-29 09:48:10
*/
@Service
public class GaiaSdIntegralExchangeSetProDetailServiceImpl implements GaiaSdIntegralExchangeSetProDetailService{

    @Autowired
    private GaiaSdIntegralExchangeSetProDetailMapper gaiaSdIntegralExchangeSetProDetailMapper;

}
