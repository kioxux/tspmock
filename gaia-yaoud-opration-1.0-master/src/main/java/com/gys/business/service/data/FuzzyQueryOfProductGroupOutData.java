package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回实体
 *
 * @author xiaoyuan on 2020/9/29
 */
@Data
public class FuzzyQueryOfProductGroupOutData implements Serializable {
    private static final long serialVersionUID = 6251523874655490322L;

    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;

    @ApiModelProperty(value = "计量单位")
    private String proUnit;

    @ApiModelProperty(value = "序号")
    private Integer index;
}
