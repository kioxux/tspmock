package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetReplenishTotalData {
    @ApiModelProperty(value = "商品编码")
    private String proId;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "规格")
    private String proSpec;
    @ApiModelProperty(value = "生产厂家")
    private String proFactoryName;
    @ApiModelProperty(value = "计量单位")
    private String proUnit;
    @ApiModelProperty(value = "需求总量")
    private String needQty;
    @ApiModelProperty(value = "补货门店数")
    private String storeQty;
    @ApiModelProperty(value = "末次供应商")
    private String lastSup;
    @ApiModelProperty(value = "末次供应商编码")
    private String lastSupId;
    @ApiModelProperty(value = "末次进价")
    private String lastProPrice;
    @ApiModelProperty(value = "补货总量")
    private String totalQty;
    @ApiModelProperty(value = "补货供应商")
    private String sup;
    @ApiModelProperty(value = "补货供应商编码")
    private String supId;
    @ApiModelProperty(value = "补货价格")
    private String replenishPrice;
    @ApiModelProperty(value = "采购税率")
    private String rate;
    @ApiModelProperty(value = "30天销量")
    private String salesQty;
    @ApiModelProperty(value = "门店总库存")
    private String storeTotalStock;
    @ApiModelProperty(value = "平均成本")
    private String argProPrice;
    @ApiModelProperty(value = "零售价")
    private String proPrice;
    @ApiModelProperty(value = "毛利率")
    private String marginGross;

}
