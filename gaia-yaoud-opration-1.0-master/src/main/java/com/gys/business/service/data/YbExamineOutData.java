package com.gys.business.service.data;

import lombok.Data;

@Data
public class YbExamineOutData {
    private String CLIENT;
    private String GSEH_BR_ID;
    private String GSEH_DATE;
    private String GSEH_VOUCHER_ID;
    private String GSEH_FROM;
    private String SUP_NAME;
    private String GSED_PRO_ID;
    private String PRO_NAME;
    private String PRO_COMMONNAME;
    private String PRO_FORM;
    private String PRO_SPECS;
    private String PRO_FACTORY_CODE;
    private String PRO_FACTORY_NAME;
    private String PRO_UNIT;
    private String GSED_QUALIFIED_QTY;
    private String GSED_BATCH_NO;
    private String PRODUCTION_DATE;
    private String GSED_VALID_DATE;
    private String PO_PRICE;
    private String GSED_BATCH;
}
