package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class PoPriceProductHistoryInData implements Serializable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 门店编码
     */
    @ApiModelProperty(value = "门店编码")
    private String stoCode;

    /**
     * 商品自编码
     */
    @ApiModelProperty(value = "商品自编码")
    private String productSelfCode;

    /**
     * 供应商编码
     */
    @ApiModelProperty(value = "供应商编码")
    private String supplierCode;

    /**
     * 页码
     */
    @ApiModelProperty(value = "页码")
    private Integer pageNo;

    /**
     * 每页显示条数
     */
    @ApiModelProperty(value = "每页显示条数")
    private Integer pageSize;

    /**
     * 有效期
     */
    @ApiModelProperty(value = "有效期")
    private String validityDate;

    /**
     * 生产日期
     */
    @ApiModelProperty(value = "生产日期")
    private String madeDate;
}
