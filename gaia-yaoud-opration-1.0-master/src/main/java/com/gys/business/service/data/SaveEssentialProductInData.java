package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdStoreEssentialProductD;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SaveEssentialProductInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "创建人")
    private String userId;

    @ApiModelProperty(value = "数据来源日期")
    private String sourceDate;

    @ApiModelProperty(value = "铺货单号 为空表示保存 不为空表示更新")
    private String orderId;

    @ApiModelProperty(value = "必备商品列表")
    private List<GaiaSdStoreEssentialProductD> essentialProductList;


}
