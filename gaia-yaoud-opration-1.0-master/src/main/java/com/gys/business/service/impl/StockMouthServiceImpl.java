package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaDcData;
import com.gys.business.mapper.entity.GaiaFranchisee;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.StockMouth;
import com.gys.business.service.StockMouthCaculateStrategy;
import com.gys.business.service.StockMouthService;
import com.gys.business.service.data.ClientSiteChoose;
import com.gys.business.service.data.StockMouthCaculateCondition;
import com.gys.business.service.data.StockMouthRes;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.DateUtil;
import com.gys.util.ExcelUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class StockMouthServiceImpl implements StockMouthService {

    @Autowired
    private StockMouthMapper stockMouthMapper;

    @Autowired
    private GaiaMaterialDocMapper materialDocMapper;

    @Autowired
    private GaiaFranchiseeMapper franchiseeMapper;

    @Autowired
    private GaiaStoreDataMapper storeDataMapper;

    @Autowired
    private GaiaDcDataMapper gaiaDcDataMapper;

    @Resource
    CosUtils cosUtils;


    @Override
    public void caculateByCondition(StockMouthCaculateCondition condition) {

        if (CollectionUtil.isEmpty(condition.getClientSiteMap())) {
            throw new BusinessException("最少选择一个用户");
        }
        if (StrUtil.isBlank(condition.getChooseDate())) {
            throw new BusinessException("年月为必填项");
        }
        if (StrUtil.isBlank(condition.getCaculateType())) {
            throw new BusinessException("库存推算模式为必填项");
        }
        if ((!condition.QC.equals(condition.getCaculateType())) && (!condition.DQ.equals(condition.getCaculateType()))) {
            throw new BusinessException("非法库存模式输入");
        }
        String chooseDateTemp = condition.getChooseDate().replace("-","");
        condition.setChooseDate(chooseDateTemp);

        /**
         * 情况相对简单，采取简单处理，不进行context构建，伪策略模式实现
         */
        StockMouthCaculateStrategy strategy = null;
        if (condition.QC.equals(condition.getCaculateType())) {
            strategy = new StockMouthCaculateQCStrategy(stockMouthMapper, materialDocMapper, condition,true);
        } else if (condition.DQ.equals(condition.getCaculateType())) {
            strategy = new StockMouthCaculateDQStrategy(stockMouthMapper, materialDocMapper, condition);
        }
        //进行库存推算
        strategy.caculate();

    }

    @Override
    public PageInfo<StockMouth> getStockMouthListPage(StockMouthCaculateCondition inData) {
        if (StrUtil.isBlank(inData.getChooseDate())) {
            throw new BusinessException("必须选择年月");
        }
        //为前端处理
        Map<String,List<String>> clientSiteMap = inData.getClientSiteMap();
        if(CollectionUtil.isNotEmpty(clientSiteMap)){
            while(clientSiteMap.containsKey("")){
                clientSiteMap.remove("");
            }
            inData.setClientSiteMap(clientSiteMap);
        }

        String chooseDateTemp = inData.getChooseDate().replace("-","");
        inData.setChooseDate(chooseDateTemp);
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        PageInfo pageInfo;
        //处理inData，方便sql处理
        List<String> clients = new ArrayList<>();
        List<String> sites = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(inData.getClientSiteMap())) {



            inData.getClientSiteMap().forEach((x, y) -> {
                if(StrUtil.isNotBlank(x)){
                    clients.add(x);
                }
                if(CollectionUtil.isNotEmpty(y)){
                    sites.addAll(y);
                }
            });

        }
        if(CollectionUtil.isNotEmpty(clients)){
            inData.setClients(clients);
        }

        if(CollectionUtil.isNotEmpty(sites)){
            inData.setSites(sites);
        }

//        Example example = new Example(StockMouth.class);
//        Example.Criteria criteria = example.createCriteria().andEqualTo("gsmYearMonth", inData.getChooseDate());
//        if (CollectionUtil.isNotEmpty(inData.getClientSiteMap())) {
//            inData.getClientSiteMap().forEach((x, y) -> {
//                clients.add(x);
//                sites.addAll(y);
//            });
//            if (CollectionUtil.isNotEmpty(clients)) {
//                criteria.andIn("client", clients);
//            }
//            if (CollectionUtil.isNotEmpty(sites)) {
//                criteria.andIn("gsmSiteCode", sites);
//            }
//        }
//        example.orderBy(" lastUpdateTime ");
//        List<StockMouth> stockMouths = stockMouthMapper.selectByExample(example);
        List<StockMouthRes> stockMouths = stockMouthMapper.selectByCondition(inData);
        List<StockMouthRes> handleStockMouths = new ArrayList<>();
//        stockMouths.forEach(x->{
//            StockMouthRes stockMouthRes = new StockMouthRes();
//            BeanUtils.copyProperties(x,stockMouthRes);
//
//        });
        if (ObjectUtil.isNotEmpty(stockMouths)) {
            pageInfo = new PageInfo(stockMouths);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public ClientSiteChoose clientSite() {
        ClientSiteChoose res = new ClientSiteChoose();
        List<String> caculateTypes = new ArrayList<>();
        caculateTypes.add("QC");
        caculateTypes.add("DQ");
        res.setCaculateTypes(caculateTypes);
        List<ClientSiteChoose.KeyValue> clients = new ArrayList<>();
        Map<String, List<ClientSiteChoose.KeyValue>> clientSiteMap = new HashMap<>();
        List<String> allClients = new ArrayList<>();
        //查询所有的加盟商
        List<GaiaFranchisee> allFranchisees = franchiseeMapper.selectAll();
        if (CollectionUtil.isNotEmpty(allFranchisees)) {
            // 处理client数组信息
            for (GaiaFranchisee franchisee : allFranchisees) {
                ClientSiteChoose.KeyValue info = res.new KeyValue();
                info.setKey(franchisee.getClient());
                info.setValue(franchisee.getFrancName());
                clients.add(info);
                allClients.add(franchisee.getClient());
            }

            //处理对应关系数组
            //查询所有的门店
            Example exampleStore = new Example(GaiaStoreData.class);
            exampleStore.createCriteria().andEqualTo("stoStatus", "0");
            List<GaiaStoreData> storeDataList = storeDataMapper.selectByExample(exampleStore);
            if (CollectionUtil.isNotEmpty(storeDataList)) {
                for (GaiaStoreData storeData : storeDataList) {
                    String client = storeData.getClient();
                    ClientSiteChoose.KeyValue info = res.new KeyValue();
                    info.setKey(storeData.getStoCode());
                    info.setValue(storeData.getStoName());
                    if (clientSiteMap.containsKey(client)) {
                        List<ClientSiteChoose.KeyValue> keyValues = clientSiteMap.get(client);
                        keyValues.add(info);
                        clientSiteMap.put(client, keyValues);
                    } else {
                        List<ClientSiteChoose.KeyValue> keyValues = new ArrayList<>();
                        keyValues.add(info);
                        clientSiteMap.put(client, keyValues);
                    }

                }
            }

            //查询所有的仓库信息
            GaiaDcData gaiaDcData = new GaiaDcData();
            gaiaDcData.setDcStatus("0");
            List<GaiaDcData> dcDataList = gaiaDcDataMapper.findList(gaiaDcData);
            if (CollectionUtil.isNotEmpty(dcDataList)) {
                for (GaiaDcData dcData : dcDataList) {
                    String client = dcData.getClient();
                    ClientSiteChoose.KeyValue info = res.new KeyValue();
                    info.setKey(dcData.getDcCode());
                    info.setValue(dcData.getDcName());
                    if (clientSiteMap.containsKey(client)) {
                        List<ClientSiteChoose.KeyValue> keyValues = clientSiteMap.get(client);
                        keyValues.add(info);
                        clientSiteMap.put(client, keyValues);
                    } else {
                        List<ClientSiteChoose.KeyValue> keyValues = new ArrayList<>();
                        keyValues.add(info);
                        clientSiteMap.put(client, keyValues);
                    }

                }
            }

            res.setClients(clients);
            res.setClientSiteMap(clientSiteMap);
        }
        return res;
    }

    private String[] headList = {
            "加盟商名称",
            "地点",
            "期间年月",
            "商品编码",
            "商品名称",
            "规格",
            "厂家",
            "单位",
            "批次",
            "库存数量",
            "去税金额",
            "税金",
            "合计金额"
    };

    @Override
    public Result export(StockMouthCaculateCondition inData) {
        String chooseDateTemp = inData.getChooseDate().replace("-","");
        inData.setChooseDate(chooseDateTemp);
        //处理inData，方便sql处理
        List<String> clients = new ArrayList<>();
        List<String> sites = new ArrayList<>();
        //为前端处理
        Map<String,List<String>> clientSiteMap = inData.getClientSiteMap();
        if(CollectionUtil.isNotEmpty(clientSiteMap)){
            while(clientSiteMap.containsKey("")){
                clientSiteMap.remove("");
            }
            inData.setClientSiteMap(clientSiteMap);
        }
        if (CollectionUtil.isNotEmpty(inData.getClientSiteMap())) {
            inData.getClientSiteMap().forEach((x, y) -> {
                if(StrUtil.isNotBlank(x)){
                    clients.add(x);
                }
                if(CollectionUtil.isNotEmpty(y)){
                    sites.addAll(y);
                }
            });
        }
        inData.setClients(clients);
        inData.setSites(sites);
        List<StockMouthRes> stockMouths = stockMouthMapper.selectByCondition(inData);
        try {
            List<List<Object>> dataList = new ArrayList<>();
            if (CollectionUtil.isNotEmpty(stockMouths)) {
                for (StockMouthRes stockMouthRes : stockMouths) {
                    List<Object> item = new ArrayList<>();
                    dataList.add(item);
                    item.add(stockMouthRes.getFrancName());
                    item.add(stockMouthRes.getSiteName());
                    item.add(stockMouthRes.getYearMonths());
                    item.add(stockMouthRes.getProCode());
                    item.add(stockMouthRes.getProName());
                    item.add(stockMouthRes.getProSpecs());
                    item.add(stockMouthRes.getProFactoryName());
                    item.add(stockMouthRes.getProUnit());
                    item.add(stockMouthRes.getGsmBatch());
                    item.add(stockMouthRes.getGsmStockQty());
                    item.add(stockMouthRes.getGsmStockAmtWithOutTaxAmt());
                    item.add(stockMouthRes.getGsmTaxAmt());
                    item.add(stockMouthRes.getGsmStockAmt());
                }
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<Object>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("月度库存");
                    }});
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, "月度库存." + ExcelUtils.OFFICE_EXCEL_XLSX);
            bos.flush();
            bos.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void caculateByCron() {

        //根据当前的时间，取出当月时间
        String currentDate = DateUtil.getCurrentDate();
        String currentYearMonth = currentDate.substring(0, currentDate.length() - 2);

        //确定caculateType，判断方法为，首先根据client + site + yearMonth查询是否存在上月数据
        //如果存在上月数据，那么确定caculateType 为DQ
        //如果不存在上月数据，判断是client + site 维度是否存在QC数据
        //如果存在QC进行QC计算，不存在QC，那么将不参与计算

        Map<String, List<ClientSiteChoose.KeyValue>> initMap = this.clientSite().getClientSiteMap();
        List<StockMouthCaculateCondition> stockMouthCaculateConditionList = new ArrayList<>();
        List<StockMouthCaculateCondition> finalStockMouthCaculateConditionList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(initMap)) {
            initMap.forEach((x, y) -> {
                if (CollectionUtil.isNotEmpty(y)) {
                    for (ClientSiteChoose.KeyValue keyValue : y) {
                        //构建每个客户每个client推算的请求参数
                        StockMouthCaculateCondition condition = new StockMouthCaculateCondition();
                        Map<String, List<String>> clientSiteMap = new HashMap<>();
                        condition.setChooseDate(currentYearMonth);
                        //因为不确定是否存在一个客户先不同门店可能不同的情况，所有根据一个客户一个门店形式，依次循环处理
                        List<String> sites = new ArrayList<>();
                        sites.add(keyValue.getKey());
                        clientSiteMap.put(x, sites);
                        condition.setClientSiteMap(clientSiteMap);
                        stockMouthCaculateConditionList.add(condition);
                    }
                }
            });

            if (CollectionUtil.isNotEmpty(stockMouthCaculateConditionList)) {
                stockMouthCaculateConditionList.forEach(x -> {
                    String client = "";
                    String site = "";
                    String yearMonth = "";
                    Map<String, List<String>> clientSiteMap = x.getClientSiteMap();
                    List<String> sites = clientSiteMap.get(x);
                    for (String m : clientSiteMap.keySet()) {
                        client = m;
                        site = clientSiteMap.get(m).get(0);
                    }
                    //确定caculateType值
                    if (this.ifHasLastStockMonth(client, site, currentYearMonth)) {
                        //表示存在上月数据，那么采取月推算模式
                        x.setCaculateType("DQ");
                    } else {
                        x.setCaculateType("QC");
                    }
                    finalStockMouthCaculateConditionList.add(x);
                });
            }

            finalStockMouthCaculateConditionList.forEach(condition->{
                StockMouthCaculateStrategy strategy = null;
                if (condition.QC.equals(condition.getCaculateType())) {
                    strategy = new StockMouthCaculateQCStrategy(stockMouthMapper, materialDocMapper, condition,false);
                } else if (condition.DQ.equals(condition.getCaculateType())) {
                    strategy = new StockMouthCaculateDQStrategy(stockMouthMapper, materialDocMapper, condition);
                }
//                进行库存推算
                strategy.caculate();
            });

        }
    }


    private boolean ifHasLastStockMonth(String client, String site, String chooseYearMonth) {
        String lastYearMonth = DateUtil.getLastMonth(chooseYearMonth);
        boolean resFlag = false;
        Example example = new Example(StockMouth.class);
        example.createCriteria()
                .andEqualTo("client", client)
                .andEqualTo("gsmSiteCode", site)
                .andEqualTo("gsmYearMonth", lastYearMonth);
        int count = this.stockMouthMapper.selectCountByExample(example);
        if (count > 0) {
            resFlag = true;
        }
        return resFlag;
    }
}
