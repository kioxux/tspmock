package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.StoreEssentialProductService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.gys.common.response.Result;
import com.gys.feign.PurchaseService;
import com.gys.util.CommonUtil;
import com.gys.util.CosUtils;
import com.gys.util.ExcelUtils;
import com.gys.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author SunJiaNan
 * @version 1.0
 * @date 2021/10/18 10:18
 * @description 门店必备商品业务层
 */
@Service
@Slf4j
public class StoreEssentialProductServiceImpl implements StoreEssentialProductService {

    @Autowired
    private GaiaSdStoreEssentialProductDMapper storeEssentialProductDMapper;
    @Autowired
    private GaiaSdStoreEssentialProductHMapper storeEssentialProductHMapper;
    @Autowired
    private GaiaSdStoreEssentialProductParaMapper storeEssentialProductParaMapper;
    @Autowired
    private GaiaWmKuCunMapper wmKuCunMapper;
    @Autowired
    private GaiaStoreDataMapper storeDataMapper;
    @Autowired
    private GaiaSdReplenishHMapper replenishHMapper;
    @Autowired
    private GaiaSdReplenishDMapper replenishDMapper;
    @Autowired
    private GaiaClSystemParaMapper systemParaMapper;
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private CosUtils cosUtils;


    @Override
    public JsonResult getCalculationList(EssentialProductCalculationInData inData) {
        String client = inData.getClient();
        //查询商品排名
        List<StoreEssentialProductRankOutData> productRankGroupList = storeEssentialProductDMapper.getProductRankList(client);
        if(ValidateUtil.isEmpty(productRankGroupList)) {
            throw new BusinessException("提示：查询品类模型数据失败!");
        }
        Integer count = storeEssentialProductHMapper.getDisableFlag(client);
        if(!new Integer(0).equals(count)) {
            throw new BusinessException("提示：距上次铺货计算不足7日或有未处理的单据, 暂不可计算!");
        }

        //将list转为Mop  key为省市编码拼接字符串 value为对应的列表
        Map<String, List<StoreEssentialProductRankOutData>> provCityCodeMap = productRankGroupList.stream().collect(Collectors.groupingBy(item -> item.getProvCityCode()));

        //根据条件查询直营店
        List<StoreStockCountOutData> directList = storeEssentialProductDMapper.getDirectListByCondition(inData);
        //查询配置表数据
        // modify by jht 20211227:用户有配置数据取用户配置，否则取默认配置表
        List<GaiaSdStoreEssentialProductPara> allParaList = storeEssentialProductParaMapper.getAllIfClient(client);
        //将list转为map  key为店效级别 value为实体
        Map<Integer, GaiaSdStoreEssentialProductPara> paramMap = allParaList.stream()
                .collect(Collectors.toMap(GaiaSdStoreEssentialProductPara::getStoreLevel, item -> item));
        //获取配置参数
        Map<String, String> configParamMap = getConfigParam(client);
        //最终铺货列表
        List<StoreEssentialProductRankOutData> distributionList = new ArrayList<>();
        //遍历每家直营店
        for(StoreStockCountOutData storeStock : directList) {
            String storeId = storeStock.getStoreId();           //门店编码
            Integer storeLevel = storeStock.getStoreLevel();    //店效级别
            //判断配置列表里是否存在此店效级别  如果没有跳出本次循环
            if(!paramMap.containsKey(storeLevel)) {
                continue;
            }
            //获取到当前门店省市对应的排名列表 进行下一步处理
            List<StoreEssentialProductRankOutData> productRankList = provCityCodeMap.get(storeStock.getProvCityCode());
            //为了避免数据问题导致的报错， 如果使用省市编码获取为空 则跳出循环 不计算该门店
            if(ValidateUtil.isEmpty(productRankList)) {
                continue;
            }
            //使用店效级别 获取  店效级别对应的参数值
            GaiaSdStoreEssentialProductPara storeEssentialPara = paramMap.get(storeLevel);
            //药品综合排名位数(配置标准值)
            Integer comprehensiveRank = storeEssentialPara.getComprehensiveRank();
            //门店库存品项合格品项数(配置标准值)
            Integer stockQualifiedCount = storeEssentialPara.getStockQualifiedCount();
            //如果当前药品综合排名位数 小于 商品总数据 则可以被截取  否则 直接获取全部
            List<StoreEssentialProductRankOutData> essentialProductRankList = subList(productRankList, comprehensiveRank.intValue());

            //查询每家门店商品编码明细
            List<String> stockProList = storeEssentialProductDMapper.getStockProListByStore(client, storeId, comprehensiveRank);
            int stockCountInt = stockProList.size();     //现有库存品项数
            int stockQualifiedCountInt = stockQualifiedCount.intValue();    //合格品项数

            //判断是否需要触发铺货  如果 库存品项数 < 合格品项数，触发铺货   如果现有库存品项数 >= 合格品项数，不触发铺货
            if(stockCountInt < stockQualifiedCountInt) {   //触发铺货
                //差异列表
                List<StoreEssentialProductRankOutData> diffProductList = new ArrayList<>();
                for(StoreEssentialProductRankOutData essentialProductRank : essentialProductRankList) {
                    String proSelfCode = essentialProductRank.getProSelfCode();
                    //如果前400位商品编码中  在该门店库存品项编码中不存在 则统一处理
                    if(!stockProList.contains(proSelfCode)) {
                        diffProductList.add(essentialProductRank);
                    }
                }
                //步进值
                Integer stepValue = storeEssentialPara.getStepValue();
                //计算最终前XX位铺货数据
                int rankIndex = getProductRankIndex(stockCountInt, stockQualifiedCountInt, stepValue);
                //从差异中截取前XX位排名数据    如果当前list元素数量 大于 截取的长度， 则可以被截取， 否则直接截取全部元素
                List<StoreEssentialProductRankOutData> storeEssentialProductRankList = subList(diffProductList, rankIndex);
                //组装参数
                for(StoreEssentialProductRankOutData storeEssentialProduct : storeEssentialProductRankList) {
                    StoreEssentialProductRankOutData copyStoreEssentialProduct = new StoreEssentialProductRankOutData();
                    BeanUtil.copyProperties(storeEssentialProduct, copyStoreEssentialProduct);    //拷贝相同属性值

                    BigDecimal wmsCostPrice = storeEssentialProduct.getWmsCostPrice();   //获取仓库成本价
                    copyStoreEssentialProduct.setWmsCostPrice(wmsCostPrice);             //仓库成本价
                    copyStoreEssentialProduct.setStoreId(storeStock.getStoreId());       //门店编码
                    copyStoreEssentialProduct.setStoreName(storeStock.getStoreName());   //门店名称
                    copyStoreEssentialProduct.setStoreAttributeCode(storeStock.getStoreAttributeCode());    //门店属性编码
                    copyStoreEssentialProduct.setStoreAttribute(storeStock.getStoreAttribute());   //门店属性
                    copyStoreEssentialProduct.setStoreLevelCode(storeStock.getStoreLevel());       //门店属性编码
                    copyStoreEssentialProduct.setStoreLevel(storeStock.getStoreLevelDesc());       //店效级别
                    Integer distributionNumber = calculationNumber(wmsCostPrice, configParamMap);  //获取铺货量
                    copyStoreEssentialProduct.setAdviseNumber(distributionNumber);                 //建议铺货量
                    copyStoreEssentialProduct.setResultNumber(distributionNumber);                 //确认铺货量
                    distributionList.add(copyStoreEssentialProduct);
                }
            }
        }

        Map<String, Object> resultMap = new HashMap<>(2);
        resultMap.put("distributionList", distributionList);
        resultMap.put("sourceDate", storeEssentialProductDMapper.getLastMonth());
        return JsonResult.success(resultMap, "success");
    }


    /**
     * 计算铺货商品排名
     * @param stockCountInt   现有库存品项数
     * @param stockQualifiedCountInt   合格品项数
     * @param stepValue
     * @return
     */
    private int getProductRankIndex(int stockCountInt, int stockQualifiedCountInt, int stepValue) {
        //如果   合格品项数 - 现有库存品项数 > 步进值  则使用步进值   否则  使用 合格品项数 - 现有库存品项数 的差值
        int diffNumber = stockQualifiedCountInt - stockCountInt;
        return (diffNumber > stepValue) ? stepValue : diffNumber;
    }


    /**
     * 计算铺货量
     * @param wmsCostPrice  仓库成本价
     * @return
     */
    private Integer calculationNumber(BigDecimal wmsCostPrice, Map<String, String> configParamMap) {
        //参数配置表数据
        String intervalDay = configParamMap.get(CommonConstant.ESSENTIAL_PRODUCT_COST_PRICE_CRITICAL_KEY);               //门店必备满足度铺货量成本价界值
        Integer pushMaxValue = Integer.valueOf(configParamMap.get(CommonConstant.ESSENTIAL_PRODUCT_PUSH_MAX_KEY));       //门店必备满足度铺货量（≥成本价界值） 2
        Integer pushMinValue = Integer.valueOf(configParamMap.get(CommonConstant.ESSENTIAL_PRODUCT_PUSH_MIN_KEY));       //门店必备满足度铺货量（<成本价界值） 3
        //判断标志  100
        BigDecimal oneHundred = new BigDecimal(intervalDay);
        //如果成本价＜100  铺货量=3   成本价≥100  铺货量=2
        return (wmsCostPrice.compareTo(oneHundred) == -1) ? pushMinValue : pushMaxValue;
    }


    /**
     * 获取排名前XX位数据
     * @param essentialProductRankList 被截取的List
     * @param subRankIndex 截取的长度
     * @return
     */
    private List<StoreEssentialProductRankOutData> subList(List<StoreEssentialProductRankOutData> essentialProductRankList, int subRankIndex) {
        int totalSize = essentialProductRankList.size();
        //如果list的长度大于截取的长度 则可以被截取， 否则截取List的全部元素
        int subIndex = (totalSize > subRankIndex) ? subRankIndex : totalSize;
        return essentialProductRankList.subList(0, subIndex);
    }

    /**
     *
     * @param client
     * @return
     */
    private Map<String, String> getConfigParam(String client) {
        Map<String, String> configParamMap = new HashMap<>(16);
        //查询参数配置
        List<GaiaClSystemPara> paramList = systemParaMapper.getAuthByParaId(client, CommonConstant.ESSENTIAL_PRODUCT_AUTH_PARAM_PREFIX);
        //如果该加盟商未配置参数 则使用默认值
        if(ValidateUtil.isEmpty(paramList)) {
            configParamMap.put(CommonConstant.ESSENTIAL_PRODUCT_COST_PRICE_CRITICAL_KEY, "100");     //门店必备满足度铺货量成本价界值
            configParamMap.put(CommonConstant.ESSENTIAL_PRODUCT_PUSH_MAX_KEY, "2");                  //门店必备满足度铺货量（≥成本价界值）
            configParamMap.put(CommonConstant.ESSENTIAL_PRODUCT_PUSH_MIN_KEY, "3");                  //门店必备满足度铺货量（<成本价界值）
        } else {
            //如果有配置 则检查每个配置是否存在
            configParamMap = paramList.stream().collect(Collectors.toMap(GaiaClSystemPara::getGcspId, GaiaClSystemPara::getGcspPara1));
            //门店必备满足度铺货量成本价界值
            if(ValidateUtil.isEmpty(configParamMap.get(CommonConstant.ESSENTIAL_PRODUCT_COST_PRICE_CRITICAL_KEY))) {
                configParamMap.put(CommonConstant.ESSENTIAL_PRODUCT_COST_PRICE_CRITICAL_KEY, "100");
            }
            //门店必备满足度铺货量（≥成本价界值）
            if(ValidateUtil.isEmpty(configParamMap.get(CommonConstant.ESSENTIAL_PRODUCT_PUSH_MAX_KEY))) {
                configParamMap.put(CommonConstant.ESSENTIAL_PRODUCT_PUSH_MAX_KEY, "2");
            }
            //门店必备满足度铺货量（<成本价界值）
            if(ValidateUtil.isEmpty(configParamMap.get(CommonConstant.ESSENTIAL_PRODUCT_PUSH_MIN_KEY))) {
                configParamMap.put(CommonConstant.ESSENTIAL_PRODUCT_PUSH_MIN_KEY, "3");
            }
        }
        return configParamMap;
    }



    @Override
    @Transactional
    public JsonResult saveOrUpdate(SaveEssentialProductInData inData) {
        //获取参数
        String client = inData.getClient();
        String orderId = inData.getOrderId();
        String sourceDate = inData.getSourceDate();
        List<GaiaSdStoreEssentialProductD> essentialProductList = inData.getEssentialProductList();
        //校验不能为空
        if(ValidateUtil.isEmpty(essentialProductList)) {
            throw new BusinessException("提示：保存数据不能为空！");
        }
        if(ValidateUtil.isEmpty(sourceDate)) {
            throw new BusinessException("提示：数据来源不能为空！");
        }

        Date date = new Date();
        String nextOrderId = "";
        //如果单号为空 则表示要保存  否则表示 更新
        if(ValidateUtil.isEmpty(orderId)) {
            //生成铺货单号
            nextOrderId = storeEssentialProductHMapper.getNextOrderId(client);
            inData.setOrderId(nextOrderId);
            saveEssentialProduct(inData, "0", date);
        } else {
            updateEssentialProduct(inData, date);
        }
        return JsonResult.success(nextOrderId, "success");
    }


    /**
     * 更新主表以及明细表
     * @param date
     */
    private void updateEssentialProduct(SaveEssentialProductInData inData, Date date) {
        String client = inData.getClient();
        String userId = inData.getUserId();
        String orderId = inData.getOrderId();
        //更新主表
        updateEssentialProductH(inData, "", date);

        List<GaiaSdStoreEssentialProductD> essentialProductList = inData.getEssentialProductList();
        List<GaiaSdStoreEssentialProductD> confirmEssentialProductList = checkDataRule(client, essentialProductList);
        //更新明细表数据
        for(GaiaSdStoreEssentialProductD essentialProductD : confirmEssentialProductList) {
            if(ValidateUtil.isEmpty(essentialProductD.getStoreId()) || ValidateUtil.isEmpty(essentialProductD.getProSelfCode())
                    || ValidateUtil.isEmpty(essentialProductD.getResultNumber())) {
                throw new BusinessException("提示：参数错误, 无法修改!");
            }
            essentialProductD.setClient(client);
            essentialProductD.setOrderId(orderId);
            essentialProductD.setUpdateTime(date);
            essentialProductD.setUpdateUser(userId);
            storeEssentialProductDMapper.updateProductByCondition(essentialProductD);
        }
    }


    /**
     * 更新主表
     * @param inData
     * @param flag
     * @param date
     */
    private void updateEssentialProductH(SaveEssentialProductInData inData, String flag, Date date) {
        //校验单号是否存在
        GaiaSdStoreEssentialProductH orderExists = new GaiaSdStoreEssentialProductH();
        orderExists.setClient(inData.getClient());
        orderExists.setOrderId(inData.getOrderId());
        GaiaSdStoreEssentialProductH orderInfo = storeEssentialProductHMapper.getOrderInfo(orderExists);
        if(ValidateUtil.isEmpty(orderInfo)) {
            throw new BusinessException("提示：该铺货单号不存在, 请检查");
        }
        //更新主表
        GaiaSdStoreEssentialProductH essentialProductH = new GaiaSdStoreEssentialProductH();
        essentialProductH.setClient(inData.getClient());
        essentialProductH.setOrderId(inData.getOrderId());

        //flag为空 表示更新请求  flag不为空 表示铺货确认
        if(ValidateUtil.isEmpty(flag)) {
            essentialProductH.setUpdateTime(date);
            essentialProductH.setUpdateUser(inData.getUserId());
        } else {
            essentialProductH.setStatus("1");
            essentialProductH.setConfirmTime(date);
            essentialProductH.setConfirmUser(inData.getUserId());
        }
        storeEssentialProductHMapper.updateStatusByOrderId(essentialProductH);
    }


    /**
     * 保存主表以及明细表
     * @param inData
     * @param status
     * @param date
     * @return
     */
    private List<GaiaSdStoreEssentialProductD> saveEssentialProduct(SaveEssentialProductInData inData, String status, Date date) {
        //获取参数
        String client = inData.getClient();
        String userId = inData.getUserId();
        String nextOrderId = inData.getOrderId();
        List<GaiaSdStoreEssentialProductD> essentialProductList = inData.getEssentialProductList();
        //保存主表
        GaiaSdStoreEssentialProductH essentialProductH = new GaiaSdStoreEssentialProductH();
        essentialProductH.setClient(client);
        essentialProductH.setOrderId(nextOrderId);
        essentialProductH.setDistributionProCount(essentialProductList.size());
        //门店数去重
        List<GaiaSdStoreEssentialProductD> storeDistinctList = essentialProductList.stream().collect(Collectors
                .collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdStoreEssentialProductD::getStoreId))), ArrayList::new));
        essentialProductH.setDistributionStoreCount(storeDistinctList.size());
        essentialProductH.setSourceDate(inData.getSourceDate());
        essentialProductH.setStatus(status);            //0-已保存 1-已铺货
        essentialProductH.setCreateTime(date);
        essentialProductH.setCreateUser(userId);
        essentialProductH.setUpdateTime(date);
        essentialProductH.setUpdateUser(userId);
        if("1".equals(status)) {
            essentialProductH.setConfirmTime(date);
            essentialProductH.setConfirmUser(userId);
        }
        storeEssentialProductHMapper.insert(essentialProductH);

        //保存明细表
        for(GaiaSdStoreEssentialProductD essentialProductD : essentialProductList) {
            if(ValidateUtil.isEmpty(essentialProductD.getProSelfCode()) || ValidateUtil.isEmpty(essentialProductD.getResultNumber())
                    || ValidateUtil.isEmpty(essentialProductD.getStoreAttributeCode()) || ValidateUtil.isEmpty(essentialProductD.getStoreId())
                    || ValidateUtil.isEmpty(essentialProductD.getStoreLevelCode()) || ValidateUtil.isEmpty(essentialProductD.getWmsCostPrice())) {
                throw new BusinessException("提示：参数错误, 无法保存!");
            }
            essentialProductD.setClient(client);
            essentialProductD.setOrderId(nextOrderId);
            essentialProductD.setReplenishOrderId("");
            essentialProductD.setStoreAttribute(essentialProductD.getStoreAttributeCode());     //门店属性将中文转换成码值
            essentialProductD.setStoreLevel(essentialProductD.getStoreLevelCode());             //将店效级别转换成码值
            essentialProductD.setCreateTime(date);
            essentialProductD.setCreateUser(userId);
            essentialProductD.setUpdateTime(date);
            essentialProductD.setUpdateUser(userId);
        }

        //标记错误数据
        List<GaiaSdStoreEssentialProductD> confirmEssentialProductList = checkDataRule(client, essentialProductList);
        storeEssentialProductDMapper.batchInsert(confirmEssentialProductList);
        return confirmEssentialProductList;
    }


    /**
     * 标记过滤掉的数据
     * @param client
     * @param essentialProductList
     * @return
     */
    private List<GaiaSdStoreEssentialProductD> checkDataRule(String client, List<GaiaSdStoreEssentialProductD> essentialProductList) {
        //不允许铺货商品查询  门店号+商品自编码拼接字符串
        List<String> notAllowList = storeEssentialProductDMapper.getNotAllowList(client);
        //查询门店是否闭店
        List<GaiaStoreData> storeDataList = storeDataMapper.findList(client);

        Map<String, String> storeDataMap = null;
        if(ValidateUtil.isNotEmpty(storeDataList)) {
            //将门店列表转为Map  key为门店编码 value为是否闭店
            storeDataMap = storeDataList.stream().collect(Collectors.toMap(GaiaStoreData::getStoCode, GaiaStoreData::getStoStatus));
        }

        //查询门店经营范围
        List<StoreJyfwOutData> storeJyfwList = storeEssentialProductDMapper.getStoreJyfwByClient(client);
        Map<String, List<StoreJyfwOutData>> storeJyfwIdMap = null;
        if(ValidateUtil.isNotEmpty(storeJyfwList)) {
            //将门店经营范围按经营范围分组  key为门店编码 value为经营范围List
            storeJyfwIdMap = storeJyfwList.stream().collect(Collectors.groupingBy(item -> item.getOrgId()));
        }

        //查询每个商品的经营类别
        List<StoreProJylbOutData> productJylbList = storeEssentialProductDMapper.getProductJylbByClient(client);
        //将商品类别转为Map  key为 门店编码+商品编码拼接字符串 value为经营范围
        Map<String, String> productJylbMap = productJylbList.stream().collect(Collectors.toMap(StoreProJylbOutData::getStoreProCodeStr, StoreProJylbOutData::getJylb));
        //查询加盟商下所有门店+商品 拼接为字符串
        List<String> storeProductList = storeEssentialProductDMapper.getProductListByClient(client);

        List<GaiaSdStoreEssentialProductD> confirmEssentialProductList = new ArrayList<>();
        for(GaiaSdStoreEssentialProductD essentialProductD : essentialProductList) {
            String storeId = essentialProductD.getStoreId();
            String proSelfCode = essentialProductD.getProSelfCode();
            Integer resultNumber = essentialProductD.getResultNumber();    //确认铺货量

            String isRule = "0";   //是否被过滤  0-没被过滤  1-被过滤
            //标记不允许铺货的数据   如果此数据在不允许铺货列表中 则不允许铺货
            String storeProCodeStr = storeId + proSelfCode;
            if(notAllowList.contains(storeProCodeStr)) {
                isRule = "1";
            }
            //标记铺货量 小于等于 0的数据
            if(resultNumber <= 0) {
                isRule = "1";
            }
            //标记闭店门店的数据
            if(ValidateUtil.isNotEmpty(storeDataMap)) {
                String stoStatus = storeDataMap.get(storeId);    //是否闭店  1-闭店
                if(!"0".equals(stoStatus)) {
                    isRule = "1";
                }
            }
            //标记门店商品没有经营范围的数据
            if(ValidateUtil.isNotEmpty(storeJyfwIdMap)) {
                List<StoreJyfwOutData> jyfwIdList = storeJyfwIdMap.get(storeId);
                if(ValidateUtil.isNotEmpty(jyfwIdList)) {
                    String proJyfw = productJylbMap.get(storeProCodeStr);     //获取门店+商品编码 经营范围
                    if(!jyfwIdList.contains(proJyfw)) {
                        isRule = "1";
                    }
                }
            }
            //标记门店不存在的商品数据
            if(!storeProductList.contains(storeProCodeStr)) {
                isRule = "1";
            }

            essentialProductD.setIsRule(isRule);
            confirmEssentialProductList.add(essentialProductD);
        }
        return confirmEssentialProductList;
    }



    @Override
    @Transactional
    public JsonResult confirm(SaveEssentialProductInData inData) {
        //获取参数
        String client = inData.getClient();
        String userId = inData.getUserId();
        String orderId = inData.getOrderId();
        List<GaiaSdStoreEssentialProductD> essentialProductList = inData.getEssentialProductList();
        //校验明细列表不能为空
        if(ValidateUtil.isEmpty(essentialProductList)) {
            throw new BusinessException("提示：保存不能为空！");
        }

        List<GaiaSdStoreEssentialProductD> distributionList = new ArrayList<>();
        Date date = new Date();
        if (ValidateUtil.isEmpty(orderId)) {
            orderId = storeEssentialProductHMapper.getNextOrderId(client);
            inData.setOrderId(orderId);
            //必备商品保存
            distributionList = saveEssentialProduct(inData, "1", date);
        } else {
            //查询数据库中是否被过滤状态
            List<StoreEssentialProductOutData> storeEssentialProductList = storeEssentialProductDMapper.getStoreEssentialProductByOrderId(client, orderId);
            Map<String, String> storeProCodeStrMap = storeEssentialProductList.stream()
                    .collect(Collectors.toMap(StoreEssentialProductOutData::getStoreProCodeStr, StoreEssentialProductOutData::getIsRule));
            //更新主表
            updateEssentialProductH(inData, "1", date);

            //标记错误数据
            List<GaiaSdStoreEssentialProductD> confirmEssentialProductList = checkDataRule(client, essentialProductList);
            //更新明细表数据
            for (GaiaSdStoreEssentialProductD essentialProductD : confirmEssentialProductList) {
                String storeId = essentialProductD.getStoreId();
                String proSelfCode = essentialProductD.getProSelfCode();
                if (ValidateUtil.isEmpty(storeId) || ValidateUtil.isEmpty(proSelfCode) || ValidateUtil.isEmpty(essentialProductD.getResultNumber())) {
                    throw new BusinessException("提示：参数错误, 无法修改!");
                }
                String dbIsRule = storeProCodeStrMap.get(storeId + proSelfCode);
                if (ValidateUtil.isEmpty(dbIsRule)) {
                    throw new BusinessException("提示：该铺货单中无此商品信息, 请确认!");
                }
                String isRule = essentialProductD.getIsRule();   //获取标记数据
                //修改标志
                String updateFlag = essentialProductD.getUpdateFlag();
                if ("1".equals(updateFlag) || !dbIsRule.equals(isRule)) {
                    essentialProductD.setClient(client);
                    essentialProductD.setOrderId(orderId);
                    essentialProductD.setUpdateTime(date);
                    essentialProductD.setUpdateUser(userId);
                    storeEssentialProductDMapper.updateProductByCondition(essentialProductD);
                }
                distributionList.add(essentialProductD);
            }
        }
        //生成补货单
        List<GaiaSdReplenishHExt> replenishHList = saveReplenishOrder(client, userId, distributionList);
        //回写补货单号至铺货明细表中
        updateReplenishOrderId(replenishHList, orderId);

        //Feign远程调用 自动开单接口
        //automaticBilling(replenishHList);
        return JsonResult.success(orderId, "success");
    }


    /**
     * 生成补货单
     * @param client
     * @param userId
     * @param distributionList 补货明细列表
     * @return
     */
    private List<GaiaSdReplenishHExt> saveReplenishOrder(String client, String userId, List<GaiaSdStoreEssentialProductD> distributionList) {
        //只保留isRule为0的数据 并且按门店编码分组
        Map<String, List<GaiaSdStoreEssentialProductD>> storeEssentialProductMap = distributionList.stream()
                .filter(item -> "0".equals(item.getIsRule())).collect(Collectors.groupingBy(GaiaSdStoreEssentialProductD::getStoreId));

        //成本价 税率  商品列表
        List<CostPriceAndTaxOutData> proCostPriceByProList = storeEssentialProductDMapper.getProCostPriceByProList(client, distributionList);
        Map<String, BigDecimal> proCostPriceMap = new HashMap<>(16);
        Map<String, String> proTaxValueMap = new HashMap<>(16);
        if(ValidateUtil.isNotEmpty(proCostPriceByProList)) {
            //转为Map  key为门店编码拼接商品自编码 value为成本价
            proCostPriceMap = proCostPriceByProList.stream().collect(Collectors.toMap(CostPriceAndTaxOutData::getStoreProSelfCodeStr, CostPriceAndTaxOutData::getCostPrice));
            //转为Map  key为门店编码拼接商品自编码 value为税率
            proTaxValueMap = proCostPriceByProList.stream().collect(Collectors.toMap(CostPriceAndTaxOutData::getStoreProSelfCodeStr, CostPriceAndTaxOutData::getTaxCodeValue));
        }

        //查询配送中心编码
        List<String> dcCodeList = wmKuCunMapper.getDcCodeList(client);
        if(ValidateUtil.isEmpty(dcCodeList)) {
            throw new BusinessException("提示：该加盟商下无配送中心, 无法铺货至门店!");
        }

        //查询门店配送方式
        List<GaiaStoreData> storeDataList = storeDataMapper.getAllByClient(client);
        Map<String, String> storeDataMap = new HashMap<>(16);
        if(ValidateUtil.isNotEmpty(storeDataList)) {
            //转为Map  key为门店编码 value为配送方式
            storeDataMap = storeDataList.stream().collect(Collectors.toMap(GaiaStoreData::getStoCode,
                    item -> ObjectUtil.defaultIfBlank(item.getStoDeliveryMode(), "2")));
        }

        List<GaiaSdReplenishHExt> replenishHList = new ArrayList<>();
        List<GaiaSdReplenishD> replenishDList = new ArrayList<>();
        //组装数据
        for(String storeId : storeEssentialProductMap.keySet()) {
            String maxVoucherId = getMaxVoucherId(client);      //铺货单号
            List<GaiaSdStoreEssentialProductD> storeEssentialProductList = storeEssentialProductMap.get(storeId);
            Integer totalResultNumber = storeEssentialProductList.stream().mapToInt(GaiaSdStoreEssentialProductD::getResultNumber).sum();    //铺货总数
            //总金额
            BigDecimal totalAmt = new BigDecimal(0);
            //计算总金额
            for(GaiaSdStoreEssentialProductD storeEssentialProductD : storeEssentialProductList) {
                String storeProSelfCode = storeEssentialProductD.getStoreId() + storeEssentialProductD.getProSelfCode();    //门店编码拼接商品自编码
                Integer resultNumber = storeEssentialProductD.getResultNumber();     //铺货数量
                BigDecimal costPrice = proCostPriceMap.get(storeProSelfCode);    //获取成本价
                if(BigDecimal.ZERO.compareTo(costPrice) != 0 && ValidateUtil.isNotEmpty(costPrice)) {
                    totalAmt = totalAmt.add(new BigDecimal(resultNumber).multiply(costPrice));
                }
            }
            //补货主表
            GaiaSdReplenishHExt replenishH = new GaiaSdReplenishHExt();
            replenishH.setClient(client);                  //加盟商
            replenishH.setGsrhBrId(storeId);               //门店
            replenishH.setGsrhAddr(dcCodeList.get(0));     //铺货地点
            replenishH.setGsrhVoucherId(maxVoucherId);     //铺货单号
            replenishH.setGsrhDate(CommonUtil.getyyyyMMdd());  //铺货日期
            replenishH.setGsrhCreateTime(CommonUtil.getHHmmss());  //创建时间
            String stoDeliveryMode = storeDataMap.get(storeId);     //配送方式(1-自采；2-总部配送；3-委托配送，4-内部批发)
            //补货类型
            if("3".equals(stoDeliveryMode) || "4".equals(stoDeliveryMode)) {
                replenishH.setGsrhType(stoDeliveryMode);
            } else {
                replenishH.setGsrhType("2");
            }
            replenishH.setGsrhTotalAmt(totalAmt);                       //铺货总金额
            replenishH.setGsrhTotalQty(totalResultNumber.toString());   //铺货总数量
            replenishH.setGsrhEmp(userId);                       //铺货人员
            replenishH.setGsrhStatus("1");                       //是否审核
            replenishH.setGsrhPattern("2");                      //补货方式 0正常补货，1紧急补货,2铺货,3互调
            replenishH.setGsrhSource("4");                       //补货来源 1门店缺断货补充，2门店品类补充，3二次开单，4-必备品满足度铺货
            replenishHList.add(replenishH);

            // 行号 (同一单号下顺序排列)
            AtomicInteger index = new AtomicInteger(1);
            //补货明细表
            for(GaiaSdStoreEssentialProductD storeEssentialProductD : storeEssentialProductList) {
                GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
                replenishD.setClientId(client);
                replenishD.setGsrdBrId(storeId);
                replenishD.setGsrdVoucherId(maxVoucherId);
                replenishD.setGsrdSerial(String.valueOf(index.getAndIncrement()));
                replenishD.setGsrdProId(storeEssentialProductD.getProSelfCode());
                replenishD.setGsrdDate(CommonUtil.getyyyyMMdd());
                replenishD.setGsrdBatch("");
                replenishD.setGsrdBatchNo("");
                replenishD.setGsrdGrossMargin("");
                replenishD.setGsrdSalesDays1("0");
                replenishD.setGsrdSalesDays2("0");
                replenishD.setGsrdSalesDays3("0");
                replenishD.setGsrdProposeQty("0");
                replenishD.setGsrdStockStore("");
                replenishD.setGsrdStockDepot("");
                replenishD.setGsrdDisplayMin("");
                replenishD.setGsrdPackMidsize("");
                replenishD.setGsrdLastSupid("");
                replenishD.setGsrdLastPrice("0");
                replenishD.setGsrdEditPrice("0");
                replenishD.setGsrdNeedQty(String.valueOf(storeEssentialProductD.getResultNumber()));
                replenishD.setGsrdVoucherAmt(BigDecimal.ZERO);
                replenishD.setGsrdAverageCost(BigDecimal.ZERO);
                String storeProSelfCode = storeEssentialProductD.getStoreId() + storeEssentialProductD.getProSelfCode();    //门店编码拼接商品自编码
                String taxValue = proTaxValueMap.get(storeProSelfCode);    //税率
                replenishD.setGsrdTaxRate((ValidateUtil.isNotEmpty(taxValue) ? taxValue : ""));
                replenishDList.add(replenishD);
            }
        }
        if(ValidateUtil.isNotEmpty(replenishHList)) {
            replenishHMapper.saveBatch(replenishHList);
        }
        if(ValidateUtil.isNotEmpty(replenishDList)) {
            replenishDMapper.saveBatch(replenishDList);
        }
        return replenishHList;
    }


    /**
     * 获取最大铺货单号
     * @param client
     * @return
     */
    private String getMaxVoucherId(String client) {
        String maxVoucherId = "";
        while (true) {
            if (ValidateUtil.isEmpty(maxVoucherId)) {
                maxVoucherId = replenishHMapper.getMaxVoucherId(client);
                maxVoucherId = String.format("%06d", (Integer.parseInt(maxVoucherId) + 1));
            } else {
                maxVoucherId = String.format("%06d", (Integer.parseInt(maxVoucherId) + 1));
            }
            String keyStr = CommonConstant.REPLENISH_MAX_VOUCHER_ID_KEY + client + maxVoucherId;
            Object exists = redisManager.get(keyStr);
            log.info("补货单号:redis.getkey:{},value:{}", CommonConstant.REPLENISH_MAX_VOUCHER_ID_KEY + client + maxVoucherId, maxVoucherId);
            if (ValidateUtil.isEmpty(exists)) {
                redisManager.set(CommonConstant.REPLENISH_MAX_VOUCHER_ID_KEY + client + maxVoucherId, maxVoucherId, 60l);
                log.info("补货单号:redis.setkey:{},value:{}", CommonConstant.REPLENISH_MAX_VOUCHER_ID_KEY + client + maxVoucherId, maxVoucherId);
                return "PD" + LocalDate.now().getYear() + maxVoucherId;
            }
        }
    }


    /**
     * 回写补货单号至明细表
     * @param replenishHList
     * @param orderId
     */
    private void updateReplenishOrderId(List<GaiaSdReplenishHExt> replenishHList, String orderId) {
        for(GaiaSdReplenishHExt replenishH : replenishHList) {
            //使用加盟商  门店  以及铺货单号 回写补货单号至明细表
            GaiaSdStoreEssentialProductD essentialProductD = new GaiaSdStoreEssentialProductD();
            essentialProductD.setClient(replenishH.getClient());
            essentialProductD.setStoreId(replenishH.getGsrhBrId());
            essentialProductD.setReplenishOrderId(replenishH.getGsrhVoucherId());   //补货单号
            essentialProductD.setOrderId(orderId);
            storeEssentialProductDMapper.updateReplenishOrderId(essentialProductD);
        }
    }


    /**
     * 自动开单
     * @param replenishHList
     */
    private void automaticBilling(List<GaiaSdReplenishHExt> replenishHList) {
        JsonResult result = null;
        try {
            log.info("开始调用自动开单接口：" + JSONObject.toJSONString(replenishHList));
            result = purchaseService.automaticBilling(replenishHList);
            Integer code = result.getCode();
            if(0 == code) {
                log.info("调用自动开单成功,返回值: {}", JSONObject.toJSONString(result));
            } else {
                log.info("调用自动开单失败,返回值: {}", JSONObject.toJSONString(result));
            }
        } catch (Exception e) {
            log.error("自动开单失败:", e);
            throw new BusinessException("提示：自动开单失败！");
        }
    }


    @Override
    public JsonResult getOrderList(EssentialProductCalculationInData inData) {
        List<EssentialProductOrderOutData> essentialProductOrderList = storeEssentialProductHMapper.getEssentialProductOrderList(inData);
        return JsonResult.success(essentialProductOrderList, "success");
    }


    @Override
    public JsonResult getSourceDateList(GetLoginOutData userInfo) {
        //查询数据来源
        List<Map<String, Object>> sourceDateList = storeEssentialProductHMapper.getSourceDateList(userInfo.getClient());
        return JsonResult.success(sourceDateList, "success");
    }


    @Override
    public JsonResult getStoreList(EssentialProductCalculationInData inData) {
        //根据店效级别 门店属性查询门店列表
        List<Map<String, Object>> storeList = storeEssentialProductHMapper.getStoreList(inData);
        return JsonResult.success(storeList, "success");
    }


    @Override
    public JsonResult getOrderDetailList(EssentialProductCalculationInData inData) {
        String orderId = inData.getOrderId();
        if(ValidateUtil.isEmpty(orderId)) {
            throw new BusinessException("提示：铺货单号不能为空!");
        }
        //查询铺货单据明细列表
        List<EssentialProductDetailOutData> detailList = storeEssentialProductDMapper.getOrderDetailList(inData);
        return JsonResult.success(detailList, "success");
    }


    @Override
    public Result export(EssentialProductCalculationInData inData) {
        String orderId = inData.getOrderId();
        if(ValidateUtil.isEmpty(orderId)) {
            throw new BusinessException("提示：铺货单号不能为空!");
        }
        //查询铺货单据明细列表
        List<EssentialProductDetailOutData> detailList = storeEssentialProductDMapper.getOrderDetailList(inData);
        if(ValidateUtil.isEmpty(detailList)) {
            throw new BusinessException("提示：导出数据为空");
        }

        // 行号 (同一单号下顺序排列)
        AtomicInteger index = new AtomicInteger(1);
        //组装导出内容
        List<List<Object>> dataList = new ArrayList<>(detailList.size());
        for(EssentialProductDetailOutData essentialProduct : detailList) {
            //每行数据
            List<Object> lineList = new ArrayList<>();
            //序号
            lineList.add(index.getAndIncrement());
            //日期
            lineList.add(essentialProduct.getCreateDate());
            //商品编码
            lineList.add(essentialProduct.getProSelfCode());
            //商品描述
            lineList.add(essentialProduct.getDesc());
            //规格
            lineList.add(essentialProduct.getSpecs());
            //生产厂家
            lineList.add(essentialProduct.getFactoryName());
            //门店编号
            lineList.add(essentialProduct.getStoreId());
            //门店名称
            lineList.add(essentialProduct.getStoreName());
            //单位
            lineList.add(essentialProduct.getUnit());
            //大类
            lineList.add(essentialProduct.getBigTypeCode());
            //大类名称
            lineList.add(essentialProduct.getBigTypeName());
            //中类
            lineList.add(essentialProduct.getMidTypeCode());
            //中类名称
            lineList.add(essentialProduct.getMidTypeName());
            //商品分类
            lineList.add(essentialProduct.getTypeCode());
            //分类名称
            lineList.add(essentialProduct.getTypeName());
            //仓库成本价
            lineList.add(essentialProduct.getWmsCostPrice());
            //门店属性
            lineList.add(essentialProduct.getStoreAttribute());
            //店效级别
            lineList.add(essentialProduct.getStoreLevel());
            //建议铺货量
            lineList.add(essentialProduct.getAdviseNumber());
            //确认铺货量
            lineList.add(essentialProduct.getResultNumber());
            //实际铺货量
            lineList.add(essentialProduct.getCurrentNumber());
            dataList.add(lineList);
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                new ArrayList<String[]>() {{
                    add(CommonConstant.ESSENTIAL_PRODUCT_EXPORT_HEAD);
                }},
                new ArrayList<List<List<Object>>>() {{
                    add(dataList);
                }},
                new ArrayList<String>() {{
                    add(orderId);
                }});
        Result uploadResult = null;
        try {
            workbook.write(bos);
            String fileName = CommonConstant.ESSENTIAL_PRODUCT_EXPORT_SHEET_NAME + "-" + CommonUtil.getyyyyMMdd() + ".xls";
            uploadResult = cosUtils.uploadFile(bos, fileName);
            bos.flush();
        } catch (IOException e) {
            log.error("导出文件失败:{}",e.getMessage(), e);
            throw new BusinessException("导出文件失败！");
        } finally {
            try {
                bos.close();
            } catch (IOException e) {
                log.error("关闭流异常:{}",e.getMessage(), e);
                throw new BusinessException("关闭流异常！");
            }
        }
        return uploadResult;
    }

    
    @Override
    public JsonResult getDisableFlag(String client) {
        Integer count = storeEssentialProductHMapper.getDisableFlag(client);
        return JsonResult.success(count, "success");
    }
}