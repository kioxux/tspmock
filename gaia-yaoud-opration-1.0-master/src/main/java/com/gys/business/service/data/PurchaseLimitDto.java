package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class PurchaseLimitDto implements Serializable {
    private static final long serialVersionUID = -7265930596356143672L;

    /**
     * 商品编码
     */
    private String proId;

    /**
     * 购买数量
     */
    private String buyNum;
}
