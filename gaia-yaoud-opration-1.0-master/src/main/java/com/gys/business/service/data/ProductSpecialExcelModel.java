package com.gys.business.service.data;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProductSpecialExcelModel extends BaseRowModel implements Serializable {

    /**
     * 商品编码  药德通用编码
     */
    @ApiModelProperty(value="商品编码  药德通用编码")
    @ExcelProperty(index = 0)
    private String proCode;

    /**
     * 省份
     */
    @ApiModelProperty(value="省份")
    @ExcelProperty(index = 1)
    private String proProv;

    /**
     * 地市
     */
    @ApiModelProperty(value="地市")
    @ExcelProperty(index = 2)
    private String proCity;

    /**
     * 类型  1-季节，2-品牌
     */
    @ApiModelProperty(value="类型  1-季节，2-品牌")
    @ExcelProperty(index = 3)
    private String proType;

    /**
     * 1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌
     */
    @ApiModelProperty(value="1-春，2-夏，3-秋，4-冬   A-全国品牌，B-省份品牌，C-地区品牌")
    @ExcelProperty(index = 4)
    private String proFlag;

    /**
     * 状态   0-正常，1-停用
     */
    @ApiModelProperty(value="状态   0-正常，1-停用")
    @ExcelProperty(index = 5)
    private String proStatus;
}
