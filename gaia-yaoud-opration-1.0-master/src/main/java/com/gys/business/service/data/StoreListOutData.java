package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: jiangbl
 * @date: 2022/1/11 15:34
 * @describe: 远程审方，上传门店出参
 */
@Data
public class StoreListOutData implements Serializable {
    @ApiModelProperty(value = "门店id")
    private String gssrBrId;
    @ApiModelProperty(value = "门店名")
    private String stoIdAndName;
}
