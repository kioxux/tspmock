package com.gys.business.service.data.DtYb.ReturnExamime;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description 大同易联众 门店退货商品
 * @Author huxinxin
 * @Date 2021/5/10 14:10
 * @Version 1.0.0
 **/
@Data
public class ReturnExamimeProVO {
    // 药品编号
    private String proCode;
    // 药品名称
    private String proName;
    // 药品通用名称
    private String proCommonName;
    // 剂型
    private String form;
    // 规格
    private String specs;
    // 生产厂家
    private String factoryName;
    // 单位
    private String unit;
    // 退货数量
    private String returnQty;
    // 药品批号
    private String batchNo;
    // 生产日期
    private String productionDate;
    // 有效日期
    private String expiryDate;
    // 进价
    private String price;
    // 入库编号
    private String stockcode;
    // 退货原因
    private String remark;
}
