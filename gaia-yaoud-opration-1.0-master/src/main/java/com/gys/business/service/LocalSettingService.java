package com.gys.business.service;

import com.gys.business.service.data.LocalSettingInData;
import com.gys.business.service.data.LocalSettingOutData;

public interface LocalSettingService {
    LocalSettingOutData selectOne(LocalSettingInData inData);

    void saveOrUpDate(LocalSettingInData inData);
}
