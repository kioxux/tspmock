package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @description 会员卡信息表
 * @author cxy
 * @date 2021-12-16
 */
@Data
@ApiModel("会员卡信息表")
public class GaiaSdMemberCard implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 加盟商
     */
    @ApiModelProperty("加盟商")
    private String client;

    /**
     * 会员id
     */
    @ApiModelProperty("会员id")
    private String gsmbcMemberId;

    /**
     * 会员卡号
     */
    @ApiModelProperty("会员卡号")
    private String gsmbcCardId;

    /**
     * 所属门店
     */
    @ApiModelProperty("所属门店")
    private String gsmbcBrId;

    /**
     * 渠道
     */
    @ApiModelProperty("渠道")
    private String gsmbcChannel;

    /**
     * 卡类型
     */
    @ApiModelProperty("卡类型")
    private String gsmbcClassId;

    /**
     * 当前积分
     */
    @ApiModelProperty("当前积分")
    private String gsmbcIntegral;

    /**
     * 最后积分日期
     */
    @ApiModelProperty("最后积分日期")
    private String gsmbcIntegralLastdate;

    /**
     * 清零积分日期
     */
    @ApiModelProperty("清零积分日期")
    private String gsmbcZeroDate;

    /**
     * 新卡创建日期
     */
    @ApiModelProperty("新卡创建日期")
    private String gsmbcCreateDate;

    /**
     * 类型
     */
    @ApiModelProperty("类型")
    private String gsmbcType;

    /**
     * openid
     */
    @ApiModelProperty("openid")
    private String gsmbcOpenId;

    /**
     * 卡状态
     */
    @ApiModelProperty("卡状态")
    private String gsmbcStatus;

    /**
     * 开卡人员
     */
    @ApiModelProperty("开卡人员")
    private String gsmbcCreateSaler;

    /**
     * 开卡门店
     */
    @ApiModelProperty("开卡门店")
    private String gsmbcOpenCard;

    /**
     * 所属组织，单体：店号，连锁：连锁编码
     */
    @ApiModelProperty("所属组织，单体：店号，连锁：连锁编码")
    private String gsmbcOrgId;

    /**
     * 累计积分
     */
    @ApiModelProperty("累计积分")
    private String gsmbcTotalIntegral;

    /**
     * 累计金额
     */
    @ApiModelProperty("累计金额")
    private BigDecimal gsmbcTotalAmt;

    /**
     * 当年累计积分
     */
    @ApiModelProperty("当年累计积分")
    private String gsmbcYearIntegral;

    /**
     * 当年累计金额
     */
    @ApiModelProperty("当年累计金额")
    private BigDecimal gsmbcYearAmt;

    /**
     * 所属组织类型，单体：1，连锁：2
     */
    @ApiModelProperty("所属组织类型，单体：1，连锁：2")
    private String gsmbcOrgType;

    /**
     * 会员组编码
     */
    @ApiModelProperty("会员组编码")
    private String gsmbcOrgGroup;

    /**
     * last_update_time
     */
    @ApiModelProperty("last_update_time")
    private Timestamp lastUpdateTime;

    /**
     * 企微绑定状态，0、null：未绑定，1：已绑定
     */
    @ApiModelProperty("企微绑定状态，0、null：未绑定，1：已绑定")
    private Integer gsmbcWorkwechatBindStatus;

    /**
     * 激活状态 0:维持现有模式不变，即免费注册后立即生效，1: 收费注册需线下激活转正，2:收费注册线上支付后激活
     */
    @ApiModelProperty("激活状态 0:维持现有模式不变，即免费注册后立即生效，1: 收费注册需线下激活转正，2:收费注册线上支付后激活")
    private String gsmbcState;

    /**
     * 激活人工号
     */
    @ApiModelProperty("激活人工号")
    private String gsmbcJhUser;

    /**
     * 激活日期
     */
    @ApiModelProperty("激活日期")
    private String gsmbcJhDate;

    /**
     * 激活时间
     */
    @ApiModelProperty("激活时间")
    private String gsmbcJhTime;

    public GaiaSdMemberCard() {}
}