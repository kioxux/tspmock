package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 电子券查询返回实体
 *
 * @author xiaoyuan on 2020/9/30
 */
@Data
public class EVoucherQueryOutData implements Serializable {
    private static final long serialVersionUID = 6739573288288829492L;

    @ApiModelProperty(value = "序号")
    private Integer index;

    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    @ApiModelProperty(value = "主题描述")
    private String gsetsName;

    @ApiModelProperty(value = "业务单号")
    private String gsebsId;

    @ApiModelProperty(value = "起始日期")
    private String gsebsBeginDate;

    @ApiModelProperty(value = "结束日期")
    private String gsebsEndDate;
}
