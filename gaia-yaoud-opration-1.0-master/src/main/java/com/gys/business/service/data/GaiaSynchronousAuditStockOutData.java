package com.gys.business.service.data;

import java.io.Serializable;
import java.util.Date;

/**
 * 库存审计(GaiaSynchronousAuditStock)实体类
 *
 * @author XiaoZY
 * @since 2021-07-01 13:43:46
 */
public class GaiaSynchronousAuditStockOutData implements Serializable {
    private static final long serialVersionUID = -70698907210161115L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店ID
     */
    private String storeId;
    /**
     * 同步日期
     */
    private String billDate;
    /**
     * 主表总数量
     */
    private Double headQty;
    /**
     * 明细表总数量
     */
    private Double detailQty;
    private Double webHQty;
    private Double webDQty;
    private Double fxHQty;
    private Double fxDQty;
    /**
     * 来源 timer:定时任务 fx: fx上传
     */
    private String source;
    /**
     * 门店电脑标识(主板)
     */
    private String computerLogo;
    private String startDate;
    private String endDate;
    private String statusFlag;
    private String type;
    private String francName;
    private String stoName;
    private String webFlag;
    private String fxFlag;

    public Double getFxHQty() {
        return fxHQty;
    }

    public void setFxHQty(Double fxHQty) {
        this.fxHQty = fxHQty;
    }

    public Double getFxDQty() {
        return fxDQty;
    }

    public void setFxDQty(Double fxDQty) {
        this.fxDQty = fxDQty;
    }

    public Double getWebHQty() {
        return webHQty;
    }

    public void setWebHQty(Double webHQty) {
        this.webHQty = webHQty;
    }

    public Double getWebDQty() {
        return webDQty;
    }

    public void setWebDQty(Double webDQty) {
        this.webDQty = webDQty;
    }

    public String getFrancName() {
        return francName;
    }

    public void setFrancName(String francName) {
        this.francName = francName;
    }

    public String getStoName() {
        return stoName;
    }

    public void setStoName(String stoName) {
        this.stoName = stoName;
    }

    public String getWebFlag() {
        return webFlag;
    }

    public void setWebFlag(String webFlag) {
        this.webFlag = webFlag;
    }

    public String getFxFlag() {
        return fxFlag;
    }

    public void setFxFlag(String fxFlag) {
        this.fxFlag = fxFlag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public Double getHeadQty() {
        return headQty;
    }

    public void setHeadQty(Double headQty) {
        this.headQty = headQty;
    }

    public Double getDetailQty() {
        return detailQty;
    }

    public void setDetailQty(Double detailQty) {
        this.detailQty = detailQty;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getComputerLogo() {
        return computerLogo;
    }

    public void setComputerLogo(String computerLogo) {
        this.computerLogo = computerLogo;
    }

}
