package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.mapper.entity.GaiaSupplierBusiness;
import com.gys.business.service.data.*;

import java.util.List;

public interface RelativePriceService {
    List<PriceGetOutData> queryReplenishData(String client, String brId);

    List<PriceGetOutData> queryChainReplenishData(String client, String brId);

    int insertPriceGet(PriceGetInData indata);

    int updateReplenishHGetStatus(GaiaSdReplenishH gaiaSdReplenishH);

    int updateRepPriceStatus(String client, String brId, String pgId);

    List<GaiaSupplierBusiness> querySupplierInfo(String client, String brId);

    List<GaiaSupplierBusiness> queryChainSupplierInfo(String client, String brId);

    int checkSupplierDataByPgId(String client, String brId, RepPriceSupplierCheckData inData);

    QueryLoginInfoInData queryLoginInfoFromRedis(QueryLoginInfoInData inData);

    int insertLoginInfoToRedis(QueryLoginInfoInData inData);
}
