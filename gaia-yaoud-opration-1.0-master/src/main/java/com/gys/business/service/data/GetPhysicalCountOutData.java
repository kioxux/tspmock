package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "APP盘点单列表返回实体")
public class GetPhysicalCountOutData implements Serializable {
    private static final long serialVersionUID = -4578812387735499106L;
    @ApiModelProperty(value = "初盘日期")
    private String gspcDate;

    @ApiModelProperty(value = "复盘时间")
    private String gspcTime;

    @ApiModelProperty(value = "复盘人")
    private String gspcEmp;

    @ApiModelProperty(value = "盘点类型")
    private String gspcType;

    @ApiModelProperty(value = "行序号")
    private String gspcRowNo;

    @ApiModelProperty(value = "编码")
    private String gspcProId;

    @ApiModelProperty(value = "批号")
    private String batchNo;

    @ApiModelProperty(value = "复盘数量")
    private String gspcQty;

    @ApiModelProperty(value = "状态 0-未审核 1-已审核")
    private String gspcStatus;

    @ApiModelProperty(value = "库存")
    private String stockQty;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "计量单位")
    private String proUnit;

    @ApiModelProperty(value = "剂型")
    private String proForm;

    @ApiModelProperty(value = "厂家")
    private String proFactoryName;

    @ApiModelProperty(value = "产地")
    private String proPlace;

    @ApiModelProperty(value = "零售价")
    private String proPrice;

    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;
    private String gsplArea;
    private String gsplGroup;
    private String gsplShelf;
    private String gsplStorey;
    private String gsplSeat;
    private String gsppPriceNormal;
    private String vaildDate;
    private String vaild;
    private String gspcdVoucherId;

    @ApiModelProperty(value = "盘点单号")
    private String gspcVoucherId;

    @ApiModelProperty(value = "初盘差异")
    private String gspcdDiffFirstQty;

    @ApiModelProperty(value = "初盘人")
    private String gspcdEmp;

    @ApiModelProperty(value = "初盘日期")
    private String gspcdDate;

    @ApiModelProperty(value = "初盘时间")
    private String gspcdTime;

    private String gspcdPcSecondQty;

    @ApiModelProperty(value = "复盘差异")
    private String gspcdDiffSecondQty;

    private String status;

    @ApiModelProperty(value = "快速搜索关键字 (商品编码 + 商品的国际条形码 + 拼音码 + 名称 + 批准文号)")
    private List<String> quickSearchList;

    @ApiModelProperty(hidden = true)
    private String proSelfCode;

    @ApiModelProperty(hidden = true)
    private String proBarCode;

    @ApiModelProperty(hidden = true)
    private String proBarCode2;

    @ApiModelProperty(hidden = true)
    private String proPym;
}
