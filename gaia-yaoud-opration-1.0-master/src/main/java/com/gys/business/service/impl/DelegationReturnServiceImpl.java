package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.DelegationReturnService;
import com.gys.business.service.data.DelegationReturn.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.ApiModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DelegationReturnServiceImpl implements DelegationReturnService {

    @Autowired
    private GaiaSdReturnDepotHMapper returnDepotHMapper;
    @Autowired
    private GaiaSdReturnDepotDMapper returnDepotDMapper;
    @Autowired
    private GaiaProductBusinessMapper productBusinessMapper;
    @Autowired
    private GaiaReturnApprovalMapper returnApprovalMapper;
    @Autowired
    private GaiaPoHeaderMapper poHeaderMapper;
    @Autowired
    private GaiaPoItemMapper poItemMapper;

    @Override
    public List<DelegationReturnOutData> selectDelegationList(DelegationReturnInData data) {
        return returnDepotHMapper.selectDelegationList(data);
    }

    @Override
    public List<DelegationProductOutData> getDelegationProductList(DelegationReturnInData data) {
        return productBusinessMapper.getDelegationProductList(data);
    }

    @Override
    public List<ExcelReturnApprovalData> selectByExport() {
        List<ExcelReturnApprovalData> list= returnApprovalMapper.selectByExport();
        if(list.size()>0){
            list=new ArrayList<>();
        }

        return list;
    }

    @Override
    public boolean check(List<String> data,GetLoginOutData userInfo ) {
        Boolean falg =true;
        if(ObjectUtil.isNotEmpty(data)){
            //根据退库单号  判断有几个商品
            Set<String> uniqueSet = new HashSet(data);
            List<VoucherCount> voucherALs=new ArrayList();
            for (String temp : uniqueSet) {
                VoucherCount voucherCount=new VoucherCount(temp,Collections.frequency(data, temp));
                voucherALs.add(voucherCount);
            }
            //查询退库表对应条数
            Map map =new HashMap();
            map.put("client",userInfo.getClient());
            map.put("uniqueSet",uniqueSet);
            List<VoucherCount>returnDLs = returnDepotDMapper.checkReturnDepotDCount(map);
            for (VoucherCount voucherAL : voucherALs) {
                for (VoucherCount returnDL : returnDLs) {
                    //通过退库单号对比  是不是整单完成
                    if (voucherAL.getVoucherId().equals( returnDL.getVoucherId())) {
                        if (voucherAL.getCounts() != returnDL.getCounts()) {
                            falg = false;
                            break;
                        }
                    }
                }
                //如果有数量对不上  跳出循环 返回flase
                if (!falg){
                    break;
                }
            }
        }
        return falg;
    }

    @Override
    @Transactional
    public void save(DelegationReturnInfoData data) {
        if(ObjectUtil.isNotEmpty(data)){
            if(ObjectUtil.isNotEmpty(data.getRetrunApprovalInData())){
                //退库审批表
                List<GaiaReturnApproval> returnApprovalList =new ArrayList<>();
                //退库采购单主表
                List<GaiaPoHeader> poHeaderList =new ArrayList<>();
                //退库采购单明细表
                List<GaiaPoItem> poItemList = new ArrayList<>();
                //退库主表
                List<GaiaSdReturnDepotH> returnDepotHList =new ArrayList<>();
                //退库明细表
                List<GaiaSdReturnDepotD> returnDepotDList =new ArrayList<>();
                //当前列表
                List<RetrunApprovalInData> remainProList =data.getRetrunApprovalInData();
                //修改退库主表状态 将退库表的状态 同意3 驳回6
                returnDepotHMapper.updateStatusBatch(data.getRetrunApprovalInData(),data.getClient());
                //被驳回的退库单
                List<RetrunApprovalInData> rejectList = null;
                rejectList = data.getRetrunApprovalInData().stream()
                        .filter((RetrunApprovalInData s) -> s.getStatus().equals("6"))
                        .collect(Collectors.toList());
                //驳回的从列表中删除
                if(ObjectUtil.isNotEmpty(rejectList)){
                    remainProList.removeAll(rejectList);
                }
                //没有同意的跳过
                if(ObjectUtil.isNotEmpty(remainProList)){
                    //提取退库单号  判断有几个 去重
                    List<String> vocherIdArray=remainProList.stream().map(RetrunApprovalInData::getVoucherId).distinct().collect(Collectors.toList());
                    System.out.println("输出单号集合："+vocherIdArray);
                    //因为列表可能只显示了退库单一部分  查出全部退库单
                    if(data.getVoucherType().equals("1")){
//                    List<String> poProIdArray = data.getRetrunApprovalInData().stream().map(RetrunApprovalInData ::getProId).collect(Collectors.toList());
//                    System.out.println("列表商品集合："+poProIdArray);
                        Map<String,Object> map =new HashMap<>();
                        map.put("client",data.getClient());
                        map.put("vocherIdArray",vocherIdArray);
                        remainProList= returnDepotHMapper.remainReturnD(map);
                        //填写表格修改值
                        for (RetrunApprovalInData approvalInPro:data.getRetrunApprovalInData()){ //前端传
                            for (RetrunApprovalInData remainPro: remainProList){ //后端查询
                                if(approvalInPro.getVoucherId().equals(remainPro.getVoucherId()) && approvalInPro.getProId().equals(remainPro.getProId()) && approvalInPro.getSerial().equals(remainPro.getSerial())){//根据退库单和商品修改值
                                    if(ObjectUtil.isNotEmpty(approvalInPro.getReviseQty())){
                                        remainPro.setReviseQty(approvalInPro.getReviseQty());//如果是前端的数据  修正数量是前端传值
                                        break;
                                    }else {
                                        throw new BusinessException("提示：请填写退货数量！");
                                    }
                                }
                            }
                        }
                    }

                    for(String returnId : vocherIdArray){
                        GaiaPoHeader poHeader = new GaiaPoHeader();
                        GaiaSdReturnDepotH returnDepotH=new GaiaSdReturnDepotH();
                        String poId = this.returnDepotHMapper.selectNextVoucherId1(data.getClient());
                        poHeader.setClient(data.getClient());
                        poHeader.setPoId(poId);
                        poHeader.setPoType("Z002");
                        poHeader.setPoSubjectType("1");
                        poHeader.setPoDate(DateUtil.format(new Date(), "yyyyMMdd"));
                        poHeader.setPoCreateBy(data.getUserId());
                        poHeader.setPoCreateDate(DateUtil.format(new Date(), "yyyyMMdd"));
                        poHeader.setPoCreateTime(DateUtil.format(new Date(), "HHmmss"));
                        BigDecimal totalAmt = BigDecimal.ZERO;
                        BigDecimal totalQty = BigDecimal.ZERO;
                        for(RetrunApprovalInData approvalData : remainProList){
                            if(returnId.equals(approvalData.getVoucherId())){
                                poHeader.setPoSupplierId(approvalData.getSupplierCode());
                                poHeader.setPoCompanyCode(approvalData.getStoDcCode());
                                poHeader.setPoPaymentId(approvalData.getPaymentId());
                                poHeader.setPoDeliveryTypeStore(approvalData.getStoCode());
                                poHeader.setPoReqId(approvalData.getVoucherId());

                                GaiaPoItem poItem = new GaiaPoItem();
                                poItem.setClient(data.getClient());
                                poItem.setPoId(poId);
                                poItem.setPoLineNo(approvalData.getSerial());
                                poItem.setPoProCode(approvalData.getProId());
                                //接口查询的数量是 reviseQty 修正数量
                                poItem.setPoQty(approvalData.getReviseQty());
                                totalQty =totalQty.add(approvalData.getReviseQty());
                                poItem.setPoUnit(approvalData.getProUnit());
                                poItem.setPoPrice(approvalData.getPoPrice());
                                BigDecimal lineAmt=approvalData.getReviseQty().multiply(approvalData.getPoPrice()).setScale(2, 4);
                                totalAmt = totalAmt.add(lineAmt);
                                poItem.setPoLineAmt(lineAmt);
                                poItem.setPoSiteCode(approvalData.getStoDcCode());
                                poItem.setPoLocationCode("1000");
                                poItem.setPoBatch(approvalData.getBatchNO());
                                poItem.setPoRate(approvalData.getInputTax());
                                poItem.setPoDeliveryDate(DateUtil.format(new Date(), "yyyyMMdd"));
                                poItemList.add(poItem);

                                GaiaReturnApproval returnApproval =new GaiaReturnApproval();
                                returnApproval.setClient(data.getClient());
                                returnApproval.setGsrdhBrId(approvalData.getStoCode());
                                returnApproval.setGsrdhVoucherId(approvalData.getVoucherId());
                                returnApproval.setGsrddSerial(approvalData.getSerial());
                                returnApproval.setGsrdhDate(DateUtil.format(new Date(), "yyyyMMdd"));
                                returnApproval.setGsrdhType("Z002");
                                returnApproval.setGsrdhStatus("3");
                                returnApproval.setGsrdhEmp(approvalData.getEmp());
                                returnApproval.setGsrdhRemaks(approvalData.getRemaks());
                                returnApproval.setGsrddProId(approvalData.getProId());
                                returnApproval.setGsrddBatch(approvalData.getBatch());
                                returnApproval.setGsrddRetdepQty(approvalData.getRetdepQty());
                                returnApproval.setGsrddReviseQty(approvalData.getReviseQty());
                                returnApproval.setGsrddPoId(poId);
                                returnApproval.setGsrddPoLine(approvalData.getSerial());
                                returnApprovalList.add(returnApproval);

                                GaiaSdReturnDepotD returnDepotD=new GaiaSdReturnDepotD();
                                returnDepotD.setClientId(data.getClient());
                                returnDepotD.setGsrddVoucherId(approvalData.getVoucherId());
                                returnDepotD.setGsrddBrId(approvalData.getStoCode());
                                returnDepotD.setGsrddProId(approvalData.getProId());
                                returnDepotD.setGsrddReviseQty(String.valueOf(approvalData.getReviseQty()));
                                returnDepotDList.add(returnDepotD);



                            }

                        }
                        returnDepotH.setClientId(data.getClient());
                        returnDepotH.setGsrdhVoucherId(returnId);
                        returnDepotH.setGsrdhTotalAmt(totalAmt);
                        returnDepotH.setGsrdhTotalQty(String.valueOf(totalQty));
                        returnDepotHList.add(returnDepotH);
                        poHeaderList.add(poHeader);
                    }
                    returnDepotHMapper.updateBatch(returnDepotHList);
                    returnDepotDMapper.updateReviseBatch(returnDepotDList);
                    poHeaderMapper.insertBatch(poHeaderList);
                    poItemMapper.insertBatch(poItemList);
                    returnApprovalMapper.insertBatch(returnApprovalList);
                    //                this.poItemMapper.insert(poItem);

                }
            }
        }
    }
}
