//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdHandover;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;

import java.text.ParseException;
import java.util.List;


public interface CashboxService {

    Integer checkPassword(CashboxInData inData);

    void save(CashboxInData inData);

    CashboxOutData thePreviousData(CashboxInData inData);

    CashboxOutData getThisFrom(CashboxInData inData, GetLoginOutData userInfo) throws ParseException;

    CashboxOutData getCashSource(CashboxInData inData, GetLoginOutData userInfo) throws ParseException;

    List<CashboxOutData> queryList(CashboxInData inData);

    List<GetUserOutData> queryUserList(CashboxInData inData);
}
