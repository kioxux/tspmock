package com.gys.business.service.impl;

import com.gys.business.mapper.TenCentMapMapper;
import com.gys.business.service.TenCentMapService;
import com.gys.util.TenCentMapUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TenCentMapServiceImpl implements TenCentMapService {

    @Autowired
    private TenCentMapMapper tenCentMapMapper;

    @Override
    public void updateLngAndLat(Map<String, Object> inData) {
        List<Map<String, String>> addressList = tenCentMapMapper.getAddressList(inData);

        addressList=addressList.stream().filter(add->StringUtils.isNotEmpty(add.get("STO_ADD"))).collect(Collectors.toList());

        addressList.stream().forEach(add->{
            System.out.println(add.get("STO_ADD"));
            Map<String, Object> lngAndLat = TenCentMapUtil.getLngAndLat(URLEncoder.encode(add.get("STO_ADD")));
            if (lngAndLat!=null&&lngAndLat.get("lng") != null && lngAndLat.get("lat")!=null) {
                add.put("lng", (String) lngAndLat.get("lng"));
                add.put("lat", (String) lngAndLat.get("lat"));
            }
        });

        addressList=addressList.stream().filter(add->{
            return add.get("lng")!=null&&add.get("lng").length()>0&&add.get("lat")!=null&&add.get("lat").length()>0;
        }).collect(Collectors.toList());

        addressList.stream().forEach(add->{
            tenCentMapMapper.updateLatAndLngByAddress(add);
        });
    }
}
