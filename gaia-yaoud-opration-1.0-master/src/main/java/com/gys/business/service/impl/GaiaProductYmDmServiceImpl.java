package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaProductYmDmMapper;
import com.gys.business.service.GaiaProductYmDmService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 药盟对码表(GaiaProductYmDm)表服务实现类
 *
 * @author XIAOZY
 * @since 2021-11-11 20:26:42
 */
@Service("gaiaProductYmDmService")
public class GaiaProductYmDmServiceImpl implements GaiaProductYmDmService {
    @Resource
    private GaiaProductYmDmMapper gaiaProductYmDmDao;
}
