package com.gys.business.service.data;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author csm
 * @date 1/16/2022 - 2:05 PM
 */
@Data
public class ProductsOrderInData {
    /**
     *门店ID集合
     */
    private List<String> gsphBrIdList;
    /**
     * 开始日期
     */
    private String startDate;
    /**
     * 结束日期
     */
    private String endDate;
}
