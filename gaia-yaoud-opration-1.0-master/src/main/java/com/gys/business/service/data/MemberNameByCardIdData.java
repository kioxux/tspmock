package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 会员id 和名称
 *
 * @author xiaoyuan on 2020/8/18
 */
@Data
public class MemberNameByCardIdData implements Serializable {
    private static final long serialVersionUID = -5002222447122418591L;

    /**
     * 会员id
     */
    private String cardId;

    /**
     * 会员名称
     */
    private String memberName;

    /**
     * 手机号
     */
    private String phone;

    private String memberId;
}
