package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 实体返回
 *
 * @author xiaoyuan on 2020/12/23
 */
@Data
public class OrderPayOutData implements Serializable {
    private static final long serialVersionUID = 21642095909398111L;

    /**
     * 支付编码
     */
    private String gspmId;

    /**
     * 支付名称
     */
    private String gspmName;

    /**
     *  1为正常支付方式，2为显示支付方式（实为抵扣）
     */
    private String gspmType;

    /**
     * 支付方式说明
     */
    private String gspmRemark1;

    /**
     * 储值卡充值是否可用   0.不可用   1.可用
     */
    private String gspmRecharge;
}
