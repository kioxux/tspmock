package com.gys.business.service.response;

import com.gys.business.service.response.SelectSmallOasSicknessPageInfoResponse;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data

public class SelectMiddleOasSicknessPageInfoResponse {

    @ApiModelProperty(value = "主键")
    private String a;
    @ApiModelProperty(value = "疾病编码")
    private String middleSicknessCoding;

    @ApiModelProperty(value = "疾病名称")
    private String middleSicknessCodingName;

    @ApiModelProperty(value = "中类分类集合")
    private List<SelectSmallOasSicknessPageInfoResponse> smallSickness;


}
