package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 库存变动返回vo
 */
@Data
public class ConsumeStockResponse {
    @ApiModelProperty(value = "批次")
    private String batch;

    @ApiModelProperty(value = "变动数量")
    private BigDecimal num;
}
