package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/13 10:38
 **/
@Data
public class OrderDetailForm {
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(hidden = true)
    private String userId;
    @ApiModelProperty(value = "补货单号", example = "PD202100028")
    private String replenishCode;
    @ApiModelProperty(value = "门店ID", example = "10005")
    private String storeId;
}
