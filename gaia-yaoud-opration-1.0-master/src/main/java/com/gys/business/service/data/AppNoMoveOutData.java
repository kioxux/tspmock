package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class AppNoMoveOutData implements Serializable {

    @ApiModelProperty(value = "类型编码")
    private String typeCode;

    @ApiModelProperty(value = "类型名称")
    private String typeName;

    @ApiModelProperty(value = "不动销库存品项数")
    private BigDecimal noMoveCount;

    @ApiModelProperty(value = "不动销库存成本额")
    private BigDecimal noMoveCostAmt;

    @ApiModelProperty(value = "总成本额")
    private BigDecimal totalCostAmt;

    @ApiModelProperty(value = "不动销库存占比")
    private BigDecimal noMoveCostAmtProp;

}
