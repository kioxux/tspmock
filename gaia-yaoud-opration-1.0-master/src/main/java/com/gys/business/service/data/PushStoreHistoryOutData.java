package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PushStoreHistoryOutData implements Serializable {

    @ApiModelProperty(value = "历史日期（周或月）")
    private String weeksOrMonths;


    @ApiModelProperty(value = "历史数据")
    private List<PushStoreHistoryData> historyList;

}
