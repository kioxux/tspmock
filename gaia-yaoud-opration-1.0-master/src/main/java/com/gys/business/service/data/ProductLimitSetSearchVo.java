package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductLimitSetSearchVo implements Serializable {

    private static final long serialVersionUID = 3215022047454054448L;

    private List<String> proIds;

    //是否所有门店0：否，1：是
    private String ifAllSto;

    //门店选择
    private List<String> sites;

    //是否一直生效. 0：否，1：是
    private String ifAllTime;

    //限购类型. 0：每单限购，1：会员限购
    private String limitType;

    //限购数量
    private String limitQty;

    //起始日期
    private String beginDate;

    //结束日期
    private String endDate;

    @ApiModelProperty(value = "页面展示数量", required = false)
    public int pageSize;

    @ApiModelProperty(value = "页数", required = false)
    private int pageNum;
}
