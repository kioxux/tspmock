package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaMenuAuthConfigMapper;
import com.gys.business.mapper.GaiaMenuAuthMapper;
import com.gys.business.mapper.GaiaMenuConfigMapper;
import com.gys.business.mapper.GaiaMenuMapper;
import com.gys.business.mapper.entity.GaiaMenu;
import com.gys.business.mapper.entity.GaiaMenuAuth;
import com.gys.business.service.MenuService;
import com.gys.business.service.data.Menu.MenuInData;
import com.gys.business.service.data.Menu.MenuOutData;
import com.gys.business.service.data.Menu.MenuTree;
import com.gys.business.service.data.MenuConfig.MenuConfigInput;
import com.gys.business.service.data.MenuConfig.MenuConfigOutput;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.exception.BusinessException;
import com.gys.util.MenuTreeUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private GaiaMenuMapper menuMapper;

    @Autowired
    private GaiaMenuAuthMapper menuAuthMapper;

    @Autowired
    private GaiaMenuConfigMapper menuConfigMapper;

    @Autowired
    private GaiaMenuAuthConfigMapper menuAuthConfigMapper;

    @Override
    @Transactional
    public void insertMenu(GetLoginOutData userInfo, MenuInData inData) {
        int count = menuMapper.getCount(inData);
        if(count < 1) {
            if(ObjectUtil.isEmpty(inData.getParentId())){
                inData.setParentId(Long.valueOf(0));
            }
            GaiaMenu menu = new GaiaMenu();
            BeanUtils.copyProperties(inData,menu);
            long id= menuMapper.getMaxId(inData); //查最大id
            int seq= menuMapper.getMaxSeq(inData);//查该父菜单下最大排序
            menu.setId(id);
            menu.setSeq(seq);
            menu.setStatus("1");
            menu.setModule("1");
            menu.setCreator(userInfo.getUserId());
            menu.setCreateDate(new Date());
            menuMapper.insertSelective(menu);
            if(ObjectUtil.isNotEmpty(inData.getAuthGroup())){
                List<GaiaMenuAuth> menuAuths = new ArrayList<>();
                for (GaiaMenuAuth menuAuth : inData.getAuthGroup()){
                    menuAuth.setMenuId(id);
                    menuAuth.setStatus("1");
                    menuAuth.setCreator(userInfo.getUserId());
                    menuAuth.setCreateDate(new Date());
                    menuAuths.add(menuAuth);
                }
                //保存菜单权限
                menuAuthMapper.insertList(menuAuths);
            }
        }else {
            throw new BusinessException("该菜单编码已存在!");
        }
    }

    @Override
    @Transactional
    public void updateMenu(GetLoginOutData userInfo, MenuInData inData) {


        GaiaMenu menu = new GaiaMenu();
        BeanUtils.copyProperties(inData,menu);
        GaiaMenu menuInfo = menuMapper.selectByPrimaryKey(menu);
        //如果父id而没变 不管他
        if(menuInfo.getParentId().equals(menu.getParentId())){
            int seq= menuMapper.getMaxSeq(inData);
            menu.setSeq(seq);
        }
        menu.setUpdator(userInfo.getUserId());
        menu.setUpdateDate(new Date());
        menuMapper.updateByPrimaryKeySelective(menu);
        //保存菜单权限
        Example exampleDel = new Example(GaiaMenuAuth.class);
        exampleDel.createCriteria().andEqualTo("menuId", menu.getId());
        menuAuthMapper.deleteByExample(exampleDel);
        List<GaiaMenuAuth> menuAuths = new ArrayList<>();
        if(ObjectUtil.isNotEmpty(inData.getAuthGroup())){
            for (GaiaMenuAuth menuAuth : inData.getAuthGroup()){
                menuAuth.setMenuId(menu.getId());
                menuAuth.setStatus("1");
                menuAuth.setCreator(userInfo.getUserId());
                menuAuth.setCreateDate(new Date());
                menuAuths.add(menuAuth);
            }
            //保存菜单权限
            menuAuthMapper.insertList(menuAuths);
        }
    }

    @Override
    @Transactional
    public void deleteMenu(GetLoginOutData userInfo, MenuInData inData) {
        GaiaMenu inMenu = new GaiaMenu();
//        inMenu.setId(inData.getId());
        inMenu.setStatus("2");
        inMenu.setUpdator(userInfo.getUserId());
        inMenu.setUpdateDate(new Date());
        List<Object> menuIds =new ArrayList<>(); //存放要修改的id
        List<Object> menuParentIds =new ArrayList<>();//拿id到parentId去找 一直到找不到最后一级菜单
        menuParentIds.add(inData.getId());
        menuIds.add(inData.getId());
        do {
            menuParentIds = menuMapper.selectMenuList(menuParentIds);
            menuIds.addAll(menuParentIds);
        }while (ObjectUtil.isNotEmpty(menuParentIds));
        menuMapper.deleteMenu(inMenu,menuIds);

        GaiaMenuAuth inMenuAuth = new  GaiaMenuAuth();
        inMenuAuth.setStatus("2");
        inMenuAuth.setUpdator(userInfo.getUserId());
        inMenuAuth.setUpdateDate(new Date());
        menuAuthMapper.deleteMenuAuth(inMenuAuth,menuIds);
    }

    @Override
    public MenuOutData selectMenu(MenuInData inData) {
        MenuOutData outData = new MenuOutData();

        Example exampleM = new Example(GaiaMenu.class);
        exampleM.createCriteria().andEqualTo("id", inData.getId());
        GaiaMenu outMenu = menuMapper.selectOneByExample(exampleM);
        if(ObjectUtil.isNotEmpty(outMenu)){
            BeanUtils.copyProperties(outMenu,outData);
            Example exampleMA = new Example(GaiaMenuAuth.class);
            exampleMA.createCriteria().andEqualTo("menuId", inData.getId());
            outData.setAuthGroup(menuAuthMapper.selectByExample(exampleMA));
        }
        return  outData;
    }

    @Override
    public List<Object> selectMenuTree() {
        List<MenuOutData> outData = new ArrayList<>();
        MenuTreeUtil menuTree = new MenuTreeUtil();
        List<GaiaMenu> menu = menuMapper.selectAll();
        List<Object> menuList = menuTree.menuList(menu);
        return menuList;
    }



    @Override
    @Transactional
    public void updateMenuTree(GetLoginOutData userInfo, List<MenuTree> inData) {
        List<GaiaMenu> menus = new ArrayList<>();
        List<MenuTree> menuTrees = inData;
        MenuTreeUtil menuTreeUtil = new MenuTreeUtil();
        if(ObjectUtil.isNotEmpty(menuTrees)){
            for (int i=0;i<menuTrees.size();i++){
                GaiaMenu menu = new GaiaMenu();
                MenuTree menuTree = new MenuTree();
                menuTree = menuTrees.get(i);
                BeanUtils.copyProperties(menuTree,menu);
                menu.setSeq(i);
                menu.setUpdator(userInfo.getUserId());
                menu.setUpdateDate(new Date());
                menus.add(menu);
                if(ObjectUtil.isNotEmpty(menuTree.getLists())){
                    //查询下一级菜单
                    menus.addAll(menuTreeUtil.menuTreeToMenu(menuTree.getLists(),userInfo.getUserId()));
                }
            }
            menuMapper.updateBatch(menus);
        }
    }


    @Override
    public List<Object> selectMenuConfig(MenuConfigInput inData) {
        List<MenuConfigOutput> output = menuConfigMapper.selectMenuConfig(inData);
        MenuTreeUtil menuTree = new MenuTreeUtil();
        List<Object> menuList = menuTree.menuConfigList(output);
        System.out.println(output);

        return menuList;
    }


}
