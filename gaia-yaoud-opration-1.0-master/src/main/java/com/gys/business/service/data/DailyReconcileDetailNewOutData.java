package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
@Data
public class DailyReconcileDetailNewOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "店号")
    private String gsspmBrId;
    @ApiModelProperty(value = "日期")
    private String gsspmDate;
    @ApiModelProperty(value = "支付编码")
    private String gsspmId;
    @ApiModelProperty(value = "支付类型 1.销售支付 2.储值卡充值 3.挂号支付")
    private String gsspmType;
    @ApiModelProperty(value = "支付名称")
    private String gsspmName;
    @ApiModelProperty(value = "支付卡号")
    private String gsspmCardNo;
    @ApiModelProperty(value = "支付金额")
    private BigDecimal gsspmAmt;
    @ApiModelProperty(value = "RMB收款金额")
    private String gsspmRmbAmt;
    @ApiModelProperty(value = "找零金额")
    private String gsspmZlAmt;
}
