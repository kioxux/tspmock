//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class GetSaleReturnInData implements Serializable {
    private static final long serialVersionUID = 4362840615293176784L;
    private String clientId;
    private String gsshBillNo;
    private String gsshBrId;
//    private String gsshDate;
//    private String gsshTime;
//    private String gsshEmp;
    private String gsshTaxNo;
    private String gsshHykNo;
    private BigDecimal gsshZkAmt;
//    private BigDecimal gsshYsAmt;
    private BigDecimal gsshRmbZlAmt;
    private BigDecimal gsshRmbAmt;
    private String gsshDyqNo;
    private BigDecimal gsshDyqAmt;
    private String gsshDyqType;
    private String gsshRechargeCardNo;
    private BigDecimal gsshRechargeCardAmt;
    private String gsshDzqczActno1;
    private BigDecimal gsshDzqczAmt1;
    private String gsshDzqdyActno1;
    private BigDecimal gsshDzqdyAmt1;
    private String gsshIntegralAdd;
    private String gsshIntegralExchange;
    private BigDecimal gsshIntegralExchangeAmt;
    private String gsshIntegralCash;
    private BigDecimal gsshIntegralCashAmt;
    private String gsshPaymentNo1;
    private BigDecimal gsshPaymentAmt1;
    private String gsshBillNoReturn;
    private String gsshEmpReturn;
    private String gsshPromotionType1;
    private String gsshRegisterVoucherId;
    private String gsshReplaceBrId;
    private String gsshReplaceSalerId;
    private String gsshHideFlag;
    private String gsshCallQty;
    private List<GetSaleReturnDetailInData> saleReturnDetailInDataList;
    private String type;
    private String userId;
}
