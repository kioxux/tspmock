package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2021/5/7
 */
@Data
public class PartialSynDetailsVO implements Serializable {
    private static final long serialVersionUID = 4622155482088215211L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String brId;
}
