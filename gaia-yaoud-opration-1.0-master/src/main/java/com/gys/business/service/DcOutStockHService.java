package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaDcData;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/23 10:36
 */
public interface DcOutStockHService {

    void generateOutStock(GaiaDcData dcData);

}
