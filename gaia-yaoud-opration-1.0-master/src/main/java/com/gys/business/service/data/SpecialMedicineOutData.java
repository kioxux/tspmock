package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/12/17
 */
@Data
public class SpecialMedicineOutData implements Serializable {
    private static final long serialVersionUID = -3457974354626088084L;

    /**
     * 序号
     */
    @ApiModelProperty(value = "序号")
    private Integer index;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 销售单号
     */
    @ApiModelProperty(value = "销售单号")
    private String gssdBillNo;

    /**
     * 销售日期
     */
    @ApiModelProperty(value = "销售日期")
    private String gssdDate;

    /**
     * 销售编码
     */
    @ApiModelProperty(value = "销售编码")
    private String gssdProId;

    /**
     * 销售名称
     */
    @ApiModelProperty(value = "销售名称")
    private String proName;

    /**
     * 规格
     */
    @ApiModelProperty(value = "规格")
    private String proSpecs;

    /**
     * 销售数量
     */
    @ApiModelProperty(value = "销售数量")
    private String gssdQty;


    /**
     * 批号
     */
    @ApiModelProperty(value = "批号")
    private String  gssdBatchNo;

    /**
     * 有效期
     */
    @ApiModelProperty(value = "有效期")
    private String gssdValidDate;


    /**
     * 顾客姓名
     */
    @ApiModelProperty(value = "顾客姓名")
    private String gssdSpecialmedName;

    /**
     * 顾客性别
     */
    @ApiModelProperty(value = "顾客性别")
    private String gssdSpecialmedSex;

    /**
     * 出生年月
     */
    @ApiModelProperty(value = "出生年月")
    private String gssdSpecialmedBirthday;

    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证")
    private String gssdSpecialmedIdcard;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String gssdSpecialmedMobile;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String gssdSpecialmedAddress;

    /**
     * 登记人员
     */
    @ApiModelProperty(value = "登记人员")
    private String userNam;

    /**
     * 厂家
     */
    @ApiModelProperty(value = "厂家")
    private String proFactoryName;

    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "门店名称")
    private String stoShortName;

}
