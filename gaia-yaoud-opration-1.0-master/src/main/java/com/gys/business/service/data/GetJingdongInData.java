//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetJingdongInData {
    private String storeCode;
    private String orderId;
    private String serviceOrder;
    private String reason;
    private String reasonCode;
    private String account;

    public GetJingdongInData() {
    }

    public String getStoreCode() {
        return this.storeCode;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public String getServiceOrder() {
        return this.serviceOrder;
    }

    public String getReason() {
        return this.reason;
    }

    public String getReasonCode() {
        return this.reasonCode;
    }

    public String getAccount() {
        return this.account;
    }

    public void setStoreCode(final String storeCode) {
        this.storeCode = storeCode;
    }

    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    public void setServiceOrder(final String serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    public void setReason(final String reason) {
        this.reason = reason;
    }

    public void setReasonCode(final String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public void setAccount(final String account) {
        this.account = account;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetJingdongInData)) {
            return false;
        } else {
            GetJingdongInData other = (GetJingdongInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$storeCode = this.getStoreCode();
                Object other$storeCode = other.getStoreCode();
                if (this$storeCode == null) {
                    if (other$storeCode != null) {
                        return false;
                    }
                } else if (!this$storeCode.equals(other$storeCode)) {
                    return false;
                }

                Object this$orderId = this.getOrderId();
                Object other$orderId = other.getOrderId();
                if (this$orderId == null) {
                    if (other$orderId != null) {
                        return false;
                    }
                } else if (!this$orderId.equals(other$orderId)) {
                    return false;
                }

                Object this$serviceOrder = this.getServiceOrder();
                Object other$serviceOrder = other.getServiceOrder();
                if (this$serviceOrder == null) {
                    if (other$serviceOrder != null) {
                        return false;
                    }
                } else if (!this$serviceOrder.equals(other$serviceOrder)) {
                    return false;
                }

                label62: {
                    Object this$reason = this.getReason();
                    Object other$reason = other.getReason();
                    if (this$reason == null) {
                        if (other$reason == null) {
                            break label62;
                        }
                    } else if (this$reason.equals(other$reason)) {
                        break label62;
                    }

                    return false;
                }

                label55: {
                    Object this$reasonCode = this.getReasonCode();
                    Object other$reasonCode = other.getReasonCode();
                    if (this$reasonCode == null) {
                        if (other$reasonCode == null) {
                            break label55;
                        }
                    } else if (this$reasonCode.equals(other$reasonCode)) {
                        break label55;
                    }

                    return false;
                }

                Object this$account = this.getAccount();
                Object other$account = other.getAccount();
                if (this$account == null) {
                    if (other$account != null) {
                        return false;
                    }
                } else if (!this$account.equals(other$account)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetJingdongInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $storeCode = this.getStoreCode();
        result = result * 59 + ($storeCode == null ? 43 : $storeCode.hashCode());
        Object $orderId = this.getOrderId();
        result = result * 59 + ($orderId == null ? 43 : $orderId.hashCode());
        Object $serviceOrder = this.getServiceOrder();
        result = result * 59 + ($serviceOrder == null ? 43 : $serviceOrder.hashCode());
        Object $reason = this.getReason();
        result = result * 59 + ($reason == null ? 43 : $reason.hashCode());
        Object $reasonCode = this.getReasonCode();
        result = result * 59 + ($reasonCode == null ? 43 : $reasonCode.hashCode());
        Object $account = this.getAccount();
        result = result * 59 + ($account == null ? 43 : $account.hashCode());
        return result;
    }

    public String toString() {
        return "GetJingdongInData(storeCode=" + this.getStoreCode() + ", orderId=" + this.getOrderId() + ", serviceOrder=" + this.getServiceOrder() + ", reason=" + this.getReason() + ", reasonCode=" + this.getReasonCode() + ", account=" + this.getAccount() + ")";
    }
}
