package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:55 2021/10/14
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
public interface RegionalPlanningService {
    void defaultDate(EditDefaultRegionalInData inData);

    List<RegionalOutData> getRegional(GetLoginOutData userInfo, SelectRegionalInData inData);

    void saveRegional(SaveRegionalInData inData);

    RegionalStoreOutData getRegionalStore(SelectRegionalStoreInData inData);

    void saveRegionalStore(SaveRegionalStoreInData inData);

    void removeRegional(RemoveRegionalInData inData);

    void editDefaultRegional(EditDefaultRegionalInData inData);
}
