package com.gys.business.service.data.zjnb;

import lombok.Data;

/**
 * 个人信息
 * @author cai si jun
 * @date 2021/10/22 16:10
 */
@Data
public class PersonalInfoVo {
    /**
     * 姓名
     */
    private String name;
    /**
     * 身份证号（社会保障号）
     */
    private String id;
    /**
     * 性别
     */
    private String sex;
    /**
     * 出生年月
     */
    private String birthday;
    /**
     * 区县代码
     */
    private String areaCode;
    /**
     * 参保人员类别
     */
    private String personalType;
    /**
     * 参保人员子类别
     */
    private String subPersonalType;
    /**
     * 享受待遇类别
     */
    private String treatmentCategory;
    /**
     * 帐户信息
     */
    private AccountInfo accountInfo;
    /**
     * 登记信息
     */
    private RegisterInfo registerInfo;
    /**
     * 其他信息
     */
    private OtherInfo otherInfo;


    /**
     * 账户信息
     */
    @Data
    public static class AccountInfo {
        /**
         * 帐户状态 见标准字典解释
         */
        private String accountStatus;
        /**
         * 异地居住标识		0：不是；1：是
         */
        private String nonLocalSign;
        /**
         * 交易监控标识		0：不是；1：是
         * 需要医疗机构配合医保中心关注此人
         */
        private String monitoringTag;
        /**
         * 代配药标志		0：未登记；1：已登记
         */
        private String dispensingMark;
        /**
         * 单位医保号
         */
        private String unitInsuranceNo;
        /**
         * 系统保留
         */
        private String systemRetention;
    public AccountInfo(){}
    }

    /**
     * 登记信息
     */
    @Data
    public static class RegisterInfo {
        /**
         * 登记类别	2	01：住院；02：家床；03：急观；04：日间手术；05：预住院
         */
        private String regType;
        /**
         * 登记医疗机构
         */
        private String organization;
        /**
         * 登记时间
         */
        private String regTime;
        /**
         * 就诊编号
         */
        private String visitNumber;
        /**
         * 住院/家床编号
         */
        private String inpatientNumber;
        public RegisterInfo(){}
    }

    /**
     * 其他信息
     */
    @Data
    public static class OtherInfo {
        /**
         * 综合减负标志	0：不在综合减负名单内；1：在综合减负名单内
         */
        private String loadReductionMark;
        /**
         * 民政优抚对象标识	0：不是；1：市级民政人员；2：余姚民政人员
         */
        private String objectMark;
        /**
         * 社保卡刷卡是否需要输入密码	0：不需要；1：需要
         */
        private String isCheckPwd;
        /**
         * 预授权标识	0：没有预授权；1：有预授权
         */
        private String authorizationID;
        /**
         * 第三方代扣签约标识	0：无；1：线下免密代扣；2：移动支付；9：都有
         */
        private String thirdPartyWithholding;
        /**
         * 系统保留
         */
        private String systemRetention;
        public OtherInfo(){}
    }
}
