package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Data
public class GaiaKeyCommodityTaskBillRelationInData implements Serializable {
    private static final long serialVersionUID = -35095695145838149L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 任务单号
     */
    private String billCode;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 商品规格
     */
    private String proSpecs;
    /**
     * 生产厂家
     */
    private String proFactory;
    /**
     * 零售价
     */
    private BigDecimal price;
    /**
     * 同期销售额
     */
    private BigDecimal tqSalesAmt;
    /**
     * 上期销售额
     */
    private BigDecimal sqSalesAmt;
    /**
     * 同期销售量
     */
    private BigDecimal tqSalesQty;
    /**
     * 上期销售量
     */
    private BigDecimal sqSalesQty;
    /**
     * 本期计划销售量
     */
    private BigDecimal planSalesQty;
    /**
     * 本期计划销售额
     */
    private BigDecimal planSalesAmt;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;

    private List<LevelInfoOutData> levelInfos;


}
