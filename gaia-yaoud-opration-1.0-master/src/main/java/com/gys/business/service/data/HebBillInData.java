package com.gys.business.service.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.gys.business.service.HebBillData;
import lombok.Data;

import java.util.List;

@Data
@JSONType(orders={"billList","storeCode"})
public class HebBillInData {
    @JSONField(ordinal=1)
    List<HebBillData> billList;
    @JSONField(ordinal=2)
    String storeCode;

//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder("{");
//        sb.append("\"billList\":")
//                .append(billList);
//        sb.append(",\"storeCode\":\"")
//                .append(storeCode).append('\"');
//        sb.append('}');
//        return sb.toString();
//    }
}
