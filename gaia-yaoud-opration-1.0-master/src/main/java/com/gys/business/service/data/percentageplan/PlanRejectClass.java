package com.gys.business.service.data.percentageplan;

import lombok.Data;

@Data
public class PlanRejectClass {
    private Integer pid;
    private String proBigClass;
    private String proMidClass;
    private String proClass;
    private String client;
}
