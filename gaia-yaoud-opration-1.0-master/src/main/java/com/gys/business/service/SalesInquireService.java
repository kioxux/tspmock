
package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface SalesInquireService {
    PageInfo<SalesInquireOutData> getSalesInquireList(SalesInquireInData inData);

    List<SalesInquireDetailOutData> getSalesInquireDetailList(SalesInquireInData inData);

    List<GaiaSdPayMethodOutData> queryPayMethod(GetLoginOutData userInfo);
}
