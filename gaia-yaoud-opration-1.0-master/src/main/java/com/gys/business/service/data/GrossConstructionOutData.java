package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 14:16 2021/9/21
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class GrossConstructionOutData {
    /**
     *定价毛利率
     */
    private BigDecimal pricingGrossRate;
    /**
     * 销售毛利率
     */
    private BigDecimal saleGrossRate;
    /**
     * 毛利贡献率
     */
    private BigDecimal grossContributionRate;
    /**
     * 折扣率
     */
    private BigDecimal zkRate;
    /**
     * 动销品项
     */
    private Integer proCount;
    /**
     * 销售额
     */
    private BigDecimal amt;
    /**
     * 销售占比
     */
    private BigDecimal saleProportion;
    /**
     * 毛利区间
     */
    private String interval;
    /**
     * 分类名称
     */
    private String className;
    /**
     * 分类编码
     */
    private String classCode;
}
