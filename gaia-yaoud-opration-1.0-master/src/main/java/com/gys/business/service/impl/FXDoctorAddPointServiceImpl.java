package com.gys.business.service.impl;

import com.gys.business.mapper.FXDoctorAddPointMapper;
import com.gys.business.service.FXDoctorAddPointService;
import com.gys.business.service.data.GaiaDoctorAddPointInData;
import com.gys.business.service.data.GaiaDoctorAddPointQueryData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 9:58 2021/7/23
 * @Description：医生加点service
 * @Modified By：guoyuxi.
 * @Version:
 */
@Service
public class FXDoctorAddPointServiceImpl implements FXDoctorAddPointService {

    @Autowired
    private FXDoctorAddPointMapper fxDoctorAddPointMapper;

    @Override
    public JsonResult saveDoctorAddPoint(GaiaDoctorAddPointInData inData) {

        if (inData.getGsdaRate() != null && inData.getGsdaRate().length() > 0) {
            if (inData.getGsdaAmt() != null && inData.getGsdaAmt().length() > 0) {
                return JsonResult.fail(200,"加点率和加点金额不能同时存在");
            }
        }
        if (inData.getGsdaRate()!=null&&inData.getGsdaRate().length() == 0) {
            inData.setGsdaRate(null);
        }
        if (inData.getGsdaAmt()!=null&&inData.getGsdaAmt().length() == 0) {
            inData.setGsdaAmt(null);
        }
        if (inData.getGsdaRate()!=null&&inData.getGsdaRate().contains("%")) {
            inData.setGsdaRate(inData.getGsdaRate().replace("%",""));
        }
        GaiaDoctorAddPointInData findOne=fxDoctorAddPointMapper.findAddPointByDoctorId(inData);
        Date now = new Date();
        String date = new SimpleDateFormat("yyyyMMdd").format(now);
        inData.setGsdaUpdateDate(date);
        String time = new SimpleDateFormat("HHmmss").format(now);
        inData.setGsdaUpdateTime(time);
        if (ObjectUtils.isEmpty(findOne)) {
            fxDoctorAddPointMapper.saveDoctorAddPoint(inData);
        } else {
            fxDoctorAddPointMapper.updateDoctorAddPoint(inData);
        }
        return JsonResult.success(null,"保存成功");
    }

    @Override
    public PageInfo findDoctorAddPointPage(GaiaDoctorAddPointQueryData queryData) {

        if (queryData.getStartDate() != null) {
        queryData.setStartDate(queryData.getStartDate().replace("-",""));
        }
        if (queryData.getEndDate() != null) {
        queryData.setEndDate(queryData.getEndDate().replace("-",""));
        }
        List<GaiaDoctorAddPointInData> pageDataList=fxDoctorAddPointMapper.findDoctorAddPointPageByClient(queryData);
        pageDataList.forEach(addPoint->{
            if (addPoint.getGsdaRate()!=null&&!addPoint.getGsdaRate().contains("%")) {
                addPoint.setGsdaRate(addPoint.getGsdaRate()+"%");
            }

            if (addPoint.getGsdaUpdateTime() != null) {
                String date = addPoint.getGsdaUpdateDate();
                String time = addPoint.getGsdaUpdateTime();
                SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
                SimpleDateFormat sdfDate=new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat sdftime = new SimpleDateFormat("HHmmdd");
                SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:dd");

                try {
                    addPoint.setGsdaUpdateDate(sdfDate.format(sdf.parse(date)));
                    addPoint.setGsdaUpdateTime(sdft.format(sdftime.parse(time)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        return new PageInfo<GaiaDoctorAddPointInData>(pageDataList);
    }

    @Override
    public JsonResult findDoctorNameList(GetLoginOutData loginUser) {
        List<Map<String,String>> doctorList=fxDoctorAddPointMapper.findDoctorListByClient(loginUser.getClient());
        return JsonResult.success(doctorList,"查询成功");
    }
}
