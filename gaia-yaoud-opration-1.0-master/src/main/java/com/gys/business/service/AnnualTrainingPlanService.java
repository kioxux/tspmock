package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaAnnualTrainingPlan;
import com.gys.business.service.data.AnnualTrainingPlanInData;
import com.gys.business.service.data.AnnualTrainingPlanOutData;
import com.gys.common.response.Result;

import java.util.List;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2022/1/3 18:35
 */
public interface AnnualTrainingPlanService {
    void saveAnnualTrainingPlan(List<GaiaAnnualTrainingPlan> plan, String client, String user);

    List<AnnualTrainingPlanOutData> getAnnualTrainingPlanList(AnnualTrainingPlanInData inData);

    void deleteAnnualTrainingPlan(List<GaiaAnnualTrainingPlan> plan, String client);

    Result exportAnnualTrainingPlanList(AnnualTrainingPlanInData inData);
}
