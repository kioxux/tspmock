package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GetProductThirdlyOutDataExtends extends GetProductThirdlyOutData{

    private String proId ;// "商品编码";

    public String getProId() {
        return super.getProCode();
    }
}
