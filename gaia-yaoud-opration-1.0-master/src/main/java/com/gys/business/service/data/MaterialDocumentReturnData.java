package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xiaoyuan on 2020/12/19
 */
@Data
public class MaterialDocumentReturnData implements Serializable {
    private static final long serialVersionUID = -2644568440690677183L;

    /**
     * 移动平均单价
     */
    private String gssdMovPrice;

    /**
     * 店号
     */
    private String gssdBrId;

    /**
     * 移动税额
     */
    private String gssdTaxRate;

    /**
     * 销售单号
     */
    private String gssdBillNo;

    /**
     * 行号
     */
    private String gssdSerial;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 移动平均总价
     */
    private String gssdBatchCost;

    /**
     * 销售日期
     */
    private String gssdDate;

    /**
     * 加点后金额
     */
    private BigDecimal gssdAddAmt;

    /**
     * 加点后税金
     */
    private BigDecimal gssdAddTax;


}
