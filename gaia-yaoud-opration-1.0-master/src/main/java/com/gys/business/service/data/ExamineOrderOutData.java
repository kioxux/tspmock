package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ExamineOrderOutData {
    @ApiModelProperty(value = "验收单号")
    private String voucherId;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "商品编码")
    private String proId;
    @ApiModelProperty(value = "收货日期")
    private String examineDate;
    @ApiModelProperty(value = "验收数量")
    private BigDecimal recipientQty;
    @ApiModelProperty(value = "合格数量")
    private BigDecimal qualifiedQty;
    @ApiModelProperty(value = "不合格数量")
    private BigDecimal unqualifiedQty;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "生产厂家")
    private String factoryName;
    @ApiModelProperty(value = "有效期")
    private String validDate;
    @ApiModelProperty(value = "验收人员")
    private String gsedEmp;
    @ApiModelProperty(value = "验收结论")
    private String gsedResult;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    @ApiModelProperty(value = "收货单号")
    private String voucherAcceptId;
    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "门店名称")
    private String stoShortName;
    @ApiModelProperty(value = "国家贯标编码")
    private String medProductCode;
    @ApiModelProperty(value = "医保机构定点编码")
    private String stoYbCode;
    @ApiModelProperty(value = "商品通用名")
    private String proCommonname;
    @ApiModelProperty(value = "商品描述")
    private String proDepict;
}
