package com.gys.business.service.data.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/24 16:45
 **/
@ApiModel
@Data
public class AddDeviceForm {
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(hidden = true)
    private String deviceSite;
    @ApiModelProperty(value = "门店名称", example = "福星店")
    private String storeName;
    @ApiModelProperty(value = "设备编号",example = "0001")
    private String deviceNo;
    @ApiModelProperty(value = "设备名称", example = "0001")
    private String deviceName;
    @ApiModelProperty(value = "设备型号", example = "SN-01")
    private String deviceSpec;
    @ApiModelProperty(value = "生产厂家", example = "济南制药")
    private String factoryName;
    @ApiModelProperty(value = "启用日期", example = "20210622")
    private String startDate;
    @ApiModelProperty(value = "设备数量", example = "2")
    private String deviceNum;
    @ApiModelProperty(value = "是否有说明书:1-是 0-否", example = "1")
    private String instructionFlag;
    @ApiModelProperty(value = "是否复核:1-是 0-否", example = "1")
    private String checkFlag;
    @ApiModelProperty(value = "是否检验合格：1-是 0-否", example = "0")
    private String qualifyFlag;
}
