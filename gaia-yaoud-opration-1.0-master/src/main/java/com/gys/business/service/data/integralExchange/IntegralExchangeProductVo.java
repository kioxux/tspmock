package com.gys.business.service.data.integralExchange;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author ：gyx
 * @Date ：Created in 10:27 2021/11/29
 * @Description：积分换购批量导入vo
 * @Modified By：gyx
 * @Version:
 */

@Data
public class IntegralExchangeProductVo {
    //导入时@ExcelProperty注解要么只用index定位要么只用value定位，不可以两个一起用
    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 门店ID
     */
    private String stoCode;

    /**
     * 商品编码
     */
    @ExcelProperty(value = "商品编码")
    private String proSelfCode;


    /**
     * 拼音码
     */
    private String pym;

    /**
     * 商品名
     */
    private String productName;
    /**
     * 规格
     */
    private String spec;
    /**
     * 单位
     */
    private String unit;

    /**
     * 买满金额
     */
    @ExcelProperty(value = "买满金额")
    private BigDecimal buyAmt;

    /**
     * 所需积分
     */
    @ExcelProperty(value = "所需积分")
    private BigDecimal exchangeIntegra;

    /**
     * 换购金额
     */
    @ExcelProperty(value = "换购金额")
    private BigDecimal exchangeAmt;

    /**
     * 兑换数量
     */
    @ExcelProperty(value = "兑换数量")
    private BigDecimal exchangeQty;

    /**
     * 限兑数量
     */
    @ExcelProperty(value = "限兑数量")
    private BigDecimal periodMaxQty;

    /**
     * 期间限兑（购）天数
     */
    @ExcelProperty(value = "限兑期间天数")
    private BigDecimal periodDays;

    /**
     * 本商品是否积分（1-是、0-否）
     */
    @ExcelProperty(value = "是否积分")
    private String integralFlag;

    /**
     * 是否删除（1-是、0-否）
     */
    private String deleteFlag;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateUser;



}
