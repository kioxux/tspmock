package com.gys.business.service.data.check;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/10 17:48
 **/
@Data
public class RequestOrderDetailVO {
    @ApiModelProperty(value = "序号", example = "1")
    private Integer index;
    @ApiModelProperty(value = "商品编码", example = "42100003")
    private String proSelfCode;
    @ApiModelProperty(value = "商品名称", example = "阿莫西林分散片")
    private String proName;
    @ApiModelProperty(value = "商品规格", example = "50mg*12片")
    private String proSpec;
    @ApiModelProperty(value = "生产厂家", example = "广州白云山")
    private String factoryName;
    @ApiModelProperty(value = "商品单位", example = "盒")
    private String proUnit;
    @ApiModelProperty(value = "要货数量", example = "10")
    private BigDecimal requestQuantity;
    @ApiModelProperty(value = "门店库存", example = "0")
    private BigDecimal storeStock;
    @ApiModelProperty(value = "委托方库存", example = "20")
    private BigDecimal supplierStock;
    @ApiModelProperty(value = "含税进价", example = "23.01")
    private BigDecimal costPrice;
    @ApiModelProperty(value = "配送单价", example = "176.99")
    private BigDecimal sendPrice;
    @ApiModelProperty(value = "配送金额", example = "200。00")
    private BigDecimal sendAmount;
    @ApiModelProperty(value = "零售价", example = "29.80")
    private BigDecimal salePrice;
    @ApiModelProperty(value = "零售金额", example = "300")
    private BigDecimal saleAmount;
    @ApiModelProperty(value = "仓库库存", example = "0")
    private BigDecimal dcStock;
    @ApiModelProperty(value = "7天销量", example = "3")
    private BigDecimal weekSales;
    @ApiModelProperty(value = "30天销量", example = "9")
    private BigDecimal monthSales;
    @ApiModelProperty(value = "中包装量", example = "1")
    private BigDecimal midPackSize;
    @ApiModelProperty(value = "是否医保：0-否、1-是", example = "0")
    private String medicalFlag;
    @ApiModelProperty(value = "备注", example = "已电话沟通。")
    private String remarks;
}
