package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaStoreOutSuggestionRelation;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionH)实体类
 *
 * @author XIAO-ZY
 * @since 2021-10-28 10:54:04
 */
@Data
public class GaiaStoreOutSuggestionInData {

    private Long id;

    private String client;

    private String stoCode;

    private String stoName;

    private String billCode;

    private String startTime;

    private String endTime;

    private String status;

    private String type;

    private int pageNum;

    private int pageSize;

    private List<GaiaStoreOutSuggestionDOutData> insertList;

    private List<GaiaStoreOutSuggestionRelation> stoList;

    private List<HashMap<String,Object>> confirmDetailList;

}

