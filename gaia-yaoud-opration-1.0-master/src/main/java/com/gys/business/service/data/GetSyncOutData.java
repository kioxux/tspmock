//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.util.List;

public class GetSyncOutData {
    private List<GetSyncProductBusinessOutData> productBusinessList;
    private List<GetSyncSdProductPriceOutData> productPriceList;
    private List<GetSyncSdProductLocationOutData> productLocationList;
    private List<GetSyncSdStockOutData> sdStockList;
    private List<GetSyncSdStockBatchOutData> sdStockBatchList;
    private List<GetSyncSdStoreOutData> sdStoreList;
    private List<GetSyncSdEmpGroupOutData> sdEmpGroupList;
    private List<GetSyncSdSaleHOutData> sdSaleHList;
    private List<GetSyncSdSaleDOutData> sdSaleDList;

    public GetSyncOutData() {
    }

    public List<GetSyncProductBusinessOutData> getProductBusinessList() {
        return this.productBusinessList;
    }

    public List<GetSyncSdProductPriceOutData> getProductPriceList() {
        return this.productPriceList;
    }

    public List<GetSyncSdProductLocationOutData> getProductLocationList() {
        return this.productLocationList;
    }

    public List<GetSyncSdStockOutData> getSdStockList() {
        return this.sdStockList;
    }

    public List<GetSyncSdStockBatchOutData> getSdStockBatchList() {
        return this.sdStockBatchList;
    }

    public List<GetSyncSdStoreOutData> getSdStoreList() {
        return this.sdStoreList;
    }

    public List<GetSyncSdEmpGroupOutData> getSdEmpGroupList() {
        return this.sdEmpGroupList;
    }

    public List<GetSyncSdSaleHOutData> getSdSaleHList() {
        return this.sdSaleHList;
    }

    public List<GetSyncSdSaleDOutData> getSdSaleDList() {
        return this.sdSaleDList;
    }

    public void setProductBusinessList(final List<GetSyncProductBusinessOutData> productBusinessList) {
        this.productBusinessList = productBusinessList;
    }

    public void setProductPriceList(final List<GetSyncSdProductPriceOutData> productPriceList) {
        this.productPriceList = productPriceList;
    }

    public void setProductLocationList(final List<GetSyncSdProductLocationOutData> productLocationList) {
        this.productLocationList = productLocationList;
    }

    public void setSdStockList(final List<GetSyncSdStockOutData> sdStockList) {
        this.sdStockList = sdStockList;
    }

    public void setSdStockBatchList(final List<GetSyncSdStockBatchOutData> sdStockBatchList) {
        this.sdStockBatchList = sdStockBatchList;
    }

    public void setSdStoreList(final List<GetSyncSdStoreOutData> sdStoreList) {
        this.sdStoreList = sdStoreList;
    }

    public void setSdEmpGroupList(final List<GetSyncSdEmpGroupOutData> sdEmpGroupList) {
        this.sdEmpGroupList = sdEmpGroupList;
    }

    public void setSdSaleHList(final List<GetSyncSdSaleHOutData> sdSaleHList) {
        this.sdSaleHList = sdSaleHList;
    }

    public void setSdSaleDList(final List<GetSyncSdSaleDOutData> sdSaleDList) {
        this.sdSaleDList = sdSaleDList;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSyncOutData)) {
            return false;
        } else {
            GetSyncOutData other = (GetSyncOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label119: {
                    Object this$productBusinessList = this.getProductBusinessList();
                    Object other$productBusinessList = other.getProductBusinessList();
                    if (this$productBusinessList == null) {
                        if (other$productBusinessList == null) {
                            break label119;
                        }
                    } else if (this$productBusinessList.equals(other$productBusinessList)) {
                        break label119;
                    }

                    return false;
                }

                Object this$productPriceList = this.getProductPriceList();
                Object other$productPriceList = other.getProductPriceList();
                if (this$productPriceList == null) {
                    if (other$productPriceList != null) {
                        return false;
                    }
                } else if (!this$productPriceList.equals(other$productPriceList)) {
                    return false;
                }

                label105: {
                    Object this$productLocationList = this.getProductLocationList();
                    Object other$productLocationList = other.getProductLocationList();
                    if (this$productLocationList == null) {
                        if (other$productLocationList == null) {
                            break label105;
                        }
                    } else if (this$productLocationList.equals(other$productLocationList)) {
                        break label105;
                    }

                    return false;
                }

                Object this$sdStockList = this.getSdStockList();
                Object other$sdStockList = other.getSdStockList();
                if (this$sdStockList == null) {
                    if (other$sdStockList != null) {
                        return false;
                    }
                } else if (!this$sdStockList.equals(other$sdStockList)) {
                    return false;
                }

                label91: {
                    Object this$sdStockBatchList = this.getSdStockBatchList();
                    Object other$sdStockBatchList = other.getSdStockBatchList();
                    if (this$sdStockBatchList == null) {
                        if (other$sdStockBatchList == null) {
                            break label91;
                        }
                    } else if (this$sdStockBatchList.equals(other$sdStockBatchList)) {
                        break label91;
                    }

                    return false;
                }

                Object this$sdStoreList = this.getSdStoreList();
                Object other$sdStoreList = other.getSdStoreList();
                if (this$sdStoreList == null) {
                    if (other$sdStoreList != null) {
                        return false;
                    }
                } else if (!this$sdStoreList.equals(other$sdStoreList)) {
                    return false;
                }

                label77: {
                    Object this$sdEmpGroupList = this.getSdEmpGroupList();
                    Object other$sdEmpGroupList = other.getSdEmpGroupList();
                    if (this$sdEmpGroupList == null) {
                        if (other$sdEmpGroupList == null) {
                            break label77;
                        }
                    } else if (this$sdEmpGroupList.equals(other$sdEmpGroupList)) {
                        break label77;
                    }

                    return false;
                }

                label70: {
                    Object this$sdSaleHList = this.getSdSaleHList();
                    Object other$sdSaleHList = other.getSdSaleHList();
                    if (this$sdSaleHList == null) {
                        if (other$sdSaleHList == null) {
                            break label70;
                        }
                    } else if (this$sdSaleHList.equals(other$sdSaleHList)) {
                        break label70;
                    }

                    return false;
                }

                Object this$sdSaleDList = this.getSdSaleDList();
                Object other$sdSaleDList = other.getSdSaleDList();
                if (this$sdSaleDList == null) {
                    if (other$sdSaleDList != null) {
                        return false;
                    }
                } else if (!this$sdSaleDList.equals(other$sdSaleDList)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSyncOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $productBusinessList = this.getProductBusinessList();
        result = result * 59 + ($productBusinessList == null ? 43 : $productBusinessList.hashCode());
        Object $productPriceList = this.getProductPriceList();
        result = result * 59 + ($productPriceList == null ? 43 : $productPriceList.hashCode());
        Object $productLocationList = this.getProductLocationList();
        result = result * 59 + ($productLocationList == null ? 43 : $productLocationList.hashCode());
        Object $sdStockList = this.getSdStockList();
        result = result * 59 + ($sdStockList == null ? 43 : $sdStockList.hashCode());
        Object $sdStockBatchList = this.getSdStockBatchList();
        result = result * 59 + ($sdStockBatchList == null ? 43 : $sdStockBatchList.hashCode());
        Object $sdStoreList = this.getSdStoreList();
        result = result * 59 + ($sdStoreList == null ? 43 : $sdStoreList.hashCode());
        Object $sdEmpGroupList = this.getSdEmpGroupList();
        result = result * 59 + ($sdEmpGroupList == null ? 43 : $sdEmpGroupList.hashCode());
        Object $sdSaleHList = this.getSdSaleHList();
        result = result * 59 + ($sdSaleHList == null ? 43 : $sdSaleHList.hashCode());
        Object $sdSaleDList = this.getSdSaleDList();
        result = result * 59 + ($sdSaleDList == null ? 43 : $sdSaleDList.hashCode());
        return result;
    }

    public String toString() {
        return "GetSyncOutData(productBusinessList=" + this.getProductBusinessList() + ", productPriceList=" + this.getProductPriceList() + ", productLocationList=" + this.getProductLocationList() + ", sdStockList=" + this.getSdStockList() + ", sdStockBatchList=" + this.getSdStockBatchList() + ", sdStoreList=" + this.getSdStoreList() + ", sdEmpGroupList=" + this.getSdEmpGroupList() + ", sdSaleHList=" + this.getSdSaleHList() + ", sdSaleDList=" + this.getSdSaleDList() + ")";
    }
}
