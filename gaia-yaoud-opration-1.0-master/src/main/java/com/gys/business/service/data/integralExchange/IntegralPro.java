package com.gys.business.service.data.integralExchange;

import lombok.Data;

@Data
public class IntegralPro {
    private String clientId;
    private String vourcherId;
    private String serial;
    private String proId;
}
