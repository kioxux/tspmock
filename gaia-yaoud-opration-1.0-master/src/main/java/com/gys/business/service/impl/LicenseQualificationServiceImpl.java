package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.GaiaSdMessageMapper;
import com.gys.business.mapper.LicenseQualificationMapper;
import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.service.GaiaLicenseQualificationOutData;
import com.gys.business.service.LicenseQualificationService;
import com.gys.business.service.data.GaiaLicenseQualificationInData;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.SdMessageTypeEnum;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LicenseQualificationServiceImpl implements LicenseQualificationService {

    @Autowired
    private LicenseQualificationMapper licenseQualificationMapper;
    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Override
    public List<Map<String, String>> listSite(Map<String,String> queryMap) {
        if (StrUtil.isEmpty(queryMap.get("client"))) {
            throw new BusinessException("请重新登录");
        }
        return licenseQualificationMapper.listSite(queryMap);
    }

    @Override
    public List<Map<String, String>> listSup(Map<String, String> queryMap) {
        if (StrUtil.isEmpty(queryMap.get("client"))) {
            throw new BusinessException("请重新登录");
        }
        if (StrUtil.isEmpty(queryMap.get("siteCode"))) {
            throw new BusinessException("地点编码必填");
        }
        return licenseQualificationMapper.listSup(queryMap);
    }

    @Override
    public List<Map<String, String>> listCus(Map<String, String> queryMap) {
        if (StrUtil.isEmpty(queryMap.get("client"))) {
            throw new BusinessException("请重新登录");
        }
        if (StrUtil.isEmpty(queryMap.get("siteCode"))) {
            throw new BusinessException("地点编码必填");
        }
        return licenseQualificationMapper.listCus(queryMap);
    }

    @Override
    public List<Map<String, String>> listSto(Map<String, String> queryMap) {
        if (StrUtil.isEmpty(queryMap.get("client"))) {
            throw new BusinessException("请重新登录");
        }
        if (StrUtil.isEmpty(queryMap.get("siteCode"))) {
            throw new BusinessException("地点编码必填");
        }
        // 通过地点编码判断是否为配置中心
        Integer count = licenseQualificationMapper.getCount(queryMap);
        if (count>0){
            // 当查询机构为配送中心时，默认可选择所有门店；(去掉门店参数)
            queryMap.remove("siteCode");
        }
        return licenseQualificationMapper.listSto(queryMap);
    }

    @Override
    public PageInfo<GaiaLicenseQualificationOutData> list(GaiaLicenseQualificationInData inData) {
        if (StrUtil.isEmpty(inData.getClient())) {
            throw new BusinessException("请重新登录");
        }

        if (StrUtil.isNotEmpty(inData.getDays())&&!NumberUtil.isNumber(inData.getDays())) {
            throw new BusinessException("请输入纯数字的有效天数");
        }
        List<GaiaLicenseQualificationOutData> outData=new ArrayList<>();

        switch (inData.getQueryType()) {
            case "sup":
                outData=licenseQualificationMapper.listSupQueryType(inData);
                break;
            case "cus":
                outData=licenseQualificationMapper.listCusQueryType(inData);
                break;
            case "pro":
                if (StrUtil.isNotEmpty(inData.getProCode())) {
                    inData.setProCodeList(Arrays.stream(inData.getProCode().split(",")).collect(Collectors.toList()));
                }
                outData=licenseQualificationMapper.listProQueryType(inData);
                break;
            case "sto":
                if (StrUtil.isNotEmpty(inData.getStoCode())) {
                    inData.setStoCodeList(Arrays.stream(inData.getStoCode().split(",")).collect(Collectors.toList()));
                }
                HashMap<String, String> queryMap = new HashMap<>(2);
                queryMap.put("client",inData.getClient());
                queryMap.put("siteCode",inData.getSiteCode());
                // 通过地点编码判断是否为配置中心
                Integer count = licenseQualificationMapper.getCount(queryMap);
                if (count>0){
                    // 当查询机构为配送中心时，去掉门店参数
                    inData.setSiteCode(null);
                }
                outData=licenseQualificationMapper.listStoQueryType(inData);
                break;
        }
        if (CollectionUtil.isEmpty(outData)) {
            return new PageInfo<>();
        }
        return new PageInfo<>(outData);
    }


    /**
     * 每天生成所有加盟商小于180天的证照资质预警消息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void dailyInsertToMessage() {
        //删除历史消息
        deleteMessage();
        //获取所有加盟商
        List<String> clientList=licenseQualificationMapper.listClients();

        clientList.forEach(client->{
                //组装参数
                GaiaLicenseQualificationInData inData=makeParams(client);
                //查询结果
                List<GaiaLicenseQualificationOutData> outData=queryForResult(inData);
                //生成消息
                if (CollectionUtil.isEmpty(outData)) {
                    return;
                }

                inertToMessage(client,outData);
        });
    }

    private void deleteMessage() {
        gaiaSdMessageMapper.deleteLicenseMessage("WEB");
    }

    private void inertToMessage(String client,List<GaiaLicenseQualificationOutData> outData) {
        String voucherId = gaiaSdMessageMapper.selectNextVoucherId(client, "company");
        GaiaSdMessage message=new GaiaSdMessage();
        message.setClient(client);
        message.setGsmId("");
        message.setGsmVoucherId(voucherId);//流水号
        message.setGsmType(SdMessageTypeEnum.LICENSE_QUALIFICATION.code);
        message.setGsmPage(SdMessageTypeEnum.LICENSE_QUALIFICATION.page);
        message.setGsmRemark("截至目前，公司共有<font color=\"#FF0000\">" + outData.size() + "</font>条证照资质效期不足180天，请尽快处理。");
        message.setGsmFlag("N");
        message.setGsmPlatForm("WEB");
        message.setGsmDeleteFlag("0");
        message.setGsmArriveDate(DateUtil.format(DateUtil.date(),"yyyyMMdd"));
        message.setGsmArriveTime(DateUtil.format(DateUtil.date(),"HHmmss"));
        message.setGsmWarningDay("180");
        gaiaSdMessageMapper.insertSelective(message);
    }

    private List<GaiaLicenseQualificationOutData> queryForResult(GaiaLicenseQualificationInData inData) {
        List<GaiaLicenseQualificationOutData> outData=new ArrayList<>();
        inData.setDays("180");
        outData.addAll(licenseQualificationMapper.listSupQueryType(inData));
        outData.addAll(licenseQualificationMapper.listCusQueryType(inData));
        outData.addAll(licenseQualificationMapper.listProQueryType(inData));
        outData.addAll(licenseQualificationMapper.listStoQueryType(inData));
        return outData;
    }

    private GaiaLicenseQualificationInData makeParams(String client) {
        GaiaLicenseQualificationInData inData=new GaiaLicenseQualificationInData();
        inData.setClient(client);
        return inData;
    }
}
