package com.gys.business.service.data.DtYb.ProfitLoss;

import lombok.Data;

import java.util.List;

/**
 * @Description 大同易联众 损益商品
 * @Author huxinxin
 * @Date 2021/5/11 16:56
 * @Version 1.0.0
 **/
@Data
public class ProfitlossVO {
    // 损溢时间
    private String plDate;
    // 损溢单据号
    private String InventoryVO;
    // 损溢类型
    private String plType;
    private List<ProfitlossProVO> proList;
}
