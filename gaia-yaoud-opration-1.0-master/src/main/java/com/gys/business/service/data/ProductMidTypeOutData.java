package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ProductMidTypeOutData implements Serializable {

    /**
     * 中类编码
     */
    @ApiModelProperty(value="中类编码")
    private String midCode;

    /**
     * 中类名称
     */
    @ApiModelProperty(value="中类名称")
    private String midName;

    /**
     * 库存品项数
     */
    @ApiModelProperty(value="库存品项数")
    private BigDecimal stockCount;

    /**
     * 库存品成本额
     */
    @ApiModelProperty(value="库存品成本额")
    private BigDecimal costAmt;

    /**
     * 周转天数
     */
    @ApiModelProperty(value="周转天数")
    private BigDecimal turnoverDays;
}
