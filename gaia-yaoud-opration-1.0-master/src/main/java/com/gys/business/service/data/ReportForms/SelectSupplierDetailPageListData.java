package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectSupplierDetailPageListData {

    @ApiModelProperty(value = "加盟商")
    private String client;

   /* @ApiModelProperty(value = "仓库地点")
    private String siteCode;*/

    @ApiModelProperty(value = "仓库地点/门店编码")
    private String stoCode;

    @ApiModelProperty(value = "仓库地点/门店编码")
    private String depId;

    @ApiModelProperty(value = "供应商编码")
    private String supSelfCode;

    @ApiModelProperty(value = "开始时间")
    private String beginTime;

    @ApiModelProperty(value = "结束时间")
    private String endTime;

    @ApiModelProperty(value = "业务员名称")
    private String salesmanName;

    @ApiModelProperty(value = "业务员类型")
    private String matType;

}
