package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserOutData {
   @ApiModelProperty(value = "用户编码")
   private String userId;
   @ApiModelProperty(value = "加盟商")
   private String client;
   @ApiModelProperty(value = "用户名")
   private String userNam;
   @ApiModelProperty(value = "性别")
   private String userSex;
   @ApiModelProperty(value = "手机号")
   private String userTel;
   private String userIdc;
   private String userYsId;
   private String userYsZyfw;
   private String userYsZylb;
   @ApiModelProperty(value = "部门编码")
   private String depId;
   @ApiModelProperty(value = "部门")
   private String depName;
   @ApiModelProperty(value = "地址")
   private String userAddr;
   private String userSta;
   private String francCreDate;
   private String francCreTime;
   private String francCreId;
   private String francModiDate;
   private String francModiTime;
   private String francModiId;
   private String userJoinDate;
   private String userDisDate;
   private String userEmail;
   private String userPassword;
}
