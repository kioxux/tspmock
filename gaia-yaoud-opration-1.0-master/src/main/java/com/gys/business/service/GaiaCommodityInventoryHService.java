package com.gys.business.service;

import com.gys.business.service.data.GaiaCommodityInventoryHInData;
import com.gys.common.response.Result;

/**
 * 商品调库-主表(GaiaCommodityInventoryH)表服务接口
 *
 * @author makejava
 * @since 2021-10-19 16:00:45
 */
public interface GaiaCommodityInventoryHService {

    Object insertInventoryData(String param);

    Object listBill(GaiaCommodityInventoryHInData param);

    Object listBillDetail(GaiaCommodityInventoryHInData param);

    Object listUnSendStore(GaiaCommodityInventoryHInData param);

    Object listStoreLine(GaiaCommodityInventoryHInData param);

    Result exportBillDetail(GaiaCommodityInventoryHInData param);

    void updateInventoryData(String param);

}
