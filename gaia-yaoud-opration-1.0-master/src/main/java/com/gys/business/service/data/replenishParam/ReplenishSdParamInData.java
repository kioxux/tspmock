package com.gys.business.service.data.replenishParam;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReplenishSdParamInData {
    private String gsepId;
    private BigDecimal gsepPara1;
    private BigDecimal gsepPara2;
    private BigDecimal gsepPara3;
}
