//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;

public class PromGiftSetOutData {
    private String clientId;
    private String gspgsVoucherId;
    private String gspgsSerial;
    private String gspgsSeriesProId;
    private String gspgsReachQty1;
    private BigDecimal gspgsReachAmt1;
    private String gspgsResultQty1;
    private String gspgsReachQty2;
    private BigDecimal gspgsReachAmt2;
    private String gspgsResultQty2;
    private String gspgsReachQty3;
    private BigDecimal gspgsReachAmt3;
    private String gspgsResultQty3;

    public PromGiftSetOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGspgsVoucherId() {
        return this.gspgsVoucherId;
    }

    public String getGspgsSerial() {
        return this.gspgsSerial;
    }

    public String getGspgsSeriesProId() {
        return this.gspgsSeriesProId;
    }

    public String getGspgsReachQty1() {
        return this.gspgsReachQty1;
    }

    public BigDecimal getGspgsReachAmt1() {
        return this.gspgsReachAmt1;
    }

    public String getGspgsResultQty1() {
        return this.gspgsResultQty1;
    }

    public String getGspgsReachQty2() {
        return this.gspgsReachQty2;
    }

    public BigDecimal getGspgsReachAmt2() {
        return this.gspgsReachAmt2;
    }

    public String getGspgsResultQty2() {
        return this.gspgsResultQty2;
    }

    public String getGspgsReachQty3() {
        return this.gspgsReachQty3;
    }

    public BigDecimal getGspgsReachAmt3() {
        return this.gspgsReachAmt3;
    }

    public String getGspgsResultQty3() {
        return this.gspgsResultQty3;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGspgsVoucherId(final String gspgsVoucherId) {
        this.gspgsVoucherId = gspgsVoucherId;
    }

    public void setGspgsSerial(final String gspgsSerial) {
        this.gspgsSerial = gspgsSerial;
    }

    public void setGspgsSeriesProId(final String gspgsSeriesProId) {
        this.gspgsSeriesProId = gspgsSeriesProId;
    }

    public void setGspgsReachQty1(final String gspgsReachQty1) {
        this.gspgsReachQty1 = gspgsReachQty1;
    }

    public void setGspgsReachAmt1(final BigDecimal gspgsReachAmt1) {
        this.gspgsReachAmt1 = gspgsReachAmt1;
    }

    public void setGspgsResultQty1(final String gspgsResultQty1) {
        this.gspgsResultQty1 = gspgsResultQty1;
    }

    public void setGspgsReachQty2(final String gspgsReachQty2) {
        this.gspgsReachQty2 = gspgsReachQty2;
    }

    public void setGspgsReachAmt2(final BigDecimal gspgsReachAmt2) {
        this.gspgsReachAmt2 = gspgsReachAmt2;
    }

    public void setGspgsResultQty2(final String gspgsResultQty2) {
        this.gspgsResultQty2 = gspgsResultQty2;
    }

    public void setGspgsReachQty3(final String gspgsReachQty3) {
        this.gspgsReachQty3 = gspgsReachQty3;
    }

    public void setGspgsReachAmt3(final BigDecimal gspgsReachAmt3) {
        this.gspgsReachAmt3 = gspgsReachAmt3;
    }

    public void setGspgsResultQty3(final String gspgsResultQty3) {
        this.gspgsResultQty3 = gspgsResultQty3;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PromGiftSetOutData)) {
            return false;
        } else {
            PromGiftSetOutData other = (PromGiftSetOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label167: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label167;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label167;
                    }

                    return false;
                }

                Object this$gspgsVoucherId = this.getGspgsVoucherId();
                Object other$gspgsVoucherId = other.getGspgsVoucherId();
                if (this$gspgsVoucherId == null) {
                    if (other$gspgsVoucherId != null) {
                        return false;
                    }
                } else if (!this$gspgsVoucherId.equals(other$gspgsVoucherId)) {
                    return false;
                }

                label153: {
                    Object this$gspgsSerial = this.getGspgsSerial();
                    Object other$gspgsSerial = other.getGspgsSerial();
                    if (this$gspgsSerial == null) {
                        if (other$gspgsSerial == null) {
                            break label153;
                        }
                    } else if (this$gspgsSerial.equals(other$gspgsSerial)) {
                        break label153;
                    }

                    return false;
                }

                Object this$gspgsSeriesProId = this.getGspgsSeriesProId();
                Object other$gspgsSeriesProId = other.getGspgsSeriesProId();
                if (this$gspgsSeriesProId == null) {
                    if (other$gspgsSeriesProId != null) {
                        return false;
                    }
                } else if (!this$gspgsSeriesProId.equals(other$gspgsSeriesProId)) {
                    return false;
                }

                label139: {
                    Object this$gspgsReachQty1 = this.getGspgsReachQty1();
                    Object other$gspgsReachQty1 = other.getGspgsReachQty1();
                    if (this$gspgsReachQty1 == null) {
                        if (other$gspgsReachQty1 == null) {
                            break label139;
                        }
                    } else if (this$gspgsReachQty1.equals(other$gspgsReachQty1)) {
                        break label139;
                    }

                    return false;
                }

                Object this$gspgsReachAmt1 = this.getGspgsReachAmt1();
                Object other$gspgsReachAmt1 = other.getGspgsReachAmt1();
                if (this$gspgsReachAmt1 == null) {
                    if (other$gspgsReachAmt1 != null) {
                        return false;
                    }
                } else if (!this$gspgsReachAmt1.equals(other$gspgsReachAmt1)) {
                    return false;
                }

                label125: {
                    Object this$gspgsResultQty1 = this.getGspgsResultQty1();
                    Object other$gspgsResultQty1 = other.getGspgsResultQty1();
                    if (this$gspgsResultQty1 == null) {
                        if (other$gspgsResultQty1 == null) {
                            break label125;
                        }
                    } else if (this$gspgsResultQty1.equals(other$gspgsResultQty1)) {
                        break label125;
                    }

                    return false;
                }

                label118: {
                    Object this$gspgsReachQty2 = this.getGspgsReachQty2();
                    Object other$gspgsReachQty2 = other.getGspgsReachQty2();
                    if (this$gspgsReachQty2 == null) {
                        if (other$gspgsReachQty2 == null) {
                            break label118;
                        }
                    } else if (this$gspgsReachQty2.equals(other$gspgsReachQty2)) {
                        break label118;
                    }

                    return false;
                }

                Object this$gspgsReachAmt2 = this.getGspgsReachAmt2();
                Object other$gspgsReachAmt2 = other.getGspgsReachAmt2();
                if (this$gspgsReachAmt2 == null) {
                    if (other$gspgsReachAmt2 != null) {
                        return false;
                    }
                } else if (!this$gspgsReachAmt2.equals(other$gspgsReachAmt2)) {
                    return false;
                }

                label104: {
                    Object this$gspgsResultQty2 = this.getGspgsResultQty2();
                    Object other$gspgsResultQty2 = other.getGspgsResultQty2();
                    if (this$gspgsResultQty2 == null) {
                        if (other$gspgsResultQty2 == null) {
                            break label104;
                        }
                    } else if (this$gspgsResultQty2.equals(other$gspgsResultQty2)) {
                        break label104;
                    }

                    return false;
                }

                label97: {
                    Object this$gspgsReachQty3 = this.getGspgsReachQty3();
                    Object other$gspgsReachQty3 = other.getGspgsReachQty3();
                    if (this$gspgsReachQty3 == null) {
                        if (other$gspgsReachQty3 == null) {
                            break label97;
                        }
                    } else if (this$gspgsReachQty3.equals(other$gspgsReachQty3)) {
                        break label97;
                    }

                    return false;
                }

                Object this$gspgsReachAmt3 = this.getGspgsReachAmt3();
                Object other$gspgsReachAmt3 = other.getGspgsReachAmt3();
                if (this$gspgsReachAmt3 == null) {
                    if (other$gspgsReachAmt3 != null) {
                        return false;
                    }
                } else if (!this$gspgsReachAmt3.equals(other$gspgsReachAmt3)) {
                    return false;
                }

                Object this$gspgsResultQty3 = this.getGspgsResultQty3();
                Object other$gspgsResultQty3 = other.getGspgsResultQty3();
                if (this$gspgsResultQty3 == null) {
                    if (other$gspgsResultQty3 != null) {
                        return false;
                    }
                } else if (!this$gspgsResultQty3.equals(other$gspgsResultQty3)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PromGiftSetOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gspgsVoucherId = this.getGspgsVoucherId();
        result = result * 59 + ($gspgsVoucherId == null ? 43 : $gspgsVoucherId.hashCode());
        Object $gspgsSerial = this.getGspgsSerial();
        result = result * 59 + ($gspgsSerial == null ? 43 : $gspgsSerial.hashCode());
        Object $gspgsSeriesProId = this.getGspgsSeriesProId();
        result = result * 59 + ($gspgsSeriesProId == null ? 43 : $gspgsSeriesProId.hashCode());
        Object $gspgsReachQty1 = this.getGspgsReachQty1();
        result = result * 59 + ($gspgsReachQty1 == null ? 43 : $gspgsReachQty1.hashCode());
        Object $gspgsReachAmt1 = this.getGspgsReachAmt1();
        result = result * 59 + ($gspgsReachAmt1 == null ? 43 : $gspgsReachAmt1.hashCode());
        Object $gspgsResultQty1 = this.getGspgsResultQty1();
        result = result * 59 + ($gspgsResultQty1 == null ? 43 : $gspgsResultQty1.hashCode());
        Object $gspgsReachQty2 = this.getGspgsReachQty2();
        result = result * 59 + ($gspgsReachQty2 == null ? 43 : $gspgsReachQty2.hashCode());
        Object $gspgsReachAmt2 = this.getGspgsReachAmt2();
        result = result * 59 + ($gspgsReachAmt2 == null ? 43 : $gspgsReachAmt2.hashCode());
        Object $gspgsResultQty2 = this.getGspgsResultQty2();
        result = result * 59 + ($gspgsResultQty2 == null ? 43 : $gspgsResultQty2.hashCode());
        Object $gspgsReachQty3 = this.getGspgsReachQty3();
        result = result * 59 + ($gspgsReachQty3 == null ? 43 : $gspgsReachQty3.hashCode());
        Object $gspgsReachAmt3 = this.getGspgsReachAmt3();
        result = result * 59 + ($gspgsReachAmt3 == null ? 43 : $gspgsReachAmt3.hashCode());
        Object $gspgsResultQty3 = this.getGspgsResultQty3();
        result = result * 59 + ($gspgsResultQty3 == null ? 43 : $gspgsResultQty3.hashCode());
        return result;
    }

    public String toString() {
        return "PromGiftSetOutData(clientId=" + this.getClientId() + ", gspgsVoucherId=" + this.getGspgsVoucherId() + ", gspgsSerial=" + this.getGspgsSerial() + ", gspgsSeriesProId=" + this.getGspgsSeriesProId() + ", gspgsReachQty1=" + this.getGspgsReachQty1() + ", gspgsReachAmt1=" + this.getGspgsReachAmt1() + ", gspgsResultQty1=" + this.getGspgsResultQty1() + ", gspgsReachQty2=" + this.getGspgsReachQty2() + ", gspgsReachAmt2=" + this.getGspgsReachAmt2() + ", gspgsResultQty2=" + this.getGspgsResultQty2() + ", gspgsReachQty3=" + this.getGspgsReachQty3() + ", gspgsReachAmt3=" + this.getGspgsReachAmt3() + ", gspgsResultQty3=" + this.getGspgsResultQty3() + ")";
    }
}
