package com.gys.business.service.data.mib.mibEnum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：gyx
 * @Date ：Created in 14:09 2021/12/28
 * @Description：人员类别枚举
 * @Modified By：gyx
 * @Version:
 */
@Getter
@AllArgsConstructor
public enum PersonTypeEnum {

    ZZ("11","在职"),
    TXRY("12","退休人员"),
    LX("13","离休"),
    JMWCN("14","居民(未成年)"),
    JMCN("15","居民(成年)"),
    JMLN("16","居民(老年)");

    private String code;
    private String value;

    public static PersonTypeEnum getEnumByCode(String code){
        for (PersonTypeEnum personTypeEnum : values()) {
            if (code.equals(personTypeEnum.getCode())) {
                return personTypeEnum;
            }
        }
        return null;
    }
}
