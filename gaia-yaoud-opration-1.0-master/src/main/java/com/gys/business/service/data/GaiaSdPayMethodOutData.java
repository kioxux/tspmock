package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回参数
 *
 * @author xiaoyuan on 2020/9/7
 */
@Data
public class GaiaSdPayMethodOutData implements Serializable {
    private static final long serialVersionUID = -8411091159131988079L;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 门店号
     */
    @ApiModelProperty(value = "店号")
    private String gspmBrId;

    /**
     * 编号
     */
    @ApiModelProperty(value = "支付编码")
    private String gspmId;

    /**
     * 名称
     */
    @ApiModelProperty(value = "支付名称")
    private String gspmName;

}
