package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2021/3/9
 */
@Data
public class GaiaStoreOutData implements Serializable {
    private static final long serialVersionUID = -3815716563352662052L;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private String userId;

    /**
     * 门店编码
     */
    @ApiModelProperty(value = "门店编码")
    private String stoCode;

    /**
     * 门店名称
     */
    @ApiModelProperty(value = "门店名称")
    private String stoName;


    @ApiModelProperty(value = "门店处方是否必填 0:必填 / 1 : 是非必填")
    private String flag;

    /**
     * 门店简称
     */
    private String stoShortName;
}
