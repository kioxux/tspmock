package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-gys-business-service-data-GaiaChannelDefault")
@Data
public class GaiaChannelDefault {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 渠道编码
    */
    @ApiModelProperty(value="渠道编码")
    private String gcdChannel;

    /**
    * 渠道名称
    */
    @ApiModelProperty(value="渠道名称")
    private String gcdChannelName;
}