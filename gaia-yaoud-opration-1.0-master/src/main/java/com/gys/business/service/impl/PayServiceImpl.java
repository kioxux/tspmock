package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.*;
import com.gys.business.service.data.*;
import com.gys.business.service.data.his.AddOrderForm;
import com.gys.business.service.data.his.FxBillInfoForm;
import com.gys.business.service.data.his.OrderDetailForm;
import com.gys.business.service.data.integralExchange.*;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.MQReceiveData;
import com.gys.common.enums.MaterialTypeEnum;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import com.gys.common.http.HttpJson;
import com.gys.common.http.HttpRequestUtil;
import com.gys.common.pay.BsaoCPay;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import com.gys.feign.PurchaseService;
import com.gys.util.CommonUtil;
import com.gys.util.SnowflakeIdUtil;
import com.gys.util.UtilMessage;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@SuppressWarnings("all")
public class PayServiceImpl implements PayService {
    @Resource
    private GaiaSdPaymentMethodMapper paymentMethodMapper;

    @Resource
    private GaiaSdIntegralCashSetMapper integralCashSetMapper;

    @Resource
    private GaiaSdSaleHMapper saleHMapper;

    @Resource
    private GaiaSdSaleDMapper saleDMapper;

    @Resource
    private GaiaSdMemberBasicMapper memberBasicMapper;

    @Resource
    private GaiaTaxCodeMapper taxCodeMapper;

    @Resource
    private GaiaSdPromCouponBasicMapper promCouponBasicMapper;

    @Resource
    private GaiaSdMemberCardDataMapper gaiaSdMemberCardDataMapper;

    @Resource
    private GaiaSdRechargeCardMapper rechargeCardMapper;

    @Resource
    private GaiaSdRechargeCardSetMapper rechargeCardSetMapper;

    @Resource
    private GaiaSdSalePayMsgMapper payMsgMapper;

    @Resource
    private GaiaMaterialAssessMapper materialAssessMapper;

    @Resource
    private GaiaSdSrSaleRecordMapper gaiaSdSrSaleRecordMapper;

    @Resource
    private GaiaSdSrRecordMapper gaiaSdSrRecordMapper;

    @Resource
    private GaiaSdMemberCardDataMapper memberCardDataMapper;

    @Resource
    private GaiaSdElectronChangeMapper electronChangeMapper;

    @Resource
    private GaiaSdStockMapper stockMapper;

    @Resource
    private GaiaSdStockBatchMapper stockBatchMapper;

    @Resource
    private StockService stockService;

    @Resource
    private PurchaseService purchaseService;

    @Resource
    private SalesReceiptsService salesReceiptsService;

    @Resource
    private ElectronicCouponsServiceImpl electronicCouponsService;

    @Resource
    private GaiaProductRelateMapper productRelateMapper;

    @Resource
    private GaiaSdBatchChangeMapper batchChangeMapper;

    @Resource
    private GaiaStoreDataMapper storeDataMapper;

    @Autowired
    private GaiaSdSaleRecipelMapper gaiaSdSaleRecipelMapper;

    @Autowired
    private CacheService synchronizedService;

    @Autowired
    private RabbitTemplateHelper rabbitTemplate;

    @Autowired
    private GaiaTAccountMapper gaiaTAccountMapper;

    @Autowired
    private BaseService baseService;

    @Autowired
    private GaiaSdSaleHandHMapper gaiaSdSaleHandHMapper;

    @Autowired
    private GaiaSdSaleHandDMapper gaiaSdSaleHandDMapper;
    @Autowired
    private TakeAwayService takeAwayService;

    @Autowired
    private GaiaSdMessageMapper gaiaSdMessageMapper;

    @Autowired
    private GaiaPurchaseRemindMapper purchaseRemindMapper;

    @Autowired
    private GaiaProductBusinessMapper productBusinessMapper;

    @Autowired
    private GaiaSdSytAccountMapper sytAccountMapper;

    @Autowired
    private RechargeChangeMapper rechargeChangeMapper;

    @Autowired
    private GaiaSdSytOperationMapper sytOperationMapper;

    @Autowired
    private GaiaSdRechargeCardPaycheckMapper rechargeCardPaycheckMapper;

    @Resource
    private MaterialDocService materialDocService;

    @Autowired
    private RelatedSaleEjectMapper relatedSaleEjectMapper;

    @Autowired
    private GaiaSdIntegralChangeMapper integralChangeMapper;

    @Autowired
    private PayService payService;

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;


    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    @Value("${chinaums.payUrl}")
    private String CHINAUMS_PAY_URL;

    @Value("${his.URL}")
    private String HIS_URL;

    private String payCallbackRoute = "/his/fx/pay/callback";
    private String payIsChangeRoute = "/his/fx/pay/isChange";
    private String payRefundRoute = "/his/fx/pay/refund";

    @Override
    public List<GetPayTypeOutData> payTypeList(GetPayInData inData) {
        List<GetPayTypeOutData> res = this.paymentMethodMapper.payTypeList(inData);
        //检查积分抵现 计算最大可抵现的积分金额
        if (StringUtils.isNotEmpty(inData.getCardNo())) {
            //如果有积分抵现的支付方式
            res.stream().filter(item -> "9001".equals(item.getGspmId())).findFirst().ifPresent(item -> {
                //查询当前会员卡的积分
                GaiaSdMemberCardData memberCardData = this.memberBasicMapper.findMemberNameByCardId2(inData.getCardNo(), inData.getClientId(), inData.getStoreCode());
                //计算剩余可用积分
                if (ObjectUtil.isNotEmpty(memberCardData)) {
                    Example example = new Example(GaiaSdIntegralCashSet.class);
                    example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsicsBrId", inData.getStoreCode());
                    List<GaiaSdIntegralCashSet> integralCashSetList = this.integralCashSetMapper.selectByExample(example);
                    //判断卡类型 如果卡类型一致 则用对应设置 不一致 则用全部的
                    GaiaSdIntegralCashSet integralCashSet = integralCashSetList.stream()
                            .filter(setItem -> memberCardData.getGsmbcClassId().equals(setItem.getGsicsMemberClass()))
                            .findFirst()
                            .orElse(integralCashSetList.stream()
                                    .filter(setItem -> setItem.getGsicsMemberClass().equals("0"))
                                    .findFirst()
                                    .orElse(null)
                            );
                    if (integralCashSet != null) {
                        BigDecimal amt = new BigDecimal(memberCardData.getGsmbcIntegral()).divide(new BigDecimal(integralCashSet.getGsicsIntegralDeduct()), 4, RoundingMode.DOWN).multiply(integralCashSet.getGsicsAmtOffset());
                        if (ObjectUtil.isNotEmpty(integralCashSet.getGsicsAmtOffsetMax())) {//每单最大抵现金额
                            if (amt.compareTo(integralCashSet.getGsicsAmtOffsetMax()) > 0) {
                                amt = integralCashSet.getGsicsAmtOffsetMax();
                            }
                        }
                        if ("1".equals(integralCashSet.getGsicsChangeSet())) {//允许小数
                            item.setGspmRemark1("(" + amt.setScale(2, BigDecimal.ROUND_DOWN) + "元)  当前积分可抵金额");
                        } else {
                            item.setGspmRemark1("(" + amt.setScale(2, BigDecimal.ROUND_DOWN) + "元)  当前积分可抵金额");
                        }
                    }
                }
            });
        }

        return res;
    }

    @Override
    public List<GetPayTypeOutData> payTypeListByClient(GetPayInData inData) {
        return this.paymentMethodMapper.payTypeListByClient(inData);
    }


    @Override
    public GetPayOutData payInfo(GetPayInData inData) {
        GetPayOutData outData = new GetPayOutData();
        Example example = new Example(GaiaSdSaleH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBrId", inData.getStoreCode()).andEqualTo("gsshBillNo", inData.getBillNo());
        GaiaSdSaleH saleH = this.saleHMapper.selectOneByExample(example);
        if (saleH == null) {
            return outData;
        }
        outData.setBillNo(saleH.getGsshBillNo());
        outData.setYsAmount(saleH.getGsshYsAmt().setScale(2, 4));
        BigDecimal gsshIntegralCashAmt = saleH.getGsshIntegralCashAmt() == null ? BigDecimal.ZERO : saleH.getGsshIntegralCashAmt();
        BigDecimal gsshDyqAmt = saleH.getGsshDyqAmt() == null ? BigDecimal.ZERO : saleH.getGsshDyqAmt();
        BigDecimal gsshDzqdyAmt1 = saleH.getGsshDzqdyAmt1() == null ? BigDecimal.ZERO : saleH.getGsshDzqdyAmt1();
        BigDecimal gsshDzqdyAmt2 = saleH.getGsshDzqdyAmt2() == null ? BigDecimal.ZERO : saleH.getGsshDzqdyAmt2();
        BigDecimal gsshDzqdyAmt3 = saleH.getGsshDzqdyAmt3() == null ? BigDecimal.ZERO : saleH.getGsshDzqdyAmt3();
        BigDecimal gsshDzqdyAmt4 = saleH.getGsshDzqdyAmt4() == null ? BigDecimal.ZERO : saleH.getGsshDzqdyAmt4();
        BigDecimal gsshDzqdyAmt5 = saleH.getGsshDzqdyAmt5() == null ? BigDecimal.ZERO : saleH.getGsshDzqdyAmt5();
        outData.setDkAmount(gsshIntegralCashAmt.add(gsshDyqAmt).add(gsshDzqdyAmt1).add(gsshDzqdyAmt2).add(gsshDzqdyAmt3).add(gsshDzqdyAmt4).add(gsshDzqdyAmt5).setScale(2, 4));
        BigDecimal gsshRmbAmt = saleH.getGsshRmbAmt() == null ? BigDecimal.ZERO : saleH.getGsshRmbAmt();
        BigDecimal gsshRechargeCardAmt = saleH.getGsshRechargeCardAmt() == null ? BigDecimal.ZERO : saleH.getGsshRechargeCardAmt();
        BigDecimal gsshPaymentAmt1 = saleH.getGsshPaymentAmt1() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt1();
        BigDecimal gsshPaymentAmt2 = saleH.getGsshPaymentAmt2() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt2();
        BigDecimal gsshPaymentAmt3 = saleH.getGsshPaymentAmt3() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt3();
        BigDecimal gsshPaymentAmt4 = saleH.getGsshPaymentAmt4() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt4();
        BigDecimal gsshPaymentAmt5 = saleH.getGsshPaymentAmt5() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt5();
        outData.setSsAmount(gsshRmbAmt.add(gsshRechargeCardAmt).add(gsshPaymentAmt1).add(gsshPaymentAmt2).add(gsshPaymentAmt3).add(gsshPaymentAmt4).add(gsshPaymentAmt5).setScale(2, 4));
        outData.setDsAmount(outData.getYsAmount().subtract(outData.getSsAmount()).setScale(2, 4));
        outData.setZlAmount((saleH.getGsshRmbZlAmt() == null ? BigDecimal.ZERO : saleH.getGsshRmbZlAmt()).setScale(2, 4));
        List<GetPayTypeOutData> payTypeList = paymentMethodMapper.payTypeList(inData);

        for (GetPayTypeOutData payTypeOutData : payTypeList) {
            payTypeOutData.setPayAmount(BigDecimal.ZERO.setScale(2, 4));
            if ("1".equals(payTypeOutData.getGspmType())) {
                if (payTypeOutData.getGspmId().equals(saleH.getGsshPaymentNo1())) {
                    payTypeOutData.setPayAmount((saleH.getGsshPaymentAmt1() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt1()).setScale(2, 4));
                }

                if (payTypeOutData.getGspmId().equals(saleH.getGsshPaymentNo2())) {
                    payTypeOutData.setPayAmount((saleH.getGsshPaymentAmt2() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt2()).setScale(2, 4));
                }

                if (payTypeOutData.getGspmId().equals(saleH.getGsshPaymentNo3())) {
                    payTypeOutData.setPayAmount((saleH.getGsshPaymentAmt3() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt3()).setScale(2, 4));
                }

                if (payTypeOutData.getGspmId().equals(saleH.getGsshPaymentNo4())) {
                    payTypeOutData.setPayAmount((saleH.getGsshPaymentAmt4() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt4()).setScale(2, 4));
                }

                if (payTypeOutData.getGspmId().equals(saleH.getGsshPaymentNo5())) {
                    payTypeOutData.setPayAmount((saleH.getGsshPaymentAmt5() == null ? BigDecimal.ZERO : saleH.getGsshPaymentAmt5()).setScale(2, 4));
                }

                if ("1000".equals(payTypeOutData.getGspmId())) {
                    payTypeOutData.setPayAmount((saleH.getGsshRmbAmt() == null ? BigDecimal.ZERO : saleH.getGsshRmbAmt()).setScale(2, 4));
                }
            } else {
                if ("9001".equals(payTypeOutData.getGspmId())) {
                    payTypeOutData.setPayAmount((saleH.getGsshIntegralCashAmt() == null ? BigDecimal.ZERO : saleH.getGsshIntegralCashAmt()).setScale(2, 4));
                }

                if ("9002".equals(payTypeOutData.getGspmId())) {
                    payTypeOutData.setPayAmount(gsshDzqdyAmt1.add(gsshDzqdyAmt1).add(gsshDzqdyAmt1).add(gsshDzqdyAmt1).add(gsshDzqdyAmt1).setScale(2, 4));
                }

                if ("9003".equals(payTypeOutData.getGspmId())) {
                    payTypeOutData.setPayAmount((saleH.getGsshDyqAmt() == null ? BigDecimal.ZERO : saleH.getGsshDyqAmt()).setScale(2, 4));
                }

                if ("8000".equals(payTypeOutData.getGspmId())) {
                    payTypeOutData.setPayAmount((saleH.getGsshRechargeCardAmt() == null ? BigDecimal.ZERO : saleH.getGsshRechargeCardAmt()).setScale(2, 4));
                }
            }
        }

        outData.setPayTypeList(payTypeList);
        return outData;
    }

    @Override
    public ExchangeCashOutData exchangeCash(GetPayInData inData) {
        List<ExchangeCashOutData> outData = this.integralCashSetMapper.exchangeCash(inData);
        if (ObjectUtil.isEmpty(outData)) {
            throw new BusinessException("未设置积分抵现规则！");
        } else {
            return (ExchangeCashOutData) outData.get(0);
        }
    }

    @Override
    public void pay(GetPayInData inData, GetLoginOutData userInfo) {
        BigDecimal amountDecimal = inData.getAmount() == null ? BigDecimal.ZERO : inData.getAmount();
        Long amount = amountDecimal.multiply(new BigDecimal(100)).longValue();
        GaiaSdPaymentMethod record = new GaiaSdPaymentMethod();
        record.setClient(userInfo.getClient());
        record.setGspmBrId(userInfo.getDepId());
        record.setGspmId(inData.getType());
        GaiaSdPaymentMethod paymentMethod = paymentMethodMapper.selectByPrimaryKey(record);
        if (ObjectUtil.isNull(paymentMethod)) {
            throw new BusinessException("门店无此支付方式");
        } else if (StrUtil.isBlank(paymentMethod.getGspmRemark())) {
            throw new BusinessException("请配置支付渠道参数");
        } else {
            JSONObject payJson = JSON.parseObject(paymentMethod.getGspmRemark());
            BsaoCPay pay = BsaoCPay.getInstance(payJson.getString("appId"), payJson.getString("appKey"), payJson.getString("merchantCode"), payJson.getString("terminalCode"));
            HttpJson httpJson = pay.payV2(this.CHINAUMS_PAY_URL, inData.getPayCode(), amount, inData.getBillNo(), userInfo.getDepName());
            if (!httpJson.isSuccess()) {
                throw new BusinessException(httpJson.getMsg());
            } else {
                if (ObjectUtil.isNotEmpty(inData.getBillNo())) {
                    Example example = new Example(GaiaSdSaleH.class);
                    example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBrId", inData.getStoreCode()).andEqualTo("gsshBillNo", inData.getBillNo());
                    GaiaSdSaleH saleH = (GaiaSdSaleH) this.saleHMapper.selectOneByExample(example);
                    if (StrUtil.isBlank(saleH.getGsshPaymentNo1())) {
                        saleH.setGsshPaymentNo1(inData.getType());
                        saleH.setGsshPaymentAmt1(inData.getAmount());
                    } else if (StrUtil.isBlank(saleH.getGsshPaymentNo2())) {
                        saleH.setGsshPaymentNo2(inData.getType());
                        saleH.setGsshPaymentAmt2(inData.getAmount());
                    } else if (StrUtil.isBlank(saleH.getGsshPaymentNo3())) {
                        saleH.setGsshPaymentNo3(inData.getType());
                        saleH.setGsshPaymentAmt3(inData.getAmount());
                    } else if (StrUtil.isBlank(saleH.getGsshPaymentNo4())) {
                        saleH.setGsshPaymentNo4(inData.getType());
                        saleH.setGsshPaymentAmt4(inData.getAmount());
                    } else {
                        saleH.setGsshPaymentNo5(inData.getType());
                        saleH.setGsshPaymentAmt5(inData.getAmount());
                    }

                    this.saleHMapper.updateByPrimaryKeySelective(saleH);
                }

            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Map<String, Object> payCommit(GetPayInData inData) {
        log.info("inData:{}", JSON.toJSONString(inData));
        Map<String, Object> resultMap = Maps.newHashMap();
        StockInData stockInData = new StockInData();
        Example example = new Example(GaiaSdSaleH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBrId", inData.getStoreCode()).andEqualTo("gsshBillNo", inData.getBillNo());
        GaiaSdSaleH saleH = saleHMapper.selectOneByExample(example);
        List<GetPayTypeInData> payTypeList = inData.getPayTypeList();
        Iterator<GetPayTypeInData> payTypeIterator = payTypeList.iterator();
        //是否为中药代煎
        String crFlag = saleH.getGsshCrFlag();

        //遍历支付列表
        while (payTypeIterator.hasNext()) {
            GetPayTypeInData payTypeInData = payTypeIterator.next();
            GaiaSdSalePayMsg payMsg = new GaiaSdSalePayMsg();
            if ("1".equals(payTypeInData.getGspmType())) {
                if (ObjectUtil.isEmpty(saleH.getGsshPaymentNo1())) {
                    saleH.setGsshPaymentNo1(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt1(payTypeInData.getPayAmount());
                } else if (ObjectUtil.isEmpty(saleH.getGsshPaymentNo2())) {
                    saleH.setGsshPaymentNo2(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt2(payTypeInData.getPayAmount());
                } else if (ObjectUtil.isEmpty(saleH.getGsshPaymentNo3())) {
                    saleH.setGsshPaymentNo3(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt3(payTypeInData.getPayAmount());
                } else if (ObjectUtil.isEmpty(saleH.getGsshPaymentNo4())) {
                    saleH.setGsshPaymentNo4(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt4(payTypeInData.getPayAmount());
                } else {
                    saleH.setGsshPaymentNo5(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt5(payTypeInData.getPayAmount());
                }

                if ("1000".equals(payTypeInData.getGspmId())) {
                    saleH.setGsshRmbAmt(payTypeInData.getPayAmount());
                    //只有现金支付才记录
                    payMsg.setGsspmRmbAmt(payTypeInData.getPayAmount());
                }
                if ("5000".equals(payTypeInData.getGspmId())) {
                    //只有储值卡支付才记录
                    payMsg.setGsspmCardNo(inData.getCzkNo());
                }
                if ("4000".equals(payTypeInData.getGspmId())) {
                    //只有医保卡支付才记录
                    payMsg.setGsspmCardNo(inData.getYbCardNo());
                }
            }
            if ("9003".equals(payTypeInData.getGspmId())) {
                inData.setDyqAmount(payTypeInData.getPayAmount());
            }

            if (payTypeInData.getPayAmount().compareTo(BigDecimal.ZERO) > 0 && "1".equals(payTypeInData.getGspmType())) { //只记录 type ==1 的
                payMsg.setClient(inData.getClientId());
                payMsg.setGsspmBrId(inData.getStoreCode());
                payMsg.setGsspmDate(DateUtil.format(new Date(), "yyyyMMdd"));
                payMsg.setGsspmBillNo(inData.getBillNo());
                payMsg.setGsspmId(payTypeInData.getGspmId());
                /*
                 * 设置支付类型 1.销售支付 2.储值卡充值 3.挂号支付
                 * 目前暂时都为1
                 */
                payMsg.setGsspmType("1");
                payMsg.setGsspmName(payTypeInData.getGspmName());
                payMsg.setGsspmAmt(payTypeInData.getPayAmount());
                //非现金支付无需记录
                if (payMsg.getGsspmRmbAmt() != null) {
                    payMsg.setGsspmZlAmt(inData.getZlAmount());
                    payMsg.setGsspmAmt(saleH.getGsshYsAmt().subtract(payTypeList.stream().filter(item -> !"1000".equals(item.getGspmId()) && item.getPayAmount().compareTo(BigDecimal.ZERO) > 0).map(GetPayTypeInData::getPayAmount).reduce(BigDecimal.ZERO, BigDecimal::add)));
                }

                payMsgMapper.insert(payMsg);
            }
        }

        // 判断是否有积分抵现
        if (NumberUtil.parseInt(inData.getJfdxPoint()) != 0) {
            saleH.setGsshIntegralCash(inData.getJfdxPoint());
        }

        //判断积分抵现金额是否大于0
        if (CommonUtil.stripTrailingZeros(inData.getJfdxAmount()).compareTo(BigDecimal.ZERO) > 0) {
            saleH.setGsshIntegralCashAmt(inData.getJfdxAmount());
        }

        BigDecimal dzqAmount = BigDecimal.ZERO;
        Iterator dzqIterator = inData.getDzqList().iterator();

        Example exampleRecharge;
        //遍历打折券列表 --暂无使用
        while (dzqIterator.hasNext()) {
            GetDzqInData dzqInData = (GetDzqInData) dzqIterator.next();
            if (ObjectUtil.isEmpty(dzqInData.getProCode())) {
                dzqAmount = dzqAmount.add((new BigDecimal(dzqInData.getGspcbCount())).multiply(dzqInData.getGspcbCouponAmt()));
            }

            exampleRecharge = new Example(GaiaSdPromCouponBasic.class);
            exampleRecharge.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gspcbActNo", dzqInData.getGspcbActNo());
            List<GaiaSdPromCouponBasic> list = this.promCouponBasicMapper.selectByExample(exampleRecharge);

            for (int i = 0; i < Integer.parseInt(dzqInData.getGspcbCount()); ++i) {
                GaiaSdPromCouponBasic promCouponBasic = (GaiaSdPromCouponBasic) list.get(i);
                promCouponBasic.setGspcbUseBillNo(inData.getBillNo());
                promCouponBasic.setGspcbUseBrId(inData.getStoreCode());
                promCouponBasic.setGspcbUseDate(DateUtil.format(new Date(), "yyyyMMdd"));
                promCouponBasic.setGspcbStatus("0");
                this.promCouponBasicMapper.updateByPrimaryKeySelective(promCouponBasic);
                if (ObjectUtil.isEmpty(saleH.getGsshDzqdyActno1())) {
                    saleH.setGsshDzqdyActno1(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt1(promCouponBasic.getGspcbCouponAmt());
                } else if (ObjectUtil.isEmpty(saleH.getGsshDzqdyActno2())) {
                    saleH.setGsshDzqdyActno2(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt2(promCouponBasic.getGspcbCouponAmt());
                } else if (ObjectUtil.isEmpty(saleH.getGsshDzqdyActno3())) {
                    saleH.setGsshDzqdyActno3(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt3(promCouponBasic.getGspcbCouponAmt());
                } else if (ObjectUtil.isEmpty(saleH.getGsshDzqdyActno4())) {
                    saleH.setGsshDzqdyActno4(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt4(promCouponBasic.getGspcbCouponAmt());
                } else {
                    saleH.setGsshDzqdyActno5(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt5(promCouponBasic.getGspcbCouponAmt());
                }
            }
        }

        saleH.setGsshDyqType(inData.getDyqReason());
        saleH.setGsshDyqAmt(inData.getDyqAmount());
        saleH.setGsshDyqNo(inData.getDyqNo());
        saleH.setGsshRechargeCardNo(inData.getCzkNo());
        saleH.setGsshRechargeCardAmt(inData.getCzkAmount());
        saleH.setGsshHideFlag("0");
        saleH.setGsshRmbZlAmt(inData.getZlAmount());
        saleH.setGsshZkAmt(inData.getDkAmount());
        Example exampleD = new Example(GaiaSdSaleD.class);
        exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssdBrId", inData.getStoreCode()).andEqualTo("gssdBillNo", inData.getBillNo());
        List<GaiaSdSaleD> saleDList = this.saleDMapper.selectByExample(exampleD);

        Example exampleR = new Example(GaiaSdSrRecord.class);
        exampleR.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssrBillNo", inData.getBillNo()).andEqualTo("gssrBrId", inData.getStoreCode());
        List<GaiaSdSrRecord> srRecordList = this.gaiaSdSrRecordMapper.selectByExample(exampleR);

        List<GaiaSdSrSaleRecord> saleRInsertList = Lists.newArrayList();

        Iterator<GaiaSdSaleD> saleDIterator = saleDList.iterator();

        //判断电子券金额是否大于0 刷新电子券状态
        if (ObjectUtil.isNotEmpty(inData.getDzqAmount()) && inData.getDzqAmount().compareTo(BigDecimal.ZERO) > 0) {
            List<GaiaSdElectronChange> gaiaSdElectronChangeList = new ArrayList<>();
            for (ElectronDetailOutData item : inData.getSelectElectronList()) {
                GaiaSdElectronChange gaiaSdElectronChange = new GaiaSdElectronChange();
                gaiaSdElectronChange.setClient(item.getClient());
                gaiaSdElectronChange.setGsecId(item.getGsecId());
                gaiaSdElectronChange.setGsecMemberId(item.getGsecMemberId());

                GaiaSdElectronChange gaiaSdElectronChangeRecord = new GaiaSdElectronChange();
                BeanUtils.copyProperties(gaiaSdElectronChange, gaiaSdElectronChangeRecord);
                gaiaSdElectronChangeRecord.setGsecStatus("Y");
                gaiaSdElectronChangeRecord.setGsecBillNo(saleH.getGsshBillNo());
                gaiaSdElectronChangeRecord.setGsecBrId(saleH.getGsshBrId());
                gaiaSdElectronChangeRecord.setGsecSaleDate(saleH.getGsshDate());
                gaiaSdElectronChangeList.add(gaiaSdElectronChangeRecord);
            }
            //更新电子券异动表
            electronChangeMapper.updateStatus(gaiaSdElectronChangeList);
        }

        //临时变量 用于分摊电子券优惠金额到每个商品
        BigDecimal dzpAmtTemp = inData.getDzqAmount();
        //临时变量 用于分摊积分抵现的金额到订单每条商品
        BigDecimal jfdxTemp = inData.getJfdxAmount();
        //临时变量 用于分摊抵用券的金额到订单每条商品
        BigDecimal dyqAmtTemp = inData.getDyqAmount();

        List<MaterialDocRequestDto> materialList = Lists.newArrayList();
        // 遍历顶单明细 修改分摊金额价格等数据 库存扣账 关联推荐
        int guiNoCount = 1;

        BigDecimal owingStock = BigDecimal.ZERO;
        while (saleDIterator.hasNext()) {
            GaiaSdSaleD saleD = saleDIterator.next();
            String currentDate = CommonUtil.getyyyyMMdd();

            //计算每一行的优惠金额分摊比例
            BigDecimal rate = saleH.getGsshYsAmt().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : saleD.getGssdAmt().divide(saleH.getGsshYsAmt(), 8, 4);

            if (ObjectUtil.isNotEmpty(jfdxTemp) && jfdxTemp.compareTo(BigDecimal.ZERO) > 0) {
                if (saleH.getGsshYsAmt().compareTo(jfdxTemp) == 0) { // 全额抵扣时不用计算 实收单价全部为0
                    //更新明细行内积分抵现折扣金额 = 实收金额
                    saleD.setGssdZkJfdx(saleD.getGssdAmt());
                    //价格为0
                    saleD.setGssdAmt(BigDecimal.ZERO);
                } else {
                    //计算每一行的 “积分抵现” 分摊金额
                    BigDecimal zkJfdx = inData.getJfdxAmount().multiply(rate).setScale(2, 4);
                    jfdxTemp = jfdxTemp.subtract(zkJfdx);
                    //如果是最后一条记录 把多余的折扣值分摊到这一行
                    if (!saleDIterator.hasNext()) {
                        zkJfdx = zkJfdx.add(jfdxTemp);
                    }
                    //更新明细行内积分抵现折扣金额
                    saleD.setGssdZkJfdx(zkJfdx);
                    //将汇总应收金额减去积分抵现的折扣
                    saleD.setGssdAmt(saleD.getGssdAmt().subtract(zkJfdx));
                }

            }
            if (ObjectUtil.isNotEmpty(dzpAmtTemp) && dzpAmtTemp.compareTo(BigDecimal.ZERO) > 0) {
                if (saleH.getGsshYsAmt().compareTo(dzpAmtTemp) == 0) { // 全额抵扣时不用计算 实收单价全部为0
                    //更新明细行内电子券的折扣金额 = 实收金额
                    saleD.setGssdZkDzq(saleD.getGssdAmt());
                    //价格为0
                    saleD.setGssdAmt(BigDecimal.ZERO);
                } else {
                    //计算每一行的 ”电子券“ 分摊金额
                    BigDecimal dzqZk = inData.getDzqAmount().multiply(rate).setScale(2, 4);
                    dzpAmtTemp = dzpAmtTemp.subtract(dzqZk);
                    //如果是最后一条记录 把多余的折扣值分摊到这一行
                    if (!saleDIterator.hasNext()) {
                        dzqZk = dzqZk.add(dzpAmtTemp);
                    }
                    //将汇总应收金额减去电子券的折扣
                    saleD.setGssdAmt(saleD.getGssdAmt().subtract(dzqZk));
                    //更新明细行内电子券折扣金额
                    saleD.setGssdZkDzq(dzqZk);
                }
            }
            if (ObjectUtil.isNotEmpty(dyqAmtTemp) && dyqAmtTemp.compareTo(BigDecimal.ZERO) > 0) {
                if (saleH.getGsshYsAmt().compareTo(dyqAmtTemp) == 0) { // 全额抵扣时不用计算 实收单价全部为0
                    //更新明细行内电子券的折扣金额 = 实收金额
                    saleD.setGssdZkDyq(saleD.getGssdAmt());
                    //价格为0
                    saleD.setGssdAmt(BigDecimal.ZERO);
                } else {
                    //计算每一行的 ”抵用券“ 分摊金额
                    BigDecimal dyqZk = inData.getDyqAmount().multiply(rate).setScale(2, 4);
                    dyqAmtTemp = dyqAmtTemp.subtract(dyqZk);
                    //如果是最后一条记录 把多余的折扣值分摊到这一行
                    if (!saleDIterator.hasNext()) {
                        dyqZk = dyqZk.add(dyqAmtTemp);
                    }
                    //更新明细行内积分抵现折扣金额
                    saleD.setGssdZkDyq(dyqZk);
                    //将汇总应收金额减去电子券的折扣
                    saleD.setGssdAmt(saleD.getGssdAmt().subtract(dyqZk));
                }
            }


            for (GetDzqInData dzqInData : inData.getDzqList()) {
                if (ObjectUtil.isNotEmpty(dzqInData.getProCode()) && saleD.getGssdProId().equals(dzqInData.getProCode())) {
                    saleD.setGssdZkDzq(saleD.getGssdZkDzq().add(dzqInData.getGspcbCouponAmt().multiply(new BigDecimal(dzqInData.getGspcbCount()))).setScale(2, 4));
                }
            }

            saleD.setGssdZkAmt(saleD.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty())).subtract(saleD.getGssdAmt()));
            saleD.setGssdPrc2(saleD.getGssdAmt().divide(new BigDecimal(saleD.getGssdQty()), 2, 4));
            //移动平均价
            Example materialAssessExample = new Example(GaiaMaterialAssess.class);
            materialAssessExample.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("matAssessSite", inData.getStoreCode())
                    .andEqualTo("matProCode", saleD.getGssdProId());
            GaiaMaterialAssess materialAssess = materialAssessMapper.selectOneByExample(materialAssessExample);
            if (materialAssess != null) {
                saleD.setGssdMovPrice(materialAssess.getMatMovPrice());
            }

            saleDMapper.updateByPrimaryKeySelective(saleD);
            Example exampleStock = new Example(GaiaSdStock.class);
            exampleStock.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", inData.getStoreCode()).andEqualTo("gssProId", saleD.getGssdProId());
//            GaiaSdStock stockFrom = this.stockMapper.selectOneByExample(exampleStock);
//            if (ObjectUtil.isNotEmpty(stockFrom)) {
//                stockFrom.setGssQty((new BigDecimal(stockFrom.getGssQty())).subtract(new BigDecimal(saleD.getGssdQty())).toString());
//                this.stockMapper.updateByPrimaryKeySelective(stockFrom);
//            }

//            Example exampleBatch = new Example(GaiaSdStockBatch.class);
//            exampleBatch.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssbBrId", inData.getStoreCode()).andEqualTo("gssbProId", saleD.getGssdProId()).andEqualTo("gssbBatchNo", saleD.getGssdBatchNo()).andEqualTo("gssbBatch", saleD.getGssdBatch());
//            GaiaSdStockBatch stockBatchFrom = (GaiaSdStockBatch)this.stockBatchMapper.selectOneByExample(exampleBatch);
//            if (ObjectUtil.isNotEmpty(stockBatchFrom)) {
//                stockBatchFrom.setGssbQty((new BigDecimal(stockBatchFrom.getGssbQty())).subtract(new BigDecimal(saleD.getGssdQty())).toString());
//                stockBatchMapper.updateByPrimaryKeySelective(stockBatchFrom);
//            }

            //更新库存
            stockInData.setClientId(saleH.getClientId());
            stockInData.setGssBrId(saleH.getGsshBrId());
            stockInData.setGssProId(saleD.getGssdProId());
            stockInData.setBatchNo(saleD.getGssdBatchNo());
            stockInData.setNum(new BigDecimal(saleD.getGssdQty()));
            stockInData.setVoucherId(saleH.getGsshBillNo());
            stockInData.setSerial(saleD.getGssdSerial());
            stockInData.setEmpId(inData.getUserId());
            stockInData.setCrFlag(saleH.getGsshCrFlag());
            Object[] stockObj = stockService.consumeShopStock(stockInData, owingStock, saleDIterator.hasNext());
            owingStock = (BigDecimal) stockObj[0];
            List<ConsumeStockResponse> consumeBatchList = (List<ConsumeStockResponse>) stockObj[1];

            //组装物料凭证接口数据
            try {
                for (int i = 0; i < consumeBatchList.size(); i++) {
                    ConsumeStockResponse consumeStockResponse = consumeBatchList.get(i);
                    String consumeBatch = consumeStockResponse.getBatch();
                    BigDecimal stockNum = consumeStockResponse.getNum();
                    MaterialDocRequestDto dto = new MaterialDocRequestDto();
                    dto.setClient(inData.getClientId());
                    dto.setGuid(UUID.randomUUID().toString());
                    dto.setGuidNo(StrUtil.toString(guiNoCount));
                    if (StringUtils.isNotEmpty(crFlag)) {
                        dto.setMatType(MaterialTypeEnum.ZHONG_YAO_DAI_JIAN.getCode());
                        GaiaProductRelate productRelate = this.getCrProductRelate(saleH.getClientId(), saleH.getGsshBrId(), saleD.getGssdProId());
                        dto.setMatPrice(productRelate != null ? new BigDecimal(productRelate.getProPrice()) : BigDecimal.ZERO);
                    } else {
                        dto.setMatType(MaterialTypeEnum.LING_SHOU.getCode());
                        dto.setMatPrice(saleD.getGssdPrc2());
                    }
                    dto.setMatPostDate(currentDate);
                    dto.setMatHeadRemark("");
                    dto.setMatProCode(saleD.getGssdProId());
                    dto.setMatSiteCode(inData.getStoreCode());
                    dto.setMatLocationCode("1000");
                    dto.setMatLocationTo("");
                    dto.setMatBatch(consumeBatch);
                    dto.setMatQty(stockNum.negate());
                    //            dto.setMatAmt(saleH.getGsshYsAmt());
                    dto.setMatUnit("盒");
                    dto.setMatDebitCredit("H");
                    dto.setMatPoId(saleH.getGsshBillNo());
                    dto.setMatPoLineno(saleD.getGssdSerial());
                    dto.setMatDnId(saleH.getGsshBillNo());
                    dto.setMatDnLineno(saleD.getGssdSerial());
                    dto.setMatLineRemark("");
                    dto.setMatCreateBy(inData.getUserId());
                    dto.setMatCreateDate(currentDate);
                    dto.setMatCreateTime(CommonUtil.getHHmmss());
                    materialList.add(dto);
                    guiNoCount++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            if (StrUtil.isNotBlank(currentBatch)) {
//                Example batchStockExample = new Example(GaiaBatchStock.class);
//                batchStockExample.createCriteria().andEqualTo("client", saleH.getClientId()).andEqualTo("batProCode", saleD.getGssdProId())
//                        .andEqualTo("batSiteCode", saleH.getGsshBrId()).andEqualTo("batBatch", currentBatch);
//                GaiaBatchStock batchStock = batchStockMapper.selectOneByExample(batchStockExample);
//                batchStock.setBatNormalQty(batchStock.getBatNormalQty().subtract(new BigDecimal(saleD.getGssdQty())));
//                batchStock.setBatChangeDate(CommonUtil.getyyyyMMdd());
//                batchStock.setBatChangeTime(CommonUtil.getHHmmss());
//                batchStock.setBatChangeUser(inData.getUserId());
//                batchStockMapper.updateByExampleSelective(batchStock, batchStockExample);
//            }

            //关联推荐插入GaiaSdSrSaleRecord
            srRecordList.forEach(item -> {
                if (item.getGssrProId().equals(saleD.getGssdProId()) && "1".equals(saleD.getGssdRelationFlag())) {
                    GaiaSdSrSaleRecord gaiaSdSrSaleRecord = new GaiaSdSrSaleRecord();
                    gaiaSdSrSaleRecord.setClientId(saleD.getClientId());
                    gaiaSdSrSaleRecord.setGsssBillNo(saleD.getGssdBillNo());
                    gaiaSdSrSaleRecord.setGsssBrId(saleD.getGssdBrId());
                    gaiaSdSrSaleRecord.setGsssCompclass(item.getGssrCompclass());
                    gaiaSdSrSaleRecord.setGsssDate(saleD.getGssdDate());
                    gaiaSdSrSaleRecord.setGsssEmp(saleD.getGssdSalerId());
                    gaiaSdSrSaleRecord.setGsssTime(saleH.getGsshTime());
                    gaiaSdSrSaleRecord.setGsssProId(saleD.getGssdProId());
                    gaiaSdSrSaleRecord.setGsssReleProType(item.getGssrReleProType());
                    gaiaSdSrSaleRecord.setGsssSaleAmt(saleD.getGssdAmt().toString());
                    gaiaSdSrSaleRecord.setGsssSaleQty(saleD.getGssdQty());
                    gaiaSdSrSaleRecord.setGsssSerial(saleD.getGssdSerial());
                    saleRInsertList.add(gaiaSdSrSaleRecord);
                }
            });
        }

        if (saleRInsertList.size() > 0) {
            //去重后插入GaiaSdSrSaleRecord表
            this.gaiaSdSrSaleRecordMapper.saleRecordInsert(saleRInsertList.stream().collect(Collectors.collectingAndThen(
                    Collectors.toCollection(() -> new TreeSet<>(
                            Comparator.comparing(p -> p.getClientId() + ";" + p.getGsssBillNo() + ";" + p.getGsssBrId() + ";" + p.getGsssDate() + ";" + p.getGsssSerial()))), ArrayList::new)));
        }

        saleH.setGsshDzqdyAmt1(inData.getDzqAmount());
        saleH.setGsshDyqAmt(inData.getDyqAmount());
        saleH.setGsshIntegralCash(inData.getJfdxPoint());
        saleH.setGsshIntegralCashAmt(inData.getJfdxAmount());
        saleH.setGsshZkAmt(saleH.getGsshZkAmt().add(inData.getJfdxAmount()).add(inData.getDzqAmount()).add(inData.getDyqAmount()));
        saleH.setGsshYsAmt(saleH.getGsshYsAmt().subtract(inData.getJfdxAmount()).subtract(inData.getDzqAmount()).subtract(inData.getDyqAmount())); //应收金额= 实收金额-（GSPM_TYPE = 2的支付方式）抵用券-电子券-积分抵现
        BigDecimal newPoint = givePoint(saleH, inData.getMemberId());
        saleH.setGsshIntegralAdd(String.valueOf(newPoint.setScale(0, RoundingMode.FLOOR)));
//            saleH.setGsshNormalAmt(saleD.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty())));
        saleHMapper.updateByPrimaryKeySelective(saleH);

        //会员积分增减
//        GaiaSdMemberCardData gaiaSdMemberCardinData = new GaiaSdMemberCardData();
//        gaiaSdMemberCardinData.setClient(saleH.getClientId());
//        gaiaSdMemberCardinData.setGsmbcBrId(saleH.getGsshBrId());
//        gaiaSdMemberCardinData.setGsmbcCardId(saleH.getGsshHykNo());
        GaiaSdMemberCardData gaiaSdMemberCard = null;
        if (ObjectUtil.isNotEmpty(saleH.getGsshHykNo())) {
            gaiaSdMemberCard = this.memberBasicMapper.findMemberNameByCardId2(saleH.getGsshHykNo(), saleH.getClientId(), saleH.getGsshBrId());
            BigDecimal beforPoint = BigDecimal.ZERO;
            if (ObjectUtil.isNotEmpty(gaiaSdMemberCard)) {
                if (!StringUtils.isEmpty(gaiaSdMemberCard.getGsmbcIntegral())) {
                    beforPoint = new BigDecimal(gaiaSdMemberCard.getGsmbcIntegral().trim());
                }
                if (ObjectUtil.isNotEmpty(inData.getIntegralAddSet()) && "1".equals(inData.getIntegralAddSet())) { //判断是否增加积分
                    gaiaSdMemberCard.setGsmbcIntegral(String.valueOf(beforPoint.add(newPoint.setScale(0, RoundingMode.FLOOR)).setScale(0, RoundingMode.FLOOR)));
                }
                BigDecimal jfdxPoint = new BigDecimal(inData.getJfdxPoint());
                if (ObjectUtil.isNotEmpty(inData.getJfdxPoint()) && jfdxPoint.compareTo(BigDecimal.ZERO) > 0) { //判断是否要扣积分
                    gaiaSdMemberCard.setGsmbcIntegral(String.valueOf(new BigDecimal(gaiaSdMemberCard.getGsmbcIntegral()).subtract(jfdxPoint.setScale(0, RoundingMode.FLOOR)).setScale(0, RoundingMode.FLOOR)));
                }
                if (ObjectUtil.isNotEmpty(saleH.getGsshIntegralExchange()) && new BigDecimal(saleH.getGsshIntegralExchange()).compareTo(BigDecimal.ZERO) > 0) { //判断是否要扣积分
                    gaiaSdMemberCard.setGsmbcIntegral(String.valueOf(new BigDecimal(gaiaSdMemberCard.getGsmbcIntegral()).subtract(new BigDecimal(saleH.getGsshIntegralExchange()).setScale(0, RoundingMode.FLOOR)).setScale(0, RoundingMode.FLOOR)));
                }
                gaiaSdMemberCard.setGsmbcIntegralLastdate(DateUtil.format(DateUtil.date(), "yyyyMMdd"));
                gaiaSdMemberCardDataMapper.updateByPrimaryKeySelective(gaiaSdMemberCard);

                Map<String, Object> map = new HashMap<>(16);
                map.put("CLIENT", inData.getClientId());
                map.put("GSMBC_MEMBER_ID", gaiaSdMemberCard.getGsmbcMemberId());
                map.put("GSMBC_CARD_ID", gaiaSdMemberCard.getGsmbcCardId());
                map.put("GSMBC_BR_ID", inData.getStoreCode());
                GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
                aSynchronized.setClient(inData.getClientId());
                aSynchronized.setSite(inData.getStoreCode());
                aSynchronized.setVersion(IdUtil.randomUUID());
                aSynchronized.setTableName("GAIA_SD_MEMBER_CARD");
                aSynchronized.setCommand("update");
                aSynchronized.setSourceNo("会员-销售修改");
                aSynchronized.setArg(JSON.toJSONString(map));
                this.synchronizedService.addOne(aSynchronized);
                MQReceiveData mqReceiveData = new MQReceiveData();
                mqReceiveData.setType("DataSync");
                mqReceiveData.setCmd("payCommit");
                mqReceiveData.setData(inData.getBillNo());
                this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClientId() + "." + inData.getStoreCode(), JSON.toJSON(mqReceiveData));

            }
//            exampleRecharge = new Example(GaiaSdMemberBasic.class);
//            exampleRecharge.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsmbCardId", inData.getCardNo());
//            GaiaSdMemberBasic memberBasic = (GaiaSdMemberBasic)this.memberBasicMapper.selectOneByExample(exampleRecharge);
//            memberBasic.setGsmbIntegral((new BigDecimal(memberBasic.getGsmbIntegral())).subtract(new BigDecimal(inData.getJfdxPoint())).toString());
//            this.memberBasicMapper.updateByPrimaryKeySelective(memberBasic);
        }

        if (ObjectUtil.isNotEmpty(inData.getCzkNo())) {
            exampleRecharge = new Example(GaiaSdRechargeCard.class);
            exampleRecharge.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrcId", inData.getCzkNo()).andEqualTo("gsrcStatus", "1");
            GaiaSdRechargeCard rechargeCard = (GaiaSdRechargeCard) this.rechargeCardMapper.selectOneByExample(exampleRecharge);
            rechargeCard.setGsrcAmt(rechargeCard.getGsrcAmt().subtract(inData.getCzkAmount()));
            this.rechargeCardMapper.updateByPrimaryKeySelective(rechargeCard);
        }

        //送电子券
//        if(ObjectUtil.isNotEmpty(gaiaSdMemberCard)){
//            //1.首先查询所有送券条件
//            GaiaSdStoreData gaiaSdStoreData = new GaiaSdStoreData();
//            gaiaSdStoreData.setClientId(saleH.getClientId());
//            gaiaSdStoreData.setGsstBrId(saleH.getGsshBrId());
//            List<ElectronDetailOutData> electronDetailOutDataList = electronicCouponsService.selectGiftCoupons(gaiaSdStoreData);
//            List<GaiaSdElectronChange> electronChangeList=new ArrayList<>();
//            //2.遍历所有送券条件 筛选出符合条件的
//            for(ElectronDetailOutData item :electronDetailOutDataList){
//                int qty = 0;
//                if("Y".equals(item.getGsecsFlag2())){//全场商品
//                    //遍历商品列表 筛选
//                    BigDecimal isSelectQty = BigDecimal.ZERO; //已选择的数量
//                    BigDecimal isSelectAmt = BigDecimal.ZERO; //已选择的金额
//                    //遍历选择的电子券 累加
//                    for (GaiaSdSaleD saleDItem : saleDList) {
//                        isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
//                        isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
//                    }
//                    //判断是否重复
//                    if("Y".equals(item.getGsecsFlag1())){//循环赠送
//                        if(!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2()))>-1){
//                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty2()));
//                            qty=temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
//                        }else if(ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2())>-1){
//                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt2());
//                            qty=temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
//                        }else if(!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1()))>-1){
//                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty1()));
//                            qty=temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
//                        }else if(ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1())>-1){
//                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt1());
//                            qty=temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
//                        }
//                    }else{//单次赠送
//                        if(!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2()))>-1){
//                            qty=new BigDecimal(item.getGsecsResultQty2()).intValue();
//                        }else if(ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2())>-1){
//                            qty=new BigDecimal(item.getGsecsResultQty2()).intValue();
//                        }else if(!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1()))>-1){
//                            qty=new BigDecimal(item.getGsecsResultQty1()).intValue();
//                        }else if(ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1())>-1){
//                            qty=new BigDecimal(item.getGsecsResultQty1()).intValue();
//                        }
//                    }
//                }else{ //部分商品
//                    //遍历商品列表 筛选
//                    BigDecimal isSelectQty = BigDecimal.ZERO; //已选择的数量
//                    BigDecimal isSelectAmt = BigDecimal.ZERO; //已选择的金额
//                    //遍历选择的电子券 累加
//                    for (GaiaSdSaleD saleDItem : saleDList) {
//                        for(GaiaSdProductsGroup proItem : item.getProGroup()){
//                            if(proItem.getGspgProId().equals(saleDItem.getGssdProId())){
//                                isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
//                                isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
//                            }
//                        }
//                    }
//                    //判断是否重复
//                    if("Y".equals(item.getGsecsFlag1())){//循环赠送
//                        if(!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2()))>-1){
//                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty2()));
//                            qty=temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
//                        }else if(ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2())>-1){
//                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt2());
//                            qty=temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
//                        }else if(!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1()))>-1){
//                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty1()));
//                            qty=temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
//                        }else if(ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1())>-1){
//                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt1());
//                            qty=temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
//                        }
//                    }else{ //单次赠送
//                        if(!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2()))>-1){
//                            qty=new BigDecimal(item.getGsecsResultQty2()).intValue();
//                        }else if(ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2())>-1){
//                            qty=new BigDecimal(item.getGsecsResultQty2()).intValue();
//                        }else if(!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1()))>-1){
//                            qty=new BigDecimal(item.getGsecsResultQty1()).intValue();
//                        }else if(ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1())>-1){
//                            qty=new BigDecimal(item.getGsecsResultQty1()).intValue();
//                        }
//                    }
//                }
//                //生成优惠券
//                for(int i=0;i<qty;i++){
//                    GaiaSdElectronChange gaiaSdElectronChange =new GaiaSdElectronChange();
//                    gaiaSdElectronChange.setClient(saleH.getClientId());
//                    gaiaSdElectronChange.setGsebName(item.getGsecName());
//                    assert gaiaSdMemberCard != null;
//                    gaiaSdElectronChange.setGsecMemberId(gaiaSdMemberCard.getGsmbcMemberId());
////                gaiaSdElectronChange.setGsecId();
//                    gaiaSdElectronChange.setGsebId(item.getGsebId());
//                    gaiaSdElectronChange.setGsecAmt(item.getGsecAmt());
//                    gaiaSdElectronChange.setGsecFlag("0");
//                    gaiaSdElectronChange.setGsecStatus("N");
//                    gaiaSdElectronChange.setGsecCreateDate(sdf.format(new Date()));
//                    gaiaSdElectronChange.setGsecFailDate(null);
//                    gaiaSdElectronChange.setGsecBrId(null);
//                    gaiaSdElectronChange.setGsecSaleDate(null);
//                    electronChangeList.add(gaiaSdElectronChange);
//                }
//            };
//            //3.插入表
//            if(electronChangeList.size()>0){
//                electronChangeMapper.batchInsert(electronChangeList);
//            }
//        }

        //物料凭证接口请求list
        resultMap.put("materialList", materialList);
        return resultMap;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Map<String, Object> payCommit2(GetPayInData inData) {
        Map<String, Object> resultMap = Maps.newHashMap();

        //表单重复验证
        Example example = new Example(GaiaSdSaleH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBrId", inData.getStoreCode()).andEqualTo("gsshBillNo", inData.getBillNo()).andEqualTo("gsshHideFlag", "0");
        GaiaSdSaleH gaiaSdSaleH = saleHMapper.selectOneByExample(example);
        if (ObjectUtil.isNotEmpty(gaiaSdSaleH)) {
            resultMap.put("N", inData.getBillNo());
            return resultMap;
        }
        resultMap.put("Y", inData.getBillNo());

        log.info("inData:{}", JSON.toJSONString(inData));
        GetCreateSaleOutData createSaleOutData = new GetCreateSaleOutData();
        createSaleOutData.setGaiaSdSaleH(inData.getGaiaSdSaleH());
        createSaleOutData.setGaiaSdSaleDList(inData.getGaiaSdSaleDList());
        createSaleOutData.setSelectDataIndexList(inData.getSelectDataIndexList());
        createSaleOutData.setGiftPromOutDataList(inData.getGiftPromOutDataList());
        //创建订单
        salesReceiptsService.createSale(createSaleOutData);


        StockInData stockInData = new StockInData();
        example = new Example(GaiaSdSaleH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBrId", inData.getStoreCode()).andEqualTo("gsshBillNo", inData.getBillNo());
        GaiaSdSaleH saleH = saleHMapper.selectOneByExample(example);
        List<GetPayTypeInData> payTypeList = inData.getPayTypeList();
        Iterator<GetPayTypeInData> payTypeIterator = payTypeList.iterator();
        //是否为中药代煎
        String crFlag = saleH.getGsshCrFlag();

        //遍历支付列表
        while (payTypeIterator.hasNext()) {
            GetPayTypeInData payTypeInData = payTypeIterator.next();
            GaiaSdSalePayMsg payMsg = new GaiaSdSalePayMsg();
            if ("1".equals(payTypeInData.getGspmType())) {
                if (ObjectUtil.isEmpty(saleH.getGsshPaymentNo1())) {
                    saleH.setGsshPaymentNo1(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt1(payTypeInData.getPayAmount());
                } else if (ObjectUtil.isEmpty(saleH.getGsshPaymentNo2())) {
                    saleH.setGsshPaymentNo2(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt2(payTypeInData.getPayAmount());
                } else if (ObjectUtil.isEmpty(saleH.getGsshPaymentNo3())) {
                    saleH.setGsshPaymentNo3(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt3(payTypeInData.getPayAmount());
                } else if (ObjectUtil.isEmpty(saleH.getGsshPaymentNo4())) {
                    saleH.setGsshPaymentNo4(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt4(payTypeInData.getPayAmount());
                } else {
                    saleH.setGsshPaymentNo5(payTypeInData.getGspmId());
                    saleH.setGsshPaymentAmt5(payTypeInData.getPayAmount());
                }

                if ("1000".equals(payTypeInData.getGspmId())) {
                    saleH.setGsshRmbAmt(payTypeInData.getPayAmount());
                    //只有现金支付才记录
                    payMsg.setGsspmRmbAmt(payTypeInData.getPayAmount());
                }
                if ("5000".equals(payTypeInData.getGspmId())) {
                    //只有储值卡支付才记录
                    payMsg.setGsspmCardNo(inData.getCzkNo());
                }
                if ("4000".equals(payTypeInData.getGspmId())) {
                    //只有医保卡支付才记录
                    payMsg.setGsspmCardNo(inData.getYbCardNo());
                }
            }
            if ("9003".equals(payTypeInData.getGspmId())) {
                inData.setDyqAmount(payTypeInData.getPayAmount());
            }

            if (payTypeInData.getPayAmount().compareTo(BigDecimal.ZERO) > 0 && "1".equals(payTypeInData.getGspmType())) { //只记录 type ==1 的
                payMsg.setClient(inData.getClientId());
                payMsg.setGsspmBrId(inData.getStoreCode());
                payMsg.setGsspmDate(DateUtil.format(new Date(), "yyyyMMdd"));
                payMsg.setGsspmBillNo(inData.getBillNo());
                payMsg.setGsspmId(payTypeInData.getGspmId());
                /*
                 * 设置支付类型 1.销售支付 2.储值卡充值 3.挂号支付
                 * 目前暂时都为1
                 */
                payMsg.setGsspmType("1");
                payMsg.setGsspmName(payTypeInData.getGspmName());
                payMsg.setGsspmAmt(payTypeInData.getPayAmount());
                //非现金支付无需记录
                if (payMsg.getGsspmRmbAmt() != null) {
                    payMsg.setGsspmZlAmt(inData.getZlAmount());
                    payMsg.setGsspmAmt(saleH.getGsshYsAmt().subtract(payTypeList.stream().filter(item -> !"1000".equals(item.getGspmId()) && item.getPayAmount().compareTo(BigDecimal.ZERO) > 0).map(GetPayTypeInData::getPayAmount).reduce(BigDecimal.ZERO, BigDecimal::add)));
                }

                payMsgMapper.insert(payMsg);
            }
        }

        // 判断是否有积分抵现
        if (NumberUtil.parseInt(inData.getJfdxPoint()) != 0) {
            saleH.setGsshIntegralCash(inData.getJfdxPoint());
        }

        //判断积分抵现金额是否大于0
        if (CommonUtil.stripTrailingZeros(inData.getJfdxAmount()).compareTo(BigDecimal.ZERO) > 0) {
            saleH.setGsshIntegralCashAmt(inData.getJfdxAmount());
        }

        BigDecimal dzqAmount = BigDecimal.ZERO;
        Iterator dzqIterator = inData.getDzqList().iterator();

        Example exampleRecharge;
        //遍历打折券列表 --暂无使用
        while (dzqIterator.hasNext()) {
            GetDzqInData dzqInData = (GetDzqInData) dzqIterator.next();
            if (ObjectUtil.isEmpty(dzqInData.getProCode())) {
                dzqAmount = dzqAmount.add((new BigDecimal(dzqInData.getGspcbCount())).multiply(dzqInData.getGspcbCouponAmt()));
            }

            exampleRecharge = new Example(GaiaSdPromCouponBasic.class);
            exampleRecharge.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gspcbActNo", dzqInData.getGspcbActNo());
            List<GaiaSdPromCouponBasic> list = this.promCouponBasicMapper.selectByExample(exampleRecharge);
            //todo 电子券暂时不缓存
            for (int i = 0; i < Integer.parseInt(dzqInData.getGspcbCount()); ++i) {
                GaiaSdPromCouponBasic promCouponBasic = (GaiaSdPromCouponBasic) list.get(i);
                promCouponBasic.setGspcbUseBillNo(inData.getBillNo());
                promCouponBasic.setGspcbUseBrId(inData.getStoreCode());
                promCouponBasic.setGspcbUseDate(DateUtil.format(new Date(), "yyyyMMdd"));
                promCouponBasic.setGspcbStatus("0");
                this.promCouponBasicMapper.updateByPrimaryKeySelective(promCouponBasic);
                if (ObjectUtil.isEmpty(saleH.getGsshDzqdyActno1())) {
                    saleH.setGsshDzqdyActno1(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt1(promCouponBasic.getGspcbCouponAmt());
                } else if (ObjectUtil.isEmpty(saleH.getGsshDzqdyActno2())) {
                    saleH.setGsshDzqdyActno2(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt2(promCouponBasic.getGspcbCouponAmt());
                } else if (ObjectUtil.isEmpty(saleH.getGsshDzqdyActno3())) {
                    saleH.setGsshDzqdyActno3(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt3(promCouponBasic.getGspcbCouponAmt());
                } else if (ObjectUtil.isEmpty(saleH.getGsshDzqdyActno4())) {
                    saleH.setGsshDzqdyActno4(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt4(promCouponBasic.getGspcbCouponAmt());
                } else {
                    saleH.setGsshDzqdyActno5(promCouponBasic.getGspcbCouponId());
                    saleH.setGsshDzqdyAmt5(promCouponBasic.getGspcbCouponAmt());
                }
            }
        }

        saleH.setGsshDyqType(inData.getDyqReason());
        saleH.setGsshDyqAmt(inData.getDyqAmount());
        saleH.setGsshDyqNo(inData.getDyqNo());
        saleH.setGsshRechargeCardNo(inData.getCzkNo());
        saleH.setGsshRechargeCardAmt(inData.getCzkAmount());
        saleH.setGsshHideFlag("0");
        saleH.setGsshRmbZlAmt(inData.getZlAmount());
        saleH.setGsshZkAmt(inData.getDkAmount());
        Example exampleD = new Example(GaiaSdSaleD.class);
        exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssdBrId", inData.getStoreCode()).andEqualTo("gssdBillNo", inData.getBillNo());
        List<GaiaSdSaleD> saleDList = this.saleDMapper.selectByExample(exampleD);

        Example exampleR = new Example(GaiaSdSrRecord.class);
        exampleR.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssrBillNo", inData.getBillNo()).andEqualTo("gssrBrId", inData.getStoreCode());
        List<GaiaSdSrRecord> srRecordList = this.gaiaSdSrRecordMapper.selectByExample(exampleR);

        List<GaiaSdSrSaleRecord> saleRInsertList = Lists.newArrayList();

        Iterator<GaiaSdSaleD> saleDIterator = saleDList.iterator();

        //判断电子券金额是否大于0 刷新电子券状态
        if (ObjectUtil.isNotEmpty(inData.getDzqAmount()) && inData.getDzqAmount().compareTo(BigDecimal.ZERO) > 0) {
            List<GaiaSdElectronChange> gaiaSdElectronChangeList = new ArrayList<>();
            for (ElectronDetailOutData item : inData.getSelectElectronList()) {
                GaiaSdElectronChange gaiaSdElectronChange = new GaiaSdElectronChange();
                gaiaSdElectronChange.setClient(item.getClient());
                gaiaSdElectronChange.setGsecId(item.getGsecId());
                gaiaSdElectronChange.setGsecMemberId(item.getGsecMemberId());

                GaiaSdElectronChange gaiaSdElectronChangeRecord = new GaiaSdElectronChange();
                BeanUtils.copyProperties(gaiaSdElectronChange, gaiaSdElectronChangeRecord);
                gaiaSdElectronChangeRecord.setGsecStatus("Y");
                gaiaSdElectronChangeRecord.setGsecBillNo(saleH.getGsshBillNo());
                gaiaSdElectronChangeRecord.setGsecBrId(saleH.getGsshBrId());
                gaiaSdElectronChangeRecord.setGsecSaleDate(saleH.getGsshDate());
                gaiaSdElectronChangeList.add(gaiaSdElectronChangeRecord);
            }
            //更新电子券异动表
            electronChangeMapper.updateStatus(gaiaSdElectronChangeList);
        }

        //临时变量 用于分摊电子券优惠金额到每个商品
        BigDecimal dzpAmtTemp = inData.getDzqAmount();
        //临时变量 用于分摊积分抵现的金额到订单每条商品
        BigDecimal jfdxTemp = inData.getJfdxAmount();
        //临时变量 用于分摊抵用券的金额到订单每条商品
        BigDecimal dyqAmtTemp = inData.getDyqAmount();

        List<MaterialDocRequestDto> materialList = Lists.newArrayList();
        // 遍历顶单明细 修改分摊金额价格等数据 库存扣账 关联推荐
        int guiNoCount = 1;

        BigDecimal owingStock = BigDecimal.ZERO;
        while (saleDIterator.hasNext()) {
            GaiaSdSaleD saleD = saleDIterator.next();
            String currentDate = CommonUtil.getyyyyMMdd();

            //计算每一行的优惠金额分摊比例
            BigDecimal rate = saleH.getGsshYsAmt().compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : saleD.getGssdAmt().divide(saleH.getGsshYsAmt(), 8, 4);

            if (ObjectUtil.isNotEmpty(jfdxTemp) && jfdxTemp.compareTo(BigDecimal.ZERO) > 0) {
                if (saleH.getGsshYsAmt().compareTo(jfdxTemp) == 0) { // 全额抵扣时不用计算 实收单价全部为0
                    //更新明细行内积分抵现折扣金额 = 实收金额
                    saleD.setGssdZkJfdx(saleD.getGssdAmt());
                    //价格为0
                    saleD.setGssdAmt(BigDecimal.ZERO);
                } else {
                    //计算每一行的 “积分抵现” 分摊金额
                    BigDecimal zkJfdx = inData.getJfdxAmount().multiply(rate).setScale(2, 4);
                    jfdxTemp = jfdxTemp.subtract(zkJfdx);
                    //如果是最后一条记录 把多余的折扣值分摊到这一行
                    if (!saleDIterator.hasNext()) {
                        zkJfdx = zkJfdx.add(jfdxTemp);
                    }
                    //更新明细行内积分抵现折扣金额
                    saleD.setGssdZkJfdx(zkJfdx);
                    //将汇总应收金额减去积分抵现的折扣
                    saleD.setGssdAmt(saleD.getGssdAmt().subtract(zkJfdx));
                }
            }
            if (ObjectUtil.isNotEmpty(dzpAmtTemp) && dzpAmtTemp.compareTo(BigDecimal.ZERO) > 0) {
                if (saleH.getGsshYsAmt().compareTo(dzpAmtTemp) == 0) { // 全额抵扣时不用计算 实收单价全部为0
                    //更新明细行内电子券的折扣金额 = 实收金额
                    saleD.setGssdZkDzq(saleD.getGssdAmt());
                    //价格为0
                    saleD.setGssdAmt(BigDecimal.ZERO);
                } else {
                    //计算每一行的 ”电子券“ 分摊金额
                    BigDecimal dzqZk = inData.getDzqAmount().multiply(rate).setScale(2, 4);
                    dzpAmtTemp = dzpAmtTemp.subtract(dzqZk);
                    //如果是最后一条记录 把多余的折扣值分摊到这一行
                    if (!saleDIterator.hasNext()) {
                        dzqZk = dzqZk.add(dzpAmtTemp);
                    }
                    //将汇总应收金额减去电子券的折扣
                    saleD.setGssdAmt(saleD.getGssdAmt().subtract(dzqZk));
                    //更新明细行内电子券折扣金额
                    saleD.setGssdZkDzq(dzqZk);
                }
            }
            if (ObjectUtil.isNotEmpty(dyqAmtTemp) && dyqAmtTemp.compareTo(BigDecimal.ZERO) > 0) {
                if (saleH.getGsshYsAmt().compareTo(dyqAmtTemp) == 0) { // 全额抵扣时不用计算 实收单价全部为0
                    //更新明细行内电子券的折扣金额 = 实收金额
                    saleD.setGssdZkDyq(saleD.getGssdAmt());
                    //价格为0
                    saleD.setGssdAmt(BigDecimal.ZERO);
                } else {
                    //计算每一行的 ”抵用券“ 分摊金额
                    BigDecimal dyqZk = inData.getDyqAmount().multiply(rate).setScale(2, 4);
                    dyqAmtTemp = dyqAmtTemp.subtract(dyqZk);
                    //如果是最后一条记录 把多余的折扣值分摊到这一行
                    if (!saleDIterator.hasNext()) {
                        dyqZk = dyqZk.add(dyqAmtTemp);
                    }
                    //更新明细行内积分抵现折扣金额
                    saleD.setGssdZkDyq(dyqZk);
                    //将汇总应收金额减去电子券的折扣
                    saleD.setGssdAmt(saleD.getGssdAmt().subtract(dyqZk));
                }
            }

            for (GetDzqInData dzqInData : inData.getDzqList()) {
                if (ObjectUtil.isNotEmpty(dzqInData.getProCode()) && saleD.getGssdProId().equals(dzqInData.getProCode())) {
                    saleD.setGssdZkDzq(saleD.getGssdZkDzq().add(dzqInData.getGspcbCouponAmt().multiply(new BigDecimal(dzqInData.getGspcbCount()))).setScale(2, 4));
                }
            }

            saleD.setGssdZkAmt(saleD.getGssdPrc1().multiply(new BigDecimal(saleD.getGssdQty())).subtract(saleD.getGssdAmt()));
            //  saleD.setGssdPrc2(saleD.getGssdAmt().divide(new BigDecimal(saleD.getGssdQty()), 4, 4));
            //移动平均价
            Example materialAssessExample = new Example(GaiaMaterialAssess.class);
            materialAssessExample.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("matAssessSite", inData.getStoreCode())
                    .andEqualTo("matProCode", saleD.getGssdProId());
            GaiaMaterialAssess materialAssess = materialAssessMapper.selectOneByExample(materialAssessExample);
            if (materialAssess != null) {
                saleD.setGssdMovPrice(materialAssess.getMatMovPrice());
                saleD.setGssdMovPrices(materialAssess.getMatMovPrice().multiply(new BigDecimal(saleD.getGssdQty())));
                saleD.setGssdTaxRate(saleD.getGssdMovPrices().multiply(saleD.getGssdMovTax()));
                saleD.setGssdAddAmt(materialAssess.getMatMovPrice().multiply(new BigDecimal(saleD.getGssdQty())));
                saleD.setGssdAddTax(saleD.getGssdMovPrices().multiply(saleD.getGssdMovTax()));
            }

            //修改D表的销售日期
            saleD.setGssdDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
            saleDMapper.updateByPrimaryKeySelective(saleD);
            Example exampleStock = new Example(GaiaSdStock.class);
            exampleStock.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", inData.getStoreCode()).andEqualTo("gssProId", saleD.getGssdProId());

            //更新库存
            stockInData.setClientId(saleH.getClientId());
            stockInData.setGssBrId(saleH.getGsshBrId());
            stockInData.setGssProId(saleD.getGssdProId());
            stockInData.setBatchNo(saleD.getGssdBatchNo());
            stockInData.setNum(new BigDecimal(saleD.getGssdQty()));
            stockInData.setVoucherId(saleH.getGsshBillNo());
            stockInData.setSerial(saleD.getGssdSerial());
            stockInData.setEmpId(inData.getUserId());
            stockInData.setCrFlag(saleH.getGsshCrFlag());
            Object[] stockObj = stockService.consumeShopStock(stockInData, owingStock, saleDIterator.hasNext());
            owingStock = (BigDecimal) stockObj[0];
            List<ConsumeStockResponse> consumeBatchList = (List<ConsumeStockResponse>) stockObj[1];

            //组装物料凭证接口数据
            try {
                for (int i = 0; i < consumeBatchList.size(); i++) {
                    ConsumeStockResponse consumeStockResponse = consumeBatchList.get(i);
                    String consumeBatch = consumeStockResponse.getBatch();
                    BigDecimal stockNum = consumeStockResponse.getNum();
                    MaterialDocRequestDto dto = new MaterialDocRequestDto();
                    dto.setClient(inData.getClientId());
                    dto.setGuid(UUID.randomUUID().toString());
                    dto.setGuidNo(StrUtil.toString(guiNoCount));
                    if (StringUtils.isNotEmpty(crFlag)) {
                        dto.setMatType(MaterialTypeEnum.ZHONG_YAO_DAI_JIAN.getCode());
                        GaiaProductRelate productRelate = this.getCrProductRelate(saleH.getClientId(), saleH.getGsshBrId(), saleD.getGssdProId());
                        dto.setMatPrice(productRelate != null ? new BigDecimal(productRelate.getProPrice()) : BigDecimal.ZERO);
                    } else {
                        dto.setMatType(MaterialTypeEnum.LING_SHOU.getCode());
                        dto.setMatPrice(saleD.getGssdPrc2());
                    }
                    dto.setMatPostDate(currentDate);
                    dto.setMatHeadRemark("");
                    dto.setMatProCode(saleD.getGssdProId());
                    dto.setMatSiteCode(inData.getStoreCode());
                    dto.setMatLocationCode("1000");
                    dto.setMatLocationTo("");
                    dto.setMatBatch(consumeBatch);
                    dto.setMatQty(stockNum.negate());
                    //            dto.setMatAmt(saleH.getGsshYsAmt());
                    dto.setMatUnit("盒");
                    dto.setMatDebitCredit("H");
                    dto.setMatPoId(saleH.getGsshBillNo());
                    dto.setMatPoLineno(saleD.getGssdSerial());
                    dto.setMatDnId(saleH.getGsshBillNo());
                    dto.setMatDnLineno(saleD.getGssdSerial());
                    dto.setMatLineRemark("");
                    dto.setMatCreateBy(inData.getUserId());
                    dto.setMatCreateDate(currentDate);
                    dto.setMatCreateTime(CommonUtil.getHHmmss());
                    materialList.add(dto);
                    guiNoCount++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            //关联推荐插入GaiaSdSrSaleRecord
            srRecordList.forEach(item -> {
                if (item.getGssrProId().equals(saleD.getGssdProId()) && "1".equals(saleD.getGssdRelationFlag())) {
                    GaiaSdSrSaleRecord gaiaSdSrSaleRecord = new GaiaSdSrSaleRecord();
                    gaiaSdSrSaleRecord.setClientId(saleD.getClientId());
                    gaiaSdSrSaleRecord.setGsssBillNo(saleD.getGssdBillNo());
                    gaiaSdSrSaleRecord.setGsssBrId(saleD.getGssdBrId());
                    gaiaSdSrSaleRecord.setGsssCompclass(item.getGssrCompclass());
                    gaiaSdSrSaleRecord.setGsssDate(saleD.getGssdDate());
                    gaiaSdSrSaleRecord.setGsssEmp(saleD.getGssdSalerId());
                    gaiaSdSrSaleRecord.setGsssTime(saleH.getGsshTime());
                    gaiaSdSrSaleRecord.setGsssProId(saleD.getGssdProId());
                    gaiaSdSrSaleRecord.setGsssReleProType(item.getGssrReleProType());
                    gaiaSdSrSaleRecord.setGsssSaleAmt(saleD.getGssdAmt().toString());
                    gaiaSdSrSaleRecord.setGsssSaleQty(saleD.getGssdQty());
                    gaiaSdSrSaleRecord.setGsssSerial(saleD.getGssdSerial());
                    saleRInsertList.add(gaiaSdSrSaleRecord);
                }
            });
        }

        if (saleRInsertList.size() > 0) {
            //去重后插入GaiaSdSrSaleRecord表
            this.gaiaSdSrSaleRecordMapper.saleRecordInsert(saleRInsertList.stream().collect(Collectors.collectingAndThen(
                    Collectors.toCollection(() -> new TreeSet<>(
                            Comparator.comparing(p -> p.getClientId() + ";" + p.getGsssBillNo() + ";" + p.getGsssBrId() + ";" + p.getGsssDate() + ";" + p.getGsssSerial()))), ArrayList::new)));
        }
        saleH.setGsshDzqdyAmt1(inData.getDzqAmount());
        saleH.setGsshDyqAmt(inData.getDyqAmount());
        saleH.setGsshIntegralCash(inData.getJfdxPoint());
        saleH.setGsshIntegralCashAmt(inData.getJfdxAmount());
        saleH.setGsshZkAmt(saleH.getGsshZkAmt().add(inData.getJfdxAmount()).add(inData.getDzqAmount()).add(inData.getDyqAmount()));
        saleH.setGsshYsAmt(saleH.getGsshYsAmt().subtract(inData.getJfdxAmount()).subtract(inData.getDzqAmount()).subtract(inData.getDyqAmount())); //应收金额= 实收金额-（GSPM_TYPE = 2的支付方式）抵用券-电子券-积分抵现
        BigDecimal newPoint = this.givePoint(saleH, inData.getMemberId());
        saleH.setGsshIntegralAdd(String.valueOf(newPoint.setScale(0, RoundingMode.FLOOR)));
        saleH.setGsshNormalAmt(inData.getGaiaSdSaleH().getGsshNormalAmt());
        saleH.setGsshDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
        saleH.setGsshTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
        saleHMapper.updateByPrimaryKeySelective(saleH);

        //会员积分增减
        Map<String, Object> map = new HashMap<>(16);
        GaiaSdMemberCardData gaiaSdMemberCard = null;
        if (ObjectUtil.isNotEmpty(saleH.getGsshHykNo())) {
            gaiaSdMemberCard = this.memberBasicMapper.findMemberNameByCardId2(saleH.getGsshHykNo(), saleH.getClientId(), saleH.getGsshBrId());
            BigDecimal beforPoint = BigDecimal.ZERO;
            if (ObjectUtil.isNotEmpty(gaiaSdMemberCard)) {
                if (!StringUtils.isEmpty(gaiaSdMemberCard.getGsmbcIntegral())) {
                    beforPoint = new BigDecimal(gaiaSdMemberCard.getGsmbcIntegral().trim());
                }
                if (ObjectUtil.isNotEmpty(inData.getIntegralAddSet()) && "1".equals(inData.getIntegralAddSet())) { //判断是否增加积分
                    gaiaSdMemberCard.setGsmbcIntegral(String.valueOf(beforPoint.add(newPoint.setScale(0, RoundingMode.FLOOR)).setScale(0, RoundingMode.FLOOR)));
                }
                BigDecimal jfdxPoint = new BigDecimal(inData.getJfdxPoint());
                if (ObjectUtil.isNotEmpty(inData.getJfdxPoint()) && jfdxPoint.compareTo(BigDecimal.ZERO) > 0) { //判断是否要扣积分
                    gaiaSdMemberCard.setGsmbcIntegral(String.valueOf(new BigDecimal(gaiaSdMemberCard.getGsmbcIntegral()).subtract(jfdxPoint.setScale(0, RoundingMode.FLOOR)).setScale(0, RoundingMode.FLOOR)));
                }
                if (ObjectUtil.isNotEmpty(saleH.getGsshIntegralExchange()) && new BigDecimal(saleH.getGsshIntegralExchange()).compareTo(BigDecimal.ZERO) > 0) { //判断是否要扣积分
                    gaiaSdMemberCard.setGsmbcIntegral(String.valueOf(new BigDecimal(gaiaSdMemberCard.getGsmbcIntegral()).subtract(new BigDecimal(saleH.getGsshIntegralExchange()).setScale(0, RoundingMode.FLOOR)).setScale(0, RoundingMode.FLOOR)));
                }
                gaiaSdMemberCard.setGsmbcIntegralLastdate(DateUtil.format(DateUtil.date(), "yyyyMMdd"));
                gaiaSdMemberCardDataMapper.updateByPrimaryKeySelective(gaiaSdMemberCard);


                map.put("CLIENT", inData.getClientId());
                map.put("GSMBC_MEMBER_ID", gaiaSdMemberCard.getGsmbcMemberId());
                map.put("GSMBC_CARD_ID", gaiaSdMemberCard.getGsmbcCardId());
                map.put("GSMBC_BR_ID", gaiaSdMemberCard.getGsmbcBrId());
                GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
                aSynchronized.setClient(inData.getClientId());
                aSynchronized.setSite(gaiaSdMemberCard.getGsmbcBrId());
                aSynchronized.setVersion(IdUtil.randomUUID());
                aSynchronized.setTableName("GAIA_SD_MEMBER_CARD");
                aSynchronized.setCommand("update");
                aSynchronized.setSourceNo("会员-销售修改");
                aSynchronized.setArg(JSON.toJSONString(map));
                this.synchronizedService.addOne(aSynchronized);
            }
        }

        //todo  储值卡 暂时不管
        if (ObjectUtil.isNotEmpty(inData.getCzkNo())) {
            exampleRecharge = new Example(GaiaSdRechargeCard.class);
            exampleRecharge.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsrcId", inData.getCzkNo()).andEqualTo("gsrcStatus", "1");
            GaiaSdRechargeCard rechargeCard = this.rechargeCardMapper.selectOneByExample(exampleRecharge);
            rechargeCard.setGsrcAmt(rechargeCard.getGsrcAmt().subtract(inData.getCzkAmount()));
            this.rechargeCardMapper.updateByPrimaryKeySelective(rechargeCard);
        }


        //物料凭证
//        if (CollUtil.isNotEmpty(materialList)) {
//            String result = this.purchaseService.insertMaterialDoc(materialList);
//            if (StrUtil.isBlank(result)) {
//                throw new BusinessException(UtilMessage.SERVICE_EXCEPTION);
//            }
//            FeignResult feignResult = JSONObject.parseObject(result, FeignResult.class);
//            if (feignResult.getCode() == 0 && "执行成功".equals(feignResult.getMessage())) {
//                List<MaterialDocumentReturnData> saleDList2 = JSONObject.parseArray(feignResult.getData().toString(), MaterialDocumentReturnData.class);
//                if (CollUtil.isEmpty(saleDList)) {
//                    throw new BusinessException(UtilMessage.SERVICE_EXCEPTION);
//                }
//                this.saleDMapper.updateList(saleDList2);
//            } else {
//                throw new BusinessException(UtilMessage.SERVICE_EXCEPTION + ":" + feignResult.getMessage());
//            }
//        }
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("payCommit2");
        mqReceiveData.setData(inData.getBillNo());
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClientId() + "." + inData.getStoreCode(), JSON.toJSON(mqReceiveData));
        resultMap.put("materialList", materialList);

        List<GaiaSdBatchChange> batchChangeList = new ArrayList<>();
        for (GaiaSdSaleD sdSaleD : saleDList) {
            GaiaSdBatchChange item = new GaiaSdBatchChange();
            item.setGsbcProId(sdSaleD.getGssdProId());
            batchChangeList.add(item);
        }

        List<GaiaSdStock> stockList = stockMapper.findByClientAndGssBrIdAndGssProId(inData.getClientId(),
                inData.getStoreCode(), batchChangeList);
        //调外卖平台更新库存接口; GaiaSdStock是库存总表
        if (!CollectionUtils.isEmpty(stockList)) {
            stockService.syncStock(stockList);
//            try {
//                List<Map<String, Object>> paramMapList = stockService.getPlatformParamMap(stockList);
//                if (paramMapList != null) {
//                    log.info("库存更新，调外卖平台库存更新接口======");
//                    for (Map<String, Object> paramMap : paramMapList) {
//                        String accessToken = null;
//                        if (Constants.PLATFORMS_MT.equalsIgnoreCase((String) paramMap.get("platform"))) {
//                            GaiaTAccount account = (GaiaTAccount) paramMap.get("account");
//                            accessToken = takeAwayService.getAccessToken(account);
//                        }
//                        String result = baseService.updateStockWithMap(paramMap, accessToken);
//                    }
//                }
//            } catch (Exception e) {
//                log.error("调外卖平台更新库存接口报错，error:{}", e.getMessage());
//            }
        }

        return resultMap;
    }

    /**
     * @param clientId
     * @param siteId
     * @param proId
     * @return
     */
    private GaiaProductRelate getCrProductRelate(String clientId, String siteId, String proId) {
        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", clientId).andEqualTo("stoCode", siteId);
        GaiaStoreData storeData = storeDataMapper.selectOneByExample(example);

        //是否连锁
        boolean isChain = StringUtils.equals("2", storeData.getStoAttribute());
        example = new Example(GaiaProductRelate.class);
        if (isChain) {
            example.createCriteria().andEqualTo("client", clientId).andEqualTo("proSite", storeData.getStoDcCode()).andEqualTo("proSelfCode", proId);
        } else {
            example.createCriteria().andEqualTo("client", clientId).andEqualTo("proSite", siteId).andEqualTo("proSelfCode", proId);
        }

        return productRelateMapper.selectOneByExample(example);
    }

    @Override
    public void insertMaterialDoc(List<MaterialDocRequestDto> materialList) {
        log.info("materialList:{}", JSON.toJSONString(materialList));
        String result = purchaseService.insertMaterialDoc(materialList);
        log.info("insertMaterialDoc result:{}", JSON.toJSONString(result));
        JSONObject jsonObject = JSONObject.parseObject(result);
        if (!"0".equals(jsonObject.getString("code"))) {
            log.info("insertMaterialDoc :\"物料凭证接口错误\"");
//            throw new BusinessException(jsonObject.getString("物料凭证接口错误"));
            if ("-1".equals(jsonObject.getString("code"))) {
                materialDocService.addLog(materialList);
            }
        }
    }

    private BigDecimal givePoint(GaiaSdSaleH saleH, String memberId) {
        BigDecimal result = BigDecimal.ZERO;
        if (StrUtil.isNotBlank(saleH.getGsshHykNo()) && StrUtil.isNotBlank(memberId)) {
            BigDecimal amount = saleH.getGsshYsAmt();
            GetQueryMemberInData getQueryMemberInData = new GetQueryMemberInData();
            getQueryMemberInData.setClientId(saleH.getClientId());
            getQueryMemberInData.setBrId(saleH.getGsshBrId());
            getQueryMemberInData.setMemberId(memberId);
            getQueryMemberInData.setCardId(saleH.getGsshHykNo());
            GetQueryMemberOutData getQueryMemberOutData = this.memberBasicMapper.queryMember(getQueryMemberInData);
            BigDecimal perSale = BigDecimal.ZERO;
            BigDecimal perPoint = BigDecimal.ZERO;
            if (!StringUtils.isEmpty(getQueryMemberOutData.getAmtSale().trim())) {
                perSale = new BigDecimal(getQueryMemberOutData.getAmtSale().trim());
            }
            if (!StringUtils.isEmpty(getQueryMemberOutData.getIntegerSale().trim())) {
                perPoint = new BigDecimal(getQueryMemberOutData.getIntegerSale().trim());
            }
            if (perSale.compareTo(BigDecimal.ZERO) > 0 && perPoint.compareTo(BigDecimal.ZERO) > 0) {
                result = amount.divide(perSale, 0, RoundingMode.DOWN).multiply(perPoint).setScale(0, BigDecimal.ROUND_DOWN);
            }
        }
        return result;
    }

    @Override
    public List<GetDzqOutData> dzqList(GetPayInData inData) {
        List<GetDzqOutData> outData = this.promCouponBasicMapper.dzqList(inData);

        for (GetDzqOutData dzq : outData) {
            if (ObjectUtil.isNotEmpty(dzq.getReachAmount3()) && ObjectUtil.isNotEmpty(dzq.getReachQty3()) && inData.getAmount().compareTo(new BigDecimal(dzq.getReachAmount3())) >= 0 && (new BigDecimal(inData.getCount())).compareTo(new BigDecimal(dzq.getReachQty3())) >= 0 && (new BigDecimal(dzq.getGspcbCount())).compareTo(new BigDecimal(dzq.getResultQty3())) > 0) {
                dzq.setGspcbCount(dzq.getResultQty3());
            }

            if (ObjectUtil.isNotEmpty(dzq.getReachAmount2()) && ObjectUtil.isNotEmpty(dzq.getReachQty2()) && inData.getAmount().compareTo(new BigDecimal(dzq.getReachAmount2())) >= 0 && (new BigDecimal(inData.getCount())).compareTo(new BigDecimal(dzq.getReachQty2())) >= 0 && (new BigDecimal(dzq.getGspcbCount())).compareTo(new BigDecimal(dzq.getResultQty2())) > 0) {
                dzq.setGspcbCount(dzq.getResultQty2());
            }

            if (ObjectUtil.isNotEmpty(dzq.getReachAmount1()) && ObjectUtil.isNotEmpty(dzq.getReachQty1()) && inData.getAmount().compareTo(new BigDecimal(dzq.getReachAmount1())) >= 0 && (new BigDecimal(inData.getCount())).compareTo(new BigDecimal(dzq.getReachQty1())) >= 0 && (new BigDecimal(dzq.getGspcbCount())).compareTo(new BigDecimal(dzq.getResultQty1())) > 0) {
                dzq.setGspcbCount(dzq.getResultQty1());
            }
        }

        return outData;
    }

    //会员积分抵现验证
    @Override
    @Transactional(rollbackFor = Exception.class)
    public MemeberJfValidateOutData memeberJfDxValidate(MemeberJfValidateInData inData) {
        MemeberJfValidateOutData result = new MemeberJfValidateOutData();

        Example example;
//        example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("gsmbcCardId", inData.getCardNo()).andEqualTo("gsmbcBrId", inData.getBrId());
        GaiaSdMemberCardData memberCardData = memberBasicMapper.findMemberNameByCardId2(inData.getCardNo(), inData.getClient(), inData.getBrId()); //会员积分信息

        if (ObjectUtil.isEmpty(memberCardData)) {
            result.setCode("0");
            result.setMessage("找不到会员卡信息！");
            return result;
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String now = formatter.format(date);

        example = new Example(GaiaSdIntegralCashSet.class);
        example.createCriteria().andEqualTo("clientId", inData.getClient()).andEqualTo("gsicsBrId", inData.getBrId()).andGreaterThanOrEqualTo("gsicsDateEnd", now).andLessThanOrEqualTo("gsicsDateStart", now);
        List<GaiaSdIntegralCashSet> integralCashSetList = integralCashSetMapper.selectByExample(example); //门店积分抵现规则

        if (CollectionUtil.isEmpty(integralCashSetList)) {
            result.setCode("0");
            result.setMessage("本店未设置积分抵现规则，暂不可使用积分抵用！");
            return result;
        }

        integralCashSetList.forEach(item -> {
            if (memberCardData.getGsmbcClassId().equals(item.getGsicsMemberClass())) {
                if ("0".equals(item.getGsicsChangeSet()) && inData.getIntegralAmt().divideAndRemainder(BigDecimal.ONE)[1].compareTo(BigDecimal.ZERO) > 0) {
                    result.setCode("0");
                    result.setMessage("不允许支付小数！");
                    return;
                }
                if (item.getGsicsAmtOffsetMax() != null) {
                    if (item.getGsicsAmtOffsetMax().compareTo(inData.getIntegralAmt()) < 0) {
                        result.setCode("0");
                        result.setMessage("每单最大抵现金额不得超过" + item.getGsicsAmtOffsetMax().toString());
                        return;
                    }
                }
                BigDecimal jf = inData.getIntegralAmt().divide(item.getGsicsAmtOffset(), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(item.getGsicsIntegralDeduct()));
                if (jf.add(inData.getIntegralDhPoint()).compareTo(new BigDecimal(memberCardData.getGsmbcIntegral())) > 0) {
                    result.setCode("0");
                    result.setMessage("可用积分不足！");
                    return;
                } else if (jf.compareTo(BigDecimal.ZERO) > 0) {
                    result.setCode("1");
                    result.setMessage(jf.toString());
                    result.setIntegral(jf);
                    result.setIntegralAmt(inData.getIntegralAmt());
                    result.setIntegralAddSet(item.getGsicsIntegralAddSet());
                    return;
                }
            } else if ("0".equals(item.getGsicsMemberClass())) {
                if ("0".equals(item.getGsicsChangeSet()) && inData.getIntegralAmt().divideAndRemainder(BigDecimal.ONE)[1].compareTo(BigDecimal.ZERO) > 0) {
                    result.setCode("0");
                    result.setMessage("不允许支付小数！");
                    return;
                }
                if (item.getGsicsAmtOffsetMax() != null) {
                    if (item.getGsicsAmtOffsetMax().compareTo(inData.getIntegralAmt()) < 0) {
                        result.setCode("0");
                        result.setMessage("每单最大抵现金额不得超过" + item.getGsicsAmtOffsetMax().toString());
                        return;
                    }
                }
                BigDecimal jf = inData.getIntegralAmt().divide(item.getGsicsAmtOffset(), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(item.getGsicsIntegralDeduct()));
                if (jf.add(inData.getIntegralDhPoint()).compareTo(new BigDecimal(memberCardData.getGsmbcIntegral())) > 0) {
                    result.setCode("0");
                    result.setMessage("可用积分不足！");
                    return;
                } else if (jf.compareTo(BigDecimal.ZERO) > 0) {
                    result.setCode("1");
                    result.setMessage(jf.toString());
                    result.setIntegral(jf);
                    result.setIntegralAmt(inData.getIntegralAmt());
                    result.setIntegralAddSet(item.getGsicsIntegralAddSet());
                    return;
                }
            }
        });
        return result;
    }

    //送电子券
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<GaiaSdElectronChange> giveElectrons(GetPayInData inData) {
        Example example = new Example(GaiaSdSaleH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsshBrId", inData.getStoreCode()).andEqualTo("gsshBillNo", inData.getBillNo());
        GaiaSdSaleH saleH = saleHMapper.selectOneByExample(example);

        if (StringUtils.isEmpty(saleH.getGsshHykNo())) {
            return null;
        }

        GaiaSdMemberCardData gaiaSdMemberCardinData = new GaiaSdMemberCardData();
        gaiaSdMemberCardinData.setClient(saleH.getClientId());
        gaiaSdMemberCardinData.setGsmbcCardId(saleH.getGsshHykNo());

        GaiaSdMemberCardData gaiaSdMemberCard = this.gaiaSdMemberCardDataMapper.selectOne(gaiaSdMemberCardinData);

        if (ObjectUtil.isNotEmpty(gaiaSdMemberCard)) {
            Example exampleD = new Example(GaiaSdSaleD.class);
            exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssdBrId", inData.getStoreCode()).andEqualTo("gssdBillNo", inData.getBillNo());
            List<GaiaSdSaleD> saleDList = this.saleDMapper.selectByExample(exampleD);

            //1.首先查询所有送券条件
            GaiaSdStoreData gaiaSdStoreData = new GaiaSdStoreData();
            gaiaSdStoreData.setClientId(saleH.getClientId());
            gaiaSdStoreData.setGsstBrId(saleH.getGsshBrId());
            List<ElectronDetailOutData> electronDetailOutDataList = electronicCouponsService.selectGiftCouponsAndUse(gaiaSdStoreData);
            List<GaiaSdElectronChange> electronChangeList = new ArrayList<>();
            int qty = 0;
            //2.遍历所有送券条件 筛选出符合条件的
            for (ElectronDetailOutData item : electronDetailOutDataList) {
                BigDecimal canSend1 = null;
                BigDecimal canSend2 = null;
                if (StringUtils.isNotEmpty(item.getGsetsMaxQty())) {
                    if (StringUtils.isNotEmpty(item.getGsetsQty())) {
                        canSend1 = new BigDecimal(item.getGsetsMaxQty()).subtract(new BigDecimal(item.getGsetsQty()));
                        if (canSend1.compareTo(BigDecimal.ZERO) < 1) {
                            continue;
                        }
                    } else {
                        canSend1 = new BigDecimal(item.getGsetsMaxQty());
                    }
                }
                if (StringUtils.isNotEmpty(item.getGsebsMaxQty())) {
                    if (StringUtils.isNotEmpty(item.getGsebsQty())) {
                        canSend2 = new BigDecimal(item.getGsebsMaxQty()).subtract(new BigDecimal(item.getGsebsQty()));
                        if (canSend2.compareTo(BigDecimal.ZERO) < 1) {
                            continue;
                        }
                    } else {
                        canSend2 = new BigDecimal(item.getGsebsMaxQty());
                    }
                }
                qty = 0;
                BigDecimal isSelectQty = BigDecimal.ZERO; //已选择的数量
                BigDecimal isSelectAmt = BigDecimal.ZERO; //已选择的金额
                if ("Y".equals(item.getGsecsFlag2())) {//全场商品
                    //遍历商品列表 筛选
                    //遍历选择的电子券 累加
                    for (GaiaSdSaleD saleDItem : saleDList) {
                        if ("Y".equals(item.getGsebsProm())) {//Y为全参加  按整单实收金额计算是否达到送券/用券金额/数量
                            isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                            isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                        }else if ("N".equals(item.getGsebsProm())) {//N为全不参加 按销售单中无促销、无会员价、无会员日价、无会员日折扣、无会员卡折扣
                            if ("促销".equals(saleDItem.getGssdProStatus()) || "会员价".equals(saleDItem.getGssdProStatus()) ||
                            "退货".equals(saleDItem.getGssdProStatus()) || "会员卡折扣".equals(saleDItem.getGssdProStatus()) || "会员日价".equals(saleDItem.getGssdProStatus())
                            || "会员日折扣".equals(saleDItem.getGssdProStatus()) || "折后会员价".equals(saleDItem.getGssdProStatus())
                            || "折后会员日价".equals(saleDItem.getGssdProStatus())) {
                                continue;
                            }
                            isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                            isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                        } else if ("C".equals(item.getGsebsProm())) {
                            if ("退货".equals(saleDItem.getGssdProStatus()) || "促销".equals(saleDItem.getGssdProStatus())) {
                                continue;
                            }
                            isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                            isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                        } else if ("H".equals(item.getGsebsProm())) {
                            if ("会员价".equals(saleDItem.getGssdProStatus()) ||
                                    "退货".equals(saleDItem.getGssdProStatus()) || "会员卡折扣".equals(saleDItem.getGssdProStatus()) || "会员日价".equals(saleDItem.getGssdProStatus())
                                    || "会员日折扣".equals(saleDItem.getGssdProStatus()) || "折后会员价".equals(saleDItem.getGssdProStatus())
                                    || "折后会员日价".equals(saleDItem.getGssdProStatus())) {
                                continue;
                            }
                            isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                            isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                        }
                    }
                    //判断是否重复
                    if ("Y".equals(item.getGsecsFlag1())) {//循环赠送
                        if (!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2())) > -1) {
                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty2()));
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2()) > -1) {
                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt2());
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
                        } else if (!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1())) > -1) {
                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty1()));
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1()) > -1) {
                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt1());
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
                        }
                    } else {//单次赠送
                        if (!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2())) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty2()).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2()) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty2()).intValue();
                        } else if (!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1())) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty1()).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1()) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty1()).intValue();
                        }
                    }
                } else { //部分商品
                    //遍历选择的电子券 累加
                    for (GaiaSdSaleD saleDItem : saleDList) {
                        if (CollUtil.isEmpty(item.getProGroup())) {
                            continue;
                        }
                        for (GaiaSdProductsGroup proItem : item.getProGroup()) {
                            if (proItem.getGspgProId().equals(saleDItem.getGssdProId())) {
                                if ("Y".equals(item.getGsebsProm())) {//Y为全参加  按整单实收金额计算是否达到送券/用券金额/数量
                                    isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                                    isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                                }else if ("N".equals(item.getGsebsProm())) {//N为全不参加 按销售单中无促销、无会员价、无会员日价、无会员日折扣、无会员卡折扣
                                    if ("促销".equals(saleDItem.getGssdProStatus()) || "会员价".equals(saleDItem.getGssdProStatus()) ||
                                            "退货".equals(saleDItem.getGssdProStatus()) || "会员卡折扣".equals(saleDItem.getGssdProStatus()) || "会员日价".equals(saleDItem.getGssdProStatus())
                                            || "会员日折扣".equals(saleDItem.getGssdProStatus()) || "折后会员价".equals(saleDItem.getGssdProStatus())
                                            || "折后会员日价".equals(saleDItem.getGssdProStatus())) {
                                        continue;
                                    }
                                    isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                                    isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                                } else if ("C".equals(item.getGsebsProm())) {//按销售单中无促销类型
                                    if ("退货".equals(saleDItem.getGssdProStatus()) || "促销".equals(saleDItem.getGssdProStatus())) {
                                        continue;
                                    }
                                    isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                                    isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                                } else if ("H".equals(item.getGsebsProm())) {//按销售单中无会员价、会员日价、会员日折扣，会员卡折扣
                                    if ("会员价".equals(saleDItem.getGssdProStatus()) ||
                                            "退货".equals(saleDItem.getGssdProStatus()) || "会员卡折扣".equals(saleDItem.getGssdProStatus()) || "会员日价".equals(saleDItem.getGssdProStatus())
                                            || "会员日折扣".equals(saleDItem.getGssdProStatus()) || "折后会员价".equals(saleDItem.getGssdProStatus())
                                            || "折后会员日价".equals(saleDItem.getGssdProStatus())) {
                                        continue;
                                    }
                                    isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                                    isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                                }
                            }
                        }
                    }
                    //判断是否重复
                    if ("Y".equals(item.getGsecsFlag1())) {//循环赠送
                        if (!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2())) > -1) {
                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty2()));
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2()) > -1) {
                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt2());
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
                        } else if (!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1())) > -1) {
                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty1()));
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1()) > -1) {
                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt1());
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
                        }
                    } else { //单次赠送
                        if (!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2())) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty2()).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2()) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty2()).intValue();
                        } else if (!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1())) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty1()).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1()) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty1()).intValue();
                        }
                    }
                }
                if (ObjectUtil.isNotEmpty(canSend1)) {
                    assert canSend1 != null;
                    if (qty > canSend1.intValue()) {
                        qty = canSend1.intValue();
                    }
                }
                if (ObjectUtil.isNotEmpty(canSend2)) {
                    assert canSend2 != null;
                    if (qty > canSend2.intValue()) {
                        qty = canSend2.intValue();
                    }
                }
                //生成优惠券
                for (int i = 0; i < qty; i++) {
                    GaiaSdElectronChange gaiaSdElectronChange = new GaiaSdElectronChange();
                    gaiaSdElectronChange.setClient(saleH.getClientId());//加盟商
                    gaiaSdElectronChange.setGsebName(item.getGsecName());//电子券描述
                    assert gaiaSdMemberCard != null;
                    gaiaSdElectronChange.setGsecMemberId(gaiaSdMemberCard.getGsmbcCardId());//会员卡号
                    gaiaSdElectronChange.setGsecId("E"+SnowflakeIdUtil.getNextId());
                    gaiaSdElectronChange.setGsebId(item.getGsebId());//电子券活动号
                    gaiaSdElectronChange.setGsecAmt("0".equals(item.getGsebType()) ? item.getGsecAmt() : isSelectAmt.multiply(item.getGsecAmt()).setScale(2, BigDecimal.ROUND_HALF_UP));//电子券金额  电子券类型为0-固定值  1-非固定值
                    gaiaSdElectronChange.setGsecFlag("0");//0 :线上 1 :线下
                    gaiaSdElectronChange.setGsecStatus("N");//是否已使用
                    gaiaSdElectronChange.setGsecCreateDate(sdf.format(new Date()));//创建日期
                    gaiaSdElectronChange.setGsecFailDate(null);//失效日期
                    gaiaSdElectronChange.setGsecBrId(null);//销售门店
                    gaiaSdElectronChange.setGsecSaleDate(null);//销售日期
                    gaiaSdElectronChange.setGsebsId(item.getGsebsId());//业务单号
                    gaiaSdElectronChange.setGsetsId(item.getGsetsId());//主题号
                    gaiaSdElectronChange.setBrId(inData.getStoreCode());//门店
                    gaiaSdElectronChange.setGsecGiveBillNo(inData.getBillNo());//送券销售单
                    if("1".equals(item.getGsebsRule()) && CommonUtil.isBigDecimal(item.getGsebsValidDay())) {
                        gaiaSdElectronChange.setGsecFailDate(com.gys.util.DateUtil.addDay(new Date(), com.gys.util.DateUtil.DEFAULT_FORMAT,new BigDecimal(item.getGsebsValidDay()).intValue()));
                    }
                    gaiaSdElectronChange.setGsecUseExpirationDate(item.getGsecUseExpirationDate());//用券截止日期
                    electronChangeList.add(gaiaSdElectronChange);
                }
            }

            //3.插入表
            if (electronChangeList.size() > 0) {
                //记录数量
                electronChangeMapper.themeSendQtyIncr(electronChangeList);
                electronChangeMapper.bussinessSendQtyIncr(electronChangeList);
                electronChangeMapper.batchInsert(electronChangeList);
                return electronChangeList;
            }
            return null;
        }
        return null;
    }

    @Override
    public List<ElectronDetailOutData> calculateElectrons(GetPayInData inData) {


        GaiaSdMemberCardData gaiaSdMemberCardinData = new GaiaSdMemberCardData();
        gaiaSdMemberCardinData.setClient(inData.getClientId());
        gaiaSdMemberCardinData.setGsmbcCardId(inData.getMemberId());

        GaiaSdMemberCardData gaiaSdMemberCard = this.gaiaSdMemberCardDataMapper.selectOne(gaiaSdMemberCardinData);

        if (ObjectUtil.isNotEmpty(gaiaSdMemberCard)) {
            if (ObjectUtil.isEmpty(inData.getGaiaSdSaleDList())) {
                return null;
            }
            List<GaiaSdSaleD> saleDList = inData.getGaiaSdSaleDList();

            //1.首先查询所有送券条件
            GaiaSdStoreData gaiaSdStoreData = new GaiaSdStoreData();
            gaiaSdStoreData.setClientId(inData.getClientId());
            gaiaSdStoreData.setGsstBrId(inData.getStoreCode());
            List<ElectronDetailOutData> electronDetailOutDataList = electronicCouponsService.selectGiftCouponsAndUse(gaiaSdStoreData);
            List<GaiaSdElectronChange> electronChangeList = new ArrayList<>();
            int qty = 0;
            //2.遍历所有送券条件 筛选出符合条件的
            for (ElectronDetailOutData item : electronDetailOutDataList) {
                BigDecimal canSend1 = null;
                BigDecimal canSend2 = null;
                if (StringUtils.isNotEmpty(item.getGsetsMaxQty())) {
                    if (StringUtils.isNotEmpty(item.getGsetsQty())) {
                        canSend1 = new BigDecimal(item.getGsetsMaxQty()).subtract(new BigDecimal(item.getGsetsQty()));
                        if (canSend1.compareTo(BigDecimal.ZERO) < 1) {
                            continue;
                        }
                    } else {
                        canSend1 = new BigDecimal(item.getGsetsMaxQty());
                    }
                }
                if (StringUtils.isNotEmpty(item.getGsebsMaxQty())) {
                    if (StringUtils.isNotEmpty(item.getGsebsQty())) {
                        canSend2 = new BigDecimal(item.getGsebsMaxQty()).subtract(new BigDecimal(item.getGsebsQty()));
                        if (canSend2.compareTo(BigDecimal.ZERO) < 1) {
                            continue;
                        }
                    } else {
                        canSend2 = new BigDecimal(item.getGsebsMaxQty());
                    }
                }
                qty = 0;
                BigDecimal isSelectQty = BigDecimal.ZERO; //已选择的数量
                BigDecimal isSelectAmt = BigDecimal.ZERO; //已选择的金额
                if ("Y".equals(item.getGsecsFlag2())) {//全场商品
                    //遍历商品列表 筛选
                    //遍历选择的电子券 累加
                    for (GaiaSdSaleD saleDItem : saleDList) {
                        if ("Y".equals(item.getGsebsProm())) {//Y为全参加  按整单实收金额计算是否达到送券/用券金额/数量
                            isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                            isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                        }else if ("N".equals(item.getGsebsProm())) {//N为全不参加 按销售单中无促销、无会员价、无会员日价、无会员日折扣、无会员卡折扣
                            if ("促销".equals(saleDItem.getGssdProStatus()) || "会员价".equals(saleDItem.getGssdProStatus()) ||
                                    "退货".equals(saleDItem.getGssdProStatus()) || "会员卡折扣".equals(saleDItem.getGssdProStatus()) || "会员日价".equals(saleDItem.getGssdProStatus())
                                    || "会员日折扣".equals(saleDItem.getGssdProStatus()) || "折后会员价".equals(saleDItem.getGssdProStatus())
                                    || "折后会员日价".equals(saleDItem.getGssdProStatus())) {
                                continue;
                            }
                            isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                            isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                        } else if ("C".equals(item.getGsebsProm())) {
                            if ("退货".equals(saleDItem.getGssdProStatus()) || "促销".equals(saleDItem.getGssdProStatus())) {
                                continue;
                            }
                            isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                            isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                        } else if ("H".equals(item.getGsebsProm())) {
                            if ("会员价".equals(saleDItem.getGssdProStatus()) ||
                                    "退货".equals(saleDItem.getGssdProStatus()) || "会员卡折扣".equals(saleDItem.getGssdProStatus()) || "会员日价".equals(saleDItem.getGssdProStatus())
                                    || "会员日折扣".equals(saleDItem.getGssdProStatus()) || "折后会员价".equals(saleDItem.getGssdProStatus())
                                    || "折后会员日价".equals(saleDItem.getGssdProStatus())) {
                                continue;
                            }
                            isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                            isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                        }
                    }
                    //判断是否重复
                    if ("Y".equals(item.getGsecsFlag1())) {//循环赠送
                        if (!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2())) > -1) {
                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty2()));
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2()) > -1) {
                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt2());
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
                        } else if (!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1())) > -1) {
                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty1()));
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1()) > -1) {
                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt1());
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
                        }
                    } else {//单次赠送
                        if (!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2())) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty2()).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2()) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty2()).intValue();
                        } else if (!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1())) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty1()).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1()) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty1()).intValue();
                        }
                    }
                } else { //部分商品
                    //遍历选择的电子券 累加
                    for (GaiaSdSaleD saleDItem : saleDList) {
                        if (CollUtil.isEmpty(item.getProGroup())) {
                            continue;
                        }
                        for (GaiaSdProductsGroup proItem : item.getProGroup()) {
                            if (proItem.getGspgProId().equals(saleDItem.getGssdProId())) {
                                if ("Y".equals(item.getGsebsProm())) {//Y为全参加  按整单实收金额计算是否达到送券/用券金额/数量
                                    isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                                    isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                                }else if ("N".equals(item.getGsebsProm())) {//N为全不参加 按销售单中无促销、无会员价、无会员日价、无会员日折扣、无会员卡折扣
                                    if ("促销".equals(saleDItem.getGssdProStatus()) || "会员价".equals(saleDItem.getGssdProStatus()) ||
                                            "退货".equals(saleDItem.getGssdProStatus()) || "会员卡折扣".equals(saleDItem.getGssdProStatus()) || "会员日价".equals(saleDItem.getGssdProStatus())
                                            || "会员日折扣".equals(saleDItem.getGssdProStatus()) || "折后会员价".equals(saleDItem.getGssdProStatus())
                                            || "折后会员日价".equals(saleDItem.getGssdProStatus())) {
                                        continue;
                                    }
                                    isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                                    isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                                } else if ("C".equals(item.getGsebsProm())) {//按销售单中无促销类型
                                    if ("退货".equals(saleDItem.getGssdProStatus()) || "促销".equals(saleDItem.getGssdProStatus())) {
                                        continue;
                                    }
                                    isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                                    isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                                } else if ("H".equals(item.getGsebsProm())) {//按销售单中无会员价、会员日价、会员日折扣，会员卡折扣
                                    if ("会员价".equals(saleDItem.getGssdProStatus()) ||
                                            "退货".equals(saleDItem.getGssdProStatus()) || "会员卡折扣".equals(saleDItem.getGssdProStatus()) || "会员日价".equals(saleDItem.getGssdProStatus())
                                            || "会员日折扣".equals(saleDItem.getGssdProStatus()) || "折后会员价".equals(saleDItem.getGssdProStatus())
                                            || "折后会员日价".equals(saleDItem.getGssdProStatus())) {
                                        continue;
                                    }
                                    isSelectQty = isSelectQty.add(new BigDecimal(saleDItem.getGssdQty()));
                                    isSelectAmt = isSelectAmt.add(saleDItem.getGssdAmt());
                                }
                            }
                        }
                    }
                    //判断是否重复
                    if ("Y".equals(item.getGsecsFlag1())) {//循环赠送
                        if (!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2())) > -1) {
                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty2()));
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2()) > -1) {
                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt2());
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty2())).intValue();
                        } else if (!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1())) > -1) {
                            BigDecimal[] temp = isSelectQty.divideAndRemainder(new BigDecimal(item.getGsecsReachQty1()));
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1()) > -1) {
                            BigDecimal[] temp = isSelectAmt.divideAndRemainder(item.getGsecsReachAmt1());
                            qty = temp[0].multiply(new BigDecimal(item.getGsecsResultQty1())).intValue();
                        }
                    } else { //单次赠送
                        if (!StringUtils.isEmpty(item.getGsecsReachQty2()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty2())) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty2()).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt2()) && item.getGsecsReachAmt2().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt2()) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty2()).intValue();
                        } else if (!StringUtils.isEmpty(item.getGsecsReachQty1()) && isSelectQty.compareTo(new BigDecimal(item.getGsecsReachQty1())) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty1()).intValue();
                        } else if (ObjectUtil.isNotEmpty(item.getGsecsReachAmt1()) && item.getGsecsReachAmt1().compareTo(BigDecimal.ZERO) > 0 && isSelectAmt.compareTo(item.getGsecsReachAmt1()) > -1) {
                            qty = new BigDecimal(item.getGsecsResultQty1()).intValue();
                        }
                    }
                }
                if (ObjectUtil.isNotEmpty(canSend1)) {
                    assert canSend1 != null;
                    if (qty > canSend1.intValue()) {
                        qty = canSend1.intValue();
                    }
                }
                if (ObjectUtil.isNotEmpty(canSend2)) {
                    assert canSend2 != null;
                    if (qty > canSend2.intValue()) {
                        qty = canSend2.intValue();
                    }
                }
                //生成优惠券
                for (int i = 0; i < qty; i++) {
                    GaiaSdElectronChange gaiaSdElectronChange = new GaiaSdElectronChange();
                    gaiaSdElectronChange.setClient(inData.getClientId());//加盟商
                    gaiaSdElectronChange.setGsebName(item.getGsecName());//电子券描述
                    assert gaiaSdMemberCard != null;
                    gaiaSdElectronChange.setGsecMemberId(gaiaSdMemberCard.getGsmbcCardId());//会员卡号
                    gaiaSdElectronChange.setGsecId("E"+SnowflakeIdUtil.getNextId());
                    gaiaSdElectronChange.setGsebId(item.getGsebId());//电子券活动号
                    gaiaSdElectronChange.setGsecAmt("0".equals(item.getGsebType()) ? item.getGsecAmt() : isSelectAmt.multiply(item.getGsecAmt()).setScale(2, BigDecimal.ROUND_HALF_UP));//电子券金额  电子券类型为0-固定值  1-非固定值
                    gaiaSdElectronChange.setGsecFlag("0");//0 :线上 1 :线下
                    gaiaSdElectronChange.setGsecStatus("N");//是否已使用
                    gaiaSdElectronChange.setGsecCreateDate(sdf.format(new Date()));//创建日期
                    gaiaSdElectronChange.setGsecFailDate(null);//失效日期
                    gaiaSdElectronChange.setGsecBrId(null);//销售门店
                    gaiaSdElectronChange.setGsecSaleDate(null);//销售日期
                    gaiaSdElectronChange.setGsebsId(item.getGsebsId());//业务单号
                    gaiaSdElectronChange.setGsetsId(item.getGsetsId());//主题号
                    gaiaSdElectronChange.setBrId(inData.getStoreCode());//门店
                    gaiaSdElectronChange.setGsecGiveBillNo(inData.getBillNo());//送券销售单
                    if("1".equals(item.getGsebsRule()) && CommonUtil.isBigDecimal(item.getGsebsValidDay())) {
                        gaiaSdElectronChange.setGsecFailDate(com.gys.util.DateUtil.addDay(new Date(), com.gys.util.DateUtil.DEFAULT_FORMAT,new BigDecimal(item.getGsebsValidDay()).intValue()));
                    }
                    gaiaSdElectronChange.setGsecUseExpirationDate(item.getGsecUseExpirationDate());//用券截止日期
                    gaiaSdElectronChange.setGsecsFlag2(item.getGsecsFlag2());//是否全部商品
                    electronChangeList.add(gaiaSdElectronChange);
                }
            }
            if (CollUtil.isNotEmpty(electronChangeList)) {
                List<ElectronDetailOutData> sendInfo = new ArrayList<>();
                Map<String, List<GaiaSdElectronChange>> collect = electronChangeList.stream().collect(Collectors.groupingBy(GaiaSdElectronChange::getGsebId));
                Set<String> gsebIdtSet = collect.keySet();
                for (String gsebId : gsebIdtSet) {
                    List<GaiaSdElectronChange> sendList = collect.get(gsebId);
                    GaiaSdElectronChange gaiaSdElectronChange = sendList.get(0);
                    ElectronDetailOutData outData = new ElectronDetailOutData();
                    outData.setClient(inData.getClientId());//加盟商
                    outData.setGsebName(gaiaSdElectronChange.getGsebName());//电子券描述
                    outData.setGsecMemberId(gaiaSdMemberCard.getGsmbcCardId());//会员卡号
                    outData.setGsebId(gaiaSdElectronChange.getGsebId());//电子券活动号
                    outData.setGsecAmt(gaiaSdElectronChange.getGsecAmt());//电子券金额  电子券类型为0-固定值  1-非固定值
                    outData.setGsecFlag("0");//0 :线上 1 :线下
                    outData.setGsecStatus("N");//是否已使用
                    outData.setGsecCreateDate(sdf.format(new Date()));//创建日期
                    outData.setGsecFailDate(null);//失效日期
                    outData.setGsecBrId(null);//销售门店
                    outData.setGsecSaleDate(null);//销售日期
                    outData.setGsebsId(gaiaSdElectronChange.getGsebsId());//业务单号
                    outData.setGsetsId(gaiaSdElectronChange.getGsetsId());//主题号
                    outData.setGsecFailDate(gaiaSdElectronChange.getGsecFailDate());
                    outData.setGsecUseExpirationDate(gaiaSdElectronChange.getGsecUseExpirationDate());//用券截止日期
                    outData.setGsecsFlag2(gaiaSdElectronChange.getGsecsFlag2());//是否全部商品
                    outData.setSendQty(String.valueOf(sendList.size()));
                    outData.setGsecName(gaiaSdElectronChange.getGsebName());
                    sendInfo.add(outData);
                }
                return sendInfo;
            }
            return null;
        }
        return null;
    }

    @Override
    public void insertSendElectronList(String client, String brId, String gsshBillNo, List<GaiaSdSendElectron> sendElectronList) {
        if (CollUtil.isNotEmpty(sendElectronList)) {
            List<GaiaSdElectronChange> electronChangeList = new ArrayList<>();
            for (GaiaSdSendElectron gaiaSdSendElectron : sendElectronList) {
                if (StringUtil.isNotBlank(gaiaSdSendElectron.getGsecSendQty())) {
                    for (Integer i = 0; i < Integer.valueOf(gaiaSdSendElectron.getGsecSendQty()); i++) {
                        GaiaSdElectronChange change = new GaiaSdElectronChange();
                        change.setClient(gaiaSdSendElectron.getClient());//加盟商
                        change.setGsecId("E"+SnowflakeIdUtil.getNextId());;//电子券号
                        change.setGsebName(gaiaSdSendElectron.getGsebName());//电子券描述
                        change.setGsecMemberId(gaiaSdSendElectron.getGsecMemberId());//会员卡号
                        change.setGsebId(gaiaSdSendElectron.getGsebId());//电子券活动号
                        change.setGsecAmt(new BigDecimal(gaiaSdSendElectron.getGsecAmt()));//电子券金额  电子券类型为0-固定值  1-非固定值
                        change.setGsecFlag("0");//0 :线上 1 :线下
                        change.setGsecStatus("N");//是否已使用
                        change.setGsecCreateDate(sdf.format(new Date()));//创建日期
                        change.setGsecFailDate(null);//失效日期
                        change.setGsecBrId(null);//销售门店
                        change.setGsecSaleDate(null);//销售日期
                        change.setGsecGiveBrId(gaiaSdSendElectron.getGsecGiveBrId());//门店
                        change.setGsecGiveBillNo(gaiaSdSendElectron.getGsecGiveBillNo());//送券销售单
                        change.setGsecFailDate(StringUtil.isNotBlank(gaiaSdSendElectron.getGsecFailDate()) ? gaiaSdSendElectron.getGsecFailDate() : null);
                        change.setGsecUseExpirationDate(StringUtil.isNotBlank(gaiaSdSendElectron.getGsecUseExpirationDate()) ? gaiaSdSendElectron.getGsecUseExpirationDate() : null);//用券截止日期
                        change.setGsecGiveSaleDate(DateUtil.parse(gaiaSdSendElectron.getGsecGiveSaleDate()));//用券截止日期
                        electronChangeList.add(change);
                    }
                }
            }
            //记录数量
            if (CollUtil.isNotEmpty(electronChangeList)) {
                electronChangeMapper.themeSendQtyIncr(electronChangeList);
                electronChangeMapper.bussinessSendQtyIncr(electronChangeList);
                electronChangeMapper.batchInsert(electronChangeList);
            }
        }
    }


    @Override
    public List<GaiaSdRechargeCard> queryRecharge(RechargeQueryInData inData) {

        Example example = new Example(GaiaStoreData.class);
        example.createCriteria().andEqualTo("client", inData.getClient()).andEqualTo("stoCode", inData.getBrId());
        GaiaStoreData storeData = storeDataMapper.selectOneByExample(example);
        if (storeData == null) {
            return null;
        }
        //判断是否是连锁 如果是连锁 查询条件变为连锁编码
        if ("2".equals(storeData.getStoAttribute())) {
            inData.setBrId(storeData.getStoChainHead());
        }

        //2022-01-04 update by SunJianan begin
        /*example = new Example(GaiaSdRechargeCard.class);
        example.createCriteria()
                .andEqualTo("clientId", inData.getClient())
                .andEqualTo("gsrcBrId", inData.getBrId())
                .andEqualTo("gsrcStatus", "1")
                .andEqualTo("gsrcMemberCardId", inData.getQueryParam());

        Example.Criteria criteria2 = example.createCriteria()
                .andEqualTo("clientId", inData.getClient())
                .andEqualTo("gsrcBrId", inData.getBrId())
                .andEqualTo("gsrcStatus", "1")
                .andEqualTo("gsrcMobile", inData.getQueryParam());

        Example.Criteria criteria3 = example.createCriteria()
                .andEqualTo("clientId", inData.getClient())
                .andEqualTo("gsrcBrId", inData.getBrId())
                .andEqualTo("gsrcStatus", "1")
                .andEqualTo("gsrcId", inData.getQueryParam());

        example.or(criteria2);
        example.or(criteria3);
        return rechargeCardMapper.selectByExample(example);*/
        //2022-01-04 update by SunJianan end

        return rechargeCardMapper.getEffectRechargeList(inData);
    }

    @Override
    public GaiaSdRechargeCardSet isNeedPassword(String client, String brId) {
        Example example = new Example(GaiaSdRechargeCardSet.class);
        example.createCriteria().andEqualTo("clientId", client).andEqualTo("gsrcsBrId", brId);
        return rechargeCardSetMapper.selectOneByExample(example);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> receiveSaleOrder(MQSaleOrderInData inData) {
        Map<String, Object> resultMap = new HashMap<>();
        if (ObjectUtil.isNotEmpty(inData.getSaleH())) {
            //检查单据是否重复
            Example example = new Example(GaiaSdSaleH.class);
            example.createCriteria().andEqualTo("clientId", inData.getClient()).andEqualTo("gsshBrId", inData.getBrId()).andEqualTo("gsshBillNo", inData.getSaleH().getGsshBillNo()).andNotEqualTo("gsshHideFlag", "1");
            GaiaSdSaleH gaiaSdSaleH = saleHMapper.selectOneByExample(example);
            if (ObjectUtil.isNotEmpty(gaiaSdSaleH)) {
                throw new BusinessException("销售单号：" + inData.getSaleH().getGsshBillNo() + "," + UtilMessage.SALE_ORDER_EXISTS);
            }
            //clientId的坑.........
            inData.getSaleH().setClientId(inData.getClient());

            //判断是否是挂单，需要修改挂单表状态
            GaiaSdSaleHandH billNo = this.gaiaSdSaleHandHMapper.getSaleHByBillNo(inData.getSaleH().getGsshBillNo(), inData.getSaleH().getClientId(), inData.getSaleH().getGsshBrId());
            if (ObjectUtil.isNotEmpty(billNo)) {
                billNo.setSaleHFlag(UtilMessage.Y);
                //远程审方挂单
                if ("5".equals(billNo.getGsshHideFlag())) {
                    billNo.setGsshCallAllow("0");
                } else {
                    billNo.setGsshHideFlag("0");
                }
                this.gaiaSdSaleHandHMapper.update(billNo);

                List<GaiaSdSaleHandD> handDList = this.gaiaSdSaleHandDMapper.getAllByBillNo(inData.getSaleH().getClientId(), inData.getSaleH().getGsshBrId(), inData.getSaleH().getGsshBillNo());
                handDList.forEach(handD -> handD.setSaleDFlag(UtilMessage.Y));
                this.gaiaSdSaleHandDMapper.updateList(handDList);

            }

            //调用次数默认为1
            if (StrUtil.isEmpty(inData.getSaleH().getGsshCallQty())) {
                inData.getSaleH().setGsshCallQty("1");
            }

            //插入saleH表
            saleHMapper.insert(inData.getSaleH());

            List<GaiaSdSaleD> list = new ArrayList<>();
            for (GaiaSdSaleD sdSaleD : inData.getSaleDList()) {
                sdSaleD.setClientId(inData.getClient());
                if (StrUtil.isBlank(sdSaleD.getGssdIfXnsp()) || "0".equals(sdSaleD.getGssdIfXnsp())) {
                    //移动平均价
                    Example materialAssessExample = new Example(GaiaMaterialAssess.class);
                    materialAssessExample.createCriteria()
                            .andEqualTo("client", inData.getClient())
                            .andEqualTo("matAssessSite", inData.getBrId())
                            .andEqualTo("matProCode", sdSaleD.getGssdProId());
                    GaiaMaterialAssess materialAssess = materialAssessMapper.selectOneByExample(materialAssessExample);
                    if (ObjectUtil.isNotEmpty(materialAssess)) {
                        sdSaleD.setGssdMovPrice(materialAssess.getMatMovPrice());//参考价
                        sdSaleD.setGssdMovPrices(materialAssess.getMatMovPrice().multiply(new BigDecimal(sdSaleD.getGssdQty())));//参考价/数量?
                        sdSaleD.setGssdTaxRate(sdSaleD.getGssdMovPrices().multiply(sdSaleD.getGssdMovTax()));//(参考价/数量)*税率?
                        sdSaleD.setGssdAddAmt(materialAssess.getMatMovPrice().multiply(new BigDecimal(sdSaleD.getGssdQty())));
                        sdSaleD.setGssdAddTax(sdSaleD.getGssdMovPrices().multiply(sdSaleD.getGssdMovTax()));
                    }
                }
                if (UtilMessage.Suffix1.equals(sdSaleD.getGssdIFkc())) {//表示当前行商品 属于负库存
                    list.add(sdSaleD);
                }

            }

            if (list.size() > 0) { //表示有负库存销售
                GaiaSdMessage fxMsg = new GaiaSdMessage();
                fxMsg.setClient(inData.getSaleH().getClientId());
                fxMsg.setGsmId(inData.getSaleH().getGsshBrId());
                fxMsg.setGsmVoucherId(inData.getSaleH().getGsshBillNo());
                fxMsg.setGsmValue("isNegativeInventory=1&type=0");
                fxMsg.setGsmRemark(DateUtil.now() + "发生负库存销售，请及时核对库存信息！");
                fxMsg.setGsmFlag(UtilMessage.N);
                fxMsg.setGsmPage("reportQuery");
                fxMsg.setGsmArriveDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                fxMsg.setGsmArriveTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
                fxMsg.setGsmPlatForm("FX");
                fxMsg.setGsmDeleteFlag("0");
                this.gaiaSdMessageMapper.insertSelective(fxMsg);
            }

            //插入saleD表
            saleDMapper.insertLists(inData.getSaleDList());

            //插入库存异动表
            if (CollUtil.isNotEmpty(inData.getBatchChangeList())) {
                batchChangeMapper.insertLists(inData.getBatchChangeList());
            }

            //支付明细表
            if (CollectionUtil.isNotEmpty(inData.getPayMsgList())) {
                payMsgMapper.insertLists(inData.getPayMsgList());
                //收银通支付时 刷新中间表状态
                inData.getPayMsgList().stream().filter(item -> "2001".equals(item.getGsspmId())).findFirst().ifPresent(item -> {
                    Example sytExample = new Example(GaiaSdSytOperation.class);
                    sytExample.createCriteria().andEqualTo("client", item.getClient()).andEqualTo("brId", item.getGsspmBrId()).andEqualTo("outerNumber", item.getGsspmCardNo());
                    GaiaSdSytOperation sytOperation = sytOperationMapper.selectOneByExample(sytExample);
                    if (ObjectUtil.isNotEmpty(sytOperation)) {
                        sytOperation.setResult("1");
                        sytOperation.setBillNo(inData.getSaleH().getGsshBillNo());
                        sytOperationMapper.updateByExample(sytOperation, sytExample);
                    }
                });
            }

            //处方登记表
            if (CollectionUtil.isNotEmpty(inData.getSaleRecipelList())) {
                inData.getSaleRecipelList().forEach(item -> {
                    item.setClientId(inData.getClient());
                });
                gaiaSdSaleRecipelMapper.insertLists(inData.getSaleRecipelList());
            }

            //判断是否中药代煎订单，中药代煎订单中中药不需要扣库存
            if (StringUtils.isEmpty(inData.getSaleH().getGsshCrFlag())) {
                //扣库存
                List<StockInData> stockList = new ArrayList<>();
                inData.getBatchChangeList().forEach(item -> {
                    StockInData stockInData = new StockInData();
                    stockInData.setClientId(inData.getSaleH().getClientId());
                    stockInData.setGssBrId(inData.getSaleH().getGsshBrId());
                    stockInData.setGssProId(item.getGsbcProId());
                    stockInData.setBatchNo(item.getGsbcBatchNo());
                    stockInData.setBatch(item.getGsbcBatch());
                    stockInData.setNum(item.getGsbcQty());
                    stockInData.setVoucherId(inData.getSaleH().getGsshBillNo());
                    stockInData.setSerial(item.getGsbcSerial());
                    stockInData.setEmpId(inData.getSaleH().getGsshEmp());
                    stockInData.setCrFlag(inData.getSaleH().getGsshCrFlag());
                    stockInData.setUpdateDate(CommonUtil.getyyyyMMdd());
                    if (StringUtils.isEmpty(inData.getSaleH().getGsshCrFlag())) {
                        stockList.add(stockInData);

                        Map<String, Object> map = new HashMap<>(16);
                        map.put("CLIENT", stockInData.getClientId());
                        map.put("GSSB_BR_ID", stockInData.getGssBrId());
                        map.put("GSSB_PRO_ID", stockInData.getGssProId());
                        map.put("GSSB_BATCH_NO", stockInData.getBatchNo());
                        map.put("GSSB_BATCH", stockInData.getBatch());
                        GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
                        aSynchronized.setClient(stockInData.getClientId());
                        aSynchronized.setSite(stockInData.getGssBrId());
                        aSynchronized.setVersion(IdUtil.randomUUID());
                        aSynchronized.setTableName("GAIA_SD_STOCK_BATCH");
                        aSynchronized.setCommand("update");
                        aSynchronized.setSourceNo("销售");
                        aSynchronized.setDatetime(String.valueOf(Calendar.getInstance().getTimeInMillis()));
                        aSynchronized.setArg(JSON.toJSONString(map));
                        this.synchronizedService.addOne(aSynchronized);

                        Map<String, Object> map2 = new HashMap<>(16);
                        map2.put("CLIENT", stockInData.getClientId());
                        map2.put("GSS_BR_ID", stockInData.getGssBrId());
                        map2.put("GSS_PRO_ID", stockInData.getGssProId());
                        GaiaDataSynchronized aSynchronized2 = new GaiaDataSynchronized();
                        aSynchronized2.setClient(stockInData.getClientId());
                        aSynchronized2.setSite(stockInData.getGssBrId());
                        aSynchronized2.setVersion(IdUtil.randomUUID());
                        aSynchronized2.setTableName("GAIA_SD_STOCK");
                        aSynchronized2.setCommand("update");
                        aSynchronized2.setSourceNo("销售");
                        aSynchronized2.setArg(JSON.toJSONString(map2));
                        this.synchronizedService.addOne(aSynchronized2);
                    }
                    Map<String, Object> map2 = new HashMap<>(16);
                    map2.put("CLIENT", item.getClient());
                    map2.put("GSBC_BR_ID", item.getGsbcBrId());
                    map2.put("GSBC_VOUCHER_ID", item.getGsbcVoucherId());
                    map2.put("GSBC_DATE", item.getGsbcDate());
                    map2.put("GSBC_SERIAL", item.getGsbcSerial());
                    map2.put("GSBC_PRO_ID", item.getGsbcProId());
                    map2.put("GSBC_BATCH", item.getGsbcBatch());
                    GaiaDataSynchronized aSynchronized2 = new GaiaDataSynchronized();
                    aSynchronized2.setClient(stockInData.getClientId());
                    aSynchronized2.setSite(stockInData.getGssBrId());
                    aSynchronized2.setVersion(IdUtil.randomUUID());
                    aSynchronized2.setTableName("GAIA_SD_BATCH_CHANGE");
                    aSynchronized2.setCommand("insert");
                    aSynchronized2.setSourceNo("销售");
                    aSynchronized2.setArg(JSON.toJSONString(map2));
                    this.synchronizedService.addOne(aSynchronized2);
                });
                if (CollUtil.isNotEmpty(stockList)) {
                    //根据料号扣减总库存
                    stockMapper.updateQtyList(stockList);
                    //库存更新需要同步外卖库存
                    List<GaiaSdStock> allNeedSyncStock = stockMapper.findAllByStockInData(stockList);
                    Map<String, List<GaiaSdStock>> stockMap = allNeedSyncStock.stream().collect(Collectors.groupingBy(r -> r.getClientId() + r.getGssBrId()));
                    //调外卖平台更新库存接口;
                    for (List<GaiaSdStock> tempList : stockMap.values()) {
                        if (!CollectionUtils.isEmpty(tempList)) {
                            stockService.syncStock(tempList);
                        }
                    }
                    //扣减批号批次库存
                    stockBatchMapper.updateQtyList(stockList);
                }
            }

            //组装物料凭证list
            List<MaterialDocRequestDto> materialList = Lists.newArrayList();
            try {
                int guiNoCount = 1;
                String currentDate = CommonUtil.getyyyyMMdd();
                for (int i = 0; i < inData.getBatchChangeList().size(); i++) {
                    GaiaSdBatchChange batchChange = inData.getBatchChangeList().get(i);
                    String consumeBatch = batchChange.getGsbcBatch();
                    BigDecimal stockNum = batchChange.getGsbcQty();
                    MaterialDocRequestDto dto = new MaterialDocRequestDto();
                    dto.setClient(inData.getClient());
                    dto.setGuid(UUID.randomUUID().toString());
                    dto.setGuidNo(StrUtil.toString(guiNoCount));
                    if (StringUtils.isNotEmpty(inData.getSaleH().getGsshCrFlag())) {
                        dto.setMatType(MaterialTypeEnum.ZHONG_YAO_DAI_JIAN.getCode());
                        GaiaProductRelate productRelate = this.getCrProductRelate(inData.getSaleH().getClientId(), inData.getSaleH().getGsshBrId(), batchChange.getGsbcProId());
                        dto.setMatPrice(productRelate != null ? new BigDecimal(productRelate.getProPrice()) : BigDecimal.ZERO);
                    } else {
                        dto.setMatType(MaterialTypeEnum.LING_SHOU.getCode());
                        dto.setMatPrice(inData.getSaleDList().stream().filter(item -> item.getGssdProId().equals(batchChange.getGsbcProId()) && item.getGssdSerial().equals(batchChange.getGsbcSerial())).findFirst().get().getGssdPrc2());
                    }
                    dto.setMatPostDate(currentDate);
                    dto.setMatHeadRemark("");
                    dto.setMatProCode(batchChange.getGsbcProId());
                    dto.setMatSiteCode(inData.getSaleH().getGsshBrId());
                    dto.setMatLocationCode("1000");
                    dto.setMatLocationTo("");
                    dto.setMatBatch(consumeBatch);
                    dto.setMatQty(stockNum.negate());
                    //            dto.setMatAmt(saleH.getGsshYsAmt());
                    dto.setMatUnit("盒");
                    dto.setMatDebitCredit("H");
                    dto.setMatPoId(batchChange.getGsbcVoucherId());
                    dto.setMatPoLineno(batchChange.getGsbcSerial());
                    dto.setMatDnId(batchChange.getGsbcVoucherId());
                    dto.setMatDnLineno(batchChange.getGsbcSerial());
                    dto.setMatLineRemark("");
                    dto.setMatCreateBy(inData.getSaleH().getGsshEmp());
                    dto.setMatCreateDate(currentDate);
                    dto.setMatCreateTime(CommonUtil.getHHmmss());
                    materialList.add(dto);
                    guiNoCount++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            resultMap.put("materialList", materialList);

            //会员积分增减 (增加积分，兑换积分，抵现积分)
            Map<String, Object> map = new HashMap<>(16);
            GaiaSdMemberCardData gaiaSdMemberCard = null;
            if (ObjectUtil.isNotEmpty(inData.getSaleH().getGsshHykNo())) {
                gaiaSdMemberCard = this.memberBasicMapper.findMemberNameByCardId2(inData.getSaleH().getGsshHykNo(), inData.getSaleH().getClientId(), inData.getSaleH().getGsshBrId());
                BigDecimal beforPoint = BigDecimal.ZERO;
                if (ObjectUtil.isNotEmpty(gaiaSdMemberCard)) {
                    if (StrUtil.isNotBlank(gaiaSdMemberCard.getGsmbcIntegral())) {
                        beforPoint = new BigDecimal(gaiaSdMemberCard.getGsmbcIntegral().trim());
                    }
                    //积分兑换
                    if (ObjectUtil.isNotEmpty(inData.getSaleH().getGsshIntegralExchange()) && new BigDecimal(inData.getSaleH().getGsshIntegralExchange()).compareTo(BigDecimal.ZERO) > 0) { //判断是否要扣积分 积分兑换
                        BigDecimal nowJf = beforPoint;
                        BigDecimal Exchange = new BigDecimal(inData.getSaleH().getGsshIntegralExchange()).setScale(0, RoundingMode.FLOOR); //兑换积分
                        beforPoint = beforPoint.subtract(Exchange);//当前积分 - 兑换积分
                        gaiaSdMemberCard.setGsmbcIntegral(String.valueOf(beforPoint));

                        //插入异动表记录
                        GaiaSdIntegralChange changeRecord = this.getGaiaSdIntegralChange(inData,nowJf , Exchange.negate(), "3");
                        changeRecord.setGsicResultIntegral(beforPoint);//结果积分
                        this.integralChangeMapper.insert(changeRecord);//插入积分字段待定
                    }

                    //当前抵现积分
                    BigDecimal jfdxPoint = new BigDecimal(inData.getSaleH().getGsshIntegralCash()).setScale(2, BigDecimal.ROUND_HALF_UP);
                    if (jfdxPoint.compareTo(BigDecimal.ZERO) > 0) { //判断是否要扣积分 //积分抵现
                        BigDecimal nowJf = beforPoint ;//当前积分
                        beforPoint = beforPoint.subtract(jfdxPoint).setScale(2, BigDecimal.ROUND_HALF_UP); //当前积分 - 积分抵现
                        gaiaSdMemberCard.setGsmbcIntegral(String.valueOf(beforPoint));
                        //插入异动表记录
                        GaiaSdIntegralChange changeRecord = this.getGaiaSdIntegralChange(inData, nowJf,jfdxPoint.negate(), "2");
                        changeRecord.setGsicResultIntegral(beforPoint);//结果积分
                        this.integralChangeMapper.insert(changeRecord);//插入积分字段待定
                    }

                    //增加积分
                    if (ObjectUtil.isNotEmpty(inData.getSaleH().getGsshIntegralAdd())) { //判断是否增加积分
                        BigDecimal nowJf = beforPoint ;//当前积分
                        BigDecimal integralAdd = new BigDecimal(inData.getSaleH().getGsshIntegralAdd()).setScale(2, BigDecimal.ROUND_HALF_UP); //本次增加积分
                        BigDecimal add = beforPoint.add(integralAdd);//增加积分
                        gaiaSdMemberCard.setGsmbcIntegral(String.valueOf(add));

                        GaiaSdIntegralChange changeRecord = this.getGaiaSdIntegralChange(inData, nowJf, integralAdd,"1");
                        changeRecord.setGsicResultIntegral(add);//结果积分
                        this.integralChangeMapper.insert(changeRecord);//插入积分字段待定
                    }

                    gaiaSdMemberCard.setGsmbcIntegralLastdate(DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_PATTERN));
                    this.gaiaSdMemberCardDataMapper.updateByPrimaryKeySelective(gaiaSdMemberCard);


                    map.put("CLIENT", inData.getSaleH().getClientId());
                    map.put("GSMBC_MEMBER_ID", gaiaSdMemberCard.getGsmbcMemberId());
                    map.put("GSMBC_CARD_ID", gaiaSdMemberCard.getGsmbcCardId());
                    map.put("GSMBC_BR_ID", gaiaSdMemberCard.getGsmbcBrId());
                    GaiaDataSynchronized aSynchronized = new GaiaDataSynchronized();
                    aSynchronized.setClient(inData.getSaleH().getClientId());
                    aSynchronized.setSite(gaiaSdMemberCard.getGsmbcBrId());
                    aSynchronized.setVersion(IdUtil.randomUUID());
                    aSynchronized.setTableName("GAIA_SD_MEMBER_CARD");
                    aSynchronized.setCommand("update");
                    aSynchronized.setSourceNo("会员-销售修改");
                    aSynchronized.setArg(JSON.toJSONString(map));
                    this.synchronizedService.addOne(aSynchronized);
                }
            }

            //电子券扣减
            if (CollectionUtil.isNotEmpty(inData.getSelectElectronList())) {
                List<GaiaSdElectronChange> gaiaSdElectronChangeList = new ArrayList<>();
                for (ElectronDetailOutData ecItem : inData.getSelectElectronList()) {
                    GaiaSdElectronChange gaiaSdElectronChange = new GaiaSdElectronChange();
                    gaiaSdElectronChange.setClient(ecItem.getClient());
                    gaiaSdElectronChange.setGsecId(ecItem.getGsecId());
                    gaiaSdElectronChange.setGsecMemberId(ecItem.getGsecMemberId());

                    GaiaSdElectronChange gaiaSdElectronChangeRecord = new GaiaSdElectronChange();
                    BeanUtils.copyProperties(gaiaSdElectronChange, gaiaSdElectronChangeRecord);
                    gaiaSdElectronChangeRecord.setGsecStatus("Y");
                    gaiaSdElectronChangeRecord.setGsecBillNo(inData.getSaleH().getGsshBillNo());
                    gaiaSdElectronChangeRecord.setGsecBrId(inData.getSaleH().getGsshBrId());
                    gaiaSdElectronChangeRecord.setGsecSaleDate(inData.getSaleH().getGsshDate());
                    gaiaSdElectronChangeList.add(gaiaSdElectronChangeRecord);
                }
                electronChangeMapper.updateStatus(gaiaSdElectronChangeList);
            }

            //储值卡使用扣减
            for (GaiaSdSalePayMsg salePayMsg : inData.getPayMsgList()) {
                if ("5000".equals(salePayMsg.getGsspmId()) && StringUtils.isNotEmpty(salePayMsg.getGsspmCardNo())) {
                    Example exampleRecharge = new Example(GaiaSdRechargeCard.class);
                    exampleRecharge.createCriteria().andEqualTo("clientId", salePayMsg.getClient()).andEqualTo("gsrcId", salePayMsg.getGsspmCardNo()).andEqualTo("gsrcStatus", "1");
                    GaiaSdRechargeCard rechargeCard = this.rechargeCardMapper.selectOneByExample(exampleRecharge);
                    BigDecimal gsrcAmt = rechargeCard.getGsrcAmt();//初始金额
                    BigDecimal gsspmAmt = salePayMsg.getGsspmAmt();//支付金额
                    rechargeCard.setGsrcAmt(rechargeCard.getGsrcAmt().subtract(salePayMsg.getGsspmAmt()));
                    this.rechargeCardMapper.updateByPrimaryKeySelective(rechargeCard);
                    //生成一条充值卡金额异动记录
                    GaiaSdRechargeChange gaiaSdRechargeChange = new GaiaSdRechargeChange();
                    //加盟商
                    gaiaSdRechargeChange.setClient(rechargeCard.getClientId());
                    //储值卡号
                    gaiaSdRechargeChange.setGsrcAccountId(rechargeCard.getGsrcId());
                    //异动门店
                    gaiaSdRechargeChange.setGsrcBrId(inData.getSaleH().getGsshBrId());
                    //异动单号
                    gaiaSdRechargeChange.setGsrcVoucherId(inData.getSaleH().getGsshBillNo());
                    //异动日期
                    gaiaSdRechargeChange.setGsrcDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                    //异动时间
                    gaiaSdRechargeChange.setGsrcTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
                    //异动类型1:充值，2：退款 3：消费 4：退货
                    gaiaSdRechargeChange.setGsrcType("3");
                    //储值卡初始金额
                    gaiaSdRechargeChange.setGsrcInitialAmt(gsrcAmt);
                    //储值卡异动金额
                    gaiaSdRechargeChange.setGsrcChangeAmt(gsspmAmt.negate());
                    //储值卡结果金额
                    gaiaSdRechargeChange.setGsrcResultAmt(gsrcAmt.subtract(gsspmAmt));
                    //异动人员
                    gaiaSdRechargeChange.setGsrcEmp(inData.getSaleH().getGsshEmp());

                    this.rechargeChangeMapper.insertOne(gaiaSdRechargeChange);
                    break;
                }
            }

            //  插入 零钱转入储值卡
            if (ObjectUtil.isNotEmpty(inData.getIntoMemberCardInfo())) {
                Example exampleRecharge = new Example(GaiaSdRechargeCard.class);
                exampleRecharge.createCriteria().andEqualTo("clientId", inData.getSaleH().getClientId()).andEqualTo("gsrcId", inData.getIntoMemberCardInfo().getIntoMemberCard()).andEqualTo("gsrcStatus", "1");
                GaiaSdRechargeCard intoRechargeCard = this.rechargeCardMapper.selectOneByExample(exampleRecharge);
                BigDecimal csAmt = intoRechargeCard.getGsrcAmt();//初始金额
                BigDecimal lqAmt = inData.getIntoMemberCardInfo().getIntoMemberPrice();//零钱金额
                intoRechargeCard.setGsrcAmt(csAmt.add(lqAmt));
                intoRechargeCard.setGsrcUpdateBrId(inData.getSaleH().getGsshBrId());
                intoRechargeCard.setGsrcUpdateDate(inData.getSaleH().getGsshDate());
                intoRechargeCard.setGsrcUpdateEmp(inData.getSaleH().getGsshEmp());
                this.rechargeCardMapper.updateByPrimaryKeySelective(intoRechargeCard);

                GaiaSdRechargeCardPaycheck rechargeCardPaycheck = new GaiaSdRechargeCardPaycheck();
                rechargeCardPaycheck.setClient(inData.getSaleH().getClientId());
                rechargeCardPaycheck.setGsrcpAccountId(intoRechargeCard.getGsrcAccountId());
                String gsrcpVoucher = rechargeCardPaycheckMapper.selectNextVoucherId(inData.getSaleH().getClientId());
                rechargeCardPaycheck.setGsrcpVoucherId(gsrcpVoucher);
                rechargeCardPaycheck.setGsrcpCardId(inData.getIntoMemberCardInfo().getIntoMemberCard());
                rechargeCardPaycheck.setGsrcpBrId(inData.getSaleH().getGsshBrId());
                rechargeCardPaycheck.setGsrcpDate(inData.getSaleH().getGsshDate());
                rechargeCardPaycheck.setGsrcpTime(inData.getSaleH().getGsshTime());
                rechargeCardPaycheck.setGsrcpEmp(inData.getSaleH().getGsshEmp());
                rechargeCardPaycheck.setGsrcpYsAmt(lqAmt);
                rechargeCardPaycheck.setGsrcpAfterAmt(lqAmt);
                //  默认存 2 零钱转入
                rechargeCardPaycheck.setGsrcpFlag("0");
                rechargeCardPaycheckMapper.insert(rechargeCardPaycheck);

                //生成一条充值卡金额异动记录
                GaiaSdRechargeChange intoRechargeChange = new GaiaSdRechargeChange();
                //加盟商
                intoRechargeChange.setClient(inData.getSaleH().getClientId());
                //储值卡号
                intoRechargeChange.setGsrcAccountId(inData.getIntoMemberCardInfo().getIntoMemberCard());
                //异动门店
                intoRechargeChange.setGsrcBrId(inData.getSaleH().getGsshBrId());
                //异动单号
                intoRechargeChange.setGsrcVoucherId(gsrcpVoucher);
                //异动日期
                intoRechargeChange.setGsrcDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));
                //异动时间
                intoRechargeChange.setGsrcTime(DateUtil.format(new Date(), DatePattern.PURE_TIME_PATTERN));
                //异动类型1:充值，2：退款 3：消费 4：退货
                intoRechargeChange.setGsrcType("1");
                //储值卡初始金额
                intoRechargeChange.setGsrcInitialAmt(csAmt);
                //储值卡异动金额
                intoRechargeChange.setGsrcChangeAmt(lqAmt);
                //储值卡结果金额
                intoRechargeChange.setGsrcResultAmt(csAmt.add(lqAmt));
                //异动人员
                intoRechargeChange.setGsrcEmp(inData.getSaleH().getGsshEmp());

                this.rechargeChangeMapper.insertOne(intoRechargeChange);

                GaiaSdSalePayMsg sdSalePayMsg = new GaiaSdSalePayMsg();
                sdSalePayMsg.setClient(inData.getSaleH().getClientId());
                sdSalePayMsg.setGsspmBrId(inData.getSaleH().getGsshBrId());
                sdSalePayMsg.setGsspmDate(inData.getSaleH().getGsshDate());
                sdSalePayMsg.setGsspmBillNo(gsrcpVoucher);
                sdSalePayMsg.setGsspmId("1000");
                sdSalePayMsg.setGsspmType("2");
                sdSalePayMsg.setGsspmName("现金");
                sdSalePayMsg.setGsspmCardNo(inData.getIntoMemberCardInfo().getIntoMemberCard());
                sdSalePayMsg.setGsspmAmt(lqAmt);
                sdSalePayMsg.setGsspmRmbAmt(lqAmt);
                sdSalePayMsg.setGsspmZlAmt(BigDecimal.ZERO);
                payMsgMapper.insert(sdSalePayMsg);
            }

            //老版本关联推荐
            if (CollUtil.isNotEmpty(inData.getSrRecordList())) {
                inData.getSrRecordList().forEach(record -> record.setClientId(inData.getClient()));
                this.gaiaSdSrRecordMapper.insertBatchs(inData.getSrRecordList());
            }
            if (CollUtil.isNotEmpty(inData.getSaleRInsertList())) {//
                inData.getSaleRInsertList().forEach(record -> record.setClientId(inData.getClient()));
                this.gaiaSdSrSaleRecordMapper.saleRecordInsert(inData.getSaleRInsertList());
            }

            //新版关联推荐记录
            if (CollUtil.isNotEmpty(inData.getSaleEjectList())) {
                this.relatedSaleEjectMapper.insertBatch(inData.getSaleEjectList());
            }

            //特殊分类商品记录
            if (CollUtil.isNotEmpty(inData.getSpecialClassOutDataList())){
                this.gaiaSdSrRecordMapper.batchSpecialClassPro(inData.getSpecialClassOutDataList());
            }
            //微信提醒记录
            if (ObjectUtil.isNotEmpty(gaiaSdMemberCard)) {
                List<GaiaPurchaseRemind> purchaseDataList = new ArrayList<>();
                example = new Example(GaiaProductBusiness.class);
                example.createCriteria()
                        .andEqualTo("client", inData.getSaleH().getClientId())
                        .andEqualTo("proSite", inData.getSaleH().getGsshBrId())
                        .andIn("proSelfCode", inData.getSaleDList().stream().map(GaiaSdSaleD::getGssdProId).distinct().collect(Collectors.toList()));
                List<GaiaProductBusiness> productBusinessList = this.productBusinessMapper.selectByExample(example);

                example = new Example(GaiaSdMemberBasic.class);
                example.createCriteria()
                        .andEqualTo("clientId", inData.getSaleH().getClientId())
                        .andEqualTo("gsmbMemberId", gaiaSdMemberCard.getGsmbcMemberId());
                GaiaSdMemberBasic memberBasic = this.memberBasicMapper.selectOneByExample(example);
                inData.getSaleDList().forEach(item -> {
                    GaiaProductBusiness pro = productBusinessList.stream().filter(productInfo -> item.getGssdProId().equals(productInfo.getProSelfCode())).findFirst().orElse(null);
                    if (pro != null && "1".equals(pro.getProIfSms()) && NumberUtil.isInteger(pro.getProTakeDays())) {
                        GaiaPurchaseRemind temp = new GaiaPurchaseRemind();
                        temp.setClient(inData.getSaleH().getClientId());
                        temp.setBrId(inData.getSaleH().getGsshBrId());
                        temp.setBillNo(inData.getSaleH().getGsshBillNo());
                        temp.setChTradeName(pro.getProCommonname());
                        temp.setMemberCard(inData.getSaleH().getGsshHykNo());
                        temp.setProId(pro.getProSelfCode());
                        temp.setTel(memberBasic.getGsmbMobile());
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//注意月份是MM
                        try {
                            temp.setPurchaseTime(simpleDateFormat.parse(inData.getSaleH().getGsshDate() + inData.getSaleH().getGsshTime()));
                            Calendar remindCalendar = Calendar.getInstance();
                            Date date2 = sdf.parse(inData.getSaleH().getGsshDate());
                            remindCalendar.setTime(date2);
                            remindCalendar.add(Calendar.DATE, new BigDecimal(pro.getProTakeDays()).intValue() - 2);
                            temp.setRemindDate(remindCalendar.getTime());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        purchaseDataList.add(temp);
                    }
                });
                if (CollUtil.isNotEmpty(purchaseDataList)) {
                    purchaseRemindMapper.insertList(purchaseDataList);
                }
            }
        }
        return resultMap;
    }

    /**
     * 封装参数
     * @param inData
     * @param beforPoint
     * @param s
     * @return
     */
    private GaiaSdIntegralChange getGaiaSdIntegralChange(MQSaleOrderInData inData, BigDecimal beforPoint,BigDecimal decimal, String s) {
        //插入异动表记录
        GaiaSdIntegralChange changeRecord = new GaiaSdIntegralChange();
        changeRecord.setClient(inData.getSaleH().getClientId());//异动加盟
        changeRecord.setGsicBrId(inData.getSaleH().getGsshBrId());//异动门店
        changeRecord.setGsicVoucherId(inData.getSaleH().getGsshBillNo());//异动单号
        changeRecord.setGsicFlag("1");//销售单
        changeRecord.setGsicDate(DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN));//异动日期
        changeRecord.setGsicType(s);//增加积分
        changeRecord.setGsicCardId(inData.getSaleH().getGsshHykNo());//会员卡号
        changeRecord.setGsicInitialIntegral(beforPoint);   //初始积分
        changeRecord.setGsicChangeIntegral(decimal);    //异动积分
        changeRecord.setGsicEmp(inData.getSaleH().getGsshEmp());//移动人
        return changeRecord;
    }

    @Override
    public GaiaSdSytAccount getSytAccount(String client, String brId) {
        Example example = new Example(GaiaSdSytAccount.class);
        example.createCriteria().andEqualTo("client", client).andEqualTo("brId", brId);
        GaiaSdSytAccount res = sytAccountMapper.selectOneByExample(example);
        return res;
    }

    @Override
    public MemeberJfMessageOutData memeberJfMessage(MemeberJfMessageInData inData) {
        MemeberJfMessageOutData result = new MemeberJfMessageOutData();
        MemberCardData memberCardData = memberBasicMapper.findMemberNameByCardId3(inData.getCardNo(), inData.getClientId(), inData.getBrId()); //会员积分信息
        if (ObjectUtil.isEmpty(memberCardData)) {
            result.setCode("0");
            result.setMessage("找不到会员卡信息！");
            return result;
        }
        result.setMemberName(memberCardData.getMemberName());
        IntegralServeInData integral = new IntegralServeInData();
        integral.setClientId(inData.getClientId());
        integral.setMemberCordType(memberCardData.getGsmbcClassId());
        integral.setStoCode(inData.getBrId());
        IntegralMessageOutData storeIntegralActive = integralCashSetMapper.selectActiveByStore(integral);
        if (ObjectUtil.isEmpty(storeIntegralActive)){
            result.setCode("0");
            result.setMessage("当前门店未设置积分抵用规则！");
            return result;
        }
        IntegralMessageOutData integralMessageOutData = integralCashSetMapper.selectIntegralMessage(integral);
        if (ObjectUtil.isEmpty(integralMessageOutData)){
            result.setCode("0");
            result.setMessage("该类型会员未设置积分抵用规则！");
            return result;
        }
        integralMessageOutData.setIntegral(new BigDecimal(memberCardData.getGsmbcIntegral()));
        result.setIntegralType(integralMessageOutData.getGsicsType());
        result.setUsableIntegral(new BigDecimal(memberCardData.getGsmbcIntegral()));
        //根据类型计算抵用金额
        if ("0".equals(integralMessageOutData.getGsicsType())){//单次固定
            MemeberJfMessageOutData data  =  fixedPurpose(integralMessageOutData,inData.getPro());
            result.setIntegral(data.getIntegral());
            result.setCreditUpperLimit(data.getCreditUpperLimit());
            result.setIntegralMaxAmt(data.getIntegralMaxAmt().setScale(2,BigDecimal.ROUND_HALF_UP));
            result.setIntegralRules(data.getIntegralRules());
            result.setGsicsIntegralAddSet(integralMessageOutData.getGsicsIntegralAddSet());
            result.setCode("1");
        }else if ("1".equals(integralMessageOutData.getGsicsType())){//最优
            MemeberJfMessageOutData data  =  optimalIntegral(integralMessageOutData,inData.getPro());
            result.setIntegral(data.getIntegral());
            result.setCreditUpperLimit(data.getCreditUpperLimit());
            result.setIntegralMaxAmt(data.getIntegralMaxAmt().setScale(2,BigDecimal.ROUND_HALF_UP));
            result.setIntegralRules(data.getIntegralRules());
            result.setGsicsIntegralAddSet(integralMessageOutData.getGsicsIntegralAddSet());
            result.setCode("1");
        }
        return result;
    }

    public MemeberJfMessageOutData fixedPurpose(IntegralMessageOutData integralData,List<IntegralOrderPro> proList){
        MemeberJfMessageOutData result = new MemeberJfMessageOutData();
        //查询积分抵现规则
        List<IntegralRule> integralRuleList = integralCashSetMapper.selectIntegralRuleList(integralData.getClientId(),integralData.getGsicsCode());
        if (ObjectUtil.isNotEmpty(integralRuleList)){
            result.setIntegralRules(integralRuleList);
        }
        //按积分由大到小排序
        List<IntegralRule> sortList = integralRuleList.stream().sorted(Comparator.comparing(IntegralRule::getNeedIntegral).reversed()).collect(Collectors.toList());
//        Map<String, IntegralRule>map = integralRuleList.stream().collect(Collectors.toMap(IntegralRule::getNeedIntegral, x -> x, (a, b) -> b));
        if("0".equals(integralData.getGsicsAmtLimitFlag())){//无抵用上限
            if ("0".equals(integralData.getGsicProRange())){//抵用商品范围：所有
                BigDecimal orderAmt = BigDecimal.ZERO;
                for (IntegralOrderPro pro : proList) {//获取订单金额
                    orderAmt = orderAmt.add(pro.getProPrice().multiply(pro.getProQty()));
                }
                //设置本单可抵用上限
                result.setCreditUpperLimit(orderAmt);
                //按会员积分获取最大的积分抵现规则及金额
                boolean f = true;
                for (int i = 0; i < sortList.size(); i++) {
                    IntegralRule rule = sortList.get(i);
                    if (integralData.getIntegral().compareTo(rule.getNeedIntegral()) > -1){
                        if (ObjectUtil.isNotEmpty(rule.getServeAmt())) {
                            if (orderAmt.compareTo(rule.getServeAmt()) > -1) {
                                result.setIntegralMaxAmt(rule.getServeAmt());
                                result.setIntegral(rule.getNeedIntegral());
                                f = false;
                                break;
                            }
                        }else {
                            BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                            BigDecimal ruleAmt = rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP));
                            if (orderAmt.compareTo(ruleAmt) > -1){
                                result.setIntegralMaxAmt(ruleAmt);
                                result.setIntegral(rule.getNeedIntegral());
                                f = false;
                                break;
                            }
                        }
                    }
                }
                if (f){
                    result.setIntegralMaxAmt(BigDecimal.ZERO);
                    result.setIntegral(BigDecimal.ZERO);
                }
            }else if("1".equals(integralData.getGsicProRange())){//抵用商品范围：部分
                BigDecimal orderAmt = BigDecimal.ZERO;
                List<IntegralPro> integralProList = integralCashSetMapper.selectIntegralProList(integralData.getClientId(),integralData.getGsicVoucherId());
                Map<String, IntegralPro> map = integralProList.stream().collect(Collectors.toMap(IntegralPro::getProId, x -> x, (a, b) -> b));
                //获取订单部分可积分抵现商品订单金额
                for (int i = 0; i < proList.size(); i++) {
                    IntegralOrderPro orderPro = proList.get(i);
                    IntegralPro integralPro = map.get(orderPro.getProCode());
                    if (ObjectUtil.isNotEmpty(integralPro)){
                        orderAmt = orderAmt.add(orderPro.getProPrice().multiply(orderPro.getProQty()));
                    }
                }
                result.setCreditUpperLimit(orderAmt);
                if (orderAmt.compareTo(BigDecimal.ZERO) == 0){//没有对应金额则该单不可积分抵现
                    result.setIntegralMaxAmt(BigDecimal.ZERO);
                    result.setIntegral(BigDecimal.ZERO);
                }else {
                    boolean f = true;
                    for (int i = 0; i < sortList.size(); i++) {
                        IntegralRule rule = sortList.get(i);
                        if (integralData.getIntegral().compareTo(rule.getNeedIntegral()) > -1){
                            if (ObjectUtil.isNotEmpty(rule.getServeAmt())) {
                                if (orderAmt.compareTo(rule.getServeAmt()) > -1) {
                                    result.setIntegralMaxAmt(rule.getServeAmt());
                                    result.setIntegral(rule.getNeedIntegral());
                                    f = false;
                                    break;
                                }
                            }else {
                                BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                                BigDecimal ruleAmt = rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                                if (orderAmt.compareTo(ruleAmt) > -1){
                                    result.setIntegralMaxAmt(ruleAmt);
                                    result.setIntegral(rule.getNeedIntegral());
                                    f = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (f){
                        result.setIntegralMaxAmt(BigDecimal.ZERO);
                        result.setIntegral(BigDecimal.ZERO);
                    }
                }
            }
        }else if("1".equals(integralData.getGsicsAmtLimitFlag())){//有抵用上限
            if ("0".equals(integralData.getGsicProRange())){
                BigDecimal orderAmt = BigDecimal.ZERO;
                for (IntegralOrderPro pro : proList) {
                    orderAmt = orderAmt.add(pro.getProPrice().multiply(pro.getProQty()));
                }
                //获取单笔抵用上限金额
                BigDecimal limitAmt = BigDecimal.ZERO;
                if(StringUtils.isNotEmpty(integralData.getGsicsLimitRate()) && StringUtils.isNotEmpty(integralData.getGsicsAmtOffsetMax())){
                    limitAmt = orderAmt.multiply(new BigDecimal(integralData.getGsicsLimitRate()).divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                    if(limitAmt.compareTo(new BigDecimal(integralData.getGsicsAmtOffsetMax())) > -1){
                        limitAmt = new BigDecimal(integralData.getGsicsAmtOffsetMax());
                    }
                }else{
                    if(orderAmt.compareTo(BigDecimal.ZERO) == 1) {
                        if (StringUtils.isEmpty(integralData.getGsicsLimitRate())) {
                            limitAmt = new BigDecimal(integralData.getGsicsAmtOffsetMax());
                        }
                        if (StringUtils.isEmpty(integralData.getGsicsAmtOffsetMax())) {
                            limitAmt = orderAmt.multiply(new BigDecimal(integralData.getGsicsLimitRate()).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);;
                        }
                        if (orderAmt.compareTo(limitAmt) < 1){
                            limitAmt = orderAmt;
                        }
                    }
                }
                result.setCreditUpperLimit(limitAmt);
                boolean f = true;
                for (int i = 0; i < sortList.size(); i++) {
                    IntegralRule rule = sortList.get(i);
                    if (integralData.getIntegral().compareTo(rule.getNeedIntegral()) > -1){
                        if (ObjectUtil.isNotEmpty(rule.getServeAmt())) {
                            if (limitAmt.compareTo(rule.getServeAmt()) > -1) {
                                result.setIntegralMaxAmt(rule.getServeAmt());
                                result.setIntegral(rule.getNeedIntegral());
                                f = false;
                                break;
                            }
                        }else {
                            BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                            BigDecimal ruleAmt = rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);;
                            if (limitAmt.compareTo(ruleAmt) > -1){
                                result.setIntegralMaxAmt(ruleAmt);
                                result.setIntegral(rule.getNeedIntegral());
                                f = false;
                                break;
                            }
                        }
                    }
                }
                if (f){
                    result.setIntegralMaxAmt(BigDecimal.ZERO);
                    result.setIntegral(BigDecimal.ZERO);
                }
            }else if("1".equals(integralData.getGsicProRange())){
                BigDecimal orderAmt = BigDecimal.ZERO;
                List<IntegralPro> integralProList = integralCashSetMapper.selectIntegralProList(integralData.getClientId(),integralData.getGsicVoucherId());
                Map<String, IntegralPro> map = integralProList.stream().collect(Collectors.toMap(IntegralPro::getProId, x -> x, (a, b) -> b));
                for (int i = 0; i < proList.size(); i++) {
                    IntegralOrderPro orderPro = proList.get(i);
                    IntegralPro integralPro = map.get(orderPro.getProCode());
                    if (ObjectUtil.isNotEmpty(integralPro)){
                        orderAmt = orderAmt.add(orderPro.getProPrice().multiply(orderPro.getProQty()).setScale(2,BigDecimal.ROUND_HALF_UP));
                    }
                }
                //获取单笔抵用上限金额
                BigDecimal limitAmt = BigDecimal.ZERO;
                if(StringUtils.isNotEmpty(integralData.getGsicsLimitRate()) && StringUtils.isNotEmpty(integralData.getGsicsAmtOffsetMax())){
                    limitAmt = orderAmt.multiply(new BigDecimal(integralData.getGsicsLimitRate()).divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                    if(limitAmt.compareTo(new BigDecimal(integralData.getGsicsAmtOffsetMax())) > -1){
                        limitAmt = new BigDecimal(integralData.getGsicsAmtOffsetMax());
                    }
                }else{
                    if(orderAmt.compareTo(BigDecimal.ZERO) == 1) {
                        if (StringUtils.isEmpty(integralData.getGsicsLimitRate())) {
                            limitAmt = new BigDecimal(integralData.getGsicsAmtOffsetMax());
                        }
                        if (StringUtils.isEmpty(integralData.getGsicsAmtOffsetMax())) {
                            limitAmt = orderAmt.multiply(new BigDecimal(integralData.getGsicsLimitRate()).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                        }
                        if (orderAmt.compareTo(limitAmt) < 1){
                            limitAmt = orderAmt;
                        }
                    }
                }
                result.setCreditUpperLimit(limitAmt);
                if (limitAmt.compareTo(BigDecimal.ZERO) == 0){
                    result.setIntegralMaxAmt(BigDecimal.ZERO);
                    result.setIntegral(BigDecimal.ZERO);
                }else {
                    boolean f = true;
                    for (int i = 0; i < sortList.size(); i++) {
                        IntegralRule rule = sortList.get(i);
                        if (integralData.getIntegral().compareTo(rule.getNeedIntegral()) > -1){
                            if (ObjectUtil.isNotEmpty(rule.getServeAmt())) {
                                if (limitAmt.compareTo(rule.getServeAmt()) > -1) {
                                    result.setIntegralMaxAmt(rule.getServeAmt());
                                    result.setIntegral(rule.getNeedIntegral());
                                    f = false;
                                    break;
                                }
                            }else {
                                BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                                BigDecimal ruleAmt = rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                                if (limitAmt.compareTo(ruleAmt) > -1){
                                    result.setIntegralMaxAmt(ruleAmt);
                                    result.setIntegral(rule.getNeedIntegral());
                                    f = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (f){
                        result.setIntegralMaxAmt(BigDecimal.ZERO);
                        result.setIntegral(BigDecimal.ZERO);
                    }
                }
            }
        }
        return result;
    }

    public MemeberJfMessageOutData optimalIntegral(IntegralMessageOutData integralData,List<IntegralOrderPro> proList){
        MemeberJfMessageOutData result = new MemeberJfMessageOutData();
        List<IntegralRule> integralRuleList = integralCashSetMapper.selectIntegralRuleList(integralData.getClientId(),integralData.getGsicsCode());
        if (ObjectUtil.isNotEmpty(integralRuleList)){
            result.setIntegralRules(integralRuleList);
        }
        List<IntegralRule> sortList = integralRuleList.stream().sorted(Comparator.comparing(IntegralRule::getNeedIntegral).reversed()).collect(Collectors.toList());
        if("0".equals(integralData.getGsicsAmtLimitFlag())){
            if ("0".equals(integralData.getGsicProRange())){
                BigDecimal orderAmt = BigDecimal.ZERO;
                for (IntegralOrderPro pro : proList) {
                    orderAmt = orderAmt.add(pro.getProPrice().multiply(pro.getProQty()).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                result.setCreditUpperLimit(orderAmt);
                if(sortList.size() > 1) {
                    if (ObjectUtil.isNotEmpty(sortList.get(0).getServeAmt())) {
                        Map<String, Object> integralMap = new HashMap<>();
                        for (int i = 0; i < sortList.size(); i++) {
                            IntegralRule rule = sortList.get(i);
                            BigDecimal intgralNow = BigDecimal.ZERO;
                            BigDecimal integralAmt = BigDecimal.ZERO;
                            if (i == 0) {
                                intgralNow = integralData.getIntegral();
                            } else {
                                intgralNow = new BigDecimal(integralMap.get("surplusIntegral").toString());
                                integralAmt = new BigDecimal(integralMap.get("integralAmt").toString());
                            }
                            integralMap = getAmtByIntegral(intgralNow, integralAmt, sortList);
                        }
//                BigDecimal usedIntegral = integralData.getIntegral().subtract(new BigDecimal(integralMap.get("surplusIntegral").toString()));
//                result.setIntegralMaxAmt(new BigDecimal(integralMap.get("integralAmt").toString()));
//                result.setIntegral(usedIntegral);
                        if (new BigDecimal(integralMap.get("integralAmt").toString()).compareTo(orderAmt) == 1) {
                            Map<String, Object> amtMap = new HashMap<>();
                            for (int i = 0; i < sortList.size(); i++) {
                                IntegralRule rule = sortList.get(i);
                                BigDecimal surplusAmt = BigDecimal.ZERO;
                                BigDecimal integral = BigDecimal.ZERO;
                                if (i == 0) {
                                    surplusAmt = orderAmt;
                                } else {
                                    surplusAmt = new BigDecimal(amtMap.get("surplusAmt").toString());
                                    integral = new BigDecimal(amtMap.get("integral").toString());
                                }
                                amtMap = getIntegralByAmt(surplusAmt, integral, sortList);
                            }
                            BigDecimal usedAmt = orderAmt.subtract(new BigDecimal(amtMap.get("surplusAmt").toString()));
                            result.setIntegralMaxAmt(usedAmt);
                            result.setIntegral(new BigDecimal(amtMap.get("integral").toString()));
                        } else {
                            BigDecimal usedIntegral = integralData.getIntegral().subtract(new BigDecimal(integralMap.get("surplusIntegral").toString()));
                            result.setIntegralMaxAmt(new BigDecimal(integralMap.get("integralAmt").toString()));
                            result.setIntegral(usedIntegral);
                        }
                    }else if (ObjectUtil.isNotEmpty(sortList.get(0).getServeRate())){
                        BigDecimal integralMaxAmt = BigDecimal.ZERO;
                        BigDecimal usedIntegral = BigDecimal.ZERO;
                        BigDecimal rate = BigDecimal.ZERO;
                        boolean f = false;
                        for (int i = 0; i < sortList.size(); i++) {
                            IntegralRule rule = sortList.get(i);
                            if (integralData.getIntegral().compareTo(rule.getNeedIntegral()) > -1){
                                rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                                integralMaxAmt = integralData.getIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                                usedIntegral = integralData.getIntegral();
                                f = true;
                                break;
                            }
                        }
                        if (f){
                            if (integralMaxAmt.compareTo(orderAmt) == 1){
                                usedIntegral = orderAmt.divide(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP),2,BigDecimal.ROUND_HALF_UP);
                                integralMaxAmt = orderAmt;
                            }
                        }
                        result.setIntegralMaxAmt(integralMaxAmt);
                        result.setIntegral(usedIntegral);
                    }
                }else {
                    //获取会员积分能抵现金额
                    for (int i = 0; i < sortList.size(); i++) {
                        IntegralRule rule = sortList.get(i);
                        BigDecimal a = integralData.getIntegral().divide(rule.getNeedIntegral(), 0, BigDecimal.ROUND_DOWN);//积分对应的倍数
                        if (ObjectUtil.isNotEmpty(rule.getServeAmt())){
                            BigDecimal integralAmt = rule.getServeAmt().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                            BigDecimal usedIntegral = BigDecimal.ZERO;
                            if (integralAmt.compareTo(orderAmt) == 1){//如果抵扣金额大于订单金额
                                BigDecimal b = orderAmt.divide(rule.getServeAmt(), 0, BigDecimal.ROUND_DOWN);
                                usedIntegral = rule.getNeedIntegral().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP);
                                result.setIntegralMaxAmt(rule.getServeAmt().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP));
                                result.setIntegral(usedIntegral);
                            }else {
                                usedIntegral = rule.getNeedIntegral().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                                result.setIntegralMaxAmt(integralAmt);
                                result.setIntegral(usedIntegral);
                            }
                        }
                        if (ObjectUtil.isNotEmpty(rule.getServeRate())){
                            BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                            BigDecimal integralAmt = rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                            if (integralAmt.compareTo(orderAmt) == 1) {//如果抵扣金额大于订单金额
                                BigDecimal b = orderAmt.divide(rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP),2, BigDecimal.ROUND_HALF_UP);
                                result.setIntegralMaxAmt(orderAmt);
                                result.setIntegral(rule.getNeedIntegral().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP));
                            }else {
                                result.setIntegralMaxAmt(integralAmt);
                                result.setIntegral(rule.getNeedIntegral().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP));
                            }
                        }
                    }
                }
            }else if("1".equals(integralData.getGsicProRange())){
                BigDecimal orderAmt = BigDecimal.ZERO;
                List<IntegralPro> integralProList = integralCashSetMapper.selectIntegralProList(integralData.getClientId(),integralData.getGsicVoucherId());
                Map<String, IntegralPro> map = integralProList.stream().collect(Collectors.toMap(IntegralPro::getProId, x -> x, (a, b) -> b));
                for (int i = 0; i < proList.size(); i++) {
                    IntegralOrderPro orderPro = proList.get(i);
                    IntegralPro integralPro = map.get(orderPro.getProCode());
                    if (ObjectUtil.isNotEmpty(integralPro)){
                        orderAmt = orderAmt.add(orderPro.getProPrice().multiply(orderPro.getProQty()).setScale(2,BigDecimal.ROUND_HALF_UP));
                    }
                }
                result.setCreditUpperLimit(orderAmt);
                if (orderAmt.compareTo(BigDecimal.ZERO) == 0){
                    result.setIntegralMaxAmt(BigDecimal.ZERO);
                    result.setIntegral(BigDecimal.ZERO);
                }else {
                    if (sortList.size() > 1) {
                        if (ObjectUtil.isNotEmpty(sortList.get(0).getServeAmt())) {
                            Map<String, Object> integralMap = new HashMap<>();
                            for (int i = 0; i < sortList.size(); i++) {
                                IntegralRule rule = sortList.get(i);
                                BigDecimal intgralNow = BigDecimal.ZERO;
                                BigDecimal integralAmt = BigDecimal.ZERO;
                                if (i == 0) {
                                    intgralNow = integralData.getIntegral();
                                } else {
                                    intgralNow = new BigDecimal(integralMap.get("surplusIntegral").toString());
                                    integralAmt = new BigDecimal(integralMap.get("integralAmt").toString());
                                }
                                integralMap = getAmtByIntegral(intgralNow, integralAmt, sortList);
                            }
//                    BigDecimal usedIntegral = integralData.getIntegral().subtract(new BigDecimal(integralMap.get("surplusIntegral").toString()));
//                    result.setIntegralMaxAmt(new BigDecimal(integralMap.get("integralAmt").toString()));
//                    result.setIntegral(usedIntegral);
                            if (new BigDecimal(integralMap.get("integralAmt").toString()).compareTo(orderAmt) == 1) {
                                Map<String, Object> amtMap = new HashMap<>();
                                for (int i = 0; i < sortList.size(); i++) {
                                    IntegralRule rule = sortList.get(i);
                                    BigDecimal surplusAmt = BigDecimal.ZERO;
                                    BigDecimal integral = BigDecimal.ZERO;
                                    if (i == 0) {
                                        surplusAmt = orderAmt;
                                    } else {
                                        surplusAmt = new BigDecimal(amtMap.get("surplusAmt").toString());
                                        integral = new BigDecimal(amtMap.get("integral").toString());
                                    }
                                    amtMap = getIntegralByAmt(surplusAmt, integral, sortList);
                                }
                                BigDecimal usedAmt = orderAmt.subtract(new BigDecimal(amtMap.get("surplusAmt").toString()));
                                result.setIntegralMaxAmt(usedAmt);
                                result.setIntegral(new BigDecimal(amtMap.get("integral").toString()));
                            } else {
                                BigDecimal usedIntegral = integralData.getIntegral().subtract(new BigDecimal(integralMap.get("surplusIntegral").toString()));
                                result.setIntegralMaxAmt(new BigDecimal(integralMap.get("integralAmt").toString()));
                                result.setIntegral(usedIntegral);
                            }
                        }else if (ObjectUtil.isNotEmpty(sortList.get(0).getServeRate())){
                            BigDecimal integralMaxAmt = BigDecimal.ZERO;
                            BigDecimal usedIntegral = BigDecimal.ZERO;
                            BigDecimal rate = BigDecimal.ZERO;
                            boolean f = false;
                            for (int i = 0; i < sortList.size(); i++) {
                                IntegralRule rule = sortList.get(i);
                                if (integralData.getIntegral().compareTo(rule.getNeedIntegral()) > -1){
                                    rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                                    integralMaxAmt = integralData.getIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                                    usedIntegral = integralData.getIntegral();
                                    f = true;
                                    break;
                                }
                            }
                            if (f){
                                if (integralMaxAmt.compareTo(orderAmt) == 1){
                                    usedIntegral = orderAmt.divide(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP),2,BigDecimal.ROUND_HALF_UP);
                                    integralMaxAmt = orderAmt;
                                }
                            }
                            result.setIntegralMaxAmt(integralMaxAmt);
                            result.setIntegral(usedIntegral);
                        }
                    }else {
                        //获取会员积分能抵现金额
                        for (int i = 0; i < sortList.size(); i++) {
                            IntegralRule rule = sortList.get(i);
                            BigDecimal a = integralData.getIntegral().divide(rule.getNeedIntegral(), 0, BigDecimal.ROUND_DOWN);//积分对应的倍数
                            if (ObjectUtil.isNotEmpty(rule.getServeAmt())){
                                BigDecimal integralAmt = rule.getServeAmt().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                                BigDecimal usedIntegral = BigDecimal.ZERO;
                                if (integralAmt.compareTo(orderAmt) == 1){//如果抵扣金额大于订单金额
                                    BigDecimal b = orderAmt.divide(rule.getServeAmt(), 0, BigDecimal.ROUND_DOWN);
                                    usedIntegral = rule.getNeedIntegral().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP);
                                    result.setIntegralMaxAmt(rule.getServeAmt().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP));
                                    result.setIntegral(usedIntegral);
                                }else {
                                    usedIntegral = rule.getNeedIntegral().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                                    result.setIntegralMaxAmt(orderAmt);
                                    result.setIntegral(usedIntegral);
                                }
                            }
                            if (ObjectUtil.isNotEmpty(rule.getServeRate())){
                                BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                                BigDecimal integralAmt = rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                                if (integralAmt.compareTo(orderAmt) == 1) {//如果抵扣金额大于订单金额
                                    BigDecimal b = orderAmt.divide(rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP),2, BigDecimal.ROUND_HALF_UP);
                                    result.setIntegralMaxAmt(orderAmt);
                                    result.setIntegral(rule.getNeedIntegral().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP));
                                }else {
                                    result.setIntegralMaxAmt(integralAmt);
                                    result.setIntegral(rule.getNeedIntegral().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP));
                                }
                            }
                        }
                    }
                }
            }
        }else if("1".equals(integralData.getGsicsAmtLimitFlag())){
            if ("0".equals(integralData.getGsicProRange())){
                BigDecimal orderAmt = BigDecimal.ZERO;
                for (IntegralOrderPro pro : proList) {
                    orderAmt = orderAmt.add(pro.getProPrice().multiply(pro.getProQty()).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                //获取单笔抵用上限金额
                BigDecimal limitAmt = BigDecimal.ZERO;
                if(StringUtils.isNotEmpty(integralData.getGsicsLimitRate()) && StringUtils.isNotEmpty(integralData.getGsicsAmtOffsetMax())){
                    limitAmt = orderAmt.multiply(new BigDecimal(integralData.getGsicsLimitRate()).divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                    if(limitAmt.compareTo(new BigDecimal(integralData.getGsicsAmtOffsetMax())) > -1){
                        limitAmt = new BigDecimal(integralData.getGsicsAmtOffsetMax());
                    }
                }else{
                    if(orderAmt.compareTo(BigDecimal.ZERO) == 1) {
                        if (StringUtils.isEmpty(integralData.getGsicsLimitRate())) {
                            limitAmt = new BigDecimal(integralData.getGsicsAmtOffsetMax());
                        }
                        if (StringUtils.isEmpty(integralData.getGsicsAmtOffsetMax())) {
                            limitAmt = orderAmt.multiply(new BigDecimal(integralData.getGsicsLimitRate()).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                        }
                        if (orderAmt.compareTo(limitAmt) < 1){
                            limitAmt = orderAmt;
                        }
                    }
                }
                result.setCreditUpperLimit(limitAmt);
                if (sortList.size() > 1) {
                    //获取最低抵现规则
                    IntegralRule minRule = sortList.get(sortList.size() - 1);
                    BigDecimal minRuleAmt = BigDecimal.ZERO;
                    if (ObjectUtil.isNotEmpty(minRule.getServeRate())) {
                        BigDecimal rate = new BigDecimal(minRule.getServeRate().split("%")[0]);
                        minRuleAmt = minRule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
                    } else if (ObjectUtil.isNotEmpty(minRule.getServeAmt())) {
                        minRuleAmt = minRule.getServeAmt();
                    }
                    if (minRuleAmt.compareTo(limitAmt) == 1){
                        result.setIntegralMaxAmt(BigDecimal.ZERO);
                        result.setIntegral(BigDecimal.ZERO);
                    }else {
                        if (ObjectUtil.isNotEmpty(sortList.get(0).getServeAmt())) {
                            if (ObjectUtil.isNotEmpty(sortList.get(0).getServeAmt())) {
                                Map<String, Object> integralMap = new HashMap<>();
                                for (int i = 0; i < sortList.size(); i++) {
                                    IntegralRule rule = sortList.get(i);
                                    BigDecimal intgralNow = BigDecimal.ZERO;
                                    BigDecimal integralAmt = BigDecimal.ZERO;
                                    if (i == 0) {
                                        intgralNow = integralData.getIntegral();
                                    } else {
                                        intgralNow = new BigDecimal(integralMap.get("surplusIntegral").toString());
                                        integralAmt = new BigDecimal(integralMap.get("integralAmt").toString());
                                    }
                                    integralMap = getAmtByIntegral(intgralNow, integralAmt, sortList);
                                }
    //                BigDecimal usedIntegral = integralData.getIntegral().subtract(new BigDecimal(integralMap.get("surplusIntegral").toString()));
    //                result.setIntegralMaxAmt(new BigDecimal(integralMap.get("integralAmt").toString()));
    //                result.setIntegral(usedIntegral);
                                if (new BigDecimal(integralMap.get("integralAmt").toString()).compareTo(limitAmt) == 1) {
                                    Map<String, Object> amtMap = new HashMap<>();
                                    for (int i = 0; i < sortList.size(); i++) {
                                        IntegralRule rule = sortList.get(i);
                                        BigDecimal surplusAmt = BigDecimal.ZERO;
                                        BigDecimal integral = BigDecimal.ZERO;
                                        if (i == 0) {
                                            surplusAmt = limitAmt;
                                        } else {
                                            surplusAmt = new BigDecimal(amtMap.get("surplusAmt").toString());
                                            integral = new BigDecimal(amtMap.get("integral").toString());
                                        }
                                        amtMap = getIntegralByAmt(surplusAmt, integral, sortList);
                                    }
                                    BigDecimal usedAmt = limitAmt.subtract(new BigDecimal(amtMap.get("surplusAmt").toString()));
                                    result.setIntegralMaxAmt(usedAmt);
                                    result.setIntegral(new BigDecimal(amtMap.get("integral").toString()));
                                } else {
                                    BigDecimal usedIntegral = integralData.getIntegral().subtract(new BigDecimal(integralMap.get("surplusIntegral").toString()));
                                    result.setIntegralMaxAmt(new BigDecimal(integralMap.get("integralAmt").toString()));
                                    result.setIntegral(usedIntegral);
                                }
                            }
                        } else if (ObjectUtil.isNotEmpty(sortList.get(0).getServeRate())) {
                            BigDecimal integralMaxAmt = BigDecimal.ZERO;
                            BigDecimal usedIntegral = BigDecimal.ZERO;
                            BigDecimal rate = BigDecimal.ZERO;
                            boolean f = false;
                            for (int i = 0; i < sortList.size(); i++) {
                                IntegralRule rule = sortList.get(i);
                                if (integralData.getIntegral().compareTo(rule.getNeedIntegral()) > -1) {
                                    rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                                    integralMaxAmt = integralData.getIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
                                    usedIntegral = integralData.getIntegral();
                                    f = true;
                                    break;
                                }
                            }
                            if (f) {
                                if (integralMaxAmt.compareTo(limitAmt) == 1) {
                                    usedIntegral = limitAmt.divide(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP), 2, BigDecimal.ROUND_HALF_UP);
                                    integralMaxAmt = limitAmt;
                                }
                            }
                            result.setIntegralMaxAmt(integralMaxAmt);
                            result.setIntegral(usedIntegral);
                        }
                    }
                }else {
                    //获取会员积分能抵现金额
                    for (int i = 0; i < sortList.size(); i++) {
                        IntegralRule rule = sortList.get(i);
                        BigDecimal a = integralData.getIntegral().divide(rule.getNeedIntegral(), 0, BigDecimal.ROUND_DOWN);//积分对应的倍数
                        if (ObjectUtil.isNotEmpty(rule.getServeAmt())){
                            BigDecimal integralAmt = rule.getServeAmt().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                            BigDecimal usedIntegral = BigDecimal.ZERO;
                            if (integralAmt.compareTo(limitAmt) == 1){//如果抵扣金额大于订单金额
                                BigDecimal b = limitAmt.divide(rule.getServeAmt(), 0, BigDecimal.ROUND_DOWN);
                                usedIntegral = rule.getNeedIntegral().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP);
                                result.setIntegralMaxAmt(rule.getServeAmt().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP));
                                result.setIntegral(usedIntegral);
                            }else {
                                usedIntegral = rule.getNeedIntegral().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                                result.setIntegralMaxAmt(integralAmt);
                                result.setIntegral(usedIntegral);
                            }
                        }
                        if (ObjectUtil.isNotEmpty(rule.getServeRate())){
                            BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                            BigDecimal integralAmt = rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                            if (integralAmt.compareTo(limitAmt) == 1) {//如果抵扣金额大于订单金额
                                BigDecimal b = limitAmt.divide(rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP),2, BigDecimal.ROUND_HALF_UP);
                                result.setIntegralMaxAmt(limitAmt);
                                result.setIntegral(rule.getNeedIntegral().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP));
                            }else {
                                result.setIntegralMaxAmt(integralAmt);
                                result.setIntegral(rule.getNeedIntegral().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP));
                            }
                        }
                    }
                }
            }else if("1".equals(integralData.getGsicProRange())){
                BigDecimal orderAmt = BigDecimal.ZERO;
                List<IntegralPro> integralProList = integralCashSetMapper.selectIntegralProList(integralData.getClientId(),integralData.getGsicVoucherId());
                Map<String, IntegralPro> map = integralProList.stream().collect(Collectors.toMap(IntegralPro::getProId, x -> x, (a, b) -> b));
                for (int i = 0; i < proList.size(); i++) {
                    IntegralOrderPro orderPro = proList.get(i);
                    IntegralPro integralPro = map.get(orderPro.getProCode());
                    if (ObjectUtil.isNotEmpty(integralPro)){
                        orderAmt = orderAmt.add(orderPro.getProPrice().multiply(orderPro.getProQty()).setScale(2,BigDecimal.ROUND_HALF_UP));
                    }
                }
                //获取单笔抵用上限金额
                BigDecimal limitAmt = BigDecimal.ZERO;
                if(StringUtils.isNotEmpty(integralData.getGsicsLimitRate()) && StringUtils.isNotEmpty(integralData.getGsicsAmtOffsetMax())){
                    limitAmt = orderAmt.multiply(new BigDecimal(integralData.getGsicsLimitRate()).divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                    if(limitAmt.compareTo(new BigDecimal(integralData.getGsicsAmtOffsetMax())) > -1){
                        limitAmt = new BigDecimal(integralData.getGsicsAmtOffsetMax());
                    }
                }else{
                    if(orderAmt.compareTo(BigDecimal.ZERO) == 1) {
                        if (StringUtils.isEmpty(integralData.getGsicsLimitRate())) {
                            limitAmt = new BigDecimal(integralData.getGsicsAmtOffsetMax());
                        }
                        if (StringUtils.isEmpty(integralData.getGsicsAmtOffsetMax())) {
                            limitAmt = orderAmt.multiply(new BigDecimal(integralData.getGsicsLimitRate()).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                        }
                        if (orderAmt.compareTo(limitAmt) < 1){
                            limitAmt = orderAmt;
                        }
                    }
                }
                result.setCreditUpperLimit(limitAmt);
                if (limitAmt.compareTo(BigDecimal.ZERO) == 0){
                    result.setIntegralMaxAmt(BigDecimal.ZERO);
                    result.setIntegral(BigDecimal.ZERO);
                }else {
                    if (sortList.size() > 1) {
                        if (ObjectUtil.isNotEmpty(sortList.get(0).getServeAmt())) {
                            Map<String, Object> integralMap = new HashMap<>();
                            for (int i = 0; i < sortList.size(); i++) {
                                IntegralRule rule = sortList.get(i);
                                BigDecimal intgralNow = BigDecimal.ZERO;
                                BigDecimal integralAmt = BigDecimal.ZERO;
                                if (i == 0) {
                                    intgralNow = integralData.getIntegral();
                                } else {
                                    intgralNow = new BigDecimal(integralMap.get("surplusIntegral").toString());
                                    integralAmt = new BigDecimal(integralMap.get("integralAmt").toString());
                                }
                                integralMap = getAmtByIntegral(intgralNow, integralAmt, sortList);
                            }
//                    BigDecimal usedIntegral = integralData.getIntegral().subtract(new BigDecimal(integralMap.get("surplusIntegral").toString()));
//                    result.setIntegralMaxAmt(new BigDecimal(integralMap.get("integralAmt").toString()));
//                    result.setIntegral(usedIntegral);
                            if (new BigDecimal(integralMap.get("integralAmt").toString()).compareTo(limitAmt) == 1) {
                                Map<String, Object> amtMap = new HashMap<>();
                                for (int i = 0; i < sortList.size(); i++) {
                                    IntegralRule rule = sortList.get(i);
                                    BigDecimal surplusAmt = BigDecimal.ZERO;
                                    BigDecimal usedIntegral = BigDecimal.ZERO;
                                    if (i == 0) {
                                        surplusAmt = orderAmt;
                                    } else {
                                        surplusAmt = new BigDecimal(amtMap.get("surplusAmt").toString());
                                        usedIntegral = new BigDecimal(amtMap.get("integral").toString());
                                    }
                                    amtMap = getIntegralByAmt(surplusAmt, usedIntegral, sortList);
                                }
                                BigDecimal usedAmt = limitAmt.subtract(new BigDecimal(amtMap.get("surplusAmt").toString()));
                                result.setIntegralMaxAmt(usedAmt);
                                result.setIntegral(new BigDecimal(amtMap.get("integral").toString()));
                            } else {
                                BigDecimal usedIntegral = integralData.getIntegral().subtract(new BigDecimal(integralMap.get("surplusIntegral").toString()));
                                result.setIntegralMaxAmt(new BigDecimal(integralMap.get("integralAmt").toString()));
                                result.setIntegral(usedIntegral);
                            }
                        }else if (ObjectUtil.isNotEmpty(sortList.get(0).getServeRate())){
                            BigDecimal integralMaxAmt = BigDecimal.ZERO;
                            BigDecimal usedIntegral = BigDecimal.ZERO;
                            BigDecimal rate = BigDecimal.ZERO;
                            boolean f = false;
                            for (int i = 0; i < sortList.size(); i++) {
                                IntegralRule rule = sortList.get(i);
                                if (integralData.getIntegral().compareTo(rule.getNeedIntegral()) > -1){
                                    rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                                    integralMaxAmt = integralData.getIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP);
                                    usedIntegral = integralData.getIntegral();
                                    f = true;
                                    break;
                                }
                            }
                            if (f){
                                if (integralMaxAmt.compareTo(limitAmt) == 1){
                                    usedIntegral = limitAmt.divide(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP),2,BigDecimal.ROUND_HALF_UP);
                                    integralMaxAmt = limitAmt;
                                }
                            }
                            result.setIntegralMaxAmt(integralMaxAmt);
                            result.setIntegral(usedIntegral);
                        }
                    }else {
                        //获取会员积分能抵现金额
                        for (int i = 0; i < sortList.size(); i++) {
                            IntegralRule rule = sortList.get(i);
                            BigDecimal a = integralData.getIntegral().divide(rule.getNeedIntegral(), 0, BigDecimal.ROUND_DOWN);//积分对应的倍数
                            if (ObjectUtil.isNotEmpty(rule.getServeAmt())){
                                BigDecimal integralAmt = rule.getServeAmt().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                                BigDecimal usedIntegral = BigDecimal.ZERO;
                                if (integralAmt.compareTo(limitAmt) == 1){//如果抵扣金额大于订单金额
                                    BigDecimal b = limitAmt.divide(rule.getServeAmt(), 0, BigDecimal.ROUND_DOWN);
                                    usedIntegral = rule.getNeedIntegral().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP);
                                    result.setIntegralMaxAmt(rule.getServeAmt().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP));
                                    result.setIntegral(usedIntegral);
                                }else {
                                    usedIntegral = rule.getNeedIntegral().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                                    result.setIntegralMaxAmt(integralAmt);
                                    result.setIntegral(usedIntegral);
                                }
                            }
                            if (ObjectUtil.isNotEmpty(rule.getServeRate())){
                                BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
                                BigDecimal integralAmt = rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP);
                                if (integralAmt.compareTo(limitAmt) == 1) {//如果抵扣金额大于订单金额
                                    BigDecimal b = limitAmt.divide(rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP),2, BigDecimal.ROUND_HALF_UP);
                                    result.setIntegralMaxAmt(limitAmt);
                                    result.setIntegral(rule.getNeedIntegral().multiply(b).setScale(2,BigDecimal.ROUND_HALF_UP));
                                }else {
                                    result.setIntegralMaxAmt(integralAmt);
                                    result.setIntegral(rule.getNeedIntegral().multiply(a).setScale(2,BigDecimal.ROUND_HALF_UP));
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    //根据会员积分获取最优抵现金额
    public static Map<String,Object> getAmtByIntegral(BigDecimal integral,BigDecimal integralAmt,List<IntegralRule> integralRuleList){
        Map<String,Object> result = new HashMap<>();
//        BigDecimal integralAmt = BigDecimal.ZERO;
        BigDecimal surplusIntegral = BigDecimal.ZERO;
        boolean f = false;
        for (int i = 0; i < integralRuleList.size(); i++) {
            IntegralRule rule = integralRuleList.get(i);
            if (integral.divide(rule.getNeedIntegral(),4,BigDecimal.ROUND_HALF_UP).compareTo(new BigDecimal(1))  > -1 ){
//                if(ObjectUtil.isNotEmpty(rule.getServeAmt())) {
                    integralAmt = integralAmt.add(integral.divide(rule.getNeedIntegral(), 0, BigDecimal.ROUND_DOWN).multiply(rule.getServeAmt()).setScale(2,BigDecimal.ROUND_HALF_UP));
//                }else if (ObjectUtil.isNotEmpty(rule.getServeRate())){
//                    BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
//                    integralAmt = integralAmt.add(rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP)).multiply(integral.divide(rule.getNeedIntegral(), 0, BigDecimal.ROUND_DOWN)));
//                }
//                usedIntegral = usedIntegral.add(rule.getNeedIntegral().multiply(integral.divide(rule.getNeedIntegral(), 0, BigDecimal.ROUND_HALF_DOWN)));
                surplusIntegral = integral.subtract(rule.getNeedIntegral().multiply(integral.divide(rule.getNeedIntegral(), 0, BigDecimal.ROUND_DOWN)).setScale(2,BigDecimal.ROUND_HALF_UP));
                f = true;
                break;
            }
        }
        if (!f) {
            surplusIntegral = integral;
        }
        result.put("integralAmt",integralAmt);
        result.put("surplusIntegral",surplusIntegral);
        return result;
    }

    //根据订单金额获取最优可用积分
    public static Map<String,Object> getIntegralByAmt(BigDecimal orderAmt,BigDecimal integral,List<IntegralRule> sortList){
        Map<String,Object> result = new HashMap<>();
//        BigDecimal integral = BigDecimal.ZERO;
        boolean f = false;
        BigDecimal surplusAmt = BigDecimal.ZERO;
        for (int i = 0; i < sortList.size(); i++) {
            IntegralRule rule = sortList.get(i);
            BigDecimal ruleAmt = BigDecimal.ZERO;
//            if(ObjectUtil.isNotEmpty(rule.getServeAmt())) {
                ruleAmt = rule.getServeAmt();
//            }else if (ObjectUtil.isNotEmpty(rule.getServeRate())){
//                BigDecimal rate = new BigDecimal(rule.getServeRate().split("%")[0]);
//                ruleAmt = rule.getNeedIntegral().multiply(rate.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
//            }
            if (orderAmt.divide(ruleAmt,4,BigDecimal.ROUND_HALF_UP).compareTo(new BigDecimal(1))  > -1 ){
                integral = integral.add(rule.getNeedIntegral().multiply(orderAmt.divide(ruleAmt,0,BigDecimal.ROUND_DOWN)).setScale(2,BigDecimal.ROUND_HALF_UP));
                surplusAmt = orderAmt.subtract(ruleAmt.multiply(orderAmt.divide(ruleAmt,0,BigDecimal.ROUND_DOWN)).setScale(2,BigDecimal.ROUND_HALF_UP));
                f = true;
                break;
            }
        }
        if (!f){
            surplusAmt = orderAmt;
        }
        result.put("integral",integral);
        result.put("surplusAmt",surplusAmt);
        return result;
    }

    @Override
    public int checkClinicalSheet(String client, String brId, FxBillInfoForm fxBillInfoForm) {
        GaiaSdSaleHandH saleHandH = gaiaSdSaleHandHMapper.getSaleHByBillNo(fxBillInfoForm.getBillNo(),client,brId);
        if(saleHandH == null) {
            throw new BusinessException("门诊单为空！");
        }
        List<GaiaSdSaleHandD> handDList = gaiaSdSaleHandDMapper.getAllByBillNo(client,brId,fxBillInfoForm.getBillNo());
        if(CollUtil.isEmpty(handDList)) {
            throw new BusinessException("门诊单明细为空！");
        }
        //请求验证his单据和当前订单是否一致
        Map<String, Object> params = new HashMap();
        params.put("dsfOrderCode", saleHandH.getGsshRegisterVoucherId());
        List<OrderDetailForm> orderDetailList = new ArrayList<>();
        handDList.forEach(handD -> {
            OrderDetailForm item = new OrderDetailForm();
            item.setClinicalSheetSn(handD.getClinicalSheetSn());
            item.setBatchNo(handD.getGssdBatchNo());
            item.setProCode(handD.getGssdProId());
            item.setProPrice(handD.getGssdPrc1());
            item.setProPayPrice(handD.getGssdPrc2());
            item.setQuantity(new BigDecimal(handD.getGssdQty()));
            item.setReduceAmount(handD.getGssdZkAmt());
            item.setProAmount(handD.getGssdAmt());
            orderDetailList.add(item);
        });
        params.put("orderDetailList", orderDetailList);
        String result;
        try{
            log.info("HIS异动查询接口：" + JSON.toJSONString(params));
            result = HttpRequestUtil.post(HIS_URL + payIsChangeRoute, params, MediaType.APPLICATION_JSON);
            log.info("HIS异动查询接口：" + result);
            JSONObject object = JSONObject.parseObject(result);
            if (!"0".equals(object.getString("code"))) {
                throw new BusinessException("调用HIS异动查询接口失败："+object.getString("message"));
            }
            if(ObjectUtil.isNotEmpty(object.getString("data"))) {
                AddOrderForm data = JSON.parseObject(object.getString("data"), AddOrderForm.class);
                saleHandH.setGsshYsAmt(data.getPaymentAmount());
                saleHandH.setGsshZkAmt(data.getReduceAmount());
                saleHandH.setGsshNormalAmt(data.getOrderAmount());
                gaiaSdSaleHandHMapper.update(saleHandH);

                gaiaSdSaleHandDMapper.delete(client,brId,fxBillInfoForm.getBillNo());

                gaiaSdSaleHandDMapper.insertList(transformHandDList(saleHandH,data));
                return 0;
            }
        } catch (Exception ex) {
            throw new BusinessException("调用HIS异动查询接口失败："+ex);
        }
        return 1;
    }

    @Override
    public boolean hisPayCallback(GaiaSdSaleH saleH, List<GaiaSdSaleD> saleDList) {

        //请求验证his单据和当前订单是否一致
        Map<String, Object> params = new HashMap();
        params.put("dsfOrderCode", saleH.getGsshRegisterVoucherId());

        List<OrderDetailForm> orderDetailList = new ArrayList<>();
        BigDecimal orderAmount = BigDecimal.ZERO;
        BigDecimal reduceAmount = BigDecimal.ZERO;
        BigDecimal paymentAmount = BigDecimal.ZERO;
        for(GaiaSdSaleD handD : saleDList){
            OrderDetailForm item = new OrderDetailForm();
            item.setClinicalSheetSn(handD.getClinicalSheetSn());
            item.setBatchNo(handD.getGssdBatchNo());
            item.setProCode(handD.getGssdProId());
            item.setProPrice(handD.getGssdPrc1());
            item.setProPayPrice(handD.getGssdPrc2());
            item.setQuantity(new BigDecimal(handD.getGssdQty()));
            item.setReduceAmount(handD.getGssdZkAmt());
            item.setProAmount(handD.getGssdAmt());
            orderDetailList.add(item);
            orderAmount = orderAmount.add(item.getProAmount());
            reduceAmount = reduceAmount.add(item.getReduceAmount());
            paymentAmount = paymentAmount.add(item.getProAmount());
        }

        params.put("orderDetailList", orderDetailList);
        params.put("orderAmount",orderAmount);
        params.put("reduceAmount",reduceAmount);
        params.put("paymentAmount",paymentAmount);
        params.put("payStatus",1);
        String result;
        log.info("his订单支付："+JSON.toJSONString(params));
        result = HttpRequestUtil.post(HIS_URL + payCallbackRoute, params, MediaType.APPLICATION_JSON);
        log.info("his订单支付："+result);
        return true;
    }

    @Override
    public boolean hisPayRefund(GaiaSdSaleH saleH, List<GaiaSdSaleD> saleDList) {

        //请求验证his单据和当前订单是否一致
        Map<String, Object> params = new HashMap();
        params.put("dsfOrderCode", saleH.getGsshRegisterVoucherId());
        List<OrderDetailForm> orderDetailList = new ArrayList<>();
        saleDList.forEach(handD -> {
            OrderDetailForm item = new OrderDetailForm();
            item.setClinicalSheetSn(handD.getClinicalSheetSn());
            item.setBatchNo(handD.getGssdBatchNo());
            item.setProCode(handD.getGssdProId());
            item.setProPrice(handD.getGssdPrc1());
            item.setProPayPrice(handD.getGssdPrc2());
            item.setQuantity(new BigDecimal(handD.getGssdQty()));
            item.setReduceAmount(handD.getGssdZkAmt());
            item.setProAmount(handD.getGssdAmt());
            orderDetailList.add(item);
        });
        params.put("orderDetailList", orderDetailList);
        String result;
        log.info("his订单退货："+JSON.toJSONString(params));
        result = HttpRequestUtil.post(HIS_URL + payRefundRoute, params, MediaType.APPLICATION_JSON);
        log.info("his订单退货："+result);
        return true;
    }

    private List<GaiaSdSaleHandD> transformHandDList(GaiaSdSaleHandH saleHandH,AddOrderForm data) {
        List<GaiaSdSaleHandD> list = new ArrayList<>();
        if (data.getOrderDetailList() != null && data.getOrderDetailList().size() > 0) {
            int i = 1;
            for (OrderDetailForm orderDetail : data.getOrderDetailList()) {
                GaiaSdSaleHandD gaiaSdSaleHandD = new GaiaSdSaleHandD();

                //移动平均价
                Example productExample = new Example(GaiaProductBusiness.class);
                productExample.createCriteria().andEqualTo("client", saleHandH.getClientId()).andEqualTo("proSite", saleHandH.getGsshBrId())
                        .andEqualTo("proSelfCode", orderDetail.getProCode());
                GaiaProductBusiness productBusiness = productBusinessMapper.selectOneByExample(productExample);


                Example taxExample = new Example(GaiaTaxCode.class);
                taxExample.createCriteria().andEqualTo("taxCode", productBusiness.getProOutputTax());
                GaiaTaxCode taxCode = taxCodeMapper.selectOneByExample(taxExample);

                gaiaSdSaleHandD.setGssdMovTax(new BigDecimal(taxCode.getTaxCodeValue().replace("%", "").trim()).scaleByPowerOfTen(-2));

                gaiaSdSaleHandD.setClientId(saleHandH.getClientId());
                gaiaSdSaleHandD.setGssdBillNo(saleHandH.getGsshBillNo());
                gaiaSdSaleHandD.setGssdBrId(saleHandH.getGsshBrId());
                gaiaSdSaleHandD.setGssdDate(saleHandH.getGsshDate());
                gaiaSdSaleHandD.setGssdSerial(String.valueOf(i++));
                gaiaSdSaleHandD.setGssdProId(orderDetail.getProCode());
                gaiaSdSaleHandD.setGssdBatchNo(orderDetail.getBatchNo());
                gaiaSdSaleHandD.setGssdValidDate(null);
                gaiaSdSaleHandD.setGssdPrc1(orderDetail.getProPrice());
                gaiaSdSaleHandD.setGssdPrc2(orderDetail.getProPayPrice());
                gaiaSdSaleHandD.setGssdQty(String.valueOf(orderDetail.getQuantity()));
                gaiaSdSaleHandD.setGssdAmt(orderDetail.getProAmount());
                gaiaSdSaleHandD.setGssdZkAmt(orderDetail.getReduceAmount());
                gaiaSdSaleHandD.setGssdSalerId("");//销售人员编号
                gaiaSdSaleHandD.setGssdDoctorId("");//医师编号
                gaiaSdSaleHandD.setGssdProStatus("");//正常、会员价、改价、会员卡折扣、积分换购
                gaiaSdSaleHandD.setGssdOriQty(orderDetail.getQuantity());//初始数量
                gaiaSdSaleHandD.setGssdDose(null);//贴数
                gaiaSdSaleHandD.setSaleDFlag(null);//是否支付：N Y
                gaiaSdSaleHandD.setClinicalSheetSn(orderDetail.getClinicalSheetSn());
                list.add(gaiaSdSaleHandD);
            }
        }
        return list;
    }
}
