package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PercentageBasicInData implements Serializable {

    private static final long serialVersionUID = -2627994937625717097L;

    @ApiModelProperty(value = "主键id")
    private Integer id;
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店编码组")
    private String[] storeCodes;
    @ApiModelProperty(value = "方案名称")
    private String planName;
    @ApiModelProperty(value = "创建人编码")
    private String planCreaterId;
    @ApiModelProperty(value = "创建人")
    private String planCreater;

    @ApiModelProperty(value = "方案起始日期")
    private String planStartDate;
    @ApiModelProperty(value = "方案结束日期")
    private String planEndDate;
    @ApiModelProperty(value = "提成类型 1 销售提成 2 单品提成")
    private String planType;
    @ApiModelProperty(value = "门店分配比例（默认0）")
    private String planScaleSto;
    @ApiModelProperty(value = "员工分配比例（默认100）")
    private String planScaleSaler;
    @ApiModelProperty(value = "时间数组（planStartDate,planEndDate）")
    private String[] dateArr;

    private String planReason;

}
