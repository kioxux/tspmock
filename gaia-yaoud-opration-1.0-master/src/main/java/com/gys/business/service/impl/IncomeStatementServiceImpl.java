package com.gys.business.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.IncomeStatementService;
import com.gys.business.service.PayService;
import com.gys.business.service.StockService;
import com.gys.business.service.data.*;
import com.gys.common.YiLianDaModel.YPSYXX.SymxlbParam;
import com.gys.common.enums.IncomeStatementTypeEnum;
import com.gys.common.enums.MaterialTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.feign.AuthService;
import com.gys.feign.StoreService;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author 陈浩
 */
@Service
@Slf4j
public class IncomeStatementServiceImpl implements IncomeStatementService {
    @Resource
    private GaiaSdIncomeStatementHMapper incomeStatementHMapper;

    @Resource
    private GaiaSdIncomeStatementDMapper incomeStatementDetailMapper;

    @Resource
    private GaiaStoreDataMapper storeDataMapper;

    @Resource
    private GaiaProductBusinessMapper productBusinessMapper;

    @Resource
    private GaiaMaterialAssessMapper materialAssessMapper;

    @Resource
    private AuthService authService;

    @Resource
    private StoreService storeService;

    @Resource
    private GaiaSdPhysicalHeaderMapper physicalHeaderMapper;

    @Resource
    private GaiaSdPhysicalCountDiffMapper physicalCountDiffMapper;

    @Resource
    private PayService payService;

    @Resource
    private StockService stockService;

    @Resource
    private GaiaWfRecordMapper wfRecordMapper;

    @Override
    public List<GetPhysicalCountHeadOutData> list(IncomeStatementInData inData) {
        GetPhysicalCountInData physicalInData = new GetPhysicalCountInData();
        BeanUtil.copyProperties(inData, physicalInData);
        physicalInData.setGspcBrId(inData.getGsishBrId());
        List<GetPhysicalCountHeadOutData> physicalCountHeadOutDataList = physicalHeaderMapper.listPhysicalDiff(physicalInData);
        List<GetPhysicalCountHeadOutData> resultList = Lists.newArrayList();
        GetWorkflowInData workflowInData = new GetWorkflowInData();
        workflowInData.setClient(inData.getClientId());
        workflowInData.setDefineCode("GAIA_WSD_1001");
        for (GetPhysicalCountHeadOutData physicalHeader : physicalCountHeadOutDataList) {
            String voucherId = physicalHeader.getGspcVoucherId();
            physicalHeader.setGspcVoucherId(voucherId);
            workflowInData.setOrderNo(voucherId);
            //查看是否有门店盘点新增批号申请
            List<GaiaWfRecord> workflowOutDataList = wfRecordMapper.fetchByOrder(workflowInData);
            if(CollectionUtils.isNotEmpty(workflowOutDataList)){
                GaiaWfRecord workflowOutData = CollectionUtil.getFirst(workflowOutDataList);
                String wfStatus = workflowOutData.getWfStatus();
                List<GetWfNewBatchNoInData> wfNewBatchNoInDataList = JSONArray.parseArray(workflowOutData.getWfJsonString(), GetWfNewBatchNoInData.class);

                //已驳回
                if(StringUtils.equalsIgnoreCase("1", wfStatus)){
                    physicalInData.setGspcVoucherId(physicalHeader.getGspcVoucherId());
                    List<String> proIds = physicalHeaderMapper.fetchProIds(physicalInData);
                    //盘点少于一行
                    if(proIds.size() <= wfNewBatchNoInDataList.size() - 1){
                        continue;
                    }

                    //审批中
                }else if(StringUtils.equalsIgnoreCase("2", wfStatus)){
                    continue;
                }
            }

            physicalHeader.setGsphFlag1(physicalHeader.getGsphFlag1().equals("0") ? "部分盘点" : "全盘");
            physicalHeader.setGsphFlag2(physicalHeader.getGsphFlag2().equals("0") ? "明盘" : "盲盘");
            physicalHeader.setGsphFlag3(physicalHeader.getGsphFlag3().equals("0") ? "数量盘点" : "批号盘点");
            physicalHeader.setGsphDate(CommonUtil.parseyyyyMMdd(physicalHeader.getGsphDate()));
            physicalHeader.setGsphTime(CommonUtil.parseHHmmss(physicalHeader.getGsphTime()));

            resultList.add(physicalHeader);
        }

        return resultList;
    }

    @Override
    public List<IncomeStatementOutData> getIncomeStatementList(IncomeStatementInData inData) {
        List<IncomeStatementOutData> incomeStatementOutDataList = incomeStatementHMapper.getIncomeStatementList(inData);
        if (CollectionUtil.isNotEmpty(incomeStatementOutDataList)) {
            for(int i = 0; i < incomeStatementOutDataList.size(); ++i) {
                (incomeStatementOutDataList.get(i)).setIndex(i + 1);
            }
        }

        return incomeStatementOutDataList;
    }

    @Override
    public List<IncomeStatementDetailOutData> getIncomeStatementDetailList(IncomeStatementInData inData) {
        //查询未删除的损益明细记录
        List<IncomeStatementDetailOutData> incomeStatementDetailOutDataList = incomeStatementDetailMapper.getIncomeStatementDetailList(inData);
        //判断是否驳回过损益记录
        fillIncomeStatementStatus(incomeStatementDetailOutDataList, inData);

        List<IncomeStatementDetailOutData> resultList = Lists.newArrayList();

        GetWorkflowInData workflowInData = new GetWorkflowInData();
        workflowInData.setClient(inData.getClientId());
        workflowInData.setDefineCode("GAIA_WSD_1001");
        if (CollectionUtil.isNotEmpty(incomeStatementDetailOutDataList)) {
            incomeOut:
            for(IncomeStatementDetailOutData detailOutData : incomeStatementDetailOutDataList) {
                String proId = detailOutData.getGsisdProId();
                String batchNo = detailOutData.getGsisdBatchNo();
                workflowInData.setOrderNo(detailOutData.getVoucherId());
                List<GaiaWfRecord> workflowOutDataList = wfRecordMapper.fetchByOrder(workflowInData);
                if(CollectionUtils.isNotEmpty(workflowOutDataList)){
                    GaiaWfRecord workflowOutData = CollectionUtil.getFirst(workflowOutDataList);
                    String wfStatus = workflowOutData.getWfStatus();
                    List<GetWfNewBatchNoInData> wfNewBatchNoInDataList = JSONArray.parseArray(workflowOutData.getWfJsonString(), GetWfNewBatchNoInData.class);
                    wfNewBatchNoInDataList.remove(0);

                    //已驳回
                    if(StringUtils.equalsIgnoreCase("1", wfStatus)){
                        for(GetWfNewBatchNoInData wfNewBatchNoInData : wfNewBatchNoInDataList){
                            if(StringUtils.equalsIgnoreCase(wfNewBatchNoInData.getWsdSpBm(), proId) && StringUtils.equalsIgnoreCase(wfNewBatchNoInData.getWsdPh(), batchNo)){
                                continue incomeOut;
                            }
                        }
                    }else if(StringUtils.equalsIgnoreCase("2", wfStatus)){  //审批中
                        continue;
                    }
                }

                detailOutData.setGsisdValidUntil(CommonUtil.parseyyyyMMdd(detailOutData.getGsisdValidUntil()));
                detailOutData.setGsisdQty(CommonUtil.stripTrailingZeros(detailOutData.getGsisdQty()));
                detailOutData.setGsisdRealQty(CommonUtil.stripTrailingZeros(detailOutData.getGsisdRealQty()));

                resultList.add(detailOutData);
            }
        }

        return resultList;
    }

    private void fillIncomeStatementStatus(List<IncomeStatementDetailOutData> incomeStatementDetailOutDataList, IncomeStatementInData inData) {
        String rejectStatus = "8";
        //判断盘点导入单是否驳回状态
        Example example = new Example(GaiaSdPhysicalHeader.class);
        example.createCriteria()
                .andEqualTo("client", inData.getClientId())
                .andEqualTo("gsphBrId", inData.getGsishBrId())
                .andEqualTo("gsphVoucherId", inData.getGsishVoucherId());
        GaiaSdPhysicalHeader physicalHeader = physicalHeaderMapper.selectOneByExample(example);
        for (IncomeStatementDetailOutData detailOutData : incomeStatementDetailOutDataList) {
            if (physicalHeader != null && rejectStatus.equals(physicalHeader.getGsphSchedule())) {
                detailOutData.setCanDelete(1);
            } else {
                detailOutData.setCanDelete(0);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insert(IncomeStatementInData inData) {
        List<IncomeStatementDetailInData> details = inData.getDetailOutDataList();
        if(CollectionUtil.isEmpty(details)){
            throw new BusinessException("请输入数据后提交");
        }
        GaiaSdIncomeStatementH incomeStatement;
        String type = inData.getGsishIsType();
        String gsishVoucherId = StringUtils.isEmpty(inData.getGsishVoucherId()) ? IncomeStatementTypeEnum.getOrderNo(type) + RandomUtil.randomNumbers(6) : inData.getGsishVoucherId();
        List<GaiaSdIncomeStatementD> detailList = Lists.newArrayList();
        String tableType = inData.getGsishTableType();
        BigDecimal totalAmt = BigDecimal.ZERO;
        BigDecimal totalQty = BigDecimal.ZERO;

        if(StrUtil.isBlank(tableType)){
            Example example = new Example(GaiaSdIncomeStatementH.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsishBrId", inData.getGsishBrId())
                                    .andEqualTo("gsishVoucherId", CollectionUtil.getFirst(details).getGsisdVoucherId());
            incomeStatement = incomeStatementHMapper.selectOneByExample(example);
            incomeStatement.setGsishDate(CommonUtil.getyyyyMMdd());
            incomeStatement.setGsishRemark(inData.getGsishRemark());
            incomeStatement.setGsishTotalQty(totalQty.toString());
            incomeStatement.setGsishTotalAmt(totalAmt);
            incomeStatement.setGsishExamineEmp(inData.getUserId());
            incomeStatement.setGsishExamineDate(CommonUtil.getyyyyMMdd());
           /* incomeStatement.setGsishBranch(inData.getGsishBranch());
            if(CollectionUtils.isNotEmpty(inData.getImgs())){
                incomeStatement.setGsishFj(StringUtils.join(inData.getImgs(),","));
            }*/
            incomeStatementHMapper.updateByPrimaryKey(incomeStatement);
        }else{//盘点单导入后，提交审核
            incomeStatement = new GaiaSdIncomeStatementH();
            BeanUtil.copyProperties(inData, incomeStatement);
            incomeStatement.setClientId(inData.getClientId());
            incomeStatement.setGsishBrId(inData.getGsishBrId());
            incomeStatement.setGsishVoucherId(gsishVoucherId);
            incomeStatement.setGsishDate(CommonUtil.getyyyyMMdd());
            incomeStatement.setGsishIsType(type);
            incomeStatement.setGsishTableType(IncomeStatementTypeEnum.getTypeName(type));
            incomeStatement.setGsishPcVoucherId(inData.getGsishPcVoucherId());
            incomeStatement.setGsishRemark(inData.getGsishRemark());
            incomeStatement.setGsishTotalQty(totalQty.toString());
            incomeStatement.setGsishTotalAmt(totalAmt);
            incomeStatement.setGsishExamineEmp(inData.getUserId());
            incomeStatement.setGsishExamineDate(CommonUtil.getyyyyMMdd());
            incomeStatement.setGsishStatus("1");
            if (CollectionUtils.isNotEmpty(inData.getImgs())) {
                incomeStatement.setGsishFj(StringUtils.join(inData.getImgs(), ","));
            }
            incomeStatementHMapper.insert(incomeStatement);
        }

        for(int i = 0; i < details.size(); ++i) {
            GaiaSdIncomeStatementD detail = new GaiaSdIncomeStatementD();
            IncomeStatementDetailInData detailInData = details.get(i);
            //判断数值是否为负数
            /*if(new BigDecimal(detailInData.getDifference()).compareTo(BigDecimal.ZERO)<0){
                throw new BusinessException("损溢数量不为负数");
            }*/
            detail.setClient(inData.getClientId());
            detail.setGsisdBrId(inData.getGsishBrId());
            detail.setGsisdVoucherId(gsishVoucherId);
            detail.setGsisdDate(CommonUtil.getyyyyMMdd());
            detail.setGsisdProId(detailInData.getGsisdProId());
            detail.setGsisdBatchNo(detailInData.getGsisdBatchNo());
            detail.setGsisdBatch(detailInData.getGsisdBatch());
            detail.setGsisdStockQty(new BigDecimal(detailInData.getGsisdQty()));
            detail.setGsisdRealQty(new BigDecimal(detailInData.getGsisdRealQty()));
            detail.setGsisdIsCause(detailInData.getGsisdIsCause());
            if (ObjectUtil.isNotEmpty(detailInData.getGsisdRetailPrice()) && ObjectUtil.isNotEmpty(detailInData.getGsisdIsQty())) {
                totalAmt = totalAmt.add((new BigDecimal(detailInData.getGsisdRetailPrice())).multiply(new BigDecimal(detailInData.getGsisdIsQty())));
            }

            if (ObjectUtil.isNotEmpty(detailInData.getGsisdIsQty())) {
                totalQty = totalQty.add(new BigDecimal(detailInData.getGsisdIsQty()));
            }

            detail.setGsisdValidUntil(detailInData.getGsisdValidUntil());
            detail.setGsisdStockStatus("0");
            detail.setGsisdSerial(detailInData.getGsisdSerial() == 0 ? StrUtil.toString(i + 1) : detailInData.getGsisdSerial() + "");
            detail.setGsisdIsQty(new BigDecimal(detailInData.getDifference()));
            detailList.add(detail);
            if(StrUtil.isNotBlank(tableType)){
                incomeStatementDetailMapper.insert(detail);
            }
        }

        if(StrUtil.isBlank(tableType) || StringUtils.isEmpty(inData.getGsishVoucherId())){
            this.createWorkFlow(incomeStatement, detailList, inData.getToken());
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approve(IncomeStatementInData inData) {
        Example example = new Example(GaiaSdIncomeStatementH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsishBrId", inData.getGsishBrId()).andEqualTo("gsishVoucherId", inData.getGsishVoucherId());
        GaiaSdIncomeStatementH incomeStatement = this.incomeStatementHMapper.selectOneByExample(example);
        if (ObjectUtil.isNull(incomeStatement)) {
            throw new BusinessException("损溢单不存在");
        } else if (!"2".equals(incomeStatement.getGsishStatus())) {
            throw new BusinessException("只有已退回状态的才能提交");
        } else {
            incomeStatement.setGsishStatus("0");
            this.incomeStatementHMapper.updateByPrimaryKeySelective(incomeStatement);
            example = new Example(GaiaSdIncomeStatementD.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsisdBrId", inData.getGsishBrId()).andEqualTo("gsisdVoucherId", inData.getGsishVoucherId());
            List<GaiaSdIncomeStatementD> detailList = this.incomeStatementDetailMapper.selectByExample(example);
//            this.createWorkFlow(incomeStatement, detailList, inData.getToken());
        }
    }

    private void createWorkFlow(GaiaSdIncomeStatementH incomeStatement, List<GaiaSdIncomeStatementD> details, String token) {
        GaiaStoreData storeRecord = new GaiaStoreData();
//        String voucherId = incomeStatement.getGsishPcVoucherId();
        String clientId = incomeStatement.getClientId();
        String brId = incomeStatement.getGsishBrId();
        String type = incomeStatement.getGsishIsType();
        storeRecord.setClient(clientId);
        storeRecord.setStoCode(brId);
        boolean isIncomeStatementWf = IncomeStatementTypeEnum.PAN_DIAN_DAO_RU.getType().equals(type);
        GaiaStoreData storeData = this.storeDataMapper.selectByPrimaryKey(storeRecord);
        if (!ObjectUtil.isNull(storeData)) {
            GetWfCreateInData wfInData = new GetWfCreateInData();
            wfInData.setToken(token);
            wfInData.setWfTitle(IncomeStatementTypeEnum.getTitle(type));
            wfInData.setWfDescription(IncomeStatementTypeEnum.getTitle(type));
            wfInData.setWfOrder(incomeStatement.getGsishVoucherId());
            wfInData.setWfSite(storeData.getStoCode());
            //是否为连锁
            boolean isChain = StrUtil.isNotBlank(storeData.getStoChainHead());
            wfInData.setWfDefineCode(IncomeStatementTypeEnum.getWfCode(type, isChain));

            List<GetWfCreateLossCompadmInData> wfDetailList = Lists.newArrayList();
            GetWfCreateLossCompadmInData lossCompadmInData;
            Iterator<GaiaSdIncomeStatementD> detailIterator = details.iterator();
            int rowIndex = 1;
            while (detailIterator.hasNext()){
                GaiaSdIncomeStatementD detail = detailIterator.next();
                lossCompadmInData = new GetWfCreateLossCompadmInData();
                lossCompadmInData.setIndex(rowIndex);
                lossCompadmInData.setValidUntil(detail.getGsisdValidUntil());
                lossCompadmInData.setLossDate(incomeStatement.getGsishDate());
                lossCompadmInData.setLossOrder(incomeStatement.getGsishVoucherId());
                lossCompadmInData.setStoreId(storeData.getStoName());
                lossCompadmInData.setProCode(detail.getGsisdProId());
                lossCompadmInData.setBatch(detail.getGsisdBatchNo());
                lossCompadmInData.setQty(StrUtil.toString(detail.getGsisdIsQty()));
                lossCompadmInData.setReason(detail.getGsisdIsCause());
                lossCompadmInData.setSerial(detail.getGsisdSerial());

                Example example = new Example(GaiaProductBusiness.class);
                example.createCriteria().andEqualTo("client", detail.getClient()).andEqualTo("proSite", detail.getGsisdBrId()).andEqualTo("proSelfCode", detail.getGsisdProId());
                GaiaProductBusiness productBusiness = productBusinessMapper.selectOneByExample(example);
                if (ObjectUtil.isNotNull(productBusiness)) {
                    lossCompadmInData.setProName(productBusiness.getProName());
                    lossCompadmInData.setProSpecs(productBusiness.getProSpecs());
                    lossCompadmInData.setSccj(productBusiness.getProFactoryName());
                }

                example = new Example(GaiaMaterialAssess.class);
                example.createCriteria().andEqualTo("client", detail.getClient()).andEqualTo("matAssessSite", detail.getGsisdBrId()).andEqualTo("matProCode", detail.getGsisdProId());
                GaiaMaterialAssess materialAssess = materialAssessMapper.selectOneByExample(example);
                if (ObjectUtil.isNotNull(materialAssess)) {
//                    lossCompadmInData.setBatchCost(StrUtil.toString(materialAssess.getBatBatchCost()));
//                    if (ObjectUtil.isNotNull(materialAssess.getBatBatchCost()) && StrUtil.isNotBlank(inputTax)) {
//                        lossCompadmInData.setTax(StrUtil.toString(materialAssess.getBatBatchCost().multiply(new BigDecimal(inputTax))));
//                    }

                    BigDecimal matMovPrice = materialAssess.getMatMovPrice();
                    if(matMovPrice != null){
                        lossCompadmInData.setWmaCost(StrUtil.toString(matMovPrice));
                        lossCompadmInData.setWmaCostTax(StrUtil.toString(detail.getGsisdIsQty().multiply(matMovPrice)));
                    }
                }
                wfDetailList.add(lossCompadmInData);
                rowIndex++;
                if (rowIndex > 20) {
                    break;
                }
            }

            if(isIncomeStatementWf){
                //插入新版工作流数据
                this.generateNewIncomeStatementWorkflow(wfInData, incomeStatement);

                Example physicalHeaderExample = new Example(GaiaSdPhysicalHeader.class);
                physicalHeaderExample.createCriteria().andEqualTo("client", incomeStatement.getClientId()).andEqualTo("gsphBrId", incomeStatement.getGsishBrId())
                        .andEqualTo("gsphVoucherId", incomeStatement.getGsishPcVoucherId());
                GaiaSdPhysicalHeader physicalHeader = physicalHeaderMapper.selectOneByExample(physicalHeaderExample);
                physicalHeader.setGsphSchedule("7");
                physicalHeader.setGsphUpdateDate(CommonUtil.getyyyyMMdd());
                physicalHeaderMapper.updateByPrimaryKey(physicalHeader);
            }

            wfInData.setWfDetail(wfDetailList);
            String result = authService.createWorkflow(wfInData);
            log.info("请求创建工作流：{}", JSON.toJSONString(wfInData));
            JSONObject jsonObject = JSONObject.parseObject(result);
            log.info("工作流回调：{}", JSON.toJSONString(result));
            if (jsonObject.containsKey("code") && !"0".equals(jsonObject.getString("code"))) {
                throw new BusinessException(jsonObject.getString("message"));
            }
        }
    }

    /**
     * 新版工作流数据
     * @param wfInData
     * @param incomeStatement
     */
    private void generateNewIncomeStatementWorkflow(GetWfCreateInData wfInData, GaiaSdIncomeStatementH incomeStatement){
        //盘点单号
        String pcVoucherId = incomeStatement.getGsishPcVoucherId();
        String clientId = incomeStatement.getClientId();
        String brId = incomeStatement.getGsishBrId();
        String voucherId = incomeStatement.getGsishPcVoucherId();
        GetPhysicalCountInData physicalCountInData = new GetPhysicalCountInData();
        GetWfNewPhysicalWorkflowInData newPhysicalWorkflowInData = new GetWfNewPhysicalWorkflowInData();

        //品项 盘点总数
        BigDecimal itemInventoryTotal = BigDecimal.ZERO;
        //品项 盘盈数
        BigDecimal itemInventoryProfit = BigDecimal.ZERO;
        //品项 盘亏数
        BigDecimal itemInventoryLosses = BigDecimal.ZERO;
        //品项 差异合计(绝对值
        BigDecimal itemAbsoluteDiff = BigDecimal.ZERO;
        //品项 绝对差异率
        BigDecimal itemAbsoluteDiffRatio = BigDecimal.ZERO;
        //品项 差异合计(相对值)
        BigDecimal itemRelativeDiff = BigDecimal.ZERO;
        //品项 相对差异率
        BigDecimal itemRelativeDiffRatio = BigDecimal.ZERO;
        //批号行数 盘点总数
        int batchInventoryTotal = 0;
        //批号行数 盘盈数
        int batchInventoryProfit = 0;
        //批号行数 盘亏数
        int batchInventoryLosses = 0;
        //批号行数 差异合计(绝对值)
        int batchAbsoluteDiff = 0;
        //批号行数 绝对差异率
        BigDecimal batchAbsoluteDiffRatio = BigDecimal.ZERO;
        //批号行数 差异合计(相对值)
        int batchRelativeDiff = 0;
        //批号行数 相对差异率
        BigDecimal batchRelativeDiffRatio = BigDecimal.ZERO;
        //数量 盘点总数
        BigDecimal numInventoryTotal = BigDecimal.ZERO;
        //数量 盘盈数
        BigDecimal numInventoryProfit = BigDecimal.ZERO;
        //数量 盘亏数
        BigDecimal numInventoryLosses = BigDecimal.ZERO;
        //数量 差异合计(绝对值)
        BigDecimal numAbsoluteDiff = BigDecimal.ZERO;
        //数量 绝对差异率
        BigDecimal numAbsoluteDiffRatio = BigDecimal.ZERO;
        //数量 差异合计(相对值)
        BigDecimal numRelativeDiff = BigDecimal.ZERO;
        //数量 相对差异率
        BigDecimal numRelativeDiffRatio = BigDecimal.ZERO;
        //成本额 盘点总数
        BigDecimal costInventoryTotal = BigDecimal.ZERO;
        //成本额 盘盈数
        BigDecimal costInventoryProfit = BigDecimal.ZERO;
        //成本额 盘亏数
        BigDecimal costInventoryLosses = BigDecimal.ZERO;
        //成本额 差异合计(绝对值)
        BigDecimal costAbsoluteDiff = BigDecimal.ZERO;
        //成本额 绝对差异率
        BigDecimal costAbsoluteDiffRatio = BigDecimal.ZERO;
        //成本额 差异合计(相对值)
        BigDecimal costRelativeDiff = BigDecimal.ZERO;
        //成本额 相对差异率
        BigDecimal costRelativeDiffRatio = BigDecimal.ZERO;
        //零售额 盘点总数
        BigDecimal retailInventoryTotal = BigDecimal.ZERO;
        //零售额 盘盈数
        BigDecimal retailInventoryProfit = BigDecimal.ZERO;
        //零售额 盘亏数
        BigDecimal retailInventoryLosses = BigDecimal.ZERO;
        //零售额 差异合计(绝对值)
        BigDecimal retailAbsoluteDiff = BigDecimal.ZERO;
        //零售额 绝对差异率
        BigDecimal retailAbsoluteDiffRatio = BigDecimal.ZERO;
        //零售额 差异合计(相对值)
        BigDecimal retailRelativeDiff = BigDecimal.ZERO;
        //零售额 相对差异率
        BigDecimal retailRelativeDiffRatio = BigDecimal.ZERO;

        Example physicalHeaderExample = new Example(GaiaSdPhysicalHeader.class);
        physicalHeaderExample.createCriteria().andEqualTo("client", clientId) .andEqualTo("gsphBrId", brId) .andEqualTo("gsphVoucherId", voucherId);
        GaiaSdPhysicalHeader physicalHeader = physicalHeaderMapper.selectOneByExample(physicalHeaderExample);
        Table<String, String, Integer> productIdTable = HashBasedTable.create();
        String isFull = physicalHeader.getGsphFlag1();
        String isShowStock = physicalHeader.getGsphFlag2();
        String isBatch = physicalHeader.getGsphFlag3();
        StringBuffer inventoryTypeSb = new StringBuffer();
        inventoryTypeSb.append(isFull.equals("0") ? "部分盘点" : "全盘");
        inventoryTypeSb.append("、");
        inventoryTypeSb.append(isShowStock.equals("0") ? "明盘" : "盲盘");
        inventoryTypeSb.append("、");
        inventoryTypeSb.append(isBatch.equals("0") ? "数量盘点" : "批号盘点");


        physicalCountInData.setClientId(clientId);
        physicalCountInData.setGspcBrId(brId);
        physicalCountInData.setGspcVoucherId(pcVoucherId);
//            Example physicalCountDiffExample = new Example(GaiaSdPhysicalCountDiff.class);
//            physicalCountDiffExample.createCriteria().andEqualTo("clientId", clientId)
//                                                     .andEqualTo("gspcdBrId", brId)
//                                                     .andEqualTo("gspcVoucherId", voucherId);
        List<PhysicalNewReportOutData> physicalNewReportOutDataList = physicalCountDiffMapper.fetchPhysicalNewReport(physicalCountInData);
        for(PhysicalNewReportOutData physicalNewReportOutData : physicalNewReportOutDataList){
            //盘点数量
            BigDecimal pcFirstQty = physicalNewReportOutData.getPcFirstQty();
            //盘点差异
            BigDecimal diffSecondQty = physicalNewReportOutData.getDiffSecondQty() == null ? BigDecimal.ZERO : physicalNewReportOutData.getDiffSecondQty();
            String proId = physicalNewReportOutData.getProId();
            BigDecimal movPrice = physicalNewReportOutData.getMovPrice() == null ? BigDecimal.ZERO : physicalNewReportOutData.getMovPrice();
            //含税成本价
            BigDecimal tpp = BigDecimal.ZERO;
            if(physicalNewReportOutData.getMatTotalAmt() != null || physicalNewReportOutData.getMatTotalQty() != null){
                BigDecimal matTotalQty = physicalNewReportOutData.getMatTotalQty();
                if(BigDecimal.ZERO.compareTo(matTotalQty) == 0){
                    tpp = physicalNewReportOutData.getPoPrice();
                }else{
                    tpp = physicalNewReportOutData.getMatTotalAmt().add(physicalNewReportOutData.getMatRateAmt()).divide(physicalNewReportOutData.getMatTotalQty(), 2, BigDecimal.ROUND_HALF_UP);
                }
            }
            //BigDecimal movAmt = physicalNewReportOutData.getMovAmt() == null ? BigDecimal.ZERO : physicalNewReportOutData.getMovAmt();
            BigDecimal pcSecondQty = physicalNewReportOutData.getPcSecondQty() == null ? BigDecimal.ZERO : physicalNewReportOutData.getPcSecondQty();
            BigDecimal priceNormal = physicalNewReportOutData.getPriceNormal() == null ? BigDecimal.ZERO : physicalNewReportOutData.getPriceNormal();
            numInventoryTotal = numInventoryTotal.add(pcFirstQty.abs()).add(pcSecondQty);
            if(diffSecondQty.compareTo(BigDecimal.ZERO) > 0){
                numInventoryProfit = numInventoryProfit.add(diffSecondQty);
                costInventoryProfit = costInventoryProfit.add(diffSecondQty.multiply(tpp));
                retailInventoryProfit = retailInventoryProfit.add(diffSecondQty.multiply(priceNormal));

                if(productIdTable.get("profit", proId) == null){
                    productIdTable.put("profit", proId, 1);
                }else {
                    productIdTable.put("profit", proId, productIdTable.get("profit", proId) + 1);
                }
                batchInventoryProfit++;
            }else if(diffSecondQty.compareTo(BigDecimal.ZERO) < 0){
                numInventoryLosses = numInventoryLosses.add(diffSecondQty);
                costInventoryLosses = costInventoryLosses.add(diffSecondQty.multiply(tpp));
                retailInventoryLosses = retailInventoryLosses.add(diffSecondQty.multiply(priceNormal));

                if(productIdTable.get("losses", proId) == null){
                    productIdTable.put("losses", proId, 1);
                }else {
                    productIdTable.put("losses", proId, productIdTable.get("losses", proId) + 1);
                }

                batchInventoryLosses++;
            }else {
                if(productIdTable.get("empty", proId) == null){
                    productIdTable.put("empty", proId, 1);
                }else {
                    productIdTable.put("empty", proId, productIdTable.get("empty", proId) + 1);
                }
            }

            //品项 按数量盘点
            if(StringUtils.equals("0", isBatch)){
                if(diffSecondQty.compareTo(BigDecimal.ZERO) > 0){
                    itemInventoryProfit = itemInventoryProfit.add(BigDecimal.ONE);

                }else if(diffSecondQty.compareTo(BigDecimal.ZERO) < 0){
                    itemInventoryLosses = itemInventoryLosses.add(BigDecimal.ONE);
                }
                itemInventoryTotal = itemInventoryTotal.add(BigDecimal.ONE);
            }

            //成本额
            costInventoryTotal = costInventoryTotal.add(pcFirstQty.multiply(tpp));

            //零售额
            retailInventoryTotal = retailInventoryTotal.add(pcFirstQty.multiply(priceNormal));
        }

        //按批号盘点
        if(StringUtils.equals("1", isBatch)){
            Map<String ,Integer> itemInventoryProfitMap = productIdTable.row("profit");
            Map<String ,Integer> itemInventoryLossesMap = productIdTable.row("losses");
            Map<String ,Integer> itemInventoryEmptyMap = productIdTable.row("empty");
            newPhysicalWorkflowInData.setItemInventoryProfit(new BigDecimal(itemInventoryProfitMap.size()));
            newPhysicalWorkflowInData.setItemInventoryLosses(new BigDecimal(itemInventoryLossesMap.size()));
            itemInventoryTotal = newPhysicalWorkflowInData.getItemInventoryProfit().abs().add(newPhysicalWorkflowInData.getItemInventoryLosses().abs()).add(new BigDecimal(itemInventoryEmptyMap.size()));

            //批号行数
            double listSize = physicalNewReportOutDataList.size();
            newPhysicalWorkflowInData.setBatchInventoryTotal(NumberUtil.decimalFormat("#", physicalNewReportOutDataList.size()));
            newPhysicalWorkflowInData.setBatchInventoryProfit(batchInventoryProfit);
            newPhysicalWorkflowInData.setBatchInventoryLosses(batchInventoryLosses);
            newPhysicalWorkflowInData.setBatchAbsoluteDiff(batchInventoryProfit + batchInventoryLosses);
            newPhysicalWorkflowInData.setBatchAbsoluteDiffRatio(NumberUtil.decimalFormat("#.##%", listSize == 0 ? 0 : newPhysicalWorkflowInData.getBatchAbsoluteDiff() / listSize));
            newPhysicalWorkflowInData.setBatchRelativeDiff(batchInventoryProfit - batchInventoryLosses);
            newPhysicalWorkflowInData.setBatchRelativeDiffRatio(NumberUtil.decimalFormat("#.##%", listSize == 0 ? 0 : newPhysicalWorkflowInData.getBatchRelativeDiff() / listSize));
        }else{
            newPhysicalWorkflowInData.setItemInventoryProfit(itemInventoryProfit);
            newPhysicalWorkflowInData.setItemInventoryLosses(itemInventoryLosses);
        }

        //品项
        newPhysicalWorkflowInData.setItemInventoryTotal(itemInventoryTotal);
        newPhysicalWorkflowInData.setItemAbsoluteDiff(newPhysicalWorkflowInData.getItemInventoryProfit().add(newPhysicalWorkflowInData.getItemInventoryLosses().abs()));
        newPhysicalWorkflowInData.setItemAbsoluteDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getItemAbsoluteDiff(), itemInventoryTotal, "#.##%"));
        newPhysicalWorkflowInData.setItemRelativeDiff(newPhysicalWorkflowInData.getItemInventoryProfit().subtract(newPhysicalWorkflowInData.getItemInventoryLosses().abs()));
        newPhysicalWorkflowInData.setItemRelativeDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getItemRelativeDiff(), itemInventoryTotal, "#.##%"));

        //数量
        newPhysicalWorkflowInData.setNumInventoryTotal(NumberUtil.decimalFormat("#.####",numInventoryTotal));
        newPhysicalWorkflowInData.setNumInventoryProfit(numInventoryProfit);
        newPhysicalWorkflowInData.setNumInventoryLosses(numInventoryLosses.abs());
        newPhysicalWorkflowInData.setNumAbsoluteDiff(numInventoryProfit.add(numInventoryLosses.abs()));
        newPhysicalWorkflowInData.setNumAbsoluteDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getNumAbsoluteDiff(), numInventoryTotal, "#.##%"));
        newPhysicalWorkflowInData.setNumRelativeDiff(numInventoryProfit.subtract(numInventoryLosses.abs()));
        newPhysicalWorkflowInData.setNumRelativeDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getNumRelativeDiff(), numInventoryTotal, "#.##%"));

        //成本额
        newPhysicalWorkflowInData.setCostInventoryTotal(NumberUtil.decimalFormat("#.##", costInventoryTotal));
        newPhysicalWorkflowInData.setCostInventoryProfit(NumberUtil.decimalFormat("#.##", costInventoryProfit));
        newPhysicalWorkflowInData.setCostInventoryLosses(NumberUtil.decimalFormat("#.##", costInventoryLosses.abs()));
        newPhysicalWorkflowInData.setCostAbsoluteDiff(new BigDecimal(NumberUtil.decimalFormat("#.##", costInventoryProfit.add(costInventoryLosses.abs()))));
        newPhysicalWorkflowInData.setCostRelativeDiff(new BigDecimal(NumberUtil.decimalFormat("#.##", costInventoryProfit.subtract(costInventoryLosses.abs()))));
        newPhysicalWorkflowInData.setCostAbsoluteDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getCostAbsoluteDiff(), costInventoryTotal, "#.##%"));
        newPhysicalWorkflowInData.setCostRelativeDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getCostRelativeDiff(), costInventoryTotal, "#.##%"));

        //零售额
        newPhysicalWorkflowInData.setRetailInventoryTotal(NumberUtil.decimalFormat("#.##", retailInventoryTotal.abs()));
        newPhysicalWorkflowInData.setRetailInventoryProfit(NumberUtil.decimalFormat("#.##", retailInventoryProfit.abs()));
        newPhysicalWorkflowInData.setRetailInventoryLosses(NumberUtil.decimalFormat("#.##", retailInventoryLosses.abs()));
        newPhysicalWorkflowInData.setRetailAbsoluteDiff(new BigDecimal(NumberUtil.decimalFormat("#.##", retailInventoryProfit.add(retailInventoryLosses.abs()))));
        newPhysicalWorkflowInData.setRetailAbsoluteDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getRetailAbsoluteDiff(), retailInventoryTotal, "#.##%"));
        newPhysicalWorkflowInData.setRetailRelativeDiff(retailInventoryProfit.subtract(retailInventoryLosses.abs()));
        newPhysicalWorkflowInData.setRetailRelativeDiffRatio(CommonUtil.divideFormat(newPhysicalWorkflowInData.getRetailRelativeDiff(), retailInventoryTotal, "#.##%"));

        newPhysicalWorkflowInData.setInventoryDate(CommonUtil.parseyyyyMMdd(incomeStatement.getGsishDate()));
        newPhysicalWorkflowInData.setVoucherId(voucherId);
        newPhysicalWorkflowInData.setIsBatch(isBatch);
        newPhysicalWorkflowInData.setInventoryType(inventoryTypeSb.toString());
        wfInData.setNewWorkflowInData(newPhysicalWorkflowInData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approveIncomeStatement(GetWfApproveInData inData, int isDeductStock) {
        log.info("approveIncomeStatement inData:{}", JSON.toJSONString(inData));
        String defineCode = inData.getWfDefineCode();
        Example example = new Example(GaiaSdIncomeStatementH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsishVoucherId", inData.getWfOrder());
        GaiaSdIncomeStatementH incomeStatement = incomeStatementHMapper.selectOneByExample(example);
        if (ObjectUtil.isNull(incomeStatement)) {
            throw new BusinessException("损溢单不存在");
        } else {
            incomeStatement.setGsishExamineDate(DateUtil.format(new Date(), "yyyyMMdd"));
            incomeStatement.setGsishExamineEmp(inData.getCreateUser());
            //已通过
            if ("3".equals(inData.getWfStatus())) {
                if ("GAIA_WF_006".equals(defineCode) || "GAIA_WF_007".equals(defineCode)) {
                    //库存更新
                    String result = storeService.approval(inData);
                    log.info("approveIncomeStatement result:{}", result);
                    if (StringUtils.isNotEmpty(result)){
                        JSONObject jsonObject = JSON.parseObject(result);
                        if (jsonObject.getInteger("code") != 0) {
                            log.error("盘点接口,{}", jsonObject.getString("message"));
                            throw new BusinessException("盘点错误：" + jsonObject.getString("message"));
                        }else{
                            Example physicalHeaderExample = new Example(GaiaSdPhysicalHeader.class);
                            physicalHeaderExample.createCriteria().andEqualTo("client", incomeStatement.getClientId()).andEqualTo("gsphBrId", incomeStatement.getGsishBrId())
                                    .andEqualTo("gsphVoucherId", incomeStatement.getGsishPcVoucherId());
                            GaiaSdPhysicalHeader physicalHeader = physicalHeaderMapper.selectOneByExample(physicalHeaderExample);
                            physicalHeader.setGsphSchedule("9");
                            physicalHeader.setGsphUpdateDate(CommonUtil.getyyyyMMdd());
                            physicalHeaderMapper.updateByPrimaryKey(physicalHeader);
                        }
                    }else{
                        throw new BusinessException("盘点错误："+"没有回传数据！");
                    }
                }

                int guiNoCount = 1;
                BigDecimal owingStock = BigDecimal.ZERO;
                List<MaterialDocRequestDto> materialList = Lists.newArrayList();
                if(isDeductStock == 1){
                    example = new Example(GaiaSdIncomeStatementD.class);
                    example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("gsisdVoucherId", inData.getWfOrder());
                    List<GaiaSdIncomeStatementD> incomeDetailList = incomeStatementDetailMapper.selectByExample(example);
                    Iterator<GaiaSdIncomeStatementD> incomeDetailIterator = incomeDetailList.iterator();
                    while(incomeDetailIterator.hasNext()) {
                        GaiaSdIncomeStatementD detail = incomeDetailIterator.next();
    //                    detail.setGsisdStockStatus("2");
    //                    incomeStatementDetailMapper.updateByPrimaryKey(detail);
                            StockInData stockInData = new StockInData();
                            stockInData.setClientId(detail.getClient());
                            stockInData.setGssBrId(detail.getGsisdBrId());
                            stockInData.setGssProId(detail.getGsisdProId());
                            stockInData.setBatchNo(detail.getGsisdBatchNo());
                            stockInData.setNum(detail.getGsisdIsQty());
                            stockInData.setVoucherId(detail.getGsisdVoucherId());
                            stockInData.setSerial(StrUtil.toString(guiNoCount));
                            stockInData.setEmpId(inData.getCreateUser());
                            stockInData.setCrFlag("");
                            Object[] stockObj = new Object[2];
                            if("GAIA_WF_050".equals(defineCode) || "GAIA_WF_051".equals(defineCode)){
                                stockObj = stockService.addShopStock(stockInData);
                            }else{
                                stockObj = stockService.consumeShopStock(stockInData, owingStock, incomeDetailIterator.hasNext());
                                owingStock = (BigDecimal) stockObj[0];
                            }

                            List<ConsumeStockResponse> consumeBatchList = (List<ConsumeStockResponse>) stockObj[1];
                            for(ConsumeStockResponse consumeStockResponse : consumeBatchList){
                                MaterialDocRequestDto dto = new MaterialDocRequestDto();
                                dto.setClient(inData.getClientId());
                                dto.setGuid(UUID.randomUUID().toString());
                                dto.setGuidNo(StrUtil.toString(guiNoCount));
                                if ("GAIA_WF_035".equals(defineCode) || "GAIA_WF_036".equals(defineCode)) {
                                    dto.setMatType(MaterialTypeEnum.BAO_SUN.getCode());
                                }else if("GAIA_WF_037".equals(defineCode) || "GAIA_WF_038".equals(defineCode)){
                                    dto.setMatType(MaterialTypeEnum.ZI_YONG.getCode());
                                }else if("GAIA_WF_050".equals(defineCode) || "GAIA_WF_051".equals(defineCode)){
                                    dto.setMatType(MaterialTypeEnum.SUN_YI.getCode());
                                }
                                dto.setMatPostDate(CommonUtil.getyyyyMMdd());
                                dto.setMatHeadRemark("");
                                dto.setMatProCode(detail.getGsisdProId());
                                dto.setMatSiteCode(detail.getGsisdBrId());
                                dto.setMatLocationCode("1000");
                                dto.setMatLocationTo("");
                                dto.setMatBatch(consumeStockResponse.getBatch());
                                dto.setMatQty(consumeStockResponse.getNum());
                                dto.setMatPrice(BigDecimal.ZERO);
                                dto.setMatUnit("盒");
                                dto.setMatDebitCredit("H");
                                dto.setMatPoId(detail.getGsisdVoucherId());
                                dto.setMatPoLineno(detail.getGsisdSerial());
                                dto.setMatDnId(detail.getGsisdVoucherId());
                                dto.setMatDnLineno(detail.getGsisdSerial());
                                dto.setMatLineRemark("");
                                dto.setMatCreateBy(inData.getCreateUser());
                                dto.setMatCreateDate(CommonUtil.getyyyyMMdd());
                                dto.setMatCreateTime(CommonUtil.getHHmmss());
                                materialList.add(dto);

                                guiNoCount++;
                            }
                        }
                    }

                if(isDeductStock == 1){
                    incomeStatement.setGsishStatus("2");
                    incomeStatementHMapper.updateByPrimaryKeySelective(incomeStatement);
                    log.info("更新损溢单:{}", JSON.toJSONString(incomeStatement));

                    payService.insertMaterialDoc(materialList);
                }
            } else if ("1".equals(inData.getWfStatus())){
                if ("GAIA_WF_006".equals(defineCode) || "GAIA_WF_007".equals(defineCode)) {
                    incomeStatement.setGsishStatus("0");
                    incomeStatementHMapper.updateByPrimaryKeySelective(incomeStatement);

                    Example physicalHeaderExample = new Example(GaiaSdPhysicalHeader.class);
                    physicalHeaderExample.createCriteria().andEqualTo("client", incomeStatement.getClientId()).andEqualTo("gsphBrId", incomeStatement.getGsishBrId())
                            .andEqualTo("gsphVoucherId", incomeStatement.getGsishPcVoucherId());
                    GaiaSdPhysicalHeader physicalHeader = physicalHeaderMapper.selectOneByExample(physicalHeaderExample);
                    physicalHeader.setGsphSchedule("8");
                    physicalHeader.setGsphUpdateDate(CommonUtil.getyyyyMMdd());
                    physicalHeaderMapper.updateByPrimaryKey(physicalHeader);
                }
            }
        }
    }

    @Override
    public List<SymxlbParam> getIncomeStatementByYLZ(Map<String,String> map){
        //这里调的是医保易联达的对象
        List<SymxlbParam> symxlb = incomeStatementDetailMapper.getIncomeStatementByYLZ(map);
        if(ObjectUtil.isEmpty(symxlb)){
            return new ArrayList<>();
        }
        return symxlb;
    }

    @Override
    public void removeIncomeDetail(RemoveIncomeForm removeIncomeForm) {
        try {
            Example example = new Example(GaiaSdIncomeStatementD.class);
            example.createCriteria()
                    .andEqualTo("client", removeIncomeForm.getClient())
                    .andEqualTo("gsisdBrId", removeIncomeForm.getDepId())
                    .andEqualTo("gsisdVoucherId", removeIncomeForm.getGsisdVoucherId())
                    .andEqualTo("gsisdProId", removeIncomeForm.getGsisdProId())
                    .andEqualTo("gsisdSerial",removeIncomeForm.getGsisdSerial());
            GaiaSdIncomeStatementD incomeStatementD = incomeStatementDetailMapper.selectOneByExample(example);
            if (incomeStatementD == null) {
                throw new BusinessException("移除明细失败：未查询到损益明细记录");
            }
            incomeStatementD.setDeleteFlag(1);
            incomeStatementDetailMapper.updateByExample(incomeStatementD, example);
        } catch (Exception e) {
            throw new BusinessException("移除损益明细失败", e);
        }
    }
}
