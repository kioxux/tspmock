package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdMemberArea;
import com.gys.business.mapper.entity.GaiaSdSaleD;
import com.gys.business.mapper.entity.UncouplingMemberCardInfo;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.MemberAddDto;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import com.gys.common.vo.MemberClassVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public interface MemberAddOrEditService {
    PageInfo<GetMemberOutData> findMembersByPage(GetMemberInData inData);

    Boolean editMember(GetMemberInData inData);

    MemberAddDto addMember(GetMemberInData inData, GetLoginOutData userInfo);

    GetMemberOutData findMemberByPrimary(GetMemberInData inData);

    String getIncrMemberCard(String prefix, GetLoginOutData userInfo);

    void addMemberClass(GetLoginOutData userInfo,MemberClassVo lists);

    JsonResult findMemberListByPage(MemberListInData inData);

    Result exportMemberListByPage(MemberListInData inData);

    HashMap<String,Object> listVIPParam(GetLoginOutData userInfo, MemberClassVo param);

    JsonResult findMemberListByCondition(MemberConditionInData inData);

    List<GaiaSdSaleD> getControlProNumByMember(GetMemberInData param);

    void pushMemberInfo(String client, MemberAddDto dto);

    Result findMemberListByConditionExport(MemberConditionInData inData);

    Boolean activateMemberCard(GetMemberInData inData);

    // 获取会员卡状态
    ArrayList<HashMap<String,String>> getMemberCardStatus();

    // 解挂会员卡
    void uncouplingMemberCard(GetLoginOutData userInfo, String cardId, String remark);

    // 获取会员卡解挂信息
    UncouplingMemberCardInfo getUncouplingMemberCardInfo(GetLoginOutData userInfo, String cardId);

    JsonResult cardLose(MemberCardLoseInData inData);

    JsonResult cardChange(MemberCardLoseInData inData);

    JsonResult getMemberAreaList(GetLoginOutData userInfo);

    /**
     *  修改会员卡积分
     * @param inData
     * @return
     */
    void editMemberIntegral(MemberIntegralInData inData);

    void cancelMemberCard(MemberCardCancelInData inData);
}
