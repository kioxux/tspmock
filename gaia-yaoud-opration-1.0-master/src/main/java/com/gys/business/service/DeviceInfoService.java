//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdDeviceInfo;
import com.gys.business.service.data.DeviceInfoInData;
import com.gys.business.service.data.DeviceInfoOutData;
import com.gys.business.service.data.device.DeviceForm;
import com.gys.business.service.data.device.UserVO;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import java.util.List;
import java.util.Map;

public interface DeviceInfoService {
    PageInfo<DeviceInfoOutData> getDeviceInfoList(DeviceInfoInData inData);

    void insertDeviceInfo(DeviceInfoInData inData);

    void editDeviceInfo(DeviceInfoInData inData);

    DeviceInfoOutData getDeviceInfoById(DeviceInfoInData inData);

    List<DeviceInfoOutData> selectBrDeviceList(DeviceInfoInData inData);

    void add(GaiaSdDeviceInfo deviceInfo);

    void update(GaiaSdDeviceInfo deviceInfo);

    void save(List<DeviceInfoInData> list, GetLoginOutData loginUser);

    List<DeviceInfoOutData> findList(DeviceForm deviceForm);

    List<UserVO> getUserNameList(DeviceForm deviceForm);

}
