package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@ApiModel
public class Saletask {
    /**
     * 客户ID
     */
    private String client;

    /**
     * 月份
     */
    @ApiModelProperty("月份")
    private String monthPlan;

    @ApiModelProperty("天数")
    private String daysPlan;

    /**
     * 门店编号
     */
    @ApiModelProperty("门店编号")
    private String stoCode;

    /**
     * 总计划营业额
     */
    @ApiModelProperty("总计划营业额")
    private BigDecimal tSaleAmt;


    /**
     * 总计划毛利额
     */
    @ApiModelProperty("总计划毛利额")
    private BigDecimal tSaleGross;



    /**
     * 总计划会员卡
     */
    @ApiModelProperty("总计划会员卡")
    private BigDecimal tMcardQty;



    /**
     * 日均计划营业额
     */
    @ApiModelProperty("日均计划营业额")
    private BigDecimal aSaleAmt;

    /**
     * 日均计划营业额
     */
    @ApiModelProperty("毛利额")
    private BigDecimal aSaleGross;

    /**
     * 日均计划会员卡
     */
    @ApiModelProperty("日均计划会员卡")
    private BigDecimal aMcardQty;

    @ApiModelProperty(value = "门店简称")
    private String stoName;
    @ApiModelProperty(value = "店型")
    private String state;
    @ApiModelProperty(value = "开业时间")
    private String openData;
    @ApiModelProperty(value = "销售天数")
    private String days;
    @ApiModelProperty(value = "累计达成营业额")
    private String monthAmtRate;
    @ApiModelProperty(value = "累计达成毛利额")
    private String monthGrossRate;
    @ApiModelProperty(value = "累计达成会员卡")
    private String monthCardRate;
    @ApiModelProperty(value = "累计达成营业额")
    private String dayAmtRate;
    @ApiModelProperty(value = "累计达成毛利额")
    private String dayGrossRate;
    @ApiModelProperty(value = "累计达成会员卡")
    private String dayCardRate;
    private Integer pageNum;
    private Integer pageSize;
}
