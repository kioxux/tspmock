package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class AppStoreSaleOutData implements Serializable {

    @ApiModelProperty(value = "门店编号")
    private String brId;

    @ApiModelProperty(value = "门店名称")
    private String brName;

    @ApiModelProperty(value = "动销品项")
    private Integer productCount;

    @ApiModelProperty(value = "销售额")
    private BigDecimal saleAmt;

    @ApiModelProperty(value = "毛利率")
    private BigDecimal profitRate;
}
