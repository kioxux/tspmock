package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/12 14:14
 */
@Data
public class YunNanPrescriptionInData {
    @ApiModelProperty("机构编号 易慧10000308：YN219（取值YunnanSuperviseUtil.instNo）")
    private String instNo;
    @ApiModelProperty("唯一编号 日期 + 机构编号 + 序列号（机构自定义）yyyyMMddYN001000001")
    private String seqNo;
    @ApiModelProperty("处方编号")
    private String prescriptionNo;
    @ApiModelProperty("开方医院")
    private String hospitalName;
    @ApiModelProperty("开方医生姓名")
    private String doctorName;
    @ApiModelProperty("开方时间yyyyMMdd")
    private String prescribeDate;
    @ApiModelProperty("药店编号")
    private String storeNo;
    @ApiModelProperty("药店名称")
    private String storeName;
    @ApiModelProperty("药店所在区域 88604")
    private String areaNo;
    @ApiModelProperty("病人姓名")
    private String patientName;
    @ApiModelProperty("病人性别0:男；1:女")
    private Integer patientGender;
    @ApiModelProperty("病人年龄")
    private Integer patientAge;
    @ApiModelProperty("病人联系方式")
    private String patientPhone;
    @ApiModelProperty("临床诊断")
    private String diagnosis;
    @ApiModelProperty("处方图片网络地址")
    private String imgUrl;
    @ApiModelProperty("审方药师注册证编号")
    private String registrationNo;
    @ApiModelProperty("审方时间yyyyMMddHHmmss")
    private String verificationTime;
    @ApiModelProperty("药品信息说明")
    private List<YunNanPrescriptionDrugInData> drugList;
}
