package com.gys.business.service;

import java.util.Map;

/**
 * @author
 */
public interface RabbitmqService {

    void send(String client, String brId, Map<String, Object> map);
}
