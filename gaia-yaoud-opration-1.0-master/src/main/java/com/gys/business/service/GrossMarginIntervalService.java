package com.gys.business.service;

import com.gys.business.service.data.CustomerOutData;
import com.gys.business.service.data.GrossMarginInData;
import com.gys.business.service.data.GrossMarginOutData;
import com.gys.business.service.data.GrossMarginOutDataCSV;
import com.gys.common.data.GetLoginOutData;

import java.util.List;

/**
 * 毛利区间划分
 *
 * @author Zhangchi
 * @since 2021/09/16/9:20
 */
public interface GrossMarginIntervalService {

    List<CustomerOutData> selectCustomer();

    List<GrossMarginOutData> selectGrossMarginInterval(GrossMarginInData inData);

    List<GrossMarginOutDataCSV> selectGrossMarginIntervalCSV(GrossMarginInData inData);

    void saveGrossMargin(GetLoginOutData userInfo,List<GrossMarginOutData> inDataList);

    void insertData();
}
