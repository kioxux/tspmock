package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ZjnbYbService;
import com.gys.business.service.data.MibSettlementZjnbDto;
import com.gys.business.service.data.SaleDDetail;
import com.gys.business.service.data.zjnb.MibProductMatchDto;
import com.gys.common.exception.BusinessException;
import com.gys.util.CopyUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ZjnbYbServiceImpl implements ZjnbYbService {
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd/HHmmss/");

    @Resource
    private MibSettlementZjnbMapper mibSettlementZjnbMapper;
    @Resource
    private GaiaSdNbMedicareMapper gaiaSdNbMedicareMapper;
    @Resource
    private GaiaNbybProCatalogMapper nbybProCatalogMapper;
    @Resource
    private GaiaSdSaleDMapper saleDMapper;
    @Resource
    private GaiaSdSaleHMapper saleHMapper;
    @Resource
    private GaiaSdSalePayMsgMapper payMsgMapper;


    @Override
    public void insertSaleInfo(Map<String, Object> inData) {
        mibSettlementZjnbMapper.insertSaleInfo(inData);
    }


    @Override
    public List<MibSettlementZjnb> selectSaleInfo(Map<String, Object> inData) {
        return mibSettlementZjnbMapper.selectSaleInfo(inData);
    }


    @Override
    public MibSettlementZjnb selectSaleInfoSum(Map<String, Object> inData) {
        return mibSettlementZjnbMapper.selectSaleInfoSum(inData);
    }

    @Override
    public void updateDetail(MibSettlementZjnb inData) {
        if (ObjectUtil.isEmpty(inData) && StringUtils.isEmpty(String.valueOf(inData.getGuid()))) {
            throw new BusinessException("请选择订单");
        }
        Example example = new Example(User.class);
        example.createCriteria()
                .andEqualTo("client", inData.getClient())
                .andEqualTo("stoCode", inData.getStoCode())
                .andEqualTo("detailSerialCode", inData.getDetailSerialCode());

        mibSettlementZjnbMapper.updateByExampleSelective(inData, example);
    }

    @Override
    public List<GaiaNbybProCatalog> getNbybProCatalog(Map inData) {
        return nbybProCatalogMapper.getNbybProCatalog(inData);
    }

    @Override
    public GaiaNbybProCatalog getNbybProCatalogOne(Map inData) {
        return nbybProCatalogMapper.getNbybProCatalogOne(inData);
    }

    @Override
    public List<MibProductMatchDto> getNBMibCodeByCode(Map<String, Object> inData) {
        if ("1".equals((String) inData.get("status"))) {
            return gaiaSdNbMedicareMapper.getNBMibCodeByCode((String) inData.get("client")
                    , (String) inData.get("brId")
                    , (String) inData.get("status")
                    , (String) inData.get("nameOrCode"));
        } else if ("0".equals((String) inData.get("status"))) {
            return gaiaSdNbMedicareMapper.getNBMibCodeByCode2((String) inData.get("client")
                    , (String) inData.get("brId")
                    , (String) inData.get("status")
                    , (String) inData.get("nameOrCode"));
        }
        return null;
    }

    @Override
    @Transactional
    public int submitMibMatchPro(String site, List<MibProductMatchDto> matchList) {
        if (CollUtil.isNotEmpty(matchList)) {
            gaiaSdNbMedicareMapper.insertOrUpdateMedPrd(matchList);
            gaiaSdNbMedicareMapper.update(site, matchList);
            return 1;
        } else {
            throw new BusinessException("商品列表为空");
        }
    }

    @Override
    public int submitMibMatchPro2(String site, List<MibProductMatchDto> matchList) {
        if (CollUtil.isNotEmpty(matchList)) {
            return gaiaSdNbMedicareMapper.insertOrUpdateMedPrd2(site, matchList);
        } else {
            throw new BusinessException("商品列表为空");
        }
    }

    @Override
    public List<MibProductMatchDto> getNBMibCodeByWzbmCode(Map<String, Object> inData) {
        return gaiaSdNbMedicareMapper.getNBMibCodeByWzbmCode((String) inData.get("client")
                , (String) inData.get("brId")
                , (String) inData.get("status")
                , (String) inData.get("nameOrCode"));
    }

    @Override
    public void saveNBMibStockCode(Map<String, Object> inData) {
        gaiaSdNbMedicareMapper.saveNBMibStockCode((String) inData.get("client")
                , (String) inData.get("brId")
                , (List<MibProductMatchDto>) inData.get("proList"));
    }

    @Override
    public MibSettlementZjnb accessToHealthCareInformation(Map<String, String> inData) {
        List<GaiaSdSaleH> hList = this.saleHMapper.getBillOrReturnBill(inData);
        if (CollUtil.isNotEmpty(hList)) {
            for (GaiaSdSaleH saleH : hList) {
                if (inData.get("bill").equals(saleH.getGsshBillNo()) && StrUtil.isBlank(saleH.getGsshBillNoReturn())) { //正常销售
                    return mibSettlementZjnbMapper.accessToHealthCareInformation(inData); //销售
                }  else if (inData.get("bill").equals(saleH.getGsshBillNo()) && StrUtil.isNotBlank(saleH.getGsshBillNoReturn())) {//退货
                    return mibSettlementZjnbMapper.accessToHealthCareInformation2(inData);  //表示退货
                }
            }
        }
        return null;
    }

    @Override
    public MibSettlementZjnbDto retypeItInvoice(Map<String, String> inData) {
        MibSettlementZjnbDto zjnbDto = null;
        MibSettlementZjnb access = mibSettlementZjnbMapper.accessToHealthCareInformation(inData);
        // 原退货单不是空 代表是退货单
        if (StringUtils.isNotEmpty(access.getDetailSerialCodeBef())){
            inData.put("thFlag","Y");
        }
        if (ObjectUtil.isNotEmpty(access)) {
            zjnbDto = CopyUtil.copy(access, MibSettlementZjnbDto.class);
            List<SaleDDetail> saleDList = this.saleDMapper.retypeItInvoice(inData);
            if (CollUtil.isEmpty(saleDList)) {
                throw new BusinessException("销售数据异常,请联系工作人员!");
            }
            zjnbDto.setUserName(saleDList.get(0).getUserName());
            zjnbDto.setSaleDList(saleDList);
        }
        return zjnbDto;
    }

    @Override
    public void insert(MibSettlementZjnb zjnb) {
//        Example saleDExample = new Example(MibSettlementZjnb.class);
        Map<String, Object> inData = new HashMap<>();
        inData.put("client", zjnb.getClient());
        inData.put("stoCode", zjnb.getStoCode());
        inData.put("detailSerialCode", zjnb.getDetailSerialCode());
        List<MibSettlementZjnb> mibSettlementZjnbs = mibSettlementZjnbMapper.selectSaleInfo(inData);
        if(CollUtil.isEmpty(mibSettlementZjnbs)){
            this.mibSettlementZjnbMapper.insertDetail(zjnb);
        }
    }
}
