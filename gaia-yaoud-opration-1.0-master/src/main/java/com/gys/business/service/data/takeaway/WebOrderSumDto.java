package com.gys.business.service.data.takeaway;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zhangdong
 * @date 2021/7/21 17:44
 */
@Data
public class WebOrderSumDto {

    @ApiModelProperty("门店去重计数")
    private long stoCodeSum;
    @ApiModelProperty("订单平台去重计数")
    private long platformSum;
    @ApiModelProperty("平台单号计数")
    private long platformOrderIdSum;
    @ApiModelProperty("本地单号计数")
    private long nativeOrderIdSum;
    @ApiModelProperty("平台零售总额")
    private BigDecimal platformAmtSum;
    @ApiModelProperty("平台折扣金额")
    private BigDecimal platformZkSum;
    @ApiModelProperty("平台配送费")
    private BigDecimal platformFeeSum;
    @ApiModelProperty("平台使用费")
    private BigDecimal platformUseSum;
    @ApiModelProperty("平台实收金额")
    private BigDecimal platformActualSum;
    @ApiModelProperty("顾客实付金额汇总")
    private BigDecimal customerPaySum;
    @ApiModelProperty("销售单号去重计数")
    private long saleOrderIdSum;
    @ApiModelProperty("销售零售总额")
    private BigDecimal saleAmtSum;
    @ApiModelProperty("销售实收金额")
    private BigDecimal saleActualSum;
    @ApiModelProperty("销售折扣金额汇总")
    private BigDecimal saleZkSum;


}
