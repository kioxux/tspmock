package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdStock;
import com.gys.business.service.data.StockInData;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 库存Service
 * @author chenhao
 */
public interface StockService {
    /**
     * 消耗门店库存
     * @param stockInData
     */
    Object[] consumeShopStock(StockInData stockInData, BigDecimal owingStock, boolean hasNext);

    List<Map<String,  Object>> getPlatformParamMap(List<GaiaSdStock> list);

    /**
     * 新增门店库存
     * @param stockInData
     */
    Object[] addShopStock(StockInData stockInData);

    void syncStock(List<GaiaSdStock> stockList);
}
