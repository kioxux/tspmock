package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class AppStoreSaleInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "周或月 1-月 2-周 传空默认为月")
    private String weekOrMonth;

    @ApiModelProperty(value = "排序标志 1-升序 2-降序 传空默认为降序")
    private String orderByFlag;

    @ApiModelProperty(value = "销售报表（排序字段 1-动销品项 2-销售额 3-毛利率 传空默认为销售额）  / 库存报表（1-库存品项 2-库存成本额 3-周转天数 传空默认为 2库存成本额）")
    private String orderByCondition;

    @ApiModelProperty(value = "排序前后 0-全部 1-前(从大到小) 2-后(从小到大)")
    private String topOrLast;

    @ApiModelProperty(value = "查询条数 传空默认10条")
    private Integer pageSize;
}
