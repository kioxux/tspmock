package com.gys.business.service;

import com.gys.business.service.data.SalesInquireData;

import java.util.Map;

/**
 * @author xiaoyuan
 */
public interface TianLingSReturnService {

    /**
     * 天灵退货保存
     * @param inData
     */
    Map<String, Object> tianLingReturn(SalesInquireData inData);
}
