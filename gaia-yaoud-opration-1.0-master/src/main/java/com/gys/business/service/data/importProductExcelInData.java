//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;

public class importProductExcelInData implements Serializable {
    private String gspgId;
    private String gspgProId;

    public importProductExcelInData() {
    }

    public String getGspgId() {
        return this.gspgId;
    }

    public String getGspgProId() {
        return this.gspgProId;
    }

    public void setGspgId(final String gspgId) {
        this.gspgId = gspgId;
    }

    public void setGspgProId(final String gspgProId) {
        this.gspgProId = gspgProId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof importProductExcelInData)) {
            return false;
        } else {
            importProductExcelInData other = (importProductExcelInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$gspgId = this.getGspgId();
                Object other$gspgId = other.getGspgId();
                if (this$gspgId == null) {
                    if (other$gspgId != null) {
                        return false;
                    }
                } else if (!this$gspgId.equals(other$gspgId)) {
                    return false;
                }

                Object this$gspgProId = this.getGspgProId();
                Object other$gspgProId = other.getGspgProId();
                if (this$gspgProId == null) {
                    if (other$gspgProId != null) {
                        return false;
                    }
                } else if (!this$gspgProId.equals(other$gspgProId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof importProductExcelInData;
    }

    public int hashCode() {

        int result = 1;
        Object $gspgId = this.getGspgId();
        result = result * 59 + ($gspgId == null ? 43 : $gspgId.hashCode());
        Object $gspgProId = this.getGspgProId();
        result = result * 59 + ($gspgProId == null ? 43 : $gspgProId.hashCode());
        return result;
    }

    public String toString() {
        return "importProductExcelInData(gspgId=" + this.getGspgId() + ", gspgProId=" + this.getGspgProId() + ")";
    }
}
