package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaAuthconfiDataMapper;
import com.gys.business.mapper.GaiaProductClassMapper;
import com.gys.business.mapper.GaiaSdAppSaleRecordMapper;
import com.gys.business.mapper.GaiaSdSaleHMapper;
import com.gys.business.mapper.entity.GaiaSdAppSaleRecord;
import com.gys.business.service.AppSaleReportService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.constant.ProPositionConstant;
import com.gys.common.data.JsonResult;
import com.gys.common.data.app.salereport.ProBigClass;
import com.gys.common.data.app.salereport.ProPositionDTO;
import com.gys.common.data.app.salereport.ProPositionSummaryVO;
import com.gys.common.enums.StorePositionEnum;
import com.gys.common.exception.BusinessException;
import com.gys.util.BigDecimalUtil;
import com.gys.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * @author SunJiaNan
 * @version 1.0
 * @date 2021/8/30 10:38
 * @description app销售报表实现类
 */
@Service
@Slf4j
public class AppSaleReportServiceImpl implements AppSaleReportService {

    @Autowired
    private GaiaSdSaleHMapper saleHMapper;
    @Autowired
    private GaiaSdAppSaleRecordMapper appSaleRecordMapper;
    @Autowired
    private GaiaAuthconfiDataMapper authconfiDataMapper;

    @Resource
    private GaiaProductClassMapper gaiaProductClassMapper;

    @Override
    public JsonResult getChartInfo(ProductBigTypeSaleInData inData) {
        String weekOrMonth = inData.getWeekOrMonth();
        //默认为 月
        if (ValidateUtil.isEmpty(weekOrMonth)) {
            inData.setWeekOrMonth(CommonConstant.YEAR_OR_MONTH_1);
        }
        //获取图表基础信息
        List<ProductBigTypeSaleOutData> bigTypeSaleList = getBigTypeSaleDetailList(inData);
        if (ValidateUtil.isEmpty(bigTypeSaleList)) {
            return JsonResult.success(bigTypeSaleList, "success");
        }

        //将list转为Map  key为大类编码 value为对应的实体
        Map<String, ProductBigTypeSaleOutData> bigTypeSaleMap = bigTypeSaleList.stream().collect(Collectors.toMap(ProductBigTypeSaleOutData::getBigCode, item -> item));
        SaleChartOutData chartData = null;
        if (ValidateUtil.isNotEmpty(bigTypeSaleMap)) {
            //动销品相图表数据
            chartData = getAllChartData(bigTypeSaleMap);
        }

        //动销天数
        SaleChartOutData movInfo = saleHMapper.getMovInfo(inData);
        chartData.setMovDays(movInfo.getMovDays());
        //动销门店数
        chartData.setMovStoreCount(movInfo.getMovStoreCount());
        return JsonResult.success(chartData, "success");
    }


    /**
     * 获取动销品相图表数据
     *
     * @param bigTypeSaleMap
     * @return
     */
    private SaleChartOutData getAllChartData(Map<String, ProductBigTypeSaleOutData> bigTypeSaleMap) {
        //非药品编码  4-9
        ProductBigTypeSaleOutData bigType1 = bigTypeSaleMap.get("1");     //1-处方药
        ProductBigTypeSaleOutData bigType2 = bigTypeSaleMap.get("2");     //2-OTC
        ProductBigTypeSaleOutData bigType3 = bigTypeSaleMap.get("3");     //3-中药
        ProductBigTypeSaleOutData bigType4 = bigTypeSaleMap.get("4");     //4-保健食品
        ProductBigTypeSaleOutData bigType5 = bigTypeSaleMap.get("5");     //5-医疗器械及护理
        ProductBigTypeSaleOutData bigType6 = bigTypeSaleMap.get("6");     //6-个人护理
        ProductBigTypeSaleOutData bigType7 = bigTypeSaleMap.get("7");     //7-家庭用品
        ProductBigTypeSaleOutData bigType8 = bigTypeSaleMap.get("8");     //8-赠品
        ProductBigTypeSaleOutData bigType9 = bigTypeSaleMap.get("9");     //9-其它
        ProductBigTypeSaleOutData bigType888 = bigTypeSaleMap.get(CommonConstant.SUBTOTAL_CODE_888);     //888-药品-小计
        ProductBigTypeSaleOutData bigType999 = bigTypeSaleMap.get(CommonConstant.TOTAL_CODE_999);        //999-合计
        //出参
        SaleChartOutData saleChart = new SaleChartOutData();
        //1.动销品项
        //动销品项  非药品
        Integer movNoDrug = bigType4.getProductCount() + bigType5.getProductCount() + bigType6.getProductCount()
                + bigType7.getProductCount() + bigType8.getProductCount() + bigType9.getProductCount();
        saleChart.setMovNoDrug(movNoDrug);
        //动销品项  中药
        saleChart.setMovChineseDrug(bigType3.getProductCount());
        //动销品项  药品-小计
        saleChart.setMovDrug(bigType888.getProductCount());
        //动销品项 处方药
        saleChart.setMovPrescription(bigType1.getProductCount());
        //动销品项  OTC
        saleChart.setMovOTC(bigType2.getProductCount());

        //2.销售占比
        //销售占比  中药
        saleChart.setSaleAmtChineseDrugProp(bigType3.getSaleAmtProp().setScale(0, BigDecimal.ROUND_HALF_UP));
        //销售占比  药品
        saleChart.setSaleAmtDrugProp(bigType888.getSaleAmtProp().setScale(0, BigDecimal.ROUND_HALF_UP));
        //销售占比  处方药
        saleChart.setSaleAmtPrescriptionProp(bigType1.getSaleAmtProp().setScale(0, BigDecimal.ROUND_HALF_UP));
        //销售占比  OTC
        saleChart.setSaleAmtOTCProp(bigType2.getSaleAmtProp().setScale(0, BigDecimal.ROUND_HALF_UP));
        //销售占比  非药品
        /*BigDecimal saleAmtNoDrugProp = bigType4.getSaleAmtProp().add(bigType5.getSaleAmtProp()).add(bigType6.getSaleAmtProp()).add(bigType7.getSaleAmtProp())
                .add(bigType8.getSaleAmtProp()).add(bigType9.getSaleAmtProp());*/
        BigDecimal saleAmtNoDrugProp = new BigDecimal(100).subtract(saleChart.getSaleAmtPrescriptionProp()).subtract(saleChart.getSaleAmtOTCProp()).subtract(saleChart.getSaleAmtChineseDrugProp());
        saleChart.setSaleAmtNoDrugProp(saleAmtNoDrugProp.setScale(0, BigDecimal.ROUND_HALF_UP));

        //3.毛利率
        //毛利率  合计
        saleChart.setProfitRateTotal(bigType999.getProfitRate());
        //毛利率  处方药
        saleChart.setProfitRatePrescription(bigType1.getProfitRate());
        //毛利率  OTC
        saleChart.setProfitRateOTC(bigType2.getProfitRate());
        //毛利率  药品-小计
        saleChart.setProfitRateDrug(bigType888.getProfitRate());
        //毛利率  中药
        saleChart.setProfitRateChineseDrug(bigType3.getProfitRate());
        //毛利率  非药品  = 非药品毛利额 / 非药品销售额 * 100
        //获取非药品毛利额
        BigDecimal profitAmt = bigType4.getProfitAmt().add(bigType5.getProfitAmt()).add(bigType6.getProfitAmt())
                .add(bigType7.getProfitAmt()).add(bigType8.getProfitAmt()).add(bigType9.getProfitAmt());
        //获取非药品销售额
        BigDecimal saleAmt = bigType4.getSaleAmt().add(bigType5.getSaleAmt()).add(bigType6.getSaleAmt())
                .add(bigType7.getSaleAmt()).add(bigType8.getSaleAmt()).add(bigType9.getSaleAmt());

        BigDecimal noDrugProfitRate = BigDecimal.ZERO;
        if (BigDecimal.ZERO.compareTo(saleAmt) != 0) {
            //做除法  非药品毛利额 / 非药品销售额 * 100
            noDrugProfitRate = profitAmt.divide(saleAmt, 5, BigDecimal.ROUND_HALF_UP);
        }
        //最终非药品毛利率
        saleChart.setProfitRateNoDrug(new BigDecimal(100).multiply(noDrugProfitRate).setScale(0, BigDecimal.ROUND_HALF_UP));
        return saleChart;
    }


    /**
     * 大类销售情况列表
     *
     * @param inData
     * @return
     */
    @Override
    public JsonResult getBigTypeSaleList(ProductBigTypeSaleInData inData) {
        String weekOrMonth = inData.getWeekOrMonth();
        //默认为 月
        if (ValidateUtil.isEmpty(weekOrMonth)) {
            inData.setWeekOrMonth(CommonConstant.YEAR_OR_MONTH_1);
        }
        //销售大类情况
        List<ProductBigTypeSaleOutData> bigTypeSaleList = getBigTypeSaleDetailList(inData);
        return JsonResult.success(bigTypeSaleList, "success");
    }


    /**
     * 处理大类销售情况
     *
     * @param inData
     * @return
     */
    private List<ProductBigTypeSaleOutData> getBigTypeSaleDetailList(ProductBigTypeSaleInData inData) {
        //开启线程1
        CompletableFuture<List<ProductBigTypeSaleOutData>> resultFir = CompletableFuture.supplyAsync(() ->{
            log.info("线程1开始");
            long start = System.currentTimeMillis();
            //查询销售大类情况
            List<ProductBigTypeSaleOutData> productBigTypeSaleList = saleHMapper.getProductBigTypeSaleList(inData);
            log.info("线程1结束，共耗时：" + (System.currentTimeMillis() - start) + "毫秒");
            return productBigTypeSaleList;
        });

        //开启线程2
        CompletableFuture<ProductBigTypeSaleOutData> resultSed = CompletableFuture.supplyAsync(() ->{
            log.info("线程2开始");
            long start = System.currentTimeMillis();
            //计算药品小计
            ProductBigTypeSaleOutData drugSubTotalInfo = saleHMapper.getDrugSubTotalInfo(inData);
            log.info("线程2结束，共耗时：" + (System.currentTimeMillis() - start) + "毫秒");
            return drugSubTotalInfo;
        });
        CompletableFuture.allOf(resultFir, resultSed).join();   //等待后执行完的线程结束


        List<ProductBigTypeSaleOutData> productBigTypeSaleList = null;
        ProductBigTypeSaleOutData drugSubTotalInfo = null;
        try {
            productBigTypeSaleList = resultFir.get();
            drugSubTotalInfo = resultSed.get();
        } catch (Exception e) {
            log.error("销售报表查询异常：", e.getMessage());
            throw new BusinessException("提示：查询销售报表失败！");
        }

        if (ValidateUtil.isEmpty(productBigTypeSaleList)) {
            return productBigTypeSaleList;
        }
        //销售总金额
        BigDecimal totalAmt = productBigTypeSaleList.stream().map(ProductBigTypeSaleOutData::getSaleAmt).reduce(BigDecimal.ZERO, BigDecimal::add);

        //计算销售占比
        BigDecimal saleAmtProp = null;
        for (ProductBigTypeSaleOutData saleInfo : productBigTypeSaleList) {
            BigDecimal saleAmt = saleInfo.getSaleAmt();
            //如果销售额是否为空 或者 为 0 则不直接为0
            if (ValidateUtil.isNotEmpty(totalAmt) && BigDecimal.ZERO.compareTo(totalAmt) != 0) {
                //计算销售占比
                BigDecimal tempSaleAmtProp = saleAmt.divide(totalAmt, 4, BigDecimal.ROUND_HALF_UP);
                saleAmtProp = tempSaleAmtProp.multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP);
            } else {
                saleAmtProp = BigDecimal.ZERO;
            }
            //销售占比赋值 保留1位小数
            saleInfo.setSaleAmtProp(saleAmtProp.setScale(1, BigDecimal.ROUND_HALF_UP));
            //销售额 取整
            saleInfo.setSaleAmt(saleAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
        }

        //返回参数
        List<ProductBigTypeSaleOutData> resultList = new ArrayList<>(productBigTypeSaleList.size());
        //计算合计
        ProductBigTypeSaleOutData totalInfo = getTotalInfo(productBigTypeSaleList, totalAmt);
        resultList.add(totalInfo);
        //药品小计
        if (ValidateUtil.isNotEmpty(drugSubTotalInfo)) {
            BigDecimal saleAmt = drugSubTotalInfo.getSaleAmt();    //获取小计销售额
            if(ValidateUtil.isNotEmpty(totalAmt) && BigDecimal.ZERO.compareTo(totalAmt) != 0) {
                drugSubTotalInfo.setSaleAmtProp(saleAmt.divide(totalAmt, 5, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP));
            } else {
                drugSubTotalInfo.setSaleAmtProp(BigDecimal.ZERO);
            }
            resultList.add(drugSubTotalInfo);
        }
        //大类销售情况列表（除去合计 除去小计）
        resultList.addAll(productBigTypeSaleList);
        return resultList;
    }


    /**
     * 计算合计
     *
     * @param productBigTypeSaleList
     * @param totalAmt
     * @return
     */
    private ProductBigTypeSaleOutData getTotalInfo(List<ProductBigTypeSaleOutData> productBigTypeSaleList, BigDecimal totalAmt) {
        //计算合计
        ProductBigTypeSaleOutData total = new ProductBigTypeSaleOutData();
        total.setBigCode(CommonConstant.TOTAL_CODE_999);
        total.setBigName(CommonConstant.TOTAL_DESC);
        total.setProductCount(productBigTypeSaleList.stream().mapToInt(ProductBigTypeSaleOutData::getProductCount).sum());
        total.setSaleAmt(totalAmt.setScale(0, BigDecimal.ROUND_HALF_UP));

        //如果总销售额为0 则销售占比为100
        if (BigDecimal.ZERO.compareTo(totalAmt) == 0) {
            total.setSaleAmtProp(BigDecimal.ZERO);
        } else {
            total.setSaleAmtProp(new BigDecimal(100));
        }
        total.setProfitRate(getTotalProfitAmt(productBigTypeSaleList, totalAmt));    //计算毛利率
        return total;
    }


    /**
     * 获取合计毛利额
     *
     * @param productBigTypeSaleList
     * @param totalAmt
     * @return
     */
    private BigDecimal getTotalProfitAmt(List<ProductBigTypeSaleOutData> productBigTypeSaleList, BigDecimal totalAmt) {
        BigDecimal totalProfitAmt = productBigTypeSaleList.stream()
                .map(ProductBigTypeSaleOutData::getProfitAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        //如果总销售额为空 则毛利为0
        if (ValidateUtil.isEmpty(totalAmt) || BigDecimal.ZERO.compareTo(totalAmt) == 0) {
            return BigDecimal.ZERO;
        }
        BigDecimal totalProfitRate = totalProfitAmt.divide(totalAmt, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
        return totalProfitRate.setScale(0, BigDecimal.ROUND_HALF_UP);
    }


    @Override
    public JsonResult getChartInfoByBigCode(ProductBigTypeSaleInData inData) {
        String weekOrMonth = inData.getWeekOrMonth();
        String bigCode = inData.getBigCode();
        //默认为 月
        if (ValidateUtil.isEmpty(weekOrMonth)) {
            inData.setWeekOrMonth(CommonConstant.YEAR_OR_MONTH_1);
        }
        //默认 为药品小计
        if (ValidateUtil.isEmpty(bigCode)) {
            inData.setBigCode(CommonConstant.SUBTOTAL_CODE_888);
        }
        List<SaleChartInfoOutData> chartInfoList = saleHMapper.getChartInfoByBigCode(inData);
        return JsonResult.success(chartInfoList, "success");
    }


    @Override
    public JsonResult getMidTypeSaleList(ProductBigTypeSaleInData inData) {
        String weekOrMonth = inData.getWeekOrMonth();
        String bigCode = inData.getBigCode();
        //默认为-月
        if (ValidateUtil.isEmpty(weekOrMonth)) {
            inData.setWeekOrMonth(CommonConstant.YEAR_OR_MONTH_1);
        }
        //默认为-药品小计
        if (ValidateUtil.isEmpty(bigCode)) {
            inData.setBigCode(CommonConstant.SUBTOTAL_CODE_888);
        }
        List<ProductBigTypeSaleOutData> productMidTypeSaleList = saleHMapper.getProductMidTypeSaleList(inData);

        //总销售额
        BigDecimal totalAmt = productMidTypeSaleList.stream().map(ProductBigTypeSaleOutData::getSaleAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        //计算销售占比
        BigDecimal saleAmtProp = null;
        for (ProductBigTypeSaleOutData saleInfo : productMidTypeSaleList) {
            BigDecimal saleAmt = saleInfo.getSaleAmt();
            //如果销售额是否为空 或者 为 0 则不直接为0
            if (ValidateUtil.isNotEmpty(totalAmt) && BigDecimal.ZERO.compareTo(totalAmt) != 0) {
                //计算销售占比
                BigDecimal tempSaleAmtProp = saleAmt.divide(totalAmt, 4, BigDecimal.ROUND_HALF_UP);
                saleAmtProp = tempSaleAmtProp.multiply(new BigDecimal(100)).setScale(1, BigDecimal.ROUND_HALF_UP);
            } else {
                saleAmtProp = BigDecimal.ZERO;
            }
            //销售占比赋值 保留1位小数
            saleInfo.setSaleAmtProp(saleAmtProp.setScale(1, BigDecimal.ROUND_HALF_UP));
            //销售额 取整
            saleInfo.setSaleAmt(saleAmt.setScale(0, BigDecimal.ROUND_HALF_UP));
        }

        //返回参数
        List<ProductBigTypeSaleOutData> resultList = new ArrayList<>();
        //计算合计
        ProductBigTypeSaleOutData totalInfo = getTotalInfo(productMidTypeSaleList, totalAmt);
        resultList.add(totalInfo);
        resultList.addAll(productMidTypeSaleList);
        return JsonResult.success(resultList, "success");
    }


    /**
     * 获取所有大类编码
     *
     * @return
     */
    private List<String> getBigCodeList() {
        //所有大类编码
        List<String> bigCodeList = new ArrayList<>();
        bigCodeList.add("1");       //处方药
        bigCodeList.add("2");       //OTC
        bigCodeList.add("3");       //中药
        bigCodeList.add("888");     //药品小计
        bigCodeList.add("999");     //非药品
        return bigCodeList;
    }


    /**
     * 跑批当年历史数据
     */
    @Override
    @Transactional
    public void initSaleData() {
        //查询当前所有加盟商
        List<String> clientList = saleHMapper.getAllClientInfo();
        //获取所有大类编码
        List<String> bigCodeList = getBigCodeList();
        Date now = new Date();

        //删除当年所有数据
        appSaleRecordMapper.deleteDataByYear();

        List<GaiaSdAppSaleRecord> insertRecordList = new ArrayList<>();
        for (String client : clientList) {
            for (String bigCode : bigCodeList) {
                //获取指定加盟商下 指定商品类型所有销售数据（实时数据）
                List<SaleChartInfoOutData> saleDataRecordList = saleHMapper.getSaleDataRecordForCurrent(client, bigCode, null);
                //组装参数
                for (SaleChartInfoOutData saleInfo : saleDataRecordList) {
                    GaiaSdAppSaleRecord record = new GaiaSdAppSaleRecord();
                    record.setClient(client);
                    record.setYears(saleInfo.getWeekOrMonth().substring(0, 4));
                    record.setMonths(saleInfo.getWeekOrMonth());
                    record.setProductType(Integer.valueOf(bigCode));
                    record.setMovProductCount(saleInfo.getMovCount().toString());
                    record.setSaleAmt(saleInfo.getSaleAmt());
                    record.setProfitRate(saleInfo.getProfitRate());
                    record.setCreateTime(now);
                    insertRecordList.add(record);
                }
            }
        }
        appSaleRecordMapper.batchInsert(insertRecordList);
    }


    @Override
    public JsonResult getStoreList(AppStoreSaleInData inData) {
        String weekOrMonth = inData.getWeekOrMonth();     //周或月 1-月 2-周 传空默认为月
        String orderByFlag = inData.getOrderByFlag();     //排序标志 1-升序 2-降序 传空默认为降序
        String topOrLast = inData.getTopOrLast();         //排序前后 0-全部 1-前(从大到小) 2-后(从小到大)
        String orderByCondition = inData.getOrderByCondition();    //排序字段 1-动销品项 2-销售额 3-毛利率 传空默认为销售额
        Integer pageSize = inData.getPageSize();          //传空默认10条
        if (ValidateUtil.isEmpty(weekOrMonth)) {
            inData.setWeekOrMonth(CommonConstant.YEAR_OR_MONTH_1);
        }
        if (ValidateUtil.isEmpty(orderByFlag)) {
            inData.setOrderByFlag("2");
        }
        if (ValidateUtil.isEmpty(orderByCondition)) {
            inData.setOrderByCondition("2");
        }
        if (ValidateUtil.isEmpty(topOrLast)) {
            inData.setTopOrLast("0");
        }
        if (ValidateUtil.isEmpty(pageSize)) {
            inData.setPageSize(10);
        }

        List<AppStoreSaleOutData> storeSaleList = saleHMapper.getStoreSaleList(inData);
        return JsonResult.success(storeSaleList, "success");
    }


    @Override
    public JsonResult getGroupOrScManagerList(String client, String userId) {
        Map<String, String> result = new HashMap<>(2);
        //查询是否公司负责人
        List<Map<String, Object>> groupManagerList =
                authconfiDataMapper.getGroupOrScManagerList(client, userId, "GAIA_AL_MANAGER");
        //如果查询为空 表示不是公司负责人
        result.put("groupManager", (ValidateUtil.isEmpty(groupManagerList) ? "0" : "1"));
        //查询是否商采负责人
        List<Map<String, Object>> scManagerList =
                authconfiDataMapper.getGroupOrScManagerList(client, userId, "GAIA_MM_SCZG");
        result.put("scManager", (ValidateUtil.isEmpty(scManagerList) ? "0" : "1"));
        return JsonResult.success(result, "success");
    }

    @Override
    public JsonResult selectProPositionSummaryCategory() {
        List<SelectData> selectDatas = new LinkedList<>();
        for (ProBigClass proBigClass : ProPositionConstant.CATEGORY) {
            SelectData selectData = new SelectData();
            selectData.setId(proBigClass.getId());
            selectData.setName(proBigClass.getName());
            selectDatas.add(selectData);
        }
        return JsonResult.success(selectDatas);
    }

    @Override
    public JsonResult selectProPositionSummary(ProPositionDTO proPositionDTO) {
        Integer dateDimension = proPositionDTO.getDateDimension();

        // 获取商品大类
        List<String> proBigClass = wrapperProBigClass(proPositionDTO.getBigClass());
        proPositionDTO.setBigClasses(proBigClass);
        List<ProPositionSummaryVO> proPositionSummaryVOS = new ArrayList<>();
        if (dateDimension == 1) {
            // 上月
            // 获取商品大类，商品定位统计金额
            proPositionSummaryVOS = saleHMapper.selectStorePositionSummaryWithMonth(proPositionDTO);
        } else if (dateDimension == 2) {
            // 上周
            // 获取商品大类，商品定位统计金额
            proPositionSummaryVOS = saleHMapper.selectStorePositionSummaryWithWeek(proPositionDTO);
            List<ProPositionSummaryVO> storePositionSummaryProCountWithWeek = saleHMapper.selectStorePositionSummaryProCountWithWeek(proPositionDTO);
            Map<String, Integer> storePositionSummaryProCountWithWeekMap = storePositionSummaryProCountWithWeek.stream()
                    .collect(Collectors.toMap(ProPositionSummaryVO::getProPosition, ProPositionSummaryVO::getProductCount));
            for (ProPositionSummaryVO proPositionSummaryVO : proPositionSummaryVOS) {
                proPositionSummaryVO.setProductCount(storePositionSummaryProCountWithWeekMap.getOrDefault(proPositionSummaryVO.getProPosition(), 0));
            }
        }

        // 根据商品定位分组
        Map<String, ProPositionSummaryVO> collect = proPositionSummaryVOS.stream()
                .collect(Collectors.toMap(ProPositionSummaryVO::getProPosition, s -> s));

        // 销售额之和
        BigDecimal saleAmtTotal = proPositionSummaryVOS.stream()
                .map(ProPositionSummaryVO::getSaleAmt)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        StorePositionEnum[] values = StorePositionEnum.values();
        List<ProPositionSummaryVO> result = new ArrayList<>();
        ProPositionSummaryVO proPositionSummary;
        // 合计动销品项数
        Integer productCountTotal = 0;
        // 合计毛利额
        BigDecimal profitAmtTotal = BigDecimal.ZERO;
        for (StorePositionEnum value : values) {
            proPositionSummary = new ProPositionSummaryVO();
            proPositionSummary.setProPositionName(value.getName());
            ProPositionSummaryVO tempProPositionSummary = collect.get(value.getType());
            if (tempProPositionSummary != null) {
                // 动销品项数
                Integer productCount = tempProPositionSummary.getProductCount();
                productCountTotal += productCount;
                proPositionSummary.setProductCount(productCount);
                // 销售额
                BigDecimal saleAmt = tempProPositionSummary.getSaleAmt();
                proPositionSummary.setSaleAmt(saleAmt);
                // 销售占比 该项销售额/计算范围内的销售额之和
                BigDecimal percentageOfSales = BigDecimalUtil.divideWithPercent(saleAmt, saleAmtTotal, 4);
                proPositionSummary.setPercentageOfSales(percentageOfSales);
                // 毛利额
                BigDecimal profitAmt = tempProPositionSummary.getProfitAmt();
                profitAmtTotal = profitAmtTotal.add(profitAmt);
                // 毛利率：毛利额/销售额
                proPositionSummary.setProfitRate(BigDecimalUtil.divideWithPercent(profitAmt, saleAmt, 2));
            }
            result.add(proPositionSummary);
        }
        // 计算合计
        proPositionSummary = new ProPositionSummaryVO();
        // 动销品项数
        proPositionSummary.setProductCount(productCountTotal);
        // 销售额
        proPositionSummary.setSaleAmt(saleAmtTotal);
        // 销售占比 该项销售额/计算范围内的销售额之和
        proPositionSummary.setPercentageOfSales(BigDecimalUtil.divide(saleAmtTotal, saleAmtTotal).multiply(BigDecimalUtil.ONE_HUNDRED));
        // 毛利率：毛利额/销售额
        proPositionSummary.setProfitRate(BigDecimalUtil.divideWithPercent(profitAmtTotal, saleAmtTotal, 2));
        proPositionSummary.setProPositionName("合计");
        result.add(0, proPositionSummary);
        return JsonResult.success(result);
    }

    private List<String> wrapperProBigClass(String bigClass) {
        if (StringUtils.isNotBlank(bigClass)) {
            ProBigClass proBigClass = ProPositionConstant.CATEGORY.stream()
                    .filter(item -> item.getId().equals(bigClass))
                    .collect(Collectors.toList()).get(0);
            return proBigClass.getProBigClasses();
        }
        return Collections.emptyList();
    }

}
