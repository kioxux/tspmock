//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "销售班次记录表")
public class GetSaleScheduleInData implements Serializable {
    private static final long serialVersionUID = -8599651801018827232L;
    @ApiModelProperty(value = "加盟商", required = true)
    private String clientId;
    @ApiModelProperty(value = "门店", required = true)
    private String gsegBrId;
    private String gsegBrName;
    private String gsegId;
    private String gsegName;
    private String gsegNameId;
    private String gsegBeginTime;
    private String gsegEndTime;
    private String gsegEmpId1;
    private String gsegEmpId2;
    private String gsegEmpId3;
    private String gsegEmpId4;
    private String gsegEmpId5;
    private String gsegUpdateDate;
    private String gsegUpdateEmp;
}
