//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetSyncSdEmpGroupOutData {
    private String clientId;
    private String gsegId;
    private String gsegName;
    private String gsegBrId;
    private String gsegBrName;
    private String gsegBeginTime;
    private String gsegEndTime;
    private String gsegEmp1;
    private String gsegEmp2;
    private String gsegEmp3;
    private String gsegEmp4;
    private String gsegEmp5;
    private String gsegUpdateDate;
    private String gsegUpdateEmp;

    public GetSyncSdEmpGroupOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsegId() {
        return this.gsegId;
    }

    public String getGsegName() {
        return this.gsegName;
    }

    public String getGsegBrId() {
        return this.gsegBrId;
    }

    public String getGsegBrName() {
        return this.gsegBrName;
    }

    public String getGsegBeginTime() {
        return this.gsegBeginTime;
    }

    public String getGsegEndTime() {
        return this.gsegEndTime;
    }

    public String getGsegEmp1() {
        return this.gsegEmp1;
    }

    public String getGsegEmp2() {
        return this.gsegEmp2;
    }

    public String getGsegEmp3() {
        return this.gsegEmp3;
    }

    public String getGsegEmp4() {
        return this.gsegEmp4;
    }

    public String getGsegEmp5() {
        return this.gsegEmp5;
    }

    public String getGsegUpdateDate() {
        return this.gsegUpdateDate;
    }

    public String getGsegUpdateEmp() {
        return this.gsegUpdateEmp;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsegId(final String gsegId) {
        this.gsegId = gsegId;
    }

    public void setGsegName(final String gsegName) {
        this.gsegName = gsegName;
    }

    public void setGsegBrId(final String gsegBrId) {
        this.gsegBrId = gsegBrId;
    }

    public void setGsegBrName(final String gsegBrName) {
        this.gsegBrName = gsegBrName;
    }

    public void setGsegBeginTime(final String gsegBeginTime) {
        this.gsegBeginTime = gsegBeginTime;
    }

    public void setGsegEndTime(final String gsegEndTime) {
        this.gsegEndTime = gsegEndTime;
    }

    public void setGsegEmp1(final String gsegEmp1) {
        this.gsegEmp1 = gsegEmp1;
    }

    public void setGsegEmp2(final String gsegEmp2) {
        this.gsegEmp2 = gsegEmp2;
    }

    public void setGsegEmp3(final String gsegEmp3) {
        this.gsegEmp3 = gsegEmp3;
    }

    public void setGsegEmp4(final String gsegEmp4) {
        this.gsegEmp4 = gsegEmp4;
    }

    public void setGsegEmp5(final String gsegEmp5) {
        this.gsegEmp5 = gsegEmp5;
    }

    public void setGsegUpdateDate(final String gsegUpdateDate) {
        this.gsegUpdateDate = gsegUpdateDate;
    }

    public void setGsegUpdateEmp(final String gsegUpdateEmp) {
        this.gsegUpdateEmp = gsegUpdateEmp;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSyncSdEmpGroupOutData)) {
            return false;
        } else {
            GetSyncSdEmpGroupOutData other = (GetSyncSdEmpGroupOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gsegId = this.getGsegId();
                Object other$gsegId = other.getGsegId();
                if (this$gsegId == null) {
                    if (other$gsegId != null) {
                        return false;
                    }
                } else if (!this$gsegId.equals(other$gsegId)) {
                    return false;
                }

                Object this$gsegName = this.getGsegName();
                Object other$gsegName = other.getGsegName();
                if (this$gsegName == null) {
                    if (other$gsegName != null) {
                        return false;
                    }
                } else if (!this$gsegName.equals(other$gsegName)) {
                    return false;
                }

                label158: {
                    Object this$gsegBrId = this.getGsegBrId();
                    Object other$gsegBrId = other.getGsegBrId();
                    if (this$gsegBrId == null) {
                        if (other$gsegBrId == null) {
                            break label158;
                        }
                    } else if (this$gsegBrId.equals(other$gsegBrId)) {
                        break label158;
                    }

                    return false;
                }

                label151: {
                    Object this$gsegBrName = this.getGsegBrName();
                    Object other$gsegBrName = other.getGsegBrName();
                    if (this$gsegBrName == null) {
                        if (other$gsegBrName == null) {
                            break label151;
                        }
                    } else if (this$gsegBrName.equals(other$gsegBrName)) {
                        break label151;
                    }

                    return false;
                }

                Object this$gsegBeginTime = this.getGsegBeginTime();
                Object other$gsegBeginTime = other.getGsegBeginTime();
                if (this$gsegBeginTime == null) {
                    if (other$gsegBeginTime != null) {
                        return false;
                    }
                } else if (!this$gsegBeginTime.equals(other$gsegBeginTime)) {
                    return false;
                }

                label137: {
                    Object this$gsegEndTime = this.getGsegEndTime();
                    Object other$gsegEndTime = other.getGsegEndTime();
                    if (this$gsegEndTime == null) {
                        if (other$gsegEndTime == null) {
                            break label137;
                        }
                    } else if (this$gsegEndTime.equals(other$gsegEndTime)) {
                        break label137;
                    }

                    return false;
                }

                label130: {
                    Object this$gsegEmp1 = this.getGsegEmp1();
                    Object other$gsegEmp1 = other.getGsegEmp1();
                    if (this$gsegEmp1 == null) {
                        if (other$gsegEmp1 == null) {
                            break label130;
                        }
                    } else if (this$gsegEmp1.equals(other$gsegEmp1)) {
                        break label130;
                    }

                    return false;
                }

                Object this$gsegEmp2 = this.getGsegEmp2();
                Object other$gsegEmp2 = other.getGsegEmp2();
                if (this$gsegEmp2 == null) {
                    if (other$gsegEmp2 != null) {
                        return false;
                    }
                } else if (!this$gsegEmp2.equals(other$gsegEmp2)) {
                    return false;
                }

                Object this$gsegEmp3 = this.getGsegEmp3();
                Object other$gsegEmp3 = other.getGsegEmp3();
                if (this$gsegEmp3 == null) {
                    if (other$gsegEmp3 != null) {
                        return false;
                    }
                } else if (!this$gsegEmp3.equals(other$gsegEmp3)) {
                    return false;
                }

                label109: {
                    Object this$gsegEmp4 = this.getGsegEmp4();
                    Object other$gsegEmp4 = other.getGsegEmp4();
                    if (this$gsegEmp4 == null) {
                        if (other$gsegEmp4 == null) {
                            break label109;
                        }
                    } else if (this$gsegEmp4.equals(other$gsegEmp4)) {
                        break label109;
                    }

                    return false;
                }

                label102: {
                    Object this$gsegEmp5 = this.getGsegEmp5();
                    Object other$gsegEmp5 = other.getGsegEmp5();
                    if (this$gsegEmp5 == null) {
                        if (other$gsegEmp5 == null) {
                            break label102;
                        }
                    } else if (this$gsegEmp5.equals(other$gsegEmp5)) {
                        break label102;
                    }

                    return false;
                }

                Object this$gsegUpdateDate = this.getGsegUpdateDate();
                Object other$gsegUpdateDate = other.getGsegUpdateDate();
                if (this$gsegUpdateDate == null) {
                    if (other$gsegUpdateDate != null) {
                        return false;
                    }
                } else if (!this$gsegUpdateDate.equals(other$gsegUpdateDate)) {
                    return false;
                }

                Object this$gsegUpdateEmp = this.getGsegUpdateEmp();
                Object other$gsegUpdateEmp = other.getGsegUpdateEmp();
                if (this$gsegUpdateEmp == null) {
                    if (other$gsegUpdateEmp != null) {
                        return false;
                    }
                } else if (!this$gsegUpdateEmp.equals(other$gsegUpdateEmp)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSyncSdEmpGroupOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsegId = this.getGsegId();
        result = result * 59 + ($gsegId == null ? 43 : $gsegId.hashCode());
        Object $gsegName = this.getGsegName();
        result = result * 59 + ($gsegName == null ? 43 : $gsegName.hashCode());
        Object $gsegBrId = this.getGsegBrId();
        result = result * 59 + ($gsegBrId == null ? 43 : $gsegBrId.hashCode());
        Object $gsegBrName = this.getGsegBrName();
        result = result * 59 + ($gsegBrName == null ? 43 : $gsegBrName.hashCode());
        Object $gsegBeginTime = this.getGsegBeginTime();
        result = result * 59 + ($gsegBeginTime == null ? 43 : $gsegBeginTime.hashCode());
        Object $gsegEndTime = this.getGsegEndTime();
        result = result * 59 + ($gsegEndTime == null ? 43 : $gsegEndTime.hashCode());
        Object $gsegEmp1 = this.getGsegEmp1();
        result = result * 59 + ($gsegEmp1 == null ? 43 : $gsegEmp1.hashCode());
        Object $gsegEmp2 = this.getGsegEmp2();
        result = result * 59 + ($gsegEmp2 == null ? 43 : $gsegEmp2.hashCode());
        Object $gsegEmp3 = this.getGsegEmp3();
        result = result * 59 + ($gsegEmp3 == null ? 43 : $gsegEmp3.hashCode());
        Object $gsegEmp4 = this.getGsegEmp4();
        result = result * 59 + ($gsegEmp4 == null ? 43 : $gsegEmp4.hashCode());
        Object $gsegEmp5 = this.getGsegEmp5();
        result = result * 59 + ($gsegEmp5 == null ? 43 : $gsegEmp5.hashCode());
        Object $gsegUpdateDate = this.getGsegUpdateDate();
        result = result * 59 + ($gsegUpdateDate == null ? 43 : $gsegUpdateDate.hashCode());
        Object $gsegUpdateEmp = this.getGsegUpdateEmp();
        result = result * 59 + ($gsegUpdateEmp == null ? 43 : $gsegUpdateEmp.hashCode());
        return result;
    }

    public String toString() {
        return "GetSyncSdEmpGroupOutData(clientId=" + this.getClientId() + ", gsegId=" + this.getGsegId() + ", gsegName=" + this.getGsegName() + ", gsegBrId=" + this.getGsegBrId() + ", gsegBrName=" + this.getGsegBrName() + ", gsegBeginTime=" + this.getGsegBeginTime() + ", gsegEndTime=" + this.getGsegEndTime() + ", gsegEmp1=" + this.getGsegEmp1() + ", gsegEmp2=" + this.getGsegEmp2() + ", gsegEmp3=" + this.getGsegEmp3() + ", gsegEmp4=" + this.getGsegEmp4() + ", gsegEmp5=" + this.getGsegEmp5() + ", gsegUpdateDate=" + this.getGsegUpdateDate() + ", gsegUpdateEmp=" + this.getGsegUpdateEmp() + ")";
    }
}
