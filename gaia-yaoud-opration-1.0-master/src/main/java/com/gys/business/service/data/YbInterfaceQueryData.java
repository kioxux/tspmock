package com.gys.business.service.data;

import lombok.Data;

@Data
public class YbInterfaceQueryData {
    private String clientId;
    private String brId;
    private String startDate;
    private String endDate;
    private String type;
    private String isStock;
    private String filePath;
}
