package com.gys.business.service.data.sdReplenishConfig;

import com.gys.business.service.data.replenishParam.ReplenishSdParamInData;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 门店补货参数
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-18
 */
@Data
@Accessors(chain = true)
public class GaiaSdReplenishConfigDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String[] stoCodes;

    @ApiModelProperty(value = "最小补货量验证，0为否，1为是")
    private String minQtyFlag;

    @ApiModelProperty(value = "最小补货量")
    @DecimalMin("0")
    @DecimalMax("9999999999")
    private BigDecimal minQty;

    @ApiModelProperty(value = "最大补货量验证，0为否，1为是")
    private String maxQtyFlag;

    @ApiModelProperty(value = "最大补货量")
    @DecimalMin("0")
    @DecimalMax("9999999999")
    private BigDecimal maxQty;

    @ApiModelProperty(value = "散装中药饮片是否按系统中包装补货，0为否，1为是")
    private String midsizeMedFlag;

    @ApiModelProperty(value = "正常商品中包装系数")
    private String midsizeNormRatio;

    @ApiModelProperty(value = "null/0不控制，1删除行数百分比，2删除行数")
    private String delLineFlag;

    @ApiModelProperty(value = "删除行数百分比")
    private String delLineRate;

    @ApiModelProperty(value = "删除行数")
    @DecimalMin("0")
    @DecimalMax("9999999999")
    private BigDecimal delLineQty;

    private String addLineRate;

    @ApiModelProperty(value = "null/0不控制，1修改行数百分比，2修改行数")
    private String reviseLineFlag;

    @ApiModelProperty(value = "修改行数百分比")
    private String reviseLineRate;

    @ApiModelProperty(value = "修改行数")
    @DecimalMin("0")
    @DecimalMax("9999999999")
    private BigDecimal reviseLineQty;

    @ApiModelProperty(value = "null/0不控制，1修改单行数量百分比，2修改单行可减少数量")
    private String reviseSingleFlag;

    @ApiModelProperty(value = "修改单行数量百分比")
    private String reviseSingleRate;

    @ApiModelProperty(value = "修改单行可减少数量")
    @DecimalMin("0")
    @DecimalMax("9999999999")
    private BigDecimal reviseSingleQty;

    private String reviseSingleAddRate;

    List<ReplenishSdParamInData> paramInDataList;

}
