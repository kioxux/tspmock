package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PercentageStoInData {
    @ApiModelProperty(name = "主键ID")
    private Long id;
    @ApiModelProperty(name = "加盟商")
    private String clientId;
    @ApiModelProperty(name = "门店编码")
    private String stoCode;
    @ApiModelProperty(name = "提成方案主表主键ID")
    private Integer pid;
    @ApiModelProperty(name = "适用月份")
    private String planSuitMouth;
}
