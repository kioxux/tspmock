package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PercentageSaleInData {
    @ApiModelProperty(value = "主键ID")
    private Long id;
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "提成方案主表主键ID")
    private Integer pid;
    @ApiModelProperty(value = "最小日均销售额")
    private BigDecimal minDailySaleAmt;
    @ApiModelProperty(value = "最大日均销售额")
    private BigDecimal maxDailySaleAmt;
    @ApiModelProperty(value = "毛利级别")
    private String proSaleClass;
    @ApiModelProperty(value = "最小毛利率")
    private String minProMll;
    @ApiModelProperty(value = "最大毛利率")
    private String maxProMll;
    @ApiModelProperty(value = "提成比例")
    private String tichengScale;
    @ApiModelProperty(value = "操作状态 0 未删除 1 已删除")
    private String deleteFlag;
}
