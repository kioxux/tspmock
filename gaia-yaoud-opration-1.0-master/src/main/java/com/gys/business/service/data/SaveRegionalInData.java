package com.gys.business.service.data;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 8:57 2021/10/15
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class SaveRegionalInData implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;


    /**
     * 区域名称
     */
    @NotBlank(message = "区域名称不能为空！")
    @Length(max = 50,message = "区域名称不能超过50个字符！")
    private String regionalName;

    /**
     * 上级区域ID
     */
    @NotNull(message = "上级区域ID不能为空！")
    private Long parentId;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 修改人
     */
    private String updateUser;
}
