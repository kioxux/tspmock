package com.gys.business.service.data;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@ApiModel(value="com-gys-business-service-data-GaiaChannelProduct")
@Data
public class GaiaChannelProduct extends BaseRowModel implements Serializable {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 渠道
    */
    @ApiModelProperty(value="渠道")
    @ExcelProperty(index = 1)
    private String gcdChannel;

    /**
    * 门店
    */
    @ApiModelProperty(value="门店")
    @ExcelProperty(index = 0)
    private String gcpStoreCode;

    /**
    * 商品编码
    */
    @ApiModelProperty(value="商品编码")
    @ExcelProperty(index = 2)
    private String gcpProCode;

    /**
    * 渠道价格
    */
    @ApiModelProperty(value="渠道价格")
    private BigDecimal gcpChannelPrice;

    /**
    * 状态（1-上架，2-下架）
    */
    @ApiModelProperty(value="状态（1-上架，2-下架）")
    @ExcelProperty(index = 4)
    private String gcpStatus;

    /**
    * 创建人
    */
    @ApiModelProperty(value="创建人")
    private String gcpCreateBy;

    /**
    * 创建日期
    */
    @ApiModelProperty(value="创建日期")
    private Date gcpCreateDate;


    /**
    * 更新人
    */
    @ApiModelProperty(value="更新人")
    private String gcpUpdateBy;

    /**
    * 更新日期
    */
    @ApiModelProperty(value="更新日期")
    private Date gcpUpdateDate;


    /**
     * 渠道价格字符串
     */
    @ApiModelProperty(value = "渠道价格字符串")
    @ExcelProperty(index = 3)
    private String gcpChannelPriceStr;

    private int isDelete;

}