package com.gys.business.service;

import com.gys.business.service.data.yaoshibang.CustormerQueryFrom;

import java.util.HashMap;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/12/22 13:36
 */
public interface GAIAYaoshibangStoreService {
    HashMap<String,String> getYaoshibangStoreByKey(CustormerQueryFrom queryFrom);

    boolean synchYaoshibangStore(Integer isAll);
}
