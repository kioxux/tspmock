//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.CashPaymentDetailOutData;
import com.gys.business.service.data.CashPaymentInData;
import com.gys.business.service.data.CashPaymentOutData;
import com.gys.business.service.data.GetEmpOutData;
import com.gys.business.service.data.GetSaleScheduleOutData;
import com.gys.business.service.data.GetSaleScheduleQueryInData;
import com.gys.common.data.GetLoginOutData;
import java.util.List;

public interface CashPaymentService {
    List<CashPaymentOutData> getCashPaymentList(CashPaymentInData inData);

    List<CashPaymentDetailOutData> getCashPaymentDetailList(CashPaymentOutData inData);

    void insert(CashPaymentOutData inData);

    void approve(CashPaymentOutData inData);

    List<GetSaleScheduleOutData> getEmpGroupList(GetSaleScheduleQueryInData inData);

    List<GetEmpOutData> getEmpList(GetLoginOutData inData);
}
