package com.gys.business.service.takeAway;

import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.PtsOrderInfo;

import java.util.List;
import java.util.Map;

public interface BaseService {
    void insertOperatelogWithMap(Map<String, Object> param);


    String insertOrderInfoWithMap(BaseOrderInfo param);

    String insertOrderHistoryInfoWithMap(Map<String, Object> param);

    String updateOrderInfoWithMap(Map<String, Object> param);

    String updateOrderInfoWithEntity(BaseOrderInfo param);

    int updateOrderDispatchInfoWithMap(Map<String, Object> param);

    String updateStockWithMap(Map<String, Object> param, String   accessToken);

    String updateOrderStatus(Map<String, Object> param);

    void ddkyUpdateShippingFee(String platformOrderId, String shippingFee);

    String shopOpenUpdate(Map<String, Object> param);

    String cancelOrder(Map<String, Object> param);

    PtsOrderInfo GetOrderInfo(Map<String, Object> param);

    Map<String, Object> selectOrderInfoByMap(Map<String, Object> param);

    //查询所有门店药品信息
    List<Map <String, Object>> queryMedicine(Map<String,Object> param);

    //插入门店药品信息
    String insertMedicine(Map<String,Object> param);

    //更新门店药品信息
    String updateMedicine(Map<String,Object> param);

//    //更新门店药品信息
//    void updateMedicine(Map<String,Object> param,Map<String,Object> medicineList);

    //更新门店药品状态
    String updateMedicineStatus(Map<String, Object> param);

    //插入药品平台价格信息
    String insertMedicinePlatforms(Map<String,Object> param);

    //更新药品平台价格信息
    String updateMedicinePlatforms(Map<String, Object> param);

    //更新药品平台状态结果
    String updateMedicinePlatformsStatus(Map<String, Object> param);

    //加载已有药品分类信息
    List<Map<String,Object>> load_medicine_category_info();

    //插入药品分类信息
    String insert_medicine_category_info(List<Map<String,Object>> param);



    //查询所有门店id信息
    List<Map<String,Object>> queryShop();

    //加载配置信息
    List<Map<String,Object>> loadConfig();

    List<Map<String,Object>> loadAppSecretForJd();

    List<Map<String,Object>> loadAppSecretForJdByIndex();

    //加载门店配置信息
    List<Map<String,Object>> loadShop(Map<String, Object> param);

    //查询开关设置信息
    List<Map<String,Object>> selectSwitchInfo(Map<String, Object> param);

    //更新锁定库存
    void lockStockForOrderStatus(Map<String,Object> proMap);

    // 查询每家门店某一种药品的库存锁定数量
    List<Map<String,Object>> selectStoreMedicineStockLockByMap(Map<String, Object> param);

    //查询待处理消息队列
    List<Map<String,Object>> selectMessageQueue(Map<String, Object> param);

    //更新事务队列
    void updateMessageQueue(Map<String, Object> param);

    //插入新的事务进事务队列
    void insertMessageQueue(Map<String, Object> param);


    //插入排他信息
    void insertSyncObj(Map<String, Object> param);

    //删除排他信息
    void deleteSyncObj(Map<String, Object> param);

    List<Map<String, Object>> selectGroupMedicine(Map<String, Object> param);

    //批量查询门店商品清单
    List<Map<String, Object>> queryMedicineByList(Map<String, Object> param);

    //定量查询待处理商品清单
    List<Map<String, Object>> queryUnExecMedicine(Map<String, Object> param);

    void kyCreateNewOrder(String client, String stoCode, Map<String, Object> paramMap, String orderOnService, String appId, String secret);

    void orderCompleteForJdSync(String orderId);


    boolean applyAfterSaleBillForJd(Map<String, Object> paramMap, String token, String appKey, String platformOrderId, String appSecret);

    int validApiParamForJd(Map<String, Object> paramMap, String appSecret);

    String updateRecipientPhone(List<Map<String, String>> items);

}
