package com.gys.business.service.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetTakeAwayInData {
    private String client;
    private String stoCode;
    private String startDate;
    private String endDate;
    private String orderNo;
    private String payTimeCondition;//已支付；未支付

    public GetTakeAwayInData(String client, String stoCode, String orderNo) {
        this.client = client;
        this.stoCode = stoCode;
        this.orderNo = orderNo;
    }
}
