package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 10:29 2021/10/20
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class RecallDTO {
    /**
     * 门店
     */
    @ApiModelProperty(value = "门店")
    private String store;
    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码")
    @NotBlank(message = "商品不能为空")
    private String commodityCode;
    /**
     * 批号
     */
    @ApiModelProperty(value = "批号")
    private String batchNo;
    /**
     * 供应商编号
     */
    @ApiModelProperty(value = "供应商编号")
    private String supplierNo;
    /**
     * 召回数量
     */
    @ApiModelProperty(value = "召回数量")
    private BigDecimal recallNumber;
    /**
     * 召回备注
     */
    @ApiModelProperty(value = "召回备注")
    private String remark;
    /**
     * 连锁公司
     */
    @ApiModelProperty(value = "连锁公司")
//	@NotBlank(message = "连锁公司不能为空")
    private String chainCompany;
}
