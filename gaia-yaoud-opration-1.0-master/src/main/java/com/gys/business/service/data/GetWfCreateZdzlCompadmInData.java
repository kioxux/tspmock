//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetWfCreateZdzlCompadmInData {
    private String saleOrder = "";
    private String saleDate = "";
    private String storeId = "";
    private String proCode = "";
    private String proName = "";
    private String proSpecs = "";
    private String qty = "";
    private String totalPrice = "";
    private String rate = "";
    private String changeTotalPrice = "";
    private String batchCost = "";
    private String wmaCost = "";

    public GetWfCreateZdzlCompadmInData() {
    }

    public String getSaleOrder() {
        return this.saleOrder;
    }

    public String getSaleDate() {
        return this.saleDate;
    }

    public String getStoreId() {
        return this.storeId;
    }

    public String getProCode() {
        return this.proCode;
    }

    public String getProName() {
        return this.proName;
    }

    public String getProSpecs() {
        return this.proSpecs;
    }

    public String getQty() {
        return this.qty;
    }

    public String getTotalPrice() {
        return this.totalPrice;
    }

    public String getRate() {
        return this.rate;
    }

    public String getChangeTotalPrice() {
        return this.changeTotalPrice;
    }

    public String getBatchCost() {
        return this.batchCost;
    }

    public String getWmaCost() {
        return this.wmaCost;
    }

    public void setSaleOrder(final String saleOrder) {
        this.saleOrder = saleOrder;
    }

    public void setSaleDate(final String saleDate) {
        this.saleDate = saleDate;
    }

    public void setStoreId(final String storeId) {
        this.storeId = storeId;
    }

    public void setProCode(final String proCode) {
        this.proCode = proCode;
    }

    public void setProName(final String proName) {
        this.proName = proName;
    }

    public void setProSpecs(final String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public void setQty(final String qty) {
        this.qty = qty;
    }

    public void setTotalPrice(final String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setRate(final String rate) {
        this.rate = rate;
    }

    public void setChangeTotalPrice(final String changeTotalPrice) {
        this.changeTotalPrice = changeTotalPrice;
    }

    public void setBatchCost(final String batchCost) {
        this.batchCost = batchCost;
    }

    public void setWmaCost(final String wmaCost) {
        this.wmaCost = wmaCost;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetWfCreateZdzlCompadmInData)) {
            return false;
        } else {
            GetWfCreateZdzlCompadmInData other = (GetWfCreateZdzlCompadmInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label155: {
                    Object this$saleOrder = this.getSaleOrder();
                    Object other$saleOrder = other.getSaleOrder();
                    if (this$saleOrder == null) {
                        if (other$saleOrder == null) {
                            break label155;
                        }
                    } else if (this$saleOrder.equals(other$saleOrder)) {
                        break label155;
                    }

                    return false;
                }

                Object this$saleDate = this.getSaleDate();
                Object other$saleDate = other.getSaleDate();
                if (this$saleDate == null) {
                    if (other$saleDate != null) {
                        return false;
                    }
                } else if (!this$saleDate.equals(other$saleDate)) {
                    return false;
                }

                Object this$storeId = this.getStoreId();
                Object other$storeId = other.getStoreId();
                if (this$storeId == null) {
                    if (other$storeId != null) {
                        return false;
                    }
                } else if (!this$storeId.equals(other$storeId)) {
                    return false;
                }

                label134: {
                    Object this$proCode = this.getProCode();
                    Object other$proCode = other.getProCode();
                    if (this$proCode == null) {
                        if (other$proCode == null) {
                            break label134;
                        }
                    } else if (this$proCode.equals(other$proCode)) {
                        break label134;
                    }

                    return false;
                }

                label127: {
                    Object this$proName = this.getProName();
                    Object other$proName = other.getProName();
                    if (this$proName == null) {
                        if (other$proName == null) {
                            break label127;
                        }
                    } else if (this$proName.equals(other$proName)) {
                        break label127;
                    }

                    return false;
                }

                label120: {
                    Object this$proSpecs = this.getProSpecs();
                    Object other$proSpecs = other.getProSpecs();
                    if (this$proSpecs == null) {
                        if (other$proSpecs == null) {
                            break label120;
                        }
                    } else if (this$proSpecs.equals(other$proSpecs)) {
                        break label120;
                    }

                    return false;
                }

                Object this$qty = this.getQty();
                Object other$qty = other.getQty();
                if (this$qty == null) {
                    if (other$qty != null) {
                        return false;
                    }
                } else if (!this$qty.equals(other$qty)) {
                    return false;
                }

                label106: {
                    Object this$totalPrice = this.getTotalPrice();
                    Object other$totalPrice = other.getTotalPrice();
                    if (this$totalPrice == null) {
                        if (other$totalPrice == null) {
                            break label106;
                        }
                    } else if (this$totalPrice.equals(other$totalPrice)) {
                        break label106;
                    }

                    return false;
                }

                Object this$rate = this.getRate();
                Object other$rate = other.getRate();
                if (this$rate == null) {
                    if (other$rate != null) {
                        return false;
                    }
                } else if (!this$rate.equals(other$rate)) {
                    return false;
                }

                label92: {
                    Object this$changeTotalPrice = this.getChangeTotalPrice();
                    Object other$changeTotalPrice = other.getChangeTotalPrice();
                    if (this$changeTotalPrice == null) {
                        if (other$changeTotalPrice == null) {
                            break label92;
                        }
                    } else if (this$changeTotalPrice.equals(other$changeTotalPrice)) {
                        break label92;
                    }

                    return false;
                }

                Object this$batchCost = this.getBatchCost();
                Object other$batchCost = other.getBatchCost();
                if (this$batchCost == null) {
                    if (other$batchCost != null) {
                        return false;
                    }
                } else if (!this$batchCost.equals(other$batchCost)) {
                    return false;
                }

                Object this$wmaCost = this.getWmaCost();
                Object other$wmaCost = other.getWmaCost();
                if (this$wmaCost == null) {
                    if (other$wmaCost != null) {
                        return false;
                    }
                } else if (!this$wmaCost.equals(other$wmaCost)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetWfCreateZdzlCompadmInData;
    }

    public int hashCode() {

        int result = 1;
        Object $saleOrder = this.getSaleOrder();
        result = result * 59 + ($saleOrder == null ? 43 : $saleOrder.hashCode());
        Object $saleDate = this.getSaleDate();
        result = result * 59 + ($saleDate == null ? 43 : $saleDate.hashCode());
        Object $storeId = this.getStoreId();
        result = result * 59 + ($storeId == null ? 43 : $storeId.hashCode());
        Object $proCode = this.getProCode();
        result = result * 59 + ($proCode == null ? 43 : $proCode.hashCode());
        Object $proName = this.getProName();
        result = result * 59 + ($proName == null ? 43 : $proName.hashCode());
        Object $proSpecs = this.getProSpecs();
        result = result * 59 + ($proSpecs == null ? 43 : $proSpecs.hashCode());
        Object $qty = this.getQty();
        result = result * 59 + ($qty == null ? 43 : $qty.hashCode());
        Object $totalPrice = this.getTotalPrice();
        result = result * 59 + ($totalPrice == null ? 43 : $totalPrice.hashCode());
        Object $rate = this.getRate();
        result = result * 59 + ($rate == null ? 43 : $rate.hashCode());
        Object $changeTotalPrice = this.getChangeTotalPrice();
        result = result * 59 + ($changeTotalPrice == null ? 43 : $changeTotalPrice.hashCode());
        Object $batchCost = this.getBatchCost();
        result = result * 59 + ($batchCost == null ? 43 : $batchCost.hashCode());
        Object $wmaCost = this.getWmaCost();
        result = result * 59 + ($wmaCost == null ? 43 : $wmaCost.hashCode());
        return result;
    }

    public String toString() {
        return "GetWfCreateZdzlCompadmInData(saleOrder=" + this.getSaleOrder() + ", saleDate=" + this.getSaleDate() + ", storeId=" + this.getStoreId() + ", proCode=" + this.getProCode() + ", proName=" + this.getProName() + ", proSpecs=" + this.getProSpecs() + ", qty=" + this.getQty() + ", totalPrice=" + this.getTotalPrice() + ", rate=" + this.getRate() + ", changeTotalPrice=" + this.getChangeTotalPrice() + ", batchCost=" + this.getBatchCost() + ", wmaCost=" + this.getWmaCost() + ")";
    }
}
