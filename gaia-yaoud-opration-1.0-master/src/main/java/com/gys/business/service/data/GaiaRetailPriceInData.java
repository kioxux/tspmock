package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * 请求参数
 *
 * @author xiaoyuan on 2020/8/14
 */
@Data
public class GaiaRetailPriceInData implements Serializable {
    private static final long serialVersionUID = 6069955066540919758L;

    /**
     * 加盟号
     */
    private String client;

    /**
     * 店号
     */
    private String brId;

    /**
     * 商品编码
     */
    private String gsppProId;

    /**
     * 开始日期
     */
    private String startDate;

    /**
     * 结束日期
     */
    private String endDate;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 收银员
     */
    private String cashier;

    /**
     * 日期
     */
    private String selectDate;
}
