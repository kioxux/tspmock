//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service;

import com.gys.business.service.data.GetAllotDetailOutData;
import com.gys.business.service.data.GetAllotInData;
import com.gys.business.service.data.GetAllotOutData;
import com.gys.business.service.data.GetProductInData;
import com.gys.business.service.data.GetQueryProVoOutData;
import com.gys.common.data.PageInfo;
import java.util.List;

public interface AllotService {
    PageInfo<GetAllotOutData> selectList(GetAllotInData inData);

    GetAllotOutData detail(GetAllotInData inData);

    List<GetAllotDetailOutData> detailList(GetAllotInData inData);

    void insert(GetAllotInData inData);

    void approve(GetAllotInData inData);

    void update(GetAllotInData inData);

    List<GetQueryProVoOutData> queryPro(GetProductInData inData);

    List<GetQueryProVoOutData> exportIn(GetAllotInData inData);
}
