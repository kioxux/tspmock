package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class StoreReplenishPlanInData implements Serializable {

    private static final long serialVersionUID = -3781934088499015328L;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "门店编号")
    private String brId;

    @ApiModelProperty(value = "员工号")
    private String userId;

    @ApiModelProperty(value = "补货方式 0-普通补货 1-紧急补货")
    private String pattern;

    @ApiModelProperty(value = "补货明细列表")
    private List<OutOfStockOutListData> outOfStockOutList;
}
