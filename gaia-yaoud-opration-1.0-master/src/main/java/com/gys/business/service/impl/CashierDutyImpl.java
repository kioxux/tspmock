//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.GaiaSdEmpGroupMapper;
import com.gys.business.service.CashierDutyService;
import com.gys.business.service.data.GetLoginStoreInData;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CashierDutyImpl implements CashierDutyService {
    @Autowired
    private GaiaSdEmpGroupMapper gaiaSdEmpGroupMapper;

    public CashierDutyImpl() {
    }

    public String checkChange(GetLoginStoreInData inData) {
        List<String> res = this.gaiaSdEmpGroupMapper.checkChange(inData);
        return ObjectUtil.isNotEmpty(res) ? (String)res.get(0) : null;
    }
}
