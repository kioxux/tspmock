package com.gys.business.service.member.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.gys.business.mapper.GaiaSdIntegralCashMapper;
import com.gys.business.mapper.GaiaSdIntegralCashSetNewMapper;
import com.gys.business.mapper.GaiaSdIntegralCashSetRuleMapper;
import com.gys.business.mapper.entity.member.*;
import com.gys.business.service.data.SelectData;
import com.gys.business.service.member.PointCashOutService;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.data.member.*;
import com.gys.common.enums.MemberCardTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.util.DateUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wu mao yin
 * @Title: 积分抵现
 * @date 2021/12/815:09
 */
@Service
public class PointCashOutServiceImpl implements PointCashOutService {

    @Resource
    private GaiaSdIntegralCashMapper gaiaSdIntegralCashMapper;

    @Resource
    private GaiaSdIntegralCashSetNewMapper gaiaSdIntegralCashSetNewMapper;

    @Resource
    private GaiaSdIntegralCashSetRuleMapper gaiaSdIntegralCashSetRuleMapper;

    private final List<String> CARD_TYPE = MemberCardTypeEnum.getTypes();

    @Override
    public JsonResult selectPointRuleSettingList(PointSettingSearchDTO pointSettingSearchDTO) {

        PageHelper.startPage(pointSettingSearchDTO.getPageNum(), pointSettingSearchDTO.getPageSize());

        String memberCardType = pointSettingSearchDTO.getGsicsMemberCardType();
        if (StringUtils.isNotBlank(memberCardType)) {
            List<String> cardTypes = Splitter.on(CharUtil.COMMA).omitEmptyStrings().splitToList(memberCardType);
            StringJoiner sj = new StringJoiner("|");
            for (String cardType : cardTypes) {
                if (StringUtils.isNotBlank(cardType)) {
                    sj.add(cardType);
                }
            }
            if (sj.length() > 0) {
                pointSettingSearchDTO.setGsicsMemberCardType("(" + sj.toString() + ")");
            }
        }

        List<PointSettingVO> pointSettings = gaiaSdIntegralCashSetNewMapper.selectPointRuleSettingList(pointSettingSearchDTO);
        for (PointSettingVO pointSetting : pointSettings) {
            String gsicsMemberCardType = pointSetting.getGsicsMemberCardType();
            if (StringUtils.isNotBlank(gsicsMemberCardType)) {
                List<String> cardType = new ArrayList<>(Splitter.on(CharUtil.COMMA).omitEmptyStrings().splitToList(gsicsMemberCardType));
                pointSetting.setGsicsMemberCardTypeName(MemberCardTypeEnum.getNameByTypes(cardType));
            }
        }
        return JsonResult.success(new PageInfo<>(pointSettings));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult saveOrUpdatePointRuleSetting(PointSettingDTO pointSettingDTO) {
        // 主键
        String gsicsPlanId = pointSettingDTO.getGsicsPlanId();
        GaiaSdIntegralCashSetNew gaiaSdIntegralCashSetNew = new GaiaSdIntegralCashSetNew();
        BeanUtil.copyProperties(pointSettingDTO, gaiaSdIntegralCashSetNew);
        if (StringUtils.isBlank(gaiaSdIntegralCashSetNew.getGsicsMemberCardType())) {
            gaiaSdIntegralCashSetNew.setAllMemberCardType(true);
            gaiaSdIntegralCashSetNew.setGsicsMemberCardType(Joiner.on(StrUtil.COMMA).join(CARD_TYPE));
        } else {
            gaiaSdIntegralCashSetNew.setAllMemberCardType(false);
        }
        String client = pointSettingDTO.getClient();
        String gsicsCode;
        if (Objects.isNull(gsicsPlanId)) {
            // 新增
            // 获取最新的规则代号
            String lastPlanId = gaiaSdIntegralCashSetNewMapper.selectLastPlanId(client);

            gaiaSdIntegralCashSetNew.setGsicsPlanId(lastPlanId);
            gaiaSdIntegralCashSetNew.setGsicsCrId(pointSettingDTO.getUserId());

            Integer gsicsAmtLimitFlag = gaiaSdIntegralCashSetNew.getGsicsAmtLimitFlag();
            if (gsicsAmtLimitFlag == 0) {
                gaiaSdIntegralCashSetNew.setGsicsLimitRate(null);
                gaiaSdIntegralCashSetNew.setGsicsAmtOffsetMax(null);
            }
            // 生成规则内容代号
            String lastCode = gaiaSdIntegralCashSetNewMapper.selectLastCode(client);
            gaiaSdIntegralCashSetNew.setGsicsCode(lastCode);
            gsicsCode = lastCode;
            gaiaSdIntegralCashSetNewMapper.insertSelective(gaiaSdIntegralCashSetNew);
        } else {
            // 获取规则内容代号
            GaiaSdIntegralCashSetNew tempIntegralCashSet = new GaiaSdIntegralCashSetNew();
            tempIntegralCashSet.setClient(client);
            tempIntegralCashSet.setGsicsPlanId(gsicsPlanId);
            tempIntegralCashSet = gaiaSdIntegralCashSetNewMapper.selectOne(tempIntegralCashSet);
            // 修改
            gaiaSdIntegralCashSetNew.setGsicsMdId(pointSettingDTO.getUserId());
            gaiaSdIntegralCashSetNewMapper.updateCashSetByKey(gaiaSdIntegralCashSetNew);
            gsicsCode = tempIntegralCashSet.getGsicsCode();
            // 删除老的规则内容
            gaiaSdIntegralCashSetRuleMapper.deleteIntegralCashSet(client, gsicsCode);
        }
        // 新规则内容保存
        List<ExecuteContent> executeContent = pointSettingDTO.getExecuteContent();
        int serial = 1;
        for (ExecuteContent content : executeContent) {
            GaiaSdIntegralCashSetRule gaiaSdIntegralCashSetRule = new GaiaSdIntegralCashSetRule();
            BigDecimal cash = content.getCash();
            BigDecimal points = content.getPoints();
            BigDecimal proportion = content.getProportion();
            gaiaSdIntegralCashSetRule.setClient(client);
            gaiaSdIntegralCashSetRule.setGsicsrCode(gsicsCode);
            gaiaSdIntegralCashSetRule.setGsicsrDyAmt(cash);
            gaiaSdIntegralCashSetRule.setGsicsrSerial(serial++);
            gaiaSdIntegralCashSetRule.setGsicsrNeedJf(points);
            gaiaSdIntegralCashSetRule.setGsicsrDyRate(proportion);
            gaiaSdIntegralCashSetRuleMapper.insertSelective(gaiaSdIntegralCashSetRule);
        }
        return JsonResult.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult deletePointRuleSetting(String client, String planIds) {
        if (StringUtils.isBlank(planIds)) {
            throw new BusinessException("规则代号必传!");
        }

        List<String> planIdList = new ArrayList<>(Splitter.on(CharUtil.COMMA).omitEmptyStrings()
                .splitToList(planIds));

        for (String planId : planIdList) {
            GaiaSdIntegralCashSetNew gaiaSdIntegralCashSetNew = new GaiaSdIntegralCashSetNew();
            gaiaSdIntegralCashSetNew.setClient(client);
            gaiaSdIntegralCashSetNew.setGsicsPlanId(planId);
            gaiaSdIntegralCashSetNew = gaiaSdIntegralCashSetNewMapper.selectOne(gaiaSdIntegralCashSetNew);
            if (gaiaSdIntegralCashSetNew == null) {
                throw new BusinessException("规则代号无效!");
            }

            String gsicsPlanId = gaiaSdIntegralCashSetNew.getGsicsPlanId();
            // 验证是否被积分抵现活动引用
            Integer checkPlanIdIsInUse = gaiaSdIntegralCashMapper.checkPlanIdIsInUse(client, gsicsPlanId);
            if (checkPlanIdIsInUse != null) {
                throw new BusinessException(String.format("规则代码%s已被使用!", gsicsPlanId));
            }

            // 删除附表数据
            gaiaSdIntegralCashSetRuleMapper.deleteIntegralCashSet(client, gaiaSdIntegralCashSetNew.getGsicsCode());
        }

        // 删除主表数据
        gaiaSdIntegralCashSetNewMapper.deleteCashSetBatch(planIdList);
        return JsonResult.success();
    }

    @Override
    public JsonResult selectPlanIdList(String client) {
        List<SelectData> selectData = gaiaSdIntegralCashSetNewMapper.selectPlanIdList(client);
        return JsonResult.success(selectData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult saveOrUpdateIntegralCashActivity(PointCashOutActivityDTO pointCashOutActivityDTO) {
        // 检查同一门店、同一会员卡别、同一时间，是否有多种积分设置被应用
        // 获取抵现规则对应的会员卡类别

        checkEffectiveTimePeriod(pointCashOutActivityDTO);

        String client = pointCashOutActivityDTO.getClient();
        // 活动单号
        String voucherId = pointCashOutActivityDTO.getGsicVoucherId();
        GaiaSdIntegralCash gaiaSdIntegralCash = new GaiaSdIntegralCash();
        BeanUtil.copyProperties(pointCashOutActivityDTO, gaiaSdIntegralCash);
        if (StringUtils.isNotBlank(gaiaSdIntegralCash.getGsicDateFrequency())) {
            gaiaSdIntegralCash.setGsicFrequencyType(1);
        } else {
            if (StringUtils.isNotBlank(gaiaSdIntegralCash.getGsicWeekFrequency())) {
                gaiaSdIntegralCash.setGsicFrequencyType(2);
            } else {
                gaiaSdIntegralCash.setGsicFrequencyType(null);
            }
        }

        if (StringUtils.isBlank(voucherId)) {
            // 新增

            // 生成抵现活动单号
            voucherId = gaiaSdIntegralCashMapper.selectLastVoucherId(client);
            gaiaSdIntegralCash.setGsicVoucherId(voucherId);
            gaiaSdIntegralCash.setGsicCreateUser(pointCashOutActivityDTO.getUserId());
            gaiaSdIntegralCashMapper.insertSelective(gaiaSdIntegralCash);
        } else {
            // 修改
            gaiaSdIntegralCash.setGsicUpdateUser(pointCashOutActivityDTO.getUserId());
            gaiaSdIntegralCashMapper.updateIntegralCash(gaiaSdIntegralCash);

            // 删除之前关联的商品
            gaiaSdIntegralCashMapper.deleteProWithCash(client, voucherId);
            // 删除之前关联的门店
            gaiaSdIntegralCashMapper.deleteStoWithCash(client, voucherId);
            // 删除之前关联的自定义时段
            gaiaSdIntegralCashMapper.deleteTimeIntervalsWithCash(client, voucherId);
        }

        // 保存活动商品，关联门店，活动时段
        saveProAndStoreAndTimeInterval(client, voucherId, pointCashOutActivityDTO);

        return JsonResult.success();
    }

    private void saveProAndStoreAndTimeInterval(String client, String voucherId, PointCashOutActivityDTO pointCashOutActivityDTO) {
        // 保存商品
        List<String> proCodes = pointCashOutActivityDTO.getProCodes();
        if (CollectionUtils.isNotEmpty(proCodes)) {
            List<GaiaSdIntegralCashConds> gaiaSdIntegralCashConds = new ArrayList<>();
            int index = 1;
            for (String proCode : proCodes) {
                GaiaSdIntegralCashConds cashConds = new GaiaSdIntegralCashConds();
                cashConds.setClient(client);
                cashConds.setGsiccVoucherId(voucherId);
                cashConds.setGsiccProId(proCode);
                cashConds.setGsiccSerial((long) index++);
                gaiaSdIntegralCashConds.add(cashConds);

                if (gaiaSdIntegralCashConds.size() == 1000) {
                    gaiaSdIntegralCashMapper.saveProWithCash(gaiaSdIntegralCashConds);
                    gaiaSdIntegralCashConds.clear();
                }
            }

            if (CollectionUtils.isNotEmpty(gaiaSdIntegralCashConds)) {
                gaiaSdIntegralCashMapper.saveProWithCash(gaiaSdIntegralCashConds);
                gaiaSdIntegralCashConds.clear();
            }
        }

        // 保存门店
        List<String> stoCodes = pointCashOutActivityDTO.getStoCodes();
        if (CollectionUtils.isNotEmpty(stoCodes)) {
            List<GaiaSdIntegralCashSto> gaiaSdIntegralCashStos = new ArrayList<>();
            for (String stoCode : stoCodes) {
                GaiaSdIntegralCashSto cashSto = new GaiaSdIntegralCashSto();
                cashSto.setClient(client);
                cashSto.setStoCode(stoCode);
                cashSto.setGsicsVoucherId(voucherId);
                gaiaSdIntegralCashStos.add(cashSto);
            }
            gaiaSdIntegralCashMapper.saveStoWithCash(gaiaSdIntegralCashStos);
        }

        // 保存自定义时段
        List<String> timeIntervals = pointCashOutActivityDTO.getTimeIntervals();
        if (CollectionUtils.isNotEmpty(timeIntervals)) {
            List<GaiaSdIntegralCashTimeInterval> gaiaSdIntegralCashTimeIntervals = new ArrayList<>();
            for (String timeInterval : timeIntervals) {
                GaiaSdIntegralCashTimeInterval cashTimeInterval = new GaiaSdIntegralCashTimeInterval();
                String[] split = timeInterval.split("~");
                cashTimeInterval.setClient(client);
                cashTimeInterval.setGsictiVoucherId(voucherId);
                cashTimeInterval.setGsictiStartTime(split[0]);
                cashTimeInterval.setGsictiEndTime(split[1]);
                gaiaSdIntegralCashTimeIntervals.add(cashTimeInterval);
            }
            gaiaSdIntegralCashMapper.saveTimeIntervalsWithCash(gaiaSdIntegralCashTimeIntervals);
        }
    }

    private void checkEffectiveTimePeriod(PointCashOutActivityDTO pointCashOutActivityDTO) {
        List<Map<String, String>> tempDateList = parseNewTimeInterval(pointCashOutActivityDTO);

        if (CollectionUtils.isEmpty(tempDateList)) {
            throw new BusinessException("请设置时段");
        }

        String client = pointCashOutActivityDTO.getClient();
        GaiaSdIntegralCashSetNew gaiaSdIntegralCashSetNew = new GaiaSdIntegralCashSetNew();
        gaiaSdIntegralCashSetNew.setClient(client);
        gaiaSdIntegralCashSetNew.setGsicsPlanId(pointCashOutActivityDTO.getGsicPlanId());
        gaiaSdIntegralCashSetNew = gaiaSdIntegralCashSetNewMapper.selectOne(gaiaSdIntegralCashSetNew);
        if (gaiaSdIntegralCashSetNew != null) {
            String gsicsMemberCardType = gaiaSdIntegralCashSetNew.getGsicsMemberCardType();
            List<String> cardTypes = Splitter.on(CharUtil.COMMA).omitEmptyStrings()
                    .splitToList(gsicsMemberCardType);

            StringJoiner sj = new StringJoiner("|");
            for (String cardType : cardTypes) {
                if (StringUtils.isNotBlank(cardType)) {
                    sj.add(cardType);
                }
            }
            StringBuilder sb = new StringBuilder();
            if (sj.length() > 0) {
                sb.append("(").append(sj.toString()).append(")");
            }

            List<String> stoCodes = pointCashOutActivityDTO.getStoCodes();
            String gsicVoucherId = pointCashOutActivityDTO.getGsicVoucherId();
            // 有同一门店、同一会员卡别的被设置，再检查时间

            // 存在同一门店，同一会员卡类别的抵现活动
            List<Map<String, String>> checkSameRuleInUse = gaiaSdIntegralCashMapper.checkSameRuleInUse(client, gsicVoucherId, sb.toString(), stoCodes);
            if (CollectionUtils.isNotEmpty(checkSameRuleInUse)) {
                // 获取除单签单号的所有满足条件时段，先验证时段范围
                List<Map<String, String>> timeIntervals = gaiaSdIntegralCashMapper.selectTimeIntervals(client, gsicVoucherId);

                Map<String, List<Map<String, String>>> voucherIdMap = timeIntervals.stream().collect(Collectors.groupingBy(s -> s.get("voucherId")));
                for (Map<String, String> sameRuleInUse : checkSameRuleInUse) {
                    String voucherId = sameRuleInUse.get("voucherId");
                    String startDate = sameRuleInUse.get("startDate");
                    String endDate = sameRuleInUse.get("endDate");
                    // 活动单号设置的时段
                    List<Map<String, String>> voucherIdList = voucherIdMap.get(voucherId);

                    List<String> tempTimeIntervals = new ArrayList<>();
                    // 没有设置时段，则根据活动的开始结束时间范围验证
                    if (CollectionUtils.isEmpty(voucherIdList)) {
                        String startTime = sameRuleInUse.get("startTime");
                        String endTime = sameRuleInUse.get("endTime");
                        tempTimeIntervals.add(startTime + "~" + endTime);
                    } else {
                        tempTimeIntervals = voucherIdList.stream().map(s -> s.get("timeInterval")).collect(Collectors.toList());
                    }

                    List<Map<String, String>> dateList;
                    // 日期频率
                    String dateFrequency = sameRuleInUse.get("dateFrequency");
                    // 星期频率
                    String weekFrequency = sameRuleInUse.get("weekFrequency");
                    // 频率作用于起止时间和时段
                    if (StringUtils.isNotBlank(dateFrequency)) {
                        // 起止时间内对应日期频率的时间段范围
                        dateList = transferTimeIntervalToSpecificTime(startDate, endDate, tempTimeIntervals,
                                Splitter.on(CharUtil.COMMA).splitToList(dateFrequency), 1);
                    } else if (StringUtils.isNotBlank(weekFrequency)) {
                        // 起止时间内对应星期频率的时间段范围
                        dateList = transferTimeIntervalToSpecificTime(startDate, endDate, tempTimeIntervals,
                                Splitter.on(CharUtil.COMMA).splitToList(weekFrequency), 2);
                    } else {
                        // 自定义时段
                        dateList = wrapperCustomTimeInterval(startDate, endDate, tempTimeIntervals);
                    }

                    if (CollectionUtils.isNotEmpty(dateList)) {
                        for (Map<String, String> date : dateList) {
                            // 已设置的抵现活动起止日期时段
                            long startDateTime = Long.parseLong(date.get("startDateTime"));
                            long endDateTime = Long.parseLong(date.get("endDateTime"));
                            for (Map<String, String> tempDate : tempDateList) {
                                // 新设置的抵现活动起止日期时段
                                long tempStartDateTime = Long.parseLong(tempDate.get("startDateTime"));
                                long tempEndDateTime = Long.parseLong(tempDate.get("endDateTime"));
                                // 比较时段不能有交集
                                if (startDateTime >= tempStartDateTime && startDateTime <= tempEndDateTime) {
                                    throw new BusinessException("时段与活动" + voucherId + "冲突");
                                }
                                if (startDateTime <= tempStartDateTime && endDateTime >= tempStartDateTime) {
                                    throw new BusinessException("时段与活动" + voucherId + "冲突");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult updateEndDateTime(String client, String voucherId, String endDate, String endTime) {
        if (org.apache.commons.lang3.StringUtils.isAnyBlank(endDate, endTime)) {
            throw new BusinessException("请输入结束时间!");
        }
        // 验证修改后的结束时间是否去其他活动冲突
        GaiaSdIntegralCash gaiaSdIntegralCash = new GaiaSdIntegralCash();
        gaiaSdIntegralCash.setClient(client);
        gaiaSdIntegralCash.setGsicVoucherId(voucherId);
        gaiaSdIntegralCash = gaiaSdIntegralCashMapper.selectOne(gaiaSdIntegralCash);
        // 验证开始结束时间大小
        String gsicStartDate = gaiaSdIntegralCash.getGsicStartDate();
        String gsicStartTime = gaiaSdIntegralCash.getGsicStartTime();

        if (Long.parseLong(gsicStartDate + gsicStartTime) > Long.parseLong(endDate + endTime)) {
            throw new BusinessException("结束时间不能小于于开始时间!");
        }

        PointCashOutActivityDTO pointCashOutActivityDTO = new PointCashOutActivityDTO();
        BeanUtil.copyProperties(gaiaSdIntegralCash, pointCashOutActivityDTO);
        List<GaiaSdIntegralCashSto> gaiaSdIntegralCashStos = gaiaSdIntegralCashMapper.selectStoListByVoucherId(client, voucherId);
        pointCashOutActivityDTO.setStoCodes(gaiaSdIntegralCashStos.stream().map(GaiaSdIntegralCashSto::getStoCode).collect(Collectors.toList()));

        // 验证活动时段范围
        pointCashOutActivityDTO.setGsicEndDate(endDate);
        pointCashOutActivityDTO.setGsicEndTime(endTime);
        checkEffectiveTimePeriod(pointCashOutActivityDTO);

        // 修改活动表中结束时间
        gaiaSdIntegralCash = new GaiaSdIntegralCash();
        gaiaSdIntegralCash.setClient(client);
        gaiaSdIntegralCash.setGsicVoucherId(voucherId);
        gaiaSdIntegralCash.setGsicEndDate(endDate);
        gaiaSdIntegralCash.setGsicEndTime(endTime);
        gaiaSdIntegralCashMapper.updateIntegralCashEndTime(gaiaSdIntegralCash);
        return JsonResult.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult reviewActivity(String client, String reviewer, String voucherId) {
        GaiaSdIntegralCash gaiaSdIntegralCash = new GaiaSdIntegralCash();
        gaiaSdIntegralCash.setClient(client);
        gaiaSdIntegralCash.setGsicVoucherId(voucherId);
        gaiaSdIntegralCash.setGsicReviewer(reviewer);
        gaiaSdIntegralCash.setGsicStatus(1);
        gaiaSdIntegralCash.setGsicReviewDate(new Date());
        gaiaSdIntegralCashMapper.reviewActivity(gaiaSdIntegralCash);
        return JsonResult.success();
    }

    @Override
    public JsonResult selectStoListByVoucherId(String client, String voucherId) {
        return JsonResult.success(gaiaSdIntegralCashMapper.selectStoListByVoucherId(client, voucherId));
    }

    @Override
    public JsonResult selectProListByVoucherId(PointProSearchDTO pointProSearchDTO) {
        PageHelper.startPage(pointProSearchDTO.getPageNum(), pointProSearchDTO.getPageSize());
        List<GaiaSdIntegralCashConds> gaiaSdIntegralCashConds = gaiaSdIntegralCashMapper.selectProListByVoucherId(pointProSearchDTO);
        return JsonResult.success(new PageInfo<>(gaiaSdIntegralCashConds));
    }

    @Override
    public JsonResult selectIntegralCashActivity(PointCashOutActivitySearchDTO pointCashOutActivitySearchDTO) {
        PageHelper.startPage(pointCashOutActivitySearchDTO.getPageNum(), pointCashOutActivitySearchDTO.getPageSize());
        List<String> cardTypes = pointCashOutActivitySearchDTO.getMemberCardTypes();
        if (CollectionUtils.isNotEmpty(cardTypes)) {
            StringJoiner sj = new StringJoiner("|");
            for (String cardType : cardTypes) {
                if (StringUtils.isNotBlank(cardType)) {
                    sj.add(cardType);
                }
            }
            if (sj.length() > 0) {
                pointCashOutActivitySearchDTO.setMemberCardType("(" + sj.toString() + ")");
            }
        }

        List<PointCashOutActivityVO> pointCashOutActivitys = gaiaSdIntegralCashMapper.selectIntegralCashActivity(pointCashOutActivitySearchDTO);
        String client = pointCashOutActivitySearchDTO.getClient();
        List<Map<String, String>> maps = gaiaSdIntegralCashMapper.selectTimeIntervals(client, null);
        Map<String, List<Map<String, String>>> stringListMap = maps.stream().collect(Collectors.groupingBy(item -> item.get("voucherId")));
        for (PointCashOutActivityVO pointCashOutActivity : pointCashOutActivitys) {
            String gsicVoucherId = pointCashOutActivity.getGsicVoucherId();
            List<Map<String, String>> stringStringMap = stringListMap.get(gsicVoucherId);
            if (CollectionUtils.isNotEmpty(stringStringMap)) {
                List<String> res = new ArrayList<>();
                for (Map<String, String> stringMap : stringStringMap) {
                    String startTime = stringMap.get("startTime");
                    String endTime = stringMap.get("endTime");
                    res.add(startTime + "~" + endTime);
                }
                pointCashOutActivity.setTimeIntervals(res);
            }
        }
        return JsonResult.success(new PageInfo<>(pointCashOutActivitys));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public JsonResult deleteIntegralCashActivity(String client, String voucherId) {
        GaiaSdIntegralCash gaiaSdIntegralCash = new GaiaSdIntegralCash();
        gaiaSdIntegralCash.setClient(client);
        gaiaSdIntegralCash.setGsicVoucherId(voucherId);
        gaiaSdIntegralCash = gaiaSdIntegralCashMapper.selectOne(gaiaSdIntegralCash);

        if (gaiaSdIntegralCash == null) {
            throw new BusinessException("抵现活动单号无效!");
        } else {
            Integer gsicStatus = gaiaSdIntegralCash.getGsicStatus();
            if (gsicStatus == 1) {
                throw new BusinessException("抵现活动已审核，无法删除!");
            }
        }

        // 删除主表数据
        gaiaSdIntegralCashMapper.deleteCash(client, voucherId);

        // 删除关联门店
        gaiaSdIntegralCashMapper.deleteStoWithCash(client, voucherId);

        // 删除关联时段
        gaiaSdIntegralCashMapper.deleteTimeIntervalsWithCash(client, voucherId);

        // 删除关联商品
        gaiaSdIntegralCashMapper.deleteProWithCash(client, voucherId);
        return JsonResult.success();
    }

    /**
     * 解析需要设置的时段范围
     *
     * @param pointCashOutActivityDTO pointCashOutActivityDTO
     * @return List<Map < String, String>>
     */
    private List<Map<String, String>> parseNewTimeInterval(PointCashOutActivityDTO pointCashOutActivityDTO) {
        String gsicStartDate = pointCashOutActivityDTO.getGsicStartDate();
        String gsicEndDate = pointCashOutActivityDTO.getGsicEndDate();
        String gsicStartTime = pointCashOutActivityDTO.getGsicStartTime();
        String gsicEndTime = pointCashOutActivityDTO.getGsicEndTime();

        List<String> tempTimeIntervals = pointCashOutActivityDTO.getTimeIntervals();
        if (CollectionUtils.isEmpty(tempTimeIntervals)) {
            tempTimeIntervals = new ArrayList<>();
            tempTimeIntervals.add(gsicStartTime + "~" + gsicEndTime);
        }

        String gsicDateFrequency = pointCashOutActivityDTO.getGsicDateFrequency();
        String gsicWeekFrequency = pointCashOutActivityDTO.getGsicWeekFrequency();
        // 频率作用于起止时间和时段
        if (StringUtils.isNotBlank(gsicDateFrequency)) {
            if (checkChooseTimeRang(gsicStartDate, gsicEndDate, Splitter.on(CharUtil.COMMA).splitToList(gsicDateFrequency), 1)) {
                throw new BusinessException("日期频率不在起止日期范围!");
            }
            // 起止时间内对应日期频率的时间段范围
            return transferTimeIntervalToSpecificTime(gsicStartDate, gsicEndDate, tempTimeIntervals,
                    Splitter.on(CharUtil.COMMA).splitToList(gsicDateFrequency), 1);
        } else if (StringUtils.isNotBlank(gsicWeekFrequency)) {
            if (checkChooseTimeRang(gsicStartDate, gsicEndDate, Splitter.on(CharUtil.COMMA).splitToList(gsicWeekFrequency), 2)) {
                throw new BusinessException("星期频率不在起止日期范围!");
            }
            // 起止时间内对应星期频率的时间段范围
            return transferTimeIntervalToSpecificTime(gsicStartDate, gsicEndDate, tempTimeIntervals, Splitter.on(CharUtil.COMMA).splitToList(gsicWeekFrequency), 2);
        } else {
            // 没设置频率则作用于每一天
            return wrapperCustomTimeInterval(gsicStartDate, gsicEndDate, tempTimeIntervals);
        }
    }

    /**
     * 将时段转换成具体生效的时间
     *
     * @param startDate         开始日期
     * @param endDate           结束日期
     * @param tempTimeIntervals 时段
     * @param frequency         频率
     * @param flag              flag
     * @return List<Map < String, String>>
     */
    private List<Map<String, String>> transferTimeIntervalToSpecificTime(String startDate, String endDate,
                                                                         List<String> tempTimeIntervals,
                                                                         List<String> frequency, Integer flag) {
        LocalDate startLocalDate = LocalDate.parse(DateUtil.dateConvert(startDate));
        LocalDate endLocalDate = LocalDate.parse(DateUtil.dateConvert(endDate));
        Period daysDiff = Period.between(startLocalDate, endLocalDate);
        int days = daysDiff.getDays();
        List<Map<String, String>> res = new ArrayList<>();
        if (days > 0) {
            if (1 == flag) {
                int startValue = startLocalDate.getDayOfMonth();
                int endValue = endLocalDate.getDayOfMonth();
                // 日期频率 从大于开始日期那天开始
                // 过滤掉小于开始日期,大于结束日期的日期频率
                frequency = frequency.stream()
                        .filter(item -> Long.parseLong(item) >= startValue && Long.parseLong(item) <= endValue)
                        .sorted()
                        .collect(Collectors.toList());
                if (CollectionUtils.isEmpty(frequency)) {
                    throw new BusinessException("日期频率不在所选时间范围内!");
                }
                for (String fre : frequency) {
                    LocalDate localDate = startLocalDate.plusDays(Integer.parseInt(fre));
                    String tempLocalDate = localDate.toString().replaceAll(StrUtil.DASHED, StrUtil.EMPTY);
                    wrapperActiveTimeIntervals(res, tempTimeIntervals, tempLocalDate);
                }
            } else {
                // 星期频率
                frequency = frequency.stream()
                        .sorted()
                        .collect(Collectors.toList());
                for (int i = 0; i <= days; i++) {
                    LocalDate localDate = startLocalDate.plusDays(i);
                    if (localDate.compareTo(endLocalDate) <= 0 && frequency.contains(String.valueOf(localDate.getDayOfWeek().getValue()))) {
                        String tempLocalDate = localDate.toString().replaceAll(StrUtil.DASHED, StrUtil.EMPTY);
                        wrapperActiveTimeIntervals(res, tempTimeIntervals, tempLocalDate);
                    }
                }
                if (CollectionUtils.isEmpty(res)) {
                    throw new BusinessException("星期频率不在所选时间范围内!");
                }
            }
        } else {
            // 同一天
            wrapperActiveTimeIntervals(res, tempTimeIntervals, startDate);
        }
        return res;
    }

    /**
     * 验证时间范围
     *
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @param frequency 频率
     * @param flag      flag
     * @return boolean
     */
    private boolean checkChooseTimeRang(String startDate, String endDate, List<String> frequency, Integer flag) {
        // 同一天验证选择的 星期，日期是否是单天
        if (startDate.equals(endDate)) {
            LocalDate startLocalDate = LocalDate.parse(DateUtil.dateConvert(startDate));
            if (flag == 2) {
                // 日期频率
                DayOfWeek dayOfWeek = startLocalDate.getDayOfWeek();
                return !frequency.contains(String.valueOf(dayOfWeek.getValue()));
            } else {
                // 星期频率
                int dayOfMonth = startLocalDate.getDayOfMonth();
                return !frequency.contains(String.valueOf(dayOfMonth));
            }
        }
        return false;
    }

    /**
     * 组装自定义时段
     *
     * @param startDate         开始日期
     * @param endDate           结束日期
     * @param tempTimeIntervals 时段
     * @return List<Map < String, String>>
     */
    private List<Map<String, String>> wrapperCustomTimeInterval(String startDate, String endDate, List<String> tempTimeIntervals) {
        List<String> res = new ArrayList<>();
        LocalDate startLocalDate = LocalDate.parse(DateUtil.dateConvert(startDate));
        LocalDate endLocalDate = LocalDate.parse(DateUtil.dateConvert(endDate));
        Period daysDiff = Period.between(startLocalDate, endLocalDate);
        int days = daysDiff.getDays();
        if (days > 0) {
            for (int i = -1; i < days; i++) {
                res.add(startLocalDate.plusDays(i + 1).toString().replaceAll(StrUtil.DASHED, StrUtil.EMPTY));
            }
        } else {
            res.add(startDate);
        }

        List<Map<String, String>> resMap = new ArrayList<>();
        for (String re : res) {
            wrapperActiveTimeIntervals(resMap, tempTimeIntervals, re);
        }
        return resMap;
    }

    /**
     * 组装生效时段
     *
     * @param resMap            resMap
     * @param tempTimeIntervals 时段集合
     * @param date              日期
     */
    private void wrapperActiveTimeIntervals(List<Map<String, String>> resMap,
                                            List<String> tempTimeIntervals, String date) {
        for (String tempTimeInterval : tempTimeIntervals) {
            Map<String, String> map = new HashMap<>(4);
            String[] split = tempTimeInterval.split("~");
            String startTime = split[0];
            String endTime = split[1];
            map.put("startDateTime", date + startTime);
            map.put("endDateTime", date + endTime);
            resMap.add(map);
        }
    }

}
