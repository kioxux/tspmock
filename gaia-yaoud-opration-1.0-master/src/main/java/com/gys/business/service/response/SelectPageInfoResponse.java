package com.gys.business.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SelectPageInfoResponse {

    @ApiModelProperty(value = "商品分类主键")
    private String id;

    @ApiModelProperty(value = "疾病大类编码")
    private String largeSicknessCoding;

    @ApiModelProperty(value = "疾病大类名称")
    private String largeSicknessName;

    @ApiModelProperty(value = "疾病中类编码")
    private String middleSicknessCoding;

    @ApiModelProperty(value = "疾病中类名称")
    private String middleSicknessName;

    @ApiModelProperty(value = "成分细类编码")
    private String smallSicknessCoding;

    @ApiModelProperty(value = "成分细类名称")
    private String smallSicknessName;

    @ApiModelProperty(value = "维护日期")
    private String maintenanceDate;

    @ApiModelProperty(value = "维护人")
    private String maintenanceUser;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "状态中文")
    private String statusStr;

}
