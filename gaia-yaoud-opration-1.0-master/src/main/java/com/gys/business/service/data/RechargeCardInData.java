//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class RechargeCardInData {
    private String clientId;
    private String gsrcId;
    private String gsrcUpdateBrId;
    private String gsrcUsableBrId;
    private String gsrcUsableStoreClass;
    private String gsrcBrId;
    private String gsrcStatus;
    private String gsrcName;
    private String gsrcMobile;
    private String gsrcSex;
    private String gsrcTel;
    private String gsrcAddress;
    private String gsrcPassword;
    private String newPassword;
    private boolean insert;

    public RechargeCardInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsrcId() {
        return this.gsrcId;
    }

    public String getGsrcUpdateBrId() {
        return this.gsrcUpdateBrId;
    }

    public String getGsrcUsableBrId() {
        return this.gsrcUsableBrId;
    }

    public String getGsrcUsableStoreClass() {
        return this.gsrcUsableStoreClass;
    }

    public String getGsrcBrId() {
        return this.gsrcBrId;
    }

    public String getGsrcStatus() {
        return this.gsrcStatus;
    }

    public String getGsrcName() {
        return this.gsrcName;
    }

    public String getGsrcMobile() {
        return this.gsrcMobile;
    }

    public String getGsrcSex() {
        return this.gsrcSex;
    }

    public String getGsrcTel() {
        return this.gsrcTel;
    }

    public String getGsrcAddress() {
        return this.gsrcAddress;
    }

    public String getGsrcPassword() {
        return this.gsrcPassword;
    }

    public String getNewPassword() {
        return this.newPassword;
    }

    public boolean isInsert() {
        return this.insert;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsrcId(final String gsrcId) {
        this.gsrcId = gsrcId;
    }

    public void setGsrcUpdateBrId(final String gsrcUpdateBrId) {
        this.gsrcUpdateBrId = gsrcUpdateBrId;
    }

    public void setGsrcUsableBrId(final String gsrcUsableBrId) {
        this.gsrcUsableBrId = gsrcUsableBrId;
    }

    public void setGsrcUsableStoreClass(final String gsrcUsableStoreClass) {
        this.gsrcUsableStoreClass = gsrcUsableStoreClass;
    }

    public void setGsrcBrId(final String gsrcBrId) {
        this.gsrcBrId = gsrcBrId;
    }

    public void setGsrcStatus(final String gsrcStatus) {
        this.gsrcStatus = gsrcStatus;
    }

    public void setGsrcName(final String gsrcName) {
        this.gsrcName = gsrcName;
    }

    public void setGsrcMobile(final String gsrcMobile) {
        this.gsrcMobile = gsrcMobile;
    }

    public void setGsrcSex(final String gsrcSex) {
        this.gsrcSex = gsrcSex;
    }

    public void setGsrcTel(final String gsrcTel) {
        this.gsrcTel = gsrcTel;
    }

    public void setGsrcAddress(final String gsrcAddress) {
        this.gsrcAddress = gsrcAddress;
    }

    public void setGsrcPassword(final String gsrcPassword) {
        this.gsrcPassword = gsrcPassword;
    }

    public void setNewPassword(final String newPassword) {
        this.newPassword = newPassword;
    }

    public void setInsert(final boolean insert) {
        this.insert = insert;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RechargeCardInData)) {
            return false;
        } else {
            RechargeCardInData other = (RechargeCardInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label183: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label183;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label183;
                    }

                    return false;
                }

                Object this$gsrcId = this.getGsrcId();
                Object other$gsrcId = other.getGsrcId();
                if (this$gsrcId == null) {
                    if (other$gsrcId != null) {
                        return false;
                    }
                } else if (!this$gsrcId.equals(other$gsrcId)) {
                    return false;
                }

                label169: {
                    Object this$gsrcUpdateBrId = this.getGsrcUpdateBrId();
                    Object other$gsrcUpdateBrId = other.getGsrcUpdateBrId();
                    if (this$gsrcUpdateBrId == null) {
                        if (other$gsrcUpdateBrId == null) {
                            break label169;
                        }
                    } else if (this$gsrcUpdateBrId.equals(other$gsrcUpdateBrId)) {
                        break label169;
                    }

                    return false;
                }

                Object this$gsrcUsableBrId = this.getGsrcUsableBrId();
                Object other$gsrcUsableBrId = other.getGsrcUsableBrId();
                if (this$gsrcUsableBrId == null) {
                    if (other$gsrcUsableBrId != null) {
                        return false;
                    }
                } else if (!this$gsrcUsableBrId.equals(other$gsrcUsableBrId)) {
                    return false;
                }

                label155: {
                    Object this$gsrcUsableStoreClass = this.getGsrcUsableStoreClass();
                    Object other$gsrcUsableStoreClass = other.getGsrcUsableStoreClass();
                    if (this$gsrcUsableStoreClass == null) {
                        if (other$gsrcUsableStoreClass == null) {
                            break label155;
                        }
                    } else if (this$gsrcUsableStoreClass.equals(other$gsrcUsableStoreClass)) {
                        break label155;
                    }

                    return false;
                }

                Object this$gsrcBrId = this.getGsrcBrId();
                Object other$gsrcBrId = other.getGsrcBrId();
                if (this$gsrcBrId == null) {
                    if (other$gsrcBrId != null) {
                        return false;
                    }
                } else if (!this$gsrcBrId.equals(other$gsrcBrId)) {
                    return false;
                }

                label141: {
                    Object this$gsrcStatus = this.getGsrcStatus();
                    Object other$gsrcStatus = other.getGsrcStatus();
                    if (this$gsrcStatus == null) {
                        if (other$gsrcStatus == null) {
                            break label141;
                        }
                    } else if (this$gsrcStatus.equals(other$gsrcStatus)) {
                        break label141;
                    }

                    return false;
                }

                label134: {
                    Object this$gsrcName = this.getGsrcName();
                    Object other$gsrcName = other.getGsrcName();
                    if (this$gsrcName == null) {
                        if (other$gsrcName == null) {
                            break label134;
                        }
                    } else if (this$gsrcName.equals(other$gsrcName)) {
                        break label134;
                    }

                    return false;
                }

                Object this$gsrcMobile = this.getGsrcMobile();
                Object other$gsrcMobile = other.getGsrcMobile();
                if (this$gsrcMobile == null) {
                    if (other$gsrcMobile != null) {
                        return false;
                    }
                } else if (!this$gsrcMobile.equals(other$gsrcMobile)) {
                    return false;
                }

                label120: {
                    Object this$gsrcSex = this.getGsrcSex();
                    Object other$gsrcSex = other.getGsrcSex();
                    if (this$gsrcSex == null) {
                        if (other$gsrcSex == null) {
                            break label120;
                        }
                    } else if (this$gsrcSex.equals(other$gsrcSex)) {
                        break label120;
                    }

                    return false;
                }

                label113: {
                    Object this$gsrcTel = this.getGsrcTel();
                    Object other$gsrcTel = other.getGsrcTel();
                    if (this$gsrcTel == null) {
                        if (other$gsrcTel == null) {
                            break label113;
                        }
                    } else if (this$gsrcTel.equals(other$gsrcTel)) {
                        break label113;
                    }

                    return false;
                }

                Object this$gsrcAddress = this.getGsrcAddress();
                Object other$gsrcAddress = other.getGsrcAddress();
                if (this$gsrcAddress == null) {
                    if (other$gsrcAddress != null) {
                        return false;
                    }
                } else if (!this$gsrcAddress.equals(other$gsrcAddress)) {
                    return false;
                }

                Object this$gsrcPassword = this.getGsrcPassword();
                Object other$gsrcPassword = other.getGsrcPassword();
                if (this$gsrcPassword == null) {
                    if (other$gsrcPassword != null) {
                        return false;
                    }
                } else if (!this$gsrcPassword.equals(other$gsrcPassword)) {
                    return false;
                }

                label92: {
                    Object this$newPassword = this.getNewPassword();
                    Object other$newPassword = other.getNewPassword();
                    if (this$newPassword == null) {
                        if (other$newPassword == null) {
                            break label92;
                        }
                    } else if (this$newPassword.equals(other$newPassword)) {
                        break label92;
                    }

                    return false;
                }

                if (this.isInsert() != other.isInsert()) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RechargeCardInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsrcId = this.getGsrcId();
        result = result * 59 + ($gsrcId == null ? 43 : $gsrcId.hashCode());
        Object $gsrcUpdateBrId = this.getGsrcUpdateBrId();
        result = result * 59 + ($gsrcUpdateBrId == null ? 43 : $gsrcUpdateBrId.hashCode());
        Object $gsrcUsableBrId = this.getGsrcUsableBrId();
        result = result * 59 + ($gsrcUsableBrId == null ? 43 : $gsrcUsableBrId.hashCode());
        Object $gsrcUsableStoreClass = this.getGsrcUsableStoreClass();
        result = result * 59 + ($gsrcUsableStoreClass == null ? 43 : $gsrcUsableStoreClass.hashCode());
        Object $gsrcBrId = this.getGsrcBrId();
        result = result * 59 + ($gsrcBrId == null ? 43 : $gsrcBrId.hashCode());
        Object $gsrcStatus = this.getGsrcStatus();
        result = result * 59 + ($gsrcStatus == null ? 43 : $gsrcStatus.hashCode());
        Object $gsrcName = this.getGsrcName();
        result = result * 59 + ($gsrcName == null ? 43 : $gsrcName.hashCode());
        Object $gsrcMobile = this.getGsrcMobile();
        result = result * 59 + ($gsrcMobile == null ? 43 : $gsrcMobile.hashCode());
        Object $gsrcSex = this.getGsrcSex();
        result = result * 59 + ($gsrcSex == null ? 43 : $gsrcSex.hashCode());
        Object $gsrcTel = this.getGsrcTel();
        result = result * 59 + ($gsrcTel == null ? 43 : $gsrcTel.hashCode());
        Object $gsrcAddress = this.getGsrcAddress();
        result = result * 59 + ($gsrcAddress == null ? 43 : $gsrcAddress.hashCode());
        Object $gsrcPassword = this.getGsrcPassword();
        result = result * 59 + ($gsrcPassword == null ? 43 : $gsrcPassword.hashCode());
        Object $newPassword = this.getNewPassword();
        result = result * 59 + ($newPassword == null ? 43 : $newPassword.hashCode());
        result = result * 59 + (this.isInsert() ? 79 : 97);
        return result;
    }

    public String toString() {
        return "RechargeCardInData(clientId=" + this.getClientId() + ", gsrcId=" + this.getGsrcId() + ", gsrcUpdateBrId=" + this.getGsrcUpdateBrId() + ", gsrcUsableBrId=" + this.getGsrcUsableBrId() + ", gsrcUsableStoreClass=" + this.getGsrcUsableStoreClass() + ", gsrcBrId=" + this.getGsrcBrId() + ", gsrcStatus=" + this.getGsrcStatus() + ", gsrcName=" + this.getGsrcName() + ", gsrcMobile=" + this.getGsrcMobile() + ", gsrcSex=" + this.getGsrcSex() + ", gsrcTel=" + this.getGsrcTel() + ", gsrcAddress=" + this.getGsrcAddress() + ", gsrcPassword=" + this.getGsrcPassword() + ", newPassword=" + this.getNewPassword() + ", insert=" + this.isInsert() + ")";
    }
}
