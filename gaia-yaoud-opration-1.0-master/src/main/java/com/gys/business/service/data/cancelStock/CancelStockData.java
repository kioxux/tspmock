package com.gys.business.service.data.cancelStock;

import lombok.Data;

@Data
public class CancelStockData {
    private String clientId;
    private String stoCode;
    private String dcCode;
    private String cancelStockCode;
    private String cancelReason;
    private String createDate;
    private String createTime;
    private String createEmp;
    private String cancelType;
    private String orderStatus;
}
