package com.gys.business.service.data;

import lombok.Data;

@Data
public class AuthGroupOutData {
   private String groupId;

   private String groupName;

   private String authSite;
}
