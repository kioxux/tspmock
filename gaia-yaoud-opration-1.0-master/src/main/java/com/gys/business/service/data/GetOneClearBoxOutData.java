//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import com.gys.business.mapper.entity.GaiaSdCleandrawerH;
import java.util.List;

public class GetOneClearBoxOutData {
    private GaiaSdCleandrawerH gaiaSdCleandrawerH;
    private List<GaiaSdCleandrawerDOutData> gaiaSdCleandrawerDList;
    private String clientId;

    public GetOneClearBoxOutData() {
    }

    public GaiaSdCleandrawerH getGaiaSdCleandrawerH() {
        return this.gaiaSdCleandrawerH;
    }

    public List<GaiaSdCleandrawerDOutData> getGaiaSdCleandrawerDList() {
        return this.gaiaSdCleandrawerDList;
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setGaiaSdCleandrawerH(final GaiaSdCleandrawerH gaiaSdCleandrawerH) {
        this.gaiaSdCleandrawerH = gaiaSdCleandrawerH;
    }

    public void setGaiaSdCleandrawerDList(final List<GaiaSdCleandrawerDOutData> gaiaSdCleandrawerDList) {
        this.gaiaSdCleandrawerDList = gaiaSdCleandrawerDList;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetOneClearBoxOutData)) {
            return false;
        } else {
            GetOneClearBoxOutData other = (GetOneClearBoxOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$gaiaSdCleandrawerH = this.getGaiaSdCleandrawerH();
                    Object other$gaiaSdCleandrawerH = other.getGaiaSdCleandrawerH();
                    if (this$gaiaSdCleandrawerH == null) {
                        if (other$gaiaSdCleandrawerH == null) {
                            break label47;
                        }
                    } else if (this$gaiaSdCleandrawerH.equals(other$gaiaSdCleandrawerH)) {
                        break label47;
                    }

                    return false;
                }

                Object this$gaiaSdCleandrawerDList = this.getGaiaSdCleandrawerDList();
                Object other$gaiaSdCleandrawerDList = other.getGaiaSdCleandrawerDList();
                if (this$gaiaSdCleandrawerDList == null) {
                    if (other$gaiaSdCleandrawerDList != null) {
                        return false;
                    }
                } else if (!this$gaiaSdCleandrawerDList.equals(other$gaiaSdCleandrawerDList)) {
                    return false;
                }

                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetOneClearBoxOutData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $gaiaSdCleandrawerH = this.getGaiaSdCleandrawerH();
        result = result * 59 + ($gaiaSdCleandrawerH == null ? 43 : $gaiaSdCleandrawerH.hashCode());
        Object $gaiaSdCleandrawerDList = this.getGaiaSdCleandrawerDList();
        result = result * 59 + ($gaiaSdCleandrawerDList == null ? 43 : $gaiaSdCleandrawerDList.hashCode());
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        return result;
    }

    public String toString() {
        return "GetOneClearBoxOutData(gaiaSdCleandrawerH=" + this.getGaiaSdCleandrawerH() + ", gaiaSdCleandrawerDList=" + this.getGaiaSdCleandrawerDList() + ", clientId=" + this.getClientId() + ")";
    }
}
