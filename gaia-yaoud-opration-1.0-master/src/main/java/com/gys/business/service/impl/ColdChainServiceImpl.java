package com.gys.business.service.impl;

import cn.hutool.json.JSONObject;
import com.gys.business.mapper.ColdChainMapper;
import com.gys.business.service.ColdChainService;
import com.gys.business.service.data.ColdChainInData;
import com.gys.util.GuoYaoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ColdChainServiceImpl implements ColdChainService {
    @Resource
    private ColdChainMapper lenLianMapper;

    @Override
    public List<JSONObject> listColdChainGoods(ColdChainInData inData) {
        List<JSONObject> goodsDetail = GuoYaoUtil.findGoodsDetail(inData.getStartTime(), inData.getEndTime(), inData.getGoodsownid());
        if (!CollectionUtils.isEmpty(goodsDetail)) {
            List<String> thirdCodeList = goodsDetail.stream().map(jsonObject -> jsonObject.get("goodsownid").toString()).collect(Collectors.toList());
            List<HashMap<String, Object>> hashMaps = lenLianMapper.listThirdInfo(thirdCodeList, inData.getClient());
            for (JSONObject goods : goodsDetail) {
                goods.put("hasFlag", 0);
                if (!CollectionUtils.isEmpty(hashMaps)) {
                    for (HashMap<String, Object> hashMap : hashMaps) {
                        if (hashMap.get("proDsfCode").toString().equals(goods.get("goodsownid").toString())) {
                            goods.put("hasFlag", 1);
                            goods.put("status", hashMap.get("status"));
                            break;
                        }
                    }
                }
            }
            return goodsDetail;
        }
        return null;
    }
}
