package com.gys.business.service.data.device;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/25 9:33
 **/
@Data
@ApiModel
public class UserInfoVO {
    @ApiModelProperty(value = "人员标识:1-仓库人员 2-门店人员", example = "1")
    private Integer userFlag;
}
