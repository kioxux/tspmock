package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.IOasSicknessTagService;
import com.gys.business.service.request.*;
import com.gys.business.service.response.InputExcelResp;
import com.gys.business.service.response.SelectDataEchoResponse;
import com.gys.business.service.response.SelectLargeOasSicknessPageInfoResponse;
import com.gys.business.service.response.SelectPageInfoResponse;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.OasSicknesType;
import com.gys.common.exception.BusinessException;
import com.gys.util.ExcelUtils;
import com.gys.util.ValidateUtil;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


/**
 * <p>
 * 疾病大类表 服务实现类
 * </p>
 *
 * @author flynn
 * @since 2021-09-01
 */
@Log4j
@Service
@Transactional(rollbackFor = Exception.class)//事务回滚
public class OasSicknessTagServiceImpl implements IOasSicknessTagService {
    @Autowired
    private OasSicknessTagMapper oasSicknessTagMapper;
    @Autowired
    private OasSicknessCodingTagMapper oasSicknessCodingTagMapper;
    @Autowired
    private OasSicknessCodingMapper oasSicknessCodingMapper;
    //中类成分关联表
    @Autowired
    private OasSicknessCodingElementMapper oasSicknessCodingElementMapper;
    //成分明细
    @Autowired
    private OasSicknessElementMapper oasSicknessElementMapper;

    /**
     * 疾病大类中类成分列表
     *
     * @param pageInfoRequest 请求参数
     * @return 返回值
     */
    @Override
    public SelectLargeOasSicknessPageInfoResponse selectOasSicknessPageInfo(SelectOasSicknessPageInfoRequest pageInfoRequest) {
        List<SelectLargeOasSicknessPageInfoResponse.SelectMiddleOasSicknessPageInfoResponse> oaSickles = new ArrayList<>();
        SelectLargeOasSicknessPageInfoResponse response = new SelectLargeOasSicknessPageInfoResponse();
        if (Objects.isNull(pageInfoRequest.getType())) {
            throw new BusinessException("疾病类型必填");
        }
        PageHelper.startPage(pageInfoRequest.getPageNum(), pageInfoRequest.getPageSize());
        if (pageInfoRequest.getType().equals(OasSicknesType.OasSicknesLargeType.getType())) {
            oaSickles = oasSicknessTagMapper.SelectLargeOasSickness(pageInfoRequest);
        } else if (pageInfoRequest.getType().equals(OasSicknesType.OasSicknesMiddleType.getType())) {
            if (StrUtil.isBlank(pageInfoRequest.getLargeSicknessCoding())) {
                throw new BusinessException("大类疾病编码必填");
            }
            oaSickles = oasSicknessTagMapper.SelectMiddleOasSickness(pageInfoRequest);
            response.setLargeSicknessCoding(pageInfoRequest.getLargeSicknessCoding());
            response.setLargeSicknessName(pageInfoRequest.getLargeSicknessName());
        } else if (pageInfoRequest.getType().equals(OasSicknesType.OasSicknesSmalleType.getType())) {
            if (StrUtil.isBlank(pageInfoRequest.getLargeSicknessCoding()) || StrUtil.isBlank(pageInfoRequest.getMiddleSicknessCoding())) {
                throw new BusinessException("大类疾病编码必填和中类疾病编码必填");
            }
            oaSickles = oasSicknessTagMapper.SelectSmallOasSickness(pageInfoRequest);
            response.setLargeSicknessCoding(pageInfoRequest.getLargeSicknessCoding());
            response.setLargeSicknessName(pageInfoRequest.getLargeSicknessName());
            response.setMiddleSicknessCoding(pageInfoRequest.getMiddleSicknessCoding());
            response.setMiddleSicknessCodingName(pageInfoRequest.getMiddleSicknessName());
        }
        PageInfo pageInfo = new PageInfo();
        if (CollUtil.isNotEmpty(oaSickles)) {
            pageInfo = new PageInfo(oaSickles);
        } else {
            pageInfo = new PageInfo(new ArrayList());
        }
        response.setPageInfo(pageInfo);
        return response;
    }


    /**
     * 新增疾病分类
     *
     * @param request 请求参数
     */
    @Override
    public void saveOasSickness(SaveOasSicknessRequest request) {
        if (Objects.isNull(request.getType())) {
            throw new BusinessException("疾病类型必填");
        }
        List<String> sickness = new ArrayList<>();
        List<SaveOasSicknessRequest.SaveOasSickness> oasSicknessList = request.getOasSicknessList();
        if (CollUtil.isNotEmpty(oasSicknessList)) {
            Map<String, List<SaveOasSicknessRequest.SaveOasSickness>> map = oasSicknessList.stream().collect(Collectors.groupingBy(oas -> oas.getSicknessCoding()));
            for (Map.Entry<String, List<SaveOasSicknessRequest.SaveOasSickness>> entry : map.entrySet()) {
                List<SaveOasSicknessRequest.SaveOasSickness> sicknesses = entry.getValue();
                if (sicknesses != null && sicknesses.size() > 1) {
                    sickness.addAll(sicknesses.stream().distinct().map(sick -> sick.getSicknessCoding()).collect(Collectors.toList()));
                }
            }
            if (CollUtil.isNotEmpty(sickness)) {
                String name = OasSicknesType.getName(request.getType());
                throw new BusinessException(name + "编码不能重复:" + StringUtils.join(sickness, ","));
            }
            //大类
            if (request.getType().equals(OasSicknesType.OasSicknesLargeType.getType())) {
                //判断新增的大类编码是否存在
                Example example = new Example(OasSicknessTag.class);
                example.createCriteria().andIn("sioSicknessTag", oasSicknessList.stream().map(sick -> sick.getSicknessCoding()).collect(Collectors.toList()));
                List<OasSicknessTag> oasSicknessTags = oasSicknessTagMapper.selectByExample(example);
                if (oasSicknessTags != null && oasSicknessTags.size() > 0) {
                    throw new BusinessException("大类疾病编码已存在" + StringUtils.join(oasSicknessTags.stream().map(s -> s.getSioSicknessTag()).collect(Collectors.toList()), ","));
                }
                List<OasSicknessTag> sicknessTags = oasSicknessList.stream().filter(s -> StrUtil.isNotBlank(s.getSicknessCoding())).map(sick -> {
                    OasSicknessTag oasSicknessTag = new OasSicknessTag();
                    oasSicknessTag.setSioSicknessTag(sick.getSicknessCoding());
                    oasSicknessTag.setSioSicknessTagName(sick.getSicknessName());
                    oasSicknessTag.setSioCjrq(LocalDate.now());
                    oasSicknessTag.setSioCjsj(LocalDateTime.now());
                    oasSicknessTag.setSioCjr(request.getUserId());
                    oasSicknessTag.setSioCjrName(request.getUserName());
                    oasSicknessTag.setSioXgrq(LocalDate.now());
                    oasSicknessTag.setSioXgsj(LocalDateTime.now());
                    oasSicknessTag.setSioXgr(request.getUserId());
                    oasSicknessTag.setSioXgrName(request.getUserName());
                    oasSicknessTag.setStatus(Convert.toStr(0));
                    return oasSicknessTag;
                }).collect(Collectors.toList());
                if (CollUtil.isNotEmpty(sicknessTags)) {
                    oasSicknessTagMapper.insertList(sicknessTags);
                }
                //中类
            } else if (request.getType().equals(OasSicknesType.OasSicknesMiddleType.getType())) {
                if (StrUtil.isBlank(request.getSicknessCoding())) {
                    throw new BusinessException("大类疾病编码必填");
                }
                Example sicknessCoding = new Example(OasSicknessTag.class);
                sicknessCoding.createCriteria().andEqualTo("sioSicknessTag", request.getSicknessCoding());
                List<OasSicknessTag> oasSicknessTags = oasSicknessTagMapper.selectByExample(sicknessCoding);
                if (CollUtil.isEmpty(oasSicknessTags)) {
                    throw new BusinessException("大类疾病编码不存在:" + request.getSicknessCoding());
                }
                //判断新增的大类编码是否存在
                Example example = new Example(OasSicknessCoding.class);
                example.createCriteria().andIn("sioSicknessCoding", oasSicknessList.stream().map(sick -> sick.getSicknessCoding()).collect(Collectors.toList()));
                List<OasSicknessCoding> oasSicknessCodings = oasSicknessCodingMapper.selectByExample(example);
                if (oasSicknessCodings != null && oasSicknessCodings.size() > 0) {
                    throw new BusinessException("中类疾病编码已存在" + StringUtils.join(oasSicknessCodings.stream().map(s -> s.getSioSicknessCoding()).collect(Collectors.toList()), ","));
                }
                List<OasSicknessCoding> sicknessCodings = new ArrayList<>();
                List<OasSicknessCodingTag> codingTags = new ArrayList<>();
                oasSicknessList.stream().filter(s -> StrUtil.isNotBlank(s.getSicknessCoding())).forEach(sick -> {
                    //中类维护主表
                    OasSicknessCoding oasSicknessCoding = new OasSicknessCoding();
                    oasSicknessCoding.setSioSicknessCoding(sick.getSicknessCoding());
                    oasSicknessCoding.setSioSicknessCodingName(sick.getSicknessName());
                    oasSicknessCoding.setSioCjrq(LocalDate.now());
                    oasSicknessCoding.setSioCjsj(LocalDateTime.now());
                    oasSicknessCoding.setSioCjr(request.getUserId());
                    oasSicknessCoding.setSioXgrq(LocalDate.now());
                    oasSicknessCoding.setSioXgsj(LocalDateTime.now());
                    oasSicknessCoding.setSioXgr(request.getUserId());
                    sicknessCodings.add(oasSicknessCoding);
                    //中类-大类关联
                    OasSicknessCodingTag oasSicknessCodingTag = new OasSicknessCodingTag();
                    oasSicknessCodingTag.setSioSicknessTag(request.getSicknessCoding());
                    oasSicknessCodingTag.setSioSicknessCoding(sick.getSicknessCoding());
                    oasSicknessCodingTag.setSioCjrq(LocalDate.now());
                    oasSicknessCodingTag.setSioCjsj(LocalDateTime.now());
                    oasSicknessCodingTag.setSioCjr(request.getUserId());
                    oasSicknessCodingTag.setSioXgrq(LocalDate.now());
                    oasSicknessCodingTag.setSioXgsj(LocalDateTime.now());
                    oasSicknessCodingTag.setSioXgr(request.getUserId());
                    codingTags.add(oasSicknessCodingTag);
                });
                if (CollUtil.isNotEmpty(sicknessCodings)) {
                    oasSicknessCodingMapper.insertList(sicknessCodings);
                }
                if (CollUtil.isNotEmpty(codingTags)) {
                    oasSicknessCodingTagMapper.insertList(codingTags);
                }
            } else if (request.getType().equals(OasSicknesType.OasSicknesSmalleType.getType())) {
                if (StrUtil.isBlank(request.getSicknessCoding())) {
                    throw new BusinessException("疾病编码必填");
                }
                Example sickExample = new Example(OasSicknessCoding.class);
                sickExample.createCriteria().andEqualTo("sioSicknessCoding", request.getSicknessCoding());
                List<OasSicknessCoding> list = oasSicknessCodingMapper.selectByExample(sickExample);
                if (CollUtil.isEmpty(list)) {
                    throw new BusinessException("中类疾病编码不存在" + request.getSicknessCoding());
                }

                //判断新增的大类编码是否存在
                Example examples = new Example(OasSicknessCodingElement.class);
                examples.createCriteria().andEqualTo("sioSicknessCoding", request.getSicknessCoding());
                List<OasSicknessCodingElement> elements = oasSicknessCodingElementMapper.selectByExample(examples);
                if (CollUtil.isNotEmpty(elements)) {
                    List<String> collect = oasSicknessList.stream().map(s -> s.getSicknessCoding()).collect(Collectors.toList());
                    List<OasSicknessCodingElement> collect1 = elements.stream().filter(s -> collect.contains(s.getSioElementId())).collect(Collectors.toList());
                    if(CollUtil.isNotEmpty(collect1)){
                        throw new BusinessException("同一中类下成分疾病编码已存在" + StringUtils.join(collect1.stream().map(s -> s.getSioElementId()).collect(Collectors.toList()), ","));
                    }
                }

                /*//判断新增的大类编码是否存在
                Example example = new Example(OasSicknessElement.class);
                example.createCriteria().andIn("sioElementId", oasSicknessList.stream().map(sick -> sick.getSicknessCoding()).collect(Collectors.toList()));
                List<OasSicknessElement> oasSicknessCodings = oasSicknessElementMapper.selectByExample(example);
                if (oasSicknessCodings != null && oasSicknessCodings.size() > 0) {
                    throw new BusinessException("成分明细编码已存在" + StringUtils.join(oasSicknessCodings.stream().map(s -> s.getSioElementId()).collect(Collectors.toList()), ","));
                }*/
                List<OasSicknessElement> sicknessCodings = new ArrayList<>();
                List<OasSicknessCodingElement> codingTags = new ArrayList<>();
                //成分主表
                oasSicknessList.stream().filter(s -> StrUtil.isNotBlank(s.getSicknessCoding())).forEach(sick -> {
                    OasSicknessElement sicknessElement = new OasSicknessElement();
                    sicknessElement.setSioElementId(sick.getSicknessCoding());
                    sicknessElement.setSioElementName(sick.getSicknessName());
                    sicknessElement.setSioCjrq(LocalDate.now());
                    sicknessElement.setSioCjsj(LocalDateTime.now());
                    sicknessElement.setSioCjr(request.getUserId());
                    sicknessElement.setSioXgrq(LocalDate.now());
                    sicknessElement.setSioXgsj(LocalDateTime.now());
                    sicknessElement.setSioXgr(request.getUserId());
                    sicknessCodings.add(sicknessElement);
                    OasSicknessCodingElement sicknessCodingElement = new OasSicknessCodingElement();
                    sicknessCodingElement.setSioSicknessCoding(request.getSicknessCoding());
                    sicknessCodingElement.setSioElementId(sick.getSicknessCoding());
                    sicknessCodingElement.setSioCjrq(LocalDate.now());
                    sicknessCodingElement.setSioCjsj(LocalDateTime.now());
                    sicknessCodingElement.setSioCjr(request.getUserId());
                    sicknessCodingElement.setSioXgrq(LocalDate.now());
                    sicknessCodingElement.setSioXgsj(LocalDateTime.now());
                    sicknessCodingElement.setSioXgr(request.getUserId());
                    codingTags.add(sicknessCodingElement);
                });
                if (CollUtil.isNotEmpty(sicknessCodings)) {
                    oasSicknessElementMapper.insertList(sicknessCodings);
                }
                if (CollUtil.isNotEmpty(codingTags)) {
                    oasSicknessCodingElementMapper.insertList(codingTags);
                }
            }
        }
    }

    /**
     * 导入疾病分类Excel数据
     *
     * @param
     */
    @Override
    public void inputExcel(MultipartFile file, String userId, String loginName) {
        List<InputExcelResp> inputExcel = checkFileData(file);
        //查验大类编码 中类编码 成分明细是否有重复数据
        checkSicknessData(inputExcel);
        //根据疾病大类分组
        if (CollUtil.isNotEmpty(inputExcel)) {
            List<OasSicknessTag> oasSicknessTagList = new ArrayList<>();
            List<OasSicknessCodingTag> oasSicknessCodingTagList = new ArrayList<>();
            List<OasSicknessCoding> oasSicknessCodingList = new ArrayList<>();
            List<OasSicknessCodingElement> sicknessCodingElementList = new ArrayList<>();
            List<OasSicknessElement> sicknessElementList = new ArrayList<>();
            //根据大类分组
            Map<String, List<InputExcelResp>> large = inputExcel.stream().collect(Collectors.groupingBy(InputExcelResp::getLargeSicknessCoding));
            if (CollUtil.isNotEmpty(large)) {
                large.entrySet().forEach(lar -> {
                    OasSicknessTag oasSicknessTag = new OasSicknessTag();
                    oasSicknessTag.setSioSicknessTag(lar.getKey());
                    oasSicknessTag.setSioSicknessTagName(CollUtil.isNotEmpty(lar.getValue()) ? lar.getValue().stream().map(s -> s.getLargeSicknessName()).findAny().orElse("") : "");
                    oasSicknessTag.setSioCjrq(LocalDate.now());
                    oasSicknessTag.setSioCjsj(LocalDateTime.now());
                    oasSicknessTag.setStatus("0");
                    oasSicknessTag.setSioCjr(userId);
                    oasSicknessTag.setSioCjrName(loginName);
                    oasSicknessTag.setSioXgrq(LocalDate.now());
                    oasSicknessTag.setSioXgsj(LocalDateTime.now());
                    oasSicknessTag.setSioXgr(userId);
                    oasSicknessTag.setSioXgrName(loginName);
                    oasSicknessTagList.add(oasSicknessTag);
                    List<InputExcelResp> middleSickness = lar.getValue();
                    if (CollUtil.isNotEmpty(middleSickness)) {
                        //根据中类分组
                        Map<String, List<InputExcelResp>> mids = middleSickness.stream().collect(Collectors.groupingBy(s -> s.getMiddleSicknessCoding()));
                        if (CollUtil.isNotEmpty(mids)) {
                            mids.entrySet().forEach(mid -> {
                                OasSicknessCodingTag oasSicknessCodingTag = new OasSicknessCodingTag();
                                OasSicknessCoding oasSicknessCoding = new OasSicknessCoding();
                                oasSicknessCodingTag.setSioSicknessTag(lar.getKey());
                                if (CollUtil.isNotEmpty(mid.getValue())) {
                                    mid.getValue().forEach(v -> {
                                        oasSicknessCodingTag.setSioSicknessCoding(v.getMiddleSicknessCoding());
                                        oasSicknessCoding.setSioSicknessCoding(v.getMiddleSicknessCoding());
                                        oasSicknessCoding.setSioSicknessCodingName(v.getMiddleSicknessName());
                                    });
                                    oasSicknessCodingTag.setSioCjrq(LocalDate.now());
                                    oasSicknessCodingTag.setSioCjsj(LocalDateTime.now());
                                    oasSicknessCodingTag.setSioCjr(userId);
                                    oasSicknessCodingTag.setSioXgrq(LocalDate.now());
                                    oasSicknessCodingTag.setSioXgsj(LocalDateTime.now());
                                    oasSicknessCodingTag.setSioXgr(loginName);
                                    oasSicknessCodingTagList.add(oasSicknessCodingTag);
                                    oasSicknessCoding.setSioCjrq(LocalDate.now());
                                    oasSicknessCoding.setSioCjsj(LocalDateTime.now());
                                    oasSicknessCoding.setSioCjr(userId);
                                    oasSicknessCoding.setSioXgrq(LocalDate.now());
                                    oasSicknessCoding.setSioXgsj(LocalDateTime.now());
                                    oasSicknessCoding.setSioXgr(loginName);
                                    oasSicknessCodingList.add(oasSicknessCoding);
                                    if (CollUtil.isNotEmpty(mid.getValue())) {
                                        //根据成分明细分组
                                        Map<String, List<InputExcelResp>> small = mid.getValue().stream().collect(Collectors.groupingBy(s -> s.getSmallSicknessCoding()));
                                        //判断
                                        if (CollUtil.isNotEmpty(small)) {
                                            small.entrySet().forEach(sm -> {
                                                OasSicknessCodingElement sicknessCodingElement = new OasSicknessCodingElement();
                                                OasSicknessElement sicknessElement = new OasSicknessElement();
                                                sicknessCodingElement.setSioSicknessCoding(mid.getKey());
                                                if (CollUtil.isNotEmpty(sm.getValue())) {
                                                    sm.getValue().forEach(sms -> {
                                                        sicknessCodingElement.setSioElementId(sms.getSmallSicknessCoding());
                                                        sicknessElement.setSioElementId(sms.getSmallSicknessCoding());
                                                        sicknessElement.setSioElementName(sms.getSmallSicknessName());
                                                    });
                                                }
                                                sicknessCodingElement.setSioCjrq(LocalDate.now());
                                                sicknessCodingElement.setSioCjsj(LocalDateTime.now());
                                                sicknessCodingElement.setSioCjr(userId);
                                                sicknessCodingElement.setSioXgrq(LocalDate.now());
                                                sicknessCodingElement.setSioXgsj(LocalDateTime.now());
                                                sicknessCodingElement.setSioXgr(loginName);
                                                sicknessCodingElementList.add(sicknessCodingElement);
                                                sicknessElement.setSioCjrq(LocalDate.now());
                                                sicknessElement.setSioCjsj(LocalDateTime.now());
                                                sicknessElement.setSioCjr(userId);
                                                sicknessElement.setSioXgrq(LocalDate.now());
                                                sicknessElement.setSioXgsj(LocalDateTime.now());
                                                sicknessElement.setSioXgr(loginName);
                                                sicknessElementList.add(sicknessElement);
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
            if (CollUtil.isNotEmpty(oasSicknessTagList)) {
                oasSicknessTagMapper.insertList(oasSicknessTagList);
            }
            if (CollUtil.isNotEmpty(oasSicknessCodingTagList)) {
                oasSicknessCodingTagMapper.insertList(oasSicknessCodingTagList);
            }
            if (CollUtil.isNotEmpty(oasSicknessCodingList)) {
                oasSicknessCodingMapper.insertList(oasSicknessCodingList);
            }
            if (CollUtil.isNotEmpty(sicknessCodingElementList)) {
                oasSicknessCodingElementMapper.insertList(sicknessCodingElementList);
            }
            if (CollUtil.isNotEmpty(sicknessElementList)) {
                oasSicknessElementMapper.insertList(sicknessElementList);
            }
        }
    }

    /**
     * 备用方法
     */
    private void save(List<InputExcelResp> inputExcel) {
         /*for (InputExcelResp inputExcelResp : inputExcel) {
                OasSicknessTag oasSicknessTag = new OasSicknessTag();
                oasSicknessTag.setSioSicknessTag(inputExcelResp.getLargeSicknessCoding());
                oasSicknessTag.setSioSicknessTagName(inputExcelResp.getLargeSicknessName());
                oasSicknessTag.setSioCjrq(LocalDate.now());
                oasSicknessTag.setSioCjsj(LocalDateTime.now());
                oasSicknessTag.setStatus("0");
                oasSicknessTag.setSioCjr(userId);
                oasSicknessTag.setSioCjrName(loginName);
                oasSicknessTag.setSioXgrq(LocalDate.now());
                oasSicknessTag.setSioXgsj(LocalDateTime.now());
                oasSicknessTag.setSioXgr(userId);
                oasSicknessTag.setSioXgrName(loginName);
                oasSicknessTagList.add(oasSicknessTag);
                //大类-中类关联表
                OasSicknessCodingTag oasSicknessCodingTag = new OasSicknessCodingTag();
                oasSicknessCodingTag.setSioSicknessTag(inputExcelResp.getLargeSicknessCoding());
                oasSicknessCodingTag.setSioSicknessCoding(inputExcelResp.getMiddleSicknessCoding());
                oasSicknessCodingTag.setSioCjrq(LocalDate.now());
                oasSicknessCodingTag.setSioCjsj(LocalDateTime.now());
                oasSicknessCodingTag.setSioCjr(userId);
                oasSicknessCodingTag.setSioXgrq(LocalDate.now());
                oasSicknessCodingTag.setSioXgsj(LocalDateTime.now());
                oasSicknessCodingTag.setSioXgr(loginName);
                oasSicknessCodingTagList.add(oasSicknessCodingTag);
                //中类
                OasSicknessCoding oasSicknessCoding = new OasSicknessCoding();
                oasSicknessCoding.setSioSicknessCoding(inputExcelResp.getMiddleSicknessCoding());
                oasSicknessCoding.setSioSicknessCodingName(inputExcelResp.getMiddleSicknessName());
                oasSicknessCoding.setSioCjrq(LocalDate.now());
                oasSicknessCoding.setSioCjsj(LocalDateTime.now());
                oasSicknessCoding.setSioCjr(userId);
                oasSicknessCoding.setSioXgrq(LocalDate.now());
                oasSicknessCoding.setSioXgsj(LocalDateTime.now());
                oasSicknessCoding.setSioXgr(loginName);
                oasSicknessCodingList.add(oasSicknessCoding);
                //中类-成分明细关联表
                OasSicknessCodingElement sicknessCodingElement = new OasSicknessCodingElement();
                sicknessCodingElement.setSioSicknessCoding(inputExcelResp.getMiddleSicknessCoding());
                sicknessCodingElement.setSioElementId(inputExcelResp.getSmallSicknessCoding());
                sicknessCodingElement.setSioCjrq(LocalDate.now());
                sicknessCodingElement.setSioCjsj(LocalDateTime.now());
                sicknessCodingElement.setSioCjr(userId);
                sicknessCodingElement.setSioXgrq(LocalDate.now());
                sicknessCodingElement.setSioXgsj(LocalDateTime.now());
                sicknessCodingElement.setSioXgr(loginName);
                sicknessCodingElementList.add(sicknessCodingElement);
                OasSicknessElement sicknessElement = new OasSicknessElement();
                sicknessElement.setSioElementId(inputExcelResp.getSmallSicknessCoding());
                sicknessElement.setSioElementName(inputExcelResp.getSmallSicknessName());
                sicknessElement.setSioCjrq(LocalDate.now());
                sicknessElement.setSioCjsj(LocalDateTime.now());
                sicknessElement.setSioCjr(userId);
                sicknessElement.setSioXgrq(LocalDate.now());
                sicknessElement.setSioXgsj(LocalDateTime.now());
                sicknessElement.setSioXgr(loginName);
                sicknessElementList.add(sicknessElement);
            }
            if (CollUtil.isNotEmpty(oasSicknessTagList)) {
                ArrayList<OasSicknessTag> arrayList = oasSicknessTagList.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(s -> s.getSioSicknessTag()))), ArrayList::new)
                );
                oasSicknessTagMapper.insertList(arrayList);
            }
            if (CollUtil.isNotEmpty(oasSicknessCodingTagList)) {
                ArrayList<OasSicknessCodingTag> arrayList = oasSicknessCodingTagList.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(s -> s.getSioSicknessCoding()))), ArrayList::new)
                );
                oasSicknessCodingTagMapper.insertList(arrayList);
            }
            if (CollUtil.isNotEmpty(oasSicknessCodingList)) {
                ArrayList<OasSicknessCoding> arrayList = oasSicknessCodingList.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(s -> s.getSioSicknessCoding()))), ArrayList::new)
                );
                oasSicknessCodingMapper.insertList(arrayList);
            }
            if (CollUtil.isNotEmpty(sicknessCodingElementList)) {
                ArrayList<OasSicknessCodingElement> arrayList = sicknessCodingElementList.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(s -> s.getSioElementId()))), ArrayList::new)
                );
                oasSicknessCodingElementMapper.insertList(arrayList);
            }
            if (CollUtil.isNotEmpty(sicknessElementList)) {
                ArrayList<OasSicknessElement> arrayList = sicknessElementList.stream().collect(
                        Collectors.collectingAndThen(
                                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(s -> s.getSioElementId()))), ArrayList::new)
                );
                oasSicknessElementMapper.insertList(arrayList);
            }*/
    }

    /**
     * 查验大类编码 中类编码 成分明细是否有重复数据
     *
     * @param inputExcel
     */
    private void checkSicknessData(List<InputExcelResp> inputExcel) {

        //大类编码
        List<String> largeSicknessCoding = inputExcel.stream().map(InputExcelResp::getLargeSicknessCoding).distinct().collect(Collectors.toList());
        if (CollUtil.isNotEmpty(largeSicknessCoding)) {
            Example example = new Example(OasSicknessTag.class);
            example.createCriteria().andIn("sioSicknessTag", largeSicknessCoding);
            List<OasSicknessTag> oasSicknessTags = oasSicknessTagMapper.selectByExample(example);
            //大类编码已存在  查看
            if (oasSicknessTags != null && oasSicknessTags.size() > 0) {
                throw new BusinessException("大类疾病编码已存在" + StringUtils.join(oasSicknessTags.stream().map(s -> s.getSioSicknessTag()).collect(Collectors.toList()), ","));
            }
        }
        //中类
        List<String> middleSicknessCoding = inputExcel.stream().map(InputExcelResp::getMiddleSicknessCoding).distinct().collect(Collectors.toList());
        if (CollUtil.isNotEmpty(middleSicknessCoding)) {
            //判断新增的大类编码是否存在
            Example example = new Example(OasSicknessCoding.class);
            example.createCriteria().andIn("sioSicknessCoding", middleSicknessCoding);
            List<OasSicknessCoding> oasSicknessCodings = oasSicknessCodingMapper.selectByExample(example);
            if (oasSicknessCodings != null && oasSicknessCodings.size() > 0) {
                throw new BusinessException("中类疾病编码已存在" + StringUtils.join(oasSicknessCodings.stream().map(s -> s.getSioSicknessCoding()).collect(Collectors.toList()), ","));
            }
        }
        //检验成分明细存不存在
        List<Map<String,Object>> list =   oasSicknessCodingElementMapper.selectMingx();
        if(CollUtil.isNotEmpty(list)){
            List<String> smallSicknessCoding = inputExcel.stream().map(InputExcelResp::getSmallSicknessCoding).distinct().collect(Collectors.toList());
            List<String> code = list.stream().filter(s -> smallSicknessCoding.contains(Convert.toStr(s.get("code")))).map(s->Convert.toStr(s.get("code"))).collect(Collectors.toList());
            smallSicknessCoding.removeAll(code);
            if(CollUtil.isNotEmpty(smallSicknessCoding)){
                throw new BusinessException("【"+StringUtils.join(smallSicknessCoding,",") +"】成分明细不存在,请检查后重新导入");
            }
        }


        /*List<String> smallSicknessCoding = inputExcel.stream().map(InputExcelResp::getSmallSicknessCoding).distinct().collect(Collectors.toList());
        if (CollUtil.isNotEmpty(smallSicknessCoding)) {
            //判断新增的大类编码是否存在
            Example example = new Example(OasSicknessElement.class);
            example.createCriteria().andIn("sioElementId", smallSicknessCoding);
            List<OasSicknessElement> oasSicknessCodings = oasSicknessElementMapper.selectByExample(example);
            if (oasSicknessCodings != null && oasSicknessCodings.size() > 0) {
                throw new BusinessException("成分明细疾病编码已存在" + StringUtils.join(oasSicknessCodings.stream().map(s -> s.getSioElementId()).collect(Collectors.toList()), ","));
            }
        }*/
    }

    /**
     * 检查文件以及数据完整性
     *
     * @param file
     * @author SunJiaNan
     * @Date 2021-07-08
     */
    private List<InputExcelResp> checkFileData(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (ValidateUtil.isEmpty(fileName)) {
            throw new BusinessException("提示：上传文件名不能为空");
        }
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        if (!".xlsx".equals(suffixName)) {
            throw new BusinessException("提示：请检查文件格式是否为xlsx格式");
        }
        //创建sheet  第一页，第一行
        Sheet sheet = new Sheet(1, 0, com.gys.business.service.response.InputExcelResp.class);
        //获取到 GaiaChannelProduct实体集合
        List<Object> excelDataList = null;
        try {
            excelDataList = EasyExcelFactory.read(file.getInputStream(), sheet);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("上传特殊商品,读取excel失败: ", e);
            throw new BusinessException("提示：读取Excel失败");
        }
        if (ValidateUtil.isEmpty(excelDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }

        //转换list泛型
        List<InputExcelResp> channelProductList = new ArrayList<>(excelDataList.size());
        try {
            channelProductList = JSON.parseArray(JSON.toJSONString(excelDataList), InputExcelResp.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据类型转换异常：", e);
            throw new BusinessException("提示：数据格式不正确, 请检查数据");
        }
        if (ValidateUtil.isEmpty(channelProductList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        //校验表头
        InputExcelResp dataHead = channelProductList.get(0);
        String[] channelManagerExcelHead = CommonConstant.CHANNEL_SICKNESS;
        if (!channelManagerExcelHead[0].equals(dataHead.getLargeSicknessCoding()) || !channelManagerExcelHead[1].equals(dataHead.getLargeSicknessName()) ||
                !channelManagerExcelHead[2].equals(dataHead.getMiddleSicknessCoding()) || !channelManagerExcelHead[3].equals(dataHead.getMiddleSicknessName()) ||
                !channelManagerExcelHead[4].equals(dataHead.getSmallSicknessCoding()) || !channelManagerExcelHead[5].equals(dataHead.getSmallSicknessName())) {
            throw new BusinessException("提示：请填写正确的表头");
        }
        //截取除表头以外数据
        List<InputExcelResp> channelProductDataList = channelProductList.subList(1, channelProductList.size());
        if (ValidateUtil.isEmpty(channelProductDataList)) {
            throw new BusinessException("提示：上传数据为空, 请检查数据");
        }
        return channelProductDataList;
    }

    /**
     * 获取疾病分类列表
     *
     * @param request 请求参数
     */
    @Override
    public PageInfo<List<SelectPageInfoResponse>> selectPageInfo(SelectPageInfoRequest request) {
        if (Objects.isNull(request.getPageNum()) || Objects.isNull(request.getPageSize())) {
            throw new BusinessException("分页参数必填");
        }
        PageHelper.startPage(request.getPageNum(), request.getPageSize());
        List<SelectPageInfoResponse> pageInfoList = oasSicknessTagMapper.selectPageInfo(request);
        PageInfo pageInfo = new PageInfo();
        if (CollUtil.isNotEmpty(pageInfoList)) {
            pageInfo = new PageInfo(pageInfoList);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    /**
     * 导出疾病分类
     *
     * @param request
     * @return
     */
    @Override
    public SXSSFWorkbook export(SelectPageInfoRequest request) {
        List<SelectPageInfoResponse> data = oasSicknessTagMapper.selectPageInfo(request);
        if (CollUtil.isEmpty(data)) {
            throw new BusinessException("提示：导出数据为空");
        }
        // 获取数据
        SXSSFWorkbook workbook = new SXSSFWorkbook(data.size() + 100);
        org.apache.poi.ss.usermodel.Sheet sheet = workbook.createSheet("疾病分类列表");
        //设置标题
        String[] keys = CommonConstant.GAIA_OAS_SICKNESS_TAG;
        // 设置表头
        Row titleRow = sheet.createRow(0);
        int i = 0;
        for (String key : keys) {
            Cell cell = titleRow.createCell(i);
            cell.setCellValue(key);
            i++;
        }
        for (int m = 1; m <= data.size(); m++) {
            Row dataRow = sheet.createRow(m);
            SelectPageInfoResponse response = data.get(m - 1);
            // 列数
            int increase = 0;
            Cell cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getLargeSicknessCoding()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getLargeSicknessName()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getMiddleSicknessCoding()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getMiddleSicknessName()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSmallSicknessCoding()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getSmallSicknessName()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getMaintenanceDate()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getMaintenanceUser()));
            cell = dataRow.createCell(increase++);
            cell.setCellValue(ExcelUtils.tranColumn(response.getStatusStr()));
        }
        //自适应列宽
        ExcelUtils.setSizeColumnLength(sheet, keys.length);
        return workbook;
    }

    /**
     * 数据回显
     *
     * @param
     * @return
     */
    @Override
    public SelectDataEchoResponse selectDataEcho(SelectDataEchoRequest request) {
        if (StrUtil.isBlank(request.getLargeSicknessCoding()) && StrUtil.isBlank(request.getMiddleSicknessCoding()) && StrUtil.isBlank(request.getSmallSicknessCoding())) {
            throw new BusinessException("疾病大类编码,疾病中类编码,成分明细编码必填");
        }
        SelectDataEchoResponse response = oasSicknessTagMapper.selectDataEcho(request);
        return response;
    }

    /**
     * 启用/禁用
     *
     * @param id
     */
    @Override
    public void updateStatus(String id, String status) {
        OasSicknessTag oasSicknessTag = oasSicknessTagMapper.selectByPrimaryKey(Convert.toLong(id));
        if (Objects.isNull(oasSicknessTag)) {
            throw new BusinessException("疾病分类不存在");
        }
        //判断新增的大类编码是否存在
        Example example = new Example(OasSicknessTag.class);
        example.createCriteria().andEqualTo("id", id);
        OasSicknessTag tag = new OasSicknessTag();
        tag.setStatus(status);
        oasSicknessTagMapper.updateByExampleSelective(tag, example);
    }

    /**
     * 修改疾病分类编码或者名称
     *
     * @param request
     */
    @Override
    public void updateOasSickness(UpdateOasSicknessRequest request) {
        if (Objects.isNull(request.getType())) {
            throw new BusinessException("疾病类型必填");
        }
        if (StrUtil.isBlank(request.getOriginalSicknessCoding())) {
            throw new BusinessException("原疾病编码必填");
        }
        if (request.getType().equals(OasSicknesType.OasSicknesLargeType.getType())) {

            Example example = new Example(OasSicknessTag.class);
            example.createCriteria().andEqualTo("sioSicknessTag", request.getOriginalSicknessCoding());
            OasSicknessTag tag = new OasSicknessTag();
            if (StrUtil.isNotBlank(request.getNewSicknessCoding())) {
                if (!(Objects.equals(request.getNewSicknessCoding(), request.getOriginalSicknessCoding()))) {
                    //判断新增的大类编码是否存在
                    Example select = new Example(OasSicknessTag.class);
                    select.createCriteria().andEqualTo("sioSicknessTag", request.getNewSicknessCoding());
                    List<OasSicknessTag> sicknessTags = oasSicknessTagMapper.selectByExample(select);
                    if (CollUtil.isNotEmpty(sicknessTags)) {
                        throw new BusinessException("疾病大类编码已存在");
                    }
                }
                tag.setSioSicknessTag(request.getNewSicknessCoding());
            } else {
                tag.setSioSicknessTag(request.getOriginalSicknessCoding());
            }
            tag.setSioSicknessTagName(request.getSicknessName());
            tag.setSioXgrq(LocalDate.now());
            tag.setSioXgsj(LocalDateTime.now());
            tag.setSioXgr(request.getUserId());
            tag.setSioXgrName(request.getUserName());
            oasSicknessTagMapper.updateByExampleSelective(tag, example);

            if (StrUtil.isNotBlank(request.getNewSicknessCoding())) {
                //修改大类中类关联表
                Example codingTag = new Example(OasSicknessCodingTag.class);
                codingTag.createCriteria().andEqualTo("sioSicknessTag", request.getOriginalSicknessCoding());
                OasSicknessCodingTag sicknessCodingTag = new OasSicknessCodingTag();
                sicknessCodingTag.setSioSicknessTag(request.getNewSicknessCoding());
                oasSicknessCodingTagMapper.updateByExampleSelective(sicknessCodingTag, codingTag);
            }
        } else if (request.getType().equals(OasSicknesType.OasSicknesMiddleType.getType())) {

            Example example = new Example(OasSicknessCoding.class);
            example.createCriteria().andEqualTo("sioSicknessCoding", request.getOriginalSicknessCoding());
            OasSicknessCoding tag = new OasSicknessCoding();
            if (StrUtil.isNotBlank(request.getNewSicknessCoding())) {
                if (!(Objects.equals(request.getNewSicknessCoding(), request.getOriginalSicknessCoding()))) {
                    Example sicknessCoding = new Example(OasSicknessCoding.class);
                    sicknessCoding.createCriteria().andEqualTo("sioSicknessCoding", request.getNewSicknessCoding());
                    List<OasSicknessCoding> sicknessCodings = oasSicknessCodingMapper.selectByExample(sicknessCoding);
                    if (CollUtil.isNotEmpty(sicknessCodings)) {
                        throw new BusinessException("疾病中类编码已存在");
                    }
                }
                tag.setSioSicknessCoding(request.getNewSicknessCoding());
            } else {
                tag.setSioSicknessCoding(request.getOriginalSicknessCoding());
            }
            tag.setSioSicknessCodingName(request.getSicknessName());
            tag.setSioXgrq(LocalDate.now());
            tag.setSioXgsj(LocalDateTime.now());
            oasSicknessCodingMapper.updateByExampleSelective(tag, example);
            if (StrUtil.isNotBlank(request.getNewSicknessCoding())) {
                //修改大类中类关联表
                Example codingTag = new Example(OasSicknessCodingTag.class);
                codingTag.createCriteria().andEqualTo("sioSicknessCoding", request.getOriginalSicknessCoding());
                OasSicknessCodingTag sicknessCodingTag = new OasSicknessCodingTag();
                sicknessCodingTag.setSioSicknessCoding(request.getNewSicknessCoding());
                oasSicknessCodingTagMapper.updateByExampleSelective(sicknessCodingTag, codingTag);
            }
            if (StrUtil.isNotBlank(request.getNewSicknessCoding())) {
                //修改中类-明细
                Example element = new Example(OasSicknessCodingElement.class);
                element.createCriteria().andEqualTo("sioSicknessCoding", request.getOriginalSicknessCoding());
                OasSicknessCodingElement codingElement = new OasSicknessCodingElement();
                codingElement.setSioSicknessCoding(request.getNewSicknessCoding());
                oasSicknessCodingElementMapper.updateByExampleSelective(codingElement, element);
            }
        } else if (request.getType().equals(OasSicknesType.OasSicknesSmalleType.getType())) {
            if(StrUtil.isBlank(request.getLargeSicknessCoding())){
                throw new BusinessException("大类疾病编码不存在");
            }
            if(StrUtil.isBlank(request.getMiddleSicknessCoding())){
                throw new BusinessException("中类疾病编码不存在");
            }

            //判断新增的大类编码是否存在
            Example example = new Example(OasSicknessElement.class);
            example.createCriteria().andEqualTo("sioElementId", request.getOriginalSicknessCoding());
            OasSicknessElement tag = new OasSicknessElement();
            if (StrUtil.isNotBlank(request.getNewSicknessCoding())) {
                //判断新增的大类编码是否存在
                Example examples = new Example(OasSicknessCodingElement.class);
                examples.createCriteria().andEqualTo("sioSicknessCoding", request.getMiddleSicknessCoding());
                List<OasSicknessCodingElement> elements = oasSicknessCodingElementMapper.selectByExample(examples);
                if (CollUtil.isNotEmpty(elements)) {
                    //List<String> collect = oasSicknessList.stream().map(s -> s.getSicknessCoding()).collect(Collectors.toList());
                    List<OasSicknessCodingElement> collect1 = elements.stream().filter(s->Objects.equals(s.getSioElementId(),request.getNewSicknessCoding())).collect(Collectors.toList());
                    if(CollUtil.isNotEmpty(collect1)){
                        throw new BusinessException("同一中类下成分疾病编码已存在" + StringUtils.join(collect1.stream().map(s -> s.getSioElementId()).collect(Collectors.toList()), ","));
                    }
                }

                tag.setSioElementId(request.getNewSicknessCoding());
            } else {
                tag.setSioElementId(request.getOriginalSicknessCoding());
            }
            tag.setSioElementName(request.getSicknessName());
            tag.setSioXgrq(LocalDate.now());
            tag.setSioXgsj(LocalDateTime.now());

            oasSicknessElementMapper.updateByExampleSelective(tag, example);

            if (StrUtil.isNotBlank(request.getNewSicknessCoding())) {
                //修改中类-明细
                Example element = new Example(OasSicknessCodingElement.class);
                element.createCriteria().andEqualTo("sioElementId", request.getOriginalSicknessCoding());
                OasSicknessCodingElement codingElement = new OasSicknessCodingElement();
                codingElement.setSioElementId(request.getNewSicknessCoding());
                oasSicknessCodingElementMapper.updateByExampleSelective(codingElement, element);
            }
        }
    }
}
