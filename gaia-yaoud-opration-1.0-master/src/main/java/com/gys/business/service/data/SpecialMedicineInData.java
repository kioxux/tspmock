package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
/**
 * @author xiaoyuan on 2020/12/17
 */
@Data
public class SpecialMedicineInData implements Serializable {
    private static final long serialVersionUID = 1552533481876488352L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "销售单")
    private String billNo;

    @ApiModelProperty(value = "销售日期")
    private String saleDate;

    @ApiModelProperty(value = "商品")
    private String proId = "";

    @ApiModelProperty(value = "登记人员")
    private String registrationOfficer;

    @ApiModelProperty(value = "顾客姓名")
    private String customerName;

    @ApiModelProperty(value = "出生日期")
    private String dateOfBirth;

    @ApiModelProperty(value = "手机")
    private String cellPhone;

    @ApiModelProperty(value = "身份证号")
    private String idCard;
    @NotNull(message = "门店不可为空")
    @ApiModelProperty(value = "门店")
    private String[] brIdList;
}
