//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data.member;

import lombok.Data;

@Data
public class MemberInData {
    //  加盟商
    private String client;
    //  会员卡号
    private String memberCardId;
}
