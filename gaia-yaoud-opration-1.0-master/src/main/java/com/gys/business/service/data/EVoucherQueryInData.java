package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 电子券查询
 *
 * @author xiaoyuan on 2020/9/30
 */
@Data
public class EVoucherQueryInData implements Serializable {
    private static final long serialVersionUID = 1710570385047277922L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "主题描述")
    private String gsetsName;

    @NotBlank(message = "起始日期不可为空")
    @ApiModelProperty(value = "起始日期")
    private String gsebsBeginDate;

    @NotBlank(message = "结束日期不可为空")
    @ApiModelProperty(value = "结束日期")
    private String gsebsEndDate;

    @NotBlank(message = "业务类型不可为空")
    @ApiModelProperty(value = "业务类型 0为送券 / 1为用券")
    private String gsebsType;

    private String userId;

}
