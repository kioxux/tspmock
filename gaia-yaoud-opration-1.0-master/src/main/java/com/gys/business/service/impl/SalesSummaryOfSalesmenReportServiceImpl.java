package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaSdSaleDMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.mapper.RelatedSaleEjectMapper;
import com.gys.business.service.SalesSummaryOfSalesmenReportService;
import com.gys.business.service.data.*;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.PageInfo;
import com.gys.common.data.SupplierInfoDTO;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CommonUtil;
import com.gys.util.CosUtils;
import com.gys.util.ExportStatusUtil;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class SalesSummaryOfSalesmenReportServiceImpl implements SalesSummaryOfSalesmenReportService {

    @Autowired
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Autowired
    private GaiaUserDataMapper gaiaUserDataMapper;
    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Autowired
    private RelatedSaleEjectMapper relatedSaleEjectMapper;

    @Resource
    CosUtils cosUtils;


    @Override
    public PageInfo<GetSalesSummaryOfSalesmenReportOutData> queryReport(GetSalesSummaryOfSalesmenReportInData inData) {
        //先查询权限   flag  0：不开启  1：开启
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        if (ObjectUtil.isEmpty(flag)) {
            inData.setFlag("0");
        } else {
            inData.setFlag(flag);
        }

        if (StringUtils.isEmpty(inData.getQueryStartDate())) {
            throw new BusinessException(" 起始日期不能为空！");
        }
        if (StringUtils.isEmpty(inData.getQueryEndDate())) {
            throw new BusinessException(" 结束日期不能为空！");
        }

        List<GetSalesSummaryOfSalesmenReportOutData> outData = this.gaiaSdSaleDMapper.querySalesSummaryOfSalesmenReport(inData);
        int index = 0;
        for (GetSalesSummaryOfSalesmenReportOutData i : outData) {
            index++;
            i.setIndex(index);
        }

        PageInfo pageInfo;
        if (CollUtil.isNotEmpty(outData)) {
            GetSalesSummaryOfSalesmenReportOutData countAll = this.gaiaSdSaleDMapper.queryCountSalesSummar(inData);
            pageInfo = new PageInfo(outData);
            pageInfo.setListNum(countAll);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public PageInfo<GetSalesSummaryOfSalesmenReportOutDataUnion> unionQueryReport(GetSalesSummaryOfSalesmenReportInData inData) {
        //先查询权限   flag  0：不开启  1：开启
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        if (ObjectUtil.isEmpty(flag)) {
            inData.setFlag("0");
        } else {
            inData.setFlag(flag);
        }


        if (StringUtils.isEmpty(inData.getQueryStartDate())) {
            throw new BusinessException(" 起始日期不能为空！");
        }
        if (StringUtils.isEmpty(inData.getQueryEndDate())) {
            throw new BusinessException(" 结束日期不能为空！");
        }
        List<GetSalesSummaryOfSalesmenReportOutDataUnion> resList = new ArrayList<>();
        List<GetSalesSummaryOfSalesmenReportOutData> outData = this.gaiaSdSaleDMapper.querySalesSummaryOfSalesmenReport(inData);
        Map<String, GetSalesSummaryOfSalesmenReportOutData> outDataMap = new HashMap<>();
        int index = 0;
        for (GetSalesSummaryOfSalesmenReportOutData i : outData) {
            index++;
            i.setIndex(index);
            outDataMap.put(i.getSellerCode(), i);
        }
        if (inData.getPageNum() == null) {
            inData.setPageNum(1);
        }
        if (inData.getPageSize() == null) {
            inData.setPageSize(10000);
        }

        RelatedSaleEjectInData relatedSaleEjectInData = new RelatedSaleEjectInData();
        relatedSaleEjectInData.setClient(inData.getClientId());
        relatedSaleEjectInData.setSiteCode(inData.getBrId());
        relatedSaleEjectInData.setQueryUserId(inData.getQueryUserId());
        relatedSaleEjectInData.setQueryEndDate(inData.getQueryEndDate());
        relatedSaleEjectInData.setQueryStartDate(inData.getQueryStartDate());
        List<RelatedSaleEjectRes> relatedSaleEjectResList = relatedSaleEjectMapper.selectTclGroupByUserId(relatedSaleEjectInData);//查询弹出率
        Map<String, RelatedSaleEjectRes> relatedSaleEjectResMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(relatedSaleEjectResList)) {
            for (RelatedSaleEjectRes relatedSaleEjectRes : relatedSaleEjectResList) {
                if (StrUtil.isNotBlank(relatedSaleEjectRes.getCreateBy())) {
                    relatedSaleEjectResMap.put(relatedSaleEjectRes.getCreateBy(), relatedSaleEjectRes);
                }
            }
        }

        List<GetSalesSummaryOfSalesmenReportOutDataUnion> unionOrderList = this.gaiaSdSaleDMapper.selectUnionSaleOrderByUserId(relatedSaleEjectInData);
        Map<String, GetSalesSummaryOfSalesmenReportOutDataUnion> unionOrderMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(unionOrderList)) {
            for (GetSalesSummaryOfSalesmenReportOutDataUnion res : unionOrderList) {
                if (StrUtil.isNotBlank(res.getSellerCode())) {
                    unionOrderMap.put(res.getSellerCode(), res);
                }
            }
        }

        List<GetSalesSummaryOfSalesmenReportOutDataUnion> countAndAmtList = this.gaiaSdSaleDMapper.selectUnionSaleCountAndAmtByUserId(relatedSaleEjectInData);
        Map<String, GetSalesSummaryOfSalesmenReportOutDataUnion> countAndAmtMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(countAndAmtList)) {
            for (GetSalesSummaryOfSalesmenReportOutDataUnion res : countAndAmtList) {
                if (StrUtil.isNotBlank(res.getSellerCode())) {
                    countAndAmtMap.put(res.getSellerCode(), res);
                }
            }
        }

        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GetSalesSummaryOfSalesmenReportOutDataUnion> outDataUnion = this.gaiaSdSaleDMapper.querySalesSummaryOfSalesmenReportUnion(inData);
//        Map<String,GetSalesSummaryOfSalesmenReportOutDataUnion> outDataUnionUnMap = new HashMap<>();

        for (GetSalesSummaryOfSalesmenReportOutDataUnion i : outDataUnion) {
            if (outDataMap.get(i.getSellerCode()) != null) {
                RelatedSaleEjectRes relatedSaleEjectRes = relatedSaleEjectResMap.get(i.getSellerCode());
                BeanUtils.copyProperties(outDataMap.get(i.getSellerCode()), i);
                i.setInputCount(relatedSaleEjectResMap.get(i.getSellerCode()) == null ? 0 : relatedSaleEjectResMap.get(i.getSellerCode()).getInputCount());
                i.setUnitCount(relatedSaleEjectResMap.get(i.getSellerCode()) == null ? 0 : relatedSaleEjectResMap.get(i.getSellerCode()).getUnitCount());
                if (relatedSaleEjectRes != null && relatedSaleEjectRes.getInputCount() != null && relatedSaleEjectRes.getUnitCount() != null && relatedSaleEjectRes.getUnitCount() != 0 && relatedSaleEjectRes.getInputCount() != 0) {
                    BigDecimal res = new BigDecimal(relatedSaleEjectRes.getUnitCount()).divide(new BigDecimal(relatedSaleEjectRes.getInputCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
                    i.setEjectRate(res.setScale(2, BigDecimal.ROUND_HALF_UP));
                    i.setEjectRateStr(res.toPlainString() + "%");
                } else {
                    i.setEjectRate(new BigDecimal(0));
                    i.setEjectRateStr(0 + "%");
                }
                GetSalesSummaryOfSalesmenReportOutDataUnion countAndAmt = countAndAmtMap.get(i.getSellerCode());
                GetSalesSummaryOfSalesmenReportOutDataUnion unionOrder = unionOrderMap.get(i.getSellerCode());
                i.setBusinessCount(countAndAmtMap.get(i.getSellerCode()) == null ? 0 : countAndAmtMap.get(i.getSellerCode()).getBusinessCount());
                i.setUnionBusinessCount(countAndAmtMap.get(i.getSellerCode()) == null ? 0 : countAndAmtMap.get(i.getSellerCode()).getUnionBusinessCount());
                if (unionOrder != null && unionOrder.getUnionBusinessCount() != null && countAndAmt != null && countAndAmt.getUnionBusinessCount() != null && countAndAmt.getUnionBusinessCount() != 0) {
                    i.setGlcjcs(new BigDecimal(countAndAmt.getUnionBusinessCount()));
                    i.setYgltcxpcs(new BigDecimal(unionOrder.getUnionBusinessCount()));
                    BigDecimal res = new BigDecimal(countAndAmt.getUnionBusinessCount()).divide(new BigDecimal(unionOrder.getUnionBusinessCount()), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
                    i.setBusinessCountRate(res);
                    i.setBusinessCountRateStr(res.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "%");
                } else {
                    i.setGlcjcs(new BigDecimal(0));
                    i.setYgltcxpcs(new BigDecimal(0));
                    i.setBusinessCountRate(new BigDecimal(0));
                    i.setBusinessCountRateStr(0 + "%");
                }
                i.setUnionBusinessAmt(countAndAmtMap.get(i.getSellerCode()) == null ? BigDecimal.ZERO : countAndAmtMap.get(i.getSellerCode()).getUnionBusinessAmt().setScale(2, RoundingMode.HALF_UP));
                if (countAndAmt != null && countAndAmt.getBusinessAmt() != null && countAndAmt.getUnionBusinessAmt() != null && countAndAmt.getBusinessAmt().intValue() != 0 && countAndAmt.getUnionBusinessAmt().intValue() != 0) {
                    BigDecimal businessAmt = new BigDecimal(i.getSsAmount());
                    i.setUnionBusinessAmtRate(businessAmt.compareTo(BigDecimal.ZERO) == 0 ? new BigDecimal("0") : i.getUnionBusinessAmt().divide(businessAmt, 4, RoundingMode.HALF_UP).multiply(new BigDecimal("100")).setScale(2, RoundingMode.HALF_UP));
//
//                    BigDecimal res = countAndAmt.getUnionBusinessAmt().divide(businessAmt, 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
//                    i.setUnionBusinessAmtRate(res);
                    i.setUnionBusinessAmtRateStr(i.getUnionBusinessAmtRateStr());
                } else {
                    i.setUnionBusinessAmtRate(new BigDecimal(0));
                    i.setUnionBusinessAmtRateStr(0 + "%");
                }

                i.setBusinessAmt(countAndAmtMap.get(i.getSellerCode()) == null ? BigDecimal.ZERO : countAndAmtMap.get(i.getSellerCode()).getBusinessAmt());

                resList.add(i);

            }
        }
        PageInfo pageInfoSearch = new PageInfo(outDataUnion);

        PageInfo pageInfo;
        if (CollUtil.isNotEmpty(resList)) {
            GetSalesSummaryOfSalesmenReportOutDataUnion totalInfo = getTotalInfo(resList);
//            GetSalesSummaryOfSalesmenReportOutData countAll = this.gaiaSdSaleDMapper.queryCountSalesSummar(inData);
            pageInfo = new PageInfo(resList);
            pageInfo.setTotal(pageInfoSearch.getTotal());
            pageInfo.setPages(pageInfoSearch.getPages());
            pageInfo.setListNum(totalInfo);
//            Map<String,Object> extendsInfo = new HashMap<>();
//            extendsInfo.put("totalInfo",totalInfo);
//            pageInfo.setExtendsInfo(extendsInfo);
//            pageInfo.setListNum(countAll);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }


    private GetSalesSummaryOfSalesmenReportOutDataUnion getTotalInfo(List<GetSalesSummaryOfSalesmenReportOutDataUnion> resList) {
        GetSalesSummaryOfSalesmenReportOutDataUnion res = new GetSalesSummaryOfSalesmenReportOutDataUnion();
        Integer salesDaysTotal = 0;
        BigDecimal ssAmount = BigDecimal.ZERO;
        Integer tradedTime = 0;
        Integer xspx = 0;
        BigDecimal visitUnitPrice = BigDecimal.ZERO;
        BigDecimal kpc = BigDecimal.ZERO;
        BigDecimal pdj = BigDecimal.ZERO;
        Integer inputCount = 0;
        Integer unitCount = 0;
        BigDecimal ejectRate = BigDecimal.ZERO;
        ;//弹出率
        Integer unionBusinessCount = 0;
        BigDecimal businessCountRate = BigDecimal.ZERO;
        BigDecimal unionBusinessAmt = BigDecimal.ZERO;
        BigDecimal unionBusinessAmtRate = BigDecimal.ZERO;
        BigDecimal totalAmt = BigDecimal.ZERO;
        BigDecimal glcjcs = BigDecimal.ZERO;//关联成交次数
        BigDecimal ygltcxpcs = BigDecimal.ZERO;//有关联弹出的小票张数
        Integer totalQyt = 0;
        if (CollectionUtil.isNotEmpty(resList)) {
            for (GetSalesSummaryOfSalesmenReportOutDataUnion union : resList) {
                totalAmt = totalAmt.add(union.getTotalAmt());
                totalQyt = totalQyt + union.getTotalQyt();
                xspx = xspx + union.getXspx();
                if (StrUtil.isNotBlank(union.getSalesDays())) {
                    salesDaysTotal = salesDaysTotal + Integer.parseInt(union.getSalesDays());
                }
                if (StrUtil.isNotBlank(union.getSsAmount())) {
                    ssAmount = ssAmount.add(new BigDecimal(union.getSsAmount()));
                }
                if (union.getJycs() != null) {
                    tradedTime = tradedTime + Integer.parseInt(union.getTradedTime());
                }
                if (StrUtil.isNotBlank(union.getVisitUnitPrice())) {
                    visitUnitPrice = visitUnitPrice.add(new BigDecimal(union.getVisitUnitPrice()));
                }
                if (StrUtil.isNotBlank(union.getKpc())) {
                    kpc = kpc.add(new BigDecimal(union.getKpc()));
                }
                if (StrUtil.isNotBlank(union.getPdj())) {
                    pdj = pdj.add(new BigDecimal(union.getPdj()));
                }
                if (union.getInputCount() != null) {
                    inputCount = inputCount + union.getInputCount();
                }
                if (union.getUnitCount() != null) {
                    unitCount = unitCount + union.getUnitCount();
                }
                if (union.getEjectRate() != null) {
                    ejectRate = ejectRate.add((union.getEjectRate()));
                }
                if (union.getUnionBusinessCount() != null) {
                    unionBusinessCount = unionBusinessCount + union.getUnionBusinessCount();
                }
                if (union.getBusinessCountRate() != null) {
                    businessCountRate = businessCountRate.add((union.getBusinessCountRate()));
                }
                if (union.getUnionBusinessAmt() != null) {
                    unionBusinessAmt = unionBusinessAmt.add(union.getUnionBusinessAmt());
                }
                if (union.getUnionBusinessAmtRate() != null) {
                    unionBusinessAmtRate = unionBusinessAmtRate.add(union.getUnionBusinessAmtRate());
                }
                if (union.getGlcjcs() != null) {
                    glcjcs = glcjcs.add((union.getGlcjcs()));
                }
                if (union.getYgltcxpcs() != null) {
                    ygltcxpcs = ygltcxpcs.add((union.getYgltcxpcs()));
                }


            }
            res.setSalesDays(salesDaysTotal + "");
            res.setSsAmount(ssAmount.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "");
            res.setTradedTime(tradedTime + "");
            res.setVisitUnitPrice(new BigDecimal(res.getSsAmount()).divide(new BigDecimal(tradedTime), 4, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP).toPlainString() + "");
            res.setKpc(new BigDecimal(xspx).divide(new BigDecimal(res.getTradedTime()), 4, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP).toPlainString() + "");
            res.setPdj(totalAmt.divide(new BigDecimal(totalQyt), 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "");
            res.setInputCount(inputCount);
            res.setUnitCount(unitCount);
            if (inputCount != null && unitCount != null && unitCount != 0 && inputCount != 0) {
                BigDecimal resEjectRate = new BigDecimal(unitCount).divide(new BigDecimal(inputCount), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
                res.setEjectRate(resEjectRate.setScale(2, BigDecimal.ROUND_HALF_UP));
                res.setEjectRateStr(resEjectRate.toPlainString() + "%");
            } else {
                res.setEjectRate(new BigDecimal(0));
                res.setEjectRateStr(0 + "%");
            }
//            res.setEjectRate(ejectRate);
//            res.setEjectRateStr(res.getEjectRateStr());
            res.setUnionBusinessCount(unionBusinessCount);
            if (glcjcs.compareTo(BigDecimal.ZERO) != 0 && ygltcxpcs.compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal resUnionBusinessCountRate = glcjcs.divide(ygltcxpcs, 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
                res.setBusinessCountRate(resUnionBusinessCountRate);
                res.setBusinessCountRateStr(resUnionBusinessCountRate.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "%");
            } else {
                res.setBusinessCountRate(new BigDecimal(0));
                res.setBusinessCountRateStr(0 + "%");
            }
//            res.setBusinessCountRate(businessCountRate);
//            res.setBusinessCountRateStr(res.getBusinessCountRateStr());
            res.setUnionBusinessAmt(unionBusinessAmt);

            if (ssAmount.compareTo(BigDecimal.ZERO) != 0 && unionBusinessAmt.compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal resUnionBusinessAmtRate = unionBusinessAmt.divide(ssAmount, 4, RoundingMode.HALF_UP).multiply(new BigDecimal("100")).setScale(2, RoundingMode.HALF_UP);
                res.setUnionBusinessAmtRate(resUnionBusinessAmtRate);
                res.setUnionBusinessAmtRateStr(res.getUnionBusinessAmtRateStr());
            } else {
                res.setUnionBusinessAmtRate(new BigDecimal(0));
                res.setUnionBusinessAmtRateStr(0 + "%");
            }
//            res.setUnionBusinessAmtRate(unionBusinessAmtRate);
//            res.setUnionBusinessAmtRateStr(res.getUnionBusinessAmtRateStr());
        }

        return res;
    }

    @Override
    public Result unionQueryExport(GetSalesSummaryOfSalesmenReportInData inData) {
        PageInfo<GetSalesSummaryOfSalesmenReportOutDataUnion> pageInfo = this.unionQueryReport(inData);
        List<GetSalesSummaryOfSalesmenReportOutDataUnion> resList = pageInfo.getList();
        String fileName = "联合用药-员工";
        if (resList.size() > 0) {
            CsvFileInfo csvInfo = null;
            // 导出
            GetSalesSummaryOfSalesmenReportOutDataUnion totalInfo = getTotalInfo(resList);
            totalInfo.setSellerName("合计");
            totalInfo.setSellerCode("");
            resList.add(totalInfo);
            csvInfo = CsvClient.getCsvByteWithSupperClass(resList, fileName, Collections.singletonList((short) 1));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;
        } else {
            throw new BusinessException("提示：没有查询到数据,请修改查询条件!");
        }
    }

    @Override
    public List<GetEmpOutData> getUserListByClient(String clientId) {
        List<GetEmpOutData> outData = this.gaiaUserDataMapper.getListByClientId(clientId);
        return outData;
    }

    @Override
    public Map<String, Object> querySalesSummaryOfSalesmenList(GetSalesSummaryOfSalesmenReportInData inData) {
        if (ObjectUtil.isEmpty(inData.getQueryStartDate())) {
            throw new BusinessException("起始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getQueryEndDate())) {
            throw new BusinessException("结束日期不能为空！");
        }
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> totalCensus = new HashMap<>();
        BigDecimal grossProfitAmt = BigDecimal.ZERO;
        String grossProfitRate = "0.00%";
        BigDecimal discountAmt = BigDecimal.ZERO;
        String discountRate = "";
        BigDecimal costAmt = BigDecimal.ZERO;
        BigDecimal ssAmount = BigDecimal.ZERO;
        BigDecimal receivableAmt = BigDecimal.ZERO;
        BigDecimal dailyPayAmt = BigDecimal.ZERO;
        BigDecimal dailyPayCount = BigDecimal.ZERO;
        BigDecimal visitUnitPrice = BigDecimal.ZERO;
        BigDecimal memeberSaleAmt = BigDecimal.ZERO;
        String memeberSaleRate = "0.00%";
        BigDecimal selledDays = BigDecimal.ZERO;
        BigDecimal tradedTime = BigDecimal.ZERO;
        BigDecimal proAvgCount = BigDecimal.ZERO;
        BigDecimal billAvgPrice = BigDecimal.ZERO;
        BigDecimal allCostAmt = BigDecimal.ZERO;
        BigDecimal mproCount = BigDecimal.ZERO;
        BigDecimal ntradedTime = BigDecimal.ZERO;
        List<Map<String, Object>> itemList = this.gaiaSdSaleDMapper.querySalesSummaryOfSalesmenList(inData);
        if (itemList.size() > 0 && itemList != null) {
            result.put("itemCensus", itemList);
            for (Map<String, Object> item : itemList) {
                grossProfitAmt = grossProfitAmt.add(new BigDecimal(item.get("grossProfitAmt").toString()));
                discountAmt = discountAmt.add(new BigDecimal(item.get("discountAmt").toString()));
                costAmt = costAmt.add(new BigDecimal(item.get("costAmt").toString()));
                ssAmount = ssAmount.add(new BigDecimal(item.get("ssAmount").toString()));
                receivableAmt = receivableAmt.add(new BigDecimal(item.get("receivableAmt").toString()));
                dailyPayAmt = dailyPayAmt.add(new BigDecimal(item.get("dailyPayAmt").toString()));
                dailyPayCount = dailyPayCount.add(new BigDecimal(item.get("dailyPayCount").toString()));
                visitUnitPrice = visitUnitPrice.add(new BigDecimal(item.get("visitUnitPrice").toString()));
                memeberSaleAmt = memeberSaleAmt.add(new BigDecimal(item.get("memeberSaleAmt").toString()));
                selledDays = selledDays.add(new BigDecimal(item.get("selledDays").toString()));
                tradedTime = tradedTime.add(new BigDecimal(item.get("tradedTime").toString()));
                if (item.containsKey("proAvgCount")) {
                    proAvgCount = proAvgCount.add(new BigDecimal(item.get("proAvgCount").toString()));
                } else {
                    proAvgCount = proAvgCount.add(new BigDecimal("0"));
                }
//                if (item.containsKey("billAvgPrice")) {
//                    billAvgPrice = billAvgPrice.add(new BigDecimal(item.get("billAvgPrice").toString()));
//                } else {
//                    billAvgPrice = billAvgPrice.add(new BigDecimal("0"));
//                }
                if (ObjectUtil.isNotEmpty(item.containsKey("allCostAmt"))) {
                    allCostAmt = allCostAmt.add(new BigDecimal(item.get("allCostAmt").toString()));
                } else {
                    allCostAmt = allCostAmt.add(new BigDecimal("0"));
                }
                if (ObjectUtil.isNotEmpty(item.containsKey("mproCount"))) {
                    mproCount = mproCount.add(new BigDecimal(item.get("mproCount").toString()));
                } else {
                    mproCount = mproCount.add(new BigDecimal("0"));
                }
                if (ObjectUtil.isNotEmpty(item.containsKey("ntradedTime"))) {
                    ntradedTime = ntradedTime.add(new BigDecimal(item.get("ntradedTime").toString()));
                } else {
                    ntradedTime = ntradedTime.add(new BigDecimal("0"));
                }
                String discountRateNum = item.get("discountRate").toString().substring(0, item.get("discountRate").toString().indexOf("%"));
                item.put("discountRate", new BigDecimal(discountRateNum).setScale(2, BigDecimal.ROUND_HALF_UP) + "%");
                String grossProfitRateNum = item.get("grossProfitRate").toString().substring(0, item.get("grossProfitRate").toString().indexOf("%"));
                item.put("grossProfitRate", new BigDecimal(grossProfitRateNum).setScale(2, BigDecimal.ROUND_HALF_UP) + "%");
                String memeberSaleRateNum = item.get("memeberSaleRate").toString().substring(0, item.get("memeberSaleRate").toString().indexOf("%"));
                item.put("memeberSaleRate", new BigDecimal(memeberSaleRateNum).setScale(2, BigDecimal.ROUND_HALF_UP) + "%");
            }
            totalCensus.put("grossProfitAmt", grossProfitAmt.setScale(2, BigDecimal.ROUND_HALF_UP));
            totalCensus.put("discountAmt", discountAmt.setScale(2, BigDecimal.ROUND_HALF_UP));
            totalCensus.put("costAmt", costAmt.setScale(2, BigDecimal.ROUND_HALF_UP));
            totalCensus.put("ssAmount", ssAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
            totalCensus.put("memeberSaleAmt", memeberSaleAmt.setScale(2, BigDecimal.ROUND_HALF_UP));
            totalCensus.put("receivableAmt", receivableAmt.setScale(2, BigDecimal.ROUND_HALF_UP));
            totalCensus.put("selledDays", selledDays.setScale(2, BigDecimal.ROUND_HALF_UP));
            totalCensus.put("tradedTime", tradedTime.setScale(2, BigDecimal.ROUND_HALF_UP));
            totalCensus.put("allCostAmt", allCostAmt.setScale(2, BigDecimal.ROUND_HALF_UP));
            if (!(selledDays.compareTo(BigDecimal.ZERO) == 0)) {
                dailyPayCount = tradedTime.divide(selledDays, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
                dailyPayAmt = ssAmount.divide(selledDays, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            totalCensus.put("dailyPayAmt", dailyPayAmt);
            totalCensus.put("dailyPayCount", dailyPayCount);
//            totalCensus.put("proAvgCount", proAvgCount.divide(new BigDecimal(itemList.size()), 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP));
            if (!(tradedTime.compareTo(BigDecimal.ZERO) == 0)) {
                visitUnitPrice = ssAmount.divide(tradedTime, 4, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
                billAvgPrice = ssAmount.divide(tradedTime, 2, BigDecimal.ROUND_HALF_UP);
            }
            if (!(ntradedTime.compareTo(BigDecimal.ZERO) == 0)) {
                proAvgCount = mproCount.divide(ntradedTime, 2, BigDecimal.ROUND_HALF_UP);
            }
            totalCensus.put("proAvgCount", proAvgCount);
            totalCensus.put("visitUnitPrice", visitUnitPrice);
            totalCensus.put("billAvgPrice", billAvgPrice);
            if (!(ssAmount.compareTo(BigDecimal.ZERO) == 0)) {
                grossProfitRate = grossProfitAmt.divide(ssAmount, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
                memeberSaleRate = memeberSaleAmt.divide(ssAmount, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
            }
            discountRate = discountAmt.divide(receivableAmt, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
            totalCensus.put("grossProfitRate", grossProfitRate);
            totalCensus.put("memeberSaleRate", memeberSaleRate);
            totalCensus.put("discountRate", discountRate);
            result.put("totalCensus", totalCensus);
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> selectSaleList(Map<String, Object> inData) {
        return gaiaSdSaleDMapper.selectSellerList(inData);
    }

    @Override
    public PageInfo getSalespersonsSalesDetails(GetSalesSummaryOfSalesmenReportInData inData) {
        if (StringUtils.isEmpty(inData.getQueryStartDate())) {
            throw new BusinessException(" 起始日期不能为空！");
        }
        if (StringUtils.isEmpty(inData.getQueryEndDate())) {
            throw new BusinessException(" 结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getBillNo())) {
            inData.setBillNoArr(inData.getBillNo().split("\\s+ |\\s+"));
        }
        if (StringUtils.isNotEmpty(inData.getQueryProId())) {
            inData.setProArr(inData.getQueryProId().split("\\s+ |\\s+|,"));
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        inData.setFlag(flag);
        List<SalespersonsSalesDetailsOutData> outData = gaiaSdSaleDMapper.getSalespersonsSalesDetails(inData);
        PageInfo pageInfo;
        SalespersonsSalesDetailsOutTotal outTotal = new SalespersonsSalesDetailsOutTotal();

        if (ObjectUtil.isNotEmpty(outData)) {
            outTotal = gaiaSdSaleDMapper.getSalespersonsSalesDetailsTotal(inData);
            pageInfo = new PageInfo(outData, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    /**
     * 商品销售明细表导出
     *
     * @param inData
     * @param response
     * @return
     */
    @Override
    public void exportSalespersonsSalesDetails(GetSalesSummaryOfSalesmenReportInData inData, HttpServletResponse response) {
        ExportStatusUtil.checkExportAuthority(inData.getClientId(), inData.getBrId());
        if (StringUtils.isEmpty(inData.getQueryStartDate())) {
            throw new BusinessException(" 起始日期不能为空！");
        }
        if (StringUtils.isEmpty(inData.getQueryEndDate())) {
            throw new BusinessException(" 结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getBillNo())) {
            inData.setBillNoArr(inData.getBillNo().split("\\s+ |\\s+"));
        }
        if (StringUtils.isNotEmpty(inData.getQueryProId())) {
            inData.setProArr(inData.getQueryProId().split("\\s+ |\\s+|,"));
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        String flag = gaiaStoreDataMapper.selectStoPriceComparison(inData.getClientId(), null, CommonConstant.GSSP_ID_IMPRO_DETAIL);
        inData.setFlag(flag);

        CsvFileInfo fileInfo = new CsvFileInfo(new byte[0], 0, "商品销售明细表");

        List<SalespersonsSalesDetailsOutData> list = new ArrayList<>();
        AtomicInteger i = new AtomicInteger(1);
        gaiaSdSaleDMapper.getSalespersonsSalesDetails(inData,resultContext -> {
            SalespersonsSalesDetailsOutData outData = resultContext.getResultObject();
            outData.setIndex(i.get());
            i.getAndIncrement();
            if (StringUtils.isEmpty(outData.getDiscountRate())){
                outData.setDiscountRate("0");
            }
            outData.setDiscountRate(new BigDecimal(outData.getDiscountRate()).setScale(2, BigDecimal.ROUND_HALF_UP) + "%");
            list.add(outData);
            if (list.size() == CsvClient.BATCH_SIZE) {
                CsvClient.handle(list, fileInfo);
            }
        });

        CsvClient.endHandle(response,list,fileInfo,()->{
            if (ObjectUtil.isEmpty(fileInfo.getFileContent())){
                throw new BusinessException("导出数据不能为空！");
            }
            SalespersonsSalesDetailsOutTotal outTotal = gaiaSdSaleDMapper.getSalespersonsSalesDetailsTotal(inData);;
            byte[] bytes = ("\r\n合计,,,,,,,,,,,,,,,,,,,,,,,,0.00," + outTotal.getQty() + "," + outTotal.getAmountReceivable() + "," + outTotal.getAmt() + "," + outTotal.getDiscount() + "," + outTotal.getDiscountRate()).getBytes(StandardCharsets.UTF_8);
            byte[] all = ArrayUtils.addAll(fileInfo.getFileContent(), bytes);
            fileInfo.setFileContent(all);
            fileInfo.setFileSize(all.length);
        });
    }

    @Override
    public List<SupplierInfoDTO> getSupByClient(String client) {
        return gaiaSdSaleDMapper.getSupByClient(client);
    }

    @Override
    public PageInfo getSalespersonsSalesDetailsByClient(GetSalesSummaryOfSalesmenReportInData inData) {
        if (StringUtils.isEmpty(inData.getQueryStartDate())) {
            throw new BusinessException(" 起始日期不能为空！");
        }
        if (StringUtils.isEmpty(inData.getQueryEndDate())) {
            throw new BusinessException(" 结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getBillNo())) {
            inData.setBillNoArr(inData.getBillNo().split("\\s+ |\\s+"));
        }
        if (StringUtils.isNotEmpty(inData.getQueryProId())) {
            inData.setProArr(inData.getQueryProId().split("\\s+ |\\s+|,"));
        }
        if (ObjectUtil.isNotEmpty(inData.getClassArr())) {
            inData.setClassArrs(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        List<SalespersonsSalesDetailsOutData> outData = gaiaSdSaleDMapper.getSalespersonsSalesDetails(inData);
        PageInfo pageInfo;
        SalespersonsSalesDetailsOutTotal outTotal = new SalespersonsSalesDetailsOutTotal();

        if (ObjectUtil.isNotEmpty(outData)) {
            outTotal = gaiaSdSaleDMapper.getSalespersonsSalesDetailsTotal(inData);
            pageInfo = new PageInfo(outData, outTotal);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public List<SalespersonsSalesDetailsOutData> getSalespersonsSalesByPro(GetSalesSummaryOfSalesmenReportInData inData) {
        if (StringUtils.isEmpty(inData.getQueryStartDate())) {
            throw new BusinessException(" 起始日期不能为空！");
        }
        if (StringUtils.isEmpty(inData.getQueryEndDate())) {
            throw new BusinessException(" 结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getBillNo())) {
            inData.setBillNoArr(inData.getBillNo().split("\\s+ |\\s+"));
//            System.out.println(inData.getBillNoArr());
        }
        if (StringUtils.isNotEmpty(inData.getQueryProId())) {
            inData.setProArr(inData.getQueryProId().split("\\s+ |\\s+|,"));
//            System.out.println(inData.getBillNoArr());
        }
        return gaiaSdSaleDMapper.getSalespersonsSalesByPro(inData);
    }

    @Override
    public List<SalespersonsSalesDetailsOutData> getSalespersonsSalesByUser(GetSalesSummaryOfSalesmenReportInData inData) {
        if (StringUtils.isEmpty(inData.getQueryStartDate())) {
            throw new BusinessException(" 起始日期不能为空！");
        }
        if (StringUtils.isEmpty(inData.getQueryEndDate())) {
            throw new BusinessException(" 结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getBillNo())) {
            inData.setBillNoArr(inData.getBillNo().split("\\s+ |\\s+"));
            System.out.println(inData.getBillNoArr());
        }
        if (StringUtils.isNotEmpty(inData.getQueryProId())) {
            inData.setProArr(inData.getQueryProId().split("\\s+ |\\s+|,"));
//            System.out.println(inData.getBillNoArr());
        }
        return gaiaSdSaleDMapper.getSalespersonsSalesByUser(inData);
    }


    @Override
    public List<SalespersonsSalesDetailsOutData> getSalespersonsSalesByDoctor(GetSalesSummaryOfSalesmenReportInData inData) {
        if (StringUtils.isEmpty(inData.getQueryStartDate())) {
            throw new BusinessException(" 起始日期不能为空！");
        }
        if (StringUtils.isEmpty(inData.getQueryEndDate())) {
            throw new BusinessException(" 结束日期不能为空！");
        }
        if (StringUtils.isNotEmpty(inData.getBillNo())) {
            inData.setBillNoArr(inData.getBillNo().split("\\s+ |\\s+"));
            System.out.println(inData.getBillNoArr());
        }
        if (StringUtils.isNotEmpty(inData.getQueryProId())) {
            inData.setProArr(inData.getQueryProId().split("\\s+ |\\s+|,"));
//            System.out.println(inData.getBillNoArr());
        }
        return gaiaSdSaleDMapper.getSalespersonsSalesByDoctor(inData);
    }


}
