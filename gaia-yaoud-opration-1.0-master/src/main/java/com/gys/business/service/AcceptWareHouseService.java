package com.gys.business.service;

import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface AcceptWareHouseService {
    List<AcceptWareHouseOutData> pickOrderList(GetLoginOutData userInfo);

    Map<String,Object> selectList(AcceptWareHouseInData inData);

    List<AcceptWareHouseOutData>selectListById(AcceptWareHouseInData inData);

    AcceptVoucherOutData selectTotalDetail(AcceptWareHouseInData inData);

    List<AcceptWareHouseProDetailData> selectPickProductList(AcceptWareHouseInData inData);

    void approve(AcceptWareHouseInData inData,GetLoginOutData userInfo);

    Map<String,String> showWtpsLabel(GetLoginOutData userInfo);

    Map<String,Object>selectDsfTrustList(TrustSendInData inData);

    List<TrustSendOutData>selectWtpsOrderList(String clientId,String stoCode);

    List<TrustDetailOutData> selectWtpsDetail(AcceptWareHouseInData inData);

    void approveWtps(AcceptWareHouseInData inData,GetLoginOutData userInfo);

    void editWtpsDetail(EditWtpsDetailDto dto);

    boolean hasCold(String client);

    void manageGuoYaoOrder();

    ColdchainInfo getColdchainInfo(AcceptWareHouseInData inData, GetLoginOutData userInfo);

    void saveColdchainInfo(ColdchainInfo info);

    void insertMessageForReturning();

    JsonResult saveWtpsDetail(List<AcceptWareSaveWtpsRefuseInData> inDataList,String client,String stoCode,String emp);

    List<RejecteReasonVo> getgetRejecteReasons();
}
