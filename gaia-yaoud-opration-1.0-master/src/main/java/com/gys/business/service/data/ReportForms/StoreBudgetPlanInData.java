package com.gys.business.service.data.ReportForms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class StoreBudgetPlanInData {
    @ApiModelProperty(value = "开始时间")
    private String startDate;
    @ApiModelProperty(value = "结束时间")
    private String endDate;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "折扣率")
    private String stoRebate;
    @ApiModelProperty(value = "税率")
    private String stoTaxRate;
    private String client;
    @ApiModelProperty(value = "时间类型 1.年 2.月 3.日")
    private String dateType;
    private Integer pageNum;
    private Integer pageSize;
    private String orderBy;

    /**
     * 是否开启查询白名单标志   0：不开启  1：开启
     */
    private String flag;
}
