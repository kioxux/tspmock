//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "",description = "销售处方记录")
public class GaiaSdSaleRecipelRecordInData {
    private String clientId;
    private String gssrVoucherId;
    private String gssrType;
    private String gssrBrId;
    private String gssrBrName;
    private String gssrDate;
    private String gssrTime;
    private String gssrEmp;
    private String gssrUploadFlag;
    private String gssrSaleBrId;
    private String gssrSaleDate;
    private String gssrSaleBillNo;
    private String gssrCustName;
    private String gssrCustSex;
    private String gssrCustAge;
    private String gssrCustIdcard;
    private String gssrCustMobile;
    private String gssrPharmacistId;
    private String gssrPharmacistName;
    private String gssrCheckDate;
    private String gssrCheckTime;
    private String gssrCheckStatus;
    private String gssrProId;
    private String gssrBatchNo;
    private String gssrQty;
    private String gssrRecipelId;
    private String gssrRecipelHospital;
    private String gssrRecipelDepartment;
    private String gssrRecipelDoctor;
    private String gssrSymptom;
    private String gssrDiagnose;
    private String gssrRecipelPicId;
    private String gssrRecipelPicName;
    private String gssrRecipelPicAddress;
    @ApiModelProperty(value = "页数", required = true)
    private Integer pageNum;
    @ApiModelProperty(value = "条数", required = true)
    private Integer pageSize;
    @ApiModelProperty(value = "门店code", required = true)
    private String gsshBrId;
    @ApiModelProperty(value = "是否为关联推荐", required = true)
    private String recipelFlag;

    public GaiaSdSaleRecipelRecordInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGssrVoucherId() {
        return this.gssrVoucherId;
    }

    public String getGssrType() {
        return this.gssrType;
    }

    public String getGssrBrId() {
        return this.gssrBrId;
    }

    public String getGssrBrName() {
        return this.gssrBrName;
    }

    public String getGssrDate() {
        return this.gssrDate;
    }

    public String getGssrTime() {
        return this.gssrTime;
    }

    public String getGssrEmp() {
        return this.gssrEmp;
    }

    public String getGssrUploadFlag() {
        return this.gssrUploadFlag;
    }

    public String getGssrSaleBrId() {
        return this.gssrSaleBrId;
    }

    public String getGssrSaleDate() {
        return this.gssrSaleDate;
    }

    public String getGssrSaleBillNo() {
        return this.gssrSaleBillNo;
    }

    public String getGssrCustName() {
        return this.gssrCustName;
    }

    public String getGssrCustSex() {
        return this.gssrCustSex;
    }

    public String getGssrCustAge() {
        return this.gssrCustAge;
    }

    public String getGssrCustIdcard() {
        return this.gssrCustIdcard;
    }

    public String getGssrCustMobile() {
        return this.gssrCustMobile;
    }

    public String getGssrPharmacistId() {
        return this.gssrPharmacistId;
    }

    public String getGssrPharmacistName() {
        return this.gssrPharmacistName;
    }

    public String getGssrCheckDate() {
        return this.gssrCheckDate;
    }

    public String getGssrCheckTime() {
        return this.gssrCheckTime;
    }

    public String getGssrCheckStatus() {
        return this.gssrCheckStatus;
    }

    public String getGssrProId() {
        return this.gssrProId;
    }

    public String getGssrBatchNo() {
        return this.gssrBatchNo;
    }

    public String getGssrQty() {
        return this.gssrQty;
    }

    public String getGssrRecipelId() {
        return this.gssrRecipelId;
    }

    public String getGssrRecipelHospital() {
        return this.gssrRecipelHospital;
    }

    public String getGssrRecipelDepartment() {
        return this.gssrRecipelDepartment;
    }

    public String getGssrRecipelDoctor() {
        return this.gssrRecipelDoctor;
    }

    public String getGssrSymptom() {
        return this.gssrSymptom;
    }

    public String getGssrDiagnose() {
        return this.gssrDiagnose;
    }

    public String getGssrRecipelPicId() {
        return this.gssrRecipelPicId;
    }

    public String getGssrRecipelPicName() {
        return this.gssrRecipelPicName;
    }

    public String getGssrRecipelPicAddress() {
        return this.gssrRecipelPicAddress;
    }

    public Integer getPageNum() {
        return this.pageNum;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public String getGsshBrId() {
        return this.gsshBrId;
    }

    public String getRecipelFlag() {
        return this.recipelFlag;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGssrVoucherId(final String gssrVoucherId) {
        this.gssrVoucherId = gssrVoucherId;
    }

    public void setGssrType(final String gssrType) {
        this.gssrType = gssrType;
    }

    public void setGssrBrId(final String gssrBrId) {
        this.gssrBrId = gssrBrId;
    }

    public void setGssrBrName(final String gssrBrName) {
        this.gssrBrName = gssrBrName;
    }

    public void setGssrDate(final String gssrDate) {
        this.gssrDate = gssrDate;
    }

    public void setGssrTime(final String gssrTime) {
        this.gssrTime = gssrTime;
    }

    public void setGssrEmp(final String gssrEmp) {
        this.gssrEmp = gssrEmp;
    }

    public void setGssrUploadFlag(final String gssrUploadFlag) {
        this.gssrUploadFlag = gssrUploadFlag;
    }

    public void setGssrSaleBrId(final String gssrSaleBrId) {
        this.gssrSaleBrId = gssrSaleBrId;
    }

    public void setGssrSaleDate(final String gssrSaleDate) {
        this.gssrSaleDate = gssrSaleDate;
    }

    public void setGssrSaleBillNo(final String gssrSaleBillNo) {
        this.gssrSaleBillNo = gssrSaleBillNo;
    }

    public void setGssrCustName(final String gssrCustName) {
        this.gssrCustName = gssrCustName;
    }

    public void setGssrCustSex(final String gssrCustSex) {
        this.gssrCustSex = gssrCustSex;
    }

    public void setGssrCustAge(final String gssrCustAge) {
        this.gssrCustAge = gssrCustAge;
    }

    public void setGssrCustIdcard(final String gssrCustIdcard) {
        this.gssrCustIdcard = gssrCustIdcard;
    }

    public void setGssrCustMobile(final String gssrCustMobile) {
        this.gssrCustMobile = gssrCustMobile;
    }

    public void setGssrPharmacistId(final String gssrPharmacistId) {
        this.gssrPharmacistId = gssrPharmacistId;
    }

    public void setGssrPharmacistName(final String gssrPharmacistName) {
        this.gssrPharmacistName = gssrPharmacistName;
    }

    public void setGssrCheckDate(final String gssrCheckDate) {
        this.gssrCheckDate = gssrCheckDate;
    }

    public void setGssrCheckTime(final String gssrCheckTime) {
        this.gssrCheckTime = gssrCheckTime;
    }

    public void setGssrCheckStatus(final String gssrCheckStatus) {
        this.gssrCheckStatus = gssrCheckStatus;
    }

    public void setGssrProId(final String gssrProId) {
        this.gssrProId = gssrProId;
    }

    public void setGssrBatchNo(final String gssrBatchNo) {
        this.gssrBatchNo = gssrBatchNo;
    }

    public void setGssrQty(final String gssrQty) {
        this.gssrQty = gssrQty;
    }

    public void setGssrRecipelId(final String gssrRecipelId) {
        this.gssrRecipelId = gssrRecipelId;
    }

    public void setGssrRecipelHospital(final String gssrRecipelHospital) {
        this.gssrRecipelHospital = gssrRecipelHospital;
    }

    public void setGssrRecipelDepartment(final String gssrRecipelDepartment) {
        this.gssrRecipelDepartment = gssrRecipelDepartment;
    }

    public void setGssrRecipelDoctor(final String gssrRecipelDoctor) {
        this.gssrRecipelDoctor = gssrRecipelDoctor;
    }

    public void setGssrSymptom(final String gssrSymptom) {
        this.gssrSymptom = gssrSymptom;
    }

    public void setGssrDiagnose(final String gssrDiagnose) {
        this.gssrDiagnose = gssrDiagnose;
    }

    public void setGssrRecipelPicId(final String gssrRecipelPicId) {
        this.gssrRecipelPicId = gssrRecipelPicId;
    }

    public void setGssrRecipelPicName(final String gssrRecipelPicName) {
        this.gssrRecipelPicName = gssrRecipelPicName;
    }

    public void setGssrRecipelPicAddress(final String gssrRecipelPicAddress) {
        this.gssrRecipelPicAddress = gssrRecipelPicAddress;
    }

    public void setPageNum(final Integer pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setGsshBrId(final String gsshBrId) {
        this.gsshBrId = gsshBrId;
    }

    public void setRecipelFlag(final String recipelFlag) {
        this.recipelFlag = recipelFlag;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GaiaSdSaleRecipelRecordInData)) {
            return false;
        } else {
            GaiaSdSaleRecipelRecordInData other = (GaiaSdSaleRecipelRecordInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gssrVoucherId = this.getGssrVoucherId();
                Object other$gssrVoucherId = other.getGssrVoucherId();
                if (this$gssrVoucherId == null) {
                    if (other$gssrVoucherId != null) {
                        return false;
                    }
                } else if (!this$gssrVoucherId.equals(other$gssrVoucherId)) {
                    return false;
                }

                Object this$gssrType = this.getGssrType();
                Object other$gssrType = other.getGssrType();
                if (this$gssrType == null) {
                    if (other$gssrType != null) {
                        return false;
                    }
                } else if (!this$gssrType.equals(other$gssrType)) {
                    return false;
                }

                label446: {
                    Object this$gssrBrId = this.getGssrBrId();
                    Object other$gssrBrId = other.getGssrBrId();
                    if (this$gssrBrId == null) {
                        if (other$gssrBrId == null) {
                            break label446;
                        }
                    } else if (this$gssrBrId.equals(other$gssrBrId)) {
                        break label446;
                    }

                    return false;
                }

                label439: {
                    Object this$gssrBrName = this.getGssrBrName();
                    Object other$gssrBrName = other.getGssrBrName();
                    if (this$gssrBrName == null) {
                        if (other$gssrBrName == null) {
                            break label439;
                        }
                    } else if (this$gssrBrName.equals(other$gssrBrName)) {
                        break label439;
                    }

                    return false;
                }

                Object this$gssrDate = this.getGssrDate();
                Object other$gssrDate = other.getGssrDate();
                if (this$gssrDate == null) {
                    if (other$gssrDate != null) {
                        return false;
                    }
                } else if (!this$gssrDate.equals(other$gssrDate)) {
                    return false;
                }

                label425: {
                    Object this$gssrTime = this.getGssrTime();
                    Object other$gssrTime = other.getGssrTime();
                    if (this$gssrTime == null) {
                        if (other$gssrTime == null) {
                            break label425;
                        }
                    } else if (this$gssrTime.equals(other$gssrTime)) {
                        break label425;
                    }

                    return false;
                }

                label418: {
                    Object this$gssrEmp = this.getGssrEmp();
                    Object other$gssrEmp = other.getGssrEmp();
                    if (this$gssrEmp == null) {
                        if (other$gssrEmp == null) {
                            break label418;
                        }
                    } else if (this$gssrEmp.equals(other$gssrEmp)) {
                        break label418;
                    }

                    return false;
                }

                Object this$gssrUploadFlag = this.getGssrUploadFlag();
                Object other$gssrUploadFlag = other.getGssrUploadFlag();
                if (this$gssrUploadFlag == null) {
                    if (other$gssrUploadFlag != null) {
                        return false;
                    }
                } else if (!this$gssrUploadFlag.equals(other$gssrUploadFlag)) {
                    return false;
                }

                Object this$gssrSaleBrId = this.getGssrSaleBrId();
                Object other$gssrSaleBrId = other.getGssrSaleBrId();
                if (this$gssrSaleBrId == null) {
                    if (other$gssrSaleBrId != null) {
                        return false;
                    }
                } else if (!this$gssrSaleBrId.equals(other$gssrSaleBrId)) {
                    return false;
                }

                label397: {
                    Object this$gssrSaleDate = this.getGssrSaleDate();
                    Object other$gssrSaleDate = other.getGssrSaleDate();
                    if (this$gssrSaleDate == null) {
                        if (other$gssrSaleDate == null) {
                            break label397;
                        }
                    } else if (this$gssrSaleDate.equals(other$gssrSaleDate)) {
                        break label397;
                    }

                    return false;
                }

                label390: {
                    Object this$gssrSaleBillNo = this.getGssrSaleBillNo();
                    Object other$gssrSaleBillNo = other.getGssrSaleBillNo();
                    if (this$gssrSaleBillNo == null) {
                        if (other$gssrSaleBillNo == null) {
                            break label390;
                        }
                    } else if (this$gssrSaleBillNo.equals(other$gssrSaleBillNo)) {
                        break label390;
                    }

                    return false;
                }

                Object this$gssrCustName = this.getGssrCustName();
                Object other$gssrCustName = other.getGssrCustName();
                if (this$gssrCustName == null) {
                    if (other$gssrCustName != null) {
                        return false;
                    }
                } else if (!this$gssrCustName.equals(other$gssrCustName)) {
                    return false;
                }

                label376: {
                    Object this$gssrCustSex = this.getGssrCustSex();
                    Object other$gssrCustSex = other.getGssrCustSex();
                    if (this$gssrCustSex == null) {
                        if (other$gssrCustSex == null) {
                            break label376;
                        }
                    } else if (this$gssrCustSex.equals(other$gssrCustSex)) {
                        break label376;
                    }

                    return false;
                }

                Object this$gssrCustAge = this.getGssrCustAge();
                Object other$gssrCustAge = other.getGssrCustAge();
                if (this$gssrCustAge == null) {
                    if (other$gssrCustAge != null) {
                        return false;
                    }
                } else if (!this$gssrCustAge.equals(other$gssrCustAge)) {
                    return false;
                }

                label362: {
                    Object this$gssrCustIdcard = this.getGssrCustIdcard();
                    Object other$gssrCustIdcard = other.getGssrCustIdcard();
                    if (this$gssrCustIdcard == null) {
                        if (other$gssrCustIdcard == null) {
                            break label362;
                        }
                    } else if (this$gssrCustIdcard.equals(other$gssrCustIdcard)) {
                        break label362;
                    }

                    return false;
                }

                Object this$gssrCustMobile = this.getGssrCustMobile();
                Object other$gssrCustMobile = other.getGssrCustMobile();
                if (this$gssrCustMobile == null) {
                    if (other$gssrCustMobile != null) {
                        return false;
                    }
                } else if (!this$gssrCustMobile.equals(other$gssrCustMobile)) {
                    return false;
                }

                Object this$gssrPharmacistId = this.getGssrPharmacistId();
                Object other$gssrPharmacistId = other.getGssrPharmacistId();
                if (this$gssrPharmacistId == null) {
                    if (other$gssrPharmacistId != null) {
                        return false;
                    }
                } else if (!this$gssrPharmacistId.equals(other$gssrPharmacistId)) {
                    return false;
                }

                Object this$gssrPharmacistName = this.getGssrPharmacistName();
                Object other$gssrPharmacistName = other.getGssrPharmacistName();
                if (this$gssrPharmacistName == null) {
                    if (other$gssrPharmacistName != null) {
                        return false;
                    }
                } else if (!this$gssrPharmacistName.equals(other$gssrPharmacistName)) {
                    return false;
                }

                label334: {
                    Object this$gssrCheckDate = this.getGssrCheckDate();
                    Object other$gssrCheckDate = other.getGssrCheckDate();
                    if (this$gssrCheckDate == null) {
                        if (other$gssrCheckDate == null) {
                            break label334;
                        }
                    } else if (this$gssrCheckDate.equals(other$gssrCheckDate)) {
                        break label334;
                    }

                    return false;
                }

                label327: {
                    Object this$gssrCheckTime = this.getGssrCheckTime();
                    Object other$gssrCheckTime = other.getGssrCheckTime();
                    if (this$gssrCheckTime == null) {
                        if (other$gssrCheckTime == null) {
                            break label327;
                        }
                    } else if (this$gssrCheckTime.equals(other$gssrCheckTime)) {
                        break label327;
                    }

                    return false;
                }

                Object this$gssrCheckStatus = this.getGssrCheckStatus();
                Object other$gssrCheckStatus = other.getGssrCheckStatus();
                if (this$gssrCheckStatus == null) {
                    if (other$gssrCheckStatus != null) {
                        return false;
                    }
                } else if (!this$gssrCheckStatus.equals(other$gssrCheckStatus)) {
                    return false;
                }

                label313: {
                    Object this$gssrProId = this.getGssrProId();
                    Object other$gssrProId = other.getGssrProId();
                    if (this$gssrProId == null) {
                        if (other$gssrProId == null) {
                            break label313;
                        }
                    } else if (this$gssrProId.equals(other$gssrProId)) {
                        break label313;
                    }

                    return false;
                }

                label306: {
                    Object this$gssrBatchNo = this.getGssrBatchNo();
                    Object other$gssrBatchNo = other.getGssrBatchNo();
                    if (this$gssrBatchNo == null) {
                        if (other$gssrBatchNo == null) {
                            break label306;
                        }
                    } else if (this$gssrBatchNo.equals(other$gssrBatchNo)) {
                        break label306;
                    }

                    return false;
                }

                Object this$gssrQty = this.getGssrQty();
                Object other$gssrQty = other.getGssrQty();
                if (this$gssrQty == null) {
                    if (other$gssrQty != null) {
                        return false;
                    }
                } else if (!this$gssrQty.equals(other$gssrQty)) {
                    return false;
                }

                Object this$gssrRecipelId = this.getGssrRecipelId();
                Object other$gssrRecipelId = other.getGssrRecipelId();
                if (this$gssrRecipelId == null) {
                    if (other$gssrRecipelId != null) {
                        return false;
                    }
                } else if (!this$gssrRecipelId.equals(other$gssrRecipelId)) {
                    return false;
                }

                label285: {
                    Object this$gssrRecipelHospital = this.getGssrRecipelHospital();
                    Object other$gssrRecipelHospital = other.getGssrRecipelHospital();
                    if (this$gssrRecipelHospital == null) {
                        if (other$gssrRecipelHospital == null) {
                            break label285;
                        }
                    } else if (this$gssrRecipelHospital.equals(other$gssrRecipelHospital)) {
                        break label285;
                    }

                    return false;
                }

                label278: {
                    Object this$gssrRecipelDepartment = this.getGssrRecipelDepartment();
                    Object other$gssrRecipelDepartment = other.getGssrRecipelDepartment();
                    if (this$gssrRecipelDepartment == null) {
                        if (other$gssrRecipelDepartment == null) {
                            break label278;
                        }
                    } else if (this$gssrRecipelDepartment.equals(other$gssrRecipelDepartment)) {
                        break label278;
                    }

                    return false;
                }

                Object this$gssrRecipelDoctor = this.getGssrRecipelDoctor();
                Object other$gssrRecipelDoctor = other.getGssrRecipelDoctor();
                if (this$gssrRecipelDoctor == null) {
                    if (other$gssrRecipelDoctor != null) {
                        return false;
                    }
                } else if (!this$gssrRecipelDoctor.equals(other$gssrRecipelDoctor)) {
                    return false;
                }

                label264: {
                    Object this$gssrSymptom = this.getGssrSymptom();
                    Object other$gssrSymptom = other.getGssrSymptom();
                    if (this$gssrSymptom == null) {
                        if (other$gssrSymptom == null) {
                            break label264;
                        }
                    } else if (this$gssrSymptom.equals(other$gssrSymptom)) {
                        break label264;
                    }

                    return false;
                }

                Object this$gssrDiagnose = this.getGssrDiagnose();
                Object other$gssrDiagnose = other.getGssrDiagnose();
                if (this$gssrDiagnose == null) {
                    if (other$gssrDiagnose != null) {
                        return false;
                    }
                } else if (!this$gssrDiagnose.equals(other$gssrDiagnose)) {
                    return false;
                }

                label250: {
                    Object this$gssrRecipelPicId = this.getGssrRecipelPicId();
                    Object other$gssrRecipelPicId = other.getGssrRecipelPicId();
                    if (this$gssrRecipelPicId == null) {
                        if (other$gssrRecipelPicId == null) {
                            break label250;
                        }
                    } else if (this$gssrRecipelPicId.equals(other$gssrRecipelPicId)) {
                        break label250;
                    }

                    return false;
                }

                Object this$gssrRecipelPicName = this.getGssrRecipelPicName();
                Object other$gssrRecipelPicName = other.getGssrRecipelPicName();
                if (this$gssrRecipelPicName == null) {
                    if (other$gssrRecipelPicName != null) {
                        return false;
                    }
                } else if (!this$gssrRecipelPicName.equals(other$gssrRecipelPicName)) {
                    return false;
                }

                Object this$gssrRecipelPicAddress = this.getGssrRecipelPicAddress();
                Object other$gssrRecipelPicAddress = other.getGssrRecipelPicAddress();
                if (this$gssrRecipelPicAddress == null) {
                    if (other$gssrRecipelPicAddress != null) {
                        return false;
                    }
                } else if (!this$gssrRecipelPicAddress.equals(other$gssrRecipelPicAddress)) {
                    return false;
                }

                Object this$pageNum = this.getPageNum();
                Object other$pageNum = other.getPageNum();
                if (this$pageNum == null) {
                    if (other$pageNum != null) {
                        return false;
                    }
                } else if (!this$pageNum.equals(other$pageNum)) {
                    return false;
                }

                label222: {
                    Object this$pageSize = this.getPageSize();
                    Object other$pageSize = other.getPageSize();
                    if (this$pageSize == null) {
                        if (other$pageSize == null) {
                            break label222;
                        }
                    } else if (this$pageSize.equals(other$pageSize)) {
                        break label222;
                    }

                    return false;
                }

                label215: {
                    Object this$gsshBrId = this.getGsshBrId();
                    Object other$gsshBrId = other.getGsshBrId();
                    if (this$gsshBrId == null) {
                        if (other$gsshBrId == null) {
                            break label215;
                        }
                    } else if (this$gsshBrId.equals(other$gsshBrId)) {
                        break label215;
                    }

                    return false;
                }

                Object this$recipelFlag = this.getRecipelFlag();
                Object other$recipelFlag = other.getRecipelFlag();
                if (this$recipelFlag == null) {
                    if (other$recipelFlag != null) {
                        return false;
                    }
                } else if (!this$recipelFlag.equals(other$recipelFlag)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GaiaSdSaleRecipelRecordInData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gssrVoucherId = this.getGssrVoucherId();
        result = result * 59 + ($gssrVoucherId == null ? 43 : $gssrVoucherId.hashCode());
        Object $gssrType = this.getGssrType();
        result = result * 59 + ($gssrType == null ? 43 : $gssrType.hashCode());
        Object $gssrBrId = this.getGssrBrId();
        result = result * 59 + ($gssrBrId == null ? 43 : $gssrBrId.hashCode());
        Object $gssrBrName = this.getGssrBrName();
        result = result * 59 + ($gssrBrName == null ? 43 : $gssrBrName.hashCode());
        Object $gssrDate = this.getGssrDate();
        result = result * 59 + ($gssrDate == null ? 43 : $gssrDate.hashCode());
        Object $gssrTime = this.getGssrTime();
        result = result * 59 + ($gssrTime == null ? 43 : $gssrTime.hashCode());
        Object $gssrEmp = this.getGssrEmp();
        result = result * 59 + ($gssrEmp == null ? 43 : $gssrEmp.hashCode());
        Object $gssrUploadFlag = this.getGssrUploadFlag();
        result = result * 59 + ($gssrUploadFlag == null ? 43 : $gssrUploadFlag.hashCode());
        Object $gssrSaleBrId = this.getGssrSaleBrId();
        result = result * 59 + ($gssrSaleBrId == null ? 43 : $gssrSaleBrId.hashCode());
        Object $gssrSaleDate = this.getGssrSaleDate();
        result = result * 59 + ($gssrSaleDate == null ? 43 : $gssrSaleDate.hashCode());
        Object $gssrSaleBillNo = this.getGssrSaleBillNo();
        result = result * 59 + ($gssrSaleBillNo == null ? 43 : $gssrSaleBillNo.hashCode());
        Object $gssrCustName = this.getGssrCustName();
        result = result * 59 + ($gssrCustName == null ? 43 : $gssrCustName.hashCode());
        Object $gssrCustSex = this.getGssrCustSex();
        result = result * 59 + ($gssrCustSex == null ? 43 : $gssrCustSex.hashCode());
        Object $gssrCustAge = this.getGssrCustAge();
        result = result * 59 + ($gssrCustAge == null ? 43 : $gssrCustAge.hashCode());
        Object $gssrCustIdcard = this.getGssrCustIdcard();
        result = result * 59 + ($gssrCustIdcard == null ? 43 : $gssrCustIdcard.hashCode());
        Object $gssrCustMobile = this.getGssrCustMobile();
        result = result * 59 + ($gssrCustMobile == null ? 43 : $gssrCustMobile.hashCode());
        Object $gssrPharmacistId = this.getGssrPharmacistId();
        result = result * 59 + ($gssrPharmacistId == null ? 43 : $gssrPharmacistId.hashCode());
        Object $gssrPharmacistName = this.getGssrPharmacistName();
        result = result * 59 + ($gssrPharmacistName == null ? 43 : $gssrPharmacistName.hashCode());
        Object $gssrCheckDate = this.getGssrCheckDate();
        result = result * 59 + ($gssrCheckDate == null ? 43 : $gssrCheckDate.hashCode());
        Object $gssrCheckTime = this.getGssrCheckTime();
        result = result * 59 + ($gssrCheckTime == null ? 43 : $gssrCheckTime.hashCode());
        Object $gssrCheckStatus = this.getGssrCheckStatus();
        result = result * 59 + ($gssrCheckStatus == null ? 43 : $gssrCheckStatus.hashCode());
        Object $gssrProId = this.getGssrProId();
        result = result * 59 + ($gssrProId == null ? 43 : $gssrProId.hashCode());
        Object $gssrBatchNo = this.getGssrBatchNo();
        result = result * 59 + ($gssrBatchNo == null ? 43 : $gssrBatchNo.hashCode());
        Object $gssrQty = this.getGssrQty();
        result = result * 59 + ($gssrQty == null ? 43 : $gssrQty.hashCode());
        Object $gssrRecipelId = this.getGssrRecipelId();
        result = result * 59 + ($gssrRecipelId == null ? 43 : $gssrRecipelId.hashCode());
        Object $gssrRecipelHospital = this.getGssrRecipelHospital();
        result = result * 59 + ($gssrRecipelHospital == null ? 43 : $gssrRecipelHospital.hashCode());
        Object $gssrRecipelDepartment = this.getGssrRecipelDepartment();
        result = result * 59 + ($gssrRecipelDepartment == null ? 43 : $gssrRecipelDepartment.hashCode());
        Object $gssrRecipelDoctor = this.getGssrRecipelDoctor();
        result = result * 59 + ($gssrRecipelDoctor == null ? 43 : $gssrRecipelDoctor.hashCode());
        Object $gssrSymptom = this.getGssrSymptom();
        result = result * 59 + ($gssrSymptom == null ? 43 : $gssrSymptom.hashCode());
        Object $gssrDiagnose = this.getGssrDiagnose();
        result = result * 59 + ($gssrDiagnose == null ? 43 : $gssrDiagnose.hashCode());
        Object $gssrRecipelPicId = this.getGssrRecipelPicId();
        result = result * 59 + ($gssrRecipelPicId == null ? 43 : $gssrRecipelPicId.hashCode());
        Object $gssrRecipelPicName = this.getGssrRecipelPicName();
        result = result * 59 + ($gssrRecipelPicName == null ? 43 : $gssrRecipelPicName.hashCode());
        Object $gssrRecipelPicAddress = this.getGssrRecipelPicAddress();
        result = result * 59 + ($gssrRecipelPicAddress == null ? 43 : $gssrRecipelPicAddress.hashCode());
        Object $pageNum = this.getPageNum();
        result = result * 59 + ($pageNum == null ? 43 : $pageNum.hashCode());
        Object $pageSize = this.getPageSize();
        result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());
        Object $gsshBrId = this.getGsshBrId();
        result = result * 59 + ($gsshBrId == null ? 43 : $gsshBrId.hashCode());
        Object $recipelFlag = this.getRecipelFlag();
        result = result * 59 + ($recipelFlag == null ? 43 : $recipelFlag.hashCode());
        return result;
    }

    public String toString() {
        return "GaiaSdSaleRecipelRecordInData(clientId=" + this.getClientId() + ", gssrVoucherId=" + this.getGssrVoucherId() + ", gssrType=" + this.getGssrType() + ", gssrBrId=" + this.getGssrBrId() + ", gssrBrName=" + this.getGssrBrName() + ", gssrDate=" + this.getGssrDate() + ", gssrTime=" + this.getGssrTime() + ", gssrEmp=" + this.getGssrEmp() + ", gssrUploadFlag=" + this.getGssrUploadFlag() + ", gssrSaleBrId=" + this.getGssrSaleBrId() + ", gssrSaleDate=" + this.getGssrSaleDate() + ", gssrSaleBillNo=" + this.getGssrSaleBillNo() + ", gssrCustName=" + this.getGssrCustName() + ", gssrCustSex=" + this.getGssrCustSex() + ", gssrCustAge=" + this.getGssrCustAge() + ", gssrCustIdcard=" + this.getGssrCustIdcard() + ", gssrCustMobile=" + this.getGssrCustMobile() + ", gssrPharmacistId=" + this.getGssrPharmacistId() + ", gssrPharmacistName=" + this.getGssrPharmacistName() + ", gssrCheckDate=" + this.getGssrCheckDate() + ", gssrCheckTime=" + this.getGssrCheckTime() + ", gssrCheckStatus=" + this.getGssrCheckStatus() + ", gssrProId=" + this.getGssrProId() + ", gssrBatchNo=" + this.getGssrBatchNo() + ", gssrQty=" + this.getGssrQty() + ", gssrRecipelId=" + this.getGssrRecipelId() + ", gssrRecipelHospital=" + this.getGssrRecipelHospital() + ", gssrRecipelDepartment=" + this.getGssrRecipelDepartment() + ", gssrRecipelDoctor=" + this.getGssrRecipelDoctor() + ", gssrSymptom=" + this.getGssrSymptom() + ", gssrDiagnose=" + this.getGssrDiagnose() + ", gssrRecipelPicId=" + this.getGssrRecipelPicId() + ", gssrRecipelPicName=" + this.getGssrRecipelPicName() + ", gssrRecipelPicAddress=" + this.getGssrRecipelPicAddress() + ", pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ", gsshBrId=" + this.getGsshBrId() + ", recipelFlag=" + this.getRecipelFlag() + ")";
    }
}
