//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan
 */
@Data
public class GetPointExchangeOutData implements Serializable {
    private static final long serialVersionUID = 7786365981699796342L;
    private String gsiesExchangeIntegra;
    private String gsiesExchangeAmt;
}
