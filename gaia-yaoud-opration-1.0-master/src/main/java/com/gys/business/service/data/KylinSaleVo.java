package com.gys.business.service.data;

import lombok.Data;

/**
 * @Auther: tzh
 * @Date: 2022/2/11 13:47
 * @Description: kylinSaleVo
 * @Version 1.0.0
 */
@Data
public class KylinSaleVo {
    /**
     * 商品id
     */
    private String  gssdProId;
    /**
     * 7天分类
     */
    private String  type7;

    /**
     * 7天销量
     */
    private String  storeSaleQty7;
    /**
     * 30天分类
     */
    private String  type30;
    /**
     * 30天 销量
     */
    private String  storeSaleQty30;
    /**
     * 90天分类
     */
    private String  type90;
    /**
     * 90天销量
     */
    private String  storeSaleQty90;

}
