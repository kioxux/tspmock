package com.gys.business.service.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SmallSicknessCoding {

    @ApiModelProperty(value = "成分细类编码")
    private String smallSicknessCoding;

    @ApiModelProperty(value = "成分细类名称")
    private String smallSicknessName;
}
