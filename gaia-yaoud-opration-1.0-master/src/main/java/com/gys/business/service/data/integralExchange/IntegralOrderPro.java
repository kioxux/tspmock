package com.gys.business.service.data.integralExchange;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class IntegralOrderPro {
    private String proCode;//商品编码
    private BigDecimal proQty;//商品数量
    private BigDecimal proPrice;//商品价格
}
