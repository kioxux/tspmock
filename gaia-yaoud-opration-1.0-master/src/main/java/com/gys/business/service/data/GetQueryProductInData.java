
package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
@ApiModel(value = "收货相关请求实体")
public class GetQueryProductInData implements Serializable {
    private static final long serialVersionUID = -3184466020676161350L;
    @ApiModelProperty(value = "商品编码 通用名称 国际条形码1 国际条形码2 助记码 商品名（模糊匹配）")
    private String nameOrCode;

    @ApiModelProperty(value = "会员手机号 /或者 卡号")
    private String memberId;

    private String proId;
    private String num;
    private String clientId;
    private String proName;

    @ApiModelProperty(value = "门店号")
    private String brId;
    private String gssbBatchNo;
    private String memberType;
    //判断催销数量
    private String totalNum;
    private BigDecimal totalMoney;
    private String storageArea;
    private String isStock;
    private String isDateValidate;
    @ApiModelProperty(value = "按照中药排前 1 是")
    private String isChineseMedicine;
    //催销查询时 暂时没用
    private Map<String, GetNumAmtData> proNumAmtMap;
    //促销查询时 暂时没用
    private Map<String, GetNumAmtData> proIdNumMap;
    private List<GetSalesReceiptsTableOutData> allPros;
    private List<GetSalesReceiptsTableOutData> allProduct;
    private GetSalesReceiptsTableOutData selePro;

    /**
     * 所有商品编码
     */
    private List<String> proList;

    /**
     * 当前商品零售价
     */
    private String prcAmount;
    /**
     * 中药收银
     */
    private String proStorageArea;

    @ApiModelProperty(value = "API调用方(非必传) 1：报损报溢调用")
    private int accessType;
    @ApiModelProperty(value = "中药代煎模式")
    private String isThird;
    @ApiModelProperty(value = "中药代煎厂商编码")
    private String thirdPartySalesId;

    private String type;

    @ApiModelProperty(value = "国家贯标编码")
    private String medProductCode;

    @ApiModelProperty(value = "是否查询批号效期 0-否 1-是")
    private String isShowBatchNo;
}
