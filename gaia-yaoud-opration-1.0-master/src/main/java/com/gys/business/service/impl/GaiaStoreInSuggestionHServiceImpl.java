package com.gys.business.service.impl;
;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.gys.business.entity.dto.CallUpOrInStoreDetailDTO;
import com.gys.business.entity.vo.CallInStoreDetailVO;
import com.gys.business.entity.vo.CallUpAndInStoreTotalVO;
import com.gys.business.mapper.GaiaStoreInSuggestionHMapper;
import com.gys.business.service.GaiaStoreInSuggestionHService;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import com.gys.util.ExcelStyleUtil;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 门店调入建议-主表(GaiaStoreInSuggestionH)表服务实现类
 *
 * @author makejava
 * @since 2021-10-28 10:53:20
 */
@Service("gaiaStoreInSuggestionHService")
public class GaiaStoreInSuggestionHServiceImpl implements GaiaStoreInSuggestionHService {
    @Resource
    private GaiaStoreInSuggestionHMapper gaiaStoreInSuggestionHDao;

    @Override
    public PageInfo queryDetail(CallUpOrInStoreDetailDTO storeDetailDTO) {
        if (StrUtil.isAllBlank(storeDetailDTO.getFinishEndDate(), storeDetailDTO.getFinishStartDate()
                , storeDetailDTO.getSuggestedEndDate(), storeDetailDTO.getSuggestedStartDate())) {
            throw new BusinessException("日期不能为空");
        }
        List<CallInStoreDetailVO> storeDetailVOS = this.gaiaStoreInSuggestionHDao.queryDetail(storeDetailDTO);
        //统计计算
        if(CollectionUtil.isNotEmpty(storeDetailVOS)){
            //品项数
            BigDecimal numberOfStoreOUtTotal = BigDecimal.ZERO;
            //实际品项数
            BigDecimal actualAmountTotal = BigDecimal.ZERO;
            for (CallInStoreDetailVO storeDetailVO : storeDetailVOS) {
                numberOfStoreOUtTotal = numberOfStoreOUtTotal.add(storeDetailVO.getNumberOfStoreOUt());
                actualAmountTotal = actualAmountTotal.add(storeDetailVO.getActualAmount());
            }
            String fillRate = CommonUtil.divideFormat(actualAmountTotal, numberOfStoreOUtTotal, "00.00%");
            CallUpAndInStoreTotalVO totalVO = new CallUpAndInStoreTotalVO();
            totalVO.setNumberOfStoreOUt(numberOfStoreOUtTotal);
            totalVO.setActualAmount(actualAmountTotal);
            totalVO.setFillRate(fillRate);
            return  new PageInfo(storeDetailVOS,totalVO) ;
        }

        return new PageInfo(storeDetailVOS);
    }

    @Override
    public void queryDetailExport(HttpServletResponse response, CallUpOrInStoreDetailDTO storeDetailDTO) {
        if (StrUtil.isAllBlank(storeDetailDTO.getFinishEndDate(), storeDetailDTO.getFinishStartDate()
                , storeDetailDTO.getSuggestedEndDate(), storeDetailDTO.getSuggestedStartDate())) {
            throw new BusinessException("日期不能为空");
        }
        List<CallInStoreDetailVO> storeDetailVOS = this.gaiaStoreInSuggestionHDao.queryDetail(storeDetailDTO);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = "调入门店明细表";
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

        try {
            EasyExcel.write(response.getOutputStream(), CallInStoreDetailVO.class)
                    .registerWriteHandler(ExcelStyleUtil.getStyle())
                    .sheet(0).doWrite(storeDetailVOS);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Map<String, String>> queryBillCode(String startDate,String endDate) {
        return this.gaiaStoreInSuggestionHDao.queryBillCode(startDate,endDate);
    }

}
