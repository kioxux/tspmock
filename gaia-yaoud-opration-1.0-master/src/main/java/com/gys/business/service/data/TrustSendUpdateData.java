package com.gys.business.service.data;

import lombok.Data;

@Data
public class TrustSendUpdateData {
    private String voucherId;
    private String acceptDate;
    private String acceptTime;
    private String acceptEmp;
    private String clientId;
    private String psVoucherId;
    private String stoCode;
}
