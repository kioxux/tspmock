package com.gys.business.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ExamineService;
import com.gys.business.service.SyncService;
import com.gys.business.service.data.*;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.enums.MaterialTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.feign.PurchaseService;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class ExamineServiceImpl implements ExamineService {
    @Resource
    private GaiaSdExamineHMapper examineHMapper;

    @Resource
    private GaiaSdAcceptHMapper acceptHMapper;

    @Resource
    private GaiaSdAcceptDMapper acceptDMapper;

    @Resource
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;

    @Resource
    private GaiaSdExamineDMapper examineDMapper;

    @Resource
    private GaiaSdStockBatchMapper stockBatchMapper;

    @Resource
    private GaiaSdStockMapper stockMapper;

    @Resource
    private GaiaSdBatchChangeMapper batchChangeMapper;

    @Resource
    private GaiaPoItemMapper poItemMapper;

    @Resource
    private PurchaseService purchaseService;

    @Resource
    private SyncService syncService;

    @Value("${thirdServer}")
    private String thirdServer;

    @Resource
    private CommonMapper commonMapper;

    @Resource
    private GaiaBatchInfoMapper batchInfoMapper;

    @Resource
    private GaiaProductBusinessMapper productBusinessMapper;


    @Override
    public PageInfo<GetExamineOutData> selectList(GetExamineInData inData) {
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<GetExamineOutData> outData = this.examineHMapper.selectList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(outData)) {
            pageInfo = new PageInfo(outData);
        } else {
            pageInfo = new PageInfo();
        }

        return pageInfo;
    }

    @Override
    public List<GetAcceptOutData> acceptList(GetExamineInData inData) {
        System.out.print(inData);
        inData.setGsehStatus("1");
        Example example=new Example(GaiaSdSystemPara.class);
        example.createCriteria()
                .andEqualTo("clientId",inData.getClientId())
                .andEqualTo("gsspBrId",inData.getGsehBrId())
                .andEqualTo("gsspId","EMAMINE_NOCS");
        GaiaSdSystemPara gaiaSdSystemPara = this.gaiaSdSystemParaMapper.selectOneByExample(example);
        if (ObjectUtil.isNotNull(gaiaSdSystemPara) && StrUtil.isNotEmpty(gaiaSdSystemPara.getGsspPara())) {
            inData.setNotCS(gaiaSdSystemPara.getGsspPara());
        }
        List<GetAcceptOutData> outData = this.acceptHMapper.acceptList(inData);
        return outData;
    }

    @Override
    public List<GetAcceptOutData> acceptCSList(GetExamineInData inData) {
        inData.setGsehStatus("1");
        List<GetAcceptOutData> outData = this.acceptHMapper.acceptCSList(inData);
        return outData;
    }

    @Override
    public List<GetExamineDetailOutData> detailList(GetExamineInData inData) {
        List<GetExamineDetailOutData> outData = this.examineDMapper.detailList(inData);
        return outData;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String save(GetExamineInData inData) {
        Example example = new Example(GaiaSdAcceptH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsahVoucherId", inData.getGsehVoucherAcceptId());
        GaiaSdAcceptH acceptH = acceptHMapper.selectOneByExample(example);
        GaiaSdExamineH examineH = new GaiaSdExamineH();
        examineH.setClientId(inData.getClientId());
        String voucherId = this.examineHMapper.selectNextVoucherId(inData.getClientId());
        examineH.setGsehVoucherId(voucherId);
        examineH.setGsehBrId(inData.getStoreCode());
        examineH.setGsehStatus("1");
        examineH.setGsehType("收货单");
        examineH.setGsehFrom(acceptH.getGsahFrom());
        examineH.setGsehRemaks(acceptH.getGsahRemaks());
        examineH.setGsehTo(acceptH.getGsahTo());
        examineH.setGsehTotalAmt(acceptH.getGsahTotalAmt());
        examineH.setGsehTotalQty(acceptH.getGsahTotalQty());
        examineH.setGsehVoucherAcceptId(acceptH.getGsahVoucherId());
        examineH.setGsehDate(CommonUtil.getyyyyMMdd());
        this.examineHMapper.insert(examineH);
        Example exampleD = new Example(GaiaSdAcceptD.class);
        exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsadVoucherId", inData.getGsehVoucherAcceptId());
        List<GaiaSdAcceptD> acceptDList = this.acceptDMapper.selectByExample(exampleD);
        int i = 1;
        List<GetExamineDetailInData> examineDetailInDataList = new ArrayList<>();
        for (GaiaSdAcceptD acceptD : acceptDList) {
//            String batchNum = commonMapper.selectNextCode(inData.getClientId(),"Y",7);
            GetExamineDetailInData item = new GetExamineDetailInData();
            GaiaSdExamineD examineD = new GaiaSdExamineD();
            examineD.setClientId(inData.getClientId());
            examineD.setGsedVoucherId(voucherId);
            examineD.setGsedBrId(inData.getGsehBrId());
            examineD.setGsedProId(acceptD.getGsadProId());
            examineD.setGsedSerial(String.valueOf(i));
            examineD.setGsedDate(CommonUtil.getyyyyMMdd());
            examineD.setGsedQualifiedQty(acceptD.getGsadRecipientQty());
            examineD.setGsedRecipientQty(acceptD.getGsadInvoicesQty());
            examineD.setGsedUnqualifiedQty("0");
            examineD.setGsedBatch(acceptD.getGsadBatch());
            examineD.setGsedBatchNo(acceptD.getGsadBatchNo());
            examineD.setGsedValidDate(acceptD.getGsadValidDate());
//            examineD.setGsedMadeDate(acceptD.getGsadMadeDate());
            examineD.setGsedStockStatus("0");
            this.examineDMapper.insert(examineD);
            item.setGsedProId(acceptD.getGsadProId());
            item.setGsedBatchNo(acceptD.getGsadBatchNo());
            item.setGsedRecipientQty(acceptD.getGsadInvoicesQty());
            item.setGsedBatch(acceptD.getGsadBatch());
            item.setGsedQualifiedQty(acceptD.getGsadRecipientQty());
            item.setGsedUnqualifiedQty("0");
            item.setGsedResult("合格");
            item.setGsedEmp(inData.getGsedEmp());
            item.setGsedValidDate(acceptD.getGsadValidDate());
            item.setGsedStockStatus(examineD.getGsedStockStatus());
            item.setGsedSerial(examineD.getGsedSerial());
            examineDetailInDataList.add(item);
            i++;
        }
        //变更库存
        if (ObjectUtil.isNotEmpty(inData.getSendType())) {
            if (examineDetailInDataList.size() > 0 && examineDetailInDataList != null){
                inData.setExamineDetailInDataList(examineDetailInDataList);
            }
            if ("1".equals(inData.getSendType())){
                inData.setGsehVoucherId(voucherId);
                this.updateStatus(inData);
            }

            /**
             * 2021/2/26
             * 验收时调用接口增减库存
             */
//            log.info("库存新增/修改:{}", JSON.toJSONString(list));
            List<String> params = new ArrayList<>();
            params.add(voucherId);
            StockSyncData stockSyncData=new StockSyncData();
            stockSyncData.setClient(inData.getClientId());
            stockSyncData.setSite(inData.getGsehBrId());
            stockSyncData.setType("YS");
            stockSyncData.setParams(params);
            this.syncService.stockSync(stockSyncData);
            log.info("库存新增/修改:{}", JSON.toJSONString(stockSyncData));
        }
        return voucherId;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void approve(GetExamineInData inData) {
        if (!ObjectUtil.isEmpty(inData.getGsehVoucherId())) {
            Example example = new Example(GaiaSdExamineH.class);
            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsehVoucherId", inData.getGsehVoucherId());
            GaiaSdExamineH examineH = examineHMapper.selectOneByExample(example);
            examineH.setGsehStatus("1");
            examineH.setGsehDate(DateUtil.format(new Date(), "yyyyMMdd"));
            examineHMapper.updateByPrimaryKeySelective(examineH);
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
            GaiaSdExamineD examineD;
            int count = 1;
            for (GetExamineDetailInData detail : inData.getExamineDetailInDataList()) {
                Example exampleD = new Example(GaiaSdExamineD.class);
                exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsedVoucherId", inData.getGsehVoucherId()).andEqualTo("gsedSerial", String.valueOf(count));
                examineD = examineDMapper.selectOneByExample(exampleD);
                examineD.setGsedStockStatus("1");
                examineD.setGsedQualifiedQty(detail.getGsedQualifiedQty());
                examineD.setGsedUnqualifiedQty(detail.getGsedUnqualifiedQty());
                examineD.setGsedDate(DateUtil.format(new Date(), "yyyyMMdd"));
                examineD.setGsedEmp(detail.getGsedEmp());
                if (ObjectUtil.isEmpty(detail.getGsedResult())) {
                    examineD.setGsedResult("0");
                } else {
                    examineD.setGsedResult(detail.getGsedResult());
                }

                if (ObjectUtil.isEmpty(detail.getGsedUnqualifiedCause())) {
                    examineD.setGsedUnqualifiedCause("0");
                } else {
                    examineD.setGsedUnqualifiedCause(detail.getGsedUnqualifiedCause());
                }
                examineDMapper.updateByPrimaryKeySelective(examineD);

                example = new Example(GaiaSdStockBatch.class);
                example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssbBrId", inData.getGsehBrId()).andEqualTo("gssbProId", detail.getGsedProId()).andEqualTo("gssbBatchNo", detail.getGsedBatchNo()).andEqualTo("gssbBatch", detail.getGsedBatch());
                GaiaSdStockBatch stockBatchTo = this.stockBatchMapper.selectOneByExample(example);
                if (stockBatchTo == null) {
                    stockBatchTo = new GaiaSdStockBatch();
                    stockBatchTo.setClientId(inData.getClientId());
                    stockBatchTo.setGssbBrId(inData.getGsehBrId());
                    stockBatchTo.setGssbProId(detail.getGsedProId());
                    stockBatchTo.setGssbBatchNo(detail.getGsedBatchNo());
                    stockBatchTo.setGssbBatch(detail.getGsedBatch() == null ? "" : detail.getGsedBatch());
                    stockBatchTo.setGssbQty(detail.getGsedQualifiedQty());
                    stockBatchTo.setGssbVaildDate(detail.getGsedValidDate());
                    stockBatchTo.setGssbUpdateEmp(inData.getGsedEmp());
                    stockBatchTo.setGssbUpdateDate(formatDate.format(new Date()));
                    stockBatchMapper.insert(stockBatchTo);
                } else {
//                    int recCount = Integer.valueOf(new Double(Double.parseDouble(stockBatchTo.getGssbQty())).intValue())
//                            + Integer.valueOf(new Double(Double.parseDouble(detail.getGsedQualifiedQty())).intValue());
                    BigDecimal recCount = new BigDecimal(stockBatchTo.getGssbQty()).add(new BigDecimal(detail.getGsedQualifiedQty()));
                    stockBatchTo.setGssbQty(recCount.toString());
                    stockBatchTo.setGssbUpdateEmp(inData.getGsedEmp());
                    stockBatchTo.setGssbUpdateDate(formatDate.format(new Date()));
                    stockBatchMapper.updateByPrimaryKey(stockBatchTo);
                }

                Example exampleStock = new Example(GaiaSdStock.class);
                exampleStock.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", inData.getStoreCode()).andEqualTo("gssProId", detail.getGsedProId());
                GaiaSdStock stockFrom = this.stockMapper.selectOneByExample(exampleStock);
                if (stockFrom == null) {
                    stockFrom = new GaiaSdStock();
                    stockFrom.setClientId(inData.getClientId());
                    stockFrom.setGssBrId(inData.getGsehBrId());
                    stockFrom.setGssProId(detail.getGsedProId());
                    stockFrom.setGssQty(detail.getGsedQualifiedQty());
                    stockFrom.setGssUpdateDate(formatDate.format(new Date()));
                    stockFrom.setGssUpdateEmp(inData.getGsedEmp());
                    stockMapper.insert(stockFrom);
                } else {
                    BigDecimal recCount = new BigDecimal(stockFrom.getGssQty()).add(new BigDecimal(detail.getGsedQualifiedQty()));
                    stockFrom.setGssQty(recCount.toString());
                    stockFrom.setGssUpdateDate(formatDate.format(new Date()));
                    stockFrom.setGssUpdateEmp(inData.getGsedEmp());
                    stockMapper.updateByPrimaryKey(stockFrom);
                }

                //批次异动
                GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
                batchChange.setClient(inData.getClientId());
                batchChange.setGsbcBrId(inData.getGsehBrId());
                batchChange.setGsbcVoucherId(StrUtil.isEmpty(detail.getGsedVoucherId()) ? inData.getGsehVoucherId() : detail.getGsedVoucherId());
                batchChange.setGsbcDate(CommonUtil.getyyyyMMdd());
                batchChange.setGsbcSerial(StrUtil.toString(count));
                batchChange.setGsbcProId(detail.getGsedProId());
                batchChange.setGsbcBatchNo(detail.getGsedBatchNo());
                batchChange.setGsbcBatch(detail.getGsedBatch());
                batchChange.setGsbcQty(new BigDecimal(detail.getGsedQualifiedQty()));
                batchChangeMapper.insert(batchChange);
                count++;
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void approveEx(GetExamineInData inData, GetLoginOutData userInfo) {
        if(ObjectUtil.isEmpty(inData.getExamineDetailInDataList()) || inData.getExamineDetailInDataList().size() == 0){
            throw new BusinessException("当前界面验收明细不全，请刷新页面后重试！");
        }
        Example example = new Example(GaiaSdAcceptH.class);
        example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsahVoucherId", inData.getGsehVoucherAcceptId());
        GaiaSdAcceptH acceptH = acceptHMapper.selectOneByExample(example);
        if (ObjectUtil.isEmpty(acceptH.getGsahAcceptType())){
            acceptH.setGsahAcceptType(acceptH.getGsahType());
        }
        int count = examineHMapper.selectExCount(inData.getClientId(),inData.getGsehVoucherAcceptId());
        if (count > 0){
            throw new BusinessException("该收货单已验收，请刷新页面后重试！");
        }
        GaiaSdExamineH examineH = new GaiaSdExamineH();
        examineH.setClientId(inData.getClientId());
        String voucherId = this.examineHMapper.selectNextVoucherId(inData.getClientId());
        examineH.setGsehVoucherId(voucherId);
        examineH.setGsehBrId(inData.getStoreCode());
        examineH.setGsehStatus("1");
        examineH.setGsehType("收货单");
        examineH.setGsehFrom(acceptH.getGsahFrom());
        examineH.setGsehRemaks(acceptH.getGsahRemaks());
        examineH.setGsehTo(acceptH.getGsahTo());
        examineH.setGsehTotalAmt(acceptH.getGsahTotalAmt());
        examineH.setGsehTotalQty(acceptH.getGsahTotalQty());
        examineH.setGsehVoucherAcceptId(acceptH.getGsahVoucherId());//收货单号
        examineH.setGsehDate(CommonUtil.getyyyyMMdd());
        this.examineHMapper.insert(examineH);
        Example exampleD = new Example(GaiaSdAcceptD.class);
        exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsadVoucherId", inData.getGsehVoucherAcceptId());
        List<GaiaSdAcceptD> acceptDList = this.acceptDMapper.selectByExample(exampleD);
        List<MaterialDocRequestDto> list = new ArrayList();
        List<GetExamineDetailInData> detailInData = new ArrayList<>();

        for (GaiaSdAcceptD acceptD : acceptDList) {
            GaiaPoItem poItem = new GaiaPoItem();
            String batchNum = "";
            if ("2".equals(acceptH.getGsahAcceptType())) {
                Example exampleM = new Example(GaiaPoItem.class);
                exampleM.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("poId", acceptH.getGsahPoid()).andEqualTo("poLineNo", acceptD.getGsadSerial());
                poItem = this.poItemMapper.selectOneByExample(exampleM);
                GaiaAcceptID acceptID = this.examineDMapper.getPoSupplierId(inData.getClientId(), acceptH.getGsahVoucherId(), inData.getGsehBrId());
                //生成批次
                BatchInfoInData batchInfoInData = new BatchInfoInData();
                batchInfoInData.setClient(inData.getClientId());
                batchInfoInData.setProSite(userInfo.getDepId());
                batchInfoInData.setBatProCode(acceptD.getGsadProId());
                batchInfoInData.setBatPoId(acceptID.getGsahPoid());//采购单号
                batchInfoInData.setBatSupplierCode(acceptID.getPoSupplierId());//供应商编码
                batchInfoInData.setBatSupplierName(acceptID.getPoSupplierName());//供应商名称
                batchInfoInData.setBatPoLineno(acceptD.getGsadSerial());
                batchInfoInData.setBatExpiryDate(acceptD.getGsadValidDate());
                batchInfoInData.setBatPoPrice(poItem.getPoPrice().toString());//采购单价
                batchInfoInData.setBatReceiveQty(acceptD.getGsadRecipientQty());
                batchInfoInData.setBatBatchNo(acceptD.getGsadBatchNo());
                batchInfoInData.setBatCreateDate(CommonUtil.getyyyyMMdd());
                batchInfoInData.setBatCreateTime(CommonUtil.getHHmmss());
                batchInfoInData.setBatCreateUser(userInfo.getUserId());
                batchInfoInData.setBatSourceNo(voucherId);
                //转接第三方接口生成批次
//                log.info("saveBatchInfo传入参数:{}", JSON.toJSONString(batchInfoInData));
//                String wmsResult = wmsService.saveBatchInfo(batchInfoInData);
//                log.info("saveBatchInfo返回:{}", JSON.toJSONString(wmsResult));
//                BaseResponse batchInfoRes = JSONObject.parseObject(wmsResult, BaseResponse.class);
//                if (batchInfoRes.getCode() != HttpStatus.SC_OK) {
//                    throw new BusinessException("调用生成批次接口失败");
//                }
//                acceptD.setGsadBatch(batchInfoRes.getData().getBatBatch());
//                batchNum = commonMapper.selectNextCode(inData.getClientId(),"Y",7);
                batchNum = this.maxBatch(inData.getClientId(),"Y");
                log.info("获取批次号：{}" + batchNum);
                //直接生成批次
                Example exampleP = new Example(GaiaProductBusiness.class);
                exampleP.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("proSelfCode", acceptD.getGsadProId()).andEqualTo("proSite", userInfo.getDepId());
                GaiaProductBusiness productBusiness = productBusinessMapper.selectOneByExample(exampleP);
                batchInfoInData.setBatFactoryName(productBusiness.getProFactoryName());
                batchInfoInData.setBatProPlace(productBusiness.getProPlace());
                GaiaBatchInfo batchInfo = new GaiaBatchInfo();
                BeanUtils.copyProperties(batchInfoInData,batchInfo);
                batchInfo.setBatBatch(batchNum);
                batchInfo.setBatPoPrice(poItem.getPoPrice());
                batchInfo.setBatSiteCode(batchInfoInData.getProSite());
                batchInfo.setBatProductDate(acceptD.getGsadMadeDate());
                batchInfo.setBatReceiveQty(new BigDecimal(acceptD.getGsadRecipientQty()));
                batchInfoMapper.insert(batchInfo);
                //物料凭证
                MaterialDocRequestDto dto = new MaterialDocRequestDto();
                dto.setClient(inData.getClientId());
                dto.setGuid(UUID.randomUUID().toString());
                dto.setGuidNo(acceptD.getGsadSerial());
                dto.setMatPostDate(acceptD.getGsadDate());
                dto.setMatHeadRemark("");
                dto.setMatType(MaterialTypeEnum.CAI_GOU.getCode());
                dto.setMatProCode(acceptD.getGsadProId());
                dto.setMatPrice(new BigDecimal(CommonUtil.divideFormat(poItem.getPoDeliveredAmt(), poItem.getPoDeliveredQty(), "#.####")));
                dto.setMatStoCode("");
                dto.setMatSiteCode(inData.getGsehBrId());
                dto.setMatLocationCode("1000");
                dto.setMatLocationTo("");
                dto.setMatBatch(batchNum);
                dto.setMatQty(poItem.getPoDeliveredQty());
                dto.setMatUnit(poItem.getPoUnit());
                dto.setMatDebitCredit("S");
                dto.setMatPoId(acceptH.getGsahPoid());
                dto.setMatPoLineno(acceptD.getGsadSerial());
                dto.setMatDnId(acceptH.getGsahVoucherId());
                dto.setMatDnLineno(acceptD.getGsadSerial());
                dto.setMatLineRemark(acceptD.getGsadRowRemark());
                dto.setMatCreateBy(inData.getGsedEmp());
                dto.setMatPostDate(CommonUtil.getyyyyMMdd());
                dto.setMatCreateDate(CommonUtil.getyyyyMMdd());
                dto.setMatCreateTime(CommonUtil.getHHmmss());
                list.add(dto);
            }


            GaiaSdExamineD examineD = new GaiaSdExamineD();
            examineD.setClientId(inData.getClientId());
            examineD.setGsedVoucherId(voucherId);
            examineD.setGsedBrId(inData.getGsehBrId());
            examineD.setGsedProId(acceptD.getGsadProId());
            examineD.setGsedSerial(acceptD.getGsadSerial());
            examineD.setGsedDate(CommonUtil.getyyyyMMdd());
            examineD.setGsedQualifiedQty(acceptD.getGsadRecipientQty());
            examineD.setGsedRecipientQty(acceptD.getGsadInvoicesQty());
            examineD.setGsedUnqualifiedQty("0");
            if (StringUtil.isNotEmpty(batchNum)) {
                examineD.setGsedBatch(batchNum);
            }else {
                examineD.setGsedBatch(acceptD.getGsadBatch());
            }
            examineD.setGsedBatchNo(acceptD.getGsadBatchNo());
            examineD.setGsedValidDate(acceptD.getGsadValidDate());
            examineD.setGsedStockStatus("0");
            examineD.setGsedEmp(userInfo.getUserId());
            for (GetExamineDetailInData a : inData.getExamineDetailInDataList()){
                if (a.getGsedSerial().equals(examineD.getGsedSerial())
                        && a.getGsedProId().equals(examineD.getGsedProId())){
                    if (StringUtils.isEmpty(a.getGsedResult())){
                        examineD.setGsedResult("合格");
                    }else {
                        examineD.setGsedResult(a.getGsedResult());
                    }
                    if (StringUtils.isEmpty(a.getGsedUnqualifiedQty())){
                        examineD.setGsedUnqualifiedQty("0");
                    }else {
                        examineD.setGsedUnqualifiedQty(a.getGsedUnqualifiedQty());
                    }
                }
            }
            this.examineDMapper.insert(examineD);
            GetExamineDetailInData item = new GetExamineDetailInData();
            item.setGsedProId(acceptD.getGsadProId());
            item.setGsedBatchNo(acceptD.getGsadBatchNo());
            item.setGsedRecipientQty(acceptD.getGsadInvoicesQty());
            item.setGsedBatch(examineD.getGsedBatch());
            item.setGsedQualifiedQty(acceptD.getGsadRecipientQty());
            item.setGsedUnqualifiedQty(examineD.getGsedUnqualifiedQty());
            item.setGsedResult(examineD.getGsedResult());
            item.setGsedEmp(inData.getGsedEmp());
            item.setGsedValidDate(acceptD.getGsadValidDate());
            item.setGsedStockStatus(examineD.getGsedStockStatus());
            item.setGsedSerial(examineD.getGsedSerial());
            detailInData.add(item);
        }


        if ("2".equals(acceptH.getGsahType()) && ObjectUtil.isNotEmpty(list) && "1".equals(this.thirdServer)) {
            log.info("物料凭证请求参数:{}", JSON.toJSONString(list));
            purchaseService.insertMaterialDoc(list);
//            log.info("物料凭证返回:{}", JSON.toJSONString(result));
//            BaseResponse resultObj = JSONObject.parseObject(result, BaseResponse.class);
//            int code = resultObj.getCode();
//            if (code != 0) {
//                throw new BusinessException("物料凭证接口服务异常");
//            }
        }
        if (ObjectUtil.isNotEmpty(voucherId)) {
            inData.setGsehVoucherId(voucherId);
            if (ObjectUtil.isNotEmpty(detailInData) && detailInData.size() > 0){
                inData.setExamineDetailInDataList(detailInData);
            }
            this.updateStatus(inData);
        }

        /**
         * 2021/2/26
         * 验收时调用接口增减库存
         */
        log.info("库存新增/修改:{}", JSON.toJSONString(list));
        List<String> params = new ArrayList<>();
        params.add(voucherId);
        StockSyncData stockSyncData=new StockSyncData();
        stockSyncData.setClient(inData.getClientId());
        stockSyncData.setSite(inData.getGsehBrId());
        stockSyncData.setType("YS");
        stockSyncData.setParams(params);
        this.syncService.stockSync(stockSyncData);
        log.info("库存新增/修改:{}", JSON.toJSONString(stockSyncData));
    }

    @Override
    public List<GetExamineOutData> selectExamineList(GetExamineInData inData) {
        List<GetExamineOutData> outData = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(inData.getGsehVoucherId())) {
            outData = this.examineHMapper.selectList(inData);
        } else {
            outData = this.examineHMapper.selectAcceptList(inData);
        }
        return outData;
    }

    @Override
    public List<GetExamineDetailOutData> examineDetailList(GetExamineInData inData) {
        List<GetExamineDetailOutData> outData = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(inData.getGsehVoucherId())) {
            outData = this.examineDMapper.detailList(inData);
        } else {
            outData = this.examineDMapper.examineDetailList(inData);
        }
        return outData;
    }

    @Override
    public Map<String,Object> examineOrderList(ExamineOrderInData inData, GetLoginOutData userInfo) {

        if(ObjectUtil.isEmpty(inData.getBrIdList()) || inData.getBrIdList().length< 0) {
            throw new BusinessException("请输入门店");
        }

        Map<String,Object> map = new HashMap<>();
        ExamineOrderTotalOutData totalOutData = new ExamineOrderTotalOutData();
//        Map<String,String> stoInfo = productBasicMapper.selectStoreAttribute(inData.getClientId(),userInfo.getDepId());
        inData.setStoCode(userInfo.getDepId());
//        if (stoInfo.get("stoAttribute").equals("2")){
//            if (ObjectUtil.isNotEmpty(stoInfo.get("stoDcCode"))){
//                inData.setStoCode(stoInfo.get("stoDcCode"));
//            }
//        }
        BigDecimal a = BigDecimal.ZERO;
        BigDecimal b = BigDecimal.ZERO;
        List<ExamineOrderOutData> examineOrderOutDataList = examineDMapper.selectExamineOrderList(inData);
        if (examineOrderOutDataList.size() > 0 && examineOrderOutDataList != null) {
            for (ExamineOrderOutData outData : examineOrderOutDataList) {
                a = a.add(outData.getRecipientQty());
                b = b.add(outData.getQualifiedQty());
            }
            map.put("ExamineOrderOutDataList",examineOrderOutDataList);
            totalOutData.setQualifiedTotalQty(b.toString());
            totalOutData.setRecipientTotalQty(a.toString());
        }
        map.put("totalOutData",totalOutData);
        return map;
    }

    @Override
    public void rejectAcceptOreder(GetExamineInData inData) {
        acceptHMapper.rejectAcceptOrder(inData.getClientId(),inData.getGsehBrId(),inData.getGsehVoucherAcceptId(),"2");
    }

    public void updateStatus(GetExamineInData inData) {
        if (!ObjectUtil.isEmpty(inData.getGsehVoucherId())) {
//            Example example = new Example(GaiaSdExamineH.class);
//            example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsehVoucherId", inData.getGsehVoucherId());
//            GaiaSdExamineH examineH = examineHMapper.selectOneByExample(example);
//            examineH.setGsehStatus("1");
//            examineH.setGsehDate(DateUtil.format(new Date(), "yyyyMMdd"));
//            examineHMapper.updateByPrimaryKeySelective(examineH);
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
            GaiaSdExamineD examineD;
            int count = 1;
            for (GetExamineDetailInData detail : inData.getExamineDetailInDataList()) {
//                Example exampleD = new Example(GaiaSdExamineD.class);
//                exampleD.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gsedVoucherId", inData.getGsehVoucherId()).andEqualTo("gsedSerial",detail.getGsedSerial());
//                examineD = examineDMapper.selectOneByExample(exampleD);
//                examineD.setGsedStockStatus("1");
//                examineD.setGsedQualifiedQty(detail.getGsedQualifiedQty());
//                examineD.setGsedUnqualifiedQty(detail.getGsedUnqualifiedQty());
//                examineD.setGsedDate(DateUtil.format(new Date(), "yyyyMMdd"));
//                examineD.setGsedEmp(detail.getGsedEmp());
//                if (ObjectUtil.isEmpty(detail.getGsedResult())) {
//                    examineD.setGsedResult("0");
//                } else {
//                    examineD.setGsedResult(detail.getGsedResult());
//                }
//
//                if (ObjectUtil.isEmpty(detail.getGsedUnqualifiedCause())) {
//                    examineD.setGsedUnqualifiedCause("0");
//                } else {
//                    examineD.setGsedUnqualifiedCause(detail.getGsedUnqualifiedCause());
//                }
//                examineDMapper.updateByPrimaryKeySelective(examineD);

                Example example = new Example(GaiaSdStockBatch.class);
                example.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssbBrId", inData.getGsehBrId()).andEqualTo("gssbProId", detail.getGsedProId()).andEqualTo("gssbBatchNo", detail.getGsedBatchNo()).andEqualTo("gssbBatch", detail.getGsedBatch());
                GaiaSdStockBatch stockBatchTo = this.stockBatchMapper.selectOneByExample(example);
                if (stockBatchTo == null) {
                    stockBatchTo = new GaiaSdStockBatch();
                    stockBatchTo.setClientId(inData.getClientId());
                    stockBatchTo.setGssbBrId(inData.getGsehBrId());
                    stockBatchTo.setGssbProId(detail.getGsedProId());
                    stockBatchTo.setGssbBatchNo(detail.getGsedBatchNo());
                    stockBatchTo.setGssbBatch(detail.getGsedBatch() == null ? "" : detail.getGsedBatch());
                    stockBatchTo.setGssbQty(detail.getGsedQualifiedQty());
                    stockBatchTo.setGssbVaildDate(detail.getGsedValidDate());
                    stockBatchTo.setGssbUpdateEmp(inData.getGsedEmp());
                    stockBatchTo.setGssbUpdateDate(formatDate.format(new Date()));
                    stockBatchMapper.insert(stockBatchTo);
                } else {
//                    int recCount = Integer.valueOf(new Double(Double.parseDouble(stockBatchTo.getGssbQty())).intValue())
//                            + Integer.valueOf(new Double(Double.parseDouble(detail.getGsedQualifiedQty())).intValue());
                    BigDecimal recCount = new BigDecimal(stockBatchTo.getGssbQty()).add(new BigDecimal(detail.getGsedQualifiedQty()));
                    stockBatchTo.setGssbQty(recCount.toString());
                    stockBatchTo.setGssbUpdateEmp(inData.getGsedEmp());
                    stockBatchTo.setGssbUpdateDate(formatDate.format(new Date()));
                    stockBatchMapper.updateByPrimaryKey(stockBatchTo);
                }

                Example exampleStock = new Example(GaiaSdStock.class);
                exampleStock.createCriteria().andEqualTo("clientId", inData.getClientId()).andEqualTo("gssBrId", inData.getStoreCode()).andEqualTo("gssProId", detail.getGsedProId());
                GaiaSdStock stockFrom = this.stockMapper.selectOneByExample(exampleStock);
                if (stockFrom == null) {
                    stockFrom = new GaiaSdStock();
                    stockFrom.setClientId(inData.getClientId());
                    stockFrom.setGssBrId(inData.getGsehBrId());
                    stockFrom.setGssProId(detail.getGsedProId());
                    stockFrom.setGssQty(detail.getGsedQualifiedQty());
                    stockFrom.setGssUpdateDate(formatDate.format(new Date()));
                    stockFrom.setGssUpdateEmp(inData.getGsedEmp());
                    stockMapper.insert(stockFrom);
                } else {
                    BigDecimal recCount = new BigDecimal(stockFrom.getGssQty()).add(new BigDecimal(detail.getGsedQualifiedQty()));
                    stockFrom.setGssQty(recCount.toString());
                    stockFrom.setGssUpdateDate(formatDate.format(new Date()));
                    stockFrom.setGssUpdateEmp(inData.getGsedEmp());
                    stockMapper.updateByPrimaryKey(stockFrom);
                }

                //批次异动
                GaiaSdBatchChange batchChange = new GaiaSdBatchChange();
                batchChange.setClient(inData.getClientId());
                batchChange.setGsbcBrId(inData.getGsehBrId());
                batchChange.setGsbcVoucherId(StrUtil.isEmpty(detail.getGsedVoucherId()) ? inData.getGsehVoucherId() : detail.getGsedVoucherId());
                batchChange.setGsbcDate(CommonUtil.getyyyyMMdd());
                batchChange.setGsbcSerial(StrUtil.toString(count));
                batchChange.setGsbcProId(detail.getGsedProId());
                batchChange.setGsbcBatchNo(detail.getGsedBatchNo());
                batchChange.setGsbcBatch(detail.getGsedBatch());
                batchChange.setGsbcQty(new BigDecimal(detail.getGsedQualifiedQty()));
                batchChangeMapper.insert(batchChange);
                count++;
            }
        }
    }
    
    //批次生成方法
    public String maxBatch(String clientId,String type){
        String batch = commonMapper.selectNextCodeByBatch(clientId,type);//获取batchinfo表中是否存在Y的最大批次号
        if (StringUtils.isNotEmpty(batch)){//存在则变更code_val表信息
            commonMapper.addBatchToCode(clientId,batch,type);
        }
        return commonMapper.selectNextCode(clientId,type,7);
    }
}
