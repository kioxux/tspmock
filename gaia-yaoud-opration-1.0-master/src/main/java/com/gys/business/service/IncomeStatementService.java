package com.gys.business.service;

import com.gys.business.service.data.*;

import java.util.List;
import java.util.Map;

public interface IncomeStatementService {
    List<GetPhysicalCountHeadOutData> list(IncomeStatementInData inData);

    List<IncomeStatementOutData> getIncomeStatementList(IncomeStatementInData inData);

    List<IncomeStatementDetailOutData> getIncomeStatementDetailList(IncomeStatementInData inData);

    void insert(IncomeStatementInData inData);

    void approve(IncomeStatementInData inData);

    void approveIncomeStatement(GetWfApproveInData inData, int isDeductStock);

    List getIncomeStatementByYLZ(Map<String,String> map);

    void removeIncomeDetail(RemoveIncomeForm removeIncomeForm);
}
