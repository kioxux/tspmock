package com.gys.business.service.data.DailySettlement;

import lombok.Data;

import java.io.Serializable;

@Data
public class DailyReconcileDetailAuthPay implements Serializable {
    private static final long serialVersionUID = -8162083147802884391L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String brId;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 抵用券
     */
    private String voucher;

    /**
     * 电子券
     */
    private String eBrokers;


    /**
     * 积分抵现
     */
    private String integral;

}
