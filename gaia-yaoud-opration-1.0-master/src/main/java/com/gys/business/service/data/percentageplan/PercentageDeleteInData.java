package com.gys.business.service.data.percentageplan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PercentageDeleteInData {
    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "方案id")
    private Long id;

    @ApiModelProperty(value = "提成类型 1 销售 2 单品")
    private String planType;

}
