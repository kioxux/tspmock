package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@ApiModel(value = "商品信息")
@Data
public class GetQueryProductOutData implements Serializable {
    private static final long serialVersionUID = -5795636988508056253L;

    @ApiModelProperty(value = "门店号")
    private String brId;

    @ApiModelProperty(value = "门店名")
    private String brName;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @ApiModelProperty(value = "")
    private String index;

    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "商品通用名")
    private String proCommonName;

    @ApiModelProperty(value = "零售价")
    private String prcAmount;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;

    @ApiModelProperty(value = "产地")
    private String proPlace;

    @ApiModelProperty(value = "剂型")
    private String proForm;

    @ApiModelProperty(value = "国际条形码1")
    private String proBarcode;

    @ApiModelProperty(value = "批准文号")
    private String proRegisterNo;

    @ApiModelProperty(value = "成分分类")
    private String proCompclass;

    @ApiModelProperty(value = "计量单位")
    private String proUnit;

    /**
     * 判断是否是处方 1 / 4  处方
     */
    @ApiModelProperty(value = "处方类别 1-处方药，2-甲类OTC，3-乙类OTC，4-双跨处方")
    private String proPresclass;

    @ApiModelProperty(value = "管制特殊分类 1-毒性药品，2-麻醉药品，3-一类精神药品，4-二类精神药品，5-易制毒药品（麻黄碱），6-放射性药品，7-生物制品（含胰岛素），8-兴奋剂（除胰岛素），9-第一类器械，10-第二类器械，11-第三类器械，12-其它管制")
    private String proControlClass;

    @ApiModelProperty(value = "助记码")
    private String proPym;

    @ApiModelProperty(value = "库存数量")
    private String proGssQty;

    @ApiModelProperty(value = "贮存条件 1-常温，2-阴凉，3-冷藏")
    private String proStorageCondition;

    @ApiModelProperty(value = "批号")
    private String gssbBatchNo;

    @ApiModelProperty(value = "批次")
    private String gssbBatch;

    @ApiModelProperty(value = "有效期")
    private String gssbValidDate;

    @ApiModelProperty(value = "中包装量")
    private String proMidPackage;

    @ApiModelProperty(value = "保质期")
    private String proLife;

    @ApiModelProperty(value = "税率编码")
    private String gsrdInputTax;

    @ApiModelProperty(value = "税率值")
    private String gsrdInputTaxValue;

    @ApiModelProperty(value = "税率值")
    private String proRate;

    @ApiModelProperty(value = "商品仓储分区 1-内服药品，2-外用药品，3-中药饮片区，4-精装中药区，5-针剂，6-二类精神药品，7-含麻药品，8-冷藏商品，9-外用非药，10-医疗器械，11-食品，12-保健食品，13-易燃商品区")
    private String proStorageArea;

    @ApiModelProperty(value = "定位")
    private String proPosition;

    @ApiModelProperty(value = "大类")
    private String bigClass;

    @ApiModelProperty(value = "中类")
    private String midClass;

    @ApiModelProperty(value = "小类")
    private String minClass;

    @ApiModelProperty(value = "禁止销售  1:禁止销售"  )
    private String proNoRelate;

    @ApiModelProperty(value = "货位号")
    private String huowei;

    @ApiModelProperty(value = "医保刷卡数量")
    private String proZdy1;

    @ApiModelProperty(value = "是否医保")
    private String proIfMed;

    @ApiModelProperty(value = "等级")
    private String saleClass;

    @ApiModelProperty(value = "生产日期")
    private String proMadeDate;

    @ApiModelProperty(value = "批号")
    private String proBatchNo;

    @ApiModelProperty(value = "单价")
    private String proPrice;

    @ApiModelProperty(value = "数量")
    private String proQty;

    @ApiModelProperty(value = "折扣额")
    private String remark;

    @ApiModelProperty("所有门店库存")
    private BigDecimal allStoreStock;

    @ApiModelProperty(value = "本店库存")
    private String gsrdStockStore;

    @ApiModelProperty(value = "仓库库存")
    private String gsrdStockDepot;

    @ApiModelProperty(value = "禁止采购 0 否 1 是")
    private String noPurchase;

    @ApiModelProperty(value = "特殊属性 1：防疫")
    private String proTssx;

    @ApiModelProperty(value = "不打折商品 1：是")
    private String proBdz;

    @ApiModelProperty(value = "用法用量")
    private String proUsage;

    @ApiModelProperty(value = "注意事项")
    private String proContraindication;

    @ApiModelProperty(value = "商品提示")
    private String gsstExplain5;

    @ApiModelProperty(value = "商品禁忌")
    private String gsscExplain5;

    @ApiModelProperty(value = "主成分优先级")
    private String gssrPriority1;

    @ApiModelProperty(value = "关联默认优先级")
    private String gssrPriority3;

    @ApiModelProperty(value = "图片路径")
    private String imageUrl = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201609%2F24%2F20160924025917_XKWzG.thumb.700_0.jpeg&refer=http%3A%2F%2Fb-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1617861328&t=5f4002e61120b4475898362f57380857";

    @ApiModelProperty(value = "商品会员信息")
    private GetPromByProCodeOutData prom;

    @ApiModelProperty(value = "商品促销信息")
    private List<PromotionalConditionData> conditionList;

    @ApiModelProperty(value = "零售价")
    private BigDecimal gssdPrc1;

    @ApiModelProperty(value = "国家贯标编码")
    private String medProductCode;

    @ApiModelProperty(value = "末次进价")
    private BigDecimal lastTimePoPrice;

    private String prcHy;
    /**
     * 国家医保编码
     */
    private String proMedProdctcode;
    /**
     * 医保限量
     */
    private String proMedQty;
    /**
     * 是否拆零 1是0否
     */
    private String proIfpart;
    /**
     * 国家医保目录名称
     */
    private String proMedListname;

    /**
     * 拆零单位
     */
    private String proPartUint;

    /**
     * 拆零比例
     */
    private String proPartRate;
    /**
     * 会员价
     */
    private BigDecimal gsppPriceHy;
    /**
     * 批号
     */
    private String batchNo;
    /**
     * 效期
     */
    private String validDate;

}
