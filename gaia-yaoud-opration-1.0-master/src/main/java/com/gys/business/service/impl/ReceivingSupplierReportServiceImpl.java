package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdAcceptDMapper;
import com.gys.business.service.ReceivingSupplierReportService;
import com.gys.business.service.data.SupplierRecordInData;
import com.gys.business.service.data.SupplierRecordOutData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xiaoyuan on 2020/9/10
 */
@Service
public class ReceivingSupplierReportServiceImpl implements ReceivingSupplierReportService {

    @Autowired
    private GaiaSdAcceptDMapper gaiaSdAcceptDMapper;

    @Override
    public List<SupplierRecordOutData> findReportRecord(SupplierRecordInData inData) {
        return gaiaSdAcceptDMapper.findReportRecord(inData);
    }
}
