package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @author xiaoyuan on 2020/9/30
 */
@Data
public class ElectronChildInVO implements Serializable {
    private static final long serialVersionUID = -2936522816682371352L;

    @NotBlank(message = "电子券活动号不可为空")
    @ApiModelProperty(value = "电子券活动号")
    private String gsebId;


    @ApiModelProperty(value = "达到数量")
    private String gsecsReachQty1;


    @ApiModelProperty(value = "达到金额")
    private String gsecsReachAmt1;

    @Length(max = 16,message = "送券数量最大长度为16")
    @ApiModelProperty(value = "送券数量")
    private String gsecsResultQty1;

    @ApiModelProperty(value = "是否重复 N为否 / Y为是")
    private String gsecsFlag1;

    @ApiModelProperty(value = "是否全场商品 N为否 / Y为是")
    private String gsecsFlag2;

    @ApiModelProperty(value = "商品组下的 商品id 集合方式")
    private List<String> gspgProIds;
}
