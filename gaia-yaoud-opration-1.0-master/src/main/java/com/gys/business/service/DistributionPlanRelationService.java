package com.gys.business.service;

import com.gys.business.mapper.entity.DistributionPlanRelation;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 15:09
 */
public interface DistributionPlanRelationService {

    DistributionPlanRelation getById(Long id);

    DistributionPlanRelation add(DistributionPlanRelation distributionPlanRelation);

}
