package com.gys.business.service;

import com.gys.business.mapper.entity.RecommendFirstPlanProductRes;
import com.gys.business.service.data.RecommendChooseProductInData;
import com.gys.business.service.data.RecommendFirstPlanInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;

import java.util.Map;

/**
 * <p>
 * 首推商品方案表 服务类
 * </p>
 *
 * @author flynn
 * @since 2021-08-04
 */
public interface IRecommendFirstPlanService {

        //新增初始化
        Map<String,Object> buildInit(GetLoginOutData userInfo) ;

        //新增
        Object build(GetLoginOutData userInfo, RecommendFirstPlanInData inData) ;

        //修改初始化
        Map<String,Object> updateInit(GetLoginOutData userInfo, Integer id) ;

        //修改
        Object update(GetLoginOutData userInfo, RecommendFirstPlanInData updateVo) ;

        //删除
        int delete(GetLoginOutData userInfo, Integer id) ;

        //详情
        Object getDetailById(GetLoginOutData userInfo, Integer id) ;


        //获取分页列表
        PageInfo getListPage(GetLoginOutData userInfo, RecommendFirstPlanInData inData) ;

        Object audit(GetLoginOutData userInfo, Integer id);

        Object stop(GetLoginOutData userInfo, Integer id);

        Object copy(GetLoginOutData userInfo, Integer id);

        PageInfo<RecommendFirstPlanProductRes> chooseProduct(GetLoginOutData userInfo, RecommendChooseProductInData inData);
}
