package com.gys.business.service.data.mib;

import lombok.Data;

/**
 * @Author ：gyx
 * @Date ：Created in 17:10 2021/12/30
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
@Data
public class MibSXSZJSProListData {

    //项目名称(商品名称)
    private String proName;
    //规格
    private String spec;
    //单位
    private String unit;
    //数量
    private String qty;
    //单价
    private String price;
    //金额
    private String amt;
}
