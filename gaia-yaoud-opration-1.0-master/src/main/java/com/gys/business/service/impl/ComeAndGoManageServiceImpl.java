package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.dianping.lion.client.util.CollectionUtils;
import com.gys.business.mapper.ComeAndGoManageMapper;
import com.gys.business.mapper.GaiaClSystemParaMapper;
import com.gys.business.mapper.GaiaSdSaleDMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.GaiaComeAndGoDetailData;
import com.gys.business.mapper.entity.GaiaComeAndGoResidualData;
import com.gys.business.service.ComeAndGoManageService;
import com.gys.business.service.GaiaComeAndGoBatchImportInData;
import com.gys.business.service.data.GaiaClSystemPara;
import com.gys.business.service.data.GaiaComeAndGoQueryData;
import com.gys.business.service.data.GaiaComeAndGoSavePaymentInData;
import com.gys.business.service.data.GaiaComeAndGoSummeryQueryData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 15:17 2021/7/21
 * @Description：往来管理service
 * @Modified By：guoyuxi.
 * @Version:
 */
@Service
@Slf4j
public class ComeAndGoManageServiceImpl implements ComeAndGoManageService {

    @Autowired
    private ComeAndGoManageMapper comeAndGoManageMapper;
    @Autowired
    private GaiaClSystemParaMapper gaiaClSystemParaMapper;

    @Autowired
    private GaiaSdSaleDMapper saleDMapper;

    @Autowired
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Override
    public PageInfo getComeAndGoDetailList(GaiaComeAndGoQueryData inData) {
        List<GaiaComeAndGoDetailData> detailList = comeAndGoManageMapper.getComeAndGoDetailList(inData);
        List<Map<String, String>> paymentMethods = comeAndGoManageMapper.findPaymentMethodsByClient(inData.getClient());

        detailList.forEach(detail -> {
            paymentMethods.forEach(payment -> {
                if (detail.getPaymentId().equals(payment.get("PAYMENT_ID"))) {
                    detail.setPaymentName(payment.get("PAYMENT_NAME"));
                }
            });
        });
        Map<String, Object> totalMap = new HashMap<>();
        for (GaiaComeAndGoDetailData detail : detailList) {
            if (totalMap.get("amountOfPayment") != null) {
                BigDecimal paymentAmt = new BigDecimal(totalMap.get("amountOfPayment").toString());
                BigDecimal add = detail.getAmountOfPayment();
                totalMap.put("amountOfPayment", paymentAmt.add(add).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            } else {
                totalMap.put("amountOfPayment", detail.getAmountOfPayment());
            }

            if (totalMap.get("amountOfPaymentRealistic") != null) {
                BigDecimal paymentAmt = new BigDecimal(totalMap.get("amountOfPaymentRealistic").toString());
                BigDecimal add = detail.getAmountOfPaymentRealistic();
                totalMap.put("amountOfPaymentRealistic", paymentAmt.add(add).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            } else {
                totalMap.put("amountOfPaymentRealistic", detail.getAmountOfPaymentRealistic());
            }
        }
        if (!totalMap.isEmpty() && totalMap.get("amountOfPayment") != null && totalMap.get("amountOfPaymentRealistic") != null) {
            BigDecimal countAmt = new BigDecimal(totalMap.get("amountOfPayment").toString());
            BigDecimal countRealAmt = new BigDecimal(totalMap.get("amountOfPaymentRealistic").toString());
            if (countAmt.compareTo(BigDecimal.ZERO) != 0) {
                String amountRate = countRealAmt.divide(countAmt, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP) + "%";
                totalMap.put("amountRate", amountRate);
            }
        }


        return new PageInfo(detailList, totalMap);
    }

    @Override
    public JsonResult getStoList(String client, Map<String, String> queryString) {
        List<Map<String, String>> stoList = comeAndGoManageMapper.getStoListByClient(client, queryString.get("queryString"));
        return JsonResult.success(stoList, "查询成功");
    }

    @Transactional
    @Override
    public JsonResult batchSavePayment(List<GaiaComeAndGoSavePaymentInData> inData, GetLoginOutData userInfo) {


        List<Map<String, String>> paymentMethods = comeAndGoManageMapper.findPaymentMethodsByClient(userInfo.getClient());
        List<String> paymentIds = paymentMethods.stream().map(map -> map.get("PAYMENT_ID")).collect(Collectors.toList());
        List<String> paymentNames = paymentMethods.stream().map(map -> map.get("PAYMENT_NAME")).collect(Collectors.toList());
        List<String> insertDataPaymentIds = inData.stream().map(GaiaComeAndGoSavePaymentInData::getPaymentId).collect(Collectors.toList());
        List<String> insertDataPaymentNames = inData.stream().map(GaiaComeAndGoSavePaymentInData::getPaymentName).collect(Collectors.toList());
        for (String id : insertDataPaymentIds) {
            if (paymentIds.contains(id)) {
                throw new BusinessException("您要添加的支付方式编码已存在，请不要重复添加");
            }
        }
        for (String paymentName : insertDataPaymentNames) {
            if (paymentNames.contains(paymentName)) {
                throw new BusinessException("您要添加的支付方式名称已存在，请不要重复添加");
            }
        }
        Date now = new Date();

        inData.forEach(in -> {
            in.setClient(userInfo.getClient());
            in.setPaymentCreId(userInfo.getLoginName());
            in.setPaymentCreTime(DateUtil.format(now, "HHmmss"));
            in.setPaymentCreDate(DateUtil.format(now, "yyyyMMdd"));
        });

        comeAndGoManageMapper.batchSavePayment(inData);
        return JsonResult.success(null, "保存成功");
    }

    @Transactional
    @Override
    public JsonResult batchRemovePayment(List<GaiaComeAndGoSavePaymentInData> inData, GetLoginOutData loginUser) {
        List<String> paymentIdsBeRemoved = inData.stream().map(GaiaComeAndGoSavePaymentInData::getPaymentId).collect(Collectors.toList());
        if (paymentIdsBeRemoved.contains("8888") || paymentIdsBeRemoved.contains("9999") || paymentIdsBeRemoved.contains("6666")) {
            throw new BusinessException("6666调拨往来, 8888期初余额，9999仓库往来为系统支付方式，不可删除。");
        }
        List<String> paymentIds = comeAndGoManageMapper.findPaymentIdsByClient(loginUser.getClient());
        for (String id : paymentIdsBeRemoved) {
            if (!paymentIds.contains(id)) {
                return JsonResult.error(200, "你要删除的支付方式列表中有还未添加的支付方式。删除失败。");
            }
        }
        comeAndGoManageMapper.batchRemovePayment(loginUser.getClient(), paymentIdsBeRemoved);
        return JsonResult.success(null, "删除成功");
    }


    @Override
    @Transactional
    public JsonResult comeAndGoBatchImportExcel(GaiaComeAndGoBatchImportInData inData) {
        List<GaiaComeAndGoDetailData> comeAndGoDetailDataList = inData.getImportInDataList();
        if (CollUtil.isEmpty(comeAndGoDetailDataList)) {
            throw new BusinessException("录入参数不能为空");
        }
        List<String> paymentIds = comeAndGoManageMapper.findPaymentIdsByClient(inData.getClientId());
        List<String> importPaymentIds = comeAndGoDetailDataList.stream().map(GaiaComeAndGoDetailData::getPaymentId).collect(Collectors.toList());
        for (String imp : importPaymentIds) {
            if (!paymentIds.contains(StrUtil.isEmpty(imp) ? null : imp.trim())) {
                throw new BusinessException("您导入的数据中存在未添加过的支付方式，请确保您导入的数据中的所有支付方式都是您已添加过的支付方式！");
            }
        }
        List<GaiaComeAndGoDetailData> saveDetailList = new ArrayList<>();
        comeAndGoDetailDataList.forEach(detail -> {
            if ("8888".equals(detail.getPaymentId())) {
                GaiaComeAndGoDetailData detailToSave = setDetail(inData.getClientId(), detail);
                saveDetailList.add(detailToSave);
                comeAndGoManageMapper.batchSaveComeAndGoDetail(saveDetailList);
                saveDetailList.clear();
            } else {
                Date startDate = comeAndGoManageMapper.find8888(inData.getClientId(), detail.getDcCode(), detail.getStoCode());
                if (ObjectUtil.isNull(startDate)) {
                    throw new BusinessException("请先设置8888期初余额的开始日期再导入明细");
                }
                if (DateUtil.parse(detail.getPaymentDate(), "yyyyMMdd").isBefore(startDate)) {
                    throw new BusinessException("不可以导入交易日期比预先设置的期初余额开始日期更早的明细!");
                }
                GaiaComeAndGoDetailData detailToSave = setDetail(inData.getClientId(), detail);
                saveDetailList.add(detailToSave);
            }
        });
        if (CollectionUtils.isNotEmpty(saveDetailList)) {
            // 将余额汇总到GAIA_STORE_DATA
//            for (GaiaComeAndGoDetailData gaiaComeAndGoDetailData : saveDetailList) {
//                // 不管什么支付类型，都要插入余额
//                BigDecimal bigDecimal = gaiaComeAndGoDetailData.getAmountOfPaymentRealistic().setScale(4, BigDecimal.ROUND_HALF_UP);
//                gaiaStoreDataMapper.increaserRemainingSum(inData.getClientId(),gaiaComeAndGoDetailData.getStoCode(),bigDecimal);
//            }
            comeAndGoManageMapper.batchSaveComeAndGoDetail(saveDetailList);
        }
        return JsonResult.success(null, "批量导入成功");
    }

    @Override
    public JsonResult batchSaveComeAndGoDetail(List<GaiaComeAndGoDetailData> detailList, GetLoginOutData loginUser) {
        if (detailList.isEmpty()) {
            throw new BusinessException("录入参数不能为空");
        }
        List<GaiaComeAndGoDetailData> saveDetailList = new ArrayList<>();
        detailList.forEach(detail -> {
            if ("8888".equals(detail.getPaymentId())) {
                GaiaComeAndGoDetailData detailToSave = setDetail(loginUser.getClient(), detail);
                saveDetailList.add(detailToSave);
                comeAndGoManageMapper.batchSaveComeAndGoDetail(saveDetailList);
                // 将余额汇总到GAIA_STORE_DATA
//                for (GaiaComeAndGoDetailData gaiaComeAndGoDetailData : saveDetailList) {
//                    BigDecimal bigDecimal = gaiaComeAndGoDetailData.getAmountOfPaymentRealistic().setScale(4, BigDecimal.ROUND_HALF_UP);
//                    gaiaStoreDataMapper.increaserRemainingSum(loginUser.getClient(),gaiaComeAndGoDetailData.getStoCode(),bigDecimal);
//                }
                saveDetailList.clear();
            } else {
                Date startDate = comeAndGoManageMapper.find8888(loginUser.getClient(), detail.getDcCode(), detail.getStoCode());
                if (ObjectUtil.isNull(startDate)) {
                    throw new BusinessException("请先设置8888期初余额的开始日期再导入明细");
                }
                if (DateUtil.parseDate(detail.getPaymentDate()).isBefore(startDate)) {
                    throw new BusinessException("不可以导入交易日期比预先设置的期初余额开始日期更早的明细!");
                }
                GaiaComeAndGoDetailData detailToSave = setDetail(loginUser.getClient(), detail);
                saveDetailList.add(detailToSave);
            }
        });
        if (CollectionUtils.isNotEmpty(saveDetailList)) {
            comeAndGoManageMapper.batchSaveComeAndGoDetail(saveDetailList);
            // 将余额汇总到GAIA_STORE_DATA
//            for (GaiaComeAndGoDetailData gaiaComeAndGoDetailData : saveDetailList) {
//                BigDecimal bigDecimal = gaiaComeAndGoDetailData.getAmountOfPaymentRealistic().setScale(4, BigDecimal.ROUND_HALF_UP);
//                gaiaStoreDataMapper.increaserRemainingSum(loginUser.getClient(),gaiaComeAndGoDetailData.getStoCode(),bigDecimal);
//            }
        }
        return JsonResult.success(null, "批量导入成功");
    }

    private GaiaComeAndGoDetailData setDetail(String client, GaiaComeAndGoDetailData detail) {
        detail.setClient(client);
        setPaymentDate(detail);
        detail.setUploadDate(DateUtil.format(DateUtil.parse(DateUtil.now()), "yyyyMMdd"));
        detail.setUploadTime(DateUtil.format(DateUtil.parse(DateUtil.now()), "HHmmss"));
        String stoName = comeAndGoManageMapper.findStoNameByClientAndStoCode(client, detail.getStoCode());
        if (StrUtil.isEmpty(stoName)) {
            throw new BusinessException("该加盟商下没有这个门店");
        }
        detail.setStoName(stoName);
        String dcName = comeAndGoManageMapper.findDcNameByClientAndDcCode(client, detail.getDcCode());
        if (StrUtil.isEmpty(dcName)) {
            throw new BusinessException("该加盟商下没有这个仓库");
        }
        detail.setDcName(dcName);

        detail.setAmountOfPaymentRealistic(
                detail.getAmountOfPayment()
                        .multiply(new BigDecimal(detail.getAmountRate()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP))
                        .setScale(2, BigDecimal.ROUND_HALF_UP));
        detail.setAmountRate(new BigDecimal(detail.getAmountRate().replace("%", "")).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).toString());
        return detail;
    }

    private void setPaymentDate(GaiaComeAndGoDetailData detail) {
        if (detail.getPaymentDate().contains("-")) {
            detail.setPaymentDate(DateUtil.format(DateUtil.parseDate(detail.getPaymentDate()), "yyyyMMdd"));
        }
    }

    @Override
    public JsonResult getDcList(String client) {
        List<Map<String, String>> dcList = comeAndGoManageMapper.getDcListByClient(client);
        return JsonResult.success(dcList, "查询成功");
    }


    @Override
    public PageInfo getComeAndGoSummaryList(GaiaComeAndGoSummeryQueryData inData) {

        //查询支付方式
        List<String> paymentIds = comeAndGoManageMapper.findPaymentIdsByClient(inData.getClient());
        List<Map<String, String>> paymentMethods = comeAndGoManageMapper.findPaymentMethodsByClient(inData.getClient());
        List<Map<String, Object>> resultData = comeAndGoManageMapper.findComeAndGoSummaryData(paymentMethods, inData);

        //合计
        Map<String, Object> totalMap = new HashMap<>();
        totalMap.put("residualAmt", "0.00");
        paymentMethods.forEach(payment -> {
            totalMap.put(payment.get("PAYMENT_ID"), "0.00");
        });
        totalMap.put("endAmt", "0.00");
        resultData.forEach(result -> {
            if (totalMap.get("residualAmt") != null) {
                totalMap.put("residualAmt", new BigDecimal(totalMap.get("residualAmt").toString()).add((BigDecimal) result.get("residualAmt")).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            } else {
                if (result.get("residualAmt") != null) {
                    totalMap.put("residualAmt", result.get("residualAmt"));
                } else {
                    totalMap.put("residualAmt", "0");
                }
            }
            paymentIds.forEach(paymentId -> {
                if (result.containsKey(paymentId)) {
                    if (result.get(paymentId) != null) {
                        if (totalMap.get(paymentId) != null) {
                            totalMap.put(paymentId, new BigDecimal(totalMap.get(paymentId).toString()).add(new BigDecimal(result.get(paymentId).toString())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                        } else {
                            totalMap.put(paymentId, result.get(paymentId));
                        }
                    } else {
                        totalMap.put(paymentId, "0");
                    }
                }
            });
            if (totalMap.get("endAmt") != null) {
                if (result.get("endAmt") != null) {
                    totalMap.put("endAmt", new BigDecimal(totalMap.get("endAmt").toString()).add(new BigDecimal(result.get("endAmt").toString())).setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                } else {
                    totalMap.put("endAmt", new BigDecimal(totalMap.get("endAmt").toString()));
                }
            } else {
                if (result.get("endAmt") != null) {
                    totalMap.put("endAmt", new BigDecimal(result.get("endAmt").toString()));
                } else {
                    totalMap.put("endAmt", totalMap.get("residualAmt"));
                }
            }
        });

        return new PageInfo(resultData, totalMap);
    }

    @Override
    public JsonResult getStartDate(String client, Map<String, String> map) {
        Date startDate = comeAndGoManageMapper.find8888(client, map.get("dcCode"), map.get("stoCode"));
        return JsonResult.success(DateUtil.format(startDate, "yyyyMMdd"), "查询成功");
    }

    @Override
    public JsonResult findPaymentList(String client) {
        List<String> paymentIds = comeAndGoManageMapper.findPaymentIdsByClient(client);
        List<String> idFor8888 = paymentIds.stream().filter((id) -> id.equals("8888")).collect(Collectors.toList());
        List<String> idFor9999 = paymentIds.stream().filter((id) -> id.equals("9999")).collect(Collectors.toList());
        List<String> idFor6666 = paymentIds.stream().filter((id) -> id.equals("6666")).collect(Collectors.toList());
        if (idFor8888.isEmpty()) {
            addPayment(client, "8888", "期初余额");
        }
        if (idFor9999.isEmpty()) {
            addPayment(client, "9999", "仓库往来");
        }
        if (idFor6666.isEmpty()) {
            addPayment(client, "6666", "调拨往来");
        }
        List<Map<String, String>> paymentMethodsByClient = comeAndGoManageMapper.findPaymentMethodsByClient(client);
        return JsonResult.success(paymentMethodsByClient, "查询成功");
    }

    private void addPayment(String client, String paymentId, String paymentName) {
        GaiaComeAndGoSavePaymentInData payment = new GaiaComeAndGoSavePaymentInData();
        Date now = new Date();
        payment.setClient(client);
        payment.setPaymentId(paymentId);
        payment.setPaymentName(paymentName);
        payment.setPaymentCreId("system");
        payment.setPaymentCreDate(DateUtil.format(now, "yyyyMMdd"));
        payment.setPaymentCreTime(DateUtil.format(now, "HHmmss"));
        comeAndGoManageMapper.savePayment(payment);
    }

    /**
     * 计算往来管理余额
     */
    @Transactional
    public void computeComeAndGoResAmt() {

        Calendar cc = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        cc.add(Calendar.DATE, -1);
        String yesterDay = sdf.format(cc.getTime());
        List<GaiaComeAndGoDetailData> detailDataList = comeAndGoManageMapper.findYesterDayUploadDetail(yesterDay);//日期+门店集合
        List<GaiaComeAndGoResidualData> residualDataList = new ArrayList<>();
        for (GaiaComeAndGoDetailData detailData : detailDataList) {
            try {
                //2.查当前日期+门店的前一天的余额
                cc.setTime(sdf.parse(detailData.getPaymentDate()));
                cc.add(Calendar.DATE, -1);
                String beforePaymentDate = sdf.format(cc.getTime());
                GaiaComeAndGoResidualData historyResidual = comeAndGoManageMapper.findHistoryResidual(beforePaymentDate, detailData.getClient(), detailData.getDcCode(), detailData.getStoCode());

                if (historyResidual == null) {
                    //补充所有当前日期-门店的日期之前的所有余额数据，
                    //1.按照日期-门店查询门店汇总 asc排序
                    //删除日期之前的所有余额
                    comeAndGoManageMapper.batchRemoveResidualByStoAndPayDate(detailData.getPaymentDate(), detailData.getClient(), detailData.getDcCode(), detailData.getStoCode());
                    List<GaiaComeAndGoDetailData> beforeDetailDataList = comeAndGoManageMapper.findComeAndGoDetailSummeryByDateBefore(detailData.getPaymentDate(), detailData.getClient(), detailData.getDcCode(), detailData.getStoCode());
                    String minDate = comeAndGoManageMapper.selectMinDateDetailTableBySto(detailData.getClient(), detailData.getDcCode(), detailData.getStoCode());

                    /*GaiaComeAndGoResidualData defaultResidual = new GaiaComeAndGoResidualData();
                    defaultResidual.setClient(detailData.getClient());
                    defaultResidual.setDcCode(detailData.getDcCode());
                    defaultResidual.setDcName(detailData.getDcName());
                    defaultResidual.setStoCode(detailData.getStoCode());
                    defaultResidual.setStoName(detailData.getStoName());
//                    defaultResidual.setBalanceDate(beforePaymentDate);
                    defaultResidual.setBalanceDate(yesterDay);
                    defaultResidual.setResidualAmt("0");*/

                    cc.setTime(sdf.parse(minDate));
                    List<GaiaComeAndGoResidualData> insertList = new ArrayList<>();
                    while (cc.getTime().before(sdf.parse(detailData.getPaymentDate())) || cc.getTime().equals(sdf.parse(detailData.getPaymentDate()))) {
                        GaiaComeAndGoResidualData currentZuotian = new GaiaComeAndGoResidualData();
                        currentZuotian.setClient(detailData.getClient());
                        currentZuotian.setDcCode(detailData.getDcCode());
                        currentZuotian.setDcName(detailData.getDcName());
                        currentZuotian.setStoCode(detailData.getStoCode());
                        currentZuotian.setStoName(detailData.getStoName());
//                        currentZuotian.setBalanceDate(sdf.format(cc.getTime()));
                        currentZuotian.setBalanceDate(sdf.format(cc.getTime()));
                        currentZuotian.setResidualAmt("0");
                        List<GaiaComeAndGoDetailData> collect = beforeDetailDataList.stream().filter(stoDetail -> {
                            return stoDetail.getPaymentDate().equals(sdf.format(cc.getTime()));
                        }).collect(Collectors.toList());

                        if (!collect.isEmpty()) {
                            //如果明细汇总有这一天的数据
                            if (!insertList.isEmpty()) {
                                GaiaComeAndGoResidualData residualData = insertList.get(insertList.size() - 1);
                                BigDecimal count = new BigDecimal(residualData.getResidualAmt());
                                BigDecimal add = new BigDecimal(collect.get(0).getAmt());
                                currentZuotian.setResidualAmt(count.add(add).setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                            } else {
                                currentZuotian.setResidualAmt(collect.get(0).getAmt());
                            }
                        } else {
                            //如果没有就取上一次的
                            if (!insertList.isEmpty()) {
                                GaiaComeAndGoResidualData residualData = insertList.get(insertList.size() - 1);
                                currentZuotian.setResidualAmt(residualData.getResidualAmt());
                            }
                        }
                        insertList.add(currentZuotian);
                        cc.add(Calendar.DATE, +1);
                    }
                    if (!insertList.isEmpty()) {
                        //情况一: 没查到当前明细的当前交易时间的前一天的余额并且交易时间不是昨天
                        comeAndGoManageMapper.batchSaveComeAndGoResidualData(insertList);
                        log.info("新增余额记录{}", insertList);
                    }

                    if (!detailData.getPaymentDate().equals(yesterDay)) {
                        //情况二: 没查到当前明细的当前交易时间的前一天的余额但是交易时间是昨天
                        comeAndGoManageMapper.batchUpdateComeAndGoResidualDataNotEqualDate(detailData);
                        log.info("修改余额记录,门店:{},时间:{},金额:{}", detailData.getStoCode() + detailData.getStoName(), detailData.getPaymentDate(), detailData.getAmt());
                    }

                } else {
                    //判断交易日期是否为昨日
                    if (detailData.getPaymentDate().equals(yesterDay)) {

                        //新增余额表数据
                        List<GaiaComeAndGoResidualData> insertList = new ArrayList<>();
                        GaiaComeAndGoResidualData zuotian = new GaiaComeAndGoResidualData();
                        zuotian.setClient(detailData.getClient());
                        zuotian.setDcCode(detailData.getDcCode());
                        zuotian.setDcName(detailData.getDcName());
                        zuotian.setStoCode(detailData.getStoCode());
                        zuotian.setStoName(detailData.getStoName());

                        zuotian.setBalanceDate(yesterDay);
                        BigDecimal count = new BigDecimal(historyResidual.getResidualAmt());
                        BigDecimal add = new BigDecimal(detailData.getAmt());
                        zuotian.setResidualAmt(count.add(add).setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                        insertList.add(zuotian);
                        //情况三: 查到了当前明细的当前交易时间的前一天的余额而且交易时间是昨天
                        comeAndGoManageMapper.batchSaveComeAndGoResidualData(insertList);
                        log.info("新增余额记录{}", insertList);
                    } else {
                        //情况四: 查到了当前明细的当前交易时间的前一天的余额并且交易时间不是昨天
                        //修改交易日期之后的所有余额
                        GaiaComeAndGoResidualData payResidual = comeAndGoManageMapper.findHistoryResidual(detailData.getPaymentDate(), detailData.getClient(), detailData.getDcCode(), detailData.getStoCode());
                        if (payResidual == null) {
                            //新增余额表数据
                            List<GaiaComeAndGoResidualData> insertList = new ArrayList<>();
                            GaiaComeAndGoResidualData zuotian = new GaiaComeAndGoResidualData();
                            zuotian.setClient(detailData.getClient());
                            zuotian.setDcCode(detailData.getDcCode());
                            zuotian.setDcName(detailData.getDcName());
                            zuotian.setStoCode(detailData.getStoCode());
                            zuotian.setStoName(detailData.getStoName());

                            zuotian.setBalanceDate(detailData.getPaymentDate());
                            BigDecimal count = new BigDecimal(historyResidual.getResidualAmt());
                            BigDecimal add = new BigDecimal(detailData.getAmt());
                            zuotian.setResidualAmt(count.add(add).setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                            insertList.add(zuotian);
                            comeAndGoManageMapper.batchSaveComeAndGoResidualData(insertList);
                            comeAndGoManageMapper.batchUpdateComeAndGoResidualDataNotEqualDate(detailData);
                        } else {
                            comeAndGoManageMapper.batchUpdateComeAndGoResidualData(detailData);
                        }
                        log.info("修改余额记录,门店:{},时间:{},金额:{}", detailData.getStoCode() + detailData.getStoName(), detailData.getPaymentDate(), detailData.getAmt());
                    }
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


    }


    @Override
    public void autoComputeDistributionDataAndAddComeAndGoDetail() {
        long startTime = System.currentTimeMillis();
        //1.计算昨天的配送数据的调拨额合计
        List<String> clientList = comeAndGoManageMapper.getClientList();
        clientList.forEach(client -> {
            List<String> dcCodeList = comeAndGoManageMapper.getDcCodeListByClient(client);
            if (!dcCodeList.isEmpty()) {
                dcCodeList.forEach(dcCode -> {
                    List<String> stoList = comeAndGoManageMapper.findStoListByClient(client);
                    stoList.forEach(stoCode -> {
                        //1.查询此门店是否设置期初日期
                        List<Map<String, Object>> detailDataList = new ArrayList<>();
                        Date startDate = comeAndGoManageMapper.find8888(client, dcCode, stoCode);
                        if (!ObjectUtil.isNull(startDate)) {
                            //2.查询此门店是否是第一次执行job
                            Integer count = comeAndGoManageMapper.findIsFirst(client, dcCode, stoCode, startDate);
                            if (count > 0) {
                                //不是第一次
                                List<Map<String, Object>> dataList = comeAndGoManageMapper.autoComputeDistributionData(client, dcCode, stoCode, DateUtil.format(DateUtil.yesterday(), "yyyyMMdd"));
                                detailDataList.addAll(dataList);
                            } else {
                                //是第一次
                                List<String> dateList = getDateList(startDate);
                                dateList.forEach(date -> {
                                    List<Map<String, Object>> dataList = comeAndGoManageMapper.autoComputeDistributionData(client, dcCode, stoCode, date);
                                    detailDataList.addAll(dataList);
                                });
                            }
                            if (!detailDataList.isEmpty()) {
                                //2.将数据导入明细表
                                try {
                                    addToComeAndGoDetail(detailDataList);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    log.info("<自动计算配送往来数据并添加到往来明细表失败><昨日的仓库往来数据:{}><加盟商:{}><门店:{}>", detailDataList, client, stoCode);
                                }
                            }
                        }
                    });
                });
                // 绕过原逻辑直接进行处理门店往来的pd\td 逻辑
                GaiaClSystemPara mdhdsj = gaiaClSystemParaMapper.getAuthByClient(client, "MDHDSJ");
                if (mdhdsj != null && StrUtil.isNotBlank(mdhdsj.getGcspPara1()) && "0".equals(mdhdsj.getGcspPara1())) {
                    dcCodeList.forEach(dcCode -> {
                        //
//                        List<StoreDistributaionDataOutData> outSideData = new ArrayList<>();
//                        StoreDistributaionDataInData inData = new StoreDistributaionDataInData();
//                        inData.setClient(client);
//                        inData.setDcCode(dcCode);
//                        inData.setCustomerFlag("2");
                        List<Map<String, Object>> outSideData = this.saleDMapper.getCusList(client, dcCode);
//                        outSideData = this.saleDMapper.getDistributionDataByOutSide(inData);
//                        List<String> stoList = comeAndGoManageMapper.findStoListByClient(client);
                        outSideData.forEach(outData -> {
                            //1.查询此门店是否设置期初日期
                            List<Map<String, Object>> detailDataList = new ArrayList<>();
                            String stoCode = (String) outData.get("cusCode");
                            Date startDate = comeAndGoManageMapper.find8888(client, dcCode, stoCode);
                            if (!ObjectUtil.isNull(startDate)) {
                                //2.查询此门店是否是第一次执行job
                                Integer count = comeAndGoManageMapper.findIsFirstWithPayWay666(client, dcCode, stoCode, startDate);
                                if (count > 0) {
                                    //不是第一次
                                    List<Map<String, Object>> dataList = comeAndGoManageMapper.autoComputeDistributionDataPT(client, dcCode, stoCode, DateUtil.format(DateUtil.yesterday(), "yyyyMMdd"));
                                    detailDataList.addAll(dataList);
                                } else {
                                    //是第一次
                                    List<String> dateList = getDateList(startDate);
                                    dateList.forEach(date -> {
                                        List<Map<String, Object>> dataList = comeAndGoManageMapper.autoComputeDistributionDataPT(client, dcCode, stoCode, date);
                                        detailDataList.addAll(dataList);
                                    });
                                }
                                if (!detailDataList.isEmpty()) {
                                    //2.将数据导入明细表
                                    try {
                                        addToComeAndGoDetailPT(detailDataList);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        log.info("<自动计算配送往来数据并添加到往来明细表失败><昨日的仓库往来数据:{}><加盟商:{}><门店:{}>", detailDataList, client, stoCode);
                                    }
                                }
                            }
                        });
                    });
                }
            }
        });

        log.info("<任务执行完毕,总计耗时:{}毫秒>", System.currentTimeMillis() - startTime);
    }

    private List<String> getDateList(Date startDate) {
        List<String> dateList = new ArrayList<>();
        Calendar cc = Calendar.getInstance();
        cc.setTime(startDate);
        cc.add(Calendar.DATE, 1);
        while (cc.getTime().before(DateUtil.parseDate(DateUtil.today()))) {
            dateList.add(CommonUtil.dateToStr(cc.getTime()));
            cc.add(Calendar.DATE, 1);
        }
        return dateList;
    }


    public void addToComeAndGoDetail(List<Map<String, Object>> detailDataList) {
        List<GaiaComeAndGoDetailData> detailList = new ArrayList<>();
        detailDataList.forEach(detail -> {
            Date now = new Date();
            GaiaComeAndGoDetailData detailData = new GaiaComeAndGoDetailData();
            detailData.setClient(detail.get("client").toString());
            detailData.setDcCode(detail.get("dcCode").toString());
            detailData.setDcName(detail.get("dcName").toString());
            detailData.setStoCode(detail.get("cusCode").toString());
            detailData.setStoName(detail.get("cusName") == null ? "" : detail.get("cusName").toString());
            detailData.setAmountRate("1.00");
            detailData.setAmountOfPayment(new BigDecimal(detail.get("dbeRightTotal").toString()));
            detailData.setAmountOfPaymentRealistic(new BigDecimal(detail.get("dbeRightTotal").toString()));
            detailData.setRemarks("system");
            detailData.setPaymentDate(detail.get("paymentDate").toString());
            detailData.setPaymentId("9999");
            detailData.setPaymentName("仓库往来");
            detailData.setUploadDate(DateUtil.format(now, "yyyyMMdd"));
            detailData.setUploadTime(DateUtil.format(now, "HHmmss"));
            detailList.add(detailData);
        });
        comeAndGoManageMapper.batchSaveComeAndGoDetail(detailList);
        log.info("<自动计算配送往来数据并添加到往来明细表成功>");
    }

    public void addToComeAndGoDetailPT(List<Map<String, Object>> detailDataList) {
        List<GaiaComeAndGoDetailData> detailList = new ArrayList<>();
        detailDataList.forEach(detail -> {
            Date now = new Date();
            GaiaComeAndGoDetailData detailData = new GaiaComeAndGoDetailData();
            detailData.setClient(detail.get("client").toString());
            detailData.setDcCode(detail.get("dcCode").toString());
            detailData.setDcName(detail.get("dcName").toString());
            detailData.setStoCode(detail.get("cusCode").toString());
            detailData.setStoName(detail.get("cusName") == null ? "" : detail.get("cusName").toString());
            detailData.setAmountRate("1.00");
            detailData.setAmountOfPayment(new BigDecimal(detail.get("dbeRightTotal").toString()));
            detailData.setAmountOfPaymentRealistic(new BigDecimal(detail.get("dbeRightTotal").toString()));
            detailData.setRemarks("system");
            detailData.setPaymentDate(detail.get("paymentDate").toString());
            detailData.setPaymentId("6666");
            detailData.setPaymentName("调拨往来");
            detailData.setUploadDate(DateUtil.format(now, "yyyyMMdd"));
            detailData.setUploadTime(DateUtil.format(now, "HHmmss"));
            detailList.add(detailData);
        });
        comeAndGoManageMapper.batchSaveComeAndGoDetail(detailList);
        log.info("<自动计算配送往来数据并添加到往来明细表成功>");
    }
}
