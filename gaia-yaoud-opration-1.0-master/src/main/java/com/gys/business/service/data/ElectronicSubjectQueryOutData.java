package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/10/28
 */
@Data
public class ElectronicSubjectQueryOutData implements Serializable {
    private static final long serialVersionUID = 704778800269048501L;

    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    @ApiModelProperty(value = "主题属性  CM 消费型  / NM 新会员 / FM 会员推送")
    private String gsetsFlag;

    @ApiModelProperty(value = "主题描述")
    private String gsetsName;

    @ApiModelProperty(value = "创建日期")
    private String gsetsCreateDate;

    @ApiModelProperty(value = "是否审核 N为否，Y为是")
    private String gsebsStatus;

    @ApiModelProperty(value = "创建人")
    private String userNam;

    @ApiModelProperty(value = "送券起始日期")
    private String couponStartDate;

    @ApiModelProperty(value = "送券结束日期")
    private String couponEndDate;

    @ApiModelProperty(value = "用券起始日期")
    private String voucherStartDate;

    @ApiModelProperty(value = "用券结束日期")
    private String voucherEndDate;


    private String gsebsType;
}
