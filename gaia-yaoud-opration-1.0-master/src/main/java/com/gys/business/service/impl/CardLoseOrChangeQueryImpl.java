//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdRechargeCardLoseMapper;
import com.gys.business.service.CardLoseOrChangeQueryService;
import com.gys.business.service.data.GetCardLoseOrChangeInData;
import com.gys.business.service.data.GetCardLoseOrChangeOutData;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardLoseOrChangeQueryImpl implements CardLoseOrChangeQueryService {
    @Autowired
    private GaiaSdRechargeCardLoseMapper gaiaSdRechargeCardLoseMapper;

    public CardLoseOrChangeQueryImpl() {
    }

    public List<GetCardLoseOrChangeOutData> queryTableList(GetCardLoseOrChangeInData inData) {
        List<GetCardLoseOrChangeOutData> outData = this.gaiaSdRechargeCardLoseMapper.queryLostOrChange(inData);

        for(int i = 0; i < outData.size(); ++i) {
            GetCardLoseOrChangeOutData item = (GetCardLoseOrChangeOutData)outData.get(i);
            item.setIndex(String.valueOf(i + 1));
            if ("0".equals(item.getSex())) {
                item.setSex("女");
            } else if ("1".equals(item.getSex())) {
                item.setSex("男");
            }
        }

        return outData;
    }
}
