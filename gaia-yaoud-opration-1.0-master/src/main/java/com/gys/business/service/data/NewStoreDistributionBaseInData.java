package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class NewStoreDistributionBaseInData implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "门店编码")
    private String storeId;

    @ApiModelProperty(value = "用户编号")
    private String userId;

    @ApiModelProperty(value = "查询类型，必填 1-获取数据来源以及现状  2-使用门店号获取店效级别  3-使用新店面积以及预估药品销售占比获取计划药品铺货品项数")
    private String type;

    @ApiModelProperty(value = "新店面积，必填 1-(60-80)   2-(80-100)  3-(100-120)  4-(120以上)")
    private String newStoreSize;

    @ApiModelProperty(value = "预估药品销售占比，必填  1-60  2-70  3-80 4-80及以上")
    private String expectDrugSaleProp;

    @ApiModelProperty(value = "计划药品铺货品项数")
    private Integer drugsDistributionCount;

    @ApiModelProperty(value = "参考门店 最多选中三家")
    private List<String> storeIdList;

    @ApiModelProperty(value = "参考门店数量")
    private Integer storeIdCount;
}
