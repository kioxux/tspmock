package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/4 14:30
 **/
@Data
public class DistributionOperateForm {
    @ApiModelProperty(value = "铺货计划单号", example = "20210001")
    private String planCode;
    @ApiModelProperty(value = "加盟商", hidden = true)
    private String client;
    @ApiModelProperty(value = "门店id", hidden = true)
    private String storeId;
}
