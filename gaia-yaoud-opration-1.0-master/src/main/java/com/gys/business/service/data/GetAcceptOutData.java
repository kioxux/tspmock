package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@ApiModel(value = "输出结果")
@Data
public class GetAcceptOutData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "创建时间")
    private String orderDate;
    @ApiModelProperty(value = "收货单号")
    private String gsahVoucherId;
    @ApiModelProperty(value = "收货日期")
    private String gsahDate;
    @ApiModelProperty(value = "配送单号")
    private String gsahPsVoucherId;
    @ApiModelProperty(value = "采购订单号")
    private String gsahPoid;
    @ApiModelProperty(value = "发运地点")
    private String gsahDeparturePlace;
    @ApiModelProperty(value = "启运时间")
    private String gsahDepartureTime;
    @ApiModelProperty(value = "启运温度")
    private String gsahDepartureTemperature;
    @ApiModelProperty(value = "到货日期")
    private String gsahArriveDate;
    @ApiModelProperty(value = "到货时间")
    private String gsahArriveTime;
    @ApiModelProperty(value = "到货温度")
    private String gsahArriveTemperature;
    @ApiModelProperty(value = "运输单位")
    private String gsahTransportOrganization;
    @ApiModelProperty(value = "运输方式")
    private String gsahTransportMode;
    @ApiModelProperty(value = "运输方式")
    private String gsahFrom;
    @ApiModelProperty(value = "收货地点")
    private String gsahTo;
    @ApiModelProperty(value = "单据类型")
    private String gsahType;
    @ApiModelProperty(value = "收货状态")
    private String gsahStatus;
    @ApiModelProperty(value = "收货金额")
    private BigDecimal gsahTotalAmt;
    @ApiModelProperty(value = "收货数量")
    private String gsahTotalQty;
    @ApiModelProperty(value = "收货人")
    private String gsahEmp;
    @ApiModelProperty(value = "备注")
    private String gsahRemaks;
    private String supName;
    @ApiModelProperty(value = "门店编码")
    private String gsahBrId;
    @ApiModelProperty(value = "门店名称")
    private String gsahBrName;
    private List<GetAcceptDetailOutData> acceptDetailOutData;

}
