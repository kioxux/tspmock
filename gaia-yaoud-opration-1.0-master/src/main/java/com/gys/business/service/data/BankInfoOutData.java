//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class BankInfoOutData {
    private String clientId;
    private String gsbsBrId;
    private String gsbsBankId;
    private String gsbsBankName;
    private String gsbsBankAccount;
    private Integer index;

    public BankInfoOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsbsBrId() {
        return this.gsbsBrId;
    }

    public String getGsbsBankId() {
        return this.gsbsBankId;
    }

    public String getGsbsBankName() {
        return this.gsbsBankName;
    }

    public String getGsbsBankAccount() {
        return this.gsbsBankAccount;
    }

    public Integer getIndex() {
        return this.index;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsbsBrId(final String gsbsBrId) {
        this.gsbsBrId = gsbsBrId;
    }

    public void setGsbsBankId(final String gsbsBankId) {
        this.gsbsBankId = gsbsBankId;
    }

    public void setGsbsBankName(final String gsbsBankName) {
        this.gsbsBankName = gsbsBankName;
    }

    public void setGsbsBankAccount(final String gsbsBankAccount) {
        this.gsbsBankAccount = gsbsBankAccount;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof BankInfoOutData)) {
            return false;
        } else {
            BankInfoOutData other = (BankInfoOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$clientId = this.getClientId();
                Object other$clientId = other.getClientId();
                if (this$clientId == null) {
                    if (other$clientId != null) {
                        return false;
                    }
                } else if (!this$clientId.equals(other$clientId)) {
                    return false;
                }

                Object this$gsbsBrId = this.getGsbsBrId();
                Object other$gsbsBrId = other.getGsbsBrId();
                if (this$gsbsBrId == null) {
                    if (other$gsbsBrId != null) {
                        return false;
                    }
                } else if (!this$gsbsBrId.equals(other$gsbsBrId)) {
                    return false;
                }

                Object this$gsbsBankId = this.getGsbsBankId();
                Object other$gsbsBankId = other.getGsbsBankId();
                if (this$gsbsBankId == null) {
                    if (other$gsbsBankId != null) {
                        return false;
                    }
                } else if (!this$gsbsBankId.equals(other$gsbsBankId)) {
                    return false;
                }

                label62: {
                    Object this$gsbsBankName = this.getGsbsBankName();
                    Object other$gsbsBankName = other.getGsbsBankName();
                    if (this$gsbsBankName == null) {
                        if (other$gsbsBankName == null) {
                            break label62;
                        }
                    } else if (this$gsbsBankName.equals(other$gsbsBankName)) {
                        break label62;
                    }

                    return false;
                }

                label55: {
                    Object this$gsbsBankAccount = this.getGsbsBankAccount();
                    Object other$gsbsBankAccount = other.getGsbsBankAccount();
                    if (this$gsbsBankAccount == null) {
                        if (other$gsbsBankAccount == null) {
                            break label55;
                        }
                    } else if (this$gsbsBankAccount.equals(other$gsbsBankAccount)) {
                        break label55;
                    }

                    return false;
                }

                Object this$index = this.getIndex();
                Object other$index = other.getIndex();
                if (this$index == null) {
                    if (other$index != null) {
                        return false;
                    }
                } else if (!this$index.equals(other$index)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof BankInfoOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsbsBrId = this.getGsbsBrId();
        result = result * 59 + ($gsbsBrId == null ? 43 : $gsbsBrId.hashCode());
        Object $gsbsBankId = this.getGsbsBankId();
        result = result * 59 + ($gsbsBankId == null ? 43 : $gsbsBankId.hashCode());
        Object $gsbsBankName = this.getGsbsBankName();
        result = result * 59 + ($gsbsBankName == null ? 43 : $gsbsBankName.hashCode());
        Object $gsbsBankAccount = this.getGsbsBankAccount();
        result = result * 59 + ($gsbsBankAccount == null ? 43 : $gsbsBankAccount.hashCode());
        Object $index = this.getIndex();
        result = result * 59 + ($index == null ? 43 : $index.hashCode());
        return result;
    }

    public String toString() {
        return "BankInfoOutData(clientId=" + this.getClientId() + ", gsbsBrId=" + this.getGsbsBrId() + ", gsbsBankId=" + this.getGsbsBankId() + ", gsbsBankName=" + this.getGsbsBankName() + ", gsbsBankAccount=" + this.getGsbsBankAccount() + ", index=" + this.getIndex() + ")";
    }
}
