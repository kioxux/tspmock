package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel
public class StartDayAndEndDayInData implements Serializable {

    private static final long serialVersionUID = 1305221129600577416L;

    @ApiModelProperty(value = "加盟商")
    private String clientId;

    @NotBlank(message = "开始日期不可为空")
    @ApiModelProperty(value = "开始时间")
    private String startDate;

    @NotBlank(message = "结束日期不可为空")
    @ApiModelProperty(value = "结束时间")
    private String endDate;

    @ApiModelProperty(value = "查询标志")
    private String flag;

    @ApiModelProperty(value = "筛选条件 1-销售总数 2-销售总金额 3-销售笔数 4-销售金额 5-退货笔数 6-退货金额")
    private String condition;
}
