package com.gys.business.service;

import com.gys.business.service.request.*;
import com.gys.business.service.response.SelectDataEchoResponse;
import com.gys.business.service.response.SelectLargeOasSicknessPageInfoResponse;
import com.gys.business.service.response.SelectPageInfoResponse;
import com.gys.common.data.PageInfo;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 疾病大类表 服务类
 * </p>
 *
 * @author flynn
 * @since 2021-09-01
 */
public interface IOasSicknessTagService {

    /**
     * 疾病大类中类成分列表
     *
     * @param pageInfoRequest 请求参数
     * @return 返回值
     */
    SelectLargeOasSicknessPageInfoResponse selectOasSicknessPageInfo(SelectOasSicknessPageInfoRequest pageInfoRequest);


    /**
     * 新增疾病分类
     *
     * @param request 请求参数
     */
    void saveOasSickness(SaveOasSicknessRequest request);


    /**
     * 导入疾病分类Excel数据
     *
     * @param file      文件
     * @param userId    用户id
     * @param loginName 用户名称
     */
    void inputExcel(MultipartFile file, String userId, String loginName);

    /**
     * 获取疾病分类列表
     *
     * @param request 请求参数
     */
    PageInfo<List<SelectPageInfoResponse>> selectPageInfo(SelectPageInfoRequest request);

    /**
     * 数据回显
     *
     * @param request 请求参数
     */
    SelectDataEchoResponse selectDataEcho(SelectDataEchoRequest request);

    /**
     * 导出疾病分类
     *
     * @param request 请求参数
     * @return
     */
    SXSSFWorkbook export(SelectPageInfoRequest request);

    /**
     * 启用/禁用
     *
     * @param id
     */
    void updateStatus(String id, String status);

    /**
     * 修改疾病分类编码或者名称
     *
     * @param request
     */
    void updateOasSickness(UpdateOasSicknessRequest request);

}
