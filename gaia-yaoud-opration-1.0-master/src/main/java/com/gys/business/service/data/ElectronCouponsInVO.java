package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * 传参实体
 *
 * @author xiaoyuan on 2020/9/28
 */
@Data
public class ElectronCouponsInVO implements Serializable {
    private static final long serialVersionUID = 4240447563450091267L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    //@NotBlank(message = "起始不可为空")
    @ApiModelProperty(value = "起始店号")
    private String brStart;

    //@NotBlank(message = "店号不可为空")
    @ApiModelProperty(value = "结束店号")
    private String brEnd;

    @ApiModelProperty(value = "主题单号")
    private String gsetsId;

    @NotBlank(message = "起始日期不可为空")
    @ApiModelProperty(value = "起始日期")
    private String gsebsBeginDate;

    @NotBlank(message = "结束日期不可为空")
    @ApiModelProperty(value = "结束日期")
    private String gsebsEndDate;

    @NotBlank(message = "起始时间不可为空")
    @ApiModelProperty(value = "起始时间")
    private String gsebsBeginTime;

    @NotBlank(message = "结束时间不可为空")
    @ApiModelProperty(value = "结束时间")
    private String gsebsEndTime;

    @ApiModelProperty(value = "0为送券，1为用券")
    private String gsebsType;

    @Valid
    @NotEmpty(message = "活动等信息不可为空")
    @ApiModelProperty(value = "电子券系列" ,required = true)
    private List<ElectronChildInVO> childInVOS;

    @ApiModelProperty(value = "审核状态  是否审核   N为否，Y为是")
    private String gsebsStatus;

    @NotBlank(message = "店号状态不可为空")
    @ApiModelProperty(value = "店号状态")
    private String storeType;

    @ApiModelProperty(value = "业务单号 修改时不可为空!")
    private String gsebsId;

    @ApiModelProperty(value = "店号分类")
    private String storeGroup;

    @ApiModelProperty(value = "店号集合")
    private List<String> storeIds;

    @NotBlank(message = "促销商品是否参与不可为空")
    @ApiModelProperty(value = "促销商品是否参与 N为否，Y为是")
    private String gsebsProm;


    /**
     * 创建日期
     */
    private String gsebsCreateDate;

    /**
     * 创建时间
     */
    private String gsebsCreateTime;

    /**
     * 总发券数量
     */
    @Length(max = 10,message = "总发券数量最大长度为10")
    @ApiModelProperty(value = "总发券数量")
    private String gsetsMaxQty;

    /**
     * 每家门店
     */
    @Length(max = 10,message = "每家门店发券数量最大长度为10")
    @ApiModelProperty(value = "每家门店发券数量")
    private String gsebsMaxQty;

    /**
     * 备注
     */
    @Length(max = 50,message = "备注最大长度为10")
    @ApiModelProperty(value = "备注")
    private String gsebsRemark;

    /**
     * 商品组id
     */
    private String groupId;
    /**
     * NULL/0为按固定日期范围生效（现有逻辑），为1则按券有效天数生效
     */
    private String gsebsRule;
    /**
     *循环日期 按/分割
     */
    private String gsebsMonth;
    /**
     *循环星期 按/分割
     */
    private String gsebsWeek;
    /**
     *发券后有效天数
     */
    private String gsebsValidDay;

}
