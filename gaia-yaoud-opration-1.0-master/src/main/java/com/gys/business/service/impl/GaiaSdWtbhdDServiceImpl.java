package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdWtbhdDMapper;
import com.gys.business.mapper.entity.GaiaSdWtbhdD;
import com.gys.business.service.GaiaSdWtbhdDService;
import com.gys.common.exception.BusinessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/13 18:53
 **/
@Service("gaiaSdWtbhdDService")
public class GaiaSdWtbhdDServiceImpl implements GaiaSdWtbhdDService {
    @Resource
    private GaiaSdWtbhdDMapper gaiaSdWtbhdDMapper;

    @Transactional
    @Override
    public GaiaSdWtbhdD add(GaiaSdWtbhdD gaiaSdWtbhdD) {
        try {
            gaiaSdWtbhdDMapper.add(gaiaSdWtbhdD);
        } catch (Exception e) {
            throw new BusinessException("保存三方补货明细异常", e);
        }
        return gaiaSdWtbhdDMapper.selectByPrimaryKey(gaiaSdWtbhdD);
    }

    @Transactional
    @Override
    public GaiaSdWtbhdD update(GaiaSdWtbhdD gaiaSdWtbhdD) {
        try {
            gaiaSdWtbhdDMapper.update(gaiaSdWtbhdD);
        } catch (Exception e) {
            throw new BusinessException("更新三方补货明细异常", e);
        }
        return gaiaSdWtbhdDMapper.selectByPrimaryKey(gaiaSdWtbhdD);
    }
}
