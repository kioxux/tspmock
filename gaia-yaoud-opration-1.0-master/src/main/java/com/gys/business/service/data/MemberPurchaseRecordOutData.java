package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员购买记录
 *
 * @author wavesen.shen
 */
@Data
public class MemberPurchaseRecordOutData implements Serializable {
    private static final long serialVersionUID = 2808547823090861701L;

    private String index;
    /**
     * 销售日期
     */
    private String saleDate;
    /**
     * 时间
     */
    private String saleTime;
    /**
     * 商品编码
     */
    private String proCode;
    /**
     * 商品通用名
     */
    private String proCommonName;
    /**
     * 规格
     */
    private String proSpec;
    /**
     * 厂家
     */
    private String factoryName;
    /**
     * 数量
     */
    private BigDecimal num;
    /**
     * 零售价
     */
    private BigDecimal normalPrice;
    /**
     * 实收价
     */
    private BigDecimal realPrice;
    /**
     * 合计金额
     */
    private BigDecimal amount;
    /**
     * 批号
     */
    private String batchNo;
    /**
     * 有效期
     */
    private String validate;
    /**
     * 销售单号
     */
    private String billNo;
    /**
     * 门店编码
     */
    private String storeCode;
    /**
     * 门店姓名
     */
    private String storeName;
    /**
     * 整单金额
     */
    private BigDecimal billAmt;
    /**
     * 医生编码
     */
    private String doctorId;
    /**
     * 医生姓名
     */
    private String doctorName;
    /**
     * 帖数
     */
    private String dose;
    /**
     *  会员价
     */
    private String prcHy;
}
