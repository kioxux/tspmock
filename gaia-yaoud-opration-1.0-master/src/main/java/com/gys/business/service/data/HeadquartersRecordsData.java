package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaoyuan on 2020/12/24
 */
@Data
public class HeadquartersRecordsData implements Serializable {
    private static final long serialVersionUID = 9120765350465570720L;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;
    /**
     * 店号
     */
    @ApiModelProperty(value = "店号")
    private String brId;
    /**
     * 订购单号
     */
    @ApiModelProperty(value = "订单单号")
    private String voucherId;

    /**
     * 支付金额
     */
    @ApiModelProperty(value = "支付金额")
    private String gsphAmt;

    /**
     * 顾客手机
     */
    @ApiModelProperty(value = "顾客手机")
    private String costoerMobile;

    /**
     * 订购日期
     */
    @ApiModelProperty(value = "订购日期")
    private String gsphDate;

}
