
package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.service.data.*;
import com.gys.business.service.data.replenishParam.ReplenishGiftOutParam;
import com.gys.business.service.data.replenishParam.ReplenishParamInData;
import com.gys.business.service.data.replenishParam.ReplenishParamOutData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ReplenishWebService {
    PageInfo<GetReplenishOutData> selectList(GetReplenishInData inData);

    List<GetReplenishDetailOutData> detailList(GetReplenishInData inData);

    GetReplenishOutData getDetail(GetReplenishInData inData);

    Map<String,Object> insert(GetReplenishInData inData,GetLoginOutData userInfo);

    void approve(GetReplenishInData inData, GetLoginOutData userInfo);

    GetReplenishOutData getStock(GetReplenishInData inData);

    GetReplenishOutData getParam(GetReplenishInData inData);

    GetReplenishOutData getSales(GetReplenishInData inData);

    GetReplenishOutData getCost(GetReplenishInData inData);

    boolean isRepeat(GetReplenishInData inData);

    GetReplenishOutData getVoucherAmt(GetReplenishInData inData);

    String selectNextVoucherId(GaiaSdReplenishH inData);

    void deleteDetail(GetReplenishInData inData);

//    String queryNextVoucherId(GaiaSdReplenishH inData);

    GetReplenishDetailOutData replenishDetail(GetReplenishInData inData);

    String addRepPrice(RatioProInData inData);

    Map<String,Object> selectRatioStatus(RatioProInData inData);

    Map<String, Object> sortImportData(GetReplenishInData inData);

    List<GetReplenishDetailOutData> detaiAutolList(GetReplenishInData inData);

   void saveAutoPrice(GetReplenishInData inData,GetLoginOutData userInfo);

    Map<String, Object> detailAutoProList(GetReplenishInData inData);

    Map<String,String> isPriceRatio(GetLoginOutData userInfo);

    Map<String,String> stoAttribute(GetLoginOutData userInfo);

    /**
     * 请求前判断
     * @param userInfo
     * @return
     */
    boolean judgment(GetLoginOutData userInfo);

    Map<String, Object> detailRatioedList(GetReplenishInData inData);

    Map<String, Object> hasReplenished(GetReplenishInData inData);

    Map<String,String> showWareHouseStock(GetLoginOutData userInfo);

    void insertToRedis(GetReplenishInData inData);

    GetReplenishInData selectFromRedis(GetReplenishInData inData);

    HashMap<String,Object> listDifferentReplenish(GetReplenishInData inData, GetLoginOutData userInfo);

    HashMap<String,Object> listDifferentReplenishDetail(GetReplenishInData inData, GetLoginOutData userInfo);

    JsonResult getOutOfStockExplain(OutOfStockInData inData);

    JsonResult addStoreReplenishPlan(StoreReplenishPlanInData inData);

    JsonResult getOutOfStockHistory(OutOfStockInData inData);

    JsonResult invalidOutOfStock(InvalidStockInData inData);

    HashMap<String,Object> listOriReplenishDetail(GetReplenishInData inData, GetLoginOutData userInfo);

    JsonResult getStoreStockReport(StoreStockReportInData inData);

    JsonResult getValidProductInfo(ValidProductInData userInfo);

    Result exportData(GetReplenishInData inData, GetLoginOutData userInfo);

    JsonResult cancelReplenish(GetReplenishInData inData);

    Object replenishDetailForColdChain(GetReplenishInData inData);

    Object insertReplenishDetailForColdChain(GetReplenishInData inData);

    Object replenishDetailList(GetReplenishInData inData);

    List<ReplenishParamOutData> selectReplenishParamList(ReplenishParamInData inData);

    void batchAddReplenishParam(ReplenishParamInData inData);

    List<ReplenishGiftOutParam> selectGiftList(GetReplenishDetailInData inData);
}
