package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TakeAwayOrderDetailOutData {
    private String serial;
    private String proId;
    private String proName;
    private String proCommonName;
    private String proFactoryName;//厂家名称
    private String proSpecs;
    private String proUnit;
    private String batchNo;
    private String expireDate;
    private BigDecimal qty;
    private BigDecimal returnQty;
    private BigDecimal stockQty;
    private Integer boxNum;//包装数量
    private BigDecimal boxPrice;//包装价格
    private BigDecimal price;//单价--外卖平台上的单价
    //商品在外卖平台上的名称
    private String platformProductName;
}
