package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GaiaProductSpecialInData implements Serializable {

    /**
     * 省
     */
    @ApiModelProperty(value="省")
    private List<String> provinceList;

    /**
     * 市
     */
    @ApiModelProperty(value="市")
    private List<String> cityList;

    /**
     * 标识 1-春，2-夏，3-秋，4-冬
     */
    @ApiModelProperty(value="标识 1-春，2-夏，3-秋，4-冬")
    private String flag;

    /**
     * 状态 0-正常，1-停用
     */
    @ApiModelProperty(value="状态 0-正常，1-停用")
    private String status;

    /**
     * 查询类型 1-季节，2-品牌
     */
    @ApiModelProperty(value="查询类型 1-季节，2-品牌")
    private String proType;

    /**
     * 商品编码  药德通用编码
     */
    @ApiModelProperty(value="商品编码  药德通用编码 长字符串 逗号分割")
    private String proCodeStr;

    /**
     * 成分中类
     */
    @ApiModelProperty(value="成分中类")
    private List<String> midCodeList;

    /**
     * 成分小类
     */
    @ApiModelProperty(value="成分小类")
    private List<String> litCodeList;

    /**
     * 成分分类
     */
    @ApiModelProperty(value="成分分类")
    private List<String> compCodeList;

    /**
     * 成分分类开关  0-下拉列表 1-手动输入
     */
    @ApiModelProperty(value="成分分类开关  0-下拉列表 1-手动输入")
    private String queryType;

    /**
     * 成分分类编码
     */
    @ApiModelProperty(value="成分分类编码")
    private String compCode;
}
