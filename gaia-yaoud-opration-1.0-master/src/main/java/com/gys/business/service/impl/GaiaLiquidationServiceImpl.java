package com.gys.business.service.impl;

import com.gys.business.mapper.entity.GaiaLiquidation;
import com.gys.business.mapper.GaiaLiquidationMapper;
import com.gys.business.service.GaiaLiquidationService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.excel.ObjectUtil;
import com.gys.common.excel.StringUtil;
import com.gys.common.exception.BusinessException;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 医保清算表(GaiaLiquidation)表服务实现类
 *
 * @author makejava
 * @since 2021-12-29 15:00:29
 */
@Service("gaiaLiquidationService")
public class GaiaLiquidationServiceImpl implements GaiaLiquidationService {
    @Resource
    private GaiaLiquidationMapper gaiaLiquidationMapper;

    @Override
    public Boolean commit(GetLoginOutData loginUser, GaiaLiquidation inData) {
        Date now = new Date();
        inData.setIsDelete(0);
        inData.setCreateTime(now);
        inData.setUpdateTime(now);
        inData.setCreateUser(loginUser.getUserId());
        inData.setUpdateUser(loginUser.getUserId());
        gaiaLiquidationMapper.insert(inData);
        return true;
    }

    @Override
    public Boolean revoke(GetLoginOutData loginUser, GaiaLiquidation inData) {
        GaiaLiquidation liquidation = gaiaLiquidationMapper.getLiquidation(inData);
        if (ObjectUtil.isNotEmpty(liquidation)) {
            Date now = new Date();
            liquidation.setStatus(0);
            liquidation.setUpdateTime(now);
            liquidation.setUpdateUser(loginUser.getUserId());
            gaiaLiquidationMapper.updateStatus(liquidation);
        }
        return true;
    }

    @Override
    public List<GaiaLiquidation> getList(GetLoginOutData loginUser, GaiaLiquidation inData) {
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        return gaiaLiquidationMapper.getList(inData);
    }


}
