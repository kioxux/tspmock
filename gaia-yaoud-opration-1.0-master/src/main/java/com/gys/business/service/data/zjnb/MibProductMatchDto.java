package com.gys.business.service.data.zjnb;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author shentao
 */
@Data
public class MibProductMatchDto {
    private Integer index;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 商品自编码
     */
    private String proCode;
    /**
     * 地点
     */
    private String proSite;
    /**
     * 商品自编码
     */
    private String proSelfCode;
    /**
     * 匹配状态 0-未匹配，1-部分匹配，2-完全匹配
     */
    private String proMatchStatus;
    /**
     * 通用名称
     */
    private String proCommonname;
    /**
     * 商品描述
     */
    private String proDepict;
    /**
     * 助记码
     */
    private String proPym;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 剂型
     */
    private String proForm;

    /**
     * 细分剂型
     */
    private String proPartform;


    /**
     * 国际条形码1
     */
    private String proBarcode;

    /**
     * 国际条形码2
     */
    private String proBarcode2;

    /**
     * 批准文号分类 1-国产药品，2-进口药品，3-国产器械，4-进口器械，5-中药饮片，6-特殊化妆品，7-消毒用品，8-保健食品，9-QS商品，10-其它
     */
    private String proRegisterClass;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 处方类别 1-处方药，2-甲类OTC，3-乙类OTC，4-双跨处方
     */
    private String proPresclass;

    /**
     * 管制特殊分类 1-毒性药品，2-麻醉药品，3-一类精神药品，4-二类精神药品，5-易制毒药品（麻黄碱），6-放射性药品，7-生物制品（含胰岛素），8-兴奋剂（除胰岛素），9-第一类器械，10-第二类器械，11-第三类器械，12-其它管制
     */
    private String proControlClass;

    /**
     * 生产类别 1-辅料，2-化学药品，3-生物制品，4-中药，5-器械
     */
    private String proProduceClass;

    /**
     * 商品仓储分区 1-内服药品，2-外用药品，3-中药饮片区，4-精装中药区，5-针剂，6-二类精神药品，7-含麻药品，8-冷藏商品，9-外用非药，10-医疗器械，11-食品，12-保健食品，13-易燃商品区
     */
    private String proStorageArea;

    /**
     * 国家医保品种 0-否，1-是
     */
    private String proMedProdct;

    /**
     * 商品状态 0 可用 1不可用
     */
    private String proStatus;

    /**
     * 国家医保品种编码
     */
    private String proMedProdctcode;

    /**
     * 生产国家
     */
    private String proCountry;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 可服用天数
     */
    private String proTakeDays;

    /**
     * 是否医保
     */
    private String proIfMed;

    /**
     * 中药规格
     */
    private String proTcmSpecs;

    /**
     * 参考零售价
     */
    private BigDecimal proLsj;

    /**
     * 国家医保目录编号
     */
    private String proMedListnum;

    /**
     * 国家医保目录名称
     */
    private String proMedListname;

    /**
     * 进价
     */
    private String purchasePrice;

    /**
     * 零售价
     */
    private String normalPrice;

    /**
     * 国家零售价
     */
    private String countryPrice;

    /**
     * 剂型代码
     */
    private String medForm;

    /**
     * 剂型名称
     */
    private String medFormName;

    /**
     * 产地性质
     */
    private String placeType;

    /**
     * 产地性质名称
     */
    private String placeName;

    /**
     * 计价单位
     */
    private String priceUnit;
    /**
     * 计价单位名称
     */
    private String priceUnitName;

    /**
     * 用量单位
     */
    private String ylUnit;

    /**
     * 用法标准
     */
    private String yfBz;

    /**
     * 执行周期
     */
    private String zxzq;

    /**
     * 医保对照编码
     */
    private String contrastId;

    /**
     * 医保目录编码
     */
    private String medCateCode;
    /**
     * 医保目录名称
     */
    private String medCateName;
    /**
     * 通用码
     */
    private String commonCode;

    /**
     * 医保唯一码
     */
    private String onlyId;

    /**
     * 库存对照编码
     */
    private String medicareProId;
    /**
     * 每次用量
     */
    private String mcyl;
    /**
     * 生产企业
     */
    private String proFactoryName;
    /**
     * 医保分类
     */
    private String ybfl;
	private String proType;
  	/**
     * 转换比
     */
    private String zhb;
    /**
     * 物资编号
     */
    private String wzbm;
    /**
     * 是否上传
     */
    private String isUpload;
    /**
     * 限制用药标志 0否 1是
     */
    private String xzyybz;
}
