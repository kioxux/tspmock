package com.gys.business.service;

import com.gys.business.service.data.GrossConstructionInData;
import com.gys.business.service.data.GrossConstructionOutData;

import java.util.List;
import java.util.Map;

public interface GrossConstructionService {
    public void grossConstructionJob(String xxlData);

    List<GrossConstructionOutData> bigClassList(GrossConstructionInData inData);

    List<GrossConstructionOutData> getGrossConstruction(GrossConstructionInData inData);

    List<Map<String, Object>> getMidGross(GrossConstructionInData inData);

    List<Map<String, Object>> getTotalList(GrossConstructionInData inData);
}
