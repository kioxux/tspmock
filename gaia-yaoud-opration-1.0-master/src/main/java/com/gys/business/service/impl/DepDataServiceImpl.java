package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaAuthconfiDataMapper;
import com.gys.business.mapper.GaiaDepDataMapper;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.service.DepDataService;
import com.gys.business.service.StoreDataService;
import com.gys.business.service.data.CompadmDcOutData;
import com.gys.business.service.data.DcOutData;
import com.gys.business.service.data.GetUserStoreOutData;
import com.gys.common.data.GetLoginOutData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DepDataServiceImpl implements DepDataService {
    @Autowired
    private GaiaDepDataMapper depDataMapper;

    @Override
    public List<DcOutData> selectDepList(String client) {
        List<DcOutData> outData = depDataMapper. selectDepList(client);
        return outData;
    }
    @Override
    public List<DcOutData> selectDcAndStore(String client) {
        List<DcOutData> outData = depDataMapper. selectDcAndStore(client);
        return outData;
    }
}
