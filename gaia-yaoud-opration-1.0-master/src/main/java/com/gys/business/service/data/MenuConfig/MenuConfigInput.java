package com.gys.business.service.data.MenuConfig;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/4/6 17:32
 * @Version 1.0.0
 **/
@Data
@ApiModel(value = "菜单分配")
public class MenuConfigInput {
    @ApiModelProperty(value = "加盟商")
    private String client;
}
