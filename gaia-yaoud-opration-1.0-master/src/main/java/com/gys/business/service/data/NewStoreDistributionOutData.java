package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class NewStoreDistributionOutData implements Serializable {

    @ApiModelProperty(value = "商品自编码")
    private String proSelfCode;

    @ApiModelProperty(value = "大类编码")
    private String bigTypeCode;

    @ApiModelProperty(value = "大类名称")
    private String bigTypeName;

    @ApiModelProperty(value = "中类编码")
    private String midTypeCode;

    @ApiModelProperty(value = "中类名称")
    private String midTypeName;

    @ApiModelProperty(value = "商品分类编码")
    private String typeCode;

    @ApiModelProperty(value = "商品分类名称")
    private String typeName;

    @ApiModelProperty(value = "商品描述")
    private String desc;

    @ApiModelProperty(value = "商品规格")
    private String specs;

    @ApiModelProperty(value = "生产厂家")
    private String factoryName;

    @ApiModelProperty(value = "单位")
    private String unit;

    @ApiModelProperty(value = "门店编码")
    private String storeId;

    @ApiModelProperty(value = "门店名称")
    private String storeName;

    @ApiModelProperty(value = "门店属性编码")
    private String storeAttributeCode;

    @ApiModelProperty(value = "门店属性")
    private String storeAttribute;

    @ApiModelProperty(value = "店效级别编码")
    private String storeLevelCode;

    @ApiModelProperty(value = "店效级别")
    private String storeLevel;

    @ApiModelProperty(value = "参考成本价")
    private BigDecimal referenceCostAmt;

    @ApiModelProperty(value = "中包装")
    private BigDecimal midPackage;

    @ApiModelProperty(value = "公司店均月销量")
    private BigDecimal companyAvgSaleQty;

    @ApiModelProperty(value = "门店店均月销量")
    private BigDecimal storeAvgSaleQty;

    @ApiModelProperty(value = "仓库量")
    private BigDecimal wmsNumber;

    @ApiModelProperty(value = "本店库存")
    private BigDecimal storeStockNumber;

    @ApiModelProperty(value = "建议铺货量")
    private BigDecimal adviseNumber;

    @ApiModelProperty(value = "参考零售价")
    private BigDecimal referenceRetailPrice;
}
