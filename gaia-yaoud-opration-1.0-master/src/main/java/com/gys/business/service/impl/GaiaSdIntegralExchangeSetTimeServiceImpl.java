package com.gys.business.service.impl;

import com.gys.business.mapper.GaiaSdIntegralExchangeSetTimeMapper;
import com.gys.business.service.GaiaSdIntegralExchangeSetTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author RULAISZ
* @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_TIME(门店积分换购商品设置时间段)】的数据库操作Service实现
* @createDate 2021-11-29 09:49:26
*/
@Service
public class GaiaSdIntegralExchangeSetTimeServiceImpl implements GaiaSdIntegralExchangeSetTimeService{

    @Autowired
    private GaiaSdIntegralExchangeSetTimeMapper gaiaSdIntegralExchangeSetTimeMapper;

}
