package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:52 2021/10/20
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
public class EditDefaultRegionalInData {
    /**
     * 加盟商
     */
    private List<String> clients;

    /**
     * 三级区域编码
     */
    @NotBlank(message = "三级区域编码不能为空！")
    private String threeLevelCode;

    /**
     * 三级区域名称
     */
    @NotBlank(message = "三级区域名称不能为空！")
    private String threeLevelName;

    /**
     * 二级区域编码
     */
    @NotBlank(message = "二级区域编码不能为空！")
    private String twoLevelCode;

    /**
     * 二级区域名称
     */
    @NotBlank(message = "二级区域名称不能为空！")
    private String twoLevelName;

    /**
     * 一级区域编码
     */
    @NotBlank(message = "一级区域编码不能为空！")
    private String oneLevelCode;

    /**
     * 一级区域名称
     */
    @NotBlank(message = "一级区域名称不能为空！")
    private String oneLevelName;
}
