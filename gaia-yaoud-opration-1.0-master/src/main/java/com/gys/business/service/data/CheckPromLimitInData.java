package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CheckPromLimitInData {
    private String client;
    private String brId;
    @NotNull
    private String proId;
    @NotNull
    private String promId;
    @NotNull
    private String memberCard;
//    @NotNull
//    private String startDate;
//    @NotNull
//    private String endDate;
    @NotNull
    private String limitType;
    @NotNull
    private String promType;
}
