package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 用券查看返回实体
 *
 * @author xiaoyuan on 2020/10/29
 */
@Data
public class ElectronicAllVoucherOutData implements Serializable {
    private static final long serialVersionUID = 6442449763247638382L;

    @ApiModelProperty(value = "起始店号")
    private String brStart;

    @ApiModelProperty(value = "结束店号")
    private String brEnd;
    // 用券list 返回
    private  List<String> storeIds;

    @ApiModelProperty(value = "备注")
    private String gsebsRemark;

    @ApiModelProperty(value = "主题单号")
    private String gsetsId;


    /**
     * 业务类型   0为送券，1为用券
     */
    @ApiModelProperty(value = "业务类型   0为送券，1为用券")
    private String gsebsType;

    /**
     * 业务单号
     */
    @ApiModelProperty(value = "业务单号")
    private String gsebsId;

    /**
     * 起始日期
     */
    @ApiModelProperty(value = "起始日期")
    private String gsebsBeginDate;

    /**
     * 结束日期
     */
    @ApiModelProperty(value = "结束日期")
    private String gsebsEndDate;

    /**
     * 起始时间
     */
    @ApiModelProperty(value = "起始时间")
    private String gsebsBeginTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private String gsebsEndTime;

    /**
     * 门店编码
     */
    private String gsebsBrId;

    @ApiModelProperty(value = "是否参与促销")
    private String gsebsProm;
    /**
     * 电子券业务条件表
     */
    @ApiModelProperty(value = "电子券业务条件")
    private List<ElectronicDetailedOutData> childInVOS;
    /**
     * NULL/0为按固定日期范围生效（现有逻辑），为1则按券有效天数生效
     */
    private String gsebsRule;
    /**
     *循环日期 按/分割
     */
    private String gsebsMonth;
    /**
     *循环星期 按/分割
     */
    private String gsebsWeek;
    /**
     *发券后有效天数
     */
    private String gsebsValidDay;

    private String[] monthList;

    private String[] weekList;
}
