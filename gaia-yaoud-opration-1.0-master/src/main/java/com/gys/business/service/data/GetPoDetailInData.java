package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GetPoDetailInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "行号")
    private String indexNo;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "商品名")
    private String proName;
    @ApiModelProperty(value = "商品通用名")
    private String proCommonName;
    @ApiModelProperty(value = "商品数量")
    private String proQty;
    @ApiModelProperty(value = "生产日期")
    private String proMadeDate;
    @ApiModelProperty(value = "批号")
    private String proBatchNo;
    @ApiModelProperty(value = "有效期至")
    private String proValiedDate;
    @ApiModelProperty(value = "产地")
    private String proPlace;
    @ApiModelProperty(value = "单价")
    private String proPrice;
    @ApiModelProperty(value = "单位")
    private String proUnit;
    @ApiModelProperty(value = "税率")
    private String proRate;
    @ApiModelProperty(value = "验收结论")
    private String proKuwei;

    private String proDate;
    @ApiModelProperty(value = "发票号")
    private String txNo;
    @ApiModelProperty(value = "折扣额")
    private String remark;
    @ApiModelProperty(value = "行金额")
    private String proLineAmt;
    @ApiModelProperty(value = "规格")
    private String proSpecs;
    @ApiModelProperty(value = "生产厂家")
    private String proFactoryName;
    @ApiModelProperty(value = "税率值")
    private String gsrdInputTaxValue;
    @ApiModelProperty(value = "零售价")
    private BigDecimal gssdPrc1;
    @ApiModelProperty(value = "行号")
    private String poLineNo;

    //折扣率
    private BigDecimal discountRate;
    //折后单价
    private BigDecimal discountPrice;

    @ApiModelProperty(value = "发票价")
    private BigDecimal invoicePrice;
    @ApiModelProperty(value = "发票额")
    private BigDecimal invoiceAmount;
    @ApiModelProperty(value = "末次进价")
    private BigDecimal lastTimePoPrice;

    @ApiModelProperty(value = "贮存条件 1-常温，2-阴凉，3-冷藏条件")
    private String proStorageCondition;
}
