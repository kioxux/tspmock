package com.gys.business.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.TlSaleService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.WebService.Prescrip;
import com.gys.business.service.data.WebService.PrescripMaterial;
import com.gys.business.service.webServiceImpl.TlServiceImpl;
import com.gys.common.exception.BusinessException;
import com.gys.common.vo.BaseResponse;
import com.gys.common.webService.InterfaceVO;
import com.gys.feign.GysStoreWebService;
import com.gys.feign.WmsService;
import com.gys.util.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TlSaleServiceImpl implements TlSaleService {
    private static final Logger log = LoggerFactory.getLogger(TlSaleServiceImpl.class);
    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Resource
    GaiaSdSaleHMapper gaiaSdSaleHMapper;
    @Resource
    GaiaSdChinmedRelateDMapper gaiaSdChinmedRelateDMapper;
    @Resource
    GaiaSdChinmedRelateHMapper gaiaSdChinmedRelateHMapper;
    @Resource
    TlServiceImpl tlService;
    @Resource
    GysStoreWebService gysStoreWebService;
    @Resource
    GaiaProductRelateMapper gaiaProductRelateMapper;


    @Override
    public List<GaiaSupplierBusiness> querySupplier(String client, String brId) {
        return gaiaSupplierBusinessMapper.queryThirdSupplier(client, brId);
    }

    @Override
    public List<ZydjSaleH> queryDjSaleOrder(QueryParam inData) {
        List<ZydjSaleH> res = gaiaSdSaleHMapper.queryZyDjSaleH(inData);
        if (CollectionUtil.isNotEmpty(res)) {
            res.forEach(item -> {
                if ("已完成".equals(item.getStatus()) && !StringUtils.isEmpty(item.getPid())) {
                    InterfaceVO inVo = tlGetStatus(inData.getUniqueId(), item.getPid());
                    if (inVo.isSuccess()) {
                        String status = "";
                        switch (inVo.getResult()) { //D 未处理/O 已确认/X 作废/C 完成
                            case "D":
                                status = "未处理";
                                break;
                            case "O":
                                status = "已确认";
                                break;
                            case "X":
                                status = "作废";
                                break;
                            case "C":
                                status = "完成";
                                break;
                        }
                        item.setStatus2(status);
                    }
                }
            });
        }
        return res;
//        Example example = new Example(GaiaSdSaleH.class);
//        example.createCriteria().andEqualTo("clientId",inData.getClientId()).andEqualTo("gsshBrId",inData.getBrId()).andIsNotNull("gsshCrFlag").andBetween("gsshDate",inData.getStartDate(),inData.getEndDate());
//        return gaiaSdSaleHMapper.selectByExample(example);
    }

    @Override
    public List<GetSaleReturnDetailOutData> queryDjSaleOrderDetail(QueryParam inData) {
        return gaiaSdSaleDMapper.queryDjSaleOrderDetail(inData);
    }

    @Override
    @Transactional
    public InterfaceVO tlSaveAndPush(String client, String brId, Prescrip inData) {
        InterfaceVO res = new InterfaceVO();
        if (StringUtils.isEmpty(inData.getUniqueId())) {
            res.failed();
            res.setMessage("唯一Id为空，请登录后再试！");
            return res;
        }
        //检查此销售单的状态 是否未开方
        Example example = new Example(GaiaSdSaleH.class);
        example.createCriteria().andEqualTo("clientId", client).andEqualTo("gsshBrId", brId).andEqualTo("gsshBillNo", inData.getPid());
        GaiaSdSaleH saleH = gaiaSdSaleHMapper.selectOneByExample(example);
        if (StringUtils.isEmpty(saleH.getGsshCrFlag())) {
            res.failed();
            res.setMessage("此销售单不是中药代煎订单！");
            return res;
        }
        if (!"0".equals(saleH.getGsshCrStatus())) {
            res.failed();
            res.setMessage("销售单状态错误，请选择未开方的记录！");
            return res;
        }
        //查询商品明细
        List<PrescripMaterial> materialList = gaiaSdSaleDMapper.queryMaterialFromSaleHByBillNo(client, brId, inData.getPid());
        if (CollectionUtil.isNotEmpty(materialList)) {
            //天灵单头表 插入
            GaiaSdChinmedRelateH gaiaSdChinmedRelateH = new GaiaSdChinmedRelateH();
            BeanUtils.copyProperties(inData, gaiaSdChinmedRelateH);
            gaiaSdChinmedRelateH.setClient(client);
            gaiaSdChinmedRelateH.setBrId(brId);
            gaiaSdChinmedRelateH.setSaleId(inData.getPid());
            gaiaSdChinmedRelateH.setDate(gaiaSdChinmedRelateH.getDate().replace("-", ""));
            gaiaSdChinmedRelateHMapper.insert(gaiaSdChinmedRelateH);
            List<GaiaSdChinmedRelateD> gaiaSdChinmedRelateDList = new ArrayList<>();
            for (int i = 0; i < materialList.size(); i++) {
                GaiaSdChinmedRelateD temp = new GaiaSdChinmedRelateD();
                BeanUtils.copyProperties(materialList.get(i), temp);
                temp.setClient(client);
                temp.setBrId(brId);
                temp.setPid(gaiaSdChinmedRelateH.getPid());
                temp.setSerial(String.valueOf(i + 1));
                //数量为kg
                if ("克".equals(temp.getUnit())) {
                    materialList.get(i).setQuantity(new BigDecimal(temp.getQuantity()).divide(new BigDecimal(gaiaSdChinmedRelateH.getQuantity()), 8, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal("1000"), 4, BigDecimal.ROUND_HALF_UP).toString());
                    temp.setQuantity(new BigDecimal(temp.getQuantity()).divide(new BigDecimal(gaiaSdChinmedRelateH.getQuantity()), 8, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal("1000"), 4, BigDecimal.ROUND_HALF_UP).toString());
                } else {
                    materialList.get(i).setQuantity(new BigDecimal(temp.getQuantity()).divide(new BigDecimal(gaiaSdChinmedRelateH.getQuantity()), 8, BigDecimal.ROUND_HALF_UP).toString());
                    temp.setQuantity(new BigDecimal(temp.getQuantity()).divide(new BigDecimal(gaiaSdChinmedRelateH.getQuantity()), 8, BigDecimal.ROUND_HALF_UP).toString());
                }
                gaiaSdChinmedRelateDList.add(temp);
            }
            inData.setMaterials(materialList);
            //天灵明细表 插入
            gaiaSdChinmedRelateDMapper.insertBatch(gaiaSdChinmedRelateDList);

            //更新销售单 天灵销售状态 1 已完成
            saleH.setGsshCrStatus("1");
            gaiaSdSaleHMapper.updateByExampleSelective(saleH, example);

            //组装 补单接口参数
            example = new Example(GaiaSdSaleD.class);
            example.createCriteria().andEqualTo("clientId", client).andEqualTo("gssdBrId", brId).andEqualTo("gssdBillNo", inData.getPid());
            List<GaiaSdSaleD> saleDList = gaiaSdSaleDMapper.selectByExample(example);
            List<SaleProxyReplenishmentsDetailInputData> saleList = new ArrayList<>();
            Calendar calendar = new GregorianCalendar();
            for (GaiaSdSaleD item : saleDList) {
                SaleProxyReplenishmentsDetailInputData temp = new SaleProxyReplenishmentsDetailInputData();
                temp.setBatchNo(item.getGssdBatchNo());
                temp.setCommodityCode(item.getGssdProId());
                Date date = CommonUtil.strToDate(item.getGssdDate());
                calendar.setTime(date);
                calendar.add(calendar.DATE, 10);//把日期往后增加一天.整数往后推,负数往前移动
                temp.setExpiryDate(CommonUtil.dateToStr(calendar.getTime()));
                GaiaProductRelate productRelate = gaiaProductRelateMapper.queryProductPrice(client, brId, item.getGssdProId(), saleH.getGsshCrFlag());
                temp.setCostPrice(new BigDecimal(productRelate.getProPrice()));
                temp.setQuantity(new BigDecimal(item.getGssdQty()));
                saleList.add(temp);
            }

            SaleProxyReplenishmentsInputData saleProxyReplenishmentsInputData = new SaleProxyReplenishmentsInputData();
            saleProxyReplenishmentsInputData.setClient(saleH.getClientId());
            saleProxyReplenishmentsInputData.setStoreCode(saleH.getGsshBrId());
            saleProxyReplenishmentsInputData.setSaleNo(saleH.getGsshBillNo());
            saleProxyReplenishmentsInputData.setSupplier(saleH.getGsshCrFlag());
            saleProxyReplenishmentsInputData.setUserId(saleH.getGsshEmp());
            saleProxyReplenishmentsInputData.setCommodityList(saleList);

            //补单接口调用
            String implRes = gysStoreWebService.saleProxyReplenishments(saleProxyReplenishmentsInputData);
            log.info("gysStoreWebService.saleProxyReplenishments:" + saleProxyReplenishmentsInputData + ";res:" + implRes);
            BaseResponse resultObj = JSONObject.parseObject(implRes, BaseResponse.class);
            int code = resultObj.getCode();
            if (code != 200) {
                throw new BusinessException("中药代煎补单接口服务异常");
            } else {
                res.success();
            }

//            天灵接口调用
            InterfaceVO serviceRes = tlService.create(inData.getUniqueId(), inData);
            if (serviceRes.isSuccess()) {
                return res;
            } else {
                throw new BusinessException("天灵接口调用失败");
            }
        } else {
            res.failed();
            res.setMessage("销售单明细为空，不可上传！");
            return res;
        }
    }

    @Override
    public InterfaceVO tlLogin(LoginParam loginParam) {
        return tlService.getUniqueID(loginParam.getUserName(), loginParam.getPassword());
    }

    @Override
    public InterfaceVO tlGetStatus(String uniqueId, String pid) {
        return tlService.getStatus(uniqueId, pid);
    }

    @Override
    public GaiaSdChinmedRelateH queryChinmedRelateDByBillNo(QueryParam inData) {
        Example example = new Example(GaiaSdChinmedRelateH.class);
        example.createCriteria().andEqualTo("client", inData.getClientId()).andEqualTo("brId", inData.getBrId()).andEqualTo("saleId", inData.getOrderNO());
        return gaiaSdChinmedRelateHMapper.selectOneByExample(example);
    }
}
