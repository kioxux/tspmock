package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "本地设置返回实体")
public class LocalSettingOutData extends BaseVO implements Serializable {
    @ApiModelProperty(value = "店号")
    private String gsstBrId;

    @ApiModelProperty(value = "店名")
    private String gsstBrName;

    @ApiModelProperty(value = "每单销售最大数量")
    private String gsstSaleMaxQty;

    @ApiModelProperty(value = "")
    private String gsstLastDay;

    @ApiModelProperty(value = "全场盘点开始日期")
    private String gsstPdStartDate;

    @ApiModelProperty(value = "")
    private String gsstVersion;

    @ApiModelProperty(value = "最低折率设置")
    private String gsstMinDiscountRate;

    @ApiModelProperty(value = "抵用券最大使用金额")
    private BigDecimal gsstDyqMaxAmt;

    @ApiModelProperty(value = "开店时间")
    private String gsstDailyOpenDate;

    @ApiModelProperty(value = "交班时间")
    private String gsstDailyChangeDate;

    @ApiModelProperty(value = "闭店时间")
    private String gsstDailyCloseDate;

    @ApiModelProperty(value = "")
    private String gsstJfQldate;

    @ApiModelProperty(value = "")
    private String gsstPsDate;

    private static final long serialVersionUID = -8274059234243274401L;
}
