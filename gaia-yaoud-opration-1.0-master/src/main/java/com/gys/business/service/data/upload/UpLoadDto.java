package com.gys.business.service.data.upload;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: tzh
 * @Date: 2021/12/29 17:59
 * @Description: upLoadDto
 * @Version 1.0.0
 */
@Data
public class UpLoadDto {
    /**
     * 上传路径
     */
    private String path;
    /**
     * 文件
     */
    private MultipartFile file;
    /**
     * 来源
     */
    private String source;
}
