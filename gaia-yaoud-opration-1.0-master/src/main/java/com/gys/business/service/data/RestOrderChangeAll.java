package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RestOrderChangeAll implements Serializable {
    private static final long serialVersionUID = -5099011621729701264L;

    private List<RestOrderChangeDto> dtoList;

    private List<RestOrderDetailDto> detailDtos;
}
