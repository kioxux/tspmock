//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class GetSyncSdStockBatchOutData {
    private String clientId;
    private String gssbBrId;
    private String gssbProId;
    private String gssbBatchNo;
    private String gssbBatch;
    private String gssbQty;
    private String gssbVaildDate;
    private String gssbUpdateDate;
    private String gssbUpdateEmp;

    public GetSyncSdStockBatchOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGssbBrId() {
        return this.gssbBrId;
    }

    public String getGssbProId() {
        return this.gssbProId;
    }

    public String getGssbBatchNo() {
        return this.gssbBatchNo;
    }

    public String getGssbBatch() {
        return this.gssbBatch;
    }

    public String getGssbQty() {
        return this.gssbQty;
    }

    public String getGssbVaildDate() {
        return this.gssbVaildDate;
    }

    public String getGssbUpdateDate() {
        return this.gssbUpdateDate;
    }

    public String getGssbUpdateEmp() {
        return this.gssbUpdateEmp;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGssbBrId(final String gssbBrId) {
        this.gssbBrId = gssbBrId;
    }

    public void setGssbProId(final String gssbProId) {
        this.gssbProId = gssbProId;
    }

    public void setGssbBatchNo(final String gssbBatchNo) {
        this.gssbBatchNo = gssbBatchNo;
    }

    public void setGssbBatch(final String gssbBatch) {
        this.gssbBatch = gssbBatch;
    }

    public void setGssbQty(final String gssbQty) {
        this.gssbQty = gssbQty;
    }

    public void setGssbVaildDate(final String gssbVaildDate) {
        this.gssbVaildDate = gssbVaildDate;
    }

    public void setGssbUpdateDate(final String gssbUpdateDate) {
        this.gssbUpdateDate = gssbUpdateDate;
    }

    public void setGssbUpdateEmp(final String gssbUpdateEmp) {
        this.gssbUpdateEmp = gssbUpdateEmp;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetSyncSdStockBatchOutData)) {
            return false;
        } else {
            GetSyncSdStockBatchOutData other = (GetSyncSdStockBatchOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label119: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label119;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label119;
                    }

                    return false;
                }

                Object this$gssbBrId = this.getGssbBrId();
                Object other$gssbBrId = other.getGssbBrId();
                if (this$gssbBrId == null) {
                    if (other$gssbBrId != null) {
                        return false;
                    }
                } else if (!this$gssbBrId.equals(other$gssbBrId)) {
                    return false;
                }

                label105: {
                    Object this$gssbProId = this.getGssbProId();
                    Object other$gssbProId = other.getGssbProId();
                    if (this$gssbProId == null) {
                        if (other$gssbProId == null) {
                            break label105;
                        }
                    } else if (this$gssbProId.equals(other$gssbProId)) {
                        break label105;
                    }

                    return false;
                }

                Object this$gssbBatchNo = this.getGssbBatchNo();
                Object other$gssbBatchNo = other.getGssbBatchNo();
                if (this$gssbBatchNo == null) {
                    if (other$gssbBatchNo != null) {
                        return false;
                    }
                } else if (!this$gssbBatchNo.equals(other$gssbBatchNo)) {
                    return false;
                }

                label91: {
                    Object this$gssbBatch = this.getGssbBatch();
                    Object other$gssbBatch = other.getGssbBatch();
                    if (this$gssbBatch == null) {
                        if (other$gssbBatch == null) {
                            break label91;
                        }
                    } else if (this$gssbBatch.equals(other$gssbBatch)) {
                        break label91;
                    }

                    return false;
                }

                Object this$gssbQty = this.getGssbQty();
                Object other$gssbQty = other.getGssbQty();
                if (this$gssbQty == null) {
                    if (other$gssbQty != null) {
                        return false;
                    }
                } else if (!this$gssbQty.equals(other$gssbQty)) {
                    return false;
                }

                label77: {
                    Object this$gssbVaildDate = this.getGssbVaildDate();
                    Object other$gssbVaildDate = other.getGssbVaildDate();
                    if (this$gssbVaildDate == null) {
                        if (other$gssbVaildDate == null) {
                            break label77;
                        }
                    } else if (this$gssbVaildDate.equals(other$gssbVaildDate)) {
                        break label77;
                    }

                    return false;
                }

                label70: {
                    Object this$gssbUpdateDate = this.getGssbUpdateDate();
                    Object other$gssbUpdateDate = other.getGssbUpdateDate();
                    if (this$gssbUpdateDate == null) {
                        if (other$gssbUpdateDate == null) {
                            break label70;
                        }
                    } else if (this$gssbUpdateDate.equals(other$gssbUpdateDate)) {
                        break label70;
                    }

                    return false;
                }

                Object this$gssbUpdateEmp = this.getGssbUpdateEmp();
                Object other$gssbUpdateEmp = other.getGssbUpdateEmp();
                if (this$gssbUpdateEmp == null) {
                    if (other$gssbUpdateEmp != null) {
                        return false;
                    }
                } else if (!this$gssbUpdateEmp.equals(other$gssbUpdateEmp)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetSyncSdStockBatchOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gssbBrId = this.getGssbBrId();
        result = result * 59 + ($gssbBrId == null ? 43 : $gssbBrId.hashCode());
        Object $gssbProId = this.getGssbProId();
        result = result * 59 + ($gssbProId == null ? 43 : $gssbProId.hashCode());
        Object $gssbBatchNo = this.getGssbBatchNo();
        result = result * 59 + ($gssbBatchNo == null ? 43 : $gssbBatchNo.hashCode());
        Object $gssbBatch = this.getGssbBatch();
        result = result * 59 + ($gssbBatch == null ? 43 : $gssbBatch.hashCode());
        Object $gssbQty = this.getGssbQty();
        result = result * 59 + ($gssbQty == null ? 43 : $gssbQty.hashCode());
        Object $gssbVaildDate = this.getGssbVaildDate();
        result = result * 59 + ($gssbVaildDate == null ? 43 : $gssbVaildDate.hashCode());
        Object $gssbUpdateDate = this.getGssbUpdateDate();
        result = result * 59 + ($gssbUpdateDate == null ? 43 : $gssbUpdateDate.hashCode());
        Object $gssbUpdateEmp = this.getGssbUpdateEmp();
        result = result * 59 + ($gssbUpdateEmp == null ? 43 : $gssbUpdateEmp.hashCode());
        return result;
    }

    public String toString() {
        return "GetSyncSdStockBatchOutData(clientId=" + this.getClientId() + ", gssbBrId=" + this.getGssbBrId() + ", gssbProId=" + this.getGssbProId() + ", gssbBatchNo=" + this.getGssbBatchNo() + ", gssbBatch=" + this.getGssbBatch() + ", gssbQty=" + this.getGssbQty() + ", gssbVaildDate=" + this.getGssbVaildDate() + ", gssbUpdateDate=" + this.getGssbUpdateDate() + ", gssbUpdateEmp=" + this.getGssbUpdateEmp() + ")";
    }
}
