package com.gys.business.service.data;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class DsfPordWtproInData implements Serializable {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 商品名称/商品通用名proSelfCode
     */
    private String proName;
    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     *商品编码
     */
    private List<String> proSelfCode;

    /**
     * 对码状态 0-未对码，1-已对码，无值，全部
     */
    private String state;

   @NotNull(message = "请输入当前页号")
    private Integer pageNum;

   @NotNull(message = "请输入一页数量")
    private Integer pageSize;

    /**
     * 第三方商品编号
     */
    private String idDsf;
}
