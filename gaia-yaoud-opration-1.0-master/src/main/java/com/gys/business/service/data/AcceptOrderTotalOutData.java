package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AcceptOrderTotalOutData {
    @ApiModelProperty("总金额")
    private String totalSumAmt;
    @ApiModelProperty("总数量")
    private String recipientQty;
    @ApiModelProperty("行金额汇总")
    private String voucherAmt;
}
