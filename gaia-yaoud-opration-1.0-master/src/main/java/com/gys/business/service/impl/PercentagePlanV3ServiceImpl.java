package com.gys.business.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gys.business.mapper.PercentagePlanV2Mapper;
import com.gys.business.mapper.PercentagePlanV3Mapper;
import com.gys.business.mapper.entity.GaiaTichengProPlan;
import com.gys.business.mapper.entity.GaiaTichengProplanZN;
import com.gys.business.mapper.entity.GaiaTichengSalePlan;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.service.ImportPercentageData;
import com.gys.business.service.PercentagePlanV3Service;
import com.gys.business.service.StoreDataService;
import com.gys.business.service.data.MessageParams;
import com.gys.business.service.data.percentageplan.*;
import com.gys.common.exception.BusinessException;
import com.gys.common.vo.BaseResponse;
import com.gys.feign.OperateService;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Service
@Slf4j
public class PercentagePlanV3ServiceImpl implements PercentagePlanV3Service {
    @Resource
    private PercentagePlanV3Mapper planMapper;

    @Autowired
    private StoreDataService storeDataService;

    @Resource
    private OperateService operateService;

    @Override
    @Transactional
    public Integer insert(PercentageInData inData) {
        Integer id = 0;
        if(ObjectUtil.isNotEmpty(inData.getPlanSuitMonth())){
            inData.setPlanSuitMonth(inData.getPlanSuitMonth().substring(0,6));
        }
        if (ObjectUtil.isEmpty(inData.getPlanName())){
            throw new BusinessException("方案名称不能为空");
        }
        if (ObjectUtil.isEmpty(inData.getDateArr()[0])){
            throw new BusinessException("起始日期不能为空");
        }
        if (ObjectUtil.isEmpty(inData.getDateArr()[1])){
            throw new BusinessException("结束日期不能为空");
        }
        if (inData.getStoreCodes().length <= 0){
            throw new BusinessException("适用门店必选");
        }
        inData.setPlanStartDate(inData.getDateArr()[0]);
        inData.setPlanEndDate(inData.getDateArr()[1]);
        if ("1".equals(inData.getPlanType())){
            //销售提成维护
            id = setSalePlan(inData);
        }else if("2".equals(inData.getPlanType())){
            id = setProPlan(inData);
        }
        return id;
    }

    @Override
    public PageInfo<PercentageOutData> list(PercentageInData inData) {
        if(ObjectUtil.isEmpty(inData.getPlanDate())){
            inData.setPlanStartDate("");
            inData.setPlanEndDate("");
        }
        PageHelper.startPage(inData.getPageNum(), inData.getPageSize());
        List<PercentageOutData> list = planMapper.selectTichengZList(inData);
        PageInfo pageInfo;
        if (ObjectUtil.isNotEmpty(list)) {
            pageInfo = new PageInfo(list);
        } else {
            pageInfo = new PageInfo();
        }
        return pageInfo;
    }

    @Override
    public PercentageInData tichengDetail(Long planId, Long type) {
        PercentageInData outData = new PercentageInData();
        if (type.intValue() == 1) {
            outData = planMapper.selectTichengZSaleDetail(planId);
            String[] dateArr = new String[2];
            dateArr[0] = outData.getPlanStartDate();
            dateArr[1] = outData.getPlanEndDate();
            outData.setDateArr(dateArr);
            List<Map<String,String>> stoList = planMapper.selectTichengSaleStoList(planId);
            //适用门店
            if (stoList.size() > 0 && stoList != null){
                String[] array = new String[stoList.size()];
                for(int i = 0; i < stoList.size();i++){
                    array[i] = stoList.get(i).get("stoCode");
                }
                outData.setStoreCodes(array);
            }
            //销售提成明细
            List<PercentageSaleInData> saleList = planMapper.selectTichengSaleList(planId);
            if (saleList.size() > 0 && saleList != null) {
                outData.setSaleInDataList(saleList);
            }
            //剔除商品分类
            List<Map<String,String>> rejectClassList = planMapper.selectRejectClass(planId);
            if (ObjectUtil.isNotEmpty(rejectClassList) && rejectClassList.size() > 0){
                String[][] rejectArr = new String[rejectClassList.size()][3];
                for (int i = 0;i < rejectClassList.size(); i ++) {
                    String[] arr = new String[3];
                    arr[0] = rejectClassList.get(i).get("proBigClass");
                    arr[1] = rejectClassList.get(i).get("proMidClass");
                    arr[2] = rejectClassList.get(i).get("proClass");
                    rejectArr[i] = arr;
                }
                outData.setClassArr(rejectArr);
            }
            List<PercentageProInData> rejectProList = planMapper.selectRejectProList(planId);
            if (ObjectUtil.isNotEmpty(rejectProList) && rejectProList.size() > 0){
                outData.setRejectProList(rejectProList);
            }
        }else if(type.intValue() == 2) {
            outData = planMapper.selectTichengZProDetail(planId);
            String[] dateArr = new String[2];
            dateArr[0] = outData.getPlanStartDate();
            dateArr[1] = outData.getPlanEndDate();
            outData.setDateArr(dateArr);
            List<Map<String,String>> stoList = planMapper.selectTichengProStoList(planId);
            //适用门店
            if (stoList.size() > 0 && stoList != null){
                String[] array = new String[stoList.size()];
                for(int i = 0; i < stoList.size();i++){
                    array[i] = stoList.get(i).get("stoCode");
                }
                outData.setStoreCodes(array);
            }
            List<PercentageProInData> proList = planMapper.selectTichengProList(planId);
            if (proList.size() > 0 && proList != null) {
                outData.setProInDataList(proList);
            }
        }
        return outData;
    }

    @Override
    public void approve(Long planId, String planStatus, String type) {
        //提成方案审核
        if ("1".equals(type)) {
            PercentageInData saleInfo = planMapper.selectTichengZSaleDetail(planId);
            if ("0".equals(saleInfo.getPlanStatus())) {
                planMapper.approveSalePlan(planId, planStatus,"审核已通过");
                saleInfo.setPlanStatus(planStatus);
                saleInfo.setPlanReason("审核已通过");
            }else {
                planMapper.approveSalePlan(planId, planStatus,"");
                saleInfo.setPlanStatus(planStatus);
            }
            //消息推送到APP
            sendMessage(saleInfo);
        }else if("2".equals(type)){
            PercentageInData proInfo = planMapper.selectTichengZProDetail(planId);
            if ("0".equals(proInfo.getPlanStatus())) {
                planMapper.approveProPlan(planId, planStatus, "审核已通过");
                proInfo.setPlanStatus(planStatus);
                proInfo.setPlanReason("审核已通过");
            }else{
                planMapper.approveProPlan(planId, planStatus, "");
                proInfo.setPlanStatus(planStatus);
            }
            //消息推送到APP
            sendMessage(proInfo);
        }
    }

    private void sendMessage(PercentageInData planInfo) {
        if ("1".equals(planInfo.getPlanType())){
            List<Map<String,String>> result = planMapper.selectSaleStoList(planInfo.getId().longValue());
            String[] stoCodeArr = new String[result.size()];
            for (int i = 0; i < result.size();i++) {
                stoCodeArr[i] = result.get(i).get("stoCode");
            }
            planInfo.setStoreCodes(stoCodeArr);
            List<String> userIds = planMapper.selectUserIdList(planInfo);
            MessageParams messageParams = new MessageParams();
            messageParams.setClient(planInfo.getClient());
            messageParams.setUserIdList(userIds);
            messageParams.setContentParmas(new ArrayList<String>() {{
                add(planInfo.getPlanName());add(planInfo.getPlanReason());
            }});
            messageParams.setId("MSG00009");
            log.info("销售提成消息参数:{}", messageParams.toString());
            String message = operateService.sendMessageList(messageParams);
            log.info("培训推送消息结果:{}", message);
        }else {
            List<Map<String,String>> result = planMapper.selectProStoList(planInfo.getId().longValue());
            String[] stoCodeArr = new String[result.size()];
            for (int i = 0; i < result.size();i++) {
                stoCodeArr[i] = result.get(i).get("stoCode");
            }
            planInfo.setStoreCodes(stoCodeArr);
            List<String> userIds = planMapper.selectUserIdList(planInfo);
            MessageParams messageParams = new MessageParams();
            messageParams.setClient(planInfo.getClient());
            messageParams.setUserIdList(userIds);
            messageParams.setContentParmas(new ArrayList<String>() {{
                add(planInfo.getPlanName());add(planInfo.getPlanReason());
            }});
            messageParams.setId("MSG00010");
            log.info("销售提成消息参数:{}", messageParams.toString());
            String message = operateService.sendMessageList(messageParams);
            log.info("培训推送消息结果:{}", message);
        }
    }

    @Override
    public void deletePlan(Long planId, String type) {
        if ("1".equals(type)) {
            planMapper.deleteSalePlan(planId);
        }else if("2".equals(type)){
            planMapper.deleteProPlan(planId);
        }
    }

    @Override
    public PercentageProInData selectProductByClient(String client, String proCode) {
        PercentageProInData proOutData = planMapper.selectProductByClient(client,proCode);
        List<StoreOutData> storeOutDataList = storeDataService.getStoreList(client,null);
        for (StoreOutData outData : storeOutDataList) {
            if (ObjectUtil.isNotEmpty(outData.getStoAttribute())){
                if ("2".equals(outData.getStoAttribute())){
                    String movPrice = planMapper.selectMovPriceByDcCode(client,outData.getStoDcCode(),proCode);
                    if (ObjectUtil.isNotEmpty(movPrice)) {
                        proOutData.setProCostPrice(new BigDecimal(movPrice));
                        proOutData.setGrossProfitAmt(proOutData.getProPrice().subtract(proOutData.getProCostPrice()));
                        if (proOutData.getProPrice().compareTo(BigDecimal.ZERO) == 0){
                            proOutData.setGrossProfitRate("0.00%");
                        }else {
                            proOutData.setGrossProfitRate(proOutData.getGrossProfitAmt().divide(proOutData.getProPrice(),2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100"))  + "%");
                        }
                    }
                    break;
                }
            }
        }
        return proOutData;
    }

    @Override
    public Map<String,PercentageProInData> selectProductByClientProCodes(String client, List<String> proCodes) {
        Map<String,PercentageProInData> resMap = new HashMap<>();
//        List<PercentageProInData> resList = new ArrayList<>();
        List<PercentageProInData> proOutDatas = planMapper.selectProductByClientProCodes(client,proCodes);
        List<StoreOutData> storeOutDataList = storeDataService.getStoreList(client,null);
        Map<String,String> proMovePriceMap = new HashMap<>();
        for (StoreOutData outData : storeOutDataList) {
            if (ObjectUtil.isNotEmpty(outData.getStoAttribute())){
                if ("2".equals(outData.getStoAttribute())){
                    List<Map<String,String>> resMapList = planMapper.selectMovPriceByDcCodeProCodes(client,outData.getStoDcCode(),proCodes);
                    if(CollectionUtil.isNotEmpty(resMapList)){
                        resMapList.forEach(x->{
                            String proCode = x.get("proCode");
                            String movePrice = x.get("movePrice");
                            proMovePriceMap.put(proCode,movePrice);
                        });
                    }
                }
            }
        }

        if(CollectionUtil.isNotEmpty(proOutDatas)){
            for (PercentageProInData proOutData : proOutDatas) {
                proOutData.setProCostPrice(new BigDecimal(proMovePriceMap.get(proOutData.getProCode())));
                proOutData.setGrossProfitAmt(proOutData.getProPrice().subtract(proOutData.getProCostPrice()));
                if (proOutData.getProPrice().compareTo(BigDecimal.ZERO) == 0){
                    proOutData.setGrossProfitRate("0.00%");
                }else {
                    proOutData.setGrossProfitRate(proOutData.getGrossProfitAmt().divide(proOutData.getProPrice(),2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100"))  + "%");
                }
                resMap.put(proOutData.getProCode(),proOutData);
//                resList.add(proOutData);
            }
        }
        return resMap;
    }

    @Override
    public List<PercentageProInData> getImportExcelDetailList(String clientId, List<ImportPercentageData> importInDataList) {
        ImportProInData inData = new ImportProInData();
        inData.setClientId(clientId);
        List<String> proCodes = new ArrayList<>();
        List<PercentageProInData> resultList = new ArrayList<>();
        for (ImportPercentageData data: importInDataList) {
            String proCode = data.getProCode();
            if (ObjectUtil.isNotEmpty(proCode)){
                proCodes.add(proCode);
            }
        }
        if (proCodes.size() > 0 && proCodes != null){
            inData.setProCodes(proCodes);
            resultList = planMapper.selectProductByProCodes(inData);
            if (resultList.size() > 0 && resultList != null) {
                for (ImportPercentageData data: importInDataList) {
                    String proCode = data.getProCode();
                    boolean f = true;
                    for (PercentageProInData result : resultList) {
                        if (result.getProCode().equals(proCode)){
                            f = false;
                            break;
                        }
                    }
                    if (f){
                        throw new BusinessException("商品编码:"+proCode + "信息不存在，请核实");
                    }
                }
                List<StoreOutData> storeOutDataList = storeDataService.getStoreList(clientId,null);
                for (StoreOutData outData : storeOutDataList) {
                    if (ObjectUtil.isNotEmpty(outData.getStoAttribute())) {
                        if ("2".equals(outData.getStoAttribute())) {
                            inData.setDcCode(outData.getStoDcCode());
                            List<Map<String, String>> movPriceList = planMapper.selectMovPriceListByDcCode(inData);
                            if (movPriceList.size() > 0 && movPriceList != null) {
                                for (Map<String, String> movPriceMap : movPriceList) {
                                    String proCode = movPriceMap.get("proCode");
                                    String movPrice = "0";
                                    if (movPriceMap.containsKey("movPrice")) {
                                        movPrice = movPriceMap.get("movPrice");
                                    }
                                    for (PercentageProInData result : resultList) {
                                        if (result.getProCode().equals(proCode)) {
                                            result.setProCostPrice(new BigDecimal(movPrice));
                                            result.setGrossProfitAmt(result.getProPrice().subtract(result.getProCostPrice()));
                                            if (result.getProPrice().compareTo(BigDecimal.ZERO) == 0) {
                                                result.setGrossProfitRate("0.00%");
                                            } else {
                                                result.setGrossProfitRate(result.getGrossProfitAmt().divide(result.getProPrice(), 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")) + "%");
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }else{
                throw new BusinessException("未查询到对应的商品信息");
            }
            for (PercentageProInData result : resultList) {
                for (ImportPercentageData data: importInDataList) {
                    String proCode = data.getProCode();
                    if (result.getProCode().equals(proCode)){
                        if (StringUtils.isNotEmpty(data.getSeleQty())){
                            result.setSaleQty(data.getSeleQty());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengAmt())){
                            result.setTichengAmt(data.getTichengAmt());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengRate())){
                            result.setTichengRate(data.getTichengRate());
                        }
                        if (StringUtils.isNotEmpty(data.getSeleQty2())){
                            result.setSaleQty2(data.getSeleQty2());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengAmt2())){
                            result.setTichengAmt2(data.getTichengAmt2());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengRate2())){
                            result.setTichengRate2(data.getTichengRate2());
                        }
                        if (StringUtils.isNotEmpty(data.getSeleQty3())){
                            result.setSaleQty3(data.getSeleQty3());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengAmt3())){
                            result.setTichengAmt3(data.getTichengAmt3());
                        }
                        if (StringUtils.isNotEmpty(data.getTichengRate3())){
                            result.setTichengRate3(data.getTichengRate3());
                        }
                    }
                }
            }
        }
        return resultList;
    }

    @Override
    public PercentageInData tichengDetailCopy(Long planId,Long type) {
        PercentageInData outData = new PercentageInData();
        if (type.intValue() == 1) {
            outData = planMapper.selectTichengZSaleDetail(planId);
            String[] dateArr = new String[2];
            dateArr[0] = outData.getPlanStartDate();
            dateArr[1] = outData.getPlanEndDate();
            outData.setDateArr(dateArr);
            List<Map<String,String>> stoList = planMapper.selectTichengSaleStoList(planId);
            //适用门店
            if ( ObjectUtil.isNotEmpty(stoList) && stoList.size() > 0){
                String[] array = new String[stoList.size()];
                for(int i = 0; i < stoList.size();i++){
                    array[i] = stoList.get(i).get("stoCode");
                }
                outData.setStoreCodes(array);
            }
            //销售提成明细
            List<PercentageSaleInData> saleList = planMapper.selectTichengSaleList(planId);
            if ( ObjectUtil.isNotEmpty(saleList) && saleList.size() > 0) {
                for (PercentageSaleInData sale : saleList) {
                    sale.setId(null);
                    sale.setPid(null);
//                    sale.setClientId(null);
                }
                outData.setSaleInDataList(saleList);
            }
            //剔除商品分类
            List<Map<String,String>> rejectClassList = planMapper.selectRejectClass(planId);
            if (ObjectUtil.isNotEmpty(rejectClassList) && rejectClassList.size() > 0){
                String[][] rejectArr = new String[rejectClassList.size()][3];
                for (int i = 0;i < rejectClassList.size(); i ++) {
                    String[] arr = new String[3];
                    arr[0] = rejectClassList.get(i).get("proBigClass");
                    arr[1] = rejectClassList.get(i).get("proMidClass");
                    arr[2] = rejectClassList.get(i).get("proClass");
                    rejectArr[i] = arr;
                }
                outData.setClassArr(rejectArr);
            }
            List<PercentageProInData> rejectProList = planMapper.selectRejectProList(planId);
            if (ObjectUtil.isNotEmpty(rejectProList) && rejectProList.size() > 0){
                for (PercentageProInData pro : rejectProList) {
                    pro.setId(null);
                    pro.setPid(null);
//                    sale.setClientId(null);
                }
                outData.setRejectProList(rejectProList);
            }
        }else if(type.intValue() == 2) {
            outData = planMapper.selectTichengZProDetail(planId);
            String[] dateArr = new String[2];
            dateArr[0] = outData.getPlanStartDate();
            dateArr[1] = outData.getPlanEndDate();
            outData.setDateArr(dateArr);
            List<Map<String,String>> stoList = planMapper.selectTichengProStoList(planId);
            //适用门店
            if (stoList.size() > 0 && stoList != null){
                String[] array = new String[stoList.size()];
                for(int i = 0; i < stoList.size();i++){
                    array[i] = stoList.get(i).get("stoCode");
                }
                outData.setStoreCodes(array);
            }
            List<PercentageProInData> proList = planMapper.selectTichengProList(planId);
            if (proList.size() > 0 && proList != null) {
                for (PercentageProInData pro : proList) {
                    pro.setId(null);
                    pro.setPid(null);
//                    sale.setClientId(null);
                }
                outData.setProInDataList(proList);
            }
        }
        if(Objects.nonNull(outData)){
            //方案名称重命名
            outData.setPlanName(outData.getPlanName()+"-副本");
        }
        outData.setId(null);
        return outData;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void stopPlan(Long planId,String planType,String stopType, String stopReason) {
        //提成方案审核
        if ("1".equals(planType)) {
            PercentageInData saleInfo = planMapper.selectTichengZSaleDetail(planId);
            if ("1".equals(saleInfo.getPlanStatus())) {
                if ("1".equals(stopType)) {//立即停用
                    planMapper.stopSalePlan(planId,"2", CommonUtil.getyyyyMMdd(), stopReason);
                    //消息推送至APP
                    List<Map<String,String>> result = planMapper.selectSaleStoList(saleInfo.getId().longValue());
                    String[] stoCodeArr = new String[result.size()];
                    for (int i = 0; i < result.size();i++) {
                        stoCodeArr[i] = result.get(i).get("stoCode");
                    }
                    saleInfo.setStoreCodes(stoCodeArr);
                    List<String> userIds = planMapper.selectUserIdList(saleInfo);
                    MessageParams messageParams = new MessageParams();
                    messageParams.setClient(saleInfo.getClient());
                    messageParams.setUserIdList(userIds);
                    messageParams.setContentParmas(new ArrayList<String>() {{
                        add(saleInfo.getPlanName());add(stopReason);
                    }});
                    messageParams.setId("MSG00011");
                    log.info("销售提成消息参数:{}", messageParams.toString());
                    String message = operateService.sendMessageList(messageParams);
                    log.info("销售提成消息结果:{}", message);
                    BaseResponse resultObj = JSONObject.parseObject(message, BaseResponse.class);
                    int code = resultObj.getCode();
                    if (code != 0) {
                        throw new BusinessException("消息接口服务异常");
                    }
                }else {
                    planMapper.stopSalePlan(planId,"1",saleInfo.getPlanEndDate(), stopReason);
                }
            }else {
                throw new BusinessException("该方案尚未审核，无法停用");
            }
            //消息推送到APP
            sendMessage(saleInfo);
        }else if("2".equals(planType)){
            PercentageInData proInfo = planMapper.selectTichengZProDetail(planId);
            if ("1".equals(proInfo.getPlanStatus())) {
                if ("1".equals(stopType)) {//立即停用
                    planMapper.stopProPlan(planId,"2",CommonUtil.getyyyyMMdd(), stopReason);
                    //消息推送至APP
                    List<Map<String,String>> result = planMapper.selectProStoList(proInfo.getId().longValue());
                    String[] stoCodeArr = new String[result.size()];
                    for (int i = 0; i < result.size();i++) {
                        stoCodeArr[i] = result.get(i).get("stoCode");
                    }
                    proInfo.setStoreCodes(stoCodeArr);
                    List<String> userIds = planMapper.selectUserIdList(proInfo);
                    MessageParams messageParams = new MessageParams();
                    messageParams.setClient(proInfo.getClient());
                    messageParams.setUserIdList(userIds);
                    messageParams.setContentParmas(new ArrayList<String>() {{
                        add(proInfo.getPlanName());add(stopReason);
                    }});
                    messageParams.setId("MSG00011");
                    log.info("销售提成消息参数:{}", messageParams.toString());
                    String message = operateService.sendMessageList(messageParams);
                    log.info("培训推送消息结果:{}", message);
                }else {
                    planMapper.stopProPlan(planId,"1",proInfo.getPlanEndDate(), stopReason);
                }
            }else {
                throw new BusinessException("该方案尚未审核，无法停用");
            }
        }
    }

    @Override
    public void timerStopPlan() {
        //获取定时停用任务总列表
        List<PercentageOutData> planList = planMapper.selectPlanStopList();
        if(!CollectionUtils.isEmpty(planList)){
            for (PercentageOutData outData : planList) {
                if(!(CommonUtil.getyyyyMMdd().compareTo(outData.getPlanStopDate()) == -1)){
                    if ("1".equals(outData.getPlanType())){
                        //变更方案状态为停用
                        planMapper.stopSalePlan(outData.getId(),"2","", "");
                    }else {
                        //变更方案状态为停用
                        planMapper.stopProPlan(outData.getId(),"2","", "");
                    }
                    //消息推送到APP
//                        List<Map<String,String>> result = planMapper.selectSaleStoList(outData.getId().longValue());
//                        String[] stoCodeArr = new String[result.size()];
//                        for (int i = 0; i < result.size();i++) {
//                            stoCodeArr[i] = result.get(i).get("stoCode");
//                        }
//                        outData.setStoreCodes(stoCodeArr);
                    PercentageInData inData = new PercentageInData();
                    inData.setClient(outData.getClient());
                    inData.setStoreCodes(outData.getStoreCodes());
                    List<String> userIds = planMapper.selectUserIdList(inData);
                    MessageParams messageParams = new MessageParams();
                    messageParams.setClient(outData.getClient());
                    messageParams.setUserIdList(userIds);
                    messageParams.setContentParmas(new ArrayList<String>() {{
                        add(outData.getPlanName());add(outData.getPlanReason());
                    }});
                    messageParams.setId("MSG00011");
                    log.info("销售提成消息参数:{}", messageParams.toString());
                    String message = operateService.sendMessageList(messageParams);
                    log.info("销售提成消息参数:{}", message);
                }
            }
        }
    }

    @Override
    public List<Map<String, String>> selectStoList(Long planId,Long type) {
        List<Map<String, String>> result = new ArrayList<>();
        if (type.intValue() == 1){
            result = planMapper.selectSaleStoList(planId);
        }else if(type.intValue() == 2){
            result = planMapper.selectProStoList(planId);
        }
        return result;
    }

    public Integer setSalePlan(PercentageInData inData){
        Integer id = 0;
        if(ObjectUtil.isNotEmpty(inData.getDeleteFlag())){
            if ("2".equals(inData.getDeleteFlag())){
                inData.setDateArr(inData.getTrialDateArr());
            }
        }
        if (ObjectUtil.isNotEmpty(inData.getDeleteFlag())) {
            if (!"2".equals(inData.getDeleteFlag())) {
                int count1 = planMapper.checkStartDate(inData);
                if (count1 > 0) {
                    throw new BusinessException("已存在相同时间段内提成方案");
                } else {
                    int count2 = planMapper.checkEndDate(inData);
                    if (count2 > 0) {
                        throw new BusinessException("已存在相同时间段内提成方案");
                    }
                }
            }
        }
        GaiaTichengSalePlan tichengPlan = new GaiaTichengSalePlan();
        if (ObjectUtil.isNotEmpty(inData.getId())){
            tichengPlan.setId(inData.getId());
            tichengPlan.setClient(inData.getClient());
            tichengPlan.setPlanName(inData.getPlanName());
            tichengPlan.setPlanAmtWay(inData.getPlanAmtWay());
            tichengPlan.setPlanIfNegative(inData.getPlanIfNegative());
            tichengPlan.setPlanRateWay(inData.getPlanRateWay());
            tichengPlan.setPlanUpdateDateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengPlan.setPlanUpdater(inData.getPlanCreater());
            tichengPlan.setPlanUpdateId(inData.getPlanCreaterId());
            tichengPlan.setPlanStartDate(inData.getPlanStartDate());
            tichengPlan.setPlanEndDate(inData.getPlanEndDate());
            tichengPlan.setPlanScaleSaler(inData.getPlanScaleSaler());
            tichengPlan.setPlanScaleSto(inData.getPlanScaleSto());
            tichengPlan.setPlanRejectPro(inData.getPlanRejectPro());
            if (ObjectUtil.isNotEmpty(inData.getDeleteFlag())){
                tichengPlan.setDeleteFlag(inData.getDeleteFlag());
            }else {
                tichengPlan.setDeleteFlag("0");
            }
            if ("1".equals(inData.getPlanStatus())){
                tichengPlan.setPlanStatus("3");
                tichengPlan.setPlanReason(inData.getPlanReason());
            }
            planMapper.updateTichengSaleZ(tichengPlan);
        }else {
            String planCode = planMapper.selectNextPlanCode(inData.getClient());
            tichengPlan.setClient(inData.getClient());
            tichengPlan.setPlanCreater(inData.getPlanCreater());
            tichengPlan.setPlanCreateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengPlan.setPlanName(inData.getPlanName());
            tichengPlan.setPlanAmtWay(inData.getPlanAmtWay());
            tichengPlan.setPlanCode(planCode);
            tichengPlan.setPlanStatus("0");
            if (ObjectUtil.isNotEmpty(inData.getDeleteFlag())){
                tichengPlan.setDeleteFlag(inData.getDeleteFlag());
            }else {
                tichengPlan.setDeleteFlag("0");
            }
            tichengPlan.setPlanType(inData.getPlanType());
            tichengPlan.setPlanCreaterId(inData.getPlanCreaterId());
            tichengPlan.setPlanIfNegative(inData.getPlanIfNegative());
            tichengPlan.setPlanRateWay(inData.getPlanRateWay());
            tichengPlan.setPlanReason("已通过审核");
            tichengPlan.setPlanUpdateDateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss() + "");
            tichengPlan.setPlanUpdater(inData.getPlanCreater());
            tichengPlan.setPlanUpdateId(inData.getPlanCreaterId());
            tichengPlan.setPlanStartDate(inData.getPlanStartDate());
            tichengPlan.setPlanEndDate(inData.getPlanEndDate());
            tichengPlan.setPlanScaleSaler(inData.getPlanScaleSaler());
            tichengPlan.setPlanScaleSto(inData.getPlanScaleSto());
            tichengPlan.setPlanRejectPro(inData.getPlanRejectPro());
            planMapper.addPlanMain(tichengPlan);
        }
        id = tichengPlan.getId();
        //初始化加盟商门店
        planMapper.deleteStoreByPid(id);
        //新增销售明细
        List<PercentageStoInData> stoList = new ArrayList<>();
        if (inData.getStoreCodes().length > 0) {
            for (String storeCode : inData.getStoreCodes()) {
                PercentageStoInData item = new PercentageStoInData();
                item.setClientId(inData.getClient());
                item.setPid(tichengPlan.getId());
                item.setPlanSuitMouth(inData.getPlanSuitMonth());
                item.setStoCode(storeCode);
                stoList.add(item);
            }
        }
        if (stoList.size() > 0 && stoList != null) {
            planMapper.addPlanStores(stoList);
        }
        //新增销售提成明细
        if (ObjectUtil.isNotEmpty(tichengPlan.getId())){
            planMapper.deleteSalePlanByPid(tichengPlan.getId());
            if (CollUtil.isNotEmpty(inData.getSaleInDataList())) {
                List<PercentageSaleInData> saleList = new ArrayList<>();
                for (PercentageSaleInData saleItem : inData.getSaleInDataList()) {
                    saleItem.setClientId(inData.getClient());
                    saleItem.setPid(tichengPlan.getId());
                    if (ObjectUtil.isEmpty(saleItem.getProSaleClass())) {
                        saleItem.setProSaleClass("");
                    }
                    saleList.add(saleItem);
                }
                if (saleList.size() > 0 && saleList != null) {
                    planMapper.batchSaleTichengList(saleList);
                }
            }
            if (ObjectUtil.isNotEmpty(inData.getClassArr())){
                //软删除剔除分类
                planMapper.deleteClassByPid(tichengPlan.getId());
                List<PlanRejectClass> classList = new ArrayList<>();
                for (int i = 0; i < inData.getClassArr().length; i++) {
                    String[] c = inData.getClassArr()[i];
                    PlanRejectClass rejectClass = new PlanRejectClass();
                    rejectClass.setPid(tichengPlan.getId());
                    rejectClass.setProBigClass(c[0]);
                    rejectClass.setProMidClass(c[1]);
                    rejectClass.setProClass(c[2]);
                    classList.add(rejectClass);
                }
                if (ObjectUtil.isNotEmpty(classList) && classList.size() > 0){
                    planMapper.batchPlanRejectClass(classList);
                }
            }
            if (ObjectUtil.isNotEmpty(inData.getRejectProList()) && inData.getRejectProList().size() > 0) {
                planMapper.deleteRejectProByPid(tichengPlan.getId());
                List<PercentageProInData> proList = new ArrayList<>();
                for (PercentageProInData proInData : inData.getRejectProList()) {
                    if (ObjectUtil.isNotEmpty(proInData.getProCode())) {
//                            if (ObjectUtil.isEmpty(proInData.getTichengAmt())) {
//                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额未填");
//                            }
                        proInData.setClientId(inData.getClient());
                        proInData.setPid(tichengPlan.getId());
//                            proInData.setPlanProductWay(inData.getPlanProductWay());
                        proList.add(proInData);
                    }
                }
                if (proList.size() > 0 && proList != null) {
                    planMapper.batchSaleRejectProList(proList);
                }
            }
        }
        return id;
    }

    public Integer setProPlan(PercentageInData inData){
        Integer id = 0;
        if (ObjectUtil.isNotEmpty(inData.getDeleteFlag())) {
            if (!"2".equals(inData.getDeleteFlag())) {
                int count1 = planMapper.checkProStartDate(inData);
                if (count1 > 0) {
                    throw new BusinessException("已存在相同时间段内提成方案");
                } else {
                    int count2 = planMapper.checkProEndDate(inData);
                    if (count2 > 0) {
                        throw new BusinessException("已存在相同时间段内提成方案");
                    }
                }
            }
        }
        GaiaTichengProplanZN tichengPlan = new GaiaTichengProplanZN();
        if (ObjectUtil.isNotEmpty(inData.getId())){
            tichengPlan.setId(inData.getId());
            tichengPlan.setClient(inData.getClient());
            tichengPlan.setPlanName(inData.getPlanName());
            tichengPlan.setPlanProductWay(inData.getPlanProductWay());
            tichengPlan.setPlanUpdateDatetime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengPlan.setPlanUpdater(inData.getPlanCreater());
            tichengPlan.setPlanUpdateId(inData.getPlanCreaterId());
            tichengPlan.setPlanStartDate(inData.getPlanStartDate());
            tichengPlan.setPlanEndDate(inData.getPlanEndDate());
            tichengPlan.setPlanScaleSaler(inData.getPlanScaleSaler());
            tichengPlan.setPlanScaleSto(inData.getPlanScaleSto());
            tichengPlan.setPliantPercentageType(inData.getPlanPercentageWay());
            if (ObjectUtil.isNotEmpty(inData.getDeleteFlag())){
                tichengPlan.setDeleteFlag(inData.getDeleteFlag());
            }else {
                tichengPlan.setDeleteFlag("0");
            }
            if ("1".equals(inData.getPlanStatus())){
                tichengPlan.setPlanStatus("3");
                tichengPlan.setPlanReason(inData.getPlanReason());
            }
            planMapper.updateTichengProZ(tichengPlan);
        }else {
            String planCode = planMapper.selectNextPlanCode(inData.getClient());
            tichengPlan.setClient(inData.getClient());
            tichengPlan.setPlanCreater(inData.getPlanCreater());
            tichengPlan.setPlanCreateTime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss());
            tichengPlan.setPlanName(inData.getPlanName());
            tichengPlan.setPlanProductWay(inData.getPlanProductWay());
            tichengPlan.setPlanCode(planCode);
            tichengPlan.setPlanStatus("0");
            if (ObjectUtil.isNotEmpty(inData.getDeleteFlag())){
                tichengPlan.setDeleteFlag(inData.getDeleteFlag());
            }else {
                tichengPlan.setDeleteFlag("0");
            }
            tichengPlan.setPlanType(inData.getPlanType());
            tichengPlan.setPlanCreaterId(inData.getPlanCreaterId());
            tichengPlan.setPliantPercentageType(inData.getPlanPercentageWay());
//            tichengPlan.setPlanReason("已通过审核");
            tichengPlan.setPlanUpdateDatetime(CommonUtil.getyyyyMMdd() + CommonUtil.getHHmmss() + "");
            tichengPlan.setPlanUpdater(inData.getPlanCreater());
            tichengPlan.setPlanUpdateId(inData.getPlanCreaterId());
            tichengPlan.setPlanStartDate(inData.getPlanStartDate());
            tichengPlan.setPlanEndDate(inData.getPlanEndDate());
            tichengPlan.setPlanScaleSaler(inData.getPlanScaleSaler());
            tichengPlan.setPlanScaleSto(inData.getPlanScaleSto());
            planMapper.addPlanProMain(tichengPlan);
        }
        id = tichengPlan.getId();
        //初始化加盟商门店
        planMapper.deleteProStoreByPid(id);
        //新增销售明细
        List<PercentageStoInData> stoList = new ArrayList<>();
        if (inData.getStoreCodes().length > 0) {
            for (String storeCode : inData.getStoreCodes()) {
                PercentageStoInData item = new PercentageStoInData();
                item.setClientId(inData.getClient());
                item.setPid(tichengPlan.getId());
                item.setPlanSuitMouth(inData.getPlanSuitMonth());
                item.setStoCode(storeCode);
                stoList.add(item);
            }
        }
        if (stoList.size() > 0 && stoList != null) {
            planMapper.addPlanProStores(stoList);
        }
        //新增单品提成明细
        if (ObjectUtil.isNotEmpty(tichengPlan.getId())){
            if (CollUtil.isNotEmpty(inData.getProInDataList())) {
                planMapper.deleteProPlanByPid(tichengPlan.getId());
                List<PercentageProInData> proList = new ArrayList<>();
                for (PercentageProInData proInData : inData.getProInDataList()) {
                    if (ObjectUtil.isNotEmpty(proInData.getProCode())) {
                        if (StringUtils.isEmpty(proInData.getSaleQty())){
                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量1未填");
                        }else {
                            if (new BigDecimal(proInData.getSaleQty()).compareTo(BigDecimal.ZERO) != 1){
                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量1应大于0");
                            }
                        }
                        if (ObjectUtil.isNotEmpty(proInData.getTichengAmt()) || ObjectUtil.isNotEmpty(proInData.getTichengRate())) {//第1级别
                            if (new BigDecimal(proInData.getSaleQty()).compareTo(BigDecimal.ZERO) != 1) {
                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量1应大于0");
                            }
                            if (ObjectUtil.isNotEmpty(proInData.getTichengAmt()) && ObjectUtil.isNotEmpty(proInData.getTichengRate())) {
                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额1或提成比例1二选一");
                            }
                            if (StringUtils.isNotEmpty(proInData.getTichengRate()) && ObjectUtil.isEmpty(proInData.getTichengAmt())) {
                                if (new BigDecimal(proInData.getTichengRate()).compareTo(BigDecimal.ZERO) < 1 || new BigDecimal(proInData.getTichengRate()).compareTo(new BigDecimal(100)) == 1) {
                                    throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成比例1应在0%到100%之间");
                                }
                            }
                            if (ObjectUtil.isNotEmpty(proInData.getSaleQty2()) || ObjectUtil.isNotEmpty(proInData.getTichengAmt2()) || ObjectUtil.isNotEmpty(proInData.getTichengRate2())) {
                                if (ObjectUtil.isEmpty(proInData.getSaleQty2())) {
                                    throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量2未填");
                                }
                                if (ObjectUtil.isNotEmpty(proInData.getTichengAmt2()) || ObjectUtil.isNotEmpty(proInData.getTichengRate2())) {
                                    if (new BigDecimal(proInData.getSaleQty2()).compareTo(new BigDecimal(proInData.getSaleQty())) != 1) {
                                        throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量2应大于达成数量1");
                                    }
                                    if (ObjectUtil.isNotEmpty(proInData.getTichengAmt2()) && ObjectUtil.isNotEmpty(proInData.getTichengRate2())) {
                                        throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额2或提成比例2二选一");
                                    }
                                    if (StringUtils.isNotEmpty(proInData.getTichengRate2()) && ObjectUtil.isEmpty(proInData.getTichengAmt2())) {
                                        if (new BigDecimal(proInData.getTichengRate2()).compareTo(BigDecimal.ZERO) < 1 || new BigDecimal(proInData.getTichengRate2()).compareTo(new BigDecimal(100)) == 1) {
                                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成比例2应在0%到100%之间");
                                        }
                                    }
                                    if (ObjectUtil.isNotEmpty(proInData.getSaleQty3()) || ObjectUtil.isNotEmpty(proInData.getTichengAmt3()) || ObjectUtil.isNotEmpty(proInData.getTichengRate3())) {
                                        if (ObjectUtil.isEmpty(proInData.getSaleQty3())) {
                                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量3未填");
                                        }
                                        if (ObjectUtil.isNotEmpty(proInData.getTichengAmt3()) || ObjectUtil.isNotEmpty(proInData.getTichengRate3())) {
                                            if (new BigDecimal(proInData.getSaleQty3()).compareTo(new BigDecimal(proInData.getSaleQty2())) != 1) {
                                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的达成数量3应大于达成数量2");
                                            }
                                            if (ObjectUtil.isNotEmpty(proInData.getTichengAmt3()) && ObjectUtil.isNotEmpty(proInData.getTichengRate3())) {
                                                throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额3或提成比例3二选一");
                                            }
                                            if (StringUtils.isNotEmpty(proInData.getTichengRate3()) && ObjectUtil.isEmpty(proInData.getTichengAmt3())) {
                                                if (new BigDecimal(proInData.getTichengRate3()).compareTo(BigDecimal.ZERO) < 1 || new BigDecimal(proInData.getTichengRate3()).compareTo(new BigDecimal(100)) == 1) {
                                                    throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成比例3应在0%到100%之间");
                                                }
                                            }
                                            proInData.setTichengLevel("3");
                                        } else {
                                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额3或提成比例3未填");
                                        }
                                    }else {
                                        proInData.setTichengLevel("2");
                                    }
                                } else {
                                    throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额2或提成比例2未填");
                                }
                            }else {
                                proInData.setTichengLevel("1");
                            }
                        }else {
                            throw new BusinessException("商品编码为:" + proInData.getProCode() + "的单品提成金额1或提成比例1未填");
                        }
                        proInData.setClientId(inData.getClient());
                        proInData.setPid(tichengPlan.getId());
                        proInData.setPlanProductWay(inData.getPlanProductWay());
                        proInData.setPlanPercentageWay(inData.getPlanPercentageWay());
                        proList.add(proInData);
                    }
                }
                if (proList.size() > 0 && proList != null) {
                    planMapper.batchProTichengList(proList);
                }
            }
        }
        return id;
    }
}
