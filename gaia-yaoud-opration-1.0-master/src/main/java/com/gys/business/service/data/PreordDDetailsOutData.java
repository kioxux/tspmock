package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xiaoyuan on 2020/12/24
 */
@Data
public class PreordDDetailsOutData implements Serializable {
    private static final long serialVersionUID = 8461463342678131761L;
    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 订购门店
     */
    @ApiModelProperty(value = "订购门店")
    private String gspdBrId;

    /**
     * 订购单号
     */
    @ApiModelProperty(value = "订购单号")
    private String gspdVoucherId;

    /**
     * 订购日期
     */
    @ApiModelProperty(value = "订购日期")
    private String gspdDate;

    /**
     * 行号
     */
    @ApiModelProperty(value = "行号")
    private String gspdSeril;

    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码")
    private String gspdProId;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String gspdProName;

    /**
     * 通用名
     */
    @ApiModelProperty(value = "通用名")
    private String gspdProCommonname;

    /**
     * 规格
     */
    @ApiModelProperty(value = "规格")
    private String gspdProSpecs;

    /**
     * 厂家
     */
    @ApiModelProperty(value = "厂家")
    private String gspdProFactoryName;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位")
    private String gspdProUnit;

    /**
     * 条形码
     */
    @ApiModelProperty(value = "条形码")
    private String gspdProBarcode;

    /**
     * 批准文号
     */
    @ApiModelProperty(value = "批准文号")
    private String gspdProRegisterNo;

    /**
     * 参考零售价
     */
    @ApiModelProperty(value = "参考零售价")
    private BigDecimal gspdProLsj;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量")
    private BigDecimal gspdQty;
}
