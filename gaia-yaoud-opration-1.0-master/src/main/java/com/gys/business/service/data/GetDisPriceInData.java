//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;
@Data
public class GetDisPriceInData implements Serializable {
    private static final long serialVersionUID = 6629528935800212977L;

    /**
     * 加盟号
     */
    private String clientId;
    /**
     * 店号
     */
    private String brId;
    /**
     * 数量
     */
    private String num;

    /**
     * 商品编码
     */
    private String proCode;
    /**
     * 促销单号
     */
    private String voucherId;

    /**
     * 单价
     */
    private String normalPrice;
    private String gsphType;
    private String totalMoney;
    private String serial;

    /**
     * 单品数量,
     */
    private Map<String, GetNumAmtData> numAmtDataMap;
}
