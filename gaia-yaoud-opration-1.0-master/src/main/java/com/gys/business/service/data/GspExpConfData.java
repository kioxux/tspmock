package com.gys.business.service.data;

import lombok.Data;

import java.util.Date;

@Data
public class GspExpConfData {
    //ID
    private Long id;
    // 加盟商
    private String client;
    // 类型(1-供应商,2-客户,3-商品,4-报表预警天数)
    private String type;
	// 字段名
    private String expiryDateField;
    // 字段描述
    private String expiryDateFieldName;
	//  预警天数
    private String warnDays;
	// 删除标记
    private Integer deleteFlag;
	// 创建人
    private String createUser;
	//创建时间
    private Date createTime;
    // 更新人
    private String updateUser;
	//更新时间
    private Date updateTime;
}
