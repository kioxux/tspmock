package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 退货详细
 *
 * @author xiaoyuan on 2020/9/1
 */
@Data
public class SaleReturnDetailData implements Serializable {
    private static final long serialVersionUID = 4689223459999072999L;

    @ApiModelProperty(value = "销售单号")
    private String gssdBillNo;

    @ApiModelProperty(value = "店号")
    private String gssdBrId;

    @ApiModelProperty(value = "销售日期")
    private String gssdDate;

    @ApiModelProperty(value = "行号")
    private String gssdSerial;

    @ApiModelProperty(value = "商品编码")
    private String gssdProId;

    @ApiModelProperty(value = "批号")
    private String gssdBatchNo;

    @ApiModelProperty(value = "有效期")
    private String gssdValidDate;

    @ApiModelProperty(value = "零售价")
    private String gssdPrc1;

    @ApiModelProperty(value = "单品应收价")
    private String gssdPrc2;

    @ApiModelProperty(value = "数量")
    private String gssdQty;

    @ApiModelProperty(value = "退货数量")
    private String gssdReturnQty;

    @ApiModelProperty(value = "实收金额")
    private String gssdAmt;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "厂家")
    private String proFactoryName;

    @ApiModelProperty(value = "剂型")
    private String proForm;

    @ApiModelProperty(value = "单位")
    private String proUnit;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "国家准字号")
    private String proRegisterNo;

    @ApiModelProperty(value = "产地")
    private String proPlace;

    @ApiModelProperty(value = "营业员")
    private String gssdSalerName;

    @ApiModelProperty(value = "医生")
    private String gssdDoctorName;

    @ApiModelProperty(value = "参加促销单号")
    private String gssdPmActivityId;

    @ApiModelProperty(value = "参加促销活动名称")
    private String gssdPmActivityName;

    @ApiModelProperty(value = "")
    private String gssdZkPm;

    @ApiModelProperty(value = "条件状态")
    private String gssdProStatus;

    @ApiModelProperty(value = "移动平均价")
    private String gssdMovPrice;

    @ApiModelProperty(value = "税率")
    private String gssdMovTax;

    private String gssdTaxRate;

    private String gssdOriQty;

    private String gssdDose;

    /**
     * 移动平均总价
     */
    private String gssdMovPrices;

    private String gssdZkAmt;
}
