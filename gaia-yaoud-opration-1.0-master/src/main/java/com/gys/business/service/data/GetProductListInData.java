//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.io.Serializable;
import java.util.List;

public class GetProductListInData implements Serializable {
    List<GetProductInData> inDataList;

    public GetProductListInData() {
    }

    public List<GetProductInData> getInDataList() {
        return this.inDataList;
    }

    public void setInDataList(final List<GetProductInData> inDataList) {
        this.inDataList = inDataList;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetProductListInData)) {
            return false;
        } else {
            GetProductListInData other = (GetProductListInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$inDataList = this.getInDataList();
                Object other$inDataList = other.getInDataList();
                if (this$inDataList == null) {
                    if (other$inDataList != null) {
                        return false;
                    }
                } else if (!this$inDataList.equals(other$inDataList)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetProductListInData;
    }

    public int hashCode() {

        int result = 1;
        Object $inDataList = this.getInDataList();
        result = result * 59 + ($inDataList == null ? 43 : $inDataList.hashCode());
        return result;
    }

    public String toString() {
        return "GetProductListInData(inDataList=" + this.getInDataList() + ")";
    }
}
