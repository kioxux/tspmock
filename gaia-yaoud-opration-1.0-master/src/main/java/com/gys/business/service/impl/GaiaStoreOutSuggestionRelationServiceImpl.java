package com.gys.business.service.impl;

import com.gys.business.mapper.entity.GaiaStoreOutSuggestionRelation;
import com.gys.business.mapper.GaiaStoreOutSuggestionRelationMapper;
import com.gys.business.service.GaiaStoreOutSuggestionRelationService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;

/**
 * 门店调出建议关系表(GaiaStoreOutSuggestionRelation)表服务实现类
 *
 * @author makejava
 * @since 2021-10-28 10:54:23
 */
@Service("gaiaStoreOutSuggestionRelationService")
public class GaiaStoreOutSuggestionRelationServiceImpl implements GaiaStoreOutSuggestionRelationService {
    @Resource
    private GaiaStoreOutSuggestionRelationMapper gaiaStoreOutSuggestionRelationDao;

}
