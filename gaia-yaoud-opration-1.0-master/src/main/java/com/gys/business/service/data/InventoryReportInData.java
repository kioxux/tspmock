package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 传参实体
 *
 * @author xiaoyuan on 2020/9/8
 */
@Data
public class InventoryReportInData implements Serializable {
    private static final long serialVersionUID = 7060383436120722949L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "店号")
    private String brId;

    @ApiModelProperty(value = "盘点单号")
    private String gspcdVoucherId;

    @ApiModelProperty(value = "商品编码")
    private String gspcProId;

    @ApiModelProperty(value = "初盘日期")
    private String gspcDate;

    @ApiModelProperty(value = "复盘日期")
    private String gspcdDate;

    @ApiModelProperty(value = "批号")
    private String batchNo;

    @ApiModelProperty(value = "盘点单号")
    private String voucherId;

    /*@ApiModelProperty(value = "商品编码 多个")
    private String[] proCode;*/
}
