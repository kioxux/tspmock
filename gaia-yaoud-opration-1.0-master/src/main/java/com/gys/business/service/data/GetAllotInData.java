//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

import java.math.BigDecimal;
import java.util.List;

public class GetAllotInData {
    private String clientId;
    private String gsamhVoucherId;
    private String gsamhDate;
    private String gsamhFinishDate;
    private String gsamhFrom;
    private String gsamhTo;
    private String gsamhType;
    private String gsamhStatus;
    private BigDecimal gsamhTotalAmt;
    private String gsamhTotalQty;
    private String gsamhEmp;
    private String gsamhEmp1;
    private String gsamhInvoicesId;
    private String gsamhProcedure;
    private List<GetAllotDetailInData> allotDetailInDataList;
    private String proId;
    private String batchNo;
    private String storeCode;
    private Integer pageNum;
    private Integer pageSize;

    public GetAllotInData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getGsamhVoucherId() {
        return this.gsamhVoucherId;
    }

    public String getGsamhDate() {
        return this.gsamhDate;
    }

    public String getGsamhFinishDate() {
        return this.gsamhFinishDate;
    }

    public String getGsamhFrom() {
        return this.gsamhFrom;
    }

    public String getGsamhTo() {
        return this.gsamhTo;
    }

    public String getGsamhType() {
        return this.gsamhType;
    }

    public String getGsamhStatus() {
        return this.gsamhStatus;
    }

    public BigDecimal getGsamhTotalAmt() {
        return this.gsamhTotalAmt;
    }

    public String getGsamhTotalQty() {
        return this.gsamhTotalQty;
    }

    public String getGsamhEmp() {
        return this.gsamhEmp;
    }

    public String getGsamhEmp1() {
        return this.gsamhEmp1;
    }

    public String getGsamhInvoicesId() {
        return this.gsamhInvoicesId;
    }

    public String getGsamhProcedure() {
        return this.gsamhProcedure;
    }

    public List<GetAllotDetailInData> getAllotDetailInDataList() {
        return this.allotDetailInDataList;
    }

    public String getProId() {
        return this.proId;
    }

    public String getBatchNo() {
        return this.batchNo;
    }

    public String getStoreCode() {
        return this.storeCode;
    }

    public Integer getPageNum() {
        return this.pageNum;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setGsamhVoucherId(final String gsamhVoucherId) {
        this.gsamhVoucherId = gsamhVoucherId;
    }

    public void setGsamhDate(final String gsamhDate) {
        this.gsamhDate = gsamhDate;
    }

    public void setGsamhFinishDate(final String gsamhFinishDate) {
        this.gsamhFinishDate = gsamhFinishDate;
    }

    public void setGsamhFrom(final String gsamhFrom) {
        this.gsamhFrom = gsamhFrom;
    }

    public void setGsamhTo(final String gsamhTo) {
        this.gsamhTo = gsamhTo;
    }

    public void setGsamhType(final String gsamhType) {
        this.gsamhType = gsamhType;
    }

    public void setGsamhStatus(final String gsamhStatus) {
        this.gsamhStatus = gsamhStatus;
    }

    public void setGsamhTotalAmt(final BigDecimal gsamhTotalAmt) {
        this.gsamhTotalAmt = gsamhTotalAmt;
    }

    public void setGsamhTotalQty(final String gsamhTotalQty) {
        this.gsamhTotalQty = gsamhTotalQty;
    }

    public void setGsamhEmp(final String gsamhEmp) {
        this.gsamhEmp = gsamhEmp;
    }

    public void setGsamhEmp1(final String gsamhEmp1) {
        this.gsamhEmp1 = gsamhEmp1;
    }

    public void setGsamhInvoicesId(final String gsamhInvoicesId) {
        this.gsamhInvoicesId = gsamhInvoicesId;
    }

    public void setGsamhProcedure(final String gsamhProcedure) {
        this.gsamhProcedure = gsamhProcedure;
    }

    public void setAllotDetailInDataList(final List<GetAllotDetailInData> allotDetailInDataList) {
        this.allotDetailInDataList = allotDetailInDataList;
    }

    public void setProId(final String proId) {
        this.proId = proId;
    }

    public void setBatchNo(final String batchNo) {
        this.batchNo = batchNo;
    }

    public void setStoreCode(final String storeCode) {
        this.storeCode = storeCode;
    }

    public void setPageNum(final Integer pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(final Integer pageSize) {
        this.pageSize = pageSize;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof GetAllotInData)) {
            return false;
        } else {
            GetAllotInData other = (GetAllotInData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label251: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label251;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label251;
                    }

                    return false;
                }

                Object this$gsamhVoucherId = this.getGsamhVoucherId();
                Object other$gsamhVoucherId = other.getGsamhVoucherId();
                if (this$gsamhVoucherId == null) {
                    if (other$gsamhVoucherId != null) {
                        return false;
                    }
                } else if (!this$gsamhVoucherId.equals(other$gsamhVoucherId)) {
                    return false;
                }

                Object this$gsamhDate = this.getGsamhDate();
                Object other$gsamhDate = other.getGsamhDate();
                if (this$gsamhDate == null) {
                    if (other$gsamhDate != null) {
                        return false;
                    }
                } else if (!this$gsamhDate.equals(other$gsamhDate)) {
                    return false;
                }

                label230: {
                    Object this$gsamhFinishDate = this.getGsamhFinishDate();
                    Object other$gsamhFinishDate = other.getGsamhFinishDate();
                    if (this$gsamhFinishDate == null) {
                        if (other$gsamhFinishDate == null) {
                            break label230;
                        }
                    } else if (this$gsamhFinishDate.equals(other$gsamhFinishDate)) {
                        break label230;
                    }

                    return false;
                }

                label223: {
                    Object this$gsamhFrom = this.getGsamhFrom();
                    Object other$gsamhFrom = other.getGsamhFrom();
                    if (this$gsamhFrom == null) {
                        if (other$gsamhFrom == null) {
                            break label223;
                        }
                    } else if (this$gsamhFrom.equals(other$gsamhFrom)) {
                        break label223;
                    }

                    return false;
                }

                label216: {
                    Object this$gsamhTo = this.getGsamhTo();
                    Object other$gsamhTo = other.getGsamhTo();
                    if (this$gsamhTo == null) {
                        if (other$gsamhTo == null) {
                            break label216;
                        }
                    } else if (this$gsamhTo.equals(other$gsamhTo)) {
                        break label216;
                    }

                    return false;
                }

                Object this$gsamhType = this.getGsamhType();
                Object other$gsamhType = other.getGsamhType();
                if (this$gsamhType == null) {
                    if (other$gsamhType != null) {
                        return false;
                    }
                } else if (!this$gsamhType.equals(other$gsamhType)) {
                    return false;
                }

                label202: {
                    Object this$gsamhStatus = this.getGsamhStatus();
                    Object other$gsamhStatus = other.getGsamhStatus();
                    if (this$gsamhStatus == null) {
                        if (other$gsamhStatus == null) {
                            break label202;
                        }
                    } else if (this$gsamhStatus.equals(other$gsamhStatus)) {
                        break label202;
                    }

                    return false;
                }

                Object this$gsamhTotalAmt = this.getGsamhTotalAmt();
                Object other$gsamhTotalAmt = other.getGsamhTotalAmt();
                if (this$gsamhTotalAmt == null) {
                    if (other$gsamhTotalAmt != null) {
                        return false;
                    }
                } else if (!this$gsamhTotalAmt.equals(other$gsamhTotalAmt)) {
                    return false;
                }

                label188: {
                    Object this$gsamhTotalQty = this.getGsamhTotalQty();
                    Object other$gsamhTotalQty = other.getGsamhTotalQty();
                    if (this$gsamhTotalQty == null) {
                        if (other$gsamhTotalQty == null) {
                            break label188;
                        }
                    } else if (this$gsamhTotalQty.equals(other$gsamhTotalQty)) {
                        break label188;
                    }

                    return false;
                }

                Object this$gsamhEmp = this.getGsamhEmp();
                Object other$gsamhEmp = other.getGsamhEmp();
                if (this$gsamhEmp == null) {
                    if (other$gsamhEmp != null) {
                        return false;
                    }
                } else if (!this$gsamhEmp.equals(other$gsamhEmp)) {
                    return false;
                }

                Object this$gsamhEmp1 = this.getGsamhEmp1();
                Object other$gsamhEmp1 = other.getGsamhEmp1();
                if (this$gsamhEmp1 == null) {
                    if (other$gsamhEmp1 != null) {
                        return false;
                    }
                } else if (!this$gsamhEmp1.equals(other$gsamhEmp1)) {
                    return false;
                }

                label167: {
                    Object this$gsamhInvoicesId = this.getGsamhInvoicesId();
                    Object other$gsamhInvoicesId = other.getGsamhInvoicesId();
                    if (this$gsamhInvoicesId == null) {
                        if (other$gsamhInvoicesId == null) {
                            break label167;
                        }
                    } else if (this$gsamhInvoicesId.equals(other$gsamhInvoicesId)) {
                        break label167;
                    }

                    return false;
                }

                label160: {
                    Object this$gsamhProcedure = this.getGsamhProcedure();
                    Object other$gsamhProcedure = other.getGsamhProcedure();
                    if (this$gsamhProcedure == null) {
                        if (other$gsamhProcedure == null) {
                            break label160;
                        }
                    } else if (this$gsamhProcedure.equals(other$gsamhProcedure)) {
                        break label160;
                    }

                    return false;
                }

                Object this$allotDetailInDataList = this.getAllotDetailInDataList();
                Object other$allotDetailInDataList = other.getAllotDetailInDataList();
                if (this$allotDetailInDataList == null) {
                    if (other$allotDetailInDataList != null) {
                        return false;
                    }
                } else if (!this$allotDetailInDataList.equals(other$allotDetailInDataList)) {
                    return false;
                }

                Object this$proId = this.getProId();
                Object other$proId = other.getProId();
                if (this$proId == null) {
                    if (other$proId != null) {
                        return false;
                    }
                } else if (!this$proId.equals(other$proId)) {
                    return false;
                }

                label139: {
                    Object this$batchNo = this.getBatchNo();
                    Object other$batchNo = other.getBatchNo();
                    if (this$batchNo == null) {
                        if (other$batchNo == null) {
                            break label139;
                        }
                    } else if (this$batchNo.equals(other$batchNo)) {
                        break label139;
                    }

                    return false;
                }

                Object this$storeCode = this.getStoreCode();
                Object other$storeCode = other.getStoreCode();
                if (this$storeCode == null) {
                    if (other$storeCode != null) {
                        return false;
                    }
                } else if (!this$storeCode.equals(other$storeCode)) {
                    return false;
                }

                Object this$pageNum = this.getPageNum();
                Object other$pageNum = other.getPageNum();
                if (this$pageNum == null) {
                    if (other$pageNum != null) {
                        return false;
                    }
                } else if (!this$pageNum.equals(other$pageNum)) {
                    return false;
                }

                Object this$pageSize = this.getPageSize();
                Object other$pageSize = other.getPageSize();
                if (this$pageSize == null) {
                    if (other$pageSize != null) {
                        return false;
                    }
                } else if (!this$pageSize.equals(other$pageSize)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof GetAllotInData;
    }

    public int hashCode() {
        
        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $gsamhVoucherId = this.getGsamhVoucherId();
        result = result * 59 + ($gsamhVoucherId == null ? 43 : $gsamhVoucherId.hashCode());
        Object $gsamhDate = this.getGsamhDate();
        result = result * 59 + ($gsamhDate == null ? 43 : $gsamhDate.hashCode());
        Object $gsamhFinishDate = this.getGsamhFinishDate();
        result = result * 59 + ($gsamhFinishDate == null ? 43 : $gsamhFinishDate.hashCode());
        Object $gsamhFrom = this.getGsamhFrom();
        result = result * 59 + ($gsamhFrom == null ? 43 : $gsamhFrom.hashCode());
        Object $gsamhTo = this.getGsamhTo();
        result = result * 59 + ($gsamhTo == null ? 43 : $gsamhTo.hashCode());
        Object $gsamhType = this.getGsamhType();
        result = result * 59 + ($gsamhType == null ? 43 : $gsamhType.hashCode());
        Object $gsamhStatus = this.getGsamhStatus();
        result = result * 59 + ($gsamhStatus == null ? 43 : $gsamhStatus.hashCode());
        Object $gsamhTotalAmt = this.getGsamhTotalAmt();
        result = result * 59 + ($gsamhTotalAmt == null ? 43 : $gsamhTotalAmt.hashCode());
        Object $gsamhTotalQty = this.getGsamhTotalQty();
        result = result * 59 + ($gsamhTotalQty == null ? 43 : $gsamhTotalQty.hashCode());
        Object $gsamhEmp = this.getGsamhEmp();
        result = result * 59 + ($gsamhEmp == null ? 43 : $gsamhEmp.hashCode());
        Object $gsamhEmp1 = this.getGsamhEmp1();
        result = result * 59 + ($gsamhEmp1 == null ? 43 : $gsamhEmp1.hashCode());
        Object $gsamhInvoicesId = this.getGsamhInvoicesId();
        result = result * 59 + ($gsamhInvoicesId == null ? 43 : $gsamhInvoicesId.hashCode());
        Object $gsamhProcedure = this.getGsamhProcedure();
        result = result * 59 + ($gsamhProcedure == null ? 43 : $gsamhProcedure.hashCode());
        Object $allotDetailInDataList = this.getAllotDetailInDataList();
        result = result * 59 + ($allotDetailInDataList == null ? 43 : $allotDetailInDataList.hashCode());
        Object $proId = this.getProId();
        result = result * 59 + ($proId == null ? 43 : $proId.hashCode());
        Object $batchNo = this.getBatchNo();
        result = result * 59 + ($batchNo == null ? 43 : $batchNo.hashCode());
        Object $storeCode = this.getStoreCode();
        result = result * 59 + ($storeCode == null ? 43 : $storeCode.hashCode());
        Object $pageNum = this.getPageNum();
        result = result * 59 + ($pageNum == null ? 43 : $pageNum.hashCode());
        Object $pageSize = this.getPageSize();
        result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());
        return result;
    }

    public String toString() {
        return "GetAllotInData(clientId=" + this.getClientId() + ", gsamhVoucherId=" + this.getGsamhVoucherId() + ", gsamhDate=" + this.getGsamhDate() + ", gsamhFinishDate=" + this.getGsamhFinishDate() + ", gsamhFrom=" + this.getGsamhFrom() + ", gsamhTo=" + this.getGsamhTo() + ", gsamhType=" + this.getGsamhType() + ", gsamhStatus=" + this.getGsamhStatus() + ", gsamhTotalAmt=" + this.getGsamhTotalAmt() + ", gsamhTotalQty=" + this.getGsamhTotalQty() + ", gsamhEmp=" + this.getGsamhEmp() + ", gsamhEmp1=" + this.getGsamhEmp1() + ", gsamhInvoicesId=" + this.getGsamhInvoicesId() + ", gsamhProcedure=" + this.getGsamhProcedure() + ", allotDetailInDataList=" + this.getAllotDetailInDataList() + ", proId=" + this.getProId() + ", batchNo=" + this.getBatchNo() + ", storeCode=" + this.getStoreCode() + ", pageNum=" + this.getPageNum() + ", pageSize=" + this.getPageSize() + ")";
    }
}
