package com.gys.business.service.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@ApiModel(value = "查询条件", description = "收货表")
@Data
public class GetAcceptInData {
    @ApiModelProperty(value = "加盟商")
    private String clientId;
    @ApiModelProperty(value = "店号")
    private String gsahBrId;
    @ApiModelProperty(value = "收货单号")
    private String gsahVoucherId;
    @ApiModelProperty(value = "创建时间")
    private String orderDate;
    @ApiModelProperty(value = "收货日期")
    private String gsahDate;
    @ApiModelProperty(value = "配送单号")
    private String gsahPsVoucherId;
    @ApiModelProperty(value = "采购订单号")
    private String gsahPoid;
    @ApiModelProperty(value = "发运地点")
    private String gsahDeparturePlace;
    @ApiModelProperty(value = "启运时间")
    private String gsahDepartureTime;
    @ApiModelProperty(value = "启运日期")
    private String gsahDepartureDate;
    @ApiModelProperty(value = "启运温度")
    private String gsahDepartureTemperature;
    @ApiModelProperty(value = "到货日期")
    private String gsahArriveDate;
    @ApiModelProperty(value = "到货时间")
    private String gsahArriveTime;
    @ApiModelProperty(value = "到货温度")
    private String gsahArriveTemperature;
    @ApiModelProperty(value = "运输单位")
    private String gsahTransportOrganization;
    @ApiModelProperty(value = "运输方式")
    private String gsahTransportMode;
    @ApiModelProperty(value = "发货地点")
    private String gsahFrom;
    @ApiModelProperty(value = "收货地点")
    private String gsahTo;
    @ApiModelProperty(value = "单据类型")
    private String gsahType;
    @ApiModelProperty(value = "收货状态")
    private String gsahStatus;
    @ApiModelProperty(value = "收货金额")
    private BigDecimal gsahTotalAmt;
    @ApiModelProperty(value = "收货数量")
    private String gsahTotalQty;
    @ApiModelProperty(value = "收货人")
    private String gsahEmp;
    @ApiModelProperty(value = "备注")
    private String gsahRemaks;
    @ApiModelProperty(value = "发票号")
    private String gsahTxNo;
    private List<GetAcceptDetailOutData> acceptDetailInDataList;
    private List<GetPoDetailInData> poDetailList;
    @ApiModelProperty(value = "商品编码")
    private String gsadProId;
    @ApiModelProperty(value = "商品编码集合")
    private List<String> proIdList;
    @ApiModelProperty(value = "批号")
    private String batchNo;
    @ApiModelProperty(value = "门店编码")
    private String storeCode;
    private Integer pageNum;
    private Integer pageSize;
    //模糊匹配字段
    private String nameOrCode;
    @ApiModelProperty(value = "税率类型")
    private String codeClass;
    private String userId;
    @ApiModelProperty(value = "多门店编码")
    private String[] storeCodes;
    @ApiModelProperty(value = "连锁公司")
    private String stoChainHead;
    @ApiModelProperty(value = "多门店编码")
    private List<Map<String,String>> itemList;
    @ApiModelProperty(value = "供应商编码")
    private String supCode;

    @ApiModelProperty(value = "判断结果")
    private Boolean flag;

    @ApiModelProperty(value = "到货数量")
    private BigDecimal gsahBoxQty;
    @ApiModelProperty(value = "保温方式")
    private String gsahInsulationMode;
    @ApiModelProperty(value = "发票同行")
    private String gsahTxFollow;
    @ApiModelProperty(value = "途中温度是否达标")
    private String gsahTempFit;
    @ApiModelProperty(value = "运输人员")
    private String gsahTransportEmp;
    @ApiModelProperty(value = "冷链开关")
    private String coldFlag;
    @ApiModelProperty(value = "仓库地点")
    private String dcCode;
    //仓库收货明细
    private List<AcceptWareHouseProDetailData> acceptWareHouseProDetailList;
    //拒收入参
    private List<AcceptWareSaveWtpsRefuseInData> refuseInDataList;

    private ColdchainInfo coldchainInfo;

    /**
     * 当前登陆人的ID,名称
     */
    private String userCode;
    private String userName;
}
