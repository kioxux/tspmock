//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.service.data;

public class PrescriptionDetailInfoOutData {
    private String clientId;
    private String commodityCode;
    private String commodityName;
    private String specification;
    private String unit;
    private String batchNum;
    private String stockQty;
    private String quantity;
    private String retailPrice;
    private String factory;
    private String origin;
    private String dosageForm;
    private String approvalNum;

    public PrescriptionDetailInfoOutData() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getCommodityCode() {
        return this.commodityCode;
    }

    public String getCommodityName() {
        return this.commodityName;
    }

    public String getSpecification() {
        return this.specification;
    }

    public String getUnit() {
        return this.unit;
    }

    public String getBatchNum() {
        return this.batchNum;
    }

    public String getStockQty() {
        return this.stockQty;
    }

    public String getQuantity() {
        return this.quantity;
    }

    public String getRetailPrice() {
        return this.retailPrice;
    }

    public String getFactory() {
        return this.factory;
    }

    public String getOrigin() {
        return this.origin;
    }

    public String getDosageForm() {
        return this.dosageForm;
    }

    public String getApprovalNum() {
        return this.approvalNum;
    }

    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    public void setCommodityCode(final String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public void setCommodityName(final String commodityName) {
        this.commodityName = commodityName;
    }

    public void setSpecification(final String specification) {
        this.specification = specification;
    }

    public void setUnit(final String unit) {
        this.unit = unit;
    }

    public void setBatchNum(final String batchNum) {
        this.batchNum = batchNum;
    }

    public void setStockQty(final String stockQty) {
        this.stockQty = stockQty;
    }

    public void setQuantity(final String quantity) {
        this.quantity = quantity;
    }

    public void setRetailPrice(final String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public void setFactory(final String factory) {
        this.factory = factory;
    }

    public void setOrigin(final String origin) {
        this.origin = origin;
    }

    public void setDosageForm(final String dosageForm) {
        this.dosageForm = dosageForm;
    }

    public void setApprovalNum(final String approvalNum) {
        this.approvalNum = approvalNum;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PrescriptionDetailInfoOutData)) {
            return false;
        } else {
            PrescriptionDetailInfoOutData other = (PrescriptionDetailInfoOutData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label167: {
                    Object this$clientId = this.getClientId();
                    Object other$clientId = other.getClientId();
                    if (this$clientId == null) {
                        if (other$clientId == null) {
                            break label167;
                        }
                    } else if (this$clientId.equals(other$clientId)) {
                        break label167;
                    }

                    return false;
                }

                Object this$commodityCode = this.getCommodityCode();
                Object other$commodityCode = other.getCommodityCode();
                if (this$commodityCode == null) {
                    if (other$commodityCode != null) {
                        return false;
                    }
                } else if (!this$commodityCode.equals(other$commodityCode)) {
                    return false;
                }

                label153: {
                    Object this$commodityName = this.getCommodityName();
                    Object other$commodityName = other.getCommodityName();
                    if (this$commodityName == null) {
                        if (other$commodityName == null) {
                            break label153;
                        }
                    } else if (this$commodityName.equals(other$commodityName)) {
                        break label153;
                    }

                    return false;
                }

                Object this$specification = this.getSpecification();
                Object other$specification = other.getSpecification();
                if (this$specification == null) {
                    if (other$specification != null) {
                        return false;
                    }
                } else if (!this$specification.equals(other$specification)) {
                    return false;
                }

                label139: {
                    Object this$unit = this.getUnit();
                    Object other$unit = other.getUnit();
                    if (this$unit == null) {
                        if (other$unit == null) {
                            break label139;
                        }
                    } else if (this$unit.equals(other$unit)) {
                        break label139;
                    }

                    return false;
                }

                Object this$batchNum = this.getBatchNum();
                Object other$batchNum = other.getBatchNum();
                if (this$batchNum == null) {
                    if (other$batchNum != null) {
                        return false;
                    }
                } else if (!this$batchNum.equals(other$batchNum)) {
                    return false;
                }

                label125: {
                    Object this$stockQty = this.getStockQty();
                    Object other$stockQty = other.getStockQty();
                    if (this$stockQty == null) {
                        if (other$stockQty == null) {
                            break label125;
                        }
                    } else if (this$stockQty.equals(other$stockQty)) {
                        break label125;
                    }

                    return false;
                }

                label118: {
                    Object this$quantity = this.getQuantity();
                    Object other$quantity = other.getQuantity();
                    if (this$quantity == null) {
                        if (other$quantity == null) {
                            break label118;
                        }
                    } else if (this$quantity.equals(other$quantity)) {
                        break label118;
                    }

                    return false;
                }

                Object this$retailPrice = this.getRetailPrice();
                Object other$retailPrice = other.getRetailPrice();
                if (this$retailPrice == null) {
                    if (other$retailPrice != null) {
                        return false;
                    }
                } else if (!this$retailPrice.equals(other$retailPrice)) {
                    return false;
                }

                label104: {
                    Object this$factory = this.getFactory();
                    Object other$factory = other.getFactory();
                    if (this$factory == null) {
                        if (other$factory == null) {
                            break label104;
                        }
                    } else if (this$factory.equals(other$factory)) {
                        break label104;
                    }

                    return false;
                }

                label97: {
                    Object this$origin = this.getOrigin();
                    Object other$origin = other.getOrigin();
                    if (this$origin == null) {
                        if (other$origin == null) {
                            break label97;
                        }
                    } else if (this$origin.equals(other$origin)) {
                        break label97;
                    }

                    return false;
                }

                Object this$dosageForm = this.getDosageForm();
                Object other$dosageForm = other.getDosageForm();
                if (this$dosageForm == null) {
                    if (other$dosageForm != null) {
                        return false;
                    }
                } else if (!this$dosageForm.equals(other$dosageForm)) {
                    return false;
                }

                Object this$approvalNum = this.getApprovalNum();
                Object other$approvalNum = other.getApprovalNum();
                if (this$approvalNum == null) {
                    if (other$approvalNum != null) {
                        return false;
                    }
                } else if (!this$approvalNum.equals(other$approvalNum)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PrescriptionDetailInfoOutData;
    }

    public int hashCode() {

        int result = 1;
        Object $clientId = this.getClientId();
        result = result * 59 + ($clientId == null ? 43 : $clientId.hashCode());
        Object $commodityCode = this.getCommodityCode();
        result = result * 59 + ($commodityCode == null ? 43 : $commodityCode.hashCode());
        Object $commodityName = this.getCommodityName();
        result = result * 59 + ($commodityName == null ? 43 : $commodityName.hashCode());
        Object $specification = this.getSpecification();
        result = result * 59 + ($specification == null ? 43 : $specification.hashCode());
        Object $unit = this.getUnit();
        result = result * 59 + ($unit == null ? 43 : $unit.hashCode());
        Object $batchNum = this.getBatchNum();
        result = result * 59 + ($batchNum == null ? 43 : $batchNum.hashCode());
        Object $stockQty = this.getStockQty();
        result = result * 59 + ($stockQty == null ? 43 : $stockQty.hashCode());
        Object $quantity = this.getQuantity();
        result = result * 59 + ($quantity == null ? 43 : $quantity.hashCode());
        Object $retailPrice = this.getRetailPrice();
        result = result * 59 + ($retailPrice == null ? 43 : $retailPrice.hashCode());
        Object $factory = this.getFactory();
        result = result * 59 + ($factory == null ? 43 : $factory.hashCode());
        Object $origin = this.getOrigin();
        result = result * 59 + ($origin == null ? 43 : $origin.hashCode());
        Object $dosageForm = this.getDosageForm();
        result = result * 59 + ($dosageForm == null ? 43 : $dosageForm.hashCode());
        Object $approvalNum = this.getApprovalNum();
        result = result * 59 + ($approvalNum == null ? 43 : $approvalNum.hashCode());
        return result;
    }

    public String toString() {
        return "PrescriptionDetailInfoOutData(clientId=" + this.getClientId() + ", commodityCode=" + this.getCommodityCode() + ", commodityName=" + this.getCommodityName() + ", specification=" + this.getSpecification() + ", unit=" + this.getUnit() + ", batchNum=" + this.getBatchNum() + ", stockQty=" + this.getStockQty() + ", quantity=" + this.getQuantity() + ", retailPrice=" + this.getRetailPrice() + ", factory=" + this.getFactory() + ", origin=" + this.getOrigin() + ", dosageForm=" + this.getDosageForm() + ", approvalNum=" + this.getApprovalNum() + ")";
    }
}
