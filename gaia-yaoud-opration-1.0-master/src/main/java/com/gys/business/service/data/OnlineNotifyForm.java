package com.gys.business.service.data;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/***
 * @desc: 入库上架通知实体
 * @author: ryan
 * @createTime: 2021/6/2 14:57
 **/
@Data
public class OnlineNotifyForm {

    private List<NotifyDTO> list;

    @Data
    public static class NotifyDTO {
        /**订单号(入库单号)*/
        private String orderCode;
        /**加盟商*/
        private String client;
        /**商品编码*/
        private String proSelfCode;
        /**本次入库数量*/
        private BigDecimal quantity;
    }
}
