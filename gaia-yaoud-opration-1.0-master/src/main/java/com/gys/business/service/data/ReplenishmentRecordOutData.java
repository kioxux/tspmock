package com.gys.business.service.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 补货记录返回实体
 *
 * @author xiaoyuan on 2020/9/11
 */
@Data
public class ReplenishmentRecordOutData implements Serializable {
    private static final long serialVersionUID = -242837980954894284L;

    @ApiModelProperty(value = "日期")
    private String gsrdDate;

    @ApiModelProperty(value = "补货单号")
    private String gsrdVoucherId;


    @ApiModelProperty(value = "商品编码")
    private String proSelfCode;

    @ApiModelProperty(value = "移动平均价")
    private String gsrdAverageCost;

    @ApiModelProperty(value = "毛利率")
    private String gsrdGrossMargin;

    @ApiModelProperty(value = "90天销量")
    private String gsrdSalesDays1;

    @ApiModelProperty(value = "30天销量")
    private String gsrdSalesDays2;

    @ApiModelProperty(value = "7天销量")
    private String gsrdSalesDays3;

    @ApiModelProperty(value = "建议补货量")
    private String gsrdProposeQty;

    @ApiModelProperty(value = "补货量")
    private String gsrdNeedQty;

    @ApiModelProperty(value = "本店库存")
    private String gsrdStockStore;

    @ApiModelProperty(value = "仓库库存")
    private String gsrdStockDepot;

    @ApiModelProperty(value = "最小陈列")
    private String gsrdDisplayMin;

    @ApiModelProperty(value = "中包装")
    private String gsrdPackMidsize;

    @ApiModelProperty(value = "最后一次供应商")
    private String gsrdLastSupid;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;

    @ApiModelProperty(value = "计量单位")
    private String proUnit;

    @ApiModelProperty(value = "产地")
    private String proPlace;

    @ApiModelProperty(value = "中包装量")
    private String proMidPackage;

    @ApiModelProperty(value = "零售价")
    private String gsppPriceNormal;

    @ApiModelProperty(value = "单据金额")
    private String gsrhTotalAmt;
}
