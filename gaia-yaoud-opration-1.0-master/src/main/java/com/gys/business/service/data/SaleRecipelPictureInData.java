package com.gys.business.service.data;

import lombok.Data;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2021/12/24 11:29
 */
@Data
public class SaleRecipelPictureInData {
    /**
     * 加盟商
     */
    private String client;

    /**
     *销售单号
     */
    private String gssrSaleBillNo;

    /**
     * 图片地址
     */
    private String pictureUrl;
}
