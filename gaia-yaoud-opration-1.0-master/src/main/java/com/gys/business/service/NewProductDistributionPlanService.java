package com.gys.business.service;

import com.github.pagehelper.PageInfo;
import com.gys.business.service.data.NewProductDistributionPlanDto;
import com.gys.business.service.data.NewProductDistributionPlanVo;
import com.gys.business.service.data.NewProductDto;
import com.gys.common.response.Result;

import java.util.List;
import java.util.Map;

import com.gys.common.data.*;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 10:26 2021/8/4
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
public interface NewProductDistributionPlanService {
    /**
     * 有计划-新品铺货查询参数获取
     * @return
     */
    Map<String,Object> getQueryCondition(NewProductDistributionPlanDto planDto);

    /**
     * 有计划-新品铺货计划详情获取
     * @param planDto
     * @return
     */
    List<NewProductDistributionPlanVo> plan(NewProductDistributionPlanDto planDto);

    /**
     * 有计划-新品铺货详情导出
     * @param planDto
     * @return
     */
    Result export(NewProductDistributionPlanDto planDto);

    /**
     * 有计划-查询商品条件
     * @param dto
     * @return
     */
    PageInfo getProduct(NewProductDto dto);
    JsonResult noPlan(NewProductDistributionNoPlanDto noPlanDto);

    JsonResult getNoPlanClientList(NewProductDistributionNoPlanClientDto noPlanClientDto);

    JsonResult getProductCodeList(NewProductDistributionNoPlanProductCodeDto noPlanProductCodeDto);

    JsonResult getProv(NewProductDistributionNoPlanProvAndCityDto noPlanProvAndCityDto);

    JsonResult getCityList(NewProductDistributionNoPlanProvAndCityDto noPlanProvAndCityDto);
}
