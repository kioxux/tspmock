package com.gys.business.service;

import com.gys.business.mapper.entity.GaiaFiArPayment;
import com.gys.common.data.GaiaBillOfArInData;
import com.gys.common.data.GaiaBillOfArReportData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 明细清单表(GaiaBillOfAr)表服务接口
 *
 * @author XIAOZY
 * @since 2021-09-18 15:13:13
 */
public interface GaiaBillOfArService {

    Boolean insertBill(GetLoginOutData loginUser, GaiaBillOfArInData inData);

    Boolean insertBillList(GetLoginOutData loginUser, List<GaiaBillOfArInData> inData);

    Object listBill(GetLoginOutData loginUser, GaiaBillOfArInData inData);

    PageInfo listSalesBill(GetLoginOutData loginUser, GaiaBillOfArInData inData);

    Boolean updateBillHXStatus(GetLoginOutData loginUser, GaiaBillOfArInData inData);

    PageInfo listReceivableDetail(GetLoginOutData loginUser, GaiaBillOfArInData inDate);

    Result exportBill(GetLoginOutData userInfo, GaiaBillOfArInData inData);

    Object importBill(MultipartFile file, GetLoginOutData userInfo);

    Result exportReceivableDetail(GetLoginOutData loginUser, GaiaBillOfArInData inData);

    PageInfo<GaiaBillOfArReportData> listReport(GetLoginOutData loginUser, GaiaBillOfArInData inData);

    void exportListReport(GetLoginOutData loginUser, HttpServletResponse response, GaiaBillOfArInData inData);

    List<GaiaFiArPayment> listBillType(GetLoginOutData loginUser, GaiaFiArPayment inData);

    Object insertGaiaFiArPayment(GetLoginOutData loginUser, GaiaFiArPayment inData);

    Object deleteArBill(GetLoginOutData loginUser, GaiaBillOfArInData inData);

    Object insertArBill(GetLoginOutData loginUser, GaiaBillOfArInData inData);

    List<Map<String,String>> selectSoDnByList(GetLoginOutData loginUser,String soDnByName);
}
