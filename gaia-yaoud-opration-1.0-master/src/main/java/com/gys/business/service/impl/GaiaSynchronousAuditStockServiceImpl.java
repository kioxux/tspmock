package com.gys.business.service.impl;


import com.gys.business.mapper.GaiaSynchronousAuditStockMapper;
import com.gys.business.mapper.entity.GaiaSynchronousAuditStock;
import com.gys.business.service.GaiaSynchronousAuditStockService;
import com.gys.business.service.data.GaiaSynchronousAuditStockOutData;
import com.gys.common.data.GetLoginOutData;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 库存审计(GaiaSynchronousAuditStock)表服务实现类
 *
 * @author makejava
 * @since 2021-07-01 13:43:14
 */
@Service("gaiaSynchronousAuditStockService")
public class GaiaSynchronousAuditStockServiceImpl implements GaiaSynchronousAuditStockService {
    @Resource
    private GaiaSynchronousAuditStockMapper gaiaSynchronousAuditStockMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public GaiaSynchronousAuditStock queryById(Long id) {
        return this.gaiaSynchronousAuditStockMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<GaiaSynchronousAuditStock> queryAllByLimit(int offset, int limit) {
        return this.gaiaSynchronousAuditStockMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param gaiaSynchronousAuditStock 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaSynchronousAuditStock insert(GaiaSynchronousAuditStock gaiaSynchronousAuditStock) {
        this.gaiaSynchronousAuditStockMapper.insert(gaiaSynchronousAuditStock);
        return gaiaSynchronousAuditStock;
    }

    /**
     * 修改数据
     *
     * @param gaiaSynchronousAuditStock 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaSynchronousAuditStock update(GaiaSynchronousAuditStock gaiaSynchronousAuditStock) {
        this.gaiaSynchronousAuditStockMapper.update(gaiaSynchronousAuditStock);
        return this.queryById(gaiaSynchronousAuditStock.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.gaiaSynchronousAuditStockMapper.deleteById(id) > 0;
    }

    @Override
    public Object insertStockInfo(GaiaSynchronousAuditStock inData) {
        //1.0查询库存的数据，按加盟商，门店分类
        List<GaiaSynchronousAuditStock> list = gaiaSynchronousAuditStockMapper.listStockInfo();
        if (!CollectionUtils.isEmpty(list)) {
            //插入数据
            gaiaSynchronousAuditStockMapper.insertBatch(list);
        }
        return true;
    }

    @Override
    public Object listAuditInfo(GaiaSynchronousAuditStockOutData inData) {
        return gaiaSynchronousAuditStockMapper.listAuditInfo(inData);
    }

    @Override
    public GaiaSynchronousAuditStock auditQuery(Map<String, String> map, GetLoginOutData userInfo) {
        GaiaSynchronousAuditStock auditStock = new GaiaSynchronousAuditStock();
        auditStock.setClient(userInfo.getClient());
        auditStock.setStoreId(userInfo.getDepId());
        auditStock.setSource("fx");
        auditStock.setComputerLogo(map.get("computerLogo"));
        return gaiaSynchronousAuditStockMapper.auditQuery(auditStock);
    }
}
