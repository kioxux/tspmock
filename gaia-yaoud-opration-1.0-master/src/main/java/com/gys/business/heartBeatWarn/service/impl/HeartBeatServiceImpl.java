package com.gys.business.heartBeatWarn.service.impl;

import com.gys.business.heartBeatWarn.service.HeartBeatService;
import com.gys.business.mapper.GaiaServericeWatchMapper;
import com.gys.business.mapper.entity.GaiaServericeWatch;
import com.gys.common.data.JsonResult;
import com.gys.util.CommonUtil;
import com.gys.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2022/1/7 13:35
 */
@Slf4j
@Service("heartBeatService")
public class HeartBeatServiceImpl implements HeartBeatService {
    @Resource
    private GaiaServericeWatchMapper gaiaServericeWatchMapper;

    @Override
    public JsonResult addServericeWatch(Map<String,Object> inData){
        GaiaServericeWatch gaiaServericeWatch=new GaiaServericeWatch();
        gaiaServericeWatch.setClient(inData.get("client").toString());
        gaiaServericeWatch.setGswSite(inData.get("site").toString());
        gaiaServericeWatch.setGswIp(inData.get("IP").toString());
        gaiaServericeWatch.setGswRequestDate(DateUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
        gaiaServericeWatch.setGswName(inData.get("Name").toString());
        gaiaServericeWatch.setGswComment(inData.get("Comment").toString());
        gaiaServericeWatch.setGswWatchType(Integer.parseInt(inData.get("WatchType").toString()));
        if(inData.containsKey("Comsume")){
            gaiaServericeWatch.setGswComsumeSecond(Integer.parseInt(inData.get("Comsume").toString()));
        }else{
            gaiaServericeWatch.setGswComsumeSecond(0);
        }
        gaiaServericeWatch.setGswCreateDate(DateUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss"));
        gaiaServericeWatch.setGswIsDelete(0);
        gaiaServericeWatchMapper.insert(gaiaServericeWatch);
        return JsonResult.success(true,"");
    }
}
