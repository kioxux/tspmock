package com.gys.business.heartBeatWarn.service;

import com.gys.common.data.JsonResult;

import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2022/1/7 13:35
 */
public interface HeartBeatService {
    /**
     * 新增心跳监测表
     * @param inData
     * @return
     */
    JsonResult addServericeWatch(Map<String,Object> inData);
}
