package com.gys.business.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.gys.business.service.GaiaLicenseQualificationOutData;
import com.gys.business.service.LicenseQualificationService;
import com.gys.business.service.data.GaiaLicenseQualificationInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Api(tags = "证照资质效期预警", description = "/licenseQualifacation")
@RestController
@RequestMapping("/licenseQualifacation")
public class LicenseQualificationController extends BaseController {


    @Autowired
    private LicenseQualificationService licenseQualificationService;
    @Resource
    private CosUtils cosUtils;

    @PostMapping("/listSite")
    public JsonResult listSite(HttpServletRequest request, @RequestBody Map<String, String> queryMap) {
        queryMap.put("client", this.getLoginUser(request).getClient());
        return JsonResult.success(licenseQualificationService.listSite(queryMap), "查询成功");
    }

    @PostMapping("/list")
    public JsonResult list(HttpServletRequest request, @RequestBody @Valid GaiaLicenseQualificationInData inData) {
        inData.setClient(this.getLoginUser(request).getClient());
        return JsonResult.success(licenseQualificationService.list(inData), "查询成功");
    }

    @PostMapping("/list/export")
    public Result export(HttpServletRequest request, @RequestBody @Valid GaiaLicenseQualificationInData inData) {
        inData.setClient(this.getLoginUser(request).getClient());
        List<GaiaLicenseQualificationOutData> outData = licenseQualificationService.list(inData).getList();
        if (CollectionUtil.isEmpty(outData)) {
            throw new BusinessException("导出结果为空");
        }
        Result result = null;
        String fileName = "证书资质预警报表导出";
        CsvFileInfo csvFileInfo = CsvClient.getCsvByte(outData, fileName, Collections.singletonList((short) 1));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            bos.write(csvFileInfo.getFileContent());
            result = cosUtils.uploadFile(bos, fileName + ".csv");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @PostMapping("/listSup")
    public JsonResult listSup(HttpServletRequest request, @RequestBody Map<String, String> queryMap) {
        queryMap.put("client", this.getLoginUser(request).getClient());
        return JsonResult.success(licenseQualificationService.listSup(queryMap), "查询成功");
    }

    @PostMapping("/listCus")
    public JsonResult listCus(HttpServletRequest request, @RequestBody Map<String, String> queryMap) {
        queryMap.put("client", this.getLoginUser(request).getClient());
        return JsonResult.success(licenseQualificationService.listCus(queryMap), "查询成功");
    }

    @ApiOperation(value = "查询门店下拉数据,")
    @PostMapping("/listSto")
    public JsonResult listSto(HttpServletRequest request, @RequestBody Map<String, String> queryMap) {
        queryMap.put("client", this.getLoginUser(request).getClient());
        return JsonResult.success(licenseQualificationService.listSto(queryMap), "查询成功");
    }

}
