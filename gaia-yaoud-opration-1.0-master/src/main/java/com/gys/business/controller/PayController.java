package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.entity.GaiaSdElectronChange;
import com.gys.business.mapper.entity.GaiaSdRechargeCard;
import com.gys.business.mapper.entity.GaiaSdRechargeCardSet;
import com.gys.business.mapper.entity.GaiaSdSytAccount;
import com.gys.business.service.PayService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.his.FxBillInfoForm;
import com.gys.business.service.data.integralExchange.MemeberJfMessageInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping({"/pay/"})
@Api(tags = "支付")
@Slf4j
public class PayController extends BaseController {
    @Resource
    private PayService payService;

    @ApiOperation(value = "支付方式列表",response = GetPayTypeOutData.class)
    @PostMapping({"payTypeList"})
    public JsonResult payTypeList(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.payService.payTypeList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "根据加盟商获取支付方式列表",response = GetPayTypeOutData.class)
    @PostMapping({"payTypeListByClient"})
    public JsonResult payTypeListByClient(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.payService.payTypeListByClient(inData), "提示：获取数据成功！");
    }

    @PostMapping({"payInfo"})
    public JsonResult payInfo(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.payService.payInfo(inData), "提示：获取数据成功！");
    }

    @PostMapping({"exchangeCash"})
    public JsonResult exchangeCash(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.payService.exchangeCash(inData), "提示：获取数据成功！");
    }

    @PostMapping({"pay"})
    public JsonResult pay(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.payService.pay(inData, userInfo);
        return JsonResult.success(null, "提示：获取数据成功！");
    }

    @ApiModelProperty(value = "支付提交")
    @PostMapping({"payCommit"})
    public JsonResult payCommit(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("支付提交 inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        Map<String, Object> resultMap = payService.payCommit(inData); //Todo：弃用 暂时不删除 等后续payCommit2测试完成时再删除

        //物料凭证
        payService.insertMaterialDoc((List<MaterialDocRequestDto>) resultMap.get("materialList"));
        return JsonResult.success(null, "提示：获取数据成功！");
    }

    @ApiModelProperty(value = "支付提交v2")
    @PostMapping({"payCommit2"})
    public JsonResult payCommit2(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("支付提交 inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        Map<String, Object> resultMap = payService.payCommit2(inData);
        payService.insertMaterialDoc((List<MaterialDocRequestDto>) resultMap.get("materialList"));
        resultMap.remove("materialList");
        return JsonResult.success(resultMap, "提示：获取数据成功！");
    }

    @PostMapping({"dzqList"})
    public JsonResult dzqList(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.payService.dzqList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"memeberJfDxValidate"})
    public JsonResult memeberJfValidate(HttpServletRequest request, @RequestBody MemeberJfValidateInData inData) {
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.payService.memeberJfDxValidate(inData), "提示：获取数据成功！");
    }

    @ApiModelProperty(value = "送电子券")
    @PostMapping({"giveElectrons"})
    public JsonResult giveElectrons(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("送券 inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        List<GaiaSdElectronChange> res = payService.giveElectrons(inData);

        return JsonResult.success(res, "提示：获取数据成功！");
    }

    @ApiModelProperty(value = "计算需要赠送电子券")
    @PostMapping({"calculateElectrons"})
    public JsonResult calculateElectrons(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("计算需要赠送电子券 inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        List<ElectronDetailOutData> res = payService.calculateElectrons(inData);

        return JsonResult.success(res, "提示：获取数据成功！");
    }

    @ApiModelProperty(value = "储值卡查询")
    @PostMapping({"queryRecharge"})
    public JsonResult queryRecharge(HttpServletRequest request, @RequestBody RechargeQueryInData inData) {
        log.info("储值卡查询 inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        List<GaiaSdRechargeCard> res = payService.queryRecharge(inData);

        return JsonResult.success(res, "提示：获取数据成功！");
    }

    @ApiModelProperty(value = "查询此门店是否需要密码")
    @PostMapping({"isNeedPassword"})
    public JsonResult isNeedPassword(HttpServletRequest request) {
//        log.info("查询此门店是否需要密码 inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        GaiaSdRechargeCardSet res = payService.isNeedPassword(userInfo.getClient(),userInfo.getDepId());

        return JsonResult.success(res, "提示：获取数据成功！");
    }

    @ApiModelProperty(value = "查询此门店收银通账号")
    @PostMapping({"getSytAccount"})
    public JsonResult getSytAccount(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        GaiaSdSytAccount res = payService.getSytAccount(userInfo.getClient(),userInfo.getDepId());
        return JsonResult.success(res, "提示：获取数据成功！");
    }

    //积分抵现规则查询
    @PostMapping({"memeberJfMessage"})
    public JsonResult memeberJfMessage(HttpServletRequest request, @RequestBody MemeberJfMessageInData inData) {
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.payService.memeberJfMessage(inData), "提示：获取数据成功！");
    }

    @ApiModelProperty(value = "检查并更新门诊单处方")
    @PostMapping({"checkClinicalSheet"})
    public JsonResult checkClinicalSheet(HttpServletRequest request,@RequestBody FxBillInfoForm param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        int res = payService.checkClinicalSheet(userInfo.getClient(),userInfo.getDepId(), param);
        if(res==0){
            return JsonResult.fail(999, "提示：门诊单发生变更，请重新调出！");

        }else{
            return JsonResult.success(res, "提示：获取数据成功！");
        }
    }
}
