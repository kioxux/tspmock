package com.gys.business.controller;

import com.gys.business.service.AcceptWareHouseService;
import com.gys.business.service.data.*;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.util.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "仓库收货相关", description = "/acceptWareHouse")
@RestController
@RequestMapping({"/acceptWareHouse/"})
public class AcceptWareHouseController extends BaseController {

    @Autowired
    private AcceptWareHouseService acceptWareHouseService;
    //调取配送单
    @ApiOperation(value = "调取配送单" ,response = AcceptWareHouseOutData.class)
    @PostMapping({"pickOrderList"})
    public JsonResult pickOrderList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.acceptWareHouseService.pickOrderList(userInfo), "提示：获取数据成功！");
    }

    //拣货单列表查询
    @ApiOperation(value = "拣货单列表查询" ,response = AcceptWareHouseOutData.class)
    @PostMapping({"list"})
    public JsonResult selectList(HttpServletRequest request,@RequestBody AcceptWareHouseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.acceptWareHouseService.selectList(inData), "提示：获取数据成功！");
    }

    //根据拣货单号查询拣货单列表
    @ApiOperation(value = "根据拣货单号查询拣货单列表" ,response = AcceptWareHouseOutData.class)
    @PostMapping({"selectListById"})
    public JsonResult selectListById(HttpServletRequest request,@RequestBody AcceptWareHouseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.acceptWareHouseService.selectListById(inData), "提示：获取数据成功！");
    }

    //根据拣货单号汇总该拣货单下配送数量及金额
    @ApiOperation(value = "根据拣货单号汇总该拣货单下配送数量及金额" ,response = AcceptVoucherOutData.class)
    @PostMapping({"selectTotalDetail"})
    public JsonResult selectTotalDetail(HttpServletRequest request,@RequestBody AcceptWareHouseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.acceptWareHouseService.selectTotalDetail(inData), "提示：获取数据成功！");
    }

    //根据拣货单号获取商品明细
    @ApiOperation(value = "根据拣货单号获取商品明细" ,response = AcceptWareHouseProDetailData.class)
    @PostMapping({"selectPickProductList"})
    public JsonResult selectPickProductList(HttpServletRequest request,@RequestBody AcceptWareHouseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.acceptWareHouseService.selectPickProductList(inData), "提示：获取数据成功！");
    }

    //审核
    @FrequencyValid
    @ApiOperation(value = "审核")
    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request,@RequestBody AcceptWareHouseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        inData.setUserName(userInfo.getLoginName());
        this.acceptWareHouseService.approve(inData,userInfo);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    //获取冷链信息
    @PostMapping("getColdchainInfo")
    public JsonResult getColdchainInfo(HttpServletRequest request, @RequestBody AcceptWareHouseInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        return JsonResult.success(this.acceptWareHouseService.getColdchainInfo(inData,userInfo), "提示：获取数据成功！");
    }

    @PostMapping("saveColdchainInfo")
    public JsonResult saveColdchainInfo(HttpServletRequest request, @RequestBody @Valid ColdchainInfo info){
        GetLoginOutData loginUser = this.getLoginUser(request);
        info.setClient(loginUser.getClient());
        info.setStoCode(loginUser.getDepId());
        this.acceptWareHouseService.saveColdchainInfo(info);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    //获取门店是否可比价
    @ApiOperation(value = "isWtpsParam:是否开启委托配送相应功能 0：关 1：开")
    @PostMapping({"showWtpsLabel"})
    public JsonResult showWtpsLabel(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.acceptWareHouseService.showWtpsLabel(userInfo), "提示：获取数据成功！");
    }

    //委托配送单列表查询
    @ApiOperation(value = "委托配送单列表查询",response = TrustSendOutData.class)
    @PostMapping({"selectDsfTrustList"})
    public JsonResult selectDsfTrustList(HttpServletRequest request,@RequestBody TrustSendInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.acceptWareHouseService.selectDsfTrustList(inData), "提示：获取数据成功！");
    }

    //调取委托配送单
    @ApiOperation(value = "调取委托配送单",response = TrustSendOutData.class)
    @PostMapping({"selectWtpsOrderList"})
    public JsonResult selectWtpsOrderList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.acceptWareHouseService.selectWtpsOrderList(userInfo.getClient(),userInfo.getDepId()), "提示：获取数据成功！");
    }

    //调取委托配送单
    @ApiOperation(value = "查询委托配送单明细",response = TrustDetailOutData.class)
    @PostMapping({"selectWtpsDetail"})
    public JsonResult selectWtpsDetail(HttpServletRequest request,@RequestBody AcceptWareHouseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.acceptWareHouseService.selectWtpsDetail(inData), "提示：获取数据成功！");
    }

    //调取委托配送单
    @ApiOperation(value = "修改委托配送单明细拒收数量")
    @PostMapping({"saveWtpsDetail"})
    public JsonResult saveWtpsDetail(HttpServletRequest request,@RequestBody@Valid List<AcceptWareSaveWtpsRefuseInData> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.acceptWareHouseService.saveWtpsDetail(inData,userInfo.getClient(),userInfo.getDepId(),userInfo.getLoginName()), "提示：获取数据成功！");
    }

    //审核
    @FrequencyValid
    @ApiOperation(value = "委托配送单审核")
    @PostMapping({"approveWtps"})
    public JsonResult approveWtps(HttpServletRequest request,@RequestBody AcceptWareHouseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        this.acceptWareHouseService.approveWtps(inData,userInfo);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    /**
     * 委托配送单明细修改
     */
    @PostMapping("editWtpsDetail")
    public JsonResult editWtpsDetail(HttpServletRequest request,@RequestBody @Valid EditWtpsDetailDto dto){
        GetLoginOutData userInfo = this.getLoginUser(request);
        dto.setClient(userInfo.getClient());
        dto.setStoCode(userInfo.getDepId());
        dto.setUpdateUser(userInfo.getLoginName());
        dto.setUpdateDate(DateUtil.getCurrentDateStr("yyyyMMdd"));
        dto.setUpdateTime(DateUtil.getCurrentTimeStr("HHmmss"));
        this.acceptWareHouseService.editWtpsDetail(dto);
        return JsonResult.success("","提示：操作成功！");
    }

    /**
     * 查询是否有冷链配送
     */
    @GetMapping("hasCold")
    public JsonResult hasCold(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(acceptWareHouseService.hasCold(userInfo.getClient()),"提示：获取数据成功！");
    }

    /**
     * 查询拒收原因
     */
    @PostMapping("rejecteReasons")
    public JsonResult getRejecteReasons(){
        return JsonResult.success(acceptWareHouseService.getgetRejecteReasons(),"提示：获取数据成功！");
    }

}
