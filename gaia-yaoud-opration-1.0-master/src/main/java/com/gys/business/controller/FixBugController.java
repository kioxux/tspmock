package com.gys.business.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.GaiaSdSaleDMapper;
import com.gys.business.mapper.GaiaSdSaleHMapper;
import com.gys.business.mapper.entity.GaiaSdSaleD;
import com.gys.business.mapper.entity.GaiaSdSaleH;
import com.gys.business.service.data.MaterialDocRequestDto;
import com.gys.common.base.BaseController;
import com.gys.common.data.JsonResult;
import com.gys.common.enums.MaterialTypeEnum;
import com.gys.feign.PurchaseService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping({"/fix/"})
@Slf4j
public class FixBugController extends BaseController {
    @Resource
    private PurchaseService purchaseService;

    @Resource
    private GaiaSdSaleHMapper saleHMapper;

    @Resource
    private GaiaSdSaleDMapper saleDMapper;

    @PostMapping({"fixInsertMaterialDoc"})
    public JsonResult fixInsertMaterialDoc(HttpServletRequest request) {
        Example saleHExample = new Example(GaiaSdSaleH.class);
        saleHExample.createCriteria().andIsNotNull("gsshCrFlag");
        List<GaiaSdSaleH> saleHList = saleHMapper.selectByExample(saleHExample);

        for (GaiaSdSaleH saleH : saleHList) {
            List<MaterialDocRequestDto> materialList = Lists.newArrayList();
            int guiNoCount = 1;
            Example saleDExample = new Example(GaiaSdSaleD.class);
            saleDExample.createCriteria().andEqualTo("clientId", saleH.getClientId()).andEqualTo("gssdBrId", saleH.getGsshBrId()).andEqualTo("gssdBillNo", saleH.getGsshBillNo());
            List<GaiaSdSaleD> saleDList = saleDMapper.selectByExample(saleDExample);

            for (GaiaSdSaleD saleD : saleDList) {
                MaterialDocRequestDto dto = new MaterialDocRequestDto();
                dto.setClient(saleD.getClientId());
                dto.setGuid(UUID.randomUUID().toString());
                dto.setGuidNo(StrUtil.toString(guiNoCount));

                if (StringUtils.isNotEmpty(saleH.getGsshCrFlag())) {
                    dto.setMatType(MaterialTypeEnum.ZHONG_YAO_DAI_JIAN.getCode());
                } else {
                    dto.setMatType(MaterialTypeEnum.LING_SHOU.getCode());
                }

                dto.setMatPostDate(saleD.getGssdDate());
                dto.setMatHeadRemark("");
                dto.setMatProCode(saleD.getGssdProId());
                dto.setMatSiteCode(saleD.getGssdBrId());
                dto.setMatLocationCode("1000");
                dto.setMatLocationTo("");
                dto.setMatBatch("P00000000000");
                dto.setMatQty(new BigDecimal(saleD.getGssdQty()));
                dto.setMatPrice(saleD.getGssdPrc2());
                dto.setMatUnit("盒");
                dto.setMatDebitCredit("H");
                dto.setMatPoId(saleH.getGsshBillNo());
                dto.setMatPoLineno(saleD.getGssdSerial());
                dto.setMatDnId(saleH.getGsshBillNo());
                dto.setMatDnLineno(saleD.getGssdSerial());
                dto.setMatLineRemark("");
                dto.setMatCreateBy(saleH.getGsshEmp());
                dto.setMatCreateDate(saleD.getGssdDate());
                dto.setMatCreateTime(saleH.getGsshTime());
                materialList.add(dto);

                guiNoCount++;
            }

            log.info("fixInsertMaterialDoc:{}", JSON.toJSONString(materialList));
            String result = purchaseService.insertMaterialDoc(materialList);
            log.info("fixInsertMaterialDoc success result:{}", JSON.toJSONString(result));
            JSONObject jsonObject = JSONObject.parseObject(result);
            if (!"0".equals(jsonObject.getString("code"))) {
                log.info("fixInsertMaterialDoc fail result:{}", JSON.toJSONString(result));
            }
        }
        return JsonResult.success(null, "提示：获取数据成功！");
    }
}
