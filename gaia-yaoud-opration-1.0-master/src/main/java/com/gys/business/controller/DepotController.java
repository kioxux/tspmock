//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.DepotService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "退库（供应商）", description = "/depot")
@RestController
@RequestMapping({"/depot/"})
public class DepotController extends BaseController {
    @Resource
    private DepotService depotService;

    public DepotController() {
    }

    @ApiOperation(value = "退库信息查询" , response = GetDepotOutData.class)
    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody GetDepotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.depotService.selectList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "修改退库商品信息" , response = GetDepotOutData.class)
    @PostMapping({"detail"})
    public JsonResult detail(HttpServletRequest request, @RequestBody GetDepotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.depotService.detail(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "废弃" , response = GetDepotOutData.class)
    @PostMapping({"detailList"})
    public JsonResult detailList(HttpServletRequest request, @RequestBody GetDepotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.depotService.detailList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "退库信息录入")
    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody GetDepotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setToken(request.getHeader("X-Token"));
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        inData.setGsrdhBrId(userInfo.getDepId());
        return JsonResult.success(this.depotService.insert(inData, userInfo), "提示：保存成功！");
    }

    @ApiOperation(value = "修改退库商品")
    @PostMapping({"update"})
    public JsonResult update(HttpServletRequest request, @RequestBody GetDepotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setToken(request.getHeader("X-Token"));
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        this.depotService.update(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "新增退库商品查询", response = GetQueryProVoOutData.class)
    @PostMapping({"queryPro"})
    public JsonResult queryPro(HttpServletRequest request, @RequestBody GetProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.depotService.queryPro(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "退库信息审核")
    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody GetDepotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        this.depotService.approve(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

    @ApiOperation(value = "导入", response = GetQueryProVoOutData.class)
    @PostMapping({"exportIn"})
    public JsonResult exportIn(HttpServletRequest request, @RequestBody GetDepotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.depotService.exportIn(inData), "提示：更新成功！");
    }

    @ApiOperation(value = "修改退库商品查询", response = GetQueryProVoOutData.class)
    @PostMapping({"getPoDetail"})
    public JsonResult getPoDetail(HttpServletRequest request, @RequestBody GetDepotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.depotService.getPoDetail(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "批次商品信息查询(废弃)", response = GetStockBatchProOutData.class)
    @PostMapping({"getStockBatchPro"})
    public JsonResult getStockBatchPro(HttpServletRequest request, @RequestBody GetStockBatchProInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssbBrId(userInfo.getDepId());
        return JsonResult.success(this.depotService.getStockBatchPro(inData), "提示：获取数据成功！");
    }
}
