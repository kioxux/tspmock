//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.ExamineService;
import com.gys.business.service.data.*;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/examine/"})
public class ExamineController extends BaseController {
    @Resource
    private ExamineService examineService;

    public ExamineController() {
    }

    @ApiOperation(value = "验收信息查询",response = GetAcceptOutData.class)
    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsehBrId(userInfo.getDepId());
        return JsonResult.success(this.examineService.selectList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "web验收信息查询",response = GetAcceptOutData.class)
    @PostMapping({"examineList"})
    public JsonResult examineList(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsehBrId(userInfo.getDepId());
        return JsonResult.success(this.examineService.selectExamineList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "调取收货单接口",response = GetAcceptOutData.class)
    @PostMapping({"acceptList"})
    public JsonResult acceptList(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsehBrId(userInfo.getDepId());
        return JsonResult.success(this.examineService.acceptList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "调取收货单接口CS",response = GetAcceptOutData.class)
    @PostMapping({"acceptCSList"})
    public JsonResult acceptCSList(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsehBrId(userInfo.getDepId());
        return JsonResult.success(this.examineService.acceptCSList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "验收明细",response = GetExamineDetailOutData.class)
    @PostMapping({"detailList"})
    public JsonResult detailList(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.examineService.detailList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "web验收明细",response = GetExamineDetailOutData.class)
    @PostMapping({"examineDetailList"})
    public JsonResult examineDetailList(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.examineService.examineDetailList(inData), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "保存验收商品信息")
    @PostMapping({"save"})
    public JsonResult save(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.examineService.save(inData), "提示：保存成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "保存商品信息成功后审核")
    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        this.examineService.approve(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "验收审核")
    @PostMapping({"approveEx"})
    public JsonResult approveEx(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsehBrId(userInfo.getDepId());
        inData.setStoreCode(userInfo.getDepId());
        inData.setGsedEmp(userInfo.getUserId());
        this.examineService.approveEx(inData,userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "验收记录查询",response = ExamineOrderTotalOutData.class)
    @PostMapping({"examineOrderList"})
    public JsonResult examineOrderList(HttpServletRequest request, @RequestBody ExamineOrderInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.examineService.examineOrderList(inData,userInfo), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "验收驳回")
    @PostMapping({"rejectAcceptOreder"})
    public JsonResult rejectAcceptOreder(HttpServletRequest request, @RequestBody GetExamineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsehBrId(userInfo.getDepId());
        this.examineService.rejectAcceptOreder(inData);
        return JsonResult.success("", "提示：操作成功！");
    }
}
