package com.gys.business.controller;

import cn.hutool.core.collection.CollUtil;
import com.gys.business.mapper.entity.MibSettlementZjnb;
import com.gys.business.service.*;
import com.gys.business.service.data.MibSettlementZjnbDto;
import com.gys.business.service.data.zjnb.MibProductMatchDto;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping({"zjnbYb"})
public class ZjnbYbController extends BaseController {

    @Autowired
    private ZjnbYbService zjnbYbService;


    //保存销售数据
    @PostMapping({"insertSaleInfo"})
    public JsonResult insertSaleInfo(HttpServletRequest request, @RequestBody Map<String,Object> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("stoCode", userInfo.getDepId());
        zjnbYbService.insertSaleInfo(inData);
        return JsonResult.success();
    }

    @PostMapping({"selectSaleInfo"})
    public JsonResult selectSaleInfo(HttpServletRequest request, @RequestBody Map<String,Object> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("stoCode", userInfo.getDepId());
        List<MibSettlementZjnb> mibSettlementZjnbList =  zjnbYbService.selectSaleInfo(inData);
        return JsonResult.success(mibSettlementZjnbList);
    }

    @PostMapping({"selectSaleInfoSum"})
    public JsonResult selectSaleInfoSum(HttpServletRequest request, @RequestBody Map<String,Object> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("stoCode", userInfo.getDepId());
        MibSettlementZjnb mibSettlementZjnbSum = zjnbYbService.selectSaleInfoSum(inData);
        return JsonResult.success(mibSettlementZjnbSum);
    }

    @PostMapping({"getNBMibCodeByCode"})
    public JsonResult getNBMibCodeByCode(HttpServletRequest request, @RequestBody Map<String,Object> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("brId", userInfo.getDepId());
        List<MibProductMatchDto> mibSettlementZjnbSum = zjnbYbService.getNBMibCodeByCode(inData);
        return JsonResult.success(mibSettlementZjnbSum);
    }
    @PostMapping({"submitMibMatchPro"})
    public JsonResult submitMibMatchPro(HttpServletRequest request, @RequestBody Map<String,Object> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("brId", userInfo.getDepId());
        zjnbYbService.submitMibMatchPro(userInfo.getDepId(),(List<MibProductMatchDto>)inData.get("matchList"));
        return JsonResult.success();
    }

    @PostMapping({"submitMibMatchPro2"})
    public JsonResult submitMibMatchPro2(HttpServletRequest request, @RequestBody Map<String,Object> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("brId", userInfo.getDepId());
        zjnbYbService.submitMibMatchPro2(userInfo.getDepId(),(List<MibProductMatchDto>)inData.get("matchList"));
        return JsonResult.success();
    }

    @PostMapping({"updateDetail"})
    public JsonResult updateDetail(HttpServletRequest request, @RequestBody MibSettlementZjnb inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        zjnbYbService.updateDetail(inData);
        return JsonResult.success();
    }
    @PostMapping({"getNbybProCatalog"})
    public JsonResult getNbybProCatalog(HttpServletRequest request, @RequestBody Map inData) {
        return JsonResult.success(zjnbYbService.getNbybProCatalog(inData));
    }
    @PostMapping({"getNbybProCatalogOne"})
    public JsonResult getNbybProCatalogOne(HttpServletRequest request, @RequestBody Map inData) {
        return JsonResult.success(zjnbYbService.getNbybProCatalogOne(inData));
    }
    @PostMapping({"getNBMibCodeByWzbmCode"})
    public JsonResult getNBMibCodeByWzbmCode(HttpServletRequest request, @RequestBody Map<String,Object> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("brId", userInfo.getDepId());
        List<MibProductMatchDto> mibSettlementZjnbSum = zjnbYbService.getNBMibCodeByWzbmCode(inData);
        return JsonResult.success(mibSettlementZjnbSum);
    }

    @PostMapping({"saveNBMibStockCode"})
    public JsonResult saveNBMibStockCode(HttpServletRequest request, @RequestBody Map<String,Object> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("brId", userInfo.getDepId());
        zjnbYbService.saveNBMibStockCode(inData);
        return JsonResult.success("维护成功");
    }


    @PostMapping({"accessToHealthCareInformation"})
    public JsonResult accessToHealthCareInformation(HttpServletRequest request, @RequestBody Map<String,String> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("brId", userInfo.getDepId());
        MibSettlementZjnb  access = zjnbYbService.accessToHealthCareInformation(inData);
        return JsonResult.success(access);
    }

    @PostMapping({"retypeItInvoice"})
    public JsonResult retypeItInvoice(HttpServletRequest request, @RequestBody Map<String,String> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("client", userInfo.getClient());
        inData.put("brId", userInfo.getDepId());
        MibSettlementZjnbDto access = zjnbYbService.retypeItInvoice(inData);
        return JsonResult.success(access);
    }

    @PostMapping({"saveMibZjnb"})
    public JsonResult saveMibZjnb(HttpServletRequest request, @RequestBody MibSettlementZjnb zjnb){
        zjnbYbService.insert(zjnb);
        return JsonResult.success("维护成功");
    }
}
