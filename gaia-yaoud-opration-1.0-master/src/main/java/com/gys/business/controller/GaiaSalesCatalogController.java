package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.gys.business.mapper.entity.GaiaSalesCatalog;
import com.gys.business.service.GaiaSalesCatalogService;
import com.gys.business.service.data.GaiaSalesCatalogInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 销售目录维护表(GaiaSalesCatalog)表控制层
 *
 * @author makejava
 * @since 2021-08-25 10:51:56
 */
@Slf4j
@RestController
@RequestMapping("gaiaSalesCatalog")
public class GaiaSalesCatalogController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaSalesCatalogService gaiaSalesCatalogService;

    @ApiOperation(value = "新增")
    @PostMapping({"/insertInfo"})
    public JsonResult insert(HttpServletRequest request, @RequestBody GaiaSalesCatalog inData) {
        log.info(String.format("<商品销售目录维护><新增><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setCreateUser(userInfo.getUserId());
        return JsonResult.success(gaiaSalesCatalogService.insertInfo(inData), "提示：保存数据成功！");
    }

    @ApiOperation(value = "查询销售目录列表")
    @PostMapping({"/list"})
    public JsonResult list(HttpServletRequest request, @RequestBody GaiaSalesCatalogInData inData) {
        log.info(String.format("<商品销售目录维护><查询销售目录列表><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(gaiaSalesCatalogService.list(inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "删除")
    @PostMapping({"/deleteBatch"})
    public JsonResult deleteBatch(HttpServletRequest request, @RequestBody GaiaSalesCatalogInData inData) {
        log.info(String.format("<商品销售目录维护><删除><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUpdateUser(userInfo.getUserId());
        return JsonResult.success(gaiaSalesCatalogService.deleteBatch(inData), "提示：删除数据成功！");
    }

    @ApiOperation(value = "编辑状态")
    @PostMapping({"/updateStatus"})
    public JsonResult updateStatus(HttpServletRequest request, @RequestBody GaiaSalesCatalogInData inData) {
        log.info(String.format("<商品销售目录维护><编辑状态><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUpdateUser(userInfo.getUserId());
        return JsonResult.success(gaiaSalesCatalogService.updateStatus(inData), "提示：修改数据成功！");
    }

    @ApiOperation(value = "是否启用该功能")
    @PostMapping({"/enableFunction"})
    public JsonResult enableFunction(HttpServletRequest request, @RequestBody GaiaSalesCatalogInData inData) {
        log.info(String.format("<商品销售目录维护><启用功能><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUpdateUser(userInfo.getUserId());
        return JsonResult.success(gaiaSalesCatalogService.enableFunction(inData), "提示：修改数据成功！");
    }

    @ApiOperation(value = "查询功能开关")
    @PostMapping({"/getParam"})
    public JsonResult getParam(HttpServletRequest request, @RequestBody GaiaSalesCatalogInData inData) {
        log.info(String.format("<商品销售目录维护><查询功能开关><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(gaiaSalesCatalogService.getParam(inData), "提示：修改数据成功！");
    }

    @ApiOperation(value = "批量导入-新增")
    @PostMapping("/batchImport")
    public JsonResult batchImport(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        GetLoginOutData userInfo = getLoginUser(request);
        return JsonResult.success(gaiaSalesCatalogService.batchImport(file,userInfo), "提示：导入数据成功！");
    }
}
