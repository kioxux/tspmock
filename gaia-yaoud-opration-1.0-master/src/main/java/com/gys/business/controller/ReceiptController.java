package com.gys.business.controller;

import com.gys.business.service.ReceiptService;
import com.gys.business.service.data.ReceiptInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 小票请求控制器
 *
 * @author xiaoyuan on 2020/8/3
 */
@RestController
@RequestMapping({"/receipt"})
public class ReceiptController extends BaseController {

    @Autowired
    private ReceiptService receiptService;


    @PostMapping({"/receipt"})
    public JsonResult findReceipt(HttpServletRequest request, @RequestBody ReceiptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(receiptService.findReceipt(inData), "提示：保存成功！");
    }

    @PostMapping({"/receiptPayType"})
    public JsonResult findTypeByOrderNumber(HttpServletRequest request, @RequestBody ReceiptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(receiptService.findTypeByOrderNumber(inData), "提示：保存成功！");
    }

}
