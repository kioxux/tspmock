//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.SaleScheduleService;
import com.gys.business.service.data.ConllectionInData;
import com.gys.business.service.data.GetSaleScheduleInData;
import com.gys.business.service.data.GetSaleScheduleQueryInData;
import com.gys.business.service.data.GetUserOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "销售计划制定" , description = "/saleScheduleSetting")
@RestController
@RequestMapping({"/saleScheduleSetting/"})
public class SaleScheduleSettingController extends BaseController {
    @Autowired
    private SaleScheduleService saleScheduleService;

    public SaleScheduleSettingController() {
    }

    @PostMapping({"selectByPage"})
    public JsonResult selectByPage(HttpServletRequest request, @RequestBody GetSaleScheduleQueryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.saleScheduleService.selectByPage(inData), "提示：获取数据成功！");
    }

    @PostMapping({"update"})
    public JsonResult update(@RequestBody ConllectionInData inData) {
        this.saleScheduleService.update(inData.getSaleScheduleInData());
        return JsonResult.success("", "提示：更新成功！");
    }

    @PostMapping({"delete"})
    public JsonResult delete(@RequestBody ConllectionInData inData) {
        this.saleScheduleService.delete(inData.getSaleScheduleInData());
        return JsonResult.success("", "提示：删除成功！");
    }

    @PostMapping({"addSave"})
    public JsonResult addSave(HttpServletRequest request, @RequestBody GetSaleScheduleInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsegBrId(userInfo.getDepId());
        this.saleScheduleService.addSave(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "查询收货人员",response = GetUserOutData.class)
    @PostMapping({"queryUser"})
    public JsonResult queryUser(HttpServletRequest request,@ApiParam(value = "查询条件", required = true) @RequestBody GetSaleScheduleInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsegBrId(userInfo.getDepId());
        return JsonResult.success(this.saleScheduleService.queryUser(inData), "提示：保存成功！");
    }
}
