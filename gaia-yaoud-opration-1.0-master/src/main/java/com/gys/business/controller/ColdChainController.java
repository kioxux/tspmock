package com.gys.business.controller;

import com.gys.business.service.ColdChainService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Api(tags = "国药冷链", description = "/coldChain")
@RestController
@RequestMapping({"/coldChain"})
public class ColdChainController extends BaseController {

    @Resource
    private ColdChainService coldChainService;

    /**
     * 冷链商品查询
     * @param request
     * @param inData
     * @return
     */
    @ApiOperation(value = "listColdChainGoods", response = InvoicingOutData.class)
    @PostMapping({"/listColdChainGoods"})
    public JsonResult listColdChainGoods(HttpServletRequest request, @RequestBody ColdChainInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(coldChainService.listColdChainGoods(inData), "提示：获取数据成功！");
    }
}
