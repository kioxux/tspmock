package com.gys.business.controller;

import com.gys.business.service.LocalSettingService;
import com.gys.business.service.data.LocalSettingInData;
import com.gys.business.service.data.LocalSettingOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/localSetting/"})
@Slf4j
@Api(tags = "基础资料")
public class LocalSettingController extends BaseController {
    @Resource
    private LocalSettingService gaiaSdStoreDataService;

    @ApiOperation(value = "获取本地设置", response = LocalSettingOutData.class)
    @PostMapping({"selectOne"})
    public JsonResult selectOne(HttpServletRequest request, @RequestBody LocalSettingInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsstBrId(userInfo.getDepId());
        return JsonResult.success(gaiaSdStoreDataService.selectOne(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "保存本地设置")
    @PostMapping({"saveOrUpDate"})
    public JsonResult saveOrUpDate(HttpServletRequest request, @RequestBody LocalSettingInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsstBrId(userInfo.getDepId());
        gaiaSdStoreDataService.saveOrUpDate(inData);
        return JsonResult.success("", "提示：更新成功！");
    }
}
