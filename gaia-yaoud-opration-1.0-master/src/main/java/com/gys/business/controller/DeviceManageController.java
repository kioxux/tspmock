package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.util.StringUtil;
import com.gys.business.service.DeviceCheckService;
import com.gys.business.service.DeviceInfoService;
import com.gys.business.service.data.DeviceInfoInData;
import com.gys.business.service.data.DeviceInfoOutData;
import com.gys.business.service.data.GetDeviceCheckInData;
import com.gys.business.service.data.GetDeviceCheckOutData;
import com.gys.business.service.data.device.DeviceForm;
import com.gys.business.service.data.device.MaintenanceForm;
import com.gys.business.service.data.device.UserInfoVO;
import com.gys.business.service.data.device.UserVO;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/***
 * @desc: 设备档案、设备维护控制类
 * @author: ryan
 * @createTime: 2021/6/24 16:36
 **/
@Api(tags = "质量管理-设备管理（设备档案、设备维护）")
@Slf4j
@RestController
@RequestMapping("/api/deviceManage")
public class DeviceManageController extends BaseController {
    @Resource
    private DeviceInfoService deviceInfoService;
    @Resource
    private DeviceCheckService deviceCheckService;

    @ApiOperation(value = "保存设备")
    @PostMapping("saveDevice")
    public JsonResult addDevice(HttpServletRequest request, @RequestBody List<DeviceInfoInData> list) {
        log.info(String.format("<设备管理><保存设备><请求参数：%s>", JSON.toJSONString(list)));
        validParams(list);
        GetLoginOutData loginUser = getLoginUser(request);
        deviceInfoService.save(list, loginUser);
        return JsonResult.success(null, "保存设备成功");
    }

    @ApiOperation(value = "设备列表", response = DeviceInfoOutData.class)
    @PostMapping("deviceList")
    public JsonResult<List<DeviceInfoOutData>> deviceList(HttpServletRequest request, @RequestBody DeviceForm deviceForm) {
        log.info(String.format("<设备管理><设备列表><请求参数：%s>", JSON.toJSONString(deviceForm)));
        GetLoginOutData loginUser = getLoginUser(request);
        deviceForm.setClient(loginUser.getClient());
        return JsonResult.success(deviceInfoService.findList(deviceForm), "查询成功");
    }

    @ApiOperation(value = "保存维护记录")
    @PostMapping("auditMaintenance")
    public JsonResult auditMaintenance(HttpServletRequest request, @RequestBody GetDeviceCheckInData deviceCheckInData) {
        if (deviceCheckInData.getGsdcStatus()!=null && deviceCheckInData.getGsdcStatus().equals("1")) {
            return JsonResult.fail(null, "已审核过的数据不可以重复审核");
        }
        validDeviceCheck(deviceCheckInData);
        log.info(String.format("<设备管理><保存维护记录><请求参数：%s>", JSON.toJSONString(deviceCheckInData)));
        GetLoginOutData loginUser = getLoginUser(request);
        fillDeviceCheckData(deviceCheckInData, loginUser);
        deviceCheckService.save(deviceCheckInData);
        return JsonResult.success(null, "保存设备维护记录成功");
    }

    @ApiOperation(value = "维护记录列表", response = GetDeviceCheckOutData.class)
    @PostMapping("maintenanceList")
    public JsonResult<List<GetDeviceCheckOutData>> maintenanceList(HttpServletRequest request, @RequestBody MaintenanceForm maintenanceForm) {
        log.info(String.format("<设备管理><维护记录列表><请求参数：%s>", JSON.toJSONString(maintenanceForm)));
        GetLoginOutData loginUser = getLoginUser(request);
        maintenanceForm.setClient(loginUser.getClient());
        List<GetDeviceCheckOutData> list = deviceCheckService.findList(maintenanceForm);
        return JsonResult.success(list, "设备维护记录查询成功");
    }

    @ApiOperation(value = "登记人员列表", response = UserVO.class)
    @PostMapping("userList")
    public JsonResult<List<UserVO>> userList(HttpServletRequest request) {
        log.info(String.format("<设备管理><登记人员><请求参数:无"));
        GetLoginOutData loginUser = getLoginUser(request);
        DeviceForm deviceForm = new DeviceForm();
        deviceForm.setClient(loginUser.getClient());
        deviceForm.setGsthBrId(loginUser.getDepId());
        List<UserVO> nameList = deviceInfoService.getUserNameList(deviceForm);
        return JsonResult.success(nameList, "查询登记人员列表成功");
    }

    @ApiOperation(value = "用户信息", response = UserInfoVO.class)
    @PostMapping("userInfo")
    public JsonResult userInfo(HttpServletRequest request) {
        GetLoginOutData loginUser = getLoginUser(request);
        UserInfoVO userInfoVO = new UserInfoVO();
        userInfoVO.setUserFlag(2);
        return JsonResult.success(userInfoVO, "查询用户信息成功");
    }


    private void fillDeviceCheckData(GetDeviceCheckInData inData, GetLoginOutData loginUser) {
        inData.setClientId(loginUser.getClient());
        if (loginUser.getDepId() != null) {
        inData.setGsdcBrId(loginUser.getDepId());
        }
        inData.setGsdcBrName(loginUser.getDepName());
        inData.setGsdcUpdateEmp(loginUser.getLoginName());
        inData.setGsdcUpdateDate(CommonUtil.getyyyyMMdd());
        inData.setGsdcUpdateTime(CommonUtil.getHHmmss());
        inData.setGsdcStatus("1");
    }

    private void validParams(List<DeviceInfoInData> list) {
        for (DeviceInfoInData inData : list) {
            if (StringUtil.isNotEmpty(inData.getGsdiName()) && inData.getGsdiName().length() > 20) {
                throw new BusinessException("设备名称长度不可超过20");
            }
            if (StringUtil.isNotEmpty(inData.getGsdiModel()) && inData.getGsdiModel().length() > 20) {
                throw new BusinessException("设备型号长度不可超过20");
            }
            if (StringUtil.isNotEmpty(inData.getGsdiFactory()) && inData.getGsdiFactory().length() > 50) {
                throw new BusinessException("生产企业长度不可超过50");
            }
        }
    }

    private void validDeviceCheck(GetDeviceCheckInData deviceCheckInData) {
        if (StringUtil.isNotEmpty(deviceCheckInData.getGsdcDeviceName()) && deviceCheckInData.getGsdcDeviceName().length() > 20) {
            throw new BusinessException("设备名称长度不可超过20");
        }
        if (StringUtil.isNotEmpty(deviceCheckInData.getGsdcDeviceModel()) && deviceCheckInData.getGsdcDeviceModel().length() > 20) {
            throw new BusinessException("设备型号长度不可超过50");
        }
        if (StringUtil.isNotEmpty(deviceCheckInData.getGsdcCheckRemarks()) && deviceCheckInData.getGsdcCheckRemarks().length() > 50) {
            throw new BusinessException("维护内容长度不可超过50");
        }
        if (StringUtil.isNotEmpty(deviceCheckInData.getGsdcCheckResult()) && deviceCheckInData.getGsdcCheckResult().length() > 50) {
            throw new BusinessException("检查情况长度不可超过50");
        }
        if (StringUtil.isNotEmpty(deviceCheckInData.getGsdcCheckStep()) && deviceCheckInData.getGsdcCheckStep().length() > 50) {
            throw new BusinessException("调控措施长度不可超过50");
        }
    }

}
