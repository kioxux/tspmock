package com.gys.business.controller;

import com.gys.business.service.GaiaSdPrintSaleService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/printSale"})
public class PrintSaleController extends BaseController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private GaiaSdPrintSaleService gaiaSdPrintSaleService;


    @PostMapping("/salesTicket")
    public JsonResult salesTicket() {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return gaiaSdPrintSaleService.getPrintSaleByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
    }

}
