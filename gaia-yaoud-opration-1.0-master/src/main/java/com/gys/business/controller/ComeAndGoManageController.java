package com.gys.business.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.gys.business.mapper.ComeAndGoManageMapper;
import com.gys.business.mapper.entity.GaiaComeAndGoDetailData;
import com.gys.business.mapper.entity.GaiaComeAndGoDetailImportData;
import com.gys.business.service.ComeAndGoManageService;
import com.gys.business.service.GaiaComeAndGoBatchImportInData;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.ExcelUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 15:16 2021/7/21
 * @Description：往来管理接口
 * @Modified By：guoyuxi.
 * @Version:
 */
@Api(tags = "往来管理")
@RestController
@RequestMapping("/comeAndGoManage")
public class ComeAndGoManageController extends BaseController {
    @Autowired
    private ComeAndGoManageService comeAndGoManageService;
    @Autowired
    private ComeAndGoManageMapper comeAndGoManageMapper;
    @Resource
    private CosUtils cosUtils;

    @ApiOperation(value = "往来明细查询", response = GaiaComeAndGoDetailData.class)
    @PostMapping({"getComeAndGoDetailList"})
    public JsonResult getComeAndGoDetailList(HttpServletRequest request,@RequestBody GaiaComeAndGoQueryData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        return JsonResult.success(comeAndGoManageService.getComeAndGoDetailList(inData),"查询成功");
    }

    @ApiOperation(value = "仓库列表查询")
    @PostMapping({"getDcList"})
    public JsonResult getDcList(HttpServletRequest request) {
        GetLoginOutData userInfo = getLoginUser(request);
        return comeAndGoManageService.getDcList(userInfo.getClient());
    }

    @ApiOperation(value = "门店列表查询")
    @PostMapping({"getStoList"})
    public JsonResult getStoList(HttpServletRequest request,@RequestBody Map<String,String> queryString) {
        GetLoginOutData loginUser = getLoginUser(request);
        return comeAndGoManageService.getStoList(loginUser.getClient(),queryString);
    }

    @ApiOperation(value = "查询支付方式")
    @PostMapping({"getPaymentList"})
    public JsonResult getPaymentList(HttpServletRequest request) {
        GetLoginOutData loginUser = getLoginUser(request);
        return comeAndGoManageService.findPaymentList(loginUser.getClient());
    }

    @ApiOperation(value = "保存支付方式")
    @PostMapping({"savePayment"})
    public JsonResult savePayment(HttpServletRequest request, @RequestBody List<GaiaComeAndGoSavePaymentInData> inData) {
        GetLoginOutData loginUser = this.getLoginUser(request);
        return comeAndGoManageService.batchSavePayment(inData,loginUser);
    }

    @ApiOperation(value = "移除支付方式")
    @PostMapping({"removePayment"})
    public JsonResult removePayment(HttpServletRequest request,@RequestBody List<GaiaComeAndGoSavePaymentInData> inData) {
        GetLoginOutData loginUser = this.getLoginUser(request);
        return comeAndGoManageService.batchRemovePayment(inData,loginUser);
    }

    @ApiOperation(value = "手动批量录入往来明细")
    @PostMapping({"batchSaveComeAndGoDetail"})
    public JsonResult batchSaveComeAndGoDetail(HttpServletRequest request, @RequestBody List<GaiaComeAndGoDetailData> detailList) {

        GetLoginOutData loginUser = this.getLoginUser(request);
        return comeAndGoManageService.batchSaveComeAndGoDetail(detailList,loginUser);
    }

    @ApiOperation(value = "查询门店期初日期")
    @PostMapping({"getStartDate"})
    public JsonResult getStartDate(HttpServletRequest request,@RequestBody Map<String,String> map) {
        return comeAndGoManageService.getStartDate(this.getLoginUser(request).getClient(),map);
    }

    @ApiOperation(value = "批量导入往来明细")
    @PostMapping({"comeAndGoBatchImportExcel"})
    public JsonResult comeAndGoBatchImportExcel(HttpServletRequest request, @RequestParam("file") MultipartFile file) {

        GaiaComeAndGoBatchImportInData inData = new GaiaComeAndGoBatchImportInData();
        int begin = file.getOriginalFilename().indexOf(".");
        int last = file.getOriginalFilename().length();
//获得文件后缀名
        String a = file.getOriginalFilename().substring(begin, last);
        if (!a.endsWith(".xls")) {
            throw new BusinessException("只支持上传xls文件");
        }
        try {
            //取数据
            List<GaiaComeAndGoDetailImportData> dataList = importExcel(file, GaiaComeAndGoDetailImportData.class, 0, 1);
            //List<GaiaComeAndGoDetailData> importInDataList = getRowAndCell(sheet);
            List<GaiaComeAndGoDetailData> importInDataList=new ArrayList<>();
            for (GaiaComeAndGoDetailImportData data : dataList) {
                GaiaComeAndGoDetailData detailData = new GaiaComeAndGoDetailData();
                BeanUtil.copyProperties(data,detailData);
                detailData.setAmountOfPayment(StrUtil.isEmpty(data.getAmountOfPayment()) ?BigDecimal.ZERO:new BigDecimal(data.getAmountOfPayment()));
                detailData.setAmountOfPaymentRealistic(StrUtil.isEmpty(data.getAmountOfPaymentRealistic())?BigDecimal.ZERO:new BigDecimal(data.getAmountOfPaymentRealistic()));
                importInDataList.add(detailData);
            }
            inData.setImportInDataList(importInDataList);
        }catch (Exception e){
            e.printStackTrace();
        }
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return comeAndGoManageService.comeAndGoBatchImportExcel(inData);
    }

    @ApiOperation(value = "查询往来汇总列表数据")
    @PostMapping({"getComeAndGoSummaryList"})
    public JsonResult getComeAndGoSummaryList(HttpServletRequest request,@RequestBody GaiaComeAndGoSummeryQueryData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        return JsonResult.success(comeAndGoManageService.getComeAndGoSummaryList(inData),"查询成功");
    }

    @ApiModelProperty(value = "excel模板下载")
    @RequestMapping(value = "/export")
    public void exportSysTest(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Map<String,Object>> rows = new ArrayList<>();
            List<String[]> headers = new ArrayList<>();
            List<List<List<String>>> datas = Lists.newArrayList();
            String[] desc={"仓库编码","仓库名称","门店编码","门店名称","支付方式编码","支付方式名称","支付金额","扣率","实际支付金额","支付日期","备注"};
            String excelName = "DetailTemplate";
            String headerStr = "仓库编码,仓库名称,门店编码,门店名称,支付方式编码,支付方式名称,支付金额,扣率,实际支付金额,支付日期,备注";
            headers.add(desc);
            List<String> sheetNameList=new ArrayList<>();
            sheetNameList.add(excelName);
            HSSFWorkbook wb = ExcelUtils.exportExcelByTemplate(headers,sheetNameList);
            File tempFile = File.createTempFile("vehicle", ".csv");
//            CsvWriter csvWriter = new CsvWriter(tempFile.getCanonicalPath(),',', Charset.forName("UTF-8"));
            OutputStream ouputStream = response.getOutputStream();
            byte[] bytes = headerStr.getBytes("GBK");

            /*response.reset();*/
            response.setHeader("Content-disposition", "attachment;filename=DetailTemplate.csv");
            response.setContentType("application/csv;charset=GBK");
            ouputStream.write(bytes);
            ouputStream.flush();
            ouputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiModelProperty(value = "导出往来明细")
    @RequestMapping(value = "/exportDetail")
    public Result exportDetail(HttpServletRequest request, @RequestBody GaiaComeAndGoQueryData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        PageInfo pageInfo = comeAndGoManageService.getComeAndGoDetailList(inData);
        List<GaiaComeAndGoDetailData> list = pageInfo.getList();
        list.forEach(detail->{
            detail.setPaymentId(detail.getPaymentId()+"\t");
        });
        Map<String,Object> totalMap= (Map<String, Object>) pageInfo.getListNum();
        GaiaComeAndGoDetailData gaiaComeAndGoDetailData = new GaiaComeAndGoDetailData();
        gaiaComeAndGoDetailData.setDcCode("合计");
        if (totalMap.get("amountOfPayment") != null) {
        gaiaComeAndGoDetailData.setAmountOfPayment(new BigDecimal(totalMap.get("amountOfPayment").toString()));
        }
        if (totalMap.get("amountOfPaymentRealistic") != null) {
        gaiaComeAndGoDetailData.setAmountOfPaymentRealistic(new BigDecimal(totalMap.get("amountOfPaymentRealistic").toString()));
        }
        if (totalMap.get("amountRate") != null) {
        gaiaComeAndGoDetailData.setAmountRate(totalMap.get("amountRate").toString());
        }
        String fileName = "往来管理明细导出";
        List outData=new ArrayList();
        outData.addAll(list);
        outData.add(gaiaComeAndGoDetailData);
        if (outData.size() > 0) {
            CsvFileInfo csvInfo =null;
            // 导出
            // byte数据
            csvInfo = CsvClient.getCsvByte(outData,fileName, Collections.singletonList((short) 1));

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据!");
        }
    }

    @ApiModelProperty(value = "导出往来汇总")
    @RequestMapping(value = "/exportSummary")
    public Result exportSummary(HttpServletRequest request, @RequestBody GaiaComeAndGoSummeryQueryData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        PageInfo pageInfo = comeAndGoManageService.getComeAndGoSummaryList(inData);
        List list = pageInfo.getList();
        Map<String,Object> totalMap= (Map<String, Object>) pageInfo.getListNum();

        List<LinkedHashMap<String,Object>> outData=new ArrayList<>();
        outData.addAll(list);
        LinkedHashMap<String,Object> totalMapData=new LinkedHashMap<>();
        totalMap.put("stoCode","合计");
        totalMapData.putAll(totalMap);
        outData.add(totalMapData);
        String fileName = "往来管理汇总导出";

        if (outData.size() > 0) {
            CsvFileInfo csvInfo =null;
            // 导出
            // byte数据
            Map<String,String> titleMap=new LinkedHashMap<>();
            titleMap.put("stoCode","门店");
            titleMap.put("stoName","门店名称");
            titleMap.put("residualAmt","期初金额");
            List<Map<String, String>> paymentMethodsByClient = comeAndGoManageMapper.findPaymentMethodsByClient(userInfo.getClient());
            paymentMethodsByClient.forEach(paymentMap->{
                titleMap.put(paymentMap.get("PAYMENT_ID"),paymentMap.get("PAYMENT_NAME"));
            });
            titleMap.put("endAmt","期末金额");

            csvInfo = CsvClient.getCsvByteByMap(outData,fileName,titleMap);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据!");
        }
    }


    @ApiOperation(value = "往来明细-添加NDMD计算逻辑")
    @GetMapping({"/ndmd"})
    public JsonResult ndmd() {
        comeAndGoManageService.autoComputeDistributionDataAndAddComeAndGoDetail();
        return JsonResult.success("","运算成功");
    }


}
