package com.gys.business.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/25 19:55
 */
@RequestMapping("health")
@RestController
public class HealthCheckController {

    @RequestMapping("check")
    public void check(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.getWriter().print("ok");
    }

}
