package com.gys.business.controller;

import com.gys.business.service.IRecommendFirstPlanService;
import com.gys.business.service.data.RecommendChooseProductInData;
import com.gys.business.service.data.RecommendFirstPlanInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 首推商品方案表 前端控制器
 * </p>
 *
 * @author flynn
 * @since 2021-08-04
 */
@RestController
@Api(tags = "首推商品方案表接口")
@RequestMapping("/recommendFirstPlan")
public class RecommendFirstPlanController extends BaseController {


    @Autowired
    private IRecommendFirstPlanService recommendFirstPlanService;

    @GetMapping("/buildInit")
    @ApiOperation(value = "首推商品方案表新增初始化", notes = "首推商品方案表新增初始化")
    public JsonResult buildInit(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.buildInit(userInfo), "提示：获取数据成功！");
    }

    @PostMapping("/chooseProduct")
    @ApiOperation(value = "首推商品方案表新增", notes = "首推商品方案表新增")
    public JsonResult chooseProduct(HttpServletRequest request, @RequestBody RecommendChooseProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.chooseProduct(userInfo, inData), "提示：保存成功！");
    }

    @PostMapping("/save")
    @ApiOperation(value = "首推商品方案表新增", notes = "首推商品方案表新增")
    public JsonResult save(HttpServletRequest request, @RequestBody RecommendFirstPlanInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.build(userInfo, inData), "提示：保存成功！");
    }

    @GetMapping("/audit/{id}")
    @ApiOperation(value = "首推商品方案审核", notes = "首推商品方案审核")
    public JsonResult audit(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.audit(userInfo, id), "提示：审核成功！");
    }

    @GetMapping("/stop/{id}")
    @ApiOperation(value = "首推商品方案审核", notes = "首推商品方案审核")
    public JsonResult stop(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.stop(userInfo, id), "提示：已停用！");
    }

    @GetMapping("/editInit/{id}")
    @ApiOperation(value = "首推商品方案表编辑初始化", notes = "首推商品方案表编辑初始化")
    public JsonResult updateInit(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.updateInit(userInfo, id), "提示：获取数据成功！");
    }

    @PostMapping("/edit")
    @ApiOperation(value = "首推商品方案表修改", notes = "首推商品方案表修改")
    public JsonResult update(HttpServletRequest request, @RequestBody RecommendFirstPlanInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.update(userInfo, inData), "提示：修改成功！");
    }


    @GetMapping("/delete/{id}")
    @ApiOperation(value = "首推商品方案表删除", notes = "首推商品方案表删除")
    public JsonResult delete(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.delete(userInfo, id), "提示：删除成功！");
    }

    @GetMapping("/copy/{id}")
    @ApiOperation(value = "首推商品方案表复制", notes = "首推商品方案表复制")
    public JsonResult copy(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.copy(userInfo, id), "提示：复制成功！");
    }


    @PostMapping("/list")
    @ApiOperation(value = "首推商品方案表查询（分页）", notes = "首推商品方案表查询（分页）")
    public JsonResult listPage(HttpServletRequest request, @RequestBody RecommendFirstPlanInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(recommendFirstPlanService.getListPage(userInfo, inData), "提示：查询成功！");
    }

}

