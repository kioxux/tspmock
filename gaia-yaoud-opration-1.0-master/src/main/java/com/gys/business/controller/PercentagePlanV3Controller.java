package com.gys.business.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.service.ImportPercentageData;
import com.gys.business.service.PercentagePlanV3Service;
import com.gys.business.service.data.percentageplan.PercentageInData;
import com.gys.business.service.data.percentageplan.PercentageOutData;
import com.gys.business.service.data.percentageplan.PercentageProInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@RestController
@RequestMapping({"/percentage/plan/v3/"})
@Api(tags = "提成方案V3相关")
@Slf4j
public class PercentagePlanV3Controller extends BaseController {
    @Autowired
    private PercentagePlanV3Service service;

    @ApiOperation(value = "新增/修改提成方案,(试算-deleteFlag = 2 必填)")
    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody PercentageInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setPlanCreaterId(userInfo.getUserId());
        inData.setPlanCreater(userInfo.getLoginName());
        return JsonResult.success(service.insert(inData), "提示：操作数据成功！");
    }

    @ApiOperation(value = "提成方案列表查询",response = PercentageOutData.class)
    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody PercentageInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(service.list(inData), "提示：操作数据成功！");
    }

    @ApiOperation(value = "提成方案详情查询，入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成)",response = PercentageInData.class)
    @PostMapping({"tichengDetail"})
    public JsonResult tichengDetail(@RequestBody Map<String,Long> inData) {
        return JsonResult.success(service.tichengDetail(inData.get("planId"),inData.get("planType")), "提示：操作数据成功！");
    }

//    @ApiOperation(value = "删除销售提成明细，入参：saleId(销售提成明细主键ID),planType(提成方案类型 1 销售提成 2 单品提成)")
//    @PostMapping({"deleteSalePlan"})
//    public JsonResult deleteSalePlan(@RequestBody Map<String,Long> inData ) {
////        service.deleteSalePlan(inData.get("saleId"),inData.get("saleId"));
//        return JsonResult.success("", "提示：操作数据成功！");
//    }
//
//    @ApiOperation(value = "删除单品提成明细，入参：proPlanId(单品提成明细主键ID)")
//    @PostMapping({"deleteProPlan"})
//    public JsonResult deleteProPlan(@RequestBody Map<String,Long> inData) {
////        service.deleteProPlan(inData.get("proPlanId"));
//        return JsonResult.success("", "提示：操作数据成功！");
//    }


    @ApiOperation(value = "方案审核,入参：planId(提成方案主键ID),planStatus(审核状态 0 已保存 1 已审核 2 已停用),planType(提成方案类型 1 销售提成 2 单品提成)")
    @PostMapping({"approve"})
    public JsonResult approve(@RequestBody Map<String,String> inData) {
        service.approve(Long.valueOf(inData.get("planId")),inData.get("planStatus"),inData.get("planType"));
        return JsonResult.success("", "提示：审核成功！");
    }

    @ApiOperation(value = "删除方案,入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成)")
    @PostMapping({"deletePlan"})
    public JsonResult deletePlan(@RequestBody Map<String,String> inData) {
        service.deletePlan(Long.valueOf(inData.get("planId")),inData.get("planType"));
        return JsonResult.success("", "提示：删除成功！");
    }

    @ApiOperation(value = "商品查询,入参：proCode(商品编码)")
    @PostMapping({"selectProductByClient"})
    public JsonResult selectProductByClient(HttpServletRequest request, @RequestBody Map<String,String> inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return JsonResult.success(service.selectProductByClient(userInfo.getClient(),inData.get("proCode")), "提示：查询成功！");
//        return JsonResult.success("", "提示：查询成功！");
    }

    @ApiOperation(value = "商品查询,入参：proCodes(商品编码)")
    @PostMapping({"selectProductByClientProCodes"})
    public JsonResult selectProductByClientProCodes(HttpServletRequest request, @RequestBody Map<String,List<String>> inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return JsonResult.success(service.selectProductByClientProCodes(userInfo.getClient(),inData.get("proCodes")), "提示：查询成功！");
//        return JsonResult.success("", "提示：查询成功！");
    }

    @ApiOperation(value = "导入信息查询", response = PercentageProInData.class)
    @PostMapping({"getImportExcelDetailList"})
    public JsonResult getImportExcelDetailList(HttpServletRequest request, @RequestParam("file") MultipartFile file, @RequestParam("chooseProCodeStr") String chooseProCodeStr) {
        List<ImportPercentageData> importInDataList = new ArrayList<>();
        try {


//            String rootPath = request.getSession().getServletContext().getRealPath("/");
//            String fileName = file.getOriginalFilename();
//            String path = rootPath + fileName;
//            File newFile = new File(path);
//            //判断文件是否存在
//            if(!newFile.exists()) {
//                newFile.mkdirs();//不存在的话，就开辟一个空间
//            }
//            //将上传的文件存储
//            file.transferTo(newFile);
            InputStream in = file.getInputStream();
            //导入已存在的Excel文件，获得只读的工作薄对象
            XSSFWorkbook wk = new XSSFWorkbook(in);
            //获取第一张Sheet表
            Sheet sheet = wk.getSheetAt(0);
            //取数据
            importInDataList = getRowAndCell(sheet);
            if(StrUtil.isNotBlank(chooseProCodeStr)){
                //添加校验逻辑,校验重复编码导入
                List<String> chooseProCodes = Arrays.asList(chooseProCodeStr.split(","));
                if (CollectionUtil.isNotEmpty(chooseProCodes) && CollectionUtil.isNotEmpty(importInDataList)) {
                    //chooseProCodes为前端已经选择的数据，新导入的需要去掉重复
                    StringBuilder builder = new StringBuilder();
                    builder.append("");
                    int i = 1;
                    for (ImportPercentageData importData : importInDataList) {
                        if(chooseProCodes.contains(importData.getProCode())){
                            builder.append("第" + i + "行"+ importData.getProCode() + "编码重复;");
                        }
                        i++;
                    }
                    if(StrUtil.isNotBlank(builder.toString())){
                        throw new BusinessException(builder.toString());
                    }
                }
            }


//            inData.setImportInDataList(importInDataList);
            in.close();
            wk.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.service.getImportExcelDetailList(userInfo.getClient(), importInDataList), "提示：获取成功！");

//        return JsonResult.success("", "提示：获取成功！");
    }

//    @ApiOperation(value = "导入信息查询",response = PercentageProInData.class)
//    @PostMapping({"getImportExcelDetailList"})
//    public JsonResult getImportExcelDetailList(HttpServletRequest request,@RequestParam("file") MultipartFile file) {
//        List<ImportPercentageData> importInDataList = new ArrayList<>();
//        try {
////            String rootPath = request.getSession().getServletContext().getRealPath("/");
////            String fileName = file.getOriginalFilename();
////            String path = rootPath + fileName;
////            File newFile = new File(path);
////            //判断文件是否存在
////            if(!newFile.exists()) {
////                newFile.mkdirs();//不存在的话，就开辟一个空间
////            }
////            //将上传的文件存储
////            file.transferTo(newFile);
//            InputStream in = file.getInputStream();
//            //导入已存在的Excel文件，获得只读的工作薄对象
//            XSSFWorkbook wk = new XSSFWorkbook(in);
//            //获取第一张Sheet表
//            Sheet sheet = wk.getSheetAt(0);
//            //取数据
//            importInDataList = getRowAndCell(sheet);
////            inData.setImportInDataList(importInDataList);
//            in.close();
//            wk.close();
//        }catch (FileNotFoundException e){
//            e.printStackTrace();
//        }catch (IOException e){
//            e.printStackTrace();
//        }
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        return JsonResult.success(this.service.getImportExcelDetailList(userInfo.getClient(),importInDataList), "提示：获取成功！");
//
////        return JsonResult.success("", "提示：获取成功！");
//    }

    public List<ImportPercentageData> getRowAndCell(Sheet sheet) {
        List<ImportPercentageData> importInDataList = new ArrayList<>();
        String msg = "";
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        System.out.println("总行数==" + totalRows);
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            for (int i = 1; i <= totalRows; i++) {
                Row row = sheet.getRow(i); //取出一行数据放入row
                ImportPercentageData newExpert = new ImportPercentageData();
                newExpert.setProCode(dataFormatter.formatCellValue(row.getCell(0)).trim());
                String saleQty = dataFormatter.formatCellValue(row.getCell(1)).trim();
                String tichengAmt = dataFormatter.formatCellValue(row.getCell(2)).trim();
                String tichengRate = dataFormatter.formatCellValue(row.getCell(3)).trim();
                String saleQty2 = dataFormatter.formatCellValue(row.getCell(4)).trim();
                String tichengAmt2 = dataFormatter.formatCellValue(row.getCell(5)).trim();
                String tichengRate2 = dataFormatter.formatCellValue(row.getCell(6)).trim();
                String saleQty3 = dataFormatter.formatCellValue(row.getCell(7)).trim();
                String tichengAmt3 = dataFormatter.formatCellValue(row.getCell(8)).trim();
                String tichengRate3 = dataFormatter.formatCellValue(row.getCell(9)).trim();
                if (StringUtils.isEmpty(saleQty)){
                    msg = msg + "第" + i + "行达到数量1未填;";
                }else {
                    newExpert.setSeleQty(saleQty);
                }
                if (StringUtils.isNotEmpty(tichengAmt) && StringUtils.isNotEmpty(tichengRate)){
                    msg = msg + "第" + i + "行提成金额1与提成比例1二选一填写;";
                }
                if (StringUtils.isNotEmpty(tichengAmt2) || StringUtils.isNotEmpty(tichengRate2)){
                    if (StringUtils.isEmpty(saleQty2)){
                        msg = msg + "第" + i + "行达到数量2未填;";
                    }else {
                        newExpert.setSeleQty2(saleQty2);
                    }
                    if (StringUtils.isNotEmpty(tichengAmt2) && StringUtils.isNotEmpty(tichengRate2)){
                        msg = msg + "第" + i + "行提成金额2与提成比例2二选一填写;";
                    }
                }
                if (StringUtils.isNotEmpty(tichengAmt3) || StringUtils.isNotEmpty(tichengRate3)){
                    if (StringUtils.isEmpty(saleQty3)){
                        msg = msg + "第" + i + "行达到数量3未填;";
                    }else {
                        newExpert.setSeleQty3(saleQty3);
                    }
                    if (StringUtils.isNotEmpty(tichengAmt3) && StringUtils.isNotEmpty(tichengRate3)){
                        msg = msg + "第" + i + "行提成金额3与提成比例3二选一填写;";
                    }
                }
                if (StringUtils.isNotEmpty(tichengAmt)){
                    newExpert.setTichengAmt(tichengAmt);
                }
                if (StringUtils.isNotEmpty(tichengRate)){
                    newExpert.setTichengRate(tichengRate);
                }
                if (StringUtils.isNotEmpty(tichengAmt2)){
                    newExpert.setTichengAmt2(tichengAmt2);
                }
                if (StringUtils.isNotEmpty(tichengRate2)){
                    newExpert.setTichengRate2(tichengRate2);
                }
                if (StringUtils.isNotEmpty(tichengAmt3)){
                    newExpert.setTichengAmt3(tichengAmt3);
                }
                if (StringUtils.isNotEmpty(tichengRate3)){
                    newExpert.setTichengRate3(tichengRate3);
                }
                importInDataList.add(newExpert);
            } //行end
        }else{
            throw new BusinessException("EXCEL导入信息为空");
        }
        if (StringUtils.isNotEmpty(msg)){
            throw new BusinessException(msg);
        }
        return importInDataList;
    }

    @ApiOperation(value = "提成方案复制,入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成)",response = PercentageInData.class)
    @PostMapping({"tichengDetailCopy"})
    public JsonResult tichengDetailCopy(@RequestBody Map<String,Long> inData) {
        return JsonResult.success(service.tichengDetailCopy(inData.get("planId"),inData.get("planType")), "提示：操作数据成功！");
//        return JsonResult.success("", "提示：操作数据成功！");
    }

    @ApiOperation(value = "提成方案门店查询,入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成) 出参：stoCode(门店编码),stoName(门店名称)")
    @PostMapping({"selectStoList"})
    public JsonResult selectStoList(@RequestBody Map<String,Long> inData) {
        return JsonResult.success(service.selectStoList(inData.get("planId"),inData.get("planType")), "提示：操作数据成功！");
//        return JsonResult.success("", "提示：操作数据成功！");
    }

    @ApiOperation(value = "方案停用,入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成),stopType(停用状态 1 立即停用 2 限时停用),stopReason(停用原因)")
    @PostMapping({"stopPlan"})
    public JsonResult stopPlan(@RequestBody Map<String,String> inData) {
        service.stopPlan(Long.valueOf(inData.get("planId")),inData.get("planType"),inData.get("stopType"),inData.get("stopReason"));
        return JsonResult.success("", "提示：审核成功！");
    }

    @ApiOperation(value = "定时停用方案")
    @PostMapping({"timerStopPlan"})
    public JsonResult timerStopPlan(@RequestBody Map<String,String> inData) {
        service.timerStopPlan();
        return JsonResult.success("", "提示：审核成功！");
    }
}
