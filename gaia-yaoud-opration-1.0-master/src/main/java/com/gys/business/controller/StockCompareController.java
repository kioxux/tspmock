package com.gys.business.controller;


import com.gys.business.mapper.entity.StockMouth;
import com.gys.business.service.StockCompareService;
import com.gys.business.service.StockMouthService;
import com.gys.business.service.data.StockCompareCondition;
import com.gys.business.service.data.StockDiffCondition;
import com.gys.business.service.data.StockMouthCaculateCondition;
import com.gys.business.service.data.StockMouthRes;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 月度库存表
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-07-13
 */
@Api(tags = "月度库存审计", value = "flynn")
@RestController
@RequestMapping("/stockCompare")
public class StockCompareController extends BaseController {

    @Autowired
    private StockMouthService stockMouthService;

    @Autowired
    private StockCompareService stockCompareService;

    @ApiOperation(value = "对比结果查询初始化接口")
    @GetMapping("/stockCompareInit")
    public JsonResult clientSite() {
        return JsonResult.success(this.stockCompareService.clientSite(), "提示：获取数据成功！");
    }


    @ApiOperation(value = "点击对比", response = StockMouth.class)
    @PostMapping("/compare")
    public JsonResult queryStoLeader(HttpServletRequest request,@RequestBody StockCompareCondition condition) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.stockCompareService.compare(condition,userInfo);
        return JsonResult.success("", "提示：对比成功！");
    }


    @ApiOperation(value = "对比结果汇总查询", response = StockMouthRes.class)
    @PostMapping("/stockCompareList")
    public JsonResult list(@RequestBody StockCompareCondition condition) {
        return JsonResult.success(this.stockCompareService.getStockCompareListPage(condition), "提示：获取数据成功！");
    }

    @ApiOperation(value = "对比结果汇总导出", response = StockMouthRes.class)
    @PostMapping("/stockCompareExport")
    public JsonResult export(@RequestBody StockCompareCondition condition) {
        return JsonResult.success(this.stockCompareService.export(condition), "提示：导出数据成功！");
    }

    @ApiOperation(value = "对比结果明细查询", response = StockMouth.class)
    @PostMapping("/stockCompareDetailList")
    public JsonResult detailList(@RequestBody StockDiffCondition condition) {
        return JsonResult.success(this.stockCompareService.getStockDiffListPage(condition), "提示：获取数据成功！");
    }

    @ApiOperation(value = "对比结果明细导出", response = StockMouth.class)
    @PostMapping("/stockCompareDetailExport")
    public JsonResult detailListExport(@RequestBody StockDiffCondition condition) {
        return JsonResult.success(this.stockCompareService.detailListExport(condition), "提示：获取数据成功！");
    }



}

