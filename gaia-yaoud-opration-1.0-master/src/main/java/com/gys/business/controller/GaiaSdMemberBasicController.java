package com.gys.business.controller;

import com.gys.business.service.GaiaSdMemberBasicService;
import com.gys.business.service.data.FranchiseeInData;
import com.gys.business.service.data.GetQueryMemberOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/memberBasic/"})
public class GaiaSdMemberBasicController extends BaseController {

    @Autowired
    private GaiaSdMemberBasicService gaiaSdMemberBasicService;

    @PostMapping({"queryMemberByCondition"})
    public JsonResult queryMemberByCondition(HttpServletRequest request, @RequestBody GetQueryMemberOutData inData) {
        GetLoginOutData loginUser = this.getLoginUser(request);
        inData.setClientId(loginUser.getClient());
        inData.setBrId(loginUser.getDepId());
        return JsonResult.success(this.gaiaSdMemberBasicService.queryMemberByCondition(inData),"提示：获取数据成功！");
    }
}
