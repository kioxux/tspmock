package com.gys.business.controller.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/9 14:45
 **/
@Data
@ApiModel
public class DcPushVO {
    @ApiModelProperty(value = "缺断货商品数量", example = "20")
    private Integer num;
    @ApiModelProperty(value = "月均销售额", example = "71919")
    private String monthSaleAmount;
    @ApiModelProperty(value = "月均毛利额", example = "44935")
    private String monthProfitAmount;
}
