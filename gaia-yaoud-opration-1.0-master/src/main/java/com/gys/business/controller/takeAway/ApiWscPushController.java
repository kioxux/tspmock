package com.gys.business.controller.takeAway;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.base.BaseController;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.common.data.JsonResult;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.UuidUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @desc: 微商城订单推送
 * @author: Ryan
 * @createTime: 2021/10/11 17:43
 */
@Slf4j
@RequestMapping("his")
@RestController
public class ApiWscPushController extends BaseController {
    @Resource
    private BaseService baseService;
    @Resource
    private TakeAwayService takeAwayService;

    @PostMapping("createOrder")
    public JsonResult createOrder(@RequestBody BaseOrderInfo baseOrderInfo) {
        log.info("<微商城订单><创建订单><请求参数：{}>", JSON.toJSONString(baseOrderInfo));
        //填充订单ID
        fillOrderInfo(baseOrderInfo);
        String result = baseService.insertOrderInfoWithMap(baseOrderInfo);
        if (result.startsWith("ok")) {
            takeAwayService.execOrdersPush(getOrderInfoToPTSByMap(baseOrderInfo), Constants.PLATFORMS_MINI_MALL, null, null, null);
        } else {
            return JsonResult.error("订单插入失败");
        }
        return JsonResult.success(null, "成功");
    }

    private void fillOrderInfo(BaseOrderInfo baseOrderInfo) {
        String orderId = UuidUtil.getUUID("wsc-");
        baseOrderInfo.setOrder_id(orderId);
        baseOrderInfo.setPlatforms(Constants.PLATFORMS_MINI_MALL);
        baseOrderInfo.setNotice_message("new");
        List<BaseOrderProInfo> items = baseOrderInfo.getItems();
        if (items != null && items.size() > 0) {
            for (BaseOrderProInfo item : items) {
                item.setOrder_id(orderId);
            }
        }
    }

}
