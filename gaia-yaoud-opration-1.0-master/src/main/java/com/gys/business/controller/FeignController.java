package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.gys.business.service.DepotService;
import com.gys.business.service.IncomeStatementService;
import com.gys.business.service.SyncService;
import com.gys.business.service.data.GetDepotInData;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.business.service.data.StockSyncData;
import com.gys.common.base.BaseController;
import com.gys.common.data.JsonResult;
import com.gys.common.enums.OrderRuleEnum;
import com.gys.common.webSocket.WebSocket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping({"/"})
@Slf4j
public class FeignController extends BaseController {
    @Resource
    private DepotService depotService;

    @Resource
    private IncomeStatementService incomeStatementService;

    @Resource
    private WebSocket webSocket;

    @Resource
    private SyncService syncService;

    @PostMapping({"getApprovalResult"})
    public JsonResult getApprovalResult(@RequestBody GetWfApproveInData inData) {
        log.info("getApprovalResult:{}", JSON.toJSONString(inData));

        String defineCode = inData.getWfDefineCode();
        try {
            if ("GAIA_WF_005".equals(defineCode)) {
                this.depotService.approveDepot(inData);
            }else if ("GAIA_WF_006".equals(defineCode) || "GAIA_WF_007".equals(defineCode)) {
                incomeStatementService.approveIncomeStatement(inData, 0);

                StockSyncData stockSyncData = new StockSyncData(inData.getClientId(), inData.getSite(), Lists.newArrayList(inData.getWfOrder()), OrderRuleEnum.SUN_YI.getOrderNoRule());
                log.info("调用库存新增/修改:{}", JSON.toJSONString(stockSyncData));
                syncService.stockSync(stockSyncData);
            }else if ("GAIA_WF_035".equals(defineCode) || "GAIA_WF_036".equals(defineCode) || "GAIA_WF_037".equals(defineCode) || "GAIA_WF_038".equals(defineCode) ||
                      "GAIA_WF_050".equals(defineCode) || "GAIA_WF_051".equals(defineCode)) {
                incomeStatementService.approveIncomeStatement(inData, 1);

                StockSyncData stockSyncData = new StockSyncData();
                if("GAIA_WF_035".equals(defineCode) || "GAIA_WF_036".equals(defineCode)){
                    stockSyncData = new StockSyncData(inData.getClientId(), inData.getSite(), Lists.newArrayList(inData.getWfOrder()), OrderRuleEnum.MEN_DIAN_BAO_SUN.getOrderNoRule());
                }else if("GAIA_WF_037".equals(defineCode) || "GAIA_WF_038".equals(defineCode)){
                    stockSyncData = new StockSyncData(inData.getClientId(), inData.getSite(), Lists.newArrayList(inData.getWfOrder()), OrderRuleEnum.MEN_DIAN_LING_YONG.getOrderNoRule());
                }else if("GAIA_WF_050".equals(defineCode) || "GAIA_WF_051".equals(defineCode)){
                    stockSyncData = new StockSyncData(inData.getClientId(), inData.getSite(), Lists.newArrayList(inData.getWfOrder()), OrderRuleEnum.MEN_DIAN_BAO_YI.getOrderNoRule());
                }
                log.info("调用库存新增/修改:{}", JSON.toJSONString(stockSyncData));
                try {
                    syncService.stockSync(stockSyncData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if ("GAIA_WF_008".equals(defineCode) || "GAIA_WF_009".equals(defineCode) || "GAIA_WF_010".equals(defineCode) || "GAIA_WF_011".equals(defineCode) ||
                "GAIA_WF_012".equals(defineCode) || "GAIA_WF_013".equals(defineCode)) {
                this.webSocket.AppointSending(inData.getCreateUser(), inData.getWfStatus());
            }
        }catch (Exception ex){
            return JsonResult.fail(101, ex.getMessage());
        }
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"returnDepotApprove"})
    public JsonResult approve(@RequestBody GetDepotInData inData) {
        this.depotService.approve(inData);
        return JsonResult.success("", "提示：保存成功！");
    }
}
