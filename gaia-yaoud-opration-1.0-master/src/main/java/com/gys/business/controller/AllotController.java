//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.AllotService;
import com.gys.business.service.data.GetAllotInData;
import com.gys.business.service.data.GetProductInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/allot/"})
public class AllotController extends BaseController {
    @Resource
    private AllotService allotService;

    public AllotController() {
    }

    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody GetAllotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.allotService.selectList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"detail"})
    public JsonResult detail(HttpServletRequest request, @RequestBody GetAllotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.allotService.detail(inData), "提示：获取数据成功！");
    }

    @PostMapping({"detailList"})
    public JsonResult detailList(HttpServletRequest request, @RequestBody GetAllotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.allotService.detailList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody GetAllotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsamhEmp(userInfo.getUserId());
        inData.setStoreCode(userInfo.getDepId());
        this.allotService.insert(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"queryPro"})
    public JsonResult queryPro(HttpServletRequest request, @RequestBody GetProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.allotService.queryPro(inData), "提示：获取数据成功！");
    }

    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody GetAllotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsamhEmp1(userInfo.getUserId());
        inData.setStoreCode(userInfo.getDepId());
        this.allotService.approve(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

    @PostMapping({"update"})
    public JsonResult update(HttpServletRequest request, @RequestBody GetAllotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        this.allotService.update(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

    @PostMapping({"exportIn"})
    public JsonResult exportIn(HttpServletRequest request, @RequestBody GetAllotInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.allotService.exportIn(inData), "提示：更新成功！");
    }
}
