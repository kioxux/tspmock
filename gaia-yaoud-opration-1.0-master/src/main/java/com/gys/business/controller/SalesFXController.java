package com.gys.business.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.service.SaleReturnFXService;
import com.gys.business.service.TianLingSReturnService;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.*;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.DataSubServer;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.MQReceiveData;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author xiaoyuan
 */
@Slf4j
@RestController
@RequestMapping({"/returnFX/"})
public class SalesFXController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(SalesFXController.class);

    @Autowired
    private SaleReturnFXService fxService;

    @Autowired
    private TianLingSReturnService tianLingReturn;

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @PostMapping({"salesInquireList"})
    public JsonResult getSalesInquireList(HttpServletRequest request, @Valid @RequestBody SalesInquireInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.fxService.getSalesInquireList(inData), "success");
    }
    @PostMapping({"updateSalesInquire"})
    public JsonResult updateSalesInquire(HttpServletRequest request, @Valid @RequestBody List<SalesInquireInData> inData) {

        GetLoginOutData userInfo = this.getLoginUser(request);
        List<String> billNoList = new ArrayList<>();


        for (int i = 0; i < inData.size(); i++) {
            SalesInquireInData salesInquireInData = inData.get(i);
            salesInquireInData.setClientId(userInfo.getClient());
            salesInquireInData.setBrId(userInfo.getDepId());
            billNoList.add(salesInquireInData.getBillNo());

        }
        boolean returnBool = this.fxService.updateSalesInquire(userInfo.getClient(), userInfo.getDepId(), billNoList);
        if(returnBool)
            return JsonResult.success("标记开票成功", "success");
        else
            return JsonResult.error("标记开票失败");
    }

    @PostMapping({"salesReturnDetailList"})
    public JsonResult getSalesReturnDetailList(HttpServletRequest request, @Valid @RequestBody SalesInquireData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.fxService.detail(inData), "success");
    }

    //


    @FrequencyValid
    @PostMapping({"returnAndSave2"})
    public JsonResult returnAndSave2(HttpServletRequest request, @Valid @RequestBody SalesInquireData inData) {
        TimeInterval timer = DateUtil.timer();
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        Map<String, Object> map = this.fxService.returnAndSave2(inData);
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("returnAndSave");
        mqReceiveData.setData( map.get("billNo"));
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, inData.getClientId() + "." + inData.getBrId(), JSON.toJSON(mqReceiveData));
        logger.info("退货完成 : 用时 : " + timer.intervalRestart());
        this.fxService.materialDocumentInterface((List<MaterialDocRequestDto>) map.get("listDto"));
        logger.info("退货完成 调用物料凭证  : " + timer.intervalRestart());
        logger.info("退货完成 发送 MQ 用时 : " + timer.intervalRestart());
        return JsonResult.success(map.get("billNo"), "success");
    }

    /**
     * 无销售退货
     * @param request
     * @param map
     * @return
     */
    @FrequencyValid
    @PostMapping({"returnWithoutOrder"})
    public JsonResult returnWithoutOrder(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        TimeInterval timer = DateUtil.timer();
        GetLoginOutData userInfo = super.getLoginUser(request);
        Map<String, Object> dtoMap = this.fxService.returnWithoutOrder(map,userInfo);
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("DataSync");
        mqReceiveData.setCmd("returnAndSave");
        mqReceiveData.setData( map.get("billNo"));
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, userInfo.getClient() + "." + userInfo.getDepId(), JSON.toJSON(mqReceiveData));
        logger.info("退货完成 发送 MQ 用时 : " + timer.intervalRestart());
        logger.info("退货完成 : 用时 : " + timer.intervalRestart());
        this.fxService.materialDocumentInterface((List<MaterialDocRequestDto>) dtoMap.get("listDto"));
        logger.info("退货完成 调用物料凭证  : " + timer.intervalRestart());
        return JsonResult.success("success");
    }

    @PostMapping({"queryUser"})
    public JsonResult queryUser(HttpServletRequest request) {
        GetSaleScheduleInData inData = new GetSaleScheduleInData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsegBrId(userInfo.getDepId());
        return JsonResult.success(this.fxService.queryUser(inData), "success");
    }

    @PostMapping({"queryUserNew"})
    public JsonResult queryUserNew(HttpServletRequest request, @RequestBody InData inData) {
        GetSaleScheduleInData ssinData = new GetSaleScheduleInData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        ssinData.setClientId(userInfo.getClient());
        //type 1门店 2加盟商
        if("1".equals(inData.getType())){
            ssinData.setGsegBrId(userInfo.getDepId());
        }
        return JsonResult.success(this.fxService.queryUserNew(ssinData), "success");
    }

    @PostMapping({"queryDoctor"})
    public JsonResult queryDoctor(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.fxService.queryDoctor(userInfo), "success");
    }

    @PostMapping({"judgment"})
    public JsonResult judgment(HttpServletRequest request, @Valid @RequestBody SalesInquireData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.fxService.judgment(inData), "success");
    }

    @PostMapping({"JudgmentPoints"})
    public JsonResult JudgmentPoints(HttpServletRequest request, @Valid @RequestBody SalesInquireData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.fxService.JudgmentPoints(inData), "success");
    }

    @PostMapping({"cache"})
    public JsonResult cache(HttpServletRequest request, @Valid @RequestBody SalesInquireData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.fxService.cache(inData), "success");
    }

    //获取支付方式
    @PostMapping({"queryPayMethod"})
    public JsonResult queryPayMethod(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.fxService.queryPayMethod(userInfo), "success");
    }

    /**
     * 天灵退货
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"tianLingReturn"})
    public JsonResult tianLingReturn(HttpServletRequest request, @Valid @RequestBody SalesInquireData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        Map<String, Object> map = this.tianLingReturn.tianLingReturn(inData);
        this.fxService.materialDocumentInterface((List<MaterialDocRequestDto>) map.get("listDto"));
        return JsonResult.success(map.get("billNo"), "success");
    }

    @PostMapping({"updateReturnBillStatus"})
    public JsonResult updateReturnBillStatus(HttpServletRequest request, @Valid @RequestBody ReturnSaleBillInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.fxService.updateReturnBillStatus(inData), "success");
    }

    /**
     * 查询加盟商下所有门店此商品库存
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"queryClientStock"})
    public JsonResult queryClientStock(HttpServletRequest request, @Valid @RequestBody GetQueryProductInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.fxService.queryClientStock(inData), "success");
    }


    @PostMapping({"querySaleMember"})
    public JsonResult querySaleMember(HttpServletRequest request, @Valid @RequestBody Map<String, String> map){
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("clientId",userInfo.getClient());
        map.put("brId",userInfo.getDepId());
        return JsonResult.success(this.fxService.querySaleMember(map), "success");
    }

    @PostMapping({"memberPurchaseRecord"})
    public JsonResult memberPurchaseRecord(HttpServletRequest request, @Valid @RequestBody Map<String, String> map){
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client",userInfo.getClient());
        map.put("brId",userInfo.getDepId());
        List<MemberPurchaseRecordOutData> outData = this.fxService.memberPurchaseRecord(map);
        return JsonResult.success(outData, "success");
    }

    @PostMapping({"queryBillInfo"})
    public JsonResult queryBillInfo(HttpServletRequest request, @Valid @RequestBody Map<String, String> map){
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client",userInfo.getClient());
        map.put("brId",userInfo.getDepId());
        return JsonResult.success(this.fxService.queryBillInfo(map), "success");
    }


    @PostMapping({"getPendingOrderInformation"})
    public JsonResult getPendingOrderInformation(HttpServletRequest request, @RequestBody Map<String, String> map){
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client",userInfo.getClient());
        map.put("brId",userInfo.getDepId());
        PendingPrintDto printDto = this.fxService.getPendingOrderInformation(map);
        return JsonResult.success(printDto, "success");
    }
    //根据身份证 查询 该身份证当天一共购买 含麻商品数量
    @PostMapping({"listControlGoodsQtyByIdCard"})
    public JsonResult listControlGoodsQtyByIdCard(HttpServletRequest request, @RequestBody Map<String, Object> map){
        log.info(String.format("<首页结算><含麻品控制当天购买量><请求参数：%s>", JSONUtil.toJsonStr(map)));
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client",userInfo.getClient());
        List<RestOrderDetailOutData> result = this.fxService.listControlGoodsQtyByIdCard(map);
        return JsonResult.success(result, "success");
    }

    //调用调剂调入单主表
    @PostMapping({"listAdjustBill"})
    public JsonResult listAdjustBill(HttpServletRequest request, @RequestBody GaiaWmsHudiaoZOutData param){
        log.info(String.format("<调出挂单><调用调剂调入单-查询主表><请求参数：%s>", JSONUtil.toJsonStr(param)));
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        param.setWmDrmd(userInfo.getDepId());
        List<GaiaWmsHudiaoZOutData> result = this.fxService.listAdjustBill(param);
        return JsonResult.success(result, "success");
    }

    //调用调剂调入单主表
    @PostMapping({"listAdjustBillDetail"})
    public JsonResult listAdjustBillDetail(HttpServletRequest request, @RequestBody GaiaWmsHudiaoZOutData param){
        log.info(String.format("<调出挂单><调用调剂调入单-查询主表><请求参数：%s>", JSONUtil.toJsonStr(param)));
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        param.setWmDrmd(userInfo.getDepId());
        List<GaiaWmsHudiaoZOutData> result = this.fxService.listAdjustBillDetail(param);
        return JsonResult.success(result, "success");
    }

    /**
     * FX本地数据同步服务器
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"dataSubServerRequest"})
    public JsonResult dataSubServer(HttpServletRequest request, @RequestBody InData param){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        this.fxService.dataSubServer(param);
        this.fxService.dataSubServerRequest(param);

        return JsonResult.success("", "success");
    }

    /**
     * FX本地数据同步服务器
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"dataSubServer"})
    public JsonResult dataSubServer(HttpServletRequest request, @RequestBody DataSubServer param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.fxService.dataSubServer(param);
        return JsonResult.success("", "success");
    }

    /**
     * 门店代售-查询代售门店商品信息
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"listOtherStoreProduct"})
    public JsonResult listOtherStoreProduct(HttpServletRequest request, @RequestBody HashMap<String,String> param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<GetQueryProductOutData> res = this.fxService.listOtherStoreProduct(param);
        return JsonResult.success(res, "success");
    }

    /**
     *  门店代售查询服务器库存
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"queryStockAndExpGroupByArea"})
    public JsonResult queryStockAndExpGroupByArea(HttpServletRequest request, @RequestBody HashMap<String,String> param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<GetPdAndExpOutData> res = this.fxService.queryStockAndExpGroupByArea(param);
        return JsonResult.success(res, "success");
    }

    /**
     *  门店代售查询服务器库存
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"queryStockAndExpGroupByBatchNo"})
    public JsonResult queryStockAndExpGroupByBatchNo(HttpServletRequest request, @RequestBody HashMap<String,String> param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<GetPdAndExpOutData> res = this.fxService.queryStockAndExpGroupByBatchNo(param);
        return JsonResult.success(res, "success");
    }

    /**
     * 门店代售-查询代售门商品单价
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"queryOtherProductDetail"})
    public JsonResult queryOtherProductDetail(HttpServletRequest request, @RequestBody HashMap<String,String> param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        GetSalesReceiptsTableOutData res = this.fxService.queryOtherProductDetail(param);
        return JsonResult.success(res, "success");
    }

    /**
     * 电子券检索
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"commercePlatform"})
    public JsonResult commercePlatform(HttpServletRequest request, @RequestBody HashMap<String,String> param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        boolean res = fxService.commercePlatform(userInfo,param);
        return JsonResult.success(res, "success");
    }


    /**
     * 门店代售-查询代售门店商品信息-带效期 批号
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"listOtherStoreProductBatch"})
    public JsonResult listOtherStoreProductBatch(HttpServletRequest request, @RequestBody HashMap<String,String> param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<GetQueryProductOutData> res = this.fxService.listOtherStoreProductBatch(param);
        return JsonResult.success(res, "success");
    }

    /**
     * 保存挂单备注删除
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"updateHideCommonRemark"})
    public JsonResult updateHideCommonRemark(HttpServletRequest request, @RequestBody List<HashMap<String,String>> param){
        this.fxService.updateHideCommonRemark(param);
        return JsonResult.success("success");
    }

    /**
     * 保存挂单备注删除
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"listHideCommonRemark"})
    public JsonResult listHideCommonRemark(HttpServletRequest request, @RequestBody HashMap<String,String> param){
        List<GaiaHideCommonRemark> list = this.fxService.listHideCommonRemark(param);
        return JsonResult.success(list,"success");
    }

    /**
     * 保存挂单备注删除
     * @param request
     * @param param
     * @return
     */
    @PostMapping({"saveHideRemark"})
    public JsonResult saveHideRemark(HttpServletRequest request, @RequestBody HashMap<String,Object> param){
        this.fxService.saveHideRemark(param);
        return JsonResult.success("success");
    }

}
