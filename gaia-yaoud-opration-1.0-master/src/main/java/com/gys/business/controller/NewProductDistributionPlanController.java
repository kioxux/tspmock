package com.gys.business.controller;

import com.gys.business.service.NewProductDistributionPlanService;
import com.gys.business.service.data.NewProductDistributionPlanDto;
import com.gys.business.service.data.NewProductDto;
import com.gys.common.data.*;
import com.gys.common.enums.ActualDistributionTypeEnum;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @Author ：liuzhiwen.guoyuxi
 * @Date ：Created in 10:26 2021/8/4
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@RestController
@RequestMapping({"/newProduct"})
public class NewProductDistributionPlanController {

    @Autowired
    private NewProductDistributionPlanService newProductDistributionPlanService;
    @Autowired
    private CosUtils cosUtils;

    /**
     * 有计划-新品铺货计划详情获取
     * @param planDto
     * @return
     */
    @PostMapping("plan")
    public JsonResult plan(@RequestBody NewProductDistributionPlanDto planDto){
        return JsonResult.success(this.newProductDistributionPlanService.plan(planDto),"提示：获取数据成功！");
    }

    /**
     * 有计划-新品铺货查询参数获取
     * @return
     */
    @PostMapping("getQueryCondition")
    public JsonResult getQueryCondition(@RequestBody NewProductDistributionPlanDto planDto){
        return JsonResult.success(this.newProductDistributionPlanService.getQueryCondition(planDto), "提示：获取数据成功！");
    }

    @PostMapping({"noPlan"})
    public JsonResult noPlan(@RequestBody NewProductDistributionNoPlanDto noPlanDto) {
        return newProductDistributionPlanService.noPlan(noPlanDto);
    }

    @PostMapping({"getClientList"})
    public JsonResult getClientList(@RequestBody@Valid NewProductDistributionNoPlanClientDto noPlanClientDto) {
        return newProductDistributionPlanService.getNoPlanClientList(noPlanClientDto);
    }

    @PostMapping({"getProductCodeList"})
    public JsonResult getProductCodeList(@RequestBody@Valid NewProductDistributionNoPlanProductCodeDto noPlanProductCodeDto) {
        return newProductDistributionPlanService.getProductCodeList(noPlanProductCodeDto);
    }

    @PostMapping({"getProvList"})
    public JsonResult getProvList(@RequestBody NewProductDistributionNoPlanProvAndCityDto noPlanProvAndCityDto) {
        return newProductDistributionPlanService.getProv(noPlanProvAndCityDto);
    }

    @PostMapping({"getCityList"})
    public JsonResult getCityList(@RequestBody@Valid NewProductDistributionNoPlanProvAndCityDto noPlanProvAndCityDto) {
        return newProductDistributionPlanService.getCityList(noPlanProvAndCityDto);
    }


    @RequestMapping({"exportNoPlan"})
    public Result exportNoPlan(@RequestBody NewProductDistributionNoPlanDto noPlanDto) {
        List<GaiaNewProductDistributionNoPlanVo> voList= (List<GaiaNewProductDistributionNoPlanVo>) newProductDistributionPlanService.noPlan(noPlanDto).getData();
        for (GaiaNewProductDistributionNoPlanVo noPlanVo : voList) {
            noPlanVo.setProCode("\t" + (noPlanVo.getProCode()==null?"":noPlanVo.getProCode()));
        }
        if (voList.isEmpty()) {
            throw new BusinessException("没查到结果");
        }
        String fileName="无新品计划铺货明细导出";
        Result result=null;
        CsvFileInfo csvFileInfo = CsvClient.getCsvByte(voList, fileName, Collections.singletonList((short) 1));
        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        try {
            bos.write(csvFileInfo.getFileContent());
            result=cosUtils.uploadFile(bos,csvFileInfo.getFileName());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                bos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    /**
     * 有计划-新品铺货详情导出
     * @param planDto
     * @return
     */
    @PostMapping("exportPlanDetail")
    public Result export(@RequestBody NewProductDistributionPlanDto planDto){
        return this.newProductDistributionPlanService.export(planDto);
    }

    /**
     * 有计划-获取查询商品详情
     * @param dto
     * @return
     */
    @PostMapping("getProduct")
    public JsonResult getProduct(@RequestBody @Valid NewProductDto dto){
        return JsonResult.success(this.newProductDistributionPlanService.getProduct(dto),"提示：获取数据成功！");
    }

    /**
     * 实际铺货方式查询
     * @return
     */
    @GetMapping("getDistributionType")
    public JsonResult getType(){
        return JsonResult.success(ActualDistributionTypeEnum.toJson());
    }
}
