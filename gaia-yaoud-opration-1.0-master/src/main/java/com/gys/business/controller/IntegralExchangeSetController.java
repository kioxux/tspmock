package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.GaiaSdIntegralExchangeSetDetailService;
import com.gys.business.service.GaiaSdIntegralExchangeSetHeadService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GaiaSdIntegralExchangeRecord;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.NewExchangeTotalNumBean;
import com.gys.common.response.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;


/**
 * @desc:
 * @author: RULAISZ,guoyuxi
 * @createTime: 2021/11/29 9:59
 */
@RestController
@RequestMapping("/integralExchange/")
@Slf4j
public class IntegralExchangeSetController extends BaseController {

    @Autowired
    private GaiaSdIntegralExchangeSetHeadService gaiaSdIntegralExchangeSetHeadService;
    @Autowired
    private GaiaSdIntegralExchangeSetDetailService detailService;

    //积分换购：查询
    @PostMapping({"/integralExchangeList"})
    public JsonResult integralExchangeList(HttpServletRequest request,@RequestBody IntegralInData inData){
        GetLoginOutData user = this.getLoginUser(request);
        inData.setClient(user.getClient());
        return JsonResult.success(gaiaSdIntegralExchangeSetHeadService.getIntegralExchangeList(inData),"");
    }

    @PostMapping("setDetail")
    public JsonResult setDetail(@RequestBody @Valid IntegralExchangeSetInData inData, HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setCreateUser(userInfo.getUserId());
        inData.setCreateTime(new Date());
        log.info("<积分兑换设置><设置><参数:{}>", JSON.toJSONString(inData));
        String voucherId = gaiaSdIntegralExchangeSetHeadService.setDetail(inData);
        return JsonResult.success("","提示：" + voucherId + "单号保存成功");
    }

    @PostMapping("updateDetail")
    public JsonResult updateDetail(@RequestBody @Valid IntegralExchangeSetInData inData, HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setCreateUser(userInfo.getUserId());
        inData.setCreateTime(new Date());
        inData.setUpdateUser(userInfo.getUserId());
        inData.setUpdateTime(new Date());
        log.info("<积分兑换设置><修改><参数:{}>", JSON.toJSONString(inData));
        gaiaSdIntegralExchangeSetHeadService.updateDetail(inData);
        return JsonResult.success(null,"修改成功");
    }

    @PostMapping("audit")
    public JsonResult audit(@RequestBody @Valid AuditIntegralExchangeInData inData, HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUpdateUser(userInfo.getUserId());
        inData.setUpdateTime(new Date());
        log.info("<积分兑换设置><审核><参数:{}>", JSON.toJSONString(inData));
        gaiaSdIntegralExchangeSetHeadService.audit(inData);
        return JsonResult.success(null,"审核成功");
    }

    @PostMapping("setEndTime")
    public JsonResult setEndTime(@RequestBody @Valid SetEndTimeIntegralExchangeInData inData, HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUpdateUser(userInfo.getUserId());
        inData.setUpdateTime(new Date());
        log.info("<积分兑换设置><修改结束时间><参数:{}>", JSON.toJSONString(inData));
        gaiaSdIntegralExchangeSetHeadService.setEndTime(inData);
        return JsonResult.success(null,"结束日期修改成功");
    }

    @PostMapping("/batchExcelImport")
    public JsonResult batchExcelImport(HttpServletRequest request, MultipartFile file) {
        GetLoginOutData loginOutData = this.getLoginUser(request);
        return gaiaSdIntegralExchangeSetHeadService.batchExcelImport(loginOutData,file);
    }

    @PostMapping("/downLoadTemplate")
    public void downLoadTemplate(HttpServletRequest request, HttpServletResponse response) {
        GetLoginOutData loginOutData = this.getLoginUser(request);
        gaiaSdIntegralExchangeSetHeadService.downLoadTemplate(loginOutData,response);
    }

    //积分换购：查看（活动门店）
    @PostMapping({"/storeList"})
    public JsonResult storeList(HttpServletRequest request, @RequestBody IntegralInData inData){
        GetLoginOutData user = this.getLoginUser(request);
        inData.setClient(user.getClient());
        return JsonResult.success(gaiaSdIntegralExchangeSetHeadService.getStoreList(inData),"提示：查询成功！");
    }

    //积分换购：查看（明细）
    @PostMapping({"/detailList"})
    public JsonResult detailList(HttpServletRequest request, @RequestBody IntegralInData inData){
        GetLoginOutData user = this.getLoginUser(request);
        inData.setClient(user.getClient());
        return JsonResult.success(detailService.detailList(inData),"提示：查询成功！");
    }

    //积分换购：查看（明细）导出
    @PostMapping({"/exportDetailList"})
    public Result exportDetailList(HttpServletRequest request, @RequestBody IntegralInData inData){
        GetLoginOutData user = this.getLoginUser(request);
        inData.setClient(user.getClient());
        return detailService.exportDetailList(inData);
    }

    //积分换购：复制
    @PostMapping({"/copyActivity"})
    public JsonResult copyActivity(HttpServletRequest request, @RequestBody IntegralInData inData){
        GetLoginOutData user = this.getLoginUser(request);
        inData.setClient(user.getClient());
        return JsonResult.success(gaiaSdIntegralExchangeSetHeadService.getActivityData(inData),"提示：查询成功！");
    }

    //积分换购：查看（零售价）
    @PostMapping({"/getPrice"})
    public JsonResult getPrice(HttpServletRequest request, @RequestBody @Valid ProductPriceInData inData){
        GetLoginOutData user = this.getLoginUser(request);
        inData.setClient(user.getClient());
        return JsonResult.success(gaiaSdIntegralExchangeSetHeadService.getPrice(inData),"提示：查询成功！");
    }

    //新积分换购新增换购记录
    @PostMapping("addRecord")
    public JsonResult addRecord(HttpServletRequest request, @RequestBody List<GaiaSdIntegralExchangeRecord> list) {
        GetLoginOutData user = this.getLoginUser(request);
        return gaiaSdIntegralExchangeSetHeadService.addRecord(list);
    }

    //新积分换购-查询当前会员*天内兑换某商品总数
    @PostMapping("exchangeTotalNum")
    public JsonResult exchangeTotalNum(HttpServletRequest request, @RequestBody NewExchangeTotalNumBean bean) {
        GetLoginOutData user = this.getLoginUser(request);
        return gaiaSdIntegralExchangeSetHeadService.exchangeTotalNum(bean);
    }
}
