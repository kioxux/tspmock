package com.gys.business.controller;

import com.gys.business.service.data.fiApPayment.GaiaFiApPaymentDto;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.gys.business.service.IGaiaFiApPaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.gys.common.base.BaseController;

/**
 * <p>
 * 应付方式维护表 前端控制器
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@RestController
@Api(tags = "应付方式维护表接口")
@RequestMapping("/fiApPayment")
public class GaiaFiApPaymentController extends BaseController {


    @Autowired
    private IGaiaFiApPaymentService fiApPaymentService;

    @PostMapping("/save")
    @ApiOperation(value = "应付方式维护表保存", notes = "应付方式维护表保存")
    public JsonResult save(HttpServletRequest request, @Valid @RequestBody GaiaFiApPaymentDto inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        return JsonResult.success(fiApPaymentService.save(userInfo, inData), "提示：保存成功！");
    }


    @PostMapping("/list")
    @ApiOperation(value = "应付方式维护表查询（分页）", notes = "应付方式维护表查询（分页）")
    public JsonResult listPage(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        return JsonResult.success(fiApPaymentService.getListPage(userInfo), "提示：查询成功！");
    }

}

