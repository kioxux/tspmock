package com.gys.business.controller;

import com.gys.business.service.GaiaClientBenefitLevelInfoService;
import com.gys.business.service.data.GaiaClientBenefitLevelInfoData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author csm
 * @date 2021/12/16 - 13:21
 */
@Api(tags = "查询客户级别相关参数")
@Slf4j
@RestController
@RequestMapping({"/clientLievel/"})
public class GaiaClientBenefitLevelInfoController extends BaseController {
    @Resource
    private GaiaClientBenefitLevelInfoService gaiaClientBenefitLevelInfoService;

    @ApiOperation(value = "根据加盟ID查询客户级别相关参数")
    @PostMapping({"getPromotionGroupList"})
    public JsonResult getPromotionGroupList(@RequestBody GaiaClientBenefitLevelInfoData gcliData) {
        log.info("*****进入查询查询客户级别相关参数:");
        return JsonResult.success(gaiaClientBenefitLevelInfoService.getByClient(gcliData));
    }
    @ApiOperation(value = "根据加盟ID修改或新增客户级别相关参数")
    @PostMapping({"updatePromotionGroupList"})
    public JsonResult update(HttpServletRequest request,@Valid @RequestBody GaiaClientBenefitLevelInfoData gcbld) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        log.info("*****修改客户级别相关参数:"+userInfo.toString());
        gaiaClientBenefitLevelInfoService.update(userInfo,gcbld);
        return JsonResult.success("","修改成功！");
    }
    @ApiOperation(value = "查询客户默认级别相关参数")
    @PostMapping({"getDefaultPromotions"})
    public JsonResult selectDefaultValues() {
        return JsonResult.success(gaiaClientBenefitLevelInfoService.getDefaultValues());
    }
    @ApiOperation(value = "客户级别-导出")
    @PostMapping(value = "exportClientBenefitLeveData")
    public Result exportBill( @RequestBody GaiaClientBenefitLevelInfoData gcbld) {
        log.info("<客户级别相关数据导出><请求参数：%s>",gcbld.getClient() );
        return gaiaClientBenefitLevelInfoService.exportClientLevel(gcbld.getClient());
    }
}
