package com.gys.business.controller;

import com.gys.business.service.ReceivingSupplierReportService;
import com.gys.business.service.data.SupplierRecordInData;
import com.gys.business.service.data.SupplierRecordOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 供应商报表
 *
 * @author xiaoyuan on 2020/9/10
 */
@RestController
@Api(tags = "收货（供应商）记录")
@RequestMapping({"/receivingSupplier"})
public class ReceivingSupplierReportController extends BaseController {

    @Autowired
    private ReceivingSupplierReportService supplierReportService;

    @ApiOperation(value = "列表查询/条件查询", response = SupplierRecordOutData.class)
    @PostMapping("/list")
    public JsonResult reportRecord(HttpServletRequest request, @Valid @RequestBody SupplierRecordInData inData){
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(supplierReportService.findReportRecord(inData),"success");
    }
}
