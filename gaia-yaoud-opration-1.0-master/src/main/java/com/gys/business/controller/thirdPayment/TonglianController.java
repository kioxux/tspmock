package com.gys.business.controller.thirdPayment;

import com.gys.business.mapper.entity.GaiaThirdPaymentTonglian;
import com.gys.business.service.TongLianService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author huxinxin
 */
@RestController
@RequestMapping({"/tongLianPay/"})
public class TonglianController extends BaseController {
    @Resource
    private TongLianService tongLianService;

    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody GaiaThirdPaymentTonglian inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setCLIENT(userInfo.getClient());
        inData.setSTO_CODE(userInfo.getDepId());
        return JsonResult.success(this.tongLianService.insert(inData), "提示：插入数据成功！");
    }

    @PostMapping({"selectById"})
    public JsonResult selectById(HttpServletRequest request, @RequestBody GaiaThirdPaymentTonglian inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.tongLianService.selectById(inData), "提示：查询数据成功！");
    }
}
