//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.DeviceInfoService;
import com.gys.business.service.data.DeviceInfoInData;
import com.gys.business.service.data.DeviceInfoOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/deviceInfo/"})
public class DeviceInfoController extends BaseController {
    @Autowired
    private DeviceInfoService deviceInfoService;

    public DeviceInfoController() {
    }

    @PostMapping({"/deviceInfoList"})
    public JsonResult deviceInfoList(HttpServletRequest request, @RequestBody DeviceInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsthBrId(userInfo.getDepId());
        return JsonResult.success(this.deviceInfoService.getDeviceInfoList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/addDeviceInfo"})
    public JsonResult addDeviceInfo(HttpServletRequest request, @RequestBody DeviceInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsthBrId(userInfo.getDepId());
        inData.setGsthBrName(userInfo.getDepName());
        this.deviceInfoService.insertDeviceInfo(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/getDeviceInfo"})
    public JsonResult getDeviceInfo(@RequestBody DeviceInfoInData inData) {
        return JsonResult.success(this.deviceInfoService.getDeviceInfoById(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/editDeviceInfo"})
    public JsonResult editDeviceInfo(HttpServletRequest request, @RequestBody DeviceInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsthBrId(userInfo.getDepId());
        inData.setGsthBrName(userInfo.getDepName());
        this.deviceInfoService.editDeviceInfo(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/selectBrDeviceList"})
    public JsonResult selectBrDeviceList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        DeviceInfoInData inData = new DeviceInfoInData();
        inData.setClientId(userInfo.getClient());
        inData.setGsthBrId(userInfo.getDepId());
        List<DeviceInfoOutData> list = this.deviceInfoService.selectBrDeviceList(inData);
        return JsonResult.success(list, "提示：审核成功！");
    }
}
