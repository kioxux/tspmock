package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdReplenishConfig;
import com.gys.business.service.data.sdReplenishConfig.GaiaSdReplenishConfigDto;
import com.gys.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.gys.business.service.IGaiaSdReplenishConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.gys.common.base.BaseController;

/**
 * <p>
 * 门店补货参数 前端控制器
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-17
 */
@RestController
@Api(tags = "门店补货参数接口")
@RequestMapping("/replenishConfig")
public class GaiaSdReplenishConfigController extends BaseController {

    @Autowired
    private IGaiaSdReplenishConfigService sdReplenishConfigService;

    @PostMapping("/save")
    @ApiOperation(value = "门店补货参数新增", notes = "门店补货参数新增")
    public JsonResult save(HttpServletRequest request,@Valid @RequestBody GaiaSdReplenishConfigDto inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(sdReplenishConfigService.build(userInfo, inData), "提示：保存成功！");
    }

    @PostMapping("/addReplenishParam")
    @ApiOperation(value = "门店补货参数新增", notes = "门店补货参数新增")
    public JsonResult addReplenishParam(HttpServletRequest request,@Valid @RequestBody GaiaSdReplenishConfigDto inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        sdReplenishConfigService.addReplenishParam(userInfo, inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PutMapping("/edit")
    @ApiOperation(value = "门店补货参数修改", notes = "门店补货参数修改")
    public JsonResult update(HttpServletRequest request,@Valid @RequestBody GaiaSdReplenishConfigDto inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(sdReplenishConfigService.update(userInfo, inData), "提示：修改成功！");
    }


    @GetMapping("/delete/{id}")
    @ApiOperation(value = "门店补货参数删除", notes = "门店补货参数删除")
    public JsonResult delete(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(sdReplenishConfigService.delete(userInfo, id), "提示：删除成功！");
    }

    @GetMapping("/get")
    @ApiOperation(value = "门店补货参数查询", notes = "门店补货参数查询")
    public JsonResult get(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        return JsonResult.success(sdReplenishConfigService.getDetailById(userInfo), "提示：查询成功！");
    }


    @PostMapping("/list")
    @ApiOperation(value = "门店补货参数查询（分页）", notes = "门店补货参数查询（分页）")
    public JsonResult listPage(HttpServletRequest request, @RequestBody Object inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(sdReplenishConfigService.getListPage(userInfo, inData), "提示：查询成功！");
    }

    @GetMapping("/getRemain")
    @ApiOperation(value = "门店补货配置参数余量查询", notes = "门店补货配置参数余量查询")
    public JsonResult getRemain(HttpServletRequest request, @RequestParam(value = "gsrhVoucherId",required = false) String gsrhVoucherId) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        return JsonResult.success(sdReplenishConfigService.getRemain(userInfo, gsrhVoucherId), "提示：查询成功！");
    }

    @PostMapping("/setRemain")
    @ApiOperation(value = "设置门店补货配置参数余量", notes = "设置门店补货配置参数余量")
    public JsonResult setRemain(HttpServletRequest request, @RequestBody GaiaSdReplenishConfig gaiaSdReplenishConfig) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        return JsonResult.success(sdReplenishConfigService.setRemain(userInfo, gaiaSdReplenishConfig), "提示：操作成功！");
    }

}

