package com.gys.business.controller.member;

import com.gys.business.service.member.PointCashOutService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.member.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author wu mao yin
 * @Title: 积分抵现
 * @date 2021/12/815:08
 */
@RestController
@Api(tags = "积分抵现")
@RequestMapping("/pointsCashOut")
public class PointCashOutController extends BaseController {

    @Resource
    private HttpServletRequest request;

    @Resource
    private PointCashOutService pointCashOutService;

    @ApiOperation("查询积分规则设置列表")
    @PostMapping("selectPointRuleSettingList")
    public JsonResult selectPointRuleSettingList(@RequestBody PointSettingSearchDTO pointSettingSearchDTO) {
        pointSettingSearchDTO.setClient(super.getLoginUser(request).getClient());
        return pointCashOutService.selectPointRuleSettingList(pointSettingSearchDTO);
    }

    @ApiOperation("新增修改积分规则设置")
    @PostMapping("saveOrUpdatePointRuleSetting")
    public JsonResult saveOrUpdatePointRuleSetting(@RequestBody PointSettingDTO pointSettingDTO) {
        GetLoginOutData loginUser = super.getLoginUser(request);
        pointSettingDTO.setClient(loginUser.getClient());
        pointSettingDTO.setUserId(loginUser.getUserId());
        return pointCashOutService.saveOrUpdatePointRuleSetting(pointSettingDTO);
    }

    @ApiOperation("删除积分规则设置")
    @PostMapping("deletePointRuleSetting")
    public JsonResult deletePointRuleSetting(String planIds) {
        return pointCashOutService.deletePointRuleSetting(super.getLoginUser(request).getClient(), planIds);
    }

    @ApiOperation("获取规则下拉列表")
    @GetMapping("selectPlanIdList")
    public JsonResult selectPlanIdList() {
        return pointCashOutService.selectPlanIdList(super.getLoginUser(request).getClient());
    }

    @ApiOperation("新增修改积分抵现活动")
    @PostMapping("saveOrUpdateIntegralCashActivity")
    public JsonResult saveOrUpdateIntegralCashActivity(@RequestBody PointCashOutActivityDTO pointCashOutActivityDTO) {
        GetLoginOutData loginUser = super.getLoginUser(request);
        pointCashOutActivityDTO.setClient(loginUser.getClient());
        pointCashOutActivityDTO.setUserId(loginUser.getUserId());
        return pointCashOutService.saveOrUpdateIntegralCashActivity(pointCashOutActivityDTO);
    }

    @ApiOperation("修改结束日期")
    @PostMapping("updateEndDateTime")
    public JsonResult updateEndDateTime(String voucherId, String endDate, String endTime) {
        return pointCashOutService.updateEndDateTime(super.getLoginUser(request).getClient(), voucherId, endDate, endTime);
    }

    @ApiOperation("审核活动")
    @GetMapping("reviewActivity")
    public JsonResult reviewActivity(String voucherId) {
        GetLoginOutData loginUser = super.getLoginUser(request);
        return pointCashOutService.reviewActivity(loginUser.getClient(), loginUser.getUserId(), voucherId);
    }

    @ApiOperation("根据单号查看关联门店")
    @GetMapping("selectStoListByVoucherId")
    public JsonResult selectStoListByVoucherId(String voucherId) {
        GetLoginOutData loginUser = super.getLoginUser(request);
        return pointCashOutService.selectStoListByVoucherId(loginUser.getClient(), voucherId);
    }

    @ApiOperation("根据单号查看关联商品")
    @PostMapping("selectProListByVoucherId")
    public JsonResult selectProListByVoucherId(@RequestBody PointProSearchDTO pointProSearchDTO) {
        GetLoginOutData loginUser = super.getLoginUser(request);
        pointProSearchDTO.setClient(loginUser.getClient());
        pointProSearchDTO.setBrId(loginUser.getDepId());
        return pointCashOutService.selectProListByVoucherId(pointProSearchDTO);
    }

    @ApiOperation("查询积分抵现活动列表")
    @PostMapping("selectIntegralCashActivity")
    public JsonResult selectIntegralCashActivity(@RequestBody PointCashOutActivitySearchDTO pointCashOutActivitySearchDTO) {
        pointCashOutActivitySearchDTO.setClient(super.getLoginUser(request).getClient());
        return pointCashOutService.selectIntegralCashActivity(pointCashOutActivitySearchDTO);
    }

    @ApiOperation("删除积分抵现活动")
    @GetMapping("deleteIntegralCashActivity")
    public JsonResult deleteIntegralCashActivity(String voucherId) {
        return pointCashOutService.deleteIntegralCashActivity(super.getLoginUser(request).getClient(), voucherId);
    }

}
