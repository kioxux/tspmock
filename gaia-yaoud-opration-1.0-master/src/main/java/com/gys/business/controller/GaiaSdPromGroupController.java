package com.gys.business.controller;

import com.gys.business.service.GaiaSdPromGroupService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * @author csm
 * @date 2021/12/15 - 13:30
 */
@RestController
@RequestMapping({"/promotionProductGroup/"})
@Slf4j
@Api(tags = "促销商品组查询")
public class GaiaSdPromGroupController extends BaseController {
    @Resource
    GaiaSdPromGroupService gaiaSdPromGroupService;
    @ApiOperation(value = "促销商品组查询")
    @PostMapping({"getGroup"})
    public JsonResult getPromotionGroup(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
       log.info("*****进入促销商品分组查询:"+userInfo.getClient());
        return JsonResult.success(gaiaSdPromGroupService.selectAll(userInfo.getClient()));
    }
}
