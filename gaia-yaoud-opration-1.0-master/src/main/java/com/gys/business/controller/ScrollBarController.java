package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.service.GaiaSdMessageService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 消息队列
 *
 * @author xiaoyuan on 2020/12/23
 */
@RestController
@RequestMapping({"/scrollBar/"})
public class ScrollBarController extends BaseController {

    @Autowired
    private GaiaSdMessageService gaiaSdMessageService;

    @PostMapping({"getList"})
    public JsonResult getSupByBrIds(HttpServletRequest request,@RequestBody GaiaSdMessage inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.gaiaSdMessageService.getAll(userInfo,inData), "success");
    }

    @PostMapping({"updateMessage"})
    public JsonResult updateMessage(HttpServletRequest request, @RequestBody GaiaSdMessage inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.gaiaSdMessageService.updateMessage(userInfo,inData);
        return JsonResult.success("", "success");
    }
}
