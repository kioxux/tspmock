package com.gys.business.controller;

import com.gys.business.service.GaiaCommodityInventoryDService;
import com.gys.business.service.data.CommodityAllocationPushInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

/**
 * 商品调库-明细表(GaiaCommodityInventoryD)表控制层
 *
 * @author makejava
 * @since 2021-10-19 16:01:14
 */
@RestController
@RequestMapping("gaiaCommodityInventoryD")
public class GaiaCommodityInventoryDController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaCommodityInventoryDService gaiaCommodityInventoryDService;

    @PostMapping("push")
    public JsonResult push(HttpServletRequest request, @RequestBody @Valid CommodityAllocationPushInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUpdateUser(userInfo.getUserId());
        inData.setUpdateTime(new Date());
        inData.setToken(request.getHeader("X-Token"));
        this.gaiaCommodityInventoryDService.push(inData);
        return JsonResult.success("", "提示：推送成功！");
    }

}

