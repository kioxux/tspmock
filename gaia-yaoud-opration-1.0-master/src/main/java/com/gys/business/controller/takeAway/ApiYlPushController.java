package com.gys.business.controller.takeAway;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.business.mapper.takeAway.GaiaYlCooperationDao;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.base.BaseController;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.common.data.JsonResult;
import com.gys.common.data.yaolian.YlCategoryInputBean;
import com.gys.common.data.yaolian.common.YlResponseData;
import com.gys.common.exception.BusinessException;
import com.gys.util.UtilConst;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.UuidUtil;
import com.gys.util.takeAway.yl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ZhangDong
 * @date 2021/9/7 16:09
 */
@Controller
@RequestMapping("/yl/push")
public class ApiYlPushController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(ApiYlPushController.class);
    @Autowired
    private BaseService baseService;
    @Autowired
    private TakeAwayService takeAwayService;
    @Resource
    private GaiaYlCooperationDao gaiaYlCooperationDao;

    /**
     * 订单回推
     * 每个推送请求只包含一个完整订单信息
     */
    @PostMapping("newOrder")
    @ResponseBody
    public Map<String, Object> newOrder(@RequestBody YlOrderInBean inputBean) {
        log.info("药联推送新订单，data:{}", JSON.toJSONString(inputBean));
        List<YlOrderBean> orders = inputBean.getOrders();
        if (CollUtil.isEmpty(orders)) {
            log.error("药联推送新订单，data:{}，无订单信息", JSON.toJSONString(inputBean));
            return newOrderReturnMsg("0", "", "无订单信息");
        }
        YlOrderBean bean = orders.get(0);
        String storeId = bean.getStore_id();
        String[] split = storeId.split(UtilConst.GYS);
        String client = split[0];
        String stoCode = split[1];
        List<GaiaTAccount> accountList = takeAwayService.getAllAccountByClientAndStoCode(Constants.PLATFORMS_YL, client, stoCode);
        if (CollUtil.isEmpty(accountList)) {
            log.info("药联推送新订单，查不到account数据，storeId:{}", storeId);
            return newOrderReturnMsg("0", bean.getId(), "");
        }
        GaiaTAccount gaiaTAccount = accountList.get(0);
        BaseOrderInfo orderInfo = convertOrder(bean);
        orderInfo.setClient(client);
        orderInfo.setStoCode(stoCode);
        orderInfo.setPlatforms(Constants.PLATFORMS_YL);
        String orderId = UuidUtil.getUUID("yl-");
        orderInfo.setOrder_id(orderId);
        orderInfo.setShop_no(gaiaTAccount.getPublicCode());
        orderInfo.setAction("1");
        orderInfo.setNotice_message("new");
        for (BaseOrderProInfo item : orderInfo.getItems()) {
            item.setClient(client);
            item.setStoCode(stoCode);
            item.setOrder_id(orderId);
        }
        //插入订单基础信息
        String resultForInsert = baseService.insertOrderInfoWithMap(orderInfo);
        if (resultForInsert.startsWith("ok")) {
            takeAwayService.execOrdersPush(getOrderInfoToPTSByMap(orderInfo), Constants.PLATFORMS_YL, null, null, null);
        } else {
            return newOrderReturnMsg("0", bean.getId(), "");
        }
        return newOrderReturnMsg("1", bean.getId(), "");
    }

    /**
     * 退单
     */
    @PostMapping("returnOrder")
    @ResponseBody
    public Map<String, Object> returnOrder(@RequestBody YlReturnOrderBean bean) {
        log.info("药联推送退单，data:{}", JSON.toJSONString(bean));
        List<YlReturnOrderDetailBean> list = bean.getRefund();
        if (CollUtil.isEmpty(list)) {
            log.error("药联推送退单，data:{}，无退单信息", JSON.toJSONString(bean));
            return returnOrderReturnMsg("0", "", "", "无退单信息");
        }
        YlReturnOrderDetailBean orderDetailBean = list.get(0);
        String storeId = orderDetailBean.getStore_id();
        String id = orderDetailBean.getId();
        String[] split = storeId.split(UtilConst.GYS);
        String client = split[0];
        String stoCode = split[1];
        List<String> successList = new ArrayList<>(list.size());
        for (YlReturnOrderDetailBean detailBean : list) {
            Map<String, Object> queryMap = new HashMap<>(4);
            queryMap.put("platforms_order_id", detailBean.getId());
            queryMap.put("platforms", Constants.PLATFORMS_YL);
            Map<String, Object> retMap = baseService.selectOrderInfoByMap(queryMap);
            if (nullToEmpty(retMap.get("result")).equals("ok")) {
                retMap.put("status", "4");
                //用户申请退单
                retMap.put("action", "4");
                retMap.put("last_action", "4");
                retMap.put("notice_message", "cancel");
                String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                if (resultForXB.equals("ok")) {
                    String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                    retMap.put("storeLogo", storeLogo);
                    takeAwayService.pushToStoreMq(client, stoCode, "O2O", "cancel", retMap);
                    log.info("药联推送取消订单success，orderId:{},client:{},stoCode:{},storeLogo:{}", nullToEmpty(retMap.get("order_id")), client, stoCode, storeLogo);
                    successList.add(detailBean.getId());
                } else {
                    log.error("药联推送取消订单fail，orderId:{},client:{},stoCode:{},updateResult:{}", nullToEmpty(retMap.get("order_id")), client, stoCode, resultForXB);
                }
            }
        }
        //传的是多个退单号，返回的却是一个
        //雨格：退单号可返回空字符串
        return returnOrderReturnMsg("1", id, "", "");
    }

    //雨格：目前total_price 和free_price 值一样，都是订单价格
    //目前回推是没有优惠价格的
    //张浩：用户付款金额没推送给我们
    private BaseOrderInfo convertOrder(YlOrderBean bean) {
        String freePrice = bean.getFree_price();
        String salePrice = bean.getSale_price();
        String totalPrice = bean.getTotal_price();
        BaseOrderInfo info = new BaseOrderInfo();
        info.setPlatforms_order_id(bean.getId());
        info.setCreate_time(bean.getCreate_time());
        info.setCustomer_pay(totalPrice);
        info.setIs_invoiced("0");//无发票字段
        info.setPay_type("0");//他们家属于客户到门店用药联软件可以享受一定的优惠，所以不需要快递
        info.setStatus("1");
        info.setOrder_info("0");
        info.setTotal_price(totalPrice);
        info.setOriginal_price(totalPrice);
        info.setShipping_fee("0.0000");
        //商品明细
        List<YlOrderDetailBean> list = bean.getRetailDetail();
        List<BaseOrderProInfo> items = new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++) {
            YlOrderDetailBean detailBean = list.get(i);
            BaseOrderProInfo proInfo = new BaseOrderProInfo();
            proInfo.setMedicine_code(detailBean.getDrug_id());
            proInfo.setSequence(String.valueOf(i + 1));
            proInfo.setPrice(detailBean.getPrice());
            proInfo.setQuantity(detailBean.getAmount());
            items.add(proInfo);
        }
        info.setItems(items);
        return info;
    }

    /**
     * 新订单返回值
     *
     * @param result  1表示确认收到可以正常下单，2表示因为业务原因导致无法销账，0为数据保存失败
     * @param orderId 订单流水号
     * @param msg
     * @return
     */
    private Map<String, Object> newOrderReturnMsg(String result, String orderId, String msg) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("result", result);
        map.put("order_id", orderId);
        map.put("msg", msg);
        return map;
    }

    /**
     * 退单返回值
     *
     * @param result   1表示退单成功，0表示退单失败
     * @param orderId  药联订单号
     * @param refundId 退单号
     * @return
     */
    private Map<String, Object> returnOrderReturnMsg(String result, String orderId, String refundId, String msg) {
        Map<String, Object> map = new HashMap<>(4);
        map.put("result", result);
        map.put("order_id", orderId);
        map.put("refund_id", refundId);
        map.put("msg", msg);
        return map;
    }

    /**
     * 药联-初始上传门店数据
     */
    @ResponseBody
    @PostMapping({"queryStoreByYaolian"})
    public YlResponseData queryStoreByYaolian(@RequestBody Map map) {
        if (ObjectUtil.isNotEmpty(map.get("client"))) {
            return takeAwayService.queryStoreByYaolian(String.valueOf(map.get("client")));
        } else {
            throw new BusinessException("请输入加盟商编码！");
        }
    }

    /**
     * 药联-初始上传商品类目
     */
    @ResponseBody
    @PostMapping("medicineCategoryInput")
    public JsonResult medicineCategoryInput(@RequestBody YlCategoryInputBean bean) {
        String client = bean.getClient();
        List<String> categoryNameList = bean.getCategoryNameList();
        if (CollUtil.isEmpty(categoryNameList) || StrUtil.isEmpty(client)) {
            return JsonResult.success(null, "client or list is empty");
        }
        String cooperation = gaiaYlCooperationDao.getCooperationByClient(client);
        boolean success = YlApiUtil.medicineCategoryInput(cooperation, categoryNameList);
        return success ? JsonResult.success(null, "success") : JsonResult.fail(101, "fail");
    }

    /**
     * 药联-初始上传商品数据(含价格)
     */
    @PostMapping("commodityDataEntry")
    @ResponseBody
    public YlResponseData commodityDataEntry(@RequestBody Map<String, Object> map) {
        return this.takeAwayService.commodityDataEntry(map);
    }

    /**
     * 药联-初始上传商品库存
     *
     * @param map 加盟商不可为空  门店列表和商品列表可为空
     */
    @PostMapping({"syncStockToYaolian"})
    @ResponseBody
    public JsonResult syncStockToYaolian(@RequestBody Map<String, Object> map) {
        if (StrUtil.isEmpty((String) map.get("client"))) {
            return JsonResult.fail(500, "提示：加盟商编码为空！");
        }
        return JsonResult.success(takeAwayService.syncStockToYaolian((String) map.get("client"), (List<String>) map.get("brId"),
                (List<String>) map.get("proIds"), false), "提示：数据同步成功！");
    }

}
