//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.CashPaymentService;
import com.gys.business.service.data.CashPaymentInData;
import com.gys.business.service.data.CashPaymentOutData;
import com.gys.business.service.data.GetSaleScheduleQueryInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/cashPayment/"})
public class CashPaymentController extends BaseController {
    @Autowired
    private CashPaymentService cashPaymentService;

    public CashPaymentController() {
    }

    @PostMapping({"/cashPaymentList"})
    public JsonResult cashPaymentList(@RequestBody CashPaymentInData inData) {
        return JsonResult.success(this.cashPaymentService.getCashPaymentList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/cashPaymentDetailList"})
    public JsonResult cashPaymentDetailList(@RequestBody CashPaymentOutData inData) {
        return JsonResult.success(this.cashPaymentService.getCashPaymentDetailList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody CashPaymentOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.cashPaymentService.insert(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody CashPaymentOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.cashPaymentService.approve(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

    @PostMapping({"/empGroupList"})
    public JsonResult getEmpGroupList(HttpServletRequest request, @RequestBody GetSaleScheduleQueryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.cashPaymentService.getEmpGroupList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/empList"})
    public JsonResult getEmpList(HttpServletRequest request, @RequestBody GetLoginOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setDepId(userInfo.getDepId());
        return JsonResult.success(this.cashPaymentService.getEmpList(inData), "提示：获取数据成功！");
    }
}
