package com.gys.business.controller;

import com.gys.business.service.IGaiaBillOfApService;
import com.gys.business.service.data.billOfAp.GaiaBillOfApResponse;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.Objects;

/**
 * <p>
 * 供应商应付业务员明细清单表 前端控制器
 * </p>
 *
 * @author Yifan.wang
 * @since 2021-10-26
 */
@Slf4j
@RestController
@Api(tags = "供应商应付业务员明细清单表接口")
@RequestMapping("/assistantBillOfAp")
public class GaiaBillOfApSupplierController extends BaseController {
    @Autowired
    private IGaiaBillOfApService billOfApService;


    @ApiOperation("供应商业务员汇总列表")
    @PostMapping("list")
    public JsonResult list(HttpServletRequest request, @RequestBody GaiaBillOfApResponse response) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        response.setClient(userInfo.getClient());
        PageInfo pageInfo = billOfApService.assistantList(response);
        return JsonResult.success(pageInfo, "数据获取成功");
    }
    @ApiOperation("供应商业务员汇总列表导出")
    @PostMapping("exportExcel")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response,@RequestBody GaiaBillOfApResponse data) throws Exception{
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        data.setClient(userInfo.getClient());

        SXSSFWorkbook workbook = billOfApService.exportExcel(data,userInfo);

        String filename = "供应商应付列表.csv";
        OutputStream output = response.getOutputStream();
        response.reset();
        String codedFileName = java.net.URLEncoder.encode(filename, "UTF-8");
        response.setHeader("Content-disposition", "attachment; filename=" + new String(codedFileName.getBytes(), "ISO8859-1"));
        response.setContentType("application/vnd.ms-excel");
        workbook.write(output);
        workbook.close();
        output.flush();
        output.close();



    }


}

