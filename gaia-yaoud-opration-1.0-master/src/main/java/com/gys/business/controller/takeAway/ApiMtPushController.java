package com.gys.business.controller.takeAway;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.business.mapper.entity.GaiaTOrderInfo;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.base.BaseController;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.*;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.UuidUtil;
import com.gys.util.takeAway.mt.MtApiUtil;
import com.gys.util.takeAway.mt.SignUtil;
import com.qcloud.cos.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/mt/push")
public class ApiMtPushController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(ApiMtPushController.class);
    @Autowired
    private BaseService baseService;
    @Autowired
    private TakeAwayService takeAwayService;

    /**
     * 美团外卖：推送已支付订单（必接）
     * 推送正常返回ok，不会推第二次
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderPay", method = RequestMethod.POST)
    public Map<String, Object> orderPayForMt(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //定义接口调用操作日志map对象(外卖平台调用小宝接口、美团)
        Map<String, Object> logMap = createOperateLogMap("5", "1", "orderPayForMt", "推送已支付订单");
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            logger.info("美团推送已支付订单，空请求pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        logger.info("美团推送已支付订单：" + JSON.toJSONString(paramMap));
        String appId = (String) paramMap.get("app_id");
        String platformShopId = (String) paramMap.get("app_poi_code");//商家授权时，绑定三方门店id，即我们系统门店publicCode
        String platformOrderId = (String) paramMap.getOrDefault("order_id", "");

        //根据当前appid和店铺id 查找私钥
        GaiaTAccount account = takeAwayService.getMtPlatformAccount(Constants.PLATFORMS_MT, appId, platformShopId);
        String defaultSecret = "";
        String client = "";
        String stoCode = "";
        String orderOnService = "N";
        String nativeShopId = "";
        if (ObjectUtil.isNotEmpty(account)) {
            defaultSecret = account.getAppSecret();
            client = account.getClient();
            stoCode = account.getStoCode();
            orderOnService = account.getOrderOnService();
            nativeShopId = account.getPublicCode();
        }
        if (account == null || !"Y".equalsIgnoreCase(orderOnService)) {
            logger.error("美团外卖-推送已支付订单：找不到对应门店信息！ platformOrderId:{}, appId:{}, platformShopId:{}", platformOrderId, appId, platformShopId);
            resultMap.put("data", "ok");
            return resultMap;
        }

        // 2.获取美团商家配置信息
        int resultForValid = validApiParamForMt(paramMap, "orderPay", defaultSecret);
        logger.info("美团推送已支付订单，验证sign， result:" + resultForValid);
        resultForValid = 1;
        //1.校验接口参数
        if (resultForValid == 0) {
            //接口存活验证成功！
            resultMap.put("data", "ok");
            return resultMap;
        } else if (resultForValid == 2) {
            //美团订单推送API签名验证失败
            resultMap.put("code", Constants.ERROR_CODE_1001);
            resultMap.put("msg", "美团订单推送API签名验证失败!");
            setOperateLogInfo(logMap, "0", "美团订单推送API签名验证失败。" + "平台订单ID：" + nullToEmpty(paramMap.get("order_id")));
            // 5.接口调用操作日志记录
            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        } else {
            logger.info("美团外卖-推送API：推送已支付订单接收成功！ 平台订单ID：" + platformOrderId + "门店号：" + nativeShopId);
            //对于回调美团商家确认订单这一接口，须等到订单成功推到PTS，再去调
            BaseOrderInfo baseOrderInfo = MtApiUtil.convertToBaseOrder(paramMap, client);
            //也要判断对于map转实体类是否成功
            if (baseOrderInfo.getResult().equals("ok")) {
                baseOrderInfo.setShop_no(nativeShopId);
                baseOrderInfo.setClient(client);
                baseOrderInfo.setStoCode(stoCode);
                baseOrderInfo.setPlatforms(Constants.PLATFORMS_MT);
                baseOrderInfo.setPlatforms_order_id(platformOrderId);
                String order_id = UuidUtil.getUUID("mt-");
                baseOrderInfo.setOrder_id(order_id);
                baseOrderInfo.setAction("1");
                baseOrderInfo.setNotice_message("new");
                for (BaseOrderProInfo item : baseOrderInfo.getItems()) {
                    item.setClient(client);
                    item.setStoCode(stoCode);
                    item.setOrder_id(order_id);
                }
                List<WeideProIdDto> weideMtProId = takeAwayService.getWeideMtProId(client, stoCode, "1".equals(baseOrderInfo.getPrescriptionOrNot()), order_id);
                if (CollUtil.isNotEmpty(weideMtProId)) {
                    List<BaseOrderProInfo> resultAddList = new ArrayList<>(weideMtProId.size());
                    int size = baseOrderInfo.getItems().size();
                    for (int i = 0; i < weideMtProId.size(); i++) {
                        WeideProIdDto weideProIdDto = weideMtProId.get(i);
                        BaseOrderProInfo tempInfo = new BaseOrderProInfo();
                        tempInfo.setClient(client);
                        tempInfo.setStoCode(stoCode);
                        tempInfo.setOrder_id(order_id);
                        tempInfo.setSequence(String.valueOf(size + i + 1));
                        tempInfo.setPrice(weideProIdDto.getPrice() == null ? "0.0000" : weideProIdDto.getPrice().toString());
                        tempInfo.setQuantity("1.0000");
                        tempInfo.setMedicine_code(weideProIdDto.getProId());
                        tempInfo.setDiscount("1.0");
                        tempInfo.setBox_num("0");
                        tempInfo.setBox_price("0.0000");
                        tempInfo.setUnit("份");
                        resultAddList.add(tempInfo);
                    }
                    baseOrderInfo.getItems().addAll(resultAddList);
                }

                // 5.插入订单基础信息 | 商品明细 | 在途明细（攘外必须安内原则)
                String resultForXB = baseService.insertOrderInfoWithMap(baseOrderInfo);
                if (resultForXB.startsWith("ok")) {
                    resultMap.put("data", "ok");
                    takeAwayService.execOrdersPush(getOrderInfoToPTSByMap(baseOrderInfo), Constants.PLATFORMS_MT, appId, defaultSecret, account);
                } else {
                    resultMap.put("data", "error");
                }
            } else {
                resultMap.put("data", "error");
//                setOperateLogInfo(logMap, "0", baseOrderInfo.getResult());
                logger.info("订单ID：" + platformOrderId + " 新订单转换失败：" + baseOrderInfo.getResult());
            }
        }
//        // 5.接口调用操作日志记录
//        baseService.insertOperatelogWithMap(logMap);
        return resultMap;
    }

    @Resource
    private RabbitTemplate rabbitTemplate;

//    @GetMapping("testPush")
//    @ResponseBody
    public String push() {
        String s = "{\"original_price\":\"32.3\",\"invoice_title\":\"\",\"delivery_time\":\"0\",\"platforms\":\"MT\",\"result\":\"ok\",\"update_time\":\"2021/09/27 00:20:19\",\"client\":\"10000072\",\"pay_type\":\"0\",\"caution\":\"收餐人隐私号 15546405149_6787，手机号 156****1025\",\"shop_no\":\"creah1jo\",\"stoCode\":\"10004\",\"create_time\":\"2021/09/27 00:20:19\",\"total_price\":\"13.43\",\"platforms_order_id\":\"28952443119442195\",\"customer_pay\":\"19.8\",\"notice_message\":\"new\",\"recipient_address\":\"林兴小区-2号楼 (2单元203)@#黑龙江省哈尔滨市南岗区和兴路街道林兴小区\",\"is_invoiced\":\"0\",\"order_info\":\"5.50\",\"shipping_fee\":\"7.0\",\"taxpayer_id\":\"\",\"day_seq\":\"2\",\"storeLogo\":\"yaoud09b8525b9498d53bf257c2ff522843a8\",\"detail\":[{\"stoCode\":\"10004\",\"sequence\":1,\"unit\":\"份\",\"quantity\":\"1\",\"medicine_code\":\"04180\",\"box_num\":\"1.0\",\"price\":\"24.3\",\"client\":\"10000072\",\"box_price\":\"0.0\",\"discount\":\"1.0\"}],\"recipient_name\":\"陈(先生)\",\"order_id\":\"mt-2021092740122051137\",\"recipient_phone\":\"15546405149_6787\",\"status\":\"1\"}";
        Map<String, Object> map = JSON.parseObject(s, Map.class);
        takeAwayService.pushToStoreMq("10000005", "10000", "O2O", "newOrder", map);
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("O2O");
        mqReceiveData.setCmd("newOrder");
        mqReceiveData.setData(map);
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, "10000005" + "." + "10000", JSON.toJSON(mqReceiveData));
        return "success";
    }

    /**
     * 美团外卖：推送催单消息（必接）
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderReminder", method = RequestMethod.POST)
    public Map<String, Object> orderReminderForMt(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //定义接口调用操作日志map对象(外卖平台调用小宝接口、美团)
        Map<String, Object> logMap = createOperateLogMap("5", "1", "orderReminderForMt", "推送催单消息");
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            logger.info("美团推送催单消息，空请求pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        logger.info("美团推送催单消息：" + JSON.toJSONString(paramMap));
        String appId = (String) paramMap.get("app_id");
        String platformShopId = (String) paramMap.get("app_poi_code");
        String platformOrderId = (String) paramMap.getOrDefault("order_id", "");

        //由于platformShopId为空，不得不从orderInfo表里先取stoCode
        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_MT, platformOrderId);
        if (orderInfo == null) {
            logger.info("美团推送催单消息, orderInfo = null , pass");
            resultMap.put("data", "error");
            return resultMap;
        }
        //根据当前appid和店铺id 查找私钥
        //GaiaTAccount account = takeAwayService.getPlatformAccount("MT", appId, platformShopId);
        String stoCode = orderInfo.getStoCode();
        String client = orderInfo.getClient();
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_MT, appId, stoCode, client);
        String defaultSecret = "";
        String orderOnService = "N";
        String nativeShopId = "";
        if (ObjectUtil.isNotEmpty(account)) {
            defaultSecret = account.getAppSecret();
            orderOnService = account.getOrderOnService();
            nativeShopId = account.getPublicCode();
        }
        if (account == null || !"Y".equalsIgnoreCase(orderOnService)) {
            logger.error("美团外卖-推送催单消息：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", platformOrderId, appId, stoCode);
            resultMap.put("data", "ok");
            return resultMap;
        }

        int resultForValid = validApiParamForMt(paramMap, "orderReminder", defaultSecret);
        logger.info("美团推送催单消息，验证sign， result:" + resultForValid);
        resultForValid = 1;
        // 1.校验接口参数
        if (resultForValid == 0) {
            //接口存活验证成功！
            resultMap.put("data", "ok");
            return resultMap;
        }
        if (resultForValid == 2) {
            //美团订单推送API签名验证失败
            resultMap.put("code", Constants.ERROR_CODE_1001);
            resultMap.put("msg", "美团订单推送API签名验证失败!");
            setOperateLogInfo(logMap, "0", "美团订单推送API签名验证失败" + "平台订单ID：" + platformOrderId);
            // 5.接口调用操作日志记录
            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        }
        // 2.根据平台订单ID，获取内部订单全部信息，存入订单历史变更表
        paramMap.put("platforms_order_id", platformOrderId);
        paramMap.put("order_id", "");
        paramMap.put("platforms", Constants.PLATFORMS_MT);
        //同一个外卖平台订单ID不会重复，不同平台之间暂不清楚，所以加上平台条件
        Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
        //如果调用不成功，视为不成功，也要记录不成功信息
        if (!nullToEmpty(retMap.get("result")).equals("ok")) {
//            setOperateLogInfo(logMap,"0",nullToEmpty(retMap.get("result")));
        } else {
            //每次更新订单信息表，都得存入相应信息到订单历史表，后期可追溯
            retMap.put("last_action", "2");
            retMap.put("notice_message", "hint");
            //攘外必须安内，先更新我们系统
            String resultForXBUpdate = baseService.updateOrderInfoWithMap(retMap);
            if (!resultForXBUpdate.equals("ok")) {
//                setOperateLogInfo(logMap,"0",resultForXBUpdate);
            } else {
                paramMap.put("order_id", platformOrderId);
                // TODO 3. 调用PTS-HANA接口：发送催单提醒（参数：门店ID，订单ID）
                //催单不变更状态，只变更动作。状态还是原来状态
                paramMap.put("status", nullToEmpty(retMap.get("status")));
                paramMap.put("shop_no", nullToEmpty(retMap.get("shop_no")));
                //加一个action
                paramMap.put("action", "2");
                takeAwayService.pushToStoreMq(client, stoCode, "O2O", "remind", retMap);
//                setOperateLogInfo(logMap, "0", "订单ID：" + nullToEmpty(paramMap.get("platforms_order_id"))+" 状态："+"催单");
            }
        }
//        // 5.接口调用操作日志记录
//        baseService.insertOperatelogWithMap(logMap);
        resultMap.put("data", "ok");
        return resultMap;
    }

    /**
     * 美团外卖：推送用户或客服取消订单（必接）
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderCancel")
    public Map<String, Object> orderCancelForMt(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //定义接口调用操作日志map对象(外卖平台调用小宝接口、美团)
        Map<String, Object> logMap = createOperateLogMap("5", "1", "orderCancelForMt", "推送用户或客服取消订单");
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            logger.info("美团推送用户或客服取消订单，空请求pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        logger.info("美团推送推送用户或客服取消订单：" + JSON.toJSONString(paramMap));
        String appId = (String) paramMap.get("app_id");
        //String platformShopId = (String) paramMap.get("app_poi_code");
        String platformOrderId = (String) paramMap.getOrDefault("order_id", "");

        //由于platformShopId为空，不得不从orderInfo表里先取stoCode
        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_MT, platformOrderId);
        if (orderInfo == null) {
            logger.info("美团推送用户或客服取消订单, orderInfo = null , pass");
            resultMap.put("data", "error");
            return resultMap;
        }
        String stoCode = orderInfo.getStoCode();
        String client = orderInfo.getClient();
        //根据当前appid和店铺id 查找私钥
        //GaiaTAccount account = takeAwayService.getPlatformAccount("MT", appId, platformShopId);
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_MT, appId, stoCode, client);
        String defaultSecret = "";
        String orderOnService = "N";
        String nativeShopId = "";
        if (ObjectUtil.isNotEmpty(account)) {
            defaultSecret = account.getAppSecret();
            orderOnService = account.getOrderOnService();
            nativeShopId = account.getPublicCode();
        }
        if (account == null || !"Y".equalsIgnoreCase(orderOnService)) {
            logger.error("美团外卖-推送催单消息：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", platformOrderId, appId, stoCode);
            resultMap.put("data", "ok");
            return resultMap;
        }

        int resultForValid = validApiParamForMt(paramMap, "orderCancel", defaultSecret);
        logger.info("美团推送推送用户或客服取消订单，验证sign， result:" + resultForValid);
        resultForValid = 1;
        // 1.校验接口参数
        if (resultForValid == 0) {
            //接口存活验证成功！
            resultMap.put("data", "ok");
            return resultMap;
        } else if (resultForValid == 2) {
            //美团订单推送API签名验证失败
            resultMap.put("code", Constants.ERROR_CODE_1001);
            resultMap.put("msg", "美团订单推送API签名验证失败!");
            setOperateLogInfo(logMap, "0", "美团订单推送API签名验证失败" + "平台订单ID：" + platformOrderId);
            // 5.接口调用操作日志记录
            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        } else {
            // 2.根据平台订单ID，获取内部订单ID
            logger.info("美团外卖-推送API：用户取消订单！ 订单ID：" + platformOrderId);
            paramMap.put("platforms_order_id", platformOrderId);
            //重置order_id
            paramMap.put("order_id", "");
            //这边是美团平台
            paramMap.put("platforms", Constants.PLATFORMS_MT);
            Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
            if (nullToEmpty(retMap.get("result")).equals("ok")) {
                //新的状态：已取消
                retMap.put("status", "4");
                //用户申请退单
                retMap.put("action", "4");
                retMap.put("last_action", "4");
                retMap.put("reason_code", nullToEmpty(paramMap.get("reason_code")));
                retMap.put("reason", nullToEmpty(paramMap.get("reason")));
                retMap.put("notice_message", "cancel");
                String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                if (resultForXB.equals("ok")) {
                    String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                    retMap.put("storeLogo", storeLogo);
                    takeAwayService.pushToStoreMq(client, stoCode, "O2O", "cancel", retMap);
                    logger.info("美团推送取消订单，orderId:{},client:{},stoCode:{},storeLogo:{}", nullToEmpty(retMap.get("order_id")), client, stoCode, storeLogo);
                    resultMap.put("data", "ok");
//                    setOperateLogInfo(logMap, "0", "订单ID：" + orderId+" 状态："+"订单取消");
                    //用户发起的取消订单要不要调用更新药品库存接口？
                } else {
//                    setOperateLogInfo(logMap,"0",resultForXB);
                    resultMap.put("data", resultForXB);
                }
            } else {
//                setOperateLogInfo(logMap,"0",nullToEmpty(retMap.get("result")));
                resultMap.put("data", retMap.get("result"));
            }
        }
//        // 5.接口调用操作日志记录
//        baseService.insertOperatelogWithMap(logMap);
        return resultMap;
    }

    /**
     * 美团外卖：推送已完成订单
     *
     * @param request
     * @return
     * @Author：Li.Deman
     * @Date：2018/11/29
     */
    @ResponseBody
    @RequestMapping(value = "/orderComplete", method = RequestMethod.POST)
    public Map<String, Object> orderCompleteForMt(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //定义接口调用操作日志map对象(外卖平台调用小宝接口、美团)
        Map<String, Object> logMap = createOperateLogMap("5", "1", "orderCompleteForMt", "推送已完成订单");
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            logger.info("美团推送已完成订单，空请求pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        logger.info("美团推送已完成订单：" + JSON.toJSONString(paramMap));
        String appId = (String) paramMap.get("app_id");
        String platformShopId = (String) paramMap.get("app_poi_code");
        String platformOrderId = (String) paramMap.getOrDefault("order_id", "");

        //根据当前appid和店铺id 查找私钥
        GaiaTAccount account = takeAwayService.getMtPlatformAccount(Constants.PLATFORMS_MT, appId, platformShopId);
        String defaultSecret = "";
        String client = "";
        String stoCode = "";
        String orderOnService = "N";
        String nativeShopId = "";
        if (ObjectUtil.isNotEmpty(account)) {
            defaultSecret = account.getAppSecret();
            client = account.getClient();
            stoCode = account.getStoCode();
            orderOnService = account.getOrderOnService();
            nativeShopId = account.getPublicCode();
        }
        if (account == null || !"Y".equalsIgnoreCase(orderOnService)) {
            logger.error("美团外卖-推送已完成订单：找不到对应门店信息！ platformOrderId:{}, appId:{}, platformShopId:{}", platformOrderId, appId, platformShopId);
            resultMap.put("data", "ok");
            return resultMap;
        }

        int resultForValid = validApiParamForMt(paramMap, "orderComplete", defaultSecret);
        logger.info("美团推送已完成订单，验证sign， result:" + resultForValid);
        resultForValid = 1;
        //1.检验接口参数
        if (resultForValid == 0) {
            //接口存活验证成功！
            resultMap.put("data", "ok");
            return resultMap;
        } else if (resultForValid == 2) {
            ////美团订单推送API签名验证失败
            resultMap.put("code", Constants.ERROR_CODE_1001);
            resultMap.put("msg", "//美团订单推送API签名验证失败！");
            setOperateLogInfo(logMap, "0", "美团订单推送API签名验证失败" + "平台订单ID：" + nullToEmpty(paramMap.get("order_id")));
            // 5.接口调用操作日志记录
            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        } else {
            //2.根据平台订单ID，获取内部订单ID
            logger.info("美团外卖-推送API：已完成订单！订单ID：" + platformOrderId);
            paramMap.put("platforms_order_id", platformOrderId);
            paramMap.put("order_id", "");
            //加一下平台类型
            paramMap.put("platforms", Constants.PLATFORMS_MT);
            Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
            if (nullToEmpty(retMap.get("result")).equals("ok")) {
                //已完成状态
                retMap.put("status", "3");
                retMap.put("action", "0");
                retMap.put("notice_message", "");
                //4.更新订单状态为已完成
                String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                if (resultForXB.equals("ok")) {
                    paramMap.put("order_id", platformOrderId);
                    paramMap.put("status", "3");
                    paramMap.put("shop_no", nullToEmpty(retMap.get("shop_no")));
//                    pushOrderStatusChange(paramMap);
                    takeAwayService.pushToStoreMq(client, stoCode, "O2O", "orderStatus", retMap);
//                    setOperateLogInfo(logMap, "0", "订单ID：" + orderId+" 状态："+"订单完成");
                } else {
//                    setOperateLogInfo(logMap,"0",resultForXB);
                }
            } else {
//                setOperateLogInfo(logMap,"0",nullToEmpty(retMap.get("result")));
            }
        }
//        // 5.接口调用操作日志记录
//        baseService.insertOperatelogWithMap(logMap);
        resultMap.put("data", "ok");
        return resultMap;
    }

    /**
     * @Description: 隐私号降级处理（必接）
     * @Param:
     * @return:
     * @Author: Qin.Qing
     * @Date: 2018/11/29
     */
    @ResponseBody
    @RequestMapping(value = "/privacyLevelDown", method = RequestMethod.POST)
    public Map<String, Object> privacyLevelDown(HttpServletRequest request) {
        //定义接口调用操作日志map对象(外卖平台调用小宝接口、美团)
        Map<String, Object> logMap = createOperateLogMap("5", "1", "privacyLevelDown", "隐私号降级处理");
        Map<String, Object> paramMap = getParameterMap(request);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        if (paramMap == null || paramMap.size() == 0) {
            logger.info("美团推送隐私号降级处理，空请求pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        //推送实例：{"sig":"1081b9fdcf0e51612e09687a08ed6884","app_id":"5085","timestamp":"1629970755"}
        logger.info("美团推送推送隐私号降级处理：" + JSON.toJSONString(paramMap));
        String appId = (String) paramMap.get("app_id");

        //根据当前appid和店铺id 查找私钥
        List<GaiaTAccount> accountList = takeAwayService.getAllAccountByAppId(Constants.PLATFORMS_MT, appId);
        accountList = accountList.stream().filter(r -> "Y".equalsIgnoreCase(r.getOrderOnService())
                && Constants.PLATFORMS_MT.equalsIgnoreCase(r.getPlatform())).collect(Collectors.toList());
        if (CollUtil.isEmpty(accountList)) {
            logger.error("美团外卖-隐私号降级处理：找不到对应门店信息！appId:{}", appId);
            resultMap.put("data", "ok");
            return resultMap;
        }
        String appSecret = accountList.get(0).getAppSecret();
        int resultForValid = validApiParamForMt(paramMap, "privacyLevelDown", appSecret);
        logger.info("美团推送隐私号降级处理，验证sign， result:" + resultForValid);
        resultForValid = 1;
        // 1.校验接口参数
        if (resultForValid == 0) {
            //接口存活验证成功！
            resultMap.put("data", "ok");
            return resultMap;
        } else if (resultForValid == 2) {
            //美团订单推送API签名验证失败
            resultMap.put("code", Constants.ERROR_CODE_1001);
            resultMap.put("msg", "美团订单推送API签名验证失败!");
            setOperateLogInfo(logMap, "0", "美团订单推送API签名验证失败");
            // 5.接口调用操作日志记录
            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        } else {
            logger.info("美团外卖-推送API：隐私号降级处理！开始获取所有门店订单信息，并更新本地用户号码...");
            List<Map<String, String>> items = MtApiUtil.getBatchPullPhoneNumber(appId, appSecret);
            logger.info("美团外卖-推送API：隐私号降级处理！items:{}，appId:{}", JSON.toJSONString(items), appId);
            if (CollUtil.isNotEmpty(items)) {
                //order_id  real_phone_number
                String resultForXB = baseService.updateRecipientPhone(items);
                if (!resultForXB.equals("ok")) {
//                    setOperateLogInfo(logMap,"0",resultForXB+"\n");
                    logger.error("美团外卖-推送API：隐私号降级处理！修改订单客户隐私号失败，appId:{}", appId);
                } else {
//                    setOperateLogInfo(logMap, "0", "隐私号降级");
                }
            }
            resultMap.put("data", "ok");
        }
//        // 5.接口调用操作日志记录
//        baseService.insertOperatelogWithMap(logMap);
        return resultMap;
    }

    /**
     * 美团外卖：推送全额退款信息（必接）   --通
     * 用户申请退款
     *
     * @param request
     * @return
     * @author：Li.Deman
     * @date：2018/11/30
     */
    @ResponseBody
    @RequestMapping(value = "/allRefundReceive")
    public Map<String, Object> allRefundReceive(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //定义接口调用操作日志map对象(外卖平台调用小宝接口、美团)
        Map<String, Object> logMap = createOperateLogMap("5", "1", "allRefundReceive", "推送全额退款信息");
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            logger.info("美团推送全额退款信息，空请求pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        logger.info("美团推送全额退款信息：" + JSON.toJSONString(paramMap));
        String appId = (String) paramMap.get("app_id");
        //String platformShopId = (String) paramMap.get("app_poi_code");
        String platformOrderId = (String) paramMap.getOrDefault("order_id", "");

        //由于platformShopId为空，不得不从orderInfo表里先取stoCode
        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_MT, platformOrderId);
        if (orderInfo == null) {
            logger.info("美团推送全额退款信息, orderInfo = null , pass");
            resultMap.put("data", "error");
            return resultMap;
        }
        String stoCode = orderInfo.getStoCode();
        String client = orderInfo.getClient();
        //根据当前appid和店铺id 查找私钥
        //GaiaTAccount account = takeAwayService.getPlatformAccount("MT", appId, platformShopId);
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_MT, appId, stoCode, client);
        String defaultSecret = "";
        String orderOnService = "N";
        String nativeShopId = "";
        if (ObjectUtil.isNotEmpty(account)) {
            defaultSecret = account.getAppSecret();
            orderOnService = account.getOrderOnService();
            nativeShopId = account.getPublicCode();
        }
        if (account == null || !"Y".equalsIgnoreCase(orderOnService)) {
            logger.error("美团外卖-推送全额退款消息：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", platformOrderId, appId, stoCode);
            resultMap.put("data", "ok");
            return resultMap;
        }

        int resultForValid = validApiParamForMt(paramMap, "allRefundReceive", defaultSecret);
        logger.info("美团推送全额退款消息，验证sign， result:" + resultForValid);
        resultForValid = 1;
        // 1.校验接口参数
        if (resultForValid == 0) {
            //接口存活验证成功！
            resultMap.put("data", "ok");
            return resultMap;
        } else if (resultForValid == 2) {
            //美团订单推送API签名验证失败
            resultMap.put("code", Constants.ERROR_CODE_1001);
            resultMap.put("msg", "美团订单推送API签名验证失败!" + "平台订单ID：" + nullToEmpty(paramMap.get("order_id")));
            setOperateLogInfo(logMap, "0", "美团订单推送API签名验证失败" + "平台订单ID：" + platformOrderId);
            // 5.接口调用操作日志记录
            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        } else {
            // 2.根据平台订单ID，获取内部订单ID
            logger.info("美团外卖-推送API：全额退款信息！ 订单ID：" + platformOrderId);
            paramMap.put("platforms_order_id", platformOrderId);
            //重置order_id，只保留platforms_order_id
            paramMap.put("order_id", "");
            //这边是美团平台
            paramMap.put("platforms", Constants.PLATFORMS_MT);
            /**
             * 通知类型：apply：发起退款；agree：确认退款；
             * reject：驳回退款；cancelRefund：用户取消退款申请
             */
            String notify_type = nullToEmpty(paramMap.get("notify_type"));
            //记一下这个notify_type
            logger.info("美团外卖-推送API：全额退款信息！ 订单ID：" + platformOrderId + "，notify_type：" + notify_type);
            /**
             * 0：未处理；1：商家驳回退款请求；2、商家同意退款；
             * 3、客服驳回退款请求；4、客服帮商家同意退款；
             * 5、超过3小时自动同意；6、系统自动确认；
             * 7：用户取消退款申请；8：用户取消退款申诉
             */
            String res_type = nullToEmpty(paramMap.get("res_type"));
            /**
             * 是否申诉退款：0-否；1-是
             */
            String is_appeal = nullToEmpty(paramMap.get("is_appeal"));
            logger.info("美团外卖-推送API：全额退款信息！ 订单ID：" + platformOrderId
                    + "，res_type：" + res_type
                    + "，notify_type：" + notify_type
                    + "，is_appeal：" + is_appeal
                    + "，reason：" + nullToEmpty(paramMap.get("reason")));
            //先以通知类型=apply（发起退款）为例，这时候才进行下面的各项操作
            if (notify_type.equals("apply")) {
                //订单+订单明细。model+items
                PtsOrderInfo ptsOrderInfo = baseService.GetOrderInfo(paramMap);
                if (ptsOrderInfo.getResult().equals("ok")) {
                    BaseOrderInfo baseOrderInfo = getBaseOrderInfo(paramMap, ptsOrderInfo);
                    Map<Integer, String> refundQtyMap = new HashMap<>(4);
                    for (BaseOrderProInfo item : baseOrderInfo.getItems()) {
                        refundQtyMap.put(Integer.valueOf(item.getSequence()), item.getRefund_qty());
                    }
                    String resultParamChange = nullToEmpty(getOrderInfoToPTSByMapForRefund(baseOrderInfo).get("result"));
                    if (resultParamChange.equals("ok")) {
                        String resultForXB = baseService.updateOrderInfoWithEntity(baseOrderInfo);
                        if (!resultForXB.equals("ok")) {
//                            setOperateLogInfo(logMap, "0", resultForXB);
                        } else {
                            /**
                             * 攘外必须安内，先更新小宝系统
                             * 由于调用PTS接口都是走多线程，因此
                             * 直接调用PTS，更新小宝系统同步进行
                             */
                            //TaskExecutorUtil.asyncExecOrderPushForRefund(getOrderInfoToPTSByMapForRefund(baseOrderInfo));
                            Map<String, Object> retMap = getOrderInfoToPTSByMapForRefund(baseOrderInfo);
                            String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                            retMap.put("storeLogo", storeLogo);
                            retMap.put("refundQtyMap", refundQtyMap);
                            takeAwayService.pushToStoreMq(client, stoCode, "O2O", "tuikuan", retMap);
                            logger.info("美团推送全单退款订单，orderId:{},client:{},stoCode:{},storeLogo:{},refundQtyMap:{}",
                                    nullToEmpty(retMap.get("order_id")), client, stoCode, storeLogo, JSON.toJSONString(refundQtyMap));
                        }
                    } else {
//                        setOperateLogInfo(logMap, "0", resultParamChange);
                    }
                } else {
//                    setOperateLogInfo(logMap,"0",ptsOrderInfo.getResult());
                }
            } else if (notify_type.equals("agree")) {
                //成功退款的信息，不需要推送给PTS了
                Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
                if (nullToEmpty(retMap.get("result")).equals("ok")) {
                    /**
                     * 订单退款。订单退款状态，对于小宝来说就是外卖平台同意退款，
                     * 已发生退款，区别于订单取消
                     */
                    retMap.put("status", "5");
                    //同意用户退款申请
                    retMap.put("action", "6");
                    //之所以加这个，是为了记录到小宝订单系统
                    retMap.put("last_action", "6");
                    //取消原因
                    retMap.put("reason", nullToEmpty(paramMap.get("reason")));
//                    //取消类型
//                    retMap.put("reason_code",type);
                    retMap.put("notice_message", "refund");
                    //要不要再记一次退款金额和数量呢——全额退全部，暂时不计
                    String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                    if (!resultForXB.equals("ok")) {
                        //同意退款的订单状态不需要推送给PTS了
//                        setOperateLogInfo(logMap,"0",resultForXB);
                    }
                } else {
//                    setOperateLogInfo(logMap,"0",nullToEmpty(retMap.get("result")));
                }
            }
//            // 5.接口调用操作日志记录
//            baseService.insertOperatelogWithMap(logMap);
            resultMap.put("data", "ok");
            return resultMap;
        }
    }

    private BaseOrderInfo getBaseOrderInfo(Map<String, Object> paramMap, PtsOrderInfo ptsOrderInfo) {
        BaseOrderInfo baseOrderInfo = new BaseOrderInfo();
        baseOrderInfo.setPlatforms(ptsOrderInfo.getPlatforms());
        baseOrderInfo.setOrder_id(ptsOrderInfo.getOrder_id());
        baseOrderInfo.setPlatforms_order_id(ptsOrderInfo.getPlatforms_order_id());
        baseOrderInfo.setShop_no(ptsOrderInfo.getShop_no());
        baseOrderInfo.setRecipient_address(ptsOrderInfo.getRecipient_address());
        baseOrderInfo.setRecipient_name(ptsOrderInfo.getRecipient_name());
        baseOrderInfo.setRecipient_phone(ptsOrderInfo.getRecipient_phone());
        baseOrderInfo.setShipping_fee(ptsOrderInfo.getShipping_fee());
        baseOrderInfo.setTotal_price(ptsOrderInfo.getTotal_price());
        baseOrderInfo.setOriginal_price(ptsOrderInfo.getOriginal_price());
        baseOrderInfo.setCustomer_pay(ptsOrderInfo.getCustomer_pay());
        baseOrderInfo.setCaution(ptsOrderInfo.getCaution());
        baseOrderInfo.setShipper_name(ptsOrderInfo.getShipper_name());
        baseOrderInfo.setShipper_phone(ptsOrderInfo.getShipper_phone());
        baseOrderInfo.setStatus(ptsOrderInfo.getStatus());
        baseOrderInfo.setAction("5");
        baseOrderInfo.setIs_invoiced(ptsOrderInfo.getIs_invoiced());
        baseOrderInfo.setInvoice_title(ptsOrderInfo.getInvoice_title());
        baseOrderInfo.setTaxpayer_id(ptsOrderInfo.getTaxpayer_id());
        baseOrderInfo.setDelivery_time(ptsOrderInfo.getDelivery_time());
        baseOrderInfo.setPay_type(ptsOrderInfo.getPay_type());
        baseOrderInfo.setOrder_info(ptsOrderInfo.getOrder_info());
        baseOrderInfo.setCreate_time(ptsOrderInfo.getCreate_time());
        baseOrderInfo.setUpdate_time(ptsOrderInfo.getUpdate_time());
        //reason_code
        baseOrderInfo.setReason(nullToEmpty(paramMap.get("reason")));
        baseOrderInfo.setDay_seq(ptsOrderInfo.getDay_seq());
        baseOrderInfo.setRefund_money(ptsOrderInfo.getTotal_price());
        baseOrderInfo.setNotice_message("refund");
        baseOrderInfo.setClient(ptsOrderInfo.getClient());
        baseOrderInfo.setStoCode(ptsOrderInfo.getSto_code());
        List<BaseOrderProInfo> items = new ArrayList<>();
        for (PtsOrderProInfo item : ptsOrderInfo.getItems()) {
            BaseOrderProInfo order_item = new BaseOrderProInfo();
            order_item.setOrder_id(ptsOrderInfo.getOrder_id());
            order_item.setMedicine_code(item.getMedicine_code());
            //全额退款每个商品的退款数量都是下单数量
            order_item.setRefund_qty(item.getQuantity());
            order_item.setRefund_price(item.getPrice());
            //暂时不计退款金额
            order_item.setSequence(item.getSequence());
            order_item.setClient(item.getClient());
            order_item.setStoCode(item.getSto_code());
            items.add(order_item);
        }
        baseOrderInfo.setItems(items);
        return baseOrderInfo;
    }

    /**
     * 美团外卖：推送部分退款信息（必接）
     *
     * @param request
     * @return
     * @author：Li.Deman
     * @date：2018/11/30
     */
    @ResponseBody
    @RequestMapping(value = "/partRefundReceive")
    public Map<String, Object> partRefundReceive(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //定义接口调用操作日志map对象(外卖平台调用小宝接口、美团)
        Map<String, Object> logMap = createOperateLogMap("5",
                "1", "partRefundReceive",
                "推送部分退款信息");
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            logger.info("美团推送部分退款信息，空请求pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        logger.info("美团推送部分退款信息：" + JSON.toJSONString(paramMap));
        String appId = (String) paramMap.get("app_id");
        //String platformShopId = (String) paramMap.get("app_poi_code");
        String platformOrderId = (String) paramMap.getOrDefault("order_id", "");

        //由于platformShopId为空，不得不从orderInfo表里先取stoCode
        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_MT, platformOrderId);
        if (orderInfo == null) {
            logger.info("美团推送部分退款信息, orderInfo = null , pass");
            resultMap.put("data", "error");
            return resultMap;
        }
        String client = orderInfo.getClient();
        String stoCode = orderInfo.getStoCode();
        //根据当前appid和店铺id 查找私钥
        //GaiaTAccount account = takeAwayService.getPlatformAccount("MT", appId, platformShopId);
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_MT, appId, stoCode, client);
        String defaultSecret = "";
        String orderOnService = "N";
        String nativeShopId = "";
        if (ObjectUtil.isNotEmpty(account)) {
            defaultSecret = account.getAppSecret();
            orderOnService = account.getOrderOnService();
            nativeShopId = account.getPublicCode();
        }
        if (account == null || !"Y".equalsIgnoreCase(orderOnService)) {
            logger.error("美团外卖-推送部分退款信息：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", platformOrderId, appId, stoCode);
            resultMap.put("data", "ok");
            return resultMap;
        }

        int resultForValid = validApiParamForMt(paramMap, "partRefundReceive", defaultSecret);
        logger.info("美团推送部分退款信息，验证sign， result:" + resultForValid);
        resultForValid = 1;
        // 1.校验接口参数
        if (resultForValid == 0) {
            //接口存活验证成功！
            resultMap.put("data", "ok");
            return resultMap;
        } else if (resultForValid == 2) {
            //美团订单推送API签名验证失败
            resultMap.put("code", Constants.ERROR_CODE_1001);
            resultMap.put("msg", "美团订单推送API签名验证失败!");
            setOperateLogInfo(logMap, "0", "美团订单推送API签名验证失败" + "平台订单ID：" + platformOrderId);
            // 5.接口调用操作日志记录
            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        } else {
            // 2.根据平台订单ID
            //String orderId = nullToEmpty(paramMap.get("order_id"));
            BaseOrderInfo baseOrderInfo = MtApiUtil.convertToBaseOrderForRefund(paramMap);
            if (baseOrderInfo.getResult().equals("ok")) {
                baseOrderInfo.setPlatforms_order_id(platformOrderId);
                logger.info("美团外卖-推送API：部分退款信息！ 订单ID：" + platformOrderId);
                paramMap.put("platforms_order_id", platformOrderId);
                paramMap.put("order_id", "");
                //加一下平台类型
                paramMap.put("platforms", Constants.PLATFORMS_MT);
                /**
                 * 通知类型，part：发起部分退款；agree：确认退款；
                 * reject：驳回退款；cancelRefund：用户取消退款申请
                 */
                String notify_type = nullToEmpty(paramMap.get("notify_type"));
                //记一下这个notify_type
                logger.info("美团外卖-推送API：部分退款信息！ 订单ID：" + platformOrderId + "，notify_type：" + notify_type);
                /**
                 * 0：未处理；1：商家驳回退款请求；2、商家同意退款；
                 * 3、客服驳回退款请求；4、客服帮商家同意退款；
                 * 5、超过3小时自动同意；6、系统自动确认；
                 * 7：用户取消退款申请；8：用户取消退款申诉
                 */
                String res_type = nullToEmpty(paramMap.get("res_type"));
                //记一下这个res_type
                logger.info("美团外卖-推送API：部分退款信息！ 订单ID：" + platformOrderId + "，res_type：" + res_type);
                /**
                 * 是否申诉退款：0-否；1-是
                 */
                String is_appeal = nullToEmpty(paramMap.get("is_appeal"));
                //记一下这个is_appeal
                logger.info("美团外卖-推送API：部分退款信息！ 订单ID：" + platformOrderId + "，is_appeal：" + is_appeal);
                //先以通知类型=part（发起退款）为例，这时候才进行下面的各项操作
                if (notify_type.equals("part")) {
                    //订单+订单明细。model+items
                    PtsOrderInfo ptsOrderInfo = baseService.GetOrderInfo(paramMap);
                    if (ptsOrderInfo.getResult().equals("ok")) {
                        baseOrderInfo.setPlatforms(ptsOrderInfo.getPlatforms());
                        baseOrderInfo.setOrder_id(ptsOrderInfo.getOrder_id());
                        baseOrderInfo.setShop_no(ptsOrderInfo.getShop_no());
                        baseOrderInfo.setRecipient_address(ptsOrderInfo.getRecipient_address());
                        baseOrderInfo.setRecipient_name(ptsOrderInfo.getRecipient_name());
                        baseOrderInfo.setRecipient_phone(ptsOrderInfo.getRecipient_phone());
                        baseOrderInfo.setShipping_fee(ptsOrderInfo.getShipping_fee());
                        baseOrderInfo.setTotal_price(ptsOrderInfo.getTotal_price());
                        baseOrderInfo.setOriginal_price(ptsOrderInfo.getOriginal_price());
                        baseOrderInfo.setCustomer_pay(ptsOrderInfo.getCustomer_pay());
                        baseOrderInfo.setCaution(ptsOrderInfo.getCaution());
                        baseOrderInfo.setShipper_name(ptsOrderInfo.getShipper_name());
                        baseOrderInfo.setShipper_phone(ptsOrderInfo.getShipper_phone());
                        baseOrderInfo.setStatus(ptsOrderInfo.getStatus());
                        baseOrderInfo.setAction("5");
                        baseOrderInfo.setIs_invoiced(ptsOrderInfo.getIs_invoiced());
                        baseOrderInfo.setInvoice_title(ptsOrderInfo.getInvoice_title());
                        baseOrderInfo.setTaxpayer_id(ptsOrderInfo.getTaxpayer_id());
                        baseOrderInfo.setDelivery_time(ptsOrderInfo.getDelivery_time());
                        baseOrderInfo.setPay_type(ptsOrderInfo.getPay_type());
                        baseOrderInfo.setOrder_info(ptsOrderInfo.getOrder_info());
                        baseOrderInfo.setCreate_time(ptsOrderInfo.getCreate_time());
                        baseOrderInfo.setUpdate_time(ptsOrderInfo.getUpdate_time());
                        baseOrderInfo.setDay_seq(ptsOrderInfo.getDay_seq());
                        baseOrderInfo.setNotice_message("refund");
                        baseOrderInfo.setClient(ptsOrderInfo.getClient());
                        baseOrderInfo.setStoCode(ptsOrderInfo.getSto_code());
                        Map<Integer, String> refundQtyMap = new HashMap<>(4);
                        for (BaseOrderProInfo item : baseOrderInfo.getItems()) {
                            item.setOrder_id(ptsOrderInfo.getOrder_id());
                            //使用lambda表达式
                            List<PtsOrderProInfo> ptsList = ptsOrderInfo.getItems().stream().filter((PtsOrderProInfo p) ->
                                            item.getMedicine_code().equals(p.getMedicine_code())).collect(Collectors.toList());
                            //如果有，那么取第一个
                            if (ptsList.size() > 0) {
                                List<PtsOrderProInfo> tempList = ptsList.stream().filter(r -> {
                                    String price = r.getPrice();
                                    String foodPrice = item.getFoodPrice();
                                    return new BigDecimal(price).compareTo(new BigDecimal(foodPrice)) == 0;
                                }).collect(Collectors.toList());
                                PtsOrderProInfo originalProInfo;
                                if (CollUtil.isEmpty(tempList)) {
                                    logger.error("美团推送部分退款，无匹配的foodPrice,item:{},ptsList:{}", JSON.toJSONString(item), JSON.toJSONString(ptsList));
                                    originalProInfo = ptsList.get(0);
                                } else {
                                    //价格一样的会做合并，直接取第一个即可
                                    originalProInfo = tempList.get(0);
                                }
                                String sequence = originalProInfo.getSequence();
                                item.setSequence(sequence);
                                refundQtyMap.put(Integer.valueOf(sequence), item.getRefund_qty());
                            }
                            item.setClient(ptsOrderInfo.getClient());
                            item.setStoCode(ptsOrderInfo.getSto_code());
//                        for (PtsOrderProInfo ptsItem : ptsOrderInfo.getItems()){
//                            if (item.getOrder_id().equals(ptsItem.getOrder_id())
//                            && item.getMedicine_code().equals(ptsItem.getMedicine_code())){
//                                item.setSequence(ptsItem.getSequence());
//                                break;
//                            }
//                        }
                        }
                        String resultParamChange = nullToEmpty(getOrderInfoToPTSByMapForRefund(baseOrderInfo).get("result"));
                        if (resultParamChange.equals("ok")) {
                            String resultForXB = baseService.updateOrderInfoWithEntity(baseOrderInfo);
                            if (!resultForXB.equals("ok")) {
//                                setOperateLogInfo(logMap, "0", resultForXB);
                            } else {
                                /**
                                 * 攘外必须安内，先更新小宝系统
                                 * 由于调用PTS接口都是走多线程，因此
                                 * 直接调用PTS，更新小宝系统同步进行
                                 */
                                //TaskExecutorUtil.asyncExecOrderPushForRefund(getOrderInfoToPTSByMapForRefund(baseOrderInfo));
                                Map<String, Object> retMap = getOrderInfoToPTSByMapForRefund(baseOrderInfo);
                                String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                                retMap.put("storeLogo", storeLogo);
                                retMap.put("refundQtyMap", refundQtyMap);
                                takeAwayService.pushToStoreMq(client, stoCode, "O2O", "tuikuan", retMap);
                                logger.info("美团推送部分退款订单，orderId:{},client:{},stoCode:{},storeLogo:{},refundQtyMap:{}",
                                        nullToEmpty(retMap.get("order_id")), client, stoCode, storeLogo, JSON.toJSONString(refundQtyMap));
                            }
                        } else {
//                            setOperateLogInfo(logMap, "0", resultParamChange);
                        }
                    } else {
//                        setOperateLogInfo(logMap,"0",ptsOrderInfo.getResult());
                    }
                } else if (notify_type.equals("agree")) {
                    Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
                    if (nullToEmpty(retMap.get("result")).equals("ok")) {
                        /**
                         * 订单退款。订单退款状态，对于小宝来说就是外卖平台同意退款，
                         * 已发生退款，区别于订单取消
                         */
                        retMap.put("status", "5");
                        //同意用户退款申请
                        retMap.put("action", "6");
                        //之所以加这个，是为了记录到小宝订单系统
                        retMap.put("last_action", "6");
                        //取消原因
                        retMap.put("reason", nullToEmpty(paramMap.get("reason")));
//                        //取消类型
//                        retMap.put("reason_code",type);
                        retMap.put("notice_message", "refund");
                        //部分也暂时不计，就算重新修改退款信息，我们先都认为从第一步申请开启
                        String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                        if (!resultForXB.equals("ok")) {
//                            setOperateLogInfo(logMap,"0",resultForXB);
                        }
                    } else {
//                        setOperateLogInfo(logMap,"0",nullToEmpty(retMap.get("result")));
                    }
                }
            } else {
//                setOperateLogInfo(logMap, "0", baseOrderInfo.getResult());
            }
            resultMap.put("data", "ok");
//            // 5.接口调用操作日志记录
//            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        }
    }

    /**
     * @param request
     * @return
     * @description：推送订单配送状态
     * @author：Li.Deman
     * @date：2018/11/30
     */
    @ResponseBody
    @RequestMapping(value = "/orderLogisticsStatus", method = RequestMethod.POST)
    public Map<String, Object> orderLogisticsStatus(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //定义接口调用操作日志map对象(外卖平台调用小宝接口、美团)
        Map<String, Object> logMap = createOperateLogMap("5", "1", "orderLogisticsStatus", "推送订单配送状态");
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            logger.info("美团推送订单配送状态，空请求pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        logger.info("美团推送订单配送状态：" + JSON.toJSONString(paramMap));
        String appId = (String) paramMap.get("app_id");
        //String platformShopId = (String) paramMap.get("app_poi_code");
        String platformOrderId = (String) paramMap.getOrDefault("order_id", "");

        //由于platformShopId为空，不得不从orderInfo表里先取stoCode
        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_MT, platformOrderId);
        if (orderInfo == null) {
            logger.info("美团推送订单配送状态, orderInfo = null , pass");
            resultMap.put("data", "error");
            return resultMap;
        }
        String client = orderInfo.getClient();
        String stoCode = orderInfo.getStoCode();
        //根据当前appid和店铺id 查找私钥
        //GaiaTAccount account = takeAwayService.getPlatformAccount("MT", appId, platformShopId);
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_MT, appId, stoCode, client);
        String defaultSecret = "";
        String orderOnService = "N";
        String nativeShopId = "";
        if (ObjectUtil.isNotEmpty(account)) {
            defaultSecret = account.getAppSecret();
            orderOnService = account.getOrderOnService();
            nativeShopId = account.getPublicCode();
        }
        if (account == null || !"Y".equalsIgnoreCase(orderOnService)) {
            logger.error("美团外卖-推送订单配送状态：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", platformOrderId, appId, stoCode);
            resultMap.put("data", "ok");
            return resultMap;
        }

        int resultForValid = validApiParamForMt(paramMap, "orderLogisticsStatus", defaultSecret);
        logger.info("美团推送订单配送状态，验证sign， result:" + resultForValid);
        resultForValid = 1;
        // 1.校验接口参数
        if (resultForValid == 0) {
            //接口存活验证成功！
            resultMap.put("data", "ok");
            return resultMap;
        } else if (resultForValid == 2) {
            //美团订单推送API签名验证失败
            resultMap.put("code", Constants.ERROR_CODE_1001);
            resultMap.put("msg", "美团订单推送API签名验证失败!");
            setOperateLogInfo(logMap, "0", "美团订单推送API签名验证失败"
                    + "平台订单ID：" + nullToEmpty(paramMap.get("order_id")));
            // 5.接口调用操作日志记录
            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        } else {
            // 2.根据平台订单ID，获取内部订单ID
            //String orderId = nullToEmpty(paramMap.get("order_id"));
            logger.info("美团外卖-推送API：订单配送状态！ 订单ID：" + platformOrderId);
            paramMap.put("platforms_order_id", platformOrderId);
            //重置order_id，只保留platforms_order_id
            paramMap.put("order_id", "");
            //平台：美团
            paramMap.put("platforms", Constants.PLATFORMS_MT);
            Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
            if (nullToEmpty(retMap.get("result")).equals("ok")) {
                //status不变
                //新的状态：已取消
//                retMap.put("status","2");
                //确认配送员动作
//                retMap.put("action", "8");
//                retMap.put("last_action", "8");
                retMap.put("shipper_name", nullToEmpty(paramMap.get("dispatcher_name")));
                retMap.put("shipper_phone", nullToEmpty(paramMap.get("dispatcher_mobile")));
//                retMap.put("notice_message", "");
                logger.info("美团外卖推送订单配送状态，配送订单状态：" + nullToEmpty(paramMap.get("logistics_status")));
                /**
                 * 新版本状态：
                 * 0	配送单发往配送
                 * 5	配送压单
                 * 10	配送单已确认
                 * 15	骑手已到店
                 * 20	骑手已取餐
                 * 40	骑手已送达
                 * 100	配送单已取消 -- 骑手的取消操作
                 */
                if (nullToEmpty(paramMap.get("logistics_status")).equals("15") || nullToEmpty(paramMap.get("logistics_status")).equals("10")) {
                    if (!nullToEmpty(retMap.get("shipper_name")).equals("")) {
                        String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                        if (resultForXB.equals("ok")) {
                            //订单状态变更推送PTS：用线程处理
                            //TaskExecutorUtil.asyncExecOrderStatusChangePush(retMap); //秦青于删除20190116删除，暂时不需要推送配送员信息
                            resultMap.put("data", "ok");
//                            setOperateLogInfo(logMap, "0", "订单ID：" + orderId+" 状态："+nullToEmpty(paramMap.get("logistics_status")));
                        } else {
//                            setOperateLogInfo(logMap, "0", resultForXB);
                            resultMap.put("data", resultForXB);
                        }
                    }
                }
                /**
                 * 主动调用取消订单API，美团只会推送订单配送状态接口
                 * 退单原因没传过来，只能在FX调用退单的时候塞到数据里
                 */
                //骑手的状态暂无需关注
                /*if (nullToEmpty(paramMap.get("logistics_status")).equals("100")) {
                    retMap.put("status", "4");
                    //用户申请退单
                    retMap.put("action", "4");
                    retMap.put("last_action", "4");
                    retMap.put("notice_message", "cancel");
                    String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                    if ("ok".equals(resultForXB)) {
                        String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                        retMap.put("storeLogo", storeLogo);
                        takeAwayService.pushToStoreMq(client, stoCode, "O2O", "cancel", retMap);
                        logger.info("美团推送取消订单，orderId:{},client:{},stoCode:{},storeLogo:{}", nullToEmpty(retMap.get("order_id")), client, stoCode, storeLogo);
                        resultMap.put("data", "ok");
                    } else {
                        resultMap.put("data", "error");
                    }
                }*/
                //其余暂时pass
                /*else {
                    resultMap.put("data", "ok");
                }*/
                resultMap.put("data", "ok");
            } else {
//                setOperateLogInfo(logMap,"0",nullToEmpty(retMap.get("result")));
                resultMap.put("data", retMap.get("result"));
            }
//            // 5.接口调用操作日志记录
//            setOperateLogInfo(logMap, "0", "订单ID：" + orderId+" 状态："+nullToEmpty(paramMap.get("logistics_status")));
//            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        }
    }


    /**
     * @Description: 推送订单应结算金额变更
     * 测试发现，在用户新建订单还未付款的时候就推送，实际不需要，暂时不接此接口
     * @Param: [request]
     * @return: java.util.Map<java.lang.String, java.lang.Object>
     * @Author: Qin.Qing
     * @Date: 2019/06/04
     */
    @ResponseBody
    @RequestMapping(value = "/orderPayChanged", method = RequestMethod.POST)
    public Map<String, Object> orderPayChanged(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        Map<String, Object> logMap = createOperateLogMap("5", "1", "orderPayChanged", "推送订单应结算金额变更");
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            logger.info("美团推送订单应结算金额变更，空请求pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        logger.info("美团推送订单应结算金额变更：" + JSON.toJSONString(paramMap));
        String appId = (String)paramMap.get("app_id");
        //String platformShopId = (String)paramMap.get("app_poi_code");
        String platformOrderId = (String)paramMap.getOrDefault("order_id","");

        //由于platformShopId为空，不得不从orderInfo表里先取stoCode
        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_MT, platformOrderId);
        if (orderInfo == null) {
            logger.info("美团推送订单应结算金额变更, orderInfo = null , pass");
            resultMap.put("data", "ok");
            return resultMap;
        }
        String client = orderInfo.getClient();
        String stoCode = orderInfo.getStoCode();
        //根据当前appid和店铺id 查找私钥
        //GaiaTAccount account = takeAwayService.getPlatformAccount("MT", appId, platformShopId);
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_MT, appId, stoCode, client);
        String defaultSecret = "";
        String orderOnService = "N";
        String nativeShopId = "";
        if (ObjectUtil.isNotEmpty(account)) {
            defaultSecret = account.getAppSecret();
            orderOnService = account.getOrderOnService();
            nativeShopId = account.getPublicCode();
        }
        if (account == null || !"Y".equalsIgnoreCase(orderOnService)) {
            logger.error("美团外卖-推送订单应结算金额变更信息：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", platformOrderId, appId, stoCode);
            resultMap.put("data", "ok");
            return resultMap;
        }

        int resultForValid = validApiParamForMt(paramMap, "orderPayChanged", defaultSecret);
        logger.info("美团推送订单应结算金额变更信息，验证sign， result:" + resultForValid);
        resultForValid = 1;
        // 1.校验接口参数
        if (resultForValid == 0) {
            //接口存活验证成功！
            resultMap.put("data", "ok");
            return resultMap;
        } else if (resultForValid == 2) {
            //美团订单推送API签名验证失败
            resultMap.put("code", Constants.ERROR_CODE_1001);
            resultMap.put("msg", "美团订单推送API签名验证失败!");
            setOperateLogInfo(logMap, "0", "美团订单推送API签名验证失败" + "平台订单ID：" + platformOrderId);
            // 5.接口调用操作日志记录
            baseService.insertOperatelogWithMap(logMap);
            return resultMap;
        } else {
            logger.info("美团外卖-推送API：推送订单应结算金额变更！ 订单ID：" + platformOrderId);
            paramMap.put("platforms_order_id", platformOrderId);
            paramMap.put("order_id", "");
            paramMap.put("platforms", Constants.PLATFORMS_MT);
            Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
            if (nullToEmpty(retMap.get("result")).equals("ok")) {
                //确认更改金额
                String settleAmount = nullToEmpty(paramMap.get("settleAmount"));
                retMap.put("total_price", settleAmount);
                retMap.put("refund_money", "0");
                //retMap.put("notice_message", "");
                logger.info("美团外卖推送订单金额变更，订单号：" + platformOrderId + " 订单金额：" + settleAmount);
                String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                if (resultForXB.equals("ok")) {
                    //订单状态变更推送PTS：用线程处理
                    //todo 目前并无此功能点
                    //TaskExecutorUtil.asyncExecOrderStatusChangePush(retMap);
                    resultMap.put("data", "ok");
                } else {
                    resultMap.put("data", resultForXB);
                }

            } else {
                resultMap.put("data", retMap.get("result"));
            }
            return resultMap;
        }
    }


    /**
     * 美团外卖：推送API接口参数验证
     *
     * @param paramMap
     * @param url
     * @return 0-接口存活验证成功|1-签名验证成功|2-签名验证失败
     * @author：Li.Deman
     * @date：2018/12/14
     */
    public int validApiParamForMt(Map<String, Object> paramMap, String url, String appSecret) {
        String sig = nullToEmpty(paramMap.get("sig"));// 签名结果
        String timestamp = nullToEmpty(paramMap.get("timestamp"));// 时间戳
        String app_id = nullToEmpty(paramMap.get("app_id"));// 美团分配给APP方的id
        //系统级3个参数都要验证，如果都为空，说明美团只是在做一个接口存活验证
        if (StringUtils.isNullOrEmpty(sig)
                && StringUtils.isNullOrEmpty(timestamp)
                && StringUtils.isNullOrEmpty(app_id)) {
            logger.debug("接口存活验证成功！");
            return 0;
        }
        //咋样验证签名，需要单独计算签名计算
        boolean flag = SignUtil.checkSign(paramMap, url, appSecret);
        if (flag) {
            logger.debug("美团订单推送API签名验证成功,签名: " + sig);
            return 1;
        } else {
            logger.debug("美团订单推送API签名验证失败,签名: " + sig);
            System.out.println(paramMap);
            return 2;
        }
    }
    
    @GetMapping("test")
    @ResponseBody
    public  String test() {
//        GaiaTAccount account = takeAwayService.selectStoCodeByPlatformsAndPublicCodeAndAppId("MT","cre9n1ap", "7165");
//        System.err.println(JSON.toJSONString(account));
//        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId("MT", "69772742153862843");
//        System.err.println(JSON.toJSONString(orderInfo));
//        GaiaTAccount mt = takeAwayService.getPlatformAccountByStoCode("MT", "7165", "10000", "10000058");
//        System.err.println(JSON.toJSONString(mt));
        List<WeideProIdDto> weideMtProId = takeAwayService.getWeideMtProId("10000005", "10000", true, "12");
        return JSON.toJSONString(weideMtProId);
    }
}
