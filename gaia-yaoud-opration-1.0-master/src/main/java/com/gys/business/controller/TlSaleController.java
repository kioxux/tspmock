package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdSaleH;
import com.gys.business.mapper.entity.GaiaSupplierBusiness;
import com.gys.business.service.TlSaleService;
import com.gys.business.service.data.ElectronBasicOutData;
import com.gys.business.service.data.GetSaleReturnDetailOutData;
import com.gys.business.service.data.LoginParam;
import com.gys.business.service.data.QueryParam;
import com.gys.business.service.data.WebService.Prescrip;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.webService.InterfaceVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/tlSale/"})
public class TlSaleController extends BaseController {

    @Resource
    TlSaleService tlSaleService;

    public TlSaleController(){

    }
    @ApiOperation(value = "查询代煎供应商列表", response = GaiaSupplierBusiness.class)
    @PostMapping({"querySupplier"})
    public JsonResult querySupplier(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.tlSaleService.querySupplier(userInfo.getClient(),userInfo.getDepId()), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询代煎订单", response = GaiaSdSaleH.class)
    @PostMapping({"queryDjSaleOrder"})
    public JsonResult queryDjSaleOrder(HttpServletRequest request,@RequestBody QueryParam inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setBrId(userInfo.getDepId());
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.tlSaleService.queryDjSaleOrder(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询代煎订单商品", response = GetSaleReturnDetailOutData.class)
    @PostMapping({"queryDjSaleOrderDetail"})
    public JsonResult queryDjSaleOrderDetail(HttpServletRequest request,@RequestBody QueryParam inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setBrId(userInfo.getDepId());
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.tlSaleService.queryDjSaleOrderDetail(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "保存并上传接口", response = InterfaceVO.class)
    @PostMapping({"tlSaveAndPush"})
    public JsonResult tlSaveAndPush(HttpServletRequest request,@RequestBody Prescrip inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.tlSaleService.tlSaveAndPush(userInfo.getClient(),userInfo.getDepId(),inData), "提示：获取数据成功！");
    }
    @ApiOperation(value = "天灵登录", response = InterfaceVO.class)
    @PostMapping({"tlLogin"})
    public JsonResult tlLogin(HttpServletRequest request,@RequestBody LoginParam loginParam) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.tlSaleService.tlLogin(loginParam), "提示：获取数据成功！");
    }
    @ApiOperation(value = "天灵处方明细", response = InterfaceVO.class)
    @PostMapping({"queryChinmedRelateDByBillNo"})
    public JsonResult queryChinmedRelateDByBillNo(HttpServletRequest request,@RequestBody QueryParam inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setBrId(userInfo.getDepId());
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.tlSaleService.queryChinmedRelateDByBillNo(inData), "提示：获取数据成功！");
    }
}
