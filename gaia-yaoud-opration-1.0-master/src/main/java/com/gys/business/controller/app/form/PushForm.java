package com.gys.business.controller.app.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;



/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/9 14:35
 **/
@Data
public class PushForm {
    /**加盟商*/
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(hidden = true)
    private String userId;
    @ApiModelProperty(hidden = true)
    private String loginName;
    @ApiModelProperty(value = "缺断货流水号", example = "102")
    private String voucherId;
    @ApiModelProperty(value = "推送类型：1-商采负责人 2-营运负责人", example = "1")
    private Integer pushType;
    /**商品列表*/
    @ApiModelProperty(value = "商品编码列表", example = "[]")
    private List<String> proSelfCodeList;

    private String startDate;
    private String endDate;
}
