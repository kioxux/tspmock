package com.gys.business.controller;

import com.gys.business.service.RabbitmqService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/rabbitmqProductor/"})
public class RabbitmqProductorController extends BaseController {
    @Autowired
    private RabbitmqService rabbitmqService;

    public RabbitmqProductorController() {
    }

    /**
     * 测试rabbitmq数据传输
     * @param request
     * @return
     */
    @PostMapping({"send"})
    public JsonResult send(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
//        this.rabbitmqService.send(userInfo.getClient(), userInfo.getDepId());
        return JsonResult.success("", "提示：上传数据成功！");
    }
}
