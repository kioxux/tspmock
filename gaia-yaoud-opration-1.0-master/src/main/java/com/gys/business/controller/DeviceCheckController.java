//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.DeviceCheckService;
import com.gys.business.service.data.GetDeviceCheckInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/deviceCheck/"})
public class DeviceCheckController extends BaseController {
    @Resource
    private DeviceCheckService deviceCheckService;

    public DeviceCheckController() {
    }

    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody GetDeviceCheckInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsdcBrId(userInfo.getDepId());
        return JsonResult.success(this.deviceCheckService.selectList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"save"})
    public JsonResult save(HttpServletRequest request, @RequestBody GetDeviceCheckInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsdcBrId(userInfo.getDepId());
        inData.setGsdcBrName(userInfo.getDepName());
        inData.setGsdcUpdateEmp(userInfo.getUserId());
        this.deviceCheckService.save(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"update"})
    public JsonResult update(HttpServletRequest request, @RequestBody GetDeviceCheckInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsdcBrId(userInfo.getDepId());
        inData.setGsdcUpdateEmp(userInfo.getUserId());
        this.deviceCheckService.update(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody GetDeviceCheckInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.deviceCheckService.approve(inData);
        return JsonResult.success("", "提示：保存成功！");
    }
}
