package com.gys.business.controller;

import com.gys.business.service.ClSysParaForProNewSaleService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 17:01 2021/8/20
 * @Description：商品首营页面勾选新品铺货开关
 * @Modified By：guoyuxi.
 * @Version:
 */
@RestController
@RequestMapping({"proNewSale"})
public class ClSysParaForProNewSaleController extends BaseController {
    @Autowired
    private ClSysParaForProNewSaleService clSysParaForProNewSaleService;

    @PostMapping({"saveStatus"})
    public JsonResult saveStatus(HttpServletRequest request, @RequestBody Map<String,String> statusMap) {
        statusMap.put("client",this.getLoginUser(request).getClient());
        return clSysParaForProNewSaleService.saveStatus(statusMap);
    }

    @GetMapping({"getStatus"})
    public JsonResult getStatus(HttpServletRequest request) {
        GetLoginOutData loginUser = this.getLoginUser(request);
        return clSysParaForProNewSaleService.getStatus(loginUser);
    }
}
