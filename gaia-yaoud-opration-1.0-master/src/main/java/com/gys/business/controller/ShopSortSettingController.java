//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.ShopSortService;
import com.gys.business.service.data.ConllectionInData;
import com.gys.business.service.data.GetShopInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/shopSortSetting/"})
public class ShopSortSettingController extends BaseController {
    @Resource
    private ShopSortService shopSortService;

    public ShopSortSettingController() {
    }

    @PostMapping({"querySort"})
    public JsonResult querySort(@RequestBody GetShopInData inData) {
        return JsonResult.success(this.shopSortService.querySort(inData), "提示：获取数据成功！");
    }

    @PostMapping({"queryDetail"})
    public JsonResult queryDetail(@RequestBody GetShopInData inData) {
        return JsonResult.success(this.shopSortService.queryDetail(inData), "提示：获取数据成功！");
    }

    @PostMapping({"add"})
    public JsonResult add(HttpServletRequest request, @RequestBody GetShopInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssgUpdateEmp(userInfo.getUserId());
        this.shopSortService.add(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"editSave"})
    public JsonResult editSave(HttpServletRequest request, @RequestBody GetShopInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssgUpdateEmp(userInfo.getUserId());
        this.shopSortService.editSave(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"addAll"})
    public JsonResult addAll(@RequestBody ConllectionInData inData) {
        this.shopSortService.addAll(inData.getShopInData());
        return JsonResult.success("", "提示：获取数据成功！");
    }

    @PostMapping({"delete"})
    public JsonResult delete(@RequestBody ConllectionInData inData) {
        this.shopSortService.delete(inData.getShopInData());
        return JsonResult.success("", "提示：删除成功！");
    }

    @PostMapping({"update"})
    public JsonResult update(HttpServletRequest request, @RequestBody ConllectionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.shopSortService.update(inData.getShopDetailOutData(), userInfo);
        return JsonResult.success("", "提示：更新成功！");
    }

    @PostMapping({"queryStore"})
    public JsonResult queryStore(@RequestBody GetShopInData inData) {
        return JsonResult.success(this.shopSortService.queryStore(inData), "提示：更新成功！");
    }
}
