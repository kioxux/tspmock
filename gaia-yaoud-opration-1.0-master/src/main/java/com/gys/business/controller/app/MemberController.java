package com.gys.business.controller.app;

import com.gys.business.service.appservice.MemberService;
import com.gys.business.service.data.GetMemberInData;
import com.gys.business.service.data.GetQueryMemberInData;
import com.gys.business.service.data.GetQueryMemberOutData;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.feign.ElectronicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 会员
 *
 * @author xiaoyuan on 2021/3/8
 */
@Api(tags = "会员app接口")
@RestController("appMember")
@RequestMapping({"/app/member/"})
public class MemberController extends BaseController {

    @Autowired
    private MemberService memberService;

    @Autowired
    private ElectronicService electronicService;

    @ApiOperation(value = "会员查询" ,response = GetQueryMemberOutData.class)
    @PostMapping({"queryMember"})
    public JsonResult queryMember(HttpServletRequest request,@RequestBody GetQueryMemberInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.memberService.queryMember(inData), "success");
    }


    @ApiOperation(value = "会员新增")
    @PostMapping({"addMember"})
    @FrequencyValid
    public JsonResult addMember(HttpServletRequest request,@Valid @RequestBody GetMemberInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setSaler(userInfo.getUserId());
        if(this.memberService.addMember(inData)){
            //调用注册送券服务
            this.electronicService.addRegisterElectron(inData.getClientId(), inData.getGmbCardId());

            GetQueryMemberInData member = new GetQueryMemberInData();
            member.setClientId(userInfo.getClient());
            member.setBrId(inData.getBrId());
            member.setPhone(inData.getGmbMobile());
            return JsonResult.success(this.memberService.queryMember(member), "success");
        }else{
            return JsonResult.success("", "error");
        }
    }

}
