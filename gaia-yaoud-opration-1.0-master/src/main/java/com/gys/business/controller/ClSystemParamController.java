package com.gys.business.controller;

import com.gys.business.service.GaiaClSystemParamService;
import com.gys.business.service.data.UserInData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @desc:
 * @author: RULAISZ
 * @createTime: 2021/12/30 18:46
 */
@RestController
@RequestMapping("clSystem")
public class ClSystemParamController {
    @Autowired
    private GaiaClSystemParamService gaiaClSystemParamService;

    @PostMapping("getEnv")
    public JsonResult getEnv(@RequestBody UserInData inData){
        return JsonResult.success(this.gaiaClSystemParamService.getEnv(inData), "提示：获取数据成功！");
    }
}
