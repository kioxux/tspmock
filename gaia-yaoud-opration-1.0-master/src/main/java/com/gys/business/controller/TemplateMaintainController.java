package com.gys.business.controller;

import com.github.pagehelper.PageInfo;
import com.gys.business.service.TemplateMaintainService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.wechatTemplate.GaiaMouldMaintainDTO;
import com.gys.common.data.wechatTemplate.GaiaMouldMaintainVO;
import com.gys.common.data.wechatTemplate.MouldMaintainInData;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@Api(tags = "微信模板维护")
@RestController
@RequestMapping({"template-maintain"})
public class TemplateMaintainController extends BaseController {

    @Autowired
    private TemplateMaintainService templateMaintainService;

    @ApiOperation(value = "根据模板编码查询模板")
    @PostMapping({"queryTemplateByCode"})
    public JsonResult queryTemplateByCode(@RequestBody MouldMaintainInData mouldMaintainInData) {
        GaiaMouldMaintainVO gaiaMouldMaintainVO = templateMaintainService.queryTemplateByCode(mouldMaintainInData.getMouldCode());
        return JsonResult.success(gaiaMouldMaintainVO);
    }


    @ApiOperation(value = "查询微信模板列表", response = GaiaMouldMaintainVO.class)
    @PostMapping({"queryTemplateList"})
    public JsonResult queryTemplateList(HttpServletRequest request,
                                        @RequestBody MouldMaintainInData mouldMaintainInData) {
        Integer pageNum = ObjectUtils.isEmpty(mouldMaintainInData.getPageNum()) ? 1 : mouldMaintainInData.getPageNum();
        Integer pageSize = ObjectUtils.isEmpty(mouldMaintainInData.getPageSize()) ? 10 : mouldMaintainInData.getPageSize();
        PageInfo<GaiaMouldMaintainVO> gaiaMouldMaintainVOPageInfo = templateMaintainService.queryTemplateList(mouldMaintainInData.getMouldName(), mouldMaintainInData.getStatus(), pageNum, pageSize);
        return JsonResult.success(gaiaMouldMaintainVOPageInfo);
    }


    @ApiOperation(value = "新增微信模板")
    @PostMapping({"insertTemplate"})
    public JsonResult insertTemplate(HttpServletRequest request,
                                     @ApiParam(value = "模板内容") @RequestBody GaiaMouldMaintainVO gaiaMouldMaintainVO) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        templateMaintainService.insertTemplate(userInfo.getClient(), gaiaMouldMaintainVO);
        return JsonResult.success("新增模板成功");
    }


    @ApiOperation(value = "更新微信模板")
    @PostMapping({"updateTemplate"})
    public JsonResult updateTemplate(@ApiParam(value = "模板内容") @RequestBody GaiaMouldMaintainVO gaiaMouldMaintainVO) {
        templateMaintainService.updateTemplate(gaiaMouldMaintainVO);
        return JsonResult.success("更新模板成功");
    }


    @ApiOperation(value = "启用微信模板")
    @PostMapping({"startTemplate"})
    public JsonResult startTemplate(@ApiParam(value = "模板编码") @RequestBody GaiaMouldMaintainVO gaiaMouldMaintainVO) {
        String mouldCode = gaiaMouldMaintainVO.getMouldCode();
        templateMaintainService.startTemplate(mouldCode);
        return JsonResult.success("启用模板成功");
    }


    @ApiOperation(value = "停用微信模板")
    @PostMapping({"stopTemplate"})
    public JsonResult stopTemplate(@ApiParam(value = "模板编码") @RequestBody GaiaMouldMaintainVO gaiaMouldMaintainVO) {
        String mouldCode = gaiaMouldMaintainVO.getMouldCode();
        templateMaintainService.stopTemplate(mouldCode);
        return JsonResult.success("停用模板成功");
    }


    @ApiOperation(value = "删除微信模板")
    @PostMapping({"delTemplate"})
    public JsonResult delTemplate(@ApiParam(value = "模板编码") @RequestBody GaiaMouldMaintainVO gaiaMouldMaintainVO) {
        String mouldCode = gaiaMouldMaintainVO.getMouldCode();
        templateMaintainService.delTemplate(mouldCode);
        return JsonResult.success("删除模板成功");
    }
}
