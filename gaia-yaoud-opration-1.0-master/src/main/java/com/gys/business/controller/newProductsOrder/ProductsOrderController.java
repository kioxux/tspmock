package com.gys.business.controller.newProductsOrder;

import com.gys.business.service.data.ProductsOrderInData;
import com.gys.business.service.newProductsOrder.ProductsOrderService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.newProductsOrder.NewProductsOrderInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Api(tags = "新品订购查询", description = "/products-order")
@RestController
@RequestMapping({"/products-order"})
public class ProductsOrderController extends BaseController {

    @Autowired
    ProductsOrderService productsOrderService;

    @ApiOperation(value = "获取订单列表", response = NewProductsOrderInfoDTO.class)
    @GetMapping({"getOrderList"})
    public JsonResult getOrderList(HttpServletRequest request,
                                   @ApiParam(value = " 门店列表", required = false) @RequestParam(required = false) List<String> gsphBrIdList,
                                   @ApiParam(value = " 起始时间", required = false) @RequestParam(required = false) String startDate,
                                   @ApiParam(value = " 起始时间", required = false) @RequestParam(required = false) String endDate,
                                   @ApiParam("页码") @RequestParam(required = false) Integer pageNum,
                                   @ApiParam("页大小") @RequestParam(required = false) Integer pageSize
    ) {
        pageNum = (ObjectUtils.isEmpty(pageNum) || pageNum == 0) ? 1 : pageNum;
        pageSize = (ObjectUtils.isEmpty(pageSize) || pageSize == 0) ? 10 : pageSize;
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.productsOrderService.getOrderList(userInfo, gsphBrIdList, startDate, endDate, pageNum, pageSize), "提示：获取数据成功！");
    }
    /**
     *  新品订单查询-导出
     */

    @PostMapping({"getOrderList/export"})
    public void getOrderListExport(HttpServletRequest request, HttpServletResponse response,@RequestBody ProductsOrderInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.productsOrderService.getOrderListExport(response,userInfo,inData);
    }
}
