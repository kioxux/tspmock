package com.gys.business.controller.takeAway;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.base.BaseController;
import com.gys.common.data.*;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.UuidUtil;
import com.gys.util.takeAway.jgg.JggReturnOrder;
import com.gys.util.takeAway.jgg.JggReturnOrderItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 九宫格
 *
 * @author ZhangDong
 * @date 2021/10/26 13:26
 */
@Slf4j
@RequestMapping("jgg")
@RestController
public class ApiJggPushController extends BaseController {
    @Resource
    private BaseService baseService;
    @Resource
    private TakeAwayService takeAwayService;

    /**
     * 新订单插入，并push到FX
     */
    @PostMapping("createOrder")
    public JsonResult createOrder(@RequestBody BaseOrderInfo baseOrderInfo) {
        log.info("九宫格订单，请求参数：{}", JSON.toJSONString(baseOrderInfo));
        //填充订单ID
        fillOrderInfo(baseOrderInfo);
        String result = baseService.insertOrderInfoWithMap(baseOrderInfo);
        if (result.startsWith("ok")) {
            takeAwayService.execOrdersPush(getOrderInfoToPTSByMap(baseOrderInfo), Constants.PLATFORMS_JGG, null, null, null);
        } else {
            return JsonResult.error("订单插入失败");
        }
        return JsonResult.success(null, "新订单推送成功");
    }

    private void fillOrderInfo(BaseOrderInfo baseOrderInfo) {
        String orderId = UuidUtil.getUUID("jgg-");
        baseOrderInfo.setPlatforms(Constants.PLATFORMS_JGG);
        baseOrderInfo.setNotice_message("new");
        baseOrderInfo.setShop_no(baseOrderInfo.getClient() + baseOrderInfo.getStoCode());
        baseOrderInfo.setOrder_id(orderId);
        List<BaseOrderProInfo> items = baseOrderInfo.getItems();
        if (items != null && items.size() > 0) {
            for (BaseOrderProInfo item : items) {
                item.setOrder_id(orderId);
            }
        }
    }

    /**
     * 退单
     * 他们家，如果一个商品有活动，第二件半价，这个商品买了两个，那这个商品在商品明细里就一条数据数量是2
     */
    @PostMapping("returnOrder")
    public JsonResult returnOrder(@Valid @RequestBody JggReturnOrder jggReturnOrder) {
        String platformOrderId = jggReturnOrder.getPlatformOrderId();
        Map<String, Object> param = new HashMap<>();
        param.put("platforms_order_id", platformOrderId);
        param.put("platforms", Constants.PLATFORMS_JGG);
        PtsOrderInfo ptsOrderInfo = baseService.GetOrderInfo(param);
        if (!"ok".equals(ptsOrderInfo.getResult())) {
            log.error("九宫格，退单，根据platformOrderId:{} 查不到订单数据", platformOrderId);
            return JsonResult.error("并无此订单");
        }
        BaseOrderInfo orderInfo = getRefundBaseOrderInfo(ptsOrderInfo);
        String client = orderInfo.getClient();
        String stoCode = orderInfo.getStoCode();
        String orderId = orderInfo.getOrder_id();
        List<JggReturnOrderItem> returnItems = jggReturnOrder.getReturnItems();
        List<PtsOrderProInfo> originalItems = ptsOrderInfo.getItems();
        Map<String, PtsOrderProInfo> proIdMap = originalItems.stream().collect(Collectors.toMap(r -> r.getMedicine_code(), Function.identity(), (r, v) -> r));
        List<BaseOrderProInfo> items = new ArrayList<>(returnItems.size());
        Map<Integer, String> refundQtyMap = new HashMap<>(4);
        for (JggReturnOrderItem returnItem : returnItems) {
            String proId = returnItem.getProId();
            BaseOrderProInfo proInfo = new BaseOrderProInfo();
            PtsOrderProInfo ptsOrderProInfo = proIdMap.get(proId);
            if (ptsOrderProInfo == null) {
                log.error("九宫格，退单，不存在此退单商品, proId:{}, platformOrderId:{}", proId, platformOrderId);
                return JsonResult.error("并无此退单商品:" + proId);
            }
            proInfo.setOrder_id(ptsOrderInfo.getOrder_id());
            proInfo.setMedicine_code(proId);
            proInfo.setRefund_qty(returnItem.getReturnQty());
            proInfo.setSequence(ptsOrderProInfo.getSequence());
            proInfo.setClient(orderInfo.getClient());
            proInfo.setStoCode(orderInfo.getStoCode());
            items.add(proInfo);
            refundQtyMap.put(Integer.valueOf(ptsOrderProInfo.getSequence()), returnItem.getReturnQty());
        }
        orderInfo.setItems(items);
        String resultForXB = baseService.updateOrderInfoWithEntity(orderInfo);
        if (resultForXB.equals("ok")) {
            Map<String, Object> inData = getOrderInfoToPTSByMapForRefund(orderInfo);
            String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
            inData.put("storeLogo", storeLogo);
            inData.put("refundQtyMap", refundQtyMap);
            takeAwayService.pushToStoreMq(client, stoCode, "O2O", "tuikuan", inData);
            log.info("九宫格退单推送，orderId:{},client:{},stoCode:{},storeLogo:{},refundQtyMap:{}", orderId, client, stoCode, storeLogo, JSON.toJSONString(refundQtyMap));
            return JsonResult.success(null, "退单成功");
        } else {
            return JsonResult.error("更新失败:" + resultForXB);
        }
    }

    private BaseOrderInfo getRefundBaseOrderInfo(PtsOrderInfo ptsOrderInfo) {
        BaseOrderInfo baseOrderInfoForPartRefund = new BaseOrderInfo();
        baseOrderInfoForPartRefund.setPlatforms(ptsOrderInfo.getPlatforms());
        baseOrderInfoForPartRefund.setOrder_id(ptsOrderInfo.getOrder_id());
        baseOrderInfoForPartRefund.setPlatforms_order_id(ptsOrderInfo.getPlatforms_order_id());
        baseOrderInfoForPartRefund.setShop_no(ptsOrderInfo.getShop_no());
        baseOrderInfoForPartRefund.setRecipient_address(ptsOrderInfo.getRecipient_address());
        baseOrderInfoForPartRefund.setRecipient_name(ptsOrderInfo.getRecipient_name());
        baseOrderInfoForPartRefund.setRecipient_phone(ptsOrderInfo.getRecipient_phone());
        baseOrderInfoForPartRefund.setShipping_fee(ptsOrderInfo.getShipping_fee());
        baseOrderInfoForPartRefund.setTotal_price(ptsOrderInfo.getTotal_price());
        baseOrderInfoForPartRefund.setOriginal_price(ptsOrderInfo.getOriginal_price());
        baseOrderInfoForPartRefund.setCustomer_pay(ptsOrderInfo.getCustomer_pay());
        baseOrderInfoForPartRefund.setCaution(ptsOrderInfo.getCaution());
        baseOrderInfoForPartRefund.setShipper_name(ptsOrderInfo.getShipper_name());
        baseOrderInfoForPartRefund.setShipper_phone(ptsOrderInfo.getShipper_phone());
        baseOrderInfoForPartRefund.setStatus(ptsOrderInfo.getStatus());
        baseOrderInfoForPartRefund.setAction("5");
        baseOrderInfoForPartRefund.setIs_invoiced(ptsOrderInfo.getIs_invoiced());
        baseOrderInfoForPartRefund.setInvoice_title(ptsOrderInfo.getInvoice_title());
        baseOrderInfoForPartRefund.setTaxpayer_id(ptsOrderInfo.getTaxpayer_id());
        baseOrderInfoForPartRefund.setDelivery_time(ptsOrderInfo.getDelivery_time());
        baseOrderInfoForPartRefund.setPay_type(ptsOrderInfo.getPay_type());
        baseOrderInfoForPartRefund.setOrder_info(ptsOrderInfo.getOrder_info());
        baseOrderInfoForPartRefund.setCreate_time(ptsOrderInfo.getCreate_time());
        baseOrderInfoForPartRefund.setUpdate_time(ptsOrderInfo.getUpdate_time());
        baseOrderInfoForPartRefund.setDay_seq(ptsOrderInfo.getDay_seq());
        baseOrderInfoForPartRefund.setNotice_message("refund");
        baseOrderInfoForPartRefund.setClient(ptsOrderInfo.getClient());
        baseOrderInfoForPartRefund.setStoCode(ptsOrderInfo.getSto_code());
        return baseOrderInfoForPartRefund;
    }

}
