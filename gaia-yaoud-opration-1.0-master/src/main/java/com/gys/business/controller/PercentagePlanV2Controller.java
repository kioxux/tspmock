package com.gys.business.controller;


import cn.hutool.core.util.ObjectUtil;
import com.gys.business.service.PercentagePlanV2Service;
import com.gys.business.service.data.percentageplan.PercentageInData;
import com.gys.business.service.data.percentageplan.PercentageInTrialData;
import com.gys.business.service.data.percentageplan.PercentageOutData;
import com.gys.business.service.data.percentageplan.PercentageProInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping({"/percentage/plan/v2/"})
@Api(tags = "提成方案V2相关")
@Slf4j
public class PercentagePlanV2Controller extends BaseController {
    @Autowired
    private PercentagePlanV2Service service;

    @ApiOperation(value = "新增/修改提成方案,(试算-deleteFlag = 2 必填)")
    @PostMapping({"insertNew"})
    public JsonResult insert(HttpServletRequest request, @RequestBody PercentageInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setPlanCreaterId(userInfo.getUserId());
        inData.setPlanCreater(userInfo.getLoginName());
        return JsonResult.success(service.insert(inData), "提示：操作数据成功！");
    }


    @ApiOperation(value = "新增/修改试算)")
    @PostMapping({"insertTrialNew"})
    public JsonResult insertTrialNew(HttpServletRequest request, @RequestBody PercentageInTrialData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setPlanCreaterId(userInfo.getUserId());
        inData.setPlanCreater(userInfo.getLoginName());
        return JsonResult.success(service.insertTrialNew(inData), "提示：操作数据成功！");
    }

    @ApiOperation(value = "提成方案列表查询",response = PercentageOutData.class)
    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody PercentageInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(service.list(inData), "提示：操作数据成功！");
    }

    @ApiOperation(value = "提成方案详情查询，入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成)",response = PercentageInData.class)
    @PostMapping({"tichengDetail"})
    public JsonResult tichengDetail(@RequestBody Map<String,Long> inData) {
        return JsonResult.success(service.tichengDetail(inData.get("planId"),inData.get("planType")), "提示：操作数据成功！");
    }

//    @ApiOperation(value = "删除销售提成明细，入参：saleId(销售提成明细主键ID),planType(提成方案类型 1 销售提成 2 单品提成)")
//    @PostMapping({"deleteSalePlan"})
//    public JsonResult deleteSalePlan(@RequestBody Map<String,Long> inData ) {
////        service.deleteSalePlan(inData.get("saleId"),inData.get("saleId"));
//        return JsonResult.success("", "提示：操作数据成功！");
//    }
//
//    @ApiOperation(value = "删除单品提成明细，入参：proPlanId(单品提成明细主键ID)")
//    @PostMapping({"deleteProPlan"})
//    public JsonResult deleteProPlan(@RequestBody Map<String,Long> inData) {
////        service.deleteProPlan(inData.get("proPlanId"));
//        return JsonResult.success("", "提示：操作数据成功！");
//    }


    @ApiOperation(value = "方案审核,入参：planId(提成方案主键ID),planStatus(审核状态 0 已保存 1 已审核 2 已停用),planType(提成方案类型 1 销售提成 2 单品提成)")
    @PostMapping({"approve"})
    public JsonResult approve(@RequestBody Map<String,String> inData) {
        service.approve(Long.valueOf(inData.get("planId")),inData.get("planStatus"),inData.get("planType"));
        return JsonResult.success("", "提示：审核成功！");
    }

    @ApiOperation(value = "删除方案,入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成)")
    @PostMapping({"deletePlan"})
    public JsonResult deletePlan(@RequestBody Map<String,String> inData) {
        service.deletePlan(Long.valueOf(inData.get("planId")),inData.get("planType"));
        return JsonResult.success("", "提示：删除成功！");
    }

    @ApiOperation(value = "商品查询,入参：proCode(商品编码)")
    @PostMapping({"selectProductByClient"})
    public JsonResult selectProductByClient(HttpServletRequest request, @RequestBody Map<String,String> inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return JsonResult.success(service.selectProductByClient(userInfo.getClient(),inData.get("proCode")), "提示：查询成功！");
//        return JsonResult.success("", "提示：查询成功！");
    }

    @ApiOperation(value = "导入信息查询",response = PercentageProInData.class)
    @PostMapping({"getImportExcelDetailList"})
    public JsonResult getImportExcelDetailList(HttpServletRequest request,@RequestParam("file") MultipartFile file) {
        List<Map<String,String>> importInDataList = new ArrayList<>();
        try {
//            String rootPath = request.getSession().getServletContext().getRealPath("/");
//            String fileName = file.getOriginalFilename();
//            String path = rootPath + fileName;
//            File newFile = new File(path);
//            //判断文件是否存在
//            if(!newFile.exists()) {
//                newFile.mkdirs();//不存在的话，就开辟一个空间
//            }
//            //将上传的文件存储
//            file.transferTo(newFile);
            InputStream in = file.getInputStream();
            //导入已存在的Excel文件，获得只读的工作薄对象
            XSSFWorkbook wk = new XSSFWorkbook(in);
            //获取第一张Sheet表
            Sheet sheet = wk.getSheetAt(0);
            //取数据
            importInDataList = getRowAndCell(sheet);
//            inData.setImportInDataList(importInDataList);
            in.close();
            wk.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.service.getImportExcelDetailList(userInfo.getClient(),importInDataList), "提示：获取成功！");

//        return JsonResult.success("", "提示：获取成功！");
    }

    public List<Map<String,String>> getRowAndCell(Sheet sheet) {
        List<Map<String,String>> importInDataList = new ArrayList<>();
        String msg = null;
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        System.out.println("总行数==" + totalRows);
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            for (int i = 1; i <= totalRows; i++) {
                Row row = sheet.getRow(i); //取出一行数据放入row
                Map<String,String> newExpert = new HashMap<>();
                newExpert.put("proCode",dataFormatter.formatCellValue(row.getCell(0)).trim());
                String tichengAmt = dataFormatter.formatCellValue(row.getCell(1)).trim();
                if (ObjectUtil.isEmpty(tichengAmt)){
                    tichengAmt = "0";
                }
                newExpert.put("tichengAmt",tichengAmt);
                importInDataList.add(newExpert);
            } //行end
        }else{
            throw new BusinessException("EXCEL导入信息为空");
        }
        return importInDataList;
    }

    @ApiOperation(value = "提成方案复制,入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成)",response = PercentageInData.class)
    @PostMapping({"tichengDetailCopy"})
    public JsonResult tichengDetailCopy(@RequestBody Map<String,Long> inData) {
        return JsonResult.success(service.tichengDetailCopy(inData.get("planId"),inData.get("planType")), "提示：操作数据成功！");
//        return JsonResult.success("", "提示：操作数据成功！");
    }

    @ApiOperation(value = "提成方案门店查询,入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成) 出参：stoCode(门店编码),stoName(门店名称)")
    @PostMapping({"selectStoList"})
    public JsonResult selectStoList(@RequestBody Map<String,Long> inData) {
        return JsonResult.success(service.selectStoList(inData.get("planId"),inData.get("planType")), "提示：操作数据成功！");
//        return JsonResult.success("", "提示：操作数据成功！");
    }

    @ApiOperation(value = "方案停用,入参：planId(提成方案主键ID),planType(提成方案类型 1 销售提成 2 单品提成),stopType(停用状态 1 立即停用 2 限时停用),stopReason(停用原因)")
    @PostMapping({"stopPlan"})
    public JsonResult stopPlan(@RequestBody Map<String,String> inData) {
        service.stopPlan(Long.valueOf(inData.get("planId")),inData.get("planType"),inData.get("stopType"),inData.get("stopReason"));
        return JsonResult.success("", "提示：审核成功！");
    }

    @ApiOperation(value = "定时停用方案")
    @PostMapping({"timerStopPlan"})
    public JsonResult timerStopPlan(@RequestBody Map<String,String> inData) {
        service.timerStopPlan();
        return JsonResult.success("", "提示：审核成功！");
    }
}
