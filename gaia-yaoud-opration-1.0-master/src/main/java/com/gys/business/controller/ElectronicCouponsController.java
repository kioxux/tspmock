package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import com.gys.business.mapper.entity.GaiaSdStoreData;
import com.gys.business.service.ElectronicCouponsService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 电子券
 *
 * @author xiaoyuan on 2020/9/24
 */
@Api(tags = "电子券信息")
@Slf4j
@RestController
@RequestMapping({"/electronic/"})
public class ElectronicCouponsController extends BaseController {

    @Autowired
    private ElectronicCouponsService couponsService;


    @ApiOperation(value = "列表查询", response = ElectronBasicOutData.class)
    @PostMapping({"list"})
    public JsonResult getSalesInquireList(HttpServletRequest request, @Valid @RequestBody ElectronBasicInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.couponsService.getElectronThemeSetByList(inData), "success");
    }

    @ApiOperation(value = "新增电子券")
    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request, @Valid @RequestBody ElectronThemeSetInVo inVo) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inVo.setClient(userInfo.getClient());
        this.couponsService.insertToElectronThemeSet(inVo);
        return JsonResult.success("", "success");
    }

    @ApiOperation(value = "修改电子券")
    @PostMapping({"update"})
    public JsonResult update(HttpServletRequest request, @Valid @RequestBody ElectronThemeSetInVo inVo) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inVo.setClient(userInfo.getClient());
        this.couponsService.updateToElectronThemeSet(inVo);
        return JsonResult.success("", "success");
    }

    @ApiOperation(value = "获得电子券单号")
    @PostMapping({"getGsebId"})
    public JsonResult getGsebId(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.couponsService.getGsebIdToElectronThemeSet(userInfo.getClient()), "success");
    }


    @ApiOperation(value = "品牌分类/商品分类", response = Classification.class)
    @PostMapping({"classification"})
    public JsonResult classification(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.couponsService.getclassification(userInfo.getClient()), "success");
    }

    @ApiOperation(value = "商品组模糊查询", response = FuzzyQueryOfProductGroupOutData.class)
    @PostMapping({"fuzzyQueryOfProductGroup"})
    public JsonResult fuzzyQueryOfProductGroup(HttpServletRequest request, @Valid @RequestBody FuzzyQueryOfProductGroupInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.couponsService.fuzzyQueryOfProductGroup(inData), "success");
    }

    @ApiOperation(value = "查询当前会员在当前门店可用电子券")
    @PostMapping({"queryCanUseElectron"})
    public JsonResult queryCanUseElectron(HttpServletRequest request, @Valid @RequestBody GaiaSdMemberCardData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setGsmbcBrId(userInfo.getDepId());
        return JsonResult.success(this.couponsService.queryCanUseElectron(inData), "success");
    }

    @ApiOperation(value = "查询当前门店可赠送电子券")
    @PostMapping({"selectGiftCoupons"})
    public JsonResult selectGiftCoupons(HttpServletRequest request, @Valid @RequestBody GaiaSdStoreData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsstBrId(userInfo.getDepId());
        return JsonResult.success(this.couponsService.selectGiftCoupons(inData), "success");
    }

    @ApiOperation(value = "商品组导入商品id", response = FuzzyQueryOfProductGroupOutData.class)
    @PostMapping({"importExcel"})
    public JsonResult importExcel(HttpServletRequest request, @Valid @RequestBody FuzzyQueryOfProductGroupInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.couponsService.importExcel(inData.getProCode(), userInfo.getClient(), userInfo.getDepId()), "success");
    }

    @ApiOperation(value = "电子券保存基本信息")
    @PostMapping({"insertAll"})
    public JsonResult insertAll(HttpServletRequest request, @Valid @RequestBody ElectronicAllData inVo) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inVo.setClient(userInfo.getClient());
        inVo.setBrId(userInfo.getDepId());
        inVo.setUserId(userInfo.getUserId());
        return JsonResult.success(this.couponsService.insertAll(inVo),"success");
    }

    @ApiOperation(value = "添加之前时间判断")
    @PostMapping({"timeJudgment"})
    public Map<String, Object> timeJudgment(HttpServletRequest request, @Valid @RequestBody ElectronicAllData inVo) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inVo.setClient(userInfo.getClient());
        inVo.setBrId(userInfo.getDepId());
        inVo.setUserId(userInfo.getUserId());
        Map<String, Object> map = this.couponsService.timeJudgment(inVo);
        return map;
    }


    @ApiOperation(value = "电子券审核")
    @PostMapping({"review"})
    public JsonResult review(HttpServletRequest request, @Valid @RequestBody ElectronicReviewInData inVo) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inVo.setClient(userInfo.getClient());
        this.couponsService.review(inVo);
        return JsonResult.success("", "success");
    }

    @ApiOperation(value = "电子券报表", response = ElectronicSubjectQueryOutData.class)
    @PostMapping({"subjectQuery"})
    public JsonResult subjectQuery(HttpServletRequest request, @Valid @RequestBody ElectronicSubjectQueryInData inVo) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inVo.setClient(userInfo.getClient());
        List<ElectronicSubjectQueryOutData> outData = this.couponsService.subjectQuery(inVo);
        return JsonResult.success(outData, "success");
    }

    @ApiOperation(value = "电子券报表查看", response = ElectronicDetailedViewOutData.class)
    @PostMapping({"detailedView"})
    public JsonResult detailedView(HttpServletRequest request, @Valid @RequestBody ElectronicDetailedViewInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        ElectronicDetailedViewOutData outData = this.couponsService.detailedView(inData);
        return JsonResult.success(outData, "success");
    }

    @ApiOperation(value = "查询未被使用过的电子券", response = ElectronBasicOutData.class)
    @PostMapping({"listUnusedElectronBasic"})
    public JsonResult listUnusedElectronBasic(HttpServletRequest request, @Valid @RequestBody ElectronBasicInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.couponsService.listUnusedElectronBasic(inData), "success");
    }

    @ApiOperation(value = "必备商品铺货范围-获取自定义参数")
    @PostMapping({"getZdydatas"})
    public JsonResult getZdydatas(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.couponsService.QueryOfProductGroupZdys(userInfo), "success");
    }

    @ApiOperation(value = "电子券主题再设置")
    @PostMapping({"queryStatus"})
    public JsonResult queryStatus(HttpServletRequest request,@Valid @RequestBody ElectronicStatusInData inData){
        GetLoginOutData userinfo = this.getLoginUser(request);
        inData.setClient(userinfo.getClient());
        this.couponsService.queryStatus(inData);
        return JsonResult.success("success");
    }
}
