package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.service.ReplenishWebService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.replenishParam.ReplenishParamInData;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Api(tags = "补货web相关接口")
@RestController
@RequestMapping({"/replenish/web/"})
@Slf4j
public class ReplenishWebController extends BaseController {
    @Resource
    private ReplenishWebService replenishWebService;
    @Autowired
    private HttpServletRequest request;

    public ReplenishWebController() {
    }

    @ApiOperation(value = "补货查询列表接口", notes = "补货查询列表接口", response = GetReplenishOutData.class)
    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.selectList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "新增补货载入商品列表", notes = "新增补货载入商品列表", response = GetReplenishDetailOutData.class)
    @PostMapping({"detailList"})
    public JsonResult detailList(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.detailList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "修改补货单载入补货商品列表", notes = "修改补货单载入补货商品列表", response = GetReplenishOutData.class)
    @PostMapping({"getDetail"})
    public JsonResult getDetail(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.getDetail(inData), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "补货单新增接口", notes = "补货单新增接口")
    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        Map<String,Object> map = this.replenishWebService.insert(inData,userInfo);
        String dec = map.get("msg") != null ? (String)map.get("msg")+"提示：保存成功！":"提示：保存成功！";
        return JsonResult.success(map.get("gsrhVoucherId"), dec);
    }

    @ApiOperation(value = "补货单号生成", notes = "补货单号生成")
    @PostMapping({"selectNextVoucherId"})
    public JsonResult selectNextVoucherId(HttpServletRequest request) {
        GaiaSdReplenishH inData = new GaiaSdReplenishH();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsrhBrId(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.selectNextVoucherId(inData), "提示：返回数据！");
    }

    @FrequencyValid
    @ApiOperation(value = "补货审核", notes = "补货审核")
    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        this.replenishWebService.approve(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }


    @ApiOperation(value = "补货单作废", notes = "补货单作废")
    @PostMapping({"cancel"})
    public JsonResult cancel(@RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return replenishWebService.cancelReplenish(inData);
    }

    @ApiOperation(value = "库存数量查询", notes = "库存数量查询", response = GetReplenishOutData.class)
    @PostMapping({"getStock"})
    public JsonResult getStock(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.getStock(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "管制药品限制数量查询", notes = "管制药品限制数量查询", response = GetReplenishOutData.class)
    @PostMapping({"getParam"})
    public JsonResult getParam(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.getParam(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "获取90天 30天 7天销售总数量", notes = "获取90天 30天 7天销售总数量", response = GetReplenishOutData.class)
    @PostMapping({"getSales"})
    public JsonResult getSales(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.getSales(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "批次库存查询", notes = "批次库存查询", response = GetReplenishOutData.class)
    @PostMapping({"getCost"})
    public JsonResult getCost(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.getCost(inData), "提示：获取数据成功！");
    }

//    @ApiOperation(value = "获取采购总数量", notes = "获取采购总数量", response = GetReplenishOutData.class)
//    @PostMapping({"getCount"})
//    public JsonResult getCount(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClientId(userInfo.getClient());
//        inData.setStoCode(userInfo.getDepId());
//        return JsonResult.success(this.replenishWebService.getCount(inData), "提示：获取数据成功！");
//    }

    @ApiOperation(value = "判断是否正常补货", notes = "判断是否正常补货")
    @PostMapping({"isRepeat"})
    public JsonResult isRepeat(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.isRepeat(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "获取采购总价", notes = "获取采购总价", response = GetReplenishOutData.class)
    @PostMapping({"getVoucherAmt"})
    public JsonResult getVoucherAmt(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.getVoucherAmt(inData), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "清空补货单商品", notes = "清空补货单商品")
    @PostMapping({"deleteDetail"})
    public JsonResult deleteDetail(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.replenishWebService.deleteDetail(inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询新增单个补货商品信息", notes = "查询新增单个补货商品信息", response = GetReplenishDetailOutData.class)
    @PostMapping({"replenishDetail"})
    public JsonResult replenishDetail(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.replenishDetail(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询新增多个补货商品信息", notes = "查询新增多个补货商品信息", response = GetReplenishDetailOutData.class)
    @PostMapping({"replenishDetailList"})
    public JsonResult replenishDetailList(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.replenishDetailList(inData), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "录入比价信息到比价表中")
    @PostMapping({"addRepPrice"})
    public JsonResult addRepPrice(HttpServletRequest request, @RequestBody RatioProInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        return JsonResult.success(this.replenishWebService.addRepPrice(inData), "提示：保存成功！");
    }

    @ApiOperation(value = "比价状态查询")
    @PostMapping({"selectRatioStatus"})
    public JsonResult selectRatioStatus(HttpServletRequest request, @RequestBody RatioProInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.selectRatioStatus(inData), "提示：查询成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "导出返回数据")
    @PostMapping({"sortImportData"})
    public JsonResult sortImportData(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.replenishWebService.sortImportData(inData), "提示：查询成功！");
    }


    //V1根据已有的补货单对商品比价
    @ApiOperation(value = "新增自动补货载入商品列表", notes = "新增自动补货载入商品列表", response = GetReplenishDetailOutData.class)
    @PostMapping({"detaiAutolList"})
    public JsonResult detaiAutolList(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.detaiAutolList(inData), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "比价保存", response = GetReplenishInData.class)
    @PostMapping({"saveAutoPrice"})
    public JsonResult saveAutoPrice(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
//        inData.setClientId("21020006");
        this.replenishWebService.saveAutoPrice(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    //V2根据单体门店新增商品比价
    @ApiOperation(value = "新增自动补货载入商品列表", notes = "新增自动补货载入商品列表", response = GetReplenishDetailOutData.class)
    @PostMapping({"detailAutoProList"})
    public JsonResult detailAutoProList(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.detailAutoProList(inData), "提示：获取数据成功！");
    }

    //获取门店是否可比价
    @ApiOperation(value = "获取门店是否可比价 isPriceRatio 0 不可用 1 可用", notes = "获取门店是否可比价 isPriceRatio 0 不可用 1 可用")
    @PostMapping({"isPriceRatio"})
    public JsonResult isPriceRatio(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.replenishWebService.isPriceRatio(userInfo), "提示：获取数据成功！");
    }

    //根据dcCode获取门店属性
    @ApiOperation(value = "根据dcCode获取门店属性 stoAttribute 0 单体 1 连锁", notes = "根据dcCode获取门店属性 stoAttribute 0 单体 1 连锁")
    @PostMapping({"stoAttribute"})
    public JsonResult stoAttribute(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.replenishWebService.stoAttribute(userInfo), "提示：获取数据成功！");
    }

    /**
     * 查询齐纳判断
     * @param request
     * @return
     */
    @ApiOperation(value = "判断是否显示成本价 false = 隐藏 / true = 显示", notes = "判断是否显示成本价  false = 隐藏 / true = 显示")
    @PostMapping({"judgment"})
    public JsonResult judgment(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.replenishWebService.judgment(userInfo), "提示：获取数据成功！");
    }

    //V2根据单体门店新增商品比价
    @ApiOperation(value = "查询已比价列表", notes = "查询已比价列表", response = GetReplenishDetailOutData.class)
    @PostMapping({"detailRatioedList"})
    public JsonResult detailRatioedList(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.detailRatioedList(inData), "提示：获取数据成功！");
    }

    /**
     * 正常补货检测
     * @param request
     * @return
     */
    @ApiOperation(value = "正常补货检测今天是否有补货单")
    @PostMapping({"hasReplenished"})
    public JsonResult hasReplenished(HttpServletRequest request,@RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.hasReplenished(inData), "提示：获取数据成功！");
    }

    //获取门店是否可比价
    @ApiOperation(value = "isShow:获取自动补货显示仓库库存 0 不显示 1 显示 isWtpsParam:是否开启委托配送相应功能 0：关 1：开", notes = "isShow:获取自动补货显示仓库库存 0 不显示 1 显示 isWtpsParam:是否开启委托配送相应功能 0：关 1：开")
    @PostMapping({"showWareHouseStock"})
    public JsonResult showWareHouseStock(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.replenishWebService.showWareHouseStock(userInfo), "提示：获取数据成功！");
    }

    @ApiOperation(value = "补货单暂存至redis", notes = "补货单暂存至redis")
    @PostMapping({"insertToRedis"})
    public JsonResult insertToRedis(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        this.replenishWebService.insertToRedis(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "获取已暂存的补货单", notes = "获取已暂存的补货单",response = GetReplenishInData.class)
    @PostMapping({"selectFromRedis"})
    public JsonResult selectFromRedis(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.selectFromRedis(inData), "提示：保存成功！");
    }

    @ApiOperation(value = "门店补货对比差异表", notes = "门店补货对比差异表")
    @PostMapping({"listDifferentReplenish"})
    public JsonResult listDifferentReplenish(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.replenishWebService.listDifferentReplenish(inData,userInfo), "提示：获取数据成功！");
    }

    @ApiOperation(value = "门店补货对比差异明细表", notes = "门店补货对比差异明细表")
    @PostMapping({"listDifferentReplenishDetail"})
    public JsonResult listDifferentReplenishDetail(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.replenishWebService.listDifferentReplenishDetail(inData,userInfo), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "导出门店补货对比差异表")
    @PostMapping("/export")
    public Result export(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return this.replenishWebService.exportData(inData,userInfo);
    }


    @ApiOperation(value = "原始补货单明细表", notes = "原始补货单明细表")
    @PostMapping({"listOriReplenishDetail"})
    public JsonResult listOriReplenishDetail(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.replenishWebService.listOriReplenishDetail(inData,userInfo), "提示：获取数据成功！");
    }

    @ApiOperation(value = "门店级手机报表缺断货情况查询")
    @PostMapping("/getOutOfStockExplain")
    public JsonResult getOutOfStockExplain(@RequestBody OutOfStockInData inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return replenishWebService.getOutOfStockExplain(inData);
    }

    @FrequencyValid
    @ApiOperation(value = "新增门店补货计划")
    @PostMapping("/addStoreReplenishPlan")
    public JsonResult addStoreReplenishPlan(@RequestBody StoreReplenishPlanInData inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return replenishWebService.addStoreReplenishPlan(inData);
    }

    @ApiOperation(value = "缺断货补充历史数据查询")
    @PostMapping("/getOutOfStockHistory")
    public JsonResult getOutOfStockHistory(@RequestBody OutOfStockInData inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return replenishWebService.getOutOfStockHistory(inData);
    }

    @FrequencyValid
    @ApiOperation(value = "淘汰缺断货商品(支持批量)")
    @PostMapping("/invalidOutOfStock")
    public JsonResult invalidOutOfStock(@RequestBody InvalidStockInData inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return replenishWebService.invalidOutOfStock(inData);
    }

    @ApiOperation(value = "门店级库存报表")
    @PostMapping("/storeStockReport")
    public JsonResult storeStockReport(@RequestBody StoreStockReportInData inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return replenishWebService.getStoreStockReport(inData);
    }

    @ApiOperation(value = "门店效期商品报表")
    @PostMapping("/storeValidProduct")
    public JsonResult storeValidProduct(@RequestBody ValidProductInData inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return replenishWebService.getValidProductInfo(inData);
    }

    @ApiOperation(value = "国药冷链-查询新增单个补货商品信息", notes = "国药冷链-查询新增单个补货商品信息", response = GetReplenishDetailOutData.class)
    @PostMapping({"replenishDetailForColdChain"})
    public JsonResult replenishDetailForColdChain(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.replenishDetailForColdChain(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "国药冷链-保存补货单", notes = "国药冷链-保存补货单", response = GetReplenishDetailOutData.class)
    @PostMapping({"insertReplenishDetailForColdChain"})
    public JsonResult insertReplenishDetailForColdChain(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.insertReplenishDetailForColdChain(inData), "提示：获取数据成功！");
    }


    @ApiOperation(value = "查询门店补货分单设置列表")
    @PostMapping({"selectReplenishParamList"})
    public JsonResult selectReplenishParamList(HttpServletRequest request,@RequestBody ReplenishParamInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
//        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.selectReplenishParamList(inData), "提示：获取数据成功！");
    }


    @ApiOperation(value = "门店补货分单设置批量修改")
    @PostMapping({"batchAddReplenishParam"})
    public JsonResult batchAddReplenishParam(HttpServletRequest request,@RequestBody ReplenishParamInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUpdateEmp(userInfo.getUserId());
        this.replenishWebService.batchAddReplenishParam(inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    @ApiOperation(value = "门店补货查询请货商品带赠品")
    @PostMapping({"selectGiftList"})
    public JsonResult selectGiftList(HttpServletRequest request,@RequestBody GetReplenishDetailInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsrdBrId(userInfo.getDepId());
        return JsonResult.success(this.replenishWebService.selectGiftList(inData), "提示：获取数据成功！");
    }
}
