package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaBillOfArCancel;
import com.gys.business.service.GaiaBillOfArCancelService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 应收核销清单表(GaiaBillOfArCancel)表控制层
 *
 * @author makejava
 * @since 2021-09-18 15:20:50
 */
@RestController
@RequestMapping("gaiaBillOfArCancel")
public class GaiaBillOfArCancelController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaBillOfArCancelService gaiaBillOfArCancelService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public GaiaBillOfArCancel selectOne(Long id) {
        return this.gaiaBillOfArCancelService.queryById(id);
    }

}
