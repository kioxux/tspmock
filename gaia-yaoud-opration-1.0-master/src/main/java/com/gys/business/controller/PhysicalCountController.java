package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.PhysicalCountService;
import com.gys.business.service.data.GetPhysicalCountHeadOutData;
import com.gys.business.service.data.GetPhysicalCountInData;
import com.gys.business.service.data.GetPhysicalCountOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping({"/physicalCount/"})
@Slf4j
@Api(tags = "APP盘点审核", value = "陈浩")
public class PhysicalCountController extends BaseController {
    @Resource
    private PhysicalCountService physicalCountService;

    @ApiOperation(value = "获取盘点单列表", response = GetPhysicalCountHeadOutData.class)
    @PostMapping({"/list"})
    public JsonResult list(HttpServletRequest request, @RequestBody @ApiParam GetPhysicalCountInData inData) {
        log.info("盘点单列表:{}", JSON.toJSONString(inData));
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGspcBrId(userInfo.getDepId());
        List<GetPhysicalCountHeadOutData> list = physicalCountService.listPhysical(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "调取盘点单", response = GetPhysicalCountOutData.class)
    @PostMapping({"/selectPhysical"})
    public JsonResult selectPhysical(HttpServletRequest request, @RequestBody @ApiParam(required = true) GetPhysicalCountInData inData) {
        log.info("调取盘点单:{}", JSON.toJSONString(inData));
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGspcBrId(userInfo.getDepId());
        List<GetPhysicalCountOutData> list = physicalCountService.selectPhysical(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "差异审核")
    @PostMapping({"/approvePhysicalDiff"})
    public JsonResult approvePhysicalDiff(HttpServletRequest request, @RequestBody @ApiParam GetPhysicalCountInData inData) {
        log.info("差异审核:{}", JSON.toJSONString(inData));
        GetLoginOutData userInfo = super.getLoginUser(request);
        physicalCountService.approvePhysicalDiff(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/savePhysical"})
    public JsonResult savePhysical(HttpServletRequest request, @RequestBody GetPhysicalCountInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGspcBrId(userInfo.getDepId());
        this.physicalCountService.savePhysical(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/savePhysicalDiff"})
    public JsonResult savePhysicalDiff(HttpServletRequest request, @RequestBody GetPhysicalCountInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.physicalCountService.savePhysicalDiff(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/selectPhysicalOne"})
    public JsonResult selectPhysicalOne(HttpServletRequest request, @RequestBody GetPhysicalCountInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGspcBrId(userInfo.getDepId());
        GetPhysicalCountOutData outData = this.physicalCountService.selectPhysicalOne(inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"/approvePhysical"})
    public JsonResult approvePhysical(HttpServletRequest request, @RequestBody GetPhysicalCountInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.physicalCountService.approvePhysical(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/selectPhysicalDiff"})
    public JsonResult selectPhysicalDiff(HttpServletRequest request, @RequestBody GetPhysicalCountInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGspcBrId(userInfo.getDepId());
        List<GetPhysicalCountOutData> list = this.physicalCountService.selectPhysicalDiff(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @PostMapping({"/updatePhysicalDiff"})
    public JsonResult updatePhysicalDiff(HttpServletRequest request, @RequestBody GetPhysicalCountInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.physicalCountService.updatePhysicalDiff(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }
}
