package com.gys.business.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.service.ReportFormsService;
import com.gys.business.service.data.GetPayInData;
import com.gys.business.service.data.GetPayTypeOutData;
import com.gys.business.service.data.ReportForms.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.FuzzyQueryForConditionInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

@Api(tags = "报表汇总", description = "/reportForms")
@RestController
@RequestMapping({"/reportForms/"})
@Slf4j
public class ReportFormsController extends BaseController {

    @Autowired
    private ReportFormsService reportFormsService;
    @Resource
    public CosUtils cosUtils;

    @ApiOperation(value = "供应商往来单据列表" , response = SupplierDealOutData.class)
    @GetMapping({"selectSupplierDealPage"})
    public JsonResult selectSupplierDealPage(HttpServletRequest request,SupplierDealInData data){
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectSupplierDealPage(data), "提示：获取数据成功！");
    }

//    @ApiOperation(value = "供应商往来单据列表导出" , response = SupplierDealOutData.class)
//    @GetMapping({"selectSupplierDealExcel"})
//    public void selectSupplierDealExecl(HttpServletRequest request, HttpServletResponse response, SupplierDealInData data){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        data.setClient(userInfo.getClient());
//        reportFormsService.selectSupplierDealExcel(response,data);
//    }

    @ApiOperation(value = "供应商单据明细查询" , response = SupplierDealOutData.class)
    @PostMapping({"selectSupplierDealDetailPage"})
    public JsonResult selectSupplierDealDetailPage(HttpServletRequest request,@RequestBody SupplierDealInData data){
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectSupplierDealDetailPage(data), "提示：获取数据成功！");
    }


    @ApiOperation(value = "供应商单据明细导出")
    @PostMapping({"exportSupplierDealDetailPage"})
    public Result exportSupplierDealDetailPage(HttpServletRequest request,@RequestBody SupplierDealInData data){
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        List<SupplierDealOutDataCSV> outData = reportFormsService.exportSupplierDealDetailPage(data);
        for (SupplierDealOutDataCSV outDataCSV: outData){
            if (StringUtils.isNotBlank(outDataCSV.getProCode())){
                outDataCSV.setProCode("\t"+outDataCSV.getProCode());
            }
        }
        String fileName = "供应商单据明细";
        if (outData.size() > 0) {
            CsvFileInfo csvInfo =null;
            // 导出
            // byte数据
            csvInfo = CsvClient.getCsvByte(outData,fileName, Collections.singletonList((short)1));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据,请修改查询条件!");
        }

    }


    @ApiOperation(value = "批发销售查询", response = WholesaleSaleOutData.class)
    @PostMapping({"selectWholesaleSalePage"})
    public JsonResult selectWholesaleSalePage(HttpServletRequest request,@RequestBody WholesaleSaleInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectWholesaleSalePage(data), "提示：获取数据成功！");
    }
    @ApiOperation(value = "批发销售查询导出", response = WholesaleSaleOutData.class)
    @PostMapping({"selectWholesaleSalePage/export"})
    public void selectWholesaleSalePageExport(HttpServletRequest request,HttpServletResponse response,@RequestBody WholesaleSaleInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        log.info("<用户：{}调用了批发销售查询导出接口>",userInfo.getUserId()+"-"+userInfo.getLoginName());
        reportFormsService.selectWholesaleSalePageExport(response,data);
    }

    @ApiOperation(value = "批发销售明细查询", response = WholesaleSaleOutData.class)
    @PostMapping({"selectWholesaleSaleDetailPage"})
    public JsonResult selectWholesaleSaleDetailPage(HttpServletRequest request,@RequestBody WholesaleSaleInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectWholesaleSaleDetailPage(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "批发销售明细查询导出", response = WholesaleSaleOutData.class)
    @PostMapping({"selectWholesaleSaleDetailPage/export"})
    public void selectWholesaleSaleDetailPageExport(HttpServletRequest request,HttpServletResponse response,@RequestBody WholesaleSaleInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        log.info("<用户：{}调用了批发销售明细查询导出接口>",userInfo.getUserId()+"-"+userInfo.getLoginName());
        reportFormsService.selectWholesaleSaleDetailPageExport(response,data);
    }

    @ApiOperation(value = "批发销售抬头备注下拉框模糊查询")
    @PostMapping({"selectSoHeadRemark"})
    public JsonResult selectSoHeadRemark(HttpServletRequest request, @RequestBody FuzzyQueryForConditionInData conditionQuery) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        conditionQuery.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectSoHeadRemark(conditionQuery), "提示：获取数据成功！");
    }

    @ApiOperation(value = "损益单据查询", response = BusinessDocumentOutData.class)
    @PostMapping({"selectBusinessDocumentPage"})
    public JsonResult selectBusinessDocumentPage(HttpServletRequest request,@RequestBody BusinessDocumentInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectBusinessDocumentPage(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "损益单据明细查询", response = BusinessDocumentOutData.class)
    @PostMapping({"selectBusinessDocumentDetailPage"})
    public JsonResult selectBusinessDocumentDetailPage(HttpServletRequest request,@RequestBody BusinessDocumentInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectBusinessDocumentDetailPage(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "门店分税率销售明细查询", response = StoreRateSellOutData.class)
    @GetMapping({"selectStoreRateSellDetailPage"})
    public JsonResult selectStoreRateSellDetailPage(HttpServletRequest request, StoreRateSellInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectStoreRateSellDetailPage(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "门店分税率销售明细汇总查询", response = StoreRateSellOutData.class)
    @GetMapping({"selectStoreRateSellPage"})
    public JsonResult selectStoreRateSellPage(HttpServletRequest request, StoreRateSellInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectStoreRateSellPage(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "门店日期销售明细查询(门店用)")
    @GetMapping({"selectStoreSaleByDay"})
    public JsonResult selectStoreSaleByDay(HttpServletRequest request, StoreSaleDayInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectStoreSaleByDay(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "门店日期销售明细查询(加盟商用)")
    @PostMapping({"selectStoreSaleByDayAndClient"})
    public JsonResult selectStoreSaleByDayAndClient(HttpServletRequest request,@RequestBody StoreSaleDayInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectStoreSaleByDayAndClient(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "加盟商下进销存变动查询",response = InventoryChangeSummaryOutData.class)
    @PostMapping({"selectInventoryChangeSummary"})
    public JsonResult selectInventoryChangeSummary(HttpServletRequest request, @RequestBody InventoryChangeSummaryInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectInventoryChangeSummary(data), "提示：获取数据成功！");
    }
    @ApiOperation(value = "加盟商下进销存变动明细查询",response = InventoryChangeSummaryDetailOutData.class)
    @PostMapping({"selectInventoryChangeSummaryDetail"})
    public JsonResult selectInventoryChangeSummaryDetail(HttpServletRequest request,@RequestBody InventoryChangeSummaryInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectInventoryChangeSummaryDetail(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "进销存门店库存检查")
    @PostMapping({"/selectInventoryStockCheckListBySto"})
    public JsonResult selectInventoryStockCheckListBySto(HttpServletRequest request,@RequestBody InventoryChangeCheckInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectInventoryStockCheckListBySto(data),"提示：获取数据成功");
    }

    @ApiOperation(value = "进销存仓库库存检查")
    @PostMapping({"/selectInventoryStockCheckListByDc"})
    public JsonResult selectInventoryStockCheckListByDc(HttpServletRequest request,@RequestBody InventoryChangeCheckInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectInventoryStockCheckListByDc(data),"提示：获取数据成功");
    }

    @ApiOperation(value = "进销门店存库存检查导出")
    @PostMapping({"/selectInventoryStockCheckListBySto/export"})
    public void checkListStoExport(HttpServletRequest request, HttpServletResponse response, @RequestBody InventoryChangeCheckInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        reportFormsService.checkListStoExport(data,response);
    }

    @ApiOperation(value = "进销存仓库库存检查导出")
    @PostMapping({"/selectInventoryStockCheckListByDc/export"})
    public void checkListDcExport(HttpServletRequest request, HttpServletResponse response, @RequestBody InventoryChangeCheckInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        reportFormsService.checkListDcExport(data,response);
    }

    @ApiOperation(value = "日结对账明细查询")
    @PostMapping({"selectPayDayreport"})
    public JsonResult selectPayDayreport(HttpServletRequest request, @RequestBody PayDayreportInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectPayDayreport(data), "提示：获取数据成功！");
    }
    @ApiOperation(value = "供应商商品销售汇总(供应商用)",response =ProductSalesBySupplierOutData.class)
    @PostMapping({"selectProductSalesBySupplier"} )
    public JsonResult selectProductSalesBySupplier(HttpServletRequest request, @RequestBody ProductSalesBySupplierInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectProductSalesBySupplier(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "供应商商品销售汇总(门店用)",response =ProductSalesBySupplierOutData.class)
    @PostMapping({"selectProductSalesBySupplierByStore"} )
    public JsonResult selectProductSalesBySupplierByStore(HttpServletRequest request, @RequestBody ProductSalesBySupplierInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectProductSalesBySupplierByStore(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "门店销售分类汇总查询")
    @PostMapping({"selectStoreSaleByDate"})
    public JsonResult selectStoreSaleByDate(HttpServletRequest request,@RequestBody StoreSaleDateInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectStoreSaleByDate(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "商品销售汇总查询" ,response = StoreProductSaleClientOutData.class)
    @PostMapping({"selectProductSaleByClient"})
    public JsonResult selectProductSaleByClient(HttpServletRequest request, @RequestBody StoreProductSaleClientInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectProductSaleByClient(data), "提示：获取数据成功！");
    }
    @ApiOperation(value = "门店商品销售汇总查询",response =StoreProductSaleStoreOutData.class)
    @PostMapping({"selectProductSaleByStore"})
    public JsonResult selectProductSaleByStore(HttpServletRequest request,@RequestBody StoreProductSaleStoreInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectProductSaleByStore(data), "提示：获取数据成功！");
    }

    @ApiOperation(value = "配送往来数据查询")
    @PostMapping({"selectDistributionData"})
    public JsonResult selectDistributionData(HttpServletRequest request,@RequestBody StoreDistributaionDataInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        data.setSqlString("gmd.MAT_MOV_AMT+gmd.MAT_RATE_MOV");
        return reportFormsService.selectDistributionData(data);
    }

    @ApiOperation(value = "配送往来仓库查询")
    @PostMapping({"getDcCodeList"})
    public JsonResult getDcCodeList(HttpServletRequest request) {
        GetLoginOutData loginUser = this.getLoginUser(request);
        return reportFormsService.getDcCode(loginUser.getClient());
    }

    @ApiOperation(value = "配送往来客户列表查询")
    @PostMapping({"getCusList"})
    public JsonResult getCusList(HttpServletRequest request,@RequestBody Map<String,String> dcCode) {
        GetLoginOutData loginUser = this.getLoginUser(request);
        return reportFormsService.getCusList(loginUser.getClient(),dcCode);
    }

    @ApiOperation(value = "配送往来导出CSV")
    @PostMapping({"export"})
    public Result exportCSV(HttpServletRequest request,@RequestBody StoreDistributaionDataInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setSqlString("gmd.MAT_MOV_AMT+gmd.MAT_RATE_MOV");
        List<StoreDistributaionDataOutData> outData = reportFormsService.exportCSV(inData);
        String fileName = "配送往来数据导出";
        if (outData.size() > 0) {
            CsvFileInfo csvInfo =null;
            // 导出
            // byte数据
            for (StoreDistributaionDataOutData out : outData) {
                out.setCusCode("\t"+out.getCusCode());
            }
            csvInfo = CsvClient.getCsvByte(outData,fileName, Collections.singletonList((short)1));

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据,请修改查询条件!");
        }
    }

    /**
     * 供应商单据明细列表
     * @param request 权限校验
     * @param data 请求参数
     * @return
     */
    @ApiOperation("供应商单据明细列表")
    @PostMapping("selectSupplierDetailPageList")
    public JsonResult selectSupplierDetailPageList(HttpServletRequest request,@RequestBody SelectSupplierDetailPageListData data){
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        if(StrUtil.isNotBlank(data.getDepId())){
            data.setStoCode(data.getDepId());
        }
        return JsonResult.success(reportFormsService.selectSupplierDetailPageList(data), "提示：获取数据成功！");
    }

    /**
     * 供应商单据明细列表导出
     * @param response
     * @param servletRequest
     * @param data
     * @throws IOException
     */
    @ApiOperation("供应商单据明细列表导出")
    @PostMapping("dataExport")
    public void export(HttpServletResponse response, HttpServletRequest servletRequest, @RequestBody  SelectSupplierDetailPageListData data) throws IOException {
        GetLoginOutData userInfo = this.getLoginUser(servletRequest);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        data.setClient(userInfo.getClient());
        if(StrUtil.isNotBlank(data.getDepId())){
            data.setStoCode(data.getDepId());
        }
        SXSSFWorkbook workbook = reportFormsService.export(data);
        String filename = "供应商单据明细.xlsx";
        OutputStream output = response.getOutputStream();
        response.reset();
        String codedFileName = java.net.URLEncoder.encode(filename, "UTF-8");
        response.setHeader("Content-disposition", "attachment; filename=" + new String(codedFileName.getBytes(), "ISO8859-1"));
        response.setContentType("application/vnd.ms-excel");
        workbook.write(output);
        workbook.close();
        output.flush();
        output.close();
    }

    /**
     * 供应商单据汇总列表
     * @param request
     * @param data
     * @return
     */
    @ApiOperation("供应商单据汇总列表")
    @PostMapping("selectSupplierSummaryPageList")
    public JsonResult selectSupplierSummaryPageList(HttpServletRequest request,@RequestBody SelectSupplierSummaryPageList data){
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        if(StrUtil.isNotBlank(data.getDepId())){
            data.setStoCode(data.getDepId());
        }
        return JsonResult.success(reportFormsService.selectSupplierSummaryPageList(data), "提示：获取数据成功！");
    }
    @ApiOperation("供应商单据汇总导出")
    @PostMapping("supplierExport")
    public void supplierExport(HttpServletResponse response, HttpServletRequest servletRequest,@RequestBody SelectSupplierSummaryPageList data) throws IOException{
        GetLoginOutData userInfo = this.getLoginUser(servletRequest);
        if(StrUtil.isNotBlank(data.getDepId())){
            data.setStoCode(data.getDepId());
        }
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        data.setClient(userInfo.getClient());
        data.setDepId(data.getStoCode());
        SXSSFWorkbook workbook = reportFormsService.supplierExport(data);
        String filename = "供应商单据明细.xlsx";
        OutputStream output = response.getOutputStream();
        response.reset();
        String codedFileName = java.net.URLEncoder.encode(filename, "UTF-8");
        response.setHeader("Content-disposition", "attachment; filename=" + new String(codedFileName.getBytes(), "ISO8859-1"));
        response.setContentType("application/vnd.ms-excel");
        workbook.write(output);
        workbook.close();
        output.flush();
        output.close();
    }

    @ApiOperation(value = "根据加盟商获取支付方式列表",response = GetPayTypeOutData.class)
    @PostMapping({"payTypeListByClient"})
    public JsonResult payTypeListByClient(HttpServletRequest request, @RequestBody GetPayInData inData) {
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.reportFormsService.payTypeListByClient(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "更新支付对账金额")
    @PostMapping({"updatePayTypeListValue"})
    public JsonResult updatePayTypeListValue(HttpServletRequest request, @RequestBody List<HashMap<String,String>> updateList) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        reportFormsService.updatePayTypeListValue(userInfo.getClient(),updateList);
        return JsonResult.success("提示：更新成功！");
    }

    @ApiOperation(value = "配送往来数据查询")
    @PostMapping({"selectDistributionDataByBatch"})
    public JsonResult selectDistributionDataByBatch(HttpServletRequest request,@RequestBody StoreDistributaionDataInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        data.setSqlString("gmd.MAT_BAT_AMT+gmd.MAT_RATE_BAT");
        return reportFormsService.selectDistributionData(data);
    }

    @ApiOperation(value = "配送往来导出CSV")
    @PostMapping({"exportDistributionDataByBatch"})
    public Result exportDistributionDataByBatch(HttpServletRequest request,@RequestBody StoreDistributaionDataInData inData) {
        inData.setSqlString("gmd.MAT_BAT_AMT+gmd.MAT_RATE_BAT");
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        List<StoreDistributaionDataOutData> outData = reportFormsService.exportCSV(inData);
        String fileName = "配送往来数据导出";
        if (outData.size() > 0) {
            CsvFileInfo csvInfo =null;
            // 导出
            // byte数据
            for (StoreDistributaionDataOutData out : outData) {
                out.setCusCode("\t"+out.getCusCode());
            }
            csvInfo = CsvClient.getCsvByte(outData,fileName, Collections.singletonList((short)1));

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Result result = null;
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        } else {
            throw new BusinessException("提示：没有查询到数据,请修改查询条件!");
        }
    }

    @PostMapping({"selectDistributionInvoiceByBatch"})
    public JsonResult selectDistributionInvoiceByBatch(HttpServletRequest request,@RequestBody WholesaleSaleInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectDistributionInvoiceByBatch(data), "提示：获取数据成功！");
    }
    @PostMapping({"exportDistributionInvoiceByBatch"})
    public void exportDistributionInvoiceByBatch(HttpServletRequest request,HttpServletResponse response,@RequestBody WholesaleSaleInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        reportFormsService.exportDistributionInvoiceByBatch(response,data);
    }

    @PostMapping({"selectDistributionInvoiceDetailByBatch"})
    public JsonResult selectDistributionInvoiceDetailByBatch(HttpServletRequest request,@RequestBody WholesaleSaleInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        return JsonResult.success(reportFormsService.selectDistributionInvoiceDetailByBatch(data), "提示：获取数据成功！");
    }
    @PostMapping({"exportDistributionInvoiceDetailByBatch"})
    public void exportDistributionInvoiceDetailByBatch(HttpServletRequest request,HttpServletResponse response,@RequestBody WholesaleSaleInData data) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        reportFormsService.exportDistributionInvoiceDetailByBatch(response,data);
    }

}
