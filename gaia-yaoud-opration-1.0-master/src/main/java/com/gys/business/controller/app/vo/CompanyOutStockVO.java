package com.gys.business.controller.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/***
 * @desc: 公司缺断货产品展示类
 * @author: ryan
 * @createTime: 2021/6/8 23:51
 **/
@Data
@ApiModel
public class CompanyOutStockVO {
    @ApiModelProperty(value = "缺断货流水号", example = "DC123123088", position = 0)
    private String voucherId;
    @ApiModelProperty(value = "缺断货品项数量", example = "198", position = 1)
    private Integer productNum;
    @ApiModelProperty(value = "月均销售额", example = "10.8万", position = 2)
    private BigDecimal monthSaleAmount;
    @ApiModelProperty(value = "月均毛利额", example = "6.7万", position = 3)
    private BigDecimal monthProfitAmount;
    @ApiModelProperty(value = "商品列表", example = "[]", position = 4)
    private List<ProductInfoVO> list;

    public CompanyOutStockVO() {
        this.productNum = 0;
        this.monthProfitAmount = BigDecimal.ZERO;
        this.monthSaleAmount = BigDecimal.ZERO;
        this.list = new ArrayList<>();
    }
}
