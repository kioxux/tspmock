package com.gys.business.controller;

import com.gys.business.entity.dto.CallUpOrInStoreDetailDTO;
import com.gys.business.entity.vo.CallInStoreDetailVO;
import com.gys.business.service.GaiaStoreInSuggestionHService;
import com.gys.common.data.JsonResult;
import com.gys.common.enums.StoInSuggestionEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * 门店调入建议-主表(GaiaStoreInSuggestionH)表控制层
 *
 * @author XIAO-ZY
 * @since 2021-10-28 10:53:20
 */
@RestController
@RequestMapping("gaiaStoreInSuggestionH")
@Api(tags = "门店调剂建议调入接口")
public class GaiaStoreInSuggestionHController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaStoreInSuggestionHService gaiaStoreInSuggestionHService;

    @ApiOperation(value = "门店调剂建议调入明细" ,response = CallInStoreDetailVO.class)
    @PostMapping("queryDetail")
    public JsonResult queryDetail(@RequestBody CallUpOrInStoreDetailDTO storeDetailDTO){
       return JsonResult.success(this.gaiaStoreInSuggestionHService.queryDetail(storeDetailDTO));
    }

    @PostMapping("queryDetail/export")
    public void queryDetailExport(HttpServletResponse response, @RequestBody CallUpOrInStoreDetailDTO storeDetailDTO){
        this.gaiaStoreInSuggestionHService.queryDetailExport(response,storeDetailDTO);
    }
    @GetMapping("queryType")
    public JsonResult querytype(){
        return JsonResult.success(StoInSuggestionEnum.toJson());
    }

    @GetMapping("queryBillCode")
    public JsonResult queryBillCode(@RequestParam(value = "suggestedStartDate" ,required = false) String startDate,
                                    @RequestParam(value = "suggestedEndDate",required = false) String endDate){

        return JsonResult.success(this.gaiaStoreInSuggestionHService.queryBillCode(startDate,endDate));
    }

}

