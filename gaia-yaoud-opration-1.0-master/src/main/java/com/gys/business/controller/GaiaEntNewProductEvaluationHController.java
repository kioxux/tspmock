package com.gys.business.controller;


import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationD;
import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationH;
import com.gys.business.service.GaiaEntNewProductEvaluationHService;
import com.gys.business.service.data.GaiaEntNewProductEvaluationDOutData;
import com.gys.business.service.data.GaiaEntNewProductEvaluationInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 公司级-新品评估-主表(GaiaEntNewProductEvaluationH)表控制层
 *
 * @author makejava
 * @since 2021-07-19 10:16:54
 */
@RestController
@RequestMapping("gaiaEntNewProductEvaluationH")
public class GaiaEntNewProductEvaluationHController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaEntNewProductEvaluationHService gaiaEntNewProductEvaluationHService;

    /**
     * 生成公司级新品评估数据
     *
     */
    @PostMapping("insertEntProductData")
    public JsonResult insertEntProductData(@RequestParam("param") String param) {
        return JsonResult.success(gaiaEntNewProductEvaluationHService.insertEntProductData(param), "提示：获取数据成功！");
    }

    /**
     * 查询商品评估列表
     */
    @PostMapping("listEntProductBill")
    public JsonResult listEntProductBill(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return JsonResult.success(gaiaEntNewProductEvaluationHService.listEntProductBill(param), "提示：获取数据成功！");
    }


    /**
     * 查询商品表单明细列表
     */
    @PostMapping("listEntProductBillDetail")
    public JsonResult listEntProductBillDetail(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return JsonResult.success(gaiaEntNewProductEvaluationHService.listEntProductBillDetail(param), "提示：获取数据成功！");
    }

    /**
     * 查询商品表单明细列表
     */
    @PostMapping("listEntInfo")
    public JsonResult listEntInfo(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return JsonResult.success(gaiaEntNewProductEvaluationHService.listEntInfo(param), "提示：获取数据成功！");
    }


    /**
     * 明细页面-保存
     */
    @PostMapping("updateDetailList")
    public JsonResult updateDetailList(HttpServletRequest request, @RequestBody List<GaiaEntNewProductEvaluationD> param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(gaiaEntNewProductEvaluationHService.updateDetailList(userInfo,param), "提示：保存数据成功！");
    }

    /**
     * 明细页面-确认评估
     */
    @PostMapping("confirmBill")
    public JsonResult confirmBill(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationH param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return JsonResult.success(gaiaEntNewProductEvaluationHService.confirmBill(userInfo,param), "提示：保存数据成功！");
    }
    /**
     * 明细页面-确认评估-提示
     */
    @PostMapping("confirmBillHint")
    public Object confirmBillHint(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationH param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return JsonResult.success(gaiaEntNewProductEvaluationHService.confirmBillHint(userInfo,param), "提示：保存数据成功！");
    }

    /**
     * 明细页面-导出
     */
    @PostMapping("export")
    public Object export(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return gaiaEntNewProductEvaluationHService.export(userInfo,param);
    }

    /**
     * app-老总层查看页面
     */
    @PostMapping("listEntProductionDetail")
    public JsonResult listEntProductionDetail(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return JsonResult.success(gaiaEntNewProductEvaluationHService.listEntProductionDetail(userInfo,param), "提示：获取数据成功！");
    }

    /**
     * app-查看新品评估结果
     */
    @PostMapping("listEntProductionResult")
    public JsonResult listEntProductionResult(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return JsonResult.success(gaiaEntNewProductEvaluationHService.listEntProductionResult(userInfo,param), "提示：获取数据成功！");
    }

    @PostMapping("getEntProductionResultDetail")
    public JsonResult getEntProductionResultDetail(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return JsonResult.success(gaiaEntNewProductEvaluationHService.getEntProductionResultDetail(userInfo,param), "提示：获取数据成功！");
    }

    @PostMapping("updateResult")
    public JsonResult updateResult(HttpServletRequest request, @RequestBody GaiaEntNewProductEvaluationH param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        return JsonResult.success(gaiaEntNewProductEvaluationHService.updateResult(userInfo,param), "提示：获取数据成功！");
    }

}
