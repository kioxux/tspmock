package com.gys.business.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.entity.*;
import com.gys.business.service.ElectronicCouponsService;
import com.gys.business.service.SalesReceiptsService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.CommonData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Api(tags = "销售收货", description = "/salesReceipts")
@RestController
@RequestMapping({"/salesReceipts/"})
@Slf4j
public class SalesReceiptsController extends BaseController {
    @Autowired
    private SalesReceiptsService salesReceiptsService;
    @Autowired
    private ElectronicCouponsService electronicCouponsService;



    @PostMapping({"queryMember"})
    public JsonResult queryMember(HttpServletRequest request, @RequestBody GetQueryMemberInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        GetQueryMemberOutData outData = this.salesReceiptsService.queryMember(inData);
        if (ObjectUtil.isEmpty(outData)) {
            return JsonResult.success(null, "提示：获取数据成功！");
        }
        GaiaSdMemberCardData param = new GaiaSdMemberCardData();
        param.setClient(userInfo.getClient());
        param.setGsmbcBrId(userInfo.getDepId());
        param.setGsmbcCardId(outData.getCardNum());
        List<ElectronDetailOutData> elList = this.electronicCouponsService.queryCanUseElectron(param);
        if (CollectionUtil.isNotEmpty(elList)) {
            outData.setElNum(String.valueOf(elList.size()));
        } else {
            outData.setElNum("0");
        }
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"queryProduct"})
    public JsonResult queryProduct(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        List<GetQueryProductOutData> outData = this.salesReceiptsService.queryProduct(inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"queryProductDetail"})
    public JsonResult queryProductDetail(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.queryProductDetail(inData), "提示：获取数据成功！");
    }

    @PostMapping({"queryBatchNoAndExp"})
    public JsonResult queryBatchNoAndExp(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.queryBatchNoAndExp(inData), "提示：获取数据成功！");
    }

    @PostMapping({"queryStockAndExpGroupByBatchNo"})
    public JsonResult queryStockAndExpGroupByBatchNo(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.queryStockAndExpGroupByBatchNo(inData), "提示：获取数据成功！");
    }

    @PostMapping({"queryStockAndExpGroupByThird"})
    public JsonResult queryStockAndExpGroupByThird(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.queryStockAndExpGroupByThird(inData), "提示：获取数据成功！");
    }

    @PostMapping({"pointExchange"})
    public JsonResult pointExchange(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.pointExchange(inData), "提示：获取数据成功！");
    }

    @PostMapping({"queryEmp"})
    public JsonResult queryEmp(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.salesReceiptsService.queryEmp(userInfo), "提示：获取数据成功！");
    }

    @PostMapping({"pharmacist"})
    public JsonResult pharmacist(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.salesReceiptsService.pharmacist(userInfo), "success");
    }

    @PostMapping({"queryAllEmp"})
    public JsonResult queryAllEmp(HttpServletRequest request,@RequestBody CommonData content) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.salesReceiptsService.queryAllEmp(userInfo,content), "提示：获取数据成功！");
    }


    @PostMapping({"queryDoctor"})
    public JsonResult queryDoctor(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.salesReceiptsService.queryDoctor(userInfo), "提示：获取数据成功！");
    }

    @PostMapping({"queryDruggist"})
    public JsonResult queryDruggist(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.salesReceiptsService.queryDruggist(userInfo), "提示：获取数据成功！");
    }

    @PostMapping({"promByProCode"})
    public JsonResult promByProCode(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.promByProCode(inData), "提示：获取数据成功！");
    }

    @PostMapping({"memByProCode"})
    public JsonResult memByProCode(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.memByProCode(inData), "提示：获取数据成功！");
    }

    @PostMapping({"giftCardByProCode"})
    public JsonResult giftCardByProCode(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.giftCardByProCode(inData), "提示：获取数据成功！");
    }

    @PostMapping({"createSale"})
    public JsonResult createSale(HttpServletRequest request, @RequestBody GetCreateSaleOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        GaiaSdSaleH gaiaSdSaleH = inData.getGaiaSdSaleH();
        gaiaSdSaleH.setClientId(userInfo.getClient());
        gaiaSdSaleH.setGsshBrId(userInfo.getDepId());
        this.salesReceiptsService.createSalePendingOrder(inData);
        return JsonResult.success("", "提示：保存成功！");
    }


    @PostMapping({"getDisPrice"})
    public JsonResult getDisPrice(HttpServletRequest request, @RequestBody GetDisPriceInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.getDisPrice(inData), "提示：保存成功！");
    }

    @PostMapping({"saveRecipelInfo"})
    public JsonResult saveRecipelInfo(HttpServletRequest request, @RequestBody RecipelInfoInputData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssrBrId(userInfo.getDepId());
        this.salesReceiptsService.saveRecipelInfo(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "处方生成")
    @PostMapping({"updateRecipelInfo"})
    public JsonResult updateRecipelInfo(HttpServletRequest request, @ApiParam(value = "处方记录", required = true) @RequestBody RecipelInfoInputData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssrBrId(userInfo.getDepId());
        inData.setGssrBrName(userInfo.getDepName());
        inData.setGssrEmp(userInfo.getUserId());
        inData.setGssrSaleBrId(userInfo.getDepId());
        this.salesReceiptsService.updateRecipelInfo(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"getRestOrder"})
    public JsonResult getRestOrder(HttpServletRequest request, @RequestBody GaiaSdSaleH inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsshBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.getRestOrder(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getRestOrder2"})
    public JsonResult getRestOrder2(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.salesReceiptsService.getRestOrder2(userInfo,map), "提示：获取数据成功！");
    }


    @PostMapping({"getRestOrderDetail"})
    public JsonResult getRestOrderDetail(HttpServletRequest request, @RequestBody GaiaSdSaleD inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssdBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.getRestOrderDetail(inData), "提示：获取数据成功！");
    }

    @PostMapping({"afterSplitePrice"})
    public JsonResult afterSplitePrice(HttpServletRequest request, @RequestBody GaiaRetailPriceInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.afterSplitePrice(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getStoreDiscount"})
    public JsonResult getStoreDiscount(HttpServletRequest request, @RequestBody GaiaRetailPrice inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.salesReceiptsService.getStoreDiscount(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getStoreDyqMaxAmount"})
    public JsonResult getStoreDyqMaxAmount(HttpServletRequest request, @RequestBody GaiaRetailPrice inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.salesReceiptsService.getStoreDyqMaxAmount(inData), "提示：获取数据成功！");
    }

    @PostMapping({"approveChangePrice"})
    public JsonResult approveChangePrice(HttpServletRequest request, @RequestBody ApproveChangePriceInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String token = request.getHeader("X-Token");
        inData.setClientId(userInfo.getClient());
        this.salesReceiptsService.approveChangePrice(inData, token);
        return JsonResult.success((Object) null, "提示：获取数据成功！");
    }

    @PostMapping({"approveChangeRate"})
    public JsonResult approveChangeRate(HttpServletRequest request, @RequestBody ApproveChangePriceInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String token = request.getHeader("X-Token");
        inData.setClientId(userInfo.getClient());
        this.salesReceiptsService.approveChangeRate(inData, token);
        return JsonResult.success((Object) null, "提示：获取数据成功！");
    }

    @PostMapping({"approveChangeAllRate"})
    public JsonResult approveChangeAllRate(HttpServletRequest request, @RequestBody ApproveChangePriceInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String token = request.getHeader("X-Token");
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        this.salesReceiptsService.approveChangeAllRate(inData, token);
        return JsonResult.success((Object) null, "提示：获取数据成功！");
    }

    @PostMapping({"checkPrintBill"})
    public JsonResult checkPrintBill(HttpServletRequest request, @RequestBody CheckParamData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.checkPrintBill(inData), "提示：获取数据成功！");
    }

    @PostMapping({"otherShops"})
    public JsonResult otherShops(HttpServletRequest request, @RequestBody GetLoginOutData userInfo) {
        return JsonResult.success(this.salesReceiptsService.otherShops(userInfo), "提示：获取数据成功！");
    }

    @PostMapping({"giveGiftCard"})
    public JsonResult giveGiftCard(HttpServletRequest request, @RequestBody ConllectionInData conllectionInData) {
        this.salesReceiptsService.giveGiftCard(conllectionInData.getPromCouponBasicList());
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"giftSelect"})
    public JsonResult giftSelect(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.giftSelect(inData), "提示：保存成功！");
    }

    @PostMapping({"getPointSet"})
    public JsonResult getPointSet(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.salesReceiptsService.getPointSet(userInfo), "提示：保存成功！");
    }

    @PostMapping({"getMemberPointSet"})
    public JsonResult getMemberPointSet(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesReceiptsService.getMemberPointSet(inData), "提示：保存成功！");
    }

    @PostMapping({"givePoint"})
    public JsonResult givePoint(HttpServletRequest request, @RequestBody GetQueryMemberInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        this.salesReceiptsService.givePoint(inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    @PostMapping({"findSingleProductPromotionByCode"})
    public JsonResult findSingleProductPromotionByCode(HttpServletRequest request, @RequestBody PromotionMethodInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(salesReceiptsService.findSingleProductPromotionByCode(inData), "提示：获取数据成功！");
    }

    @PostMapping({"findProductPromotionByCode"})
    public JsonResult findProductPromotionByCode(HttpServletRequest request, @RequestBody PromotionMethodInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(salesReceiptsService.findProductPromotionByCode(inData), "提示：获取数据成功！");
    }

    //改版的促销查询
    @PostMapping({"findProductPromotionByCode2"})
    public JsonResult findProductPromotionByCode2(HttpServletRequest request, @RequestBody PromotionMethodInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(salesReceiptsService.findProductPromotionByCode2(inData), "提示：获取数据成功！");
    }

    @PostMapping("insertPendingOrder")
    public JsonResult insertPendingOrder(HttpServletRequest request, @RequestBody PendingOrderInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping("findPromotionById")
    public JsonResult findPromotionById(HttpServletRequest request, @RequestBody List<FindPromotionInData> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        for (FindPromotionInData inDatum : inData) {
            inDatum.setClient(userInfo.getClient());
            inDatum.setBrId(userInfo.getDepId());
        }
        return JsonResult.success(salesReceiptsService.findPromotionById(inData), "提示：保存成功！");
    }

    @PostMapping("findProductPromotionByIdAndArgs")
    public JsonResult findProductPromotionByIdAndArgs(HttpServletRequest request, @RequestBody List<FindPromotionInData> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        for (FindPromotionInData inDatum : inData) {
            inDatum.setClient(userInfo.getClient());
            inDatum.setBrId(userInfo.getDepId());
        }
        return JsonResult.success(salesReceiptsService.findProductPromotionByIdAndArgs(inData), "提示：保存成功！");
    }

    @PostMapping({"findSeriesCondsByRebornList"})
    public JsonResult findSeriesCondsByRebornList(HttpServletRequest request, @RequestBody PromotionMethodInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(salesReceiptsService.findSeriesCondsByRebornList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"findIntegralExchangeByProCode"})
    public JsonResult findIntegralExchangeByProCode(HttpServletRequest request, @RequestBody IntegralExchangeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(salesReceiptsService.findIntegralExchangeByProCode(inData), "提示：获取数据成功！");
    }

    @PostMapping({"findMemberPromotion"})
    public JsonResult findMemberPromotion(HttpServletRequest request, @RequestBody MemberPromotionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(salesReceiptsService.findMemberPromotion(inData), "提示：获取数据成功！");
    }

    @PostMapping({"verifyInventory"})
    public JsonResult verifyInventory(HttpServletRequest request, @RequestBody MemberPromotionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(salesReceiptsService.verifyInventory(inData), "提示：获取数据成功！");
    }

    @PostMapping({"checkCurrentMembers"})
    public JsonResult checkCurrentMembers(HttpServletRequest request, @RequestBody GaiaSdSaleD inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssdBrId(userInfo.getDepId());
        CurrentMembersOutData outData = salesReceiptsService.checkCurrentMembers(inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }
    @PostMapping({"handoverReport"})
    public JsonResult handoverReport(HttpServletRequest request, @RequestBody GaiaRetailPriceInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        HandoverReportOutData outData = salesReceiptsService.handoverReport(inData);
        return JsonResult.success(outData, "success");
    }
    @PostMapping({"getAllByProIds"})
    public JsonResult getAllByProIds(HttpServletRequest request, @RequestBody MemberPromotionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(salesReceiptsService.getAllByProIds(inData), "success");
    }

    @PostMapping({"getRechargeByCardId"})
    public JsonResult getRechargeByCardId(HttpServletRequest request, @RequestBody MemberPromotionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(salesReceiptsService.getRechargeByCardId(inData), "success");
    }

    @PostMapping({"deletePendingOrder"})
    public JsonResult deletePendingOrder(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.salesReceiptsService.deletePendingOrder(userInfo,map);
        return JsonResult.success("", "success");
    }

    /**
     * 获取商品成本价
     * @param request
     * @param map
     * @return
     */
    @PostMapping({"commodityCost"})
    public JsonResult commodityCost(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success( this.salesReceiptsService.commodityCost(userInfo,map), "success");
    }

    /**
     * 挂单请求
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"createSaleHand"})
    public JsonResult createSaleHand(HttpServletRequest request, @RequestBody GetCreateSaleOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        GaiaSdSaleH gaiaSdSaleH = inData.getGaiaSdSaleH();
        //5 门店代售  brId 和client 为代售门店信息
        if (!"5".equals(gaiaSdSaleH.getGsshBillType())) {
            gaiaSdSaleH.setClientId(userInfo.getClient());
            gaiaSdSaleH.setGsshBrId(userInfo.getDepId());
        }
        this.salesReceiptsService.createSaleHand(userInfo,inData);
        return JsonResult.success("", "success");
    }

    /**
     * 调出挂单表格一
     * @param request
     * @return
     */
    @PostMapping({"getSaleHand"})
    public JsonResult getSaleHand(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<GetRestOrderInfoOutData> outData = this.salesReceiptsService.getSaleHand(userInfo,map);
        return JsonResult.success(outData, "success");
    }

    /**
     * 调出挂单表格二
     * @param request
     * @return
     */
    @PostMapping({"getSaleHandDetail"})
    public JsonResult getSaleHandDetail(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<RestOrderDetailOutData> outData = this.salesReceiptsService.getSaleHandDetail(userInfo,map);
        return JsonResult.success(outData, "success");
    }

    /**
     * 挂单作废
     * @param request
     * @return
     */
    @PostMapping({"updateHandSaleH"})
    public JsonResult updateHandSaleH(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.salesReceiptsService.updateHandSaleH(userInfo,map);
        return JsonResult.success("", "success");
    }

    /**
     * 如果此销售单有挂单数据，将挂单数据作废，并返回此单配送状态
     * @param request
     * @return
     */
    @PostMapping({"updateAndReturnStatus"})
    public JsonResult updateAndReturnStatus(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return salesReceiptsService.updateAndReturnStatus(userInfo, map);
    }

    /**
     * 锁定挂单状态
     * @param request
     * @param map
     * @return
     */
    @PostMapping({"lockedState"})
    public JsonResult lockedState(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<RestOrderDetailOutData> outData = this.salesReceiptsService.getSaleHandDetail(userInfo,map);
        return JsonResult.success(outData, "success");
    }

    /**
     * 查询加盟商下会员 所有的购买记录
     * @param request
     * @param map
     * @return
     */
    @PostMapping({"getClientPurchaseRecordsByMember"})
    public JsonResult getClientPurchaseRecordsByMember(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<MemberPurchaseRecordOutData> outData = this.salesReceiptsService.getClientPurchaseRecordsByMember(userInfo.getClient(),map.get("hykNo"));
        return JsonResult.success(outData, "success");
    }


    /**
     * 获取打印明细
     * @param request
     * @param map
     * @return
     */
    @PostMapping({"getPrintDetails"})
    public JsonResult getPrintDetails(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client",userInfo.getClient());
        map.put("brId",userInfo.getDepId());
        List<RestOrderDetailDto> detailDto = this.salesReceiptsService.getPrintDetails(map);
        return JsonResult.success(detailDto, "提示：获取数据成功！");
    }

    /**
     * 获取打印明细
     * @param request
     * @param map
     * @return
     */
    @PostMapping({"getPrintAll"})
    public JsonResult getPrintAll(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client",userInfo.getClient());
        map.put("brId",userInfo.getDepId());
        RestOrderChangeAll printAll = this.salesReceiptsService.getPrintAll(map);
        return JsonResult.success(printAll, "success");
    }

    /**
     * 查询所有电子券信息
     * @param request
     * @param map
     * @return
     */
    @PostMapping({"getGiveAndUseAll"})
    public JsonResult getGiveAndUseAll(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client",userInfo.getClient());
        map.put("brId",userInfo.getDepId());
        List<GaiaSdElectronChange> detailDto = this.salesReceiptsService.getGiveAndUseAll(map);
        return JsonResult.success(detailDto, "提示：获取数据成功！");
    }

    /**
     * 远程处方-调出挂单表格一
     * @param request
     * @return
     */
    @PostMapping({"listSaleHand"})
    public JsonResult listSaleHand(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        log.info(String.format("<远程处方><调出挂单表格一><请求参数：%s>", JSON.toJSONString(map)));
        List<GetRestOrderInfoOutData> outData = this.salesReceiptsService.listSaleHand(userInfo,map);
        return JsonResult.success(outData, "success");
    }

    /**
     * 配送代煎--修改挂单表配送状态
     * @param request
     * @return
     */
    @PostMapping({"updateSaleHandStatus"})
    public JsonResult updateSaleHandStatus(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return salesReceiptsService.updateSaleHandStatus(userInfo, map);
    }

    /**
     * 配送代煎--获取仓库编码
     * 这个建议由门店来传
     * 通过门店号找到其对应的配送中心编号1
     * 再通过这个配送中心编号1找到其对应的委托配送中心编号2
     * 如果只能找到编号1，找不到编号2，就只传编号1
     * 如果能找到编号2就传编号2
     * 如果都找不到，则提示“请维护配送中心”
     * @param request
     * @return
     */
    @PostMapping({"getSite"})
    public JsonResult getSite(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return salesReceiptsService.getSite(userInfo);
    }

    /**
     * 远程处方-调出挂单表格二
     * @param request
     * @return
     */
    @PostMapping({"listRestOrderDetail"})
    public JsonResult listRestOrderDetail(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        log.info(String.format("<远程处方><调出挂单表格二><请求参数：%s>", JSON.toJSONString(map)));
        List<RestOrderDetailOutData> outData = this.salesReceiptsService.listRestOrderDetail(userInfo,map);
        return JsonResult.success(outData, "success");
    }

    /**
     * 改价工作流-审核
     * @param request
     * @return
     */
    @PostMapping({"orderCommit"})
    public JsonResult orderCommit(HttpServletRequest request, @RequestBody Map<String, String> map) {
//        GetLoginOutData userInfo = this.getLoginUser(request);
        log.info(String.format("<改价工作流><审核><请求参数：%s>", JSON.toJSONString(map)));
        this.salesReceiptsService.orderCommit(map);
        return JsonResult.success("","success");
    }

    /**
     * 调出HIS挂单表格一
     * @param request
     * @return
     */
    @PostMapping({"getHisHand"})
    public JsonResult getHisHand(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<GetRestOrderInfoOutData> outData = this.salesReceiptsService.getHisHand(userInfo,map);
        return JsonResult.success(outData, "success");
    }
}
