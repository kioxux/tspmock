package com.gys.business.controller.yaomeng;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.yaomeng.YaomengService;
import com.gys.common.config.RabbitConfig;
import com.gys.common.data.MQReceiveData;
import com.gys.common.data.yaomeng.GaiaPrescribeInfo;
import com.gys.common.rabbit.publish.RabbitTemplateHelper;
import com.gys.util.yaomeng.ReturnRecipelInfo;
import com.gys.util.yaomeng.YaomengResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ZhangDong
 * @date 2021/11/4 15:20
 */
@Controller
@RequestMapping("yaomeng/push")
public class ApiYaomengPushController {
    private static final Logger log = LoggerFactory.getLogger(ApiYaomengPushController.class);
    @Autowired
    private YaomengService yaomengService;
    @Resource
    private RabbitTemplateHelper rabbitTemplate;

    /**
     * 回传处方信息
     * 最慢1-2分钟 第一次回推，正常是提交开方后立马回推
     * 失败重试3次  间隔5秒
     * 返参 {"code":0,"msg":null,"data":null}
     * code 成功0  失败非0
     * 药盟那边做成前端延时十几秒后提交开方
     */
    @PostMapping("returnRecipelInfo")
    @ResponseBody
    public Map<String, Object> returnRecipelInfo(@RequestBody YaomengResponse<ReturnRecipelInfo> info) {
        log.info("药盟回传处方信息，info:{}", JSON.toJSONString(info));
        ReturnRecipelInfo infoData = info.getData();
        String saleOrderId = infoData.getOrderId();
        String no = infoData.getNo();
        GaiaPrescribeInfo gaiaPrescribeInfo = yaomengService.getPrescribeBySaleOrderIdLimit(saleOrderId);
        if (gaiaPrescribeInfo == null) {
            log.error("药盟回传处方信息，根据销售单号查不到会诊数据，saleOrderId:{}", saleOrderId);
            return getReturnData(1, "失败");
        }
        String client = gaiaPrescribeInfo.getClient();
        String stoCode = gaiaPrescribeInfo.getPreBrId();
        MQReceiveData mqReceiveData = new MQReceiveData();
        mqReceiveData.setType("RecipelFinish");
        mqReceiveData.setCmd("");
        mqReceiveData.setData(no);
        this.rabbitTemplate.convertAndSend(RabbitConfig.EXCHANGE_TOPIC, client + "." + stoCode, JSON.toJSON(mqReceiveData));
        log.info("药盟回传处方信息，推送回传信息到门店, client:{},stoCode:{},no:{}", client, stoCode, no);
        return getReturnData(0, "成功");
    }

    private Map<String, Object> getReturnData(int code, String msg) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("code", code);
        map.put("msg", msg);
        return map;
    }

}
