package com.gys.business.controller;

import com.gys.business.service.YunNanZYYSSuperviseService;
import com.gys.business.service.data.AcceptWareHouseOutData;
import com.gys.business.service.data.YunNanPrescriptionInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author li_haixia@gov-info.cn
 * @desc 云南省执业药师远程药事服务及审方业务监管系统 对接api
 * @date 2021/11/11 14:20
 */
@Api(tags = "yunnanSupervise", description = "/yunnanSupervise")
@RestController
@RequestMapping({"/yunnanSupervise/"})
public class YunNanZYYSSuperviseController extends BaseController {
    @Autowired
    private YunNanZYYSSuperviseService yunNanZYYSSuperviseService;

    @ApiOperation(value = "药师签到" ,response = Result.class)
    @PostMapping({"login"})
    public JsonResult login(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return yunNanZYYSSuperviseService.login(userInfo);
    }

    @ApiOperation(value = "药师签退" ,response = Result.class)
    @PostMapping({"logout"})
    public JsonResult logout(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return yunNanZYYSSuperviseService.logout(userInfo);
    }

    @ApiOperation(value = "处方信息(西药处方)" ,response = Result.class)
    @PostMapping({"prescription"})
    public JsonResult prescription(HttpServletRequest request, @RequestBody YunNanPrescriptionInData inData) {
        return yunNanZYYSSuperviseService.prescription(inData);
    }


}
