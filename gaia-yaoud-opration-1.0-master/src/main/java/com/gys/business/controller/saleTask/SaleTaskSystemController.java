package com.gys.business.controller.saleTask;

import com.gys.business.mapper.entity.GaiaSaletaskHplanSystem;
import com.gys.business.service.data.saleTask.SalePlanPushInData;
import com.gys.business.service.data.saleTask.SaleTaskSystemInData;
import com.gys.business.service.data.saleTask.SaleTaskSystemOutData;
import com.gys.business.service.saleTask.SaleTaskSystemService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Api(tags = "月度计划药德运维维护")
@RestController
@RequestMapping({"/saleTask/system/"})
public class SaleTaskSystemController extends BaseController {
    @Resource
    private SaleTaskSystemService saleTaskSystemService;
    //审核
    @ApiOperation(value = "药德月度计划新增")
    @PostMapping({"insertSystemSalesPlan"})
    public JsonResult insertSystemSalesPlan(HttpServletRequest request, @RequestBody SaleTaskSystemInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.saleTaskSystemService.insertSystemSalesPlan(userInfo,inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    @ApiOperation(value = "月销售任务列表查询" , response = SaleTaskSystemOutData.class)
    @PostMapping({"salePlanList"})
    public JsonResult selectSalePlanList(HttpServletRequest request, @RequestBody SaleTaskSystemInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        //inData.setClientId(userInfo.getClient());
        //inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.saleTaskSystemService.selectSalePlanList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "月销售任务详情查询" , response = GaiaSaletaskHplanSystem.class)
    @PostMapping({"salePlanDetail"})
    public JsonResult selectSalePlanDetail(HttpServletRequest request, @RequestBody SaleTaskSystemInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        //inData.setClientId(userInfo.getClient());
        //inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.saleTaskSystemService.selectSalePlanDetail(inData), "提示：获取数据成功！");
    }

    //月度任务推送
    @ApiOperation(value = "月度任务推送")
    @PostMapping({"pushSalePlan"})
    public JsonResult pushSalePlan(HttpServletRequest request, @RequestBody SalePlanPushInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        //inData.setClientId(userInfo.getClient());
        //inData.setBrId(userInfo.getDepId());
        this.saleTaskSystemService.pushSalePlan(inData);
        return JsonResult.success("", "提示：推送成功！");
    }
    //月度任务定时推送
    @ApiOperation(value = "月度任务定时推送")
    @PostMapping({"timerPushSalePlan"})
    public JsonResult timerPushSalePlan() {
        this.saleTaskSystemService.timerPushSalePlan();
        return JsonResult.success("", "提示：推送成功！");
    }

    //月度任务删除
    @ApiOperation(value = "月度任务删除")
    @PostMapping({"deleteSalePlan"})
    public JsonResult deleteSalePlan(HttpServletRequest request, @RequestBody SalePlanPushInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        //inData.setClientId(userInfo.getClient());
        //inData.setBrId(userInfo.getDepId());
        this.saleTaskSystemService.deleteSalePlan(inData);
        return JsonResult.success("", "提示：删除成功！");
    }
}
