//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.MedCheckService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "药品养护", description = "/medCheck")
@RestController
@RequestMapping({"/medCheck/"})
public class MedCheckController extends BaseController {
    @Resource
    private MedCheckService medCheckService;

    public MedCheckController() {
    }

    @ApiOperation(value = "查询药品养护记录",response = GetMedCheckOutData.class)
    @PostMapping({"selectMedCheckList"})
    public JsonResult selectMedCheckList(HttpServletRequest request, @ApiParam(value = "查询条件", required = true) @RequestBody GetMedCheckInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsmchBrId(userInfo.getDepId());
        List<GetMedCheckOutData> list = this.medCheckService.selectMedCheckList(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "新增药品养护记录")
    @PostMapping({"saveMedCheck"})
    public JsonResult saveMedCheck(HttpServletRequest request, @ApiParam(value = "查询条件", required = true)@RequestBody GetMedCheckInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.medCheckService.saveMedCheck(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "查询药品信息记录",response = GetMedCheckOutData.class)
    @PostMapping({"selectMedCheckOne"})
    public JsonResult selectMedCheckOne(HttpServletRequest request, @RequestBody GetMedCheckInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsmchBrId(userInfo.getDepId());
        GetMedCheckOutData outData = this.medCheckService.selectMedCheckOne(inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @ApiOperation(value = "药品养护审核记录")
    @PostMapping({"approveMedCheck"})
    public JsonResult approveMedCheck(HttpServletRequest request, @RequestBody GetMedCheckInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.medCheckService.approveMedCheck(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "养护人员查询")
    @PostMapping({"selectPharmacistByBrId"})
    public JsonResult selectPharmacistByBrId(HttpServletRequest request, @RequestBody UserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setDepId(userInfo.getDepId());
        List<UserOutData> list = this.medCheckService.selectPharmacistByBrId(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }
}
