package com.gys.business.controller.takeAway;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.GaiaStoreDataMapper;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.base.BaseController;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.UuidUtil;
import com.gys.util.takeAway.ddky.DdkyApiUtil;
import com.gys.util.takeAway.ddky.DdkyOrderStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 叮当快药
 *
 * @author ZhangDong
 * @date 2021/10/13 17:49
 */
@Controller
@RequestMapping("ddky/push")
public class ApiDdkyPushController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(ApiDdkyPushController.class);
    private static final String METHOD_ORDER_PUSH_INFO = "order_push_info";
    private static final String METHOD_ORDER_PUSH_STATUS = "order.push.status";

    @Autowired
    private TakeAwayService takeAwayService;
    @Autowired
    private BaseService baseService;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @PostMapping("cmd")
    @ResponseBody
    public Map<String, String> pushCmdWithDdky(HttpServletRequest request) {
        Map<String, Object> paramMap = getParameterMap(request);
        log.info("叮当快药, paramMap:{}", JSON.toJSONString(paramMap));
        String method = (String) paramMap.get("method");
        String sign = (String) paramMap.get("sign");
        String companyId = (String) paramMap.get("companyId");
        String data = (String) paramMap.get("data");
        JSONObject dataJson = JSONObject.parseObject(data);
        Map<String, String> resultMap = new HashMap<>(2);
        switch (method) {
            case METHOD_ORDER_PUSH_INFO:
                newOrder(dataJson, resultMap);
                break;
            case METHOD_ORDER_PUSH_STATUS:
                orderStatus(dataJson, resultMap);
                break;
        }
        return resultMap;
    }

    /**
     * 新订单
     */
    private void newOrder(JSONObject dataJson, Map<String, String> resultMap) {
        JSONArray jsonArray = dataJson.getJSONArray("orderItems");
        JSONObject firstItem = (JSONObject) jsonArray.get(0);
        String placepointid = firstItem.getString("placepointid");//药店三方系统编码 我们这边上传的是client+stoCode 因为它要求Long
        String client = placepointid.substring(0, 8);
        String stoCode = placepointid.substring(8);
        GaiaTAccount account = takeAwayService.getAccountByClientAndStoCodeAndPlat(client, stoCode, Constants.PLATFORMS_DDKY);
        if (account == null || !"Y".equalsIgnoreCase(account.getOrderOnService())) {
            log.error("叮当快药，查不到account或订单开关未开启, client:{}, stoCode:{}", client, stoCode);
            resultMap.put("code", "0");
            resultMap.put("msg", "success");
            return;
        }
        GaiaStoreData storeData = gaiaStoreDataMapper.findByClientAndStoCode(client, stoCode);
        String shopNo = storeData != null ? storeData.getPublicCode() : "";
        BaseOrderInfo orderInfo = DdkyApiUtil.convertToBaseOrder(dataJson);
        if (DdkyOrderStatus.ORDER_CREATED.getStatus().equals(orderInfo.getStatus())) {
            String orderId = UuidUtil.getUUID("ddky-");
            orderInfo.setShop_no(shopNo);
            orderInfo.setOrder_id(orderId);
            List<BaseOrderProInfo> items = orderInfo.getItems();
            for (int i = 0; i < items.size(); i++) {
                BaseOrderProInfo proInfo = items.get(i);
                proInfo.setClient(client);
                proInfo.setStoCode(stoCode);
                proInfo.setOrder_id(orderId);
                proInfo.setSequence(String.valueOf(i + 1));
            }
            orderInfo.setStatus("1");
            orderInfo.setNotice_message("new");
            orderInfo.setClient(client);
            orderInfo.setStoCode(stoCode);
            String resultForXB = baseService.insertOrderInfoWithMap(orderInfo);
            if (resultForXB.startsWith("ok")) {
                takeAwayService.execOrdersPush(getOrderInfoToPTSByMap(orderInfo), Constants.PLATFORMS_EB, null, null, null);
                resultMap.put("code", "0");
                resultMap.put("msg", "success");
            } else {
                resultMap.put("errno", "500");
                resultMap.put("error", resultForXB);
            }
        } else if (DdkyOrderStatus.ORDER_SHIPPING.getStatus().equals(orderInfo.getStatus())) {
            String shippingFee = firstItem.getString("price");
            String platformOrderId = firstItem.getString("taskid");
            try {
                baseService.ddkyUpdateShippingFee(platformOrderId, shippingFee);
                log.info("叮当快药，更新订单运费信息成功，platformOrderId:{}, shippingFee:{}", platformOrderId, shippingFee);
                resultMap.put("code", "0");
                resultMap.put("msg", "success");
            } catch (Exception e) {
                log.error("叮当快药，更新订单运费信息失败，platformOrderId:{}, shippingFee:{}, error:{}", platformOrderId, shippingFee, e);
                resultMap.put("code", "500");
                resultMap.put("msg", "update order shippingFee fail");
            }
        }
    }

    /**
     * 订单状态推送
     */
    private void orderStatus(JSONObject dataJson, Map<String, String> resultMap) {
        String platformOrderId = dataJson.getString("taskid");
        //订单状态：6订单出库，7订单妥投，8订单拒收，10订单取消
        String orderStatus = dataJson.getString("orderStatus");
        Map<String, Object> param = new HashMap<>();
        param.put("platforms_order_id", platformOrderId);
        param.put("platforms", Constants.PLATFORMS_DDKY);
        Map<String, Object> orderInfoMap = baseService.selectOrderInfoByMap(param);
        if (!"ok".equals(orderInfoMap.get("result"))) {
            log.error("叮当快药，订单状态推送，根据platformOrderId:{} 查不到订单数据", platformOrderId);
            resultMap.put("code", "500");
            resultMap.put("msg", "cannot find order data");
            return;
        }
        String client = (String) orderInfoMap.get("client");
        String stoCode = (String) orderInfoMap.get("sto_code");
        String orderId = (String) orderInfoMap.get("order_id");

        if (DdkyOrderStatus.ORDER_CANCEL.getStatus().equals(orderStatus)) {
            orderInfoMap.put("status", "4");//订单取消
            orderInfoMap.put("action", "4");//用户申请退单
            orderInfoMap.put("last_action", "4");//之所以加这个，是为了记录到本系统订单系统
            orderInfoMap.put("notice_message", "cancel");
            String resultForXB = baseService.updateOrderInfoWithMap(orderInfoMap);
            if (resultForXB.equals("ok")) {
                String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                orderInfoMap.put("storeLogo", storeLogo);
                takeAwayService.pushToStoreMq(client, stoCode, "O2O", "cancel", orderInfoMap);
                log.info("叮当快药，推送取消订单，orderId:{},client:{},stoCode:{},storeLogo:{}", orderId, client, stoCode, storeLogo);
                resultMap.put("code", "0");
                resultMap.put("msg", "success");
            } else {
                resultMap.put("code", "500");
                resultMap.put("msg", "update order data fail");
            }
        } else if (DdkyOrderStatus.ORDER_DELIVERED.getStatus().equals(orderStatus)) {
            orderInfoMap.put("shipper_name", dataJson.getString("deliverName"));
            orderInfoMap.put("shipper_phone", dataJson.getString("deliverMobile"));
            orderInfoMap.put("recipient_name", dataJson.getString("receiverName"));
            orderInfoMap.put("real_phone_number", dataJson.getString("receiverMobile"));
            orderInfoMap.put("status", orderStatus);
            String resultForXB = baseService.updateOrderInfoWithMap(orderInfoMap);
            if (resultForXB.equals("ok")) {
                log.info("叮当快药，更新订单状态信息成功，orderId:{}, orderStatus:{}", orderId, orderStatus);
                resultMap.put("code", "0");
                resultMap.put("msg", "success");
            } else {
                log.info("叮当快药，更新订单状态信息失败，orderId:{}, orderStatus:{}", orderId, orderStatus);
                resultMap.put("code", "500");
                resultMap.put("msg", "update order orderStatus fail");
            }
        } else if (DdkyOrderStatus.ORDER_REJECT.getStatus().equals(orderStatus) || DdkyOrderStatus.ORDER_CHECKOUT.getStatus().equals(orderStatus)) {
            orderStatus = DdkyOrderStatus.ORDER_CHECKOUT.getStatus().equals(orderStatus) ? "9" : orderStatus;
            orderInfoMap.put("status", orderStatus);
            String resultForXB = baseService.updateOrderInfoWithMap(orderInfoMap);
            if (resultForXB.equals("ok")) {
                log.info("叮当快药，更新订单状态信息成功，orderId:{}, orderStatus:{}", orderId, orderStatus);
                resultMap.put("code", "0");
                resultMap.put("msg", "success");
            } else {
                log.info("叮当快药，更新订单状态信息失败，orderId:{}, orderStatus:{}", orderId, orderStatus);
                resultMap.put("code", "500");
                resultMap.put("msg", "update order orderStatus fail");
            }
        }
    }

    /**
     * 全量同步商品
     * proId 有值即为单个商品同步
     */
    @GetMapping("goodsSync")
    @ResponseBody
    public String goodsSync(String client, String stoCode, String proId) {
        return takeAwayService.goodsSync(client, stoCode, proId);
    }

}
