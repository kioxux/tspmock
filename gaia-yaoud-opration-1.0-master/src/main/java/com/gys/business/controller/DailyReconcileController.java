//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.DailyReconcileService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/dailyReconcile/"})
public class DailyReconcileController extends BaseController {
    @Autowired
    private DailyReconcileService dailyReconcileService;

    public DailyReconcileController() {
    }

    @ApiOperation(value = "日结对账信息查询",notes = "日结对账信息查询",response = DailyReconcileOutData.class)
    @PostMapping({"/dailyReconcileInfo"})
    public JsonResult dailyReconcileInfo(HttpServletRequest request,@RequestBody DailyReconcileInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGpdhBrId(userInfo.getDepId());
//        inData.setClientId("21020006");
//        inData.setGpdhBrId("1000000001");
        return JsonResult.success(this.dailyReconcileService.getDailyReconcile(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "日结对账详情查询",notes = "日结对账详情查询",response = DailyReconcileDetailOutData.class)
    @PostMapping({"/dailyReconcileDetailInfo"})
    public JsonResult dailyReconcileDetailInfo(HttpServletRequest request,@RequestBody DailyReconcileOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGpdhBrId(userInfo.getDepId());
//        inData.setClientId("21020006");
        return JsonResult.success(this.dailyReconcileService.getDailyReconcileDetailList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "新增日结对账",notes = "新增日结对账")
    @PostMapping({"/insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody DailyReconcileOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
//        inData.setClientId("21020001");
        this.dailyReconcileService.insert(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "日结对账审核",notes = "日结对账审核")
    @PostMapping({"/approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody DailyReconcileOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.dailyReconcileService.approve(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

    @ApiOperation(value = "收款列表查询",notes = "收款列表查询",response = SaleReturnOutData.class)
    @PostMapping({"/saleInfoList"})
    public JsonResult getSaleInfoList(@RequestBody DailyReconcileInData inData) {
//        inData.setClientId("21020001");
//        inData.setGpdhBrId("1000001001");
        return JsonResult.success(this.dailyReconcileService.getSaleInfoList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "储蓄卡充值列表查询",notes = "储蓄卡充值列表查询",response = RechargeCardPaycheckOutData.class)
    @PostMapping({"/rechargeInfoList"})
    public JsonResult getRechargeInfoList(@RequestBody DailyReconcileInData inData) {
        return JsonResult.success(this.dailyReconcileService.getRechargeInfoList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "门店信息查询",notes = "门店信息查询",response = StoreOutData.class)
    @PostMapping({"/storeData"})
    public JsonResult getStoreData(HttpServletRequest request ,@RequestBody StoreInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.dailyReconcileService.getStoreData(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "自动生成日结对账",notes = "自动生成日结对账")
    @PostMapping({"/checkDailyReconcileInfo"})
    public JsonResult checkDailyReconcileInfo(HttpServletRequest request, @RequestBody DailyReconcileInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGpdhBrId(userInfo.getDepId());
        this.dailyReconcileService.checkDailyReconcileInfo(inData);
        return JsonResult.success("", "自动生成日结对账成功！");
    }

    @ApiOperation(value = "日结对账信息查询(整合)",notes = "日结对账信息查询(整合)",response = DailyReconcileOutData.class)
    @PostMapping({"/querydailyReconcileInfo"})
    public JsonResult querydailyReconcileInfo(HttpServletRequest request,@RequestBody DailyReconcileInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGpdhBrId(userInfo.getDepId());
//        inData.setClientId("21020001");
//        inData.setGpdhBrId("1000001001");
        return JsonResult.success(this.dailyReconcileService.queryDailyReconcile(inData), "提示：获取数据成功！");
    }

//    @ApiOperation(value = "收款及储蓄卡充值列表查询",notes = "收款及储蓄卡充值列表查询")
//    @PostMapping({"/getSaleAndRechargeInfoList"})
//    public JsonResult getSaleAndRechargeInfoList(HttpServletRequest request,@RequestBody DailyReconcileInData inData) {
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClientId(userInfo.getClient());
//        inData.setGpdhBrId(userInfo.getDepId());
////        inData.setClientId("21020001");
////        inData.setGpdhBrId("1000001001");
//        return JsonResult.success(this.dailyReconcileService.getSaleAndRechargeInfoList(inData), "提示：获取数据成功！");
//    }

    @ApiOperation(value = "收款及储蓄卡充值列表查询",notes = "收款及储蓄卡充值列表查询",response = DailyReconcileNewOutData.class)
    @PostMapping({"/getDailyInfoList"})
    public JsonResult getDailyInfoList(HttpServletRequest request, @RequestBody DailyReconcileInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGpdhBrId(userInfo.getDepId());
//        inData.setClientId("21020001");
//        inData.setGpdhBrId("1000001001");
        return JsonResult.success(this.dailyReconcileService.getDailyInfoList(inData), "提示：获取数据成功！");
    }
}
