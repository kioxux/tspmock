package com.gys.business.controller;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.gys.business.service.IGaiaBillOfApService;
import com.gys.common.redis.RedisManager;
import com.gys.job.SupplierBillOfApJob;
import com.gys.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.gys.business.service.IGaiaFiApBalanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import com.gys.common.base.BaseController;

/**
 * <p>
 * 供应商应付期末余额 前端控制器
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Slf4j
@RestController
@Api(tags = "供应商应付期末余额接口")
@RequestMapping("/fiApBalance")
public class GaiaFiApBalanceController extends BaseController {

    @Autowired
    RedisManager redisManager;
    @Autowired
    private IGaiaFiApBalanceService fiApBalanceService;
    @Autowired
    private IGaiaBillOfApService service;
    @Autowired
    private SupplierBillOfApJob job;

    @GetMapping("/monthlySettlement")
    @ApiOperation(value = "每月结算供应商应付期末余额", notes = "每月结算供应商应付期末余额")
    public JsonResult buildInit(HttpServletRequest request,String dateTime) {
        log.info("=======开始统计上一天的供应商应付定时任务===========");
        //抢占锁
        boolean possession = redisManager.getLock("fi_ap_balance_monthly_settlement_lock", 1);
        if (possession) {
            log.info("每月结算供应商应付期末余额===执行业务逻辑");
             //LocalDate date = LocalDateTimeUtil.parseDate(dateTime);
            fiApBalanceService.monthlySettlement(DateUtil.getPreMonth(LocalDate.now()));
        }
        return JsonResult.success();
    }

     @GetMapping("test")
    public void test() throws ParseException {
         job.dailyInsert();

     }

}

