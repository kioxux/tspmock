//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.ClearBoxTipService;
import com.gys.business.service.data.GetAddBoxInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/clearBoxTip/"})
public class ClearBoxTipController extends BaseController {
    @Autowired
    private ClearBoxTipService clearBoxTipService;

    public ClearBoxTipController() {
    }

    @PostMapping({"selectList"})
    public JsonResult selectList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.clearBoxTipService.selectList(userInfo), "提示：获取数据成功！");
    }

    @PostMapping({"getAddBoxInfo"})
    public JsonResult getAddBoxInfo(HttpServletRequest request, @RequestBody GetAddBoxInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.clearBoxTipService.getAddBoxInfo(inData), "提示：获取数据成功！");
    }
}
