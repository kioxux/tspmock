package com.gys.business.controller;

import com.gys.business.service.GaiaStoreOutSuggestionRelationService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 门店调出建议关系表(GaiaStoreOutSuggestionRelation)表控制层
 *
 * @author XIAO-ZY
 * @since 2021-10-28 10:54:23
 */
@RestController
@RequestMapping("gaiaStoreOutSuggestionRelation")
public class GaiaStoreOutSuggestionRelationController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaStoreOutSuggestionRelationService gaiaStoreOutSuggestionRelationService;

}

