package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.gys.business.mapper.entity.GaiaLiquidation;
import com.gys.business.service.GaiaLiquidationService;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GaiaBillOfArInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 医保清算表(GaiaLiquidation)表控制层
 *
 * @author makejava
 * @since 2021-12-29 15:00:25
 */
@RestController
@RequestMapping("gaiaLiquidation")
@Slf4j
public class GaiaLiquidationController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaLiquidationService gaiaLiquidationService;

    @PostMapping({"commit"})
    @FrequencyValid
    public JsonResult commit(HttpServletRequest request, @RequestBody GaiaLiquidation inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<医保清算><提交><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaLiquidationService.commit(loginUser,inData), "提示：查询数据成功！");
    }

    @FrequencyValid
    @PostMapping({"revoke"})
    public JsonResult revoke(HttpServletRequest request, @RequestBody GaiaLiquidation inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<医保清算><申请撤销><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaLiquidationService.revoke(loginUser,inData), "提示：查询数据成功！");
    }

    @FrequencyValid
    @PostMapping({"getList"})
    public JsonResult getList(HttpServletRequest request, @RequestBody GaiaLiquidation inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        log.info(String.format("<医保清算><查询><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaLiquidationService.getList(loginUser,inData), "提示：查询数据成功！");
    }
}

