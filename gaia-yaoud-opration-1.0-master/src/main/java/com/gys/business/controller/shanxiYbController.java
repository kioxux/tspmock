package com.gys.business.controller;

import com.gys.business.service.ShanxiYbService;
import com.gys.business.service.data.CommenData.InData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 山西医保
 * @author 胡鑫鑫
 */
@RestController
@RequestMapping({"shanxiYb"})
public class shanxiYbController extends BaseController {

    @Autowired
    private ShanxiYbService shanxiYbService;

    @PostMapping({"saleCountReport"})
    public JsonResult saleCountReport(HttpServletRequest request,@RequestBody InData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.shanxiYbService.saleCountReport(inData,userInfo), "提示：获取数据成功！");
    }
}
