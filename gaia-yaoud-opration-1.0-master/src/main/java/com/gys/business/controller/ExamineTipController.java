//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.ExamineTipService;
import com.gys.business.service.data.ExamineTipOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/examineTip/"})
public class ExamineTipController extends BaseController {
    @Autowired
    private ExamineTipService examineTipService;

    public ExamineTipController() {
    }

    @PostMapping({"/list"})
    public JsonResult deviceInfoList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.examineTipService.list(userInfo), "提示：获取数据成功！");
    }

    @PostMapping({"/getClearBoxInfo"})
    public JsonResult getClearBoxInfo(HttpServletRequest request, @RequestBody ExamineTipOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.examineTipService.getClearBoxInfo(inData), "提示：获取数据成功！");
    }
}
