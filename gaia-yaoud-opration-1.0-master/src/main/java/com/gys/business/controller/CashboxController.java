//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.CashboxService;
import com.gys.business.service.data.CashboxInData;
import com.gys.business.service.data.CashboxOutData;
import com.gys.business.service.data.DailyReconcileInData;
import com.gys.business.service.data.GetUserOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping({"/cashbox/"})
public class CashboxController extends BaseController {
    @Autowired
    private CashboxService dashboxService;

    public CashboxController() {
    }

    @ApiOperation(value = "钱箱密码验证",notes = "钱箱密码验证")
    @PostMapping({"/checkPassword"})
    public JsonResult checkPassword(HttpServletRequest request,@RequestBody CashboxInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());       //门店
        inData.setGpdhBrId(userInfo.getDepId());        //
        Integer integer = this.dashboxService.checkPassword(inData);
        return JsonResult.success(integer, "提示：获取数据成功！");
    }

    @ApiOperation(value = "钱箱保存",notes = "钱箱保存")
    @PostMapping({"/save"})
    public JsonResult save(HttpServletRequest request,@RequestBody CashboxInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());       //加盟商
        inData.setGpdhBrId(userInfo.getDepId());        //门店
        try{
            this.dashboxService.save(inData);
            return JsonResult.success(true, "提示：数据保存成功！");
        }catch (Exception e){
            return JsonResult.error(501, "提示：数据保存失败！");
        }
    }


    @ApiOperation(value = "获取上一班次数据",notes = "获取上一班次数据")
    @PostMapping({"/thePreviousData"})
    public JsonResult thePreviousData(HttpServletRequest request,@RequestBody CashboxInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());       //门店id
        inData.setGpdhBrId(userInfo.getDepId());    //店号id
        try{
            CashboxOutData cashboxOutData = this.dashboxService.thePreviousData(inData);
            return JsonResult.success(cashboxOutData, "提示：数据获取成功！");
        }catch (Exception e){
            return JsonResult.error(501, "提示：数据获取失败！");
        }
    }

    @ApiOperation(value = "获取当前钱箱计算后数据",notes = "获取当前钱箱计算后数据")
    @PostMapping({"/fromDate"})
    public JsonResult getThisFrom(HttpServletRequest request , @RequestBody CashboxInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());       //门店id
        inData.setGpdhBrId(userInfo.getDepId());    //店号id
        try{
            CashboxOutData cashboxOutData = this.dashboxService.getThisFrom(inData ,userInfo );

            return JsonResult.success(cashboxOutData, "提示：数据获取成功！");
        }catch (Exception e){
            return JsonResult.error(501, "提示：数据获取失败！");
        }
    }


    @ApiOperation(value = "获取时间区间的现金来源",notes = "获取时间区间的现金来源")
    @PostMapping({"/getCashSource"})
    public JsonResult getCashSource(HttpServletRequest request , @RequestBody CashboxInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());       //门店id
        inData.setGpdhBrId(userInfo.getDepId());    //店号id
        try{
            CashboxOutData cashboxOutData = this.dashboxService.getCashSource(inData ,userInfo );
            return JsonResult.success(cashboxOutData, "提示：数据获取成功！");
        }catch (Exception e){
            return JsonResult.error(501, "提示：数据获取失败！");
        }
    }

    @ApiOperation(value = "根据日期获取记录列表",notes = "根据日期获取记录列表")
    @PostMapping({"/queryList"})
    public JsonResult queryList(HttpServletRequest request , @RequestBody CashboxInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setGpdhBrId(userInfo.getDepId());    //店号id
        inData.setClientId(userInfo.getClient());    //店号id


        try{
            List<CashboxOutData> cashboxOutData = this.dashboxService.queryList(inData);
            return JsonResult.success(cashboxOutData, "提示：数据获取成功！");
        }catch (Exception e){
            return JsonResult.error(501, "提示：数据获取失败！");
        }
    }


    @ApiOperation(value = "根据当前门店店号编号获取用户列表",notes = "根据当前门店店号编号获取用户列表")
    @PostMapping({"/queryUserList"})
    public JsonResult queryUserList(HttpServletRequest request , @RequestBody CashboxInData inData ){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());       //门店id
        inData.setGpdhBrId(userInfo.getDepId());    //店号id
        try{
            List<GetUserOutData> getUserOutData = this.dashboxService.queryUserList(inData);
            return JsonResult.success(getUserOutData, "提示：数据获取成功！");
        }catch (Exception e){
            return JsonResult.error(501, "提示：数据获取失败！");
        }
    }

}
