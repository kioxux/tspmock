//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.PromoteService;
import com.gys.business.service.data.CheckPromLimitInData;
import com.gys.business.service.data.PromoteInData;
import com.gys.business.service.data.PromotionMethodInData;
import com.gys.business.service.data.ReSummaryPromoteIndata;
import com.gys.common.base.BaseController;
import com.gys.common.base.ExportExcel;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping({"/promote/"})
public class PromoteController extends BaseController {
    @Autowired
    private PromoteService promoteService;

    public PromoteController() {
    }

    @PostMapping({"getList"})
    public JsonResult getList(HttpServletRequest request, @RequestBody PromoteInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.promoteService.getList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"update"})
    public JsonResult update(HttpServletRequest request, @RequestBody PromoteInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.promoteService.update(inData, userInfo);
        return JsonResult.success((Object)null, "提示：更新成功！");
    }

    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request,@Valid @RequestBody PromoteInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.promoteService.insert(inData, userInfo);
        return JsonResult.success((Object)null, "提示：保存成功！");
    }

    @PostMapping({"detail"})
    public JsonResult detail(HttpServletRequest request, @RequestBody PromoteInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsphBrId(userInfo.getDepId());
        return JsonResult.success(this.promoteService.detail(inData), "提示：保存成功！");
    }

    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody PromoteInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.promoteService.approve(inData, userInfo);
        return JsonResult.success((Object)null, "提示：保存成功！");
    }

    @PostMapping({"getParam"})
    public JsonResult getParam(HttpServletRequest request, @RequestBody PromoteInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.promoteService.getParam(inData, userInfo), "提示：保存成功！");
    }

    @GetMapping({"exportExcel"})
    public void exportExcel(HttpServletResponse res, HttpServletRequest request) {
        String fileName = request.getParameter("fileName");
        String filePath = request.getParameter("filePath");
        ExportExcel.exportFile(res, fileName, filePath);
    }

    @PostMapping({"reSummaryGiftPromotion"})
    public JsonResult reSummaryGiftPromotion(HttpServletRequest request, @RequestBody ReSummaryPromoteIndata reSummaryPromoteIndata) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.promoteService.reSummaryGiftPromotion(userInfo.getClient(),userInfo.getDepId(),reSummaryPromoteIndata), "提示：保存成功！");
    }

    @PostMapping({"queryGiftpromotion"})
    public JsonResult queryGiftpromotion(HttpServletRequest request, @RequestBody PromotionMethodInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.promoteService.queryGiftpromotion(inData), "提示：保存成功！");
    }

    @PostMapping({"checkPromLimit"})
    public JsonResult checkPromLimit(HttpServletRequest request, @RequestBody CheckPromLimitInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.promoteService.checkPromLimit(inData), "提示：保存成功！");
    }

}
