//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.CardLoseOrChangeQueryService;
import com.gys.business.service.data.GetCardLoseOrChangeInData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/cardLoseOrChangeQuery/"})
public class CardLoseOrChangeQueryController {
    @Autowired
    private CardLoseOrChangeQueryService cardLoseOrChangeQueryService;

    public CardLoseOrChangeQueryController() {
    }

    @PostMapping({"queryTableList"})
    public JsonResult queryTableList(HttpServletRequest request, @RequestBody GetCardLoseOrChangeInData inData) {
        return JsonResult.success(this.cardLoseOrChangeQueryService.queryTableList(inData), "提示：获取数据成功！");
    }
}
