package com.gys.business.controller;


import com.gys.business.mapper.entity.StockMouth;
import com.gys.business.service.StockMouthService;
import com.gys.business.service.data.StockMouthCaculateCondition;
import com.gys.common.base.BaseController;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 月度库存表
 * 前端控制器
 * </p>
 *
 * @author sy
 * @since 2021-07-13
 */
@Api(tags = "月度库存审计", value = "flynn")
@RestController
@RequestMapping("/stockMouth")
public class StockMouthController extends BaseController {

    @Autowired
    private StockMouthService stockMouthService;

    @ApiOperation(value = "库存审计客户地点联动接口", response = StockMouth.class)
    @GetMapping("/init")
    public JsonResult clientSite() {
        return JsonResult.success(this.stockMouthService.clientSite(), "提示：获取数据成功！");
    }


    @ApiOperation(value = "点击库存推算", response = StockMouth.class)
    @PostMapping("/calculation")
    public JsonResult queryStoLeader(@RequestBody StockMouthCaculateCondition condition) {
        this.stockMouthService.caculateByCondition(condition);
        return JsonResult.success("", "提示：推算成功！");
    }


    @ApiOperation(value = "月度库存查询", response = StockMouth.class)
    @PostMapping("/list")
    public JsonResult list(@RequestBody StockMouthCaculateCondition condition) {
        return JsonResult.success(this.stockMouthService.getStockMouthListPage(condition), "提示：获取数据成功！");
    }

    @ApiOperation(value = "月度库存查询导出", response = StockMouth.class)
    @PostMapping("/export")
    public JsonResult export(@RequestBody StockMouthCaculateCondition condition) {
        return JsonResult.success(this.stockMouthService.export(condition), "提示：导出数据成功！");
    }

}

