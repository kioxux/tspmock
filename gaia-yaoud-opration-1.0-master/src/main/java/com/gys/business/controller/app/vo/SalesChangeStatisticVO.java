package com.gys.business.controller.app.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author jiht
 * @Description 成分销售变化TOP排名统计出参
 * @Date 2021/12/3 14:14
 **/
@Data
@ApiModel
public class SalesChangeStatisticVO {
    @ApiModelProperty(value = "成分名称", example = "A190503药品-滋补养生用药-其他补气养血", position = 0)
    private String componentName;
    @ApiModelProperty(value = "动销品项", example = "2", position = 1)
    private Integer movableSalesItems;
    @ApiModelProperty(value = "销售额", example = "99.99", position = 2)
    private BigDecimal amt;
    @ApiModelProperty(value = "毛利额", example = "49.99", position = 3)
    private BigDecimal grossProfit;
    @ApiModelProperty(value = "毛利率", example = "0.4976", position = 4)
    private BigDecimal grossMargin;
    @ApiModelProperty(value = "销售额变化", example = "7.29", position = 5)
    private BigDecimal amtChange;
    @ApiModelProperty(value = "毛利额变化", example = "5.33", position = 6)
    private BigDecimal grossProfitChange;
    @ApiModelProperty(value = "商品自编码", example = "0101001", position = 7)
    private String proSelfCode;
    @ApiModelProperty(value = "交易日期", example = "20211012", position = 8)
    private String gssdDate;

    public SalesChangeStatisticVO() {
        this.movableSalesItems = 0;
        this.amt = BigDecimal.ZERO;
        this.grossProfit = BigDecimal.ZERO;
        this.grossMargin = BigDecimal.ZERO;
        this.amtChange = BigDecimal.ZERO;
        this.grossProfitChange = BigDecimal.ZERO;
    }
}
