package com.gys.business.controller.takeAway;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.service.data.GetQueryProductInData;
import com.gys.business.service.data.ProductBusinessEsData;
import com.gys.common.base.BaseController;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author 陈浩
 */
@RestController
@RequestMapping({"/es/"})
@Slf4j
public class EsController extends BaseController {
    @Resource
    private RestHighLevelClient restHighLevelClient;

    @PostMapping({"/search"})
    public JsonResult search(HttpServletRequest request, @RequestBody @ApiParam GetQueryProductInData inData) throws Exception{
        List<ProductBusinessEsData> resultList = Lists.newArrayList();
        SearchRequest searchRequest = new SearchRequest("product");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        MatchQueryBuilder queryBuilder = QueryBuilders.matchQuery("proSelfCode", inData.getNameOrCode());
        searchSourceBuilder.query(queryBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response2 = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        log.info(JSON.toJSONString(response2.getHits().getHits()));

        MatchQueryBuilder matchPhraseQueryBuilder = QueryBuilders.matchQuery("proPym", inData.getNameOrCode());
        MatchQueryBuilder matchPhraseQueryBuilder2 = QueryBuilders.matchQuery("proSelfCode", inData.getNameOrCode());
        MatchQueryBuilder matchPhraseQueryBuilder3 = QueryBuilders.matchQuery("proBarcode", inData.getNameOrCode());
        MatchQueryBuilder matchPhraseQueryBuilder4 = QueryBuilders.matchQuery("proBarcode2", inData.getNameOrCode());
        MatchQueryBuilder matchPhraseQueryBuilder5 = QueryBuilders.matchQuery("proCommonname", inData.getNameOrCode());
        MatchQueryBuilder matchPhraseQueryBuilder6 = QueryBuilders.matchQuery("proName", inData.getNameOrCode());
        MatchQueryBuilder matchPhraseQueryBuilder7 = QueryBuilders.matchQuery("proFactoryName", inData.getNameOrCode());
        MatchQueryBuilder matchPhraseQueryBuilder8 = QueryBuilders.matchQuery("proRegisterNo", inData.getNameOrCode());

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.should(matchPhraseQueryBuilder);
        boolQueryBuilder.should(matchPhraseQueryBuilder2);
        boolQueryBuilder.should(matchPhraseQueryBuilder3);
        boolQueryBuilder.should(matchPhraseQueryBuilder4);
        boolQueryBuilder.should(matchPhraseQueryBuilder5);
        boolQueryBuilder.should(matchPhraseQueryBuilder6);
        boolQueryBuilder.should(matchPhraseQueryBuilder7);
        boolQueryBuilder.should(matchPhraseQueryBuilder8);
        searchSourceBuilder.query(boolQueryBuilder);
        searchSourceBuilder.from(0).size(20);
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        for(SearchHit searchHit : searchResponse.getHits().getHits()){
            Map<String, Object> sourceAsMap = searchHit.getSourceAsMap();
            ProductBusinessEsData productBusiness = new ProductBusinessEsData();
            productBusiness.setClient(StrUtil.toString(sourceAsMap.get("client")));
            productBusiness.setProSite(StrUtil.toString(sourceAsMap.get("proSite")));
            productBusiness.setProClassName(StrUtil.toString(sourceAsMap.get("proClassName")));
            productBusiness.setProPym(StrUtil.toString(sourceAsMap.get("proPym")));
            productBusiness.setProSelfCode(StrUtil.toString(sourceAsMap.get("proSelfCode")));
            productBusiness.setProBarcode(StrUtil.toString(sourceAsMap.get("proBarcode")));
            productBusiness.setProBarcode2(StrUtil.toString(sourceAsMap.get("proBarcode2")));
            productBusiness.setProCommonname(StrUtil.toString(sourceAsMap.get("proCommonname")));
            productBusiness.setProName(StrUtil.toString(sourceAsMap.get("proName")));
            productBusiness.setProFactoryName(StrUtil.toString(sourceAsMap.get("proFactoryName")));
            productBusiness.setProRegisterNo(StrUtil.toString(sourceAsMap.get("proRegisterNo")));

            resultList.add(productBusiness);
        }

        return JsonResult.success(resultList, "提示：获取数据成功！");
    }
}
