
package com.gys.business.controller;

import com.gys.business.service.CashPaymentService;
import com.gys.business.service.ShiftSalesService;
import com.gys.business.service.data.shiftSalesInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "班次销售额查询")
@RequestMapping({"/shiftSales/"})
public class ShiftSalesController extends BaseController {
    @Autowired
    private ShiftSalesService shiftSalesService;

    @Autowired
    private CashPaymentService cashPaymentService;

    @ApiOperation(value = "列表条件查询")
    @PostMapping({"/shiftSalesList"})
    public JsonResult getShiftSalesList(HttpServletRequest request,@Valid @RequestBody shiftSalesInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsdhBrId(userInfo.getDepId());
        return JsonResult.success(this.shiftSalesService.getShiftSalesList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "收银员")
    @PostMapping({"/empList"})
    public JsonResult getEmpList(HttpServletRequest request) {
        GetLoginOutData inData = new GetLoginOutData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setDepId(userInfo.getDepId());
        return JsonResult.success(this.cashPaymentService.getEmpList(inData), "提示：获取数据成功！");
    }
}
