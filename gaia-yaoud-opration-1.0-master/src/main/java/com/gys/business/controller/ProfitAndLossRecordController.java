package com.gys.business.controller;

import com.gys.business.service.ProfitAndLossRecordService;
import com.gys.business.service.data.ProfitAndLossRecordInData;
import com.gys.business.service.data.ProfitAndLossRecordOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 报损报溢报表
 *
 * @author xiaoyuan on 2020/9/9
 */
@Api(tags = "损溢记录")
@RestController
@RequestMapping({"/profitAndLossRecord/"})
public class ProfitAndLossRecordController extends BaseController {
    @Resource
    private ProfitAndLossRecordService recordService;

    @ApiOperation(value = "列表查询" ,response = ProfitAndLossRecordOutData.class)
    @PostMapping("/list")
    public JsonResult list(HttpServletRequest request, @Valid @RequestBody ProfitAndLossRecordInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        if(StringUtils.isEmpty(inData.getBrId())){
            inData.setBrId(userInfo.getDepId());
        }

        return JsonResult.success(this.recordService.conditionQuery(inData),"success");
    }
}
