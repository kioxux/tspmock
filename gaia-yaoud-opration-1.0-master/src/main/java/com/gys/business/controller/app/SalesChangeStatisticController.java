package com.gys.business.controller.app;

import com.gys.business.controller.app.form.SalesChangeStatisticForm;
import com.gys.business.service.appservice.SalesChangeStatisticService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@Api(tags = "成分销售变化TOP排名")
@RestController
@RequestMapping("/app/componentSalesChange/")
public class SalesChangeStatisticController extends BaseController {

    @Autowired
    private SalesChangeStatisticService salesChangeStatisticService;

    @PostMapping("salesChangeStatistic")
    public JsonResult salesChangeStatistic(HttpServletRequest request, @RequestBody SalesChangeStatisticForm inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.salesChangeStatisticService.salesChangeStatistic(inData), "提示：获取数据成功！");
    }
}
