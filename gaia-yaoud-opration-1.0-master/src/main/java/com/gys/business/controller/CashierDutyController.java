//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.CashierDutyService;
import com.gys.business.service.data.GetLoginStoreInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/cashierDuty/"})
public class CashierDutyController extends BaseController {
    @Autowired
    private CashierDutyService cashierDutyService;

    public CashierDutyController() {
    }

    @PostMapping({"checkChange"})
    public JsonResult checkChange(HttpServletRequest request, @RequestBody GetLoginStoreInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.cashierDutyService.checkChange(inData), "提示：获取数据成功！");
    }
}
