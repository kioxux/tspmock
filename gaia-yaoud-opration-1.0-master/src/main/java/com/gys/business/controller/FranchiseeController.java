//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.FranchiseeService;
import com.gys.business.service.data.FranchiseeInData;
import com.gys.business.service.data.FranchiseeOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
@Api(tags = "加盟商")
@RestController
@RequestMapping({"/franchisee/"})
public class FranchiseeController extends BaseController {
    @Autowired
    private FranchiseeService franchiseeService;

    public FranchiseeController() {
    }

    @PostMapping({"getFranchiseeById"})
    public JsonResult getFranchiseeById(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
        return JsonResult.success(this.franchiseeService.getFranchiseeById(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getFranchisee"})
    @ApiOperation(value = "获取加盟商列表" , response = FranchiseeOutData.class)
    public JsonResult getFranchisee(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
        return JsonResult.success(this.franchiseeService.getFranchisee(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getFranchiseeByName"})
    @ApiOperation(value = "获取加盟商列表" , response = FranchiseeOutData.class)
    public JsonResult getFranchiseeByName(HttpServletRequest request, @RequestBody FranchiseeInData inData) {
        return JsonResult.success(this.franchiseeService.getFranchiseeByName(inData), "提示：获取数据成功！");
    }
}
