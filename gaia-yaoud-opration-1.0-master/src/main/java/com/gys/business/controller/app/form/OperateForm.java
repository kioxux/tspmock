package com.gys.business.controller.app.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/10 17:35
 **/
@Data
public class OperateForm {
    @ApiModelProperty(value = "加盟商",hidden = true)
    private String client;
    @ApiModelProperty(value = "缺断货流水号", example = "DS3454102")
    private String voucherId;
    @ApiModelProperty(value = "整体排序：1-按照销售额倒序 2-按店号升序")
    private Integer sortMode;
    @ApiModelProperty(value = "筛选条件-排序条件：1-销售额（默认） 2-毛利额", example = "1")
    private Integer sortCond;
    @ApiModelProperty(value = "筛选条件-排序方式：1-倒序（默认）2-升序", example = "1")
    private Integer sortType;
    @ApiModelProperty(value = "筛选条件-查询数量", example = "10")
    private Integer queryNum;
}
