package com.gys.business.controller.takeAway;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.data.*;
import com.gys.business.service.data.takeaway.*;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.base.BaseController;
import com.gys.common.data.*;
import com.gys.util.takeAway.ddky.DdkyRefundBean;
import com.qcloud.cos.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

@Api(tags = "外卖订单", description = "/takeAway")
@Controller
@RequestMapping({"/takeAway/"})
public class TakeAwayController extends BaseController {
    private BaseService baseService;
    public BaseService getBaseService() {
        return baseService;
    }
    private static final Logger logger = LoggerFactory.getLogger(TakeAwayController.class);
    @Autowired
    public void setBaseService(BaseService baseService) {
        this.baseService = baseService;
    }
    @Resource
    private TakeAwayService takeAwayService;

    @ApiOperation(value = "查询外卖订单" ,response = TakeAwayOrderOutData.class)
    @ResponseBody
    @PostMapping({"getOrderList"})
    public JsonResult getOrderList(HttpServletRequest request, @ApiParam(value = "查询条件", required = true)@RequestBody GetTakeAwayInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.takeAwayService.getOrderList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询商品默认选择的批号" ,response = BaseOrderInfo.class)
    @ResponseBody
    @PostMapping({"getProStockBatch"})
    public JsonResult getProStockBatch(HttpServletRequest request, @ApiParam(value = "查询条件", required = true)@RequestBody List<TakeAwayOrderDetailOutData> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return null;
        //return JsonResult.success(this.takeAwayService.getProStockBatch(inData,userInfo.getClient(),userInfo.getDepId()), "提示：获取数据成功！");
    }

    //发货
    @ApiOperation(value = "发货")
    @ResponseBody
    @PostMapping({"send"})
    public JsonResult send(HttpServletRequest request, @ApiParam(value = "订单编号", required = true) @RequestBody TakeAwayOrderBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.takeAwayService.sendAction(bean.getOrderNo(), userInfo.getClient(), userInfo.getDepId()), "提示：获取数据成功！");
    }
    //退单
    @ApiOperation(value = "退货" ,response = BaseOrderInfo.class)
    @ResponseBody
    @PostMapping({"returnOrder"})
    public JsonResult returnOrder(HttpServletRequest request, @ApiParam(value = "查询条件", required = true) @RequestBody TakeAwayOrderBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return takeAwayService.returnOrder(bean, userInfo.getClient(), userInfo.getDepId());
    }
    //日志
    @ApiOperation(value = "log" ,response = BaseOrderInfo.class)
    @ResponseBody
    @PostMapping({"log"})
    public JsonResult log(HttpServletRequest request, @ApiParam(value = "查询条件", required = true)@RequestBody TakeAwayOrderBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return takeAwayService.log(bean, userInfo.getClient(), userInfo.getDepId());
    }

    //同步外卖库存
    @ApiOperation("同步外卖库存")
    @ResponseBody
    @PostMapping("syncStock")
    public JsonResult syncStock(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return takeAwayService.syncStock(userInfo.getClient(), userInfo.getDepId());
    }

    @ApiOperation("同步支付时间")
    @ResponseBody
    @PostMapping("syncPayTime")
    public JsonResult syncPayTime(HttpServletRequest request, @RequestBody OrderPayTimeBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        bean.setStoCode(userInfo.getDepId());
        return takeAwayService.syncPayTime(bean);
    }

    @ApiOperation("查询某订单是否已支付")
    @ResponseBody
    @PostMapping("getPayTime")
    public JsonResult getPayTime(HttpServletRequest request, @RequestBody OrderPayTimeBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        bean.setStoCode(userInfo.getDepId());
        return takeAwayService.getPayTime(bean);
    }

    @ApiOperation("设置主收银")
    @ResponseBody
    @PostMapping("setMainSale")
    public JsonResult setMainSale(HttpServletRequest request, @RequestBody FxMainSaleSetBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        bean.setStoCode(userInfo.getDepId());
        return takeAwayService.setMainSale(bean);
    }

    @ApiOperation("查询主收银")
    @ResponseBody
    @PostMapping("getMainSale")
    public JsonResult getMainSale(HttpServletRequest request, @RequestBody FxMainSaleSetBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        bean.setStoCode(userInfo.getDepId());
        return takeAwayService.getMainSale(bean);
    }

    @ApiOperation("查询当前门店下是否有主收银")
    @ResponseBody
    @PostMapping("getMainSaleByClientAndBrId")
    public JsonResult getMainSaleByClientAndBrId(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
        return JsonResult.success(storeLogo, "提示：获取数据成功！");
    }

    @ApiOperation("查询当前门店外卖提示框是否弹出")
    @ResponseBody
    @PostMapping("getTipPopByClientAndBrId")
    public JsonResult getTipPopByClientAndBrId(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        boolean tipPop = takeAwayService.getTipPopByClientAndBrId(userInfo.getClient(), userInfo.getDepId());
        return JsonResult.success(tipPop, "提示：获取数据成功！");
    }

    @ApiOperation("查询当前门店主收银按钮是否可用")
    @ResponseBody
    @PostMapping("getMainSaleBtn")
    public JsonResult getMainSaleBtn(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String mainSaleBtn = takeAwayService.getMainSaleBtn(userInfo.getClient(), userInfo.getDepId());
        return JsonResult.success(mainSaleBtn, "提示：获取数据成功！");
    }

    @ApiOperation("叮当快药退货")
    @ResponseBody
    @PostMapping("ddkyReturnOrder")
    public JsonResult ddkyReturnOrder(HttpServletRequest request, @RequestBody DdkyRefundBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return takeAwayService.ddkyReturnOrder(userInfo, bean);
    }

    @ApiOperation("查询销售明细数据")
    @ResponseBody
    @PostMapping("getByProdIdAndOrderNo")
    public JsonResult getByProdIdAndOrderNo(HttpServletRequest request, @RequestBody OrderDetailBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        bean.setStoCode(userInfo.getDepId());
        return takeAwayService.getByProdIdAndOrderNo(bean);
    }

    @ApiOperation("查询外卖订单生成的销售单&退货单")
    @ResponseBody
    @PostMapping("getSaleByOrderId")
    public JsonResult getSaleByOrderId(HttpServletRequest request, @RequestBody FxSaleQueryBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        bean.setStoCode(userInfo.getDepId());
        return takeAwayService.getSaleByOrderId(bean);
    }

    @ApiOperation("药联订单-调用订单录入接口")
    @ResponseBody
    @PostMapping("ylOrderInput")
    public JsonResult ylOrderInput(HttpServletRequest request, @RequestBody FxYlOrderInputBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        bean.setStoCode(userInfo.getDepId());
        return takeAwayService.ylOrderInput(bean);
    }

    @ApiOperation(value = "O2O订单汇总查询-web", response = WebOrderDataDto.class)
    @ResponseBody
    @PostMapping("orderQuery")
    public JsonResult orderQuery(HttpServletRequest request, @RequestBody WebOrderQueryBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        return takeAwayService.orderQuery(bean);
    }

    @ApiOperation("O2O订单查询结果-导出")
    @PostMapping("orderQueryOutput")
    public void orderQueryOutput(HttpServletRequest request, HttpServletResponse response, @RequestBody WebOrderQueryBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        takeAwayService.orderQueryOutput(bean, request, response);
    }

    @ApiOperation(value = "O2O订单明细查询-web", response = WebOrderDetailDataDto.class)
    @ResponseBody
    @PostMapping("orderDetailQuery")
    public JsonResult orderDetailQuery(HttpServletRequest request, @RequestBody WebOrderQueryBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        return takeAwayService.orderDetailQuery(bean);
    }

    @ApiOperation("O2O订单明细结果-导出")
    @PostMapping("orderDetailQueryOutput")
    public void orderDetailQueryOutput(HttpServletRequest request, HttpServletResponse response, @RequestBody WebOrderQueryBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        takeAwayService.orderDetailQueryOutput(bean, request, response);
    }




    /**
     * @Description: PTS更新订单状态（更新系统订单状态）
     * @param request 传入JSON数据
     * @return java.util.Map<java.lang.String,java.lang.Object> 回传JSON数据
     * @Author: Li.Deman
     * @Date: 2018/11/21
     */
    @ResponseBody
    @RequestMapping(value = "/orderupdate", method = RequestMethod.POST,produces = { "application/x-www-form-urlencoded;charset=UTF-8" })
    public String orderUpdate(@RequestBody String request){
        try {
            request= URLDecoder.decode(request,"utf-8");
        }catch (Exception ex){
            logger.error(ex.getMessage());
        }
        request=trimEnd(request,"=");
        logger.debug("接收到PTS推送订单更新信息："+request);
        Map<String,Object> paramMap= JSONObject.parseObject(request);
        PtsReturnMessage returnMessage=new PtsReturnMessage();
        //定义接口调用操作日志map对象(PTS调用小宝接口)
        //Map<String, Object> logMap = createOperateLogMap("1",
        //      null,"orderUpdate",
        //      "PTS更新订单状态");
        if(!validApiParamForPtsOrder(paramMap)){
            returnMessage.setMESSAGE("ERROR:API参数异常！");
            returnMessage.setRETURNCODE("F");
            //setOperateLogInfo(logMap,"0","ERROR:API参数异常！");
            logger.info("接收到PTS更新订单状态后返回给PTS数据：" + JSON.toJSONString(returnMessage));
        }else {
            //操作订单信息表，更新订单状态
            String rev= baseService.updateOrderStatus(paramMap);
            if (rev.startsWith("OK")) {
                returnMessage.setMESSAGE("SUCCEED");
                returnMessage.setRETURNCODE("T");
            }else {
                //setOperateLogInfo(logMap,"0",rev);
                returnMessage.setMESSAGE(rev);
                returnMessage.setRETURNCODE("F");
            }
            logger.debug("接收到PTS更新订单状态后返回给PTS数据：" + JSON.toJSONString(returnMessage));
        }
        //// 5.接口调用操作日志记录
        //baseService.insertOperatelogWithMap(logMap);
        String result=JSON.toJSONString(returnMessage);
        logger.debug("返回订单更新信息给PTS："+result);
        return result;
    }

    /**
     * @Description: PTS更新库存接口。
     * @Param: [request] 传入JSON数据
     * @return: java.util.Map<java.lang.String,java.lang.Object> 回传JSON数据
     * @Author: Qin.Qing
     * @Date: 2018/11/20
     */
    @ResponseBody
    @RequestMapping(value = "/stockupdate", method = RequestMethod.POST,produces = { "application/x-www-form-urlencoded;charset=UTF-8" })
    @ApiOperation(value = "stockupdate")
    public String stockUpdate(@RequestBody String request) {
        try {
            request = URLDecoder.decode(request, "utf-8");
        }catch (Exception ex){
            logger.error(ex.getMessage());
        }
        request=trimEnd(request,"=");
        logger.debug("接收到PTS推送库存更新信息："+request);
        Map<String, Object> paramMap = JSONObject.parseObject(request);
        PtsReturnCommonMessage returnMessage=new PtsReturnCommonMessage();
//        //定义接口调用操作日志map对象(PTS调用小宝接口)
//        Map<String, Object> logMap = createOperateLogMap("1",
//                null,"stockupdate",
//                "PTS更新库存接口");
        if (!validApiParamForPts(paramMap)) {
            returnMessage.setMESSAGE("ERROR:API参数异常！");
            returnMessage.setRETURNCODE("F");
//            setOperateLogInfo(logMap,"0","ERROR:API参数异常！");
            logger.debug("接收到库存更新后返回给PTS数据：" + JSON.toJSONString(returnMessage));
        }else {
            String result = baseService.updateStockWithMap(paramMap, null);//todo 这边的代码应该是没地方用到的
            if(result.equals("ok")){
                //成功
                returnMessage.setMESSAGE("SUCCEED");
                returnMessage.setRETURNCODE("T");
            }else {
//                setOperateLogInfo(logMap,"0",result);
                //具体信息贴给PTS
                returnMessage.setMESSAGE(result);
                returnMessage.setRETURNCODE("F");
            }
        }
        // 5.接口调用操作日志记录
        //baseService.insertOperatelogWithMap(logMap);//库存更新量太多，不加入日志记录。
        String result=JSON.toJSONString(returnMessage);
        logger.debug("返回库存更新信息给PTS："+result);
        return result;
    }

    /**
     * @Description: PTS获取订单详细信息。
     * @Param: [request] 传入JSON数据
     * @return: java.util.Map<java.lang.String,java.lang.Object> 回传JSON数据
     * @Author: Li.Deman
     * @Date: 2018/11/22
     */
    @ResponseBody
    @RequestMapping(value = "/getorder", method = RequestMethod.POST,produces = { "application/x-www-form-urlencoded;charset=UTF-8" })
    public String GetOrderInfo(@RequestBody String request){
        //对于request传过来的json参数做异常捕捉
        try {
            request=URLDecoder.decode(request,"utf-8");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
        }
        //过滤字符串最后的一些字符
        request=trimEnd(request,"=");
        //看看json字符串是如何转换为Map对象的
        logger.debug("接收到PTS请求订单信息："+request);
        Map<String,Object> paramMap=JSONObject.parseObject(request);
//        //定义接口调用操作日志map对象(PTS调用小宝接口)
//        Map<String, Object> logMap = createOperateLogMap("1",
//                null,"getorder",
//                "PTS获取订单详细信息");
        if(!validApiParamForPtsGetOrderProInfo(paramMap)){
            PtsReturnCommonMessage returnMessage=new PtsReturnCommonMessage();
            returnMessage.setMESSAGE("ERROR:API参数异常！");
            returnMessage.setRETURNCODE("F");
//            setOperateLogInfo(logMap,"0","ERROR:API参数异常！");
            logger.debug("接收到PTS获取订单详细信息后返回给PTS数据：" + JSON.toJSONString(returnMessage));
//            // 5.接口调用操作日志记录
//            baseService.insertOperatelogWithMap(logMap);
            String result=JSON.toJSONString(returnMessage);
            return result;
        }else {
            //这个service的处理，需要把具体的订单主体信息返回
            PtsOrderInfo returnMessage = baseService.GetOrderInfo(paramMap);
            if(returnMessage.getResult().equals("ok")){
                returnMessage.setMESSAGE("SUCCEED");
                returnMessage.setRETURNCODE("T");
            }else {
//                setOperateLogInfo(logMap,"0",returnMessage.getResult());
                returnMessage.setMESSAGE(returnMessage.getResult());
                returnMessage.setRETURNCODE("F");
            }
//            // 5.接口调用操作日志记录
//            baseService.insertOperatelogWithMap(logMap);
            String result=JSON.toJSONString(returnMessage);
            logger.debug("返回订单详细信息给PTS："+result);
            return result;
        }
    }

    /**
     * PTS-HANA商家取消订单
     *待调整，还未更新我们本地库呢
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/order/orderCancel", method = {RequestMethod.GET, RequestMethod.POST})
    public String orderCancel(HttpServletRequest request) {
        //定义接口调用操作日志map对象(PTS调用小宝接口)
        Map<String, Object> logMap = createOperateLogMap("1",
                null,"orderCancel",
                "PTS-HANA商家取消订单");
        Map<String, Object> paramMap = getParameterMap(request);
        PtsReturnCommonMessage returnMessage=new PtsReturnCommonMessage();
        // 3.商家确认订单
        String result=baseService.cancelOrder(paramMap);
        if (result.equals("ok")){
            returnMessage.setMESSAGE("SUCCEED");
            returnMessage.setRETURNCODE("T");
        }else {
            setOperateLogInfo(logMap,"0",result);
            returnMessage.setMESSAGE(result);
            returnMessage.setRETURNCODE("F");
        }
        // 5.接口调用操作日志记录
        baseService.insertOperatelogWithMap(logMap);
        return JSON.toJSONString(returnMessage);
    }

    /**
     * 门店开店闭店信息推送
     * @param request
     * @return
     * @author：Li.Deman
     * @date：2018/12/25
     */
    @ResponseBody
    @RequestMapping(value = "/shop/openupdate", method = RequestMethod.POST,produces = { "application/x-www-form-urlencoded;charset=UTF-8" })
    public String shopopenupdate(@RequestBody String request){
        try {
            request=URLDecoder.decode(request,"utf-8");
        }catch (Exception ex){
            logger.error(ex.getMessage());
        }
        request=trimEnd(request,"=");
        logger.debug("接收到PTS推送门店开店闭店信息："+request);
        Map<String,Object> paramMap=JSONObject.parseObject(request);
        PtsReturnMessage returnMessage=new PtsReturnMessage();
        //定义接口调用操作日志map对象(PTS调用小宝接口)
        Map<String, Object> logMap = createOperateLogMap("1",
                null,"shoponlineupdate",
                "门店开店闭店信息推送");
        if(!validApiParamForPtsOrder(paramMap)){
            returnMessage.setMESSAGE("ERROR:API参数异常！");
            returnMessage.setRETURNCODE("F");
            setOperateLogInfo(logMap,"0","ERROR:API参数异常！");
            logger.debug("接收到PTS推送门店开店闭店信息后返回给PTS数据：" + JSON.toJSONString(returnMessage));
        }else {
            //操作门店信息表，更新门店上线状态
            String rev= baseService.shopOpenUpdate(paramMap);
            if (rev.startsWith("OK")) {
                returnMessage.setMESSAGE("SUCCEED");
                returnMessage.setRETURNCODE("T");
            }else {
                setOperateLogInfo(logMap,"0",rev);
                returnMessage.setMESSAGE(rev);
                returnMessage.setRETURNCODE("F");
            }
            logger.debug("接收到PTS门店开店闭店信息推送后返回给PTS数据：" + JSON.toJSONString(returnMessage));
        }
        // 5.接口调用操作日志记录
        baseService.insertOperatelogWithMap(logMap);
        String result=JSON.toJSONString(returnMessage);
        logger.debug("返回门店开店闭店信息推送给PTS："+result);
        return result;
    }

    /**
     * PTS
     * @param map
     * @return
     */
    public boolean validApiParamForPts(Map<String, Object> map) {
        String shop = nullToEmpty(map.get("shop"));

        if (StringUtils.isNullOrEmpty(shop)) {
            logger.debug("PTS库存推送API参数基础验证失败," + "无门店信息!");
            return false;
        } else {
            logger.debug("PTS库存推送API参数基础验证成功!");
            return true;
        }
    }

    /**
     * PTS更新订单状态 API参数基础验证
     * @param map
     * @return
     */
    public boolean validApiParamForPtsOrder(Map<String, Object> map) {
        String data = nullToEmpty(map.get("DATA"));
        if (StringUtils.isNullOrEmpty(data)) {
            logger.debug("PTS订单状态更新API参数基础验证失败," + "无信息!");
            return false;
        } else {
            logger.debug("PTS订单状态更新API参数基础验证成功!");
            return true;
        }
    }

    /**
     * PTS获取订单详细信息 API参数基础验证
     * @param map
     * @return
     */
    public  boolean validApiParamForPtsGetOrderProInfo(Map<String, Object> map){
        //如果传过来值是null，一律转换为empty空字符串
        String shop_no=nullToEmpty(map.get("shop_no"));
        String order_id=nullToEmpty(map.get("order_id"));
        //判断每个value值是否为null或empty
        if(StringUtils.isNullOrEmpty(shop_no)){
            logger.debug("PTS获取订单详细信息API参数基础验证失败："+"无门店信息！");
            return false;
        }
        if(StringUtils.isNullOrEmpty(order_id)){
            logger.debug("PTS获取订单详细信息API参数基础验证失败："+"无订单ID信息！");
            return false;
        }
        logger.debug("PTS获取订单详细信息API参数基础验证成功!");
        return true;
    }

}
