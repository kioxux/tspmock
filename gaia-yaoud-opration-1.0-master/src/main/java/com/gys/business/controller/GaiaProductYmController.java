package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.gys.business.service.GaiaProductYmService;
import com.gys.business.service.data.GaiaCommodityInventoryHInData;
import com.gys.business.service.data.GaiaProductYmInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 药盟商品信息(GaiaProductYm)表控制层
 *
 * @author XIAOZY   功能作废
 * @since 2021-11-11 20:26:17
 */
@RestController
@RequestMapping("gaiaProductYm")
@Slf4j
public class GaiaProductYmController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaProductYmService gaiaProductYmService;

    /**
     * 查询ERP商品信息
     *
     */
   /* @PostMapping("listProInfo")
    public JsonResult listProInfo(HttpServletRequest request, @RequestBody GaiaProductYmInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        param.setStoCode(userInfo.getDepId());
        log.info(String.format("<药盟商品编码对码><查询ERP商品信息><请求参数：%s>", JSONUtil.toJsonStr(param)));
        return JsonResult.success(gaiaProductYmService.listProInfo(param), "提示：查询数据成功！");
    }*/

    /**
     * 查询药盟药品信息
     *
     */
   /* @PostMapping("listYMProInfo")
    public JsonResult listYMProInfo(HttpServletRequest request, @RequestBody GaiaProductYmInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        log.info(String.format("<药盟商品编码对码><查询药盟药品信息><请求参数：%s>", JSONUtil.toJsonStr(param)));
        return JsonResult.success(gaiaProductYmService.listYMProInfo(param), "提示：查询数据成功！");
    }*/

    /**
     * 商品对码关系-保存
     *
     */
   /* @PostMapping("saveYMProInfo")
    public JsonResult saveYMProInfo(HttpServletRequest request, @RequestBody GaiaProductYmInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        param.setStoCode(userInfo.getDepId());
        log.info(String.format("<药盟商品编码对码><商品对码关系-保存><请求参数：%s>", JSONUtil.toJsonStr(param)));
        return JsonResult.success(gaiaProductYmService.saveYMProInfo(param,userInfo), "提示：保存数据成功！");
    }*/

    /**
     * 商品对码关系-查询对码关系
     *
     */
   /* @PostMapping("listDMInfo")
    public JsonResult listDMInfo(HttpServletRequest request, @RequestBody GaiaProductYmInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        param.setStoCode(userInfo.getDepId());
        log.info(String.format("<药盟商品编码对码><查询商品对码关系><请求参数：%s>", JSONUtil.toJsonStr(param)));
        return JsonResult.success(gaiaProductYmService.listDMInfo(param,userInfo), "提示：保存数据成功！");
    }*/

}

