package com.gys.business.controller;

import com.gys.business.service.GaiaNewProductEvaluationHService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "商品评估", description = "/productEvaluation")
@RestController
@RequestMapping({"/productEvaluation/"})
public class GaiaNewProductEvaluationHController extends BaseController {
    @Resource
    private GaiaNewProductEvaluationHService gaiaNewProductEvaluationHService;


    @ApiOperation(value = "查询收货单", response = GetAcceptOutData.class)
    @PostMapping({"test"})
    public JsonResult test(HttpServletRequest request, @ApiParam(value = "查询条件", required = true) @RequestBody GetAcceptInData inData) {
        gaiaNewProductEvaluationHService.InsertGaiaNewProductEvaluation();
        return null;
    }

    @ApiOperation(value = "根据加盟商和门店号，查询没有确认的商品评估单据", response = GetAcceptOutData.class)
    @PostMapping({"getBillByClientAndStorId"})
    public JsonResult getBillByClientAndStorId(HttpServletRequest request, @RequestBody GaiaNewProductEvaluationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(gaiaNewProductEvaluationHService.getBillByClientAndStorId(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "根据加盟商和门店号，单据号，查询单据明细", response = GetAcceptOutData.class)
    @PostMapping({"getBillInfoByBillCode"})
    public JsonResult getBillInfoByBillCode(HttpServletRequest request, @RequestBody GaiaNewProductEvaluationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(gaiaNewProductEvaluationHService.getBillInfoByBillCode(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "确认评估", response = GetAcceptOutData.class)
    @PostMapping({"updateDetailList"})
    public JsonResult updateDetailList(HttpServletRequest request, @RequestBody GaiaNewProductEvaluationOutData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClientId(userInfo.getClient());
        return JsonResult.success(gaiaNewProductEvaluationHService.updateDetailList(param,userInfo), "提示：保存数据成功！");
    }

    @ApiOperation(value = "查询历史记录", response = GetAcceptOutData.class)
    @PostMapping({"listBillInfo"})
    public JsonResult listBillInfo(HttpServletRequest request, @RequestBody GaiaNewProductEvaluationOutData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClientId(userInfo.getClient());
        return JsonResult.success(gaiaNewProductEvaluationHService.listBillInfo(param,userInfo), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询成分大类", response = GetAcceptOutData.class)
    @PostMapping({"listBigClass"})
    public JsonResult listBigClass(HttpServletRequest request, @RequestBody GaiaNewProductEvaluationOutData param) {
        return JsonResult.success(gaiaNewProductEvaluationHService.listBigClass(param), "提示：获取数据成功！");
    }

}
