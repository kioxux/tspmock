package com.gys.business.controller;
import com.gys.business.mapper.entity.MibReconciliation;
import com.gys.business.service.MibService;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.mib.MibRePrintFormInData;
import com.gys.business.service.data.mib.PayInfoData;
import com.gys.business.service.data.mib.SetlInfoData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RequestMapping({"/mib/"})
@Controller
public class MibController extends BaseController {
    @Autowired
    private MibService mibService;

    /**
     * drugInfo，drugDetail保存
     *
     * @param request
     * @return
     */
    @PostMapping({"saveMibDrugInfoAndDetail"})
    @ResponseBody
    public JsonResult saveMibDrugInfoAndDetail(HttpServletRequest request, @RequestBody PayInfoData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.mibService.saveMibDrugInfoAndDetail(userInfo, inData);
        return JsonResult.success(null, "success");
    }

    /**
     * setlInfo，setlDetail,detlcutInfo保存
     *
     * @param request
     * @return
     */
    @PostMapping({"saveMibSetlInfoAndDetail"})
    @ResponseBody
    public JsonResult saveMibSetlInfoAndDetail(HttpServletRequest request, @RequestBody SetlInfoData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.mibService.saveMibSetlInfoAndDetail(userInfo, inData);
        return JsonResult.success(null, "success");
    }


    /**
     *获取山西省忻州市医疗保险普通门诊结算单数据
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"getMibSXXZJSForm"})
    @ResponseBody
    public JsonResult getMibSXXZJSForm(HttpServletRequest request, @RequestBody MibRePrintFormInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success( this.mibService.getMibSXXZJSForm(userInfo, inData), "success");
    }

    /**
     *获取山西省朔州市医疗保险普通门诊结算单数据
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"getMibSXSZJSForm"})
    @ResponseBody
    public JsonResult getMibSXSZJSForm(HttpServletRequest request, @RequestBody MibRePrintFormInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success( this.mibService.getMibSXSZJSForm(userInfo, inData), "success");
    }

    /**
     * 获取山西省医疗保险药店费用结算分割表数据
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"getMibJSSplitForm"})
    @ResponseBody
    public JsonResult getMibJSSplitForm(HttpServletRequest request, @RequestBody MibRePrintFormInData inData) {
        return JsonResult.success( this.mibService.getMibJSSplitForm(inData), "success");
    }/**
     * setlInfo查询
     * 查询医保销售
     * @param request
     * @return
     */
    @PostMapping({"getSeltInfo"})
    @ResponseBody
    public JsonResult getSeltInfo(HttpServletRequest request, @RequestBody InData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success( this.mibService.getSeltInfo(userInfo, inData), "success");
    }


    /**
     * setlInfo查询（对账明细用）
     * 查询医保销售
     * @param request
     * @return
     */
    @PostMapping({"getSeltInfo3202"})
    @ResponseBody
    public JsonResult getSeltInfo3202(HttpServletRequest request, @RequestBody InData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success( this.mibService.getSeltInfo3202(userInfo, inData), "success");
    }
    /**
     * setlInfo查询
     * 医保对账
     * @param request
     * @return
     */
    @PostMapping({"getSeltInfoSum"})
    @ResponseBody
    public JsonResult getSeltInfoSum(HttpServletRequest request, @RequestBody InData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success( this.mibService.getSeltInfoSum(userInfo, inData), "success");
    }

    /**
     * 冲正保修改订单状态
     * 医保对账
     * @param request
     * @return
     */
    @PostMapping({"updateCzFlag"})
    @ResponseBody
    public JsonResult updateCzFlag(HttpServletRequest request, @RequestBody Map inData) {
//        GetLoginOutData userInfo = this.getLoginUser(request);
        this.mibService.updateCzFlag( inData);
        return JsonResult.success( "", "success");
    }


    /**
     * 修改订单状态
     * 医保对账
     * @param request
     * @return
     */
    @PostMapping({"updateSetlInfoState"})
    @ResponseBody
    public JsonResult updateSetlInfoState(HttpServletRequest request, @RequestBody Map inData) {
//        GetLoginOutData userInfo = this.getLoginUser(request);
        this.mibService.updateSetlInfoState( inData);
        return JsonResult.success( "", "success");
    }


    /**
     * 医保
     * 医保对账结果保存
     * @param request
     * @return
     */
    @PostMapping({"reconciliationInsert"})
    @ResponseBody
    public JsonResult reconciliationInsert(HttpServletRequest request, @RequestBody MibReconciliation inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        this.mibService.reconciliationInsert( inData);
        return JsonResult.success( "", "success");
    }

    /**
     * 医保
     * 医保对账结果查询
     * @param request
     * @return
     */
    @PostMapping({"reconciliationSelect"})
    @ResponseBody
    public JsonResult reconciliationSelect(HttpServletRequest request, @RequestBody MibReconciliation inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success( this.mibService.reconciliationSelect( inData), "success");
    }

    /**
     * 医保
     * 医保对账结果修改
     * @param request
     * @return
     */
    @PostMapping({"reconciliationUpdate"})
    @ResponseBody
    public JsonResult reconciliationUpdate(HttpServletRequest request, @RequestBody MibReconciliation inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        this.mibService.reconciliationUpdate(inData);
        return JsonResult.success( "", "success");
    }

    /**
     * 医保
     * 医保对账结果维护
     * @param request
     * @return
     */
    @PostMapping({"reconciliationInsertOrUpdate"})
    @ResponseBody
    public JsonResult reconciliationInsertOrUpdate(HttpServletRequest request, @RequestBody MibReconciliation inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        this.mibService.reconciliationInsertOrUpdate(inData);
        return JsonResult.success( "", "success");
    }

    /**
     * 医保
     * 医保对账结果维护
     * @param request
     * @return
     */
    @PostMapping({"getSeltInfoOne"})
    @ResponseBody
    public JsonResult getSeltInfoOne(HttpServletRequest request, @RequestBody InData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success( this.mibService.getSeltInfoOne(inData), "success");
    }

}
