//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.BankInfoService;
import com.gys.business.service.data.BankInfoInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/bankInfo/"})
public class BankInfoController extends BaseController {
    @Autowired
    private BankInfoService bankInfoService;

    public BankInfoController() {
    }

    @PostMapping({"/bankInfoList"})
    public JsonResult bankInfoList(HttpServletRequest request, @RequestBody BankInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.bankInfoService.getBankInfoList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/getBankNames"})
    public JsonResult getBankNames(HttpServletRequest request, @RequestBody BankInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.bankInfoService.getBankNames(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/getAllBankIdByName"})
    public JsonResult getAllBankIdByName(HttpServletRequest request, @RequestBody BankInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.bankInfoService.getAllBankIdByName(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/getAllBankAccountByName"})
    public JsonResult getAllBankAccountByName(HttpServletRequest request, @RequestBody BankInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.bankInfoService.getAllBankAccountByName(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/getBankIdByBankAccount"})
    public JsonResult getBankIdByBankAccount(HttpServletRequest request, @RequestBody BankInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.bankInfoService.getBankIdByBankAccount(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/addBankInfo"})
    public JsonResult addBankInfo(@RequestBody BankInfoInData inData) {
        this.bankInfoService.insertBankInfo(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/getBankInfo"})
    public JsonResult getBankInfo(@RequestBody BankInfoInData inData) {
        return JsonResult.success(this.bankInfoService.getBankInfoById(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/editBankInfo"})
    public JsonResult editBankInfo(HttpServletRequest request, @RequestBody BankInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.bankInfoService.editBankInfo(inData);
        return JsonResult.success("", "提示：保存成功！");
    }
}
