package com.gys.business.controller;

import com.gys.business.service.ProductSortService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "商品分类设置", description = "")
@RestController
@RequestMapping({"/productSortSetting/"})
public class ProductSortSettingController extends BaseController {
    @Resource
    private ProductSortService productSortService;

    @ApiOperation(value = "商品查询列表接口", notes = "商品查询列表接口", response = GetReplenishOutData.class)
    @PostMapping({"queryProVo"})
    public JsonResult queryProVo(HttpServletRequest request, @RequestBody GetProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.productSortService.queryProVo(inData), "提示：获取数据成功！");
    }

    @PostMapping({"queryPro"})
    public JsonResult queryPro(HttpServletRequest request, @RequestBody GetProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryPro(inData), "提示：获取数据成功！");
    }

    @PostMapping({"queryProNew"})
    public JsonResult queryProNew(HttpServletRequest request, @RequestBody GetProductNewInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryProNew(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "商品查询3.0", notes = "商品条件查询接口")
    @PostMapping({"queryProThirdly"})
    public JsonResult queryProThirdly(HttpServletRequest request, @RequestBody GetProductThirdlyInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryProThirdly(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "商品查询-提成方案专用", notes = "商品查询-提成方案专用")
    @PostMapping({"queryProFourthlyForTC"})
    public JsonResult queryProFourthlyForTC(HttpServletRequest request, @RequestBody GetProductThirdlyInForTCData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryProFourthlyForTC(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "商品查询4.0", notes = "商品条件查询接口")
    @PostMapping({"queryProFourthly"})
    public JsonResult queryProFourthly(HttpServletRequest request, @RequestBody GetProductThirdlyInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryProFourthly(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "商品查询5.0-用于冷链商品补货", notes = "商品条件查询接口")
    @PostMapping({"queryProForColdChain"})
    public JsonResult queryProForColdChain(HttpServletRequest request, @RequestBody GetProductThirdlyInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryProForColdChain(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "商品查询", notes = "商品条件查询接口", response = GetReplenishOutData.class)
    @PostMapping({"queryProConditionNew"})
    public JsonResult queryProConditionNew(HttpServletRequest request, @RequestBody GetProductNewInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryProConditionNew(inData), "提示：获取数据成功！");
    }

    @PostMapping({"queryPrice"})
    public JsonResult queryPrice(HttpServletRequest request, @RequestBody GetProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryPrice(inData), "提示：获取数据成功！");
    }

    @PostMapping({"queryDetail"})
    public JsonResult queryDetail(@RequestBody GetProductInData inData) {
        return JsonResult.success(this.productSortService.queryDetail(inData), "提示：获取数据成功！");
    }

    @PostMapping({"querySort"})
    public JsonResult querySort(@RequestBody GetProductInData inData) {
        return JsonResult.success(this.productSortService.querySort(inData), "提示：获取数据成功！");
    }

    @PostMapping({"add"})
    public JsonResult add(HttpServletRequest request, @RequestBody GetProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.productSortService.add(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"update"})
    public JsonResult update(HttpServletRequest request, @RequestBody GetProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.productSortService.update(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"importExcel"})
    public JsonResult importExcel(@RequestBody GetProImportExcelInData inData) {
        this.productSortService.importExcel(inData);
        return JsonResult.success("", "提示：上传成功！");
    }

    @PostMapping({"delete"})
    public JsonResult delete(@RequestBody ConllectionInData inData) {
        this.productSortService.delete(inData.getProductInData());
        return JsonResult.success("", "提示：删除成功！");
    }

    @PostMapping({"selectProGroupByClient"})
    public JsonResult selectProGroupByClient(HttpServletRequest request, @ApiParam(value = "查询商品(传空)", required = true) @RequestBody GetProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        List<GetProductSortOutData> list = this.productSortService.selectProGroupByClient(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "商品查询5.0", notes = "商品条件查询接口 不合格品商品查询") //不合格品商品查询
    @PostMapping({"substandardProductInquiry"})
    public JsonResult substandardProductInquiry(HttpServletRequest request, @RequestBody GetProductThirdlyInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryProFourthlyS(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "web商品查询列表接口", notes = "web商品查询列表接口", response = GetReplenishDetailOutData.class)
    @PostMapping({"queryProList"})
    public JsonResult queryProList(HttpServletRequest request, @RequestBody GetProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.productSortService.queryProList(inData), "提示：获取数据成功！");
    }

    /**
     * 获取商品列表title
     * @param request
     * @param inData
     * @return
     */
    @PostMapping("productTitleSetting")
    public JsonResult productTitleSetting(HttpServletRequest request, @RequestBody @Valid GetProductThirdlyInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setProSite(userInfo.getDepId());
        return JsonResult.success(this.productSortService.productTitleSetting(inData),"提示：获取数据成功！");
    }

    @ApiOperation(value = "药德商品查询1.0",notes = "药德商品查询") //不合格品商品查询
    @PostMapping({"queryBasicProduct"})
    public JsonResult queryBasicProduct(HttpServletRequest request, @RequestBody GetProductThirdlyInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.productSortService.queryBasicProduct(inData), "提示：获取数据成功！");
    }
}
