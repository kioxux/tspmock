package com.gys.business.controller.storeorder;

import com.gys.business.service.storeorder.StoreOrderService;
import com.gys.common.base.BaseController;
import com.gys.common.data.JsonResult;
import com.gys.common.data.storeorder.StoreOrderDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wu mao yin
 * @Title: 门店请货数据查询
 * @date 2021/12/315:01
 */
@Api(tags = "门店请货数据查询")
@RestController
@RequestMapping("/storeOrder")
public class StoreOrderController extends BaseController {

    @Resource
    private StoreOrderService storeOrderService;

    @Resource
    private HttpServletRequest request;

    @ApiOperation("查询连锁公司列表")
    @GetMapping("selectChainCompList")
    public JsonResult selectChainCompList() {
        return storeOrderService.selectChainCompList(getLoginUser(request).getClient());
    }

    @ApiOperation("根据连锁公司查询门店")
    @GetMapping("selectStoListByChainComp")
    public JsonResult selectStoListByChainComp(String chainCompId) {
        return storeOrderService.selectStoListByChainComp(getLoginUser(request).getClient(), chainCompId);
    }

    @ApiOperation("查询门店明细清单")
    @PostMapping("selectStoreDetailedList")
    public JsonResult selectStoreDetailedList(@RequestBody StoreOrderDTO storeOrderDTO) {
        storeOrderDTO.setClient(getLoginUser(request).getClient());
        return storeOrderService.selectStoreDetailedList(storeOrderDTO);
    }

    @PostMapping("exportStoreDetailedListExcel")
    public void exportStoreDetailedListExcel(@RequestBody StoreOrderDTO storeOrderDTO, HttpServletResponse response){
        storeOrderDTO.setClient(getLoginUser(request).getClient());
        storeOrderService.exportExcel(storeOrderDTO,response);
    }

    @ApiOperation("查询商品汇总清单")
    @PostMapping("selectProSummaryList")
    public JsonResult selectProSummaryList(@RequestBody StoreOrderDTO storeOrderDTO) {
        storeOrderDTO.setClient(getLoginUser(request).getClient());
        return storeOrderService.selectProSummaryList(storeOrderDTO);
    }

    @PostMapping("exportProSummaryList")
    public void exportProSummaryList(@RequestBody StoreOrderDTO storeOrderDTO, HttpServletResponse response){
        storeOrderDTO.setClient(getLoginUser(request).getClient());
        storeOrderService.exportProSummaryList(storeOrderDTO,response);
    }

}
