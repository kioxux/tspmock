package com.gys.business.controller;

import com.gys.business.service.YinHaiMedicalInsuranceService;
import com.gys.business.service.data.SupplierRecordOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.yinHaiModel.ClientInfoInput;
import com.gys.common.yinHaiModel.GoodsInfoInput;
import com.gys.common.yinHaiModel.StockDetailInfoInput;
import com.gys.common.yinHaiModel.TransforStockInfoInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "银海医保对接相关接口")
@RestController
@RequestMapping("/yinHaiMedicalInsurance")
public class YinHaiMedicalInsuranceController extends BaseController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private YinHaiMedicalInsuranceService yinHaiMedicalInsuranceService;


    @ApiOperation(value = "采购退货同步数据")
    @PostMapping("/insertCancelStockInfo")
    public JsonResult insertCancelStockInfo() {
        GetLoginOutData userInfo = getLoginUser(request);
        return yinHaiMedicalInsuranceService.insertCancelStockInfo(userInfo);
    }


    @ApiOperation(value = "零售下单同步数据")
    @PostMapping("/addOrder")
    public JsonResult addOrder() {
        GetLoginOutData userInfo = getLoginUser(request);
        return yinHaiMedicalInsuranceService.addOrder(userInfo);
    }


    @ApiOperation(value = "报损或报溢制单同步数据")
    @PostMapping("/insertInventoryManagerOrders")
    public JsonResult insertInventoryManagerOrders() {
        GetLoginOutData userInfo = getLoginUser(request);
        return yinHaiMedicalInsuranceService.insertInventoryManagerOrders(userInfo);
    }


    @ApiOperation(value = "零售退单同步数据")
    @PostMapping("/orderModify")
    public JsonResult orderModify() {
        GetLoginOutData userInfo = getLoginUser(request);
        return yinHaiMedicalInsuranceService.getOrderModifyInfo(userInfo);
    }




    @ApiOperation(value = "医保对接-根据时间段查询新增的往来单位信息", response = SupplierRecordOutData.class)
    @PostMapping("/listSendClientInfo")
    public JsonResult listSendClientInfo(HttpServletRequest request,@RequestBody ClientInfoInput inData){
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(yinHaiMedicalInsuranceService.listSendClientInfo(inData),"success");
    }

    @ApiOperation(value = "医保对接-插入对接方返回的单位编码", response = SupplierRecordOutData.class)
    @PostMapping("/insertSwapClientInfo")
    public JsonResult insertSwapClientInfo(HttpServletRequest request, @RequestBody List<ClientInfoInput> inData){
        GetLoginOutData userInfo = super.getLoginUser(request);
        return JsonResult.success(yinHaiMedicalInsuranceService.insertSwapClientInfo(inData,userInfo),"success");
    }

    @ApiOperation(value = "医保对接-根据时间段查询新增药品信息", response = SupplierRecordOutData.class)
    @PostMapping("/listSendGoodsInfo")
    public JsonResult listSendGoodsInfo(HttpServletRequest request,@RequestBody GoodsInfoInput inData){
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(yinHaiMedicalInsuranceService.listSendGoodsInfo(inData),"success");
    }

    @ApiOperation(value = "医保对接-插入对接方返回的商品编码", response = SupplierRecordOutData.class)
    @PostMapping("/insertSwapGoodsInfo")
    public JsonResult insertSwapGoodsInfo(HttpServletRequest request, @RequestBody List<GoodsInfoInput> inData){
        GetLoginOutData userInfo = super.getLoginUser(request);
        return JsonResult.success(yinHaiMedicalInsuranceService.insertSwapGoodsInfo(inData,userInfo),"success");
    }

    @ApiOperation(value = "医保对接-根据时间段查询采购入库信息", response = SupplierRecordOutData.class)
    @PostMapping("/listSendStockDetailInfo")
    public JsonResult listSendStockDetailInfo(HttpServletRequest request,@RequestBody StockDetailInfoInput inData){
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(yinHaiMedicalInsuranceService.listSendStockDetailInfo(inData),"success");
    }

    @ApiOperation(value = "医保对接-插入对接方返回的入库单号", response = SupplierRecordOutData.class)
    @PostMapping("/insertSwapStockInfo")
    public JsonResult insertSwapStockInfo(HttpServletRequest request, @RequestBody List<StockDetailInfoInput> inData){
        GetLoginOutData userInfo = super.getLoginUser(request);
        return JsonResult.success(yinHaiMedicalInsuranceService.insertSwapStockInfo(inData,userInfo),"success");
    }

    @ApiOperation(value = "医保对接-根据时间段查询库房门店调拨信息", response = SupplierRecordOutData.class)
    @PostMapping("/listSendTransforStockDetailInfo")
    public JsonResult listSendTransforStockDetailInfo(HttpServletRequest request, @RequestBody TransforStockInfoInput inData){
        GetLoginOutData userInfo = super.getLoginUser(request);
        return JsonResult.success(yinHaiMedicalInsuranceService.listSendTransforStockDetailInfo(inData,userInfo),"success");
    }
}
