//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.TempHumiService;
import com.gys.business.service.data.ConllectionInData;
import com.gys.business.service.data.GetMedCheckOutData;
import com.gys.business.service.data.TempHumiInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "温湿度记录", description = "/tempHumi")
@RestController
@RequestMapping({"/tempHumi/"})
public class TempHumiController extends BaseController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private TempHumiService tempHumiService;

    public TempHumiController() {
    }

    @ApiOperation(value = "查询温湿度记录")
    @PostMapping({"/tempHumiList"})
    public JsonResult getTempHumiList(HttpServletRequest request,@ApiParam(value = "温湿度记录", required = true) @RequestBody TempHumiInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.tempHumiService.getTempHumiList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "新增温湿度记录")
    @PostMapping({"/addTempHumi"})
    public JsonResult add(HttpServletRequest request,@ApiParam(value = "温湿度记录", required = true) @RequestBody TempHumiInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.tempHumiService.insertHumi(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "审核温湿度记录")
    @PostMapping({"/auditTempHumi"})
    public JsonResult aduitTempHumi(HttpServletRequest request, @RequestBody TempHumiInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.tempHumiService.aduitTempHumi(inData, userInfo);
        return JsonResult.success("", "提示：审核成功！");
    }

    @ApiOperation(value = "修改温湿度记录")
    @PostMapping({"/updateTempHumi"})
    public JsonResult updateTempHumi(HttpServletRequest request, @RequestBody TempHumiInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.tempHumiService.updateTempHumi(inData, userInfo);
        return JsonResult.success("", "提示：审核成功！");
    }

    @ApiOperation(value = "生成当月温湿度记录")
    @PostMapping({"/genTempHumi"})
    public JsonResult genTempHumi() {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return tempHumiService.genTempHumi(userInfo);
    }

}
