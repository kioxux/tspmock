package com.gys.business.controller;

import com.gys.business.service.MemberCardReportService;
import com.gys.business.service.SaleScheduleService;
import com.gys.business.service.data.GetMemberCardReportInData;
import com.gys.business.service.data.GetSaleScheduleInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "会员卡新增数(线下)查询报表")
@RequestMapping({"/memberCardReport/"})
public class MemberCardReportController extends BaseController {
    @Autowired
    private MemberCardReportService memberCardReportService;


    @Autowired
    private SaleScheduleService saleScheduleService;

    @ApiOperation(value = "列表查询")
    @PostMapping({"queryReport"})
    public JsonResult queryReport(HttpServletRequest request,@Valid @RequestBody GetMemberCardReportInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.memberCardReportService.queryReport(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "收银工号")
    @PostMapping({"queryUser"})
    public JsonResult queryUser(HttpServletRequest request) {
        GetSaleScheduleInData inData = new GetSaleScheduleInData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsegBrId(userInfo.getDepId());
        return JsonResult.success(this.saleScheduleService.queryUser(inData), "提示：保存成功！");
    }


}
