package com.gys.business.controller.app.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/9 0:06
 **/
@Data
public class OutStockForm {
    @ApiModelProperty(value = "排序条件：1-销售额（默认） 2-毛利额 3-销量 4-排名", example = "1")
    private Integer sortCond;
    @ApiModelProperty(value = "排序类型：1-倒序（默认）2-升序", example = "1")
    private Integer sortType;
    @ApiModelProperty(value = "查询数量", example = "10")
    private Integer queryNum;
    @ApiModelProperty(value = "加盟商", hidden = true)
    private String client;
    @ApiModelProperty(value = "配送中心編碼", example = "102", hidden = true)
    private String dcCode;
    @ApiModelProperty(hidden = true)
    private String startDate;
    @ApiModelProperty(hidden = true)
    private String endDate;
    @ApiModelProperty(hidden = true)
    private List<String> dcCodeList;
    @ApiModelProperty(hidden = true)
    private String userId;
}
