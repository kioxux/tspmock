package com.gys.business.controller.goodspurchase;

import cn.hutool.core.date.DatePattern;
import com.gys.business.service.data.productBasic.GetProductBasicInData;
import com.gys.business.service.goodspurchase.ProductRelationMaintainService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.goodspurchase.AddYdProductCodeInfo;
import com.gys.common.data.goodspurchase.ConfirmYdProCodeDTO;
import com.gys.common.data.goodspurchase.ProductRelationMaintainListDTO;
import com.gys.common.data.goodspurchase.SearchSameProductDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author wu mao yin
 * @Title: 客户商品对应关系查询及维护
 * @date 2021/10/2717:07
 */
@Api(tags = "客户商品对应关系查询及维护")
@RestController
@RequestMapping("/productRelationMaintain")
public class ProductRelationMaintainController extends BaseController {

    @Resource
    private ProductRelationMaintainService productRelationMaintainService;

    @ApiOperation("查询客户 client 集合")
    @GetMapping("selectCustomerClientList")
    public JsonResult selectCustomerClientList() {
        return productRelationMaintainService.selectCustomerClientList();
    }

    @ApiOperation("根据客户获取地点集合")
    @GetMapping("selectCustomerProSiteList")
    public JsonResult selectCustomerProSiteList(String client) {
        return productRelationMaintainService.selectCustomerProSiteList(client);
    }

    @ApiOperation("查询客户商品编码列表")
    @PostMapping("selectCustomerProCodeList")
    public JsonResult selectCustomerProCodeList(@RequestBody GetProductBasicInData getProductBasicInData) {
        return productRelationMaintainService.selectCustomerProCodeList(getProductBasicInData);
    }

    @ApiOperation("查询商品对应关系维护列表")
    @PostMapping("selectProductRelationMaintainList")
    public JsonResult selectProductRelationMaintainList(@RequestBody ProductRelationMaintainListDTO productRelationMaintainListDTO) {
        return productRelationMaintainService.selectProductRelationMaintainList(productRelationMaintainListDTO);
    }

    @ApiOperation("导出商品对应关系维护列表")
    @PostMapping("exportProductRelationMaintainList")
    public void exportProductRelationMaintainList(@RequestBody ProductRelationMaintainListDTO productRelationMaintainListDTO, HttpServletResponse response) {
        productRelationMaintainService.exportProductRelationMaintainList(productRelationMaintainListDTO, response);
    }

    @ApiOperation("查询商品编码详情信息")
    @PostMapping("selectProductCodeInfo")
    public JsonResult selectProductCodeInfo(@RequestBody AddYdProductCodeInfo addYdProductCodeInfo) {
        return productRelationMaintainService.selectProductCodeInfo(addYdProductCodeInfo);
    }

    @ApiOperation("保存、新建药德编码")
    @PostMapping("saveYdProCode")
    public JsonResult saveYdProCode(HttpServletRequest request, @RequestBody AddYdProductCodeInfo addYdProductCodeInfo) {
        GetLoginOutData loginUser = this.getLoginUser(request);
        addYdProductCodeInfo.getGaiaProductBasic().setUpdateClient(loginUser.getClient());
        addYdProductCodeInfo.getGaiaProductBasic().setUpdater(loginUser.getUserId());
        addYdProductCodeInfo.getGaiaProductBasic().setUpdateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_PATTERN)));
        return productRelationMaintainService.saveYdProCode(addYdProductCodeInfo);
    }

    /** add by jinwencheng on 2021-12-30 13:50:13 新建药德商品编码页面，新建前先查询是否有符合条件的商品 **/
    @ApiOperation("查找符合条件的商品信息")
    @PostMapping("getSameProduct")
    public JsonResult getSameProduct(@RequestBody SearchSameProductDTO searchSameProductDTO) {
        return productRelationMaintainService.getSameProduct(searchSameProductDTO);
    }
    /** add end **/

    /** add by jinwencheng on 2021-12-30 14:13:39 确认药德商品编码 **/
    @ApiOperation("确认药德商品编码")
    @PostMapping("confirmYdProCode")
    public JsonResult confirmYdProCode(HttpServletRequest request, @RequestBody ConfirmYdProCodeDTO confirmYdProCodeDTO) {
        GetLoginOutData loginUser = this.getLoginUser(request);
        confirmYdProCodeDTO.setUpdateClient(loginUser.getClient());
        confirmYdProCodeDTO.setUpdater(loginUser.getUserId());
        confirmYdProCodeDTO.setUpdateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_PATTERN)));
        return productRelationMaintainService.confirmYdProCode(confirmYdProCodeDTO);
    }
    /** add end **/

}
