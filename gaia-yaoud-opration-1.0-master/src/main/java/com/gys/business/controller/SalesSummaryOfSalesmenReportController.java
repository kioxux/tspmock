package com.gys.business.controller;

import com.gys.business.service.SaleScheduleService;
import com.gys.business.service.SalesSummaryOfSalesmenReportService;
import com.gys.business.service.data.GetSaleScheduleInData;
import com.gys.business.service.data.GetSalesSummaryOfSalesmenReportInData;
import com.gys.business.service.data.GetSalesSummaryOfSalesmenReportOutData;
import com.gys.business.service.data.SalespersonsSalesDetailsOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.SupplierInfoDTO;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Map;

/**
 * @author wavesen.shen
 */
@Api(tags = "营业员销售查询")
@RestController
@RequestMapping({"/SalesSummaryOfSalesmenReport/"})
public class SalesSummaryOfSalesmenReportController extends BaseController {
    @Autowired
    private SalesSummaryOfSalesmenReportService salesSummaryOfSalesmenReportService;

    @Autowired
    private SaleScheduleService saleScheduleService;

    @ApiOperation(value = "列表条件查询")
    @PostMapping("queryReport")
    public JsonResult queryReport(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.queryReport(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "联合用药查询")
    @PostMapping("/union/queryReport")
    public JsonResult unionQueryReport(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.unionQueryReport(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "联合用药查询")
    @PostMapping("/union/queryExport")
    public Result unionQueryExport(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return this.salesSummaryOfSalesmenReportService.unionQueryExport(inData);
    }

    @PostMapping("getUserListByClient")
    public JsonResult getUserListByClient(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.getUserListByClient(userInfo.getClient()), "提示：获取数据成功！");
    }

    @ApiOperation(value = "获取营业员信息")
    @PostMapping({"queryUser"})
    public JsonResult queryUser(HttpServletRequest request) {
        GetSaleScheduleInData inData = new GetSaleScheduleInData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsegBrId(userInfo.getDepId());
        return JsonResult.success(this.saleScheduleService.queryUser(inData), "提示：保存成功！");
    }


    @ApiOperation(value = "营业员列表条件查询",response = GetSalesSummaryOfSalesmenReportOutData.class)
    @PostMapping("querySalesList")
    public JsonResult querySalesSummaryOfSalesmenList(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
//        inData.setClientId("10000005");
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.querySalesSummaryOfSalesmenList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "营业员销售明细条件查询(门店用)",response = SalespersonsSalesDetailsOutData.class)
    @PostMapping("getSalespersonsSalesDetails")
    public JsonResult getSalespersonsSalesDetails(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.getSalespersonsSalesDetails(inData), "提示：获取数据成功！");
    }

    /**
     * 商品销售明细表导出
     * @param request
     * @param inData
     * @return
     */
    @PostMapping("exportSalespersonsSalesDetails")
    public void exportSalespersonsSalesDetails(HttpServletRequest request, HttpServletResponse response, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.salesSummaryOfSalesmenReportService.exportSalespersonsSalesDetails(inData,response);
    }

    @ApiOperation(value = "营业员销售明细条件查询(加盟商用)",response = SalespersonsSalesDetailsOutData.class)
    @PostMapping("getSalespersonsSalesDetailsByClient")
    public JsonResult getSalespersonsSalesDetailsByClient(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.getSalespersonsSalesDetailsByClient(inData), "提示：获取数据成功！");
    }
//    @ApiOperation(value = "营业员销售明细条件查询(加盟商用)导出",response = SalespersonsSalesDetailsOutData.class)
//    @PostMapping("getSalespersonsSalesDetailsByClient")
//    public void getSalespersonsSalesDetailsByClientExport(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClientId(userInfo.getClient());
//        this.salesSummaryOfSalesmenReportService.getSalespersonsSalesDetailsByClient(inData);
////        return JsonResult.success(, "提示：获取数据成功！");
//    }

    @ApiOperation(value = "营业员销售明细按分类导出按商品",response = SalespersonsSalesDetailsOutData.class)
    @PostMapping("getSalespersonsSalesByPro")
    public JsonResult getSalespersonsSalesByClass(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.getSalespersonsSalesByPro(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "营业员销售明细按分类导出按人员",response = SalespersonsSalesDetailsOutData.class)
    @PostMapping("getSalespersonsSalesByUser")
    public JsonResult getSalespersonsSalesByUser(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.getSalespersonsSalesByUser(inData), "提示：获取数据成功！");
    }
    @ApiOperation(value = "营业员销售明细按分类导出按医生",response = SalespersonsSalesDetailsOutData.class)
    @PostMapping("getSalespersonsSalesByDoctor")
    public JsonResult getSalespersonsSalesByDoctor(HttpServletRequest request, @Valid @RequestBody GetSalesSummaryOfSalesmenReportInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.getSalespersonsSalesByDoctor(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "营业员列表查询",response = GetSalesSummaryOfSalesmenReportOutData.class)
    @PostMapping("selectSaleList")
    public JsonResult selectSaleList(HttpServletRequest request, @Valid @RequestBody Map<String,Object> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("clientId",userInfo.getClient());
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.selectSaleList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "供应商列表查询" ,response = SupplierInfoDTO.class)
    @PostMapping({"getSupByClient"})
    public JsonResult getSupByClient(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.salesSummaryOfSalesmenReportService.getSupByClient(userInfo.getClient()), "提示：获取数据成功！");
    }

}
