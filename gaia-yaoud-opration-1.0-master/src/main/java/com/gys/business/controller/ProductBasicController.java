//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import cn.hutool.core.date.DatePattern;
import com.gys.business.mapper.entity.GaiaProductBasicImage;
import com.gys.business.mapper.entity.GaiaSdNbMedicare;
import com.gys.business.service.ProductBasicService;
import com.gys.business.service.data.GetProductBaseInData;
import com.gys.business.service.data.GetProductInfoQueryInData;
import com.gys.business.service.data.GetProductInfoQueryOutData;
import com.gys.business.service.data.productBasic.GetProductBasicInData;
import com.gys.business.service.data.productBasic.GetProductBasicOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import com.gys.util.DateUtil;
import feign.Param;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Api(value = "商品相关接口ProductBasicController")
@RestController
@RequestMapping({"/productBasic/"})
public class ProductBasicController extends BaseController {
    @Autowired
    private ProductBasicService productBasicService;

    public ProductBasicController() {
    }

    @PostMapping({"/list"})
    public JsonResult list(HttpServletRequest request, @RequestBody GetProductBasicInData inData){
        GetLoginOutData loginUser = this.getLoginUser(request);
        inData.setClient(loginUser.getClient());
        return JsonResult.success(productBasicService.list(inData),"提示：获取数据成功！");
    }

    @PostMapping("/getOne")
    public JsonResult getOne(HttpServletRequest request, @RequestBody Map<String,String> proCode) {
        return productBasicService.getOne(proCode);
    }

    @PostMapping({"/edit"})
    public JsonResult edit(HttpServletRequest request, @RequestBody@Valid GetProductBasicOutData outData){
        GetLoginOutData loginUser = this.getLoginUser(request);
        outData.setUpdateClient(loginUser.getClient());
        outData.setUpdater(loginUser.getUserId());
        outData.setUpdateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_PATTERN)));
        return productBasicService.edit(outData);
    }

    @PostMapping("/list/export")
    public void export(HttpServletRequest request, HttpServletResponse response,@RequestBody GetProductBasicInData inData) {
        productBasicService.export(request,response,inData);
    }

    @PostMapping("/getUnitList")
    public JsonResult getUnitList() {
        return JsonResult.success(productBasicService.getUnitList(),"查询成功");
    }

    @PostMapping("/getTaxList")
    public JsonResult getTaxList(@RequestBody Map<String,String> map) {
        return JsonResult.success(productBasicService.getTaxList(map),"查询成功");
    }

    @ApiOperation(value = "商品信息列表查询",notes = "商品信息列表查询",response = GetProductInfoQueryOutData.class)
    @PostMapping({"/productBasicList"})
    public JsonResult getProductBasicList(HttpServletRequest request, @RequestBody GetProductInfoQueryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setGspp_br_id(userInfo.getDepId());
//        inData.setClient("21020006");
//        inData.setGspp_br_id("1000000001");
        return JsonResult.success(this.productBasicService.getProductBasicList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "药品养护商品信息列表查询",response = GetProductInfoQueryOutData.class)
    @PostMapping({"selectMedCheckProList"})
    public JsonResult selectMedCheckProList(HttpServletRequest request, @RequestBody GetProductInfoQueryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setGspp_br_id(userInfo.getDepId());
        List<GetProductInfoQueryOutData> list = this.productBasicService.selectMedCheckProList(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @PostMapping({"selectPhysicalProList"})
    public JsonResult selectPhysicalProList(HttpServletRequest request, @RequestBody GetProductInfoQueryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setGspp_br_id(userInfo.getDepId());
        List<GetProductInfoQueryOutData> list = this.productBasicService.selectPhysicalProList(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }



    @PostMapping({"getNBYBProCode"})
    public JsonResult getNBYBProCode(HttpServletRequest request, @RequestBody GetProductInfoQueryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setGspp_br_id(userInfo.getDepId());
        List<GaiaSdNbMedicare> list = this.productBasicService.getNBYBProCode(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }


    @ApiOperation(value = "上传图片校验")
    @PostMapping({"batchUploadCheck"})
    public JsonResult batchUploadCheck(HttpServletRequest request, @RequestBody List<GaiaProductBasicImage> imageList) {
        return this.productBasicService.batchUploadCheck(imageList);
    }

    @ApiOperation(value = "批量上传图片")
    @PostMapping({"batchUpload"})
    public JsonResult batchUpload(HttpServletRequest request, @RequestBody List<GaiaProductBasicImage> imageList) {
        return this.productBasicService.batchUpload(imageList);
    }

    @ApiOperation(value = "下载图片")
    @PostMapping({"downloadImage"})
    public JsonResult downloadImage(HttpServletRequest request, @RequestBody List<String> proCodes) {
        return this.productBasicService.downloadImage(proCodes);
    }

    @ApiOperation(value = "指定工号查询")
    @PostMapping({"appointUserQuery"})
    public JsonResult appointUserQuery() {
        return this.productBasicService.appointUserQuery();
    }
}
