package com.gys.business.controller;

import com.gys.business.service.GaiaChannelProductService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "渠道管理", description = "/channelProduct")
@RestController
@RequestMapping({"/channelProduct/"})
public class GaiaChannelProductController extends BaseController {

    @Autowired
    private HttpServletRequest request;
    @Resource
    private GaiaChannelProductService channelProductService;

    @ApiOperation(value = "查询默认渠道列表" ,response = GetAcceptOutData.class)
    @PostMapping({"listChannelDefault"})
    public JsonResult listChannelDefault(HttpServletRequest request, @RequestBody GaiaChannelProductInData inData) {
        return JsonResult.success(this.channelProductService.listChannelDefault(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "新增门店关联渠道", response = GetAcceptOutData.class)
    @PostMapping({"insertGaiaChannelData"})
    public JsonResult insertGaiaChannelData(HttpServletRequest request, @RequestBody GaiaChannelData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.insertGaiaChannelData(loginUser,inData), "提示：编辑数据成功！");
    }

    @ApiOperation(value = "查询门店关联渠道", response = GetAcceptOutData.class)
    @PostMapping({"listGaiaChannelData"})
    public JsonResult listGaiaChannelData(HttpServletRequest request, @RequestBody GaiaChannelData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.listGaiaChannelData(loginUser,inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "批量导入-新增")
    @PostMapping("/batchImport")
    public JsonResult batchImport(MultipartFile file) {
        GetLoginOutData userInfo = getLoginUser(request);
        return channelProductService.importData(file, userInfo);
    }


    @ApiOperation(value = "批量导入-修改")
    @PostMapping("/importUpdate")
    public JsonResult importUpdate(MultipartFile file) {
        GetLoginOutData userInfo = getLoginUser(request);
        return channelProductService.batchImportUpdate(file, userInfo);
    }

    @ApiOperation(value = "删除门店关联渠道", response = GetAcceptOutData.class)
    @PostMapping({"deleteGaiaChannelData"})
    public JsonResult deleteGaiaChannelData(HttpServletRequest request, @RequestBody GaiaChannelData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.deleteGaiaChannelData(loginUser,inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "根据所选门店，渠道，药品查询相关的列表", response = GetAcceptOutData.class)
    @PostMapping({"listWaiteInsertData"})
    public JsonResult listWaiteInsertData(HttpServletRequest request, @RequestBody GaiaChannelProductInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.listWaiteInsertData(loginUser,inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "保存渠道-门店-药品信息", response = GetAcceptOutData.class)
    @PostMapping({"insertBatchChannelProduct"})
    public JsonResult insertBatchChannelProduct(HttpServletRequest request, @RequestBody GaiaChannelProductInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.insertBatchChannelProduct(loginUser,inData), "提示：编辑数据成功！");
    }

    @ApiOperation(value = "初始界面-查询所有列表", response = GetAcceptOutData.class)
    @PostMapping({"listChannelProduct"})
    public JsonResult listChannelProduct(HttpServletRequest request, @RequestBody GaiaChannelProductInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.listChannelProduct(loginUser,inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "初始界面-编辑信息", response = GetAcceptOutData.class)
    @PostMapping({"updateChannelProduct"})
    public JsonResult updateChannelProduct(HttpServletRequest request, @RequestBody GaiaChannelProduct inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.updateChannelProduct(loginUser,inData), "提示：编辑数据成功！");
    }

    @ApiOperation(value = "初始界面-删除信息", response = GetAcceptOutData.class)
    @PostMapping({"deleteChannelProduct"})
    public JsonResult deleteChannelProduct(HttpServletRequest request, @RequestBody GaiaChannelProduct inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.deleteChannelProduct(loginUser,inData), "提示：编辑数据成功！");
    }

    @ApiOperation(value = "初始界面-批量编辑信息", response = GetAcceptOutData.class)
    @PostMapping({"updateBatchChannelProduct"})
    public JsonResult updateBatchChannelProduct(HttpServletRequest request, @RequestBody List<GaiaChannelProduct> list) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.updateBatchChannelProduct(loginUser,list), "提示：编辑数据成功！");
    }

    @ApiOperation(value = "初始界面-批量删除信息", response = GetAcceptOutData.class)
    @PostMapping({"deleteBatchChannelProduct"})
    public JsonResult deleteBatchChannelProduct(HttpServletRequest request, @RequestBody List<GaiaChannelProduct> list) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.deleteBatchChannelProduct(loginUser,list), "提示：编辑数据成功！");
    }

    @ApiOperation(value = "初始界面-查询加盟商下有效门店", response = GetAcceptOutData.class)
    @PostMapping({"listStores"})
    public JsonResult listStores(HttpServletRequest request, @RequestBody GaiaChannelProduct inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.listStores(loginUser,inData), "提示：编辑数据成功！");
    }

    @ApiOperation(value = "初始界面-所有商品列表", response = GetAcceptOutData.class)
    @PostMapping({"listGoods"})
    public JsonResult listGoods(HttpServletRequest request, @RequestBody GaiaChannelProduct inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.channelProductService.listGoods(loginUser,inData), "提示：编辑数据成功！");
    }
}
