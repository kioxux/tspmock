package com.gys.business.controller;

import com.gys.business.service.GaiaSdMessageService;
import com.gys.business.service.UnqualifiedService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.Unqualified.UnqualifiedInDataParam;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Api(tags = "不合格品")
@RestController
@RequestMapping({"/unqualified/"})
public class UnqualifiedController extends BaseController {
    @Autowired
    private UnqualifiedService unqualifiedService;
    @Autowired
    private GaiaSdMessageService gaiaSdMessageService;


    @PostMapping({"/listUnqualified"})
    @ApiOperation(value = "门店不合格品汇总查询", response = UnqualifiedOutData.class)
    public JsonResult listUnqualified(HttpServletRequest request, @RequestBody UnqualifiedInDataParam inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(unqualifiedService.listUnqualified(userInfo,inData), "提示：获取数据成功！");
    }


    @PostMapping({"/listUnqualifiedHead"})
    @ApiOperation(value = "门店不合格品报损-选择门店不合格商品单", response = UnqualifiedOutData.class)
    public JsonResult listUnqualifiedHead(HttpServletRequest request, UnqualifiedInDataParam inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(unqualifiedService.listUnqualifiedHead(userInfo,inData), "提示：获取数据成功！");
    }

    @PostMapping({"listUnqualifiedDetailByBillNo"})
    @ApiOperation(value = "门店不合格品报损-根据选择的单据查询明细", response = UnqualifiedOutData.class)
    public JsonResult listUnqualifiedDetailByBillNo(HttpServletRequest request, @RequestBody UnqualifiedInDataParam inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(unqualifiedService.listUnqualifiedDetailByBillNo(userInfo,inData), "提示：获取数据成功！");
    }

    @PostMapping({"commitUnqualifiedBill"})
    @ApiOperation(value = "不合格品-报损报溢-提交审核", response = UnqualifiedOutData.class)
    public JsonResult commitUnqualifiedBill(HttpServletRequest request, @RequestBody UnqualifiedInDataParam inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(unqualifiedService.commitUnqualifiedBill(userInfo,inData), "提示：获取数据成功！");
    }

    @PostMapping({"checkTodoList"})
    @ApiOperation(value = "不合格品-报损报溢-待办-审核", response = UnqualifiedOutData.class)
    public JsonResult checkTodoList(HttpServletRequest request, @RequestBody UnqualifiedInDataParam inData) {
        return JsonResult.success(unqualifiedService.checkTodoList(inData), "提示：获取数据成功！");
    }

    //不合格品审核功能
    @ApiOperation(value = "门店不合格品记录列表",response = UnqualifiedOutData.class)
    @PostMapping({"unqualifiedList"})
    public JsonResult unqualifiedList(HttpServletRequest request, @RequestBody UnqualifiedInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.unqualifiedService.unqualifiedList(inData), "提示：获取数据成功！");
    }


    @ApiOperation(value = "门店不合格 批号有效期选择")
    @PostMapping({"checkSubstandard"})
    public JsonResult checkSubstandard(HttpServletRequest request, @RequestBody GaiaSdDefecProDInData vo) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        vo.setClientId(userInfo.getClient());
        vo.setBrId(userInfo.getDepId());
        return JsonResult.success(this.unqualifiedService.checkSubstandard(vo), "查询成功！");
    }

    @ApiOperation(value = "门店不合格 保存")
    @PostMapping({"addSubstandard"})
    public JsonResult addSubstandard(HttpServletRequest request, @RequestBody GaiaSdDefecProHInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setGsdhEmp(userInfo.getUserId());
        inData.setToken(userInfo.getToken());

        return JsonResult.success(this.unqualifiedService.addSubstandard(inData), "操作成功！");
    }

    //不合格品审核功能
    @ApiOperation(value = "门店不合格品商品审核")
    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request,@RequestBody GaiaSdDefecProHInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setGsdhEmp(userInfo.getUserId());
        inData.setToken(userInfo.getToken());
        this.unqualifiedService.approve(inData);
        return JsonResult.success("", "提示：审核成功！");
    }

    //门店不合格品商品删除行
    @ApiOperation(value = "门店不合格品商品删除行")
    @PostMapping({"removeDefecProD"})
    public JsonResult removeDefecProD(HttpServletRequest request,@RequestBody GaiaSdDefecProDInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        this.unqualifiedService.removeDefecProD(inData);
        return JsonResult.success("", "提示：删除成功！");
    }

    //门店不合格品商品删除行
    @ApiOperation(value = "门店不合格品商品详情列表查询",response = GaiaSdDefecProDInData.class)
    @PostMapping({"selectDefecProDetail"})
    public JsonResult selectDefecProDetail(HttpServletRequest request,@RequestBody GaiaSdDefecProDInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
//        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.unqualifiedService.selectDefecProDetail(inData), "提示：查询成功！");
    }

    //工作流审核后调用接口
    @ApiOperation(value = "工作流审核后调用接口")
    @PostMapping({"sendLossDefecpro"})
    public JsonResult sendLossDefecpro(@RequestBody GetWfApproveInData inData) {
//        GetLoginOutData userInfo = this.getLoginUser(request);
        this.unqualifiedService.sendLossDefecpro(inData);
        return JsonResult.success("", "提示：查询成功！");
    }

    //工作流审核后调用接口
    @ApiOperation(value = "")
    @GetMapping({"timerUpdateMessage"})
    public JsonResult timerUpdateMessage() {
//        GetLoginOutData userInfo = this.getLoginUser(request);
        this.gaiaSdMessageService.timerUpdateMessage();
        return JsonResult.success("", "提示：查询成功！");
    }
}
