package com.gys.business.controller;

import com.gys.business.service.GlobalDataService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:42 2021/8/25
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@RestController
@RequestMapping("globalDate")
public class GlobalDataController extends BaseController {

    @Autowired
    private GlobalDataService globalDataService;

    @GetMapping("exportGlobalType")
    public JsonResult exportGlobalType(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.globalDataService.exportGlobalType(userInfo.getClient(),userInfo.getDepId(),"5"),"提示：获取数据成功！");
    }
}
