package com.gys.business.controller.app.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/9 14:22
 **/
@Data
@ApiModel
public class DeleteForm {
    @ApiModelProperty(hidden = true)
    private String client;
    @ApiModelProperty(hidden = true)
    private String loginName;
    @ApiModelProperty(hidden = true)
    private String userId;
    @ApiModelProperty(value = "缺断货流水号", example = "DC92342902034234994F45")
    private String voucherId;
    @ApiModelProperty(value = "商品编码", example = "10008736")
    private String proSelfCode;
}
