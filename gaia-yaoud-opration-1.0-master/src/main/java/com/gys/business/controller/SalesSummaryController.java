package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSalesStoSummary;
import com.gys.business.mapper.entity.dto.GaiaStoreCategoryDropDownDTO;
import com.gys.business.service.SalesSummaryService;
import com.gys.business.service.data.ReportForms.StoreSaleDateInData;
import com.gys.business.service.data.ReportForms.WebStoreSaleDateInData;
import com.gys.business.service.data.SalesSummaryData;
import com.gys.business.service.data.StoreOutDatas;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 销售管理汇总查询
 *
 * @author xiaoyuan on 2020/7/24
 */
@Api(tags = "销售汇总查询")
@RestController
@RequestMapping({"/salesSummary"})
public class SalesSummaryController extends BaseController {

    @Autowired
    private SalesSummaryService salesSummaryService;

    /**
     * 查询列表
     * @param request
     * @param summaryData
     * @return
     */
    @ApiOperation(value = "列表查询/条件查询")
    @PostMapping("/selectSalesSummary")
    public JsonResult selectSalesSummary(HttpServletRequest request, @Valid @RequestBody SalesSummaryData summaryData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        summaryData.setClient(userInfo.getClient());
        summaryData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(salesSummaryService.findSalesSummary(summaryData),"返回成功");
    }

    /**
     * 根据加盟号,店号,查询用户
     * @param request
     * @return
     */
    @ApiOperation(value = "营业员查询")
    @PostMapping("/findUserByClientAndDepId")
    public JsonResult findUserByClientAndDepId(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(salesSummaryService.findUserByClientAndDepId(userInfo.getClient(),userInfo.getDepId()),"返回成功");
    }

    /**
     * 根据加盟号,店号,查询 分类
     * @param request
     * @return
     */
    @ApiOperation(value = "商品分类")
    @PostMapping("/findCategoriesByClientAndDepId")
    public JsonResult findCategoriesByClientAndDepId(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(salesSummaryService.findCategoriesByClientAndDepId(userInfo.getClient(),userInfo.getDepId()),"返回成功");
    }

    @ApiOperation(value = "门店销售列表标题头")
    @PostMapping("/selectStoreSalesSummaryTitle")
    public JsonResult selectStoreSalesSummaryTitle(HttpServletRequest request, @Valid @RequestBody SalesSummaryData summaryData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        summaryData.setClient(userInfo.getClient());
        return salesSummaryService.selectStoreSalesSummaryTitle(summaryData);
    }

    @ApiOperation(value = "门店销售汇总查询",response = GaiaSalesStoSummary.class)
    @PostMapping("/selectSalesSummaryStore")
    public JsonResult selectSalesSummaryByBrId(HttpServletRequest request, @Valid @RequestBody SalesSummaryData summaryData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        summaryData.setClient(userInfo.getClient());

//        summaryData.setClient("10000005");
        return JsonResult.success(salesSummaryService.findSalesSummaryByBrId(summaryData),"返回成功");
    }

    @ApiOperation(value = "日期销售汇总查询")
    @PostMapping("/selectSalesSummaryByDate")
    public JsonResult selectSalesSummaryByDate(HttpServletRequest request, @Valid @RequestBody StoreSaleDateInData summaryData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        summaryData.setClient(userInfo.getClient());

//        summaryData.setClient("10000005");
        return JsonResult.success(salesSummaryService.findSalesSummaryByDate(summaryData),"返回成功");
    }


    @ApiOperation(value = "门店列表查询")
    @PostMapping("/selectStoreList")
    public JsonResult selectStoreList(HttpServletRequest request, @Valid @RequestBody Map<String,Object> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("clientId",userInfo.getClient());
        inData.put("stoCode",userInfo.getDepId());
        return JsonResult.success(salesSummaryService.selectStoreList(inData),"返回成功");
    }

    @ApiOperation(value = "门店报表 销售商品汇总 销售等级 商品自定义1 2 3 查询")
    @PostMapping("/salesgrade")
    public JsonResult salesgrade(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(salesSummaryService.salesgrade(userInfo),"返回成功");
    }
    @ApiOperation(value = "WEB端报表(用户)")
    @PostMapping("/selectWebSalesSummaryByDate")
    public JsonResult selectWebSalesSummaryByDate(HttpServletRequest request, @Valid @RequestBody WebStoreSaleDateInData data){
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        data.setUserId(userInfo.getUserId());
        return JsonResult.success(salesSummaryService.selectWebSalesSummaryByDate(data),"数据获取成功");
    }
    @ApiOperation(value = "WEB端报表导出")
    @PostMapping("/exportSalesSummary")
    public Result exportSalesSummary(HttpServletRequest request, @Valid @RequestBody WebStoreSaleDateInData data){
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        data.setUserId(userInfo.getUserId());
        return salesSummaryService.exportSalesSummary(data);
    }
    @ApiOperation(value = "用户下门店")
    @GetMapping({"/selectAuthStoreList"})
    public JsonResult selectAuthStoreList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<StoreOutDatas> list = this.salesSummaryService.selectAuthStoreList(userInfo.getClient(), userInfo.getUserId());
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询门店分类筛选条件")
    @GetMapping({"/selectStoreCategoryCondition"})
    public JsonResult selectStoreCategoryCondition(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return salesSummaryService.selectStoreCategoryCondition(userInfo.getClient());
    }

    @ApiOperation(value = "查询门店分类下拉")
    @PostMapping({"/selectStoreCategoryDropdown"})
    public JsonResult selectStoreCategoryDropdown(@RequestBody GaiaStoreCategoryDropDownDTO gaiaStoreCategoryDropDownDTO, HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return salesSummaryService.selectStoreCategoryDropdown(userInfo.getClient(), gaiaStoreCategoryDropDownDTO);
    }

}
