package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.gys.business.mapper.entity.DistributionPlanDetail;
import com.gys.business.mapper.entity.NewDistributionPlan;
import com.gys.business.service.DistributionPlanDetailService;
import com.gys.business.service.NewDistributionPlanService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.enums.DistributionPlanEnum;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/***
 * @desc: 新品铺货相关
 * @author: ryan
 * @createTime: 2021/6/2 15:04
 **/
@Api(tags = "新品铺货业务模块")
@RestController
@RequestMapping("/api/distributionPlan")
public class DistributionPlanController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(DistributionPlanController.class);

    @Resource
    private NewDistributionPlanService newDistributionPlanService;
    @Resource
    private DistributionPlanDetailService distributionPlanDetailService;

    @ApiOperation(value = "新品铺货列表",response = DistributionPlanVO.class )
    @PostMapping("list")
    public JsonResult<List<DistributionPlanVO>> list(HttpServletRequest request, @RequestBody DistributionPlanForm distributionPlanForm) {
        logger.info("<新品铺货><计划列表><请求参数：>" + JSONUtil.toJsonStr(distributionPlanForm));
        GetLoginOutData userData = getLoginUser(request);
        distributionPlanForm.setClient(userData.getClient());
        List<DistributionPlanVO> list = newDistributionPlanService.findPage(distributionPlanForm);
        return JsonResult.success(list, "成功");
    }

    @ApiOperation(value = "新品铺货计划明细", response = DistributionPlanDetailVO.class)
    @PostMapping("detail")
    public JsonResult<List<DistributionPlanDetailVO>> detail(HttpServletRequest request, @RequestBody DistributionOperateForm operateForm) {
        logger.info("<新品铺货><计划明细><请求参数：>" + JSONUtil.toJsonStr(operateForm));
        validOperateForm(operateForm);
        GetLoginOutData loginUser = getLoginUser(request);
        DistributionPlanDetail cond = new DistributionPlanDetail();
        cond.setClient(loginUser.getClient());
        cond.setPlanCode(operateForm.getPlanCode());
        List<DistributionPlanDetailVO> detailList = distributionPlanDetailService.getPlanDetail(cond);
        return JsonResult.success(detailList, "查询成功");
    }

    @ApiOperation(value = "调取操作")
    @PostMapping("preCall")
    public JsonResult preCall(HttpServletRequest request, @RequestBody DistributionOperateForm operateForm) {
        logger.info("<新品铺货><调取操作><请求参数：>" + JSONUtil.toJsonStr(operateForm));
        NewDistributionPlan distributionPlan = getNewDistributionPlan(request, operateForm);
        BigDecimal quantity = newDistributionPlanService.getWmsRkysQuantity(distributionPlan);
        if (BigDecimal.ZERO.compareTo(quantity) >= 0) {
            return JsonResult.success("", "无新品入库，暂时无法调取铺货");
        }
        BigDecimal needQuantity = distributionPlan.getPlanQuantity().subtract(distributionPlan.getActualQuantity()).stripTrailingZeros();
        return JsonResult.success(null, String.format("%s-%s，新品铺货计划量%s，新品入库数量%s，是否执行新品铺货？", distributionPlan.getProSelfCode(), distributionPlan.getProSelfName(),
                needQuantity.toPlainString(), quantity.stripTrailingZeros().toPlainString()));
    }

    @ApiOperation(value = "确认调取")
    @PostMapping("confirmCall")
    public JsonResult confirmCall(HttpServletRequest request, @RequestBody DistributionOperateForm operateForm) {
        logger.info("<新品铺货><确认调取><请求参数：>" + JSONUtil.toJsonStr(operateForm));
        validOperateForm(operateForm);
        NewDistributionPlan distributionPlan = getNewDistributionPlan(request, operateForm);
        newDistributionPlanService.confirmCall(distributionPlan);
        return JsonResult.success(null, "调取成功");
    }

    @ApiOperation(value = "补铺操作", response = PreReplenishVO.class)
    @PostMapping("preReplenish")
    public JsonResult<List<PreReplenishVO>> preReplenish(HttpServletRequest request, @RequestBody DistributionOperateForm operateForm) {
        logger.info("<新品铺货><补铺操作><请求参数：>" + JSONUtil.toJsonStr(operateForm));
        validOperateForm(operateForm);
        NewDistributionPlan distributionPlan = getNewDistributionPlan(request, operateForm);
        //判断库存是否充足
        if (!newDistributionPlanService.validStock(distributionPlan)) {
            return JsonResult.success(null, String.format("%s-%s，需铺%s个，仓库库存不足，补铺失败，可补足库存后，在【门店铺货】手工补铺。",
                    distributionPlan.getProSelfCode(), distributionPlan.getProSelfName(), distributionPlan.getPlanQuantity().subtract(distributionPlan.getActualQuantity()).stripTrailingZeros().toPlainString()));
        }
        List<PreReplenishVO> list = newDistributionPlanService.preReplenish(distributionPlan);
        return JsonResult.success(list, "成功");
    }

    @ApiOperation(value = "确认补铺")
    @PostMapping("confirmReplenish")
    public JsonResult confirmReplenish(HttpServletRequest request, @RequestBody DistributionOperateForm operateForm) {
        logger.info("<新品铺货><确认补铺><请求参数：>" + JSONUtil.toJsonStr(operateForm));
        validOperateForm(operateForm);
        NewDistributionPlan distributionPlan = getNewDistributionPlan(request, operateForm);
        newDistributionPlanService.confirmReplenish(distributionPlan);
        return JsonResult.success(null, "补铺成功");
    }

    @ApiOperation(value = "取消补铺")
    @PostMapping("cancelReplenish")
    public JsonResult cancelReplenish(HttpServletRequest request, @RequestBody DistributionOperateForm operateForm) {
        logger.info("<新品铺货><取消补铺><请求参数：>" + JSONUtil.toJsonStr(operateForm));
        validOperateForm(operateForm);
        NewDistributionPlan distributionPlan = getNewDistributionPlan(request, operateForm);
        distributionPlan.setStatus(DistributionPlanEnum.CANCELED.status);
        distributionPlan.setUpdateTime(new Date());
        newDistributionPlanService.update(distributionPlan);
        return JsonResult.success(null, "新品铺货计划取消成功");
    }

    private void validOperateForm(DistributionOperateForm operateForm) {
        if (StringUtils.isEmpty(operateForm.getPlanCode())) {
            throw new BusinessException("铺货计划单号不能为空");
        }
    }

    private NewDistributionPlan getNewDistributionPlan(HttpServletRequest request, DistributionOperateForm operateForm) {
        GetLoginOutData loginUser = getLoginUser(request);
        NewDistributionPlan cond = new NewDistributionPlan();
        cond.setClient(loginUser.getClient());
        cond.setPlanCode(operateForm.getPlanCode());
        NewDistributionPlan distributionPlan = newDistributionPlanService.getUnique(cond);
        distributionPlan.setUpdateUser(loginUser.getLoginName() + "|" + loginUser.getUserId());
        return distributionPlan;
    }
}
