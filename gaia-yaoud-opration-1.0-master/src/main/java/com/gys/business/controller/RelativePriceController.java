package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaPriceGet;
import com.gys.business.mapper.entity.GaiaRepPrice;
import com.gys.business.mapper.entity.GaiaSdReplenishD;
import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.service.PointsDetailsQueryService;
import com.gys.business.service.RelativePriceService;
import com.gys.business.service.data.PointsDetailsQueryInData;
import com.gys.business.service.data.PriceGetInData;
import com.gys.business.service.data.QueryLoginInfoInData;
import com.gys.business.service.data.RepPriceSupplierCheckData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "比价")
@RestController
@RequestMapping({"/relativePrice"})
public class RelativePriceController extends BaseController {

    @Autowired
    private RelativePriceService relativePriceService;

    @ApiOperation(value = "查询补货表中门店的未比价数据")
    @PostMapping("/queryReplenishData")
    public JsonResult queryReplenishData(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(relativePriceService.queryReplenishData(userInfo.getClient(),userInfo.getDepId()),"查询成功");
    }

    @ApiOperation(value = "查询补货表中连锁的未比价数据")
    @PostMapping("/queryChainReplenishData")
    public JsonResult queryChainReplenishData(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(relativePriceService.queryChainReplenishData(userInfo.getClient(),userInfo.getDepId()),"查询成功");
    }

    @ApiOperation(value = "批量插入比价信息")
    @PostMapping("/insertPriceGetBatch")
    public JsonResult insertPriceGetBatch(HttpServletRequest request, @RequestBody PriceGetInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(relativePriceService.insertPriceGet(inData),"查询成功");
    }

    @ApiOperation(value = "更新补货单状态位")
    @PostMapping("/updateReplenishHGetStatus")
    public JsonResult updateReplenishHGetStatus(HttpServletRequest request, @RequestBody GaiaSdReplenishH gaiaSdReplenishH){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        queryInData.setClient(userInfo.getClient());
//        queryInData.setBrId(userInfo.getDepId());
        return JsonResult.success(relativePriceService.updateReplenishHGetStatus(gaiaSdReplenishH),"查询成功");
    }

    @ApiOperation(value = "更新比价单状态位")
    @PostMapping("/updateRepPriceStatus")
    public JsonResult updateRepPriceStatus(HttpServletRequest request, @RequestBody GaiaRepPrice pgId){
        GetLoginOutData userInfo = this.getLoginUser(request);
//        queryInData.setClient(userInfo.getClient());
//        queryInData.setBrId(userInfo.getDepId());
        return JsonResult.success(relativePriceService.updateRepPriceStatus(userInfo.getClient(),userInfo.getDepId(),pgId.getRepPgId()),"查询成功");
    }

    @ApiOperation(value = "查询门店供应商网站信息")
    @PostMapping("/querySupplierInfo")
    public JsonResult querySupplierInfo(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(relativePriceService.querySupplierInfo(userInfo.getClient(),userInfo.getDepId()),"查询成功");
    }

    @ApiOperation(value = "查询连锁供应商网站信息")
    @PostMapping("/queryChainSupplierInfo")
    public JsonResult queryChainSupplierInfo(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(relativePriceService.queryChainSupplierInfo(userInfo.getClient(),userInfo.getDepId()),"查询成功");
    }

    @ApiOperation(value = "检查供应商爬虫数据")
    @PostMapping("/checkSupplierDataByPgId")
    public JsonResult checkSupplierDataByPgId(HttpServletRequest request, @RequestBody RepPriceSupplierCheckData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(relativePriceService.checkSupplierDataByPgId(userInfo.getClient(),userInfo.getDepId(),inData),"查询成功");
    }

    @ApiOperation(value = "从redis查询当前用户下供应商网站的登录信息")
    @PostMapping("/queryLoginInfoFromRedis")
    public JsonResult queryLoginInfoFromRedis(HttpServletRequest request, @RequestBody QueryLoginInfoInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(relativePriceService.queryLoginInfoFromRedis(inData),"查询成功");
    }

    @ApiOperation(value = "插入当前用户下供应商网站的登录信息到redis")
    @PostMapping("/insertLoginInfoToRedis")
    public JsonResult insertLoginInfoToRedis(HttpServletRequest request, @RequestBody QueryLoginInfoInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(relativePriceService.insertLoginInfoToRedis(inData),"新增成功");
    }
}
