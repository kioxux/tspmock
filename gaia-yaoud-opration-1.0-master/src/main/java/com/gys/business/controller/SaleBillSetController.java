
package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdPrintSale;
import com.gys.business.service.SaleBillSetService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "销售小票设置")
@RestController
@RequestMapping({"/saleBillSet/"})
public class SaleBillSetController extends BaseController {
    @Autowired
    private SaleBillSetService saleBillSetService;


    @ApiOperation(value = "小票保存")
    @PostMapping({"save"})
    public JsonResult save(HttpServletRequest request, @RequestBody GaiaSdPrintSale inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGspsBrId(userInfo.getDepId());
        this.saleBillSetService.save(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "小票查询")
    @PostMapping({"get"})
    public JsonResult get(HttpServletRequest request) {
        GaiaSdPrintSale inData = new GaiaSdPrintSale();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGspsBrId(userInfo.getDepId());
        return JsonResult.success(this.saleBillSetService.get(inData), "提示：保存成功！");
    }
}
