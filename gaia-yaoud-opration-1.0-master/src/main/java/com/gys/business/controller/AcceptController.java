package com.gys.business.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.entity.GaiaTaxCode;
import com.gys.business.service.AcceptService;
import com.gys.business.service.data.*;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "收货相关", description = "/accept")
@RestController
@RequestMapping({"/accept/"})
public class AcceptController extends BaseController {
    @Resource
    private AcceptService acceptService;

    @ApiOperation(value = "查询收货单" ,response = GetAcceptOutData.class)
    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @ApiParam(value = "查询条件", required = true)@RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        System.out.println("这是第二个");
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.selectList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"supplierAcceptList"})
    public JsonResult supplierAcceptList(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.supplierAcceptList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "调取配送单",response = GetDiaoboOutData.class)
    @PostMapping({"diaoboList"})
    public JsonResult diaoboList(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.diaoboList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "订单商品列表查询",response = GetPoOutData.class)
    @PostMapping({"poList"})
    public JsonResult poList(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.poList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "订单商品详情查询",response = GetPoDetailOutData.class)
    @PostMapping({"poDetailList"})
    public JsonResult poDetailList(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.poDetailList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询收货明细", response = GetAcceptDetailOutData.class)
    @PostMapping({"detailList"})
    public JsonResult detailList(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.detailList(inData), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "保存收货单")
    @PostMapping({"save"})
    public JsonResult save(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.save(inData), "提示：保存成功！");
    }

    @FrequencyValid
    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        this.acceptService.insert(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "收货供应商审核")
    @PostMapping({"insertPo"})
    public JsonResult insertPo(HttpServletRequest request, @RequestBody GetPoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.insertPo(inData, userInfo), "提示：保存成功！");
    }

    @FrequencyValid
    @PostMapping({"savePo"})
    public JsonResult savePo(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        this.acceptService.savePo(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "审核")
    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        this.acceptService.approve(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "审核")
    @PostMapping({"approvePS"})
    public JsonResult approvePS(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        inData.setStoreCode(userInfo.getDepId());
        this.acceptService.approvePS(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"proInfo"})
    public JsonResult proInfo(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.proInfo(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "税率" ,response = GaiaTaxCode.class)
    @PostMapping({"getTax"})
    public JsonResult getTax(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.getTax(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "供应商列表查询（单门店）" ,response = GetSupOutData.class)
    @PostMapping({"getSup"})
    public JsonResult getSup(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.acceptService.getSup(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value ="商品查询" ,response = GetQueryProductOutData.class)
    @PostMapping({"queryAcceptProduct"})
    public JsonResult queryAcceptProduct(HttpServletRequest request, @RequestBody @ApiParam GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.queryAcceptProduct(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value ="商品库存信息查询" ,response = GetQueryProductOutData.class)
    @PostMapping({"queryAcceptProductStock"})
    public JsonResult queryAcceptProductStock(HttpServletRequest request, @RequestBody @ApiParam GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.queryAcceptProductStock(inData), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "web端审核")
    @PostMapping({"approvePo"})
    public JsonResult approvePo(HttpServletRequest request, @RequestBody GetPoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(inData.getSupCode());
        inData.setStoreId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.approvePo(inData, userInfo), "提示：保存成功！");
    }

    @ApiOperation(value = "供应商列表查询（多门店）" ,response = GetSupOutData.class)
    @PostMapping({"getSupByBrIds"})
    public JsonResult getSupByBrIds(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        if(inData.getStoreCodes() != null && inData.getStoreCodes().length > 0){
            if (ObjectUtil.isEmpty(inData.getStoreCodes()[0])){
                inData.setStoreCodes(null);
            }
        }
        return JsonResult.success(this.acceptService.getSupByBrIds(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "供应商列表查询（模糊查询）" ,response = GetSupOutData.class)
    @PostMapping({"getSupByContent"})
    public JsonResult getSupByContent(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());

        return JsonResult.success(this.acceptService.getSupByContent(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询收货明细", response = AcceptOrderTotalOutData.class)
    @PostMapping({"acceptOrderList"})
    public JsonResult acceptOrderList(HttpServletRequest request, @RequestBody AcceptOrderInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.acceptService.acceptOrderList(inData,userInfo), "提示：获取数据成功！");
    }

    @ApiOperation(value = "调取驳回单接口",response = GetAcceptOutData.class)
    @PostMapping({"rejectList"})
    public JsonResult acceptList(HttpServletRequest request) {
        GetExamineInData inData = new GetExamineInData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsehBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.rejectList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "驳回单详情查询",response = GetPoInData.class)
    @PostMapping({"rejectDetail"})
    public JsonResult rejectDetail(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        inData.setUserCode(userInfo.getUserId());
        inData.setUserName(userInfo.getLoginName());
        return JsonResult.success(this.acceptService.rejectDetail(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "补货单暂存至redis", notes = "补货单暂存至redis")
    @PostMapping({"insertToRedis"})
    public JsonResult insertToRedis(HttpServletRequest request, @RequestBody GetPoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreId(userInfo.getDepId());
        this.acceptService.insertToRedis(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "获取已暂存的补货单", notes = "获取已暂存的补货单",response = GetPoInData.class)
    @PostMapping({"selectFromRedis"})
    public JsonResult selectFromRedis(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.acceptService.selectFromRedis(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查看当天该门店是否有未完成数据采集", notes = "查看当天该门店是否有未完成数据采集")
    @PostMapping({"hasRedisOrNot"})
    public JsonResult hasRedisOrNot(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoreCode(userInfo.getDepId());
        return JsonResult.success(this.acceptService.hasRedisOrNot(inData), "提示：保存成功！");
    }

    @ApiOperation(value = "门店收货CS导入",response = GetPoInData.class)
    @PostMapping({"getImportExcelDetailList"})
    public Result getImportExcelDetailList(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return acceptService.getImportExcelDetailList(request,file,userInfo);
    }

    @ApiOperation(value = "委托配送导入信息查询",response = GetReplenishDetailOutData.class)
    @GetMapping({"exportExcelModel"})
    public Result exportExcelModel(HttpServletRequest request) {
        List<ImportAcceptInData> list = new ArrayList<>();
        return acceptService.exportErrorAcceptPro(list,1);
    }

    @ApiOperation(value = "委托配送商品编码未匹配",response = GetReplenishDetailOutData.class)
    @GetMapping({"checkProSelfCode"})
    public void checkProSelfCode(HttpServletRequest request) {
        acceptService.checkProSelfCode();
    }

    @ApiOperation(value = "委托配送商品编码未匹配")
    @PostMapping({"listUnMatchInfo"})
    public JsonResult listUnMatchInfo(HttpServletRequest request,@RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.acceptService.listUnMatchInfo(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "调取采购订单",response = PoOrderOutData.class)
    @PostMapping({"selectOrderList"})
    public JsonResult selectOrderList(HttpServletRequest request,@RequestBody AcceptOrderInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.acceptService.selectOrderList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询供应商列表",response = PoOrderOutData.class)
    @PostMapping({"selectSupplierList"})
    public JsonResult selectSupplierList(HttpServletRequest request,@RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return JsonResult.success(this.acceptService.selectSupplierList(inData), "提示：获取数据成功！");
    }


    @ApiOperation(value = "查询单体店是否显示发票信息")
    @PostMapping({"selectInvoiceShowFlag"})
    public JsonResult selectInvoiceShowFlag(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return acceptService.selectInvoiceShowFlag(userInfo);
    }

    @ApiOperation(value = "查询营业员下拉列表")
    @PostMapping({"selectBusEmpList"})
    public JsonResult selectBusEmpList(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        return acceptService.getBusEmpListBySupSelfCode(inData);
    }


    @ApiOperation(value = "供应商下拉列表查询")
    @PostMapping({"selectSupplierListByStoCode"})
    public JsonResult selectSupplierListByStoCode(HttpServletRequest request, @RequestBody PoPriceProductHistoryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return acceptService.selectSupplierListByStoCode(inData);
    }


    @ApiOperation(value = "根据商品编码查询历史进价")
    @PostMapping({"getPoPriceProductHistory"})
    public JsonResult getPoPriceProductHistory(HttpServletRequest request, @RequestBody PoPriceProductHistoryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return acceptService.getPoPriceProductHistory(inData);
    }


    @ApiOperation(value = "计算商品生产日期")
    @PostMapping({"getProductCreateDate"})
    public JsonResult getProductCreateDate(HttpServletRequest request, @RequestBody PoPriceProductHistoryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return acceptService.getProductCreateDate(inData);
    }

    @FrequencyValid
    @ApiOperation(value = "驳回单作废功能")
    @PostMapping({"deleteAcceptOreder"})
    public JsonResult deleteAcceptOreder(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsahBrId(userInfo.getDepId());
        this.acceptService.rejectAcceptOreder(inData);
        return JsonResult.success("", "提示：操作成功！");
    }

    @PostMapping({"getCreateDateOrValidityDate"})
    public JsonResult getCreateDateOrValidityDate(HttpServletRequest request, @RequestBody PoPriceProductHistoryInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(acceptService.getCreateDateOrValidityDate(inData),"提示：操作成功！");
    }

}
