package com.gys.business.controller;

import com.gys.business.service.OrderService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 订购
 *
 * @author xiaoyuan on 2020/12/23
 */
@RestController
@RequestMapping({"/order/"})
@Api(tags = "订购")
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;


    /**
     * 商品查询（本地库/全国库）
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"getProductData"})
    public JsonResult getProductData(HttpServletRequest request, @RequestBody OrderInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<OrderOutData> outData = this.orderService.getProductData(userInfo,inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    /**
     * 获取支付类型方式
     * @param request
     * @return
     */
    @PostMapping({"payList"})
    public JsonResult payList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<OrderPayOutData> outData = this.orderService.getPayList(userInfo);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    /**
     * 订购记录查询
     * @param request
     * @return
     */
    @PostMapping({"getOrderRecord"})
    public JsonResult getOrderRecord(HttpServletRequest request, @Valid @RequestBody OrderRecordInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<OrderRecordOutData> outData = this.orderService.getOrderRecordList(userInfo,inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    /**
     * 总部订购记录
     * @param request
     * @return
     */
    @PostMapping({"headquartersRecords"})
    @ApiOperation(value = "总部订购记录",response = OrderRecordOutData.class)
    public JsonResult headquartersRecords(HttpServletRequest request, @RequestBody HeadquartersRecordsData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<OrderRecordOutData> outData = this.orderService.headquartersRecords(userInfo,inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    /**
     * 订购详情,退货查询
     * @param request
     * @return
     */
    @PostMapping({"getOrderDetails"})
    @ApiOperation(value = "订购详情",response = OrderAll.class)
    public JsonResult getOrderDetailsList(HttpServletRequest request,@RequestBody OrderInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        OrderAll outData = this.orderService.getOrderDetailsList(userInfo,inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    /**
     * 订单保存
     * @param request
     * @return
     */
    @PostMapping({"save"})
    public JsonResult save(HttpServletRequest request, @Valid @RequestBody OrderSaveInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.orderService.save(userInfo,inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }


    /**
     * 核销订单保存
     * @param request
     * @return
     */
    @PostMapping({"writeOffAndSave"})
    public JsonResult writeOffAndSave(HttpServletRequest request, @Valid @RequestBody OrderSaveInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.orderService.writeOffAndSave(userInfo,inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    /**
     * 核销退货
     * @param request
     * @return
     */
    @PostMapping({"returnOrder"})
    public JsonResult returnOrder(HttpServletRequest request, @Valid @RequestBody OrderSaveInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.orderService.returnOrder(userInfo,inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }
}
