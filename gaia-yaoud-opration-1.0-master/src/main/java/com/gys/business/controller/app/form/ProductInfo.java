package com.gys.business.controller.app.form;

import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/21 17:00
 */
@Data
public class ProductInfo {
    private String dcCode;
    private String proSelfCode;
}
