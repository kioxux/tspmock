package com.gys.business.controller;

import com.gys.business.service.DsfPordWtproService;
import com.gys.business.service.data.DsfPordWtproInData;
import com.gys.business.service.data.DsfPordWtproModInData;
import com.gys.business.service.data.DsfPordWtproModReqInData;
import com.gys.business.service.data.DsfPordWtproOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 委托配送商品对码
 */
@RestController
@RequestMapping("/dsfPordWtpro/")
public class DsfPordWtproController extends BaseController {

    @Resource
    private DsfPordWtproService dsfPordWtproService;

    @ApiOperation(value = "委托配送商品对码列表接口",response = DsfPordWtproOutData.class)
    @PostMapping("getDsfPordWtproList")
    public JsonResult getDsfPordWtproList(HttpServletRequest request,  @Valid @RequestBody DsfPordWtproInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(dsfPordWtproService.selectDsfPordWtproList(inData),"success");
    }

    @ApiOperation(value = "委托配送商品对码接口")
    @PostMapping({"updateWtpro"})
    public JsonResult updateWtpro(HttpServletRequest request, @Valid @RequestBody List<DsfPordWtproModReqInData> inData){
        DsfPordWtproModInData dsfPordWtproModInData = new DsfPordWtproModInData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        dsfPordWtproModInData.setClient(userInfo.getClient());
        dsfPordWtproModInData.setList(inData);
        dsfPordWtproService.updateWtpro(dsfPordWtproModInData);
        return JsonResult.success("","提示：保存成功！");
    }

    @ApiOperation(value = "单据未对码委托配送商品对码列表接口",response = DsfPordWtproOutData.class)
    @PostMapping("getDsfPordWtpsdDList")
    public JsonResult getDsfPordWtpsdList(HttpServletRequest request,  @Valid @RequestBody DsfPordWtproInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(dsfPordWtproService.selectDsfPordWtpsdList(inData),"success");
    }
}