package com.gys.business.controller;

import com.gys.business.service.GaiaStoreInSuggestionDService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 门店调入建议-明细表(GaiaStoreInSuggestionD)表控制层
 *
 * @author XIAO-ZY
 * @since 2021-10-28 10:52:53
 */
@RestController
@RequestMapping("gaiaStoreInSuggestionD")
public class GaiaStoreInSuggestionDController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaStoreInSuggestionDService gaiaStoreInSuggestionDService;


}

