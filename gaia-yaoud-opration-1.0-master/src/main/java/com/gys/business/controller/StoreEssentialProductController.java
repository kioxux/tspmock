package com.gys.business.controller;

import com.gys.business.service.StoreEssentialProductService;
import com.gys.business.service.data.EssentialProductCalculationInData;
import com.gys.business.service.data.SaveEssentialProductInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author SunJiaNan
 * @version 1.0
 * @date 2021/10/14 11:01
 * @description 门店必备商品相关接口
 */
@Slf4j
@Api(tags = "门店必备商品")
@RestController
@RequestMapping("/essentialProduct")
public class StoreEssentialProductController extends BaseController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private StoreEssentialProductService storeEssentialProductService;


    @ApiOperation(value = "铺货计算")
    @PostMapping("/getCalculationList")
    public JsonResult getCalculationList(@RequestBody EssentialProductCalculationInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return storeEssentialProductService.getCalculationList(inData);
    }


    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public JsonResult save(@RequestBody SaveEssentialProductInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return storeEssentialProductService.saveOrUpdate(inData);
    }


    @ApiOperation(value = "铺货确认")
    @PostMapping("/confirm")
    public JsonResult confirm(@RequestBody SaveEssentialProductInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return storeEssentialProductService.confirm(inData);
    }


    @ApiOperation(value = "数据来源-下拉列表")
    @PostMapping("/getSourceDateList")
    public JsonResult getSourceDateList() {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return storeEssentialProductService.getSourceDateList(userInfo);
    }


    @ApiOperation(value = "门店-下拉列表")
    @PostMapping("/getStoreList")
    public JsonResult getStoreList(@RequestBody EssentialProductCalculationInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return storeEssentialProductService.getStoreList(inData);
    }


    @ApiOperation(value = "铺货记录查询")
    @PostMapping("/getOrderList")
    public JsonResult getOrderList(@RequestBody EssentialProductCalculationInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return storeEssentialProductService.getOrderList(inData);
    }


    @ApiOperation(value = "铺货明细查询")
    @PostMapping("/getOrderDetailList")
    public JsonResult getOrderDetailList(@RequestBody EssentialProductCalculationInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return storeEssentialProductService.getOrderDetailList(inData);
    }


    @ApiOperation(value = "导出铺货明细查询")
    @PostMapping("/export")
    public Result export(@RequestBody EssentialProductCalculationInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return storeEssentialProductService.export(inData);
    }


    @ApiOperation(value = "获取铺货计算按钮置灰标志")
    @PostMapping("/getDisableFlag")
    public JsonResult getDisableFlag() {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return storeEssentialProductService.getDisableFlag(userInfo.getClient());
    }
}
