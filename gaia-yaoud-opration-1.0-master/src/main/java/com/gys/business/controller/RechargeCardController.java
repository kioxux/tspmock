package com.gys.business.controller;

import com.gys.business.service.RechargeCardService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/rechargecard"})
@Slf4j
public class RechargeCardController extends BaseController {
    @Resource
    private RechargeCardService rechargeCardService;

    @PostMapping({"/saveset"})
    public JsonResult saveSet(HttpServletRequest request, @RequestBody RechargeCardSetInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        rechargeCardService.saveSet(inData, userInfo);
        return JsonResult.success(null, "提示：保存成功！");
    }

    @PostMapping({"/save"})
    public JsonResult saveCardInfo(HttpServletRequest request, @RequestBody RechargeCardInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.rechargeCardService.saveCardInfo(inData, userInfo);
        return JsonResult.success((Object)null, "提示：更新成功！");
    }

    @PostMapping({"/generatecode"})
    public JsonResult generateCardCode(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String cardCode = this.rechargeCardService.generateCardCode(userInfo);
        return JsonResult.success(cardCode, "提示：获取数据成功！");
    }

    @PostMapping({"/resetpassword"})
    public JsonResult resetPassword(@RequestBody RechargeCardInData inData, HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.rechargeCardService.resetPassword(inData, userInfo);
        return JsonResult.success((Object)null, "提示：更新成功！");
    }

    @PostMapping({"/modifypassword"})
    public JsonResult modifyPassword(@RequestBody RechargeCardInData inData, HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.rechargeCardService.modifyPassword(inData, userInfo);
        return JsonResult.success((Object)null, "提示：更新成功！");
    }

    @PostMapping({"/savepayinfo"})
    public JsonResult savePayInfo(@RequestBody RechargeCardPaycheckInData inData, HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.rechargeCardService.savePayInfo(inData, userInfo);
        return JsonResult.success((Object)null, "提示：更新成功！");
    }

    @PostMapping({"/cardlist"})
    public JsonResult getCardList(HttpServletRequest request, @RequestBody RechargeCardInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.rechargeCardService.getCardList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/rechcomsu/list"})
    public JsonResult getRechComsuList(HttpServletRequest request, @RequestBody RechComsuInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoNo(userInfo.getDepId());
        return JsonResult.success(this.rechargeCardService.getRechComsuList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/setData"})
    public JsonResult getSetData(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.rechargeCardService.getSetData(userInfo), "提示：获取数据成功！");
    }

    @PostMapping({"/lose"})
    public JsonResult loseCard(HttpServletRequest request, @RequestBody GaiaSdRechargeCardLoseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.rechargeCardService.loseCard(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/change"})
    public JsonResult changeCard(HttpServletRequest request, @RequestBody GaiaSdRechargeCardChangeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.rechargeCardService.changeCard(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/getCardInfo"})
    public JsonResult getCardInfo(HttpServletRequest request, @RequestBody RechargeCardInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.rechargeCardService.getCardInfo(inData), "提示：获取数据成功！");
    }
}
