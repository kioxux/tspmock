package com.gys.business.controller;

import com.gys.business.service.ThirdPayService;
import com.gys.business.service.data.thirdPay.SytOperationInData;
import com.gys.business.service.data.thirdPay.SytOperationOutData;
import com.gys.business.service.data.thirdPay.SytRevokeOrderInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(tags = "收银通")
@RestController
@RequestMapping({"/syt/"})
public class SytController extends BaseController {

    @Autowired
    private ThirdPayService thirdPayService;

    @ApiOperation(value = "查询收银通操作记录",response = SytOperationOutData.class)
    @PostMapping({"querySytOperation"})
    public JsonResult querySytOperation(HttpServletRequest request, @RequestBody SytOperationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(thirdPayService.querySytOperation(userInfo,inData), "提示：操作数据成功！");
    }

    @ApiOperation(value = "撤销订单")
    @PostMapping({"revokeOrder"})
    public JsonResult revokeOrder(HttpServletRequest request, @RequestBody SytRevokeOrderInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        thirdPayService.revokeOrder(inData);
        return JsonResult.success(null, "提示：撤销订单成功！");
    }
}
