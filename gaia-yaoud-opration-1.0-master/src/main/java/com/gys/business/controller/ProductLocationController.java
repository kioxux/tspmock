//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.ProductLocationService;
import com.gys.business.service.data.GetProductLocationInData;
import com.gys.business.service.data.GetProductLocationOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping({"/productLocation/"})
public class ProductLocationController extends BaseController {
    @Resource
    private ProductLocationService productLoactionService;

    public ProductLocationController() {
    }

    @PostMapping({"/selectLocationList"})
    public JsonResult selectLocationList(HttpServletRequest request, @RequestBody GetProductLocationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsplBrId(userInfo.getDepId());
        List<GetProductLocationOutData> list = this.productLoactionService.selectLocationList(inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @PostMapping({"/saveLocation"})
    public JsonResult saveLocation(HttpServletRequest request, @RequestBody GetProductLocationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsplBrId(userInfo.getDepId());
        this.productLoactionService.saveLocation(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/importLocation"})
    public JsonResult importLocation(HttpServletRequest request, @RequestBody GetProductLocationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsplBrId(userInfo.getDepId());
        this.productLoactionService.importLocation(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/selectLocationDrop"})
    public JsonResult selectLocationDrop(HttpServletRequest request, @RequestBody GetProductLocationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsplBrId(userInfo.getDepId());
        GetProductLocationOutData outData = this.productLoactionService.selectLocationDrop(inData);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }
}
