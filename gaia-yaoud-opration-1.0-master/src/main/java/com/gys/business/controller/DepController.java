package com.gys.business.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.entity.GaiaTaxCode;
import com.gys.business.service.AcceptService;
import com.gys.business.service.DepDataService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "部门相关", description = "/dep")
@RestController
@RequestMapping({"/dep/"})
public class DepController extends BaseController {
    @Resource
    private DepDataService depDataService;

    @ApiOperation(value = "配送中心查询",response = CompadmDcOutData.class)
    @PostMapping({"selectDepList"})
    public JsonResult selectDepList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
//        userInfo.setClient("21020006");
//        userInfo.setUserId("10000047");
        List<DcOutData> list = this.depDataService.selectDepList(userInfo.getClient());
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询配送中心和门店查询",response = CompadmDcOutData.class)
    @PostMapping({"selectDcAndStore"})
    public JsonResult selectDcAndStore(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<DcOutData> list = this.depDataService.selectDcAndStore(userInfo.getClient());
        return JsonResult.success(list, "提示：获取数据成功！");
    }
}
