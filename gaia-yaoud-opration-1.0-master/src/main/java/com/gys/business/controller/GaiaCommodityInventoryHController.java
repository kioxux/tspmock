package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.gys.business.mapper.entity.GaiaCommodityInventoryH;
import com.gys.business.service.GaiaCommodityInventoryHService;
import com.gys.business.service.GaiaEntNewProductEvaluationHService;
import com.gys.business.service.data.GaiaCommodityInventoryHInData;
import com.gys.business.service.data.GaiaEntNewProductEvaluationInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 商品调库-主表(GaiaCommodityInventoryH)表控制层
 *
 * @author makejava
 * @since 2021-10-19 16:01:56
 */
@RestController
@RequestMapping("gaiaCommodityInventoryH")
@Slf4j
public class GaiaCommodityInventoryHController extends BaseController {

    @Resource
    private GaiaCommodityInventoryHService gaiaCommodityInventoryHService;

    /**
     * 生成公司级新品评估数据
     *
     */
    @PostMapping("insertInventoryData")
    public JsonResult insertEntProductData(@RequestParam String param) {
        return JsonResult.success(gaiaCommodityInventoryHService.insertInventoryData(param), "提示：生成数据成功！");
    }

    /**
     * 查询单据主表
     *
     */
    @PostMapping("listBill")
    public JsonResult listBill(HttpServletRequest request, @RequestBody GaiaCommodityInventoryHInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        log.info(String.format("<商品调库><查询主表集合><请求参数：%s>", JSONUtil.toJsonStr(param)));
        return JsonResult.success(gaiaCommodityInventoryHService.listBill(param), "提示：查询数据成功！");
    }

    /**
     * 查询明细集合
     *
     */
    @PostMapping("listBillDetail")
    public JsonResult listBillDetail(HttpServletRequest request, @RequestBody GaiaCommodityInventoryHInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        log.info(String.format("<商品调库><查询明细集合><请求参数：%s>", JSONUtil.toJsonStr(param)));
        return JsonResult.success(gaiaCommodityInventoryHService.listBillDetail(param), "提示：查询数据成功！");
    }

    /**
     * 查询单据内未推送门店
     *
     */
    @PostMapping("listUnSendStore")
    public JsonResult listUnSendStore(HttpServletRequest request, @RequestBody GaiaCommodityInventoryHInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        log.info(String.format("<商品调库><查询单据内未推送门店><请求参数：%s>", JSONUtil.toJsonStr(param)));
        return JsonResult.success(gaiaCommodityInventoryHService.listUnSendStore(param), "提示：查询数据成功！");
    }

    /**
     * 查询门店线路
     *
     */
    @PostMapping("listStoreLine")
    public JsonResult listStoreLine(HttpServletRequest request, @RequestBody GaiaCommodityInventoryHInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        log.info(String.format("<商品调库><查询门店线路><请求参数：%s>", JSONUtil.toJsonStr(param)));
        return JsonResult.success(gaiaCommodityInventoryHService.listStoreLine(param), "提示：查询数据成功！");
    }


    /**
     * 导出
     *
     */
    @PostMapping("exportBillDetail")
    public Result exportBillDetail(HttpServletRequest request, @RequestBody GaiaCommodityInventoryHInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClient(userInfo.getClient());
        log.info(String.format("<商品调库><导出表单明细><请求参数：%s>", JSONUtil.toJsonStr(param)));
        return gaiaCommodityInventoryHService.exportBillDetail(param);
    }
}




