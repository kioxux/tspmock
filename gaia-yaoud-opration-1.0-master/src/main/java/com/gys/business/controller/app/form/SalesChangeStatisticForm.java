package com.gys.business.controller.app.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @Author jiht
 * @Description 成分销售变化TOP排名统计入参
 * @Date 2021/12/3 14:09
 **/
@Data
public class SalesChangeStatisticForm {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private List<String> sotreList;
    /**
     * 统计类型：1-销售额，2-毛利额
     */
    private String statisticType;

    /**
     * 统计方式：1-增长，2-下降
     */
    private String statisticMode;

    /**
     * 统计数量：10，50
     */
    private String statisticCount;

    /**
     * 是否包含非直营管理门店（默认2，直营管理门店的判断用门店分类DX0003）：1-包含，2-不包含
     */
    private String isContainDirectSaleStore;

    /**
     * 统计起期
     */
    @NotBlank(message = "统计起期不能为空！")
    @Size(min = 8,max = 8,message = "时间只能包含年月日！")
    private String startDate;

    /**
     * 统计止期
     */
    @NotBlank(message = "统计止期不能为空！")
    @Size(min = 8,max = 8,message = "时间只能包含年月日！")
    private String endDate;

    /**
     * 对比统计起期
     */
    @NotBlank(message = "对比统计起期不能为空！")
    @Size(min = 8,max = 8,message = "时间只能包含年月日！")
    private String compareStartDate;

    /**
     * 对比统计止期
     */
    @NotBlank(message = "对比统计止期不能为空！")
    @Size(min = 8,max = 8,message = "时间只能包含年月日！")
    private String compareEndDate;
}
