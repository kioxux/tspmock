package com.gys.business.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.service.PercentagePlanService;
import com.gys.business.service.data.GetPlaceOrderInData;
import com.gys.business.service.data.GetReplenishDetailOutData;
import com.gys.business.service.data.ImportInData;
import com.gys.business.service.data.percentageplan.PercentageInData;
import com.gys.business.service.data.percentageplan.PercentageOutData;
import com.gys.business.service.data.percentageplan.PercentageProInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping({"/percentage/plan/"})
@Api(tags = "提成方案相关")
@Slf4j
public class PercentagePlanController extends BaseController {
    @Autowired
    private PercentagePlanService service;

    @ApiOperation(value = "新增/修改提成方案")
    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody PercentageInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setPlanCreaterId(userInfo.getUserId());
        inData.setPlanCreater(userInfo.getLoginName());
        return JsonResult.success(service.insert(inData), "提示：操作数据成功！");
    }

    @ApiOperation(value = "提成方案列表查询",response = PercentageOutData.class)
    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody PercentageInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(service.list(inData), "提示：操作数据成功！");
    }

    @ApiOperation(value = "提成方案详情查询",response = PercentageInData.class)
    @PostMapping({"tichengDetail"})
    public JsonResult tichengDetail(@RequestBody Map<String,Long> inData) {
        return JsonResult.success(service.tichengDetail(inData.get("planId")), "提示：操作数据成功！");
    }

    @ApiOperation(value = "删除销售提成明细")
    @PostMapping({"deleteSalePlan"})
    public JsonResult deleteSalePlan(@RequestBody Map<String,Long> inData ) {
        service.deleteSalePlan(inData.get("saleId"));
        return JsonResult.success("", "提示：操作数据成功！");
    }

    @ApiOperation(value = "删除单品提成明细")
    @PostMapping({"deleteProPlan"})
    public JsonResult deleteProPlan(@RequestBody Map<String,Long> inData) {
        service.deleteProPlan(inData.get("proPlanId"));
        return JsonResult.success("", "提示：操作数据成功！");
    }


    @ApiOperation(value = "方案审核")
    @PostMapping({"approve"})
    public JsonResult approve(@RequestBody Map<String,Long> inData) {
        service.approve(inData.get("planId"),inData.get("planStatus").toString());
        return JsonResult.success("", "提示：审核成功！");
    }

    @ApiOperation(value = "删除方案")
    @PostMapping({"deletePlan"})
    public JsonResult deletePlan(@RequestBody Map<String,Long> inData) {
        service.deletePlan(inData.get("planId"));
        return JsonResult.success("", "提示：删除成功！");
    }

    @ApiOperation(value = "商品查询")
    @PostMapping({"selectProductByClient"})
    public JsonResult selectProductByClient(HttpServletRequest request, @RequestBody Map<String,String> inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return JsonResult.success(service.selectProductByClient(userInfo.getClient(),inData.get("proCode")), "提示：查询成功！");
    }

    @ApiOperation(value = "导入信息查询",response = PercentageProInData.class)
    @PostMapping({"getImportExcelDetailList"})
    public JsonResult getImportExcelDetailList(HttpServletRequest request,@RequestParam("file") MultipartFile file) {
        List<Map<String,String>> importInDataList = new ArrayList<>();
        try {
//            String rootPath = request.getSession().getServletContext().getRealPath("/");
//            String fileName = file.getOriginalFilename();
//            String path = rootPath + fileName;
//            File newFile = new File(path);
//            //判断文件是否存在
//            if(!newFile.exists()) {
//                newFile.mkdirs();//不存在的话，就开辟一个空间
//            }
//            //将上传的文件存储
//            file.transferTo(newFile);
            InputStream in = file.getInputStream();
            //导入已存在的Excel文件，获得只读的工作薄对象
            XSSFWorkbook wk = new XSSFWorkbook(in);
            //获取第一张Sheet表
            Sheet sheet = wk.getSheetAt(0);
            //取数据
            importInDataList = getRowAndCell(sheet);
//            inData.setImportInDataList(importInDataList);
            in.close();
            wk.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.service.getImportExcelDetailList(userInfo.getClient(),importInDataList), "提示：获取成功！");
    }

    public List<Map<String,String>> getRowAndCell(Sheet sheet) {
        List<Map<String,String>> importInDataList = new ArrayList<>();
        String msg = null;
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        System.out.println("总行数==" + totalRows);
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            for (int i = 1; i <= totalRows; i++) {
                Row row = sheet.getRow(i); //取出一行数据放入row
                Map<String,String> newExpert = new HashMap<>();
                newExpert.put("proCode",dataFormatter.formatCellValue(row.getCell(0)).trim());
                String tichengAmt = dataFormatter.formatCellValue(row.getCell(1)).trim();
                if (ObjectUtil.isEmpty(tichengAmt)){
                    tichengAmt = "0";
                }
                newExpert.put("tichengAmt",tichengAmt);
                importInDataList.add(newExpert);
            } //行end
        }else{
            throw new BusinessException("EXCEL导入信息为空");
        }
        return importInDataList;
    }
}
