package com.gys.business.controller;

import com.gys.business.service.ProductSpecialService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "特殊商品相关接口")
@RestController
@RequestMapping("/productSpecial")
public class ProductSpecialController extends BaseController {

    @Autowired
    private ProductSpecialService productSpecialService;
    @Autowired
    private HttpServletRequest request;


    @ApiOperation(value = "下拉框数据")
    @PostMapping("/getSelectFrame")
    public JsonResult getSelectFrame(@RequestBody SelectFrameInData inData) {
        return productSpecialService.getSelectFrame(inData);
    }

    @ApiOperation(value = "药德商品信息查询")
    @PostMapping("/getProductInfo")
    public JsonResult getProductInfo(@RequestBody ProductBasicInfoInData inData) {
        return productSpecialService.getProductInfo(inData);
    }


    @ApiOperation(value = "查询特殊商品列表", response = GaiaProductSpecialOutData.class)
    @PostMapping("/list")
    public JsonResult list(@RequestBody GaiaProductSpecialInData inData) {
        return productSpecialService.getProductSpecialList(inData);
    }


    @ApiOperation(value = "新增特殊商品")
    @PostMapping("/add")
    public JsonResult add(@RequestBody AddProductSpecialInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setCreateUser(userInfo.getLoginName());
        return productSpecialService.addProductSpecial(inData);
    }


    @ApiOperation(value = "停用特殊商品")
    @PostMapping("/invalid")
    public JsonResult invalid(@RequestBody List<GaiaProductSpecial> productList) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return productSpecialService.invalidProductSpecial(productList, userInfo);
    }


    @ApiOperation(value = "特殊商品导入Excel")
    @PostMapping("/importData")
    public JsonResult importData(MultipartFile file) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return productSpecialService.importData(file, userInfo);
    }


    @ApiOperation(value = "导出特殊商品列表Excel")
    @PostMapping("/export")
    public Result export(@RequestBody GaiaProductSpecialInData inData) {
        return productSpecialService.exportData(inData);
    }


    @ApiOperation(value = "特殊商品修改")
    @PostMapping("/edit")
    public JsonResult edit(@RequestBody EditProductSpecialInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setUpdateUser(userInfo.getLoginName());
        return productSpecialService.editProductSpecial(inData);
    }


    @ApiOperation(value = "查询特殊商品修改记录", response = GaiaProductSpecialRecordOutData.class)
    @PostMapping("/editRecord")
    public JsonResult editRecord(@RequestBody GaiaProductSpecialEditRecordInData inData) {
        return productSpecialService.editProductSpecialRecord(inData);
    }


    @ApiOperation(value = "导出特殊商品修改记录")
    @PostMapping("/exportEditRecord")
    public Result exportEditRecord(@RequestBody GaiaProductSpecialEditRecordInData inData) {
        return productSpecialService.exportEditProductSpecialRecord(inData);
    }


    @ApiOperation(value = "特殊商品参数列表查询", response = GaiaProductSpecialParam.class)
    @PostMapping("/param/list")
    public JsonResult paramList(@RequestBody GaiaProductSpecialParam inData) {
        return productSpecialService.getProductSpecialParamList(inData);
    }


    @ApiOperation(value = "特殊商品参数新增")
    @PostMapping("/param/add")
    public JsonResult paramAdd(@RequestBody ProductSpecialParamInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return productSpecialService.addProductSpecialParam(inData, userInfo);
    }


    @ApiOperation(value = "特殊商品参数维护")
    @PostMapping("/param/edit")
    public JsonResult paramEdit(@RequestBody ProductSpecialParamInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return productSpecialService.editProductSpecialParam(inData, userInfo);
    }


    @ApiOperation(value = "导入特殊商品参数")
    @PostMapping("/param/import")
    public JsonResult importParam(MultipartFile file) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return productSpecialService.importProductSpecialParam(file, userInfo);
    }


    @ApiOperation(value = "导出特殊商品参数")
    @PostMapping("/param/export")
    public Result paramExport(@RequestBody GaiaProductSpecialParam inData) {
        return productSpecialService.exportProductSpecialParam(inData);
    }


    @ApiOperation(value = "特殊商品参数修改记录查询", response = GaiaProductSpecialParamChange.class)
    @PostMapping("/param/editRecord")
    public JsonResult paramEditRecord(@RequestBody GaiaProductSpecialParam inData) {
        return productSpecialService.getParamEditRecord(inData);
    }


    @ApiOperation(value = "特殊商品参数修改记录导出")
    @PostMapping("/param/editRecordExport")
    public Result paramEditRecordExport(@RequestBody GaiaProductSpecialParam inData) {
        return productSpecialService.exportParamEditRecord(inData);
    }

    @ApiOperation(value = "特殊分类药品查询")
    @PostMapping({"/specialDrugsInquiry"})
    public JsonResult specialDrugsInquiry(HttpServletRequest request, @Valid @RequestBody SpecialClassifcationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.productSpecialService.specialDrugsInquiry(inData), "success");
    }

    @PostMapping({"/specialDrugExportFile"})
    public Result specialDrugExportFile (HttpServletRequest request, @RequestBody SpecialClassifcationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return productSpecialService.exportDrugsDetail(inData);
    }

}
