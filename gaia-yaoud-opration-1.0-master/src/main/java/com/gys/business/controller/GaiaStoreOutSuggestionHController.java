package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.gys.business.entity.dto.CallUpOrInStoreDetailDTO;
import com.gys.business.entity.vo.CallUpStoreDetailVO;
import com.gys.business.service.GaiaStoreOutSuggestionHService;
import com.gys.business.service.data.GaiaStoreOutSuggestionInData;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.enums.StoOutSuggestionEnum;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionH)表控制层
 *
 * @author XIAO-ZY
 * @since 2021-10-28 10:54:04
 */
@RestController
@RequestMapping("storeOutSuggestion")
@Slf4j
public class GaiaStoreOutSuggestionHController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaStoreOutSuggestionHService gaiaStoreOutSuggestionHService;


    @ApiOperation(value = "门店调出单-计算" )
    @FrequencyValid
    @PostMapping({"calculateBill"})
    public JsonResult insertBillList(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        log.info(String.format("<门店调出单><门店调出单-计算><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.calculateBill(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "门店调出单-保存" )
    @FrequencyValid
    @PostMapping({"saveOutBill"})
    public JsonResult saveOutBill(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        inData.setStoName(loginUser.getDepName());
        log.info(String.format("<门店调出单><门店调出单-保存><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.saveOutBill(loginUser,inData), "提示：保存数据成功！");
    }

    @ApiOperation(value = "门店调出单-查询列表" )
    @PostMapping({"listBill"})
    public JsonResult listBill(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        log.info(String.format("<门店调出单><门店调出单-查询列表><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.listBill(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "门店调出单-查看" )
    @PostMapping({"listDetail"})
    public JsonResult listDetail(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        log.info(String.format("<门店调出单><门店调出单-查看><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.listDetail(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "门店调出单-推送" )
    @FrequencyValid
    @PostMapping({"pushToIn"})
    public JsonResult pushToIn(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        inData.setStoName(loginUser.getDepName());
        log.info(String.format("<门店调出单><门店调出单-推送><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.pushToIn(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "门店调出单-关联门店个数（参数）" )
    @PostMapping({"listParam"})
    public JsonResult listParam(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<门店调出单><门店调出单-关联门店个数（参数）><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.listParam(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "门店调出单-是否允许新增" )
    @PostMapping({"isAllow"})
    public JsonResult isAllow(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        log.info(String.format("<门店调出单><门店调出单-是否允许新增><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.isAllow(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "门店调出单-关联门店集合" )
    @PostMapping({"listSto"})
    public JsonResult listSto(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        log.info(String.format("<门店调出单><门店调出单-关联门店集合><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.listSto(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "门店调出单-确认分配页面-查看" )
    @PostMapping({"listDetailForConfirm"})
    public JsonResult listDetailForConfirm(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        log.info(String.format("<门店调出单><门店调出单-确认分配页面-查看><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.listDetailForConfirm(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "门店调出单-确认分配" )
    @FrequencyValid
    @PostMapping({"confirmBill"})
    public JsonResult confirmBill(HttpServletRequest request, @RequestBody GaiaStoreOutSuggestionInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setStoCode(loginUser.getDepId());
        log.info(String.format("<门店调出单><门店调出单-确认分配><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.confirmBill(loginUser,inData), "提示：查询数据成功！");
    }

    @PostMapping("queryDetail")
    @ApiOperation(value = "系统级门店建议调出明细" ,response = CallUpStoreDetailVO.class )
    public JsonResult queryDetail( @RequestBody CallUpOrInStoreDetailDTO storeDetailDTO){
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.queryDetail(storeDetailDTO),"提示:查询数据成功");
    }

    //系统级门店建议调出明细导出
    @PostMapping("queryDetail/export")
    public void queryDetail(HttpServletResponse response, @RequestBody CallUpOrInStoreDetailDTO storeDetailDTO){
        this.gaiaStoreOutSuggestionHService.queryDetailExport(response,storeDetailDTO);
    }
    /**
     * 查询门店建议调出明细
     * @return
     */
    @GetMapping("queryType")
    public JsonResult queryType(){
        return JsonResult.success(StoOutSuggestionEnum.toJson());
    }

    /***
     * 查询业务单号
     * @return
     */
    @GetMapping("queryBillCode")
    public JsonResult queryBillCode(@RequestParam(value = "suggestedStartDate",required = false) String startDate,
                                    @RequestParam(value = "suggestedEndDate",required = false) String endDate){
        return JsonResult.success(this.gaiaStoreOutSuggestionHService.queryBillCode(startDate,endDate));
    }

}

