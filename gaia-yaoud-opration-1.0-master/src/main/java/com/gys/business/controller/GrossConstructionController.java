package com.gys.business.controller;

import com.gys.business.service.GrossConstructionService;
import com.gys.business.service.data.GrossConstructionInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 15:15 2021/9/16
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@RestController
@RequestMapping("grossConstruction")
public class GrossConstructionController extends BaseController {

    @Autowired
    private GrossConstructionService grossConstructionService;

    @PostMapping("bigClassList")
    public JsonResult bigClassList(HttpServletRequest request,@Valid @RequestBody GrossConstructionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.grossConstructionService.bigClassList(inData), "提示：获取数据成功！");
    }

    @PostMapping("getGrossConstruction")
    public JsonResult getGrossConstruction(HttpServletRequest request,@Valid @RequestBody  GrossConstructionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.grossConstructionService.getGrossConstruction(inData), "提示：获取数据成功！");
    }

    @PostMapping("getMidGross")
    public JsonResult getMidGross(HttpServletRequest request,@Valid @RequestBody  GrossConstructionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.grossConstructionService.getMidGross(inData), "提示：获取数据成功！");
    }

    @PostMapping("getTotalList")
    public JsonResult getTotalList(HttpServletRequest request,@Valid @RequestBody  GrossConstructionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.grossConstructionService.getTotalList(inData), "提示：获取数据成功！");
    }
}
