package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBill;
import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBillDetail;
import com.gys.business.service.GaiaKeyCommodityTaskBillService;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillInData;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillRelationInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 重点商品任务—主表(GaiaKeyCommodityTaskBill)表控制层
 *
 * @author makejava
 * @since 2021-09-01 15:37:55
 */
@RestController
@RequestMapping("gaiaKeyCommodityTaskBill")
@Slf4j
public class GaiaKeyCommodityTaskBillController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaKeyCommodityTaskBillService gaiaKeyCommodityTaskBillService;

    /**
     * 重点商品任务列表-新增
     */
    @PostMapping("insertBillInfo")
    public JsonResult insertBillInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务列表><新增><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.insertBillInfo(userInfo, inData), "提示：新增数据成功！");
    }

    /**
     * 重点商品任务列表-新增
     */
    @PostMapping("listBillInfo")
    public JsonResult listBillInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务列表><查询><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.listBillInfo(userInfo, inData), "提示：查询数据成功！");
    }

    /**
     * 重点商品任务列表-删除
     */
    @PostMapping("deleteBillInfo")
    public JsonResult deleteBillInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBill inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务列表><删除><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.deleteBillInfo(userInfo, inData), "提示：查询数据成功！");
    }

    /**
     * 重点商品任务列表-审核或者停用
     */
    @PostMapping("checkBillInfo")
    public JsonResult checkBillInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBill inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务列表><审核或者停用><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.checkBillInfo(userInfo, inData), "提示：修改数据成功！");
    }

    /**
     * 重点商品任务列表-复制
     */
    @PostMapping("copyBillInfo")
    public JsonResult copyBillInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务列表><复制><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.copyBillInfo(userInfo, inData), "提示：修改数据成功！");
    }

    /**
     * 重点商品任务列表-查看
     */
    @PostMapping("getBillInfoById")
    public JsonResult getBillInfoById(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBill inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务列表><查看><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.getBillInfoById(userInfo, inData), "提示：修改数据成功！");
    }

    /**
     * 重点商品任务维护-根据商品编码查询商品明细
     */
    @PostMapping("getGoodsInfoById")
    public JsonResult getGoodsInfoById(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><根据商品编码查询商品明细><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.getGoodsInfoById(userInfo, inData), "提示：修改数据成功！");
    }

    /**
     * 重点商品任务维护-保存商品明细
     */
    @PostMapping("saveGoodsInfo")
    public JsonResult saveGoodsInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillRelationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><保存商品明细><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.saveGoodsInfo(userInfo, inData), "提示：保存数据成功！");
    }

    /**
     * 重点商品任务维护-获取商品任务明细
     */
    @PostMapping("getGoodsTaskById")
    public JsonResult getGoodsTaskById(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillRelationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><获取商品任务明细><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.getGoodsTaskById(userInfo, inData), "提示：查询数据成功！");
    }

    /**
     * 重点商品任务维护-保存各个门店任务信息
     */
    @PostMapping("saveStoreInfo")
    public JsonResult saveStoreInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillRelationInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><保存各个门店任务信息><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.saveStoreInfo(userInfo, inData), "提示：保存数据成功！");
    }

    /**
     * 重点商品任务维护-编辑个门店任务信息
     */
    @PostMapping("updateStoreInfo")
    public JsonResult updateStoreInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillDetail inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><编辑个门店任务信息><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.updateStoreInfo(userInfo, inData), "提示：保存数据成功！");
    }

    /**
     * 重点商品任务维护-批量删除个门店任务信息
     */
    @PostMapping("deleteBatchStoreInfo")
    public JsonResult deleteBatchStoreInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><批量删除个门店任务信息><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.deleteBatchStoreInfo(userInfo, inData), "提示：删除数据成功！");
    }

    /**
     * 重点商品任务维护-批量删除商品明细
     */
    @PostMapping("deleteBatchGoodsInfo")
    public JsonResult deleteBatchGoodsInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><批量删除商品明细><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.deleteBatchGoodsInfo(userInfo, inData), "提示：删除数据成功！");
    }

    /**
     * 重点商品任务维护-查询商品明细集合
     */
    @PostMapping("listGoodsInfo")
    public JsonResult listGoodsInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><查询商品明细集合><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.listGoodsInfo(userInfo, inData), "提示：查询数据成功！");
    }

    /**
     * 重点商品任务维护-查询商品门店明细集合
     */
    @PostMapping("listStoreInfo")
    public JsonResult listStoreInfo(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><查询商品门店明细集合><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.listStoreInfo(userInfo, inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "批量导入-重点商品任务-商品维护导入模板")
    @PostMapping("/batchImportGoods")
    public JsonResult batchImportGoods(HttpServletRequest request, @RequestParam("file") MultipartFile file, @RequestParam("billCode") String billCode) {
        GetLoginOutData userInfo = getLoginUser(request);
        return JsonResult.success(gaiaKeyCommodityTaskBillService.batchImportGoods(file, userInfo, billCode), "提示：导入数据成功！");
    }

    @ApiOperation(value = "批量导入-重点商品任务-门店维护导入模板")
    @PostMapping("/batchImportStores")
    public JsonResult batchImportStores(HttpServletRequest request, @RequestParam("file") MultipartFile file, @RequestParam("billCode") String billCode, @RequestParam("proSelfCode") String proSelfCode) {
        GetLoginOutData userInfo = getLoginUser(request);
        return JsonResult.success(gaiaKeyCommodityTaskBillService.batchImportStores(file, userInfo, billCode,proSelfCode), "提示：导入数据成功！");
    }

    @ApiOperation(value = "评估门店等级")
    @PostMapping("/assessStoreLevel")
    public JsonResult assessStoreLevel(HttpServletRequest request) {
        GetLoginOutData userInfo = getLoginUser(request);
        return JsonResult.success(gaiaKeyCommodityTaskBillService.assessStoreLevel(userInfo), "提示：导入数据成功！");
    }

    /**
     * 重点商品任务维护-查询商品门店明细集合
     */
    @PostMapping("getImportTaskHBTime")
    public JsonResult getImportTaskHBTime(HttpServletRequest request, @RequestBody GaiaKeyCommodityTaskBillInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<重点商品任务维护><查询商品门店明细集合><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(gaiaKeyCommodityTaskBillService.getImportTaskHBTime(userInfo, inData), "提示：查询数据成功！");
    }

}
