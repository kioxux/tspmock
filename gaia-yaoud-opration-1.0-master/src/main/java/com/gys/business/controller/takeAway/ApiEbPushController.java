package com.gys.business.controller.takeAway;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.base.BaseController;
import com.gys.common.config.takeAway.EbConfig;
import com.gys.common.data.*;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.UuidUtil;
import com.gys.util.takeAway.eb.EbApiUtil;
import com.gys.util.takeAway.eb.SignUtil;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/eb/push")
public class ApiEbPushController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(ApiEbPushController.class);
    private BaseService baseService;
    @Autowired
    private TakeAwayService takeAwayService;
    public BaseService getBaseService() {
        return baseService;
    }

    @Autowired
    public void setBaseService(BaseService baseService) {
        this.baseService = baseService;
    }

    /**
     * 饿百零售：订单接口（必接）
     * 根据cmd区分操作
     * 推送的请求头为 application/x-www-form-urlencoded，而非产出
     * @param request
     * @return
     */
    @ResponseBody
//    @RequestMapping(value = "/cmd", method = RequestMethod.POST,produces = { "application/x-www-form-urlencoded;charset=UTF-8" })
    @RequestMapping(value = "/cmd", method = RequestMethod.POST)
    public String pushCmdWithEb(HttpServletRequest request) {
        Map<String, Object> paramMap = getParameterMap(request);
        String retMsg = null;
        String sourceOrderId = null;
        Map<String,Object> retBody=new HashMap<>();
        logger.info("饿百零售参数：" + JSONObject.toJSONString(paramMap));

        String cmd = nullToEmpty(paramMap.get("cmd"));
        String defaultSource=nullToEmpty(paramMap.get("source")); //APPID
        String ticket =nullToEmpty(paramMap.get("ticket")); //请求流水号

        Map<String, Object> paramBody = (Map<String, Object>) JSONObject.parse(StringEscapeUtils.unescapeJava((String) paramMap.get("body")));
        String orderId = nullToEmpty(paramBody.get("order_id"));
        String platformShopId =nullToEmpty(paramBody.get("platform_shop_id"));//平台店铺ID

        /**
         * 订单取消推了三次消息：
         * 2021-04-16 15:13:11.090 [http-nio-20104-exec-2] INFO   EB-- cmd:order.user.cancel, orderId:2148366519984709759
         * 2021-04-16 15:13:11.232 [http-nio-20104-exec-2] INFO   EB-- cmd:order.user.cancel, orderId:2148366519984709759
         * 2021-04-16 15:13:22.153 [http-nio-20104-exec-7] INFO   EB-- cmd:order.user.cancel, orderId:2148366519984709759
         */
        /**
         * 催单消息推送3次：
         * 2021-04-18 09:47:01.125 [http-nio-20104-exec-10] INFO  c.g.b.controller.takeAway.ApiEbPushController - 饿百零售参数：{"ticket":"D4D5C9A9-7330-48E8-983A-94C6E6AD1323","encrypt":"","sign":"411AC1239FB991C45067E5C625C8409D","cmd":"order.remind.push","source":"33873511","body":"{\"platform_shop_id\":\"32267798003\",\"create_time\":\"1618710420\",\"order_id\":\"2148488216029159551\"}","version":"3","timestamp":"1618710420"}
         * 2021-04-18 09:47:01.491 [http-nio-20104-exec-4] INFO  c.g.b.controller.takeAway.ApiEbPushController - 饿百零售参数：{"ticket":"09ABD73E-B44F-4590-A83C-6143BEB5791C","encrypt":"","sign":"2CB1FA950A79A063A5009149A6C36C8F","cmd":"order.remind.push","source":"33873511","body":"{\"platform_shop_id\":\"32267798003\",\"create_time\":\"1618710420\",\"order_id\":\"2148488216029159551\"}","version":"3","timestamp":"1618710421"}
         * 2021-04-18 09:47:12.116 [http-nio-20104-exec-8] INFO  c.g.b.controller.takeAway.ApiEbPushController - 饿百零售参数：{"ticket":"2EB3808D-09B0-48A1-A9DE-EEB669653B4C","encrypt":"","sign":"DAB4DBC366CA29D2E9844F40DE570984","cmd":"order.remind.push","source":"33873511","body":"{\"platform_shop_id\":\"32267798003\",\"create_time\":\"1618710420\",\"order_id\":\"2148488216029159551\"}","version":"3","timestamp":"1618710431"}
         */
        /**
         * 催单接口返回：
         * 2021-04-18 09:47:01.307 [http-nio-20104-exec-10] INFO  c.g.b.controller.takeAway.ApiEbPushController - 饿百零售-带那个单确认返回值：{"ticket":"D4D5C9A9-7330-48E8-983A-94C6E6AD1323","sign":"5D7D72B0C9250980B4D6A69B4BE5C7D1","cmd":"resp.order.remind.push","source":"33873511","body":{"errno":0,"error":"success"},"version":"3","timestamp":"1618710421"}
         * 2021-04-18 09:47:01.627 [http-nio-20104-exec-4] INFO  c.g.b.controller.takeAway.ApiEbPushController - 饿百零售-带那个单确认返回值：{"ticket":"09ABD73E-B44F-4590-A83C-6143BEB5791C","sign":"49EDA9BAC6F22BF741D84A15BAB0C505","cmd":"resp.order.remind.push","source":"33873511","body":{"errno":0,"error":"success"},"version":"3","timestamp":"1618710421"}
         * 2021-04-18 09:47:12.264 [http-nio-20104-exec-8] INFO  c.g.b.controller.takeAway.ApiEbPushController - 饿百零售-带那个单确认返回值：{"ticket":"2EB3808D-09B0-48A1-A9DE-EEB669653B4C","sign":"52B0AF7CB38994086A15AABC5E27AE44","cmd":"resp.order.remind.push","source":"33873511","body":{"errno":0,"error":"success"},"version":"3","timestamp":"1618710432"}
         */
        logger.info("EB-- cmd:{}, orderId:{}", cmd, orderId);

        //根据当前appid和店铺id 查找私钥
        GaiaTAccount account = takeAwayService.getEbPlatformAccount(Constants.PLATFORMS_EB, defaultSource, platformShopId);
        String defaultSecret ="";
        String client ="";
        String stoCode ="";
        String orderOnService = "N";
        if(ObjectUtil.isNotEmpty(account)){
            defaultSecret= account.getAppSecret();
            client = account.getClient();
            stoCode = account.getStoCode();
            orderOnService = account.getOrderOnService();
        }else{
            logger.error("饿百零售-推送API：找不到对应门店信息！ 订单ID：" + orderId);
        }
        //设置默认返回信息
        retBody.put("errno",10001);
        retBody.put("error","接口尚未处理该信息!");
        if (account == null || !"Y".equalsIgnoreCase(account.getOrderOnService())) {
            retMsg = EbApiUtil.apiReturnMsg(defaultSource, defaultSecret, cmd, ticket, retBody);
            logger.info("饿百零售-推送API，拒绝处理订单推送消息，platformShopId:" + platformShopId);
            return retMsg;
        }
        //校验sign是否正确
        boolean flag = SignUtil.checkSign(paramMap, defaultSource,defaultSecret);

        if(flag) {
            switch (cmd) {
                //新订单
                case EbConfig.EB_CMD_ORDER_CREATE_PUSH:
                    execOrderCreate(client,stoCode,defaultSource,defaultSecret,orderId,
                            sourceOrderId,retBody,platformShopId,orderOnService, account);
                    break;
                //物流状态
                case EbConfig.EB_CMD_ORDER_DELIVERYSTATUS_PUSH:
                    execDeliveryStatus(client,stoCode,defaultSource,defaultSecret,orderId,paramBody,
                            retBody,orderOnService);
                    break;
                //催单
                case EbConfig.EB_CMD_ORDER_REMIND_PUSH:
                    execRemind(client,stoCode,orderId,paramBody,retBody,orderOnService);
                    break;
                //订单状态
                case EbConfig.EB_CMD_ORDER_STATUS_PUSH:
                    execOrderStatus(client,stoCode,defaultSource,defaultSecret,orderId,paramBody,retBody,orderOnService);
                    break;
                //用户申请订单取消/退款
                case EbConfig.EB_CMD_ORDER_USER_CANCEL:
                    execUserCancelOrder(client,stoCode,orderId,paramBody,retBody,orderOnService);
                    break;
                //部分退款订单信息推送
                case EbConfig.EB_CMD_ORDER_PARTREFUND_PUSH:
                    execPartRefund(client,stoCode,orderId,paramBody,retBody,orderOnService);
                    break;
                default:
                    break;
            }
        }else {
            logger.error("饿百零售-推送API：签名校验失败！ 订单ID：" + orderId);
        }
        //既然这边写了，上面就不用写了
        retMsg = EbApiUtil.apiReturnMsg(defaultSource,defaultSecret, cmd,ticket, retBody);
        logger.info("饿百零售-带那个单确认返回值：" + retMsg);
        return retMsg;
    }

    /**
     * 创建订单
     * @param defaultSource
     * @param defaultSecret
     * @param orderId
     * @param sourceOrderId
     * @param retBody
     * @author：Li.Deman
     * @date：2019/1/9
     */
    private void execOrderCreate(String client,String stoCode,String defaultSource,String defaultSecret,
                                 String orderId,String sourceOrderId,
                                 Map<String,Object> retBody,String platformShopId,String orderOnService, GaiaTAccount account){
        //定义接口调用操作日志map对象(外卖平台调用本系统接口、饿百)
        Map<String, Object> logMapForOrderPay = createOperateLogMap("5", "1",
                "order.create", "创建订单");
//        //成功、成功信息(表现为平台订单号等信息)
//        setOperateLogInfo(logMapForOrderPay,"1","创建订单成功。平台订单号："
//                +orderId);
        //定义一个公共的错误信息，用来重复利用
        String reasonForOrderPay="创建订单失败。平台订单号："
                +orderId
                +"。";
        BaseOrderInfo baseOrderInfo = EbApiUtil.orderDetail(defaultSource, defaultSecret, orderId, client);
        if (baseOrderInfo!=null){
            baseOrderInfo.setClient(client);
            baseOrderInfo.setStoCode(stoCode);
            String shop_no = baseOrderInfo.getShop_no();
            if(!"Y".equalsIgnoreCase(orderOnService)){
                logger.info("加盟商：["+client+"],门店号：["+stoCode+"]店铺Id：["+shop_no+"]订单处理开关未打开！");
                setOperateLogInfo(logMapForOrderPay,"0",
                        "加盟商：["+client+"],门店号：["+stoCode+"]店铺Id：["+shop_no+"]订单处理开关未打开！");
                retBody.put("errno",10012);
                retBody.put("error","加盟商：["+client+"],门店号：["+stoCode+"]店铺Id：["+shop_no+"]订单处理开关未打开！");
                //得把日志map记录到日志表呀
                baseService.insertOperatelogWithMap(logMapForOrderPay);
            }else {
                baseOrderInfo.setPlatforms(Constants.PLATFORMS_EB);
                baseOrderInfo.setPlatforms_order_id(orderId);
                sourceOrderId = UuidUtil.getUUID("eb-");
                baseOrderInfo.setOrder_id(sourceOrderId);
                for (BaseOrderProInfo baseOrderProInfo : baseOrderInfo.getItems()) {
                    baseOrderProInfo.setClient(client);
                    baseOrderProInfo.setStoCode(stoCode);
                    baseOrderProInfo.setOrder_id(sourceOrderId);
                }
                String prescriptionOrNot = baseOrderInfo.getPrescriptionOrNot();
                List<WeideProIdDto> weideEbProId = takeAwayService.getWeideEbProId(client, stoCode, "1".equals(prescriptionOrNot), sourceOrderId);
                if (CollUtil.isNotEmpty(weideEbProId)) {
                    List<BaseOrderProInfo> resultAddList = new ArrayList<>(weideEbProId.size());
                    int size = baseOrderInfo.getItems().size();
                    for (int i = 0; i < weideEbProId.size(); i++) {
                        WeideProIdDto weideProIdDto = weideEbProId.get(i);
                        BaseOrderProInfo tempInfo = new BaseOrderProInfo();
                        tempInfo.setClient(client);
                        tempInfo.setStoCode(stoCode);
                        tempInfo.setOrder_id(sourceOrderId);
                        tempInfo.setSequence(String.valueOf(size + i + 1));
                        tempInfo.setPrice(weideProIdDto.getPrice() == null ? "0.0000" : weideProIdDto.getPrice().toString());
                        tempInfo.setQuantity("1.0000");
                        tempInfo.setMedicine_code(weideProIdDto.getProId());
                        tempInfo.setDiscount("1");
                        tempInfo.setBox_num("0");
                        tempInfo.setBox_price("0.0000");
                        resultAddList.add(tempInfo);
                    }
                    baseOrderInfo.getItems().addAll(resultAddList);
                }
                /**
                 * 一开始存到本系统系统的notice_message：new
                 * 对于单子如果是预订单，需要在再次调用pts-order-status-change接口，
                 * 在那边将相应值变成：timeup
                 */
                baseOrderInfo.setNotice_message("new");
                String resultForXB = baseService.insertOrderInfoWithMap(baseOrderInfo);
                if (resultForXB.startsWith("ok")) {
                    takeAwayService.execOrdersPush(getOrderInfoToPTSByMap(baseOrderInfo), Constants.PLATFORMS_EB, defaultSource, defaultSecret, account);
                    Map<String,Object> dataMap=new HashMap<>();
                    dataMap.put("source_order_id",baseOrderInfo.getOrder_id());
                    retBody.put("data",dataMap);
                    retBody.put("errno",0);
                    retBody.put("error","success");
                } else {
                    retBody.put("errno",10011);
                    retBody.put("error",reasonForOrderPay + resultForXB);
                }
            }
        }else {
            retBody.put("errno",10011);
            retBody.put("error",reasonForOrderPay + "查看饿百订单详情失败！");
        }
    }

    /**
     * 物流状态
     * @param defaultSource
     * @param defaultSecret
     * @param orderId
     * @param paramBody
     * @param retBody
     * @author：Li.Deman
     * @date：2019/1/9
     */
    private void execDeliveryStatus(String client,String stoCode,String defaultSource,String defaultSecret,
                                    String orderId,Map<String, Object> paramBody,
                                    Map<String,Object> retBody,String orderOnService){
        /**
         * 运单状态
         * 目前推送的状态有：0:无配送状态 1:待请求配送
         * 2:生成运单,待分配配送商 3:请求配送
         * 4:待分配骑士 7:骑士接单 8:骑士取餐,配送中
         * 21:骑士到店
         * 15:配送取消 16:配送成功 17:配送异常
         * 18:商家自行配送 19:商家不再配送 20:物流拒单
         */
        String statusForDelivery = nullToEmpty(paramBody.get("status"));
        //定义接口调用操作日志map对象(外卖平台调用本系统接口、饿百)
        Map<String, Object> logMapForDeliveryStatus =
                createOperateLogMap("5",
                        "2",
                        "order.deliveryStatus.push",
                        "订单物流状态推送");
//        //成功、成功信息(表现为平台订单号等信息)
//        setOperateLogInfo(logMapForDeliveryStatus,"1","订单物流状态推送成功。平台订单号："
//                +orderId
//                +"，订单状态："+statusForDelivery);
        //定义一个公共的错误信息，用来重复利用
        String reasonForDeliveryStatus="订单物流状态推送失败。平台订单号："
                +orderId
                +"，订单状态："+statusForDelivery +"。";
        paramBody.put("platforms_order_id", orderId);
        //重置order_id，只保留platforms_order_id
        paramBody.put("order_id", "");
        //平台：饿百
        paramBody.put("platforms", "EB");
        Map<String, Object> retMapForDeliveryStatus = baseService.selectOrderInfoByMap(paramBody);
        if(nullToEmpty(retMapForDeliveryStatus.get("result")).equals("ok")){
            if (!"Y".equals(orderOnService)){
                logger.info("门店：["+String.valueOf(retMapForDeliveryStatus.get("shop_no"))+"]订单处理开关未打开！");
                setOperateLogInfo(logMapForDeliveryStatus,"0",
                        "门店：["+String.valueOf(retMapForDeliveryStatus.get("shop_no"))+"]订单处理开关未打开！");
                retBody.put("errno",10012);
                retBody.put("error","门店：["+String.valueOf(retMapForDeliveryStatus.get("shop_no"))+"]订单处理开关未打开！");
                //得把日志map记录到日志表呀
                baseService.insertOperatelogWithMap(logMapForDeliveryStatus);
            }else {
                //对于配送人员信息的获取，需要单独调用饿百接口
                Map<String,Object> deliveryInfoMap=EbApiUtil.getOrderDeliveryInfo(defaultSource,defaultSecret,orderId);
                if (deliveryInfoMap!=null){
                    /**
                     * 运单子状态
                     * 0:不存在对应子状态 1:商家取消 2:配送商取消
                     * 3:用户取消 4:物流系统取消
                     * 5:呼叫配送晚 6:餐厅出餐问题 7:商户中断配送
                     * 8:用户不接电话 9:用户退单 10:用户地址错误
                     * 11:超出服务范围 12:骑手标记异常
                     * 13:系统自动标记异常-订单超过3小时未送达
                     * 14:其他异常 15:超市取消/异常
                     * 101:只支持在线订单 102:超出服务范围
                     * 103:请求配送过晚,无法呼叫 104:系统异常
                     * 105:超出营业时间
                     */
                    String substatus=String.valueOf(deliveryInfoMap.get("substatus"));
                    //虽然饿百还传了更新时间这一信息，但是我们暂时不用，不存入本系统
                    String update_time=String.valueOf(deliveryInfoMap.get("update_time"));
                    /**
                     * 只有当骑士接单了，才能获取配送人员信息
                     * 唯一疑问：是否需要考虑运单子状态的各个状态？
                     */
                    if (statusForDelivery.equals("7")
                            && String.valueOf(deliveryInfoMap.get("status")).equals(statusForDelivery)){
                        //订单状态也变，置为2，就是已配送了，只记录动作
                        retMapForDeliveryStatus.put("status","2");
                        retMapForDeliveryStatus.put("action","8");
                        //之所以加这个，是为了记录到本系统订单系统
                        retMapForDeliveryStatus.put("last_action","8");
                        retMapForDeliveryStatus.put("shipper_name",nullToEmpty(deliveryInfoMap.get("name")));
                        retMapForDeliveryStatus.put("shipper_phone",nullToEmpty(deliveryInfoMap.get("phone")));
                        retMapForDeliveryStatus.put("notice_message","");
                        //只有当状态变化，才更新本系统，并推送pts
                        String resultForXB = baseService.updateOrderInfoWithMap(retMapForDeliveryStatus);
                        if (resultForXB.equals("ok")){
                            //订单状态变更推送PTS：用线程处理
                            //TaskExecutorUtil.asyncExecOrderStatusChangePush(retMapForDeliveryStatus);//秦青于删除20190116删除，暂时不需要推送配送员信息
                            retBody.put("errno",0);
                            retBody.put("error","success");
                        }else {
                            retBody.put("errno",10012);
                            retBody.put("error",resultForXB);
                        }
                    }
                    else {
                        //可能还会有其他状态下的不同处理，后续再说...暂时先返回OK
                        retBody.put("errno",0);
                        retBody.put("error","success");
                    }
                }else {
                    retBody.put("errno",10011);
                    retBody.put("error",reasonForDeliveryStatus + "获取订单配送信息失败！");
                }
            }
        }else {
            retBody.put("errno",10012);
            retBody.put("error",reasonForDeliveryStatus + retMapForDeliveryStatus.get("result"));
        }
    }

    /**
     * 催单
     * @param orderId
     * @param paramBody
     * @param retBody
     * @author：Li.Deman
     * @date：2019/1/9
     */
    private void execRemind(String client,String stoCode,String orderId,Map<String, Object> paramBody,
                                    Map<String,Object> retBody,String orderOnService){
        //定义接口调用操作日志map对象(外卖平台调用本系统接口、饿百)
        Map<String, Object> logMapForRemind = createOperateLogMap("5", "1",
                "order.remind.push", "订单催单通知");
//        //成功、成功信息(表现为平台订单号等信息)
//        setOperateLogInfo(logMapForRemind,"1","订单催单通知成功。平台订单号："
//                +orderId);
        //定义一个公共的错误信息，用来重复利用
        String reasonForRemind="订单催单通知失败。平台订单号："
                +orderId
                +"。";
        paramBody.put("platforms_order_id", orderId);
        paramBody.put("order_id", "");
        paramBody.put("platforms", "EB");
        //饿百催单，每次都会传create_time-用户发起催单时间(时间戳)，暂时不用
        String create_time=nullToEmpty(paramBody.get("create_time"));
        Map<String, Object> retMapForRemind = baseService.selectOrderInfoByMap(paramBody);
        if(nullToEmpty(retMapForRemind.get("result")).equals("ok")){
            if (!"Y".equals(orderOnService)){
                logger.info("门店：["+String.valueOf(retMapForRemind.get("shop_no"))+"]订单处理开关未打开！");
                setOperateLogInfo(logMapForRemind,"0",
                        "门店：["+String.valueOf(retMapForRemind.get("shop_no"))+"]订单处理开关未打开！");
                retBody.put("errno",10012);
                retBody.put("error","门店：["+String.valueOf(retMapForRemind.get("shop_no"))+"]订单处理开关未打开！");
                //得把日志map记录到日志表呀
                baseService.insertOperatelogWithMap(logMapForRemind);
            }else {
                //action：2-用户催单
                retMapForRemind.put("last_action", "2");
                retMapForRemind.put("action","2");
                retMapForRemind.put("notice_message","hint");
                //催单，状态不变
                String resultForXB = baseService.updateOrderInfoWithMap(retMapForRemind);
                if (resultForXB.equals("ok")){
                    //订单状态变更推送PTS：用线程处理
                    //获取当前时间timestamp
                    Timestamp lastTimestamp = new Timestamp((new Date()).getTime());
                    String shop_no = nullToEmpty(retMapForRemind.get("shop_no"));
                    String order_id = nullToEmpty(retMapForRemind.get("order_id"));
                    if (order_id.equals("") || shop_no.equals("")) return;
                    takeAwayService.pushToStoreMq(client,stoCode,"O2O","remind",retMapForRemind);
                    retBody.put("errno",0);
                    retBody.put("error","success");
                }else {
                    retBody.put("errno",10012);
                    retBody.put("error",resultForXB);
                }
            }
        }else {
            retBody.put("errno",10012);
            retBody.put("error",reasonForRemind + retMapForRemind.get("result"));
        }
    }

    /**
     * 订单状态
     * @param defaultSource
     * @param defaultSecret
     * @param orderId
     * @param paramBody
     * @param retBody
     * @author：Li.Deman
     * @date：2019/1/9
     */
    private void execOrderStatus(String client,String stoCode,String defaultSource,String defaultSecret,
                                 String orderId,Map<String, Object> paramBody,
                                 Map<String,Object> retBody,String orderOnService){
        //定义接口调用操作日志map对象(外卖平台调用本系统接口、饿百)
        Map<String, Object> logMapForOrderStatus = createOperateLogMap("5", "1",
                "order.status.push", "订单状态推送");
//        //成功、成功信息(表现为平台订单号等信息)
//        setOperateLogInfo(logMapForOrderStatus,"1","订单状态推送成功。平台订单号："
//                +orderId);
        //定义一个公共的错误信息，用来重复利用
        String reasonForOrderStatus="订单状态推送失败。平台订单号："
                +orderId
                +"。";
        /**
         * 目前推送的状态有：5、订单确认；
         * 7、骑士已接单开始取餐（此时可通过订单详情接口获取骑士手机号);
         * 8、骑士已取餐正在配送; 9、订单完成; 10、订单取消;
         * 15、订单退款；推送地址和创建订单地址相同。
         */
        String statusForOrder=nullToEmpty(paramBody.get("status"));
        //取消类型
        String type=nullToEmpty(paramBody.get("type"));
        //取消原因
        String reason=nullToEmpty(paramBody.get("reason"));
        //责任承担方
        String responsible_party=nullToEmpty(paramBody.get("responsible_party"));
        paramBody.put("platforms_order_id", orderId);
        //重置order_id，只保留platforms_order_id
        paramBody.put("order_id", "");
        //平台：饿百
        paramBody.put("platforms", "EB");
        Map<String, Object> retMapForOrderStatus = baseService.selectOrderInfoByMap(paramBody);
        if(nullToEmpty(retMapForOrderStatus.get("result")).equals("ok")){
            if (!"Y".equals(orderOnService)){
                logger.info("门店：["+String.valueOf(retMapForOrderStatus.get("shop_no"))+"]订单处理开关未打开！");
                setOperateLogInfo(logMapForOrderStatus,"0",
                        "门店：["+String.valueOf(retMapForOrderStatus.get("shop_no"))+"]订单处理开关未打开！");
                retBody.put("errno",10012);
                retBody.put("error","门店：["+String.valueOf(retMapForOrderStatus.get("shop_no"))+"]订单处理开关未打开！");
                //得把日志map记录到日志表呀
                baseService.insertOperatelogWithMap(logMapForOrderStatus);
            }else {
                //骑士已接单开始取餐，还是通过获取订单配送信息接口获取骑士信息
                if (statusForOrder.equals("7")){
                    Map<String,Object> deliveryInfoMap=EbApiUtil.getOrderDeliveryInfo(defaultSource,defaultSecret,orderId);
                    if (deliveryInfoMap!=null){
                        if (String.valueOf(deliveryInfoMap.get("status")).equals(statusForOrder)){
                            //也是骑士接单状态，获取配送人员信息
                            retMapForOrderStatus.put("shipper_name",nullToEmpty(deliveryInfoMap.get("name")));
                            retMapForOrderStatus.put("shipper_phone",nullToEmpty(deliveryInfoMap.get("phone")));
                            //这时候只记录动作，不记录状态，还未到已配送阶段
                            retMapForOrderStatus.put("action","8");
                            //之所以加这个，是为了记录到本系统订单系统
                            retMapForOrderStatus.put("last_action","8");
                            retMapForOrderStatus.put("notice_message","");
                            String resultForXB = baseService.updateOrderInfoWithMap(retMapForOrderStatus);
                            if (resultForXB.equals("ok")){
                                //订单状态变更推送PTS：用线程处理
                                //获取当前时间timestamp
                                Timestamp lastTimestamp = new Timestamp((new Date()).getTime());
                                String shop_no = nullToEmpty(retMapForOrderStatus.get("shop_no"));
                                String order_id = nullToEmpty(retMapForOrderStatus.get("order_id"));
                                if (order_id.equals("") || shop_no.equals("")) return;
                                takeAwayService.pushToStoreMq(client,stoCode,"O2O","orderStatus",retMapForOrderStatus);

                                retBody.put("errno",0);
                                retBody.put("error","success");
                            }else {
                                retBody.put("errno",10012);
                                retBody.put("error",resultForXB);
                            }
                        }else {
                            //当前订单配送信息接口，运单状态 为什么不是7？
                            logger.info("饿百API：订单状态推送接口，平台订单ID："+orderId
                                    +"，在获取订单配送信息接口时，"
                            +"运单状态不等于7（骑士已接单开始取餐），和该订单状态推送接口"
                            +"传过来的状态值不一致");
                            retBody.put("errno",10012);
                            retBody.put("error","当前订单配送信息接口，运单状态值不等于7（骑士已接单开始取餐）");
                        }
                    }else {
                        retBody.put("errno",10011);
                        retBody.put("error",reasonForOrderStatus + "获取订单配送信息失败！");
                    }
                }
//                else if (statusForOrder.equals("5")){
//                    //订单确认
//                    retMapForOrderStatus.put("status","1");
//                    retMapForOrderStatus.put("notice_message","");
//                    String resultForXB = baseService.updateOrderInfoWithMap(retMapForOrderStatus);
//                    if (resultForXB.equals("ok")){
//                        //订单状态变更推送PTS：用线程处理
//                        TaskExecutorUtil.asyncExecOrderStatusChangePush
//                                (retMapForOrderStatus);
//                        retBody.put("errno",0);
//                        retBody.put("error","success");
//                    }else {
//                        retBody.put("errno",10012);
//                        retBody.put("error",resultForXB);
//                    }
//                }
                else if (statusForOrder.equals("9")){
                    //订单完成
                    retMapForOrderStatus.put("status","3");
                    retMapForOrderStatus.put("notice_message","");
                    String resultForXB = baseService.updateOrderInfoWithMap(retMapForOrderStatus);
                    if (resultForXB.equals("ok")){
                        //订单状态变更推送PTS：用线程处理
                        //获取当前时间timestamp
                        Timestamp lastTimestamp = new Timestamp((new Date()).getTime());
                        String shop_no = nullToEmpty(retMapForOrderStatus.get("shop_no"));
                        String order_id = nullToEmpty(retMapForOrderStatus.get("order_id"));
                        if (order_id.equals("") || shop_no.equals("")) return;
                        takeAwayService.pushToStoreMq(client,stoCode,"O2O","orderStatus",retMapForOrderStatus);
                        retBody.put("errno",0);
                        retBody.put("error","success");
                    }else {
                        retBody.put("errno",10012);
                        retBody.put("error",resultForXB);
                    }
                }else if (statusForOrder.equals("10")){
                    //订单取消
                    retMapForOrderStatus.put("status","4");
                    //用户申请退单
                    retMapForOrderStatus.put("action","4");
                    //之所以加这个，是为了记录到本系统订单系统
                    retMapForOrderStatus.put("last_action","4");
                    //取消原因
                    retMapForOrderStatus.put("reason",reason);
                    //取消类型
                    retMapForOrderStatus.put("reason_code",type);
                    retMapForOrderStatus.put("notice_message","cancel");
                    String resultForXB = baseService.updateOrderInfoWithMap(retMapForOrderStatus);
                    if (resultForXB.equals("ok")){
                        //订单状态变更推送PTS：用线程处理
                        //获取当前时间timestamp
                        Timestamp lastTimestamp = new Timestamp((new Date()).getTime());
                        String shop_no = nullToEmpty(retMapForOrderStatus.get("shop_no"));
                        String order_id = nullToEmpty(retMapForOrderStatus.get("order_id"));
                        if (order_id.equals("") || shop_no.equals("")) return;
                        //takeAwayService.pushToStoreMq(client,stoCode,"O2O","orderStatus",retMapForOrderStatus);
                        String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                        retMapForOrderStatus.put("storeLogo", storeLogo);
                        takeAwayService.pushToStoreMq(client,stoCode,"O2O","cancel",retMapForOrderStatus);
                        logger.info("饿百推送取消订单，orderId:{},client:{},stoCode:{},storeLogo:{}", order_id, client, stoCode, storeLogo);
                        retBody.put("errno",0);
                        retBody.put("error","success");
                    }else {
                        retBody.put("errno",10012);
                        retBody.put("error",resultForXB);
                    }
                }else if (statusForOrder.equals("15")){
                    /**
                     * 订单退款。订单退款状态，对于本系统来说就是外卖平台同意退款，
                     * 已发生退款，区别于订单取消
                     */
                    retMapForOrderStatus.put("status","5");
                    //同意用户退款申请
                    retMapForOrderStatus.put("action","6");
                    //之所以加这个，是为了记录到本系统订单系统
                    retMapForOrderStatus.put("last_action","6");
                    //取消原因
                    retMapForOrderStatus.put("reason",reason);
                    //取消类型
                    retMapForOrderStatus.put("reason_code",type);
                    retMapForOrderStatus.put("notice_message","");
                    String resultForXB = baseService.updateOrderInfoWithMap(retMapForOrderStatus);
                    if (resultForXB.equals("ok")){
                        //同意退款的订单状态不需要推送给PTS了
                        //这边不需要记录退款金额了，因为在发起申请的时候，已经记录了退款金额
//                        //订单状态变更推送PTS：用线程处理
//                        TaskExecutorUtil.asyncExecOrderStatusChangePush
//                                (retMapForOrderStatus);
                        retBody.put("errno",0);
                        retBody.put("error","success");
                    }else {
                        retBody.put("errno",10012);
                        retBody.put("error",resultForXB);
                    }
                }else {
                    //其他状态也先不做处理
                    retBody.put("errno",0);
                    retBody.put("error","success");
                }
            }
        }else {
            retBody.put("errno",10012);
            retBody.put("error",reasonForOrderStatus +
                    retMapForOrderStatus.get("result"));
        }
    }

    /**
     * 用户申请订单取消/退款
     * @param orderId
     * @param paramBody
     * @param retBody
     * @author：Li.Deman
     * @date：2019/1/9
     */
    private void execUserCancelOrder(String client,String stoCode,String orderId,Map<String, Object> paramBody,
                                 Map<String,Object> retBody,String orderOnService){
        //定义接口调用操作日志map对象(外卖平台调用本系统接口、饿百)
        Map<String, Object> logMapForUserCancelOrder =
                createOperateLogMap("5", "2", "order.user.cancel", "用户申请订单取消/退款");
//        //成功、成功信息(表现为平台订单号等信息)
//        setOperateLogInfo(logMapForUserCancelOrder,"1","用户申请订单取消/退款成功。平台订单号："
//                +orderId);
        //定义一个公共的错误信息，用来重复利用
        String reasonForUserCancelOrder = "用户申请订单取消/退款失败。平台订单号：" + orderId + "。";
        /**
         * 消息类型。10:发起申请,20:客服介入,30:客服拒绝,40:客服同意,
         * 50:商户拒绝,60:商户同意,70:申请失效
         */
        String typeForUserCancelOrder=nullToEmpty(paramBody.get("type"));
        logger.info("用户申请订单取消/退款，消息类型（type）："+typeForUserCancelOrder);
        //申请取消原因
        String cancel_reason=nullToEmpty(paramBody.get("cancel_reason"));
        //申请取消附加原因
        String addition_reason=nullToEmpty(paramBody.get("addition_reason"));
        //拒绝原因
        String refuse_reason=nullToEmpty(paramBody.get("refuse_reason"));
        /**
         * 区分订单完成前用户全单取消或订单完成后全单退款流程。
         * 1表示订单完成前用户全单取消申请流程，
         * 2表示订单完成后用户全单退款申请流程
         */
        String cancel_type=nullToEmpty(paramBody.get("cancel_type"));

        paramBody.put("platforms_order_id", orderId);
        //重置order_id，只保留platforms_order_id
        paramBody.put("order_id", "");
        //平台：饿百
        paramBody.put("platforms", "EB");
        /**
         * 消息类型（typeForUserCancelOrder-10:发起申请），
         * 我们把取消原因记录到我们系统，并针对退款和退单做相应处理
         */
        if (typeForUserCancelOrder.equals("10")){
            /**
             * 1 表示订单完成前用户全单取消申请流程；  2表示订单完成后用户全单退款申请流程
             * 先不结合我们系统的状态进行判断，
             * 直接将当前平台传入状态覆盖到我们系统
             */
            if (cancel_type.equals("1")){
                Map<String, Object> retMapForUserCancelOrder = baseService.selectOrderInfoByMap(paramBody);
                if(nullToEmpty(retMapForUserCancelOrder.get("result")).equals("ok")){
                    if (!"Y".equals(orderOnService)){
                        logger.info("门店：["+String.valueOf(retMapForUserCancelOrder.get("shop_no"))+"]订单处理开关未打开！");
                        setOperateLogInfo(logMapForUserCancelOrder,"0",
                                "门店：["+String.valueOf(retMapForUserCancelOrder.get("shop_no"))+"]订单处理开关未打开！");
                        retBody.put("errno",10012);
                        retBody.put("error","门店：["+String.valueOf(retMapForUserCancelOrder.get("shop_no"))+"]订单处理开关未打开！");
                        //得把日志map记录到日志表呀
                        baseService.insertOperatelogWithMap(logMapForUserCancelOrder);
                    }else {
                        //为啥退
                        retMapForUserCancelOrder.put("reason",cancel_reason);
                        retMapForUserCancelOrder.put("action","4");
                        //之所以加这个，是为了记录到本系统订单系统
                        retMapForUserCancelOrder.put("last_action","4");
                        //retMapForUserCancelOrder.put("status","4"); //避免退单申请后，订单状态更新为已取消，POS无法操作的bug。
                        retMapForUserCancelOrder.put("notice_message","cancel");
                        String resultForXB = baseService.updateOrderInfoWithMap(retMapForUserCancelOrder);
                        if (resultForXB.equals("ok")){
                            //订单状态变更推送PTS：用线程处理
                            //获取当前时间timestamp
                            Timestamp lastTimestamp = new Timestamp((new Date()).getTime());
                            String shop_no = nullToEmpty(retMapForUserCancelOrder.get("shop_no"));
                            String order_id = nullToEmpty(retMapForUserCancelOrder.get("order_id"));
                            if (order_id.equals("") || shop_no.equals("")) return;
                            String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                            retMapForUserCancelOrder.put("storeLogo", storeLogo);
                            takeAwayService.pushToStoreMq(client,stoCode,"O2O","cancel",retMapForUserCancelOrder);
                            logger.info("饿百推送取消订单，orderId:{},client:{},stoCode:{},storeLogo:{}", order_id, client, stoCode, storeLogo);
                            retBody.put("errno",0);
                            retBody.put("error","success");
                        }else {
                            retBody.put("errno",10012);
                            retBody.put("error",resultForXB);
                        }
                    }
                }else {
                    retBody.put("errno",10012);
                    retBody.put("error",reasonForUserCancelOrder +
                            retMapForUserCancelOrder.get("result"));
                }
            }else{
                //2表示订单完成后用户全单退款申请流程
                PtsOrderInfo ptsOrderInfo=baseService.GetOrderInfo(paramBody);
                if (ptsOrderInfo.getResult().equals("ok")){
                    BaseOrderInfo baseOrderInfoForUserCancelOrder = getRefundBaseOrderInfo(client, stoCode, cancel_reason, ptsOrderInfo);
                    baseOrderInfoForUserCancelOrder.setRefund_money(ptsOrderInfo.getTotal_price());
                    List<BaseOrderProInfo> items = new ArrayList<>();
                    Map<Integer, String> refundQtyMap = new HashMap<>(4);
                    for (PtsOrderProInfo item : ptsOrderInfo.getItems()){
                        BaseOrderProInfo order_item=new BaseOrderProInfo();
                        order_item.setOrder_id(ptsOrderInfo.getOrder_id());
                        order_item.setMedicine_code(item.getMedicine_code());
                        //全额退款每个商品的退款数量都是下单数量
                        order_item.setRefund_qty(item.getQuantity());
                        refundQtyMap.put(Integer.valueOf(item.getSequence()), item.getQuantity());
                        //暂时不计退款金额
                        order_item.setSequence(item.getSequence());
                        order_item.setClient(baseOrderInfoForUserCancelOrder.getClient());
                        order_item.setStoCode(baseOrderInfoForUserCancelOrder.getStoCode());
                        items.add(order_item);
                    }
                    baseOrderInfoForUserCancelOrder.setItems(items);
                    String resultParamChange = nullToEmpty(getOrderInfoToPTSByMapForRefund(baseOrderInfoForUserCancelOrder).get("result"));
                    if (resultParamChange.equals("ok")){
                        if (!"Y".equals(orderOnService)){
                            logger.info("门店：["+ptsOrderInfo.getShop_no()+"]订单处理开关未打开！");
                            setOperateLogInfo(logMapForUserCancelOrder,"0","门店：["+ptsOrderInfo.getShop_no()+"]订单处理开关未打开！");
                            // 5.接口调用操作日志记录
                            baseService.insertOperatelogWithMap(logMapForUserCancelOrder);
                            retBody.put("errno",10012);
                            retBody.put("error","门店：["+ptsOrderInfo.getShop_no()+"]订单处理开关未打开！");
                        }else {
                            String resultForXB = baseService.updateOrderInfoWithEntity(baseOrderInfoForUserCancelOrder);
                            if (resultForXB.equals("ok")) {
                                //获取当前时间timestamp
                                Timestamp lastTimestamp = new Timestamp((new Date()).getTime());
                                Map<String,Object> inData = getOrderInfoToPTSByMapForRefund(baseOrderInfoForUserCancelOrder);
                                String shop_no = nullToEmpty(inData.get("shop_no"));
                                String order_id = nullToEmpty(inData.get("order_id"));
                                if (order_id.equals("") || shop_no.equals("")) return;
                                String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                                inData.put("storeLogo", storeLogo);
                                inData.put("refundQtyMap", refundQtyMap);
                                takeAwayService.pushToStoreMq(client,stoCode,"O2O","tuikuan",inData);
                                logger.info("饿百推送全额退款订单，orderId:{},client:{},stoCode:{},storeLogo:{},refundQtyMap:{}",
                                        order_id, client, stoCode, storeLogo, JSON.toJSONString(refundQtyMap));
                                retBody.put("errno",0);
                                retBody.put("error","success");
                            }else {
                                retBody.put("errno",10012);
                                retBody.put("error",resultForXB);
                            }
                        }
                    }else {
                        retBody.put("errno",10012);
                        retBody.put("error",resultParamChange);
                    }
                }else {
                    retBody.put("errno",10012);
                    retBody.put("error",ptsOrderInfo.getResult());
                }
            }
        }else {
            //其他状态不管，一律先返回OK
            retBody.put("errno",0);
            retBody.put("error","success");
        }
    }

    /**
     * 部分退款订单信息推送
     * @param orderId
     * @param paramBody
     * @param retBody
     * @author：Li.Deman
     * @date：2019/1/9
     */
    private void execPartRefund(String client,String stoCode,String orderId,Map<String, Object> paramBody,
                                     Map<String,Object> retBody,String orderOnService){
        // 定义接口调用操作日志map对象(外卖平台调用本系统接口、饿百)
        Map<String, Object> logMapForPartRefund =
                createOperateLogMap("5",
                        "2",
                        "order.partrefund.push",
                        "部分退款订单信息推送");
//        //成功、成功信息(表现为平台订单号等信息)
//        setOperateLogInfo(logMapForPartRefund,"1","部分退款订单信息推送成功。平台订单号："
//                +orderId);
        //定义一个公共的错误信息，用来重复利用
        String logReasonForPartRefund="部分退款订单信息推送失败。平台订单号："
                +orderId
                +"。";
        //部分退款类型 1表示商户发起的部分退款 2表示用户发起的部分退款
        String typeForPartRefund = nullToEmpty(paramBody.get("type"));
        //退款总金额,单位:分
        Integer refundPrice = (Integer) paramBody.get("refund_price");
        BigDecimal refundPriceResult = new BigDecimal(refundPrice).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
        String refund_price = nullToEmpty(refundPriceResult.toString());
        /**
         * 部分退款状态:10表示商家/用户发起部分退款申请
         * 20表示部分退款成功 30用户申请仲裁,客服介入
         * 40表示部分退款失败 50表示商家拒绝用户发起的部分退款申请
         */
        String statusForPartRefund=nullToEmpty(paramBody.get("status"));
        //部分退款原因
        String reasonForPartRefund=nullToEmpty(paramBody.get("reason"));
        //部分退款额外原因
        String additionReasonForPartRefund=nullToEmpty(paramBody.get("addition_reason"));
        paramBody.put("platforms_order_id", orderId);
        //重置order_id，只保留platforms_order_id
        paramBody.put("order_id", "");
        //平台：饿百
        paramBody.put("platforms", Constants.PLATFORMS_EB);
        //只支持：2表示用户发起的部分退款
        if (typeForPartRefund.equals("2")){
            //当：10表示用户发起部分退款申请，即传递信息，后续状态不再考虑
            if (statusForPartRefund.equals("10")){
                //订单+订单明细。model+items
                PtsOrderInfo ptsOrderInfo=baseService.GetOrderInfo(paramBody);
                if (ptsOrderInfo.getResult().equals("ok")){
                    BaseOrderInfo baseOrderInfoForPartRefund = getRefundBaseOrderInfo(client, stoCode, reasonForPartRefund, ptsOrderInfo);
                    baseOrderInfoForPartRefund.setRefund_money(refund_price);
                    //退款商品信息-Array。这边要不要对paramBody.get("refund_products")做是否为空判断？
                    List<Map<String, Object>> refund_products=(List<Map<String, Object>>)paramBody.get("refund_products");
                    List<BaseOrderProInfo> items = new ArrayList<>();
                    Map<String, Object> productMap = null;
                    Map<Integer, String> refundQtyMap = new HashMap<>(4);
                    for (int i = 0; i < refund_products.size(); i++){
                        productMap = refund_products.get(i);
                        BaseOrderProInfo order_item=new BaseOrderProInfo();
                        order_item.setClient(client);
                        order_item.setStoCode(stoCode);
                        //订单ID
                        order_item.setOrder_id(baseOrderInfoForPartRefund.getOrder_id());
                        //药品编码
                        String medicine_code=String.valueOf(productMap.get("custom_sku_id"));
                        order_item.setMedicine_code(medicine_code);
                        //退款商品数量
                        String returnQty = String.valueOf(productMap.get("number"));
                        order_item.setRefund_qty(returnQty);
                        //退款商品退回金额,单位:分
                        Integer totalRefund = (Integer) productMap.get("total_refund");
                        BigDecimal totalRefundResult = new BigDecimal(totalRefund).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
                        order_item.setRefund_price(totalRefundResult.toString());
                        //使用lambda表达式
                        List<PtsOrderProInfo> ptsList = ptsOrderInfo.getItems().stream().filter((PtsOrderProInfo p)->
                                medicine_code.equals(p.getMedicine_code())).collect(Collectors.toList());
                        //如果有，那么取第一个
                        if (ptsList.size() > 0) {
                            String sequence = ptsList.get(0).getSequence();
                            order_item.setSequence(sequence);
                            refundQtyMap.put(Integer.valueOf(sequence), returnQty);
                        }
//                        //部分退款也要赋值sequence
//                        for (PtsOrderProInfo ptsItem : ptsOrderInfo.getItems()){
//                            if (medicine_code.equals(ptsItem.getMedicine_code())){
//                                order_item.setSequence(ptsItem.getSequence());
//                                break;
//                            }
//                        }
                        items.add(order_item);
                    }
                    baseOrderInfoForPartRefund.setItems(items);
                    String resultParamChange = nullToEmpty(getOrderInfoToPTSByMapForRefund(baseOrderInfoForPartRefund).get("result"));
                    if (resultParamChange.equals("ok")){
                        if (!"Y".equals(orderOnService)){
                            logger.info("门店：["+ptsOrderInfo.getShop_no()+"]订单处理开关未打开！");
                            setOperateLogInfo(logMapForPartRefund,"0","门店：["+ptsOrderInfo.getShop_no()+"]订单处理开关未打开！");
                            // 5.接口调用操作日志记录
                            baseService.insertOperatelogWithMap(logMapForPartRefund);
                            retBody.put("errno",10012);
                            retBody.put("error","门店：["+ptsOrderInfo.getShop_no()+"]订单处理开关未打开！");
                        }else {
                            String resultForXB = baseService.updateOrderInfoWithEntity(baseOrderInfoForPartRefund);
                            if (resultForXB.equals("ok")){
                                //获取当前时间timestamp
                                Timestamp lastTimestamp = new Timestamp((new Date()).getTime());
                                Map<String,Object> inData = getOrderInfoToPTSByMapForRefund(baseOrderInfoForPartRefund);
                                String shop_no = nullToEmpty(inData.get("shop_no"));
                                String order_id = nullToEmpty(inData.get("order_id"));
                                if (order_id.equals("") || shop_no.equals("")) return;
                                String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                                inData.put("storeLogo", storeLogo);
                                inData.put("refundQtyMap", refundQtyMap);
                                takeAwayService.pushToStoreMq(client,stoCode,"O2O","tuikuan",inData);
                                logger.info("饿百推送部分退款订单，orderId:{},client:{},stoCode:{},storeLogo:{},refundQtyMap:{}",
                                        order_id, client, stoCode, storeLogo, JSON.toJSONString(refundQtyMap));
                                retBody.put("errno",0);
                                retBody.put("error","success");
                            }else {
                                retBody.put("errno",10012);
                                retBody.put("error",resultForXB);
                            }
                        }
                    }else {
                        retBody.put("errno",10012);
                        retBody.put("error",resultParamChange);
                    }
                }else {
                    retBody.put("errno",10012);
                    retBody.put("error",ptsOrderInfo.getResult());
                }
            }else {
                //其他状态暂时先不管，一律返回OK
                retBody.put("errno",0);
                retBody.put("error","success");
            }
        }else {
            retBody.put("errno",10012);
            retBody.put("error","暂不支持商户发起的部分退款");
        }
    }

    private BaseOrderInfo getRefundBaseOrderInfo(String client, String stoCode, String reasonForPartRefund, PtsOrderInfo ptsOrderInfo) {
        BaseOrderInfo baseOrderInfoForPartRefund = new BaseOrderInfo();
        baseOrderInfoForPartRefund.setPlatforms(ptsOrderInfo.getPlatforms());
        baseOrderInfoForPartRefund.setOrder_id(ptsOrderInfo.getOrder_id());
        baseOrderInfoForPartRefund.setPlatforms_order_id(ptsOrderInfo.getPlatforms_order_id());
        baseOrderInfoForPartRefund.setShop_no(ptsOrderInfo.getShop_no());
        baseOrderInfoForPartRefund.setRecipient_address(ptsOrderInfo.getRecipient_address());
        baseOrderInfoForPartRefund.setRecipient_name(ptsOrderInfo.getRecipient_name());
        baseOrderInfoForPartRefund.setRecipient_phone(ptsOrderInfo.getRecipient_phone());
        baseOrderInfoForPartRefund.setShipping_fee(ptsOrderInfo.getShipping_fee());
        baseOrderInfoForPartRefund.setTotal_price(ptsOrderInfo.getTotal_price());
        baseOrderInfoForPartRefund.setOriginal_price(ptsOrderInfo.getOriginal_price());
        baseOrderInfoForPartRefund.setCustomer_pay(ptsOrderInfo.getCustomer_pay());
        baseOrderInfoForPartRefund.setCaution(ptsOrderInfo.getCaution());
        baseOrderInfoForPartRefund.setShipper_name(ptsOrderInfo.getShipper_name());
        baseOrderInfoForPartRefund.setShipper_phone(ptsOrderInfo.getShipper_phone());
        baseOrderInfoForPartRefund.setStatus(ptsOrderInfo.getStatus());
        baseOrderInfoForPartRefund.setAction("5");
        baseOrderInfoForPartRefund.setIs_invoiced(ptsOrderInfo.getIs_invoiced());
        baseOrderInfoForPartRefund.setInvoice_title(ptsOrderInfo.getInvoice_title());
        baseOrderInfoForPartRefund.setTaxpayer_id(ptsOrderInfo.getTaxpayer_id());
        baseOrderInfoForPartRefund.setDelivery_time(ptsOrderInfo.getDelivery_time());
        baseOrderInfoForPartRefund.setPay_type(ptsOrderInfo.getPay_type());
        baseOrderInfoForPartRefund.setOrder_info(ptsOrderInfo.getOrder_info());
        baseOrderInfoForPartRefund.setCreate_time(ptsOrderInfo.getCreate_time());
        baseOrderInfoForPartRefund.setUpdate_time(ptsOrderInfo.getUpdate_time());
        //reason_code
        baseOrderInfoForPartRefund.setReason(reasonForPartRefund);
        baseOrderInfoForPartRefund.setDay_seq(ptsOrderInfo.getDay_seq());
        baseOrderInfoForPartRefund.setNotice_message("refund");
        baseOrderInfoForPartRefund.setClient(client);
        baseOrderInfoForPartRefund.setStoCode(stoCode);
        return baseOrderInfoForPartRefund;
    }
}
