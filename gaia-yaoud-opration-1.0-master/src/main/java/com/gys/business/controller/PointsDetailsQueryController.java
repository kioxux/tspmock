package com.gys.business.controller;

import com.gys.business.service.PointsDetailsQueryService;
import com.gys.business.service.data.PointsDetailsQueryInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 5.会员管理中增加积分明细查询
 *
 * @author xiaoyuan on 2020/7/28
 */
@Api(tags = "会员积分明细查询")
@RestController
@RequestMapping({"/pointsDetailsQuery"})
public class PointsDetailsQueryController extends BaseController {

    @Autowired
    private PointsDetailsQueryService queryService;

    @ApiOperation(value = "列表查询")
    @PostMapping("/selectPointsDetailsQuery")
    public JsonResult selectPointsDetailsQueryByNameAndId(HttpServletRequest request,@Valid @RequestBody PointsDetailsQueryInData queryInData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        queryInData.setClient(userInfo.getClient());
        queryInData.setBrId(userInfo.getDepId());
        return JsonResult.success(queryService.selectPointsDetailsQueryByNameAndId(queryInData),"查询成功");
    }

    @PostMapping("/selectPointsDetailsQueryExport")
    public Result selectPointsDetailsQueryExport(HttpServletRequest request, @Valid @RequestBody PointsDetailsQueryInData queryInData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        queryInData.setClient(userInfo.getClient());
        queryInData.setBrId(userInfo.getDepId());
        return queryService.selectPointsDetailsQueryExport(queryInData);
    }

    @PostMapping("/getSaleStore")
    public JsonResult getSaleStore(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(queryService.getSaleStore(userInfo.getClient()),"提示：查询成功！");
    }
}
