package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.gys.business.mapper.entity.GaiaStoreDiscountData;
import com.gys.business.service.GaiaStoreDiscountService;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.gys.common.base.BaseController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 门店折扣率维护
 * </p>
 *
 * @author zz
 * @since 2022-01-27
 */
@RestController
@RequestMapping("/gaiaStoreDiscount")
@Slf4j
public class GaiaStoreDiscountController extends BaseController {
    @Autowired
    private GaiaStoreDiscountService gaiaStoreDiscountService;

    @PostMapping({"getStoTaxRateList"})
    public JsonResult getStoTaxRateList(HttpServletRequest request) {
        GetLoginOutData userInfo = getLoginUser(request);
        return JsonResult.success(gaiaStoreDiscountService.getStoTaxRateList(userInfo.getClient()), "查询成功");
    }

    @PostMapping({"getStoCodeList"})
    public JsonResult getSearchList(HttpServletRequest request, @Valid @RequestBody Map<String, Object> inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.put("client", userInfo.getClient());
        return JsonResult.success(gaiaStoreDiscountService.getStoCodeList(inData), "查询成功");
    }
    /**
     * 门店折扣率维护查询
     */
    @PostMapping("/selectDiscouList")
    public JsonResult selectDiscouList(HttpServletRequest request, @RequestBody GaiaStoreDiscountData gaiaStoreDiscountData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        gaiaStoreDiscountData.setClient(userInfo.getClient());
        log.info(String.format("<门店折扣率维护><查询><请求参数：%s>\"", JSONUtil.toJsonStr(gaiaStoreDiscountData)));
        return JsonResult.success(gaiaStoreDiscountService.selectDiscouList(gaiaStoreDiscountData), "查询成功");
    }
    /**
     * 门店折扣率维护导出
     */
    @PostMapping("/exportDiscoun")
    public Result exportDiscoun(HttpServletRequest request, @RequestBody GaiaStoreDiscountData gaiaStoreDiscountData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        gaiaStoreDiscountData.setClient(userInfo.getClient());
        log.info(String.format("<门店折扣率维护><导出><请求参数：%s>", JSONUtil.toJsonStr(gaiaStoreDiscountData)));
        return this.gaiaStoreDiscountService.exportDiscoun(userInfo, gaiaStoreDiscountData);
    }
    /**
     * 门店折扣率维护添加
     */
    @PostMapping("/insertStore")
    public JsonResult insertStore(HttpServletRequest request, @RequestBody List<GaiaStoreDiscountData> gaiaStoreDiscountDataList) {
        GetLoginOutData loginUser = this.getLoginUser(request);
        log.info(String.format("<门店折扣率维护><添加><请求参数：%s>\"", JSONUtil.toJsonStr(gaiaStoreDiscountDataList)));
        return JsonResult.success(this.gaiaStoreDiscountService.insertStore(loginUser, gaiaStoreDiscountDataList), "提示：保存数据成功！");

    }
    /**
     * 门店折扣率维护导入
     */
    @PostMapping("/importStoreDiscoun")
    public JsonResult importStoreDiscoun(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        GetLoginOutData userInfo = getLoginUser(request);
        return JsonResult.success(gaiaStoreDiscountService.importStoreDiscoun(file, userInfo), "提示：导入数据成功！");
    }
    /**
    * 门店折扣率维护修改
    */
    @PostMapping("/updateStoRebate")
    public JsonResult updateStoRebate(HttpServletRequest request, @RequestBody GaiaStoreDiscountData
            gaiaStoreDiscountData) {
        GetLoginOutData userInfo = getLoginUser(request);
        return JsonResult.success(gaiaStoreDiscountService.updateStoRebate(userInfo, gaiaStoreDiscountData), "提示：修改数据成功！");
    }
}
