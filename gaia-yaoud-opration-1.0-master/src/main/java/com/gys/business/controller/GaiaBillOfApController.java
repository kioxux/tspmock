package com.gys.business.controller;

import com.gys.business.service.IGaiaBillOfApService;
import com.gys.business.service.data.billOfAp.GaiaBillOfApDto;
import com.gys.business.service.data.billOfAp.GaiaBillOfApExcelDto;
import com.gys.business.service.data.billOfAp.GaiaBillOfApParam;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.excel.ExcelUtil;
import com.gys.common.exception.BusinessException;
import com.gys.common.redis.RedisManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 供应商应付明细清单表 前端控制器
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Slf4j
@RestController
@Api(tags = "供应商应付明细清单表接口")
@RequestMapping("/billOfAp")
public class GaiaBillOfApController extends BaseController {
    @Autowired
    private IGaiaBillOfApService billOfApService;
    @Autowired
    RedisManager redisManager;

    /**
     * 功能描述: 每日凌晨 统计上一天的供应商应付
     *
     * @param
     * @return void
     * @author wangQc
     * @date 2021/8/25 3:09 下午
     */
//    @Scheduled(cron = "0 0 0 * * ?")
    @GetMapping("/dailyInsert")
    public void dailyInsert(Integer minusDays) throws ParseException {
        log.info("=======开始统计上一天的供应商应付定时任务===========");
        //抢占锁
        boolean possession = redisManager.getLock("bill_of_ap_daily_insert_lock", 1);
        if (possession) {
            log.info("统计上一天的供应商应付===执行业务逻辑");
            billOfApService.dailyInsert(LocalDate.now().minusDays(Objects.isNull(minusDays)?1:minusDays));
        }
    }


    /**
     * 功能描述: 手动录入
     *
     * @param request
     * @param inData
     * @return com.gys.common.data.JsonResult
     * @author wangQc
     * @date 2021/8/25 8:54 下午
     */
    @PostMapping("/save")
    @ApiOperation(value = "手动录入", notes = "手动录入")
    public JsonResult save(HttpServletRequest request, @Valid @RequestBody List<GaiaBillOfApDto> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        return JsonResult.success(billOfApService.save(userInfo, inData), "提示：操作成功！");
    }


    /**
     * 功能描述: 批量导入
     *
     * @param request
     * @param file
     * @return com.gys.common.data.JsonResult
     * @author wangQc
     * @date 2021/8/25 8:54 下午
     */
    @PostMapping("/batchImport")
    @ApiOperation(value = "批量导入）", notes = "批量导入")
    public JsonResult batchImport(HttpServletRequest request, @RequestParam("file") MultipartFile file) throws Exception {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        if (file.isEmpty()) {
            throw new BusinessException("上传文件为空");
        }
        ExcelUtil<GaiaBillOfApExcelDto> util = new ExcelUtil(GaiaBillOfApExcelDto.class);
        List<GaiaBillOfApExcelDto> dataList = util.importExcel(file.getInputStream());
        if (CollectionUtils.isNotEmpty(dataList)) {
            billOfApService.batchImport(userInfo, dataList);
        }
        return JsonResult.success();
    }

    /**
     * 功能描述: 期初导入
     *
     * @param request
     * @param file
     * @return com.gys.common.data.JsonResult
     * @author wangQc
     * @date 2021/8/25 8:54 下午
     */
    @PostMapping("/beginPeriodImport")
    @ApiOperation(value = "期初导入）", notes = "期初导入")
    public JsonResult beginPeriodImport(HttpServletRequest request, @RequestParam("file") MultipartFile file) throws Exception {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        if (file.isEmpty()) {
            throw new BusinessException("上传文件为空");
        }
        ExcelUtil<GaiaBillOfApExcelDto> util = new ExcelUtil(GaiaBillOfApExcelDto.class);
        List<GaiaBillOfApExcelDto> dataList = util.importExcel(file.getInputStream());
        if (CollectionUtils.isNotEmpty(dataList)) {
            billOfApService.beginPeriodImport(userInfo, dataList);
        }
        return JsonResult.success();
    }

    @PostMapping("/detailList")
    @ApiOperation(value = "供应商应付明细列表")
    public JsonResult detailList(HttpServletRequest request, @Valid @RequestBody GaiaBillOfApParam gaiaBillOfApParam) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }

        gaiaBillOfApParam.setClient(userInfo.getClient());
        return JsonResult.success(billOfApService.detailList(gaiaBillOfApParam, userInfo), "提示：查询成功！");
    }

    @PostMapping("/detailList/export")
    @ApiOperation(value = "供应商应付明细列表")
    public void detailListExport(
            HttpServletRequest request,
            HttpServletResponse response,
            @Valid @RequestBody GaiaBillOfApParam gaiaBillOfApParam) throws IOException {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }

        gaiaBillOfApParam.setClient(userInfo.getClient());

        SXSSFWorkbook workbook = billOfApService.detailListExport(gaiaBillOfApParam, userInfo);

        String filename = "供应商应付明细列表.csv";
        OutputStream output = response.getOutputStream();
        response.reset();
        String codedFileName = java.net.URLEncoder.encode(filename, "UTF-8");
        response.setHeader("Content-disposition", "attachment; filename=" + new String(codedFileName.getBytes(), "ISO8859-1"));
        response.setContentType("application/vnd.ms-excel");
        workbook.write(output);
        workbook.close();
        output.flush();
        output.close();
    }


    @PostMapping("/list")
    @ApiOperation(value = "供应商应付清单表查询（分页）")
    public JsonResult listPage(HttpServletRequest request, @Valid @RequestBody GaiaBillOfApParam gaiaBillOfApParam) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }

        gaiaBillOfApParam.setClient(userInfo.getClient());
        return JsonResult.success(billOfApService.getListPage(gaiaBillOfApParam, userInfo), "提示：查询成功！");
    }


    @ApiOperation(value = "导出列表")
    @PostMapping("/list/export")
    public void paramExport(
            HttpServletRequest request,
            HttpServletResponse response,
            @Valid @RequestBody GaiaBillOfApParam gaiaBillOfApParam) throws Exception {

        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }

        gaiaBillOfApParam.setClient(userInfo.getClient());

        SXSSFWorkbook workbook = billOfApService.listExport(gaiaBillOfApParam, userInfo);

        String filename = "供应商应付列表.csv";
        OutputStream output = response.getOutputStream();
        response.reset();
        String codedFileName = java.net.URLEncoder.encode(filename, "UTF-8");
        response.setHeader("Content-disposition", "attachment; filename=" + new String(codedFileName.getBytes(), "ISO8859-1"));
        response.setContentType("application/vnd.ms-excel");
        workbook.write(output);
        workbook.close();
        output.flush();
        output.close();
    }





    @ApiOperation(value = "获取供应商营业额列表")
    @GetMapping("/supplierSalesman")
    public JsonResult supplierSalesman(HttpServletRequest request,String depId,String stoCode,String supSelfCode,String name){
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }

        return JsonResult.success(billOfApService.supplierSalesman(userInfo,depId,stoCode,supSelfCode,name), "提示：查询成功！");
    }

    @GetMapping("updateEndingBalance")
    public ReturnT<String> updateEndingBalance() {
        XxlJobHelper.log("修改供应商余额===执行业务逻辑");
        billOfApService.updateEndingBalance();
        XxlJobHelper.log("修改供应商余额===执行结束");
        return ReturnT.SUCCESS;
    }
}

