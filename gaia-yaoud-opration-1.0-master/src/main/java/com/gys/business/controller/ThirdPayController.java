package com.gys.business.controller;

import com.gys.business.service.ThirdPayService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Api(tags = "第三方支付", description = "/tempHumi")
@RestController
@RequestMapping({"/thirdPay/"})
public class ThirdPayController extends BaseController {

    @Autowired
    private ThirdPayService thirdPayService;

    @ApiOperation(value = "支付")
    @PostMapping({"/pay"})
    public JsonResult pay(HttpServletRequest request, @RequestBody Map<String,Object> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.thirdPayService.pay(userInfo,inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "查询")
    @PostMapping({"/query"})
    public JsonResult query(HttpServletRequest request, @RequestBody Map<String,Object> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.thirdPayService.query(userInfo,inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "退款")
    @PostMapping({"/refundBack"})
    public JsonResult refundBack(HttpServletRequest request, @RequestBody Map<String,Object> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.thirdPayService.refundBack(userInfo,inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "撤销")
    @PostMapping({"/revokePay"})
    public JsonResult revokePay(HttpServletRequest request, @RequestBody Map<String,Object> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.thirdPayService.revokePay(userInfo,inData), "提示：获取数据成功！");
    }
}
