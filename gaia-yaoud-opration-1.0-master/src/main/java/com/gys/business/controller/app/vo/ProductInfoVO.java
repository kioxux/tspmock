package com.gys.business.controller.app.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/9 17:02
 **/
@Data
public class ProductInfoVO {
    @ApiModelProperty(value = "加盟商", hidden = true)
    private String client;
    @ApiModelProperty(value = "商品位置", hidden = true)
    private String proSite;
    @ApiModelProperty(value = "商品编码", example = "10008736")
    private String proSelfCode;
    @ApiModelProperty(value = "商品名称", example = "阿胶")
    private String proSelfName;
    @ApiModelProperty(value = "排名", example = "2")
    private Integer sortNum;
    @ApiModelProperty(value = "商品规格", example = "250g(红盒)")
    private String proSpec;
    @ApiModelProperty(value = "生产厂家", example = "河北东汝阿胶制药股份有限公司")
    private String proFactoryName;
    @ApiModelProperty(value = "月均销量", example = "130")
    private BigDecimal monthSaleNum;
    @ApiModelProperty(value = "月均销售额", example = "12350")
    private BigDecimal monthSaleAmount;
    @ApiModelProperty(hidden = true)
    private BigDecimal monthProfitAmount;
    @ApiModelProperty(value = "状态：0-未处理 1-已处理 2-已淘汰", example = "1")
    private Integer status;
    @ApiModelProperty(value = "缺断货流水号")
    private String voucherId;
}
