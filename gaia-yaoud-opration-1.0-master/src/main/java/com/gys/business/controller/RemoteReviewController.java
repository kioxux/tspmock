package com.gys.business.controller;

import com.gys.business.service.RemoteReviewService;
import com.gys.business.service.YunNanZYYSSuperviseService;
import com.gys.business.service.data.StoreListOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.data.remoteReview.dto.QueryReviewOutDataDTO;
import com.gys.common.data.remoteReview.dto.QueryReviewParamDTO;
import com.gys.common.excel.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "远程审方", description = "/remote-review")



@RestController
@RequestMapping("/remote-review")
public class RemoteReviewController extends BaseController {

    @Autowired
    private RemoteReviewService remoteReviewService;

    @Autowired
    private YunNanZYYSSuperviseService yunNanZYYSSuperviseService;

    @ApiOperation("返回审核状态列表")
    @GetMapping({"queryReviewCheckStatus"})
    public JsonResult queryReviewCheckStatus() {
        return JsonResult.success(remoteReviewService.queryReviewCheckStatus(), "提示：获取数据成功");
    }


    @ApiOperation("根据条件查询处方信息列表")
    @PostMapping({"queryReviewList"})
    public JsonResult queryReviewList(HttpServletRequest request,
                                      @ApiParam("页码") @RequestParam(required = false) Integer pageNum,
                                      @ApiParam("页大小") @RequestParam(required = false) Integer pageSize,
                                      @ApiParam("查询远程审方列表入参") @RequestBody QueryReviewParamDTO queryReviewParamDTO) {
        pageNum = (ObjectUtils.isEmpty(pageNum) || pageNum == 0) ? 1 : pageNum;
        pageSize = (ObjectUtils.isEmpty(pageSize) || pageSize == 0) ? 10 : pageSize;
        GetLoginOutData userInfo = this.getLoginUser(request);
        PageInfo<QueryReviewOutDataDTO> queryReviewOutDataDTOPageInfo = remoteReviewService.queryReviewList(userInfo, queryReviewParamDTO, pageNum, pageSize);
        return JsonResult.success(queryReviewOutDataDTOPageInfo, "提示：获取数据成功");
    }


    @ApiOperation("审核")
    @PostMapping({"approveReview"})
    public JsonResult approveReview(HttpServletRequest request, @ApiParam("销售单号") @RequestBody List<String> gssrSaleBillNoList) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        remoteReviewService.approveReview(userInfo, gssrSaleBillNoList);
        // 如果client == 10000308 ，审核通过调http://172.19.1.13:3000/project/17/interface/api/7439 接口
        if (StringUtil.equals(userInfo.getClient(),"10000308")){
            remoteReviewService.remoteCallYunNan(userInfo,gssrSaleBillNoList);
        }
        return JsonResult.success("提示：审核成功");
    }


    @ApiOperation("驳回")
    @PostMapping({"rejectReview"})
    public JsonResult rejectReview(HttpServletRequest request, @ApiParam("销售单号") @RequestBody List<String> gssrSaleBillNoList) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        remoteReviewService.rejectReview(userInfo, gssrSaleBillNoList);
        return JsonResult.success("提示：驳回成功");
    }


    @ApiOperation("获取审核校验方式")
    @GetMapping({"authenticationType"})
    public JsonResult authenticationType(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        Integer examineType = remoteReviewService.queryAuthenticationType(userInfo);
        return JsonResult.success(examineType);
    }


    @ApiOperation("校验登录人")
    @GetMapping({"checkRegister"})
    public JsonResult checkRegister(HttpServletRequest request,
                                    @ApiParam(value = "校验方式") @RequestParam String authenticationType,
                                    @ApiParam(value = "密码") @RequestParam(required = false) String password,
                                    @ApiParam(value = "指纹") @RequestParam(required = false) Object fingerMark) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        switch (authenticationType) {
            case "0":
                remoteReviewService.checkRegisterByPassword(userInfo, password);
                return JsonResult.success("校验成功");
            case "1":
                remoteReviewService.checkRegisterByFingerMark(userInfo, password);
                return JsonResult.success("校验成功");
            case "2":
                remoteReviewService.checkRegisterByPasswordAndFingerMark(userInfo, password,fingerMark);
                return JsonResult.success("校验成功");
            default:
                return JsonResult.error("请传入正确参数");
        }
    }

    @ApiOperation(value = "上传门店列表查询", response = StoreListOutData.class)
    @PostMapping({"storeListQry"})
    public JsonResult storeListQry(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<StoreListOutData> list = this.remoteReviewService.storeListQry(userInfo.getClient());
        return JsonResult.success(list, "提示：获取数据成功！");
    }
}
