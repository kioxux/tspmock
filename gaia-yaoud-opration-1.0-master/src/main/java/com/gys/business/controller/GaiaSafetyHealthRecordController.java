package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSafetyHealthRecord;
import com.gys.business.service.GaiaSafetyHealthRecordService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
@RequestMapping("/safetyHealthRecord/")
public class GaiaSafetyHealthRecordController extends BaseController {

    @Resource
    private GaiaSafetyHealthRecordService gaiaSafetyHealthRecordService;


    @PostMapping({"add"})
    public JsonResult add(HttpServletRequest request, @RequestBody GaiaSafetyHealthRecord inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoreId(userInfo.getDepId());
        inData.setStoreName(userInfo.getDepName());
        inData.setCreateUser(userInfo.getLoginName()+"-"+userInfo.getUserId());
        inData.setUpdateUser(userInfo.getLoginName()+"-"+userInfo.getUserId());
        inData.setUpdateTime(new Date());
        this.gaiaSafetyHealthRecordService.add(inData);
        return JsonResult.success("", "提示：新增成功！");
    }

    @PostMapping({"delete"})
    public JsonResult delete(HttpServletRequest request, @RequestBody GaiaSafetyHealthRecord inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoreId(userInfo.getDepId());
        inData.setUpdateUser(userInfo.getLoginName()+"-"+userInfo.getUserId());
        inData.setUpdateTime(new Date());
        this.gaiaSafetyHealthRecordService.delete(inData);
        return JsonResult.success("", "提示：删除成功！");
    }
    @PostMapping({"update"})
    public JsonResult update(HttpServletRequest request, @RequestBody GaiaSafetyHealthRecord inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoreId(userInfo.getDepId());
        inData.setUpdateUser(userInfo.getLoginName()+"-"+userInfo.getUserId());
        inData.setUpdateTime(new Date());
        this.gaiaSafetyHealthRecordService.update(inData);
        return JsonResult.success("", "提示：更新成功！");
    }
    @PostMapping({"query"})
    public JsonResult query(HttpServletRequest request, @RequestBody GaiaSafetyHealthRecord inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoreId(userInfo.getDepId());
        return JsonResult.success( this.gaiaSafetyHealthRecordService.query(inData), "提示：获取数据成功！");
    }
    @PostMapping({"submit"})
    public JsonResult submit(HttpServletRequest request, @RequestBody GaiaSafetyHealthRecord inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoreId(userInfo.getDepId());
        inData.setUpdateUser(userInfo.getLoginName()+"-"+userInfo.getUserId());
        inData.setUpdateTime(new Date());
        this.gaiaSafetyHealthRecordService.submit(inData);
        return JsonResult.success("", "提示：提交成功！");
    }
    @PostMapping({"export"})
    public Result export(HttpServletRequest request, @RequestBody GaiaSafetyHealthRecord inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoreId(userInfo.getDepId());
        return this.gaiaSafetyHealthRecordService.export(inData);
    }
    @PostMapping("getUserListByClientAndDep")
    public Result getUserListByClient(HttpServletRequest request) {
        GetLoginOutData userInfo = getLoginUser(request);
        GaiaSafetyHealthRecord inData = new GaiaSafetyHealthRecord();
        inData.setClient(userInfo.getClient());
        inData.setStoreId(userInfo.getDepId());
        return this.gaiaSafetyHealthRecordService.getUserList(inData);
    }

}
