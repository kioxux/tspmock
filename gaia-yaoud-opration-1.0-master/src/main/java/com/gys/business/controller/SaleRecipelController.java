//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.SaleRecipelService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "销售处方", description = "/saleRecipel")
@RestController
@RequestMapping({"/saleRecipel/"})
public class SaleRecipelController extends BaseController {
    @Autowired
    private SaleRecipelService saleRecipelService;

    public SaleRecipelController() {
    }

    @ApiOperation(value = "处方登记查询")
    @PostMapping({"/auditedSaleRecipelList"})
    public JsonResult getAuditedSaleRecipelList(HttpServletRequest request, @RequestBody GaiaSdSaleRecipelInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.saleRecipelService.getAuditedSaleRecipelList(inData), "提示：获取数据成功！");
    }


    @ApiOperation(value = "查询门店销售记录",response = GaiaSdSaleRecipelRecordOutData.class)
    @PostMapping({"/saleRecipelRecordList"})
    public JsonResult getSaleRecipelRecordList(HttpServletRequest request,@ApiParam(value = "销售处方记录", required = true) @RequestBody GaiaSdSaleRecipelRecordInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsshBrId(userInfo.getDepId());
        return JsonResult.success(this.saleRecipelService.getSaleRecipelRecordList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"/addOrEditRecipelRecord"})
    public JsonResult addOrEditRecipelRecord(@RequestBody ConllectionInData inData) {
        this.saleRecipelService.addOrEditRecipelRecord(inData.getGaiaSdSaleRecipelRecordInData());
        return JsonResult.success("", "提示：获取数据成功！");
    }

    @PostMapping({"/auditRecipelRecord"})
    public JsonResult auditRecipelRecord(@RequestBody ConllectionInData inData) {
        this.saleRecipelService.auditRecipelRecord(inData.getGaiaSdSaleRecipelRecordInData());
        return JsonResult.success("", "提示：获取数据成功！");
    }


    @PostMapping({"/getControlledDrugsManage"})
    @ApiOperation(value = "查询门店管制药品销售记录")
    public JsonResult getControlledDrugsManage(HttpServletRequest request,@RequestBody ControlledDrugsInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.saleRecipelService.getControlledDrugsManage(inData), "提示：获取数据成功！");
    }

    //获取处方商品明细
    @PostMapping({"/getProductInfo"})
    public JsonResult getProductInfo(HttpServletRequest request, @RequestBody SalePecipelProductInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setGssrBrId(userInfo.getDepId());
        return JsonResult.success(this.saleRecipelService.getProductInfo(inData),"提示：获取数据成功！");
    }

    //更新保存图片
    @PostMapping({"/updatePicture"})
    public JsonResult updatePicture(HttpServletRequest request, @RequestBody SaleRecipelPictureInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(saleRecipelService.updatePicture(inData), "提示：操作成功！");
    }
}
