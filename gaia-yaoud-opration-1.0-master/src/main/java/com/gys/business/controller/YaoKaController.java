package com.gys.business.controller;

import com.gys.business.service.YaoKaService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;

import com.pajk.CashCollectionOpen.model.pay.PayReq;
import com.pajk.CashCollectionOpen.model.pollReadyPlan.PollReadyPlanReq;
import com.pajk.CashCollectionOpen.model.preCalc.PreCalcReq;
import com.pajk.CashCollectionOpen.model.undoPay.UndoPayReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 胡鑫鑫
 */
@RestController
@RequestMapping({"/yaoKa"})
public class YaoKaController extends BaseController {
    @Autowired
    private YaoKaService yaoKaService;

    /**
     * 预结算
     */
    @PostMapping("/preCalcUtil")
    public JsonResult preCalcUtil(HttpServletRequest request, @RequestBody PreCalcReq preCalc){
        GetLoginOutData userInfo = this.getLoginUser(request);

        return JsonResult.success(yaoKaService.preCalcUtil(userInfo,preCalc), "提示：获取数据成功！");
    }

    /**
     * 结算
     */
    @PostMapping("/payUtil")
    public JsonResult payUtil(HttpServletRequest request, @RequestBody PayReq pay){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(yaoKaService.payUtil(userInfo,pay), "提示：获取数据成功！");
    }

    /**
     * 判断用户支付状态
     */
    @PostMapping("/pollReadyPlanUtil")
    public JsonResult pollReadyPlanUtil(HttpServletRequest request, @RequestBody PollReadyPlanReq preCalc){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(yaoKaService.pollReadyPlanUtil(userInfo,preCalc), "提示：获取数据成功！");
    }

    /**
     * 判断用户支付状态
     */
    @PostMapping("/pingAnRefund")
    public JsonResult pingAnRefund(HttpServletRequest request, @RequestBody UndoPayReq undoPayReq){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(yaoKaService.pingAnRefund(userInfo,undoPayReq), "提示：获取数据成功！");
    }
}
