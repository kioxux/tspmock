package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdBatchChange;
import com.gys.business.service.MedicalInsuranceService;
import com.gys.business.service.data.GetMedCheckOutData;
import com.gys.business.service.data.RebornData;
import com.gys.business.service.data.SalesInquireData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "医保")
@RestController
@RequestMapping({"/medicalInsurance/"})
public class MedicalInsuranceController extends BaseController {

    @Autowired
    MedicalInsuranceService medicalInsuranceService;

    @ApiOperation(value = "大同-易联众 医保商品查询",response = RebornData.class)
    @PostMapping({"selectMedProList"})
    public JsonResult selectMedProList(HttpServletRequest request, @ApiParam(value = "商品列表", required = true) @RequestBody List<RebornData> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<RebornData> list = this.medicalInsuranceService.selectMedProList(userInfo.getClient(),userInfo.getDepId(),inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "重庆-东软 医保商品查询",response = RebornData.class)
    @PostMapping({"selectMedProList2"})
    public JsonResult selectMedProList2(HttpServletRequest request, @ApiParam(value = "商品列表", required = true) @RequestBody List<RebornData> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<RebornData> list = this.medicalInsuranceService.selectMedProList2(userInfo.getClient(),userInfo.getDepId(),inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "大同-易联众 医保商品匹配",response = RebornData.class)
    @PostMapping({"matchMedProList"})
    public JsonResult matchMedProList(HttpServletRequest request, @ApiParam(value = "商品列表", required = true) @RequestBody List<GaiaSdBatchChange> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<RebornData> list = this.medicalInsuranceService.matchMedProList(userInfo.getClient(),userInfo.getDepId(),inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "大同-易联众 退货库存编码匹配 ",response = RebornData.class)
    @PostMapping({"matchReturnProBatchList"})
    public JsonResult matchReturnProBatchList(HttpServletRequest request, @ApiParam(value = "商品列表", required = true) @RequestBody SalesInquireData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<RebornData> list = this.medicalInsuranceService.matchReturnProBatchList(userInfo.getClient(),userInfo.getDepId(),inData);
        return JsonResult.success(list, "提示：获取数据成功！");
    }


}
