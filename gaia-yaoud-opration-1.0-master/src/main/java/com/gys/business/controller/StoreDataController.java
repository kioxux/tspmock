package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.service.DepDataService;
import com.gys.business.service.StoreDataService;
import com.gys.business.service.data.CompadmDcOutData;
import com.gys.business.service.data.GetUserStoreOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.CommonData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.webSocket.WebSocket;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping({"/storeData/"})
public class StoreDataController extends BaseController{
    @Autowired
    private StoreDataService storeDataService;
    @Autowired
    private DepDataService depDataService;
    @Autowired
    private WebSocket webSocket;

    @ApiOperation(value = "默认店长查询",response = GaiaStoreData.class)
    @PostMapping("queryStoLeader")
    public JsonResult queryStoLeader(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.storeDataService.queryStoLeader(userInfo), "提示：保存成功！");
    }

    @ApiOperation(value = "关联连锁查询门店列表",response = StoreOutData.class)
    @PostMapping("queryStoInfoList")
    public JsonResult queryStoInfoList(HttpServletRequest request,@RequestBody StoreOutData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
//        inData.setClient("21020006");
        return JsonResult.success(this.storeDataService.queryStoInfoList(inData), "提示：保存成功！");
    }

    @ApiOperation(value = "连锁公司列表查询",response = CompadmDcOutData.class)
    @PostMapping({"selectAuthCompadmList"})
    public JsonResult selectAuthCompadmList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
//        userInfo.setClient("21020006");
//        userInfo.setUserId("10000047");
        List<CompadmDcOutData> list = this.storeDataService.selectAuthCompadmList(userInfo.getClient(), userInfo.getUserId());
        return JsonResult.success(list, "提示：获取数据成功！");
    }


    @ApiOperation(value = "门店系统配置查询")
    @PostMapping("getStoreData")
    public JsonResult getStoreData(HttpServletRequest request){
        GetUserStoreOutData inData = new GetUserStoreOutData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.storeDataService.getStoreData(inData), "提示：保存成功！");
    }

    @ApiOperation(value = "门店查询")
    @PostMapping("getStoreList")
    public JsonResult getStoreList(HttpServletRequest request, @RequestBody CommonData data){
        GetUserStoreOutData inData = new GetUserStoreOutData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.storeDataService.getStoreList(userInfo.getClient(),data), "提示：保存成功！");
    }
}
