package com.gys.business.controller;

import com.gys.business.service.GaiaStoreOutSuggestionDService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionD)表控制层
 *
 * @author XIAO-ZY
 * @since 2021-10-28 10:53:43
 */
@RestController
@RequestMapping("gaiaStoreOutSuggestionD")
public class GaiaStoreOutSuggestionDController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaStoreOutSuggestionDService gaiaStoreOutSuggestionDService;

}

