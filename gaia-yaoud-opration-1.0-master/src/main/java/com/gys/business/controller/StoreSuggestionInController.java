package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.StoreInSuggestionService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.data.suggestion.InDetailSaveForm;
import com.gys.common.data.suggestion.SuggestionInForm;
import com.gys.common.data.suggestion.SuggestionInVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;

/**
 * @desc: 门店调剂-调入建议
 * @author: Ryan
 * @createTime: 2021/11/1 11:27
 */
@Slf4j
@RestController
@RequestMapping("storeInSuggestion")
public class StoreSuggestionInController extends BaseController {

    @Resource
    private StoreInSuggestionService storeInSuggestionService;

    /**
     * 调剂调入建议列表
     */
    @PostMapping("list")
    public JsonResult<PageInfo<SuggestionInVO>> list(HttpServletRequest request,@Valid @RequestBody SuggestionInForm suggestionInForm) {
        log.info("<门店调剂><调入列表><请求参数：{}>", JSON.toJSONString(suggestionInForm));
        GetLoginOutData loginUser = getLoginUser(request);
        suggestionInForm.setClient(loginUser.getClient());
        suggestionInForm.setStoCode(loginUser.getDepId());
        validParams(suggestionInForm);
        PageInfo<SuggestionInVO> pageInfo = storeInSuggestionService.list(suggestionInForm);
        return JsonResult.success(pageInfo);
    }

    /**
     * 调剂调入详情
     */
    @PostMapping("detail")
    public JsonResult detail(HttpServletRequest request, @RequestBody SuggestionInForm suggestionInForm) {
        log.info("<门店调剂><调入详情><请求参数：{}>", JSON.toJSONString(suggestionInForm));
        GetLoginOutData loginUser = getLoginUser(request);
        suggestionInForm.setClient(loginUser.getClient());
        suggestionInForm.setStoCode(loginUser.getDepId());
        Map<String, Object> detail = storeInSuggestionService.detail(suggestionInForm);
        return JsonResult.success(detail);
    }

    /**
     * 保存调入详情
     */
    @PostMapping("save")
    public JsonResult save(HttpServletRequest request, @Valid @RequestBody InDetailSaveForm detailSaveForm) {
        log.info("<门店调剂><调入详情><请求参数：{}>", JSON.toJSONString(detailSaveForm));
        GetLoginOutData loginUser = getLoginUser(request);
        detailSaveForm.setClient(loginUser.getClient());
        detailSaveForm.setStoCode(loginUser.getDepId());
        detailSaveForm.setUserId(loginUser.getUserId());
        detailSaveForm.setUserName(loginUser.getLoginName());
        storeInSuggestionService.saveDetail(detailSaveForm);
        return JsonResult.success();
    }

    /**
     * 调入确认
     */
    @PostMapping(value = "confirm")
    public JsonResult confirm(HttpServletRequest request, @Valid @RequestBody InDetailSaveForm detailSaveForm) {
        log.info("<门店调剂><调入详情><请求参数：{}>", JSON.toJSONString(detailSaveForm));
        GetLoginOutData loginUser = getLoginUser(request);
        detailSaveForm.setClient(loginUser.getClient());
        detailSaveForm.setStoCode(loginUser.getDepId());
        detailSaveForm.setUserId(loginUser.getUserId());
        detailSaveForm.setUserName(loginUser.getLoginName());
        storeInSuggestionService.confirm(detailSaveForm);
        return JsonResult.success();
    }

    private void validParams(SuggestionInForm suggestionInForm) {
        if (suggestionInForm.getPageNum() == null) {
            suggestionInForm.setPageNum(1);
        }
        if (suggestionInForm.getPageSize() == null || suggestionInForm.getPageSize() > 10000) {
            suggestionInForm.setPageNum(10);
        }
    }

}
