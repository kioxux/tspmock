package com.gys.business.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.service.AcceptService;
import com.gys.business.service.SelectService;
import com.gys.business.service.data.GetAcceptInData;
import com.gys.business.service.data.GetSupOutData;
import com.gys.business.service.data.SelectData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GaiaBillOfArInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({"/select/"})
@Api(tags = "下拉框查询", description = "/select")
public class SelectController  extends BaseController {

    @Resource
    private AcceptService acceptService;
    @Resource
    private SelectService selectService;

    @PostMapping({"getSupByBrIds"})
    @ApiOperation(value = "供应商列表查询" ,response = GetSupOutData.class)
    public JsonResult getSupByBrIds(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        if(inData.getStoreCodes() != null && inData.getStoreCodes().length > 0){
            if (ObjectUtil.isEmpty(inData.getStoreCodes()[0])){
                inData.setStoreCodes(null);
            }
        }
        return JsonResult.success(this.acceptService.getSupByBrIds(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getCustomer"})
    @ApiOperation(value = "客户下拉查询(仅供报表使用)" ,response = SelectData.class)
    public JsonResult getCustomer(HttpServletRequest request, @RequestBody GetAcceptInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        Map<String,String> map = new HashMap<>();
        map.put("client",userInfo.getClient());
        map.put("brId",userInfo.getDepId());
        if(inData.getStoreCodes() != null && inData.getStoreCodes().length > 0){
            if (ObjectUtil.isEmpty(inData.getStoreCodes()[0])){
                inData.setStoreCodes(null);
            }
        }
        return JsonResult.success(this.selectService.getCustomer(map), "提示：获取数据成功！");
    }

    @PostMapping({"listCustomer"})
    @ApiOperation(value = "查询客户集合" ,response = SelectData.class)
    public JsonResult listCustomer(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        return JsonResult.success(this.selectService.listCustomer(loginUser,inData), "提示：获取数据成功！");
    }

    @PostMapping({"listDc"})
    @ApiOperation(value = "查询仓库集合" ,response = SelectData.class)
    public JsonResult listDc(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        return JsonResult.success(this.selectService.listDc(loginUser,inData), "提示：获取数据成功！");
    }

    @PostMapping({"getProClass"})
    @ApiOperation(value = "商品分类编码" ,response = SelectData.class)
    public JsonResult getProClass() {
        return JsonResult.success(this.selectService.getProClass(), "提示：获取数据成功！");
    }
    @PostMapping({"getProBigClass"})
    @ApiOperation(value = "商品大类编码" ,response = SelectData.class)
    public JsonResult getProBigClass() {
        return JsonResult.success(this.selectService.getProBigClass(), "提示：获取数据成功！");
    }

    @PostMapping({"getProMidClass"})
    @ApiOperation(value = "商品中类编码" ,response = SelectData.class)
    public JsonResult getProMidClass() {
        return JsonResult.success(this.selectService.getProMidClass(), "提示：获取数据成功！");
    }


    @ApiOperation(value = "saleD商品查询 禁用了")
    @PostMapping("/selectProBySaleD")
    public JsonResult selectProBySaleD(HttpServletRequest request, @Valid @RequestBody Map<String,Object> inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.put("clientId",userInfo.getClient());
        return JsonResult.success(selectService.selectProBySaleD(inData),"返回成功");
    }

    @ApiOperation(value = "会员卡列表查询")
    @PostMapping({"getMemberByClientOrBrId"})
    public JsonResult selectMemeberCard(HttpServletRequest request,String brid) {
        GetLoginOutData userInfo = super.getLoginUser(request);
//        inData.put("clientId",userInfo.getClient());
        return JsonResult.success(selectService.getMemberByClientOrBrId(userInfo.getClient(),brid), "提示：获取数据成功！");
    }

    @ApiOperation(value = "医生,收营员,营业员列表查询")
    @PostMapping({"selectStaff"})
    public JsonResult selectStaff(HttpServletRequest request,String brid) {
        GetLoginOutData userInfo = super.getLoginUser(request);
//        inData.put("clientId",userInfo.getClient());
        return JsonResult.success(selectService.selectStaff(userInfo.getClient(),brid), "提示：获取数据成功！");
    }

    @ApiOperation(value = "收银员列表查询")
    @PostMapping({"selectCashier"})
    public JsonResult selectCashier(HttpServletRequest request,String brId) {
        GetLoginOutData userInfo = super.getLoginUser(request);
//        inData.put("clientId",userInfo.getClient());
        return JsonResult.success(selectService.selectCashier(userInfo.getClient(),brId), "提示：获取数据成功！");
    }

    @ApiOperation(value = "营业员列表查询")
    @PostMapping({"selectAssistant"})
    public JsonResult selectAssistant(HttpServletRequest request,String brId) {
        GetLoginOutData userInfo = super.getLoginUser(request);
//        inData.put("clientId",userInfo.getClient());
        return JsonResult.success(selectService.selectAssistant(userInfo.getClient(),brId), "提示：获取数据成功！");
    }

    @ApiOperation(value = "医生列表查询")
    @PostMapping({"selectDoctor"})
    public JsonResult selectDoctor(HttpServletRequest request,String brId) {
        GetLoginOutData userInfo = super.getLoginUser(request);
//        inData.put("clientId",userInfo.getClient());
        return JsonResult.success(selectService.selectDoctor(userInfo.getClient(),brId), "提示：获取数据成功！");
    }

}
