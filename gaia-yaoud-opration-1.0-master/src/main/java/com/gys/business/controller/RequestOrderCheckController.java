package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.util.StringUtil;
import com.gys.business.mapper.GaiaUserDataMapper;
import com.gys.business.service.RequestOrderCheckService;
import com.gys.business.service.data.GaiaClSystemPara;
import com.gys.business.service.data.check.*;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.constant.CommonConstant;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.enums.ClientParamsEnum;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

/***
 * @desc: 请货单审核模块(采芝灵)
 * @author: ryan
 * @createTime: 2021/7/10 13:59
 **/
@Slf4j
@Api(tags = "请货单审核模块(采芝灵)")
@RestController
@RequestMapping("/requestOrder/checkOrder")
public class RequestOrderCheckController extends BaseController {
    @Resource
    private RequestOrderCheckService requestOrderCheckService;
    @Resource
    private GaiaUserDataMapper gaiaUserDataMapper;

    @ApiOperation(value = "委托配送方列表", response = SupplierVO.class)
    @PostMapping("supplierList")
    public JsonResult<List<SupplierVO>> supplierList(HttpServletRequest request) {
        GetLoginOutData loginUser = getLoginUser(request);
        List<SupplierVO> supplierList = requestOrderCheckService.supplierList(loginUser.getClient());
        return JsonResult.success(supplierList, "查询成功");
    }

    @ApiOperation(value = "用户列表", response = UserVO.class)
    @PostMapping("userList")
    public JsonResult<List<UserVO>> userList(HttpServletRequest request) {
        GetLoginOutData loginUser = getLoginUser(request);
        List<UserVO> list = gaiaUserDataMapper.userList(loginUser.getClient());
        return JsonResult.success(list, "查询成功");
    }

    @ApiOperation(value = "商品列表", response = RequestOrderDetailVO.class)
    @PostMapping(value = "productList")
    public JsonResult<List<RequestOrderDetailVO>> productList(HttpServletRequest request, @RequestBody ProductForm productForm) {
        GetLoginOutData loginUser = getLoginUser(request);
        productForm.setClient(loginUser.getClient());
        List<RequestOrderDetailVO> list = requestOrderCheckService.getStoreProList(productForm);
        return JsonResult.success(list, "查询成功");
    }

    @ApiOperation(value = "订单列表",response = RequestOrderVO.class)
    @PostMapping("orderList")
    public JsonResult<List<RequestOrderVO>> orderList(HttpServletRequest request, @RequestBody OrderCheckForm orderCheckForm) {
        log.info("<请货审核><订单列表><请求参数：{}>", JSON.toJSONString(orderCheckForm));
        if (StringUtil.isEmpty(orderCheckForm.getEndDate()) || StringUtil.isEmpty(orderCheckForm.getStartDate())) {
            return JsonResult.error(1001,"请货日期不能为空");
        }
        GetLoginOutData loginUser = getLoginUser(request);
        orderCheckForm.setClient(loginUser.getClient());
        List<RequestOrderVO> list = requestOrderCheckService.orderList(orderCheckForm);
        return JsonResult.success(list, "查询成功");
    }

    @PostMapping("callList")
    public JsonResult<List<RequestOrderVO>> callList(HttpServletRequest request, @RequestBody CallOrderForm callOrderForm) {
        log.info("<请货审核><请货列表><请求参数：{}>", JSON.toJSONString(callOrderForm));
        GetLoginOutData loginUser = getLoginUser(request);
        callOrderForm.setClient(loginUser.getClient());
        List<RequestOrderVO> list = requestOrderCheckService.callList(callOrderForm);
        return JsonResult.success(list, "查询成功");
    }

    @FrequencyValid
    @ApiOperation(value = "关闭订单")
    @PostMapping(value = "closeOrder")
    public JsonResult closeOrder(HttpServletRequest request, @RequestBody CloseOrderForm closeOrderForm) {
        log.info("<请货审核><关闭订单><请求参数：{}>", JSON.toJSONString(closeOrderForm));
        GetLoginOutData loginUser = getLoginUser(request);
        if (StringUtil.isEmpty(closeOrderForm.getStoreId()) || StringUtil.isEmpty(closeOrderForm.getReplenishCode())) {
            return JsonResult.error(1001,"补货单号和门店编码均不可为空");
        }
        closeOrderForm.setClient(loginUser.getClient());
        closeOrderForm.setUserId(loginUser.getUserId());
        requestOrderCheckService.closeOrder(closeOrderForm);
        return JsonResult.success(null, "操作成功");
    }

    @ApiOperation(value = "订单明细", response = RequestOrderDetailListVO.class)
    @PostMapping("detail")
    public JsonResult<RequestOrderDetailListVO> detail(HttpServletRequest request, @RequestBody OrderDetailForm orderDetailForm) {
        log.info("<请货审核><请货明细><请求参数：{}>", JSON.toJSONString(orderDetailForm));
        if (StringUtil.isEmpty(orderDetailForm.getReplenishCode())||StringUtil.isEmpty(orderDetailForm.getStoreId())) {
            return JsonResult.error(1001,"补货单号和门店编码不能为空");
        }
        GetLoginOutData loginUser = getLoginUser(request);
        orderDetailForm.setClient(loginUser.getClient());
        return JsonResult.success(requestOrderCheckService.getOrderDetail(orderDetailForm), "查询成功");
    }

    @FrequencyValid
    @ApiOperation(value = "保存请货明细")
    @PostMapping("save")
    public JsonResult save(HttpServletRequest request, @RequestBody OrderOperateForm operateForm) {
        log.info("<请货审核><保存请货><请求参数：{}>", JSON.toJSONString(operateForm));
        try {
            GetLoginOutData loginUser = getLoginUser(request);
            operateForm.setClient(loginUser.getClient());
            operateForm.setUserId(loginUser.getUserId());
            validSaveParams(operateForm);
            requestOrderCheckService.saveOrder(operateForm);
        } catch (Exception e) {
            if (e instanceof BusinessException) {
                return JsonResult.error(1001, e.getMessage());
            } else {
                return JsonResult.error("系统繁忙，请稍后再试");
            }
        }
        return JsonResult.success(null, "操作成功");
    }

    @FrequencyValid
    @ApiOperation(value = "审核请货单")
    @PostMapping("audit")
    public JsonResult audit(HttpServletRequest request, @RequestBody OrderOperateForm orderOperateForm) {
        log.info("<请货审核><审核请货><请求参数：{}>", JSON.toJSONString(orderOperateForm));
        try {
            GetLoginOutData loginUser = getLoginUser(request);
            orderOperateForm.setClient(loginUser.getClient());
            validSaveParams(orderOperateForm);
            if (StringUtil.isEmpty(orderOperateForm.getUserId())) {
                orderOperateForm.setUserId(loginUser.getUserId());
            }
            requestOrderCheckService.auditOrder(orderOperateForm);
        } catch (Exception e) {
            if (e instanceof BusinessException) {
                return JsonResult.error(1001, e.getMessage());
            }
            return JsonResult.error("系统繁忙，请稍后再试");
        }
        return JsonResult.success(null, "操作成功");
    }

    @FrequencyValid
    @ApiOperation(value = "拒绝请货明细")
    @PostMapping(value = "reject")
    public JsonResult reject(HttpServletRequest request, @RequestBody CloseOrderForm closeOrderForm) {
        log.info("<请货审核><拒绝请货><请求参数：{}>", JSON.toJSONString(closeOrderForm));
        if (StringUtil.isEmpty(closeOrderForm.getReplenishCode()) || StringUtil.isEmpty(closeOrderForm.getStoreId())) {
            return JsonResult.error(1001,"门店编码和补货单号均不能为空");
        }
        GetLoginOutData loginUser = getLoginUser(request);
        closeOrderForm.setUserId(loginUser.getUserId());
        closeOrderForm.setClient(loginUser.getClient());
        requestOrderCheckService.rejectOrder(closeOrderForm);
        return JsonResult.success(null, "操作成功");
    }

    @FrequencyValid
    @ApiOperation(value = "删除请货明细")
    @PostMapping(value = "delete")
    public JsonResult delete(HttpServletRequest request, @RequestBody DeleteOrderForm deleteOrderForm) {
        log.info("<请货审核><删除请货><请求参数：{}>", JSON.toJSONString(deleteOrderForm));
        if (StringUtil.isEmpty(deleteOrderForm.getReplenishCode()) || StringUtil.isEmpty(deleteOrderForm.getStoreId())) {
            return JsonResult.error(1001,"门店编码和补货单号均不能为空");
        }
        if (deleteOrderForm.getDetailList() == null || deleteOrderForm.getDetailList().size() == 0) {
            return JsonResult.error(1001, "请选择要删除的明细数据");
        }
        try {
            GetLoginOutData loginUser = getLoginUser(request);
            deleteOrderForm.setUserId(loginUser.getUserId());
            deleteOrderForm.setClient(loginUser.getClient());
            requestOrderCheckService.deleteOrder(deleteOrderForm);
        } catch (Exception e) {
            if (e instanceof BusinessException) {
                return JsonResult.error(1001, e.getMessage());
            }
            return JsonResult.error("系统繁忙，请稍后再试");
        }
        return JsonResult.success(null, "操作成功");
    }

    private void validSaveParams(OrderOperateForm operateForm) {
        if (StringUtil.isEmpty(operateForm.getReplenishCode()) || StringUtil.isEmpty(operateForm.getStoreId())) {
            throw new BusinessException("补货单号和门店编码不能为空");
        }
        if (operateForm.getDetailList() == null || operateForm.getDetailList().size() == 0) {
            throw new BusinessException("补货明细不能为空");
        }
        for (OrderOperateForm.ReplenishDetail replenishDetail : operateForm.getDetailList()) {
            if (StringUtil.isEmpty(replenishDetail.getProSelfCode())) {
                throw new BusinessException("补货明细[商品编码]不能为空");
            }
            if (BigDecimal.ZERO.compareTo(replenishDetail.getRequestQuantity()) >= 0) {
                throw new BusinessException(String.format("补货明细[%s][补货数量]不能为0", replenishDetail.getProSelfCode()));
            }
            boolean flag = requestOrderCheckService.validPrice(operateForm.getClient());
            if (flag && BigDecimal.ZERO.compareTo(replenishDetail.getSendPrice()) >= 0) {
                throw new BusinessException(String.format("补货明细[%s][配送价格]不能为0", replenishDetail.getProSelfCode()));
            }
            if (replenishDetail.getSendPrice() == null || BigDecimal.ZERO.compareTo(replenishDetail.getSendPrice()) == 0) {
                replenishDetail.setSendPrice(BigDecimal.ZERO);
            }
        }
    }
}
