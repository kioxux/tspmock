package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaFiArBalance;
import com.gys.business.service.GaiaFiArBalanceService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 应收期末余额(GaiaFiArBalance)表控制层
 *
 * @author makejava
 * @since 2021-09-18 15:29:15
 */
@RestController
@RequestMapping("gaiaFiArBalance")
public class GaiaFiArBalanceController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaFiArBalanceService gaiaFiArBalanceService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public GaiaFiArBalance selectOne(Long id) {
        return this.gaiaFiArBalanceService.queryById(id);
    }

}
