package com.gys.business.controller;

import com.gys.business.service.AppNoMoveProductService;
import com.gys.business.service.data.AppNoMoveInData;
import com.gys.business.service.data.NoMoveProInData;
import com.gys.business.service.data.ProductBigTypeSaleInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author SunJiaNan
 * @version 1.0
 * @date 2021/9/15 13:38
 * @description app不动销商品报表
 */
@Slf4j
@Api(tags = "app不动销商品报表")
@RestController
@RequestMapping("/app/noMoveProduct")
public class AppNoMoveProductController extends BaseController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private AppNoMoveProductService appNoMoveProductService;


    @ApiOperation(value = "不动销商品大类列表+饼图")
    @PostMapping("/getBigTypeList")
    public JsonResult getBigTypeList(@RequestBody AppNoMoveInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return appNoMoveProductService.getBigTypeList(inData);
    }

    @ApiOperation(value = "不动销商品中类列表")
    @PostMapping("/getMidTypeList")
    public JsonResult getMidTypeList(@RequestBody AppNoMoveInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return appNoMoveProductService.getMidTypeList(inData);
    }

    @ApiOperation(value = "不动销商品-门店明细")
    @PostMapping("/getStoreList")
    public JsonResult getStoreList(@RequestBody NoMoveProInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return appNoMoveProductService.getStoreList(inData);
    }


}
