//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.JingdongService;
import com.gys.business.service.data.GetJingdongInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JingdongResult;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/jingdong/"})
public class JingdongController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(JingdongController.class);
    @Resource
    private JingdongService jingdongService;

    public JingdongController() {
    }

    @PostMapping({"authToken"})
    public JingdongResult authToken(HttpServletRequest request) {
        String token = request.getParameter("token");
        log.info(token);
        return JingdongResult.success("", "success");
    }

    @PostMapping({"djsw/newOrder"})
    public JingdongResult orderPayedPush(@RequestBody String params) {
        this.jingdongService.orderPayedPush(params);
        return JingdongResult.success("", "success");
    }

    @PostMapping({"djsw/applyCancelOrder"})
    public JingdongResult orderCancelPush(@RequestBody String params) {
        this.jingdongService.orderCancelPush(params);
        return JingdongResult.success("", "success");
    }

    @PostMapping({"djsw/newApplyAfterSaleBill"})
    public JingdongResult orderRefundAllPush(@RequestBody String params) {
        this.jingdongService.orderRefundAllPush(params);
        return JingdongResult.success("", "success");
    }

    @PostMapping({"djsw/updateApplyAfterSaleBill"})
    public JingdongResult orderRefundPush(@RequestBody String params) {
        this.jingdongService.orderRefundPush(params);
        return JingdongResult.success("", "success");
    }

    @PostMapping({"djsw/pushDeliveryStatus"})
    public JingdongResult deliveryStatusPush(@RequestBody String params) {
        this.jingdongService.deliveryStatusPush(params);
        return JingdongResult.success("", "success");
    }

    @PostMapping({"djsw/orderAdjust"})
    public JingdongResult orderStatusPush(@RequestBody String params) {
        this.jingdongService.orderStatusPush(params);
        return JingdongResult.success("", "success");
    }

    @PostMapping({"order/getOrderDetail"})
    public JsonResult orderGetOrderDetail(@RequestBody GetJingdongInData inData) {
        this.jingdongService.orderGetOrderDetail(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/confirm"})
    public JsonResult orderConfirm(HttpServletRequest request, @RequestBody GetJingdongInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.jingdongService.orderConfirm(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/cancel"})
    public JsonResult orderCancel(HttpServletRequest request, @RequestBody GetJingdongInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.jingdongService.orderCancel(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/preparationMealComplete"})
    public JsonResult orderPreparationMealComplete(HttpServletRequest request, @RequestBody GetJingdongInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.jingdongService.orderPreparationMealComplete(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/refund/agree"})
    public JsonResult orderRefundAgree(HttpServletRequest request, @RequestBody GetJingdongInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.jingdongService.orderRefundAgree(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/refund/reject"})
    public JsonResult orderRefundReject(HttpServletRequest request, @RequestBody GetJingdongInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.jingdongService.orderRefundReject(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"medicine/stock"})
    public JsonResult medicineStock(HttpServletRequest request, @RequestBody GetJingdongInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setStoreCode(userInfo.getDepId());
        this.jingdongService.medicineStock(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }
}
