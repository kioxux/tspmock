package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSynchronousAuditStock;
import com.gys.business.service.GaiaSynchronousAuditStockService;
import com.gys.business.service.data.GaiaSynchronousAuditStockOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 库存审计(GaiaSynchronousAuditStock)表控制层
 *
 * @author XiaoZY
 * @since 2021-07-01 13:42:44
 */
@RestController
@RequestMapping("gaiaSynchronousAuditStock")
public class GaiaSynchronousAuditStockController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaSynchronousAuditStockService gaiaSynchronousAuditStockService;


    @PostMapping({"test"})
    public JsonResult test(HttpServletRequest request, @RequestBody GaiaSynchronousAuditStock inData) {
        return JsonResult.success(this.gaiaSynchronousAuditStockService.insertStockInfo(inData),"提示：获取数据成功！");
    }

    @PostMapping({"listAuditInfo"})
    public JsonResult test(HttpServletRequest request, @RequestBody GaiaSynchronousAuditStockOutData inData) {
        return JsonResult.success(this.gaiaSynchronousAuditStockService.listAuditInfo(inData),"提示：获取数据成功！");
    }

    /**
     * 审计查询
     * @param request
     * @param map
     * @return
     */
    @PostMapping({"auditQuery"})
    public JsonResult auditQuery(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        GaiaSynchronousAuditStock auditStock = this.gaiaSynchronousAuditStockService.auditQuery(map,userInfo);
        return JsonResult.success(auditStock,"success");
    }

}
