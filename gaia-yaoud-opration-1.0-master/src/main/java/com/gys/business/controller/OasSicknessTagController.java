package com.gys.business.controller;

import com.gys.business.service.IOasSicknessTagService;
import com.gys.business.service.request.*;
import com.gys.business.service.response.SelectLargeOasSicknessPageInfoResponse;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 疾病大类表 前端控制器
 * </p>
 *
 * @author wangyifan
 * @since 2021-09-01
 */
@RestController
@Api(tags = "疾病大类表接口")
@RequestMapping("/oasSicknessTag")
public class OasSicknessTagController extends BaseController {


    @Autowired
    private IOasSicknessTagService oasSicknessTagService;


    @GetMapping("/selectOasSicknessPageInfo")
    @ApiOperation(value = "疾病大类中类成分列表")
    public JsonResult selectOasSicknessPageInfo(HttpServletRequest request, SelectOasSicknessPageInfoRequest pageInfoRequest) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        pageInfoRequest.setClient(userInfo.getClient());
        return JsonResult.success(oasSicknessTagService.selectOasSicknessPageInfo(pageInfoRequest), "提示：获取数据成功！");
    }

    @PostMapping("/saveOasSickness")
    @ApiOperation(value = "疾病大类中类成分添加数据")
    public JsonResult saveOasSickness(HttpServletRequest servletRequest, @RequestBody SaveOasSicknessRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(servletRequest);
        request.setUserId(userInfo.getUserId());
        request.setUserName(userInfo.getLoginName());
        oasSicknessTagService.saveOasSickness(request);
        return JsonResult.success(null, "新增成功");
    }

    @PostMapping("/import")
    @ApiOperation(value = "导入疾病分类Excel数据")
    public JsonResult inputExcel(HttpServletRequest servletRequest,MultipartFile file) {
        GetLoginOutData userInfo = this.getLoginUser(servletRequest);
        oasSicknessTagService.inputExcel(file, userInfo.getUserId(), userInfo.getLoginName());
        return JsonResult.success(null, "导入成功");
    }

    @PostMapping("/selectPageInfo")
    @ApiOperation(value = "获取疾病分类列表")
    public JsonResult selectPageInfo(HttpServletRequest servletRequest,@RequestBody SelectPageInfoRequest request) {
        this.getLoginUser(servletRequest);
        return JsonResult.success(oasSicknessTagService.selectPageInfo(request), "获取数据成功");
    }

    @GetMapping("/export")
    @ApiOperation(value = "导出分类")
    public void export(HttpServletResponse response, HttpServletRequest servletRequest, SelectPageInfoRequest request) throws IOException {
        GetLoginOutData userInfo = this.getLoginUser(servletRequest);
        if (Objects.isNull(userInfo)) {
            throw new BusinessException("请重新登陆");
        }
        SXSSFWorkbook workbook = oasSicknessTagService.export(request);
        String filename = "疾病分类.xlsx";
        OutputStream output = response.getOutputStream();
        response.reset();
        String codedFileName = java.net.URLEncoder.encode(filename, "UTF-8");
        response.setHeader("Content-disposition", "attachment; filename=" + new String(codedFileName.getBytes(), "ISO8859-1"));
        response.setContentType("application/vnd.ms-excel");
        workbook.write(output);
        workbook.close();
        output.flush();
        output.close();
    }

    @GetMapping("/selectDataEcho")
    @ApiOperation(value = "疾病分类数据回显")
    public JsonResult selectDataEcho(HttpServletRequest servletRequest,SelectDataEchoRequest request) {
        this.getLoginUser(servletRequest);
        return JsonResult.success(oasSicknessTagService.selectDataEcho(request), "获取数据成功");
    }

    @PutMapping("/updateStatus/{id}")
    @ApiOperation(value = "启用/禁用")
    public JsonResult updateStatus(HttpServletRequest servletRequest, @PathVariable(value = "id") String id, @RequestParam(value = "status") String status) {
        this.getLoginUser(servletRequest);
        oasSicknessTagService.updateStatus(id, status);
        return JsonResult.success(null, "修改成功");
    }

    @PutMapping("/updateOasSickness")
    @ApiOperation(value = "修改分类")
    public JsonResult updateOasSickness(HttpServletRequest servletRequest, @RequestBody UpdateOasSicknessRequest request) {
        GetLoginOutData loginUser = this.getLoginUser(servletRequest);
        request.setUserId(loginUser.getUserId());
        request.setUserName(loginUser.getLoginName());
        oasSicknessTagService.updateOasSickness(request);
        return JsonResult.success(null, "修改成功");
    }
}