package com.gys.business.controller;

import com.gys.business.service.DailySettlementService;
import com.gys.business.service.data.DailySettlement.DailyReconcileDetailOutData;
import com.gys.business.service.data.DailySettlement.DailyReconcileDetailVO;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * FX 日结对账
 */
@RestController
@RequestMapping({"/dailySettlement/"})
public class DailySettlementToFX extends BaseController {

    @Autowired
    private DailySettlementService servcie;

    /**
     * 日结对账查询
     * @param request
     * @param map
     * @return
     */
    @PostMapping({"generateDetails"})
    public JsonResult getSalesInquireList(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.servcie.getSalesInquireList(userInfo,map),"success");
    }


    /**
     * 日结对账提交
     * @param request
     * @param detailVO
     * @return
     */
    @PostMapping({"reconciliationReview"})
    @FrequencyValid
    public JsonResult reconciliationReview(HttpServletRequest request, @RequestBody DailyReconcileDetailVO detailVO) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.servcie.reconciliationReview(userInfo,detailVO);
        return JsonResult.success("","success");
    }

}
