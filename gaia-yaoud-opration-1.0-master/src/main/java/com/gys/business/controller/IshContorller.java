package com.gys.business.controller;

import com.gys.business.service.IshService;
import com.gys.business.service.data.InvoicingOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Description TODO
 * @Author huxinxin
 * @Date 2021/3/28 14:21
 * @Version 1.0.0
 **/
@RestController
@RequestMapping({"/ish/"})
@Slf4j
@Api(tags = "东软")
public class IshContorller extends BaseController {
    @Autowired
    private IshService ishService;

    @PostMapping({"/insertIsh"})
    public JsonResult insertIsh(HttpServletRequest request, @RequestBody Map record) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        record.put("client",userInfo.getClient());
        record.put("ishBrId",userInfo.getDepId());
        this.ishService.insertIsh(record);
        return JsonResult.success("", "提示：获取数据成功！");
    }
    @ApiOperation(value = "东软退货", response = InvoicingOutData.class)
    @PostMapping({"/ishReturn"})
    public JsonResult ishReturn(HttpServletRequest request,@RequestBody Map record) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.ishService.ishReturn(userInfo,record);
        return JsonResult.success("", "提示：获取数据成功！");
    }

//    @ApiOperation(value = "保存处方明细", response = InvoicingOutData.class)
//    @PostMapping({"/insertIshFeeDetail"})
//    public JsonResult insertIsh(HttpServletRequest request, @RequestBody GaiaSdIshFeeDetail record) {
//        this.ishService.insertIshFeeDetail(record);
//        return JsonResult.success("", "提示：获取数据成功！");
//    }
}
