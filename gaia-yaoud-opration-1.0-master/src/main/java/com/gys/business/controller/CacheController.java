package com.gys.business.controller;

import com.gys.business.mapper.entity.*;
import com.gys.business.service.CacheService;
import com.gys.business.service.data.HealthCareQueryDto;
import com.gys.business.service.data.PartialSynVO;
import com.gys.business.service.data.StartDayAndEndDayInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 缓存数据
 *
 * @author xiaoyuan on 2021/1/7
 */
@RequestMapping({"/cacheFX/"})
@Controller
public class CacheController extends BaseController {


    @Autowired
    private CacheService cacheService;

    /**
     * 获取所有数据-文件传输方式
     *
     * @param request
     * @return
     */
    @PostMapping({"cacheFXListFile"})
    public void cacheFXList(HttpServletRequest request, @RequestBody boolean flag, HttpServletResponse response) {
        response.setContentType("application/x-msdownload;");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Disposition", "attachment; fileName=file.zip");
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.cacheService.getAllCache(userInfo, flag, response);
    }


    /**
     * 获取所有表名和 加盟商名称
     *
     * @return
     */
    @PostMapping({"allTableNames"})
    @ResponseBody
    public JsonResult allTableNames() {
        return JsonResult.success(this.cacheService.getAllTableNames(), "success");
    }

    /**
     * 获取加盟商下所有店号
     *
     * @return
     */
    @PostMapping({"getAllBrIds"})
    @ResponseBody
    public JsonResult getAllBrIds(@RequestBody Map<String, Object> map) {
        return JsonResult.success(this.cacheService.getAllBrIds(map), "success");
    }


    /**
     * 前端传表名
     *
     * @param request
     * @return
     */
    @PostMapping({"realTimeFullAmount"})
    @ResponseBody
    public JsonResult realTimeFullAmount(HttpServletRequest request, @Valid @RequestBody PartialSynVO synVO) {
        this.cacheService.realTimeFullAmount(this.getLoginUser(request), synVO);
        return JsonResult.success("", "success");
    }

    /**
     * 全量表更新
     *
     * @param request
     * @return
     */
    @PostMapping({"synchronousData"})
    public void synchronousData(HttpServletRequest request, @RequestBody Map<String, String> map, HttpServletResponse response) {
        response.setContentType("application/x-msdownload;");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Disposition", "attachment; fileName=file.zip");
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.cacheService.synchronousData(userInfo, map, response);
    }

    /**
     * 获取需要更新总数
     *
     * @param request
     * @return
     */
    @PostMapping({"getTheDataYouNeed"})
    @ResponseBody
    public JsonResult getTheDataYouNeed(HttpServletRequest request, @RequestBody GaiaDataSynchronized aSynchronized) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        aSynchronized.setClient(userInfo.getClient());
        aSynchronized.setSite(userInfo.getDepId());
        return JsonResult.success(this.cacheService.getTheDataYouNeed(aSynchronized), "success");
    }


    /**
     * 获取更新内容
     * 同步更新 实时 数据
     *
     * @param aSynchronized
     * @return
     */
    @PostMapping({"/getUpdateContent"})
    @ResponseBody
    public JsonResult getUpdateContent(HttpServletRequest request, @RequestBody GaiaDataSynchronized aSynchronized) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        aSynchronized.setClient(userInfo.getClient());
        aSynchronized.setSite(userInfo.getDepId());
        return JsonResult.success(this.cacheService.updateContentAll(aSynchronized), "success");
    }


    /**
     * 更新门店 和 用户
     *
     * @param request
     * @return
     */
    @PostMapping({"/partialUpdate"})
    @ResponseBody
    public JsonResult partialUpdate(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.cacheService.partialUpdate(userInfo), "success");
    }


    /**
     * 数据审计提交
     *
     * @param request
     * @return
     */
    @PostMapping({"/dataAudit"})
    @ResponseBody
    public JsonResult dataAudit(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client", userInfo.getClient());
        map.put("brId", userInfo.getDepId());
        map.put("userId", userInfo.getUserId());
        this.cacheService.dataAuditSave(map);
        return JsonResult.success("", "success");
    }

    /**
     * 提交门店 电脑信息
     *
     * @param request
     * @return
     */
    @PostMapping({"/insertInformation"})
    @ResponseBody
    public JsonResult insertInformation(HttpServletRequest request, @RequestBody GaiaComputerInformation information) {
        this.cacheService.insertInformation(information);
        return JsonResult.success("", "success");
    }

    /**
     * 获取当前门店的最大时间
     *
     * @param request
     * @return
     */
    @PostMapping({"/getAuditMaxDate"})
    @ResponseBody
    public JsonResult getAuditMaxDate(HttpServletRequest request,@RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.cacheService.getAuditMaxDate(userInfo,map), "success");
    }

    /**
     * 数据审计
     */
    @PostMapping("/audit")
    @ResponseBody
    @ApiOperation(value = "数据审计查询", response = GaiaSynchronousAudit.class)
    public JsonResult audit(HttpServletRequest request, @Valid @RequestBody StartDayAndEndDayInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return cacheService.selectAuditByDay(inData);
    }


    /**
     * 获取当前会员的 电子券 和 储值卡
     */
    @PostMapping("/otherInformation")
    @ResponseBody
    @ApiOperation(value = "获取电子券/储值卡信息", response = GaiaSynchronousAudit.class)
    public JsonResult otherInformation(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client", userInfo.getClient());
        map.put("gsmbcBrId", userInfo.getDepId());
        return JsonResult.success(this.cacheService.otherInformation(map), "success");
    }

    /**
     * 获取当前门店的参数配置
     */
    @PostMapping("/getSystemParaByStore")
    @ResponseBody
    @ApiOperation(value = "获取当前门店的参数配置", response = GaiaSdSystemPara.class)
    public JsonResult getSystemParaByStore(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client", userInfo.getClient());
        map.put("depId", userInfo.getDepId());
        return JsonResult.success(this.cacheService.getSystemParaByStore(map), "success");
    }

    /**
     * 获取指定门店的参数配置
     */
    @PostMapping("/getSystemParaByStore2")
    @ResponseBody
    @ApiOperation(value = "获取当前门店的参数配置", response = GaiaSdSystemPara.class)
    public JsonResult getSystemParaByStore2(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client", userInfo.getClient());
        return JsonResult.success(this.cacheService.getSystemParaByStore(map), "success");
    }

    /**
     * 医保查询
     */
    @PostMapping("/healthCareQuery")
    @ResponseBody
    public JsonResult healthCareQuery(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client", userInfo.getClient());
        map.put("depId", userInfo.getDepId());
        PageInfo<List<HealthCareQueryDto>> dtos = this.cacheService.healthCareQuery(map);
        return JsonResult.success(dtos, "success");
    }

    /**
     * 韦德-医保冲正记录新增
     */
    @PostMapping("/insertRightingRecord")
    @ResponseBody
    @ApiOperation(value = "韦德-医保冲正记录新增")
    public JsonResult insertRightingRecord(HttpServletRequest request, @RequestBody GaiaSdRightingRecord inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return cacheService.insertRightingRecord(inData);
    }
}
