package com.gys.business.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.gys.business.heartBeatWarn.service.HeartBeatService;
import com.gys.business.service.ApiService;
import com.gys.business.service.HebBillData;
import com.gys.business.service.HebSaleBillService;
import com.gys.business.service.PayService;
import com.gys.business.service.data.CompadmDcOutData;
import com.gys.business.service.data.MaterialDocRequestDto;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Api(tags = "api相关", description = "/api/v1")
@RestController
@RequestMapping({"/api/v1/"})
public class ApiController {

    @Autowired
    private ApiService apiService;

    @Autowired
    private PayService payService;
    @Autowired
    private HebSaleBillService hebSaleBillService;

    @Autowired
    private HeartBeatService heartBeatService;

    //门店商品库存查询
    @ApiOperation(value = "门店商品库存查询")
    @PostMapping({"storeStock"})
    public Map<String,Object> selectPickProductList(@RequestBody Map<String,Object> inData) {
        return this.apiService.selectStoreStockList(inData);
    }

    /**
     * 委托配送第三方配送单
     * @param inData
     * @return
     */
    @ApiOperation(value = "托配送第三方配送单")
    @PostMapping({"addWTPSD"})
    public JsonResult addWTPSD(@RequestBody Map<String,Object> inData) {
        return this.apiService.addWTPSD(inData);
    }

    /**
     * 紫石第三方商品关系和库存信息
     * @param inData
     * @return
     */
    @ApiOperation(value = "紫石第三方商品关系和库存信息")
    @PostMapping({"addDSFZSStoreInfo"})
    public JsonResult addDSFZSStoreInfo(@RequestBody Map<String,Object> inData) {
        return this.apiService.addDSFZSStoreInfo(inData);
    }


    /**
     * 哈尔滨医保订单上传
     * @param inData
     * @return
     */
    @ApiOperation(value = "销售单上传")
    @PostMapping({"HebSaleBill"})
    public JsonResult sale(@Valid @RequestBody Map<String,Object> inData) {
        Map<String,Object> body = (Map<String,Object>)inData.get("body");
        if(body==null){
            throw new BusinessException("参数data不可为空");
        }
        if(CollUtil.isEmpty((List<HebBillData>)body.get("billList"))) {
            throw new BusinessException("订单不可为空");
        }
        return JsonResult.success(this.apiService.billCommit(inData), "");
    }

    /**
     * 心跳请求
     * @param inData
     * @return
     */
    @ApiOperation(value = "心跳请求")
    @PostMapping({"heartBeat"})
    public JsonResult heartBeat(HttpServletRequest request,@RequestBody Map<String,Object> inData) {

        inData.put("IP", CommonUtil.getIpAddr(request));
        inData.put("Name", "heartBeat");
        inData.put("Comment", "心跳请求");
        inData.put("WatchType", 1);
        return heartBeatService.addServericeWatch(inData);
    }

}
