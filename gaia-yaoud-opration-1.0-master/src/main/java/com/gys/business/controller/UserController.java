package com.gys.business.controller;

import com.gys.business.mapper.GaiaGroupDataMapper;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/user"})
public class UserController extends BaseController {
    @Resource
    private GaiaGroupDataMapper groupDataMapper;

    @PostMapping("/checkBackendAuth")
    public JsonResult checkBackendAuth(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);


        return JsonResult.success(groupDataMapper.selectGroupSite(userInfo.getClient(), userInfo.getUserId()).size() > 1 ? 1 : 0,"success");
    }
}
