package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaFiArPayment;
import com.gys.business.service.GaiaFiArPaymentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 应收方式维护表(GaiaFiArPayment)表控制层
 *
 * @author makejava
 * @since 2021-09-18 15:29:32
 */
@RestController
@RequestMapping("gaiaFiArPayment")
public class GaiaFiArPaymentController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaFiArPaymentService gaiaFiArPaymentService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public GaiaFiArPayment selectOne(Long id) {
        return this.gaiaFiArPaymentService.queryById(id);
    }

}
