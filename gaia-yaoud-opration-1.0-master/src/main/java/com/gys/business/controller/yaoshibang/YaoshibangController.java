package com.gys.business.controller.yaoshibang;

import com.gys.business.mapper.entity.GaiaSoBatchH;
import com.gys.business.service.GAIAYaoshibangStoreService;
import com.gys.business.service.data.yaoshibang.CustomerDto;
import com.gys.business.service.data.yaoshibang.CustormerQueryFrom;
import com.gys.business.service.data.yaoshibang.OrderDetailDto;
import com.gys.business.service.data.yaoshibang.OrderQueryBean;
import com.gys.business.service.yaoshibang.YaoshibangService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * @author zhangdong
 * @date 2021/6/17 10:35
 */
@Controller
@RequestMapping("yaoshibang")
@Api(tags = "药师帮api")
public class YaoshibangController extends BaseController {

    @Autowired
    private YaoshibangService yaoshibangService;

    @Autowired
    private GAIAYaoshibangStoreService gaiaYaoshibangStoreService;

    @PostMapping("orderQuery")
    @ResponseBody
    @ApiOperation(value = "订单查询", response = GaiaSoBatchH.class)
    public JsonResult orderQuery(@RequestBody OrderQueryBean bean, HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClientId(userInfo.getClient());
        PageInfo<GaiaSoBatchH> pageInfo = yaoshibangService.orderQuery(bean);
        return JsonResult.success(pageInfo, "提示：获取数据成功！");
    }

    @GetMapping("orderDetail")
    @ResponseBody
    @ApiOperation(value = "订单明细查询", response = OrderDetailDto.class)
    public JsonResult orderDetail(Long id, HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<OrderDetailDto> list = yaoshibangService.orderDetail(id, userInfo.getClient());
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @GetMapping("orderCreate")
    @ResponseBody
    @ApiOperation(value = "创建批发订单")
    public JsonResult orderCreate(Long id, HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        yaoshibangService.orderCreate(id, userInfo);
        return JsonResult.success(null, "批发订单创建成功");
    }

    @GetMapping("customerQuery")
    @ResponseBody
    @ApiOperation(value = "客户模糊查询", response = CustomerDto.class)
    public JsonResult customerQuery(String name, HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<CustomerDto> list = yaoshibangService.customerQuery(name, userInfo.getClient());
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @GetMapping("compensate")
    @ResponseBody
    @ApiOperation(value = "数据补偿")
    public JsonResult compensate(String customerName, String customerId, String client, HttpServletRequest request) {
        yaoshibangService.compensate(customerName, customerId, client);
        return JsonResult.success(null, "提示：获取数据成功！");
    }

    @PostMapping("customerQueryByName")
    @ResponseBody
    @ApiOperation(value = "根据客户名称查询详情", response = CustomerDto.class)
    public JsonResult customerQueryByName(@RequestBody CustormerQueryFrom queryFrom, HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        queryFrom.setClient(userInfo.getClient());
        HashMap<String,String> map= gaiaYaoshibangStoreService.getYaoshibangStoreByKey(queryFrom);
        return JsonResult.success(map, "提示：获取数据成功！");
    }

    @PostMapping("synchYaoshibangStore")
    @ResponseBody
    @ApiOperation(value = "同步药师帮客户信息全量", response = CustomerDto.class)
    public JsonResult synchYaoshibangStore( HttpServletRequest request) {
        gaiaYaoshibangStoreService.synchYaoshibangStore(1);
        return JsonResult.success("", "提示：同步成功！");
    }

    @PostMapping("priceSync")
    @ResponseBody
    @ApiOperation(value = "同步药师帮价格", response = CustomerDto.class)
    public JsonResult priceSync( HttpServletRequest request) {
        yaoshibangService.priceSync();
        return JsonResult.success("", "提示：同步成功！");
    }

    @PostMapping("stockSync")
    @ResponseBody
    @ApiOperation(value = "同步药师帮库存", response = CustomerDto.class)
    public JsonResult stockSync( HttpServletRequest request) {
        yaoshibangService.stockSync();
        return JsonResult.success("", "提示：同步成功！");
    }

    /**
     * wms 药品出货后，复核完成，调用药师帮修改订单状态接口，改状态为 "2", "拣货完成，待配送"
     * 胡俊琳："先不搞吧，能开户，能转成批发单，功能实现，就可以了。后面的我们先手工弄吧"
     */
//    @PostMapping("verifyFinish")
//    public JsonResult verifyFinish() {
//        return JsonResult.success(null, "提示：获取数据成功！");
//    }

}
