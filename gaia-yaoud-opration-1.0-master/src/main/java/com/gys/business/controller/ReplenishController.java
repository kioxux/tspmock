package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.service.ReplenishService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Api(tags = "补货相关接口")
@RestController
@RequestMapping({"/replenish/"})
@Slf4j
public class ReplenishController extends BaseController {

    @Resource
    private ReplenishService replenishService;


    @ApiOperation(value = "补货查询列表接口",notes = "补货查询列表接口",response = GetReplenishOutData.class)
    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.selectList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "新增补货载入商品列表",notes = "新增补货载入商品列表",response = GetReplenishDetailOutData.class)
    @PostMapping({"detailList"})
    public JsonResult detailList(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.detailList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "修改补货单载入补货商品列表",notes = "修改补货单载入补货商品列表",response = GetReplenishOutData.class)
    @PostMapping({"getDetail"})
    public JsonResult getDetail(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.getDetail(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "补货单新增接口",notes = "补货单新增接口")
    @PostMapping({"insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        this.replenishService.insert(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

//    @PostMapping({"update"})
//    public JsonResult update(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClientId(userInfo.getClient());
//        inData.setStoCode(userInfo.getDepId());
//        this.replenishService.update(inData);
//        return JsonResult.success("", "提示：保存成功！");
//    }

    @ApiOperation(value = "补货单号生成",notes = "补货单号生成")
    @PostMapping({"selectNextVoucherId"})
    public JsonResult selectNextVoucherId(HttpServletRequest request){
        GaiaSdReplenishH inData = new GaiaSdReplenishH();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsrhBrId(userInfo.getDepId());
        return JsonResult.success(this.replenishService.selectNextVoucherId(inData), "提示：返回数据！");
    }

//    @ApiOperation(value = "补货单号生成",notes = "补货单号生成")
//    @PostMapping({"queryNextVoucherId"})
//    public JsonResult queryNextVoucherId(HttpServletRequest request){
//        GaiaSdReplenishH inData = new GaiaSdReplenishH();
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClientId(userInfo.getClient());
//        inData.setGsrhBrId(userInfo.getDepId());
//        return JsonResult.success(this.replenishService.queryNextVoucherId(inData), "提示：返回数据！");
//    }

    @ApiOperation(value = "补货审核",notes = "补货审核")
    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        this.replenishService.approve(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @ApiOperation(value = "库存数量查询",notes = "库存数量查询",response = GetReplenishOutData.class)
    @PostMapping({"getStock"})
    public JsonResult getStock(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.getStock(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "管制药品限制数量查询",notes = "管制药品限制数量查询",response = GetReplenishOutData.class)
    @PostMapping({"getParam"})
    public JsonResult getParam(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.getParam(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "获取90天 30天 7天销售总数量",notes = "获取90天 30天 7天销售总数量",response = GetReplenishOutData.class)
    @PostMapping({"getSales"})
    public JsonResult getSales(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.getSales(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "批次库存查询",notes = "批次库存查询",response = GetReplenishOutData.class)
    @PostMapping({"getCost"})
    public JsonResult getCost(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.getCost(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "获取采购总数量",notes = "获取采购总数量",response = GetReplenishOutData.class)
    @PostMapping({"getCount"})
    public JsonResult getCount(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.getCount(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "判断是否正常补货",notes = "判断是否正常补货")
    @PostMapping({"isRepeat"})
    public JsonResult isRepeat(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.isRepeat(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "获取采购总价",notes = "获取采购总价",response = GetReplenishOutData.class)
    @PostMapping({"getVoucherAmt"})
    public JsonResult getVoucherAmt(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setStoCode(userInfo.getDepId());
        return JsonResult.success(this.replenishService.getVoucherAmt(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "管制特殊药品查询",notes = "管制特殊药品查询",response = SpecialMedicineOutData.class)
    @PostMapping({"specialDrugsInquiry"})
    public JsonResult specialDrugsInquiry(HttpServletRequest request,@Valid @RequestBody SpecialMedicineInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.replenishService.specialDrugsInquiry(inData), "success");
    }

}
