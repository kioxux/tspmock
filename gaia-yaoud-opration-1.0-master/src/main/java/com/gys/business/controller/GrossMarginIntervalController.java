package com.gys.business.controller;

import com.gys.business.service.GrossMarginIntervalService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Zhangchi
 * @since 2021/09/16/9:25
 */
@Api(tags = "毛利区间划分")
@RestController
@RequestMapping({"/interval/"})
public class GrossMarginIntervalController extends BaseController {
    @Resource
    private GrossMarginIntervalService intervalService;
    @Resource
    public CosUtils cosUtils;


    @ApiOperation(value = "客户下拉列表" ,response = CustomerOutData.class)
    @PostMapping({"customerList"})
    public JsonResult customerList() {
        return JsonResult.success(this.intervalService.selectCustomer(), "提示：获取数据成功！");
    }

    @ApiOperation(value = "毛利率区间查询",response = GrossMarginOutData.class)
    @PostMapping({"grossMarginList"})
    public JsonResult grossMarginList(@RequestBody GrossMarginInData inData){
        return JsonResult.success(this.intervalService.selectGrossMarginInterval(inData),"提示：获取数据成功！");
    }

    @ApiOperation(value = "毛利区间划分保存")
    @PostMapping({"saveGrossMargin"})
    public JsonResult saveGrossMargin(HttpServletRequest request, @RequestBody List<GrossMarginOutData> inDataList){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.intervalService.saveGrossMargin(userInfo,inDataList);
        return JsonResult.success("","提示：操作成功！");
    }

    @ApiOperation(value = "毛利区间导出")
    @PostMapping({"exportGrossMarginList"})
    public Result exportGrossMarginList(@RequestBody GrossMarginInData inData) throws IOException {
        Result result = null;
        List<GrossMarginOutDataCSV> outData = this.intervalService.selectGrossMarginIntervalCSV(inData);
        String fileName = "毛利区间导出表";
        if (outData.size() > 0){
            CsvFileInfo fileInfo = CsvClient.getCsvByte(outData,fileName,Collections.singletonList((short) 1));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                assert fileInfo != null;
                bos.write(fileInfo.getFileContent());
                result = cosUtils.uploadFile(bos, fileInfo.getFileName());
            } catch (IOException e){
                e.printStackTrace();
            } finally {
                bos.flush();
                bos.close();
            }
            return result;
        }else{
            throw new BusinessException("提示：没有查询到数据，请修改查询条件！");
        }
    }

    @ApiOperation(value = "前置条件：数据初始化")
    @PostMapping({"insertData"})
    public JsonResult insertData(){
        this.intervalService.insertData();
        return JsonResult.success("","操作成功！");
    }
}