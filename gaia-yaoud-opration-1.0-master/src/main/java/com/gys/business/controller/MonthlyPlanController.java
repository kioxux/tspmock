package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSaletaskHplanNew;
import com.gys.business.mapper.entity.GaiaSaletaskPlanClientStoNew;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.MonthService;
import com.gys.business.service.SalesSummaryService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.base.ExportExcel;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "月度计划")
@RestController
@RequestMapping({"/monthly/"})
public class MonthlyPlanController extends BaseController {

    @Autowired
    private MonthService monthService;

    @Autowired
    private SalesSummaryService salesSummaryService;//门店销售汇总查询 service

    @ApiOperation(value = "月销售任务列表查询" , response = MonthOutData.class)
    @PostMapping({"monthlySalesTaskListQuery"})
    public JsonResult monthlySalesTaskListQuery(HttpServletRequest request, @RequestBody MonthOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        //inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.monthService.selectPage(inData), "提示：获取数据成功！");
    }

    @PostMapping({"insertMonthlySalesPlan"})
    @ApiOperation(value = "新增月销售任务计划" ,response = MonthOutData.class)
    public JsonResult  insertMonthlySalesPlan(HttpServletRequest request,@RequestBody @Valid MonthOutData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success( this.monthService.insertMonthlySalesPlan(userInfo,inData),"提示：操作成功！");
    }


    @PostMapping({"deleteMonthlySalesPlan"})
    @ApiOperation(value = "删除月销售任务计划" )
    public JsonResult  deleteMonthlySalesPlan(HttpServletRequest request,@RequestBody MonthOutData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        //saletask.setClient(userInfo.getClient());
        this.monthService.deleteMonthlySalesPlan(inData);
        return JsonResult.success( "","提示：操作成功！");
    }

    @PostMapping({"PushMonthlySalesPlanUser"})//推送
   @ApiOperation(value = "推送月销售任务计划用户" ,response = MonthOutData.class)
   public JsonResult  PushMonthlySalesPlanUser(HttpServletRequest request,@RequestBody @Valid MonthOutData inData){
       GetLoginOutData userInfo = this.getLoginUser(request);
       //saletask.setClient(userInfo.getClient());
       return JsonResult.success( this.monthService.PushMonthlySalesPlanUser(userInfo,inData),"提示：操作成功！");
   }

   //用户页面 查询

    //查询门店
    @PostMapping({"getStoreBySaleTask"})
    @ApiOperation(value = "查询门店" ,response = SaleTaskStroeOutData.class)
    public JsonResult  getStoreBySaleTask(HttpServletRequest request, @RequestBody GaiaStoreData storeData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        storeData.setClient(userInfo.getClient());;
        return JsonResult.success( this.monthService.getStoreBySaleTask(storeData) ,"提示：获取数据成功！");
    }

    //新建 insertMonthlySalesPlanUser
   @PostMapping({"addSalesTaskSave"}) //addedMonthlySalesTaskUsers
   @ApiOperation(value = "用户新增保存" ,response = MonthOutData.class)
   public JsonResult  addSalesTaskSave(HttpServletRequest request,@RequestBody MonthOutData inData){
       GetLoginOutData userInfo = this.getLoginUser(request);
       //saletask.setClient(userInfo.getClient());
       this.monthService.addedMonthlySalesTaskUsers(userInfo,inData);
       return JsonResult.success( "","提示：操作成功！");
   }

    //保存
    @PostMapping ({"saveUpdateData"})
    @ApiOperation(value = "用户修改保存")
    public JsonResult saveUpdateData(HttpServletRequest request,@RequestBody MonthOutData outData){
        GetLoginOutData user = this.getLoginUser(request);
        outData.setClient(user.getClient());
        this.monthService.saveStoreData(user,outData);
        return JsonResult.success("" ,"提示：操作成功！");
    }
    @PostMapping({"modifyMonthlySalesPlan"})
    @ApiOperation(value = "修改月销售任务计划" ,response = MonthOutData.class)
    public JsonResult  modifyMonthlySalesPlan(HttpServletRequest request,@RequestBody @Valid MonthOutData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        this.monthService.modifyMonthlySalesPlan(userInfo,inData);
        return JsonResult.success( "","提示：操作成功！");
    }


//    @PostMapping({"targetTrial"})
//    @ApiOperation(value = "目标试算" ,response = MonthOutData.class)
//    public JsonResult  targetTrial(HttpServletRequest request, @RequestBody MonthOutData summaryData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        summaryData.setClient(userInfo.getClient());
//        //summaryData.setClient(userInfo.getClient());
//        return JsonResult.success( this.monthService.targetTrial(userInfo,summaryData),"提示：操作成功！");
//    }

    //用户查询
    @PostMapping({"monthlySalesTaskListUser"})
    @ApiOperation(value = "用户查询销售列表" ,response = MonthOutData.class)
    public JsonResult  monthlySalesTaskListUser(HttpServletRequest request, @RequestBody MonthOutData outData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        outData.setClient(userInfo.getClient());
        //summaryData.setClient(userInfo.getClient());
        return JsonResult.success( this.monthService.monthlySalesTaskListUser(outData),"提示：操作成功！");
    }

    //用户管理页面：查看
    @PostMapping({"viewSalesTask"})
    @ApiOperation(value = "用户月销售任务查看",response = GaiaSaletaskHplanNew.class)
    public JsonResult viewSalesTask(HttpServletRequest request,@RequestBody MonthOutData outData){
        GetLoginOutData userinfo = this.getLoginUser(request);
        outData.setClient(userinfo.getClient());
        return JsonResult.success(this.monthService.viewSalesTask(outData),"提示：操作成功！");
    }
    //用户管理：任务设置保存
    @PostMapping({"storeTaskSetting"})
    @ApiOperation(value = "用户月销售任务设置")
    public JsonResult storeTaskSetting(HttpServletRequest request, @RequestBody MonthOutDataVo inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.monthService.settingStore(inData),"提示：操作成功！");
    }

    //审核
    @PostMapping({"aduitDate"})
    @ApiOperation(value = "审核数据",response = AuditData.class)
    public JsonResult  aduitDate(HttpServletRequest request,@RequestBody MonthOutData outData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        AuditData audit = this.monthService.auditDate(userInfo,outData);
        return JsonResult.success(audit,"提示：操作成功！");
    }
    @PostMapping({"aduit"})
    @ApiOperation(value = "审核")
    public JsonResult aduit(HttpServletRequest request,@RequestBody MonthOutData outData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.monthService.audit(userInfo,outData);
        return JsonResult.success("","提示：操作成功！");
    }

    //停用
    @PostMapping({"stop"})
    @ApiOperation(value = "停用")
    public JsonResult stop(HttpServletRequest request,@RequestBody MonthOutData outData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.monthService.stop(userInfo,outData);
        return JsonResult.success("","提示：操作成功！");
    }

    //忽略
    @PostMapping({"lose"})
    @ApiOperation(value = "忽略")
    public JsonResult lose(HttpServletRequest request,@RequestBody MonthOutData outData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.monthService.lose(userInfo,outData);
        return JsonResult.success("","提示：操作成功！");
    }

    //插入行
    @PostMapping({"insertRow"})
    @ApiOperation(value = "插入行",response = MonthOutData.class)
    public JsonResult insertRow(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.monthService.insertRow(userInfo),"提示：操作成功！");
    }

    //删除行
    @PostMapping({"deleteRow"})
    @ApiOperation(value = "删除行")
    public JsonResult deleteRow(HttpServletRequest request,@RequestBody MonthOutData outData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.monthService.deleteRow(userInfo,outData);
        return JsonResult.success("","提示：操作成功！");
    }

    //目标试算
    @PostMapping({"target"})
    @ApiOperation(value = "目标试算",response = MonthOutDataVo.class)
    public JsonResult target(HttpServletRequest request,@RequestBody GaiaSaletaskHplanNew gaiaSaletaskHplanNew){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.monthService.target(userInfo,gaiaSaletaskHplanNew),"提示：操作成功！");
    }

    //删除行
//    @PostMapping({"deleteStoreData"})
//    @ApiOperation(value = "删除行")
//    public JsonResult deleteStoreData(HttpServletRequest request, GaiaSaletaskPlanClientStoNew sto){
//        this.monthService.deleteStoreData(sto);
//        return JsonResult.success("","提示：删除成功！");
//    }

    //导出模板
    @PostMapping ({"exportExcel"})
    @ApiOperation(value = "门店导入模板")
    public void exportExcel(HttpServletResponse response){
        ExportExcel.exportModelFile(response,"门店导入模板","modelFile/STORE.xls");
    }

    //导入数据
    @PostMapping({"getImportExcelDataList"})
    @ApiOperation(value = "导入",response = MonthOutDataVo.class)
    public JsonResult getImportExcelDataList(HttpServletRequest request,@RequestParam("file") MultipartFile file,@RequestParam("monthPlan") String monthPlan){
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<GetStoreInData> list = new ArrayList<>();
        try {
            InputStream in = file.getInputStream();
            //导入已存在的Excel文件，获得只读的工作薄对象
//            XSSFWorkbook wk = new XSSFWorkbook(in);
            HSSFWorkbook wb = new HSSFWorkbook(in);
            //获取第一张Sheet表
            Sheet sheet = wb.getSheetAt(0);
            list = getRowAndCell(sheet,userInfo,monthPlan);
            in.close();
            wb.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<MonthOutDataVo> list1 = this.monthService.exportIn(userInfo,list);
//        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(list1, "提示：操作成功！");
    }

    public List<GetStoreInData> getRowAndCell(Sheet sheet,GetLoginOutData userInfo,String monthPlan) {
        List<GetStoreInData> storeInData = new ArrayList<>();
        String msg = null;
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            for (int i = 1; i <= totalRows; i++) {
                String client = userInfo.getClient();
                Row row = sheet.getRow(i); //取出一行数据放入row
                GetStoreInData inData = new GetStoreInData();
                inData.setClient(client);
                inData.setMonthPlan(monthPlan);
                inData.setBrId(dataFormatter.formatCellValue(row.getCell(0)));
                inData.setMonthDays(Integer.parseInt(dataFormatter.formatCellValue(row.getCell(1))));
                inData.setTurnoverDailyPlan(String.valueOf(row.getCell(2)));
                inData.setGrossProfitDailyPlan(String.valueOf(row.getCell(3)));
                inData.setMCardDailyPlan(String.valueOf(row.getCell(4)));
                inData.setIndex(i+1+"");
                storeInData.add(inData);
            } //行end
        }else{
            throw new BusinessException("EXCEL导入信息为空");
        }
        return storeInData;
    }
}
