package com.gys.business.controller;

import com.gys.business.service.DelegationReturnService;
import com.gys.business.service.data.DelegationReturn.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping({"/delegationReturn/"})
//@Slf4j
@Api(tags = "委托退货",description = "delegationReturn")
public class DelegationReturnController extends BaseController {

    @Autowired
    private DelegationReturnService delegationReturnService;

    @ApiOperation(value = "委托退库列表查询",response = DelegationReturnOutData.class)
    @PostMapping("selectList")
    public JsonResult selectList (@RequestBody DelegationReturnInData data ,HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        data.setGsrdhBrId(userInfo.getDepId());
//        log.info("userInfo",String.valueOf(userInfo));
        List<DelegationReturnOutData>  list =delegationReturnService.selectDelegationList(data);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "委托退库商品查询",response = DelegationProductOutData.class)
    @PostMapping("getDelegationProductList")
    public JsonResult getDelegationProductList(DelegationReturnInData data ,HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        data.setGsrdhBrId(userInfo.getDepId());
        List<DelegationProductOutData> list = delegationReturnService.getDelegationProductList(data);
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "委托退库导出",response = ExcelReturnApprovalData.class)
    @PostMapping("exportExcel")
    public JsonResult exportExcel(HttpServletRequest request,HttpServletResponse response){
//        try {
//            ExcelWriter writer = null;
//            OutputStream outputStream = response.getOutputStream();
//            //添加响应头信息
//            response.setHeader("Content-disposition", "attachment; filename=" + "ExcelReturnApprovalData.xls");
//            response.setContentType("application/msexcel;charset=UTF-8");//设置类型
//            response.setHeader("Pragma", "No-cache");//设置头
//            response.setHeader("Cache-Control", "no-cache");//设置头
//            response.setDateHeader("Expires", 0);//设置日期头
//
//            //实例化 ExcelWriter
//            writer = new ExcelWriter(outputStream, ExcelTypeEnum.XLS, true);
//
//            //实例化表单
//            Sheet sheet = new Sheet(1, 0, ExcelReturnApprovalData.class);
            //获取数据
            List<ExcelReturnApprovalData> list = delegationReturnService.selectByExport();
//
//            //输出
//            writer.write(list, sheet);
//            writer.finish();
//            outputStream.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(list, "提示：获取数据成功！");

    }



    @ApiOperation(value = "委托退库审核校验")
    @PostMapping("check")
    public JsonResult check(@RequestBody List<String> data ,HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        boolean flag=delegationReturnService.check(data,userInfo);
        if(flag){
            return JsonResult.success(null, "提示：获取数据成功！");
        }else {
            return JsonResult.process(1,null,"提示：有同退库单未展示的商品;是否要整单处理");
        }
    }


    @ApiOperation(value = "委托退库审核保存")
    @PostMapping("save")
    public JsonResult save(@RequestBody DelegationReturnInfoData data , HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        data.setClient(userInfo.getClient());
        data.setStoreCode(userInfo.getDepId());
        data.setUserId(userInfo.getUserId());
        delegationReturnService.save(data);
        return JsonResult.success(null, "提示：获取数据成功！");
    }
}
