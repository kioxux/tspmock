package com.gys.business.controller;

import com.gys.business.service.DocumentManagementService;
import com.gys.business.service.data.ReplenishmentRecordInData;
import com.gys.business.service.data.ReplenishmentRecordOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author xiaoyuan
 */
@RestController
@Api(tags = "单据管理")
@RequestMapping({"/documentManagement"})
public class DocumentManagementController extends BaseController {

    @Autowired
    private DocumentManagementService managementService;

    @ApiOperation(value = "补货查询列表接口",response = ReplenishmentRecordOutData.class)
    @PostMapping({"/getReplenishmentRecord"})
    public JsonResult getReplenishmentRecord(HttpServletRequest request, @Valid @RequestBody ReplenishmentRecordInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.managementService.getReplenishmentRecord(inData),"success");
    }

}
