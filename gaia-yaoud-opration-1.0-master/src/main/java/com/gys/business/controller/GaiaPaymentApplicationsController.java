package com.gys.business.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.gys.business.service.IGaiaPaymentApplicationsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;

import com.gys.common.base.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-26
 */
@RestController
@Api(tags = "接口")
@RequestMapping("/paymentApplications")
    public class GaiaPaymentApplicationsController extends BaseController {

// 可在模版中添加相应的controller通用方法，编辑模版在resources/templates/controller.java.vm文件中

        @Autowired
        private IGaiaPaymentApplicationsService paymentApplicationsService;

        @GetMapping("/buildInit")
        @ApiOperation(value = "新增初始化", notes = "新增初始化")
        public JsonResult buildInit(HttpServletRequest request) {
            GetLoginOutData userInfo = this.getLoginUser(request);
            return JsonResult.success(paymentApplicationsService.buildInit(userInfo), "提示：获取数据成功！");
        }

        @PostMapping("/save")
        @ApiOperation(value = "新增", notes = "新增")
        public JsonResult save(HttpServletRequest request, @RequestBody Object inData) {
            GetLoginOutData userInfo = this.getLoginUser(request);
            return JsonResult.success(paymentApplicationsService.build(userInfo,inData), "提示：保存成功！");
        }

        @GetMapping("/editInit/{id}")
        @ApiOperation(value = "编辑初始化", notes = "编辑初始化")
        public JsonResult updateInit(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
            GetLoginOutData userInfo = this.getLoginUser(request);
            return JsonResult.success(paymentApplicationsService.updateInit(userInfo,id),"提示：获取数据成功！");
        }

        @PostMapping("/edit")
        @ApiOperation(value = "修改", notes = "修改")
        public JsonResult update(HttpServletRequest request, @RequestBody Object inData) {
            GetLoginOutData userInfo = this.getLoginUser(request);
            return JsonResult.success(paymentApplicationsService.update(userInfo,inData),"提示：修改成功！");
        }



        @GetMapping("/delete/{id}")
        @ApiOperation(value = "删除", notes = "删除")
        public JsonResult delete(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
            GetLoginOutData userInfo = this.getLoginUser(request);
            return JsonResult.success(paymentApplicationsService.delete(userInfo,id),"提示：删除成功！");
        }


        @PostMapping("/list")
        @ApiOperation(value = "查询（分页）", notes = "查询（分页）")
        public JsonResult listPage(HttpServletRequest request, @RequestBody Object inData) {
            GetLoginOutData userInfo = this.getLoginUser(request);
            return JsonResult.success(paymentApplicationsService.getListPage(userInfo,inData), "提示：查询成功！");
        }

    }

