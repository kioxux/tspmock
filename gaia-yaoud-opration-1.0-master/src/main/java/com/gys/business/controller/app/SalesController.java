package com.gys.business.controller.app;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.appservice.SalesService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 会员
 *
 * @author xiaoyuan on 2021/3/8
 */
@Api(tags = "销售app接口")
@RestController("appSales")
@RequestMapping({"/app/sales/"})
public class SalesController extends BaseController {

    @Autowired
    private SalesService salesService;


    @PostMapping({"queryProduct"})
    @ApiOperation(value = "商品查询", response = GetQueryProductOutData.class)
    public JsonResult queryProduct(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.salesService.queryProduct(inData), "success");
    }

    @PostMapping({"batchQuery"})
    @ApiOperation(value = "批次查询", response = GetPdAndExpOutData.class)
    public JsonResult batchQuery(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.salesService.batchQuery(inData), "success");
    }

    @PostMapping({"amountCalculation"})
    @ApiOperation(value = "金额计算", response = AppAmountCalculationDto.class)
    public JsonResult amountCalculation(HttpServletRequest request, @RequestBody AppAmountVo inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.salesService.amountCalculation(inData, userInfo), "success");
    }

    @PostMapping({"drugRecommendation"})
    @ApiOperation(value = "商品药事推荐", response = GetServerOutData.class)
    public JsonResult drugRecommendation(HttpServletRequest request, @RequestBody GetQueryProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.salesService.drugRecommendation(inData), "success");
    }

    @PostMapping({"amountCalculation2"})
    @ApiOperation(value = "金额计算2", response = AppAmountCalculationDto.class)
    public JsonResult amountCalculation2(HttpServletRequest request, @RequestBody AppAmountVo inData) {
        System.out.println(JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.salesService.amountCalculation2(inData, userInfo), "success");
    }

//    @PostMapping({"subtractProducts"})
//    @ApiOperation(value = "减商品", response = AppAmountCalculationDto.class)
//    public JsonResult subtractProducts(HttpServletRequest request, @RequestBody AppAmountVo inData) {
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.salesService.addProducts(inData, userInfo), "success");
//    }

    // ================================================ FX ===================================================================

    /**
     * 手机订单查询
     * @param request
     * @param inData
     * @return
     */
    @PostMapping({"getAllListByApp"})
    public JsonResult getAllListByApp(HttpServletRequest request, @RequestBody AppParameterVo inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesService.getAllListByApp(inData), "success");
    }

    @PostMapping({"getAppByDetails"})
    public JsonResult getAppByDetails(HttpServletRequest request, @RequestBody AppParameterVo inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesService.getAppByDetails(inData), "success");
    }

    @PostMapping({"updateAppSalesStatus"})
    public JsonResult updateAppSalesStatus(HttpServletRequest request, @RequestBody AppParameterVo inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        this.salesService.updateAppSalesStatus(inData);
        return JsonResult.success("", "success");
    }


    @PostMapping({"getAllListByAppBillNo"})
    public JsonResult getAllListByAppBillNo(HttpServletRequest request, @RequestBody AppParameterVo inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesService.getAllListByAppBillNo(inData), "success");
    }

    @PostMapping({"getAllListByAppBillNoV2"})
    public JsonResult getAllListByAppBillNoV2(HttpServletRequest request, @RequestBody AppParameterVo inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.salesService.getAllListByAppBillNoV2(inData), "success");
    }

}
