package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaAnnualTrainingPlan;
import com.gys.business.service.AnnualTrainingPlanService;
import com.gys.business.service.data.AnnualTrainingPlanInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @desc: 年度培训计划表
 * @author: ZhangChi
 * @createTime: 2021/12/31 15:11
 */
@RestController
@RequestMapping({"/annualTrainingPlan/"})
public class AnnualTrainingPlanController extends BaseController {
    @Resource
    private AnnualTrainingPlanService planService;

    //保存
    @PostMapping({"/saveAnnualTrainingPlan"})
    public JsonResult saveAnnualTrainingPlan(HttpServletRequest request, @RequestBody List<GaiaAnnualTrainingPlan> plan){
        GetLoginOutData userInfo = this.getLoginUser(request);
        String client = userInfo.getClient();
        String user = userInfo.getLoginName()+"-"+userInfo.getUserId();
        this.planService.saveAnnualTrainingPlan(plan,client,user);
        return JsonResult.success("","提示：操作成功！");
    }

    //查询
    @PostMapping({"/getAnnualTrainingPlanList"})
    public JsonResult getPlanList(HttpServletRequest request, @RequestBody AnnualTrainingPlanInData inData){
        GetLoginOutData user = this.getLoginUser(request);
        inData.setClient(user.getClient());
        return JsonResult.success(planService.getAnnualTrainingPlanList(inData),"提示：操作成功！");
    }

    //导出
    @PostMapping({"/exportAnnualTrainingPlanList"})
    public Result exportAnnualTrainingPlanList(HttpServletRequest request, @RequestBody AnnualTrainingPlanInData inData){
        GetLoginOutData user = this.getLoginUser(request);
        inData.setClient(user.getClient());
        return planService.exportAnnualTrainingPlanList(inData);
    }

    //删除
    @PostMapping({"/deleteAnnualTrainingPlan"})
    public JsonResult deleteAnnualTrainingPlan(HttpServletRequest request,@RequestBody List<GaiaAnnualTrainingPlan> plan){
        GetLoginOutData user = this.getLoginUser(request);
        String client = user.getClient();
        this.planService.deleteAnnualTrainingPlan(plan,client);
        return JsonResult.success("","提示：操作成功！");
    }
}
