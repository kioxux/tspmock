package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaUserTrainingRecord;
import com.gys.business.service.UserTrainingRecordService;
import com.gys.business.service.data.UserTrainingRecordInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @desc:
 * @author: ZhangChi
 * @createTime: 2022/1/3 20:12
 */
@RestController
@RequestMapping({"/userTrainingRecord/"})
public class UserTrainingRecordController extends BaseController {
    @Resource
    private UserTrainingRecordService recordService;

    @PostMapping({"/saveUserTrainingRecord"})
    public JsonResult saveUserTrainingRecord(HttpServletRequest request, @RequestBody List<GaiaUserTrainingRecord> recordList){
        GetLoginOutData userInfo = this.getLoginUser(request);
        String client = userInfo.getClient();
        String user = userInfo.getLoginName()+"-"+userInfo.getUserId();
        recordService.saveUserTrainingRecord(recordList,client,user);
        return JsonResult.success("","提示：操作成功！");
    }

    @PostMapping({"/getUserTrainingRecordList"})
    public JsonResult getUserTrainingRecordList(HttpServletRequest request, @RequestBody UserTrainingRecordInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(recordService.getUserTrainingRecordList(inData),"提示：操作成功！");
    }

    @PostMapping({"/exportUserTrainingRecordList"})
    public Result exportUserTrainingRecordList(HttpServletRequest request, @RequestBody UserTrainingRecordInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return recordService.exportUserTrainingRecordList(inData);
    }

    @PostMapping({"/deleteUserTrainingRecord"})
    public JsonResult deleteUserTrainingRecord(HttpServletRequest request, @RequestBody List<GaiaUserTrainingRecord> recordList){
        GetLoginOutData userInfo = this.getLoginUser(request);
        String client = userInfo.getClient();
        recordService.deleteUserTrainingRecord(recordList,client);
        return JsonResult.success("","提示：操作成功！");
    }
}
