package com.gys.business.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.service.MemberSaleSummaryService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CommonUtil;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 *  会员销售汇总
 * @author xiaoyuan
 */

@Api(tags = "会员销售汇总查询")
@RestController
@RequestMapping({"/memberSaleSummary/"})
public class MemberSaleSummaryController extends BaseController {
    @Resource
    public CosUtils cosUtils;

    @Autowired
    private MemberSaleSummaryService memberSaleSummaryService;

    @PostMapping({"findMembersByPage"})
    public JsonResult findMembersByPage(HttpServletRequest request,@Valid  @RequestBody GetMemberInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(memberSaleSummaryService.findMembersByPage(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "列表查询")
    @PostMapping({"memberSalesSummary"})
    public JsonResult memberSalesSummary(HttpServletRequest request,@Valid @RequestBody MemberSaleSummaryInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        System.out.println("inData:" + inData);
        return JsonResult.success(memberSaleSummaryService.memberSalesSummary(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "会员销售汇总列表查询",response = MemberSalesOutData.class)
    @PostMapping({"findMemberList"})
    public JsonResult findMemberList(HttpServletRequest request,@Valid  @RequestBody GetSalesSummaryOfSalesmenReportInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(memberSaleSummaryService.queryMemberCardList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "会员消费汇总表",response = MemberSalesOutData.class)
    @PostMapping({"selectMemberList"})
    public JsonResult selectMemberList(HttpServletRequest request,@RequestBody MemberSalesInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.memberSaleSummaryService.selectMemberCollectList(inData),"提示：提取数据成功！");
    }


//    @ApiOperation(value = "搜索条件：门店编码",response = MemberSalesOutData.class)
//    @PostMapping({"findStoreId"})
//    public JsonResult findStoreId(HttpServletRequest request,@RequestBody MemberSalesInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.memberSaleSummaryService.findStoreId(inData),"提示：提取数据成功！");
//    }

//    @ApiOperation(value = "搜索条件：销售等级",response = MemberSalesOutData.class)
//    @PostMapping({"findSaleClass"})
//    public JsonResult findSaleClass(HttpServletRequest request,@RequestBody MemberSalesInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.memberSaleSummaryService.findSaleClass(inData),"提示：提取数据成功！");
//    }
//
//    @ApiOperation(value = "搜索条件：商品定位",response = MemberSalesOutData.class)
//    @PostMapping({"findSalePosition"})
//    public JsonResult findSalePosition(HttpServletRequest request,@RequestBody MemberSalesInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.memberSaleSummaryService.findSalePosition(inData),"提示：提取数据成功！");
//    }
//
//    @ApiOperation(value = "搜索条件：商品自分类",response = MemberSalesOutData.class)
//    @PostMapping({"findProClass"})
//    public JsonResult findProClass(HttpServletRequest request,@RequestBody MemberSalesInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.memberSaleSummaryService.findProClass(inData),"提示：提取数据成功！");
//    }
//
//    @ApiOperation(value = "搜索条件：自定义1",response = MemberSalesOutData.class)
//    @PostMapping({"findZDY1"})
//    public JsonResult findZDY1(HttpServletRequest request,@RequestBody MemberSalesInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.memberSaleSummaryService.findZDY1(inData),"提示：提取数据成功！");
//    }
//
//    @ApiOperation(value = "搜索条件：自定义2",response = MemberSalesOutData.class)
//    @PostMapping({"findZDY2"})
//    public JsonResult findZDY2(HttpServletRequest request,@RequestBody MemberSalesInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.memberSaleSummaryService.findZDY2(inData),"提示：提取数据成功！");
//    }
//
//    @ApiOperation(value = "搜索条件：自定义3",response = MemberSalesOutData.class)
//    @PostMapping({"findZDY3"})
//    public JsonResult findZDY3(HttpServletRequest request,@RequestBody MemberSalesInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.memberSaleSummaryService.findZDY3(inData),"提示：提取数据成功！");
//    }
//
//    @ApiOperation(value = "搜索条件：自定义4",response = MemberSalesOutData.class)
//    @PostMapping({"findZDY4"})
//    public JsonResult findZDY4(HttpServletRequest request,@RequestBody MemberSalesInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.memberSaleSummaryService.findZDY4(inData),"提示：提取数据成功！");
//    }
//
//    @ApiOperation(value = "搜索条件：自定义5",response = MemberSalesOutData.class)
//    @PostMapping({"findZDY5"})
//    public JsonResult findZDY5(HttpServletRequest request,@RequestBody MemberSalesInData inData){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        inData.setClient(userInfo.getClient());
//        return JsonResult.success(this.memberSaleSummaryService.findZDY5(inData),"提示：提取数据成功！");
//    }

    //合并搜索条件
    @ApiOperation(value = "搜索条件",response = MemberSalesOutData.class)
    @PostMapping({"searchBox"})
    public JsonResult searchBox(HttpServletRequest request,@RequestBody MemberSalesInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return memberSaleSummaryService.searchBox(inData);
    }


    @ApiOperation(value = "会员消费汇总导出")
    @PostMapping({"selectMemberListCSV"})
    public Result selectMemberListCSV(HttpServletRequest request,@RequestBody MemberSalesInData inData) throws IOException {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        List<LinkedHashMap<String,Object>> outData = new ArrayList<>();
        List<LinkedHashMap<String,Object>> outDataStore = new ArrayList<>();
        String fileName = "";
        Map<String, String> titleMap = new LinkedHashMap<>(2);
        Result result = null;
        if (ObjectUtil.isEmpty(inData.getStartDate())){
            throw new BusinessException("开始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())){
            throw new BusinessException("结束日期不能为空！");
        }
        if ("0".equals(inData.getState())){
            outData = memberSaleSummaryService.selectMemberListCSV(inData);
            if (outData.size()>0){
                fileName = "会员消费汇总表导出";
                titleMap.put("cardStore","办卡门店");
                titleMap.put("hykNo","会员卡号");
                titleMap.put("hykName","会员名称");
                titleMap.put("mobile","手机号码");
                titleMap.put("sex","性别");
                titleMap.put("totalSelledDays","期间天数");
                titleMap.put("selledDays","消费天数");
                titleMap.put("gssdnormalAmt","应收金额");
                titleMap.put("ssAmount","实收金额");
                titleMap.put("allCostAmt","含税成本额");
                titleMap.put("grossProfitAmt","毛利额");
                titleMap.put("grossProfitRate","毛利率");
                titleMap.put("tradedTime","消费次数");
                titleMap.put("dailyPayAmt","消费单价");
                titleMap.put("discountAmt","折扣金额");
                titleMap.put("discountRate","折扣率");
                titleMap.put("dailyPayCount","消费频次");
                titleMap.put("proAvgCount","客品次");
                titleMap.put("billAvgPrice","品单价");


                CsvFileInfo fileInfo = CsvClient.getCsvByteByMap(outData,fileName,titleMap);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    bos.write(fileInfo.getFileContent());
                    result = cosUtils.uploadFile(bos, fileInfo.getFileName());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    bos.flush();
                    bos.close();
                }
                return result;
            }else {
                throw new BusinessException("导出失败");
            }

        }else if ("1".equals(inData.getState())){
            outDataStore = memberSaleSummaryService.selectMemberStoListCSV(inData);
            if (outDataStore.size() > 0){
                fileName = "会员消费汇总表（门店）导出";
                titleMap.put("cardStore","办卡门店");
                titleMap.put("hykNo","会员卡号");
                titleMap.put("hykName","会员名称");
                titleMap.put("mobile","手机号码");
                titleMap.put("sex","性别");
                titleMap.put("stoCode","门店编码");
                titleMap.put("saleStore","门店名称");
                titleMap.put("totalSelledDays","期间天数");
                titleMap.put("selledDays","消费天数");
                titleMap.put("gssdnormalAmt","应收金额");
                titleMap.put("ssAmount","实收金额");
                titleMap.put("allCostAmt","含税成本额");
                titleMap.put("grossProfitAmt","毛利额");
                titleMap.put("grossProfitRate","毛利率");
                titleMap.put("tradedTime","消费次数");
                titleMap.put("dailyPayAmt","消费单价");
                titleMap.put("discountAmt","折扣金额");
                titleMap.put("discountRate","折扣率");
                titleMap.put("dailyPayCount","消费频次");
                titleMap.put("proAvgCount","客品次");
                titleMap.put("billAvgPrice","品单价");

                CsvFileInfo fileInfo = CsvClient.getCsvByteByMap(outDataStore,fileName,titleMap);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    bos.write(fileInfo.getFileContent());
                    result = cosUtils.uploadFile(bos, fileInfo.getFileName());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    bos.flush();
                    bos.close();
                }
                return result;
            }else{
                throw new BusinessException("导出失败");
            }
        }
        return result;
    }


    @ApiOperation(value = "会员消费商品明细导出")
    @PostMapping({"selectMemberProductListCSV"})
    public Result selectMemberProductListCSV(HttpServletRequest request,@RequestBody MemberSalesInData inData) throws IOException {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        List<LinkedHashMap<String,Object>> outData = new ArrayList<>();
        List<LinkedHashMap<String,Object>> outDataStore = new ArrayList<>();
        String fileName = "";
        Map<String, String> titleMap = new LinkedHashMap<>(2);
        Result result = null;
        if(ObjectUtil.isNotEmpty(inData.getClassArr())){
            inData.setProductClass(CommonUtil.twoDimensionToOneDimensionArrar(inData.getClassArr()));
        }
        if(ObjectUtil.isNotEmpty(inData.getProCode())){
            inData.setProductCode(inData.getProCode().split(","));
        }
        if (ObjectUtil.isEmpty(inData.getStartDate())){
            throw new BusinessException("开始日期不能为空！");
        }
        if (ObjectUtil.isEmpty(inData.getEndDate())){
            throw new BusinessException("结束日期不能为空！");
        }
        if ("0".equals(inData.getState())){
            outData = memberSaleSummaryService.selectMemberProCSV(inData);
            if (outData.size()>0){
                fileName = "会员消费商品明细导出";
                titleMap.put("cardStore","办卡门店");
                titleMap.put("hykNo","会员卡号");
                titleMap.put("hykName","会员名称");
                titleMap.put("mobile","手机号码");
                titleMap.put("sex","性别");
                titleMap.put("proCode","商品编码");
                titleMap.put("productName","商品名称");
                titleMap.put("count","商品数量");
                titleMap.put("gssdnormalAmt","应收金额");
                titleMap.put("ssAmount","实收金额");
                titleMap.put("discountAmt","折扣金额");
                titleMap.put("discountRate","折扣率");
                titleMap.put("allCostAmt","含税成本额");
                titleMap.put("grossProfitAmt","毛利额");
                titleMap.put("grossProfitRate","毛利率");
                titleMap.put("bigClass","商品大类");
                titleMap.put("midClass","商品中类");
                titleMap.put("smallClass","商品小类");
                titleMap.put("saleClass","销售等级");
                titleMap.put("proPosition","商品定位");
                titleMap.put("proClass","商品自分类");
                titleMap.put("zdy1","自定义1");
                titleMap.put("zdy2","自定义2");
                titleMap.put("zdy3","自定义3");
                titleMap.put("zdy4","自定义4");
                titleMap.put("zdy5","自定义5");


                CsvFileInfo fileInfo = CsvClient.getCsvByteByMap(outData,fileName,titleMap);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    bos.write(fileInfo.getFileContent());
                    result = cosUtils.uploadFile(bos, fileInfo.getFileName());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    bos.flush();
                    bos.close();
                }
                return result;
            }else {
                throw new BusinessException("导出失败");
            }

        }else if ("1".equals(inData.getState())){
            outDataStore = memberSaleSummaryService.selectMemberProStoCSV(inData);
            if (outDataStore.size() > 0){
                fileName = "会员消费商品明细导出";
                titleMap.put("cardStore","办卡门店");
                titleMap.put("hykNo","会员卡号");
                titleMap.put("hykName","会员名称");
                titleMap.put("mobile","手机号码");
                titleMap.put("sex","性别");
                titleMap.put("stoCode","门店编码");
                titleMap.put("saleStore","门店名称");
                titleMap.put("proCode","商品编码");
                titleMap.put("productName","商品名称");
                titleMap.put("count","商品数量");
                titleMap.put("gssdnormalAmt","应收金额");
                titleMap.put("ssAmount","实收金额");
                titleMap.put("discountAmt","折扣金额");
                titleMap.put("discountRate","折扣率");
                titleMap.put("allCostAmt","含税成本额");
                titleMap.put("grossProfitAmt","毛利额");
                titleMap.put("grossProfitRate","毛利率");
                titleMap.put("bigClass","商品大类");
                titleMap.put("midClass","商品中类");
                titleMap.put("smallClass","商品小类");
                titleMap.put("saleClass","销售等级");
                titleMap.put("proPosition","商品定位");
                titleMap.put("proClass","商品自分类");
                titleMap.put("zdy1","自定义1");
                titleMap.put("zdy2","自定义2");
                titleMap.put("zdy3","自定义3");
                titleMap.put("zdy4","自定义4");
                titleMap.put("zdy5","自定义5");

                CsvFileInfo fileInfo = CsvClient.getCsvByteByMap(outDataStore,fileName,titleMap);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                    bos.write(fileInfo.getFileContent());
                    result = cosUtils.uploadFile(bos, fileInfo.getFileName());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    bos.flush();
                    bos.close();
                }
                return result;
            }else{
                throw new BusinessException("导出失败");
            }
        }
        return result;
    }


    @ApiOperation(value = "会员消费明细表",response = MemberSalesOutData.class)
    @PostMapping({"selectMemberDetailsList"})
    public JsonResult selectMemberDetailsList(HttpServletRequest request,@RequestBody MemberSalesInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.memberSaleSummaryService.selectMenberDetailList(inData),"提示：提取数据成功！");
    }

    @ApiOperation(value = "会员卡列表查询")
    @PostMapping({"selectHykList"})
    public JsonResult selectHykList(HttpServletRequest request,@Valid  @RequestBody Map<String,Object>inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.put("clientId",userInfo.getClient());
        inData.put("stoCode",userInfo.getDepId());
        return JsonResult.success(memberSaleSummaryService.selectHykList(inData), "提示：获取数据成功！");
    }


    @ApiOperation(value = "会员卡类型")
    @PostMapping({"membershipCardType"})
    public JsonResult membershipCardType(HttpServletRequest request) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        List<MemberShipTypeData> outData = memberSaleSummaryService.membershipCardType(userInfo.getClient());
        return JsonResult.success(outData, "提示：获取数据成功！");
    }
}
