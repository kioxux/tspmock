//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.ClearBoxService;
import com.gys.business.service.data.AddClearBoxOutData;
import com.gys.business.service.data.ClearBoxInfoOutData;
import com.gys.business.service.data.GetOneClearBoxOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/ClearBox/"})
public class ClearBoxController extends BaseController {
    @Autowired
    private ClearBoxService clearBoxService;

    public ClearBoxController() {
    }

    @PostMapping({"getOneClearBox"})
    public JsonResult getOneClearBox(HttpServletRequest request, @RequestBody ClearBoxInfoOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.clearBoxService.getOneClearBox(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getClearBox"})
    public JsonResult getClearBox(HttpServletRequest request, @RequestBody ClearBoxInfoOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.clearBoxService.getClearBox(inData), "提示：获取数据成功！");
    }

    @PostMapping({"examine"})
    public JsonResult examine(HttpServletRequest request, @RequestBody GetOneClearBoxOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.clearBoxService.examine(inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    @PostMapping({"addClearBox"})
    public JsonResult addClearBox(HttpServletRequest request, @RequestBody AddClearBoxOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        this.clearBoxService.addClearBox(inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }
}
