package com.gys.business.controller;

import com.gys.business.service.GspExpConfService;
import com.gys.business.service.data.GetGspExpConfInData;
import com.gys.business.service.data.GspExpConfData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/warnDays/")
public class GspExpConfController extends BaseController {

    @Resource
    private GspExpConfService gspExpConfService;


    @PostMapping({"saveGspExpConf"})
    public JsonResult saveGspExpConf(HttpServletRequest request, @RequestBody GetGspExpConfInData inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        inData.setCreateUser(userInfo.getLoginName());
        return JsonResult.success( this.gspExpConfService.saveGspExpConf(inData), "提示：保存数据成功！");
    }

	@PostMapping({"selectGspExpConf"})
    public JsonResult selectReportFormat(HttpServletRequest request, @RequestBody GspExpConfData inData) {
        GetLoginOutData userInfo = getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success( this.gspExpConfService.selectGspExpConf(inData), "提示：获取数据成功！");
    }
}
