//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.ElemeService;
import com.gys.business.service.data.GetElemeInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/eleme/"})
public class ElemeController extends BaseController {
    @Resource
    private ElemeService elemeService;

    public ElemeController() {
    }

    @PostMapping({"order/getOrderDetail"})
    public JsonResult orderGetOrderDetail(@RequestBody GetElemeInData inData) {
        this.elemeService.orderGetOrderDetail(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/confirm"})
    public JsonResult orderConfirm(HttpServletRequest request, @RequestBody GetElemeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.elemeService.orderConfirm(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/cancel"})
    public JsonResult orderCancel(HttpServletRequest request, @RequestBody GetElemeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.elemeService.orderCancel(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/preparationMealComplete"})
    public JsonResult orderPreparationMealComplete(HttpServletRequest request, @RequestBody GetElemeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.elemeService.orderPreparationMealComplete(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/refund/agree"})
    public JsonResult orderRefundAgree(HttpServletRequest request, @RequestBody GetElemeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.elemeService.orderRefundAgree(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"order/refund/reject"})
    public JsonResult orderRefundReject(HttpServletRequest request, @RequestBody GetElemeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setAccount(userInfo.getLoginName());
        this.elemeService.orderRefundReject(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }

    @PostMapping({"medicine/stock"})
    public JsonResult medicineStock(HttpServletRequest request, @RequestBody GetElemeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setStoreCode(userInfo.getDepId());
        this.elemeService.medicineStock(inData);
        return JsonResult.success(true, "提示：获取数据成功！");
    }
}
