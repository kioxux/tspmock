package com.gys.business.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.mapper.entity.GaiaSdRechargeCard;
import com.gys.business.service.ElectronicCouponsService;
import com.gys.business.service.GaiaSdMemberClassService;
import com.gys.business.service.MemberService;
import com.gys.business.service.SalesReceiptsService;
import com.gys.business.service.data.GetQueryMemberInData;
import com.gys.business.service.data.GetQueryMemberOutData;
import com.gys.business.service.data.MemberCardInData;
import com.gys.business.service.data.member.GetMemberCardUpgrade;
import com.gys.business.service.data.member.MemberCardUpgradeInData;
import com.gys.business.service.data.member.MemberInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping({"/member"})
public class MemberController extends BaseController {
    @Autowired
    private MemberService memberService;
    @Autowired
    private SalesReceiptsService salesReceiptsService;
    @Autowired
    private GaiaSdMemberClassService memberClassService;
    @Autowired
    private ElectronicCouponsService electronicCouponsService;

    @PostMapping({"/cardlist"})
    public JsonResult cardList(HttpServletRequest request, @RequestBody MemberCardInData memberInData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        memberInData.setClientId(userInfo.getClient());
        return JsonResult.success(this.memberService.getCardList(memberInData), "提示：获取数据成功！");
    }

    @PostMapping({"/getRechargeCardByMemberCardId"})
    public JsonResult getRechargeCardByMemberCardId(HttpServletRequest request, @RequestBody MemberInData memberInData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        memberInData.setClient(userInfo.getClient());
        GaiaSdRechargeCard outData = this.memberService.getRechargeCardByMemberCardId(memberInData);
        if(ObjectUtil.isEmpty(outData)){
            return JsonResult.error();
        }
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"getMemberCardUpgrade"})
    public JsonResult getMemberCardUpgrade(HttpServletRequest request, @RequestBody GetQueryMemberInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        GetMemberCardUpgrade memberOutData = memberClassService.getMemberCardUpgrade(inData);
        return JsonResult.success(memberOutData, "提示：获取数据成功！");
    }
    @PostMapping({"UpdMemberCardChange"})
    public JsonResult UpdMemberCardChange(HttpServletRequest request, @RequestBody MemberCardUpgradeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        memberClassService.UpdMemberCardChange(userInfo,inData);
        return JsonResult.success(null, "提示：获取数据成功！");
    }

    @PostMapping({"memberOfTheQuery"})
    public JsonResult memberOfTheQuery(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<GetQueryMemberOutData> outData = this.salesReceiptsService.memberOfTheQuery(map,userInfo);
        return JsonResult.success(outData, "success");
    }
}

