package com.gys.business.controller;

import com.gys.business.service.CommonAssemblyService;
import com.gys.business.service.data.ProvCityInData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *  @author SunJiaNan
 *  @version 1.0
 *  @date 2021/11/16 16:05
 *  @description 公用数据相关接口
 */
@Api(tags = "公用数据相关接口")
@RestController
@RequestMapping("/commonAssembly")
public class CommonAssemblyController {

    @Autowired
    private CommonAssemblyService commonAssemblyService;


    @ApiOperation(value = "查询省市列表（公用）")
    @PostMapping("/getProvCityList")
    public JsonResult getProvCityList() {
        return commonAssemblyService.getProvCityList();
    }


    @ApiOperation(value = "根据省市查询加盟商")
    @PostMapping("/getClientListByProvCity")
    public JsonResult getClientListByProvCity(@RequestBody ProvCityInData inData) {
        return commonAssemblyService.getClientListByProvCity(inData);
    }
}
