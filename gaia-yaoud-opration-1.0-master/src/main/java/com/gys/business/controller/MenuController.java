package com.gys.business.controller;

import com.gys.business.service.FunctionService;
import com.gys.business.service.GaiaProcessDiagramService;
import com.gys.business.service.MenuService;
import com.gys.business.service.data.Menu.MenuInData;
import com.gys.business.service.data.Menu.MenuOutData;
import com.gys.business.service.data.Menu.MenuTree;
import com.gys.business.service.data.Menu.ProcessDiagramOutData;
import com.gys.business.service.data.MenuConfig.MenuConfigInput;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "菜单")
@RestController
@RequestMapping({"/menu/"})
public class MenuController extends BaseController {

    @Autowired
    private GaiaProcessDiagramService processDiagramService;
    @Autowired
    private MenuService menuService;
    @Autowired
    private FunctionService functionService;

    @GetMapping({"getProcessDiagram"})
    @ApiOperation(value = "获取流程图列表" , response = ProcessDiagramOutData.class)
    @ApiImplicitParam(name = "type" ,value = "类型" , dataType = "String")
    public JsonResult getProcessDiagram(HttpServletRequest request, String type){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(processDiagramService.getProcessDiagram(userInfo,type), "提示：获取数据成功！");
    }

    @GetMapping({"checkUserAuth"})
    @ApiOperation(value = "判断权限" , response = ProcessDiagramOutData.class)
    @ApiImplicitParam(name = "menuId" ,value = "类型" , dataType = "String")
    public boolean checkUserAuth(HttpServletRequest request,String menuId){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return processDiagramService.checkUserAuth(userInfo,menuId);
    }


    @PostMapping({"insertMenu"})
    @ApiOperation(value = "新增菜单" , response = MenuOutData.class)
//    @ApiImplicitParam(name = "menuId" ,value = "类型" , dataType = "String")
    public JsonResult insertMenu(HttpServletRequest request, @RequestBody MenuInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.menuService.insertMenu(userInfo,inData);
        return JsonResult.success("", "提示：编辑成功！");
    }

    @PostMapping({"updateMenu"})
    @ApiOperation(value = "编辑菜单" , response = MenuOutData.class)
    public JsonResult updateMenu(HttpServletRequest request, @RequestBody MenuInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.menuService.updateMenu(userInfo,inData);
        return JsonResult.success("", "提示：编辑成功！");
    }

    @PostMapping({"deleteMenu"})
    @ApiOperation(value = "删除菜单" , response = MenuOutData.class)
    public JsonResult deleteMenu(HttpServletRequest request, @RequestBody MenuInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.menuService.deleteMenu(userInfo,inData);
        return JsonResult.success("", "提示：编辑成功！");
    }

    @PostMapping({"selectMenu"})
    @ApiOperation(value = "查询菜单详情" , response = MenuOutData.class)
    public JsonResult selectMenu( @Valid @RequestBody MenuInData inData) {
        return JsonResult.success( this.menuService.selectMenu(inData), "提示：操作成功！");
    }


    @PostMapping({"selectMenuTree"})
    @ApiOperation(value = "查询树状菜单" , response = MenuOutData.class)
    public JsonResult selectMenuTree() {
        return JsonResult.success( this.menuService.selectMenuTree(), "提示：操作成功！");
    }

    @PostMapping({"updateMenuTree"})
    @ApiOperation(value = "修改树状菜单" , response = MenuOutData.class)
    public JsonResult updateMenuTree(HttpServletRequest request, @RequestBody List<MenuTree> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.menuService.updateMenuTree(userInfo,inData);
        return JsonResult.success(null, "提示：操作成功！");
    }

    @PostMapping({"selectMenuConfig"})
    @ApiOperation(value = "查询菜单分配树状菜单" , response = MenuOutData.class)
    public JsonResult selectMenuConfig(@RequestBody MenuConfigInput inData) {
        return JsonResult.success( this.menuService.selectMenuConfig(inData), "提示：操作成功！");
    }


}
