package com.gys.business.controller;

import com.gys.business.service.RechargeChangeService;
import com.gys.business.service.data.RechargeChangeInData;
import com.gys.business.service.data.RechargeChangeOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


@Api(tags = "储值卡金额异动")
@RestController
@RequestMapping({"/rechargeChange"})
public class RechargeChangeController extends BaseController {

    @Autowired
    private RechargeChangeService rechargeChangeService;

    @PostMapping("/rechargeChangePage")
    @ApiOperation(value = "储值卡充值消费明细分页查询", response = RechargeChangeOutData.class)
    public JsonResult rechargeChangePage(HttpServletRequest request, @Valid @RequestBody RechargeChangeInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.rechargeChangeService.rechargeChangePage(inData,userInfo), "提示：获取数据成功！");
    }

    @PostMapping("/export")
    @ApiOperation(value = "导出消费明细查询")
    public Result export(HttpServletRequest request, @Valid @RequestBody RechargeChangeInData inData, HttpServletResponse response) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return this.rechargeChangeService.export(inData, response,userInfo);
    }
}
