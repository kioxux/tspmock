package com.gys.business.controller;

import com.gys.business.service.FXDoctorAddPointService;
import com.gys.business.service.data.GaiaDoctorAddPointInData;
import com.gys.business.service.data.GaiaDoctorAddPointQueryData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 9:50 2021/7/23
 * @Description：FX医生加点需求
 * @Modified By：guoyuxi.
 * @Version:
 */
@Api(tags = "FX医生加点")
@RestController
@RequestMapping("/doctor/addPoint")
public class FXDoctorAddPointController extends BaseController {

    @Autowired
    private FXDoctorAddPointService fxDoctorAddPointService;

    @ApiOperation(value = "保存医生加点信息",response = GaiaDoctorAddPointInData.class)
    @PostMapping({"saveDoctorAddPoint"})
    public JsonResult saveDoctorAddPoint(HttpServletRequest request,@RequestBody GaiaDoctorAddPointInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        inData.setGsdaUpdateEmp(loginUser.getLoginName());
        return fxDoctorAddPointService.saveDoctorAddPoint(inData);
    }

    @ApiOperation(value = "查询医生加点",response = GaiaDoctorAddPointQueryData.class)
    @PostMapping({"findDoctorAddPointPage"})
    public JsonResult findDoctorAddPointPage(HttpServletRequest request,@RequestBody GaiaDoctorAddPointQueryData queryData) {
        GetLoginOutData loginUser = getLoginUser(request);
        queryData.setClient(loginUser.getClient());
        return JsonResult.success(fxDoctorAddPointService.findDoctorAddPointPage(queryData),"查询成功");
    }


    @ApiOperation(value = "查询医生姓名列表")
    @PostMapping("findDoctorNameList")
    public JsonResult findDoctorNameList(HttpServletRequest request) {
        GetLoginOutData loginUser = getLoginUser(request);
        return fxDoctorAddPointService.findDoctorNameList(loginUser);
    }

}
