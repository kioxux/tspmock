package com.gys.business.controller;

import cn.hutool.core.date.DateUtil;
import com.gys.business.service.DiseaseService;
import com.gys.business.service.data.disease.RelevanceProductInData;
import com.gys.business.service.data.disease.SelectCardTagRequest;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
@RequestMapping({"/disease"})
public class DiseaseController extends BaseController {
    @Autowired
    private DiseaseService diseaseService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    /**
     * @Author huxinxin
     * @Description //FX 首页商品关联推荐  
     * @Date 22:12 2021/8/8
     * @Param [request, inData]
     * @return com.gys.common.data.JsonResult
     **/
    @PostMapping({"getRelevanceProduct"})
    public JsonResult getRelevanceProduct(HttpServletRequest request,@RequestBody RelevanceProductInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(diseaseService.getRelevanceProduct(userInfo,inData), "success");
    }

    @GetMapping("/memberLabelJob")
    public JsonResult memberLabelJob(String startDate, String endDate, Boolean delete) {
        XxlJobHelper.log("<会员卡贴标签开始> {}", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        threadPoolTaskExecutor.execute(() ->diseaseService.setMemberLabel(startDate, endDate, delete));
        XxlJobHelper.log("<会员卡贴标签结束> {}", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        return JsonResult.success();
    }

    @PostMapping("/getCardTagList")
    public JsonResult getCardTag(HttpServletRequest servletRequest,@RequestBody SelectCardTagRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(servletRequest);
        request.setClient(userInfo.getClient());
        return JsonResult.success(diseaseService.getCardTag(request), "success");
    }

}
