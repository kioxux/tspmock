package com.gys.business.controller;

import com.gys.common.base.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 门店手工流向数据表 前端控制器
 * </p>
 *
 * @author yifan.wang
 * @since 2021-08-04
 */
@RestController
@Api(tags = "门店手工流向数据表接口")
@RequestMapping("/sdBatchImportData")
public class GaiaSdBatchImportDataController extends BaseController {


}

