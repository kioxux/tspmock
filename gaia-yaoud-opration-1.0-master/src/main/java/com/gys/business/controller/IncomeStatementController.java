package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.IncomeStatementService;
import com.gys.business.service.data.*;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({"/incomeStatement/"})
@Slf4j
@Api(tags = "报损报溢")
public class IncomeStatementController extends BaseController {
    @Resource
    private IncomeStatementService incomeStatementService;

    @ApiOperation(value = "盘点导入(Web端)", response = GetPhysicalCountHeadOutData.class)
    @PostMapping({"/list"})
    public JsonResult list(HttpServletRequest request, @RequestBody @ApiParam IncomeStatementInData inData) {
        log.info("盘点导入(WEB端): {}", JSON.toJSONString(inData));
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsishBrId(userInfo.getDepId());
        return JsonResult.success(incomeStatementService.list(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "搜索损溢单(FX端)", response = IncomeStatementOutData.class)
    @PostMapping({"/incomeStatementList"})
    public JsonResult incomeStatementList(HttpServletRequest request, @RequestBody IncomeStatementInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsishBrId(userInfo.getDepId());
        return JsonResult.success(incomeStatementService.getIncomeStatementList(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "损溢单明细", response = IncomeStatementDetailOutData.class)
    @PostMapping({"/incomeStatementDetailList"})
    public JsonResult incomeStatementDetailList(HttpServletRequest request, @RequestBody IncomeStatementInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsishBrId(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        return JsonResult.success(incomeStatementService.getIncomeStatementDetailList(inData), "提示：获取数据成功！");
    }

    @FrequencyValid
    @ApiOperation(value = "提交审核")
    @PostMapping({"/insert"})
    public JsonResult insert(HttpServletRequest request, @RequestBody IncomeStatementInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsishBrId(userInfo.getDepId());
        inData.setToken(userInfo.getToken());
        inData.setUserId(userInfo.getUserId());
        log.info("提交审核:{}", JSON.toJSONString(inData));
        incomeStatementService.insert(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"/approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody IncomeStatementInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsishBrId(userInfo.getDepId());
        inData.setToken(userInfo.getToken());
        incomeStatementService.approve(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

    @ApiOperation(value = "搜索损溢单(易联达)", response = IncomeStatementOutData.class)
    @PostMapping({"/getIncomeStatementByYLZ"})
    public JsonResult getIncomeStatementByYLZ(HttpServletRequest request, @RequestBody IncomeStatementInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGsishBrId(userInfo.getDepId());
        Map<String,String> map =new HashMap<>();
        map.put("client",userInfo.getClient() );
        return JsonResult.success( incomeStatementService.getIncomeStatementByYLZ(map), "提示：更新成功！");
    }

    @ApiOperation(value = "移除损益单明细")
    @PostMapping("removeIncomeDetail")
    public JsonResult removeIncomeDetail(HttpServletRequest request, @RequestBody RemoveIncomeForm removeIncomeForm) {
        log.info("<报损报溢><移除明细><请求参数：{}>", JSON.toJSONString(removeIncomeForm));
        try {
            GetLoginOutData loginUser = getLoginUser(request);
            removeIncomeForm.setClient(loginUser.getClient());
            removeIncomeForm.setDepId(loginUser.getDepId());
            incomeStatementService.removeIncomeDetail(removeIncomeForm);
        } catch (Exception e) {
            log.error("<报损报溢><移除明细><移除明细异常：{}>", e.getMessage(), e);
            return JsonResult.error("操作失败，请稍后再试");
        }
        return JsonResult.success(null, "移除成功");
    }
}
