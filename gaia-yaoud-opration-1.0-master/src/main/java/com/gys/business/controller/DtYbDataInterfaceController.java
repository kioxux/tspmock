package com.gys.business.controller;

import com.gys.business.service.DtYbDataInterfaceService;
import com.gys.business.service.data.DtYb.Rkmxlb;
import com.gys.business.service.data.DtYb.RkmxlbEntity;
import com.gys.business.service.data.YbInterfaceQueryData;
import com.gys.common.YiLianDaModel.GYSXXGL.Gysxxlb;
import com.gys.common.YiLianDaModel.YPXXGL.Ypxxlb;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@Api(tags = "大同医保数据交互")
@RequestMapping({"/dtYbDataInterface"})
public class DtYbDataInterfaceController extends BaseController {
    @Autowired
    private DtYbDataInterfaceService dtYbDataInterfaceService;

    @ApiOperation(value = "补货查询列表接口 查询未抛转数据",response = Rkmxlb.class)
    @PostMapping({"/queryExamime"})
    public JsonResult queryExamime(HttpServletRequest request, @Valid @RequestBody YbInterfaceQueryData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.dtYbDataInterfaceService.queryExamime(inData),"success");
    }

    @ApiOperation(value = "补货接口返回结果 记录库存编码和对照表",response = Rkmxlb.class)
    @PostMapping({"/insertExamime"})
    public JsonResult insertExamime(HttpServletRequest request, @Valid @RequestBody RkmxlbEntity inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.dtYbDataInterfaceService.insertExamime(userInfo.getClient(),userInfo.getDepId(),inData),"success");
    }

    @ApiOperation(value = "大同-易联众 商品上传查询",response = Ypxxlb.class)
    @PostMapping({"/selectPushProduct"})
    public JsonResult selectPushProduct(HttpServletRequest request,@Valid @RequestBody YbInterfaceQueryData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<Ypxxlb> list = this.dtYbDataInterfaceService.selectPushProduct(userInfo.getClient(),userInfo.getDepId(),inData.getStartDate(),inData.getEndDate());
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "大同-易联众 商品上传",response = Ypxxlb.class)
    @PostMapping({"/insertPushProduct"})
    public JsonResult insertPushProduct(HttpServletRequest request,@Valid @RequestBody List<Ypxxlb> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);

        return JsonResult.success(this.dtYbDataInterfaceService.insertPushProduct(userInfo.getClient(),userInfo.getDepId(),inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "大同-易联众 供应商上传查询",response = Gysxxlb.class)
    @PostMapping({"/selectPushSupplier"})
    public JsonResult selectPushSupplier(HttpServletRequest request,@Valid @RequestBody YbInterfaceQueryData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<Gysxxlb> list = this.dtYbDataInterfaceService.selectPushSupplier(userInfo.getClient(),userInfo.getDepId(),inData.getStartDate(),inData.getEndDate());
        return JsonResult.success(list, "提示：获取数据成功！");
    }

    @ApiOperation(value = "大同-易联众 供应商上传",response = Gysxxlb.class)
    @PostMapping({"/insertPushSupplier"})
    public JsonResult insertPushSupplier(HttpServletRequest request,@Valid @RequestBody List<Gysxxlb> inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);

        return JsonResult.success(this.dtYbDataInterfaceService.insertPushSupplier(userInfo.getClient(),userInfo.getDepId(),inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "退货查询列表接口 查询未抛转数据",response = Rkmxlb.class)
    @PostMapping({"/queryReturnExamime"})
    public JsonResult queryReturnExamime(HttpServletRequest request, @Valid @RequestBody YbInterfaceQueryData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.dtYbDataInterfaceService.queryReturnExamime(inData),"success");
    }

    @ApiOperation(value = "盘点查询列表接口 查询未抛转数据",response = Rkmxlb.class)
    @PostMapping({"/queryPhysical"})
    public JsonResult queryPhysical(HttpServletRequest request, @Valid @RequestBody YbInterfaceQueryData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.dtYbDataInterfaceService.queryPhysical(inData),"success");
    }

    @ApiOperation(value = "盘点查询列表接口 查询未抛转数据",response = Rkmxlb.class)
    @PostMapping({"/queryProfitloss"})
    public JsonResult queryProfitloss(HttpServletRequest request, @Valid @RequestBody YbInterfaceQueryData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.dtYbDataInterfaceService.queryProfitloss(inData),"success");
    }

    @ApiOperation(value = "库存单据列表接口 查询未抛转数据",response = Rkmxlb.class)
    @PostMapping({"/queryInventory"})
    public JsonResult queryInventory(HttpServletRequest request, @Valid @RequestBody YbInterfaceQueryData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.dtYbDataInterfaceService.queryInventory(inData),"success");
    }
}
