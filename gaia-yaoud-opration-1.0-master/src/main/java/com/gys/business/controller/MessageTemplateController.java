package com.gys.business.controller;

import com.gys.business.mapper.entity.MessageTemplate;
import com.gys.business.service.IMessageTemplateService;
import com.gys.business.service.data.MessageTemplateChooseUserInData;
import com.gys.business.service.data.MessageTemplateInData;
import com.gys.business.service.data.MessageTemplateQueryUserInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author flynn
 * @since 2021-09-07
 */
@RestController
@Api(tags = "接口")
@RequestMapping("/messageTemplate")
public class MessageTemplateController extends BaseController {


    @Autowired
    private IMessageTemplateService messageTemplateService;

    @GetMapping("/buildInit")
    @ApiOperation(value = "新增初始化", notes = "新增初始化")
    public JsonResult buildInit(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.buildInit(userInfo), "提示：获取数据成功！");
    }

    @PostMapping("/save")
    @ApiOperation(value = "新增", notes = "新增")
    public JsonResult save(HttpServletRequest request, @RequestBody MessageTemplateInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.build(userInfo, inData), "提示：保存成功！");
    }

    @GetMapping("/editInit/{id}")
    @ApiOperation(value = "编辑初始化", notes = "编辑初始化")
    public JsonResult updateInit(HttpServletRequest request, @PathVariable(value = "id") String id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.updateInit(userInfo, id), "提示：获取数据成功！");
    }

    @PostMapping("/edit")
    @ApiOperation(value = "修改", notes = "修改")
    public JsonResult update(HttpServletRequest request, @RequestBody MessageTemplateInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.update(userInfo, inData), "提示：修改成功！");
    }


    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除", notes = "删除")
    public JsonResult delete(HttpServletRequest request, @PathVariable(value = "id") String id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.delete(userInfo, id), "提示：删除成功！");
    }


    @PostMapping("/changeStatus")
    @ApiOperation(value = "修改", notes = "修改")
    public JsonResult changeStatus(HttpServletRequest request, @RequestBody MessageTemplateInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.changeStatus(userInfo, inData), "提示：修改成功！");
    }


    @PostMapping("/list")
    @ApiOperation(value = "查询（分页）", notes = "查询（分页）")
    public JsonResult listPage(HttpServletRequest request, @RequestBody MessageTemplateInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.getListPage(userInfo, inData), "提示：查询成功！");
    }


    @PostMapping("/store/list")
    @ApiOperation(value = "平台保存消息之后店铺查看消息列表", notes = "平台保存消息之后店铺查看消息列表")
    public JsonResult storeListPage(HttpServletRequest request, @RequestBody MessageTemplateInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.storeListPage(userInfo, inData), "提示：查询成功！");
    }


    @GetMapping("/store/chooseUser/listInit/{id}")
    @ApiOperation(value = "点击设置接收人", notes = "点击设置接收人")
    public JsonResult chooseUser(HttpServletRequest request, @PathVariable(value = "id") String id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.chooseUser(userInfo,id), "提示：获取数据成功！");
    }

    @GetMapping("/store/add/chooseUser/buildInit")
    @ApiOperation(value = "设置接收人点击新增弹出选择接受人查询初始化门店接口", notes = "设置接收人点击新增弹出选择接受人查询初始化接口")
    public JsonResult chooseUseBuildInit(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.chooseUserBuildInit(userInfo), "提示：获取数据成功！");
    }

    @PostMapping("/store/add/chooseUser/list")
    @ApiOperation(value = "设置接收人点击新增弹出选择接受人查询接口", notes = "设置接收人点击新增弹出选择接受人查询接口")
    public JsonResult storeChooseUserQuery(HttpServletRequest request,  @RequestBody MessageTemplateQueryUserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.storeChooseUserQuery(userInfo,inData), "提示：获取数据成功！");
    }


    @PostMapping("/store/add/chooseUser/save")
    @ApiOperation(value = "平台保存消息之后店铺查看消息列表", notes = "平台保存消息之后店铺查看消息列表")
    public JsonResult storeChooseUserSave(HttpServletRequest request, @RequestBody MessageTemplateChooseUserInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(messageTemplateService.storeChooseUserSave(userInfo, inData), "提示：保存成功！");
    }


    @GetMapping("/testCorn")
    @ApiOperation(value = "设置接收人点击新增弹出选择接受人查询初始化门店接口", notes = "设置接收人点击新增弹出选择接受人查询初始化接口")
    public JsonResult testCorn(HttpServletRequest request) {
        messageTemplateService.runEffect();
        return JsonResult.success("", "提示：获取数据成功！");
    }




}

