package com.gys.business.controller;

import com.gys.business.service.AppSaleReportService;
import com.gys.business.service.data.AppStoreSaleInData;
import com.gys.business.service.data.ProductBigTypeSaleInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.app.salereport.ProPositionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author SunJiaNan
 * @version 1.0
 * @date 2021/8/30 10:38
 * @description app销售报表相关接口
 */
@Slf4j
@Api(tags = "app销售报表")
@RestController
@RequestMapping("/app/saleReport")
public class AppSaleReportController extends BaseController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private AppSaleReportService appSaleReportService;


    @ApiOperation(value = "销售报表图表(上)")
    @PostMapping("/getSaleChartInfo")
    public JsonResult getSaleChartInfo(@RequestBody ProductBigTypeSaleInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return appSaleReportService.getChartInfo(inData);
    }

    @ApiOperation(value = "销售报表图表(下)")
    @PostMapping("/getSaleChartInfoByCode")
    public JsonResult getSaleChartInfoByCode(@RequestBody ProductBigTypeSaleInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return appSaleReportService.getChartInfoByBigCode(inData);
    }

    @ApiOperation(value = "大类销售情况")
    @PostMapping("/getBigTypeSaleList")
    public JsonResult getBigTypeSaleList(@RequestBody ProductBigTypeSaleInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return appSaleReportService.getBigTypeSaleList(inData);
    }

    @ApiOperation(value = "中类销售情况")
    @PostMapping("/getMidTypeSaleList")
    public JsonResult getMidTypeSaleList(@RequestBody ProductBigTypeSaleInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return appSaleReportService.getMidTypeSaleList(inData);
    }

    @ApiOperation(value = "门店销售明细")
    @PostMapping("/getStoreList")
    public JsonResult getStoreList(@RequestBody AppStoreSaleInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return appSaleReportService.getStoreList(inData);
    }

    @ApiOperation(value = "初始化销售数据")
    @PostMapping("/initData")
    public JsonResult initData() {
        appSaleReportService.initSaleData();
        return JsonResult.success("", "success");
    }


    @ApiOperation(value = "判断是否公司负责人/商采主管")
    @PostMapping("/isGroupOrScManager")
    public JsonResult isGroupOrScManager() {
        GetLoginOutData userInfo = super.getLoginUser(request);
        return appSaleReportService.getGroupOrScManagerList(userInfo.getClient(), userInfo.getUserId());
    }

    @ApiOperation(value = "商品定位统计分类")
    @GetMapping("/selectProPositionSummaryCategory")
    public JsonResult selectProPositionSummaryCategory() {
        return appSaleReportService.selectProPositionSummaryCategory();
    }

    @ApiOperation(value = "商品定位统计")
    @PostMapping("/selectProPositionSummary")
    public JsonResult selectProPositionSummary(@RequestBody ProPositionDTO proPositionDTO) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        proPositionDTO.setClient(userInfo.getClient());
        return appSaleReportService.selectProPositionSummary(proPositionDTO);
    }

}
