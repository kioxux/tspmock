package com.gys.business.controller;

import cn.hutool.core.util.ObjectUtil;
import com.gys.business.service.ReplenishSendService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Api(tags = "委托配送下单相关接口")
@RestController
@RequestMapping({"/replenish/send/"})
@Slf4j
public class ReplenishSendController extends BaseController {
    @Resource
    private ReplenishSendService replenishSendService;

    @ApiOperation(value = "委托配送按商品汇总查询",response = GetReplenishDetailOutData.class)
    @PostMapping({"getProDetailList"})
    public JsonResult getProDetailList(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        if(inData.getGspgProIds() != null && inData.getGspgProIds().length > 0){
            if (ObjectUtil.isEmpty(inData.getGspgProIds()[0])){
                inData.setGspgProIds(null);
            }
        }
        if(inData.getGsrhVoucherIds() != null && inData.getGsrhVoucherIds().length > 0){
            if (ObjectUtil.isEmpty(inData.getGsrhVoucherIds()[0])){
                inData.setGsrhVoucherIds(null);
            }
        }
        if(inData.getStoCodes() != null && inData.getStoCodes().length > 0){
            if (ObjectUtil.isEmpty(inData.getStoCodes()[0])){
                inData.setStoCodes(null);
            }
        }
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
//        inData.setClientId("21020006");
//        userInfo.setUserId("10000047");
        return JsonResult.success(this.replenishSendService.getProDetailList(inData,userInfo), "提示：保存成功！");
    }

    @ApiOperation(value = "委托配送按门店汇总查询",response = GetReplenishDetailOutData.class)
    @PostMapping({"getReplenishDetailList"})
    public JsonResult getReplenishDetailList(HttpServletRequest request, @RequestBody GetReplenishInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if(inData.getGspgProIds() != null && inData.getGspgProIds().length > 0){
            if (ObjectUtil.isEmpty(inData.getGspgProIds()[0])){
                inData.setGspgProIds(null);
            }
        }
        if(inData.getGsrhVoucherIds() != null && inData.getGsrhVoucherIds().length > 0){
            if (ObjectUtil.isEmpty(inData.getGsrhVoucherIds()[0])){
                inData.setGsrhVoucherIds(null);
            }
        }
        if(inData.getStoCodes() != null && inData.getStoCodes().length > 0){
            if (ObjectUtil.isEmpty(inData.getStoCodes()[0])){
                inData.setStoCodes(null);
            }
        }
        inData.setClientId(userInfo.getClient());
//        inData.setClientId("21020006");
//        userInfo.setUserId("10000047");
        return JsonResult.success(this.replenishSendService.getReplenishDetailList(inData,userInfo), "提示：保存成功！");
    }

    @ApiOperation(value = "下单",response = GetReplenishInData.class)
    @PostMapping({"placeOrder"})
    public JsonResult placeOrder(HttpServletRequest request, @RequestBody GetPlaceOrderInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
//        inData.setClientId("21020006");
        return JsonResult.success(this.replenishSendService.placeOrder(inData,userInfo), "提示：保存成功！");
    }

    @ApiOperation(value = "委托配送导入信息查询",response = GetReplenishDetailOutData.class)
    @PostMapping({"getImportExcelDetailList"})
    public JsonResult getImportExcelDetailList(HttpServletRequest request,@RequestParam("file")MultipartFile file) {
        GetPlaceOrderInData inData = new GetPlaceOrderInData();
        try {
//            String rootPath = request.getSession().getServletContext().getRealPath("/");
//            String fileName = file.getOriginalFilename();
//            String path = rootPath + fileName;
//            File newFile = new File(path);
//            //判断文件是否存在
//            if(!newFile.exists()) {
//                newFile.mkdirs();//不存在的话，就开辟一个空间
//            }
//            //将上传的文件存储
//            file.transferTo(newFile);
            InputStream in = file.getInputStream();
            //导入已存在的Excel文件，获得只读的工作薄对象
            XSSFWorkbook wk = new XSSFWorkbook(in);
            //获取第一张Sheet表
            Sheet sheet = wk.getSheetAt(0);
            //取数据
            List<ImportInData> importInDataList = getRowAndCell(sheet);
            inData.setImportInDataList(importInDataList);
            in.close();
            wk.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.replenishSendService.getImportExcelDetailList(inData), "提示：获取成功！");
    }

    @ApiOperation(value = "录入比价信息到比价表中")
    @PostMapping({"addSendRepPrice"})
    public JsonResult addSendRepPrice(HttpServletRequest request, @RequestBody RatioProInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        return JsonResult.success(this.replenishSendService.addSendRepPrice(inData), "提示：保存成功！");
    }

    @ApiOperation(value = "比价状态查询")
    @PostMapping({"selectSendRatioStatus"})
    public JsonResult selectSendRatioStatus(HttpServletRequest request, @RequestBody RatioProInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setUserId(userInfo.getUserId());
        return JsonResult.success(this.replenishSendService.selectSendRatioStatus(inData), "提示：查询成功！");
    }

    public List<ImportInData> getRowAndCell(Sheet sheet) {
        List<ImportInData> importInDataList = new ArrayList<>();
        String msg = null;
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        System.out.println("总行数==" + totalRows);
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            for (int i = 1; i <= totalRows; i++) {
                Row row = sheet.getRow(i); //取出一行数据放入row
                ImportInData newExpert = new ImportInData();
                newExpert.setGsrdProId(dataFormatter.formatCellValue(row.getCell(0)).trim());
                newExpert.setGsrdSupid(dataFormatter.formatCellValue(row.getCell(1)).trim());
                newExpert.setGsrdBrId(dataFormatter.formatCellValue(row.getCell(2)).trim());
                newExpert.setGsrdNeedQty(dataFormatter.formatCellValue(row.getCell(3)).trim());
                String poPrice = dataFormatter.formatCellValue(row.getCell(4)).trim();
                if (ObjectUtil.isEmpty(poPrice)){
                    poPrice = "0";
                }
                newExpert.setPoPrice(poPrice);
                importInDataList.add(newExpert);
            } //行end
        }else{
            throw new BusinessException("EXCEL导入信息为空");
        }
        return importInDataList;
    }
}
