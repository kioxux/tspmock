//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.ProEffectService;
import com.gys.business.service.data.ProEffectInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/proEffect/"})
public class ProEffectController extends BaseController {
    @Resource
    private ProEffectService poEffectService;

    public ProEffectController() {
    }

    @PostMapping({"proEffectList"})
    public JsonResult proEffectList(HttpServletRequest request, @RequestBody ProEffectInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.poEffectService.proEffectList(inData), "提示：获取数据成功！");
    }
}
