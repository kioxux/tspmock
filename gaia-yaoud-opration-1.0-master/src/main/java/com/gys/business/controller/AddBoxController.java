//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.AddBoxService;
import com.gys.business.service.data.AddAddBoxOutData;
import com.gys.business.service.data.AddBoxInfoOutData;
import com.gys.business.service.data.GetOneAddBoxOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/addBox/"})
public class AddBoxController extends BaseController {
    @Autowired
    private AddBoxService addBoxService;

    public AddBoxController() {
    }

    @PostMapping({"getOneAddBox"})
    public JsonResult getOneAddBox(HttpServletRequest request, @RequestBody AddBoxInfoOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.addBoxService.getOneAddBox(inData), "提示：获取数据成功！");
    }

    @PostMapping({"getAddBox"})
    public JsonResult getAddBox(HttpServletRequest request, @RequestBody AddBoxInfoOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.addBoxService.getAddBox(inData), "提示：获取数据成功！");
    }

    @PostMapping({"examine"})
    public JsonResult examine(HttpServletRequest request, @RequestBody GetOneAddBoxOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.addBoxService.examine(inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }

    @PostMapping({"addAddBox"})
    public JsonResult addAddBox(HttpServletRequest request, @RequestBody AddAddBoxOutData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        this.addBoxService.addAddBox(inData);
        return JsonResult.success("", "提示：获取数据成功！");
    }
}
