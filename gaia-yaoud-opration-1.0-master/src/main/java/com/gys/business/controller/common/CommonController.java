package com.gys.business.controller.common;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.CommonService;
import com.gys.business.service.data.GetFileOutData;
import com.gys.business.service.data.upload.UpLoadDto;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.util.Date;

import static com.gys.business.bosen.constant.UpLoadConstant.BASE_PATH;
import static com.gys.business.bosen.constant.UpLoadConstant.SOURCE_PRESCRIPTION;
import static com.gys.util.DateUtil.DEFAULT_FORMAT;

@RestController
@RequestMapping({"/common/"})
@Slf4j
public class CommonController extends BaseController {
   @Resource
   private CommonService commonService;

   @PostMapping({"file/upload"})
   public JsonResult fileUpload(HttpServletRequest request, MultipartFile file) {
      GetFileOutData outData = this.commonService.fileUpload(file);
      return JsonResult.success(outData, "提示：上传成功！");
   }

   @PostMapping({"file/getFileUrl"})
   public JsonResult getFileUrl(HttpServletRequest request, GetFileOutData file) {
      log.info(String.format("<文件服务><文件下载-获取文件路径><请求参数：%s>", JSON.toJSONString(file)));
      return JsonResult.success(this.commonService.getFileUrl(file), "提示：查询成功！");
   }

   @PostMapping({"file/uploadFile"})
   public JsonResult imgefileUpload(HttpServletRequest request, MultipartFile file) {
      GetLoginOutData userInfo = this.getLoginUser(request);
      String client=userInfo.getClient();
      UpLoadDto upLoadDto=new UpLoadDto();
      //日期（yyyyMMdd）
      String formatDate = DateUtil.formatDate(new Date());
      StringBuilder stringBuilder=new StringBuilder();
      String path = stringBuilder.append("/").append(BASE_PATH).append("/").append(client).append("/").append(formatDate).append("/").toString();
      upLoadDto.setPath(path);
      upLoadDto.setFile(file);
      upLoadDto.setSource(SOURCE_PRESCRIPTION);
      GetFileOutData outData = this.commonService.fileUpload(upLoadDto);
      return JsonResult.success(outData, "提示：上传成功！");
   }
}
