//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.RemoteReviewerService;
import com.gys.business.service.data.PrescriptionInfoInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/remoteReviewer/"})
public class RemoteReviewerController extends BaseController {
    @Autowired
    private RemoteReviewerService remoteReviewerService;

    public RemoteReviewerController() {
    }

    @PostMapping({"list"})
    public JsonResult list(HttpServletRequest request, @RequestBody PrescriptionInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.remoteReviewerService.getPrescriptionList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"detailList"})
    public JsonResult detailList(HttpServletRequest request, @RequestBody PrescriptionInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.remoteReviewerService.getPrescriptionDetailList(inData), "提示：获取数据成功！");
    }

    @PostMapping({"approve"})
    public JsonResult approve(HttpServletRequest request, @RequestBody PrescriptionInfoInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.remoteReviewerService.approve(inData, userInfo);
        return JsonResult.success((Object)null, "提示：保存成功！");
    }

    @PostMapping({"exportOut"})
    public void exportOut(HttpServletRequest request, HttpServletResponse response, @RequestBody PrescriptionInfoInData inData) {
        try {
            GetLoginOutData userInfo = this.getLoginUser(request);
            inData.setClientId(userInfo.getClient());
            this.remoteReviewerService.exportOut(response, inData);
        } catch (Exception var5) {
            throw new BusinessException(var5.toString());
        }
    }

    @PostMapping({"queryPhoto"})
    public JsonResult queryPhoto(@RequestBody PrescriptionInfoInData inData) {
        return JsonResult.success(this.remoteReviewerService.queryPhoto(inData), "提示：获取数据成功！");
    }
}
