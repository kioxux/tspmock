package com.gys.business.controller.takeAway;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.business.mapper.entity.GaiaTOrderInfo;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.base.BaseController;
import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.util.takeAway.Constants;
import com.gys.util.takeAway.UuidUtil;
import com.gys.util.takeAway.jd.JdApiUtil;
import com.gys.util.takeAway.pts.PtsApiUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/jd/push")
public class ApiJdPushController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(ApiJdPushController.class);
    @Autowired
    private BaseService baseService;
    @Autowired
    private TakeAwayService takeAwayService;

    /**
     * 京东到家建议，推送接口处理数据采用异步，有IO连接限制，最多3S
     * old:{"token":"df383432-1d40-45a5-a69c-f7045f0625c3","expires_in":"31104000","time":"1624005323132","uid":"czlyf0512","user_nick":"czlyf0512","venderId":"329648"}
     * new:{"token":"df383432-1d40-45a5-a69c-f7045f0625c3","expires_in":"31104000","time":"1624005323132","uid":"czlyf0512","user_nick":"czlyf0512","venderId":"329648"}
     * 续签指南：https://openo2o.jddj.com/staticnew/widgets/resources.html?id=2022
     */
    @ResponseBody
    @RequestMapping(value = "/getToken", method = {RequestMethod.GET, RequestMethod.POST})
    public String getToken(HttpServletRequest request) {
        Map<String, Object> retBody = new HashMap<>();
        Map<String, Object> paramMap = getParameterMap(request);
        logger.info("京东到家API：推送token: " + JSONObject.toJSONString(paramMap));
        String tokenStr = nullToEmpty(paramMap.get("token"));
        if (StrUtil.isEmpty(tokenStr)) {
            return JdApiUtil.apiReturnMsg("0", "success", retBody);
        }
        Map<String, Object> tokenMap = (Map<String, Object>) JSON.parse(tokenStr);
//        String createTime = nullToEmpty(tokenMap.get("time"));
//        Long expiresLong = Long.valueOf(createTime) + Long.valueOf(nullToEmpty(tokenMap.get("expires_in"))) * 1000;
//        String expires = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new java.util.Date(expiresLong));
        String venderId = (String)tokenMap.get("venderId");
        String token = (String)tokenMap.get("token");
        GaiaTAccount account = takeAwayService.getAccountByVenderId(venderId);
        if (account == null) {
            logger.info("京东到家API：推送token，并无account用户，venderId:{},token:{}", venderId, token);
            return JdApiUtil.apiReturnMsg("-1", "操作失败", Maps.newHashMap());
        }
        //判断数据库有没有token，是否相等
        String dbToken = account.getToken();
        if (token.equals(dbToken)) {
            logger.info("京东到家API：推送token，token相等，pass");
            return JdApiUtil.apiReturnMsg("0", "success", retBody);
        }
        if (dbToken == null) {
            logger.info("京东到家API：第一次新增token，venderId:{},token:{}", venderId, token);
            takeAwayService.updateJdToken(venderId, token);
            return JdApiUtil.apiReturnMsg("0", "success", retBody);
        }
        //替换token, 调更新确认接口
        boolean verifyBoolean = JdApiUtil.verifyUpdateToken(dbToken, token, account.getAppId(), account.getAppSecret());
        if (verifyBoolean) {
            takeAwayService.updateJdToken(venderId, token);
            logger.info("京东到家API：替换token，venderId:{},oldToken:{},newToken:{}", venderId, dbToken, token);
        }
        return JdApiUtil.apiReturnMsg("0", "success", retBody);
    }

    /**
     * 京东到家API：推送已支付订单（必接）
     * 应用场景：
     * 客户支付订单后，推送创建新订单消息。
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/djsw/newOrder", method = RequestMethod.POST)
    public Map<String, Object> orderPayForJd(HttpServletRequest request) {
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            return apiResult("0", "success", "");
        }
        logger.info("京东到家API：推送已支付订单，paramMap：{}", JSON.toJSONString(paramMap));
        // 这个接口没做任何处理,直接pass
        return apiResult("0", "success", "");
        /*Map<String, Object> jdParamJson = (Map<String, Object>) JSON.parse(nullToEmpty(paramMap.get("jd_param_json")));
        String orderId = nullToEmpty(jdParamJson.get("billId"));
        String token = nullToEmpty(paramMap.get("token"));// 采用OAuth授权方式为必填参数
        String appKey = nullToEmpty(paramMap.get("app_key"));// 京东到家分配给APP方的id
        //1.校验接口参数
        int resultForValid = validApiParamForJd(paramMap, "");
        if (resultForValid == 0 || resultForValid == 1) {
            //接口存活验证成功！|京东订单推送API签名验证成功
            return apiResult("0", "success", "");
        } else {
            Map<String, Object> logMap = createOperateLogMap("5", "3", "orderPayForJd", "京东到家API：推送已支付订单");
            setOperateLogInfo(logMap, "0", "京东订单推送API签名验证失败。" + "平台订单ID：" + orderId);
            baseService.insertOperatelogWithMap(logMap);
            return apiResult("10014", "无效Sign签名", "");
        }*/
    }

    /**
     * 京东到家API：推送订单应结消息（必接）
     * 场景：订单记账成功后，推送订单应结消息。
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/djsw/orderAccounting", method = RequestMethod.POST)
    public Map<String, Object> orderAccountForJd(HttpServletRequest request) {
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            return apiResult("0", "success", "");
        }
        logger.info("京东到家API：推送订单应结消息，paramMap：{}", JSON.toJSONString(paramMap));
        Map<String, Object> jdParamJson = (Map<String, Object>) JSON.parse(nullToEmpty(paramMap.get("jd_param_json")));
        String platformOrderId = nullToEmpty(jdParamJson.get("billId"));
        String token = nullToEmpty(paramMap.get("token"));// 采用OAuth授权方式为必填参数
        String appKey = nullToEmpty(paramMap.get("app_key"));// 京东到家分配给APP方的id

        //先获取appSecret
        String appSecret = takeAwayService.getJdSecret(appKey);
        if (appSecret == null) {
            logger.error("京东到家-推送订单应结消息，查不到appSecret，appKey:{}, platformOrderId:{}", appKey, platformOrderId);
            return apiResult("-1", "操作失败", "");
        }

        //1.校验接口参数
        int resultForValid = baseService.validApiParamForJd(paramMap, appSecret);
        logger.info("京东到家，推送订单应结消息，参数验证，result:{}", resultForValid);
        if (resultForValid == 0) {
            //接口存活验证成功！
            return apiResult("0", "success", "");
        } else if (resultForValid == 2) {
            //京东订单推送API签名验证失败
            Map<String, Object> logMap = createOperateLogMap("5", "3", "orderAccountForJd", "京东到家API：推送订单应结消息");
            setOperateLogInfo(logMap, "0", "京东订单推送API签名验证失败。" + "平台订单ID：" + platformOrderId);
            baseService.insertOperatelogWithMap(logMap);
            return apiResult("10014", "无效Sign签名", "");
        } else {
            // 2.异步处理所有业务逻辑
            // 1.获取订单应结金额
            BaseOrderInfo tempInfo = JdApiUtil.orderAmount(appKey, appSecret, token, platformOrderId);
            // 2.获取订单应结金额成功后（并且不为零）
            if ("ok".equals(tempInfo.getResult()) && !"0".equals(tempInfo.getTotal_price()) && tempInfo.getTotal_price() != null) {
                BaseOrderInfo baseOrderInfo = JdApiUtil.orderDetail(appKey, appSecret, token, platformOrderId);
                // 判断订单是否获取成功
                if ("ok".equals(baseOrderInfo.getResult())) {
                    String shopNo = baseOrderInfo.getShop_no();//即public_code
                    GaiaTAccount account = takeAwayService.selectStoCodeByPlatformsAndPublicCodeAndAppId(Constants.PLATFORMS_JD, shopNo, appKey);
                    if (account == null || !"Y".equalsIgnoreCase(account.getOrderOnService())) {
                        logger.error("京东到家-推送订单应结消息：找不到对应门店信息！ platformOrderId:{}, appId:{}, shopNo:{}", platformOrderId, appKey, shopNo);
                        return apiResult("0", "success", "");
                    }
                    String client = account.getClient();
                    String stoCode = account.getStoCode();
                    String nativeShopId = account.getPublicCode();

                    String orderId = setInfoReturnOrderId(platformOrderId, tempInfo, baseOrderInfo, client, stoCode);
                    String resultForXB = baseService.insertOrderInfoWithMap(baseOrderInfo);
                    if (resultForXB.startsWith("ok")) {
                        // 新订单推送PTS：同门店串行，不通门店并行，用线程处理
                        Map<String, Object> order_info = PtsApiUtil.getOrderInfoToPTSByMap(baseOrderInfo);
                        //获取当前时间timestamp
                        Timestamp lastTimestamp = new Timestamp((new Date()).getTime());
                        String shop_no = nullToEmpty(order_info.get("shop_no"));
                        String delivery_time = String.valueOf(order_info.get("delivery_time"));
                        logger.debug("order_info:" + order_info);
                        if (shop_no.equals("")) return apiResult("-1", "操作失败", "");
                        //若门店接单模式为“手动接单”模式，商家需要调此接口，“确认接单”或“取消接单”
                        //不应该阻塞
                        String resultForMt = JdApiUtil.orderConfirm(appKey, appSecret, token, platformOrderId);
                        logger.info("京东到家，推送订单应结消息，确认接单，platformOrderId:{}, publicCode:{}, result:{}", platformOrderId, shopNo, resultForMt);
                        if (!resultForMt.startsWith("ok")) {
                            //消息队列只记录调用接口失败的情况
                            //shop_no|
                            logger.info("外卖-订单API：商家确认订单失败！ 平台订单ID：" + platformOrderId);
                            Map<String, Object> logMap = new HashMap<String, Object>();
                            logMap.put("operatetype", "4");
                            logMap.put("platform_type", "1");
                            logMap.put("operatename", "deliveryOrderConfirm");
                            logMap.put("remark", "订单API：商家确认订单");
                            logMap.put("IsOk", "0");
                            logMap.put("reason", "门店号：" + shop_no + "订单号：" + orderId + "平台订单号：" + platformOrderId + "错误原因：" + resultForMt);
                            baseService.insertOperatelogWithMap(logMap);
                        }
                        String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                        order_info.put("storeLogo", storeLogo);
                        takeAwayService.pushToStoreMq(client, stoCode, "O2O", "newOrder", order_info);
                        logger.info("推送外卖新订单, mq 推送， orderId:{}, storeLogo:{}, client:{}, stoCode:{}", orderId, storeLogo, client, stoCode);
                        return apiResult("0", "success", "");
                    }
                }
            }
        }
        return apiResult("-1", "操作失败", "");
    }

    private String setInfoReturnOrderId(String platformOrderId, BaseOrderInfo tempInfo, BaseOrderInfo baseOrderInfo,
                                        String client, String stoCode) {
        baseOrderInfo.setPlatforms(Constants.PLATFORMS_JD);
        baseOrderInfo.setPlatforms_order_id(platformOrderId);
        String order_id = UuidUtil.getUUID("jd-");
        baseOrderInfo.setOrder_id(order_id);
        baseOrderInfo.setAction("1");
        baseOrderInfo.setTotal_price(tempInfo.getTotal_price());
        baseOrderInfo.setClient(client);
        baseOrderInfo.setStoCode(stoCode);
        for (BaseOrderProInfo item : baseOrderInfo.getItems()) {
            item.setOrder_id(order_id);
            item.setClient(client);
            item.setStoCode(stoCode);
        }
        baseOrderInfo.setNotice_message("new");
        baseOrderInfo.setStatus("1");
        return order_id;
    }

    /**
     * 京东到家API：推送订单运单状态消息（必接）
     * 应用场景:
     * 订单到门店生成运单，配送员维护运单状态，推送订单运单状态消息。
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/djsw/pushDeliveryStatus", method = RequestMethod.POST)
    public Map<String, Object> orderDeliveryStatusForJd(HttpServletRequest request) {
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            return apiResult("0", "success", "");
        }
        logger.info("京东到家API：推送订单运单状态消息，paramMap：{}", JSON.toJSONString(paramMap));
        Map<String, Object> jdParamJson = (Map<String, Object>) JSON.parse(nullToEmpty(paramMap.get("jd_param_json")));
        String platformOrderId = nullToEmpty(jdParamJson.get("orderId"));
        String token = nullToEmpty(paramMap.get("token"));// 采用OAuth授权方式为必填参数
        String appKey = nullToEmpty(paramMap.get("app_key"));// 京东到家分配给APP方的id

        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_JD, platformOrderId);
        if (orderInfo == null) {
            logger.info("美团推送订单运单状态消息, orderInfo = null , pass");
            return apiResult("-1", "操作失败", "");
        }
        String stoCode = orderInfo.getStoCode();
        String client = orderInfo.getClient();
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_JD, appKey, stoCode, client);
        if (account == null || !"Y".equalsIgnoreCase(account.getOrderOnService())) {
            logger.error("京东到家-推送订单应结消息：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", platformOrderId, appKey, stoCode);
            return apiResult("0", "success", "");
        }
        String appSecret = account.getAppSecret();
        String nativeShopId = account.getPublicCode();
        //1.校验接口参数
        int resultForValid = baseService.validApiParamForJd(paramMap, appSecret);
        logger.info("京东到家，推送订单运单状态消息，参数验证，result:{}", resultForValid);
        if (resultForValid == 0) {
            //接口存活验证成功！
            return apiResult("0", "success", "");
        } else if (resultForValid == 2) {
            //京东订单推送API签名验证失败
            Map<String, Object> logMap = createOperateLogMap("5", "3", "orderDeliveryStatusForJd", "京东到家API：推送订单运单状态消息");
            setOperateLogInfo(logMap, "0", "京东订单推送API签名验证失败。" + "平台订单ID：" + platformOrderId);
            baseService.insertOperatelogWithMap(logMap);
            return apiResult("10014", "无效Sign签名", "");
        } else {
            // 2.异步处理所有业务逻辑
            String deliveryManName = nullToEmpty(jdParamJson.get("deliveryManName"));
            String deliveryManPhone = nullToEmpty(jdParamJson.get("deliveryManPhone"));
            String deliveryStatus = nullToEmpty(jdParamJson.get("deliveryStatus"));

            paramMap.put("platforms_order_id", platformOrderId);
            //重置order_id，只保留platforms_order_id
            paramMap.put("order_id", "");
            paramMap.put("platforms", Constants.PLATFORMS_JD);
            Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
            if ("ok".equals(nullToEmpty(retMap.get("result")))) {
                //确认配送员动作
                retMap.put("action", "8");
                retMap.put("last_action", "8");
                retMap.put("shipper_name", deliveryManName);
                retMap.put("shipper_phone", deliveryManPhone);
                //获取shop_no
                String shop_no = nullToEmpty(retMap.get("shop_no"));
                /**
                 * 配送状态:
                 * 10 等待抢单
                 * 20 已抢单
                 * 21 配送员取消抢单，等待重新抢单
                 * 22 更换配送员
                 * 23 配送员已到店
                 * 25 取货失败
                 * 26 取货失败审核驳回
                 * 27 取货失败待审核
                 * 28 骑士异常上报
                 * 29 异常上报已处理
                 * 30 取货完成
                 * 35 投递失败
                 * 40 已完成
                 */
                if ("10".equals(deliveryStatus) || "20".equals(deliveryStatus) || "23".equals(deliveryStatus) || "30".equals(deliveryStatus)) {
                    String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                    if ("ok".equals(resultForXB)) {
                        //订单状态变更推送PTS：用线程处理
                        //TaskExecutorUtil.asyncExecOrderStatusChangePush(retMap);//秦青于删除20190116删除，暂时不需要推送配送员信息
                    }
                }
            }
            return apiResult("0", "success", "");
        }
    }

    /**
     * 京东到家API：推送用户取消订单申请/用户取消消息
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/djsw/applyCancelOrder", "/djsw/userCancelOrder"}, method = RequestMethod.POST)
    public Map<String, Object> orderCancelForJd(HttpServletRequest request) {
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            return apiResult("0", "success", "");
        }
        //{"app_key":"025ffcc0534a4dd08a44cecde332120f","v":"1.0","sign":"6A86EBA781E90916F31B9BCD955FD225","format":"json",
        // "jd_param_json":"{\"billId\":\"2118528307000531\",\"remark\":\"我自己想取消:备注信息忘记填写或填错\",\"statusId\":\"20030\",\"timestamp\":\"2021-08-03 10:46:02\"}",
        // "token":"df383432-1d40-45a5-a69c-f7045f0625c3","timestamp":"2021-08-03 10:46:07"}
        logger.info("京东到家API：推送用户取消订单申请/用户取消消息，paramMap：{}", JSON.toJSONString(paramMap));
        Map<String, Object> jdParamJson = (Map<String, Object>) JSON.parse(nullToEmpty(paramMap.get("jd_param_json")));
        String platformOrderId = nullToEmpty(jdParamJson.get("billId"));// 外卖平台订单ID
        String token = nullToEmpty(paramMap.get("token"));// 采用OAuth授权方式为必填参数
        String appKey = nullToEmpty(paramMap.get("app_key"));// 京东到家分配给APP方的id

        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_JD, platformOrderId);
        if (orderInfo == null) {
            logger.info("美团推送用户取消订单申请/用户取消消息, orderInfo = null , pass");
            return apiResult("-1", "操作失败", "");
        }
        String client = orderInfo.getClient();
        String stoCode = orderInfo.getStoCode();
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_JD, appKey, stoCode, client);
        if (account == null || !"Y".equalsIgnoreCase(account.getOrderOnService())) {
            logger.error("京东到家-推送用户取消订单申请/用户取消消息：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", platformOrderId, appKey, stoCode);
            return apiResult("0", "success", "");
        }
        String appSecret = account.getAppSecret();
        String nativeShopId = account.getPublicCode();

        //1.校验接口参数
        int resultForValid = baseService.validApiParamForJd(paramMap, appSecret);
        logger.info("京东到家，推送用户取消订单申请/用户取消消息，参数验证，result:{}", resultForValid);
        if (resultForValid == 0) {
            //接口存活验证成功！
            return apiResult("0", "success", "");
        } else if (resultForValid == 2) {
            //京东订单推送API签名验证失败
            Map<String, Object> logMap = createOperateLogMap("5", "3", "orderCancelForJd", "京东到家API：推送用户取消订单申请/用户取消消息");
            setOperateLogInfo(logMap, "0", "京东到家订单推送API签名验证失败。" + "平台订单ID：" + platformOrderId);
            baseService.insertOperatelogWithMap(logMap);
            return apiResult("10014", "无效Sign签名", "");
        } else {
            // 2.异步处理所有业务逻辑
            String reasonCode = nullToEmpty(jdParamJson.get("statusId"));// 取消编码
            String reason = nullToEmpty(jdParamJson.get("remark"));// 取消原因
            String requestUrl = request.getRequestURI();// 请求地址

            paramMap.put("platforms_order_id", platformOrderId);
            //重置order_id，只保留platforms_order_id
            paramMap.put("order_id", "");
            paramMap.put("platforms", Constants.PLATFORMS_JD);
            Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
            if ("ok".equals(nullToEmpty(retMap.get("result")))) {
                String resForConfirmCancel = "ok";
                if (requestUrl.indexOf("applyCancelOrder") != -1) {
                    // 默认商家同意用户取消订单的申请 //todo 默认同意？？？
                    resForConfirmCancel = JdApiUtil.orderCancelConfirm(appKey, appSecret, token, platformOrderId);
                }
                logger.info("京东到家，推送用户取消订单申请/用户取消消息，默认商家同意用户取消订单的申请，platformOrderId:{}，result:{}", platformOrderId, resForConfirmCancel);
                //新的状态：已取消
                retMap.put("status", "4");
                //用户申请退单
                retMap.put("action", "4");
                retMap.put("last_action", "4");
                retMap.put("reason_code", reasonCode);
                retMap.put("reason", reason);
                retMap.put("notice_message", "cancel");
                String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                if ("ok".equals(resultForXB)) {
                    String storeLogo = takeAwayService.getStoreLogoByClientAndBrId(client, stoCode);
                    retMap.put("storeLogo", storeLogo);
                    takeAwayService.pushToStoreMq(client, stoCode, "O2O", "cancel", retMap);
                    logger.info("京东到家推送取消订单，orderId:{},client:{},stoCode:{},storeLogo:{}", nullToEmpty(retMap.get("order_id")), client, stoCode, storeLogo);
                } else {
                    logger.error("京东到家，推送用户取消订单申请/用户取消消息，更新表数据报错，platformOrderId:{}", platformOrderId);
                    return apiResult("-1", "操作失败", "");
                }
            } else {
                logger.error("京东到家，推送用户取消订单申请/用户取消消息，未查询到订单数据，platformOrderId:{}", platformOrderId);
                return apiResult("-1", "操作失败", "");
            }
            return apiResult("0", "success", "");
        }
    }

    /**
     * 京东到家API：推送已完成订单
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/djsw/orderFinish", method = RequestMethod.POST)
    public Map<String, Object> orderCompleteForJd(HttpServletRequest request) {
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            return apiResult("0", "success", "");
        }
        logger.info("京东到家API：推送已完成订单，paramMap：{}", JSON.toJSONString(paramMap));
        Map<String, Object> jdParamJson = (Map<String, Object>) JSON.parse(nullToEmpty(paramMap.get("jd_param_json")));
        String platformOrderId = nullToEmpty(jdParamJson.get("orderId"));// 外卖平台订单ID
        String token = nullToEmpty(paramMap.get("token"));// 采用OAuth授权方式为必填参数
        String appKey = nullToEmpty(paramMap.get("app_key"));// 京东到家分配给APP方的id

        GaiaTOrderInfo orderInfo = takeAwayService.selectStoCodeByPlatformsAndPlatformsOrderId(Constants.PLATFORMS_JD, platformOrderId);
        if (orderInfo == null) {
            logger.info("美团推送已完成订单, orderInfo = null , pass");
            return apiResult("-1", "操作失败", "");
        }
        String client = orderInfo.getClient();
        String stoCode = orderInfo.getStoCode();
        GaiaTAccount account = takeAwayService.getPlatformAccountByStoCode(Constants.PLATFORMS_JD, appKey, stoCode, client);
        if (account == null || !"Y".equalsIgnoreCase(account.getOrderOnService())) {
            logger.error("京东到家-推送已完成订单：找不到对应门店信息！ platformOrderId:{}, appId:{}, stoCode:{}", platformOrderId, appKey, stoCode);
            return apiResult("0", "success", "");
        }
        String appSecret = account.getAppSecret();
        String nativeShopId = account.getPublicCode();

        //1.校验接口参数
        int resultForValid = baseService.validApiParamForJd(paramMap, appSecret);
        logger.info("京东到家，推送已完成订单，参数验证，result:{}", resultForValid);
        if (resultForValid == 0) {
            //接口存活验证成功！
            return apiResult("0", "success", "");
        } else if (resultForValid == 2) {
            //京东订单推送API签名验证失败
            Map<String, Object> logMap = createOperateLogMap("5", "3", "orderCompleteForJd", "京东到家API：推送已完成订单");
            setOperateLogInfo(logMap, "0", "京东订单推送API签名验证失败。" + "平台订单ID：" + platformOrderId);
            baseService.insertOperatelogWithMap(logMap);
            return apiResult("10014", "无效Sign签名", "");
        } else {
            // 2.异步处理所有业务逻辑
            paramMap.put("platforms_order_id", platformOrderId);
            //重置order_id，只保留platforms_order_id
            paramMap.put("order_id", "");
            //平台：京东
            paramMap.put("platforms", Constants.PLATFORMS_JD);
            Map<String, Object> retMap = baseService.selectOrderInfoByMap(paramMap);
            if ("ok".equals(nullToEmpty(retMap.get("result")))) {
                String shop_no = nullToEmpty(retMap.get("shop_no"));
                //已完成状态
                retMap.put("status", "3");
                retMap.put("action", "0");
                retMap.put("notice_message", "");
                //4.更新订单状态为已完成
                String resultForXB = baseService.updateOrderInfoWithMap(retMap);
                if ("ok".equals(resultForXB)) {
                    paramMap.put("order_id", nullToEmpty(retMap.get("order_id")));
                    paramMap.put("status", "3");
                    paramMap.put("shop_no", nullToEmpty(retMap.get("shop_no")));
                    takeAwayService.pushToStoreMq(client, stoCode, "O2O", "orderStatus", retMap);
                }
            }
            return apiResult("0", "success", "");
        }
    }

    /**
     * 京东到家API：售后单申请消息
     * 应用场景:
     * 售后单审核通过后，开放平台根据商家编号将售后单号推送给相应的商家， 针对用户申请售后单是物流责任的售后单，平台不再推送售后单消息给商家。
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = {"/djsw/newApplyAfterSaleBill", "/djsw/updateApplyAfterSaleBill"}, method = RequestMethod.POST)
    public Map<String, Object> ApplyAfterSaleBillForJd(HttpServletRequest request) {
        Map<String, Object> paramMap = getParameterMap(request);
        if (paramMap == null || paramMap.size() == 0) {
            return apiResult("0", "success", "");
        }
        logger.info("京东到家API：售后单申请消息，paramMap：{}", JSON.toJSONString(paramMap));
        Map<String, Object> jdParamJson = (Map<String, Object>) JSON.parse(nullToEmpty(paramMap.get("jd_param_json")));
        String afterSaleOrderId = nullToEmpty(jdParamJson.get("billId"));//
        String token = nullToEmpty(paramMap.get("token"));// 采用OAuth授权方式为必填参数
        String appKey = nullToEmpty(paramMap.get("app_key"));// 京东到家分配给APP方的id

        //这边给的值太少，先由token获取appSecret
        String appSecret = takeAwayService.getAppSecretByToken(token);
        if (appSecret == null) {
            logger.info("京东到家API：售后单申请消息，由token反推appSecret失败，token:{}", token);
            return apiResult("-1", "操作失败", "");
        }
        boolean isSuccess = baseService.applyAfterSaleBillForJd(paramMap, token, appKey, afterSaleOrderId, appSecret);
        if (isSuccess) {
            return apiResult("0", "success", "");
        } else {
            return apiResult("-1", "操作失败", "");
        }
    }

    /**
     * 京东到家：推送API接口参数验证
     *
     * @param request
     * @return true| false
     */
    public boolean validApiParamForJd(HttpServletRequest request) {
        Map<String, Object> map = getParameterMap(request);
        String token = nullToEmpty(map.get("token"));// 采用OAuth授权方式为必填参数
        String app_key = nullToEmpty(map.get("app_key"));// 京东到家分配给APP方的id
        String sign = nullToEmpty(map.get("sign"));// 签名结果
        logger.debug("京东订单号： " + map.toString());
        //系统级3个参数都要验证，如果都为空，说明京东到家只是在做一个接口存活验证
        //这时候调用接口，直接返回一个OK给京东到家就行
        if (StrUtil.isEmpty(token) && StrUtil.isEmpty(app_key) && StrUtil.isEmpty(sign)) {
            logger.debug("京东接口存活验证成功！");
            return false;
        }
        if (StrUtil.isEmpty(token)) {
            logger.debug("京东到家订单推送API签名验证失败,签名: " + token);
            return false;
        } else {
            logger.debug("京东到家订单推送API签名验证成功,签名: " + token);
            return true;
        }
    }

    public Map<String, Object> apiResult(String code, String msg, String data) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("code", code);
        resultMap.put("msg", msg);
        resultMap.put("data", data);
        return resultMap;
    }

}
