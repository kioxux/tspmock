package com.gys.business.controller;

import com.gys.business.service.ProductLimitSetService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Api(tags = "会员限购", description = "")
@RestController
@RequestMapping({"/productLimitSet/"})
public class ProductLimitSetController extends BaseController {


    @Autowired
    private ProductLimitSetService productLimitSetService;


    @ApiOperation(value = "会员限购初始化接口", notes = "会员限购初始化接口")
    @GetMapping({"init"})
    public JsonResult queryProVo(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.productLimitSetService.init(userInfo), "提示：获取数据成功！");
    }


    @PostMapping({"/checkIfInDb"})
    public JsonResult checkIfInDb(HttpServletRequest request, @RequestBody ProductLimitCheckInDb inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.productLimitSetService.checkIfInDb(userInfo, inData), "提示：无商品存在设置！");
    }


    @PostMapping({"/save"})
    public JsonResult save(HttpServletRequest request, @RequestBody ProductLimitInReq inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.productLimitSetService.save(userInfo, inData), "提示：保存成功！");
    }


    @PostMapping({"/editInit/{id}"})
    public JsonResult editInit(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.productLimitSetService.editInit(userInfo, id), "提示：保存成功！");
    }

    @PostMapping({"/edit"})
    public JsonResult edit(HttpServletRequest request, @RequestBody ProductLimitInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.productLimitSetService.edit(userInfo, inData), "提示：保存成功！");
    }


    @PostMapping({"/list"})
    public JsonResult list(HttpServletRequest request, @RequestBody ProductLimitSetSearchVo inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.productLimitSetService.list(userInfo, inData), "提示：查询成功！");
    }

    @PostMapping({"/delete/{id}"})
    public JsonResult delete(HttpServletRequest request, @PathVariable(value = "id") Integer id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.productLimitSetService.delete(userInfo, id), "提示：删除成功！");
    }


    @PostMapping({"/readExcel"})
    public JsonResult readExcel(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        if (userInfo == null) {
            throw new BusinessException("请重新登陆！");
        }
        List<GetProductThirdlyOutDataExtends> outDataList = new ArrayList<>();
        try {
            InputStream in = file.getInputStream();
            //导入已存在的Excel文件，获得只读的工作薄对象
            XSSFWorkbook wk = new XSSFWorkbook(in);
            //获取第一张Sheet表
            Sheet sheet = wk.getSheetAt(0);
            //取数据
            List<String> importInDataList = getRowAndCell(sheet);
            ProductLimitCheckInDb checkInDb = new ProductLimitCheckInDb();
            checkInDb.setProIds(importInDataList);
            if (!this.productLimitSetService.checkIfInDb(userInfo, checkInDb)) {
                outDataList = productLimitSetService.getProductInfoList(userInfo, importInDataList);
            }
            in.close();
            wk.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return JsonResult.success(outDataList, "提示：获取成功！");
    }


    public List<String> getRowAndCell(Sheet sheet) {
        List<String> importInDataList = new ArrayList<>();
        String msg = null;
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        System.out.println("总行数==" + totalRows);
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            for (int i = 1; i <= totalRows; i++) {
                Row row = sheet.getRow(i); //取出一行数据放入row
                importInDataList.add(dataFormatter.formatCellValue(row.getCell(0)).trim());
            } //行end
        } else {
            throw new BusinessException("EXCEL导入信息为空");
        }
        return importInDataList;
    }

    @PostMapping({"/commodityPurchaseRestrictions"})
    public JsonResult commodityPurchaseRestrictions(HttpServletRequest request, @RequestBody PurchaseLimitVo limitVo) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.productLimitSetService.commodityPurchaseRestrictions(userInfo, limitVo);
        return JsonResult.success(null, "success!");
    }

}
