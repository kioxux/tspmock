
package com.gys.business.controller;

import com.gys.business.service.SalesReceiptsService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@Api(tags = "销售退货明细")
@Slf4j
@RestController
@RequestMapping({"/salesInquire/"})
public class SalesInquireController extends BaseController {

    @Autowired
    private SalesReceiptsService salesReceiptsService;


    @ApiOperation(value = "获取当前加盟商下医生")
    @PostMapping({"queryDoctorByClient"})
    public JsonResult queryDoctorByClient(HttpServletRequest request,@RequestBody Map map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        map.put("client",userInfo.getClient());
        return JsonResult.success(this.salesReceiptsService.queryDoctorByClient(map), "提示：获取数据成功！");
    }


}
