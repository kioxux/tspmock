package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.gys.business.mapper.entity.GaiaFiArPayment;
import com.gys.business.service.GaiaBillOfArService;
import com.gys.common.annotation.FrequencyValid;
import com.gys.common.base.BaseController;
import com.gys.common.data.GaiaBillOfArInData;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.response.Result;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 明细清单表(GaiaBillOfAr)表控制层
 *
 * @author XIAOZY
 * @since 2021-09-18 15:13:24
 */
@RestController
@RequestMapping("gaiaBillOfAr")
@Slf4j
public class GaiaBillOfArController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaBillOfArService gaiaBillOfArService;

    @ApiOperation(value = "新增应收明细" )
    @FrequencyValid
    @PostMapping({"insertBill"})
    public JsonResult insertBill(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<应收明细><应收明细列表-保存><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.insertBill(loginUser,inData), "提示：保存数据成功！");
    }

    @ApiOperation(value = "批量新增应收明细" )
    @FrequencyValid
    @PostMapping({"insertBillList"})
    public JsonResult insertBillList(HttpServletRequest request, @RequestBody List<GaiaBillOfArInData> inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        log.info(String.format("<应收明细><应收明细列表-批量保存><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.insertBillList(loginUser,inData), "提示：保存数据成功！");
    }

    @ApiOperation(value = "应收明细列表-查询" )
    @PostMapping({"listBill"})
    public JsonResult listBill(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<应收明细><应收明细列表-查询><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.listBill(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "查询销售，销退单据" )
    @PostMapping({"listSalesBill"})
    public JsonResult listSalesBill(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<应收明细><查询销售，销退单据><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.listSalesBill(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "批发核销及明细页面-保存")
    @PostMapping({"updateBillHXStatus"})
    public JsonResult updateBillHXStatus(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        log.info(String.format("<应收明细><批发核销及明细页面-保存><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.updateBillHXStatus(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "批发应收明细-查询")
    @PostMapping({"listReceivableDetail"})
    public JsonResult listReceivableDetail(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<应收明细><批发应收明细-查询><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.listReceivableDetail(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "批发管理列表-导出")
    @RequestMapping(value = "/exportBill")
    public Result exportBill(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        log.info(String.format("<应收明细><批发管理列表-导出><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return this.gaiaBillOfArService.exportBill(userInfo,inData);
    }

    @ApiOperation(value = "批发应收明细-导出")
    @PostMapping({"exportReceivableDetail"})
    public Result exportReceivableDetail(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<应收明细><批发应收明细-导出><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return this.gaiaBillOfArService.exportReceivableDetail(loginUser, inData);
    }

    @ApiOperation(value = "批发管理列表-导入")
    @PostMapping("/importBill")
    public JsonResult importBill(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        GetLoginOutData userInfo = getLoginUser(request);
        return JsonResult.success(gaiaBillOfArService.importBill(file, userInfo), "提示：导入数据成功！");
    }

    @ApiOperation(value = "批发应收汇总-查询")
    @PostMapping("/listReport")
    public JsonResult listReport(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<应收明细><批发应收汇总-查询><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.listReport(loginUser,inData), "提示：查询数据成功！");
    }
    @ApiOperation(value = "批发应收汇总-导出")
    @PostMapping({"exportListReport"})
    public void exportListReport(HttpServletRequest request,HttpServletResponse response, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<应收明细><批发应收汇总-导出><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        this.gaiaBillOfArService.exportListReport(loginUser,response ,inData);
    }

    @ApiOperation(value = "查询单据类型字典")
    @PostMapping({"listBillType"})
    public JsonResult listBillType(HttpServletRequest request, @RequestBody GaiaFiArPayment inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<应收明细><查询单据类型字典><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.listBillType(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "初始化单据类型")
    @PostMapping({"insertGaiaFiArPayment"})
    public JsonResult insertGaiaFiArPayment(HttpServletRequest request, @RequestBody GaiaFiArPayment inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        inData.setClient(loginUser.getClient());
        log.info(String.format("<应收明细><初始化单据类型><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.insertGaiaFiArPayment(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "批发应收汇总-删除")
    @PostMapping({"deleteArBill"})
    public JsonResult deleteArBill(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        log.info(String.format("<应收明细><批发应收汇总-删除><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.deleteArBill(loginUser,inData), "提示：查询数据成功！");
    }

    @ApiOperation(value = "批发应收汇总-根据日期生成应收")
    @PostMapping({"insertArBill"})
    public JsonResult insertArBill(HttpServletRequest request, @RequestBody GaiaBillOfArInData inData) {
        GetLoginOutData loginUser = getLoginUser(request);
        log.info(String.format("<应收明细><批发应收汇总-根据日期生成应收><请求参数：%s>", JSONUtil.toJsonStr(inData)));
        return JsonResult.success(this.gaiaBillOfArService.insertArBill(loginUser,inData), "提示：添加数据成功！");
    }
    @ApiOperation(value = "获取开票员")
    @GetMapping({"selectSoDnByList"})
    public JsonResult selectSoDnByList(HttpServletRequest request,String soDnByName){
        GetLoginOutData loginUser = getLoginUser(request);
        return JsonResult.success(this.gaiaBillOfArService.selectSoDnByList(loginUser,soDnByName), "提示：添加数据成功！");
    }
}
