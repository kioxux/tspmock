//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.controller;

import com.gys.business.service.RemoteAuditService;
import com.gys.business.service.data.RemoteAuditHInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/remoteAudit/"})
public class RemoteAuditController extends BaseController {
    @Autowired
    private RemoteAuditService remoteAuditService;

    public RemoteAuditController() {
    }

    @PostMapping({"/addRemoteAudit"})
    public JsonResult addRemoteAudit(HttpServletRequest request, @RequestBody RemoteAuditHInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.remoteAuditService.insertRemoteAudit(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }
}
