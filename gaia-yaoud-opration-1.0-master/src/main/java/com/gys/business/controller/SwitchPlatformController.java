package com.gys.business.controller;

import com.gys.business.service.SwitchPlatformService;
import com.gys.business.service.data.GaiaSdMemberData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 平台切换
 *
 * @author xiaoyuan on 2020/12/2
 */
@Slf4j
@RestController
@RequestMapping({"/switchPlatform"})
public class SwitchPlatformController extends BaseController {

    @Autowired
    private SwitchPlatformService platformService;


    @PostMapping({"/getTableName"})
    public JsonResult getTableName(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.platformService.getTableName(userInfo), "success");
    }


    @PostMapping({"/createTableStructure"})
    public JsonResult createTableStructure(HttpServletRequest request, @RequestBody GaiaSdMemberData vo) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.platformService.createTableStructure(userInfo,vo);
        return JsonResult.success("", "success");
    }
}
