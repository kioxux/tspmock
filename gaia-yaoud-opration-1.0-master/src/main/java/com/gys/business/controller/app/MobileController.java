package com.gys.business.controller.app;

import com.gys.business.service.appservice.MobileService;
import com.gys.business.service.data.GaiaStoreOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author xiaoyuan on 2021/3/9
 */
@Api(tags = "app门店选择接口")
@RestController("appStore")
@RequestMapping({"/app/store/"})
public class MobileController extends BaseController {

    @Autowired
    private MobileService mobileService;


    @PostMapping({"getStoreList"})
    @ApiOperation(value = "门店列表" ,response = GaiaStoreOutData.class)
    public JsonResult getStoreList(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.mobileService.getStoreList(userInfo), "提示：获取数据成功！");
    }

}
