package com.gys.business.controller;

import com.gys.business.service.ServerService;
import com.gys.business.service.data.GetServerInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/server/"})
public class ServerController extends BaseController {
    @Resource
    private ServerService serverService;

    public ServerController() {
    }

    @PostMapping({"detail"})
    public JsonResult detail(HttpServletRequest request, @RequestBody GetServerInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.serverService.detail(inData), "提示：获取数据成功！");
    }

    @PostMapping({"save"})
    public JsonResult save(HttpServletRequest request, @RequestBody GetServerInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        this.serverService.save(inData);
        return JsonResult.success(null, "提示：获取数据成功！");
    }
}
