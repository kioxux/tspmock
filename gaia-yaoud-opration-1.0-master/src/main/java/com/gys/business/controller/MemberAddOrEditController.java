
package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdMemberArea;
import com.gys.business.service.MemberAddOrEditService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.MemberAddDto;
import com.gys.common.response.Result;
import com.gys.common.vo.MemberClassVo;
import com.gys.feign.ElectronicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Api(tags = "会员新增修改")
@RestController
@RequestMapping({"/memberAddOrEdit/"})
public class MemberAddOrEditController extends BaseController {
    @Autowired
    private MemberAddOrEditService memberAddOrEditService;
    @Autowired
    private ElectronicService electronicService;
    @Autowired
    private HttpServletRequest request;

    @ApiOperation(value = "条件查询/列表查询(弃用)")
    @PostMapping({"findMembersByPage"})
    public JsonResult findMembersByPage(HttpServletRequest request,@Valid @RequestBody GetMemberInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        return JsonResult.success(this.memberAddOrEditService.findMembersByPage(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "条件查询/列表查询(会员组)")
    @PostMapping({"findMemberListByPage"})
    public JsonResult findMemberListByPage(@RequestBody MemberListInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return memberAddOrEditService.findMemberListByPage(inData);
    }

    @ApiOperation(value = "会员资料修改查询(新)", response = MemberListOutData.class)
    @PostMapping({"findMemberListByCondition"})
    public JsonResult findMemberListByCondition(@RequestBody MemberConditionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return memberAddOrEditService.findMemberListByCondition(inData);
    }

    @ApiOperation(value = "导出会员信息")
    @PostMapping({"findMemberListByConditionExport"})
    public Result findMemberListByConditionExport(@RequestBody MemberConditionInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());

        return memberAddOrEditService.findMemberListByConditionExport(inData);
    }

    @ApiOperation(value = "导出会员信息")
    @PostMapping({"exportMemberListByPage"})
    public Result exportMemberListByPage(@RequestBody MemberListInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return memberAddOrEditService.exportMemberListByPage(inData);
    }

    @ApiOperation(value = "会员条件修改")
    @PostMapping({"editMember"})
    public JsonResult editMember(HttpServletRequest request,@Valid @RequestBody GetMemberInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setSaler(userInfo.getUserId());
        inData.setUpdateEmp(userInfo.getUserId());
        this.memberAddOrEditService.editMember(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

    @ApiOperation(value = "会员新增")
    @PostMapping({"addMember"})
    public JsonResult addMember(HttpServletRequest request,@Valid @RequestBody GetMemberInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setSaler(userInfo.getUserId());
        MemberAddDto memberAddDto = memberAddOrEditService.addMember(inData, userInfo);
        //调用注册送券服务
        String result = electronicService.addRegisterElectron(inData.getClientId(),inData.getGmbCardId());
        return JsonResult.success(memberAddDto, "提示：会员新增成功！");
//        if(this.memberAddOrEditService.addMember(inData, userInfo)){
//            //调用注册送券服务
//            String result = electronicService.addRegisterElectron(inData.getClientId(),inData.getGmbCardId());
//            return JsonResult.success("", "提示：会员新增成功！");
//        }else{
//            return JsonResult.success("", "提示：会员新增失败！");
//        }
    }

    @PostMapping({"findMemberByPrimary"})
    public JsonResult findMemberByPrimary(@RequestBody GetMemberInData inData) {
        return JsonResult.success(this.memberAddOrEditService.findMemberByPrimary(inData), "提示：获取数据成功！");
    }

    /***
     * 根据前缀 获取最大会员号 默认VM开头
     * @param request
     * @param memberCardNo
     * @return
     */
    @PostMapping({"getIncrMemberCard"})
    public JsonResult getIncrMemberCard(HttpServletRequest request,@RequestBody MemberCardNoInData memberCardNo){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.memberAddOrEditService.getIncrMemberCard(memberCardNo.getPrefix(), userInfo), "提示：获取数据成功！");
    }

    //会员类型添加
    @ApiOperation(value = "会员新增")
    @PostMapping({"typeSet"})
    public JsonResult addMemberType(HttpServletRequest request,@RequestBody MemberClassVo lists) { //List<MemberClass> lists
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.memberAddOrEditService.addMemberClass(userInfo,lists);
        return JsonResult.success("", "保存成功！");
    }

    @ApiOperation(value = "会员配置参数查询")
    @PostMapping({"listVIPParam"})
    public JsonResult listVIPParam(HttpServletRequest request,@RequestBody MemberClassVo param) { //List<MemberClass> lists
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.memberAddOrEditService.listVIPParam(userInfo,param), "提示：获取数据成功！");
    }


    @ApiOperation(value = "查询同连锁同会员同天销售含麻商品数量")
    @PostMapping({"getControlProNumByMember"})
    public JsonResult getControlProNumByMember(HttpServletRequest request,@RequestBody GetMemberInData param) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClientId(userInfo.getClient());
        return JsonResult.success(this.memberAddOrEditService.getControlProNumByMember(param), "提示：获取数据成功！");
    }

    @ApiOperation(value = "会员付费激活")
    @PostMapping({"activateMemberCard"})
    public JsonResult activateMemberCard(HttpServletRequest request,@Valid @RequestBody GetMemberInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setSaler(userInfo.getUserId());
        inData.setUpdateEmp(userInfo.getUserId());
        this.memberAddOrEditService.activateMemberCard(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

    @ApiOperation(value = "获取会员卡状态")
    @GetMapping("getMemberCardStatus")
    public JsonResult getMemberCardStatus() {
        return JsonResult.success(memberAddOrEditService.getMemberCardStatus(), "提示：获取成功！");
    }

    @ApiOperation(value = "获取会员卡解挂信息")
    @GetMapping("getUncouplingMemberCardInfo")
    public JsonResult getUncouplingMemberCardInfo(HttpServletRequest request
            , @ApiParam("会员卡号") @RequestParam String cardId) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(memberAddOrEditService.getUncouplingMemberCardInfo(userInfo,cardId), "提示：获取成功！");
    }

    @ApiOperation(value = "解挂会员卡")
    @PostMapping("uncouplingMemberCard")
    public JsonResult uncouplingMemberCard(HttpServletRequest request
            , @ApiParam("会员卡号") @RequestParam String cardId
            , @ApiParam("备注") @RequestParam String remark) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        remark = StringUtils.isEmpty(remark) ? "" : remark;
        memberAddOrEditService.uncouplingMemberCard(userInfo, cardId,remark);
        return JsonResult.success("提示：解挂成功！");
    }


    @ApiOperation(value = "会员卡挂失")
    @PostMapping({"cardLose"})
    public JsonResult cardLose(@RequestBody MemberCardLoseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setEmpId(userInfo.getUserId());
        return memberAddOrEditService.cardLose(inData);
    }


    @ApiOperation(value = "会员卡换卡")
    @PostMapping({"cardChange"})
    public JsonResult cardChange(@RequestBody MemberCardLoseInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setBrId(userInfo.getDepId());
        inData.setEmpId(userInfo.getUserId());
        return memberAddOrEditService.cardChange(inData);
    }


    @ApiOperation(value = "获取会员商圈")
    @PostMapping({"getMemberAreaList"})
    public JsonResult getMemberAreaList() {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return memberAddOrEditService.getMemberAreaList(userInfo);
    }

    @ApiOperation(value = "会员卡积分修改")
    @PostMapping({"editMemberIntegral"})
    public JsonResult editMemberIntegral(HttpServletRequest request, @RequestBody MemberIntegralInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUpdateEmp(userInfo.getUserId());
        this.memberAddOrEditService.editMemberIntegral(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

    @ApiOperation(value = "会员卡注销")
    @PostMapping({"cancelMemberCard"})
    public JsonResult cancelMemberCard(HttpServletRequest request, @RequestBody MemberCardCancelInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setDepId(userInfo.getDepId());
        inData.setEmpId(userInfo.getUserId());
        this.memberAddOrEditService.cancelMemberCard(inData);
        return JsonResult.success("", "提示：更新成功！");
    }

}
