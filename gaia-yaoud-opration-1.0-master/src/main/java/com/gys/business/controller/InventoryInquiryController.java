package com.gys.business.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.service.InventoryInquiryService;
import com.gys.business.service.StoreDataService;
import com.gys.business.service.data.*;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.ReportForms.InventoryInquiryByClientAndBatchNoOutData;
import com.gys.business.service.data.ReportForms.InventoryInquiryByClientAndSiteOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping({"/inventoryInquiry/"})
@Slf4j
@Api(tags = "库存管理")
public class InventoryInquiryController extends BaseController {
    @Resource
    private InventoryInquiryService inventoryInquiryService;
    @Resource
    private StoreDataService storeDataService;

    @ApiOperation(value = "库存商品查询", response = InventoryInquiryOutData.class)
    @PostMapping({"/inventoryInquiryList"})
    public JsonResult inventoryInquiryList(HttpServletRequest request,@Valid @RequestBody InventoryInquiryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssmBrId(userInfo.getDepId());
        return JsonResult.success(inventoryInquiryService.getInventoryInquiryListWeb(inData), "提示：获取数据成功！");
    }


    @ApiOperation(value = "库存商品查询", response = InventoryInquiryByClientAndSiteOutData.class)
    @PostMapping({"/inventoryInquiryListByStoreAndDc"})
    public JsonResult inventoryInquiryListByStore(HttpServletRequest request,@Valid @RequestBody InventoryInquiryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(inventoryInquiryService.inventoryInquiryListByStoreAndDc(inData), "提示：获取数据成功！");
    }
    @ApiOperation(value = "库存商品查询", response = InventoryInquiryByClientAndSiteOutData.class)
    @PostMapping({"/inventoryInquiryListByStoreAndDc/export"})
    public void inventoryInquiryListByStoreExport(HttpServletRequest request,HttpServletResponse response,@Valid @RequestBody InventoryInquiryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        try {
            inventoryInquiryService.inventoryInquiryListByStoreExport(inData,response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @ApiOperation(value = "效期预警查询", response = InventoryInquiryOutData.class)
    @PostMapping({"/expiryDateWarning"})
    public JsonResult effectiveEarlyWarning(HttpServletRequest request,@Valid @RequestBody InventoryInquiryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssmBrId(userInfo.getDepId());
        return JsonResult.success(inventoryInquiryService.getEexpiryDateWarning(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "效期预警查询", response = InventoryInquiryOutData.class)
    @PostMapping({"/effectiveEarlyWarningAll"})
    public JsonResult effectiveEarlyWarningAll(HttpServletRequest request,@Valid @RequestBody InventoryInquiryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssmBrId(userInfo.getDepId());
        return JsonResult.success(inventoryInquiryService.getEexpiryDateWarningAll(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "效期预警导出")
    @PostMapping({"/getEexpiryDateWarningExport"})
    public JsonResult getEexpiryDateWarningExport(HttpServletRequest request, HttpServletResponse response, @RequestBody InventoryInquiryInData inData)  {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setGssmBrId(userInfo.getDepId());

        try {
            inventoryInquiryService.getEexpiryDateWarningExport(inData,response);
        } catch (Exception var5) {
            throw new BusinessException(var5.toString());
        }
        return JsonResult.success("", "提示：获取数据成功！");
    }

    @ApiOperation(value = "加盟商下库存商品查询", response = InventoryInquiryByClientAndBatchNoOutData.class)
    @PostMapping({"/inventoryInquiryListByClient"})
    public JsonResult inventoryInquiryListByClient(HttpServletRequest request,@Valid @RequestBody InventoryInquiryInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(inventoryInquiryService.inventoryInquiryListByClient(inData), "提示：获取数据成功！");
    }

    @ApiOperation(value = "获取库存门店")
    @PostMapping({"/getInventoryStore"})
    public JsonResult getInventoryStore(HttpServletRequest request) {
        InData inData = new InData();
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(storeDataService.getInventoryStore(inData), "提示：获取数据成功！");
    }


    @ApiOperation(value = "搜索条件",response = InventoryInquiryByClientAndSiteOutData.class)
    @PostMapping({"searchBox"})
    public JsonResult searchBox(HttpServletRequest request,@RequestBody InventoryInquiryInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return inventoryInquiryService.searchBox(inData);
    }

    @ApiOperation(value = "查询条件",response = InventoryInquiryByClientAndSiteOutData.class)
    @PostMapping({"search"})
    public JsonResult search(HttpServletRequest request,@RequestBody InventoryInquiryInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return inventoryInquiryService.search(inData);
    }

    @ApiOperation(value = "自定义条件",response = InventoryInquiryByClientAndSiteOutData.class)
    @PostMapping({"zdyBox"})
    public JsonResult zdyBox(HttpServletRequest request,@RequestBody InventoryInquiryInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return inventoryInquiryService.zdyBox(inData);
    }

    @ApiOperation(value = "自定义名称匹配",response = ZDYNameOutData.class)
    @PostMapping({"zdyName"})
    public JsonResult zdyName(HttpServletRequest request,@RequestBody InventoryInquiryInData inData){
        log.info("inData: {}",  JSON.toJSONString(inData));
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return JsonResult.success(this.inventoryInquiryService.zdyName(inData),"提示：获取数据成功！");
    }


}
