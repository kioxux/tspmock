package com.gys.business.controller;

import cn.hutool.json.JSONUtil;
import com.github.pagehelper.util.StringUtil;
import com.gys.business.controller.app.form.DeleteForm;
import com.gys.business.controller.app.form.OperateForm;
import com.gys.business.controller.app.form.OutStockForm;
import com.gys.business.controller.app.form.PushForm;
import com.gys.business.controller.app.vo.CompanyOutStockVO;
import com.gys.business.controller.app.vo.DcPushVO;
import com.gys.business.service.CompanyOutOfStockService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Api(tags = "公司级缺断货相关接口")
@RestController
@RequestMapping("/companyOutOfStock")
public class CompanyOutOfStockController extends BaseController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private CompanyOutOfStockService companyOutOfStockService;

    @ApiOperation(value = "效期商品")
    @PostMapping("/getValidProduct")
    public JsonResult getValidProduct(@RequestBody ValidProductInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return companyOutOfStockService.getValidProduct(inData);
    }

    @ApiOperation(value = "效期商品-门店明细")
    @PostMapping("/getValidProductByStore")
    public JsonResult getValidProductByStore(@RequestBody CompanyExpiryProChartInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return companyOutOfStockService.getValidProductChart(inData);
    }


    @ApiOperation(value = "库存报表")
    @PostMapping("/getStockReport")
    public JsonResult getStockReport(@RequestBody CompanyStoreReportInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return companyOutOfStockService.getStockReport(inData);
    }

    @ApiOperation(value = "缺断货情况查询-无配送中心", response = CompanyOutOfStockInfoOutData.class)
    @PostMapping("/getOutOfStockExplain")
    public JsonResult getOutOfStockExplain(@RequestBody OutOfStockInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return companyOutOfStockService.getCompanyOutOfStock(inData);
    }

    @ApiOperation(value = "缺断货推送门店设置")
    @PostMapping("/pushStoreOutOfStock")
    public JsonResult pushStoreOutOfStock(@RequestBody UpdateParaByStoreListInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return companyOutOfStockService.pushStoreOutOfStock(inData);
    }


    @ApiOperation(value = "缺断货推送门店设置-多选")
    @PostMapping("/choiceOutOfStockPushStore")
    public JsonResult outOfStockChoicePushStore(@RequestBody UpdateParaByStoreListInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return companyOutOfStockService.choicePushStoreOutOfStock(inData);
    }

    @ApiOperation(value = "缺断货推送历史查询-门店")
    @PostMapping("/pushStoreHistory")
    public JsonResult pushStoreHistory(@RequestBody PushStoreHistoryInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return companyOutOfStockService.getPushStoreHistory(inData);
    }

    @ApiOperation(value = "缺断货推送历史查询-仓库")
    @PostMapping("/pushWmsHistory")
    public JsonResult pushWmsHistory(@RequestBody PushStoreHistoryInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return companyOutOfStockService.getPushWmsHistory(inData);
    }

    @ApiOperation(value = "缺断货商品列表（配送中心）", response = CompanyOutStockVO.class)
    @PostMapping("/dc/productList")
    public JsonResult<CompanyOutStockVO> productList(HttpServletRequest request, @RequestBody OutStockForm outStockForm) {
        GetLoginOutData loginUser = getLoginUser(request);
        if (loginUser != null) {
            outStockForm.setClient(loginUser.getClient());
            outStockForm.setUserId(loginUser.getUserId());
        }
        validOutStockForm(outStockForm);
        CompanyOutStockVO companyOutStockVO = null;
        try {
            companyOutStockVO = companyOutOfStockService.getOutOfStockList(outStockForm);
        } catch (Exception e) {
            log.error("<公司级缺断货><商品列表><查询异常:{}>", e.getMessage(), e);
        }
        return JsonResult.success(companyOutStockVO, "查询成功");
    }

    private void validOutStockForm(OutStockForm outStockForm) {
        if (outStockForm.getQueryNum() == null) {
            //查询全部
            outStockForm.setQueryNum(1000);
        }
        if (outStockForm.getSortCond() == null) {
            outStockForm.setSortCond(1);
        }
        if (outStockForm.getSortType() == null) {
            outStockForm.setSortType(1);
        }
    }

    @ApiOperation(value = "淘汰商品（配送中心）")
    @PostMapping("/dc/delete")
    public JsonResult dcDelete(HttpServletRequest request, @RequestBody DeleteForm deleteForm) {
        try {
            GetLoginOutData loginUser = getLoginUser(request);
            deleteForm.setClient(loginUser.getClient());
            deleteForm.setUserId(loginUser.getUserId());
            deleteForm.setLoginName(loginUser.getLoginName());
            companyOutOfStockService.deleteProduct(deleteForm);
        } catch (Exception e) {
            if (e instanceof BusinessException) {
                return JsonResult.fail(500, e.getMessage());
            }
        }
        return JsonResult.success(null, "操作成功");
    }

    @ApiOperation(value = "门店明细（配送中心）", response = CompanyOutOfStockInfoOutData.class)
    @PostMapping("/dc/storeDetail")
    public JsonResult dcStoreDetail(HttpServletRequest request, @RequestBody OperateForm operateForm) {
        log.info(String.format("<公司缺断货><门店明细(DC)><请求参数：%s>", JSONUtil.toJsonStr(operateForm)));
        GetLoginOutData loginUser = getLoginUser(request);
        operateForm.setClient(loginUser.getClient());
        validStoreFrom(operateForm);
        CompanyOutOfStockInfoOutData dcStoreDetailList = companyOutOfStockService.getDcStoreDetailList(operateForm);
        return JsonResult.success(dcStoreDetailList, "操作成功");
    }

    @ApiOperation(value = "历史数据（配送中心）")
    @PostMapping("/dc/historyData")
    public JsonResult historyData(HttpServletRequest request, @RequestBody OperateForm operateForm) {
        log.info(String.format("<公司缺断货><门店明细(DC)><请求参数：%s>", JSONUtil.toJsonStr(operateForm)));

        return JsonResult.success(null, "操作成功");
    }

    @ApiOperation(value = "推送操作（配送中心）", response = DcPushVO.class)
    @PostMapping("/dc/push")
    public JsonResult<DcPushVO> dcPush(HttpServletRequest request, @RequestBody PushForm pushForm) {
        log.info(String.format("<公司缺断货><推送操作(DC)><请求参数：%s>", JSONUtil.toJsonStr(pushForm)));
        if (pushForm.getProSelfCodeList() == null || pushForm.getProSelfCodeList().size() == 0) {
            return JsonResult.fail(500, "推送商品不能为空");
        }
        if (StringUtil.isEmpty(pushForm.getVoucherId())) {
            return JsonResult.fail(500, "voucherId不能为空");
        }
        GetLoginOutData loginUser = getLoginUser(request);
        pushForm.setClient(loginUser.getClient());
        pushForm.setUserId(loginUser.getUserId());
        pushForm.setLoginName(loginUser.getLoginName());
        //生成仓库紧急补货需求
        DcPushVO pushVO = companyOutOfStockService.push(pushForm);
        return JsonResult.success(pushVO, "操作成功");
    }

    @ApiOperation(value = "仓库紧急补货列表", response = DcReplenishVO.class)
    @PostMapping("replenishList")
    public JsonResult<List<DcReplenishVO>> dcReplenishList(HttpServletRequest request, @RequestBody DcReplenishForm replenishForm) {
        GetLoginOutData loginUser = getLoginUser(request);
        replenishForm.setClient(loginUser.getClient());
        List<DcReplenishVO> list = companyOutOfStockService.replenishList(replenishForm);
        return JsonResult.success(list, "查询成功");
    }

    @ApiOperation(value = "仓库紧急补货详情", response = DcReplenishDetailVO.class)
    @PostMapping("replenishDetail")
    public JsonResult<List<DcReplenishDetailVO>> dcReplenishDetail(HttpServletRequest request, @RequestBody DcReplenishForm replenishForm) {
        if (StringUtil.isEmpty(replenishForm.getVoucherId())) {
            return JsonResult.error("紧急补货需求单号不能为空");
        }
        GetLoginOutData loginUser = getLoginUser(request);
        replenishForm.setClient(loginUser.getClient());
        List<DcReplenishDetailVO> list = companyOutOfStockService.replenishDetail(replenishForm);
        return JsonResult.success(list, "查询成功");
    }

    private void validStoreFrom(OperateForm operateForm) {
        if (operateForm.getSortMode() == null) {
            operateForm.setSortMode(1);
        }
        if (operateForm.getQueryNum() == null) {
            operateForm.setQueryNum(20);
        }
        if (operateForm.getSortCond() == null) {
            operateForm.setSortCond(1);
        }
        if (operateForm.getSortType() == null) {
            operateForm.setSortType(1);
        }
    }


    @ApiOperation(value = "库存报表-中类")
    @PostMapping("/getMidStockReport")
    public JsonResult getMidStockReport(@RequestBody ProductMidTypeSaleInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClientId(userInfo.getClient());
        return companyOutOfStockService.getProductMidTypeReport(inData);
    }


    @ApiOperation(value = "库存报表-门店明细")
    @PostMapping("/getStoreStockListReport")
    public JsonResult getStoreStockListReport(@RequestBody AppStoreSaleInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return companyOutOfStockService.getStoreStockListReport(inData);
    }
}
