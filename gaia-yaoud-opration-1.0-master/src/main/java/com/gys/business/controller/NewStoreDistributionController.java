package com.gys.business.controller;

import com.gys.business.service.NewStoreDistributionService;
import com.gys.business.service.data.NewStoreDistributionBaseInData;
import com.gys.business.service.data.NewStoreDistributionInData;
import com.gys.business.service.data.NewStoreDistributionRecordDInData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author SunJiaNan
 * @version 1.0
 * @date 2021/10/30 13:54
 * @description 门店必备商品业务层
 */
@Api(tags = "新店铺货")
@RestController
@RequestMapping("/newStoreDistribution")
public class NewStoreDistributionController extends BaseController {

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private NewStoreDistributionService newStoreDistributionService;

    @ApiOperation(value = "查询枚举基本信息")
    @PostMapping("/getBaseInfo")
    public JsonResult getBaseInfo(@RequestBody NewStoreDistributionBaseInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return newStoreDistributionService.getBaseInfo(inData);
    }


    @ApiOperation(value = "铺货计算")
    @PostMapping("/getCalculationList")
    public JsonResult getCalculationList(@RequestBody NewStoreDistributionBaseInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUserId(userInfo.getUserId());
        return newStoreDistributionService.getCalculationList(inData);
    }

    
    @ApiOperation(value = "导出计算结果")
    @PostMapping("/export")
    public JsonResult export(@RequestBody NewStoreDistributionRecordDInData inData) {
        GetLoginOutData userInfo = super.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return newStoreDistributionService.export(inData);
    }
}
