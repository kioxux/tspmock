package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSaletaskHplan;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.MonthService;
import com.gys.business.service.SaleTaskService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping({"/saleTask/"})
@Api(tags = "销售计划", description = "/saleTask")
public class SaleTaskController extends BaseController {
    @Autowired
    private SaleTaskService saleTaskService;

    @Autowired
    private MonthService monthService;

    @PostMapping({"insertH"})
    @ApiOperation(value = "新增销售计划" ,response = SaleTaskHplan.class)
    public JsonResult  insertH(HttpServletRequest request,@RequestBody SaleTaskInData saletask){
        GetLoginOutData userInfo = this.getLoginUser(request);
        saletask.setClient(userInfo.getClient());
        return JsonResult.success( this.saleTaskService.insertH(userInfo,saletask),"提示：获取数据成功！");
    }

    @PostMapping({"insertD"})
    @ApiOperation(value = "新增销售计划" ,response = SaleTaskOutData.class)
    public JsonResult  insertD(HttpServletRequest request,@RequestBody SaleTaskInData saletask){
        GetLoginOutData userInfo = this.getLoginUser(request);
        saletask.setClient(userInfo.getClient());
        this.saleTaskService.insertD(userInfo,saletask,null);
        return JsonResult.success("","提示：获取数据成功！");
    }

    @PostMapping({"selectById"})
    @ApiOperation(value = "查询销售计划" ,response = SaleTaskOutData.class)
    public JsonResult  selectById(HttpServletRequest request,@RequestBody SaleTaskInData saletask){
        GetLoginOutData userInfo = this.getLoginUser(request);
        saletask.setClient(userInfo.getClient());;
        return JsonResult.success( this.saleTaskService.selectById(saletask),"提示：获取数据成功！");
    }


    @PostMapping({"selectPage"})
    @ApiOperation(value = "销售计划表格" ,response = SaleTaskOutData.class)
    public JsonResult  selectPage(HttpServletRequest request,@RequestBody SaleTaskInData saletask){
        GetLoginOutData userInfo = this.getLoginUser(request);
        saletask.setClient(userInfo.getClient());
        return JsonResult.success( this.saleTaskService.selectPage(saletask) ,"提示：获取数据成功！");
    }

    //月销售任务查询
    @PostMapping({"selectSalePlan"})
    @ApiOperation(value = "月销售任务查询",response = MonthOutData.class)
    public JsonResult selectSalePlan(HttpServletRequest request,@RequestBody MonthOutData monthOutData){
//        GetLoginOutData userinfo = this.getLoginUser(request);
        return JsonResult.success(this.monthService.selectPage(monthOutData),"提示：获取数据成功！");
    }

    //月销售任务新增
    @PostMapping({"insertMonthlySalesPlan"})
    @ApiOperation(value = "月销售任务新增" ,response = MonthOutData.class)
    public JsonResult  insertMonthlySalesPlan(HttpServletRequest request,@RequestBody @Valid MonthOutData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success( this.monthService.insertMonthlySalesPlan(userInfo,inData),"提示：操作成功！");
    }

    //月销售任务修改
    @PostMapping({"modifyMonthlySalesPlan"})
    @ApiOperation(value = "修改月销售任务计划" ,response = MonthOutData.class)
    public JsonResult  modifyMonthlySalesPlan(HttpServletRequest request,@RequestBody @Valid MonthOutData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        this.monthService.modifyMonthlySalesPlan(userInfo,inData);
        return JsonResult.success("" ,"提示：操作成功！");
    }

    //月销售任务删除
    @PostMapping({"deleteMonthlySalesPlan"})
    @ApiOperation(value = "删除月销售任务计划" ,response = MonthOutData.class)
    public JsonResult  deleteMonthlySalesPlan(HttpServletRequest request,@RequestBody MonthOutData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        this.monthService.deleteMonthlySalesPlan(inData);
        return JsonResult.success( "","提示：操作成功！");
    }

//    @PostMapping({"edit"})
//    @ApiOperation(value = "修改销售计划" ,response = SaleTaskOutData.class)
//    public JsonResult  edit(HttpServletRequest request,@RequestBody SaleTaskInData saletask){
//        GetLoginOutData userInfo = this.getLoginUser(request);
//        saletask.setClient(userInfo.getClient());
//        this.saleTaskService.edit(userInfo,saletask);
//        return JsonResult.success( "" ,"提示：获取数据成功！");
//    }
    @PostMapping({"getStoreBySaleTask"})
    @ApiOperation(value = "查询门店" ,response = SaleTaskStroeOutData.class)
    public JsonResult  getStoreBySaleTask(HttpServletRequest request, @RequestBody GaiaStoreData storeData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        storeData.setClient(userInfo.getClient());;
        return JsonResult.success( this.saleTaskService.getStoreBySaleTask(storeData) ,"提示：获取数据成功！");
    }


    @PostMapping({"delect"})
    @ApiOperation(value = "删除销售计划" ,response = SaleTaskOutData.class)
    public JsonResult  delect(HttpServletRequest request,@RequestBody SaleTaskInData saletask){
        GetLoginOutData userInfo = this.getLoginUser(request);
        saletask.setClient(userInfo.getClient());
        this.saleTaskService.delect(userInfo,saletask);
        return JsonResult.success( "" ,"提示：获取数据成功！");
    }


    @ApiOperation(value = "销售计划导入",response = GetReplenishDetailOutData.class)
    @PostMapping({"getImportExcelSaleTask"})
    public JsonResult getImportExcelDetailList(HttpServletRequest request,@RequestParam("file") MultipartFile file,@RequestParam("monthPlan") String monthPlan,@RequestParam("daysPlan") String daysPlan) {
        SaleTaskInData inData = new SaleTaskInData();
        GetLoginOutData userInfo = this.getLoginUser(request);

        try {
//            String rootPath = request.getSession().getServletContext().getRealPath("/");
//            String fileName = file.getOriginalFilename();
//            String path = rootPath + fileName;
//            File newFile = new File(path);
//            //判断文件是否存在
//            if(!newFile.exists()) {
//                newFile.mkdirs();//不存在的话，就开辟一个空间
//            }
//            //将上传的文件存储
//            file.transferTo(newFile);
            InputStream in = file.getInputStream();
            //导入已存在的Excel文件，获得只读的工作薄对象
            XSSFWorkbook wk = new XSSFWorkbook(in);
            //获取第一张Sheet表
            Sheet sheet = wk.getSheetAt(0);
            //取数据
            List<Saletask> importInDataList = getRowAndCell(sheet);
            inData.setSaletasks(importInDataList);
            in.close();
            wk.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        inData.setClient(userInfo.getClient());
        inData.setMonthPlan(monthPlan);
        inData.setDaysPlan(Integer.valueOf(daysPlan));
        this.saleTaskService.getImportExcelSaleTask(inData, userInfo);
        return JsonResult.success("", "提示：获取成功！");
    }

    public List<Saletask> getRowAndCell(Sheet sheet) {
        List<Saletask> importInDataList = new ArrayList<>();
        String msg = null;
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        System.out.println("总行数==" + totalRows);
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            try {
                for (int i = 1; i <= totalRows; i++) {
                Row row = sheet.getRow(i); //取出一行数据放入row
                Saletask newExpert = new Saletask();
//                newExpert.setMonthPlan(dataFormatter.formatCellValue(row.getCell(0)).trim());
                newExpert.setStoCode(dataFormatter.formatCellValue(row.getCell(2)).trim());
                newExpert.setTSaleAmt(new BigDecimal(dataFormatter.formatCellValue(row.getCell(7)).trim()));
                newExpert.setTSaleGross(new BigDecimal(dataFormatter.formatCellValue(row.getCell(8)).trim()));
                newExpert.setTMcardQty(new BigDecimal(dataFormatter.formatCellValue(row.getCell(9)).trim()));
                newExpert.setASaleAmt(new BigDecimal(dataFormatter.formatCellValue(row.getCell(10)).trim()));
                newExpert.setASaleGross(new BigDecimal(dataFormatter.formatCellValue(row.getCell(11)).trim()));
                newExpert.setAMcardQty(new BigDecimal(dataFormatter.formatCellValue(row.getCell(12)).trim()));

                importInDataList.add(newExpert);}
            } catch (Exception e){
                e.printStackTrace();
            }
            //行end
        }else{
            throw new BusinessException("EXCEL导入信息为空");
        }
        return importInDataList;
    }

    @PostMapping({"selectSaleTaskReachPage"})
    @ApiOperation(value = "销售计划达成" ,response = Saletask.class)
    public JsonResult  selectSaleTaskReachPage(HttpServletRequest request,@RequestBody Saletask saletask){
        GetLoginOutData userInfo = this.getLoginUser(request);
        saletask.setClient(userInfo.getClient());
        return JsonResult.success( this.saleTaskService.selectSaleTaskReachPage(saletask) ,"提示：获取数据成功！");
    }

    @PostMapping({"update"})
    @ApiOperation(value = "销售计划达成" )
    public JsonResult  update(HttpServletRequest request,@RequestBody GaiaSaletaskHplan saletask){
        GetLoginOutData userInfo = this.getLoginUser(request);
        saletask.setClient(userInfo.getClient());
        this.saleTaskService.update(saletask);
        return JsonResult.success( "","提示：获取数据成功！");
    }
}
