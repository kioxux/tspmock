package com.gys.business.controller;

import com.gys.business.service.SyncService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/sync/"})
public class SyncController extends BaseController {
    @Resource
    private SyncService syncService;

    public SyncController() {
    }

    @PostMapping({"selectProductInfo"})
    public JsonResult selectProductInfo(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        GetSyncOutData outData = this.syncService.selectProductInfo(userInfo);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"selectStoreInfo"})
    public JsonResult selectStoreInfo(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        GetSyncOutData outData = this.syncService.selectStoreInfo(userInfo);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"selectSaleInfo"})
    public JsonResult selectSaleInfo(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        GetSyncOutData outData = this.syncService.selectSaleInfo(userInfo);
        return JsonResult.success(outData, "提示：获取数据成功！");
    }

    @PostMapping({"uploadSaleInfo"})
    public JsonResult uploadSaleInfo(HttpServletRequest request, @RequestBody GetSyncSdSaleHInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.syncService.uploadSaleInfo(inData, userInfo);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping({"stockSync"})
    public JsonResult stockSync(HttpServletRequest request, @RequestBody StockSyncData inData) {
        this.syncService.stockSync(inData);
        return JsonResult.success("", "提示：调用成功！");
    }
    @PostMapping({"batchNoAdjust"})
    public JsonResult batchNoAdjustSync(HttpServletRequest request, @RequestBody batchNoAdjustSyncData inData) {
        this.syncService.batchNoAdjustSync(inData);
        return JsonResult.success("", "提示：调用成功！");
    }
    @PostMapping({"memberSync"})
    public JsonResult memberSync(HttpServletRequest request, @RequestBody MemberSyncData inData) {
        this.syncService.memberSync(inData);
        return JsonResult.success("", "提示：调用成功！");
    }
    @PostMapping({"proPriceSync"})
    public JsonResult proPriceSync(HttpServletRequest request, @RequestBody ProPriceSyncData inData) {
        this.syncService.proPriceSync(inData);
        return JsonResult.success("", "提示：调用成功！");
    }
    @PostMapping({"multipleStoreProPriceSync"})
    public JsonResult multipleStoreProPriceSync(HttpServletRequest request, @RequestBody ProPriceSyncData2 inData) {
        this.syncService.mutilpleStoreProPriceSync(inData);
        return JsonResult.success("", "提示：调用成功！");
    }
    @PostMapping({"supplierSync"})
    public JsonResult supplierSync(HttpServletRequest request, @RequestBody SupplierSyncData inData) {
        this.syncService.supplierSync(inData);
        return JsonResult.success("", "提示：调用成功！");
    }
    @PostMapping({"batchControlSync"})
    public JsonResult batchControlSync(HttpServletRequest request, @RequestBody BatchControlSyncData inData) {
        this.syncService.batchControlSync(inData);
        return JsonResult.success("", "提示：调用成功！");
    }
    @PostMapping({"productSync"})
    public JsonResult productSync(HttpServletRequest request, @RequestBody ProductSyncData inData) {
        this.syncService.productSync(inData);
        return JsonResult.success("", "提示：调用成功！");
    }
    @PostMapping({"multipleStoreProductSync"})
    public JsonResult multipleStoreProductSync(HttpServletRequest request, @RequestBody ProductSyncData2 inData) {
        this.syncService.multipleStoreProductSync(inData);
        return JsonResult.success("", "提示：调用成功！");
    }
}
