package com.gys.business.controller;

import com.gys.business.mapper.entity.GaiaSdStoresGroup;
import com.gys.business.service.StoresGroupService;
import com.gys.business.service.data.StoresGroupInData;
import com.gys.business.service.data.StoresGroupOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "门店店型维护")
@RestController
@RequestMapping({"/storesGroup/"})
@Slf4j
public class StoresGroupController extends BaseController {

    @Autowired
    private StoresGroupService storesGroupService;

    @ApiOperation(value = "查询该加盟商下所有的店型",response = StoresGroupOutData.class)
    @PostMapping("/listAllStoresGroupByClientId")
    public JsonResult selectCampainsProDetails(HttpServletRequest request, StoresGroupInData param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClientId(userInfo.getClient());
        param.setGroupType("DX0001");
        return JsonResult.success(storesGroupService.listAllStoresGroupByClientId(param),"返回成功");
    }

    /**
     * 查询该加盟商下所有的店效级别
     * @param request
     * @param param
     * @return
     */
    @PostMapping("/listAllEffectStoresGroupByClientId")
    public JsonResult listAllEffectStoresGroupByClientId(HttpServletRequest request, StoresGroupInData param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClientId(userInfo.getClient());
        param.setGroupType("DX0002");
        return JsonResult.success(storesGroupService.listAllStoresGroupByClientId(param),"返回成功");
    }

    @ApiOperation(value = "查询该加盟商下所有非单体门店",response = StoresGroupOutData.class)
    @PostMapping("/listAllStoresInfoByClientId")
    public JsonResult listAllStoresInfoByClientId(HttpServletRequest request, StoresGroupInData param){
        GetLoginOutData userInfo = this.getLoginUser(request);
        param.setClientId(userInfo.getClient());
        return JsonResult.success(storesGroupService.listAllStoresInfoByClientId(param),"返回成功");
    }
    @ApiOperation(value = "添加门店店型",response = StoresGroupOutData.class)
    @PostMapping("/insertOrUpdateStoresGroup")
    public JsonResult insertOrUpdateStoresGroup(HttpServletRequest request, @RequestBody List<GaiaSdStoresGroup> groupList){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(storesGroupService.insertOrUpdateStoresGroup(userInfo,groupList),"返回成功");
    }


}
