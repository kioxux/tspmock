package com.gys.business.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gys.business.service.GaiaSdIntegralRateSetService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.exception.BusinessException;
import com.gys.common.request.InsertProductSettingReq;
import com.gys.common.request.SelectCheckIfInDbReq;
import com.gys.common.request.SelectInteProductListReq;
import com.gys.common.request.UpdateInteProductSettingReq;
import com.gys.common.response.SelectInteProductListResponse;
import com.gys.common.response.SelectStoreListResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 积分商品设置表 前端控制器
 * </p>
 *
 * @author zhangjiahao
 * @since 2021-07-28
 */
@RestController
@Api(tags = "不积分商品设置")
@RequestMapping("/integralRateSet")
public class GaiaSdIntegralRateSetController extends BaseController {

    @Autowired
    private GaiaSdIntegralRateSetService gaiaSdIntegralRateSetService;

    @ApiOperation("初始化星期日期数据")
    @GetMapping("getPeriodDate")
    public JsonResult getPeriodDate(HttpServletRequest request){
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.gaiaSdIntegralRateSetService.getPeriodDate(), "提示:数据获取成功");
    }



    @ApiOperation("初始化门店列表")
    @GetMapping("selectStoreList")
    public JsonResult<SelectStoreListResponse> selectStoreList(HttpServletRequest request,String stoName) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.gaiaSdIntegralRateSetService.setClientId(userInfo.getClient(),stoName), "提示:数据获取成功");
    }

    @ApiOperation("新增商品设置")
    @PostMapping("insertProductSetting")
    public JsonResult insertProductSetting(HttpServletRequest request, @RequestBody InsertProductSettingReq req) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        req.setClient(userInfo.getClient());
        return JsonResult.success(gaiaSdIntegralRateSetService.insertProductSetting(req), "提示:新增成功");
    }

    @ApiOperation("会员积分商品列表")
    @PostMapping("selectInteProductPageList")
    public JsonResult<PageInfo<SelectInteProductListResponse>> selectInteProductPageList(HttpServletRequest request, @RequestBody SelectInteProductListReq req) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        req.setClient(userInfo.getClient());
        return JsonResult.success(gaiaSdIntegralRateSetService.selectInteProductPageList(req), "提示:数据获取成功");
    }

    @ApiOperation("删除商品积分设置")
    @DeleteMapping("deleteInteProductSetting/{id}")
    public JsonResult deleteInteProductSetting(HttpServletRequest request, @PathVariable(value = "id") String id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(gaiaSdIntegralRateSetService.deleteInteProductSetting(id, userInfo.getClient()), "提示:删除成功");
    }

    @ApiOperation("修改商品积分设置")
    @PutMapping("updateInteProductSetting")
    public JsonResult updateInteProductSetting(HttpServletRequest request, @RequestBody UpdateInteProductSettingReq req) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        req.setClient(userInfo.getClient());
        return JsonResult.success(gaiaSdIntegralRateSetService.updateInteProductSetting(req), "提示:修改成功");
    }


    @ApiOperation("会员商品积分数据回显")
    @GetMapping("selectInteProductSetting/{id}")
    public JsonResult selectInteProductSetting(HttpServletRequest request, @PathVariable String id) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(gaiaSdIntegralRateSetService.selectInteProductSetting(id, userInfo.getClient()), "提示:获取数据成功");
    }

    @ApiOperation("校验商品编码是否已存在")
    @PostMapping("selectProductCodes")
    public JsonResult selectProductCode(HttpServletRequest request, @RequestBody SelectCheckIfInDbReq req) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        req.setClient(userInfo.getClient());
        return JsonResult.success(gaiaSdIntegralRateSetService.selectProductCode(req), "提示:获取数据成功");
    }

    @ApiOperation("商品编码导入")
    @PostMapping("insertProductCodeImport")
    public JsonResult insertProductCodeImport(HttpServletRequest request, MultipartFile file) {
        List<Map<String,String>> importInDataList = new ArrayList<>();
        try {
            InputStream in = file.getInputStream();
            //导入已存在的Excel文件，获得只读的工作薄对象
            XSSFWorkbook wk = new XSSFWorkbook(in);
            //获取第一张Sheet表
            Sheet sheet = wk.getSheetAt(0);
            //取数据
            importInDataList = getRowAndCell(sheet);
            in.close();
            wk.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("importInDataList = " + importInDataList);
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(gaiaSdIntegralRateSetService.insertProductCodeImport(importInDataList,userInfo.getClient()), "提示:获取数据成功");
    }
    public List<Map<String,String>> getRowAndCell(Sheet sheet) {
        List<Map<String,String>> importInDataList = new ArrayList<>();
        String msg = null;
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        System.out.println("总行数==" + totalRows);
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            for (int i = 1; i <= totalRows; i++) {
                Row row = sheet.getRow(i); //取出一行数据放入row
                Map<String,String> newExpert = new HashMap<>();
                if(StrUtil.isNotBlank(dataFormatter.formatCellValue(row.getCell(0)).trim())){
                    newExpert.put("proCode",dataFormatter.formatCellValue(row.getCell(0)).trim());
                    importInDataList.add(newExpert);
                }
            } //行end
        }else{
            throw new BusinessException("EXCEL导入信息为空");
        }
        return importInDataList;
    }
}
