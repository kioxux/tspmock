package com.gys.business.controller;

import com.gys.business.service.RegionalPlanningService;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.excel.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:54 2021/10/14
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@RestController
@RequestMapping({"/regional/"})
public class RegionalPlanningController extends BaseController {

    @Autowired
    private RegionalPlanningService regionalPlanningService;

    @PostMapping("defaultDate")
    public JsonResult defaultDate(@RequestBody EditDefaultRegionalInData inData) {
        this.regionalPlanningService.defaultDate(inData);
        return JsonResult.success("", "提示：生成数据成功！");
    }

    @PostMapping("editDefaultRegional")
    public JsonResult editDefaultRegional(@RequestBody @Valid EditDefaultRegionalInData inData){
        this.regionalPlanningService.editDefaultRegional(inData);
        return JsonResult.success("", "提示：修改成功！");
    }

    @PostMapping("getRegional")
    public JsonResult getRegional(HttpServletRequest request, @RequestBody @Valid SelectRegionalInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return JsonResult.success(this.regionalPlanningService.getRegional(userInfo, inData), "提示：获取数据成功！");
    }

    @PostMapping("saveRegional")
    public JsonResult saveRegional(HttpServletRequest request, @RequestBody @Valid SaveRegionalInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        if (ObjectUtil.isNotEmpty(inData.getId())) {
            inData.setUpdateUser(userInfo.getUserId());
        } else {
            inData.setCreateUser(userInfo.getUserId());
        }
        this.regionalPlanningService.saveRegional(inData);
        return JsonResult.success("", "提示：保存成功！");
    }

    @PostMapping("removeRegional")
    public JsonResult removeRegional(HttpServletRequest request,@RequestBody RemoveRegionalInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUpdateUser(userInfo.getUserId());
        inData.setUpdateTime(new Date());
        this.regionalPlanningService.removeRegional(inData);
        return JsonResult.success("","提示：删除成功！");
    }

    @PostMapping("getRegionalStore")
    public JsonResult getRegionalStore(HttpServletRequest request, @RequestBody @Valid SelectRegionalStoreInData inData) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        return JsonResult.success(this.regionalPlanningService.getRegionalStore(inData), "提示：获取数据成功！");
    }

    @PostMapping("saveRegionalStore")
    public JsonResult saveRegionalStore(HttpServletRequest request,@RequestBody @Valid SaveRegionalStoreInData inData){
        GetLoginOutData userInfo = this.getLoginUser(request);
        inData.setClient(userInfo.getClient());
        inData.setUpdateUser(userInfo.getUserId());
        inData.setUpdateDate(new Date());
        this.regionalPlanningService.saveRegionalStore(inData);
        return JsonResult.success("", "提示：保存成功！");
    }
}
