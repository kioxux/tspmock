package com.gys.business.controller;
import com.gys.business.service.GaiaPriceRangeService;
import com.gys.business.service.data.PriceRangeInData;
import com.gys.business.service.data.PriceRangeOutData;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.common.response.Result;
import com.gys.util.CosUtils;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Zhangchi
 * @since 2021/11/18/10:44
 */
@Api(tags = "价格区间划分")
@RestController
@RequestMapping({"/priceRange/"})
public class GaiaPriceRangeController extends BaseController {
    @Resource
    private GaiaPriceRangeService priceRangeService;
    @Resource
    public CosUtils cosUtils;

    @ApiOperation(value = "城市下拉列表")
    @PostMapping({"/getAreaList"})
    public JsonResult getAreaList(){
        return this.priceRangeService.getAreaList();
    }

    @ApiOperation(value = "价格区间查询")
    @PostMapping({"/getPriceRange"})
    public JsonResult getPriceRange(@RequestBody PriceRangeInData priceRangeInData){
        return JsonResult.success(priceRangeService.getPriceRangeByClient(priceRangeInData),"提示：查询成功！");
    }

    @ApiOperation(value = "保存")
    @PostMapping({"/savePriceRange"})
    public JsonResult savePriceRange(HttpServletRequest request, @RequestBody  List<PriceRangeOutData> inDataList){
        GetLoginOutData userInfo = this.getLoginUser(request);
        this.priceRangeService.savePriceRange(userInfo,inDataList);
        return JsonResult.success("","提示：查询成功！");
    }

    @ApiOperation(value = "价格区间导出")
    @PostMapping({"exportPriceRangeList"})
    public Result exportPriceRangeList(@RequestBody PriceRangeInData priceRangeInData) throws IOException {
        Result result = null;
        List<PriceRangeOutData> list = this.priceRangeService.getPriceRangeByClient(priceRangeInData);
        for (PriceRangeOutData priceRangeOutData : list){
            if (!StringUtils.isBlank(priceRangeOutData.getUpdateUser())){
                priceRangeOutData.setUpdateUser(priceRangeOutData.getUpdateUser().substring(0,priceRangeOutData.getUpdateUser().lastIndexOf('-')));
            }
        }
        String name = "价格区间导出表";
        if (list.size() > 0){
            CsvFileInfo csvFileInfo = CsvClient.getCsvByte(list,name, Collections.singletonList((short) 1));
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                assert csvFileInfo != null;
                outputStream.write(csvFileInfo.getFileContent());
                result = cosUtils.uploadFile(outputStream, csvFileInfo.getFileName());
            } catch (IOException e){
                e.printStackTrace();
            } finally {
                outputStream.flush();
                outputStream.close();
            }
            return result;
        }else{
            throw new BusinessException("提示：没有查询到数据，请修改查询条件！");
        }
    }

    /**
     * 前置条件：对已上加盟商配置默认区间值
     */
    @ApiOperation(value = "前置条件：价格区间设置默认值")
    @PostMapping({"/insertPriceRange"})
    public JsonResult insertPriceRange(){
        this.priceRangeService.insertPriceRange();
        return JsonResult.success("","提示：查询成功！");
    }
}
