package com.gys.business.controller.yaomeng;

import com.gys.business.mapper.entity.GaiaSdSaleRecipel;
import com.gys.business.service.yaomeng.YaomengService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.yaomeng.RecipelInfoQueryBean;
import com.gys.common.data.yaomeng.RecipelQuestionInfoBean;
import com.gys.util.yaomeng.YaomengSpeedSimpleBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author ZhangDong
 * @date 2021/11/4 16:52
 */
@Controller
@RequestMapping("yaomeng")
@Api("药盟")
public class YaomengController extends BaseController {
    @Autowired
    private YaomengService yaomengService;

    @ApiOperation("查询当前门店是否需要等待回传处方就可以继续收银")
    @ResponseBody
    @PostMapping("isPrescribe")
    public JsonResult isPrescribe(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String isPrescribe = yaomengService.isPrescribe(userInfo.getClient(), userInfo.getDepId());
        return JsonResult.success(isPrescribe, "提示：获取数据成功！");
    }

    @ApiOperation("检查该销售单中所销售的处方药是否已经和药盟对码成功")
    @ResponseBody
    @PostMapping("drugCheck")
    public JsonResult drugCheck(HttpServletRequest request, @RequestBody Map<String, List<String>> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<String> drugs = map.get("drugs");
        return yaomengService.drugCheck(userInfo.getClient(), userInfo.getDepId(), drugs);
    }

    @ApiOperation("查询病症信息")
    @ResponseBody
    @PostMapping("drugDisease")
    public JsonResult drugDisease(HttpServletRequest request, @RequestBody Map<String, List<String>> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<String> drugs = map.get("drugs");
        return yaomengService.drugDisease(userInfo.getClient(), userInfo.getDepId(), drugs);
    }

    @ApiOperation("配伍禁忌限制校验")
    @ResponseBody
    @PostMapping("checkLimit")
    public JsonResult checkLimit(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<String> drugs = (List<String>) map.get("drugs");
        String idCard = (String) map.get("idCard");
        return yaomengService.checkLimit(userInfo.getClient(), userInfo.getDepId(), drugs, idCard);
    }

    @ApiOperation("话术查询")
    @ResponseBody
    @PostMapping("dialogueQuestion")
    public JsonResult dialogueQuestion(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        List<String> diseaseIds = (List<String>) map.get("diseaseIds");
        return yaomengService.dialogueQuestion(userInfo.getClient(), diseaseIds);
    }

    @ApiOperation("获取医生信息")
    @ResponseBody
    @PostMapping("getDoctor")
    public JsonResult getDoctor(HttpServletRequest request) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return yaomengService.getDoctor(userInfo.getClient());
    }

    @ApiOperation("提交图文处方")
    @ResponseBody
    @PostMapping("speedSimple")
    public JsonResult speedSimple(HttpServletRequest request, @RequestBody YaomengSpeedSimpleBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return yaomengService.speedSimple(userInfo.getClient(), userInfo.getDepId(), bean);
    }

    @ApiOperation("获取处方图片url")
    @ResponseBody
    @PostMapping("getPrescriptionFileUrl")
    public JsonResult getPrescriptionFileUrl(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String no = map.get("no");
        String pharmacist = map.get("pharmacist");
        return yaomengService.getPrescriptionFileUrl(no, pharmacist);
    }

    @ApiOperation("提交处方问诊信息")
    @ResponseBody
    @PostMapping("submitRecipelQuestionInfo")
    public JsonResult submitRecipelQuestionInfo(HttpServletRequest request, @RequestBody RecipelQuestionInfoBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return yaomengService.submitRecipelQuestionInfo(userInfo, bean);
    }

    @ApiOperation("保存电子开方信息")
    @ResponseBody
    @PostMapping("saveEleRecipelInfo")
    public JsonResult saveEleRecipelInfo(HttpServletRequest request, @RequestBody List<GaiaSdSaleRecipel> list) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        for (GaiaSdSaleRecipel gaiaSdSaleRecipel : list) {
            gaiaSdSaleRecipel.setClientId(userInfo.getClient());
        }
        JsonResult jsonResult = yaomengService.saveEleRecipelInfo(userInfo, list);
        return jsonResult;
    }


    @ApiOperation("通过销售单号获取电子开方信息")
    @ResponseBody
    @PostMapping("getEleRecipelInfoByGssrSaleBillNo")
    public JsonResult getEleRecipelInfoByGssrSaleBillNo(HttpServletRequest request,  @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);

        JsonResult jsonResult = yaomengService.getEleRecipelInfoByGssrSaleBillNo(userInfo.getClient() , map);
        return jsonResult;
    }



    @ApiOperation("处方信息查询")
    @ResponseBody
    @PostMapping("recipelInfoQuery")
    public JsonResult recipelInfoQuery(HttpServletRequest request, @RequestBody RecipelInfoQueryBean bean) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        bean.setClient(userInfo.getClient());
        bean.setStoCode(userInfo.getDepId());
        return yaomengService.recipelInfoQuery(bean);
    }

    @ApiOperation("重开方")
    @ResponseBody
    @PostMapping("recipelAgain")
    public JsonResult recipelAgain(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String saleBillNo = map.get("saleBillNo");
        return yaomengService.recipelAgain(userInfo.getClient(), userInfo.getDepId(), saleBillNo);
    }

    @ApiOperation("保存处方单号和处方图片地址")
    @ResponseBody
    @PostMapping("saveRecipelIdAndFileUrl")
    public JsonResult saveRecipelIdAndFileUrl(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String saleBillNo = map.get("saleBillNo");
        String recipelId = map.get("recipelId");
        String fileUrl = map.get("fileUrl");
        return yaomengService.saveRecipelIdAndFileUrl(saleBillNo, recipelId, fileUrl);
    }

    @ApiOperation("获取销售明细表处方药商品数据")
    @ResponseBody
    @PostMapping("getRecipelProFromSaleD")
    public JsonResult getRecipelProFromSaleD(HttpServletRequest request, @RequestBody Map<String, String> map) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        String saleBillNo = map.get("saleBillNo");
        return yaomengService.getRecipelProFromSaleD(userInfo.getClient(), userInfo.getDepId(), saleBillNo);
    }

    @ApiOperation("修改处方的审方单号、手机号和审方类型")
    @ResponseBody
    @PostMapping("saveVoucherIdAndPhone")
    public JsonResult saveVoucherIdAndPhone(HttpServletRequest request, @RequestBody List<GaiaSdSaleRecipel> list) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        for (GaiaSdSaleRecipel recipel : list) {
            recipel.setClientId(userInfo.getClient());
        }
        return yaomengService.saveVoucherIdAndPhone(list);
    }
    @ApiOperation("自动审核处方")
    @ResponseBody
    @PostMapping("autoCheckRecipelInfo")
    public JsonResult autoCheckRecipelInfo(HttpServletRequest request, @RequestBody List<String> list) {
        GetLoginOutData userInfo = this.getLoginUser(request);
        return yaomengService.autoCheckRecipelInfo(userInfo, list);
    }

}
