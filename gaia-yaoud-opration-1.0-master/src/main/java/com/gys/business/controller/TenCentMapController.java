package com.gys.business.controller;

import com.gys.business.service.TenCentMapService;
import com.gys.common.data.JsonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping({"/tenCentMap"})
public class TenCentMapController {

    @Autowired
    private TenCentMapService tenCentMapService;

    @ApiOperation(value = "根据地址,更新经纬度信息")
    @PostMapping("/updateLngAndLat")
    public JsonResult updateLngAndLat(@RequestBody Map<String,Object> inData) {
        tenCentMapService.updateLngAndLat(inData);
        return JsonResult.success(null,"更新成功");
    }
}
