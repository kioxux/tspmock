package com.gys.business.controller;


import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationD;
import com.gys.business.service.GaiaEntNewProductEvaluationDService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 公司-新品评估-明细表(GaiaEntNewProductEvaluationD)表控制层
 *
 * @author makejava
 * @since 2021-07-19 10:16:28
 */
@RestController
@RequestMapping("gaiaEntNewProductEvaluationD")
public class GaiaEntNewProductEvaluationDController {
    /**
     * 服务对象
     */
    @Resource
    private GaiaEntNewProductEvaluationDService gaiaEntNewProductEvaluationDService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public GaiaEntNewProductEvaluationD selectOne(Long id) {
        return this.gaiaEntNewProductEvaluationDService.queryById(id);
    }

}
