package com.gys.business.controller.takeAway;

import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.business.service.takeAway.BaseService;
import com.gys.business.service.takeAway.TakeAwayService;
import com.gys.common.base.BaseController;
import com.gys.util.takeAway.ky.SignUtil;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 快药外卖推送
 *
 * @author zhangdong
 * @date 2021/5/6 15:24
 */
@Controller
@RequestMapping("/ky/push")
public class ApiKyPushController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(ApiKyPushController.class);
    @Autowired
    private BaseService baseService;
    @Autowired
    private TakeAwayService takeAwayService;

    /**
     * 推送已接单订单
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/receiveOrder", method = RequestMethod.POST)
    public Map<String, Object> receiveOrder(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>(2);
        Map<String, Object> paramMap = getParameterMap(request);
        logger.info("快药零售参数：" + JSONObject.toJSONString(paramMap));
        String appId = nullToEmpty(paramMap.get("app_id"));
        String version = nullToEmpty(paramMap.get("version"));
        String timestamp = nullToEmpty(paramMap.get("timestamp"));
        String signature = nullToEmpty(paramMap.get("signature"));
        Map<String, Object> paramBody = (Map<String, Object>) JSONObject.parse(StringEscapeUtils.unescapeJava((String) paramMap.get("body")));
        String orderNo = nullToEmpty(paramBody.get("order_no"));
        String shopNo = nullToEmpty(paramBody.get("shop_no"));//平台店铺ID

        //根据当前appid和店铺id 查找私钥
        GaiaTAccount account = takeAwayService.getEbPlatformAccount("KY", appId, shopNo);
        if (account == null) {
            logger.error("快药零售-推送API：找不到对应门店信息！shopNo:{}， orderNo:{}", shopNo, orderNo);
            resultMap.put("data", "error");
            return resultMap;
        }
        String secret = account.getAppSecret();
        String client = account.getClient();
        String orderOnService = account.getOrderOnService();
        String stoCode = account.getStoCode();
        //校验sign是否正确
        boolean flag = SignUtil.kYcheckSign(appId, secret, timestamp, version, signature);
        if (!flag) {
            resultMap.put("data", "error");
            return resultMap;
        }
        baseService.kyCreateNewOrder(client, stoCode, paramMap, orderOnService, appId, secret);
        resultMap.put("data", "ok");
        return resultMap;
    }

    /**
     * 推送接单后取消的订单
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/cancelOrder", method = RequestMethod.POST)
    public Map<String, Object> cancelOrder(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>(2);
        Map<String, Object> paramMap = getParameterMap(request);
        logger.info("快药零售参数：" + JSONObject.toJSONString(paramMap));
        String appId = nullToEmpty(paramMap.get("app_id"));
        String version = nullToEmpty(paramMap.get("version"));
        String timestamp = nullToEmpty(paramMap.get("timestamp"));
        String signature = nullToEmpty(paramMap.get("signature"));
        Map<String, Object> paramBody = (Map<String, Object>) JSONObject.parse(StringEscapeUtils.unescapeJava((String) paramMap.get("body")));
        String orderNo = nullToEmpty(paramBody.get("order_no"));
        String shopNo = nullToEmpty(paramBody.get("shop_no"));//平台店铺ID

        //根据当前appid和店铺id 查找私钥
        GaiaTAccount account = takeAwayService.getEbPlatformAccount("KY", appId, shopNo);
        String secret = "";
        String client = "";
        String orderOnService = "N";
        if (account != null) {
            secret = account.getAppSecret();
            client = account.getClient();
            orderOnService = account.getOrderOnService();
        } else {
            logger.error("快药零售-推送API：找不到对应门店信息！shopNo:{}， orderNo:{}", shopNo, orderNo);
            resultMap.put("data", "error");
            return resultMap;
        }
        //校验sign是否正确
        boolean flag = SignUtil.kYcheckSign(appId, secret, timestamp, version, signature);
        if (!flag) {
            resultMap.put("data", "error");
            return resultMap;
        }

        resultMap.put("data", "ok");
        return resultMap;
    }

    /**
     * 推送已部分退款订单
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/departDrawback", method = RequestMethod.POST)
    public Map<String, Object> departDrawback(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>(2);
        Map<String, Object> paramMap = getParameterMap(request);
        logger.info("快药零售参数：" + JSONObject.toJSONString(paramMap));
        String appId = nullToEmpty(paramMap.get("app_id"));
        String version = nullToEmpty(paramMap.get("version"));
        String timestamp = nullToEmpty(paramMap.get("timestamp"));
        String signature = nullToEmpty(paramMap.get("signature"));
        Map<String, Object> paramBody = (Map<String, Object>) JSONObject.parse(StringEscapeUtils.unescapeJava((String) paramMap.get("body")));
        String orderNo = nullToEmpty(paramBody.get("order_no"));
        String shopNo = nullToEmpty(paramBody.get("shop_no"));//平台店铺ID

        //根据当前appid和店铺id 查找私钥
        GaiaTAccount account = takeAwayService.getEbPlatformAccount("KY", appId, shopNo);
        String secret = "";
        String client = "";
        String orderOnService = "N";
        if (account != null) {
            secret = account.getAppSecret();
            client = account.getClient();
            orderOnService = account.getOrderOnService();
        } else {
            logger.error("快药零售-推送API：找不到对应门店信息！shopNo:{}， orderNo:{}", shopNo, orderNo);
            resultMap.put("data", "error");
            return resultMap;
        }
        //校验sign是否正确
        boolean flag = SignUtil.kYcheckSign(appId, secret, timestamp, version, signature);
        if (!flag) {
            resultMap.put("data", "error");
            return resultMap;
        }

        resultMap.put("data", "ok");
        return resultMap;
    }

}
