package com.gys.business.entity.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CallUpAndInStoreTotalVO {

    //品项
    private BigDecimal numberOfStoreOUt;

    //时间品项
    private BigDecimal actualAmount;

    //满足率
    private String fillRate;

}
