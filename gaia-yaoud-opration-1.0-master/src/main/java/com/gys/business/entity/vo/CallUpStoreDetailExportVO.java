package com.gys.business.entity.vo;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 系统级调出门店明细导出类
 */
@Data
public class CallUpStoreDetailExportVO {
    @ColumnWidth(30)
    @ExcelProperty(value = "门店调剂建议时间",index = 0)
    @DateTimeFormat(value = "yyyy年MM月dd日")
    private Date billDate;

    @ExcelProperty(value = "省份",index = 1)
    private String stoProvince;

    @ExcelProperty(value = "市",index = 2)
    private String stoCity;

    @ColumnWidth(35)
    @ExcelProperty(value = "客户",index = 3)
    private String clientIdAndName;

    @ColumnWidth(35)
    @ExcelProperty(value = "调出门店",index = 4)
    private String stoOutCodeAndName;

    @ExcelProperty(value = "关联门店1",index = 5)
    private String associatedStoreOne;

    @ExcelProperty(value = "关联门店2",index = 6)
    private String associatedStoreTwo;

    @ExcelProperty(value = "关联门店3",index = 7)
    private String associatedStoreThree;

    @ColumnWidth(30)
    @ExcelProperty(value = "门店调剂建议单号",index = 8)
    private String billCode;

    @ColumnWidth(30)
    @ExcelProperty(value = "门店调剂建议完成时间",index = 9)
    @DateTimeFormat(value = "yyyy年MM月dd日")
    private Date finishDate;

    @ExcelProperty(value = "门店调剂品项数",index = 10)
    @ApiModelProperty(value = "门店调剂品项数")
    private BigDecimal numberOfStoreOUt;

    @ExcelProperty(value = "门店调剂实际品项数",index = 11)
    private BigDecimal actualAmount;

    @ExcelProperty(value = "满足率",index = 12)
    private Double fillRate;

    @ExcelProperty(value = "反馈门店数",index = 13)
    private Integer numberOfFeedbackStores;

    @ExcelProperty(value = "单据类型",index = 14)
    private String documentType;

    @ExcelProperty(value = "调剂确认前是否导出",index = 15)
    private String whetherToExportBeforeConfirming;

    @ExcelProperty(value = "调剂确认后是否导出",index = 16)
    private String whetherToExportAfterConfirming;

    //"关联门店信息-返回格式 : 1-10001-安安药房,2-10002-健健药房,3-10003-康康药房 ")
    @ExcelIgnore
    private String associatedStores;


}
