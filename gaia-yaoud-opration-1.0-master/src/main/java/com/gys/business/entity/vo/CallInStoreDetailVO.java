package com.gys.business.entity.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CallInStoreDetailVO {

    @ColumnWidth(30)
    @ExcelProperty(value = "门店调剂建议时间",index = 0)
    @JSONField(format = "yyyy年MM月dd日")
    @DateTimeFormat(value = "yyyy年MM月dd日")
    @ApiModelProperty(value = "门店调剂建议时间")
    private Date billDate;

    @ExcelProperty(value = "省份",index = 1)
    @ApiModelProperty(value = "省份")
    private String stoProvince;

    @ExcelProperty(value = "市",index = 2)
    @ApiModelProperty(value = "市")
    private String stoCity;

    @ExcelProperty(value = "客户编码",index = 3)
    @ApiModelProperty(value = "客户编码")
    private String clientId;

    @ColumnWidth(30)
    @ExcelProperty(value = "客户名称",index = 4)
    @ApiModelProperty(value = "客户名称")
    private String clientName;

    @ExcelProperty(value = "调入店编码",index = 5)
    @ApiModelProperty(value = "调入门店编码")
    private String stoInCode;

    @ColumnWidth(30)
    @ExcelProperty(value = "调入门店名称",index = 6)
    @ApiModelProperty(value = "调入门店名称")
    private String stoInName;

    @ColumnWidth(40)
    @ExcelProperty(value = "调出门店",index = 7)
    @ApiModelProperty(value = "调出门店")
    private String stoOutCodeAndName;

    @ColumnWidth(30)
    @ExcelProperty(value = "门店调剂调入单号",index = 8)
    @ApiModelProperty(value = "门店调剂建议单号")
    private String billCode;

    //门店调出单号
    @ColumnWidth(30)
    @ExcelProperty(value = "门店调剂调出单号",index = 9)
    private String outBillCode;

    @ColumnWidth(30)
    @ExcelProperty(value = "门店调剂建议完成时间",index = 10)
    @JSONField(format = "yyyy年MM月dd日")
    @DateTimeFormat(value = "yyyy年MM月dd日")
    @ApiModelProperty(value = "门店调剂建议完成时间")
    private Date finishDate;

    @ExcelProperty(value = "门店调剂品项数",index = 11)
    @ApiModelProperty(value = "门店调剂品项数")
    private BigDecimal numberOfStoreOUt;

    @ExcelProperty(value = "门店调剂实际品项数",index = 12)
    @ApiModelProperty(value = "门店调剂实际品项数")
    private BigDecimal actualAmount;

    @ExcelProperty(value = "满足率",index = 13)
    @ApiModelProperty(value = "满足率")
    private Double fillRate;

    @ExcelProperty(value = "单据类型",index = 14)
    @ApiModelProperty(value = "单据类型")
    private String documentType;

    @ExcelProperty(value = "是否反馈最终调入量", index = 15)
    @ApiModelProperty(value = "是否反馈最终调入量")
    private String whetherFeedback;

    @ExcelProperty(value = "调剂确认前是否导出",index = 16)
    @ApiModelProperty(value = "调剂确认前是否导出")
    private String whetherToExportBeforeConfirming;

    @ExcelProperty(value = "调剂确认后是否导出",index = 17)
    @ApiModelProperty(value = "调剂确认后是否导出")
    private String whetherToExportAfterConfirming;
}
