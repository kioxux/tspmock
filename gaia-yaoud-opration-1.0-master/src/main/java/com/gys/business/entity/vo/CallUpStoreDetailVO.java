package com.gys.business.entity.vo;


import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 系统级调出门店明细返回类
 */
@Data
@ApiModel(value = "系统级调出门店明细返回类")
public class CallUpStoreDetailVO {

    @JSONField(format = "yyyy年MM月dd日")
    @DateTimeFormat(value = "yyyy年MM月dd日")
    @ApiModelProperty(value = "门店调剂建议时间")
    private Date billDate;

    @ApiModelProperty(value = "省份")
    private String stoProvince;

    @ApiModelProperty(value = "市")
    private String stoCity;

    @ApiModelProperty(value = "客户编码")
    private String clientId;

    @ApiModelProperty(value = "客户名称")
    private String clientName;

    @ApiModelProperty(value = "调出门店编码")
    private String stoOutCode;

    @ApiModelProperty(value = "调出门店名称")
    private String stoOutName;

    @ApiModelProperty(value = "关联门店信息-返回格式 : 1-10001-安安药房,2-10002-健健药房,3-10003-康康药房 ")
    private String associatedStores;

    @ApiModelProperty(value = "门店调剂建议单号")
    private String billCode;

    @JSONField(format = "yyyy年MM月dd日")
    @DateTimeFormat(value = "yyyy年MM月dd日")
    @ApiModelProperty(value = "门店调剂建议完成时间")
    private Date finishDate;

    @ApiModelProperty(value = "门店调剂品项数")
    private BigDecimal numberOfStoreOUt;

    @ApiModelProperty(value = "门店调剂实际品项数")
    private BigDecimal actualAmount;

    @ApiModelProperty(value = "满足率")
    private Double fillRate;

    @ApiModelProperty(value = "反馈门店数")
    private Integer numberOfFeedbackStores;

    @ApiModelProperty(value = "单据类型")
    private String documentType;

    @ApiModelProperty(value = "调剂确认前是否导出")
    private String whetherToExportBeforeConfirming;

    @ApiModelProperty(value = "调剂确认后是否导出")
    private String whetherToExportAfterConfirming;
}
