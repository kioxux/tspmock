package com.gys.business.entity.dto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.List;

/**
 * 系统级门店调出明细 入参类
 */
@Data
public class CallUpOrInStoreDetailDTO implements Serializable {

    @ApiModelProperty(value = "页码")
    private int pageNum;

    @ApiModelProperty(value = "每页记录数")
    private int pageSize;

    @ApiModelProperty(value = "加盟商编号即客户编码")
    private List<String> clientIds;

    @ApiModelProperty(value = "调库单号")
    private List<String> billCode;


    @ApiModelProperty(value = "门店建议起始时间")
    private String suggestedStartDate;

    @ApiModelProperty(value = "门店建议结束时间")
    private String suggestedEndDate;


    @ApiModelProperty(value = "建议完成起始时间")
    private String finishStartDate;

    @ApiModelProperty(value = "建议完成结束时间")
    private String finishEndDate;

    @ApiModelProperty(value = "调出单据状态 0 :未推送 1: 已推送  2 : 待确认 3 : 已确定 4: 已失效")
    private List<Integer> productType;

    @ApiModelProperty(value = "调入方单据状态 0-待处理 1-已确认 2-已失效 3-已完成")
    private List<Integer> consumerType;

    @ApiModelProperty(value = "省份")
    private List<String> province;

    @ApiModelProperty(value = "市")
    private List<String> city;



}
