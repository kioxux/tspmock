package com.gys.business.yaojian.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.bosen.form.DataForm;
import com.gys.business.heartBeatWarn.service.HeartBeatService;
import com.gys.business.yaojian.service.DongRuanService;
import com.gys.common.data.JsonResult;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 东软药监对接--全家康健
 * @date 2022/1/20 13:42
 */
@Slf4j
@RestController
@RequestMapping("/yaojian/dongruan")
public class DongRuanController {
    @Resource
    private DongRuanService dongRuanService;

    @Resource
    private HeartBeatService heartBeatService;
    /**
     * 获取商品数据
     * @param
     */
    @PostMapping("getProductForDR")
    public JsonResult getProductForDR(HttpServletRequest request, @RequestBody DataForm dataForm) {
        long startTime = System.currentTimeMillis();
        log.info("<东软医保对接api><商品数据><请求参数：{}>");
        JsonResult result= dongRuanService.getProductForDR(dataForm);
        return result;
    }

    /**
     * 获取库存数据
     * @param
     */
    @PostMapping("getStockForDR")
    public JsonResult getStockForDR(HttpServletRequest request) {
        long startTime = System.currentTimeMillis();
        log.info("<东软医保对接api><库存数据><请求参数：{}>");
        JsonResult result= dongRuanService.getStockForDR();
        return result;
    }



    /**
     * 获取订单数据
     * @param
     */
    @PostMapping("getOrderListForDR")
    public JsonResult getOrderListForDR(HttpServletRequest request) {
        long startTime = System.currentTimeMillis();
        log.info("<东软医保对接api><订单数据><请求参数：{}>");
        JsonResult result= dongRuanService.getOrderListForDR();
        return result;
    }

}
