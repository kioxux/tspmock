package com.gys.business.yaojian.service.impl;

import com.gys.business.mapper.YuanQuYaoJianMapper;
import com.gys.business.yaojian.service.YuanQuService;
import com.gys.common.data.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2022/1/15 14:14
 */
@Slf4j
@Service
public class YuanQuServiceImpl implements YuanQuService {
    @Resource
    private YuanQuYaoJianMapper yuanQuYaoJianMapper;

    List<String> clientList=Arrays.asList(new String[]{"10000110","10000111","10000112","10000113","10000114","10000115","10000116","10000117","10000118","10000119","10000120","10000121","10000122","10000123","10000124","10000125","10000126","10000127","10000128","10000129","10000130","10000131","10000132","10000133","10000134","10000135","10000136","10000137","10000138","10000139","10000140","10000141","10000142","10000143","10000144","10000145","10000146","10000147","10000148","10000149","10000150","10000151","10000152","10000153","10000154","10000155","10000156","10000157","10000158","10000159","10000160","10000161","10000162","10000163","10000164","10000165","10000166","10000167","10000168","10000169","10000170","10000171","10000172","10000173","10000174","10000175","10000176","10000177","10000178","10000179","10000180","10000181","10000182","10000183","10000184","10000185","10000186","10000187","10000188","10000189","10000190","10000191","10000192","10000193","10000194","10000195","10000196","10000197","10000198","10000199","10000200","10000201","10000202","10000203","10000204","10000205","10000206","10000207","10000208","10000209","10000210","10000211","10000212","10000213","10000214","10000215","10000216","10000217","10000218","10000219","10000220","10000221","10000222","10000223","10000224","10000225","10000226","10000227","10000228","10000229","10000230","10000231","10000232","10000233","10000234","10000235","10000236","10000237","10000238","10000239","10000240","10000241","10000242","10000243","10000244","10000245","10000246","10000247","10000248","10000249","10000250","10000251","10000252","10000253","10000254","10000255","10000256","10000257","10000258","10000259","10000260","10000261","10000262","10000263","10000264","10000265","10000266","10000267","10000268","10000269","10000270","10000271","10000272","10000273","10000274","10000275","10000276","10000277","10000278","10000279","10000280","10000281","10000282","10000283","10000284","10000285","10000286","10000287","10000288","10000289","10000290","10000291","10000292","10000293","10000294","10000295","10000296","10000297","10000298","10000299","10000300","10000303"});

    @Override
    public JsonResult getSaleRecordData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getSaleRecordData(clientList);
        return  JsonResult.success(list);
    }

    @Override
    public JsonResult getSaleRecordHData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getSaleRecordHData(clientList);
        return  JsonResult.success(list);
    }
    @Override
    public JsonResult getProductData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getProductData(clientList);
        return  JsonResult.success(list);
    }
    @Override
    public JsonResult getSupplierData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getSupplierData(clientList);
        return  JsonResult.success(list);
    }
    @Override
    public JsonResult getStockData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getStockData(clientList);
        return  JsonResult.success(list);
    }
    @Override
    public JsonResult getStockDetailData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getStockDetailData(clientList);
        return  JsonResult.success(list);
    }
    @Override
    public JsonResult getStoreUserData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getStoreUserData(clientList);
        return  JsonResult.success(list);
    }
    @Override
    public JsonResult getStoreLastDateData(){
        List<LinkedHashMap<String,Object>> list=new ArrayList<>();
        HashMap<String,String> QuaDateList=dateTransforMap(yuanQuYaoJianMapper.getStoreQuaDateData(clientList));
        HashMap<String,String> CuringDateList=dateTransforMap(yuanQuYaoJianMapper.getStoreCuringDateData(clientList));
        HashMap<String,String> TemperatureDateList=dateTransforMap(yuanQuYaoJianMapper.getStoreTemperatureDateData(clientList));
        HashMap<String,String> LoginuaDateList=dateTransforMap(yuanQuYaoJianMapper.getStoreLoginDateData(clientList));
        for (String client:clientList) {
            LinkedHashMap<String,Object> object=new LinkedHashMap<>();
            object.put("client",client);
            object.put("areaCode","07");
            object.put("storeCode","10000");
            if(QuaDateList.containsKey(client)){
                object.put("recentQuaDate",QuaDateList.get(client));
            }
            if(CuringDateList.containsKey(client)){
                object.put("recentCuringDate",CuringDateList.get(client));
            }
            if(TemperatureDateList.containsKey(client)){
                object.put("recentTemperatureDate",TemperatureDateList.get(client));
            }
            if(LoginuaDateList.containsKey(client)){
                object.put("recentLoginDate",LoginuaDateList.get(client));
            }
            list.add(object);

        }

        return  JsonResult.success(list);
    }
    private HashMap<String,String> dateTransforMap( List<LinkedHashMap<String,Object>> dateList){
        HashMap<String,String> hashMap=new HashMap<>();
        for (LinkedHashMap<String,Object> map:dateList) {
            if(map.containsKey("client")&&map.containsKey("maxDate")){
                hashMap.put(map.get("client").toString(),map.get("maxDate").toString());
            }
        }
        return hashMap;
    }

    @Override
    public JsonResult getStoreTempHumiData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getStoreTempHumiData(clientList);
        return  JsonResult.success(list);
    }

    @Override
    public JsonResult getConserveInfoData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getConserveInfoData(clientList);
        return  JsonResult.success(list);
    }

    @Override
    public JsonResult getConserveDetailData(){
        List<LinkedHashMap<String,Object>> list=yuanQuYaoJianMapper.getConserveDetailData(clientList);
        return  JsonResult.success(list);
    }
}
