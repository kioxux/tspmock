package com.gys.business.yaojian.dto;

import lombok.Data;

/**
 * @author li_haixia@gov-info.cn
 * @desc 东软医保配置实体
 * @date 2022/1/25 1:29
 */
@Data
public class DongRuanConfigDTO {
    private String client;
    private String site;
    private String appKey;
    private String appSecret;
}
