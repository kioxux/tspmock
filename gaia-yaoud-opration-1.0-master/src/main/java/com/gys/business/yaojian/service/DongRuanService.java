package com.gys.business.yaojian.service;

import com.gys.business.bosen.form.DataForm;
import com.gys.common.data.JsonResult;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2022/1/20 13:56
 */
public interface DongRuanService {
    JsonResult<List<Map<String,Object>>> getProductForDR(DataForm dataForm);
    JsonResult<List<Map<String,Object>>> getStockForDR();
    JsonResult<List<Map<String,Object>>> getOrderListForDR();
}
