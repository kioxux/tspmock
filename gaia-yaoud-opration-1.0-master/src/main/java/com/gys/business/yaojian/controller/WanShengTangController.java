package com.gys.business.yaojian.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.bosen.constant.HBYJEnum;
import com.gys.business.bosen.form.DataForm;
import com.gys.business.heartBeatWarn.service.HeartBeatService;
import com.gys.business.yaojian.service.WanShengTangService;
import com.gys.common.data.JsonResult;
import com.gys.common.exception.BusinessException;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 万盛堂进销存数据
 * @date 2021/12/13 13:50
 */

@Slf4j
@RestController
@RequestMapping("/yaojian/wanshengtang")
public class WanShengTangController {
    @Resource
    private WanShengTangService wanShengTangService;

    @Resource
    private HeartBeatService heartBeatService;
    /**
     * 获取入库数据
     * @param dataForm
     */
    @PostMapping("getInData")
    public JsonResult getInData(HttpServletRequest request, @RequestBody DataForm dataForm) {
        long startTime = System.currentTimeMillis();
        log.info("<新疆万盛堂><入库数据><请求参数：{}>", JSON.toJSONString(dataForm));
        validParams(dataForm);
        JsonResult result= wanShengTangService.getInData(dataForm);

        //添加监控
        Map<String,Object> inData=new HashMap<>();
        inData.put("client", dataForm.getClient());
        inData.put("site",dataForm.getBrId());
        inData.put("IP", CommonUtil.getIpAddr(request));
        inData.put("Name", "getInData");
        inData.put("Comment", "万盛堂药监获取入库数据");
        inData.put("WatchType", 2);
        inData.put("Comsume", System.currentTimeMillis()-startTime);
        heartBeatService.addServericeWatch(inData);

        return result;
    }

    /**
     * 获取出库数据
     * @param dataForm
     */
    @PostMapping("getOutData")
    public JsonResult getOutData(HttpServletRequest request,@RequestBody DataForm dataForm) {
        long startTime = System.currentTimeMillis();
        log.info("<新疆万盛堂><出库数据><请求参数：{}>", JSON.toJSONString(dataForm));
        validParams(dataForm);
        JsonResult result= wanShengTangService.getOutData(dataForm);

        //添加监控
        Map<String,Object> inData=new HashMap<>();
        inData.put("client", dataForm.getClient());
        inData.put("site",dataForm.getBrId());
        inData.put("IP", CommonUtil.getIpAddr(request));
        inData.put("Name", "getOutData");
        inData.put("Comment", "万盛堂药监获取出库数据");
        inData.put("WatchType", 2);
        inData.put("Comsume", System.currentTimeMillis()-startTime);
        heartBeatService.addServericeWatch(inData);

        return result;
    }

    /**
     * 获取库存数据
     * @param dataForm
     */
    @PostMapping("getStockData")
    public JsonResult getStockData(HttpServletRequest request,@RequestBody DataForm dataForm) {
        long startTime = System.currentTimeMillis();
        log.info("<新疆万盛堂><库存数据><请求参数：{}>", JSON.toJSONString(dataForm));
        validParams(dataForm);
        JsonResult result=  wanShengTangService.getStockData(dataForm);

        //添加监控
        Map<String,Object> inData=new HashMap<>();
        inData.put("client", dataForm.getClient());
        inData.put("site",dataForm.getBrId());
        inData.put("IP", CommonUtil.getIpAddr(request));
        inData.put("Name", "getStockData");
        inData.put("Comment", "万盛堂药监获取库存数据");
        inData.put("WatchType", 2);
        inData.put("Comsume", System.currentTimeMillis()-startTime);
        heartBeatService.addServericeWatch(inData);

        return result;
    }

    private void validParams(DataForm dataForm) {
        HBYJEnum hbyjEnum = HBYJEnum.getEnum(dataForm.getClient());
        if (hbyjEnum == null) {
            throw new BusinessException("当前加盟商未开放数据下载");
        }
        if (!hbyjEnum.appKey.equalsIgnoreCase(dataForm.getKey())) {
            throw new BusinessException("参数错误【key】");
        }
    }
}
