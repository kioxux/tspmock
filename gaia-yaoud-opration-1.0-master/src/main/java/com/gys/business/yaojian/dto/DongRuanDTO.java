package com.gys.business.yaojian.dto;

import lombok.Data;

/**
 * @author li_haixia@gov-info.cn
 * @desc 东软查询实体
 * @date 2022/1/20 20:41
 */
@Data
public class DongRuanDTO {
    private String client;
    private String site;
    private String appkey;
    private String appsecret;
    private Integer synctype;
}
