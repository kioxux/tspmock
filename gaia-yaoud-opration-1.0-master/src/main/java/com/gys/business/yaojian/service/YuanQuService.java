package com.gys.business.yaojian.service;

import com.gys.business.bosen.form.DataForm;
import com.gys.common.data.JsonResult;

/**
 * @author li_haixia@gov-info.cn
 * @desc 园区药监同步数据
 * @date 2022/1/15 13:40
 */
public interface YuanQuService {

    /***
     * 获取销售主表
     * @return
     */
    JsonResult getSaleRecordData();


    /***
     * 获取销售明细表
     * @return
     */
    JsonResult getSaleRecordHData();

    /***
     * 获取商品
     * @return
     */
    JsonResult getProductData();
    /***
     * 获取供应商
     * @return
     */
    JsonResult getSupplierData();
    /***
     * 获取入库
     * @return
     */
    JsonResult getStockData();
    /***
     * 获取入库明细
     * @return
     */
    JsonResult getStockDetailData();
    /***
     * 获取门店用户
     * @return
     */
    JsonResult getStoreUserData();
    /***
     * 获取门店最近时间
     * @return
     */
    JsonResult getStoreLastDateData();
    /***
     * 获取温度湿度
     * @return
     */
    JsonResult getStoreTempHumiData();
    /***
     * 获取养护
     * @return
     */
    JsonResult getConserveInfoData();
    /***
     * 获取养护明细
     * @return
     */
    JsonResult getConserveDetailData();
}
