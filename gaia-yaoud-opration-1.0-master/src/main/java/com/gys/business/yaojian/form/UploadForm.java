package com.gys.business.yaojian.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/21 13:11
 */
@Data
public class UploadForm {
    @ApiModelProperty(value = "记录id")
    private List<Integer> idList;
    @ApiModelProperty(value = "单据类型：1入库，2库存，3出库")
    private Integer type;
    private String uploadBy;
}
