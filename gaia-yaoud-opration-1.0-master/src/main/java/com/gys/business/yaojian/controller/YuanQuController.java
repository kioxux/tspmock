package com.gys.business.yaojian.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.bosen.form.DataForm;
import com.gys.business.yaojian.service.YuanQuService;
import com.gys.common.data.JsonResult;
import com.gys.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2022/1/15 13:39
 */
@Slf4j
@RestController
@RequestMapping("/yaojian/yuanqu")
public class YuanQuController {
    @Resource
    private YuanQuService yuanQuService;
    /**
     * 获取销售主表
     */
    @PostMapping("getSaleRecordData")
    public JsonResult getSaleRecordData() {
        JsonResult result= yuanQuService.getSaleRecordData();
        return result;
    }

    /**
     * 获取销售明细表
     */
    @PostMapping("getSaleRecordHData")
    public JsonResult getSaleRecordHData() {
        JsonResult result= yuanQuService.getSaleRecordHData();
        return result;
    }
    /**
     * 获取商品
     */
    @PostMapping("getProductData")
    public JsonResult getProductData() {
        JsonResult result= yuanQuService.getProductData();
        return result;
    }
    /**
     * 获取供应商
     */
    @PostMapping("getSupplierData")
    public JsonResult getSupplierData() {
        JsonResult result= yuanQuService.getSupplierData();
        return result;
    }
    /**
     * 获取入库
     */
    @PostMapping("getStockData")
    public JsonResult getStockData() {
        JsonResult result= yuanQuService.getStockData();
        return result;
    }
    /**
     * 获取入库明细
     */
    @PostMapping("getStockDetailData")
    public JsonResult getStockDetailData() {
        JsonResult result= yuanQuService.getStockDetailData();
        return result;
    }
    /**
     * 获取门店用户
     */
    @PostMapping("getStoreUserData")
    public JsonResult getStoreUserData() {
        JsonResult result= yuanQuService.getStoreUserData();
        return result;
    }
    /**
     * 获取门店最近时间
     */
    @PostMapping("getStoreLastDateData")
    public JsonResult getStoreLastDateData() {
        JsonResult result= yuanQuService.getStoreLastDateData();
        return result;
    }
    /**
     * 获取温度湿度
     */
    @PostMapping("getStoreTempHumiData")
    public JsonResult getStoreTempHumiData() {
        JsonResult result= yuanQuService.getStoreTempHumiData();
        return result;
    }
    /**
     * 获取养护
     */
    @PostMapping("getConserveInfoData")
    public JsonResult getConserveInfoData() {
        JsonResult result= yuanQuService.getConserveInfoData();
        return result;
    }
    /**
     * 获取养护明细
     */
    @PostMapping("getConserveDetailData")
    public JsonResult getConserveDetailData() {
        JsonResult result= yuanQuService.getConserveDetailData();
        return result;
    }
}
