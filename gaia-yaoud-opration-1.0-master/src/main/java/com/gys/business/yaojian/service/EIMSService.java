package com.gys.business.yaojian.service;

import com.gys.business.mapper.entity.GaiaSdEIMS;
import com.gys.business.yaojian.form.EIMSForm;
import com.gys.business.yaojian.form.UploadForm;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;

import java.io.IOException;
import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/21 10:49
 */
public interface EIMSService {
    PageInfo<GaiaSdEIMS> search(EIMSForm eimsForm);
    JsonResult<Boolean> upload(UploadForm uploadForm);
    Result export(EIMSForm eimsForm) throws IOException;
    void check();
    void batchInsertInData(String client);
    void batchInsertStockData(String client);
    void batchUpdateStockData(String client);
    void batchInsertOutData(String client);





}
