package com.gys.business.yaojian.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author li_haixia@gov-info.cn
 * @desc 入库实体
 * @date 2021/11/22 12:22
 */
@Data
public class InBoundRecordDTO {
    private String inboundDocNumber;
    private String commodityCode;
    private String packingSpec;
    private String lotNumber;
    private String enterpriseLocationCode;
    private String inboundDate;
    private Integer type;
    private String companyName;
    private String transcator;
    private String conclusion;
    private String expirationDate;
    private String productionDate;
    private BigDecimal quantity;
    private String unit;
    private String reason;
    private String relativeDocNumber;
    private String producer;
    private String producingArea;
    private String remarks;
    private Integer processType;
}
