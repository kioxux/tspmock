package com.gys.business.yaojian.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/21 10:50
 */
@Data
public class EIMSForm {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店id")
    private String site;
    @ApiModelProperty(value = "单据类型：1入库，2库存，3出库")
    private Integer type;
    @ApiModelProperty(value = "单据号")
    private String billNo;
    private String Data_Date;
    @ApiModelProperty(value = "商品编码")
    private String proId;
    @ApiModelProperty(value = "有效期")
    private String proValidDate;
    @ApiModelProperty(value = "单据开始日期")
    private String startDate;
    @ApiModelProperty(value = "单据结束日期")
    private String endDate;
    @ApiModelProperty(value = "上传状态（0：未上传，1：上传成功，2：上传失败，3：上传处理中）")
    private String uploadStatus;

    /**分页页码：默认1*/
    private Integer pageNum;
    /**分页大小：默认10*/
    private Integer pageSize;
    /**查询起始值*/
    private Integer pageIndex;
}
