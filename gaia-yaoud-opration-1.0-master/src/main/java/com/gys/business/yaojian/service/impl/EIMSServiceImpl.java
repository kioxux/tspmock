package com.gys.business.yaojian.service.impl;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.github.pagehelper.PageHelper;
import com.gys.business.mapper.GaiaSdEIMSMapper;
import com.gys.business.mapper.entity.GaiaSdEIMS;
import com.gys.business.yaojian.dto.InBoundRecordDTO;
import com.gys.business.yaojian.dto.InventoryRecordDTO;
import com.gys.business.yaojian.dto.OutboundRecordDTO;
import com.gys.business.yaojian.form.EIMSForm;
import com.gys.business.yaojian.form.UploadForm;
import com.gys.business.yaojian.service.EIMSService;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import com.gys.common.response.ResultUtil;
import com.gys.util.CosUtils;
import com.gys.util.LuXianUtil;
import com.gys.util.csv.CsvClient;
import com.gys.util.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/21 12:09
 */
@Slf4j
@Service
public class EIMSServiceImpl implements EIMSService {
    @Resource
    private GaiaSdEIMSMapper gaiaSdEIMSMapper;
    @Resource
    public CosUtils cosUtils;
    @Override
    public PageInfo<GaiaSdEIMS> search(EIMSForm eimsForm) {
        eimsForm.setPageIndex((eimsForm.getPageNum() - 1) * eimsForm.getPageSize());
        PageHelper.startPage(eimsForm.getPageNum(), eimsForm.getPageSize());
        List<GaiaSdEIMS> list=gaiaSdEIMSMapper.search(eimsForm);
        PageInfo pageInfo;
        if (CollectionUtils.isEmpty(list)) {
            pageInfo = new PageInfo(new ArrayList());
        } else {
            pageInfo = new PageInfo(list);
        }
        return pageInfo;
    }

    @Override
    public JsonResult<Boolean> upload(UploadForm uploadForm){
        if(uploadForm.getIdList().isEmpty()){
            return  JsonResult.fail(500,"未选择要上传的数据");
        }
        for(Integer id :uploadForm.getIdList()){
            JSONObject obj=new JSONObject();
            switch (uploadForm.getType()){
                case 1:
                    List<InBoundRecordDTO> inData=gaiaSdEIMSMapper.getInDataById(id);
                    if(!inData.isEmpty()){
                        obj.set("type","inboundRecord");
                        obj.set("data",inData.get(0));
                    }
                    break;
                case 2:
                    List<InventoryRecordDTO> stockData=gaiaSdEIMSMapper.getStockDataById(id);
                    if(!stockData.isEmpty()){
                        obj.set("type","inventoryRecord");
                        obj.set("data",stockData.get(0));
                    }
                    break;
                case 3:
                    List<OutboundRecordDTO> outData=gaiaSdEIMSMapper.getOutDataById(id);
                    if(!outData.isEmpty()){
                        obj.set("type","outboundRecord");
                        obj.set("data",outData.get(0));
                    }
                    break;
                default:
                    return  JsonResult.fail(500,"单据类型不正确");
            }

            try{
                if(!obj.containsKey("type")){
                    continue;
                }
                JSONObject postObject= LuXianUtil.sendOrder(obj);
                if (postObject != null && postObject.get("code",Integer.class)==0) {
                    //更新id和status
                    GaiaSdEIMS gaiaSdEIMS=new GaiaSdEIMS();
                    gaiaSdEIMS.setId(id);
                    gaiaSdEIMS.setUploadStatus(3);
                    gaiaSdEIMS.setUploadBy(uploadForm.getUploadBy());
                    gaiaSdEIMS.setUploadId(postObject.get("id",String.class));
                    gaiaSdEIMSMapper.updateStatus(gaiaSdEIMS);
                }
            }
            catch (Exception ex) {
                log.error("泸县药监上传文件失败:" + ex.getMessage());
                return JsonResult.fail(500, "上传失败");
            }
        }
        return JsonResult.success(true,"上传成功");
    }

    @Override
    public Result export(EIMSForm eimsForm) throws IOException {
        List<LinkedHashMap<String,Object>> list=gaiaSdEIMSMapper.export(eimsForm);
        String fileName = "";
        Map<String, String> titleMap = new LinkedHashMap<>(2);
        Result result = null;
        if(list.isEmpty()){return ResultUtil.error("200","暂无数据");}
        switch (eimsForm.getType()){
            case 1:
                fileName = "入库-药监数据";
                titleMap.put("Data_Site","企业内部编码");
                titleMap.put("Data_BillNo","入库单号");
                titleMap.put("Pro_Id","商品编码");
                titleMap.put("Pro_Spec","包装规格");
                titleMap.put("Pro_Manufacturer","生产厂家");
                titleMap.put("Pro_Place","产地");
                titleMap.put("Pro_BatchNo","批号");
                titleMap.put("Pro_ValidDate","有效期至");
                titleMap.put("Pro_ProduceDate","生产日期");
                titleMap.put("Data_Date","入库日期");
                titleMap.put("Data_InType","入库类型");
                titleMap.put("Company_Name","企业名称");
                titleMap.put("Operator","经办人");
                titleMap.put("Conclusion","结论");
                titleMap.put("Quantity","入库数量");
                titleMap.put("Pro_Unit","计量单位");
                titleMap.put("Reason","退货原因");
                titleMap.put("Relative_No","关联单据号");
                titleMap.put("Remarks","备注");
                titleMap.put("Upload_Status","数据处理类型");
                break;
            case 2:
                fileName = "库存-药监数据";
                titleMap.put("Data_Site","企业内部编码");
                titleMap.put("Pro_Id","商品编码");
                titleMap.put("Pro_Spec","包装规格");
                titleMap.put("Pro_Manufacturer","生产厂家");
                titleMap.put("Pro_Place","产地");
                titleMap.put("Pro_BatchNo","批号");
                titleMap.put("Pro_ValidDate","有效期至");
                titleMap.put("Pro_ProduceDate","生产日期");
                titleMap.put("Quantity","库存数量");
                titleMap.put("Pro_Unit","计量单位");
                titleMap.put("Remarks","备注");
                titleMap.put("Upload_Status","数据处理类型");
                break;
            case 3:
                titleMap.put("Data_Site","企业内部编码");
                titleMap.put("Data_BillNo","出库单号");
                titleMap.put("Pro_Id","商品编码");
                titleMap.put("Pro_Spec","包装规格");
                titleMap.put("Pro_Manufacturer","生产厂家");
                titleMap.put("Pro_Place","产地");
                titleMap.put("Pro_BatchNo","批号");
                titleMap.put("Pro_ValidDate","有效期至");
                titleMap.put("Pro_ProduceDate","生产日期");
                titleMap.put("Data_Date","出库日期");
                titleMap.put("Data_OutType","出库类型");
                titleMap.put("Company_Name","企业名称");
                titleMap.put("Operator","经办人");
                titleMap.put("Quantity","出库数量");
                titleMap.put("Pro_Unit","计量单位");
                titleMap.put("Reason","出库原因");
                titleMap.put("Relative_No","关联单据号");
                titleMap.put("Prescription_Source","处方来源");
                titleMap.put("","医生姓名");
                titleMap.put("","处方日期");
                titleMap.put("","处方内容");
                titleMap.put("Patient_Name","病人姓名");
                titleMap.put("","病人性别");
                titleMap.put("","病人年龄");
                titleMap.put("","病人电话");
                titleMap.put("","病人地址");
                titleMap.put("Patient_IdCard","病人身份证号");
                titleMap.put("","处方调配日期");
                titleMap.put("","审方人姓名");
                titleMap.put("","复核人姓名");
                titleMap.put("","处方调配人姓名");
                titleMap.put("Remarks","备注");
                titleMap.put("Upload_Status","数据处理类型");
        }

        CsvFileInfo fileInfo = CsvClient.getCsvByteByMap(list,fileName,titleMap);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            bos.write(fileInfo.getFileContent());
             result = cosUtils.uploadFile(bos, fileInfo.getFileName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            bos.flush();
            bos.close();
        }
        return result;

    }

    @Override
    public void check(){
        List<LinkedHashMap<String,Object>> uploadIdList=gaiaSdEIMSMapper.getUpdateID();
        if(!uploadIdList.isEmpty()){
            for (LinkedHashMap<String,Object> map :uploadIdList){
                JSONObject getObject= LuXianUtil.getStatus(map.get("Upload_Id").toString());
                if (getObject != null && getObject.get("status",Integer.class)!=0) {
                    //更新状态
                    GaiaSdEIMS gaiaSdEIMS=new GaiaSdEIMS();
                    gaiaSdEIMS.setId(Integer.parseInt(map.get("ID").toString()));
                    gaiaSdEIMS.setUploadStatus(getObject.get("status",Integer.class));
                    gaiaSdEIMS.setReason(getObject.get("result",String.class));
                    gaiaSdEIMSMapper.updateStatus(gaiaSdEIMS);
                }
            }
        }
    }
    @Override
    public void batchInsertInData(String client){
        gaiaSdEIMSMapper.batchInsertInData(client);
    }

    @Override
    public void batchInsertStockData(String client){
        gaiaSdEIMSMapper.batchInsertStockData(client);
    }


    @Override
    public void batchUpdateStockData(String client){
        gaiaSdEIMSMapper.batchUpdateStockData(client);
    }



    @Override
    public void batchInsertOutData(String client){
        gaiaSdEIMSMapper.batchInsertOutData(client);
    }



}
