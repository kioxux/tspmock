package com.gys.business.yaojian.controller;

import com.alibaba.fastjson.JSON;
import com.gys.business.mapper.entity.GaiaSdEIMS;
import com.gys.business.yaojian.form.EIMSForm;
import com.gys.business.yaojian.form.UploadForm;
import com.gys.business.yaojian.service.EIMSService;
import com.gys.common.base.BaseController;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.JsonResult;
import com.gys.common.data.PageInfo;
import com.gys.common.response.Result;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @desc 泸县药监对接管理
 * @date 2021/11/19 15:23
 */
@Slf4j
@RestController
@RequestMapping("luxian")
public class LuxianController extends BaseController {
@Resource
private EIMSService eimsService;

    /**
     * 查询
     */
    @PostMapping("search")
    public JsonResult search(HttpServletRequest request, @RequestBody EIMSForm eimsForm) {
        log.info("<泸县药监列表><查询><请求参数：{}>", JSON.toJSONString(eimsForm));
        GetLoginOutData userInfo = this.getLoginUser(request);
        eimsForm.setClient(userInfo.getClient());
        eimsForm.setSite(userInfo.getDepId());
        if (eimsForm.getPageNum() == null) {
            eimsForm.setPageNum(1);
        }
        if (eimsForm.getPageSize() == null) {
            eimsForm.setPageSize(10);
        }

        return JsonResult.success(eimsService.search(eimsForm),"查询成功");
    }

    /**
     * 导出
     */
    @PostMapping("export")
    public Result export(HttpServletRequest request, @RequestBody EIMSForm eimsForm) throws IOException {
        log.info("<泸县药监列表><导出><请求参数：{}>", JSON.toJSONString(eimsForm));
        GetLoginOutData userInfo = this.getLoginUser(request);
        eimsForm.setClient(userInfo.getClient());
        eimsForm.setSite(userInfo.getDepId());
        return eimsService.export(eimsForm);
    }

    /**
     * 上传
     * @param request
     * @param uploadForm
     * @return
     */
    @PostMapping("upload")
    public JsonResult<Boolean> upload(HttpServletRequest request,@RequestBody UploadForm uploadForm){
        log.info("<泸县药监列表><上传><请求参数：{}>", JSON.toJSONString(uploadForm));
        GetLoginOutData userInfo = this.getLoginUser(request);
        uploadForm.setUploadBy(userInfo.getLoginName());

        return eimsService.upload(uploadForm);
    }

}
