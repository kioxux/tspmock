package com.gys.business.yaojian.service.impl;

import com.gys.business.bosen.form.DataForm;
import com.gys.business.mapper.entity.GaiaSdSystemPara;
import com.gys.business.mapper.yaojian.YaojianMapper;
import com.gys.business.yaojian.dto.DongRuanConfigDTO;
import com.gys.business.yaojian.dto.DongRuanDTO;
import com.gys.business.yaojian.service.DongRuanService;
import com.gys.common.data.JsonResult;
import com.gys.common.excel.ObjectUtil;
import com.gys.common.excel.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2022/1/20 13:56
 */
@Slf4j
@Service
public class DongRuanServiceImpl  implements DongRuanService {
    @Resource
    private YaojianMapper yaojianMapper;
    @Override
    public JsonResult<List<Map<String,Object>>> getProductForDR(DataForm dataForm) {
        List<Map<String,Object>> result=new ArrayList<>();
        //获取配置
        Map<String,DongRuanDTO> configDTOMap=getConfigList();
        //循环获取数据
        for (DongRuanDTO configDTO :configDTOMap.values()) {
            if(!StringUtil.isEmpty(dataForm.getKey()) ){
                configDTO.setSynctype(Integer.parseInt(dataForm.getKey()) );
            }
            List<Map<String,Object>> productList=yaojianMapper.getProductForDR(configDTO);
            if(!ObjectUtil.isEmpty(productList)){
                Map<String,Object> mqMap=new HashMap<>();
                mqMap.put("appkay",configDTO.getAppkey());
                mqMap.put("appsecre",configDTO.getAppsecret());
                mqMap.put("data",productList);
                result.add(mqMap);
            }
        }
        return JsonResult.success(result);
    }
    @Override
    public JsonResult<List<Map<String,Object>>> getStockForDR() {
        List<Map<String,Object>> result=new ArrayList<>();
        //获取配置
        Map<String,DongRuanDTO> configDTOMap=getConfigList();
        //循环获取数据
        for (DongRuanDTO configDTO :configDTOMap.values()) {
            List<Map<String,Object>> productList=yaojianMapper.getStockForDR(configDTO);
            if(!ObjectUtil.isEmpty(productList)){
                Map<String,Object> mqMap=new HashMap<>();
                mqMap.put("appkay",configDTO.getAppkey());
                mqMap.put("appsecre",configDTO.getAppsecret());
                mqMap.put("data",productList);
                result.add(mqMap);
            }
        }
        return JsonResult.success(result);
    }
    @Override
    public JsonResult<List<Map<String,Object>>> getOrderListForDR() {
        List<Map<String,Object>> result=new ArrayList<>();
        //获取配置
        Map<String,DongRuanDTO> configDTOMap=getConfigList();
        //循环获取数据
        for (DongRuanDTO configDTO :configDTOMap.values()) {
            List<Map<String,Object>> productList=yaojianMapper.getOrderListForDR(configDTO);
            if(!ObjectUtil.isEmpty(productList)){
                Map<String,Object> mqMap=new HashMap<>();
                mqMap.put("appkay",configDTO.getAppkey());
                mqMap.put("appsecre",configDTO.getAppsecret());
                mqMap.put("data",productList);
                result.add(mqMap);
            }
        }
        return JsonResult.success(result);
    }
    private Map<String,DongRuanDTO> getConfigList(){
        //获取配置
        List<GaiaSdSystemPara> configList=yaojianMapper.getConfigForDR();
        Map<String,DongRuanDTO> configDTOMap=new HashMap<>();
        for (GaiaSdSystemPara config :configList) {
            String key=config.getClientId()+"|"+config.getGsspBrId();
            if(configDTOMap.containsKey(key)){
                DongRuanDTO dongRuanConfigDTO=configDTOMap.get(key);
                if("APPKEY_DONGRUAN".equals(config.getGsspId())){
                    dongRuanConfigDTO.setAppkey(config.getGsspPara());
                }else if("APPSECRET_DONGRUAN".equals(config.getGsspId())){
                    dongRuanConfigDTO.setAppsecret(config.getGsspPara());
                }
                configDTOMap.replace(key,dongRuanConfigDTO);

            }else{

                DongRuanDTO dongRuanConfigDTO=new DongRuanDTO();
                dongRuanConfigDTO.setClient(config.getClientId());
                dongRuanConfigDTO.setSite(config.getGsspBrId());
                if("APPKEY_DONGRUAN".equals(config.getGsspId())){
                    dongRuanConfigDTO.setAppkey(config.getGsspPara());
                }else if("APPSECRET_DONGRUAN".equals(config.getGsspId())){
                    dongRuanConfigDTO.setAppsecret(config.getGsspPara());
                }
                configDTOMap.put(key,dongRuanConfigDTO);
            }
        }
        return configDTOMap;
    }
}
