package com.gys.business.yaojian.service.impl;

import com.gys.business.bosen.form.DataForm;
import com.gys.business.bosen.form.InDataForm;
import com.gys.business.mapper.*;
import com.gys.business.mapper.entity.GaiaDcData;
import com.gys.business.yaojian.service.WanShengTangService;
import com.gys.common.data.JsonResult;
import com.gys.common.excel.StringUtil;
import com.gys.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/12/13 14:40
 */
@Slf4j
@Service
public class WanShengTangServiceImpl implements WanShengTangService {
    @Resource
    private GaiaSdExamineDMapper gaiaSdExamineDMapper;
    @Resource
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Resource
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaWmKuCunMapper gaiaWmKuCunMapper;


    @Override
    public JsonResult getInData(DataForm dataForm){
        try{
            InDataForm inDataForm=transformInData(dataForm);
            //判断是否是仓库
            GaiaDcData cond = new GaiaDcData();
            cond.setClient(dataForm.getClient());
            cond.setDcCode(dataForm.getBrId());
            List<GaiaDcData> dcList = gaiaDcDataMapper.findList(cond);
            List<LinkedHashMap<String,Object>> list=new ArrayList<>();
            if(dcList.isEmpty()){
                //读取门店数据
                list=gaiaSdExamineDMapper.getInData(inDataForm);
            }else {
                //读取仓库数据
                list=gaiaWmKuCunMapper.getCKInData(inDataForm);
            }
            return JsonResult.success(list);
        }catch (Exception ex){
            log.error("<新疆万盛堂><入库数据><错误信息：{}>"+ex.toString());
            return JsonResult.fail(500,"查询错误");
        }
    }

    @Override
    public JsonResult getOutData(DataForm dataForm){
        try{
            InDataForm inDataForm=transformInData(dataForm);
            //判断是否是仓库
            GaiaDcData cond = new GaiaDcData();
            cond.setClient(dataForm.getClient());
            cond.setDcCode(dataForm.getBrId());
            List<GaiaDcData> dcList = gaiaDcDataMapper.findList(cond);
            List<LinkedHashMap<String,Object>> list=new ArrayList<>();
            if(dcList.isEmpty()){
                //读取门店数据
                list=gaiaSdSaleDMapper.getOutData(inDataForm);
            }else {
                //读取仓库数据
                list=gaiaWmKuCunMapper.getCKOutData(inDataForm);
            }
            return JsonResult.success(list);
        }catch (Exception ex){
            log.error("<新疆万盛堂><出库数据><错误信息：{}>"+ex.toString());
            return JsonResult.fail(500,"查询错误");
        }
    }

    @Override
    public JsonResult getStockData(DataForm dataForm){
        try{
            InDataForm inDataForm=transformInData(dataForm);
            //判断是否是仓库
            GaiaDcData cond = new GaiaDcData();
            cond.setClient(dataForm.getClient());
            cond.setDcCode(dataForm.getBrId());
            List<GaiaDcData> dcList = gaiaDcDataMapper.findList(cond);
            List<LinkedHashMap<String,Object>> list=new ArrayList<>();
            if(dcList.isEmpty()){
                //读取门店数据
                list=gaiaSdStockBatchMapper.getStockData(inDataForm);
            }else {
                //读取仓库数据
                list=gaiaWmKuCunMapper.getCKStockData(inDataForm);
            }
            return JsonResult.success(list);
        }catch (Exception ex){
            log.error("<新疆万盛堂><库存数据><错误信息：{}>"+ex.toString());
            return JsonResult.fail(500,"查询错误");
        }
    }

    private InDataForm transformInData(DataForm dataForm){
        Date currentDate = StringUtil.isNotEmpty(dataForm.getDate()) ?
                DateUtil.stringToDate(dataForm.getDate(), "yyyyMMdd") : new Date();
        String endTime = DateUtil.dateToString(DateUtils.addDays(currentDate, 1), "yyyyMMdd");
        String startTime = DateUtil.dateToString(currentDate, "yyyyMMdd");

        InDataForm cond = new InDataForm();
        cond.setClient(dataForm.getClient());
        cond.setProSite(dataForm.getBrId());
        cond.setStartTime(startTime);
        cond.setEndTime(endTime);

        return cond;
    }
}
