package com.gys.business.yaojian.dto;

import lombok.Data;
import org.joda.time.DateTime;

import java.math.BigDecimal;

/**
 * @author li_haixia@gov-info.cn
 * @desc 库存实体
 * @date 2021/11/22 21:10
 */
@Data
public class InventoryRecordDTO {
    private String commodityCode;
    private String packingSpec;
    private String lotNumber;
    private String enterpriseLocationCode;
    private String expirationDate;
    private String productionDate;
    private BigDecimal quantity;
    private String unit;
    private String producer;
    private String producingArea;
    private String remarks;
    private Integer processType;

}
