package com.gys.business.yaojian.dto;

import lombok.Data;
import org.joda.time.DateTime;

import java.math.BigDecimal;

/**
 * @author li_haixia@gov-info.cn
 * @desc 出库实体
 * @date 2021/11/22 21:12
 */
@Data
public class OutboundRecordDTO {
    private String outboundDocNumber;
    private String commodityCode;
    private String packingSpec;
    private String lotNumber;
    private String enterpriseLocationCode;
    private String outboundDate;
    private Integer type;
    private String transcator;
    private String companyName;
    private String expirationDate;
    private String productionDate;
    private BigDecimal quantity;
    private String unit;
    private String reason;
    private String relativeDocNumber;
    private String prescriptionSource;
    private String doctor;
    private String prescriptionDate;
    private String prescriptionContent;
    private String patient;
    private Integer patientGender;
    private String patientAge;
    private String patientMobile;
    private String patientAddress;
    private String petientIdCardNumber;
    private String dispensingDate;
    private String verifier;
    private String reviewer;
    private String dispenser;
    private String remarks;
    private String producer;
    private String producingArea;
    private Integer processType;

}
