package com.gys.business.yaojian.service;

import com.gys.business.bosen.form.DataForm;
import com.gys.common.data.JsonResult;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/12/13 14:39
 */
public interface WanShengTangService {
    JsonResult getInData(DataForm dataForm);
    JsonResult getOutData(DataForm dataForm);
    JsonResult getStockData(DataForm dataForm);
}
