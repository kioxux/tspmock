//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromCouponBasic;
import com.gys.business.service.data.GetDzqOutData;
import com.gys.business.service.data.GetPayInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdPromCouponBasicMapper extends BaseMapper<GaiaSdPromCouponBasic> {
    List<GetDzqOutData> dzqList(GetPayInData inData);

    String getMaxGspcbCouponId();
}
