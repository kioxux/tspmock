package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "MIB_RECONCILIATION")
public class MibReconciliation implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店
     */
    @Id
    @Column(name = "BR_ID")
    private String brId;

    /**
     * 对账日期（yyyymmdd）
     */
    @Id
    @Column(name = "R_DATE")
    private String rDate;

    /**
     * 险种
     */
    @Id
    @Column(name = "INSUTYPE")
    private String insutype;

    /**
     * 结算类别
     */
    @Id
    @Column(name = "CLR_TYPE")
    private String clrType;

    /**
     * 结算机构
     */
    @Id
    @Column(name = "CLR_OPTINS")
    private String clrOptins;


    /**
     * 状态（0，平  1，不平） 
     */
    @Column(name = "R_STATE")
    private String rState;

    /**
     * 最后更新时间
     */
    @Transient
    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取门店
     *
     * @return BR_ID - 门店
     */
    public String getBrId() {
        return brId;
    }

    /**
     * 设置门店
     *
     * @param brId 门店
     */
    public void setBrId(String brId) {
        this.brId = brId;
    }

    /**
     * 获取对账日期（yyyymmdd）
     *
     * @return R_DATE - 对账日期（yyyymmdd）
     */
    public String getrDate() {
        return rDate;
    }

    /**
     * 设置对账日期（yyyymmdd）
     *
     * @param rDate 对账日期（yyyymmdd）
     */
    public void setrDate(String rDate) {
        this.rDate = rDate;
    }

    /**
     * 获取状态（0，平  1，不平） 
     *
     * @return R_STATE - 状态（0，平  1，不平） 
     */
    public String getrState() {
        return rState;
    }

    /**
     * 设置状态（0，平  1，不平） 
     *
     * @param rState 状态（0，平  1，不平） 
     */
    public void setrState(String rState) {
        this.rState = rState;
    }

    /**
     * 获取最后更新时间
最后更新时间
     *
     * @return LAST_UPDATE_TIME - 最后更新时间
最后更新时间
     */
    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }


    public String getInsutype() {
        return insutype;
    }

    public void setInsutype(String insutype) {
        this.insutype = insutype;
    }

    public String getClrType() {
        return clrType;
    }

    public void setClrType(String clrType) {
        this.clrType = clrType;
    }

    public String getClrOptins() {
        return clrOptins;
    }

    public void setClrOptins(String clrOptins) {
        this.clrOptins = clrOptins;
    }
}