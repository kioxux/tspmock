package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaCompAdm;
import com.gys.common.base.BaseMapper;

/**
 @author wu mao yin
 @Description: 连锁公司
 @date 2021/12/3 15:09
*/
public interface GaiaCompAdmMapper extends BaseMapper<GaiaCompAdm> {

}