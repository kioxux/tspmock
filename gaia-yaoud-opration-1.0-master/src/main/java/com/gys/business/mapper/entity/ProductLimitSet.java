package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(
        name = "GAIA_SD_PRODUCT_LIMIT_SET"
)
public class ProductLimitSet implements Serializable {

    /**
     * 加盟商
     */
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "CLIENT")
    private String client;

    @Column(name = "PRO_ID")
    private String proId;

    //限购类型. 0：每单限购，1：会员限购
    @Column(name = "LIMIT_TYPE")
    private String limitType;

    //限购方式 0：每天，1：合计
    @Column(name = "LIMIT_FLAG")
    private String limitFlag;

    //限购数量
    @Column(name = "LIMIT_QTY")
    private String limitQty;

    //是否一直生效. 0：否，1：是
    @Column(name = "IF_ALLTIME")
    private String ifAllTime;

    //起始日期
    @Column(name = "BEGIN_DATE")
    private String beginDate;

    //结束日期
    @Column(name = "END_DATE")
    private String endDate;

    //星期频率
    @Column(name = "WEEK_FREQUENCY")
    private String weekFrequency;

    //日期频率
    @Column(name = "DATE_FREQUENCY")
    private String dateFrequency;

    //是否所有门店0：否，1：是
    @Column(name = "IF_ALLSTO")
    private String ifAllSto;

    //是否销售 0：不可销售 1： 原价销售
    @Column(name = "IF_SALE")
    private Integer ifSale;
}
