package com.gys.business.mapper;

import com.gys.business.mapper.entity.RecommendFirstPlan;
import com.gys.business.service.data.RecommendFirstPlanInData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首推商品方案表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-08-04
 */
@Mapper
public interface RecommendFirstPlanMapper extends BaseMapper<RecommendFirstPlan> {
    //  获取最近且有效的 计划
    RecommendFirstPlan getRecentlyPlan(Map inData);

}
