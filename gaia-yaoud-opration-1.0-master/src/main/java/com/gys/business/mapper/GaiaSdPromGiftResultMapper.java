package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromGiftResult;
import com.gys.business.service.data.PromGiftResultOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromGiftResultMapper extends BaseMapper<GaiaSdPromGiftResult> {
    List<PromGiftResultOutData> getDetail(PromoteInData inData);

    List<GaiaSdPromGiftResult> getAllByClient(@Param("client") String client, @Param("nowDate") String nowDate,@Param("brId")String brId);
}
