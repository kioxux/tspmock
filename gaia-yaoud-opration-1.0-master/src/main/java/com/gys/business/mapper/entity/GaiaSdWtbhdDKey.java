package com.gys.business.mapper.entity;

import lombok.Data;

@Data
public class GaiaSdWtbhdDKey {

    private String client;

    private String gswbdBrId;

    private String gswbdVoucherId;

    private String gswbdProId;
}