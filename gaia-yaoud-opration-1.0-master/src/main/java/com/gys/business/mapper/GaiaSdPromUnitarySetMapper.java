package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromUnitarySet;
import com.gys.business.service.data.PromUnitarySetOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromUnitarySetMapper extends BaseMapper<GaiaSdPromUnitarySet> {
    List<PromUnitarySetOutData> getDetail(PromoteInData inData);

    void insertToList(List<GaiaSdPromUnitarySet> setList);

    List<GaiaSdPromUnitarySet> getAllByClient(@Param("client") String client, @Param("nowDate") String nowDate,@Param("brId")String brId);

    List<GaiaSdPromUnitarySet> getSingleProm(@Param("client") String client,@Param("brId") String brId,@Param("memberType") String memberType);

}
