//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_THIRD_PARA"
)
public class GaiaThirdPara implements Serializable {
    @Id
    @Column(
            name = "GTP_ID"
    )
    private String gtpId;
    @Column(
            name = "GTP_NAME"
    )
    private String gtpName;
    @Column(
            name = "GTP_PARA"
    )
    private String gtpPara;
    @Column(
            name = "GTP_TYPE"
    )
    private String gtpType;
    private static final long serialVersionUID = 1L;

    public GaiaThirdPara() {
    }

    public String getGtpId() {
        return this.gtpId;
    }

    public void setGtpId(String gtpId) {
        this.gtpId = gtpId;
    }

    public String getGtpName() {
        return this.gtpName;
    }

    public void setGtpName(String gtpName) {
        this.gtpName = gtpName;
    }

    public String getGtpPara() {
        return this.gtpPara;
    }

    public void setGtpPara(String gtpPara) {
        this.gtpPara = gtpPara;
    }

    public String getGtpType() {
        return this.gtpType;
    }

    public void setGtpType(String gtpType) {
        this.gtpType = gtpType;
    }
}
