package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "GAIA_SD_SYSTEM_PARA")
public class ParamDetailData implements Serializable {
    private static final long serialVersionUID = -8378657945258992231L;
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSSP_BR_ID")
    private String gsspBrId;

    @Id
    @Column(name = "GSSP_ID")
    private String gsspId;

    @Column(name = "GSSP_NAME")
    private String gsspName;

    @Column(name = "GSSP_PARA_REMARK")
    private String gsspParaRemark;

    @Column(name = "GSSP_PARA")
    private String gsspPara;

    @Column(name = "GSSP_UPDATE_EMP")
    private String gsspUpdateEmp;

    @Column(name = "GSSP_UPDATE_DATE")
    private String gsspUpdateDate;

}
