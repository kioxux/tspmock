package com.gys.business.mapper;

import com.gys.business.service.data.GaiaChannelProduct;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.gys.business.service.data.GaiaChannelProductInData;
import com.gys.business.service.data.GaiaChannelProductOutData;
import org.apache.ibatis.annotations.Param;

public interface GaiaChannelProductMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaChannelProduct record);

    int insertSelective(GaiaChannelProduct record);

    GaiaChannelProduct selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaChannelProduct record);

    int updateByPrimaryKey(GaiaChannelProduct record);

    int batchInsert(@Param("list") List<GaiaChannelProduct> list);

    Set<String> getStoreByClient(@Param("client") String client);

    Set<String> getConcatStrByClient(@Param("client") String client);

    Set<String> getAllChannel(@Param("client") String client);

    BigDecimal getPriceNormalByProCode(@Param("client") String client, @Param("storeCode") String storeCode, @Param("proCode") String proCode);

    List<String> getConcatStrList(@Param("client") String client);

	int batchUpdate(@Param("list") List<GaiaChannelProduct> list);

    List<GaiaChannelProductOutData> listChannelProduct(GaiaChannelProductInData inData);

    List<HashMap<String, Object>> listStores(GaiaChannelProduct inData);
}