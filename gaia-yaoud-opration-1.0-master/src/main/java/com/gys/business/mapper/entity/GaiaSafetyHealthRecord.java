package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * GAIA_SAFETY_HEALTH_RECORD
 * @author 
 */
@Data
public class GaiaSafetyHealthRecord implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单据编号
     */
    private String voucherId;

    /**
     * 门店ID
     */
    private String storeId;

    /**
     * 门店编码
     */
    private String storeName;

    /**
     * 检查日期
     */
    private String checkDate;

    /**
     * 登记日期
     */
    private String recordDate;

    /**
     * 登记人ID
     */
    private String recorderId;

    /**
     * 登记人姓名
     */
    private String recorderName;

    /**
     * 负责人ID
     */
    private String masterId;

    /**
     * 负责人姓名
     */
    private String masterName;

    /**
     * 健康检查情况
     */
    private String healthCheck;

    /**
     * 安全检查情况
     */
    private String safetyCheck;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态
     */
    private String status;

    /**
     * 删除标记：0-未删除 1-删除
     */
    private Integer isDelete;

    /**
     * 创建者
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateUser;

    /**
     * 更新时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    /**
     * 起始时间
     */
    private String startDate;

    /**
     * 结束时间
     */
    private String endDate;

    /**
     * 主键集合
     */
    private List<Long> ids;

    /**
     * 序号
     */
    private Integer seq;
}