package com.gys.business.mapper;

import com.gys.business.mapper.entity.StockMouth;
import com.gys.business.service.data.StockMouthCaculateCondition;
import com.gys.business.service.data.StockMouthRes;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 月度库存表
 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-07-13
 */
@Mapper
public interface StockMouthMapper extends BaseMapper<StockMouth> {

    List<StockMouthRes> selectByCondition(StockMouthCaculateCondition inData);
}
