package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralExchangeSetDetail;
import com.gys.business.service.data.IntegralExchangeSetDetail;
import com.gys.business.service.data.ProductPriceInData;
import com.gys.business.service.data.ProductPriceInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author RULAISZ
* @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_DETAIL】的数据库操作Mapper
* @createDate 2021-11-29 09:46:42
* @Entity com.gys.business.mapper.entity.GaiaSdIntegralExchangeSetDetail
*/
public interface GaiaSdIntegralExchangeSetDetailMapper {
    List<IntegralExchangeSetDetail> detailList(@Param("client") String client, @Param("voucherId") String voucherId);

    void batchInsert(@Param("detailList") List<GaiaSdIntegralExchangeSetDetail> detailList);

    List<GaiaSdIntegralExchangeSetDetail> getByClientAndVoucherId(@Param("client") String client,@Param("voucherId") String voucherId);

    void deleteByClientAndVoucherId(@Param("client") String client,@Param("voucherId") String voucherId);
    List<ProductPriceInfo> getPrice(ProductPriceInData inData);

    List<GaiaSdIntegralExchangeSetDetail> getByClientAndVoucherIdGroupByProSelfCode(@Param("client") String client, @Param("voucherId") String voucherId);

    List<GaiaSdIntegralExchangeSetDetail> getAllByClient(@Param("client") String client, @Param("stoCode") String stoCode);
}
