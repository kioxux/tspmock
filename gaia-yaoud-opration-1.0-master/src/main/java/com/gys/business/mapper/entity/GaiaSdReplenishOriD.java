package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SD_REPLENISH_ORI_D")
public class GaiaSdReplenishOriD implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private String client;

    /**
     * 补货门店
     */
    @Id
    @Column(name = "GSRD_BR_ID")
    private String gsrdBrId;

    /**
     * 补货单号
     */
    @Id
    @Column(name = "GSRD_VOUCHER_ID")
    private String gsrdVoucherId;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSRD_PRO_ID")
    private String gsrdProId;

    /**
     * 补货日期
     */
    @Column(name = "GSRD_DATE")
    private String gsrdDate;

    /**
     * 建议补货量
     */
    @Column(name = "GSRD_PROPOSE_QTY")
    private BigDecimal gsrdProposeQty;

    /**
     * 补货量
     */
    @Column(name = "GSRD_NEED_QTY")
    private BigDecimal gsrdNeedQty;

    /**
     * 创建日期
     */
    @Column(name = "GSRD_CR_DATE")
    private String gsrdCrDate;

    /**
     * 创建时间
     */
    @Column(name = "GSRD_CR_TIME")
    private String gsrdCrTime;
    /**
     * 零售价
     */
    @Column(name = "GSRD_PO_PRC")
    private BigDecimal gsrdPoPrc;
    /**
     * 配送价
     */
    @Column(name = "GSRD_ACC_PRC")
    private BigDecimal gsrdAccPrc;
    /**
     * 库存数量
     */
    @Column(name = "GSRD_WM_KCSL")
    private BigDecimal gsrdWmKcsl;

    /**
     * 创建人
     */
    @Column(name = "GSRD_CR_ID")
    private String gsrdCrId;

    private static final long serialVersionUID = 1L;

    public BigDecimal getGsrdWmKcsl() {
        return gsrdWmKcsl;
    }

    public void setGsrdWmKcsl(BigDecimal gsrdWmKcsl) {
        this.gsrdWmKcsl = gsrdWmKcsl;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    public BigDecimal getGsrdPoPrc() {
        return gsrdPoPrc;
    }

    public void setGsrdPoPrc(BigDecimal gsrdPoPrc) {
        this.gsrdPoPrc = gsrdPoPrc;
    }

    public BigDecimal getGsrdAccPrc() {
        return gsrdAccPrc;
    }

    public void setGsrdAccPrc(BigDecimal gsrdAccPrc) {
        this.gsrdAccPrc = gsrdAccPrc;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取补货门店
     *
     * @return GSRD_BR_ID - 补货门店
     */
    public String getGsrdBrId() {
        return gsrdBrId;
    }

    /**
     * 设置补货门店
     *
     * @param gsrdBrId 补货门店
     */
    public void setGsrdBrId(String gsrdBrId) {
        this.gsrdBrId = gsrdBrId;
    }

    /**
     * 获取补货单号
     *
     * @return GSRD_VOUCHER_ID - 补货单号
     */
    public String getGsrdVoucherId() {
        return gsrdVoucherId;
    }

    /**
     * 设置补货单号
     *
     * @param gsrdVoucherId 补货单号
     */
    public void setGsrdVoucherId(String gsrdVoucherId) {
        this.gsrdVoucherId = gsrdVoucherId;
    }

    /**
     * 获取商品编码
     *
     * @return GSRD_PRO_ID - 商品编码
     */
    public String getGsrdProId() {
        return gsrdProId;
    }

    /**
     * 设置商品编码
     *
     * @param gsrdProId 商品编码
     */
    public void setGsrdProId(String gsrdProId) {
        this.gsrdProId = gsrdProId;
    }

    /**
     * 获取补货日期
     *
     * @return GSRD_DATE - 补货日期
     */
    public String getGsrdDate() {
        return gsrdDate;
    }

    /**
     * 设置补货日期
     *
     * @param gsrdDate 补货日期
     */
    public void setGsrdDate(String gsrdDate) {
        this.gsrdDate = gsrdDate;
    }

    /**
     * 获取建议补货量
     *
     * @return GSRD_PROPOSE_QTY - 建议补货量
     */
    public BigDecimal getGsrdProposeQty() {
        return gsrdProposeQty;
    }

    /**
     * 设置建议补货量
     *
     * @param gsrdProposeQty 建议补货量
     */
    public void setGsrdProposeQty(BigDecimal gsrdProposeQty) {
        this.gsrdProposeQty = gsrdProposeQty;
    }

    /**
     * 获取补货量
     *
     * @return GSRD_NEED_QTY - 补货量
     */
    public BigDecimal getGsrdNeedQty() {
        return gsrdNeedQty;
    }

    /**
     * 设置补货量
     *
     * @param gsrdNeedQty 补货量
     */
    public void setGsrdNeedQty(BigDecimal gsrdNeedQty) {
        this.gsrdNeedQty = gsrdNeedQty;
    }

    /**
     * 获取创建日期
     *
     * @return GSRD_CR_DATE - 创建日期
     */
    public String getGsrdCrDate() {
        return gsrdCrDate;
    }

    /**
     * 设置创建日期
     *
     * @param gsrdCrDate 创建日期
     */
    public void setGsrdCrDate(String gsrdCrDate) {
        this.gsrdCrDate = gsrdCrDate;
    }

    /**
     * 获取创建时间
     *
     * @return GSRD_CR_TIME - 创建时间
     */
    public String getGsrdCrTime() {
        return gsrdCrTime;
    }

    /**
     * 设置创建时间
     *
     * @param gsrdCrTime 创建时间
     */
    public void setGsrdCrTime(String gsrdCrTime) {
        this.gsrdCrTime = gsrdCrTime;
    }

    /**
     * 获取创建人
     *
     * @return GSRD_CR_ID - 创建人
     */
    public String getGsrdCrId() {
        return gsrdCrId;
    }

    /**
     * 设置创建人
     *
     * @param gsrdCrId 创建人
     */
    public void setGsrdCrId(String gsrdCrId) {
        this.gsrdCrId = gsrdCrId;
    }
}