package com.gys.business.mapper.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

/**
 * 应收核销清单表(GaiaBillOfArCancel)实体类
 *
 * @author makejava
 * @since 2021-09-18 15:20:50
 */
public class GaiaBillOfArCancel implements Serializable {
    private static final long serialVersionUID = 564296896204271433L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 仓库编码
     */
    private String dcCode;
    /**
     * 回款单号
     */
    private String arBillCode;
    /**
     * 核销金额
     */
    private BigDecimal arAmt;
    /**
     * 发生日期
     */
    private Date arMethodDate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 客户自编码
     */
    private String cusSelfCode;
    /**
     * 客户名称
     */
    private String cusName;
    /**
     * 业务单号
     */
    private String matDnId;

    /**
     * 业务单号
     */
    private String matPoId;
    /**
     * 已开票金额
     */
    private BigDecimal invoiceAmt;
    /**
     * 已结款金额
     */
    private BigDecimal settlementAmt;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;

    public String getMatDnId() {
        return matDnId;
    }

    public void setMatDnId(String matDnId) {
        this.matDnId = matDnId;
    }

    public String getMatPoId() {
        return matPoId;
    }

    public void setMatPoId(String matPoId) {
        this.matPoId = matPoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDcCode() {
        return dcCode;
    }

    public void setDcCode(String dcCode) {
        this.dcCode = dcCode;
    }

    public String getArBillCode() {
        return arBillCode;
    }

    public void setArBillCode(String arBillCode) {
        this.arBillCode = arBillCode;
    }

    public BigDecimal getArAmt() {
        return arAmt;
    }

    public void setArAmt(BigDecimal arAmt) {
        this.arAmt = arAmt;
    }

    public Date getArMethodDate() {
        return arMethodDate;
    }

    public void setArMethodDate(Date arMethodDate) {
        this.arMethodDate = arMethodDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCusSelfCode() {
        return cusSelfCode;
    }

    public void setCusSelfCode(String cusSelfCode) {
        this.cusSelfCode = cusSelfCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getInvoiceAmt() {
        return invoiceAmt;
    }

    public void setInvoiceAmt(BigDecimal invoiceAmt) {
        this.invoiceAmt = invoiceAmt;
    }

    public BigDecimal getSettlementAmt() {
        return settlementAmt;
    }

    public void setSettlementAmt(BigDecimal settlementAmt) {
        this.settlementAmt = settlementAmt;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}
