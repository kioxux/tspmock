package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMemberGrade;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

/**
 * @Author ：gyx
 * @Date ：Created in 14:07 2021/11/19
 * @Description：会员活跃度mapper
 * @Modified By：gyx
 * @Version:
 */
public interface GaiaSdMemberGradeMapper extends BaseMapper<GaiaSdMemberGrade> {
    List<GaiaSdMemberGrade> selectMemberActiveDataFromSale(String client);

    void batchInsertMemberGradeData(List<GaiaSdMemberGrade> memberGradeDataList);

    void deleteAll();
}
