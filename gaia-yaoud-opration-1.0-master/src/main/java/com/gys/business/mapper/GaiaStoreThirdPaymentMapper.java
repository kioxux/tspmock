package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaStoreThirdPayment;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.Map;

/**
 * GaiaStoreThirdPaymentMapper继承基类
 */
public interface GaiaStoreThirdPaymentMapper {

    List<GaiaStoreThirdPayment> getAllByClient(@Param("client") String client);
    List<GaiaStoreThirdPayment> selectValue(Map map);
}