//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_RECHARGE_CARD_CHANGE"
)
public class GaiaSdRechargeCardChange implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSRCC_VOUCHER_ID"
    )
    private String gsrccVoucherId;
    @Column(
            name = "GSRCC_STATUS"
    )
    private String gsrccStatus;
    @Column(
            name = "GSRCC_BR_ID"
    )
    private String gsrccBrId;
    @Column(
            name = "GSRCC_DATE"
    )
    private String gsrccDate;
    @Column(
            name = "GSRCC_TIME"
    )
    private String gsrccTime;
    @Column(
            name = "GSRCC_EMP"
    )
    private String gsrccEmp;
    @Column(
            name = "GSRCC_OLD_CARD_ID"
    )
    private String gsrccOldCardId;
    @Column(
            name = "GSRCC_NEW_CARD_ID"
    )
    private String gsrccNewCardId;
    @Column(
            name = "GSRCC_REMARK"
    )
    private String gsrccRemark;
    private static final long serialVersionUID = 1L;

    public GaiaSdRechargeCardChange() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsrccVoucherId() {
        return this.gsrccVoucherId;
    }

    public void setGsrccVoucherId(String gsrccVoucherId) {
        this.gsrccVoucherId = gsrccVoucherId;
    }

    public String getGsrccStatus() {
        return this.gsrccStatus;
    }

    public void setGsrccStatus(String gsrccStatus) {
        this.gsrccStatus = gsrccStatus;
    }

    public String getGsrccBrId() {
        return this.gsrccBrId;
    }

    public void setGsrccBrId(String gsrccBrId) {
        this.gsrccBrId = gsrccBrId;
    }

    public String getGsrccDate() {
        return this.gsrccDate;
    }

    public void setGsrccDate(String gsrccDate) {
        this.gsrccDate = gsrccDate;
    }

    public String getGsrccTime() {
        return this.gsrccTime;
    }

    public void setGsrccTime(String gsrccTime) {
        this.gsrccTime = gsrccTime;
    }

    public String getGsrccEmp() {
        return this.gsrccEmp;
    }

    public void setGsrccEmp(String gsrccEmp) {
        this.gsrccEmp = gsrccEmp;
    }

    public String getGsrccOldCardId() {
        return this.gsrccOldCardId;
    }

    public void setGsrccOldCardId(String gsrccOldCardId) {
        this.gsrccOldCardId = gsrccOldCardId;
    }

    public String getGsrccNewCardId() {
        return this.gsrccNewCardId;
    }

    public void setGsrccNewCardId(String gsrccNewCardId) {
        this.gsrccNewCardId = gsrccNewCardId;
    }

    public String getGsrccRemark() {
        return this.gsrccRemark;
    }

    public void setGsrccRemark(String gsrccRemark) {
        this.gsrccRemark = gsrccRemark;
    }
}
