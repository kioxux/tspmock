package com.gys.business.mapper;

import com.gys.business.mapper.entity.RecommendFirstPalnSto;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 首推商品方案门店设置表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-08-04
 */
@Mapper
public interface RecommendFirstPalnStoMapper extends BaseMapper<RecommendFirstPalnSto> {
}
