package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_DISEASE_CLASSIFCATION")
public class GaiaDiseaseClassifcation implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    private Integer id;

    /**
     * 疾病编码
     */
    @Column(name = "DISEASE_CODE")
    private String diseaseCode;

    /**
     * 疾病
     */
    @Column(name = "DISEASE")
    private String disease;

    /**
     * 疾病说明
     */
    @Column(name = "DISEASE_DESCRIPTION")
    private String diseaseDescription;

    /**
     * 成分细类编码
     */
    @Column(name = "CLASSIFICATION_CODE")
    private String classificationCode;

    /**
     * 商品明细类成分名
     */
    @Column(name = "INGREDIENT_CLASSIFICATION")
    private String ingredientClassification;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取疾病编码
     *
     * @return DISEASE_CODE - 疾病编码
     */
    public String getDiseaseCode() {
        return diseaseCode;
    }

    /**
     * 设置疾病编码
     *
     * @param diseaseCode 疾病编码
     */
    public void setDiseaseCode(String diseaseCode) {
        this.diseaseCode = diseaseCode;
    }

    /**
     * 获取疾病
     *
     * @return DISEASE - 疾病
     */
    public String getDisease() {
        return disease;
    }

    /**
     * 设置疾病
     *
     * @param disease 疾病
     */
    public void setDisease(String disease) {
        this.disease = disease;
    }

    /**
     * 获取疾病说明
     *
     * @return DISEASE_DESCRIPTION - 疾病说明
     */
    public String getDiseaseDescription() {
        return diseaseDescription;
    }

    /**
     * 设置疾病说明
     *
     * @param diseaseDescription 疾病说明
     */
    public void setDiseaseDescription(String diseaseDescription) {
        this.diseaseDescription = diseaseDescription;
    }

    /**
     * 获取成分细类编码
     *
     * @return CLASSIFICATION_CODE - 成分细类编码
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * 设置成分细类编码
     *
     * @param classificationCode 成分细类编码
     */
    public void setClassificationCode(String classificationCode) {
        this.classificationCode = classificationCode;
    }

    /**
     * 获取商品明细类成分名
     *
     * @return INGREDIENT_CLASSIFICATION - 商品明细类成分名
     */
    public String getIngredientClassification() {
        return ingredientClassification;
    }

    /**
     * 设置商品明细类成分名
     *
     * @param ingredientClassification 商品明细类成分名
     */
    public void setIngredientClassification(String ingredientClassification) {
        this.ingredientClassification = ingredientClassification;
    }
}