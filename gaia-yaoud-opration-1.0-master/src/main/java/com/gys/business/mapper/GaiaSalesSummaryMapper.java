package com.gys.business.mapper;

import com.gys.business.mapper.entity.*;
import com.gys.business.service.data.ReportForms.StoreSaleDateInData;
import com.gys.business.service.data.SalesSummaryData;
import com.gys.business.service.data.SelectData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author xiaoyuan
 */
public interface GaiaSalesSummaryMapper extends BaseMapper<GaiaSalesSummary> {

    /**
     * 根据条件查询结果
     * @param summaryData
     * @return
     */
    List<GaiaSalesSummary> findSalesSummary(SalesSummaryData summaryData);

    /**
     * 根据加盟号,地址 查询员工
     * @param client
     * @param depId
     * @return
     */
    List<GaiaUser> findUserByClientAndDepId(@Param("client")String client, @Param("depId")String depId);

    /**
     * 根据加盟号,地址 查询商品分类
     * @param client
     * @param depId
     * @return
     */
    List<GaiaCategories> findCategoriesByClientAndDepId(@Param("client")String client, @Param("depId")String depId);
    List<Map<String,Object>>findSalesSummaryByBrId(SalesSummaryData summaryData);

//    List<Map<String,Object>> getPayMsgTotal(SalesSummaryData summaryData);

    List<GaiaSalesPayMent>findPayMentList(SalesSummaryData summaryData);

    List<Map<String,Object>> selectStoreList(Map<String,Object> inData);

    List<Map<String,Object>> selectProBySaleD(Map<String, Object> inData);

    GaiaSalesSummaryTotal findSalesSummaryTotal(SalesSummaryData summaryData);

    List<String> salesgrade(@Param("client")String client, @Param("depId")String depId);

    List<String> salesZDY1(@Param("client")String client, @Param("depId")String depId);
    List<String> salesZDY2(@Param("client")String client, @Param("depId")String depId);
    List<String> salesZDY3(@Param("client")String client, @Param("depId")String depId);
    List<String> salesZDY4(@Param("client")String client, @Param("depId")String depId);
    List<String> salesZDY5(@Param("client")String client, @Param("depId")String depId);

    List<Map<String, Object>> findSalesSummaryByDate(StoreSaleDateInData summaryData);

    Map<String, Object> findSalesSummaryByTotal(StoreSaleDateInData inData);

    List<Map<String, Object>> selectProductUnit(@Param("brIdList")List<String> brIdList, @Param("startDate")String startDate,@Param("endDate") String endDate,@Param("client")String client);

    /**
     * 获取弹出率和弹出次数
     * @return
     */
    List<Map<String, Object>> selectPopUpData(@Param("brIdList") List<String> brIdList,@Param("startDate")String startDate, @Param("endDate")String endDate,@Param("client") String client);

    /**
     * 获取关联成交次数 成交率 关联销售额 关联销售占比
     * @param brIds
     * @param startDate
     * @param endDate
     * @param client
     * @return
     */
    List<Map<String, Object>> selectPopUpBrData(@Param("brIdList")List<String> brIds,@Param("queryStartDate") String startDate, @Param("queryEndDate")String endDate, @Param("client")String client);

    /**
     * 获取成交率
     * @param brIdList
     * @param startDate
     * @param endDate
     * @param client
     * @return
     */
    List<Map<String, Object>> selectClosingData(@Param("brIdList")List<String> brIdList, @Param("queryStartDate")String startDate, @Param("queryEndDate")String endDate, @Param("client")String client);

    /**
     * 获取这个用户下的门店
     * @param client 加盟商
     * @param userId 用户
     * @return
     */
    List<String> getByUserSites(@Param("client")String client, @Param("userId")String userId);

    List<String> selectAuthStoreList(@Param("client")String client, @Param("userId")String userId);
}
