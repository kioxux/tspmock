package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdNewStoreDistributionRecordH;

import java.math.BigDecimal;
import java.util.List;

import com.gys.business.service.data.GaiaDictionary;
import com.gys.business.service.data.NewStoreDistributionBaseInData;
import com.gys.business.service.data.NewStoreDistributionOutData;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdNewStoreDistributionRecordHMapper {
    /**
     * delete by primary key
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(GaiaSdNewStoreDistributionRecordH record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(GaiaSdNewStoreDistributionRecordH record);

    /**
     * select by primary key
     * @param id primary key
     * @return object by primary key
     */
    GaiaSdNewStoreDistributionRecordH selectByPrimaryKey(Long id);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(GaiaSdNewStoreDistributionRecordH record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(GaiaSdNewStoreDistributionRecordH record);

    int updateBatch(List<GaiaSdNewStoreDistributionRecordH> list);

    int batchInsert(@Param("list") List<GaiaSdNewStoreDistributionRecordH> list);

    int replaceInsert(GaiaSdNewStoreDistributionRecordH record);

    int deleteByCondition(GaiaSdNewStoreDistributionRecordH record);

    String getLastMonth();

    BigDecimal getDrugsProportion(@Param("client") String client);

    List<GaiaDictionary> getAllDictionary();

    List<GaiaDictionary> getDictionaryByType(@Param("type") String type);

    List<NewStoreDistributionOutData> getCalculationList(NewStoreDistributionBaseInData inData);
}