package com.gys.business.mapper;

import com.gys.business.mapper.entity.TichengRejectPro;
import com.gys.business.service.data.percentageplan.PercentageProInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 销售提成方案剔除单品表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-11-12
 */
@Mapper
public interface TichengRejectProMapper extends BaseMapper<TichengRejectPro> {
    void deleteRejectProByPid(@Param("pid") Integer pid);
    void batchSaleRejectProList(List<PercentageProInData> list);
}
