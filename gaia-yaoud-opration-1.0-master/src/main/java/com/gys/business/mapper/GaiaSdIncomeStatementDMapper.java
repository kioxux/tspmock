package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIncomeStatementD;
import com.gys.business.service.data.IncomeStatementDetailOutData;
import com.gys.business.service.data.IncomeStatementInData;
import com.gys.business.service.data.ProfitAndLossRecordInData;
import com.gys.business.service.data.ProfitAndLossRecordOutData;
import com.gys.common.YiLianDaModel.YPSYXX.SymxlbParam;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdIncomeStatementDMapper extends BaseMapper<GaiaSdIncomeStatementD> {
    List<IncomeStatementDetailOutData> getIncomeStatementDetailList(IncomeStatementInData inData);


    /**
     * 报损报溢 报表查询
     * @param inData
     * @return
     */
    List<ProfitAndLossRecordOutData> conditionQuery(ProfitAndLossRecordInData inData);

    List<SymxlbParam> getIncomeStatementByYLZ(Map<String,String> map);

    int batchInsert(@Param("list") List<GaiaSdIncomeStatementD> list);

}
