package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SALETASK_HPLAN_SYSTEM")
@Data
public class GaiaSaletaskHplanSystem implements Serializable {
    /**
     * 任务ID
     */
    @Id
    @Column(name = "STASK_TASK_ID")
    @ApiModelProperty(value = "任务ID")
    private String staskTaskId;

    /**
     * 计划月份
     */
    @Id
    @Column(name = "MONTH_PLAN")
    @ApiModelProperty(value = "计划月份")
    private String monthPlan;

    /**
     * 任务名称
     */
    @Column(name = "STASK_TASK_NAME")
    @ApiModelProperty(value = "任务名称")
    private String staskTaskName;

    /**
     * 起始日期
     */
    @Column(name = "STASK_START_DATE")
    @ApiModelProperty(value = "起始日期")
    private String staskStartDate;

    /**
     * 结束日期
     */
    @Column(name = "STASK_END_DATE")
    @ApiModelProperty(value = "结束日期")
    private String staskEndDate;

    /**
     * 同比月份
     */
    @Column(name = "YOY_MONTH")
    @ApiModelProperty(value = "同比月份")
    private String yoyMonth;

    /**
     * 环比月份
     */
    @Column(name = "MOM_MONTH")
    @ApiModelProperty(value = "环比月份")
    private String momMonth;

    /**
     * 期间天数
     */
//    @Column(name = "MONTH_DAYS")
//    @ApiModelProperty(value = "期间天数")
//    private Integer monthDays;

    /**
     * 营业额同比增长率
     */
    @Column(name = "YOY_SALE_AMT")
    @ApiModelProperty(value = "营业额同比增长率")
    private BigDecimal yoySaleAmt;

    /**
     * 营业额环比增长率
     */
    @Column(name = "MOM_SALE_AMT")
    @ApiModelProperty(value = "营业额环比增长率")
    private BigDecimal momSaleAmt;

    /**
     * 毛利额同比增长率
     */
    @Column(name = "YOY_SALE_GROSS")
    @ApiModelProperty(value = "毛利额同比增长率")
    private BigDecimal yoySaleGross;

    /**
     * 毛利额环比增长率
     */
    @Column(name = "MOM_SALE_GROSS")
    @ApiModelProperty(value = "毛利额环比增长率")
    private BigDecimal momSaleGross;

    /**
     * 会员卡同比增长率
     */
    @Column(name = "YOY_MCARD_QTY")
    @ApiModelProperty(value = "会员卡同比增长率")
    private BigDecimal yoyMcardQty;

    /**
     * 会员卡环比增长率
     */
    @Column(name = "MOM_MCARD_QTY")
    @ApiModelProperty(value = "会员卡环比增长率")
    private BigDecimal momMcardQty;

    /**
     * 任务状态(S已保存，P已下发)
     */
    @Column(name = "STASK_SPLAN_STA")
    @ApiModelProperty(value = "任务状态(S已保存，P已下发)")
    private String staskSplanSta;

    /**
     * 创建人账号
     */
    @Column(name = "STASK_CRE_ID")
    @ApiModelProperty(value = "创建人账号")
    private String staskCreId;

    /**
     * 创建日期
     */
    @Column(name = "STASK_CRE_DATE")
    @ApiModelProperty(value = "创建日期")
    private String staskCreDate;

    /**
     * 创建时间
     */
    @Column(name = "STASK_CRE_TIME")
    @ApiModelProperty(value = "创建时间")
    private String staskCreTime;

    /**
     * 修改人
     */
    @Column(name = "STASK_MD_ID")
    @ApiModelProperty(value = "修改人")
    private String staskMdId;

    /**
     * 修改日期
     */
    @Column(name = "STASK_MD_DATE")
    @ApiModelProperty(value = "修改日期")
    private String staskMdDate;

    /**
     * 修改时间
     */
    @Column(name = "STASK_MD_TIME")
    @ApiModelProperty(value = "修改时间")
    private String staskMdTime;

    /**
     * 删除 0 未删除 1已删除
     */
    @Column(name = "STASK_TYPE")
    @ApiModelProperty(value = "删除 0 未删除 1已删除")
    private String staskType;

    /**
     * 营业额
     */
//    @Column(name = "SALE_AMT")
//    @ApiModelProperty(value = "营业额")
//    private BigDecimal saleAmt;

    /**
     * 毛利额
     */
//    @Column(name = "SALE_GROSS")
//    @ApiModelProperty(value = "毛利额")
//    private BigDecimal saleGross;

    /**
     * 会员卡
     */
//    @Column(name = "MCARD_QTY")
//    @ApiModelProperty(value = "会员卡")
//    private BigDecimal mcardQty;

    /**
     * 推送类型 1 立即推送 2 定时推送
     */
    @Column(name = "PUSH_TYPE")
    @ApiModelProperty(value = "推送类型 1 立即推送 2 定时推送")
    private String pushType;

    /**
     * 推送日期
     */
    @Column(name = "PUSH_DATE")
    @ApiModelProperty(value = "推送日期")
    private String pushDate;

    /**
     * 推送时间
     */
//    @Column(name = "PUSH_TIME")
//    @ApiModelProperty(value = "推送时间")
//    private String pushTime;

    /**
     * 推送状态 0 未推送 1 已推送
     */
    @Column(name = "PUSH_STATUS")
    @ApiModelProperty(value = "推送状态 0 未推送 1 已推送")
    private String pushStatus;
}