//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdHandover;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GaiaSdHandoverMapper extends BaseMapper<GaiaSdHandover> {

    Integer checkPassword(CashboxInData inData);

    void save(CashboxInData inData);

    CashboxOutData thePreviousData(CashboxInData inData);

    /**
     * 通过时间区间获取列表
     * @param inData
     * @return
     */
    List<CashboxOutData> queryList(CashboxInData inData);


    /**
     * 通过时间区间获取列表
     * @param inData
     * @return
     */
    List<GetUserOutData> queryUserList(CashboxInData inData);
}
