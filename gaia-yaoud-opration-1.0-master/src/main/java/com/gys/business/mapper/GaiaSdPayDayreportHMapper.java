package com.gys.business.mapper;

import com.gys.business.service.data.ReportForms.PayDayreportInData;

import java.util.List;
import java.util.Map;

public interface GaiaSdPayDayreportHMapper {
    List<Map<String,Object>> selectPayDayreport(PayDayreportInData inData);
}
