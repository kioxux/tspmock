package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(
   name = "GAIA_WF_RECORD"
)
@Data
public class GaiaWfRecord implements Serializable {
   @Id
   @Column(
      name = "CLIENT"
   )
   private String client;
   @Id
   @Column(
      name = "PO_COMPANY_CODE"
   )
   private String poCompanyCode;
   @Id
   @Column(
      name = "WF_CODE"
   )
   private String wfCode;
   @Column(
      name = "WF_DEFINE_CODE"
   )
   private String wfDefineCode;
   @Column(
      name = "WF_KIND"
   )
   private String wfKind;
   @Column(
      name = "WF_TITLE"
   )
   private String wfTitle;
   @Column(
      name = "WF_DESCRIPTION"
   )
   private String wfDescription;
   @Column(
      name = "WF_REF_PAGE"
   )
   private String wfRefPage;
   @Column(
      name = "WF_REF_RETURN"
   )
   private String wfRefReturn;
   @Column(
      name = "WF_ORDER"
   )
   private String wfOrder;
   @Column(
      name = "WF_SEQ"
   )
   private BigDecimal wfSeq;
   @Column(
      name = "WF_STATUS"
   )
   private String wfStatus;
   @Column(
      name = "WF_JSON_STRING"
   )
   private String wfJsonString;
   @Column(
           name = "WF_NEW_WORKFLOW_DETAIL"
   )
   private String wfNewWorkflowDetail;
   @Column(
      name = "WF_SITE"
   )
   private String wfSite;
   @Column(
      name = "CREATE_USER"
   )
   private String createUser;
   @Column(
      name = "CREATE_TIME"
   )
   private String createTime;
   private static final long serialVersionUID = 1L;
}
