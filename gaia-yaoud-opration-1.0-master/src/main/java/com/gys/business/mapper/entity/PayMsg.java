package com.gys.business.mapper.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
//@JSONType(orders={"amt","brId","billNo","cardNo","client","date","id","name","rmbAmt","type","zlAmt"})
public class PayMsg {
    /**
     * 加盟商
     */
    @NotNull
    @JSONField(ordinal=5)
    private String client;

    /**
     * 店号
     */
    @NotNull
    @JSONField(ordinal=3)
    private String brId;

    /**
     * 日期
     */
    @NotNull
    @JSONField(ordinal=6)
    private String date;

    /**
     * 销售单号
     */
    @NotNull
    @JSONField(ordinal=2)
    private String billNo;

    /**
     * 支付编码
     */
    @NotNull
    @JSONField(ordinal=7)
    private String id;

    /**
     * 支付类型 :1.销售支付 2.储值卡充值 3.挂号支付 4.代销售
     */
    @JSONField(ordinal=10)
    private String type;

    /**
     * 支付名称
     */
    @JSONField(ordinal=8)
    private String name;

    /**
     * 支付卡号/订单流水号
     */
    @JSONField(ordinal=4)
    private String cardNo;

    /**
     * 支付金额
     */
    @NotNull
    @JSONField(ordinal=1)
    private BigDecimal amt;

    /**
     * RMB收款金额
     */
    @JSONField(ordinal=9)
    private BigDecimal rmbAmt;

    /**
     * 找零金额
     */
    @JSONField(ordinal=11)
    private BigDecimal zlAmt;

    private static final long serialVersionUID = 1L;


//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder("{");
//        sb.append(",\"amt\":")
//                .append(amt);
//        sb.append(",\"brId\":\"")
//                .append(brId).append('\"');
//        sb.append(",\"billNo\":\"")
//                .append(billNo).append('\"');
//        sb.append(",\"cardNo\":\"")
//                .append(cardNo).append('\"');
//        sb.append("\"client\":\"")
//                .append(client).append('\"');
//        sb.append(",\"date\":\"")
//                .append(date).append('\"');
//        sb.append(",\"id\":\"")
//                .append(id).append('\"');
//        sb.append(",\"name\":\"")
//                .append(name).append('\"');
//        sb.append(",\"rmbAmt\":")
//                .append(rmbAmt);
//        sb.append(",\"type\":\"")
//                .append(type).append('\"');
//        sb.append(",\"zlAmt\":")
//                .append(zlAmt);
//        sb.append('}');
//        return sb.toString();
//    }
}