package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSaleHandD;
import com.gys.business.service.data.RestOrderDetailOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdSaleHandDMapper {

    void insert(GaiaSdSaleHandD handD);

    void delete(@Param("clientId") String clientId, @Param("gsshBrId") String gsshBrId,@Param("gsshBillNo")  String gsshBillNo);

    void insertList(@Param("list") List<GaiaSdSaleHandD> copyList);

    List<RestOrderDetailOutData> getSaleHandDetail(@Param("client")String client, @Param("brId")String depId, @Param("gssdBillNo") String gssdBillNo, @Param("gssdDate") String gssdDate);

    List<GaiaSdSaleHandD> getAllByBillNo(@Param("client")String client, @Param("brId")String depId, @Param("gssdBillNo") String gssdBillNo);

    void updateList(@Param("list")  List<GaiaSdSaleHandD> handDList);

    List<RestOrderDetailOutData> listRestOrderDetail(@Param("client")String client, @Param("brId")String depId, @Param("gssdBillNo") String gssdBillNo, @Param("gssdDate") String gssdDate ,@Param("gsshHideFlag") String gsshHideFlag);

    List<GaiaSdSaleHandD> getHisHandOrderDetail(@Param("client")String client, @Param("brId")String brId, @Param("dsfOrderCode") String dsfOrderCode);

}
