package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 
 * 挂单主表
 */
@Data
public class GaiaSdSaleHandH implements Serializable {

    private static final long serialVersionUID = 4040286039395772063L;
    /**
     * 加盟商
     */
    private String clientId;

    /**
     * 销售单号
     */
    private String gsshBillNo;

    /**
     * 店名
     */
    private String gsshBrId;

    /**
     * 销售日期
     */
    private String gsshDate;

    /**
     * 销售时间
     */
    private String gsshTime;

    /**
     * 收银工号
     */
    private String gsshEmp;

    /**
     * 发票号
     */
    private String gsshTaxNo;

    /**
     * 会员卡号
     */
    private String gsshHykNo;

    /**
     * 零售价格
     */
    private BigDecimal gsshNormalAmt;

    /**
     * 折扣金额
     */
    private BigDecimal gsshZkAmt;

    /**
     * 应收金额
     */
    private BigDecimal gsshYsAmt;

    /**
     * 现金找零
     */
    private BigDecimal gsshRmbZlAmt;

    /**
     * 现金收银金额
     */
    private BigDecimal gsshRmbAmt;

    /**
     * 抵用券卡号
     */
    private String gsshDyqNo;

    /**
     * 抵用券金额
     */
    private BigDecimal gsshDyqAmt;

    /**
     * 抵用原因
     */
    private String gsshDyqType;

    /**
     * 储值卡号
     */
    private String gsshRechargeCardNo;

    /**
     * 储值卡消费金额
     */
    private BigDecimal gsshRechargeCardAmt;

    /**
     * 电子券充值卡号1
     */
    private String gsshDzqczActno1;

    /**
     * 电子券充值金额1
     */
    private BigDecimal gsshDzqczAmt1;

    /**
     * 电子券抵用卡号1
     */
    private String gsshDzqdyActno1;

    /**
     * 电子券抵用金额1
     */
    private BigDecimal gsshDzqdyAmt1;

    /**
     * 增加积分
     */
    private String gsshIntegralAdd;

    /**
     * 兑换积分
     */
    private String gsshIntegralExchange;

    /**
     * 	加价兑换积分金额
     */
    private BigDecimal gsshIntegralExchangeAmt;

    /**
     * 抵现积分
     */
    private String gsshIntegralCash;

    /**
     * 抵现金额
     */
    private BigDecimal gsshIntegralCashAmt;

    /**
     * 支付方式1
     */
    private String gsshPaymentNo1;

    /**
     * 支付金额1
     */
    private BigDecimal gsshPaymentAmt1;

    /**
     * 退货原销售单号
     */
    private String gsshBillNoReturn;

    /**
     * 退货审核人员
     */
    private String gsshEmpReturn;

    /**
     * 单据类型：T.外卖订单
     */
    private String gsshPromotionType1;

    /**
     * 参加促销活动类型2
     */
    private String gsshPromotionType2;

    /**
     * 参加促销活动类型3
     */
    private String gsshPromotionType3;

    /**
     * 参加促销活动类型4
     */
    private String gsshPromotionType4;

    /**
     * 参加促销活动类型5
     */
    private String gsshPromotionType5;

    /**
     * 移动收银单号/外卖单号
     */
    private String gsshRegisterVoucherId;

    /**
     * 代销售店号
     */
    private String gsshReplaceBrId;

    /**
     * 代销售营业员
     */
    private String gsshReplaceSalerId;

    /**
     * 是否挂起 1挂起 0不挂起
默认不挂起 2移动收银3作废
     */
    private String gsshHideFlag;

    /**
     * 是否允许调出销售 1为是，0为否
     */
    private String gsshCallAllow;

    /**
     * 移动销售码
     */
    private String gsshEmpGroupName;

    /**
     * 调用次数
     */
    private BigDecimal gsshCallQty;

    /**
     * 支付方式2
     */
    private String gsshPaymentNo2;

    /**
     * 支付金额2
     */
    private BigDecimal gsshPaymentAmt2;

    /**
     * 支付方式3
     */
    private String gsshPaymentNo3;

    /**
     * 支付金额3
     */
    private BigDecimal gsshPaymentAmt3;

    /**
     * 支付方式4
     */
    private String gsshPaymentNo4;

    /**
     * 支付金额4
     */
    private BigDecimal gsshPaymentAmt4;

    /**
     * 支付方式5
     */
    private String gsshPaymentNo5;

    /**
     * 支付金额5
     */
    private BigDecimal gsshPaymentAmt5;

    /**
     * 电子券抵用卡号2
     */
    private String gsshDzqdyActno2;

    /**
     * 电子券抵用金额2
     */
    private BigDecimal gsshDzqdyAmt2;

    /**
     * 电子券抵用卡号3
     */
    private String gsshDzqdyActno3;

    /**
     * 电子券抵用金额3
     */
    private BigDecimal gsshDzqdyAmt3;

    /**
     * 电子券抵用卡号4
     */
    private String gsshDzqdyActno4;

    /**
     * 电子券抵用金额4
     */
    private BigDecimal gsshDzqdyAmt4;

    /**
     * 销售备注
     */
    private String gsshDzqdyActno5;

    /**
     * 电子券抵用金额5
     */
    private BigDecimal gsshDzqdyAmt5;

    /**
     * 是否日结 0/空为否1为是
     */
    private String gsshDayreport;

    /**
     * 委托代煎供应商编号
     */
    private String gsshCrFlag;

    /**
     * 代煎状态 0未开方，1已完成，2已退货
     */
    private String gsshCrStatus;

    /**
     * 订单退货状态：0：已退货，1：医保退货完成, 2:医保退货失败
     */
    private String gsshReturnStatus;

    /**
     * 订单状态
     */
    private String saleHFlag;

    /**
     * 验证 挂单前状态
     */
    private String centralAndWestern;

    /**
     * 当前贴数
     */
    private String herbalNum;
    /**
     * 4-his订单 5-门店代售
     */
    private String gsshBillType;
    /**
     * 配送单完成状态（0-否；1-是）
     */
    private String gsshBdFlag;
    /**
     * 订单类型， 0-本地pos销售单；1-中药代煎单；2-HIS门诊单
     */
    private String gsshOrderSource;

    private String gsshHideRemark;
}