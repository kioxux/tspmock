package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDcReplenishD;
import com.gys.business.service.data.DcReplenishDetailVO;
import com.gys.business.service.data.DcReplenishForm;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/21 16:36
 */
public interface GaiaDcReplenishDMapper {

    GaiaDcReplenishD getById(Long id);

    int add(GaiaDcReplenishD gaiaDcReplenishD);

    List<DcReplenishDetailVO> getReplenishDetail(DcReplenishForm replenishForm);
}
