//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromAssoConds;
import com.gys.business.service.data.PromAssoCondsOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdPromAssoCondsMapper extends BaseMapper<GaiaSdPromAssoConds> {
    List<PromAssoCondsOutData> getDetail(PromoteInData inData);
}
