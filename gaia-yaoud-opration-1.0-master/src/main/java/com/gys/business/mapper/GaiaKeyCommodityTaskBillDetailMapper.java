package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBillDetail;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillDetailVO;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillInData;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillRelationInData;
import com.gys.business.service.data.LevelInfoOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 重点商品任务—明细表(GaiaKeyCommodityTaskBillDetail)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-01 15:38:29
 */
public interface GaiaKeyCommodityTaskBillDetailMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaKeyCommodityTaskBillDetail queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaKeyCommodityTaskBillDetail> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaKeyCommodityTaskBillDetail 实例对象
     * @return 对象列表
     */
    List<GaiaKeyCommodityTaskBillDetail> queryAll(GaiaKeyCommodityTaskBillDetail gaiaKeyCommodityTaskBillDetail);

    /**
     * 新增数据
     *
     * @param gaiaKeyCommodityTaskBillDetail 实例对象
     * @return 影响行数
     */
    int insert(GaiaKeyCommodityTaskBillDetail gaiaKeyCommodityTaskBillDetail);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaKeyCommodityTaskBillDetail> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaKeyCommodityTaskBillDetail> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaKeyCommodityTaskBillDetail> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaKeyCommodityTaskBillDetail> entities);

    /**
     * 修改数据
     *
     * @param gaiaKeyCommodityTaskBillDetail 实例对象
     * @return 影响行数
     */
    int update(GaiaKeyCommodityTaskBillDetail gaiaKeyCommodityTaskBillDetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    List<LevelInfoOutData> listLevelInfo(GaiaKeyCommodityTaskBillRelationInData inData);

    List<GaiaKeyCommodityTaskBillDetailVO> listStoreLevelInfo(GaiaKeyCommodityTaskBillRelationInData inData);

    List<GaiaKeyCommodityTaskBillDetail> listInfo(GaiaKeyCommodityTaskBillInData inData);

    int getCount(GaiaKeyCommodityTaskBillInData param);

}

