package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdWtpsdD;
import com.gys.common.base.BaseMapper;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/14 19:36
 */
public interface GaiaSdWtpsdDMapper extends BaseMapper<GaiaSdWtpsdD> {
}
