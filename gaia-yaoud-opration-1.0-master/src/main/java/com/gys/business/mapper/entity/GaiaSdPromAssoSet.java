//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 组合类促销系列编码设置表
 */
@Table(name = "GAIA_SD_PROM_ASSO_SET")
@Data
public class GaiaSdPromAssoSet implements Serializable {
    private static final long serialVersionUID = -5340185096869441495L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPAS_VOUCHER_ID")
    private String gspasVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPAS_SERIAL")
    private String gspasSerial;

    /**
     * 系列编码
     */
    @Column(name = "GSPAS_SERIES_ID")
    private String gspasSeriesId;

    /**
     * 组合金额
     */
    @Column(name = "GSPAS_AMT")
    private BigDecimal gspasAmt;

    /**
     * 组合折扣
     */
    @Column(name = "GSPAS_REBATE")
    private String gspasRebate;

    /**
     * 赠送数量
     */
    @Column(name = "GSPAS_QTY")
    private String gspasQty;
}
