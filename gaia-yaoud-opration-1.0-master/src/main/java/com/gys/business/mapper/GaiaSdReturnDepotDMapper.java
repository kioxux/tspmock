//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdReturnDepotD;
import com.gys.business.service.data.DelegationReturn.VoucherCount;
import com.gys.business.service.data.GetDepotDetailOutData;
import com.gys.business.service.data.GetDepotInData;
import com.gys.common.base.BaseMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.ibatis.annotations.Param;


public interface GaiaSdReturnDepotDMapper extends BaseMapper<GaiaSdReturnDepotD> {
    List<GetDepotDetailOutData> detailList(GetDepotInData inData);

    List<VoucherCount> checkReturnDepotDCount(Map map);

    void updateReviseBatch(List<GaiaSdReturnDepotD> returnDepotDList);
}
