//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_EXAMINE_H"
)
public class GaiaSdExamineH implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSEH_VOUCHER_ID"
    )
    private String gsehVoucherId;
    @Column(
            name = "GSEH_BR_ID"
    )
    private String gsehBrId;
    @Column(
            name = "GSEH_DATE"
    )
    private String gsehDate;
    @Column(
            name = "GSEH_VOUCHER_ACCEPT_ID"
    )
    private String gsehVoucherAcceptId;
    @Column(
            name = "GSEH_FROM"
    )
    private String gsehFrom;
    @Column(
            name = "GSEH_TO"
    )
    private String gsehTo;
    @Column(
            name = "GSEH_TYPE"
    )
    private String gsehType;
    @Column(
            name = "GSEH_STATUS"
    )
    private String gsehStatus;
    @Column(
            name = "GSEH_TOTAL_AMT"
    )
    private BigDecimal gsehTotalAmt;
    @Column(
            name = "GSEH_TOTAL_QTY"
    )
    private String gsehTotalQty;
    @Column(
            name = "GSEH_REMAKS"
    )
    private String gsehRemaks;
    private static final long serialVersionUID = 1L;

    public GaiaSdExamineH() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsehVoucherId() {
        return this.gsehVoucherId;
    }

    public void setGsehVoucherId(String gsehVoucherId) {
        this.gsehVoucherId = gsehVoucherId;
    }

    public String getGsehBrId() {
        return this.gsehBrId;
    }

    public void setGsehBrId(String gsehBrId) {
        this.gsehBrId = gsehBrId;
    }

    public String getGsehDate() {
        return this.gsehDate;
    }

    public void setGsehDate(String gsehDate) {
        this.gsehDate = gsehDate;
    }

    public String getGsehVoucherAcceptId() {
        return this.gsehVoucherAcceptId;
    }

    public void setGsehVoucherAcceptId(String gsehVoucherAcceptId) {
        this.gsehVoucherAcceptId = gsehVoucherAcceptId;
    }

    public String getGsehFrom() {
        return this.gsehFrom;
    }

    public void setGsehFrom(String gsehFrom) {
        this.gsehFrom = gsehFrom;
    }

    public String getGsehTo() {
        return this.gsehTo;
    }

    public void setGsehTo(String gsehTo) {
        this.gsehTo = gsehTo;
    }

    public String getGsehType() {
        return this.gsehType;
    }

    public void setGsehType(String gsehType) {
        this.gsehType = gsehType;
    }

    public String getGsehStatus() {
        return this.gsehStatus;
    }

    public void setGsehStatus(String gsehStatus) {
        this.gsehStatus = gsehStatus;
    }

    public BigDecimal getGsehTotalAmt() {
        return this.gsehTotalAmt;
    }

    public void setGsehTotalAmt(BigDecimal gsehTotalAmt) {
        this.gsehTotalAmt = gsehTotalAmt;
    }

    public String getGsehTotalQty() {
        return this.gsehTotalQty;
    }

    public void setGsehTotalQty(String gsehTotalQty) {
        this.gsehTotalQty = gsehTotalQty;
    }

    public String getGsehRemaks() {
        return this.gsehRemaks;
    }

    public void setGsehRemaks(String gsehRemaks) {
        this.gsehRemaks = gsehRemaks;
    }
}
