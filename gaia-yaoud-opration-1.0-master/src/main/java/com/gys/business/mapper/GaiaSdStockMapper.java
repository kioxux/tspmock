package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdBatchChange;
import com.gys.business.mapper.entity.GaiaSdStock;
import com.gys.business.mapper.entity.MemberPromotionInData;
import com.gys.business.service.data.ClientStockOutData;
import com.gys.business.service.data.GetQueryProductInData;
import com.gys.business.service.data.StockInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdStockMapper extends BaseMapper<GaiaSdStock> {
    void consumeStoreStock(StockInData stockInData);

    /**
     * 批量更新
     * @param stockList
     */
    void updateList(List<GaiaSdStock> stockList);

    /**
     * 消耗库存
     * @param stockInData
     */
    void consumeShopStockQty(StockInData stockInData);

    /**
     * 增加库存
     * @param stockInData
     */
    void addShopStock(StockInData stockInData);

    /**
     * 批量减库存
     * @param list
     */
    void updateQtyList(List<StockInData> list);

    String getStoreStock(StockInData stockInData);

    List<GaiaSdStock> selectStocks(List<StockInData> inData);

    List<GaiaSdStock> selectTitleStock(StockInData inData);

    /**
     * 验证库存
     * @param inData
     * @return
     */
    GaiaSdStock verifyInventory(MemberPromotionInData inData);

    List<ClientStockOutData> queryClientStock(GetQueryProductInData inData);

    List<ClientStockOutData> queryClientStockByBatchNo(GetQueryProductInData inData);

    List<GaiaSdStock> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);

    List<GaiaSdStock> getAllByProIds(MemberPromotionInData inData);

    List<GaiaSdStock> findByClientAndGssBrIdAndGssProId(@Param("client") String client, @Param("gssBrId") String gssBrId, List<GaiaSdBatchChange> list);

    List<GaiaSdStock> findByClientAndGssBrIdAndGssProIdIn(@Param("client") String client, @Param("gssBrId") String gssBrId, List<String> list);

    List<GaiaSdStock> findByClientAndGssBrId(@Param("client") String client, @Param("gssBrId") String gssBrId);

    List<GaiaSdStock> findAllByStockInData(List<StockInData> list);
}
