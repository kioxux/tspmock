package com.gys.business.mapper.yaojian;

import com.gys.business.mapper.entity.GaiaSdSystemPara;
import com.gys.business.yaojian.dto.DongRuanDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2022/1/20 15:29
 */
@Mapper
public interface YaojianMapper {
    /**
     * 获取商品to东软
     * @param request
     * @return
     */
    List<Map<String,Object>> getProductForDR(DongRuanDTO request);
    List<Map<String,Object>> getStockForDR(DongRuanDTO request);
    List<Map<String,Object>> getOrderListForDR(DongRuanDTO request);
    List<GaiaSdSystemPara> getConfigForDR();
 }
