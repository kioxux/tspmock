//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_ALLOT_MUTUAL_H"
)
public class GaiaSdAllotMutualH implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSAMH_VOUCHER_ID"
    )
    private String gsamhVoucherId;
    @Column(
            name = "GSAMH_BR_ID"
    )
    private String gsamhBrId;
    @Column(
            name = "GSAMH_DATE"
    )
    private String gsamhDate;
    @Column(
            name = "GSAMH_FINISH_DATE"
    )
    private String gsamhFinishDate;
    @Column(
            name = "GSAMH_FROM"
    )
    private String gsamhFrom;
    @Column(
            name = "GSAMH_TO"
    )
    private String gsamhTo;
    @Column(
            name = "GSAMH_TYPE"
    )
    private String gsamhType;
    @Column(
            name = "GSAMH_STATUS"
    )
    private String gsamhStatus;
    @Column(
            name = "GSAMH_TOTAL_AMT"
    )
    private BigDecimal gsamhTotalAmt;
    @Column(
            name = "GSAMH_TOTAL_QTY"
    )
    private String gsamhTotalQty;
    @Column(
            name = "GSAMH_EMP"
    )
    private String gsamhEmp;
    @Column(
            name = "GSAMH_EMP1"
    )
    private String gsamhEmp1;
    @Column(
            name = "GSAMH_INVOICES_ID"
    )
    private String gsamhInvoicesId;
    @Column(
            name = "GSAMH_PROCEDURE"
    )
    private String gsamhProcedure;
    private static final long serialVersionUID = 1L;

    public GaiaSdAllotMutualH() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsamhVoucherId() {
        return this.gsamhVoucherId;
    }

    public void setGsamhVoucherId(String gsamhVoucherId) {
        this.gsamhVoucherId = gsamhVoucherId;
    }

    public String getGsamhBrId() {
        return this.gsamhBrId;
    }

    public void setGsamhBrId(String gsamhBrId) {
        this.gsamhBrId = gsamhBrId;
    }

    public String getGsamhDate() {
        return this.gsamhDate;
    }

    public void setGsamhDate(String gsamhDate) {
        this.gsamhDate = gsamhDate;
    }

    public String getGsamhFinishDate() {
        return this.gsamhFinishDate;
    }

    public void setGsamhFinishDate(String gsamhFinishDate) {
        this.gsamhFinishDate = gsamhFinishDate;
    }

    public String getGsamhFrom() {
        return this.gsamhFrom;
    }

    public void setGsamhFrom(String gsamhFrom) {
        this.gsamhFrom = gsamhFrom;
    }

    public String getGsamhTo() {
        return this.gsamhTo;
    }

    public void setGsamhTo(String gsamhTo) {
        this.gsamhTo = gsamhTo;
    }

    public String getGsamhType() {
        return this.gsamhType;
    }

    public void setGsamhType(String gsamhType) {
        this.gsamhType = gsamhType;
    }

    public String getGsamhStatus() {
        return this.gsamhStatus;
    }

    public void setGsamhStatus(String gsamhStatus) {
        this.gsamhStatus = gsamhStatus;
    }

    public BigDecimal getGsamhTotalAmt() {
        return this.gsamhTotalAmt;
    }

    public void setGsamhTotalAmt(BigDecimal gsamhTotalAmt) {
        this.gsamhTotalAmt = gsamhTotalAmt;
    }

    public String getGsamhTotalQty() {
        return this.gsamhTotalQty;
    }

    public void setGsamhTotalQty(String gsamhTotalQty) {
        this.gsamhTotalQty = gsamhTotalQty;
    }

    public String getGsamhEmp() {
        return this.gsamhEmp;
    }

    public void setGsamhEmp(String gsamhEmp) {
        this.gsamhEmp = gsamhEmp;
    }

    public String getGsamhEmp1() {
        return this.gsamhEmp1;
    }

    public void setGsamhEmp1(String gsamhEmp1) {
        this.gsamhEmp1 = gsamhEmp1;
    }

    public String getGsamhInvoicesId() {
        return this.gsamhInvoicesId;
    }

    public void setGsamhInvoicesId(String gsamhInvoicesId) {
        this.gsamhInvoicesId = gsamhInvoicesId;
    }

    public String getGsamhProcedure() {
        return this.gsamhProcedure;
    }

    public void setGsamhProcedure(String gsamhProcedure) {
        this.gsamhProcedure = gsamhProcedure;
    }
}
