package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author flynn
 * @since 2021-09-08
 */
@Data
@Table( name = "GAIA_MESSAGE_TEMPLATE")
public class MessageTemplate implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "GMT_ID")
    @Id
    @Column(name = "GMT_ID")
    private String gmtId;

    @ApiModelProperty(value = "消息名称")
    @Column(name = "GMT_NAME")
    private String gmtName;

    @ApiModelProperty(value = "消息标题")
    @Column(name = "GMT_TITLE")
    private String gmtTitle;

    @ApiModelProperty(value = "消息内容")
    @Column(name = "GMT_CONTENT")
    private String gmtContent;

    @ApiModelProperty(value = "业务类型 1工作 2消息")
    @Column(name = "GMT_BUSINESS_TYPE")
    private Integer gmtBusinessType;

    @ApiModelProperty(value = "是否跳转 0不跳转 1挑转")
    @Column(name = "GMT_GO_PAGE")
    private Integer gmtGoPage;

    @ApiModelProperty(value = "消息类型 1付费 2公司维护 3精准营销")
    @Column(name = "GMT_TYPE")
    private Integer gmtType;

    @ApiModelProperty(value = "表示形式 1消息 2推送 3 消息+推送")
    @Column(name = "GMT_SHOW_TYPE")
    private Integer gmtShowType;

    @ApiModelProperty(value = "是否启用 0停用 1启用 2已保存")
    @Column(name = "GMT_FLAG")
    private Integer gmtFlag;

    @ApiModelProperty(value = "按日期循环1勾选0未勾选")
    @Column(name = "GMT_SEND_DATE")
    private String gmtSendDate;

    @ApiModelProperty(value = "按日期循环设置值")
    @Column(name = "GMT_SEND_DATE_SETTING_NUM")
    private Integer gmtSendDateSettingNum;

    @ApiModelProperty(value = "按星期循环 1勾选0未勾选")
    @Column(name = "GMT_SEND_WEEK")
    private String gmtSendWeek;

    @ApiModelProperty(value = "按星期循环 设置值")
    @Column(name = "GMT_SEND_WEEK_SETTING_NUM")
    private Integer gmtSendWeekSettingNum;

    @ApiModelProperty(value = "按活动设置 1勾选0未勾选")
    @Column(name = "GMT_SEND_ACTIVITY")
    private String gmtSendActivity;

    @ApiModelProperty(value = "按活动设置值")
    @Column(name = "GMT_SEND_ACTIVITY_SETTING_NUM")
    private Integer gmtSendActivitySettingNum;

    @ApiModelProperty(value = "推送时间")
    @Column(name = "GMT_SEND_TIME")
    private String gmtSendTime;

    @ApiModelProperty(value = "启用或者停用 1立即启用 0立即停用 2定时启用 3 定时停用 ")
    @Column(name = "EFFECT_MODE")
    private String effectMode;

    @ApiModelProperty(value = "启用生效日期")
    @Column(name = "EFFECT_DATE")
    private String effectDate;

    @ApiModelProperty(value = "启用生效时间")
    @Column(name = "EFFECT_TIME")
    private String effectTime;


    @ApiModelProperty(value = "门店人员是否显示毛利数据")
    @Column(name = "GMT_IF_SHOW_MAO")
    private String gmtIfShowMao;

    @ApiModelProperty(value = "创建人账号")
    @Column(name = "CRE_ID")
    private String creId;

    @ApiModelProperty(value = "创建人姓名")
    @Column(name = "CRE_NAME")
    private String creName;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "CRE_DATE")
    private String creDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "CRE_TIME")
    private String creTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "MODI_ID")
    private String modiId;

    @ApiModelProperty(value = "更新人姓名")
    @Column(name = "MODI_NAME")
    private String modiName;

    @ApiModelProperty(value = "修改日期")
    @Column(name = "MODI_DATE")
    private String modiDate;

    @ApiModelProperty(value = "修改时间")
    @Column(name = "MODI_TIME")
    private String modiTime;

    @Transient
    @ApiModelProperty(value = "选择的加盟商")
    private List<String> clients;

    @Transient
    @ApiModelProperty(value = "选择的职位")
    private List<String> positions;


}
