package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员类促销会员日价设置表
 */
@Table(name = "GAIA_SD_PROM_HYR_PRICE")
@Data
public class GaiaSdPromHyrPrice implements Serializable {
    private static final long serialVersionUID = -359902243393404669L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPHP_VOUCHER_ID")
    private String gsphpVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPHP_SERIAL")
    private String gsphpSerial;

    /**
     *  编码
     */
    @Id
    @Column(name = "GSPHP_PRO_ID")
    private String gsphpProId;

    /**
     * 促销价
     */
    @Column(name = "GSPHP_PRICE")
    private BigDecimal gsphpPrice;

    /**
     * 促销折扣
     */
    @Column(name = "GSPHP_REBATE")
    private String gsphpRebate;

    /**
     * 是否积分
     */
    @Column(name = "GSPHP_INTE_FLAG")
    private String gsphpInteFlag;

    /**
     * 积分倍率
     */
    @Column(name = "GSPHP_INTE_RATE")
    private String gsphpInteRate;

}
