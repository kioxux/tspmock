package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/12/22 14:19
 */
@Data
@Table(name = "GAIA_YAOSHIBANG_STORE")
public class GAIAYaoshibangStore  implements Serializable {
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 药德id
     */
    @Column(name = "StoreID")
    private String storeID;

    /**
     * 加盟商
     */
    @Column(name = "DrugStore_Name")
    private String drugstoreName;

    /**
     * 统一社会信用码
     */
    @Column(name = "Busicard_No")
    private String busicardNo;

    /**
     * 营业执照期限
     */
    @Column(name = "Busicard_Valid")
    private String busicardValid;

    /**
     * 法人
     */
    @Column(name = "Proxy_Opername")
    private String proxyOpername;

    /**
     * 注册地址
     */
    @Column(name = "Reg_Adress")
    private String regAdress;

    /**
     * 仓库地址
     */
    @Column(name = "Store_Adress")
    private String storeAdress;

    /**
     * 许可证编号
     */
    @Column(name = "DrugBusitcard_No")
    private String drugBusitcardNo;

    /**
     * 发证有效期
     */
    @Column(name = "DrugBusi_Validity")
    private String drugBusiValidity;

    /**
     * 法人委托书
     */
    @Column(name = "PurchasePaper_No")
    private String purchasePaperNo;

    /**
     * 法人委托书效期
     */
    @Column(name = "PurchasePaper_Valid")
    private String purchasePaperValid;

    /**
     * 收货地址
     */
    @Column(name = "Receive_Adress")
    private String receiveAdress;

    /**
     * 食品许可证书
     */
    @Column(name = "FoodBusi_No")
    private String foodBusiNo;

    /**
     * 食品许可证有效期
     */
    @Column(name = "FoodBusi_Valid")
    private String foodBusiValid;

    /**
     * 医疗器械生产/经营许可证
     */
    @Column(name = "HiseqBusitcard_No")
    private String hiseqBusitcardNo;

    /**
     * 医疗器械生产/经营许可证有效期
     */
    @Column(name = "HiseqBusitcard_Valid")
    private String hiseqBusitcardValid;
}
