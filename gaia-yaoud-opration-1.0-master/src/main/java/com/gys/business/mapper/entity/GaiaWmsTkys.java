package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_WMS_TKYS")
public class GaiaWmsTkys implements Serializable {
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 退库单号
     */
    @Column(name = "WM_TKDH")
    private String wmTkdh;

    /**
     * 退库订单号
     */
    @Column(name = "WM_TKDDH")
    private String wmTkddh;

    /**
     * 订单序号
     */
    @Column(name = "WM_DDXH")
    private String wmDdxh;

    /**
     * 商品编码
     */
    @Column(name = "WM_SP_BM")
    private String wmSpBm;

    /**
     * 批次号
     */
    @Column(name = "WM_PCH")
    private String wmPch;

    /**
     * 订单价
     */
    @Column(name = "WM_DDJ")
    private BigDecimal wmDdj;

    /**
     * 收货数量
     */
    @Column(name = "WM_SHSL")
    private BigDecimal wmShsl;

    /**
     * 拒收数量
     */
    @Column(name = "WM_JSSL")
    private BigDecimal wmJssl;

    /**
     * 拒收原因
     */
    @Column(name = "WM_JSYY")
    private String wmJsyy;

    /**
     * 库存状态编号
     */
    @Column(name = "WM_KCZT_BH")
    private String wmKcztBh;

    /**
     * 验收结论
     */
    @Column(name = "WM_YSJL")
    private String wmYsjl;

    /**
     * 修改日期
     */
    @Column(name = "WM_XGRQ")
    private String wmXgrq;

    /**
     * 修改时间
     */
    @Column(name = "WM_XGSJ")
    private String wmXgsj;

    /**
     * 修改人ID
     */
    @Column(name = "WM_XGR_ID")
    private String wmXgrId;

    /**
     * 修改人
     */
    @Column(name = "WM_XGR")
    private String wmXgr;

    /**
     * 验收确认日期
     */
    @Column(name = "WM_YSRQ")
    private String wmYsrq;

    /**
     * 验收确认时间
     */
    @Column(name = "WM_YSSJ")
    private String wmYssj;

    /**
     * 验收确认人ID
     */
    @Column(name = "WM_YSR_ID")
    private String wmYsrId;

    /**
     * 验收确认人
     */
    @Column(name = "WM_YSR")
    private String wmYsr;

    /**
     * 批号
     */
    @Column(name = "PRODUCT_BATCH_NO")
    private String productBatchNo;

    /**
     * 地点
     */
    @Column(name = "PRO_SITE")
    private String proSite;

    /**
     * 客户编号
     */
    @Column(name = "WM_KH_BH")
    private String wmKhBh;

    /**
     * 生产日期
     */
    @Column(name = "WM_SCRQ")
    private String wmScrq;

    /**
     * 有效期
     */
    @Column(name = "WM_YXQ")
    private String wmYxq;

    /**
     * 备注
     */
    @Column(name = "REMARK")
    private String remark;

    /**
     * 到货价
     */
    @Column(name = "WM_DHJ")
    private BigDecimal wmDhj;

    /**
     * 状态（1.已收货2.已验收）
     */
    @Column(name = "STATE")
    private Integer state;

    /**
     * 已上架数量
     */
    @Column(name = "ACCEPTED_QUANTITY")
    private Long acceptedQuantity;

    /**
     * 收货人
     */
    @Column(name = "CONSIGNEE")
    private String consignee;

    /**
     * 收货日期
     */
    @Column(name = "RECEIVING_DATE")
    private String receivingDate;

    /**
     * 收货时间
     */
    @Column(name = "RECEIVING_TIME")
    private String receivingTime;

    /**
     * 库存id
     */
    @Column(name = "KUCEN_ID")
    private Long kucenId;

    /**
     * 领取人
     */
    @Column(name = "WM_LQR")
    private String wmLqr;

    /**
     * 领取人id
     */
    @Column(name = "WM_LQR_ID")
    private String wmLqrId;

    /**
     * 领取时间
     */
    @Column(name = "WM_LQSJ")
    private String wmLqsj;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取退库单号
     *
     * @return WM_TKDH - 退库单号
     */
    public String getWmTkdh() {
        return wmTkdh;
    }

    /**
     * 设置退库单号
     *
     * @param wmTkdh 退库单号
     */
    public void setWmTkdh(String wmTkdh) {
        this.wmTkdh = wmTkdh;
    }

    /**
     * 获取退库订单号
     *
     * @return WM_TKDDH - 退库订单号
     */
    public String getWmTkddh() {
        return wmTkddh;
    }

    /**
     * 设置退库订单号
     *
     * @param wmTkddh 退库订单号
     */
    public void setWmTkddh(String wmTkddh) {
        this.wmTkddh = wmTkddh;
    }

    /**
     * 获取订单序号
     *
     * @return WM_DDXH - 订单序号
     */
    public String getWmDdxh() {
        return wmDdxh;
    }

    /**
     * 设置订单序号
     *
     * @param wmDdxh 订单序号
     */
    public void setWmDdxh(String wmDdxh) {
        this.wmDdxh = wmDdxh;
    }

    /**
     * 获取商品编码
     *
     * @return WM_SP_BM - 商品编码
     */
    public String getWmSpBm() {
        return wmSpBm;
    }

    /**
     * 设置商品编码
     *
     * @param wmSpBm 商品编码
     */
    public void setWmSpBm(String wmSpBm) {
        this.wmSpBm = wmSpBm;
    }

    /**
     * 获取批次号
     *
     * @return WM_PCH - 批次号
     */
    public String getWmPch() {
        return wmPch;
    }

    /**
     * 设置批次号
     *
     * @param wmPch 批次号
     */
    public void setWmPch(String wmPch) {
        this.wmPch = wmPch;
    }

    /**
     * 获取订单价
     *
     * @return WM_DDJ - 订单价
     */
    public BigDecimal getWmDdj() {
        return wmDdj;
    }

    /**
     * 设置订单价
     *
     * @param wmDdj 订单价
     */
    public void setWmDdj(BigDecimal wmDdj) {
        this.wmDdj = wmDdj;
    }

    /**
     * 获取收货数量
     *
     * @return WM_SHSL - 收货数量
     */
    public BigDecimal getWmShsl() {
        return wmShsl;
    }

    /**
     * 设置收货数量
     *
     * @param wmShsl 收货数量
     */
    public void setWmShsl(BigDecimal wmShsl) {
        this.wmShsl = wmShsl;
    }

    /**
     * 获取拒收数量
     *
     * @return WM_JSSL - 拒收数量
     */
    public BigDecimal getWmJssl() {
        return wmJssl;
    }

    /**
     * 设置拒收数量
     *
     * @param wmJssl 拒收数量
     */
    public void setWmJssl(BigDecimal wmJssl) {
        this.wmJssl = wmJssl;
    }

    /**
     * 获取拒收原因
     *
     * @return WM_JSYY - 拒收原因
     */
    public String getWmJsyy() {
        return wmJsyy;
    }

    /**
     * 设置拒收原因
     *
     * @param wmJsyy 拒收原因
     */
    public void setWmJsyy(String wmJsyy) {
        this.wmJsyy = wmJsyy;
    }

    /**
     * 获取库存状态编号
     *
     * @return WM_KCZT_BH - 库存状态编号
     */
    public String getWmKcztBh() {
        return wmKcztBh;
    }

    /**
     * 设置库存状态编号
     *
     * @param wmKcztBh 库存状态编号
     */
    public void setWmKcztBh(String wmKcztBh) {
        this.wmKcztBh = wmKcztBh;
    }

    /**
     * 获取验收结论
     *
     * @return WM_YSJL - 验收结论
     */
    public String getWmYsjl() {
        return wmYsjl;
    }

    /**
     * 设置验收结论
     *
     * @param wmYsjl 验收结论
     */
    public void setWmYsjl(String wmYsjl) {
        this.wmYsjl = wmYsjl;
    }

    /**
     * 获取修改日期
     *
     * @return WM_XGRQ - 修改日期
     */
    public String getWmXgrq() {
        return wmXgrq;
    }

    /**
     * 设置修改日期
     *
     * @param wmXgrq 修改日期
     */
    public void setWmXgrq(String wmXgrq) {
        this.wmXgrq = wmXgrq;
    }

    /**
     * 获取修改时间
     *
     * @return WM_XGSJ - 修改时间
     */
    public String getWmXgsj() {
        return wmXgsj;
    }

    /**
     * 设置修改时间
     *
     * @param wmXgsj 修改时间
     */
    public void setWmXgsj(String wmXgsj) {
        this.wmXgsj = wmXgsj;
    }

    /**
     * 获取修改人ID
     *
     * @return WM_XGR_ID - 修改人ID
     */
    public String getWmXgrId() {
        return wmXgrId;
    }

    /**
     * 设置修改人ID
     *
     * @param wmXgrId 修改人ID
     */
    public void setWmXgrId(String wmXgrId) {
        this.wmXgrId = wmXgrId;
    }

    /**
     * 获取修改人
     *
     * @return WM_XGR - 修改人
     */
    public String getWmXgr() {
        return wmXgr;
    }

    /**
     * 设置修改人
     *
     * @param wmXgr 修改人
     */
    public void setWmXgr(String wmXgr) {
        this.wmXgr = wmXgr;
    }

    /**
     * 获取验收确认日期
     *
     * @return WM_YSRQ - 验收确认日期
     */
    public String getWmYsrq() {
        return wmYsrq;
    }

    /**
     * 设置验收确认日期
     *
     * @param wmYsrq 验收确认日期
     */
    public void setWmYsrq(String wmYsrq) {
        this.wmYsrq = wmYsrq;
    }

    /**
     * 获取验收确认时间
     *
     * @return WM_YSSJ - 验收确认时间
     */
    public String getWmYssj() {
        return wmYssj;
    }

    /**
     * 设置验收确认时间
     *
     * @param wmYssj 验收确认时间
     */
    public void setWmYssj(String wmYssj) {
        this.wmYssj = wmYssj;
    }

    /**
     * 获取验收确认人ID
     *
     * @return WM_YSR_ID - 验收确认人ID
     */
    public String getWmYsrId() {
        return wmYsrId;
    }

    /**
     * 设置验收确认人ID
     *
     * @param wmYsrId 验收确认人ID
     */
    public void setWmYsrId(String wmYsrId) {
        this.wmYsrId = wmYsrId;
    }

    /**
     * 获取验收确认人
     *
     * @return WM_YSR - 验收确认人
     */
    public String getWmYsr() {
        return wmYsr;
    }

    /**
     * 设置验收确认人
     *
     * @param wmYsr 验收确认人
     */
    public void setWmYsr(String wmYsr) {
        this.wmYsr = wmYsr;
    }

    /**
     * 获取批号
     *
     * @return PRODUCT_BATCH_NO - 批号
     */
    public String getProductBatchNo() {
        return productBatchNo;
    }

    /**
     * 设置批号
     *
     * @param productBatchNo 批号
     */
    public void setProductBatchNo(String productBatchNo) {
        this.productBatchNo = productBatchNo;
    }

    /**
     * 获取地点
     *
     * @return PRO_SITE - 地点
     */
    public String getProSite() {
        return proSite;
    }

    /**
     * 设置地点
     *
     * @param proSite 地点
     */
    public void setProSite(String proSite) {
        this.proSite = proSite;
    }

    /**
     * 获取客户编号
     *
     * @return WM_KH_BH - 客户编号
     */
    public String getWmKhBh() {
        return wmKhBh;
    }

    /**
     * 设置客户编号
     *
     * @param wmKhBh 客户编号
     */
    public void setWmKhBh(String wmKhBh) {
        this.wmKhBh = wmKhBh;
    }

    /**
     * 获取生产日期
     *
     * @return WM_SCRQ - 生产日期
     */
    public String getWmScrq() {
        return wmScrq;
    }

    /**
     * 设置生产日期
     *
     * @param wmScrq 生产日期
     */
    public void setWmScrq(String wmScrq) {
        this.wmScrq = wmScrq;
    }

    /**
     * 获取有效期
     *
     * @return WM_YXQ - 有效期
     */
    public String getWmYxq() {
        return wmYxq;
    }

    /**
     * 设置有效期
     *
     * @param wmYxq 有效期
     */
    public void setWmYxq(String wmYxq) {
        this.wmYxq = wmYxq;
    }

    /**
     * 获取备注
     *
     * @return REMARK - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取到货价
     *
     * @return WM_DHJ - 到货价
     */
    public BigDecimal getWmDhj() {
        return wmDhj;
    }

    /**
     * 设置到货价
     *
     * @param wmDhj 到货价
     */
    public void setWmDhj(BigDecimal wmDhj) {
        this.wmDhj = wmDhj;
    }

    /**
     * 获取状态（1.已收货2.已验收）
     *
     * @return STATE - 状态（1.已收货2.已验收）
     */
    public Integer getState() {
        return state;
    }

    /**
     * 设置状态（1.已收货2.已验收）
     *
     * @param state 状态（1.已收货2.已验收）
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * 获取已上架数量
     *
     * @return ACCEPTED_QUANTITY - 已上架数量
     */
    public Long getAcceptedQuantity() {
        return acceptedQuantity;
    }

    /**
     * 设置已上架数量
     *
     * @param acceptedQuantity 已上架数量
     */
    public void setAcceptedQuantity(Long acceptedQuantity) {
        this.acceptedQuantity = acceptedQuantity;
    }

    /**
     * 获取收货人
     *
     * @return CONSIGNEE - 收货人
     */
    public String getConsignee() {
        return consignee;
    }

    /**
     * 设置收货人
     *
     * @param consignee 收货人
     */
    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    /**
     * 获取收货日期
     *
     * @return RECEIVING_DATE - 收货日期
     */
    public String getReceivingDate() {
        return receivingDate;
    }

    /**
     * 设置收货日期
     *
     * @param receivingDate 收货日期
     */
    public void setReceivingDate(String receivingDate) {
        this.receivingDate = receivingDate;
    }

    /**
     * 获取收货时间
     *
     * @return RECEIVING_TIME - 收货时间
     */
    public String getReceivingTime() {
        return receivingTime;
    }

    /**
     * 设置收货时间
     *
     * @param receivingTime 收货时间
     */
    public void setReceivingTime(String receivingTime) {
        this.receivingTime = receivingTime;
    }

    /**
     * 获取库存id
     *
     * @return KUCEN_ID - 库存id
     */
    public Long getKucenId() {
        return kucenId;
    }

    /**
     * 设置库存id
     *
     * @param kucenId 库存id
     */
    public void setKucenId(Long kucenId) {
        this.kucenId = kucenId;
    }

    /**
     * 获取领取人
     *
     * @return WM_LQR - 领取人
     */
    public String getWmLqr() {
        return wmLqr;
    }

    /**
     * 设置领取人
     *
     * @param wmLqr 领取人
     */
    public void setWmLqr(String wmLqr) {
        this.wmLqr = wmLqr;
    }

    /**
     * 获取领取人id
     *
     * @return WM_LQR_ID - 领取人id
     */
    public String getWmLqrId() {
        return wmLqrId;
    }

    /**
     * 设置领取人id
     *
     * @param wmLqrId 领取人id
     */
    public void setWmLqrId(String wmLqrId) {
        this.wmLqrId = wmLqrId;
    }

    /**
     * 获取领取时间
     *
     * @return WM_LQSJ - 领取时间
     */
    public String getWmLqsj() {
        return wmLqsj;
    }

    /**
     * 设置领取时间
     *
     * @param wmLqsj 领取时间
     */
    public void setWmLqsj(String wmLqsj) {
        this.wmLqsj = wmLqsj;
    }
}