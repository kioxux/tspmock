package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdWtbhdD;
import com.gys.business.service.data.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface AcceptWareHouseMapper {
    List<AcceptWareHouseOutData> selectPickOrderList(@Param("clientId") String clientId,@Param("stoCode") String stoCode,@Param("dcCode") String dcCode);

    List<AcceptWareHouseOutData>selectList(AcceptWareHouseInData inData);

    List<AcceptSentOrderOutData>selectPsOrderTotalList(AcceptWareHouseInData inData);

    List<AcceptWareHouseOutData>selectListById(@Param("clientId") String clientId,@Param("stoCode") String stoCode,@Param("dcCode") String dcCode,@Param("pickProId") String pickProId);

    List<AcceptWareHouseProDetailData>selectPickProductList(AcceptWareHouseInData inData);

    List<AcceptWareHouseProDetailData>selectApproveProductList(AcceptWareHouseInData inData);

    List<Map<String,String>> checkAcceptPro(AcceptWareHouseInData inData);

    void modifyAcceptStatus(GetAcceptInData inData);

    void modifyPickOrderStatus(AcceptWareHouseInData inData);

    List<Map<String,String>> checkExaminePro(GetAcceptInData inData);

    void modifyExamineStatus(GetAcceptInData inData);

    String selectNextPsVoucherId(@Param("clientId") String clientId);

    List<TrustSendOutData>selectDsfTrustList(TrustSendInData inData);

    List<TrustSendOutData>selectWtpsOrderList(@Param("clientId") String clientId,@Param("stoCode") String stoCode);

    List<TrustDetailOutData> selectWtpsDetail(@Param("clientId") String clientId,@Param("stoCode") String stoCode,@Param("gswdVoucherId") String gswdVoucherId);

    Map<String,String> checkWtpsAcceptPro(AcceptWareHouseInData inData);

    void modifyWtpsOrderStatus(TrustSendUpdateData inData);

    void remove(EditWtpsDetailDto dto);

    void editAMT(EditWtpsDetailDto dto);

    void editHAMT(EditWtpsDetailDto dto);

    GaiaClSystemPara hasCold(String client);

    List<GaiaSdWtbhdD> selectWtpsOrderListInWtbd();

    void updateDsfStatus(GaiaSdWtbhdD gaiaSdWtbhdD);

    int countFuHE(AcceptWareHouseInData inData);

    String checkColdChain(AcceptWareHouseInData inData);
    List<HashMap<String, Object>> listWaitingCheckClient(@Param("clientList") List<String> clientList);

    int listReturnSupplierApproval(@Param("client")String client, @Param("siteNumber")String siteNumber);

    List<String> listColdClientInfo();

    void saveWtpsDetail(AcceptWareSaveWtpsRefuseInData inData);

    GaiaInWarehouseRejection getAcceptParams(AcceptWareHouseInData inData);

    List<WmsFuHeOutData> getFhxhInfo(@Param("client") String client, @Param("jhdh") String jhdh, @Param("proSite") String proSite);

    void updateFhxhMStatus(@Param("shzt") String shzt,@Param("client") String client,@Param("site") String site,@Param("list") List<String> list);
}
