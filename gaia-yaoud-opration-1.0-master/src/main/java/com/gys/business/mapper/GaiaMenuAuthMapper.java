package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaMenuAuth;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaMenuAuthMapper extends BaseMapper<GaiaMenuAuth> {
    void deleteMenuAuth(@Param("menuAuth")GaiaMenuAuth menuAuth, @Param("menuIds")List<Object> menuIds);
}