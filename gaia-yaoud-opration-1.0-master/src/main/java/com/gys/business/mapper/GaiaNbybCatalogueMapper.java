package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaNbybCatalogue;
import com.gys.common.base.BaseMapper;

public interface GaiaNbybCatalogueMapper extends BaseMapper<GaiaNbybCatalogue> {
}