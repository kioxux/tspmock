//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_EMP_GROUP"
)
public class GaiaSdEmpGroup implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSEG_ID"
    )
    private String gsegId;
    @Column(
            name = "GSEG_NAME"
    )
    private String gsegName;
    @Column(
            name = "GSEG_BR_ID"
    )
    private String gsegBrId;
    @Column(
            name = "GSEG_BR_NAME"
    )
    private String gsegBrName;
    @Column(
            name = "GSEG_BEGIN_TIME"
    )
    private String gsegBeginTime;
    @Column(
            name = "GSEG_END_TIME"
    )
    private String gsegEndTime;
    @Column(
            name = "GSEG_EMP1"
    )
    private String gsegEmp1;
    @Column(
            name = "GSEG_EMP2"
    )
    private String gsegEmp2;
    @Column(
            name = "GSEG_EMP3"
    )
    private String gsegEmp3;
    @Column(
            name = "GSEG_EMP4"
    )
    private String gsegEmp4;
    @Column(
            name = "GSEG_EMP5"
    )
    private String gsegEmp5;
    @Column(
            name = "GSEG_UPDATE_DATE"
    )
    private String gsegUpdateDate;
    @Column(
            name = "GSEG_UPDATE_EMP"
    )
    private String gsegUpdateEmp;
    private static final long serialVersionUID = 1L;

    public GaiaSdEmpGroup() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsegId() {
        return this.gsegId;
    }

    public void setGsegId(String gsegId) {
        this.gsegId = gsegId;
    }

    public String getGsegName() {
        return this.gsegName;
    }

    public void setGsegName(String gsegName) {
        this.gsegName = gsegName;
    }

    public String getGsegBrId() {
        return this.gsegBrId;
    }

    public void setGsegBrId(String gsegBrId) {
        this.gsegBrId = gsegBrId;
    }

    public String getGsegBrName() {
        return this.gsegBrName;
    }

    public void setGsegBrName(String gsegBrName) {
        this.gsegBrName = gsegBrName;
    }

    public String getGsegBeginTime() {
        return this.gsegBeginTime;
    }

    public void setGsegBeginTime(String gsegBeginTime) {
        this.gsegBeginTime = gsegBeginTime;
    }

    public String getGsegEndTime() {
        return this.gsegEndTime;
    }

    public void setGsegEndTime(String gsegEndTime) {
        this.gsegEndTime = gsegEndTime;
    }

    public String getGsegEmp1() {
        return this.gsegEmp1;
    }

    public void setGsegEmp1(String gsegEmp1) {
        this.gsegEmp1 = gsegEmp1;
    }

    public String getGsegEmp2() {
        return this.gsegEmp2;
    }

    public void setGsegEmp2(String gsegEmp2) {
        this.gsegEmp2 = gsegEmp2;
    }

    public String getGsegEmp3() {
        return this.gsegEmp3;
    }

    public void setGsegEmp3(String gsegEmp3) {
        this.gsegEmp3 = gsegEmp3;
    }

    public String getGsegEmp4() {
        return this.gsegEmp4;
    }

    public void setGsegEmp4(String gsegEmp4) {
        this.gsegEmp4 = gsegEmp4;
    }

    public String getGsegEmp5() {
        return this.gsegEmp5;
    }

    public void setGsegEmp5(String gsegEmp5) {
        this.gsegEmp5 = gsegEmp5;
    }

    public String getGsegUpdateDate() {
        return this.gsegUpdateDate;
    }

    public void setGsegUpdateDate(String gsegUpdateDate) {
        this.gsegUpdateDate = gsegUpdateDate;
    }

    public String getGsegUpdateEmp() {
        return this.gsegUpdateEmp;
    }

    public void setGsegUpdateEmp(String gsegUpdateEmp) {
        this.gsegUpdateEmp = gsegUpdateEmp;
    }
}
