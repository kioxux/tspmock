package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSaletaskHplanClientNew;
import com.gys.business.mapper.entity.GaiaSaletaskPlanClientStoNew;
import com.gys.business.service.data.GetStoreInData;
import com.gys.business.service.data.MonthOutData;
import com.gys.business.service.data.MonthOutDataVo;
import com.gys.business.service.data.SaleTaskInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GaiaSaletaskPlanClientStoNewMapper extends BaseMapper<GaiaSaletaskPlanClientStoNew> {

    //用户新增保存：
    void insertMonthlySalesPlan(List<GaiaSaletaskPlanClientStoNew> gaiaSaletaskHplanClientNews);

    void updataSalesPlan(GaiaSaletaskPlanClientStoNew gaiaSaletaskPlanClientStoNews);
    int selectStore(@Param("client") String client,@Param("stoCode") String stoCode, @Param("staskTaskId") String staskTaskId);

    //任务设置保存
    void updateStoreTaskSetting(GaiaSaletaskPlanClientStoNew stoNew);

    //门店导入
    MonthOutDataVo selectStoreName(GetStoreInData storeInData);
}