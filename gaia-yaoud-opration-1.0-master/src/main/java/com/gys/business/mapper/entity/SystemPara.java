package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 系统模块参数表
 * </p>
 *
 * @author flynn
 * @since 2021-08-13
 */
@Data
@Table( name = "GAIA_SYSTEM_PARA")
public class SystemPara implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @Id
    @Column(name = "ID")
    private String id;

    @ApiModelProperty(value = "编码")
    @Column(name = "CODE")
    private String code;

    @ApiModelProperty(value = "名称")
    @Column(name = "NAME")
    private String name;

    @ApiModelProperty(value = "分类(1.门店模块 , 2.后台模块,3.APP模块,4.后台加盟商配置)")
    @Column(name = "CLASSIFY")
    private String classify;

    @ApiModelProperty(value = "模块ID")
    @Column(name = "MODEL")
    private String model;

    @ApiModelProperty(value = "平台(1.web 2.app)")
    @Column(name = "PLATFORM")
    private String platform;

    @ApiModelProperty(value = "适用加盟商(默认ALL 数据用@隔开)")
    @Column(name = "APPLY_CLIENT")
    private String applyClient;

    @ApiModelProperty(value = "备注")
    @Column(name = "REMARK")
    private String remark;

    @ApiModelProperty(value = "删除(Y.删除  N.不删除)")
    @Column(name = "DELETE_FLAG")
    private String deleteFlag;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "CRE_DATE")
    private String creDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "CRE_TIME")
    private String creTime;

    @ApiModelProperty(value = "创建人")
    @Column(name = "CRE_ID")
    private String creId;

    @ApiModelProperty(value = "修改日期")
    @Column(name = "MOD_DATE")
    private String modDate;

    @ApiModelProperty(value = "修改时间")
    @Column(name = "MOD_TIME")
    private String modTime;

    @ApiModelProperty(value = "修改人")
    @Column(name = "MOD_ID")
    private String modId;


}
