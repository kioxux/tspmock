package com.gys.business.mapper;


import com.gys.business.mapper.entity.OasSicknessTag;
import com.gys.business.service.request.SelectDataEchoRequest;
import com.gys.business.service.request.SelectOasSicknessPageInfoRequest;
import com.gys.business.service.request.SelectPageInfoRequest;
import com.gys.business.service.response.SelectDataEchoResponse;
import com.gys.business.service.response.SelectLargeOasSicknessPageInfoResponse;
import com.gys.business.service.response.SelectPageInfoResponse;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 疾病大类表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-09-01
 */
@Mapper
public interface OasSicknessTagMapper extends BaseMapper<OasSicknessTag> {
    /**
     * 疾病大类中类成分列表
     *
     * @param pageInfoRequest 请求参数
     * @return 返回值
     */
    List<SelectLargeOasSicknessPageInfoResponse> SelectOasSickness(@Param("request") SelectOasSicknessPageInfoRequest pageInfoRequest);

    List<SelectLargeOasSicknessPageInfoResponse.SelectMiddleOasSicknessPageInfoResponse> SelectLargeOasSickness(@Param("request") SelectOasSicknessPageInfoRequest pageInfoRequest);

    List<SelectLargeOasSicknessPageInfoResponse.SelectMiddleOasSicknessPageInfoResponse> SelectMiddleOasSickness(@Param("request")SelectOasSicknessPageInfoRequest pageInfoRequest);

    List<SelectLargeOasSicknessPageInfoResponse.SelectMiddleOasSicknessPageInfoResponse> SelectSmallOasSickness(@Param("request")SelectOasSicknessPageInfoRequest pageInfoRequest);

    /**
     * 获取大类分页列表
     * @return
     */
    List<OasSicknessTag> selectPage(@Param("request")SelectPageInfoRequest request);

    List<SelectPageInfoResponse> selectOasSicknessTag(@Param("request")SelectPageInfoRequest request, @Param("list")List<String> list);

    SelectDataEchoResponse selectDataEcho(@Param("request") SelectDataEchoRequest request );

    List<SelectPageInfoResponse> selectPageInfo(@Param("request")SelectPageInfoRequest request);
    List<Object> selectPageInfo(@Param("request")SelectPageInfoRequest request,Integer su);
}
