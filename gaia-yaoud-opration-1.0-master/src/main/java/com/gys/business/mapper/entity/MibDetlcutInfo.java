package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Data
@Table(name = "MIB_DETLCUT_INFO")
public class MibDetlcutInfo implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店
     */
    @Column(name = "BR_ID")
    private String brId;

    /**
     * 药德订单号
     */
    @Column(name = "BILL_NO")
    private String billNo;

    /**
     * 接收方报文ID
     */
    @Column(name = "MSG_ID")
    private String msgId;

    /**
     * 费用明细流水 号
     */
    @Column(name = "FEEDETL_SN")
    private String feedetlSn;

    /**
     * 明细项目费用 总额
     */
    @Column(name = "DET_ITEM_FEE_SUMAMT")
    private BigDecimal detItemFeeSumamt;

    /**
     * 数量
     */
    @Column(name = "CNT")
    private BigDecimal cnt;

    /**
     *  单价
     */
    @Column(name = "PRIC")
    private BigDecimal pric;

    /**
     * 定价上限金额
     */
    @Column(name = "PRIC_UPLMT_AMT")
    private BigDecimal pricUplmtAmt;

    /**
     * 自付比例
     */
    @Column(name = "SELFPAY_PROP")
    private BigDecimal selfpayProp;

    /**
     * 全自费金额
     */
    @Column(name = "FULAMT_OWNPAY_AMT")
    private BigDecimal fulamtOwnpayAmt;

    /**
     * 超限价金额
     */
    @Column(name = "OVERLMT_AMT")
    private BigDecimal overlmtAmt;

    /**
     * 先行自付金额
     */
    @Column(name = "PRESELFPAY_AMT")
    private BigDecimal preselfpayAmt;

    /**
     * 符合政策范围 金额
     */
    @Column(name = "INSCP_SCP_AMT")
    private BigDecimal inscpScpAmt;

    /**
     * 收费项目等级(代码标识)
     */
    @Column(name = "CHRGITM_LV")
    private String chrgitmLv;

    /**
     * 医疗收费项目 类别(代码标识)
     */
    @Column(name = "MED_CHRGITM_TYPE")
    private String medChrgitmType;

    /**
     * 基本药物标志(代码标识)
     */
    @Column(name = "BAS_MEDN_FLAG")
    private String basMednFlag;

    /**
     * 医保谈判药品 标志(代码标识)
     */
    @Column(name = "HI_NEGO_DRUG_FLAG")
    private String hiNegoDrugFlag;

    /**
     * 儿童用药标志(代码标识)
     */
    @Column(name = "CHLD_MEDC_FLAG")
    private String chldMedcFlag;

    /**
     * 目录特项标志(代码标识)
     */
    @Column(name = "LIST_SP_ITEM_FLAG")
    private String listSpItemFlag;

    /**
     * 直报标志(代码标识)
     */
    @Column(name = "DRT_REIM_FLAG")
    private String drtReimFlag;

    /**
     *  备注
     */
    @Column(name = "MEMO")
    private String memo;

    private static final long serialVersionUID = 1L;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取门店
     *
     * @return BR_ID - 门店
     */
    public String getBrId() {
        return brId;
    }

    /**
     * 设置门店
     *
     * @param brId 门店
     */
    public void setBrId(String brId) {
        this.brId = brId;
    }

    /**
     * 获取药德订单号
     *
     * @return BILL_NO - 药德订单号
     */
    public String getBillNo() {
        return billNo;
    }

    /**
     * 设置药德订单号
     *
     * @param billNo 药德订单号
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * 获取接收方报文ID
     *
     * @return MSG_ID - 接收方报文ID
     */
    public String getMsgId() {
        return msgId;
    }

    /**
     * 设置接收方报文ID
     *
     * @param msgId 接收方报文ID
     */
    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    /**
     * 获取费用明细流水 号
     *
     * @return FEEDETL_SN - 费用明细流水 号
     */
    public String getFeedetlSn() {
        return feedetlSn;
    }

    /**
     * 设置费用明细流水 号
     *
     * @param feedetlSn 费用明细流水 号
     */
    public void setFeedetlSn(String feedetlSn) {
        this.feedetlSn = feedetlSn;
    }

    /**
     * 获取明细项目费用 总额
     *
     * @return DET_ITEM_FEE_SUMAMT - 明细项目费用 总额
     */
    public BigDecimal getDetItemFeeSumamt() {
        return detItemFeeSumamt;
    }

    /**
     * 设置明细项目费用 总额
     *
     * @param detItemFeeSumamt 明细项目费用 总额
     */
    public void setDetItemFeeSumamt(BigDecimal detItemFeeSumamt) {
        this.detItemFeeSumamt = detItemFeeSumamt;
    }

    /**
     * 获取数量
     *
     * @return CNT - 数量
     */
    public BigDecimal getCnt() {
        return cnt;
    }

    /**
     * 设置数量
     *
     * @param cnt 数量
     */
    public void setCnt(BigDecimal cnt) {
        this.cnt = cnt;
    }

    /**
     * 获取 单价
     *
     * @return PRIC -  单价
     */
    public BigDecimal getPric() {
        return pric;
    }

    /**
     * 设置 单价
     *
     * @param pric  单价
     */
    public void setPric(BigDecimal pric) {
        this.pric = pric;
    }

    /**
     * 获取定价上限金额
     *
     * @return PRIC_UPLMT_AMT - 定价上限金额
     */
    public BigDecimal getPricUplmtAmt() {
        return pricUplmtAmt;
    }

    /**
     * 设置定价上限金额
     *
     * @param pricUplmtAmt 定价上限金额
     */
    public void setPricUplmtAmt(BigDecimal pricUplmtAmt) {
        this.pricUplmtAmt = pricUplmtAmt;
    }

    /**
     * 获取自付比例
     *
     * @return SELFPAY_PROP - 自付比例
     */
    public BigDecimal getSelfpayProp() {
        return selfpayProp;
    }

    /**
     * 设置自付比例
     *
     * @param selfpayProp 自付比例
     */
    public void setSelfpayProp(BigDecimal selfpayProp) {
        this.selfpayProp = selfpayProp;
    }

    /**
     * 获取全自费金额
     *
     * @return FULAMT_OWNPAY_AMT - 全自费金额
     */
    public BigDecimal getFulamtOwnpayAmt() {
        return fulamtOwnpayAmt;
    }

    /**
     * 设置全自费金额
     *
     * @param fulamtOwnpayAmt 全自费金额
     */
    public void setFulamtOwnpayAmt(BigDecimal fulamtOwnpayAmt) {
        this.fulamtOwnpayAmt = fulamtOwnpayAmt;
    }

    /**
     * 获取超限价金额
     *
     * @return OVERLMT_AMT - 超限价金额
     */
    public BigDecimal getOverlmtAmt() {
        return overlmtAmt;
    }

    /**
     * 设置超限价金额
     *
     * @param overlmtAmt 超限价金额
     */
    public void setOverlmtAmt(BigDecimal overlmtAmt) {
        this.overlmtAmt = overlmtAmt;
    }

    /**
     * 获取先行自付金额
     *
     * @return PRESELFPAY_AMT - 先行自付金额
     */
    public BigDecimal getPreselfpayAmt() {
        return preselfpayAmt;
    }

    /**
     * 设置先行自付金额
     *
     * @param preselfpayAmt 先行自付金额
     */
    public void setPreselfpayAmt(BigDecimal preselfpayAmt) {
        this.preselfpayAmt = preselfpayAmt;
    }

    /**
     * 获取符合政策范围 金额
     *
     * @return INSCP_SCP_AMT - 符合政策范围 金额
     */
    public BigDecimal getInscpScpAmt() {
        return inscpScpAmt;
    }

    /**
     * 设置符合政策范围 金额
     *
     * @param inscpScpAmt 符合政策范围 金额
     */
    public void setInscpScpAmt(BigDecimal inscpScpAmt) {
        this.inscpScpAmt = inscpScpAmt;
    }

    /**
     * 获取收费项目等级(代码标识)
     *
     * @return CHRGITM_LV - 收费项目等级(代码标识)
     */
    public String getChrgitmLv() {
        return chrgitmLv;
    }

    /**
     * 设置收费项目等级(代码标识)
     *
     * @param chrgitmLv 收费项目等级(代码标识)
     */
    public void setChrgitmLv(String chrgitmLv) {
        this.chrgitmLv = chrgitmLv;
    }

    /**
     * 获取医疗收费项目 类别(代码标识)
     *
     * @return MED_CHRGITM_TYPE - 医疗收费项目 类别(代码标识)
     */
    public String getMedChrgitmType() {
        return medChrgitmType;
    }

    /**
     * 设置医疗收费项目 类别(代码标识)
     *
     * @param medChrgitmType 医疗收费项目 类别(代码标识)
     */
    public void setMedChrgitmType(String medChrgitmType) {
        this.medChrgitmType = medChrgitmType;
    }

    /**
     * 获取基本药物标志(代码标识)
     *
     * @return BAS_MEDN_FLAG - 基本药物标志(代码标识)
     */
    public String getBasMednFlag() {
        return basMednFlag;
    }

    /**
     * 设置基本药物标志(代码标识)
     *
     * @param basMednFlag 基本药物标志(代码标识)
     */
    public void setBasMednFlag(String basMednFlag) {
        this.basMednFlag = basMednFlag;
    }

    /**
     * 获取医保谈判药品 标志(代码标识)
     *
     * @return HI_NEGO_DRUG_FLAG - 医保谈判药品 标志(代码标识)
     */
    public String getHiNegoDrugFlag() {
        return hiNegoDrugFlag;
    }

    /**
     * 设置医保谈判药品 标志(代码标识)
     *
     * @param hiNegoDrugFlag 医保谈判药品 标志(代码标识)
     */
    public void setHiNegoDrugFlag(String hiNegoDrugFlag) {
        this.hiNegoDrugFlag = hiNegoDrugFlag;
    }

    /**
     * 获取儿童用药标志(代码标识)
     *
     * @return CHLD_MEDC_FLAG - 儿童用药标志(代码标识)
     */
    public String getChldMedcFlag() {
        return chldMedcFlag;
    }

    /**
     * 设置儿童用药标志(代码标识)
     *
     * @param chldMedcFlag 儿童用药标志(代码标识)
     */
    public void setChldMedcFlag(String chldMedcFlag) {
        this.chldMedcFlag = chldMedcFlag;
    }

    /**
     * 获取目录特项标志(代码标识)
     *
     * @return LIST_SP_ITEM_FLAG - 目录特项标志(代码标识)
     */
    public String getListSpItemFlag() {
        return listSpItemFlag;
    }

    /**
     * 设置目录特项标志(代码标识)
     *
     * @param listSpItemFlag 目录特项标志(代码标识)
     */
    public void setListSpItemFlag(String listSpItemFlag) {
        this.listSpItemFlag = listSpItemFlag;
    }

    /**
     * 获取直报标志(代码标识)
     *
     * @return DRT_REIM_FLAG - 直报标志(代码标识)
     */
    public String getDrtReimFlag() {
        return drtReimFlag;
    }

    /**
     * 设置直报标志(代码标识)
     *
     * @param drtReimFlag 直报标志(代码标识)
     */
    public void setDrtReimFlag(String drtReimFlag) {
        this.drtReimFlag = drtReimFlag;
    }

    /**
     * 获取 备注
     *
     * @return MEMO -  备注
     */
    public String getMemo() {
        return memo;
    }

    /**
     * 设置 备注
     *
     * @param memo  备注
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }
}