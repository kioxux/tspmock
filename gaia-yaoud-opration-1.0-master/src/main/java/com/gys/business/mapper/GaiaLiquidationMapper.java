package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaLiquidation;
import com.gys.business.service.data.GaiaLiquidationOutData;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * 医保清算表(GaiaLiquidation)表数据库访问层
 *
 * @author makejava
 * @since 2021-12-29 15:00:25
 */
public interface GaiaLiquidationMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaLiquidation queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaLiquidation 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaLiquidation> queryAllByLimit(GaiaLiquidation gaiaLiquidation, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaLiquidation 查询条件
     * @return 总行数
     */
    long count(GaiaLiquidation gaiaLiquidation);

    /**
     * 新增数据
     *
     * @param gaiaLiquidation 实例对象
     * @return 影响行数
     */
    int insert(GaiaLiquidation gaiaLiquidation);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaLiquidation> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaLiquidation> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaLiquidation> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaLiquidation> entities);

    /**
     * 修改数据
     *
     * @param gaiaLiquidation 实例对象
     * @return 影响行数
     */
    int update(GaiaLiquidation gaiaLiquidation);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    GaiaLiquidation getLiquidation(GaiaLiquidation inData);

    List<GaiaLiquidationOutData> getAllByClientAndBrId(@Param("client") String client , @Param("stoCode")  String stoCode);

    void updateStatus(GaiaLiquidation liquidation);

    List<GaiaLiquidation> getList(GaiaLiquidation liquidation);

}

