package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaMenu;
import com.gys.business.service.data.Menu.MenuInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaMenuMapper extends BaseMapper<GaiaMenu> {
    int getCount(MenuInData menu);
    int getMaxSeq(MenuInData inData);
    long getMaxId(MenuInData inData);

    List<GaiaMenu> selectAll();

    void deleteMenu(@Param("menu")GaiaMenu menu,@Param("menuIds")List<Object> menuIds);

    List<Object> selectMenuList(@Param("menuIds")List<Object> menuIds);

    void updateBatch(List<GaiaMenu> menus);

}