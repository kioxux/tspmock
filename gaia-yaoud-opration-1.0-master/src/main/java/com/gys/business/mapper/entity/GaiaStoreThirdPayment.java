package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 * 第三方门店配置表
 */
@Data
public class GaiaStoreThirdPayment implements Serializable {


    private static final long serialVersionUID = -7032013044453228547L;

    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 编码
     */
    private String stoKey;

    /**
     * 值
     */
    private String stoValue;

    /**
     * 最后更新时间 最后更新时间
     */
    private String lastUpdateTime;

    /**
     * 类型：1.第三方支付 2.医保
     */
    private String payType;


    /**
     * PAY_TYPE为1是第三方 为2是医保地区
     */
    private String payValue;


}