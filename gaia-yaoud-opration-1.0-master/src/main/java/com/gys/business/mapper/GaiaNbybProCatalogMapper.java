package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaNbybProCatalog;
import com.gys.common.base.BaseMapper;

import java.util.List;
import java.util.Map;

public interface GaiaNbybProCatalogMapper extends BaseMapper<GaiaNbybProCatalog> {
    List<GaiaNbybProCatalog> getNbybProCatalog(Map inData);
    GaiaNbybProCatalog getNbybProCatalogOne(Map inData);

}