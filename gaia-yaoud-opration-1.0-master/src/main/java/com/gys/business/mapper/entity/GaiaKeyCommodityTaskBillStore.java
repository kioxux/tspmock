package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 重点商品任务—参与门店表(GaiaKeyCommodityTaskBillStore)实体类
 *
 * @author makejava
 * @since 2021-09-01 15:39:15
 */
public class GaiaKeyCommodityTaskBillStore implements Serializable {
    private static final long serialVersionUID = -23690611742304218L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 任务单号
     */
    private String billCode;
    /**
     * 门店编码
     */
    private String stoCode;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    private Integer stoLevel;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;

    public Integer getStoLevel() {
        return stoLevel;
    }

    public void setStoLevel(Integer stoLevel) {
        this.stoLevel = stoLevel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getStoCode() {
        return stoCode;
    }

    public void setStoCode(String stoCode) {
        this.stoCode = stoCode;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}
