package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户实体
 *
 * @author xiaoyuan on 2020/7/26
 */
@Data
public class GaiaUser implements Serializable {
    private static final long serialVersionUID = -8699833090571359182L;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户姓名
     */
    private String userName;
}
