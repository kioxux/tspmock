package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_MENU_AUTH_CONFIG")
public class GaiaMenuAuthConfig implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 菜单ID
     */
    @Id
    @Column(name = "MENU_ID")
    private Long menuId;

    /**
     * 权限ID
     */
    @Id
    @Column(name = "AUTH_ID")
    private Long authId;

    /**
     * 状态(0.禁用 1.启用 2.删除 )
     */
    @Column(name = "STATUS")
    private String status;

    /**
     * 创建人
     */
    @Column(name = "CREATOR")
    private String creator;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 更新人
     */
    @Column(name = "UPDATOR")
    private String updator;

    /**
     * 更新时间

     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取菜单ID
     *
     * @return MENU_ID - 菜单ID
     */
    public Long getMenuId() {
        return menuId;
    }

    /**
     * 设置菜单ID
     *
     * @param menuId 菜单ID
     */
    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    /**
     * 获取权限ID
     *
     * @return AUTH_ID - 权限ID
     */
    public Long getAuthId() {
        return authId;
    }

    /**
     * 设置权限ID
     *
     * @param authId 权限ID
     */
    public void setAuthId(Long authId) {
        this.authId = authId;
    }

    /**
     * 获取状态(0.禁用 1.启用 2.删除 )
     *
     * @return STATUS - 状态(0.禁用 1.启用 2.删除 )
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态(0.禁用 1.启用 2.删除 )
     *
     * @param status 状态(0.禁用 1.启用 2.删除 )
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取创建人
     *
     * @return CREATOR - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新人
     *
     * @return UPDATOR - 更新人
     */
    public String getUpdator() {
        return updator;
    }

    /**
     * 设置更新人
     *
     * @param updator 更新人
     */
    public void setUpdator(String updator) {
        this.updator = updator;
    }

    /**
     * 获取更新时间

     *
     * @return UPDATE_DATE - 更新时间

     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间

     *
     * @param updateDate 更新时间

     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}