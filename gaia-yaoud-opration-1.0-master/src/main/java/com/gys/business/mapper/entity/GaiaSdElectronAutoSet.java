package com.gys.business.mapper.entity;


import lombok.Data;

import java.io.Serializable;

/**
 * @author xiayuan
 * 电子券自动发券设置表
 */
@Data
public class GaiaSdElectronAutoSet implements Serializable {

    private static final long serialVersionUID = 1870311504939082628L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 主题单号
     */
    private String gseasId;

    /**
     * 主题属性
     */
    private String gseasFlag;

    /**
     * 电子券活动号
     */
    private String gsebId;
    /**
     * 发送数量
     */
    private String gseasQty;

    /**
     * 起始日期
     */
    private String gseasBeginDate;

    /**
     * 结束日期
     */
    private String gseasEndDate;

    /**
     * 创建日期
     */
    private String gseasCreateDate;

    /**
     * 创建时间
     */
    private String gseasCreateTime;

    /**
     * 是否有效
     */
    private String gseasValid;

}