package com.gys.business.mapper.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 18:39 2021/9/17
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "GAIA_GROSS_CONSTRUCTION")
public class GrossConstruction {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 定价毛利等级：ABCDE
     */
    @Column(name = "INTERVAL_TYPE")
    private String intervalType;

    /**
     * 起始毛利
     */
    @Column(name = "START_VALUE")
    private BigDecimal startValue;

    /**
     * 截止毛利
     */
    @Column(name = "END_VALUE")
    private BigDecimal endValue;

    /**
     * 商品分类编码
     */
    @Column(name = "PRO_BIG_CLASS_CODE")
    private String proBigClassCode;

    /**
     * 商品分类名称
     */
    @Column(name = "PRO_BIG_CLASS_NAME")
    private String proBigClassName;

    /**
     * 商品分类编码
     */
    @Column(name = "PRO_MID_CLASS_CODE")
    private String proMidClassCode;

    /**
     * 商品分类名称
     */
    @Column(name = "PRO_MID_CLASS_NAME")
    private String proMidClassName;

    /**
     * 商品分类编码
     */
    @Column(name = "PRO_CLASS_CODE")
    private String proClassCode;

    /**
     * 商品分类名称
     */
    @Column(name = "PRO_CLASS_NAME")
    private String proClassName;

    /**
     * 分类类型 1：大类、2：中类、3：小类
     */
    @Column(name = "CLASS_TYPE")
    private Integer classType;

    /**
     * 月份
     */
    @Column(name = "SALE_MONTH")
    private String saleMonth;

    /**
     * 销售额
     */
    @Column(name = "GGC_AMT")
    private BigDecimal ggcAmt;

    /**
     * 折扣
     */
    @Column(name = "GGC_ZK_AMT")
    private BigDecimal ggcZkAmt;

    /**
     * 定价
     */
    @Column(name = "GGC_PRICING")
    private BigDecimal ggcPricing;

    /**
     * 销售占比
     */
    @Column(name = "GGC_SALE_PROPORTION")
    private BigDecimal ggcSaleProportion;

    /**
     * 成本
     */
    @Column(name = "GGC_MOV")
    private BigDecimal ggcMov;

    /**
     * 动销品项
     */
    @Column(name = "GGC_PRO_COUNT")
    private Integer ggcProCount;
}
