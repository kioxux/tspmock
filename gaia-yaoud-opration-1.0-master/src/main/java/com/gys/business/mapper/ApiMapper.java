package com.gys.business.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ApiMapper {

    String selectPartnerSecret(@Param("appid") String appid);

    Map<String,String> selectPartnerParam(@Param("appid") String appid, @Param("cmd") String cmd);

    Map<String,String> selectPartnerStoInfo(@Param("appid") String appid, @Param("storeCode") String storeCode);

    List<Map<String,Object>> selectProStockCode(@Param("clientId") String clientId, @Param("storeCode") String storeCode);
}
