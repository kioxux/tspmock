package com.gys.business.mapper.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * 客户级别相关数据
 * GAIA_CLIENT_lEVEL_INFO
 * @author 
 */
@Data
public class GaiaClientBenefitLevelInfo implements Serializable {
    /**
     * 记录ID
     */
    private Long id;

    /**
     * 客户加盟ID
     */
    private String client;

    /**
     * 综合排名上限值
     */
    @NotBlank(message = "综合排名上限值不能为空")
    private Integer comprehensiveRank;

    /**
     * 门店库存品项合格品项数
     */
    @NotBlank(message = "合格品项项数不能为空")
    private Integer stockQualifiedCount;

    /**
     * 步进值
     */
    @NotBlank(message = "铺货品项数上限值不能为空")
    private Integer stepValue;

    /**
     * 店效级别 0-无 1-超高 2-高 3-中 4-低
     */

    private Integer storeLevel;

    /**
     * 是否是删除状态  1是0否
     */
    private Integer isDelete;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 更新者ID
     */
    private String updateUser;

    private static final long serialVersionUID = 1L;
}