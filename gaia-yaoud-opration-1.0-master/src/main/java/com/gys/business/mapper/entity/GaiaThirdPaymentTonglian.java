package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
@Data
@Table(name = "GAIA_THIRD_PAYMENT_TONGLIAN")
public class GaiaThirdPaymentTonglian implements Serializable {
    /**
     * 自增主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String CLIENT;

    /**
     * 门店
     */
    @Column(name = "STO_CODE")
    private String STO_CODE;

    /**
     * 业务编码，用于多业务模式
     */
    @Column(name = "BusinessId")
    private String BusinessId;

    /**
     * 交易类型
     */
    @Column(name = "TransType")
    private String TransType;

    /**
     * 卡种代码（银行卡01，万商通联02……）
     */
    @Column(name = "CardType")
    private String CardType;

    /**
     * 操作员号
     */
    @Column(name = "Operator")
    private String Operator;

    /**
     * 门店号
     */
    @Column(name = "StoreNumber")
    private String StoreNumber;

    /**
     * 收银机号
     */
    @Column(name = "PosNumber")
    private String PosNumber;

    /**
     * 小费
     */
    @Column(name = "Tips")
    private String Tips;

    /**
     * 总计
     */
    @Column(name = "Total")
    private String Total;

    /**
     * 金额
     */
    @Column(name = "Amount")
    private String Amount;

    /**
     * 余额
     */
    @Column(name = "BalanceAmount")
    private String BalanceAmount;

    /**
     * 流水号
     */
    @Column(name = "PosTraceNumber")
    private String PosTraceNumber;

    /**
     * 卡号
     */
    @Column(name = "CardNumber")
    private String CardNumber;

    /**
     * 原始流水号
     */
    @Column(name = "OldTraceNumber")
    private String OldTraceNumber;

    /**
     * 有效期
     */
    @Column(name = "ExpireDate")
    private String ExpireDate;

    /**
     * 批次号
     */
    @Column(name = "BatchNumber")
    private String BatchNumber;

    /**
     * 商户号
     */
    @Column(name = "MerchantNumber")
    private String MerchantNumber;

    /**
     * 商户名
     */
    @Column(name = "MerchantName")
    private String MerchantName;

    /**
     * 终端号
     */
    @Column(name = "TerminalNumber")
    private String TerminalNumber;

    /**
     * 系统参考号
     */
    @Column(name = "HostSerialNumber")
    private String HostSerialNumber;

    /**
     * 授权码
     */
    @Column(name = "AuthNumber")
    private String AuthNumber;

    /**
     * 返回码
     */
    @Column(name = "RejCode")
    private String RejCode;

    /**
     * 发卡行号
     */
    @Column(name = "IssNumber")
    private String IssNumber;

    /**
     * 发卡行名称
     */
    @Column(name = "IssName")
    private String IssName;

    /**
     * 交易日期
     */
    @Column(name = "TransDate")
    private String TransDate;

    /**
     * 交易时间
     */
    @Column(name = "TransTime")
    private String TransTime;

    /**
     * 返回码解释
     */
    @Column(name = "RejCodeExplain")
    private String RejCodeExplain;

    /**
     * 卡片回收标志
     */
    @Column(name = "CardBack")
    private String CardBack;

    /**
     * 备注
     */
    @Column(name = "Memo")
    private String Memo;

    /**
     * 交易唯一标识
     */
    @Column(name = "TransCheck")
    private String TransCheck;

    /**
     * 脱机标识：0-联机、1-脱机
     */
    @Column(name = "IsOffline")
    private String IsOffline;

    /**
     * 卡组织
     */
    @Column(name = "CUPS")
    private String CUPS;

    /**
     * 收银宝-交易单号（撤销、退货需传入，当网上统一收银时表示的是商户的交易单号，此时收银宝的交易单号用TrxId字段）
     */
    @Column(name = "TransId")
    private String TransId;

    /**
     * 打印内容
     */
    @Column(name = "PrintContent")
    private String PrintContent;

    /**
     * 优惠金额
     */
    @Column(name = "DiscountAmt")
    private String DiscountAmt;

    /**
     * 实付金额
     */
    @Column(name = "ActualPayment")
    private String ActualPayment;

    /**
     * 订单编号
     */
    @Column(name = "OrderNumber")
    private String OrderNumber;

    /**
     * 订单编号
     */
    @Column(name = "OldOrderNumber")
    private String OldOrderNumber;

    /**
     * 订单交易分账信息
     */
    @Column(name = "SubAcct")
    private String SubAcct;

    /**
     * 收银宝交易单号（只用在网上统一收银时）
     */
    @Column(name = "TrxId")
    private String TrxId;

    /**
     * 交易的状态,对于刷卡支付，该状态表示实际的支付结果，其他为下单状态（只用在网上统一收银时）
     */
    @Column(name = "TrxStatus")
    private String TrxStatus;

    private static final long serialVersionUID = 1L;

}