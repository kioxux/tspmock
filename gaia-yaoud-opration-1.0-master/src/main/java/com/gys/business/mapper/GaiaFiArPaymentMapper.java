package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaFiArPayment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 应收方式维护表(GaiaFiArPayment)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-18 15:29:32
 */
public interface GaiaFiArPaymentMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaFiArPayment queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaFiArPayment> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaFiArPayment 实例对象
     * @return 对象列表
     */
    List<GaiaFiArPayment> queryAll(GaiaFiArPayment gaiaFiArPayment);

    /**
     * 新增数据
     *
     * @param gaiaFiArPayment 实例对象
     * @return 影响行数
     */
    int insert(GaiaFiArPayment gaiaFiArPayment);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaFiArPayment> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaFiArPayment> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaFiArPayment> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaFiArPayment> entities);

    /**
     * 修改数据
     *
     * @param gaiaFiArPayment 实例对象
     * @return 影响行数
     */
    int update(GaiaFiArPayment gaiaFiArPayment);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    List<String> listFrance(GaiaFiArPayment inData);
}

