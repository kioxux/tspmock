package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_YB_STOCK_BATCH")
public class GaiaYbStockBatch implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店
     */
    @Id
    @Column(name = "BR_ID")
    private String brId;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "PRO_CODE")
    private String proCode;

    /**
     * 批次号
     */
    @Id
    @Column(name = "BATCH_NO")
    private String batchNo;

    /**
     * 批次
     */
    @Id
    @Column(name = "BATCH")
    private String batch;

    /**
     * 库存编码
     */
    @Column(name = "STOCK_CODE")
    private String stockCode;

    /**
     * 库存编码
     */
    @Column(name = "YB_PRO_CODE")
    private String ybStockBatch;


    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取门店
     *
     * @return BR_ID - 门店
     */
    public String getBrId() {
        return brId;
    }

    /**
     * 设置门店
     *
     * @param brId 门店
     */
    public void setBrId(String brId) {
        this.brId = brId;
    }

    /**
     * 获取商品编码
     *
     * @return PRO_CODE - 商品编码
     */
    public String getProCode() {
        return proCode;
    }

    /**
     * 设置商品编码
     *
     * @param proCode 商品编码
     */
    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    /**
     * 获取批次号
     *
     * @return BATCH_NO - 批次号
     */
    public String getBatchNo() {
        return batchNo;
    }

    /**
     * 设置批次号
     *
     * @param batchNo 批次号
     */
    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    /**
     * 获取批次
     *
     * @return BATCH - 批次
     */
    public String getBatch() {
        return batch;
    }

    /**
     * 设置批次
     *
     * @param batch 批次
     */
    public void setBatch(String batch) {
        this.batch = batch;
    }

    /**
     * 获取库存编码
     *
     * @return STOCK_CODE - 库存编码
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * 设置库存编码
     *
     * @param stockCode 库存编码
     */
    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    /**
     * 获取医保对应编码
     *
     * @return ybStockBatch - 库存编码
     */
    public String getYbStockBatch() {
        return ybStockBatch;
    }

    /**
     * 设置医保对应编码
     *
     * @param ybStockBatch 库存编码
     */
    public void setYbStockBatch(String ybStockBatch) {
        this.stockCode = stockCode;
    }
}