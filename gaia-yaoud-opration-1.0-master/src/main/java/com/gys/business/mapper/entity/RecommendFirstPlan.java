package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 首推商品方案表
 * </p>
 *
 * @author flynn
 * @since 2021-08-04
 */
@Data
@Table( name = "GAIA_SD_RECOMMEND_FIRST_PLAN")
public class RecommendFirstPlan implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    @Id
    @Column(name = "ID")
    private Long id;

    @ApiModelProperty(value = "加盟店")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "方案名称")
    @Column(name = "PLAN_NAME")
    private String planName;

    @ApiModelProperty(value = "方案生效开始时间")
    @Column(name = "EFFECT_START_DATE")
    private String effectStartDate;

//    @ApiModelProperty(value = "方案生效结束时间")
//    @Column(name = "EFFECT_END_DATE")
//    private String effectEndDate;

    @ApiModelProperty(value = "LSJ 表示零售价优先 MLL表示毛利额优先 XQ表示效期优先")
    @Column(name = "PREFERENCES")
    private String preferences;

    @ApiModelProperty(value = "状态 0表示已保存 1表示已审核 2表示已停用")
    @Column(name = "STATUS")
    private String status;

    @ApiModelProperty(value = "审核日期")
    @Column(name = "AUDIT_DATE")
    private String auditDate;

    @ApiModelProperty(value = "审核时间")
    @Column(name = "AUDIT_TIME")
    private String auditTime;

    @ApiModelProperty(value = "审核人")
    @Column(name = "AUDIT_BY")
    private String auditBy;

    @ApiModelProperty(value = "创建人")
    @Column(name = "CREATE_BY")
    private String createBy;

    @ApiModelProperty(value = "创建人")
    @Column(name = "CREATE_NAME")
    private String createName;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "CREATE_DATE")
    private String createDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "CREATE_TIME")
    private String createTime;


}
