//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaBatchStock;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

public interface GaiaBatchStockMapper extends BaseMapper<GaiaBatchStock> {
    String selectNextVoucherId(@Param("clientId") String clientId);
}
