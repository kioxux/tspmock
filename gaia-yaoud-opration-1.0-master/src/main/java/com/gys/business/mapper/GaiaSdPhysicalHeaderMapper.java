package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPhysicalHeader;
import com.gys.business.service.data.GetPhysicalCountHeadOutData;
import com.gys.business.service.data.GetPhysicalCountInData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdPhysicalHeaderMapper extends BaseMapper<GaiaSdPhysicalHeader> {
    List<GetPhysicalCountHeadOutData> listPhysical(GetPhysicalCountInData inData);

    List<GetPhysicalCountHeadOutData> listPhysicalDiff(GetPhysicalCountInData inData);

    List<String> fetchProIds(GetPhysicalCountInData inData);
}
