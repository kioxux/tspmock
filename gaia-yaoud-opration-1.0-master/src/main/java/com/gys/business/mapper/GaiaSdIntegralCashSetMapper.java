package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralCashSet;
import com.gys.business.service.data.ExchangeCashOutData;
import com.gys.business.service.data.GetPayInData;
import com.gys.business.service.data.integralExchange.IntegralMessageOutData;
import com.gys.business.service.data.integralExchange.IntegralPro;
import com.gys.business.service.data.integralExchange.IntegralRule;
import com.gys.business.service.data.integralExchange.IntegralServeInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdIntegralCashSetMapper extends BaseMapper<GaiaSdIntegralCashSet> {
    List<ExchangeCashOutData> exchangeCash(GetPayInData inData);

    List<GaiaSdIntegralCashSet> getAllByClientAndBrId(@Param("client")String client, @Param("brId")String depId);

    IntegralMessageOutData selectIntegralMessage(IntegralServeInData inData);

    List<IntegralRule> selectIntegralRuleList(@Param("clientId")String clientId, @Param("gsicsrCode")String gsicsrCode);

    List<IntegralPro> selectIntegralProList(@Param("clientId")String clientId, @Param("vourcherId")String vourcherId);

    IntegralMessageOutData selectActiveByStore(IntegralServeInData inData);
}
