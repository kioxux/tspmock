//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_DEPOSIT_D"
)
public class GaiaSdCashPaymentDetail implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSDD_VOUCHER_ID"
    )
    private String gsddVoucherId;
    @Column(
            name = "GSDD_BR_ID"
    )
    private String gsddBrId;
    @Column(
            name = "GSDD_SALE_DATE"
    )
    private String gsddSaleDate;
    @Column(
            name = "GSDD_SERIAL"
    )
    private String gsddSerial;
    @Column(
            name = "GSDD_EMP_GROUP"
    )
    private String gsddEmpGroup;
    @Column(
            name = "GSDD_EMP"
    )
    private String gsddEmp;
    @Column(
            name = "GSDD_RMB_AMT"
    )
    private String gsddRmbAmt;
    private static final long serialVersionUID = 1L;

    public GaiaSdCashPaymentDetail() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsddVoucherId() {
        return this.gsddVoucherId;
    }

    public void setGsddVoucherId(String gsddVoucherId) {
        this.gsddVoucherId = gsddVoucherId;
    }

    public String getGsddBrId() {
        return this.gsddBrId;
    }

    public void setGsddBrId(String gsddBrId) {
        this.gsddBrId = gsddBrId;
    }

    public String getGsddSaleDate() {
        return this.gsddSaleDate;
    }

    public void setGsddSaleDate(String gsddSaleDate) {
        this.gsddSaleDate = gsddSaleDate;
    }

    public String getGsddSerial() {
        return this.gsddSerial;
    }

    public void setGsddSerial(String gsddSerial) {
        this.gsddSerial = gsddSerial;
    }

    public String getGsddEmpGroup() {
        return this.gsddEmpGroup;
    }

    public void setGsddEmpGroup(String gsddEmpGroup) {
        this.gsddEmpGroup = gsddEmpGroup;
    }

    public String getGsddEmp() {
        return this.gsddEmp;
    }

    public void setGsddEmp(String gsddEmp) {
        this.gsddEmp = gsddEmp;
    }

    public String getGsddRmbAmt() {
        return this.gsddRmbAmt;
    }

    public void setGsddRmbAmt(String gsddRmbAmt) {
        this.gsddRmbAmt = gsddRmbAmt;
    }
}
