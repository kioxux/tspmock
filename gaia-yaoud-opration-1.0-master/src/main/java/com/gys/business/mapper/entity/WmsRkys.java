package com.gys.business.mapper.entity;

import com.gys.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/***
 * @desc: 入库验收实体
 * @author: ryan
 * @createTime: 2021/6/3 16:35
 **/
@Data
@EqualsAndHashCode(callSuper = false)
public class WmsRkys extends BaseEntity {
    /***/
    private String client;
    /***/
    private String wmRkdh;
    /***/
    private String wmCgddh;
    /***/
    private String wmDdxh;
    /***/
    private String wmSpBm;
    /***/
    private String wmPh;
    /***/
    private String wmScrq;
    /***/
    private String wmYxq;
    /***/
    private BigDecimal wmDdj;
    /***/
    private BigDecimal wmDhj;
    /***/
    private String wmSccj;
    /***/
    private String wmCd;
    /***/
    private BigDecimal wmShsl;
    /***/
    private BigDecimal wmJssl;
    /***/
    private String wmJsyy;
    /***/
    private String wmKcztBh;
    /***/
    private String wmFph;
    /***/
    private String wmYsjl;
    /***/
    private String wmXgrq;
    /***/
    private String wmXgsj;
    /***/
    private String wmXgrId;
    /***/
    private String wmXgr;
    /***/
    private String wmYsrq;
    /***/
    private String wmYssj;
    /***/
    private String wmYsrId;
    /***/
    private String wmYsr;
    /***/
    private String batchno;
    /***/
    private String proSite;
    /***/
    private String wmGysBh;
    /***/
    private String remark;
    /***/
    private Integer state;
    /***/
    private Long acceptedQuantity;
    /***/
    private String consignee;
    /***/
    private String receivingDate;
    /***/
    private String receivingTime;
    /***/
    private Long kucenId;
    /***/
    private String buyerName;
    /***/
    private String poCreateDate;
    /***/
    private String consigneeId;
    /***/
    private String recommendedCargono;
    /***/
    private String wmLqr;
    /***/
    private String wmLqrId;
    /***/
    private String wmLqsj;
    /***/
    private String poPaymentId;
    /***/
    private String approvalComments;
}
