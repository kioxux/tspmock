package com.gys.business.mapper.entity;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 应付方式维护表
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Data
@Accessors(chain = true)
@Table( name = "GAIA_FI_AP_PAYMENT")
public class GaiaFiApPayment implements Serializable {


    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "应付方式编码")
    @Column(name = "PAYMENT_ID")
    private String paymentId;

    @ApiModelProperty(value = "应付方式名称")
    @Column(name = "PAYMENT_NAME")
    private String paymentName;

    @ApiModelProperty(value = "创建人")
    @Column(name = "PAYMENT_CRE_ID")
    private String paymentCreId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "PAYMENT_CRE_DATE")
    private Date paymentCreDate;


    public static final String CLIENT = "CLIENT";

    public static final String PAYMENT_ID = "PAYMENT_ID";

    public static final String PAYMENT_NAME = "PAYMENT_NAME";

    public static final String PAYMENT_CRE_ID = "PAYMENT_CRE_ID";

    public static final String PAYMENT_CRE_DATE = "PAYMENT_CRE_DATE";

}
