package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPreordH;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPreordHMapper extends BaseMapper<GaiaSdPreordH> {

    /**
     * 本地库存
     *
     * @param inData
     * @return
     */
    List<OrderOutData> getProductByBusiness(OrderInData inData);

    /**
     * 全国库存
     *
     * @param inData
     * @return
     */
    List<OrderOutData> getProductByBasic(OrderInData inData);

    /**
     * 订购记录查询
     *
     * @param inData
     * @return
     */
    List<OrderRecordOutData> getOrderRecordList(OrderRecordInData inData);

    /**
     * 根据订购单号 查询
     *
     * @param inData
     * @return
     */
    GaiaSdPreordH getPreordHByVoucherId(OrderSaveInData inData);

    /**
     * 获得订购单号
     *
     * @param client
     * @return
     */
    String getVoucherIdByDG(@Param("client") String client);

    /**
     * 新增
     *
     * @param record
     * @return
     */
    int insertSelective(GaiaSdPreordH record);

    /**
     * 修改
     *
     * @param record
     */
    void update(GaiaSdPreordH record);

    /**
     * 获取
     * @param client
     * @return
     */
    String getVoucherIdByHX(String client);
}
