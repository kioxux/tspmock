package com.gys.business.mapper.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

/**
 * 商品调库-明细表(GaiaCommodityInventoryD)实体类
 *
 * @author makejava
 * @since 2021-10-19 16:01:15
 */
@Data
public class GaiaCommodityInventoryD implements Serializable {
    private static final long serialVersionUID = 769519039007682994L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店编码
     */
    private String stoCode;
    /**
     * 门店名称
     */
    private String stoName;
    /**
     * 商品调库单号
     */
    private String billCode;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 规格
     */
    private String proSpces;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 月均销量
     */
    private BigDecimal averageSalesQty;
    /**
     * 库存量
     */
    private BigDecimal inventoryQty;
    /**
     * 建议退库量
     */
    private BigDecimal suggestionReturnQty;
    /**
     * 建议退库成本额
     */
    private BigDecimal suggestionReturnCost;
    /**
     * 月均销售额
     */
    private BigDecimal averageSalesAmt;
    /**
     * 月均毛利额
     */
    private BigDecimal averageSalesGross;
    /**
     * 月均毛利率
     */
    private BigDecimal averageSalesGrossRate;
    /**
     * 成分分类
     */
    private String proCompclass;
    /**
     * 商品分类
     */
    private String proClass;
    /**
     * 单据状态：0-未推送 1-已推送
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;

}

