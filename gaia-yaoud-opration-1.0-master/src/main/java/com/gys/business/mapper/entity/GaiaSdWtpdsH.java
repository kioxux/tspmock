package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/14 15:29
 */
@Data
@Table(name = "GAIA_SD_WTPSD_H")
public class GaiaSdWtpdsH implements Serializable {
    private static final long serialVersionUID = 2518741337174308545L;

    /**
     * 加盟商
     */
    @Id
    @Column( name = "CLIENT")
    private String client;
    /**
     * 供应商
     */
    @Id
    @Column( name = "GSWH_SUP_ID")
    private String gswhSupId;
    /**
     * 配送单号
     */
    @Id
    @Column( name = "GSWH_VOUCHER_ID")
    private String gswhVoucherId;
    /**
     * 第三方门店编码
     */
    @Id
    @Column( name = "GSWH_DSF_BR_ID")
    private String gswhDsfBrId;
    /**
     * 开单日期
     */
    @Id
    @Column( name = "GSWH_DATE")
    private String gswhDate;
    /**
     * 药德门店编码
     */
    @Id
    @Column( name = "GSWH_BR_ID")
    private String gswhBrId;
    /**
     * 总金额
     */
    @Id
    @Column( name = "GSWH_AMT")
    private BigDecimal gswhAMT;
    /**
     * 总数量
     */
    @Id
    @Column( name = "GSWH_QTY")
    private BigDecimal gswhQTY;
    /**
     * 备注
     */
    @Id
    @Column( name = "GSWH_REMARKS")
    private String gswhRemarks;
    /**
     * 收货状态
     */
    @Id
    @Column( name = "GSWH_STATUS")
    private String gswhStatus;
    /**
     * 收货单号
     */
    @Id
    @Column( name = "GSWH_ACC_ID")
    private String gswhAccId;
    /**
     * 收货日期
     */
    @Id
    @Column( name = "GSWH_ACC_DATE")
    private String gswhAccDate;
    /**
     * 收货时间
     */
    @Id
    @Column( name = "GSWH_ACC_TIME")
    private String gswhAccTime;

    /**
     * 收货人
     */
    @Id
    @Column( name = "GSWH_ACC_EMP")
    private String gswhAccEMP;

    /**
     * 创建日期
     */
    @Id
    @Column( name = "GSWH_CRE_DATE")
    private String gswhCreDate;
    /**
     * 创建时间
     */
    @Id
    @Column( name = "GSWH_CRE_TIME")
    private String gswhCreTime;
    /**
     * 创建人
     */
    @Id
    @Column( name = "GSWH_CRE_EMP")
    private String gswhCreEMP;
    /**
     * 修改日期
     */
    @Id
    @Column( name = "GSWH_UPDATE_DATE")
    private String gswhUpdateDate;
    /**
     * 修改时间
     */
    @Id
    @Column( name = "GSWH_UPDATE_TIME")
    private String gswhUpdateTime;
    /**
     * 修改人
     */
    @Id
    @Column( name = "GSWH_UPDATE_EMP")
    private String gswhUpdateEMP;
}
