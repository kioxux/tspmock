package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaYbYdMatch;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaYbYdMatchMapper extends BaseMapper<GaiaYbYdMatch> {
    int insertBatch(List<GaiaYbYdMatch> inData);
}
