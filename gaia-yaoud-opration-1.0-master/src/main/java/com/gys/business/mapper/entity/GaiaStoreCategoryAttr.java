package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wu mao yin
 * @Description: 门店分类属性
 * @date 2021/10/21 9:37
 */
@Data
public class GaiaStoreCategoryAttr implements Serializable {

    @ApiModelProperty(value = "门店属性")
    private String stoAttribute;

    @ApiModelProperty(value = "是否医保店")
    private String stoIfMedical;

    @ApiModelProperty(value = "DTP")
    private String stoIfDtp;

    @ApiModelProperty(value = "税分类")
    private String stoTaxClass;

}