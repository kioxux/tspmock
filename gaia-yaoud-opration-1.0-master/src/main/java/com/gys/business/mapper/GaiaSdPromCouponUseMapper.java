//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromCouponUse;
import com.gys.business.service.data.PromCouponUseOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdPromCouponUseMapper extends BaseMapper<GaiaSdPromCouponUse> {
    List<PromCouponUseOutData> getDetail(PromoteInData inData);
}
