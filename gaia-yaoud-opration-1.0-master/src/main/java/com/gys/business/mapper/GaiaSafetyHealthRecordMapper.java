package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSafetyHealthRecord;
import com.gys.business.mapper.entity.GaiaUserData;

import java.util.List;

public interface GaiaSafetyHealthRecordMapper {
    int insertSelective(GaiaSafetyHealthRecord record);

    int deleteByPrimaryKey(GaiaSafetyHealthRecord record);

    int updateByPrimaryKeySelective(GaiaSafetyHealthRecord record);

    int submitByPrimaryKeySelective(GaiaSafetyHealthRecord record);

    List<GaiaSafetyHealthRecord> selectSafetyHealthRecord(GaiaSafetyHealthRecord record);

    List<GaiaUserData> selectUserList(GaiaSafetyHealthRecord inData);
}