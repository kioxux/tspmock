package com.gys.business.mapper;

import com.gys.business.bosen.form.InDataForm;
import com.gys.business.mapper.entity.GaiaSdExamineD;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.LinkedHashMap;
import java.util.List;

public interface GaiaSdExamineDMapper extends BaseMapper<GaiaSdExamineD> {
    List<GetExamineDetailOutData> detailList(GetExamineInData inData);

    List<GetExamineDetailOutData> examineDetailList(GetExamineInData inData);

    GaiaAcceptID getPoSupplierId(@Param("clientId") String clientId,@Param("gsahVoucherId") String gsahVoucherId, @Param("gsehBrId")String gsehBrId);

    List<ExamineOrderOutData> selectExamineOrderList(ExamineOrderInData inData);

    List<YbExamineOutData> queryExamime(YbInterfaceQueryData inData);

    /**
     * 获取门店入库数据
     * @param dataForm
     * @return
     */
    List<LinkedHashMap<String, Object>> getInData(InDataForm dataForm);
}
