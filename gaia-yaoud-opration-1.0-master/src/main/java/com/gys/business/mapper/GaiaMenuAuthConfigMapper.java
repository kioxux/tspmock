package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaMenuAuthConfig;
import com.gys.common.base.BaseMapper;

public interface GaiaMenuAuthConfigMapper extends BaseMapper<GaiaMenuAuthConfig> {
}