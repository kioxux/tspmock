package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/16 13:55
 */
@Data
@Table(name = "GAIA_SD_DSF_STOCK")
public class GaiaSdDsfStock implements Serializable {
    private static final long serialVersionUID = 2518741337174308546L;
    /**
     * 加盟商
     */
    @Column( name = "CLIENT")
    private String client;
    /**
     * 供应商编码
     */
    @Column( name = "SUP_ID")
    private String supId;
    /**
     * 第三方商品编码
     */
    @Column( name = "PRO_ID_DSF")
    private String proIdDsf;
    /**
     * v
     */
    @Column( name = "PRO_ID")
    private String proId;
    /**
     * 成本单价
     */
    @Column( name = "PRICE")
    private BigDecimal price;
    /**
     * 加盟商
     */
    @Column( name = "QTY")
    private BigDecimal qty;

}
