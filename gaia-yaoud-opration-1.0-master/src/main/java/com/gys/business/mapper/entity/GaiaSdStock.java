package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "GAIA_SD_STOCK")
public class GaiaSdStock implements Serializable {
    private static final long serialVersionUID = 951718067301203428L;
    /**
     * 店号
     */
    @Id
    @Column(name = "GSS_BR_ID")
    private String gssBrId;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSS_PRO_ID")
    private String gssProId;

    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 数量
     */
    @Column(name = "GSS_QTY")
    private String gssQty;

    /**
     * 更新日期
     */
    @Column(name = "GSS_UPDATE_DATE")
    private String gssUpdateDate;

    /**
     * 更新人员编号
     */
    @Column(name = "GSS_UPDATE_EMP")
    private String gssUpdateEmp;


}