package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SD_PAY_DAYREPORT_H")
public class GaiaSdPayDayreportH implements Serializable {
    /**
     * 单号
     */
    @Id
    @Column(name = "GPDH_VOUCHER_ID")
    private String gpdhVoucherId;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 店号
     */
    @Column(name = "GPDH_BR_ID")
    private String gpdhBrId;

    /**
     * 销售日期
     */
    @Column(name = "GPDH_SALE_DATE")
    private String gpdhSaleDate;

    /**
     * 审核日期
     */
    @Column(name = "GPDH_CHECK_DATE")
    private String gpdhCheckDate;

    /**
     * 审核时间
     */
    @Column(name = "GPDH_CHECK_TIME")
    private String gpdhCheckTime;

    /**
     * 对账员工
     */
    @Column(name = "GPDH_EMP")
    private String gpdhEmp;

    /**
     * 应收金额汇总
     */
    @Column(name = "GPDHTOTAL_SALES_AMT")
    private BigDecimal gpdhtotalSalesAmt;

    /**
     * 对账金额汇总
     */
    @Column(name = "GPDHTOTAL_INPUT_AMT")
    private BigDecimal gpdhtotalInputAmt;

    /**
     * 审核状态
     */
    @Column(name = "GPDH_STATUS")
    private String gpdhStatus;

    private static final long serialVersionUID = 1L;

    /**
     * 获取单号
     *
     * @return GPDH_VOUCHER_ID - 单号
     */
    public String getGpdhVoucherId() {
        return gpdhVoucherId;
    }

    /**
     * 设置单号
     *
     * @param gpdhVoucherId 单号
     */
    public void setGpdhVoucherId(String gpdhVoucherId) {
        this.gpdhVoucherId = gpdhVoucherId;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取店号
     *
     * @return GPDH_BR_ID - 店号
     */
    public String getGpdhBrId() {
        return gpdhBrId;
    }

    /**
     * 设置店号
     *
     * @param gpdhBrId 店号
     */
    public void setGpdhBrId(String gpdhBrId) {
        this.gpdhBrId = gpdhBrId;
    }

    /**
     * 获取销售日期
     *
     * @return GPDH_SALE_DATE - 销售日期
     */
    public String getGpdhSaleDate() {
        return gpdhSaleDate;
    }

    /**
     * 设置销售日期
     *
     * @param gpdhSaleDate 销售日期
     */
    public void setGpdhSaleDate(String gpdhSaleDate) {
        this.gpdhSaleDate = gpdhSaleDate;
    }

    /**
     * 获取审核日期
     *
     * @return GPDH_CHECK_DATE - 审核日期
     */
    public String getGpdhCheckDate() {
        return gpdhCheckDate;
    }

    /**
     * 设置审核日期
     *
     * @param gpdhCheckDate 审核日期
     */
    public void setGpdhCheckDate(String gpdhCheckDate) {
        this.gpdhCheckDate = gpdhCheckDate;
    }

    /**
     * 获取审核时间
     *
     * @return GPDH_CHECK_TIME - 审核时间
     */
    public String getGpdhCheckTime() {
        return gpdhCheckTime;
    }

    /**
     * 设置审核时间
     *
     * @param gpdhCheckTime 审核时间
     */
    public void setGpdhCheckTime(String gpdhCheckTime) {
        this.gpdhCheckTime = gpdhCheckTime;
    }

    /**
     * 获取对账员工
     *
     * @return GPDH_EMP - 对账员工
     */
    public String getGpdhEmp() {
        return gpdhEmp;
    }

    /**
     * 设置对账员工
     *
     * @param gpdhEmp 对账员工
     */
    public void setGpdhEmp(String gpdhEmp) {
        this.gpdhEmp = gpdhEmp;
    }

    /**
     * 获取应收金额汇总
     *
     * @return GPDHTOTAL_SALES_AMT - 应收金额汇总
     */
    public BigDecimal getGpdhtotalSalesAmt() {
        return gpdhtotalSalesAmt;
    }

    /**
     * 设置应收金额汇总
     *
     * @param gpdhtotalSalesAmt 应收金额汇总
     */
    public void setGpdhtotalSalesAmt(BigDecimal gpdhtotalSalesAmt) {
        this.gpdhtotalSalesAmt = gpdhtotalSalesAmt;
    }

    /**
     * 获取对账金额汇总
     *
     * @return GPDHTOTAL_INPUT_AMT - 对账金额汇总
     */
    public BigDecimal getGpdhtotalInputAmt() {
        return gpdhtotalInputAmt;
    }

    /**
     * 设置对账金额汇总
     *
     * @param gpdhtotalInputAmt 对账金额汇总
     */
    public void setGpdhtotalInputAmt(BigDecimal gpdhtotalInputAmt) {
        this.gpdhtotalInputAmt = gpdhtotalInputAmt;
    }

    /**
     * 获取审核状态
     *
     * @return GPDH_STATUS - 审核状态
     */
    public String getGpdhStatus() {
        return gpdhStatus;
    }

    /**
     * 设置审核状态
     *
     * @param gpdhStatus 审核状态
     */
    public void setGpdhStatus(String gpdhStatus) {
        this.gpdhStatus = gpdhStatus;
    }
}