package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMemberCardChange;
import com.gys.business.mapper.entity.UncouplingMemberCardInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdMemberCardChangeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdMemberCardChange record);

    int insertSelective(GaiaSdMemberCardChange record);

    GaiaSdMemberCardChange selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdMemberCardChange record);

    int updateByPrimaryKey(GaiaSdMemberCardChange record);

    int updateBatch(List<GaiaSdMemberCardChange> list);

    int batchInsert(@Param("list") List<GaiaSdMemberCardChange> list);

    // 获取会员卡解挂信息
    UncouplingMemberCardInfo queryUncouplingMemberCardInfo(@Param("client") String client,@Param("cardId") String cardId);

    GaiaSdMemberCardChange getCardChangeByNewCardId(@Param("client") String client, @Param("oldCardId") String oldCardId);

}