package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDcData;
import com.gys.business.mapper.entity.GaiaSdPhysicalCountDiff;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPhysicalCountDiffMapper extends BaseMapper<GaiaSdPhysicalCountDiff> {
    String selectNextVoucherId(@Param("client") String client, @Param("codePre") String codePre);

    List<GetPhysicalCountOutData> selectPhysicalDiff(GetPhysicalCountInData inData);

    GaiaSdPhysicalCountDiff selectOnlyOne(GetPhysicalCountInData inData);

    /**
     * 报损报溢新版报表
     * @param inData
     * @return
     */
    List<PhysicalNewReportOutData> fetchPhysicalNewReport(GetPhysicalCountInData inData);

    /**
     * 报表查询
     * @param inData
     * @return
     */
    List<InventoryReportOutData> selectInventoyResportList(InventoryReportInData inData);

    /**
     * 查询加盟上下的门店
     * @param inData
     * @return
     */
    List<DifferenceResultQueryOutData> getDifferenceResultQueryByBrId(DifferenceResultQueryInVo inData);

    /**
     * 查询所有的
     * @param inData
     * @return
     */
    List<DifferenceResultDetailedQueryOutData> getDifferenceResultDetailedQuery(DifferenceResultQueryInVo inData);

    //盘点单号查询 仓库 商品单号查询 或 商品编码查询
    List<DifferenceResultDetailedQueryOutData> getDifferenceResultWarehouse(DifferenceResultQueryInVo inData);

    //盘点数据差异
    List<DifferenceResultDetailedQueryOutData> getGoodsNumber(DifferenceResultQueryInVo inData);

    GaiaDcData getDcData(@Param("client") String client, @Param("brId") String brId);

    /**
     * 加盟商店号查询
     * @param inData
     * @return
     */
    List<DifferenceResultDetailedQueryOutData> getDifferenceResultDetailedQueryByClientBrId(DifferenceResultQueryInVo inData);

    /**
     * 加盟商下的 仓库查询
     * @param inData
     * @return
     */
    List<DifferenceResultDetailedQueryOutData> getDifferenceResultDetailedQueryByClientBrCode(DifferenceResultQueryInVo inData);

    /**
     * 根据加盟商 商品id 获得,零售价
     * @param client
     * @param proId
     * @return
     */
    List<GsppPriceNormalOutData> getGsppPriceNormalByClientAndProIds(@Param("client")String client,@Param("list") List<String> proId);

    String getMinBrId(@Param("client") String client);

    /**
     * 查询加盟商下的仓库
     * @param inData
     * @return
     */
    List<DifferenceResultQueryOutData> getDifferenceResultQueryByBrSite(DifferenceResultQueryInVo inData);

    /**
     * 查询所有
     * @param inData
     * @return
     */
    List<DifferenceResultQueryOutData> getDifferenceResultQueryByAll(DifferenceResultQueryInVo inData);

    /**
     * 根据门店盘点单据查询
     * @param inData
     * @return
     */
    List<InventoryDocumentsOutData> inventoryDocumentQueryByBrId(DifferenceResultQueryInVo inData);

    /**
     * 门店盘点详情
     * @param inData
     * @return
     */
    List<InventoryDetailsData> storeInventoryDetails(InventoryReportInData inData);
}
