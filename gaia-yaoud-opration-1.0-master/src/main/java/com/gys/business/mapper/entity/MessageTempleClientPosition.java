package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 消息模版-门店-职位关系表
 * </p>
 *
 * @author flynn
 * @since 2021-09-10
 */
@Data
@Table( name = "GAIA_MESSAGE_TEMPLE_CLIENT_POSITION")
public class MessageTempleClientPosition implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    @Id
    @Column(name = "ID")
    private Integer id;

    @ApiModelProperty(value = "消息模版id")
    @Column(name = "GMT_ID")
    private String gmtId;

    @ApiModelProperty(value = "加盟商ID")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "职位名")
    @Column(name = "POSITION_NAME")
    private String positionName;

    @ApiModelProperty(value = "职位id")
    @Column(name = "POSITION_ID")
    private String positionId;

    @ApiModelProperty(value = "模块编码")
    @Column(name = "MODULE_CODE")
    private String moduleCode;

}
