package com.gys.business.mapper;

import com.gys.business.service.data.replenishParam.ReplenishGift;
import com.gys.business.service.data.replenishParam.ReplenishGiftOutParam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ReplenishGiftMapper {

    List<ReplenishGiftOutParam> selectGiftList(@Param("clientId")String clientId, @Param("siteCode") String siteCode,@Param("proCode") String proCode,@Param("giftCode") String giftCode);

    List<ReplenishGiftOutParam> selectGiftDetailList(@Param("clientId")String clientId, @Param("siteCode") String siteCode,@Param("proCode") String proCode,@Param("giftCode") String giftCode);

    void deleteGiftDetail(@Param("clientId")String clientId, @Param("stoCode") String stoCode,@Param("voucherId") String voucherId);

    void addGiftDetail(List<ReplenishGift> list);

    List<ReplenishGiftOutParam> selectGiftBySet(@Param("clientId")String clientId, @Param("siteCode") String siteCode,@Param("proCode") String proCode);

    List<ReplenishGiftOutParam> selectGiftByRecord(@Param("clientId")String clientId, @Param("siteCode") String siteCode,@Param("proCode") String proCode,@Param("voucherId") String voucherId);

}
