package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPaymentMethod;
import com.gys.business.service.data.GetPayInData;
import com.gys.business.service.data.GetPayTypeOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPaymentMethodMapper extends BaseMapper<GaiaSdPaymentMethod> {
    List<GetPayTypeOutData> payTypeList(GetPayInData inData);

    List<GetPayTypeOutData> payTypeListByClient(GetPayInData inData);


    List<GaiaSdPaymentMethod> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);
}
