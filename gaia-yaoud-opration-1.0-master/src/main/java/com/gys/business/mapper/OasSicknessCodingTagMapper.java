package com.gys.business.mapper;

import com.gys.business.mapper.entity.OasSicknessCodingTag;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 疾病大类-疾病中类关系表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-09-02
 */
@Mapper
public interface OasSicknessCodingTagMapper extends BaseMapper<OasSicknessCodingTag> {
}
