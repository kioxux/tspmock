package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(name = "GAIA_PRODUCT_RELATE")
public class GaiaProductRelate implements Serializable {
    private static final long serialVersionUID = 8154179428628858228L;
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "PRO_SITE"
    )
    private String proSite;
    @Id
    @Column(
            name = "PRO_SELF_CODE"
    )
    private String proSelfCode;
    @Id
    @Column(
            name = "SUP_SELF_CODE"
    )
    private String supSelfCode;
    @Column(
            name = "PRO_RELATE_CODE"
    )
    private String proRelateCode;
    @Column(
            name = "PRO_PRICE"
    )
    private String proPrice;
    @Column(
            name = "PRO_CREATE_USER"
    )
    private String proCreateUser;
    @Column(
            name = "PRO_CREATE_DATE"
    )
    private String proCreateDate;
    @Column(
            name = "PRO_CREATE_TIME"
    )
    private String proCreateTime;
    @Column(
            name = "PRO_CHANGE_USER"
    )
    private String proChangeUser;
    @Column(
            name = "PRO_CHANGE_DATE"
    )
    private String proChangeDate;
    @Column(
            name = "PRO_CHANGE_TIME"
    )
    private String proChangeTime;
}
