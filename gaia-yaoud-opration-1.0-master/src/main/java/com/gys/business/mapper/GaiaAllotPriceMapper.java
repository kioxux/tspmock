package com.gys.business.mapper;

import com.gys.business.mapper.entity.AllotPrice;
import org.apache.ibatis.annotations.Param;
import org.apache.xmlbeans.impl.xb.xsdschema.All;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/15 15:00
 **/
public interface GaiaAllotPriceMapper {

    AllotPrice getByUnique(AllotPrice cond);

    AllotPrice getStoreAllotPrice(AllotPrice cond);

    int add(AllotPrice allotPrice);

    void updateByClientAndPro(AllotPrice allotPrice);
}
