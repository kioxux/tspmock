package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_DISEASE_MEDICATION")
public class GaiaDiseaseMedication implements Serializable {
    @Id
    @Column(name = "ID")
    private Integer id;

    /**
     * 疾病编码
     */
    @Column(name = "DISEASE_CODE")
    private String diseaseCode;

    /**
     * 方案代码
     */
    @Column(name = "SCHEME_CODE")
    private String schemeCode;

    /**
     * 关联组方
     */
    @Column(name = "ASSOCIATED_PARTY")
    private String associatedParty;

    /**
     * 辅助用药1/预防保健2
     */
    @Column(name = "AUXILIARY_MEDICATION")
    private String auxiliaryMedication;

    /**
     * 关联代码
     */
    @Column(name = "ASSOCIATION_CODE")
    private String associationCode;

    /**
     * 关联成分编码
     */
    @Column(name = "COMPONENT_CODE")
    private String componentCode;

    /**
     * 关联成分细类描述
     */
    @Column(name = "INGREDIENT_DESCRIPTION")
    private String ingredientDescription;

    /**
     * 关联说明
     */
    @Column(name = "ASSOCIATION_DESCRIPTION")
    private String associationDescription;

    /**
     * 分组优先级
     */
    @Column(name = "GROUP_PRIORITY")
    private byte[] groupPriority;

    private static final long serialVersionUID = 1L;

    /**
     * @return ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取疾病编码
     *
     * @return DISEASE_CODE - 疾病编码
     */
    public String getDiseaseCode() {
        return diseaseCode;
    }

    /**
     * 设置疾病编码
     *
     * @param diseaseCode 疾病编码
     */
    public void setDiseaseCode(String diseaseCode) {
        this.diseaseCode = diseaseCode;
    }

    /**
     * 获取方案代码
     *
     * @return SCHEME_CODE - 方案代码
     */
    public String getSchemeCode() {
        return schemeCode;
    }

    /**
     * 设置方案代码
     *
     * @param schemeCode 方案代码
     */
    public void setSchemeCode(String schemeCode) {
        this.schemeCode = schemeCode;
    }

    /**
     * 获取关联组方
     *
     * @return ASSOCIATED_PARTY - 关联组方
     */
    public String getAssociatedParty() {
        return associatedParty;
    }

    /**
     * 设置关联组方
     *
     * @param associatedParty 关联组方
     */
    public void setAssociatedParty(String associatedParty) {
        this.associatedParty = associatedParty;
    }

    /**
     * 获取辅助用药1/预防保健2
     *
     * @return AUXILIARY_MEDICATION - 辅助用药1/预防保健2
     */
    public String getAuxiliaryMedication() {
        return auxiliaryMedication;
    }

    /**
     * 设置辅助用药1/预防保健2
     *
     * @param auxiliaryMedication 辅助用药1/预防保健2
     */
    public void setAuxiliaryMedication(String auxiliaryMedication) {
        this.auxiliaryMedication = auxiliaryMedication;
    }

    /**
     * 获取关联代码
     *
     * @return ASSOCIATION_CODE - 关联代码
     */
    public String getAssociationCode() {
        return associationCode;
    }

    /**
     * 设置关联代码
     *
     * @param associationCode 关联代码
     */
    public void setAssociationCode(String associationCode) {
        this.associationCode = associationCode;
    }

    /**
     * 获取关联成分编码
     *
     * @return COMPONENT_CODE - 关联成分编码
     */
    public String getComponentCode() {
        return componentCode;
    }

    /**
     * 设置关联成分编码
     *
     * @param componentCode 关联成分编码
     */
    public void setComponentCode(String componentCode) {
        this.componentCode = componentCode;
    }

    /**
     * 获取关联成分细类描述
     *
     * @return INGREDIENT_DESCRIPTION - 关联成分细类描述
     */
    public String getIngredientDescription() {
        return ingredientDescription;
    }

    /**
     * 设置关联成分细类描述
     *
     * @param ingredientDescription 关联成分细类描述
     */
    public void setIngredientDescription(String ingredientDescription) {
        this.ingredientDescription = ingredientDescription;
    }

    /**
     * 获取关联说明
     *
     * @return ASSOCIATION_DESCRIPTION - 关联说明
     */
    public String getAssociationDescription() {
        return associationDescription;
    }

    /**
     * 设置关联说明
     *
     * @param associationDescription 关联说明
     */
    public void setAssociationDescription(String associationDescription) {
        this.associationDescription = associationDescription;
    }

    /**
     * 获取分组优先级
     *
     * @return GROUP_PRIORITY - 分组优先级
     */
    public byte[] getGroupPriority() {
        return groupPriority;
    }

    /**
     * 设置分组优先级
     *
     * @param groupPriority 分组优先级
     */
    public void setGroupPriority(byte[] groupPriority) {
        this.groupPriority = groupPriority;
    }
}