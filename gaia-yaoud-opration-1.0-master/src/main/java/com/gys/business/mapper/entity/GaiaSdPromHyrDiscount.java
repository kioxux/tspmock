package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 会员类促销会员日折扣设置表
 */
@Table(name = "GAIA_SD_PROM_HYR_DISCOUNT")
@Data
public class GaiaSdPromHyrDiscount implements Serializable {
    private static final long serialVersionUID = -7884630820142665931L;
    /**
     * 加盟号
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPP_VOUCHER_ID")
    private String gsppVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPP_SERIAL")
    private String gsppSerial;

    /**
     * 起始日期
     */
    @Column(name = "GSPP_BEGIN_DATE")
    private String gsppBeginDate;

    /**
     * 结束日期
     */
    @Column(name = "GSPP_END_DATE")
    private String gsppEndDate;

    /**
     * 起始时间
     */
    @Column(name = "GSPP_BEGIN_TIME")
    private String gsppBeginTime;

    /**
     * 结束时间
     */
    @Column(name = "GSPP_END_TIME")
    private String gsppEndTime;

    /**
     * 按日期
     */
    @Column(name = "GSPP_DATE_FREQUENCY")
    private String gsppDateFrequency;

    /**
     * 按星期
     */
    @Column(name = "GSPP_TIME_FREQUENCY")
    private String gsppTimeFrequency;

    /**
     * 折扣
     */
    @Column(name = "GSPP_REBATE")
    private String gsppRebate;

    /**
     * 是否积分
     */
    @Column(name = "GSPP_INTE_FLAG")
    private String gsppInteFlag;

    /**
     * 积分倍率
     */
    @Column(name = "GSPP_INTE_RATE")
    private String gsppInteRate;

}
