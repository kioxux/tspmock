//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaPoHeader;
import com.gys.business.mapper.entity.GaiaPoItem;
import com.gys.business.service.data.GetAcceptInData;
import com.gys.business.service.data.GetPoDetailOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaPoItemMapper extends BaseMapper<GaiaPoItem> {
    List<GetPoDetailOutData> poDetailList(GetAcceptInData inData);

    void insertBatch(List<GaiaPoItem> poHeaderList);

    void modifyBatch(List<GaiaPoItem> list);
}
