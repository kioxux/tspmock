package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDiseaseClassifcation;
import com.gys.common.base.BaseMapper;

public interface GaiaDiseaseClassifcationMapper extends BaseMapper<GaiaDiseaseClassifcation> {
}