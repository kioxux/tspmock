package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SD_PAY_DAYREPORT_D")
public class GaiaSdPayDayreportD implements Serializable {
    /**
     * 行号
     */
    @Id
    @Column(name = "GSPDD_SERIAL")
    private String gspddSerial;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 单号
     */
    @Column(name = "GSPDD_VOUCHER_ID")
    private String gspddVoucherId;

    /**
     * 销售支付方式
     */
    @Column(name = "GSPDD_SALE_PAYMETHOD_ID")
    private String gspddSalePaymethodId;

    /**
     * 销售应收金额
     */
    @Column(name = "GSPDD_SALE_RECEIVABLE_AMT")
    private BigDecimal gspddSaleReceivableAmt;

    /**
     * 销售对账金额
     */
    @Column(name = "GSPDD_SALE_INPUT_AMT")
    private BigDecimal gspddSaleInputAmt;

    /**
     * 储值卡充值支付方式
     */
    @Column(name = "GSPDD_RECARD_PAYMETHOD_ID")
    private String gspddRecardPaymethodId;

    /**
     * 储值卡充值应收金额
     */
    @Column(name = "GSPDD_RECARD_RECEIVABLE_AMT")
    private BigDecimal gspddRecardReceivableAmt;

    /**
     * 储值卡对账金额
     */
    @Column(name = "GSPDD_RECARD_INPUT_AMT")
    private BigDecimal gspddRecardInputAmt;

    private static final long serialVersionUID = 1L;

    /**
     * 获取行号
     *
     * @return GSPDD_SERIAL - 行号
     */
    public String getGspddSerial() {
        return gspddSerial;
    }

    /**
     * 设置行号
     *
     * @param gspddSerial 行号
     */
    public void setGspddSerial(String gspddSerial) {
        this.gspddSerial = gspddSerial;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取单号
     *
     * @return GSPDD_VOUCHER_ID - 单号
     */
    public String getGspddVoucherId() {
        return gspddVoucherId;
    }

    /**
     * 设置单号
     *
     * @param gspddVoucherId 单号
     */
    public void setGspddVoucherId(String gspddVoucherId) {
        this.gspddVoucherId = gspddVoucherId;
    }

    /**
     * 获取销售支付方式
     *
     * @return GSPDD_SALE_PAYMETHOD_ID - 销售支付方式
     */
    public String getGspddSalePaymethodId() {
        return gspddSalePaymethodId;
    }

    /**
     * 设置销售支付方式
     *
     * @param gspddSalePaymethodId 销售支付方式
     */
    public void setGspddSalePaymethodId(String gspddSalePaymethodId) {
        this.gspddSalePaymethodId = gspddSalePaymethodId;
    }

    /**
     * 获取销售应收金额
     *
     * @return GSPDD_SALE_RECEIVABLE_AMT - 销售应收金额
     */
    public BigDecimal getGspddSaleReceivableAmt() {
        return gspddSaleReceivableAmt;
    }

    /**
     * 设置销售应收金额
     *
     * @param gspddSaleReceivableAmt 销售应收金额
     */
    public void setGspddSaleReceivableAmt(BigDecimal gspddSaleReceivableAmt) {
        this.gspddSaleReceivableAmt = gspddSaleReceivableAmt;
    }

    /**
     * 获取销售对账金额
     *
     * @return GSPDD_SALE_INPUT_AMT - 销售对账金额
     */
    public BigDecimal getGspddSaleInputAmt() {
        return gspddSaleInputAmt;
    }

    /**
     * 设置销售对账金额
     *
     * @param gspddSaleInputAmt 销售对账金额
     */
    public void setGspddSaleInputAmt(BigDecimal gspddSaleInputAmt) {
        this.gspddSaleInputAmt = gspddSaleInputAmt;
    }

    /**
     * 获取储值卡充值支付方式
     *
     * @return GSPDD_RECARD_PAYMETHOD_ID - 储值卡充值支付方式
     */
    public String getGspddRecardPaymethodId() {
        return gspddRecardPaymethodId;
    }

    /**
     * 设置储值卡充值支付方式
     *
     * @param gspddRecardPaymethodId 储值卡充值支付方式
     */
    public void setGspddRecardPaymethodId(String gspddRecardPaymethodId) {
        this.gspddRecardPaymethodId = gspddRecardPaymethodId;
    }

    /**
     * 获取储值卡充值应收金额
     *
     * @return GSPDD_RECARD_RECEIVABLE_AMT - 储值卡充值应收金额
     */
    public BigDecimal getGspddRecardReceivableAmt() {
        return gspddRecardReceivableAmt;
    }

    /**
     * 设置储值卡充值应收金额
     *
     * @param gspddRecardReceivableAmt 储值卡充值应收金额
     */
    public void setGspddRecardReceivableAmt(BigDecimal gspddRecardReceivableAmt) {
        this.gspddRecardReceivableAmt = gspddRecardReceivableAmt;
    }

    /**
     * 获取储值卡对账金额
     *
     * @return GSPDD_RECARD_INPUT_AMT - 储值卡对账金额
     */
    public BigDecimal getGspddRecardInputAmt() {
        return gspddRecardInputAmt;
    }

    /**
     * 设置储值卡对账金额
     *
     * @param gspddRecardInputAmt 储值卡对账金额
     */
    public void setGspddRecardInputAmt(BigDecimal gspddRecardInputAmt) {
        this.gspddRecardInputAmt = gspddRecardInputAmt;
    }
}