package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdNewStoreDistributionRecordD;
import java.util.List;

import com.gys.business.service.data.NewStoreDistributionOutData;
import com.gys.business.service.data.NewStoreDistributionRecordDInData;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdNewStoreDistributionRecordDMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdNewStoreDistributionRecordD record);

    int insertSelective(GaiaSdNewStoreDistributionRecordD record);

    GaiaSdNewStoreDistributionRecordD selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdNewStoreDistributionRecordD record);

    int updateByPrimaryKey(GaiaSdNewStoreDistributionRecordD record);

    int updateBatch(List<GaiaSdNewStoreDistributionRecordD> list);

    int batchInsert(@Param("list") List<GaiaSdNewStoreDistributionRecordD> list);

    int deleteByCondition(@Param("client") String client, @Param("storeId") String storeId, @Param("createDate") String createDate);

    List<NewStoreDistributionOutData> getNewStoreDistributionList(NewStoreDistributionRecordDInData inData);
}