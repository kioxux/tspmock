package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWmsDiaoboZ;
import com.gys.business.service.data.GetAcceptInData;
import com.gys.business.service.data.GetDiaoboOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GaiaWmsDiaoboZMapper extends BaseMapper<GaiaWmsDiaoboZ> {
    List<GetDiaoboOutData> diaoboList(GetAcceptInData inData);

    List<GetDiaoboOutData> diaoboListToPrice(GetAcceptInData inData);

    /**
     * 获取账期外金额
     * @param client 加盟商
     * @param storeId 门店编码
     * @param date 账期日期
     * @return 金额
     */
    BigDecimal getZqAmount(@Param("client") String client, @Param("storeId") String storeId, @Param("zqDate") String date);
}
