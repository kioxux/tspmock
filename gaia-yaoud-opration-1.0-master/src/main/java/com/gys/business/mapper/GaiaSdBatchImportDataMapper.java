package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdBatchImportData;
import com.gys.common.base.BaseMapper;

/**
 * <p>
 * 门店手工流向数据表 Mapper 接口
 * </p>
 *
 * @author yifan.wang
 * @since 2021-08-04
 */
public interface GaiaSdBatchImportDataMapper extends BaseMapper<GaiaSdBatchImportData> {
}
