package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 16:58 2021/7/22
 * @Description：往来管理余额表实体
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
public class GaiaComeAndGoResidualData {

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "仓库编码")
    private String dcCode;
    @ApiModelProperty(value = "仓库名称")
    private String dcName;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "门店名称")
    private String stoName;
    @ApiModelProperty(value = "余额")
    private String residualAmt;
    @ApiModelProperty(value = "计算日期")
    private String balanceDate;
}
