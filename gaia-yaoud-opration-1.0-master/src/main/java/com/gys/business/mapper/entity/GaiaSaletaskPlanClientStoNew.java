package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "GAIA_SALETASK_PLAN_STO")
public class GaiaSaletaskPlanClientStoNew implements Serializable {
    /**
     * 客户ID
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 计划月份
     */
    @Id
    @Column(name = "MONTH_PLAN")
    private String monthPlan;

    /**
     * 门店编号
     */
    @Id
    @Column(name = "STO_CODE")
    private String stoCode;

    /**
     * 任务ID
     */
    @Id
    @Column(name = "STASK_TASK_ID")
    private String staskTaskId;

    /**
     * 期间天数
     */
    @Column(name = "MONTH_DAYS")
    private Integer monthDays;

    /**
     * 营业额本期日均计划
     */
    @Column(name = "A_SALE_AMT")
    private BigDecimal aSaleAmt;

    /**
     * 毛利额本期日均计划
     */
    @Column(name = "A_SALE_GROSS")
    private BigDecimal aSaleGross;

    /**
     * 会员卡本期日均计划
     */
    @Column(name = "A_MCARD_QTY")
    private Integer aMcardQty;

    /**
     * 创建人账号
     */
    @Column(name = "STASK_CRE_ID")
    private String staskCreId;

    /**
     * 创建日期
     */
    @Column(name = "STASK_CRE_DATE")
    private String staskCreDate;

    /**
     * 创建时间
     */
    @Column(name = "STASK_CRE_TIME")
    private String staskCreTime;

    /**
     * 修改人
     */
    @Column(name = "STASK_MD_ID")
    private String staskMdId;

    /**
     * 修改日期
     */
    @Column(name = "STASK_MD_DATE")
    private String staskMdDate;

    /**
     * 修改时间
     */
    @Column(name = "STASK_MD_TIME")
    private String staskMdTime;

    /**
     * 删除 0 未删除 1已删除
     */
    @Column(name = "STASK_TYPE")
    private String staskType;

    @Column(name = "STASK_IS_SHOW")
    private String staskIsShow;//0 否 1 是


    @Column(name = "A_SALE_AMT_SDAILY")
    private String aSaleAmtSdaily;//营业额同期日均

    @Column(name = "A_SALE_AMT_PDAILY")
    private String aSaleAmtPdaily;//营业额上期日均

    @Column(name = "A_SALE_GROSS_SDAILY")
    private String aSaleGrossSdaily;//毛利额同期日均
    @Column(name = "A_SALE_GROSS_PDAILY")
    private String aSaleGrossPdaily;//毛利额上期日均

    @Column(name = "A_SALE_GROSS_RATE_SDAILY")
    private String aSaleGrossRateSdaily;//毛利率同期日均
    @Column(name = "A_SALE_GROSS_RATE_PDAILY")
    private String aSaleGrossRatePdaily;//毛利率上期日均

    @Column(name = "A_MCARD_QTY_SDAILY")
    private String aMcardQtySdAily;//会员卡同期日均

    @Column(name = "A_MCARD_QTY_PDAILY")
    private String aMcardQtyPdAily;//会员卡上期日均

    @Column(name = "YOY_SALE_AMT")
    private String yoySaleAmt;//营业额同比增长率

    @Column(name = "MOM_SALE_AMT")
    private String momSaleAmt;//营业额环比增长率

    @Column(name = "YOY_SALE_GROSS")
    private String yoySaleGross;//毛利额同比增长率

    @Column(name = "MOM_SALE_GROSS")
    private String momSaleGross;//毛利额环比增长率

    @Column(name = "YOY_MCARD_QTY")
    private String yoyMcardQty;//会员卡同比增长率

    @Column(name = "MOM_MCARD_QTY")
    private String momMcardQty;//会员卡环比增长率

    public String getaSaleAmtSdaily() {
        return aSaleAmtSdaily;
    }

    public String getaSaleAmtPdaily() {
        return aSaleAmtPdaily;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 1L;

    public String getYoySaleAmt(@NotNull(message = "营业额同比增长率不可为空!") String yoySaleAmt) {
        return this.yoySaleAmt;
    }

    public void setYoySaleAmt(String yoySaleAmt) {
        this.yoySaleAmt = yoySaleAmt;
    }

    public String getMomSaleAmt(@NotNull(message = "营业额环比增长率不可为空!") String momSaleAmt) {
        return this.momSaleAmt;
    }

    public void setMomSaleAmt(String momSaleAmt) {
        this.momSaleAmt = momSaleAmt;
    }

    public String getYoySaleGross(@NotNull(message = "毛利额同比增长率不可为空!") String yoySaleGross) {
        return this.yoySaleGross;
    }

    public void setYoySaleGross(String yoySaleGross) {
        this.yoySaleGross = yoySaleGross;
    }

    public String getMomSaleGross(@NotNull(message = "毛利额环比增长率不可为空!") String momSaleGross) {
        return this.momSaleGross;
    }

    public void setMomSaleGross(String momSaleGross) {
        this.momSaleGross = momSaleGross;
    }


    public String getYoyMcardQty(@NotNull(message = "会员卡同比增长率不可为空!") String yoyMcardQty) {
        return this.yoyMcardQty;
    }

    public void setYoyMcardQty(String yoyMcardQty) {
        this.yoyMcardQty = yoyMcardQty;
    }

    public String getMomMcardQty(@NotNull(message = "会员卡环比增长率不可为空!") String momMcardQty) {
        return this.momMcardQty;
    }

    public void setMomMcardQty(String momMcardQty) {
        this.momMcardQty = momMcardQty;
    }

    /**
     * 获取客户ID
     *
     * @return CLIENT - 客户ID
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置客户ID
     *
     * @param client 客户ID
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取计划月份
     *
     * @return MONTH_PLAN - 计划月份
     */
    public String getMonthPlan() {
        return monthPlan;
    }

    /**
     * 设置计划月份
     *
     * @param monthPlan 计划月份
     */
    public void setMonthPlan(String monthPlan) {
        this.monthPlan = monthPlan;
    }

    /**
     * 获取门店编号
     *
     * @return STO_CODE - 门店编号
     */
    public String getStoCode() {
        return stoCode;
    }

    /**
     * 设置门店编号
     *
     * @param stoCode 门店编号
     */
    public void setStoCode(String stoCode) {
        this.stoCode = stoCode;
    }

    /**
     * 获取任务ID
     *
     * @return STASK_TASK_ID - 任务ID
     */
    public String getStaskTaskId() {
        return staskTaskId;
    }

    /**
     * 设置任务ID
     *
     * @param staskTaskId 任务ID
     */
    public void setStaskTaskId(String staskTaskId) {
        this.staskTaskId = staskTaskId;
    }

    /**
     * 获取期间天数
     *
     * @return MONTH_DAYS - 期间天数
     */
    public Integer getMonthDays() {
        return monthDays;
    }

    /**
     * 设置期间天数
     *
     * @param monthDays 期间天数
     */
    public void setMonthDays(Integer monthDays) {
        this.monthDays = monthDays;
    }

    /**
     * 获取营业额本期日均计划
     *
     * @return A_SALE_AMT - 营业额本期日均计划
     */
    public BigDecimal getaSaleAmt() {
        return aSaleAmt;
    }

    /**
     * 设置营业额本期日均计划
     *
     * @param aSaleAmt 营业额本期日均计划
     */
    public void setaSaleAmt(BigDecimal aSaleAmt) {
        this.aSaleAmt = aSaleAmt;
    }

    /**
     * 获取毛利额本期日均计划
     *
     * @return A_SALE_GROSS - 毛利额本期日均计划
     */
    public BigDecimal getaSaleGross() {
        return aSaleGross;
    }

    /**
     * 设置毛利额本期日均计划
     *
     * @param aSaleGross 毛利额本期日均计划
     */
    public void setaSaleGross(BigDecimal aSaleGross) {
        this.aSaleGross = aSaleGross;
    }

    /**
     * 获取会员卡本期日均计划
     *
     * @return A_MCARD_QTY - 会员卡本期日均计划
     */
    public Integer getaMcardQty() {
        return aMcardQty;
    }

    /**
     * 设置会员卡本期日均计划
     *
     * @param aMcardQty 会员卡本期日均计划
     */
    public void setaMcardQty(Integer aMcardQty) {
        this.aMcardQty = aMcardQty;
    }

    /**
     * 获取创建人账号
     *
     * @return STASK_CRE_ID - 创建人账号
     */
    public String getStaskCreId() {
        return staskCreId;
    }

    /**
     * 设置创建人账号
     *
     * @param staskCreId 创建人账号
     */
    public void setStaskCreId(String staskCreId) {
        this.staskCreId = staskCreId;
    }

    /**
     * 获取创建日期
     *
     * @return STASK_CRE_DATE - 创建日期
     */
    public String getStaskCreDate() {
        return staskCreDate;
    }

    /**
     * 设置创建日期
     *
     * @param staskCreDate 创建日期
     */
    public void setStaskCreDate(String staskCreDate) {
        this.staskCreDate = staskCreDate;
    }

    /**
     * 获取创建时间
     *
     * @return STASK_CRE_TIME - 创建时间
     */
    public String getStaskCreTime() {
        return staskCreTime;
    }

    /**
     * 设置创建时间
     *
     * @param staskCreTime 创建时间
     */
    public void setStaskCreTime(String staskCreTime) {
        this.staskCreTime = staskCreTime;
    }

    /**
     * 获取修改人
     *
     * @return STASK_MD_ID - 修改人
     */
    public String getStaskMdId() {
        return staskMdId;
    }

    /**
     * 设置修改人
     *
     * @param staskMdId 修改人
     */
    public void setStaskMdId(String staskMdId) {
        this.staskMdId = staskMdId;
    }

    /**
     * 获取修改日期
     *
     * @return STASK_MD_DATE - 修改日期
     */
    public String getStaskMdDate() {
        return staskMdDate;
    }

    /**
     * 设置修改日期
     *
     * @param staskMdDate 修改日期
     */
    public void setStaskMdDate(String staskMdDate) {
        this.staskMdDate = staskMdDate;
    }

    /**
     * 获取修改时间
     *
     * @return STASK_MD_TIME - 修改时间
     */
    public String getStaskMdTime() {
        return staskMdTime;
    }

    /**
     * 设置修改时间
     *
     * @param staskMdTime 修改时间
     */
    public void setStaskMdTime(String staskMdTime) {
        this.staskMdTime = staskMdTime;
    }

    /**
     * 获取删除 0 未删除 1已删除
     *
     * @return STASK_TYPE - 删除 0 未删除 1已删除
     */
    public String getStaskType() {
        return staskType;
    }

    /**
     * 设置删除 0 未删除 1已删除
     *
     * @param staskType 删除 0 未删除 1已删除
     */
    public void setStaskType(String staskType) {
        this.staskType = staskType;
    }

    public String getStaskIsShow() {
        return staskIsShow;
    }

    public void setStaskIsShow(String staskIsShow) {
        this.staskIsShow = staskIsShow;
    }

    public String getaSaleAmtSdaily(String turnoverTSP) {
        return aSaleAmtSdaily;
    }

    public void setaSaleAmtSdaily(String aSaleAmtSdaily) {
        this.aSaleAmtSdaily = aSaleAmtSdaily;
    }

    public String getaSaleAmtPdaily(String turnoverOP) {
        return aSaleAmtPdaily;
    }

    public void setaSaleAmtPdaily(String aSaleAmtPdaily) {
        this.aSaleAmtPdaily = aSaleAmtPdaily;
    }

    public String getaSaleGrossSdaily(String grossProfitTSP) {
        return aSaleGrossSdaily;
    }

    public void setaSaleGrossSdaily(String aSaleGrossSdaily) {
        this.aSaleGrossSdaily = aSaleGrossSdaily;
    }

    public String getaSaleGrossPdaily(String grossProfitOP) {
        return aSaleGrossPdaily;
    }

    public void setaSaleGrossPdaily(String aSaleGrossPdaily) {
        this.aSaleGrossPdaily = aSaleGrossPdaily;
    }

    public String getaSaleGrossRateSdaily(String grossMarginTSP) {
        return aSaleGrossRateSdaily;
    }

    public void setaSaleGrossRateSdaily(String aSaleGrossRateSdaily) {
        this.aSaleGrossRateSdaily = aSaleGrossRateSdaily;
    }

    public String getaSaleGrossRatePdaily(String grossMarginOP) {
        return aSaleGrossRatePdaily;
    }

    public void setaSaleGrossRatePdaily(String aSaleGrossRatePdaily) {
        this.aSaleGrossRatePdaily = aSaleGrossRatePdaily;
    }

    public String getaMcardQtySdAily(String s) {
        return aMcardQtySdAily;
    }

    public void setaMcardQtySdAily(String aMcardQtySdAily) {
        this.aMcardQtySdAily = aMcardQtySdAily;
    }

    public String getaMcardQtyPdAily(String s) {
        return aMcardQtyPdAily;
    }

    public void setaMcardQtyPdAily(String aMcardQtyPdAily) {
        this.aMcardQtyPdAily = aMcardQtyPdAily;
    }
}