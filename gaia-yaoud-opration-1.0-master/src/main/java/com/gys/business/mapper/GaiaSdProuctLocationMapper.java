package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdProuctLocation;
import com.gys.business.service.data.GetProductLocationInData;
import com.gys.business.service.data.GetProductLocationOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdProuctLocationMapper extends BaseMapper<GaiaSdProuctLocation> {
    List<GetProductLocationOutData> selectLocationList(GetProductLocationInData inData);

    List<GetProductLocationOutData> selectLocationDrop(@Param("columnName") String columnName, @Param("clientId") String clientId, @Param("brId") String brId);

    List<GaiaSdProuctLocation> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);
}
