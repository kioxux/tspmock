package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**

 * @author xiayuan
 * 电子券信息表
 */
@Data
public class GaiaSdElectronBasic implements Serializable {
    private static final long serialVersionUID = 5834233085415427997L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 电子券活动号
     */
    private String gsebId;
    /**
     * 电子券描述
     */
    private String gsebName;

    /**
     * 面值
     */
    private String gsebAmt;

    /**
     * 是否启用  N为否，Y为是
     */
    private String gsebStatus;

    /**
     * 有效时长  按天
     */
    private String gsebDuration;

    /**
     * 单据类型 0-固定面额 1-非固定面额
     */
    private String gsebType;

    private String gsebTypeName;

}