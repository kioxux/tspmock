package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_PROCESS_DIAGRAM")
public class GaiaProcessDiagram implements Serializable {
    /**
     * id
     */
    @Column(name = "S_ID")
    private String sId;

    /**
     * 流程名称
     */
    @Column(name = "S_FLOW_NAME")
    private String sFlowName;

    /**
     * 流程类型（1：父节点 2：子节点）
     */
    @Column(name = "N_FLOW_TYPE")
    private Integer nFlowType;

    /**
     * 图片
     */
    @Column(name = "S_IMG_URL")
    private String sImgUrl;

    /**
     * 父节点ID
     */
    @Column(name = "S_PAIENT_ID")
    private String sPaientId;

    /**
     * 流程节点路径(暂时不用)
     */
    @Column(name = "S_ROUTE_PATH")
    private String sRoutePath;

    /**
     * 类型（1.物流导航页 2.营运导航页 3.商采导航页 4.财务导航页）
     */
    @Column(name = "N_TYPE")
    private Integer nType;

    /**
     * 排序
     */
    @Column(name = "N_SORT")
    private Integer nSort;

    /**
     * 能打开的方式(1:WEB  2:APP)
     */
    @Column(name = "S_CAN_OPEN")
    private String sCanOpen;

    /**
     * 1:启用  2：停用
     */
    @Column(name = "N_STATUS")
    private Integer nStatus;

    /**
     * 创建时间
     */
    @Column(name = "D_CREATE")
    private Date dCreate;

    /**
     * 权限组(岗位)编号(暂时不用)
     */
    @Column(name = "S_AUTH_GROUP_ID")
    private String sAuthGroupId;

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return S_ID - id
     */
    public String getsId() {
        return sId;
    }

    /**
     * 设置id
     *
     * @param sId id
     */
    public void setsId(String sId) {
        this.sId = sId;
    }

    /**
     * 获取流程名称
     *
     * @return S_FLOW_NAME - 流程名称
     */
    public String getsFlowName() {
        return sFlowName;
    }

    /**
     * 设置流程名称
     *
     * @param sFlowName 流程名称
     */
    public void setsFlowName(String sFlowName) {
        this.sFlowName = sFlowName;
    }

    /**
     * 获取流程类型（1：父节点 2：子节点）
     *
     * @return N_FLOW_TYPE - 流程类型（1：父节点 2：子节点）
     */
    public Integer getnFlowType() {
        return nFlowType;
    }

    /**
     * 设置流程类型（1：父节点 2：子节点）
     *
     * @param nFlowType 流程类型（1：父节点 2：子节点）
     */
    public void setnFlowType(Integer nFlowType) {
        this.nFlowType = nFlowType;
    }

    /**
     * 获取图片
     *
     * @return S_IMG_URL - 图片
     */
    public String getsImgUrl() {
        return sImgUrl;
    }

    /**
     * 设置图片
     *
     * @param sImgUrl 图片
     */
    public void setsImgUrl(String sImgUrl) {
        this.sImgUrl = sImgUrl;
    }

    /**
     * 获取父节点ID
     *
     * @return S_PAIENT_ID - 父节点ID
     */
    public String getsPaientId() {
        return sPaientId;
    }

    /**
     * 设置父节点ID
     *
     * @param sPaientId 父节点ID
     */
    public void setsPaientId(String sPaientId) {
        this.sPaientId = sPaientId;
    }

    /**
     * 获取流程节点路径(暂时不用)
     *
     * @return S_ROUTE_PATH - 流程节点路径(暂时不用)
     */
    public String getsRoutePath() {
        return sRoutePath;
    }

    /**
     * 设置流程节点路径(暂时不用)
     *
     * @param sRoutePath 流程节点路径(暂时不用)
     */
    public void setsRoutePath(String sRoutePath) {
        this.sRoutePath = sRoutePath;
    }

    /**
     * 获取类型（1.物流导航页 2.营运导航页 3.商采导航页 4.财务导航页）
     *
     * @return N_TYPE - 类型（1.物流导航页 2.营运导航页 3.商采导航页 4.财务导航页）
     */
    public Integer getnType() {
        return nType;
    }

    /**
     * 设置类型（1.物流导航页 2.营运导航页 3.商采导航页 4.财务导航页）
     *
     * @param nType 类型（1.物流导航页 2.营运导航页 3.商采导航页 4.财务导航页）
     */
    public void setnType(Integer nType) {
        this.nType = nType;
    }

    /**
     * 获取排序
     *
     * @return N_SORT - 排序
     */
    public Integer getnSort() {
        return nSort;
    }

    /**
     * 设置排序
     *
     * @param nSort 排序
     */
    public void setnSort(Integer nSort) {
        this.nSort = nSort;
    }

    /**
     * 获取能打开的方式(1:WEB  2:APP)
     *
     * @return S_CAN_OPEN - 能打开的方式(1:WEB  2:APP)
     */
    public String getsCanOpen() {
        return sCanOpen;
    }

    /**
     * 设置能打开的方式(1:WEB  2:APP)
     *
     * @param sCanOpen 能打开的方式(1:WEB  2:APP)
     */
    public void setsCanOpen(String sCanOpen) {
        this.sCanOpen = sCanOpen;
    }

    /**
     * 获取1:启用  2：停用
     *
     * @return N_STATUS - 1:启用  2：停用
     */
    public Integer getnStatus() {
        return nStatus;
    }

    /**
     * 设置1:启用  2：停用
     *
     * @param nStatus 1:启用  2：停用
     */
    public void setnStatus(Integer nStatus) {
        this.nStatus = nStatus;
    }

    /**
     * 获取创建时间
     *
     * @return D_CREATE - 创建时间
     */
    public Date getdCreate() {
        return dCreate;
    }

    /**
     * 设置创建时间
     *
     * @param dCreate 创建时间
     */
    public void setdCreate(Date dCreate) {
        this.dCreate = dCreate;
    }

    /**
     * 获取权限组(岗位)编号(暂时不用)
     *
     * @return S_AUTH_GROUP_ID - 权限组(岗位)编号(暂时不用)
     */
    public String getsAuthGroupId() {
        return sAuthGroupId;
    }

    /**
     * 设置权限组(岗位)编号(暂时不用)
     *
     * @param sAuthGroupId 权限组(岗位)编号(暂时不用)
     */
    public void setsAuthGroupId(String sAuthGroupId) {
        this.sAuthGroupId = sAuthGroupId;
    }
}