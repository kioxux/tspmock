package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value="com-gys-business-mapper-entity-GaiaSdNewStoreDistributionRecordD")
public class GaiaSdNewStoreDistributionRecordD {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 门店编码
    */
    @ApiModelProperty(value="门店编码")
    private String storeId;

    /**
    * 创建日期
    */
    @ApiModelProperty(value="创建日期")
    private String createDate;

    /**
    * 单号
    */
    @ApiModelProperty(value="单号")
    private String orderId;

    /**
    * 商品自编码
    */
    @ApiModelProperty(value="商品自编码")
    private String proSelfCode;

    /**
    * 门店属性
    */
    @ApiModelProperty(value="门店属性")
    private String storeAttribute;

    /**
    * 店效级别
    */
    @ApiModelProperty(value="店效级别")
    private String storeLevel;

    /**
    * 参考成本价
    */
    @ApiModelProperty(value="参考成本价")
    private BigDecimal referenceCostAmt;

    /**
    * 中包装
    */
    @ApiModelProperty(value="中包装")
    private BigDecimal midPackage;

    /**
    * 公司店均月销量
    */
    @ApiModelProperty(value="公司店均月销量")
    private BigDecimal companyAvgSaleQty;

    /**
    * 门店店均月销量
    */
    @ApiModelProperty(value="门店店均月销量")
    private BigDecimal storeAvgSaleQty;

    /**
    * 仓库量
    */
    @ApiModelProperty(value="仓库量")
    private BigDecimal wmsNumber;

    /**
    * 本店库存
    */
    @ApiModelProperty(value="本店库存")
    private BigDecimal storeStockNumber;

    /**
    * 建议铺货量
    */
    @ApiModelProperty(value="建议铺货量")
    private BigDecimal adviseNumber;

    /**
    * 创建人
    */
    @ApiModelProperty(value="创建人")
    private String createUser;

    /**
    * 创建时间
    */
    @ApiModelProperty(value="创建时间")
    private Date createTime;
}