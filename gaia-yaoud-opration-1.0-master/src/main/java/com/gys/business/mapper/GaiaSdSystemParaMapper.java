package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdEnv;
import com.gys.business.mapper.entity.GaiaSdSystemPara;
import com.gys.business.service.data.UpdateParaByStoreListInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdSystemParaMapper extends BaseMapper<GaiaSdSystemPara> {

    List<GaiaSdSystemPara> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId, @Param("paraId") String paraId);

    GaiaSdSystemPara getAuthByClientAndBrId(@Param("client") String client, @Param("depId") String depId, @Param("gsspId") String gsspId);

    int batchReplaceInsert(@Param("list")List<GaiaSdSystemPara> list);

    int updateParaByStoreList(UpdateParaByStoreListInData inData);

    String getParaByClientAndStoCode(@Param("client") String client, @Param("stoCode") String stoCode, @Param("gsspId") String gsspId);

    GaiaSdEnv getEnv(@Param("client") String client,@Param("stoCode") String stoCode);
}
