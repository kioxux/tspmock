//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_REPLENISH_PARA"
)
public class GaiaSdReplenishPara implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSEP_BR_ID"
    )
    private String gsepBrId;
    @Id
    @Column(
            name = "GSEP_ID"
    )
    private String gsepId;
    @Column(
            name = "GSEP_CHNMED_FLAG"
    )
    private String gsepChnmedFlag;
    @Column(
            name = "GSEP_SALES_MONTH"
    )
    private String gsepSalesMonth;
    @Column(
            name = "GSEP_PARA1"
    )
    private BigDecimal gsepPara1;
    @Column(
            name = "GSEP_PARA2"
    )
    private BigDecimal gsepPara2;
    @Column(
            name = "GSEP_PARA3"
    )
    private BigDecimal gsepPara3;
    private static final long serialVersionUID = 1L;

    public GaiaSdReplenishPara() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsepBrId() {
        return this.gsepBrId;
    }

    public void setGsepBrId(String gsepBrId) {
        this.gsepBrId = gsepBrId;
    }

    public String getGsepId() {
        return this.gsepId;
    }

    public void setGsepId(String gsepId) {
        this.gsepId = gsepId;
    }

    public String getGsepChnmedFlag() {
        return this.gsepChnmedFlag;
    }

    public void setGsepChnmedFlag(String gsepChnmedFlag) {
        this.gsepChnmedFlag = gsepChnmedFlag;
    }

    public String getGsepSalesMonth() {
        return this.gsepSalesMonth;
    }

    public void setGsepSalesMonth(String gsepSalesMonth) {
        this.gsepSalesMonth = gsepSalesMonth;
    }

    public BigDecimal getGsepPara1() {
        return this.gsepPara1;
    }

    public void setGsepPara1(BigDecimal gsepPara1) {
        this.gsepPara1 = gsepPara1;
    }

    public BigDecimal getGsepPara2() {
        return this.gsepPara2;
    }

    public void setGsepPara2(BigDecimal gsepPara2) {
        this.gsepPara2 = gsepPara2;
    }

    public BigDecimal getGsepPara3() {
        return this.gsepPara3;
    }

    public void setGsepPara3(BigDecimal gsepPara3) {
        this.gsepPara3 = gsepPara3;
    }
}
