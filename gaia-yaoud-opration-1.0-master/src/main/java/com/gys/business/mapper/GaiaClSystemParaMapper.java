package com.gys.business.mapper;

import com.gys.business.service.data.GaiaClSystemPara;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaClSystemParaMapper {
    int deleteByPrimaryKey(@Param("client") String client, @Param("gcspId") String gcspId);

    int insert(GaiaClSystemPara record);

    int insertSelective(GaiaClSystemPara record);

    GaiaClSystemPara selectByPrimaryKey(@Param("client") String client, @Param("gcspId") String gcspId);

    int updateByPrimaryKeySelective(GaiaClSystemPara record);

    int updateByPrimaryKey(GaiaClSystemPara record);

    int batchInsert(@Param("list") List<GaiaClSystemPara> list);

    GaiaClSystemPara getAuthByClient(@Param("client")String client, @Param("gcspId")String gcspId);

    List<GaiaClSystemPara> getAuthByParaId(@Param("client")String client, @Param("gcspId")String gcspId);

    List<GaiaClSystemPara> getAllByClientAndBrId(@Param("client") String client, @Param("paraId") String paraId);

    GaiaClSystemPara getParaByClientAndParaId(@Param("client") String client, @Param("gcspId") String gcspId);

    List<GaiaClSystemPara> getParaByGcspIdAndParaId(@Param("gcspId") String gcspId, @Param("gcspPara1") String gcspPara1);
}