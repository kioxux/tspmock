package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPreordD;
import com.gys.business.service.data.OrderInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPreordDMapper extends BaseMapper<GaiaSdPreordD> {

    /**
     * 详情
     * @param inData
     * @return
     */
    List<GaiaSdPreordD> getOrderDetailsList(OrderInData inData);

    /**
     * 批量插入
     * @param preordDS
     */
    void insertLists(@Param("list")List<GaiaSdPreordD> preordDS);
}
