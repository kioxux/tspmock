package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWmsTkys;
import com.gys.business.service.data.DtYb.ReturnExamime.ReturnExamimeVO;
import com.gys.business.service.data.YbInterfaceQueryData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaWmsTkysMapper extends BaseMapper<GaiaWmsTkys> {

    List<ReturnExamimeVO> queryTkysByYLZ(YbInterfaceQueryData inData);
}
