package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 公司级-新品评估-主表(GaiaEntNewProductEvaluationH)实体类
 *
 * @author makejava
 * @since 2021-07-19 10:16:54
 */
public class GaiaEntNewProductEvaluationH implements Serializable {
    private static final long serialVersionUID = 795425163103683770L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店ID
     */
    private String storeId;
    /**
     * 商品评估单号
     */
    private String billCode;
    /**
     * 单据日期
     */
    private Date billDate;
    /**
     * 单据状态：0-待确认 1-已确认
     */
    private Integer status;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 完成日期
     */
    private Date finishTime;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;
    /**
     * 推送范围
     */
    private Integer sendRange;

    private Integer readFlag;

    public Integer getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(Integer readFlag) {
        this.readFlag = readFlag;
    }

    public Integer getSendRange() {
        return sendRange;
    }

    public void setSendRange(Integer sendRange) {
        this.sendRange = sendRange;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}
