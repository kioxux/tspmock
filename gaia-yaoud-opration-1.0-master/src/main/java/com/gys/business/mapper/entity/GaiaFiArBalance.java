package com.gys.business.mapper.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

/**
 * 应收期末余额(GaiaFiArBalance)实体类
 *
 * @author makejava
 * @since 2021-09-18 15:29:14
 */
public class GaiaFiArBalance implements Serializable {
    private static final long serialVersionUID = 629112724085968823L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 仓库编码
     */
    private String dcCode;
    /**
     * 余额
     */
    private BigDecimal residualAmt;
    /**
     * 客户自编码
     */
    private String cusSelfCode;
    /**
     * 客户编码
     */
    private String cusCode;
    /**
     * 客户名称
     */
    private String cusName;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDcCode() {
        return dcCode;
    }

    public void setDcCode(String dcCode) {
        this.dcCode = dcCode;
    }

    public BigDecimal getResidualAmt() {
        return residualAmt;
    }

    public void setResidualAmt(BigDecimal residualAmt) {
        this.residualAmt = residualAmt;
    }

    public String getCusSelfCode() {
        return cusSelfCode;
    }

    public void setCusSelfCode(String cusSelfCode) {
        this.cusSelfCode = cusSelfCode;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}
