package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralExchangeSet;
import com.gys.business.service.data.GetPointExchangeOutData;
import com.gys.business.service.data.GetQueryProductInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdIntegralExchangeSetMapper extends BaseMapper<GaiaSdIntegralExchangeSet> {
    GetPointExchangeOutData pointExchange(GetQueryProductInData inData);

    List<GaiaSdIntegralExchangeSet> getAllByClient(@Param("client") String client);
}
