package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaAnnualTrainingPlan;
import com.gys.business.service.data.AnnualTrainingPlanInData;
import com.gys.business.service.data.AnnualTrainingPlanOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaAnnualTrainingPlanMapper extends BaseMapper<GaiaAnnualTrainingPlan> {
    String getNextGatpVoucherId(@Param("client") String client);

    Long getIdByClientAndVoucherId(@Param("client") String client, @Param("voucherId") String voucherId);

    void insertAnnualTrainingPlan(GaiaAnnualTrainingPlan plan);

    void updateAnnualTrainingPlan(GaiaAnnualTrainingPlan plan);

    List<AnnualTrainingPlanOutData> getAnnualTrainingPlanList(AnnualTrainingPlanInData inData);

    void deletePlanById(@Param("id") Long id);
}
