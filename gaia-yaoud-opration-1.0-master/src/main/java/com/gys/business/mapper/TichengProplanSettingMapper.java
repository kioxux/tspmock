package com.gys.business.mapper;

import com.gys.business.mapper.entity.TichengProplanBasic;
import com.gys.business.mapper.entity.TichengProplanSetting;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-11-15
 */
@Mapper
public interface TichengProplanSettingMapper extends BaseMapper<TichengProplanSetting> {

    void deleteProPlanSetting(@Param("id") Long planId);

    void deleteProPlanSettingBySettingId(@Param("id") Long settingId);

    /**
     * run only once,only used to synchronize history data
     *
     * @return List<TichengProplanBasic>
     */
    List<TichengProplanSetting> selectSyncData();

}
