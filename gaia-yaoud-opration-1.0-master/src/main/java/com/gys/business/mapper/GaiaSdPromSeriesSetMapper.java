package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromSeriesSet;
import com.gys.business.service.data.PromSeriesSetOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromSeriesSetMapper extends BaseMapper<GaiaSdPromSeriesSet> {
    List<PromSeriesSetOutData> getDetail(PromoteInData inData);

    void insertToList(List<GaiaSdPromSeriesSet> setList);

    List<GaiaSdPromSeriesSet> getAllByClient(@Param("client") String client, @Param("nowDate") String nowDate,@Param("brId")String brId);
}
