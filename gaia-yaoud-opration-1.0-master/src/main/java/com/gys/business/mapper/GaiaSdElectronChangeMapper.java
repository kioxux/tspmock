package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdElectronBusinessSet;
import com.gys.business.mapper.entity.GaiaSdElectronChange;
import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import com.gys.business.mapper.entity.GaiaSdStoreData;
import com.gys.business.service.data.ElectronDetailOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdElectronChangeMapper extends BaseMapper<GaiaSdElectronChange> {

    List<ElectronDetailOutData> queryCanUseElectron(GaiaSdMemberCardData inData);

    void updateStatus(List<GaiaSdElectronChange> inData);

    List<ElectronDetailOutData> selectGiftCoupons(GaiaSdStoreData inData);

    void batchInsert(List<GaiaSdElectronChange> inData);

    /**
     * 主题表数量自增
     *
     * @param inData
     */
    void themeSendQtyIncr(List<GaiaSdElectronChange> inData);

    /**
     * 业务表数量自增
     *
     * @param inData
     */
    void bussinessSendQtyIncr(List<GaiaSdElectronChange> inData);

    List<ElectronDetailOutData> selectGiftCouponsAndUse(GaiaSdStoreData inData);

    List<GaiaSdElectronChange> getGiveAndUseAll(@Param("client") String client, @Param("billNo") String billNo);

    List<GaiaSdElectronChange> getListByMemberCardId(@Param("client") String client, @Param("oldMemberCardId") String oldMemberCardId);

    void updateByMemberCardId(@Param("client") String client, @Param("oldMemberCardId") String oldMemberCardId, @Param("newMemberCardId") String newMemberCardId);

    List<GaiaSdElectronChange> listElectronChange(@Param("client") String client,@Param("billNo") String billNo);

    List<GaiaSdElectronBusinessSet> queryElectionByDate(@Param("client") String client,@Param("brId")  String depId);

}