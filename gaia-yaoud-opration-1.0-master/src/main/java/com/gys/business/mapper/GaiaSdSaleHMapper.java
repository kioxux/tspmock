package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSaleH;
import com.gys.business.service.data.*;
import com.gys.business.service.data.DailySettlement.DailyReconcileDetailAuthPay;
import com.gys.business.service.data.takeaway.FxSaleDto;
import com.gys.business.service.data.takeaway.FxSaleQueryBean;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.app.salereport.ProPositionDTO;
import com.gys.common.data.app.salereport.ProPositionSummaryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdSaleHMapper extends BaseMapper<GaiaSdSaleH> {
    String selectNextVoucherId(@Param("clientId") String clientId);

    List<ShiftSalesOutData> getShiftSalesList(shiftSalesInData inData);

    List<SalesInquireOutData> getSalesInquireList(SalesInquireInData inData);

    void updateSalesInquire(@Param("client") String client, @Param("brId") String depId, @Param("list") List<String> billNo);

    List<ZydjSaleH> queryZyDjSaleH(QueryParam inData);

    List<GetSaleReturnToData> detail(SalesInquireData inData);

    GaiaSdSaleH selectByBillNo(@Param("billNo") String billNo, @Param("clientId") String clientId, @Param("brId") String brId);

    ReturnCacheData cache(SalesInquireData inData);

    void update(GaiaSdSaleH sd);

    /**
     * 表单验证
     *
     * @param billNo
     * @param clientId
     * @param brId
     * @return
     */
    GaiaSdSaleH getSaleHByReturnBillNo(@Param("billNo") String billNo, @Param("client") String clientId, @Param("brId") String brId);

    /**
     * app销售查询
     *
     * @param inData
     * @return
     */
    List<AppParameterDto> getAllListByApp(AppParameterVo inData);

    void updateByAppPrimaryKey(GaiaSdSaleH sd);

    List<RebornData> getAllListByAppBillNo(AppParameterVo inData);

    String getMaxByBrId(@Param("client") String clientId, @Param("brId") String brId, @Param("nowDate") String nowDate);

    List<AppParameterDetailsDto> getAppByDetails(AppParameterVo inData);

    /**
     * 获取挂单信息
     *
     * @param map
     * @return
     */
    List<RestOrderInfoOutData> getAllListByGsshHideFlag(Map<String, String> map);

    List<GaiaSdSaleH> getAllListByGsshHideFlag2(GaiaSdSaleH inData);

    /**
     * 查询会员最近的商品
     *
     * @param client
     * @param gsmbcBrId
     * @param gsmbcCardId
     * @return
     */
    List<String> getRecentMedicine(@Param("client") String client, @Param("gsmbcBrId") String gsmbcBrId, @Param("gsmbcCardId") String gsmbcCardId);

    /**
     * 根据 加盟商 门店 商品 会员 当前 时间查询 当前商品购买的数量
     *
     * @param beginDate
     * @param endDate
     * @param client
     * @param proId
     * @param cardNum
     * @param nowDate
     * @return
     */
    PurchaseLimitDto getPurchaseQuantityByCardNum(@Param("client") String client, @Param("proId") String proId, @Param("cardNum") String cardNum, @Param("nowDate") String nowDate, @Param("beginDate") String beginDate, @Param("endDate") String endDate);

    List<ProductBigTypeSaleOutData> getProductBigTypeSaleList(ProductBigTypeSaleInData inData);

    SaleChartOutData getMovInfo(ProductBigTypeSaleInData inData);

    ProductBigTypeSaleOutData getDrugSubTotalInfo(ProductBigTypeSaleInData inData);

    List<SaleChartInfoOutData> getChartInfoByBigCode(ProductBigTypeSaleInData inData);

    List<ProductBigTypeSaleOutData> getProductMidTypeSaleList(ProductBigTypeSaleInData inData);

    /**
     * 获取指定加盟商下 指定商品类型所有销售数据（实时数据）
     *
     * @param client
     * @param bigCode
     * @return
     */
    List<SaleChartInfoOutData> getSaleDataRecordForCurrent(@Param("client") String client, @Param("bigCode") String bigCode, @Param("monthFlag") String monthFlag);

    List<String> getAllClientInfo();

    List<AppStoreSaleOutData> getStoreSaleList(AppStoreSaleInData inData);

    /**
     * 根据日期 和 门店 查询 销售编码
     *
     * @param map
     * @return
     */
    List<String> getAllByBillAndDate(Map<String, Object> map);

    void updateList(@Param("client") String client, @Param("brId") String depId, @Param("list") List<String> billNo);

    List<FxSaleDto> getSaleByOrderId(FxSaleQueryBean bean);

    List<String> verifyWhetherToReview(Map<String, Object> map);

    List<RestOrderDetailDto> getPrintDetails(Map<String, String> map);

    DailyReconcileDetailAuthPay getAuthPay(@Param("client") String client, @Param("brId") String depId, @Param("list") List<String> billNo);

    List<GaiaSdSaleH> getBillOrReturnBill(Map<String, String> inData);

    /**
     * 商品定位统计
     *
     * @param proPositionDTO proPositionDTO
     * @return List<ProPositionSummaryVO>
     */
    List<ProPositionSummaryVO> selectStorePositionSummary(ProPositionDTO proPositionDTO);

    /**
     * 商品定位统计上周
     *
     * @param proPositionDTO proPositionDTO
     * @return List<ProPositionSummaryVO>
     */
    List<ProPositionSummaryVO> selectStorePositionSummaryWithWeek(ProPositionDTO proPositionDTO);

    /**
     * 商品定位统计上周数量
     *
     * @param proPositionDTO proPositionDTO
     * @return List<ProPositionSummaryVO>
     */
    List<ProPositionSummaryVO> selectStorePositionSummaryProCountWithWeek(ProPositionDTO proPositionDTO);

    /**
     * 商品定位统计上月
     *
     * @param proPositionDTO proPositionDTO
     * @return List<ProPositionSummaryVO>
     */
    List<ProPositionSummaryVO> selectStorePositionSummaryWithMonth(ProPositionDTO proPositionDTO);

}
