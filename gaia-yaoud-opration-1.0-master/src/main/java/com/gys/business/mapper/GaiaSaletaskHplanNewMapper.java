package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSaletaskHplanNew;
import com.gys.business.mapper.entity.GaiaSaletaskPlanClientStoNew;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GaiaSaletaskHplanNewMapper extends BaseMapper<GaiaSaletaskHplanNew> {

    List<MonthOutData> selectPage(MonthOutData outData);
    //获取任务id
    String selectNextVoucherId(@Param("clientId") String clientId);
    String selectNextVoucherIdS(@Param("client") String clientId);

    //新增保存
    void insertMonthlySalesPlan(List<GaiaSaletaskHplanNew> gaiaSaletaskHplanNew);//添加

    //根据月份查询任务名字加盟商
    int staskTaskNameByName(@Param("staskTaskName") String staskTaskName ,@Param("monthPlan") String monthPlan,@Param("client") String client);

    void modifyMonthlySalesPlan(GaiaSaletaskHplanNew gaiaSaletaskHplanNew);

    void deleteMonthlySalesPlan(MonthOutData outData);

    List<MonthOutData> getStoreBySaleTask(GaiaStoreData storeData);

    default List<MonthOutData> listStoreSales(SalesSummaryData summaryData) {
        return null;
    }

//    List<MonthOutData> monthlySalesUser(List<GaiaSaletaskHplanNew> gaiaSaletaskHplanNew);

    List<MonthOutData> monthlySalesTaskListUser(MonthOutData outData);

    //查看月销售任务
    //返回月销售任务对象
    GaiaSaletaskHplanNew selectViewSalesTask(@Param("staskTaskId") String staskTaskId ,@Param("client") String clientId);
    //返回列表
    List<MonthOutDataVo> selectMonthOutDataVo(MonthOutData outData);
    //导入门店数据
//    MonthOutDataVo selectStoreDataList(@Param("brId") String brId,@Param("client") String clientId,@Param("monthPlan") String monthPlan);

    //审核
    void audit(MonthOutData outData);
    AuditData selectAudit(MonthOutData outData);

    //停用
    void stop(MonthOutData outData);

    //忽略
    void lose(MonthOutData outData);

    //目标试算
    List<MonthOutDataVo> targetTrial(GaiaSaletaskHplanNew gaiaSaletaskHplanNew);

    //删除行
    List<GaiaStoreData> stoCodeList(@Param("client") String clientId);
    void deleteRow(MonthOutData outData);

    //设置
    MonthOutDataVo settingStore(MonthOutDataVo inData);

    //查看
    List<MonthOutDataVo> ViewSalesTaskPlan(MonthOutData outData);

    void deletePlanSto(@Param("staskTaskId") String staskTaskId ,@Param("client") String clientId);
}