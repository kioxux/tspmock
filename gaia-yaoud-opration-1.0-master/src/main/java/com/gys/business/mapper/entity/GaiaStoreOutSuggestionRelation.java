package com.gys.business.mapper.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 门店调出建议关系表(GaiaStoreOutSuggestionRelation)实体类
 *
 * @author makejava
 * @since 2021-10-28 10:54:23
 */
public class GaiaStoreOutSuggestionRelation implements Serializable {
    private static final long serialVersionUID = 504188157994966306L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 商品调出单号
     */
    private String billCode;
    /**
     * 调出门店
     */
    private String outStoCode;
    /**
     * 调入门店
     */
    private String inStoCode;
    /**
     * 调入店名
     */
    private String inStoName;
    /**
     * 序号
     */
    private Integer sort;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getOutStoCode() {
        return outStoCode;
    }

    public void setOutStoCode(String outStoCode) {
        this.outStoCode = outStoCode;
    }

    public String getInStoCode() {
        return inStoCode;
    }

    public void setInStoCode(String inStoCode) {
        this.inStoCode = inStoCode;
    }

    public String getInStoName() {
        return inStoName;
    }

    public void setInStoName(String inStoName) {
        this.inStoName = inStoName;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}

