package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 门店积分换购商品设置时间段
 * @TableName GAIA_SD_INTEGRAL_EXCHANGE_SET_TIME
 */
@Data
public class GaiaSdIntegralExchangeSetTime implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 截止时间
     */
    private String endTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建者
     */
    private String createUser;
}