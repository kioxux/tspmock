package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdElectronConditionSet;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdElectronConditionSetMapper {

    int insertSelective(GaiaSdElectronConditionSet record);

    int updateByPrimaryKeySelective(@Param("record") GaiaSdElectronConditionSet record);

    int updateByPrimaryKey(GaiaSdElectronConditionSet record);

    int batchInsert(@Param("list") List<GaiaSdElectronConditionSet> list);

    List<GaiaSdElectronConditionSet> getAll(@Param("client") String client, @Param("gsetsId") String gsetsId);

    List<GaiaSdElectronConditionSet> getGaiaSdElectronConditionSet(@Param("client") String client, @Param("gsetsId") String gsetsId, @Param("gsebsId") String gsebsId);

    List<GaiaSdElectronConditionSet> getAllByClientAndGsetsId(@Param("client") String client, @Param("gsetsId") String gsetsId);

    void batchUpdate(@Param("list") List<GaiaSdElectronConditionSet> conditionSets);

    List<GaiaSdElectronConditionSet> getElectronConditionByGsetsIdAndGroupIds(@Param("client") String client, @Param("gsetsId")String gsetsId,  @Param("list") List<String> couponGsebsId);
}