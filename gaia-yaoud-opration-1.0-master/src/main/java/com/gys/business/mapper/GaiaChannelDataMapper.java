package com.gys.business.mapper;

import com.gys.business.service.data.GaiaChannelData;
import java.util.List;

import com.gys.business.service.data.GaiaChannelDataOutData;
import com.gys.business.service.data.GaiaChannelProductInData;
import com.gys.business.service.data.GaiaChannelProductOutData;
import org.apache.ibatis.annotations.Param;

public interface GaiaChannelDataMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaChannelData record);

    int insertSelective(GaiaChannelData record);

    GaiaChannelData selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaChannelData record);

    int updateByPrimaryKey(GaiaChannelData record);

    int batchInsert(@Param("list") List<GaiaChannelData> list);

    List<GaiaChannelDataOutData> listGaiaChannelData(GaiaChannelData inData);

    int checkExsitData(GaiaChannelData inData);

    List<GaiaChannelProductOutData> listWaiteInsertData(GaiaChannelProductInData inData);

    GaiaChannelData selectChannelCode(GaiaChannelData inData);

    int checkProduct(GaiaChannelProductOutData gaiaChannelProductOutData);
}