package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSoBatchH;
import com.gys.business.service.data.yaoshibang.CustomerDto;
import com.gys.business.service.data.yaoshibang.OrderDetailDto;
import com.gys.business.service.data.yaoshibang.OrderQueryBean;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSoBatchHMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSoBatchH record);

    int insertSelective(GaiaSoBatchH record);

    GaiaSoBatchH selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSoBatchH record);

    int updateByPrimaryKey(GaiaSoBatchH record);

    void insertList(List<GaiaSoBatchH> list);

    void updateStatus(@Param("list") List<Long> list, @Param("status") String status);

    List<GaiaSoBatchH> orderQuery(OrderQueryBean bean);

    List<OrderDetailDto> orderDetail(@Param("id") Long id, @Param("client") String client, @Param("depId") String depId);

    List<CustomerDto> customerQuery(@Param("name") String name, @Param("client") String client);

    List<GaiaSoBatchH> findAll();

    void updateCustomerId(@Param("id") Long id, @Param("customerId") String customerId);

    void updateBatchOrderId(@Param("id") Long id, @Param("batchOrderId") String batchOrderId);

    void updateStatusByClientAndCustomerName(@Param("client") String client, @Param("customerName") String customerName, @Param("status") String status);

    void updateCustomerIdClientAndCustomerName(@Param("client") String client, @Param("customerName") String customerName, @Param("customerId") String customerId);

    @MapKey("soDrugstorename")
    Map<String, GaiaSoBatchH> selectAllCustomerId(@Param("client") String client);

    List<String> queryOrderIdExist(@Param("client") String client, @Param("orderIdQueryList") List<String> orderIdQueryList);
}