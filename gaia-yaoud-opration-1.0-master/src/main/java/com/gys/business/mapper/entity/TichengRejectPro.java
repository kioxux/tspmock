package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 销售提成方案剔除单品表
 * </p>
 *
 * @author flynn
 * @since 2021-11-12
 */
@Data
@Table( name = "GAIA_TICHENG_REJECT_PRO")
public class TichengRejectPro implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键ID")
    @Id
    @Column(name = "ID")
    private Long id;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "提成方案主表主键ID")
    @Column(name = "PID")
    private Long pid;

    @ApiModelProperty(value = "商品编码")
    @Column(name = "PRO_CODE")
    private String proCode;

    @ApiModelProperty(value = "商品名")
    @Column(name = "PRO_NAME")
    private String proName;

    @ApiModelProperty(value = "规格")
    @Column(name = "PRO_SPECS")
    private String proSpecs;

    @ApiModelProperty(value = "生产企业")
    @Column(name = "PRO_FACTORY_NAME")
    private String proFactoryName;

    @ApiModelProperty(value = "成本价")
    @Column(name = "PRO_COST_PRICE")
    private BigDecimal proCostPrice;

    @ApiModelProperty(value = "零售价")
    @Column(name = "PRO_PRICE_NORMAL")
    private BigDecimal proPriceNormal;

    @ApiModelProperty(value = "操作状态 0 未删除 1 已删除")
    @Column(name = "DELETE_FLAG")
    private String deleteFlag;

    @Column(name = "LAST_UPDATE_TIME")
    private LocalDateTime lastUpdateTime;


}
