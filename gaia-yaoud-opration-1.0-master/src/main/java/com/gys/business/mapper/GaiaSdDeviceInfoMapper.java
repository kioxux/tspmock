//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdDeviceInfo;
import com.gys.business.service.OrderService;
import com.gys.business.service.data.DeviceInfoInData;
import com.gys.business.service.data.DeviceInfoOutData;
import com.gys.business.service.data.device.DeviceForm;
import com.gys.business.service.data.device.UserVO;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdDeviceInfoMapper extends BaseMapper<GaiaSdDeviceInfo> {
    List<DeviceInfoOutData> getDeviceInfoList(DeviceInfoInData inData);

    String selectNextGsdiId();

    void update(GaiaSdDeviceInfo deviceInfo);

    List<DeviceInfoOutData> findList(DeviceForm deviceForm);

    List<UserVO> getUserNameList(DeviceForm deviceForm);

}
