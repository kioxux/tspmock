package com.gys.business.mapper.entity;

import com.gys.util.ysb.YsbDrugInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * GAIA_SO_BATCH_D
 * @author 
 */
@Data
@NoArgsConstructor
public class GaiaSoBatchD {
    /**
     * 主键
     */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
     * 加盟商
     */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
     * 订单编码
     */
    @ApiModelProperty(value="订单编码")
    private String soOrderid;

    /**
     * 药师帮商品编号
     */
    @ApiModelProperty(value="药师帮商品编号")
    private String soDrugid;

    /**
     * 商品编码
     */
    @ApiModelProperty(value="商品编码")
    private String soDrugcode;

    /**
     * 商品名称
     */
    @ApiModelProperty(value="商品名称")
    private String soDrugname;

    /**
     * 单价
     */
    @ApiModelProperty(value="单价")
    private BigDecimal soPrice;

    /**
     * 折前原始价
     */
    @ApiModelProperty(value="折前原始价")
    private String soOriginalprice;

    /**
     * 发票类型
     */
    @ApiModelProperty(value="发票类型")
    private String soDruginvoicetype;

    /**
     * 数量
     */
    @ApiModelProperty(value="数量")
    private BigDecimal soNewqty;

    /**
     * 总金额
     */
    @ApiModelProperty(value="总金额")
    private String soNewamount;

    /**
     * 不同药品类别
     */
    @ApiModelProperty(value="不同药品类别")
    private String soDeliveralone;

    /**
     * 订单活动类型
     */
    @ApiModelProperty(value="订单活动类型")
    private String soDelivertype;

    /**
     * 药品序号
     */
    @ApiModelProperty(value="药品序号")
    private String soSerialno;

    /**
     * 药品备注
     */
    @ApiModelProperty(value="药品备注")
    private String soDrugremark;

    /**
     * 创建日期
     */
    @ApiModelProperty(value="创建日期")
    private String soCreDate;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private String soCreTime;

    /**
     * 创建人账号
     */
    @ApiModelProperty(value="创建人账号")
    private String soCreId;

    /**
     * 修改日期
     */
    @ApiModelProperty(value="修改日期")
    private String soModiDate;

    /**
     * 修改时间
     */
    @ApiModelProperty(value="修改时间")
    private String soModiTime;

    /**
     * 修改人账号
     */
    @ApiModelProperty(value="修改人账号")
    private String soModiId;

    public GaiaSoBatchD(YsbDrugInfo info) {
        this.soDrugid = info.getDrugId();
        this.soDrugcode = info.getDrugCode();
        this.soDrugname = info.getDrugName();
        this.soPrice = new BigDecimal(info.getPrice());
        this.soDruginvoicetype = info.getDrugInvoiceType();
        this.soOriginalprice = info.getOriginalPrice();
        this.soNewqty = new BigDecimal(info.getNewQty());
        this.soNewamount = info.getNewAmount();
        this.soDeliveralone = info.getDeliverAlone();
        this.soDelivertype = info.getDeliverType();
        this.soSerialno = info.getSerialNo();
        this.soDrugremark = info.getDrugRemark();
    }

}