package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaMaterialAssess;
import com.gys.business.service.data.StoreStockReportInData;
import com.gys.business.service.data.StoreStockReportOutData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaMaterialAssessMapper extends BaseMapper<GaiaMaterialAssess> {

    List<GaiaMaterialAssess> getAllByProIds(@Param("client") String clientId, @Param("brId") String brId, @Param("proList") List<String> proList);

    List<StoreStockReportOutData> getStoreStockReport(StoreStockReportInData inData);
}
