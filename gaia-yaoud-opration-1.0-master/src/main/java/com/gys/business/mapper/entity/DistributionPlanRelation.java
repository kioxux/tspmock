package com.gys.business.mapper.entity;

import com.gys.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 15:00
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DistributionPlanRelation extends BaseEntity {
    /**铺货计划详情ID*/
    private Long planDetailId;
    /**铺货单号*/
    private String replenishCode;
}
