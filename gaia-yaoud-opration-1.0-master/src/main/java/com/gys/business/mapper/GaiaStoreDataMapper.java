package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.mapper.entity.StoreOutData;
import com.gys.business.service.data.*;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.ReportForms.InventoryChangeCheckInData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.BaseListSelect;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.StoreDcDataDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface GaiaStoreDataMapper extends BaseMapper<GaiaStoreData> {
    StoreOutData queryStoLeader(GetLoginOutData inDate);

    List<StoreOutData> queryStoInfoList(StoreOutData inData);

    List<GaiaStoreData> selectByGaiaStoreData(@Param("client") String client, @Param("brStart") String brStart, @Param("brEnd") String brEnd);

    StoreOutData getStoreData(@Param("client") String client, @Param("brId") String brId);

    Map<String,String> getDcCodeByStore(@Param("clientId") String client, @Param("stoCode") String brId);

    List<GetUserStoreOutData> getStoreOutData(GetUserStoreOutData inData);

    List<StoreOutData> getStoreList(@Param("client") String client,@Param("content") String content);

    String selectStoPriceComparison(@Param("clientId") String client, @Param("stoCode") String stoCode, @Param("paramStr") String paramStr);

    String selectWtpsParam(@Param("clientId") String client,@Param("paramStr") String paramStr);

    String selectStoPriceRatio(@Param("clientId") String client, @Param("stoCode") String stoCode);

    List<Map<String, String>> selectStoInfoList(@Param("client") String client, @Param("stoChainHead") String stoChainHead);

    List<GaiaStoreData> getAllByClientAndBrId(@Param("client") String client);

    List<StoreOutData> getStoreListByDcCode(@Param("client") String client,@Param("dcCode") String dcCode);

    List<StoreOutData> getStoreListByDcCodeOrBrId(@Param("client") String client,@Param("site") String site);

    List<StoreOutData> getStoreListByDcCodesOrBrIds(@Param("client") String client,@Param("sites") List<String> sites);

    /**
     * 获取app 门店列表
     * @param client
     * @param userId
     * @return
     */
    List<GaiaStoreOutData> getAPPStoreList(@Param("client")String client, @Param("userId") String userId);

    GaiaStoreData findByClientAndStoCode(@Param("client")String client,@Param("stoCode") String stoCode);

    StoreOutData getInventoryStore(InData inData);

    List<PartialSynDto> getAllClient();

    List<PartialSynDto> getAllBrIds(@Param("list") List<String> client);

    List<StoreOutData> getListByClientAndAttribute(@Param("client")String client, @Param("attribute") String attribute);

    List<StoreOutData> getListByClientAndAttributeExt(@Param("client")String client, @Param("type") String type);

    List<GaiaStoreData> findList(@Param("client") String client);

    StoreOutData getUnique(StoreOutData cond);

    List<GaiaStoreData> selectByYaolian(String client);

    List<GaiaStoreData> getAllByClient(@Param("client") String client);

    /**
     * 根据连锁公司查询门店
     *
     * @param client      client
     * @param chainCompId chainCompId
     * @return  List<BaseListSelect>
     */
    List<BaseListSelect> selectStoListByChainComp(@Param("client") String client,@Param("chainCompId") String chainCompId);

    List<StoClassOutData> getStoreCodeByStoClass(InventoryChangeCheckInData inData);

    StoreDcDataDto getSite(@Param("client") String client, @Param("stoCode") String stoCode);

    // 将余额合计到GAIA_STORE_DATA
    void increaserRemainingSum(@Param("client")String client, @Param("stoCode")String stoCode, @Param("matAddAmt")BigDecimal matAddAmt);


    DcCodeVo getDcCode(@Param("client") String clientId,@Param("stoCode") String stoCode);

    List<GaiaStoreData> getStoreByClient(@Param("client")String client);
}
