//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdProductsGroup;
import com.gys.business.service.data.GetProductDetailOutData;
import com.gys.business.service.data.GetProductInData;
import com.gys.business.service.data.GetProductSortOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdProductsGroupMapper extends BaseMapper<GaiaSdProductsGroup> {
    List<GetProductSortOutData> querySort(GetProductInData inData);

    List<GetProductDetailOutData> queryDetail(GetProductInData inData);

    void add(GetProductInData inData);

    void updateDetail(GetProductDetailOutData inData);

    void addAll(List<GaiaSdProductsGroup> dataList);

    void deleteData(GetProductInData inData);

    List<GetProductSortOutData> selectProGroupByClient(@Param("clientId") String clientId);
}
