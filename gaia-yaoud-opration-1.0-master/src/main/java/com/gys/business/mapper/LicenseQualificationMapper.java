package com.gys.business.mapper;

import com.gys.business.service.GaiaLicenseQualificationOutData;
import com.gys.business.service.data.GaiaLicenseQualificationInData;

import java.util.List;
import java.util.Map;

public interface LicenseQualificationMapper {
    List<Map<String, String>> listSite(Map<String,String> queryMap);

    List<GaiaLicenseQualificationOutData> listSupQueryType(GaiaLicenseQualificationInData inData);

    List<GaiaLicenseQualificationOutData> listCusQueryType(GaiaLicenseQualificationInData inData);

    List<GaiaLicenseQualificationOutData> listProQueryType(GaiaLicenseQualificationInData inData);

    List<Map<String, String>> listSup(Map<String, String> queryMap);

    List<Map<String, String>> listCus(Map<String, String> queryMap);

    List<String> listClients();

    List<GaiaLicenseQualificationOutData> listStoQueryType(GaiaLicenseQualificationInData inData);

    List<Map<String, String>> listSto(Map<String, String> queryMap);

    Integer getCount(Map<String, String> queryMap);
}
