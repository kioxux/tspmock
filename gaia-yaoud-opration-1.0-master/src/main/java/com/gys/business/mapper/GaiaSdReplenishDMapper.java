
package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdDsfStock;
import com.gys.business.mapper.entity.GaiaSdReplenishD;
import com.gys.business.service.data.*;
import com.gys.business.service.data.check.OrderDetailForm;
import com.gys.business.service.data.check.RequestOrderDetailVO;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.SpecialClass;
import com.gys.common.data.SupInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdReplenishDMapper extends BaseMapper<GaiaSdReplenishD> {
    List<GetReplenishDetailOutData> queryDetailList(GetReplenishInData inData);

    void update(GaiaSdReplenishD gaiaSdReplenishD);

    void deleteByIds();

    List<QuerySupplierData> querySupplierName(@Param("clientId") String clientId,@Param("brId") String brId);

    void deleteDetail(GetReplenishInData inData);

    String getMovPrice(GetReplenishInData inData);

    void updatePrice(List<GaiaSdReplenishD> inData);

    List<GetReplenishDetailOutData>getReplenishDetailList(GetReplenishInData inData);

    Map<String,Object>getSupMsg(GetReplenishInData inData);

    List<Map<String,Object>>getMovPriceList(GetPlaceOrderInData inData);

    List<Map<String,Object>>getMovPriceListByBrIds(GetPlaceOrderInData inData);

    List<Map<String,Object>> getSupMsgList(Map<String,Object> inData);

    List<SpecialMedicineOutData> specialDrugsInquiry(SpecialMedicineInData inData);

    String judgment(GetLoginOutData userInfo);
    String globalType(GetLoginOutData userInfo);


    List<Map<String,String>> queryRatioProList(@Param("clientId") String clientId,@Param("stoCode") String brId,@Param("ratioCode") String ratioCode);

    List<SupInfo>nearDateSupList(GetReplenishInData inData);

    List<SupInfo> minPriceSupList(GetReplenishInData inData);

    int getCurrentSerialNo(GaiaSdReplenishD cond);

    List<SpecialClass> selectSpecialClass(@Param("clientId") String clientId,@Param("stoCode") String brId,@Param("bigClass") String bigClass);

    List<Map<String,String>> selectProThreeCode(@Param("clientId") String clientId,@Param("proCode") String proCode);

    List<GaiaSdDsfStock> selectProThreeStock(@Param("clientId") String clientId, @Param("proCode") String proCode);

    List<RequestOrderDetailVO> queryOrderDetail(OrderDetailForm orderDetailForm);

    GaiaSdReplenishD getUnique(GaiaSdReplenishD cond);

    int deleteByVoucherId(GaiaSdReplenishD replenishD);

    int updateReplenishD(GaiaSdReplenishD replenishD);

    int getCount(GaiaSdReplenishD replenishD);

    List<GaiaSdReplenishD> findList(GaiaSdReplenishD cond);

    void saveBatch(@Param("replenishDList") List<GaiaSdReplenishD> replenishDList);
}
