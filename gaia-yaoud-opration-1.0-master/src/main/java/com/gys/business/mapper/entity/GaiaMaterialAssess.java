package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author
 */
@Data
@Table(name = "GAIA_MATERIAL_ASSESS")
public class GaiaMaterialAssess implements Serializable {
    private static final long serialVersionUID = -6526417986420660945L;
    @Id
    @Column(name = "CLIENT")
    private String client;

    @Id
    @Column(name = "MAT_PRO_CODE")
    private String matProCode;

    @Id
    @Column(name = "MAT_ASSESS_SITE")
    private String matAssessSite;

    @Column(name = "MAT_TOTAL_QTY")
    private BigDecimal matTotalQty;

    @Column(name = "MAT_TOTAL_AMT")
    private BigDecimal matTotalAmt;

    @Column(name = "MAT_MOV_PRICE")
    private BigDecimal matMovPrice;

    @Column(name = "MAT_RATE_AMT")
    private BigDecimal matRateAmt;

    @Column(name = "BAT_CREATE_DATE")
    private String batCreateDate;

    @Column(name = "BAT_CREATE_TIME")
    private String batCreateTime;

    @Column(name = "BAT_CREATE_USER")
    private String batCreateUser;

    @Column(name = "BAT_CHANGE_DATE")
    private String batChangeDate;

    @Column(name = "BAT_CHANGE_TIME")
    private String batChangeTime;

    @Column(name = "BAT_CHANGE_USER")
    private String batChangeUser;

    /**
     * 加点后金额
     */
    @Column(name = "MAT_ADD_AMT")
    private BigDecimal matAddAmt;

    /**
     * 加点后税金
     */
    @Column(name = "MAT_ADD_TAX")
    private BigDecimal matAddTax;
}
