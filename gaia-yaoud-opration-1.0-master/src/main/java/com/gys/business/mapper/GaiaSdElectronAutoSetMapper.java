package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdElectronAutoSet;
import com.gys.business.service.data.ThemeSettingToAutoSetVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdElectronAutoSetMapper {

    int insertSelective(GaiaSdElectronAutoSet record);


    List<GaiaSdElectronAutoSet> selectByExample(GaiaSdElectronAutoSet example);

    GaiaSdElectronAutoSet selectByPrimaryKey(GaiaSdElectronAutoSet key);

    int updateByExampleSelective(@Param("record") GaiaSdElectronAutoSet record);

    int updateByExample(@Param("record") GaiaSdElectronAutoSet record);

    int updateByPrimaryKeySelective(@Param("record") GaiaSdElectronAutoSet record);

    int updateByPrimaryKey(GaiaSdElectronAutoSet record);

    int batchInsert(@Param("list") List<GaiaSdElectronAutoSet> list);

    List<GaiaSdElectronAutoSet> selectAll(@Param("client") String client, @Param("gsetsId") String gsetsId);

    List<GaiaSdElectronAutoSet> getAutoSetByClientAndGseasFlag(@Param("client") String client, @Param("gsetsId") String gsetsId, @Param("gsetsFlag") String gsetsFlag);

    List<GaiaSdElectronAutoSet> timeComparison(ThemeSettingToAutoSetVO inVo);

    void batchUpdate(@Param("list")List<GaiaSdElectronAutoSet> autoSetList);
}