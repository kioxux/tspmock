//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_SALE_RETURN_H"
)
public class GaiaSdSaleReturnH implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSSRH_VOUCHER_ID"
    )
    private String gssrhVoucherId;
    @Column(
            name = "GSSRH_BR_ID"
    )
    private String gssrhBrId;
    @Column(
            name = "GSSRH_SALENO"
    )
    private String gssrhSaleno;
    @Column(
            name = "GSSRH_SALEAMT"
    )
    private BigDecimal gssrhSaleamt;
    @Column(
            name = "GSSRH_SALEDATE"
    )
    private String gssrhSaledate;
    @Column(
            name = "GSSRH_SALETIME"
    )
    private String gssrhSaletime;
    @Column(
            name = "GSSRH_RETNO"
    )
    private String gssrhRetno;
    @Column(
            name = "GSSRH_RETAMT"
    )
    private BigDecimal gssrhRetamt;
    @Column(
            name = "GSSRH_RETDATE"
    )
    private String gssrhRetdate;
    @Column(
            name = "GSSRH_RETTIME"
    )
    private String gssrhRettime;
    private static final long serialVersionUID = 1L;

    public GaiaSdSaleReturnH() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGssrhVoucherId() {
        return this.gssrhVoucherId;
    }

    public void setGssrhVoucherId(String gssrhVoucherId) {
        this.gssrhVoucherId = gssrhVoucherId;
    }

    public String getGssrhBrId() {
        return this.gssrhBrId;
    }

    public void setGssrhBrId(String gssrhBrId) {
        this.gssrhBrId = gssrhBrId;
    }

    public String getGssrhSaleno() {
        return this.gssrhSaleno;
    }

    public void setGssrhSaleno(String gssrhSaleno) {
        this.gssrhSaleno = gssrhSaleno;
    }

    public BigDecimal getGssrhSaleamt() {
        return this.gssrhSaleamt;
    }

    public void setGssrhSaleamt(BigDecimal gssrhSaleamt) {
        this.gssrhSaleamt = gssrhSaleamt;
    }

    public String getGssrhSaledate() {
        return this.gssrhSaledate;
    }

    public void setGssrhSaledate(String gssrhSaledate) {
        this.gssrhSaledate = gssrhSaledate;
    }

    public String getGssrhSaletime() {
        return this.gssrhSaletime;
    }

    public void setGssrhSaletime(String gssrhSaletime) {
        this.gssrhSaletime = gssrhSaletime;
    }

    public String getGssrhRetno() {
        return this.gssrhRetno;
    }

    public void setGssrhRetno(String gssrhRetno) {
        this.gssrhRetno = gssrhRetno;
    }

    public BigDecimal getGssrhRetamt() {
        return this.gssrhRetamt;
    }

    public void setGssrhRetamt(BigDecimal gssrhRetamt) {
        this.gssrhRetamt = gssrhRetamt;
    }

    public String getGssrhRetdate() {
        return this.gssrhRetdate;
    }

    public void setGssrhRetdate(String gssrhRetdate) {
        this.gssrhRetdate = gssrhRetdate;
    }

    public String getGssrhRettime() {
        return this.gssrhRettime;
    }

    public void setGssrhRettime(String gssrhRettime) {
        this.gssrhRettime = gssrhRettime;
    }
}
