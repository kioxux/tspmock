package com.gys.business.mapper;

import com.gys.business.mapper.entity.member.GaiaSdIntegralCashSetRule;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 积分抵现规则
 *
 * @author wu mao yin
 * @date 2021/12/8 17:44
 */
public interface GaiaSdIntegralCashSetRuleMapper extends BaseMapper<GaiaSdIntegralCashSetRule> {

    /**
     * 根据规则内容代号删除规则
     *
     * @param client client
     * @param gsicsrCode gsicsrCode
     * @return int
     */
    int deleteIntegralCashSet(@Param("client") String client, @Param("gsicsrCode") String gsicsrCode);

    /**
     * 根据规则内容代号删除规则
     *
     * @param client client
     * @param gsicsrCode gsicsrCode
     * @return int
     */
    int selectList(@Param("client") String client, @Param("gsicsrCode") String gsicsrCode);

}