package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMemberClass;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdMemberClassMapper extends BaseMapper<GaiaSdMemberClass> {

    List<GaiaSdMemberClass> getAllByClientAndBrId(@Param("client") String client);
}
