package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Table(
        name = "GAIA_SD_SR_SALE_RECORD"
)
public class GaiaSdSrSaleRecord implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSSS_BR_ID"
    )
    private String gsssBrId;
    @Id
    @Column(
            name = "GSSS_DATE"
    )
    private String gsssDate;
    @Column(
            name = "GSSS_TIME"
    )
    private String gsssTime;
    @Id
    @Column(
            name = "GSSS_BILL_NO"
    )
    private String gsssBillNo;
    @Id
    @Column(
            name = "GSSS_SERIAL"
    )
    private String gsssSerial;
    @Column(
            name = "GSSS_PRO_ID"
    )
    private String gsssProId;
    @Column(
            name = "GSSS_RELE_PRO_TYPE"
    )
    private String gsssReleProType;
    @Column(
            name = "GSSS_COMPCLASS"
    )
    private String gsssCompclass;
    @Column(
            name = "GSSS_SALE_QTY"
    )
    private String gsssSaleQty;
    @Column(
            name = "GSSS_SALE_AMT"
    )
    private String gsssSaleAmt;
    @Column(
            name = "GSSS_EMP"
    )
    private String gsssEmp;
    
    private static final long serialVersionUID = 1L;

}
