package com.gys.business.mapper;

import com.gys.business.service.data.ReportForms.ProductSalesBySupplierInData;
import com.gys.business.service.data.ReportForms.ProductSalesBySupplierOutData;

import java.util.List;

public interface GaiaSdBatchChanegMapper {
    List<ProductSalesBySupplierOutData> getProductSalesBySupplier(ProductSalesBySupplierInData inData);
}
