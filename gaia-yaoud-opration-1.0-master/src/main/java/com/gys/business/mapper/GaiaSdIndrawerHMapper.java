//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIndrawerH;
import com.gys.business.service.data.AddBoxInfoOutData;
import com.gys.business.service.data.GetAddBoxInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdIndrawerHMapper extends BaseMapper<GaiaSdIndrawerH> {
    String selectMaxgsihVoucherId();

    void insertData(GetAddBoxInData inData);

    List<AddBoxInfoOutData> getAddBox(AddBoxInfoOutData inData);
}
