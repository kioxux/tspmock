package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_AL_PL_CON")
public class GaiaAlPlCon implements Serializable {
    /**
     * 区域
     */
    @Id
    @Column(name = "APL_AREA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String aplArea;

    /**
     * 门店
     */
    @Id
    @Column(name = "APL_BR_ID")
    private String aplBrId;

    /**
     * 成分
     */
    @Id
    @Column(name = "APL_COMP")
    private String aplComp;

    /**
     * 商品
     */
    @Id
    @Column(name = "APL_PRO_CODE")
    private String aplProCode;

    /**
     * 结论编号
     */
    @Id
    @Column(name = "APL_CON_CODE")
    private String aplConCode;

    /**
     * 结论日期
     */
    @Column(name = "APL_CON_DATE")
    private String aplConDate;

    /**
     * 结论时间
     */
    @Column(name = "APL_CON_TIME")
    private String aplConTime;

    /**
     * 结论
     */
    @Column(name = "APL_CON_TEXT")
    private String aplConText;

    /**
     * 建议数量
     */
    @Column(name = "APL_CON_QTY")
    private BigDecimal aplConQty;

    /**
     * 建议价格
     */
    @Column(name = "APL_CON_PRICE")
    private BigDecimal aplConPrice;

    /**
     * 状态
     */
    @Column(name = "APL_CON_STU")
    private String aplConStu;

    /**
     * 更新日期
     */
    @Column(name = "APL_UPDATE_DATE")
    private String aplUpdateDate;

    /**
     * 更新时间
     */
    @Column(name = "APL_UPDATE_TIME")
    private String aplUpdateTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取区域
     *
     * @return APL_AREA - 区域
     */
    public String getAplArea() {
        return aplArea;
    }

    /**
     * 设置区域
     *
     * @param aplArea 区域
     */
    public void setAplArea(String aplArea) {
        this.aplArea = aplArea;
    }

    /**
     * 获取门店
     *
     * @return APL_BR_ID - 门店
     */
    public String getAplBrId() {
        return aplBrId;
    }

    /**
     * 设置门店
     *
     * @param aplBrId 门店
     */
    public void setAplBrId(String aplBrId) {
        this.aplBrId = aplBrId;
    }

    /**
     * 获取成分
     *
     * @return APL_COMP - 成分
     */
    public String getAplComp() {
        return aplComp;
    }

    /**
     * 设置成分
     *
     * @param aplComp 成分
     */
    public void setAplComp(String aplComp) {
        this.aplComp = aplComp;
    }

    /**
     * 获取商品
     *
     * @return APL_PRO_CODE - 商品
     */
    public String getAplProCode() {
        return aplProCode;
    }

    /**
     * 设置商品
     *
     * @param aplProCode 商品
     */
    public void setAplProCode(String aplProCode) {
        this.aplProCode = aplProCode;
    }

    /**
     * 获取结论编号
     *
     * @return APL_CON_CODE - 结论编号
     */
    public String getAplConCode() {
        return aplConCode;
    }

    /**
     * 设置结论编号
     *
     * @param aplConCode 结论编号
     */
    public void setAplConCode(String aplConCode) {
        this.aplConCode = aplConCode;
    }

    /**
     * 获取结论日期
     *
     * @return APL_CON_DATE - 结论日期
     */
    public String getAplConDate() {
        return aplConDate;
    }

    /**
     * 设置结论日期
     *
     * @param aplConDate 结论日期
     */
    public void setAplConDate(String aplConDate) {
        this.aplConDate = aplConDate;
    }

    /**
     * 获取结论时间
     *
     * @return APL_CON_TIME - 结论时间
     */
    public String getAplConTime() {
        return aplConTime;
    }

    /**
     * 设置结论时间
     *
     * @param aplConTime 结论时间
     */
    public void setAplConTime(String aplConTime) {
        this.aplConTime = aplConTime;
    }

    /**
     * 获取结论
     *
     * @return APL_CON_TEXT - 结论
     */
    public String getAplConText() {
        return aplConText;
    }

    /**
     * 设置结论
     *
     * @param aplConText 结论
     */
    public void setAplConText(String aplConText) {
        this.aplConText = aplConText;
    }

    /**
     * 获取建议数量
     *
     * @return APL_CON_QTY - 建议数量
     */
    public BigDecimal getAplConQty() {
        return aplConQty;
    }

    /**
     * 设置建议数量
     *
     * @param aplConQty 建议数量
     */
    public void setAplConQty(BigDecimal aplConQty) {
        this.aplConQty = aplConQty;
    }

    /**
     * 获取建议价格
     *
     * @return APL_CON_PRICE - 建议价格
     */
    public BigDecimal getAplConPrice() {
        return aplConPrice;
    }

    /**
     * 设置建议价格
     *
     * @param aplConPrice 建议价格
     */
    public void setAplConPrice(BigDecimal aplConPrice) {
        this.aplConPrice = aplConPrice;
    }

    /**
     * 获取状态
     *
     * @return APL_CON_STU - 状态
     */
    public String getAplConStu() {
        return aplConStu;
    }

    /**
     * 设置状态
     *
     * @param aplConStu 状态
     */
    public void setAplConStu(String aplConStu) {
        this.aplConStu = aplConStu;
    }

    /**
     * 获取更新日期
     *
     * @return APL_UPDATE_DATE - 更新日期
     */
    public String getAplUpdateDate() {
        return aplUpdateDate;
    }

    /**
     * 设置更新日期
     *
     * @param aplUpdateDate 更新日期
     */
    public void setAplUpdateDate(String aplUpdateDate) {
        this.aplUpdateDate = aplUpdateDate;
    }

    /**
     * 获取更新时间
     *
     * @return APL_UPDATE_TIME - 更新时间
     */
    public String getAplUpdateTime() {
        return aplUpdateTime;
    }

    /**
     * 设置更新时间
     *
     * @param aplUpdateTime 更新时间
     */
    public void setAplUpdateTime(String aplUpdateTime) {
        this.aplUpdateTime = aplUpdateTime;
    }
}