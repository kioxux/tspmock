package com.gys.business.mapper;

import com.gys.business.cashbox.form.CashBoxForm;
import com.gys.business.mapper.entity.CashBox;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 02:14
 */
@Mapper
public interface CashBoxMapper {

    CashBox getById(Long id);

    CashBox getUnique(CashBox cond);

    int add(CashBox cashBox);

    int update(CashBox cashBox);

    List<CashBox> findList(CashBoxForm cond);

}
