package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaFiApPayment;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 应付方式维护表 Mapper 接口
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Mapper
public interface GaiaFiApPaymentMapper extends BaseMapper<GaiaFiApPayment> {
}
