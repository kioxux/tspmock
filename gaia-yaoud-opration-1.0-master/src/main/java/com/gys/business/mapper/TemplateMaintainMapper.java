package com.gys.business.mapper;

import com.gys.common.data.wechatTemplate.GaiaMouldMaintainD;
import com.gys.common.data.wechatTemplate.GaiaMouldMaintainDTO;
import com.gys.common.data.wechatTemplate.GaiaMouldMaintainH;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TemplateMaintainMapper {
    // 查询所有模板
    List<GaiaMouldMaintainDTO> queryAllTemplate(@Param("mouldName") String mouldName,@Param("status") String status,@Param("pageStart")Integer pageStart,@Param("pageSize") Integer pageSize);

    // 统计模板个数
    Integer countTemplate();

    // 添加微信模板主表内容
    void insertTemplateH(GaiaMouldMaintainH gaiaMouldMaintainH);

    // 添加微信模板明细表内容
    void insertTemplateD(GaiaMouldMaintainD gaiaMouldMaintainD);

    // 删除微信模板主表内容
    void delTemplateH(@Param("mouldCode") String mouldCode);

    // 删除微信模板明细表内容
    void delTemplateD(@Param("mouldCode") String mouldCode);

    // 修改微信模板状态
    void updateTemplateStatus(@Param("mouldCode")String mouldCode, @Param("status") String status);

    // 更新微信模板主表
    void updateTemplateH(GaiaMouldMaintainH gaiaMouldMaintainH);

    GaiaMouldMaintainDTO queryTemplateByCode(String mouldCode);

    // 统计启用模板个数
    Integer countStartTemplate(@Param("status") String status);


}
