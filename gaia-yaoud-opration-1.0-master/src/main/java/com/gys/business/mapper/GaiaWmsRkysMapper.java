package com.gys.business.mapper;

import com.gys.business.mapper.entity.WmsRkys;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/3 16:30
 **/
public interface GaiaWmsRkysMapper {

    WmsRkys getUnique(WmsRkys cond);

    List<WmsRkys> findList(WmsRkys cond);

    String getByClientAndProCodeAndPhAndBatchNo(@Param("client") String client,@Param("proSelfCode") String proSelfCode,@Param("batchNo") String gssbBatchNo,@Param("batch") String gssbBatch);
}
