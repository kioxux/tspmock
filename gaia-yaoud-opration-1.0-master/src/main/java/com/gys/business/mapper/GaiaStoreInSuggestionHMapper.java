package com.gys.business.mapper;

import com.gys.business.entity.dto.CallUpOrInStoreDetailDTO;
import com.gys.business.entity.vo.CallInStoreDetailVO;
import com.gys.business.mapper.entity.GaiaStoreInSuggestionD;
import com.gys.business.mapper.entity.GaiaStoreInSuggestionH;
import com.gys.common.data.suggestion.SuggestionInForm;
import com.gys.common.data.suggestion.SuggestionInVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;

/**
 * 门店调入建议-主表(GaiaStoreInSuggestionH)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-28 10:53:20
 */
public interface GaiaStoreInSuggestionHMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaStoreInSuggestionH queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaStoreInSuggestionH 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaStoreInSuggestionH> queryAllByLimit(GaiaStoreInSuggestionH gaiaStoreInSuggestionH, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaStoreInSuggestionH 查询条件
     * @return 总行数
     */
    long count(GaiaStoreInSuggestionH gaiaStoreInSuggestionH);

    /**
     * 新增数据
     *
     * @param gaiaStoreInSuggestionH 实例对象
     * @return 影响行数
     */
    int insert(GaiaStoreInSuggestionH gaiaStoreInSuggestionH);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaStoreInSuggestionH> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaStoreInSuggestionH> entities);

    /**
     * 修改数据
     *
     * @param gaiaStoreInSuggestionH 实例对象
     * @return 影响行数
     */
    int update(GaiaStoreInSuggestionH gaiaStoreInSuggestionH);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    List<SuggestionInVO> findList(SuggestionInForm suggestionInForm);

    GaiaStoreInSuggestionH getUnique(GaiaStoreInSuggestionH cond);

    List<GaiaStoreInSuggestionH> listInvalidBill();

    List<CallInStoreDetailVO> queryDetail(CallUpOrInStoreDetailDTO storeDetailDTO);

    List<Map<String, String>> queryBillCode(@Param("startDate") String startDate ,@Param("endDate") String endDate);

}

