package com.gys.business.mapper.takeAway;

import java.util.List;
import java.util.Map;

public interface BaseMapperWeb {
    List<Map<String,Object>> getInfoByProc(Map<String, Object> param);

    void setSwitchInfo(List<Map<String, Object>> param);

    int existSwichSetTable(Map<String, Object> param);

    void dropSwichSetTable(Map<String, Object> param);

    List<Map<String,Object>> SwichSetTable(Map<String, Object> param);

    Map<String, Object> getMySwitchSetInfo(Map<String, Object> param);

    int existMyAsyncTaskInfo(Map<String, Object> param);

    void insertMyAsyncTask(Map<String, Object> param);

    void updateMyAsyncTask(Map<String, Object> param);
}
