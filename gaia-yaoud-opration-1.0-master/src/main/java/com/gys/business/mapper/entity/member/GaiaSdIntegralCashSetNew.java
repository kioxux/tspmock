package com.gys.business.mapper.entity.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

/**
 * 积分抵现规则设置
 *
 * @author wu mao yin
 * @date 2021/12/8 17:41
 */
@Data
@Table(name = "GAIA_SD_INTEGRAL_CASH_SET_NEW")
public class GaiaSdIntegralCashSetNew implements Serializable {
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 积分抵现规则代号
     */
    @Column(name = "GSICS_PLAN_ID")
    private String gsicsPlanId;

    /**
     * 积分抵现规则描述
     */
    @Column(name = "GSICS_PLAN_NAME")
    private String gsicsPlanName;

    /**
     * 类型 0-单次固定，1-最优
     */
    @Column(name = "GSICS_TYPE")
    private Integer gsicsType;

    /**
     * 抵用金额上限控制 0-无，1-有
     */
    @Column(name = "GSICS_AMT_LIMIT_FLAG")
    private Integer gsicsAmtLimitFlag;

    /**
     * 抵扣金额比例
     */
    @Column(name = "GSICS_LIMIT_RATE")
    private BigDecimal gsicsLimitRate;

    /**
     * 单笔最大抵现金额
     */
    @Column(name = "GSICS_AMT_OFFSET_MAX")
    private BigDecimal gsicsAmtOffsetMax;

    /**
     * 单笔交易是否积分 0-否，1-是
     */
    @Column(name = "GSICS_INTEGRAL_ADD_SET")
    private Integer gsicsIntegralAddSet;

    /**
     * 所有会员 true:是，false否
     */
    @Column(name = "GSICS_ALL_MEMBER_CARD_TYPE")
    private Boolean allMemberCardType;

    /**
     * 会员卡类型
     */
    @Column(name = "GSICS_MEMBER_CARD_TYPE")
    private String gsicsMemberCardType;

    /**
     * 规则内容代号
     */
    @Column(name = "GSICS_CODE")
    private String gsicsCode;

    /**
     * 创建日期
     */
    @Column(name = "GSICS_CR_DATE")
    private Date gsicsCrDate;

    /**
     * 创建人
     */
    @Column(name = "GSICS_CR_ID")
    private String gsicsCrId;

    /**
     * 修改日期
     */
    @Column(name = "GSICS_MD_DATE")
    private Date gsicsMdDate;

    /**
     * 修改人
     */
    @Column(name = "GSICS_MD_ID")
    private String gsicsMdId;

    private static final long serialVersionUID = 1L;

}