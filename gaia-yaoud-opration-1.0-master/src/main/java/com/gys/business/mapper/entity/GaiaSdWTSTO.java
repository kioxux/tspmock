package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/15 21:45
 */
@Data
@Table(name = "GAIA_SD_WTSTO")
public class GaiaSdWTSTO implements Serializable {
    private static final long serialVersionUID = 2518741337174308586L;
    /**
     * ID
     */
    @Id
    @Column( name = "ID")
    private Long id;
    /**
     * 加盟商
     */
    @Column( name = "CLIENT")
    private String client;
    /**
     * 药德门店编码
     */
    @Column( name = "STO_SITE")
    private String stoSite;
    /**
     * 第三方门店编码
     */
    @Column( name = "STO_DSF_SITE")
    private String stoDsfSite;
    /**
     * 药德/原始门店内码
     */
    @Column( name = "STO_NM")
    private String stoNm;
}
