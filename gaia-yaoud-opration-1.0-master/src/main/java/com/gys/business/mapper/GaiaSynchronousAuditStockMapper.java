package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaSynchronousAuditStock;
import com.gys.business.service.data.GaiaSynchronousAuditStockOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 库存审计(GaiaSynchronousAuditStock)表数据库访问层
 *
 * @author makejava
 * @since 2021-07-01 13:43:38
 */
public interface GaiaSynchronousAuditStockMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaSynchronousAuditStock queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaSynchronousAuditStock> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaSynchronousAuditStock 实例对象
     * @return 对象列表
     */
    List<GaiaSynchronousAuditStock> queryAll(GaiaSynchronousAuditStock gaiaSynchronousAuditStock);

    /**
     * 新增数据
     *
     * @param gaiaSynchronousAuditStock 实例对象
     * @return 影响行数
     */
    int insert(GaiaSynchronousAuditStock gaiaSynchronousAuditStock);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaSynchronousAuditStock> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaSynchronousAuditStock> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaSynchronousAuditStock> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaSynchronousAuditStock> entities);

    /**
     * 修改数据
     *
     * @param gaiaSynchronousAuditStock 实例对象
     * @return 影响行数
     */
    int update(GaiaSynchronousAuditStock gaiaSynchronousAuditStock);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    List<GaiaSynchronousAuditStock> listStockInfo();


    List<GaiaSynchronousAuditStockOutData> listAuditInfo(GaiaSynchronousAuditStockOutData inData);

    GaiaSynchronousAuditStock auditQuery(GaiaSynchronousAuditStock auditStock);
}

