package com.gys.business.mapper;

import com.gys.business.mapper.entity.DcOutStockStore;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/22 16:40
 */
public interface DcOutStockStoreMapper {

    DcOutStockStore getById(Long id);

    int add(DcOutStockStore dcOutStockStore);

    List<DcOutStockStore> findList(DcOutStockStore cond);

}
