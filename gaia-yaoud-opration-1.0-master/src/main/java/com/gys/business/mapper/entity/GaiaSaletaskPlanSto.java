package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SALETASK_PLAN_STO")
@Data
public class GaiaSaletaskPlanSto implements Serializable {

    /**
     * 客户ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private int id;
    /**
     * 客户ID
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店编号
     */
    @Column(name = "STO_CODE")
    private String stoCode;

    /**
     * 任务ID
     */
    @Column(name = "STASK_TASK_ID")
    private String staskTaskId;

    /**
     * 计划月份
     */
    @Column(name = "MONTH_PLAN")
    private String monthPlan;

    /**
     * 期间天数
     */
    @Column(name = "MONTH_DAYS")
    private Integer monthDays;

    /**
     * 营业额本期日均计划
     */
    @Column(name = "A_SALE_AMT")
    private BigDecimal aSaleAmt;

    /**
     * 毛利额本期日均计划
     */
    @Column(name = "A_SALE_GROSS")
    private BigDecimal aSaleGross;

    /**
     * 会员卡本期日均计划
     */
    @Column(name = "A_MCARD_QTY")
    private Integer aMcardQty;

    /**
     * 创建人账号
     */
    @Column(name = "STASK_CRE_ID")
    private String staskCreId;

    /**
     * 创建日期
     */
    @Column(name = "STASK_CRE_DATE")
    private String staskCreDate;

    /**
     * 创建时间
     */
    @Column(name = "STASK_CRE_TIME")
    private String staskCreTime;

    /**
     * 修改人
     */
    @Column(name = "STASK_MD_ID")
    private String staskMdId;

    /**
     * 修改日期
     */
    @Column(name = "STASK_MD_DATE")
    private String staskMdDate;

    /**
     * 修改时间
     */
    @Column(name = "STASK_MD_TIME")
    private String staskMdTime;

    /**
     * 删除 0 未删除 1已删除
     */
    @Column(name = "STASK_TYPE")
    private String staskType;

    /**
     * 是否默认显示 0 否 1是
     */
    @Column(name = "STASK_IS_SHOW")
    private String staskIsShow;

    /**
     * 营业额同期日均
     */
    @Column(name = "A_SALE_AMT_SDAILY")
    private BigDecimal aSaleAmtSdaily;

    /**
     * 营业额上期日均
     */
    @Column(name = "A_SALE_AMT_PDAILY")
    private BigDecimal aSaleAmtPdaily;

    /**
     * 毛利额同期日均
     */
    @Column(name = "A_SALE_GROSS_SDAILY")
    private BigDecimal aSaleGrossSdaily;

    /**
     * 毛利额上期日均
     */
    @Column(name = "A_SALE_GROSS_PDAILY")
    private BigDecimal aSaleGrossPdaily;

    /**
     * 毛利率同期日均
     */
    @Column(name = "A_SALE_GROSS_RATE_SDAILY")
    private BigDecimal aSaleGrossRateSdaily;

    /**
     * 毛利率上期日均
     */
    @Column(name = "A_SALE_GROSS_RATE_PDAILY")
    private BigDecimal aSaleGrossRatePdaily;

    /**
     * 会员卡同期日均
     */
    @Column(name = "A_MCARD_QTY_SDAILY")
    private BigDecimal aMcardQtySdaily;

    /**
     * 会员卡上期日均
     */
    @Column(name = "A_MCARD_QTY_PDAILY")
    private BigDecimal aMcardQtyPdaily;

    /**
     * 营业额同比增长率
     */
    @Column(name = "YOY_SALE_AMT")
    private BigDecimal yoySaleAmt;

    /**
     * 营业额环比增长率
     */
    @Column(name = "MOM_SALE_AMT")
    private BigDecimal momSaleAmt;

    /**
     * 毛利额同比增长率
     */
    @Column(name = "YOY_SALE_GROSS")
    private BigDecimal yoySaleGross;

    /**
     * 毛利额环比增长率
     */
    @Column(name = "MOM_SALE_GROSS")
    private BigDecimal momSaleGross;

    /**
     * 会员卡同比增长率
     */
    @Column(name = "YOY_MCARD_QTY")
    private BigDecimal yoyMcardQty;

    /**
     * 会员卡环比增长率
     */
    @Column(name = "MOM_MCARD_QTY")
    private BigDecimal momMcardQty;

    /**
     * 营业额
     */
    @Column(name = "SALE_AMT")
    private BigDecimal saleAmt;

    /**
     * 毛利额
     */
    @Column(name = "SALE_GROSS")
    private BigDecimal saleGross;

    /**
     * 会员卡
     */
    @Column(name = "MCARD_QTY")
    private BigDecimal mcardQty;
}