package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName GAIA_SD_ENV
 */
@Data
public class GaiaSdEnv implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 环境code
     */
    private String env;

    /**
     * 描述
     */
    private String remark;

    /**
     * 地址
     */
    private String url;

    private static final long serialVersionUID = 1L;
}