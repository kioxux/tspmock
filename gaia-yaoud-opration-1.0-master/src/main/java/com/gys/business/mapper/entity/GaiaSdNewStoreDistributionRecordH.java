package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value="com-gys-business-mapper-entity-GaiaSdNewStoreDistributionRecordH")
public class GaiaSdNewStoreDistributionRecordH {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 门店编码
    */
    @ApiModelProperty(value="门店编码")
    private String storeId;

    /**
     * 创建日期
     */
    @ApiModelProperty(value="创建日期")
    private String createDate;

    /**
    * 单号
    */
    @ApiModelProperty(value="单号")
    private String orderId;

    /**
    * 实际铺货品项数
    */
    @ApiModelProperty(value="实际铺货品项数")
    private BigDecimal resultNumber;

    /**
    * 参考成本额
    */
    @ApiModelProperty(value="参考成本额")
    private BigDecimal referenceCostAmt;

    /**
    * 参考零售额
    */
    @ApiModelProperty(value="参考零售额")
    private BigDecimal referenceSaleAmt;

    /**
    * 创建人
    */
    @ApiModelProperty(value="创建人")
    private String createUser;

    /**
    * 创建时间
    */
    @ApiModelProperty(value="创建时间")
    private Date createTime;

}