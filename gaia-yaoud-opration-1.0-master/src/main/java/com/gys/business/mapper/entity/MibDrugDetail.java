package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.*;

@Data
@Table(name = "MIB_DRUG_DETAIL")
public class MibDrugDetail implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店
     */
    @Column(name = "BR_ID")
    private String brId;

    /**
     * 药德订单编码
     */
    @Column(name = "BILL_NO")
    private String billNo;

    /**
     * 发送方报文ID
     */
    @Column(name = "MSG_ID")
    private String msgId;

    /**
     * 费用明细流 水号
     */
    @Column(name = "FEEDETL_SN")
    private String feedetlSn;

    /**
     * 处方号
     */
    @Column(name = "RXNO")
    private String rxno;

    /**
     * 外购处方标 志
     */
    @Column(name = "RX_CIRC_FLAG")
    private String rxCircFlag;

    /**
     * 费用发生时 间
     */
    @Column(name = "FEE_OCUR_TIME")
    private String feeOcurTime;

    /**
     * 医疗目录编 码
     */
    @Column(name = "MED_LIST_CODG")
    private String medListCodg;

    /**
     * 医药机构目录编码
     */
    @Column(name = "MEDINS_LIST_CODG")
    private String medinsListCodg;

    /**
     * 明细项目费 用总额
     */
    @Column(name = "DET_ITEM_FEE_SUMAMT")
    private String detItemFeeSumamt;

    /**
     * 数量
     */
    @Column(name = "CNT")
    private String cnt;

    /**
     * 单价
     */
    @Column(name = "PRIC")
    private String pric;

    /**
     * 单次剂量描 述
     */
    @Column(name = "SIN_DOS_DSCR")
    private String sinDosDscr;

    /**
     * 使用频次描 述
     */
    @Column(name = "USED_FRQU_DSCR")
    private String usedFrquDscr;

    /**
     * 周期天数
     */
    @Column(name = "PRD_DAYS")
    private String prdDays;

    /**
     *  用药途径描 述
     */
    @Column(name = "MEDC_WAY_DSCR")
    private String medcWayDscr;

    /**
     * 开单医生编 码
     */
    @Column(name = "BILG_DR_CODG")
    private String bilgDrCodg;

    /**
     * 开单医师姓 名
     */
    @Column(name = "BILG_DR_NAME")
    private String bilgDrName;

    /**
     * 中药使用方 式
     */
    @Column(name = "TCMDRUG_USED_WAY")
    private String tcmdrugUsedWay;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取门店
     *
     * @return BR_ID - 门店
     */
    public String getBrId() {
        return brId;
    }

    /**
     * 设置门店
     *
     * @param brId 门店
     */
    public void setBrId(String brId) {
        this.brId = brId;
    }

    /**
     * 获取药德订单编码
     *
     * @return BILL_NO - 药德订单编码
     */
    public String getBillNo() {
        return billNo;
    }

    /**
     * 设置药德订单编码
     *
     * @param billNo 药德订单编码
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * 获取发送方报文ID
     *
     * @return MSG_ID - 发送方报文ID
     */
    public String getMsgId() {
        return msgId;
    }

    /**
     * 设置发送方报文ID
     *
     * @param msgId 发送方报文ID
     */
    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    /**
     * 获取费用明细流 水号
     *
     * @return FEEDETL_SN - 费用明细流 水号
     */
    public String getFeedetlSn() {
        return feedetlSn;
    }

    /**
     * 设置费用明细流 水号
     *
     * @param feedetlSn 费用明细流 水号
     */
    public void setFeedetlSn(String feedetlSn) {
        this.feedetlSn = feedetlSn;
    }

    /**
     * 获取处方号
     *
     * @return RXNO - 处方号
     */
    public String getRxno() {
        return rxno;
    }

    /**
     * 设置处方号
     *
     * @param rxno 处方号
     */
    public void setRxno(String rxno) {
        this.rxno = rxno;
    }

    /**
     * 获取外购处方标 志
     *
     * @return RX_CIRC_FLAG - 外购处方标 志
     */
    public String getRxCircFlag() {
        return rxCircFlag;
    }

    /**
     * 设置外购处方标 志
     *
     * @param rxCircFlag 外购处方标 志
     */
    public void setRxCircFlag(String rxCircFlag) {
        this.rxCircFlag = rxCircFlag;
    }

    /**
     * 获取费用发生时 间
     *
     * @return FEE_OCUR_TIME - 费用发生时 间
     */
    public String getFeeOcurTime() {
        return feeOcurTime;
    }

    /**
     * 设置费用发生时 间
     *
     * @param feeOcurTime 费用发生时 间
     */
    public void setFeeOcurTime(String feeOcurTime) {
        this.feeOcurTime = feeOcurTime;
    }

    /**
     * 获取医疗目录编 码
     *
     * @return MED_LIST_CODG - 医疗目录编 码
     */
    public String getMedListCodg() {
        return medListCodg;
    }

    /**
     * 设置医疗目录编 码
     *
     * @param medListCodg 医疗目录编 码
     */
    public void setMedListCodg(String medListCodg) {
        this.medListCodg = medListCodg;
    }

    /**
     * 获取医药机构目录编码
     *
     * @return MEDINS_LIST_CODG - 医药机构目录编码
     */
    public String getMedinsListCodg() {
        return medinsListCodg;
    }

    /**
     * 设置医药机构目录编码
     *
     * @param medinsListCodg 医药机构目录编码
     */
    public void setMedinsListCodg(String medinsListCodg) {
        this.medinsListCodg = medinsListCodg;
    }

    /**
     * 获取明细项目费 用总额
     *
     * @return DET_ITEM_FEE_SUMAMT - 明细项目费 用总额
     */
    public String getDetItemFeeSumamt() {
        return detItemFeeSumamt;
    }

    /**
     * 设置明细项目费 用总额
     *
     * @param detItemFeeSumamt 明细项目费 用总额
     */
    public void setDetItemFeeSumamt(String detItemFeeSumamt) {
        this.detItemFeeSumamt = detItemFeeSumamt;
    }

    /**
     * 获取数量
     *
     * @return CNT - 数量
     */
    public String getCnt() {
        return cnt;
    }

    /**
     * 设置数量
     *
     * @param cnt 数量
     */
    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    /**
     * 获取单价
     *
     * @return PRIC - 单价
     */
    public String getPric() {
        return pric;
    }

    /**
     * 设置单价
     *
     * @param pric 单价
     */
    public void setPric(String pric) {
        this.pric = pric;
    }

    /**
     * 获取单次剂量描 述
     *
     * @return SIN_DOS_DSCR - 单次剂量描 述
     */
    public String getSinDosDscr() {
        return sinDosDscr;
    }

    /**
     * 设置单次剂量描 述
     *
     * @param sinDosDscr 单次剂量描 述
     */
    public void setSinDosDscr(String sinDosDscr) {
        this.sinDosDscr = sinDosDscr;
    }

    /**
     * 获取使用频次描 述
     *
     * @return USED_FRQU_DSCR - 使用频次描 述
     */
    public String getUsedFrquDscr() {
        return usedFrquDscr;
    }

    /**
     * 设置使用频次描 述
     *
     * @param usedFrquDscr 使用频次描 述
     */
    public void setUsedFrquDscr(String usedFrquDscr) {
        this.usedFrquDscr = usedFrquDscr;
    }

    /**
     * 获取周期天数
     *
     * @return PRD_DAYS - 周期天数
     */
    public String getPrdDays() {
        return prdDays;
    }

    /**
     * 设置周期天数
     *
     * @param prdDays 周期天数
     */
    public void setPrdDays(String prdDays) {
        this.prdDays = prdDays;
    }

    /**
     * 获取 用药途径描 述
     *
     * @return MEDC_WAY_DSCR -  用药途径描 述
     */
    public String getMedcWayDscr() {
        return medcWayDscr;
    }

    /**
     * 设置 用药途径描 述
     *
     * @param medcWayDscr  用药途径描 述
     */
    public void setMedcWayDscr(String medcWayDscr) {
        this.medcWayDscr = medcWayDscr;
    }

    /**
     * 获取开单医生编 码
     *
     * @return BILG_DR_CODG - 开单医生编 码
     */
    public String getBilgDrCodg() {
        return bilgDrCodg;
    }

    /**
     * 设置开单医生编 码
     *
     * @param bilgDrCodg 开单医生编 码
     */
    public void setBilgDrCodg(String bilgDrCodg) {
        this.bilgDrCodg = bilgDrCodg;
    }

    /**
     * 获取开单医师姓 名
     *
     * @return BILG_DR_NAME - 开单医师姓 名
     */
    public String getBilgDrName() {
        return bilgDrName;
    }

    /**
     * 设置开单医师姓 名
     *
     * @param bilgDrName 开单医师姓 名
     */
    public void setBilgDrName(String bilgDrName) {
        this.bilgDrName = bilgDrName;
    }

    /**
     * 获取中药使用方 式
     *
     * @return TCMDRUG_USED_WAY - 中药使用方 式
     */
    public String getTcmdrugUsedWay() {
        return tcmdrugUsedWay;
    }

    /**
     * 设置中药使用方 式
     *
     * @param tcmdrugUsedWay 中药使用方 式
     */
    public void setTcmdrugUsedWay(String tcmdrugUsedWay) {
        this.tcmdrugUsedWay = tcmdrugUsedWay;
    }


}