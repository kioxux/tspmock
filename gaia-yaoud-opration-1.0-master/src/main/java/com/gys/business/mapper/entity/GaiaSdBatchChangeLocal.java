package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SD_BATCH_CHANGE_LOCAL")
public class GaiaSdBatchChangeLocal implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 店号
     */
    @Id
    @Column(name = "GSBC_BR_ID")
    private String gsbcBrId;

    /**
     * 异动业务单号
     */
    @Id
    @Column(name = "GSBC_VOUCHER_ID")
    private String gsbcVoucherId;

    /**
     * 异动日期
     */
    @Id
    @Column(name = "GSBC_DATE")
    private String gsbcDate;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSBC_SERIAL")
    private String gsbcSerial;

    /**
     * 商品
     */
    @Id
    @Column(name = "GSBC_PRO_ID")
    private String gsbcProId;

    /**
     * 批次
     */
    @Id
    @Column(name = "GSBC_BATCH")
    private String gsbcBatch;

    /**
     * 批号
     */
    @Column(name = "GSBC_BATCH_NO")
    private String gsbcBatchNo;

    /**
     * 数量
     */
    @Column(name = "GSBC_QTY")
    private BigDecimal gsbcQty;

    /**
     * 原库存数量
     */
    @Column(name = "GSBC_OLD_QTY")
    private BigDecimal gsbcOldQty;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取店号
     *
     * @return GSBC_BR_ID - 店号
     */
    public String getGsbcBrId() {
        return gsbcBrId;
    }

    /**
     * 设置店号
     *
     * @param gsbcBrId 店号
     */
    public void setGsbcBrId(String gsbcBrId) {
        this.gsbcBrId = gsbcBrId;
    }

    /**
     * 获取异动业务单号
     *
     * @return GSBC_VOUCHER_ID - 异动业务单号
     */
    public String getGsbcVoucherId() {
        return gsbcVoucherId;
    }

    /**
     * 设置异动业务单号
     *
     * @param gsbcVoucherId 异动业务单号
     */
    public void setGsbcVoucherId(String gsbcVoucherId) {
        this.gsbcVoucherId = gsbcVoucherId;
    }

    /**
     * 获取异动日期
     *
     * @return GSBC_DATE - 异动日期
     */
    public String getGsbcDate() {
        return gsbcDate;
    }

    /**
     * 设置异动日期
     *
     * @param gsbcDate 异动日期
     */
    public void setGsbcDate(String gsbcDate) {
        this.gsbcDate = gsbcDate;
    }

    /**
     * 获取行号
     *
     * @return GSBC_SERIAL - 行号
     */
    public String getGsbcSerial() {
        return gsbcSerial;
    }

    /**
     * 设置行号
     *
     * @param gsbcSerial 行号
     */
    public void setGsbcSerial(String gsbcSerial) {
        this.gsbcSerial = gsbcSerial;
    }

    /**
     * 获取商品
     *
     * @return GSBC_PRO_ID - 商品
     */
    public String getGsbcProId() {
        return gsbcProId;
    }

    /**
     * 设置商品
     *
     * @param gsbcProId 商品
     */
    public void setGsbcProId(String gsbcProId) {
        this.gsbcProId = gsbcProId;
    }

    /**
     * 获取批次
     *
     * @return GSBC_BATCH - 批次
     */
    public String getGsbcBatch() {
        return gsbcBatch;
    }

    /**
     * 设置批次
     *
     * @param gsbcBatch 批次
     */
    public void setGsbcBatch(String gsbcBatch) {
        this.gsbcBatch = gsbcBatch;
    }

    /**
     * 获取批号
     *
     * @return GSBC_BATCH_NO - 批号
     */
    public String getGsbcBatchNo() {
        return gsbcBatchNo;
    }

    /**
     * 设置批号
     *
     * @param gsbcBatchNo 批号
     */
    public void setGsbcBatchNo(String gsbcBatchNo) {
        this.gsbcBatchNo = gsbcBatchNo;
    }

    /**
     * 获取数量
     *
     * @return GSBC_QTY - 数量
     */
    public BigDecimal getGsbcQty() {
        return gsbcQty;
    }

    /**
     * 设置数量
     *
     * @param gsbcQty 数量
     */
    public void setGsbcQty(BigDecimal gsbcQty) {
        this.gsbcQty = gsbcQty;
    }

    /**
     * 获取原库存数量
     *
     * @return GSBC_OLD_QTY - 原库存数量
     */
    public BigDecimal getGsbcOldQty() {
        return gsbcOldQty;
    }

    /**
     * 设置原库存数量
     *
     * @param gsbcOldQty 原库存数量
     */
    public void setGsbcOldQty(BigDecimal gsbcOldQty) {
        this.gsbcOldQty = gsbcOldQty;
    }
}