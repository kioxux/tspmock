package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSaleDLocal;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdSaleDLocalMapper extends BaseMapper<GaiaSdSaleDLocal> {
    void insertOrUpdate(List<GaiaSdSaleDLocal> saleDList);
}