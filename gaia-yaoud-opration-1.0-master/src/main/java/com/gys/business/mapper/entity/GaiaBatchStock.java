//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_BATCH_STOCK"
)
public class GaiaBatchStock implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "BAT_PRO_CODE"
    )
    private String batProCode;
    @Id
    @Column(
            name = "BAT_SITE_CODE"
    )
    private String batSiteCode;
    @Id
    @Column(
            name = "BAT_LOCATION_CODE"
    )
    private String batLocationCode;
    @Id
    @Column(
            name = "BAT_BATCH"
    )
    private String batBatch;
    @Column(
            name = "BAT_NORMAL_QTY"
    )
    private BigDecimal batNormalQty;
    @Column(
            name = "BAT_BATCH_COST"
    )
    private BigDecimal batBatchCost;
    @Column(
            name = "BAT_LOCK_QTY"
    )
    private BigDecimal batLockQty;
    @Column(
            name = "BAT_MOV_PRICE"
    )
    private BigDecimal batMovPrice;
    @Column(
            name = "BAT_RATE_AMT"
    )
    private BigDecimal batRateAmt;
    @Column(
            name = "BAT_CREATE_DATE"
    )
    private String batCreateDate;
    @Column(
            name = "BAT_CREATE_TIME"
    )
    private String batCreateTime;
    @Column(
            name = "BAT_CREATE_USER"
    )
    private String batCreateUser;
    @Column(
            name = "BAT_CHANGE_DATE"
    )
    private String batChangeDate;
    @Column(
            name = "BAT_CHANGE_TIME"
    )
    private String batChangeTime;
    @Column(
            name = "BAT_CHANGE_USER"
    )
    private String batChangeUser;
    private static final long serialVersionUID = 1L;

    public GaiaBatchStock() {
    }

    public String getClient() {
        return this.client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBatProCode() {
        return this.batProCode;
    }

    public void setBatProCode(String batProCode) {
        this.batProCode = batProCode;
    }

    public String getBatSiteCode() {
        return this.batSiteCode;
    }

    public void setBatSiteCode(String batSiteCode) {
        this.batSiteCode = batSiteCode;
    }

    public String getBatLocationCode() {
        return this.batLocationCode;
    }

    public void setBatLocationCode(String batLocationCode) {
        this.batLocationCode = batLocationCode;
    }

    public String getBatBatch() {
        return this.batBatch;
    }

    public void setBatBatch(String batBatch) {
        this.batBatch = batBatch;
    }

    public BigDecimal getBatNormalQty() {
        return this.batNormalQty;
    }

    public void setBatNormalQty(BigDecimal batNormalQty) {
        this.batNormalQty = batNormalQty;
    }

    public BigDecimal getBatBatchCost() {
        return this.batBatchCost;
    }

    public void setBatBatchCost(BigDecimal batBatchCost) {
        this.batBatchCost = batBatchCost;
    }

    public BigDecimal getBatLockQty() {
        return this.batLockQty;
    }

    public void setBatLockQty(BigDecimal batLockQty) {
        this.batLockQty = batLockQty;
    }

    public BigDecimal getBatMovPrice() {
        return this.batMovPrice;
    }

    public void setBatMovPrice(BigDecimal batMovPrice) {
        this.batMovPrice = batMovPrice;
    }

    public BigDecimal getBatRateAmt() {
        return this.batRateAmt;
    }

    public void setBatRateAmt(BigDecimal batRateAmt) {
        this.batRateAmt = batRateAmt;
    }

    public String getBatCreateDate() {
        return this.batCreateDate;
    }

    public void setBatCreateDate(String batCreateDate) {
        this.batCreateDate = batCreateDate;
    }

    public String getBatCreateTime() {
        return this.batCreateTime;
    }

    public void setBatCreateTime(String batCreateTime) {
        this.batCreateTime = batCreateTime;
    }

    public String getBatCreateUser() {
        return this.batCreateUser;
    }

    public void setBatCreateUser(String batCreateUser) {
        this.batCreateUser = batCreateUser;
    }

    public String getBatChangeDate() {
        return this.batChangeDate;
    }

    public void setBatChangeDate(String batChangeDate) {
        this.batChangeDate = batChangeDate;
    }

    public String getBatChangeTime() {
        return this.batChangeTime;
    }

    public void setBatChangeTime(String batChangeTime) {
        this.batChangeTime = batChangeTime;
    }

    public String getBatChangeUser() {
        return this.batChangeUser;
    }

    public void setBatChangeUser(String batChangeUser) {
        this.batChangeUser = batChangeUser;
    }
}
