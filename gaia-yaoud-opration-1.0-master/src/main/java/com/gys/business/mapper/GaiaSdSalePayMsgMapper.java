package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSalePayMsg;
import com.gys.business.service.data.DailySettlement.DailyReconcileDetailOutData;
import com.gys.business.service.data.GaiaRetailPriceInData;
import com.gys.business.service.data.HandoverReportPayData;
import com.gys.business.service.data.PaymentInformationOutData;
import com.gys.business.service.data.SalesInquireData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdSalePayMsgMapper extends BaseMapper<GaiaSdSalePayMsg> {

    /**
     * 获取当前订单的支付信息
     * @param inData
     * @return
     */
    List<PaymentInformationOutData> getPayDetail(SalesInquireData inData);

    /**
     * 根据主键查询 信息表
     * @param billNo
     * @param client
     * @param brId
     * @param gsshDate
     * @return
     */
    List<GaiaSdSalePayMsg> selectByBillNo(@Param("billNo") String billNo,@Param("client") String client,@Param("brId") String brId,@Param("gsshDate") String gsshDate);

    /**
     * 批量添加
     * @param list
     */
    void insertLists(List<GaiaSdSalePayMsg> list);

    /**
     * 订单支付查询
     * @param inData
     * @return
     */
    List<HandoverReportPayData> handoverReport(GaiaRetailPriceInData inData);

    /**
     * 储值卡 信息 查询
     * @param inData
     * @return
     */
    List<HandoverReportPayData> handoverReportByStore(GaiaRetailPriceInData inData);

    /**
     * 根据 订单号 汇总金额
     * @param map
     * @return
     */
    List<DailyReconcileDetailOutData> getAllBiNo(Map<String, Object> map);

    void updateList(@Param("client") String client,@Param("brId") String depId,@Param("list") List<String> billNo);
}
