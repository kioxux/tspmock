package com.gys.business.mapper.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 应收方式维护表(GaiaFiArPayment)实体类
 *
 * @author makejava
 * @since 2021-09-18 15:29:32
 */
public class GaiaFiArPayment implements Serializable {
    private static final long serialVersionUID = 620469142886508967L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 应收方式编码
     */
    private String arMethodId;
    /**
     * 应收方式名称
     */
    private String arMethodName;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getArMethodId() {
        return arMethodId;
    }

    public void setArMethodId(String arMethodId) {
        this.arMethodId = arMethodId;
    }

    public String getArMethodName() {
        return arMethodName;
    }

    public void setArMethodName(String arMethodName) {
        this.arMethodName = arMethodName;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}
