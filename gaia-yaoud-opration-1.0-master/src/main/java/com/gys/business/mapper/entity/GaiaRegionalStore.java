package com.gys.business.mapper.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:36 2021/10/14
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
@Table(name = "GAIA_REGIONAL_STORE")
@AllArgsConstructor
@NoArgsConstructor
public class GaiaRegionalStore implements Serializable {
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 区域编码
     */
    @Column(name = "REGIONAL_ID")
    private Long regionalId;

    /**
     * 门店code
     */
    @Column(name = "STO_CODE")
    private String stoCode;

    /**
     * 门店名称
     */
    @Column(name = "STO_NAME")
    private String stoName;

    /**
     * 排序编码
     */
    @Column(name = "SORT_NUM")
    private Integer sortNum;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    private Date createTime;

    /**
     * 更新人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 更新时间
     */
    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    /**
     * 是否删除 0：否、1：是
     */
    @Column(name = "DELETE_FLAG")
    private String deleteFlag;

    public static GaiaRegionalStore getDefaultRegionalStore(String client, Long regionalId, String stoCode, String stoName, Integer sortNum) {
        return new GaiaRegionalStore(null, client, regionalId, stoCode, stoName, sortNum, "system", new Date(), null, null, "0");
    }
}