package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdServerRelevancy;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdServerRelevancyMapper extends BaseMapper<GaiaSdServerRelevancy> {
    List<GaiaSdServerRelevancy> getAllByCompClass(GaiaSdServerRelevancy relevancy);
}
