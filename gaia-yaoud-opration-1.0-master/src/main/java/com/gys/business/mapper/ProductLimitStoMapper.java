package com.gys.business.mapper;

import com.gys.business.mapper.entity.ProductLimitSto;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProductLimitStoMapper extends BaseMapper<ProductLimitSto> {

    List<ProductLimitSto> getAllByClientAndBrId(@Param("client") String client, @Param("brId") String depId);
}
