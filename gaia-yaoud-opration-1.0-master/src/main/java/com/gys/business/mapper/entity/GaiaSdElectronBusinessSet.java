package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**

 * @author xiayuan
 * 电子券业务基础设置表
 */
@Data
public class GaiaSdElectronBusinessSet implements Serializable {

    private static final long serialVersionUID = -5034358050502923565L;

    private String gsebId;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 主题单号
     */
    private String gsetsId;

    /**
     * 业务类型   0为送券，1为用券
     */
    private String gsebsType;

    /**
     * 业务单号
     */
    private String gsebsId;

    /**
     * 门店编码
     */
    private String gsebsBrId;
    /**
     * 起始日期
     */
    private String gsebsBeginDate;

    /**
     * 结束日期
     */
    private String gsebsEndDate;

    /**
     * 起始时间
     */
    private String gsebsBeginTime;

    /**
     * 结束时间
     */
    private String gsebsEndTime;

    /**
     * 是否审核   N为否，Y为是 , D 为停用
     */
    private String gsebsStatus;

    /**
     * 是否停用   N为否，Y为是
     */
    private String gsebsFlag;

    /**
     * 促销商品是否参与
     */
    private String gsebsProm;

    /**
     * 创建日期
     */
    private String gsebsCreateDate;

    /**
     * 创建时间
     */
    private String gsebsCreateTime;

    /**
     * 最大赠送次数
     */
    private String gsebsMaxQty;

    /**
     * 已赠送次数
     */
    private String gsebsQty;

    /**
     * 备注
     */
    private String gsebsRemark;

    /**
     * N为否，Y为是  是否有效
     */
    private String gsebsValid;
    /**
     * NULL/0为按固定日期范围生效（现有逻辑），为1则按券有效天数生效
     */
    private String gsebsRule;
    /**
     *循环日期 按/分割
     */
    private String gsebsMonth;
    /**
     *循环星期 按/分割
     */
    private String gsebsWeek;
    /**
     *发券后有效天数
     */
    private String gsebsValidDay;

    /**
     * 电子券业务条件表
     */
    private List<GaiaSdElectronConditionSet> conditionSetList;

}