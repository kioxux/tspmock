package com.gys.business.mapper;

import com.gys.business.service.data.GaiaSdOutOfStockPushRecord;
import com.gys.business.service.data.PushStoreHistoryData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdOutOfStockPushRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdOutOfStockPushRecord record);

    int insertSelective(GaiaSdOutOfStockPushRecord record);

    GaiaSdOutOfStockPushRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdOutOfStockPushRecord record);

    int updateByPrimaryKey(GaiaSdOutOfStockPushRecord record);

    int updateBatch(List<GaiaSdOutOfStockPushRecord> list);

    int batchInsert(@Param("list") List<GaiaSdOutOfStockPushRecord> list);

    List<PushStoreHistoryData> getPushStoreHistory(@Param("client") String client, @Param("weekOrMonthType") String weekOrMonthType);

    List<PushStoreHistoryData> getPushWmsHistory(@Param("client") String client, @Param("weekOrMonthType") String weekOrMonthType);

    List<GaiaSdOutOfStockPushRecord> getListByCondition(GaiaSdOutOfStockPushRecord record);

    GaiaSdOutOfStockPushRecord getLatestRecord(GaiaSdOutOfStockPushRecord cond);

}