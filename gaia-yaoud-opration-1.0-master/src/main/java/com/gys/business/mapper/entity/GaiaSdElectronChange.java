package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**

 * @author xiayuan
 * 电子券异动表
 */
@Data
@Table(
        name = "GAIA_SD_ELECTRON_CHANGE"
)
public class GaiaSdElectronChange implements Serializable {

    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 会员卡号
     */
    @Id
    @Column(name = "GSEC_MEMBER_ID")
    private String gsecMemberId;

    /**
     * 电子券号
     */
    @Id
    @Column(name = "GSEC_ID")
    private String gsecId;

    /**
     * 电子券活动号
     */
    @Id
    @Column(name = "GSEB_ID")
    private String gsebId;

    /**
     * 是否使用  N为否，Y为是
     */
    @Id
    @Column(name = "GSEC_STATUS")
    private String gsecStatus;

    /**
     * 电子券金额
     */
    @Column(name = "GSEC_AMT")
    private BigDecimal gsecAmt;
    /**
     * 电子券描述
     */
    @Column(name = "GSEB_NAME")
    private String gsebName;

    /**
     * 途径  0为线下，1为线上
     */
    @Column(name = "GSEC_FLAG")
    private String gsecFlag;

    /**
     * 销售单号
     */
    @Column(name = "GSEC_BILL_NO")
    private String gsecBillNo;

    /**
     * 销售门店
     */
    @Column(name = "GSEC_BR_ID")
    private String gsecBrId;

    /**
     * 销售日期
     */
    @Column(name = "GSEC_SALE_DATE")
    private String gsecSaleDate;

    /**
     * 创建日期
     */
    @Column(name = "GSEC_CREATE_DATE")
    private String gsecCreateDate;

    /**
     * 失效日期
     */
    @Column(name = "GSEC_FAIL_DATE")
    private String gsecFailDate;

    /**
     * 送券销售单号
     */
    @Column(name = "GSEC_GIVE_BILL_NO")
    private String gsecGiveBillNo;

    /**
     * 用券截止日期
     */
    @Column(name = "GSEC_USE_EXPIRATION_DATE")
    private String gsecUseExpirationDate;

    /**
     * 送券门店
     */
    @Column(name = "GSEC_GIVE_BR_ID")
    private String gsecGiveBrId;

    /**
     * 送券日期
     */
    @Column(name = "GSEC_GIVE_SALE_DATE")
    private Date gsecGiveSaleDate;

    //主题号 不插入数据库
    private String gsetsId;

    //业务单号 不插入数据库
    private String gsebsId;

    private String brId;

    private String stoName;
    /**
     * 退货单号
     */
    private String reBillNo;

    private String gsecsFlag2;

}