package com.gys.business.mapper;

import com.gys.business.mapper.entity.TichengProplanStoN;
import com.gys.business.service.data.percentageplan.PercentageStoInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 单品提成方案门店表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-11-15
 */
@Mapper
public interface TichengProplanStoNMapper extends BaseMapper<TichengProplanStoN> {
    void deleteProStoreByPid(@Param("pid") Integer pid);

    void addPlanProStores(List<PercentageStoInData> list);

    List<Map<String,String>> selectProStoList(@Param("tichengId") Long planId);
}
