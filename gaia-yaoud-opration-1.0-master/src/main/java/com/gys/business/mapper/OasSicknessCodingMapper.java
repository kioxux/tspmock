package com.gys.business.mapper;

import com.gys.business.mapper.entity.OasSicknessCoding;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 疾病中类表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-09-02
 */
@Mapper
public interface OasSicknessCodingMapper extends BaseMapper<OasSicknessCoding> {
}
