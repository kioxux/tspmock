//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdStoresGroup;
import com.gys.business.mapper.entity.GaiaStoreCategoryAttr;
import com.gys.business.mapper.entity.GaiaStoreCategoryType;
import com.gys.business.mapper.entity.dto.GaiaStoreCategoryDropDownDTO;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdStoresGroupMapper extends BaseMapper<GaiaSdStoresGroup> {
    List<GetShopSortOutData> querySort(GetShopInData inData);

    List<GetShopDetailOutData> queryDetail(GetShopInData inData);

    void addAll(List<GaiaSdStoresGroup> dataList);

    void deleteData(GetShopInData inData);

    void updateDetail(GetShopDetailOutData sdStoresGroup);

    List<StoresGroupOutData> listAllStoresGroupByClientId(StoresGroupInData param);

    List<StoresGroupOutData> listAllStoresInfoByClientId(StoresGroupInData param);

    void addAllList(List<GaiaSdStoresGroup> groupList);

    String getStoreTypeName(Map<String, String> params);

    GaiaSdStoresGroup getByClientAndStoreAndType(@Param("client")String client, @Param("store")String store, @Param("type")String type);

    List<String> getListByClientAndType(@Param("client") String client, @Param("type") String type);

    /**
     * 根据加盟商id获取 门店属性，dtp, 税分类，是否医保
     *
     * @param clientId 加盟商id
     * @return List<GaiaStoreCategoryAttr>
     */
    List<GaiaStoreCategoryAttr> selectStoreCategory(String clientId);

    /**
     * 根据加盟商id获取配置的分类类型和对应分类
     *
     * @param clientId 加盟商id
     * @return List<GaiaStoreCategoryType>
     */
    List<GaiaStoreCategoryType> selectStoreCategoryTypeWithConfig(String clientId);

    /**
     * 查询加盟商内所有门店的分类类型
     * @param clientId 加盟商id
     * @return List<GaiaStoreCategoryType>
     */
    List<GaiaStoreCategoryType> selectStoreCategoryByClient(String clientId);

    /**
     * 根据条件获取门店分类下拉列表
     *
     * @param gaiaStoreCategoryDropDownDTO gaiaStoreCategoryDropDownDTO
     * @return List<GaiaStoreCategoryType>
     */
    List<GaiaStoreCategoryType> selectStoreCategoryDropDown(GaiaStoreCategoryDropDownDTO gaiaStoreCategoryDropDownDTO);


    GaiaSdStoresGroup getByStoreAndType(@Param("client")String client, @Param("store")String store, @Param("type")String type);

    List<GaiaSdStoresGroup> getAllByClient(@Param("client") String client);
}
