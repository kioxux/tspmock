package com.gys.business.mapper;

import com.gys.business.mapper.entity.PayDayReportDetailOutData;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GaiaSdPayDayreportDMapper {
    List<PayDayReportDetailOutData> queryPayDayReportDetail(@Param("client")String client, @Param("voucherId")String voucherId);

    /**
     *
     * @param client 加盟商
     * @param voucherId 单号
     * @param stoCode 门店号
     * @param gspddSalePaymethodId 支付方式
     * @param gspdhtotalInputAmtChange 对账金额汇总 修改后
     */
    void updatePayTypeValue(@Param("client") String client, @Param("voucherId") String voucherId, @Param("stoCode") String stoCode, @Param("gspddSalePaymethodId") String gspddSalePaymethodId, @Param("gspdhtotalInputAmtChange") BigDecimal gspdhtotalInputAmtChange);
}
