package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 月度库存表
 *
 * </p>
 *
 * @author flynn
 * @since 2021-07-13
 */
@Data
@Table(
        name = "GAIA_STOCK_DIFF"
)
public class StockDiff implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "比对任务号")
    @Column(name = "GSC_TASK_NO")
    private String taskNo;

    @ApiModelProperty(value = "比对任务行号")
    @Column(name = "GSC_TASK_ITEM")
    private String taskItem;

    @ApiModelProperty(value = "比对任务行号")
    @Column(name = "GSD_DETAIL_ITEM")
    private String detailItem;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "商品编码")
    @Column(name = "GSD_PRO_CODE")
    private String gsdProCode;

    @ApiModelProperty(value = "地点（门店/仓库）")
    @Column(name = "GSC_SITE_CODE")
    private String gscSiteCode;

    @ApiModelProperty(value = "批次")
    @Column(name = "GSD_BATCH")
    private String gsdBatch;

    @ApiModelProperty(value = "实时数量")
    @Column(name = "GSD_STOCK_QTY")
    private BigDecimal gsdStockQty;

    @ApiModelProperty(value = "推算数量")
    @Column(name = "GSD_COUNT_QTY")
    private BigDecimal gsdCountQty;

    @ApiModelProperty(value = "差异数量")
    @Column(name = "GSD_DIFF_QTY")
    private BigDecimal gsdDiffQty;

    @ApiModelProperty(value = "实时金额")
    @Column(name = "GSD_STOCK_AMT")
    private BigDecimal gsdStockAmt;

    @ApiModelProperty(value = "推算金额")
    @Column(name = "GSD_COUNT_AMT")
    private BigDecimal gsdCountAmt;

    @ApiModelProperty(value = "差异金额")
    @Column(name = "GSD_DIFF_AMT")
    private BigDecimal gsdDiffAmt;

    @ApiModelProperty(value = "实时税金")
    @Column(name = "GSD_TAX_AMT")
    private BigDecimal gsdTaxAmt;

    @ApiModelProperty(value = "推算税金")
    @Column(name = "GSD_COUNT_TAX")
    private BigDecimal gsdCountTax;

    @ApiModelProperty(value = "差异金额")
    @Column(name = "GSD_DIFF_TAX")
    private BigDecimal gsdDiffTax;


    @ApiModelProperty(value = "创建日期")
    @Column(name = "GSD_CREATE_DATE")
    private String gscCreateDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "GSD_CREATE_TIME")
    private String gscCreateTime;

    @ApiModelProperty(value = "创建人")
    @Column(name = "GSD_CREATE_BY")
    private String gscCreateBy;

    @ApiModelProperty(value = "最后更新时间")
    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;


}
