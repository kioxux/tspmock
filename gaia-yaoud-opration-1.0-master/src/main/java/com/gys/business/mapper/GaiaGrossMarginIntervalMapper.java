package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaGrossMarginInterval;
import com.gys.business.service.data.CustomerOutData;
import com.gys.business.service.data.GrossMarginInData;
import com.gys.business.service.data.GrossMarginOutData;
import com.gys.business.service.data.GrossMarginOutDataCSV;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaGrossMarginIntervalMapper extends BaseMapper<GaiaGrossMarginInterval> {

    List<CustomerOutData> selectCustomer();

    List<GrossMarginOutData> selectGrossMarginInterval(GrossMarginInData inData);

    List<GrossMarginOutDataCSV> selectGrossMarginIntervalCSV(GrossMarginInData inData);

    GrossMarginOutData selectUserName(@Param("client") String client,@Param("userId") String userId);

    void saveGrossMarginInterval(GaiaGrossMarginInterval interval);

    GaiaGrossMarginInterval findGrossMargin(GrossMarginOutData outData);

    List<GrossMarginOutData> selectClient();

    void insertGrossMarginData(GaiaGrossMarginInterval interval);

    List<GaiaGrossMarginInterval> getByClients(@Param("clients") List<String> clients);
}