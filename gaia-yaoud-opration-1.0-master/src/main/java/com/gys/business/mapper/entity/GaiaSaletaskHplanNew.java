package com.gys.business.mapper.entity;

import lombok.Data;
import org.apache.poi.hpsf.Decimal;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SALETASK_HPLAN_NEW")
@Data
public class GaiaSaletaskHplanNew implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;
    /**
     * 计划月份
     */
    @Id
    @Column(name = "MONTH_PLAN")
    private String monthPlan;

    /**
     * 任务ID
     */
    @Id
    @Column(name = "STASK_TASK_ID")
    private String staskTaskId;

    /**
     * 任务名称
     */
    @Column(name = "STASK_TASK_NAME")
    private String staskTaskName;

    /**
     * 起始日期
     */
    @Column(name = "STASK_START_DATE")
    private String staskStartDate;

    /**
     * 结束日期
     */
    @Column(name = "STASK_END_DATE")
    private String staskEndDate;

    /**
     * 同比月份
     */
    @Column(name = "YOY_MONTH")
    private String yoyMonth;

    /**
     * 环比月份
     */
    @Column(name = "MOM_MONTH")
    private String momMonth;

    /**
     * 期间天数
     */
    @Column(name = "MONTH_DAYS")
    private Integer monthDays;

    /**
     * 营业额同比增长率
     */
    @Column(name = "YOY_SALE_AMT")
    private BigDecimal yoySaleAmt;

    /**
     * 营业额环比增长率
     */
    @Column(name = "MOM_SALE_AMT")
    private BigDecimal momSaleAmt;

    /**
     * 毛利额同比增长率
     */
    @Column(name = "YOY_SALE_GROSS")
    private BigDecimal yoySaleGross;

    /**
     * 毛利额环比增长率
     */
    @Column(name = "MOM_SALE_GROSS")
    private BigDecimal momSaleGross;

    /**
     * 会员卡同比增长率
     */
    @Column(name = "YOY_MCARD_QTY")
    private BigDecimal yoyMcardQty;

    /**
     * 会员卡环比增长率
     */
    @Column(name = "MOM_MCARD_QTY")
    private BigDecimal momMcardQty;

    /**
     * 任务状态(S已保存，P已下发)
     */
    @Column(name = "STASK_SPLAN_STA")
    private String staskSplanSta;

    /**
     * 创建人账号
     */
    @Column(name = "STASK_CRE_ID")
    private String staskCreId;

    /**
     * 创建日期
     */
    @Column(name = "STASK_CRE_DATE")
    private String staskCreDate;

    /**
     * 创建时间
     */
    @Column(name = "STASK_CRE_TIME")
    private String staskCreTime;

    /**
     * 修改人
     */
    @Column(name = "STASK_MD_ID")
    private String staskMdId;

    /**
     * 修改日期
     */
    @Column(name = "STASK_MD_DATE")
    private String staskMdDate;

    /**
     * 修改时间
     */
    @Column(name = "STASK_MD_TIME")
    private String staskMdTime;

    /**
     * 删除 0 未删除 1已删除
     */
    @Column(name = "STASK_TYPE")
    private String staskType;


    private static final long serialVersionUID = 1L;
}