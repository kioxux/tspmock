package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 商品价格表
 */
@Table( name = "GAIA_SD_PRODUCT_PRICE")
@Data
public class GaiaSdProductPrice implements Serializable {

    private static final long serialVersionUID = -3307722103701980008L;
    /**
     * 加盟号
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 门店编码
     */
    @Id
    @Column(name = "GSPP_BR_ID")
    private String gsppBrId;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSPP_PRO_ID")
    private String gsppProId;

    /**
     * 零售价
     */
    @Column(name = "GSPP_PRICE_NORMAL")
    private BigDecimal gsppPriceNormal;

    /**
     * 会员价
     */
    @Column(name = "GSPP_PRICE_HY")
    private BigDecimal gsppPriceHy;

    /**
     * 医保价
     */
    @Column(name = "GSPP_PRICE_YB")
    private BigDecimal gsppPriceYb;

    /**
     * 拆零价
     */
    @Column(name = "GSPP_PRICE_CL")
    private BigDecimal gsppPriceCl;

    /**
     * 会员日价
     */
    @Column(name = "GSPP_PRICE_HYR")
    private BigDecimal gsppPriceHyr;

    /**
     * 网上零售价
     */
    @Column(name = "GSPP_PRICE_ONLINE_NORMAL")
    private BigDecimal gsppPriceOnlineNormal;

    /**
     * 网上会员价
     */
    @Column(name = "GSPP_PRICE_ONLINE_HY")
    private BigDecimal gsppPriceOnlineHy;
}
