package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaUserTrainingRecord;
import com.gys.business.service.data.UserTrainingRecordInData;
import com.gys.business.service.data.UserTrainingRecordOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaUserTrainingRecordMapper extends BaseMapper<GaiaUserTrainingRecord> {
    void insertUserTrainingRecord(GaiaUserTrainingRecord record);

    String getNextGutrVoucherId(@Param("client") String client);

    Long getIdByClientAndGutrVoucherId(@Param("client") String client, @Param("gutrVoucherId") String gutrVoucherId);

    List<UserTrainingRecordOutData> getUserTrainingRecordList(UserTrainingRecordInData inData);

    void deleteUserTrainingRecord(@Param("id") Long id);

    void updateUserTrainingRecord(GaiaUserTrainingRecord record);
}