package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaCommodityInventoryH;
import com.gys.business.service.data.GaiaCommodityInventoryDOut;
import com.gys.business.service.data.GaiaCommodityInventoryHInData;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;
import java.util.List;

/**
 * 商品调库-主表(GaiaCommodityInventoryH)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-19 16:00:45
 */
public interface GaiaCommodityInventoryHMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaCommodityInventoryH queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaCommodityInventoryH 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaCommodityInventoryH> queryAllByLimit(GaiaCommodityInventoryH gaiaCommodityInventoryH, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaCommodityInventoryH 查询条件
     * @return 总行数
     */
    long count(GaiaCommodityInventoryH gaiaCommodityInventoryH);

    /**
     * 新增数据
     *
     * @param gaiaCommodityInventoryH 实例对象
     * @return 影响行数
     */
    int insert(GaiaCommodityInventoryH gaiaCommodityInventoryH);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaCommodityInventoryH> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaCommodityInventoryH> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaCommodityInventoryH> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaCommodityInventoryH> entities);

    /**
     * 修改数据
     *
     * @param gaiaCommodityInventoryH 实例对象
     * @return 影响行数
     */
    int update(GaiaCommodityInventoryH gaiaCommodityInventoryH);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    List<HashMap<String, String>> listClient();


    String getBillCode(@Param("client") String client);

    List<GaiaCommodityInventoryH> listBill(GaiaCommodityInventoryHInData param);

    List<GaiaCommodityInventoryDOut> listBillDetail(GaiaCommodityInventoryHInData param);

    List<HashMap<String,String>> listUnSendStore(GaiaCommodityInventoryHInData param);

    List<String> listStoreLine(GaiaCommodityInventoryHInData param);

    GaiaCommodityInventoryDOut getTotal(GaiaCommodityInventoryHInData param);

    GaiaCommodityInventoryH getByBillNo(@Param("client") String client,@Param("billNo") String billNo);

    void updateStatus(GaiaCommodityInventoryH gaiaCommodityInventoryH);

    List<GaiaCommodityInventoryH> listUnSendBill();


    List<GaiaCommodityInventoryDOut> listSalesInfo(@Param("client") String client, @Param("stoCode") String stoCode, @Param("param2") int param2);

    List<GaiaCommodityInventoryDOut> listWaitGoods(@Param("client") String client, @Param("stoCode") String stoCode);

    void updateExportStatus(@Param("client") String client,@Param("billCode") String billCode,@Param("isExport") String isExport);
}

