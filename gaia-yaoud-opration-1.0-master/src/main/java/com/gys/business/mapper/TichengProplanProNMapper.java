package com.gys.business.mapper;

import com.gys.business.mapper.entity.TichengProplanProN;
import com.gys.business.service.data.percentageplan.PercentageProInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 提成方案单品提成明细表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-11-15
 */
@Mapper
public interface TichengProplanProNMapper extends BaseMapper<TichengProplanProN> {

    void deleteProPlanByPid(@Param("pid") Integer tichengId, @Param("settingId") Integer settingId);

    void deleteProPlanBySettingId(@Param("settingId") Integer settingId);

    void batchProTichengList(List<PercentageProInData> list);

    /**
     * 更新 settingid
     *
     * @param planId                       planId
     * @param planRejectDiscountRateSymbol planRejectDiscountRateSymbol
     * @param planRejectDiscountRate       planRejectDiscountRate
     * @param settingId                    settingId
     * @return int
     */
    int updateSettingIdByPlanId(@Param("planId") Long planId,
                                @Param("planRejectDiscountRateSymbol") String planRejectDiscountRateSymbol,
                                @Param("planRejectDiscountRate") String planRejectDiscountRate,
                                @Param("settingId") Long settingId);

}
