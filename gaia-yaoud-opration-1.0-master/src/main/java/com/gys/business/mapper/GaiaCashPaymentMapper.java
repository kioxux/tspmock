//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdCashPayment;
import com.gys.business.service.data.CashPaymentOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface GaiaCashPaymentMapper extends BaseMapper<GaiaSdCashPayment> {
    List<CashPaymentOutData> getCashPaymentList(@Param("params") Map<String, Object> params);

    String selectNextVoucherId(@Param("clientId") String clientId);
}
