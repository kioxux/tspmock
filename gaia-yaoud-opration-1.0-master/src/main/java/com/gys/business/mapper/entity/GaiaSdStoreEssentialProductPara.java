package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-gys-business-mapper-entity-GaiaSdStoreEssentialProductPara")
@Data
public class GaiaSdStoreEssentialProductPara {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 药品综合排名位数
    */
    @ApiModelProperty(value="药品综合排名位数")
    private Integer comprehensiveRank;

    /**
    * 门店库存品项合格品项数
    */
    @ApiModelProperty(value="门店库存品项合格品项数")
    private Integer stockQualifiedCount;

    /**
    * 步进值
    */
    @ApiModelProperty(value="步进值")
    private Integer stepValue;

    /**
    * 店效级别 0-无 1-超高 2-高 3-中 4-低
    */
    @ApiModelProperty(value="店效级别 0-无 1-超高 2-高 3-中 4-低")
    private Integer storeLevel;

}