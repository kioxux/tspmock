package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdWtpro;
import com.gys.business.service.data.DsfPordWtproInData;
import com.gys.business.service.data.DsfPordWtproOutData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DsfPordWtproMapper {

    List<DsfPordWtproOutData> selectDsfPordWtproList(DsfPordWtproInData inData);

    int saveSdWtsto(GaiaSdWtpro GaiaSdWtpro);

    int updateSdWtsto(GaiaSdWtpro GaiaSdWtpro);

    GaiaSdWtpro selectSdWtstoById(Long id);

    List<DsfPordWtproOutData> selectDsfPordWtpsdList(DsfPordWtproInData inData);

    long countDsfPordWtpsdList(String client);
}
