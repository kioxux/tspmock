//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaBankInfo;
import com.gys.business.service.data.BankInfoInData;
import com.gys.business.service.data.BankInfoOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaBankInfoMapper extends BaseMapper<GaiaBankInfo> {
    List<BankInfoOutData> getBankInfoList(BankInfoInData inData);

    List<String> getBankNames(BankInfoInData inData);

    List<String> getAllBankIdByName(BankInfoInData inData);

    List<String> getAllBankAccountByName(BankInfoInData inData);

    List<String> getBankIdByBankAccount(BankInfoInData inData);

    String selectNextGsbsBankId();
}
