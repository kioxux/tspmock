package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 
 * 
 */
@Data
public class GaiaProductCompare implements Serializable {

    /**
     * 主键
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 商品编码
     */
    private String gpcProCode;

    /**
     * 旧编码
     */
    private String gpcOldCode;

}