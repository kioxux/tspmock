package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_NEW_PRODUCT_EVALUATION_H")
public class GaiaNewProductEvaluationH implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店ID
     */
    @Column(name = "STORE_ID")
    private String storeId;

    /**
     * 商品评估单号
     */
    @Column(name = "BILL_CODE")
    private String billCode;

    /**
     * 单据日期
     */
    @Column(name = "BILL_DATE")
    private Date billDate;

    /**
     * 单据状态：0-待确认 1-已确认
     */
    @Column(name = "STATUS")
    private Integer status;

    /**
     * 是否删除：0-正常 1-删除
     */
    @Column(name = "IS_DELETE")
    private Integer isDelete;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    private Date createTime;

    /**
     * 创建者
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 更新者
     */
    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    /**
     * 更新时间
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取门店ID
     *
     * @return STORE_ID - 门店ID
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * 设置门店ID
     *
     * @param storeId 门店ID
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     * 获取商品评估单号
     *
     * @return BILL_CODE - 商品评估单号
     */
    public String getBillCode() {
        return billCode;
    }

    /**
     * 设置商品评估单号
     *
     * @param billCode 商品评估单号
     */
    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    /**
     * 获取单据日期
     *
     * @return BILL_DATE - 单据日期
     */
    public Date getBillDate() {
        return billDate;
    }

    /**
     * 设置单据日期
     *
     * @param billDate 单据日期
     */
    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    /**
     * 获取单据状态：0-待确认 1-已确认
     *
     * @return STATUS - 单据状态：0-待确认 1-已确认
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置单据状态：0-待确认 1-已确认
     *
     * @param status 单据状态：0-待确认 1-已确认
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取是否删除：0-正常 1-删除
     *
     * @return IS_DELETE - 是否删除：0-正常 1-删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置是否删除：0-正常 1-删除
     *
     * @param isDelete 是否删除：0-正常 1-删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_TIME - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建者
     *
     * @return CREATE_USER - 创建者
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建者
     *
     * @param createUser 创建者
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * 获取更新者
     *
     * @return UPDATE_TIME - 更新者
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新者
     *
     * @param updateTime 更新者
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新时间
     *
     * @return UPDATE_USER - 更新时间
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置更新时间
     *
     * @param updateUser 更新时间
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
}