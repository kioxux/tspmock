package com.gys.business.mapper;

import com.gys.business.bosen.form.InDataForm;
import com.gys.business.mapper.entity.GaiaSdWtbhdD;
import com.gys.business.service.data.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface YuanQuYaoJianMapper {
    List<LinkedHashMap<String, Object>> getSaleRecordData(@Param("clientList") List<String> clientList);

    List<LinkedHashMap<String, Object>> getSaleRecordHData(@Param("clientList") List<String> clientList);

    List<LinkedHashMap<String, Object>> getProductData(@Param("clientList") List<String> clientList);

    List<LinkedHashMap<String, Object>> getSupplierData(@Param("clientList") List<String> clientList);

    List<LinkedHashMap<String, Object>> getStockData(@Param("clientList") List<String> clientList);
    List<LinkedHashMap<String, Object>> getStockDetailData(@Param("clientList") List<String> clientList);

    List<LinkedHashMap<String, Object>> getStoreUserData(@Param("clientList") List<String> clientList);

    /***
     * 获取验收时间
     * @return
     */
    List<LinkedHashMap<String, Object>> getStoreQuaDateData(@Param("clientList") List<String> clientList);

    /***
     * 获取养护时间
     * @return
     */
    List<LinkedHashMap<String, Object>> getStoreCuringDateData(@Param("clientList") List<String> clientList);

    /***
     * 获取调控温度时间
     * @return
     */
    List<LinkedHashMap<String, Object>> getStoreTemperatureDateData(@Param("clientList") List<String> clientList);

    /***
     * 获取登录时间
     * @return
     */
    List<LinkedHashMap<String, Object>> getStoreLoginDateData(@Param("clientList") List<String> clientList);

    List<LinkedHashMap<String, Object>> getStoreTempHumiData(@Param("clientList") List<String> clientList);

    List<LinkedHashMap<String, Object>> getConserveInfoData(@Param("clientList") List<String> clientList);
    List<LinkedHashMap<String, Object>> getConserveDetailData(@Param("clientList") List<String> clientList);

}
