package com.gys.business.mapper.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Table(name = "GAIA_SD_INTEGRAL_RATE_STO")
public class GaiaSdIntegralRateSto {


    @Column(name ="ID")
    private Long id;

    @Column(name ="CLIENT")
    private String client;

    /**
     * 是否积分	0：否，1：是
     */
    @Column(name ="PRO_ID")
    private String proId;

    /**
     * 积分倍率
     */
    @Column(name ="BR_ID")
    private String brId;

}
