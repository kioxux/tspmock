//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_INTEGRAL_ADD_SET "
)
public class GaiaSdIntegralAddSet implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSIAS_BR_ID"
    )
    private String gsiasBrId;
    @Column(
            name = "GSIAS_BR_NAME"
    )
    private String gsiasBrName;
    @Column(
            name = "GSIAS_INTEGRAL_MIN_AMT"
    )
    private BigDecimal gsiasIntegralMinAmt;
    @Column(
            name = "GSIAS_INTEGRAL_MAX"
    )
    private String gsiasIntegralMax;
    @Column(
            name = "GSIAS_CHANGE_SET"
    )
    private String gsiasChangeSet;
    @Column(
            name = "GSIAS_AMT_PM1"
    )
    private BigDecimal gsiasAmtPm1;
    @Column(
            name = "GSIAS_INTEGRAL_PM1"
    )
    private String gsiasIntegralPm1;
    @Column(
            name = "GSIAS_AMT_PM2"
    )
    private BigDecimal gsiasAmtPm2;
    @Column(
            name = "GSIAS_INTEGRAL_PM2"
    )
    private String gsiasIntegralPm2;
    @Column(
            name = "GSIAS_AMT_PM3"
    )
    private BigDecimal gsiasAmtPm3;
    @Column(
            name = "GSIAS_INTEGRAL_PM3"
    )
    private String gsiasIntegralPm3;
    @Column(
            name = "GSIAS_AMT_PM4"
    )
    private BigDecimal gsiasAmtPm4;
    @Column(
            name = "GSIAS_INTEGRAL_PM4"
    )
    private String gsiasIntegralPm4;
    @Column(
            name = "GSIAS_AMT_PM5"
    )
    private BigDecimal gsiasAmtPm5;
    @Column(
            name = "GSIAS_INTEGRAL_PM5"
    )
    private String gsiasIntegralPm5;
    @Column(
            name = "GSIAS_AMT_PM6"
    )
    private BigDecimal gsiasAmtPm6;
    @Column(
            name = "GSIAS_INTEGRAL_PM6"
    )
    private String gsiasIntegralPm6;
    private static final long serialVersionUID = 1L;

    public GaiaSdIntegralAddSet() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsiasBrId() {
        return this.gsiasBrId;
    }

    public void setGsiasBrId(String gsiasBrId) {
        this.gsiasBrId = gsiasBrId;
    }

    public String getGsiasBrName() {
        return this.gsiasBrName;
    }

    public void setGsiasBrName(String gsiasBrName) {
        this.gsiasBrName = gsiasBrName;
    }

    public BigDecimal getGsiasIntegralMinAmt() {
        return this.gsiasIntegralMinAmt;
    }

    public void setGsiasIntegralMinAmt(BigDecimal gsiasIntegralMinAmt) {
        this.gsiasIntegralMinAmt = gsiasIntegralMinAmt;
    }

    public String getGsiasIntegralMax() {
        return this.gsiasIntegralMax;
    }

    public void setGsiasIntegralMax(String gsiasIntegralMax) {
        this.gsiasIntegralMax = gsiasIntegralMax;
    }

    public String getGsiasChangeSet() {
        return this.gsiasChangeSet;
    }

    public void setGsiasChangeSet(String gsiasChangeSet) {
        this.gsiasChangeSet = gsiasChangeSet;
    }

    public BigDecimal getGsiasAmtPm1() {
        return this.gsiasAmtPm1;
    }

    public void setGsiasAmtPm1(BigDecimal gsiasAmtPm1) {
        this.gsiasAmtPm1 = gsiasAmtPm1;
    }

    public String getGsiasIntegralPm1() {
        return this.gsiasIntegralPm1;
    }

    public void setGsiasIntegralPm1(String gsiasIntegralPm1) {
        this.gsiasIntegralPm1 = gsiasIntegralPm1;
    }

    public BigDecimal getGsiasAmtPm2() {
        return this.gsiasAmtPm2;
    }

    public void setGsiasAmtPm2(BigDecimal gsiasAmtPm2) {
        this.gsiasAmtPm2 = gsiasAmtPm2;
    }

    public String getGsiasIntegralPm2() {
        return this.gsiasIntegralPm2;
    }

    public void setGsiasIntegralPm2(String gsiasIntegralPm2) {
        this.gsiasIntegralPm2 = gsiasIntegralPm2;
    }

    public BigDecimal getGsiasAmtPm3() {
        return this.gsiasAmtPm3;
    }

    public void setGsiasAmtPm3(BigDecimal gsiasAmtPm3) {
        this.gsiasAmtPm3 = gsiasAmtPm3;
    }

    public String getGsiasIntegralPm3() {
        return this.gsiasIntegralPm3;
    }

    public void setGsiasIntegralPm3(String gsiasIntegralPm3) {
        this.gsiasIntegralPm3 = gsiasIntegralPm3;
    }

    public BigDecimal getGsiasAmtPm4() {
        return this.gsiasAmtPm4;
    }

    public void setGsiasAmtPm4(BigDecimal gsiasAmtPm4) {
        this.gsiasAmtPm4 = gsiasAmtPm4;
    }

    public String getGsiasIntegralPm4() {
        return this.gsiasIntegralPm4;
    }

    public void setGsiasIntegralPm4(String gsiasIntegralPm4) {
        this.gsiasIntegralPm4 = gsiasIntegralPm4;
    }

    public BigDecimal getGsiasAmtPm5() {
        return this.gsiasAmtPm5;
    }

    public void setGsiasAmtPm5(BigDecimal gsiasAmtPm5) {
        this.gsiasAmtPm5 = gsiasAmtPm5;
    }

    public String getGsiasIntegralPm5() {
        return this.gsiasIntegralPm5;
    }

    public void setGsiasIntegralPm5(String gsiasIntegralPm5) {
        this.gsiasIntegralPm5 = gsiasIntegralPm5;
    }

    public BigDecimal getGsiasAmtPm6() {
        return this.gsiasAmtPm6;
    }

    public void setGsiasAmtPm6(BigDecimal gsiasAmtPm6) {
        this.gsiasAmtPm6 = gsiasAmtPm6;
    }

    public String getGsiasIntegralPm6() {
        return this.gsiasIntegralPm6;
    }

    public void setGsiasIntegralPm6(String gsiasIntegralPm6) {
        this.gsiasIntegralPm6 = gsiasIntegralPm6;
    }
}
