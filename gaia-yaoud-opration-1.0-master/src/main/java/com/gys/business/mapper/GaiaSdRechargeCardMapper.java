package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRechargeCard;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdRechargeCardMapper extends BaseMapper<GaiaSdRechargeCard> {
    Integer getMaxId();

    String getMaxGsrcIdWithPrefix(@Param("client") String client, @Param("brId") String brId, @Param("prefix") String prefix, @Param("length") int length);

    String getMaxGsrcIdWithNoPrefix(@Param("client") String client, @Param("brId") String brId, @Param("length") int length);

    List<RechComsuOutData> getRechComsuList(RechComsuInData inData);

    void updateRechargeCardNo(GaiaSdRechargeCardChangeInData inData);

    List<RechargeCardOutData> selectCardList(RechargeCardInData inData);

    GaiaSdRechargeCard getAccountByCardId(@Param("client")String client, @Param("gsspmCardNo")String gsspmCardNo);

    GaiaSdRechargeCard getRechargeByCardId(@Param("client")String client, @Param("gsrcAccountId")String gsrcAccountId);

    void updateByCardId(GaiaSdRechargeCard card);

    List<GaiaSdRechargeCard> getAllByClient(@Param("client") String client);

    GaiaSdRechargeCard getAccountByMemberCardId(@Param("client") String client,@Param("gsmbcCardId") String gsmbcCardId);

    List<GaiaSdRechargeCard> getRechargeCardByMemberCardId(Map map);

    void updateByNewCardId(@Param("client") String client, @Param("oldMemberCardId") String oldMemberCardId, @Param("newMemberCardId") String newMemberCardId);

    List<GaiaSdRechargeCard> getEffectRechargeList(RechargeQueryInData inData);
}
