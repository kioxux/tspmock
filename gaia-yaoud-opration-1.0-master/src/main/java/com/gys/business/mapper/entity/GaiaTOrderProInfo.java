package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_T_ORDER_PRO_INFO")
public class GaiaTOrderProInfo implements Serializable {
    /**
     * 系统订单ID
     */
    @Id
    @Column(name = "ORDER_ID")
    private Integer orderId;

    @Column(name = "CLIENT")
    private String client;

    @Column(name = "STO_CODE")
    private String stoCode;

    /**
     * 商品编码
     */
    @Column(name = "MEDICINE_CODE")
    private String medicineCode;

    /**
     * 数量
     */
    @Column(name = "QUANTITY")
    private BigDecimal quantity;

    /**
     * 订单内药品行号
     */
    @Column(name = "SEQUENCE")
    private Integer sequence;

    /**
     * 包装数量
     */
    @Column(name = "BOX_NUM")
    private Integer boxNum;

    /**
     * 包装价格
     */
    @Column(name = "BOX_PRICE")
    private BigDecimal boxPrice;

    @Column(name = "UNIT")
    private String unit;

    /**
     * 折扣信息
     */
    @Column(name = "DISCOUNT")
    private String discount;

    @Column(name = "SHOP_NO")
    private Integer shopNo;

    @Column(name = "STOCK_LOCK")
    private String stockLock;

    @Column(name = "REFUND_QTY")
    private BigDecimal refundQty;

    @Column(name = "REFUND_PRICE")
    private BigDecimal refundPrice;

    /**
     * 单价
     */
    @Column(name = "PRICE")
    private BigDecimal price;

    private static final long serialVersionUID = 1L;

    /**
     * 获取系统订单ID
     *
     * @return ORDER_ID - 系统订单ID
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置系统订单ID
     *
     * @param orderId 系统订单ID
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * @return CLIENT
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * @return STO_CODE
     */
    public String getStoCode() {
        return stoCode;
    }

    /**
     * @param stoCode
     */
    public void setStoCode(String stoCode) {
        this.stoCode = stoCode;
    }

    /**
     * 获取商品编码
     *
     * @return MEDICINE_CODE - 商品编码
     */
    public String getMedicineCode() {
        return medicineCode;
    }

    /**
     * 设置商品编码
     *
     * @param medicineCode 商品编码
     */
    public void setMedicineCode(String medicineCode) {
        this.medicineCode = medicineCode;
    }

    /**
     * 获取数量
     *
     * @return QUANTITY - 数量
     */
    public BigDecimal getQuantity() {
        return quantity;
    }

    /**
     * 设置数量
     *
     * @param quantity 数量
     */
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    /**
     * 获取订单内药品行号
     *
     * @return SEQUENCE - 订单内药品行号
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * 设置订单内药品行号
     *
     * @param sequence 订单内药品行号
     */
    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    /**
     * 获取包装数量
     *
     * @return BOX_NUM - 包装数量
     */
    public Integer getBoxNum() {
        return boxNum;
    }

    /**
     * 设置包装数量
     *
     * @param boxNum 包装数量
     */
    public void setBoxNum(Integer boxNum) {
        this.boxNum = boxNum;
    }

    /**
     * 获取包装价格
     *
     * @return BOX_PRICE - 包装价格
     */
    public BigDecimal getBoxPrice() {
        return boxPrice;
    }

    /**
     * 设置包装价格
     *
     * @param boxPrice 包装价格
     */
    public void setBoxPrice(BigDecimal boxPrice) {
        this.boxPrice = boxPrice;
    }

    /**
     * @return UNIT
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * 获取折扣信息
     *
     * @return DISCOUNT - 折扣信息
     */
    public String getDiscount() {
        return discount;
    }

    /**
     * 设置折扣信息
     *
     * @param discount 折扣信息
     */
    public void setDiscount(String discount) {
        this.discount = discount;
    }

    /**
     * @return SHOP_NO
     */
    public Integer getShopNo() {
        return shopNo;
    }

    /**
     * @param shopNo
     */
    public void setShopNo(Integer shopNo) {
        this.shopNo = shopNo;
    }

    /**
     * @return STOCK_LOCK
     */
    public String getStockLock() {
        return stockLock;
    }

    /**
     * @param stockLock
     */
    public void setStockLock(String stockLock) {
        this.stockLock = stockLock;
    }

    /**
     * @return REFUND_QTY
     */
    public BigDecimal getRefundQty() {
        return refundQty;
    }

    /**
     * @param refundQty
     */
    public void setRefundQty(BigDecimal refundQty) {
        this.refundQty = refundQty;
    }

    /**
     * @return REFUND_PRICE
     */
    public BigDecimal getRefundPrice() {
        return refundPrice;
    }

    /**
     * @param refundPrice
     */
    public void setRefundPrice(BigDecimal refundPrice) {
        this.refundPrice = refundPrice;
    }

    /**
     * 获取单价
     *
     * @return PRICE - 单价
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 设置单价
     *
     * @param price 单价
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}