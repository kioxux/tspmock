package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaFranchisee;
import com.gys.business.service.data.FranchiseeOutData;
import com.gys.business.service.data.ProvCityInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaFranchiseeMapper extends BaseMapper<GaiaFranchisee> {
    Integer selectMaxId();

    List<GaiaFranchisee> getAllByClient(String client);
    List<GaiaFranchisee> getAllByName(String francName);
    List<FranchiseeOutData> getFranchisee();
    List<FranchiseeOutData> getFranchiseeByName(@Param("francName") String francName);

    List<String> getAllClientId();

    List<Map<String, String>> getClientListByProvCity(ProvCityInData inData);

    List<GaiaFranchisee> getAllClient();

}
