package com.gys.business.mapper.entity;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@CsvRow("门店折扣率")
public class GaiaStoreDiscountData extends BaseRowModel implements Serializable {

    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 门店编码
     */
    @ExcelProperty(index = 0)
    @CsvCell(title = "门店编码", index = 2, fieldNo = 1)
    private String stoCode;


    /**
     * 门店名称
     */
    @ExcelProperty(index = 1)
    @CsvCell(title = "门店名称", index = 3, fieldNo = 1)
    private String stoName;

    private List<String> cusSelfCodeList;

    @ApiModelProperty(value = "起始日期")
    private String startDate;

    @ApiModelProperty(value = "结束日期")
    private String endDate;


    @ApiModelProperty(value = "分类id")
    private String gssgId;

    @ApiModelProperty(hidden = true)
    private List<String> gssgIds;

    @ApiModelProperty(value = "分类类型")
    private String stoGssgType;

    @ApiModelProperty(hidden = true)
    private List<GaiaStoreCategoryType> stoGssgTypes;

    @ApiModelProperty(value = "门店属性")
    private String stoAttribute;

    @ApiModelProperty(hidden = true)
    private List<String> stoAttributes;

    @ApiModelProperty(value = "是否医保店")
    private String stoIfMedical;

    @ApiModelProperty(hidden = true)
    private List<String> stoIfMedicals;

    @ApiModelProperty(value = "纳税属性")
    private String stoTaxClass;

    @ApiModelProperty(hidden = true)
    private List<String> stoTaxClasss;

    @ApiModelProperty(value = "DTP")
    private String stoIfDtp;

    @ApiModelProperty(hidden = true)
    private List<String> stoIfDtps;

    @ApiModelProperty(value = "折扣")
    @ExcelProperty(index = 3)
    private String gsdStoRebate;

    @ApiModelProperty(value = "姓名id")
    private String gsdUserId;

    @ApiModelProperty(value = "姓名")
    private String gsdUserName;
    @ApiModelProperty(value = "姓名编码集合")
    private List<String> gsdUserList;

    /**
     * 折扣开始时间
     */
    private String gsdBeginDate;
    /**
     * 折扣结束时间
     */
    private String gsdEndDate;
    /**
     * 修改日期
     */
    private String gsdStoModiDate;
    /**
     * 修改时间
     */
    private String gsdStoModiTime;
    /**
     * 修改人账号
     */
    private String gsdStoModiId;

    /**
     * 分类名称
     */
    private String gssgName;
    @ExcelProperty(index = 2)
    @CsvCell(title = "折扣期间", index = 5, fieldNo = 1)
    private String discountPeriod;
    @CsvCell(title = "操作员", index = 7, fieldNo = 1)
    private String userNameByid;
    @CsvCell(title = "维护时间", index = 8, fieldNo = 1)
    private String modiDateByTime;
    @CsvCell(title = "门店分类", index = 4, fieldNo = 1)
    private String classification;

    private String shopType;

    private String storeEfficiencyLevel;

    private String directManaged;

    private String managementArea;
    @CsvCell(title = "折扣率", index = 6, fieldNo = 1)
    private String gsdStoRebates;
    /**
     * 折扣率
     */
    private BigDecimal gsdStoRebateBy;
}
