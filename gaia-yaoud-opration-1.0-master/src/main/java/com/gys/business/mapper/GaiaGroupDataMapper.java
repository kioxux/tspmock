package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaGroupData;
import com.gys.business.service.data.AuthGroupOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaGroupDataMapper extends BaseMapper<GaiaGroupData> {
   List<AuthGroupOutData> selectGroupByModule(@Param("module") String module, @Param("client") String client);

   List<AuthGroupOutData> selectGroupList(@Param("client") String client);

   List<AuthGroupOutData> selectGroupSite(@Param("client") String client, @Param("userId") String userId);
}
