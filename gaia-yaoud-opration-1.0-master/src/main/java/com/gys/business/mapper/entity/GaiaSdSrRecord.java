package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table( name = "GAIA_SD_SR_RECORD")
public class GaiaSdSrRecord implements Serializable {
    private static final long serialVersionUID = 7144239647360841522L;
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSSR_BR_ID")
    private String gssrBrId;

    @Id
    @Column(name = "GSSR_DATE")
    private String gssrDate;

    @Id
    @Column(name = "GSSR_TIME")
    private String gssrTime;

    @Id
    @Column(name = "GSSR_BILL_NO")
    private String gssrBillNo;

    @Id
    @Column(name = "GSSR_SERIAL")
    private String gssrSerial;

    @Column(name = "GSSR_PRO_ID")
    private String gssrProId;

    @Column(name = "GSSR_RELE_PRO_TYPE")
    private String gssrReleProType;

    @Column(name = "GSSR_COMPCLASS")
    private String gssrCompclass;

    @Column(name = "GSSR_STOCK_QTY")
    private String gssrStockQty;

    @Column(name = "GSSR_FLAG")
    private String gssrFlag;

    @Column(name = "GSSR_PRIORITY1")
    private String gssrPtiority1;

    @Column(name = "GSSR_PRIORITY3")
    private String gssrPtiority3;

    @Column(name = "GSSR_STATUS")
    private String gssrStatus;
}
