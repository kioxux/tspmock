//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdDeviceInfo;
import com.gys.business.service.data.ProEffectInData;
import com.gys.business.service.data.ProEffectOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdProEffectMapper extends BaseMapper<GaiaSdDeviceInfo> {
    List<ProEffectOutData> proEffectList(ProEffectInData inData);
}
