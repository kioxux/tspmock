package com.gys.business.mapper.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 02:08
 */
@Data
public class CashBoxDetail {
    private Long id;
    private String client;
    private String storeId;
    private String machineNo;
    private Integer type;
    private BigDecimal lastAmount;
    private BigDecimal changeAmount;
    private BigDecimal currentAmount;
    private Integer deleteFlag;
    private String remark;
    private String createUser;
    private String updateUser;
    private Date createTime;
    private Date updateTime;
}
