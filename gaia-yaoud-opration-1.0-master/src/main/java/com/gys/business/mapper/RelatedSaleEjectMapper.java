package com.gys.business.mapper;

import com.gys.business.mapper.entity.RelatedSaleEject;
import com.gys.business.service.data.RelatedSaleEjectInData;
import com.gys.business.service.data.RelatedSaleEjectRes;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 关联销售弹出率统计表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-08-18
 */
@Mapper
public interface RelatedSaleEjectMapper extends BaseMapper<RelatedSaleEject> {
    List<RelatedSaleEjectRes> selectTclGroupByUserId(RelatedSaleEjectInData relatedSaleEjectInData);


    void insertBatch(@Param("list") List<RelatedSaleEject> saleEjectList);
}
