//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_STORES_GROUP"
)
public class GaiaSdStoresGroup implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSSG_ID"
    )
    private String gssgId;
    @Column(
            name = "GSSG_NAME"
    )
    private String gssgName;
    @Id
    @Column(
            name = "GSSG_BR_ID"
    )
    private String gssgBrId;
    @Column(
            name = "GSSG_BR_NAME"
    )
    private String gssgBrName;
    @Column(
            name = "GSSG_UPDATE_EMP"
    )
    private String gssgUpdateEmp;

    @Column(
            name = "GSSG_UPDATE_DATE"
    )
    private String gssgUpdateDate;

    @Column(
            name = "GSSG_UPDATE_TIME"
    )
    private String gssgUpdateTime;

    @Column(
            name = "GSSG_TYPE"
    )
    private String gssgType;

    public String getGssgType() {
        return gssgType;
    }

    public void setGssgType(String gssgType) {
        this.gssgType = gssgType;
    }

    private static final long serialVersionUID = 1L;

    public GaiaSdStoresGroup() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGssgId() {
        return this.gssgId;
    }

    public void setGssgId(String gssgId) {
        this.gssgId = gssgId;
    }

    public String getGssgName() {
        return this.gssgName;
    }

    public void setGssgName(String gssgName) {
        this.gssgName = gssgName;
    }

    public String getGssgBrId() {
        return this.gssgBrId;
    }

    public void setGssgBrId(String gssgBrId) {
        this.gssgBrId = gssgBrId;
    }

    public String getGssgBrName() {
        return this.gssgBrName;
    }

    public void setGssgBrName(String gssgBrName) {
        this.gssgBrName = gssgBrName;
    }

    public String getGssgUpdateEmp() {
        return this.gssgUpdateEmp;
    }

    public void setGssgUpdateEmp(String gssgUpdateEmp) {
        this.gssgUpdateEmp = gssgUpdateEmp;
    }

    public String getGssgUpdateDate() {
        return gssgUpdateDate;
    }

    public void setGssgUpdateDate(String gssgUpdateDate) {
        this.gssgUpdateDate = gssgUpdateDate;
    }

    public String getGssgUpdateTime() {
        return gssgUpdateTime;
    }

    public void setGssgUpdateTime(String gssgUpdateTime) {
        this.gssgUpdateTime = gssgUpdateTime;
    }
}
