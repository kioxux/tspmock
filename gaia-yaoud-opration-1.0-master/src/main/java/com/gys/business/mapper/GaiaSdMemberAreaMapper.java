package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMemberArea;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdMemberAreaMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdMemberArea record);

    int insertSelective(GaiaSdMemberArea record);

    GaiaSdMemberArea selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdMemberArea record);

    int updateByPrimaryKey(GaiaSdMemberArea record);

    int updateBatch(List<GaiaSdMemberArea> list);

    int batchInsert(@Param("list") List<GaiaSdMemberArea> list);

    List<GaiaSdMemberArea> getMemberAreaList(GaiaSdMemberArea record);
}