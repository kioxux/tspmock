package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdStoreEssentialProductPara;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdStoreEssentialProductParaMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdStoreEssentialProductPara record);

    int insertSelective(GaiaSdStoreEssentialProductPara record);

    GaiaSdStoreEssentialProductPara selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdStoreEssentialProductPara record);

    int updateByPrimaryKey(GaiaSdStoreEssentialProductPara record);

    int updateBatch(List<GaiaSdStoreEssentialProductPara> list);

    int batchInsert(@Param("list") List<GaiaSdStoreEssentialProductPara> list);

    List<GaiaSdStoreEssentialProductPara> getAll();

    List<GaiaSdStoreEssentialProductPara> getAllIfClient(@Param("clientId") String clientId);

    List<GaiaSdStoreEssentialProductPara> selectList();
}