package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SD_PREORD_PAY_MSG")
@Data
public class GaiaSdPreordPayMsg implements Serializable {
    private static final long serialVersionUID = -799490687536171929L;
    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 订购门店
     */
    @Column(name = "GSPPM_BR_ID")
    private String gsppmBrId;

    /**
     * 订购单号
     */
    @Column(name = "GSPPM_VOUCHER_ID")
    private String gsppmVoucherId;

    /**
     * 订购日期
     */
    @Column(name = "GSPPM_DATE")
    private String gsppmDate;

    /**
     * 支付编码
     */
    @Column(name = "GSPPM_ID")
    private String gsppmId;

    /**
     * 支付名称
     */
    @Column(name = "GSPPM_NAME")
    private String gsppmName;

    /**
     * 支付卡号
     */
    @Column(name = "GSPPM_CARD_NO")
    private String gsppmCardNo;

    /**
     * 支付金额
     */
    @Column(name = "GSPPM_AMT")
    private BigDecimal gsppmAmt;

    /**
     * RMB收款金额
     */
    @Column(name = "GSPPM_RMB_AMT")
    private BigDecimal gsppmRmbAmt;

    /**
     * 找零金额
     */
    @Column(name = "GSPPM_ZL_AMT")
    private BigDecimal gsppmZlAmt;

    /**
     * 是否日结 0/空为否1为是
     */
    @Column(name = "GSPPM_DAYREPORT")
    private String gsppmDayreport;


}