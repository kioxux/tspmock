package com.gys.business.mapper.entity;

import lombok.Data;

import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/21 16:35
 */
@Data
public class GaiaDcReplenishD {
    private Long id;
    private String client;
    private String proSite;
    private String voucherId;
    private String proSelfCode;
    private Integer deleteFlag;
    private Date createTime;
}
