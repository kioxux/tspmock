package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductBasic;
import com.gys.business.mapper.entity.GaiaProductBasicImage;
import com.gys.business.service.data.*;
import com.gys.business.service.data.productBasic.GetProductBasicInData;
import com.gys.business.service.data.productBasic.GetProductBasicOutData;
import com.gys.business.service.data.workflow.ChangePriceWFData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.JsonResult;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.ResultHandler;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface GaiaProductBasicMapper extends BaseMapper<GaiaProductBasic> {
    List<GetProOutData> selectProductByProCodes(@Param("proCodes") List<String> gspgProIds, @Param("client") String clientId);

    List<GetQueryProVoOutData> queryProVo(GetProductInData inData);

    List<GetQueryProVoOutData> queryPro(GetProductInData inData);

    List<GetQueryProVoOutData> queryPrice(GetProductInData inData);

    List<GetProductInfoQueryOutData> getProductBasicList(GetProductInfoQueryInData inData);

    List<GetQueryProductOutData> queryProduct(GetQueryProductInData inData);

    GetSalesReceiptsTableOutData queryProductDetail(GetQueryProductInData inData);

    List<GetPdAndExpOutData> queryBatchNoAndExp(GetQueryProductInData inData);

    List<GetProductInfoQueryOutData> selectMedCheckProList(GetProductInfoQueryInData inData);

    List<GetQueryProVoOutData> queryProFromBatch(GetProductInData inData);

    List<GetProductInfoQueryOutData> selectPhysicalProList(GetProductInfoQueryInData inData);

    List<GetServerProduct1OutData> getProductListByCompclass1(@Param("compclass") String compclass, @Param("storeCode") String storeCode, @Param("clientId") String clientId);

    List<GetServerProduct2OutData> getProductListByCompclass2(@Param("compclass") String compclass, @Param("storeCode") String storeCode, @Param("clientId") String clientId);

    List<GaiaProductBasic> selectSyncProductBasicList(@Param("client") String client, @Param("brId") String brId);

    List<GetReplenishDetailOutData> queryProFromReplenish(GetReplenishInData inData);

    List<GetQueryProductOutData> queryAcceptProduct(GetQueryProductInData inData);

    /**
     * 只查询商品信息
     *
     * @param inData
     * @return
     */
    List<GetQueryProductOutData> queryAcceptProductOnly(GetQueryProductInData inData);

    GetReplenishDetailOutData queryProFromReplenishDetail(GetReplenishInData inData);

    void addRepPrice(List<RatioProDetail> list);

    String selectNextPoId(@Param("clientId") String client);

    List<RatioProDetail> selectRatioStatus(RatioProInData inData);

    Map<String, String> selectStoreAttribute(@Param("clientId") String client, @Param("stoCode") String stoCode);

    List<GetReplenishDetailOutData> queryProAutoReplenish(GetReplenishInData inData);

    List<Map<String, String>> selectSupplierList(Map<String, String> inData);

    List<Map<String, String>> selectGetPriceBySup(Map<String, String> inData);

    void updateStatus(RatioProInData inData);

    GetProductInfoQueryOutData getProductBasicListAByApp(GetProductInfoQueryInData getProductInfoQueryInData);

    ImportAcceptInData queryProDetail(GetReplenishInData inData);

    List<String> getProCodeListByCompClass(@Param("compClass") String compClass);

    List<ProductBasicInfoOutData> getProductInfoByPage(ProductBasicInfoInData inData);

    List<GetProductBasicOutData> list(GetProductBasicInData inData);

    void edit(GetProductBasicOutData outData);

    List<LinkedHashMap<String,Object>> listForExport(GetProductBasicInData inData);

    Integer listCount();

    GetProductBasicOutData getOne(String proCode);

    List<Map<String,String>> getUnitList();

    List<Map<String, String>> getTaxList(Map<String,String> map);

    /**
     * 获取最新的药德商品编码
     * @return String
     */
    String selectLastProductCode();

    List<ChangePriceWFData> listCost(@Param("client") String client, @Param("brId") String depId, @Param("proSelfCodeList") List<String> proSelfCodeList);

    List<GaiaProductBasicImage> queryProductBasicImageList(@Param("proCodeList") List<String> proCodeList);

    int deleteProductBasicImageByProCode(@Param("proCode") String proCode);

    List<String> queryAllProBaiscCode();

}
