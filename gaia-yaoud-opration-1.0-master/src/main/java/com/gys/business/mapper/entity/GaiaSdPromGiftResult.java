package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 赠品类促销赠送商品设置表
 */
@Table(name = "GAIA_SD_PROM_GIFT_RESULT")
@Data
public class GaiaSdPromGiftResult implements Serializable {
    private static final long serialVersionUID = 7998400810177047188L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPGR_VOUCHER_ID")
    private String gspgrVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPGR_SERIAL")
    private String gspgrSerial;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSPGR_PRO_ID")
    private String gspgrProId;

    /**
     * 系列编码
     */
    @Column(name = "GSPGR_SERIES_ID")
    private String gspgrSeriesId;

    /**
     * 赠品单品价格
     */
    @Column(name = "GSPGR_GIFT_PRC")
    private BigDecimal gspgrGiftPrc;

    /**
     * 赠品单品折扣
     */
    @Column(name = "GSPGR_GIFT_REBATE")
    private String gspgrGiftRebate;

}
