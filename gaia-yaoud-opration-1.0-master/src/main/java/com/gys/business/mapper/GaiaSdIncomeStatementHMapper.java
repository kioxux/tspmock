//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIncomeStatementH;
import com.gys.business.service.data.DtYb.Physical.PhysicalVO;
import com.gys.business.service.data.DtYb.ProfitLoss.ProfitlossVO;
import com.gys.business.service.data.IncomeStatementInData;
import com.gys.business.service.data.IncomeStatementOutData;
import com.gys.business.service.data.YbInterfaceQueryData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdIncomeStatementHMapper extends BaseMapper<GaiaSdIncomeStatementH> {
    List<IncomeStatementOutData> getIncomeStatementList(IncomeStatementInData inData);

    String selectNextVoucherId(@Param("clientId") String clientId, @Param("codePre") String codePre);

    List<PhysicalVO> queryPhysical(YbInterfaceQueryData inData);

    List<ProfitlossVO> queryProfitloss(YbInterfaceQueryData inData);
}
