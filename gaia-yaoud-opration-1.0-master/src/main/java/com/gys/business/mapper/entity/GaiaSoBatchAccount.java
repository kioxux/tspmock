package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * GAIA_SO_BATCH_ACCOUNT
 * @author 
 */
@Data
public class GaiaSoBatchAccount implements Serializable {
    private Long id;

    private String client;

    private String appId;

    private String authCode;

    /**
     * 开关 0 关闭(默认); 1 打开
     */
    @ApiModelProperty(value="开关 0 关闭(默认); 1 打开")
    private String batchSwitch;

    private static final long serialVersionUID = 1L;
}