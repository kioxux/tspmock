package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaRegionalStore;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:53 2021/10/14
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
public interface GaiaRegionalStoreMapper extends BaseMapper<GaiaRegionalStore> {
    void batchInsert(@Param("gaiaRegionalStores") List<GaiaRegionalStore> gaiaRegionalStores);

    Integer getStoreNumByClientAndRegionalId(@Param("client") String client,@Param("regionalId") Long regionalId);

    List<GaiaRegionalStore> selectByRegionalId(Long regionalId);

    void batchUpdateRegionalIdAndSortNum(@Param("stores") List<GaiaRegionalStore> stores);

    List<GaiaRegionalStore> selectByRegionalIds(@Param("regionalIds") List<Long> regionalIds);
}
