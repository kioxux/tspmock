package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromGiftConds;
import com.gys.business.service.data.PromGiftCondsOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromGiftCondsMapper extends BaseMapper<GaiaSdPromGiftConds> {
    List<PromGiftCondsOutData> getDetail(PromoteInData inData);

    List<GaiaSdPromGiftConds> getAllByClient(@Param("client") String client, @Param("nowDate") String nowDate,@Param("brId")String brId);

}
