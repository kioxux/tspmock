package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRemoteAuditH;
import com.gys.business.mapper.entity.GaiaSdRemotePrescribeQd;
import com.gys.common.base.BaseMapper;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/18 10:12
 */
public interface GaiaSdRemotePrescribeQdMapper extends BaseMapper<GaiaSdRemotePrescribeQd>  {
}
