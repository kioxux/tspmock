package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 * @author 
 * 同步审计表
 */
@Data
public class GaiaSynchronousAudit implements Serializable {
    private static final long serialVersionUID = 2908794375996710682L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String brId;

    /**
     * 提交人ID
     */
    private String userId;

    /**
     * 销售日期
     */
    private String saleDate;

    /**
     * 提交日期
     */
    private Date submissionDate;

    /**
     * 总交易单数
     */
    private String totalBill;

    /**
     * 总交易金额
     */
    private BigDecimal totalBillAmount;

    /**
     * 销售笔数(不包含退货)
     */
    private String billNumber;

    /**
     * 销售金额(不包含退货)
     */
    private BigDecimal billAmount;

    /**
     * 退货笔数
     */
    private String retreatBill;

    /**
     * 退货金额
     */
    private BigDecimal retreatBillAmount;

    /**
     * 是否退货标志 1-未退货 0-已退货
     */
    private Integer isReturn;

    /**
     * 电脑标识
     */
    private String computerLogo;

}