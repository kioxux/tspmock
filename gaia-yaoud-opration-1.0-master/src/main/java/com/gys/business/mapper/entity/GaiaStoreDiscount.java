package com.gys.business.mapper.entity;

import lombok.Data;

@Data
public class GaiaStoreDiscount {
    /**
     *
     */
    private String client; //加盟商
    /**
     * 门店编码
     */
    private String stoCode;
    /**
     * 门店名称
     */
    private String stoName;
    /**
     * 助记码
     */
    private String stoPym;
    /**
     * 门店简称
     */
    private String stoShortName;
    /**
     * 折扣开始时间
     */
    private String gsdBeginDate;
    /**
     * 折扣结束时间
     */
    private String gsdEndDate;
    /**
     * 折扣
     */
    private String gsdStoRebate;
    /**
     * 修改日期
     */
    private String gsdStoModiDate;
    /**
     * 修改时间
     */
    private String gsdStoModiTime;
    /**
     * 修改人账号
     */
    private String gsdStoModiId;
    /**
     * 员工编号
     */
    private String gsdUserId;
    /**
     * 姓名
     */
    private String gsdUserName;
}

