package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Table(name = "GAIA_T_ACCOUNT")
public class GaiaTAccount implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "CLIENT")
    private String client;

    @Column(name = "STO_CODE")
    private String stoCode;

    @Column(name = "PUBLIC_CODE")
    private String publicCode;

    /**
     * 平台：EB:饿了么；MT:美团；JD：京东； KY：快药
     */
    @Column(name = "PLATFORM")
    private String platform;

    @Column(name = "APP_ID")
    private String appId;

    /**
     * 密钥
     */
    @Column(name = "APP_SECRET")
    private String appSecret;

    /**
     * 门店ID
     */
    @Column(name = "APP_STO_ID")
    private String appStoId;

    /**
     * 门店账号
     */
    @Column(name = "APP_STO_ACCOUNT")
    private String appStoAccount;

    /**
     * 门店密码
     */
    @Column(name = "APP_STO_PWD")
    private String appStoPwd;

    /**
     * 门店链接
     */
    @Column(name = "APP_STO_URL")
    private String appStoUrl;

    /**
     * 关联门店链接
     */
    @Column(name = "APP_RELATION_STO_URL")
    private String appRelationStoUrl;

    /**
     *  订单开关状态 N：未开启；Y；已开启
     */
    @Column(name="ORDER_ON_SERVICE")
    private String orderOnService;

    /**
     *  库存开关状态 N：未开启；Y；已开启
     */
    @Column(name="STOCK_ON_SERVICE")
    private String stockOnService;

    /**
     *  商家ID，京东到家专用
     */
    @Column(name="VENDER_ID")
    private String venderId;

    /**
     *  token，京东到家专用
     */
    @Column(name="TOKEN")
    private String token;

    /**
     *  公司编码(由叮当提供)，仅叮当快药使用
     */
    @Column(name="COMPANY_ID")
    private String companyId;

    private static final long serialVersionUID = 1L;


}