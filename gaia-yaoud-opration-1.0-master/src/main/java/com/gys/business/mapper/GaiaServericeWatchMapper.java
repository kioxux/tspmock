package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaServericeWatch;
import com.gys.common.base.BaseMapper;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2022/1/7 13:56
 */
public interface GaiaServericeWatchMapper  extends BaseMapper<GaiaServericeWatch> {
}
