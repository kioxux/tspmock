package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaTOrderInfo;
import com.gys.business.service.data.GetTakeAwayInData;
import com.gys.business.service.data.OrderPayTimeBean;
import com.gys.business.service.data.TakeAwayOrderOutData;
import com.gys.business.service.data.takeaway.WebOrderDataDto;
import com.gys.business.service.data.takeaway.WebOrderDetailDataDto;
import com.gys.business.service.data.takeaway.WebOrderQueryBean;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.WeideProIdDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaTOrderInfoMapper extends BaseMapper<GaiaTOrderInfo> {
    List<TakeAwayOrderOutData> getOrderList(GetTakeAwayInData inData);

    List<TakeAwayOrderOutData> getOrderList2(GetTakeAwayInData inData);

    GaiaTOrderInfo getPlatformOrderIdByOrderId(GetTakeAwayInData inData);

    GaiaTOrderInfo  selectByPlatformsAndPlatformsOrderId(@Param ("platformsOrderId")   String platformsOrderId, @Param("platform") String platform);

    void updateStatus(@Param("platformsOrderId") String platformsOrderId, @Param("platform") String platform,
                      @Param("reasonCode") String reasonCode, @Param("reason") String reason);

    void updateDdkyStatus(@Param("orderId") String orderId, @Param("status") String status);

    void updatePayTime(OrderPayTimeBean bean);

    List<WebOrderDataDto> orderQuery(WebOrderQueryBean bean);

    List<WebOrderDetailDataDto> orderDetailQuery(WebOrderQueryBean bean);

    String getPayTime(OrderPayTimeBean bean);

    List<WeideProIdDto> getWeideEbProId(@Param("client") String client, @Param("stoCode") String stoCode, @Param("list") List<String> list);
}
