package com.gys.business.mapper;

import com.gys.business.service.data.GaiaProductSpecialParam;
import com.gys.business.service.data.GaiaProductSpecialParamChange;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductSpecialParamChangeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaProductSpecialParamChange record);

    int insertSelective(GaiaProductSpecialParamChange record);

    GaiaProductSpecialParamChange selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaProductSpecialParamChange record);

    int updateByPrimaryKey(GaiaProductSpecialParamChange record);

    int batchInsert(@Param("list") List<GaiaProductSpecialParamChange> list);

    List<GaiaProductSpecialParamChange> getParamEditRecord(GaiaProductSpecialParam inData);
}