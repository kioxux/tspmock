package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaThirdPaymentTonglian;
import com.gys.common.base.BaseMapper;

public interface GaiaThirdPaymentTonglianMapper extends BaseMapper<GaiaThirdPaymentTonglian> {


    long insertRetuenKeys(GaiaThirdPaymentTonglian inData);

    GaiaThirdPaymentTonglian selectById(Long id);
}