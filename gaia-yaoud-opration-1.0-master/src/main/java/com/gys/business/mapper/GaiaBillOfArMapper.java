package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaBillOfAr;
import com.gys.business.mapper.entity.GaiaBillOfArCancel;
import com.gys.common.data.GaiaBillOfArDetailOutData;
import com.gys.common.data.GaiaBillOfArInData;
import com.gys.common.data.GaiaBillOfArOutData;
import com.gys.common.data.GaiaBillOfArReportData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 明细清单表(GaiaBillOfAr)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-18 15:13:12
 */
public interface GaiaBillOfArMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaBillOfAr queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaBillOfAr> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaBillOfAr 实例对象
     * @return 对象列表
     */
    List<GaiaBillOfAr> queryAll(GaiaBillOfAr gaiaBillOfAr);

    /**
     * 新增数据
     *
     * @param gaiaBillOfAr 实例对象
     * @return 影响行数
     */
    int insert(GaiaBillOfAr gaiaBillOfAr);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaBillOfAr> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaBillOfAr> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaBillOfAr> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaBillOfAr> entities);

    /**
     * 修改数据
     *
     * @param gaiaBillOfAr 实例对象
     * @return 影响行数
     */
    int update(GaiaBillOfAr gaiaBillOfAr);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    String getBillCode(GaiaBillOfArInData inData);

    List<GaiaBillOfArOutData> listBill(GaiaBillOfArInData inData);

    Integer getBillCount(GaiaBillOfArInData inData);

    GaiaBillOfArOutData getCusInfo(GaiaBillOfArInData inData);

    List<GaiaBillOfArDetailOutData> listSalesBills(GaiaBillOfArInData inData);

    double getLastAmt(GaiaBillOfArInData inData);

    GaiaBillOfAr getHxAmt(GaiaBillOfArInData inData);

    GaiaBillOfAr getBillInfo(GaiaBillOfArInData inData);

    List<GaiaBillOfArDetailOutData> listReceivableDetail(GaiaBillOfArInData inDate);

    List<GaiaBillOfArReportData> listReport(GaiaBillOfArInData inData);

    GaiaBillOfArOutData getBillTotal(GaiaBillOfArInData inData);

    List<GaiaBillOfArDetailOutData> listUnReceivableDetail(GaiaBillOfArInData inData);

    List<GaiaBillOfArDetailOutData> listCusInfo(GaiaBillOfArInData inDate);

    int deleteArBill(GaiaBillOfArInData inData);

    List<Map<String, Object>> listHasCustomer(GaiaBillOfArInData inData);

    List<Map<String, String>> selectSoDnByList(@Param("client") String client,@Param("soDnByName")String soDnByName);

    GaiaBillOfArOutData selectSumByArAmt(@Param("insertOrUpdate") GaiaBillOfAr insertOrUpdate);

    GaiaBillOfArOutData selectSumByArAmt(GaiaBillOfArInData inData);

    void updateByHxAmt(GaiaBillOfArCancel cancel);
}

