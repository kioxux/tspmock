package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

@Data
@Table(
        name = "GAIA_SD_PRODUCT_LIMIT_STO"
)
public class ProductLimitSto implements Serializable {

    private static final long serialVersionUID = -2686708190093331661L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "CLIENT")
    private String client;

    @Column(name = "PRO_ID")
    private String proId;

    @Column(name = "BR_ID")
    private String brId;

    @Transient
    private String brName;
}
