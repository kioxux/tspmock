//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromAssoSet;
import com.gys.business.service.data.PromAssoSetOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdPromAssoSetMapper extends BaseMapper<GaiaSdPromAssoSet> {
    List<PromAssoSetOutData> getDetail(PromoteInData inData);
}
