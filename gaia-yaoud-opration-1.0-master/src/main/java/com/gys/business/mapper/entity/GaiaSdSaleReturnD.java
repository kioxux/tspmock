//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_SALE_RETURN_D"
)
public class GaiaSdSaleReturnD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSSRD_VOUCHER_ID"
    )
    private String gssrdVoucherId;
    @Id
    @Column(
            name = "GSSRD_BR_ID"
    )
    private String gssrdBrId;
    @Id
    @Column(
            name = "GSSRD_RETNO"
    )
    private String gssrdRetno;
    @Id
    @Column(
            name = "GSSRD_RETDATE"
    )
    private String gssrdRetdate;
    @Id
    @Column(
            name = "GSSRD_SERIAL"
    )
    private String gssrdSerial;
    @Column(
            name = "GSSRD_PRO_ID"
    )
    private String gssrdProId;
    @Column(
            name = "GSSRD_BATCH"
    )
    private String gssrdBatch;
    @Column(
            name = "GSSRD_VALID_DATE"
    )
    private String gssrdValidDate;
    @Column(
            name = "GSSRD_ST_CODE"
    )
    private String gssrdStCode;
    @Column(
            name = "GSSRD_PRC1"
    )
    private BigDecimal gssrdPrc1;
    @Column(
            name = "GSSRD_PRC2"
    )
    private BigDecimal gssrdPrc2;
    @Column(
            name = "GSSRD_QTY"
    )
    private String gssrdQty;
    @Column(
            name = "GSSRD_AMT"
    )
    private BigDecimal gssrdAmt;
    @Column(
            name = "GSSRD_INTEGRAL_EXCHANGE_SINGLE"
    )
    private String gssrdIntegralExchangeSingle;
    @Column(
            name = "GSSRD_INTEGRAL_EXCHANGE_COLLECT"
    )
    private String gssrdIntegralExchangeCollect;
    @Column(
            name = "GSSRD_INTEGRAL_EXCHANGE_AMT_SINGLE"
    )
    private BigDecimal gssrdIntegralExchangeAmtSingle;
    @Column(
            name = "GSSRD_INTEGRAL_EXCHANGE_AMT_COLLECT"
    )
    private BigDecimal gssrdIntegralExchangeAmtCollect;
    @Column(
            name = "GSSRD_ZK_AMT"
    )
    private BigDecimal gssrdZkAmt;
    @Column(
            name = "GSSRD_ZK_JFDH"
    )
    private BigDecimal gssrdZkJfdh;
    @Column(
            name = "GSSRD_ZK_JFDX"
    )
    private BigDecimal gssrdZkJfdx;
    @Column(
            name = "GSSRD_ZK_DZQ"
    )
    private BigDecimal gssrdZkDzq;
    @Column(
            name = "GSSRD_ZK_DYQ"
    )
    private BigDecimal gssrdZkDyq;
    @Column(
            name = "GSSRD_ZK_PM"
    )
    private BigDecimal gssrdZkPm;
    @Column(
            name = "GSSRD_SALER_ID"
    )
    private String gssrdSalerId;
    @Column(
            name = "GSSRD_DOCTOR_ID"
    )
    private String gssrdDoctorId;
    @Column(
            name = "GSSRD_PM_SUBJECT_ID"
    )
    private String gssrdPmSubjectId;
    @Column(
            name = "GSSRD_PM_ID"
    )
    private String gssrdPmId;
    @Column(
            name = "GSSRD_PM_CONTENT"
    )
    private String gssrdPmContent;
    private static final long serialVersionUID = 1L;

    public GaiaSdSaleReturnD() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGssrdVoucherId() {
        return this.gssrdVoucherId;
    }

    public void setGssrdVoucherId(String gssrdVoucherId) {
        this.gssrdVoucherId = gssrdVoucherId;
    }

    public String getGssrdBrId() {
        return this.gssrdBrId;
    }

    public void setGssrdBrId(String gssrdBrId) {
        this.gssrdBrId = gssrdBrId;
    }

    public String getGssrdRetno() {
        return this.gssrdRetno;
    }

    public void setGssrdRetno(String gssrdRetno) {
        this.gssrdRetno = gssrdRetno;
    }

    public String getGssrdRetdate() {
        return this.gssrdRetdate;
    }

    public void setGssrdRetdate(String gssrdRetdate) {
        this.gssrdRetdate = gssrdRetdate;
    }

    public String getGssrdSerial() {
        return this.gssrdSerial;
    }

    public void setGssrdSerial(String gssrdSerial) {
        this.gssrdSerial = gssrdSerial;
    }

    public String getGssrdProId() {
        return this.gssrdProId;
    }

    public void setGssrdProId(String gssrdProId) {
        this.gssrdProId = gssrdProId;
    }

    public String getGssrdBatch() {
        return this.gssrdBatch;
    }

    public void setGssrdBatch(String gssrdBatch) {
        this.gssrdBatch = gssrdBatch;
    }

    public String getGssrdValidDate() {
        return this.gssrdValidDate;
    }

    public void setGssrdValidDate(String gssrdValidDate) {
        this.gssrdValidDate = gssrdValidDate;
    }

    public String getGssrdStCode() {
        return this.gssrdStCode;
    }

    public void setGssrdStCode(String gssrdStCode) {
        this.gssrdStCode = gssrdStCode;
    }

    public BigDecimal getGssrdPrc1() {
        return this.gssrdPrc1;
    }

    public void setGssrdPrc1(BigDecimal gssrdPrc1) {
        this.gssrdPrc1 = gssrdPrc1;
    }

    public BigDecimal getGssrdPrc2() {
        return this.gssrdPrc2;
    }

    public void setGssrdPrc2(BigDecimal gssrdPrc2) {
        this.gssrdPrc2 = gssrdPrc2;
    }

    public String getGssrdQty() {
        return this.gssrdQty;
    }

    public void setGssrdQty(String gssrdQty) {
        this.gssrdQty = gssrdQty;
    }

    public BigDecimal getGssrdAmt() {
        return this.gssrdAmt;
    }

    public void setGssrdAmt(BigDecimal gssrdAmt) {
        this.gssrdAmt = gssrdAmt;
    }

    public String getGssrdIntegralExchangeSingle() {
        return this.gssrdIntegralExchangeSingle;
    }

    public void setGssrdIntegralExchangeSingle(String gssrdIntegralExchangeSingle) {
        this.gssrdIntegralExchangeSingle = gssrdIntegralExchangeSingle;
    }

    public String getGssrdIntegralExchangeCollect() {
        return this.gssrdIntegralExchangeCollect;
    }

    public void setGssrdIntegralExchangeCollect(String gssrdIntegralExchangeCollect) {
        this.gssrdIntegralExchangeCollect = gssrdIntegralExchangeCollect;
    }

    public BigDecimal getGssrdIntegralExchangeAmtSingle() {
        return this.gssrdIntegralExchangeAmtSingle;
    }

    public void setGssrdIntegralExchangeAmtSingle(BigDecimal gssrdIntegralExchangeAmtSingle) {
        this.gssrdIntegralExchangeAmtSingle = gssrdIntegralExchangeAmtSingle;
    }

    public BigDecimal getGssrdIntegralExchangeAmtCollect() {
        return this.gssrdIntegralExchangeAmtCollect;
    }

    public void setGssrdIntegralExchangeAmtCollect(BigDecimal gssrdIntegralExchangeAmtCollect) {
        this.gssrdIntegralExchangeAmtCollect = gssrdIntegralExchangeAmtCollect;
    }

    public BigDecimal getGssrdZkAmt() {
        return this.gssrdZkAmt;
    }

    public void setGssrdZkAmt(BigDecimal gssrdZkAmt) {
        this.gssrdZkAmt = gssrdZkAmt;
    }

    public BigDecimal getGssrdZkJfdh() {
        return this.gssrdZkJfdh;
    }

    public void setGssrdZkJfdh(BigDecimal gssrdZkJfdh) {
        this.gssrdZkJfdh = gssrdZkJfdh;
    }

    public BigDecimal getGssrdZkJfdx() {
        return this.gssrdZkJfdx;
    }

    public void setGssrdZkJfdx(BigDecimal gssrdZkJfdx) {
        this.gssrdZkJfdx = gssrdZkJfdx;
    }

    public BigDecimal getGssrdZkDzq() {
        return this.gssrdZkDzq;
    }

    public void setGssrdZkDzq(BigDecimal gssrdZkDzq) {
        this.gssrdZkDzq = gssrdZkDzq;
    }

    public BigDecimal getGssrdZkDyq() {
        return this.gssrdZkDyq;
    }

    public void setGssrdZkDyq(BigDecimal gssrdZkDyq) {
        this.gssrdZkDyq = gssrdZkDyq;
    }

    public BigDecimal getGssrdZkPm() {
        return this.gssrdZkPm;
    }

    public void setGssrdZkPm(BigDecimal gssrdZkPm) {
        this.gssrdZkPm = gssrdZkPm;
    }

    public String getGssrdSalerId() {
        return this.gssrdSalerId;
    }

    public void setGssrdSalerId(String gssrdSalerId) {
        this.gssrdSalerId = gssrdSalerId;
    }

    public String getGssrdDoctorId() {
        return this.gssrdDoctorId;
    }

    public void setGssrdDoctorId(String gssrdDoctorId) {
        this.gssrdDoctorId = gssrdDoctorId;
    }

    public String getGssrdPmSubjectId() {
        return this.gssrdPmSubjectId;
    }

    public void setGssrdPmSubjectId(String gssrdPmSubjectId) {
        this.gssrdPmSubjectId = gssrdPmSubjectId;
    }

    public String getGssrdPmId() {
        return this.gssrdPmId;
    }

    public void setGssrdPmId(String gssrdPmId) {
        this.gssrdPmId = gssrdPmId;
    }

    public String getGssrdPmContent() {
        return this.gssrdPmContent;
    }

    public void setGssrdPmContent(String gssrdPmContent) {
        this.gssrdPmContent = gssrdPmContent;
    }
}
