package com.gys.business.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface ColdChainMapper {

    List<HashMap<String, Object>> listThirdInfo(@Param("thirdCodeList") List<String> thirdCodeList,@Param("client") String client);
}
