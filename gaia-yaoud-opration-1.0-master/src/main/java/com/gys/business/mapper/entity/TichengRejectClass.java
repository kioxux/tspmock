package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 销售提成方案剔除商品分类表
 * </p>
 *
 * @author flynn
 * @since 2021-11-12
 */
@Data
@Table( name = "GAIA_TICHENG_REJECT_CLASS")
public class TichengRejectClass implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键ID")
    @Id
    @Column(name = "ID")
    private Long id;

    @ApiModelProperty(value = "销售提成方案主表主键ID")
    @Column(name = "PID")
    private Long pid;

    @ApiModelProperty(value = "一级商品分类")
    @Column(name = "PRO_BIG_CLASS")
    private String proBigClass;

    @ApiModelProperty(value = "二级商品分类")
    @Column(name = "PRO_MID_CLASS")
    private String proMidClass;

    @ApiModelProperty(value = "商品分类")
    @Column(name = "PRO_CLASS")
    private String proClass;

    @ApiModelProperty(value = "操作状态 0 未删除 1 已删除")
    @Column(name = "DELETE_FLAG")
    private String deleteFlag;

    @Column(name = "LAST_UPDATE_TIME")
    private LocalDateTime lastUpdateTime;


}
