//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 促销参数表
 */
@Table(name = "GAIA_SD_PROM_PARA")
@Data
public class GaiaSdPromPara implements Serializable {
    private static final long serialVersionUID = 8432193591191783616L;

    /**
     * 参数
     */
    @Id
    @Column(name = "GSPP_PARA")
    private String gsppPara;

    /**
     * 参数描述
     */
    @Column(name = "GSPP_NAME")
    private String gsppName;

    /**
     * 所属参数类型
     */
    @Column(name = "GSPP_TYPE")
    private String gsppType;

    /**
     * 选用于促销类型
     */
    @Id
    @Column(name = "GSPP_PROM_TYPE")
    private String gsppPromType;
}
