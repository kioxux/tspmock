//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_REMOTE_AUDIT_D"
)
public class GaiaSdRemoteAuditD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSRAD_VOUCHER_ID"
    )
    private String gsradVoucherId;
    @Id
    @Column(
            name = "GSRAD_BR_ID"
    )
    private String gsradBrId;
    @Id
    @Column(
            name = "GSRAD_SERIAL"
    )
    private String gsradSerial;
    @Column(
            name = "GSRAD_PRO_ID"
    )
    private String gsradProId;
    @Column(
            name = "GSRAD_BATCH_NO"
    )
    private String gsradBatchNo;
    @Column(
            name = "GSRAD_QTY"
    )
    private String gsradQty;
    private static final long serialVersionUID = 1L;

    public GaiaSdRemoteAuditD() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsradVoucherId() {
        return this.gsradVoucherId;
    }

    public void setGsradVoucherId(String gsradVoucherId) {
        this.gsradVoucherId = gsradVoucherId;
    }

    public String getGsradBrId() {
        return this.gsradBrId;
    }

    public void setGsradBrId(String gsradBrId) {
        this.gsradBrId = gsradBrId;
    }

    public String getGsradSerial() {
        return this.gsradSerial;
    }

    public void setGsradSerial(String gsradSerial) {
        this.gsradSerial = gsradSerial;
    }

    public String getGsradProId() {
        return this.gsradProId;
    }

    public void setGsradProId(String gsradProId) {
        this.gsradProId = gsradProId;
    }

    public String getGsradBatchNo() {
        return this.gsradBatchNo;
    }

    public void setGsradBatchNo(String gsradBatchNo) {
        this.gsradBatchNo = gsradBatchNo;
    }

    public String getGsradQty() {
        return this.gsradQty;
    }

    public void setGsradQty(String gsradQty) {
        this.gsradQty = gsradQty;
    }
}
