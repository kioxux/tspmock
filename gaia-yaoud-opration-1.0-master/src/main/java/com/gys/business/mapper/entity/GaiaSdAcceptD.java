//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(
        name = "GAIA_SD_ACCEPT_D"
)
@Data
public class GaiaSdAcceptD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Column(
            name = "GSAD_BR_ID"
    )
    private String gsadBrId;
    @Id
    @Column(
            name = "GSAD_VOUCHER_ID"
    )
    private String gsadVoucherId;
    @Column(
            name = "GSAD_DATE"
    )
    private String gsadDate;
    @Id
    @Column(
            name = "GSAD_SERIAL"
    )
    private String gsadSerial;
    @Column(
            name = "GSAD_PRO_ID"
    )
    private String gsadProId;
    @Column(
            name = "GSAD_BATCH_NO"
    )
    private String gsadBatchNo;
    @Column(
            name = "GSAD_BATCH"
    )
    private String gsadBatch;
    @Column(
            name = "GSAD_MADE_DATE"
    )
    private String gsadMadeDate;
    @Column(
            name = "GSAD_VALID_DATE"
    )
    private String gsadValidDate;
    @Column(
            name = "GSAD_INVOICES_QTY"
    )
    private String gsadInvoicesQty;
    @Column(
            name = "GSAD_RECIPIENT_QTY"
    )
    private String gsadRecipientQty;
    @Column(
            name = "GSAD_REFUSE_QTY"
    )
    private String gsadRefuseQty;
    @Column(
            name = "GSAD_PACK_NO"
    )
    private String gsadGsadPackNo;
    @Column(
            name = "GSAD_TX_NO"
    )
    private String gsadTxNo;
    @Column(
            name = "GSAD_ROW_REMARK"
    )
    private String gsadRowRemark;
    @Column(
            name = "GSAD_STOCK_STATUS"
    )
    private String gsadStockStatus;
   @Column(
            name = "GSAD_INITIATE_AUDIT"
    )
    private String gsadInitiateAudit;
    @Column(
            name = "GSAD_VOUCHER_AMT"
    )
    private String gsadVoucherAmt;
    @Column(
            name = "GSAD_ACCEPT_PRICE"
    )
    private String gsadAcceptPrice;
    @Column(
            name = "GSAD_REFUSE_REASON"
    )
    private String gsadRefuseReason;
    @Column(
            name = "GSAD_DISCOUNT_PRICE"
    )
    private String discountPrice;
    @Column(
            name = "GSAD_DISCOUNT_RATE"
    )
    private String discountRate;

    @Column(
            name = "GSAD_INVOICE_PRICE"
    )
    private BigDecimal invoicePrice;

    @Column(
            name = "GSAD_INVOICE_AMT"
    )
    private BigDecimal invoiceAmount;
    private static final long serialVersionUID = 1L;

}
