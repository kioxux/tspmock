package com.gys.business.mapper.entity;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 供应商应付期末余额
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Data
@Accessors(chain = true)
@Table( name = "GAIA_FI_AP_BALANCE")
public class GaiaFiApBalance implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "DC编码")
    @Column(name = "DC_CODE")
    private String dcCode;

    @ApiModelProperty(value = "DC名称")
    @Column(name = "DC_NAME")
    private String dcName;

    @ApiModelProperty(value = "余额")
    @Column(name = "RESIDUAL_AMOUNT")
    private BigDecimal residualAmount;

    @ApiModelProperty(value = "供应商自编码")
    @Column(name = "SUP_SELF_CODE")
    private String supSelfCode;

    @ApiModelProperty(value = "供应商编码")
    @Column(name = "SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "供应商名称")
    @Column(name = "SUP_NAME")
    private String supName;

    @ApiModelProperty(value = "结算日期")
    @Column(name = "BALANCE_YEAR")
    private String balanceYear;

    @ApiModelProperty(value = "结算日期")
    @Column(name = "BALANCE_MONTH")
    private String balanceMonth;


    public static final String CLIENT = "CLIENT";

    public static final String DC_CODE = "DC_CODE";

    public static final String DC_NAME = "DC_NAME";

    public static final String RESIDUAL_AMOUNT = "RESIDUAL_AMOUNT";

    public static final String SUP_SELF_CODE = "SUP_SELF_CODE";

    public static final String SUP_CODE = "SUP_CODE";

    public static final String SUP_NAME = "SUP_NAME";


}
