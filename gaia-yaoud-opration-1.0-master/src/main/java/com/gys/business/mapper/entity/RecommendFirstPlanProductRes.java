package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class RecommendFirstPlanProductRes implements Serializable {

    private static final long serialVersionUID = -4017470398193692313L;

    private String proId;

    private String proName;

    private String proSpecs;

    private String proFactoryName;

    private String proUnit;

    private String cbPrice;

    private String normalPrice;

    private String mao;

    private Long planProductId;

    private String proCompClass;
}
