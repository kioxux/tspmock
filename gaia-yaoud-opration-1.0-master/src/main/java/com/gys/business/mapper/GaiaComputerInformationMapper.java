package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaComputerInformation;
import com.gys.business.service.data.takeaway.FxMainSaleSetBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaComputerInformationMapper {

    void insert(GaiaComputerInformation information);

    GaiaComputerInformation getComputerInformation(String identifies);

    int setMainSale(FxMainSaleSetBean bean);

    int setNoMainSale(FxMainSaleSetBean bean);

    String getMainSale(FxMainSaleSetBean bean);

    List<String> getNoMainSale(FxMainSaleSetBean bean);

    String getMainSaleByClientAndBrId(@Param("client") String client, @Param("stoCode") String stoCode);
}
