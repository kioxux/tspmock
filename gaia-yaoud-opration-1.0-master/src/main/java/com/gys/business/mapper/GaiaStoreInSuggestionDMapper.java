package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdStock;
import com.gys.business.mapper.entity.GaiaStoreInSuggestionD;
import com.gys.common.data.suggestion.SuggestionDetailVO;
import com.gys.common.data.suggestion.SuggestionInForm;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * 门店调入建议-明细表(GaiaStoreInSuggestionD)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-28 10:52:55
 */
public interface GaiaStoreInSuggestionDMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaStoreInSuggestionD queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaStoreInSuggestionD 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaStoreInSuggestionD> queryAllByLimit(GaiaStoreInSuggestionD gaiaStoreInSuggestionD, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaStoreInSuggestionD 查询条件
     * @return 总行数
     */
    long count(GaiaStoreInSuggestionD gaiaStoreInSuggestionD);

    /**
     * 新增数据
     *
     * @param gaiaStoreInSuggestionD 实例对象
     * @return 影响行数
     */
    int insert(GaiaStoreInSuggestionD gaiaStoreInSuggestionD);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaStoreInSuggestionD> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaStoreInSuggestionD> entities);

    /**
     * 修改数据
     *
     * @param gaiaStoreInSuggestionD 实例对象
     * @return 影响行数
     */
    int update(GaiaStoreInSuggestionD gaiaStoreInSuggestionD);

    List<SuggestionDetailVO> findVOList(SuggestionInForm suggestionInForm);

    List<GaiaSdStock> listProStockQty(@Param("client") String client,@Param("stoCode") String stoCode);

    List<GaiaStoreInSuggestionD> findList(GaiaStoreInSuggestionD suggestionDCond);

    int updateByCode(GaiaStoreInSuggestionD suggestionD);
}

