package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_YB_YD_MATCH")
public class GaiaYbYdMatch implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 店号
     */
    @Id
    @Column(name = "BR_ID")
    private String brId;

    /**
     * 异动业务单号
     */
    @Id
    @Column(name = "VOUCHER_ID")
    private String voucherId;

    /**
     * 异动日期
     */

    @Column(name = "DATE")
    private String date;

    /**
     * 行号
     */
    @Id
    @Column(name = "SERIAL")
    private String serial;

    /**
     * 商品
     */
    @Id
    @Column(name = "PRO_ID")
    private String proId;

    /**
     * 批次
     */
    @Column(name = "BATCH")
    private String batch;

    /**
     * 批号
     */
    @Column(name = "BATCH_NO")
    private String batchNo;

    /**
     * 数量
     */
    @Column(name = "QTY")
    private BigDecimal qty;

    /**
     * 医保接口匹配单号
     */
    @Column(name = "MATCH_ID")
    private String matchId;

    /**
     * 类型
     */
    @Id
    @Column(name = "TYPE")
    private String type;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取店号
     *
     * @return BR_ID - 店号
     */
    public String getBrId() {
        return brId;
    }

    /**
     * 设置店号
     *
     * @param brId 店号
     */
    public void setBrId(String brId) {
        this.brId = brId;
    }

    /**
     * 获取异动业务单号
     *
     * @return VOUCHER_ID - 异动业务单号
     */
    public String getVoucherId() {
        return voucherId;
    }

    /**
     * 设置异动业务单号
     *
     * @param voucherId 异动业务单号
     */
    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    /**
     * 获取异动日期
     *
     * @return DATE - 异动日期
     */
    public String getDate() {
        return date;
    }

    /**
     * 设置异动日期
     *
     * @param date 异动日期
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 获取行号
     *
     * @return SERIAL - 行号
     */
    public String getSerial() {
        return serial;
    }

    /**
     * 设置行号
     *
     * @param serial 行号
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * 获取商品
     *
     * @return PRO_ID - 商品
     */
    public String getProId() {
        return proId;
    }

    /**
     * 设置商品
     *
     * @param proId 商品
     */
    public void setProId(String proId) {
        this.proId = proId;
    }

    /**
     * 获取批次
     *
     * @return BATCH - 批次
     */
    public String getBatch() {
        return batch;
    }

    /**
     * 设置批次
     *
     * @param batch 批次
     */
    public void setBatch(String batch) {
        this.batch = batch;
    }

    /**
     * 获取批号
     *
     * @return BATCH_NO - 批号
     */
    public String getBatchNo() {
        return batchNo;
    }

    /**
     * 设置批号
     *
     * @param batchNo 批号
     */
    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    /**
     * 获取数量
     *
     * @return QTY - 数量
     */
    public BigDecimal getQty() {
        return qty;
    }

    /**
     * 设置数量
     *
     * @param qty 数量
     */
    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    /**
     * 获取医保接口匹配单号
     *
     * @return MATCH_ID - 医保接口匹配单号
     */
    public String getMatchId() {
        return matchId;
    }

    /**
     * 设置医保接口匹配单号
     *
     * @param matchId 医保接口匹配单号
     */
    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    /**
     * 获取类型
     *
     * @return TYPE - 类型
     */
    public String getType() {
        return type;
    }

    /**
     * 设置类型
     *
     * @param type 类型
     */
    public void setType(String type) {
        this.type = type;
    }
}