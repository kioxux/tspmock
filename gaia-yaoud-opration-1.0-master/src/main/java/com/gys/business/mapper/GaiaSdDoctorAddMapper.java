package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdDoctorAdd;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface GaiaSdDoctorAddMapper {

    List<GaiaSdDoctorAdd> getAllByClient(@Param("client") String client);
}
