package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "GAIA_SD_INTEGRAL_CASH_SET")
public class GaiaSdIntegralCashSet implements Serializable {
    private static final long serialVersionUID = -6060672248100044483L;
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSICS_VOUCHER_ID")
    private String gsicsVoucherId;

    @Column(name = "GSICS_BR_ID")
    private String gsicsBrId;

    @Column(name = "GSICS_BR_NAME")
    private String gsicsBrName;

    @Column(name = "GSICS_MEMBER_CLASS")
    private String gsicsMemberClass;

    @Column(name = "GSICS_DATE_START")
    private String gsicsDateStart;

    @Column(name = "GSICS_DATE_END")
    private String gsicsDateEnd;

    @Column(name = "GSICS_INTEGRAL_DEDUCT")
    private String gsicsIntegralDeduct;

    @Column(name = "GSICS_AMT_OFFSET")
    private BigDecimal gsicsAmtOffset;

    @Column(name = "GSICS_AMT_OFFSET_MAX")
    private BigDecimal gsicsAmtOffsetMax;

    @Column(name = "GSICS_DESCRIPTION")
    private String gsicsDescription;

    @Column(name = "GSICS_INTEGRAL_ADD_SET")
    private String gsicsIntegralAddSet;

    @Column(name = "GSICS_CHANGE_SET")
    private String gsicsChangeSet;

    @Column(name = "GSICS_EMP")
    private String gsicsEmp;

}
