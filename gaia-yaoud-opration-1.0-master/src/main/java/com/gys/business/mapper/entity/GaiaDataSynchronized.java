package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**

 * @author xiayuan
 * @date 2021-01-05
 */
@Data
public class GaiaDataSynchronized implements Serializable {
    private static final long serialVersionUID = -2048286020226582283L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String site;

    /**
     * 版本号
     */
    private String version;

    /**
     * 时间戳
     */
    private String datetime;

    /**
     * 来源单据
     */
    private String sourceNo;

    /**
     * 表明
     */
    private String tableName;

    /**
     * 命令类型：insert；update
     */
    private String command;

    /**
     * 参数
     */
    private String arg;

    /**
     * 数据集合
     */
    private Map<String, Object> everyMap;

    /**
     * 同步数据
     */
    private List<GaiaDataSynchronized> list;


}