package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "GAIA_SD_SERVER_DIFF")
public class GaiaSdServerDiff implements Serializable {
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSSD_BR_ID")
    private String gssdBrId;

    @Id
    @Column(name = "GSSD_ID")
    private String gssdId;

    @Column(name = "GSSD_COMPCLASS")
    private String gssdCompclass;

    @Column(name = "GSSD_COMPCLASS_REMARK")
    private String gssdCompclassRemark;

    @Column(name = "GSSD_COMPCLASS_NAME")
    private String gssdCompclassName;

    @Column(name = "GSSD_RELE_COMPCLASS")
    private String gssdReleCompclass;

    @Column(name = "GSSD_RELE_COMPCLASS_REMARK")
    private String gssdReleCompclassRemark;

    @Column(name = "GSSD_RELE_COMPCLASS_NAME")
    private String gssdReleCompclassName;

    @Column(name = "GSSD_PRIORITY")
    private Short gssdPriority;

    @Column(name = "GSSD_EXPLAIN1")
    private String gssdExplain1;

    @Column(name = "GSSD_EXPLAIN2")
    private String gssdExplain2;

    @Column(name = "GSSD_EXPLAIN3")
    private String gssdExplain3;

    @Column(name = "GSSD_EXPLAIN4")
    private String gssdExplain4;

    @Column(name = "GSSD_EXPLAIN5")
    private String gssdExplain5;

    @Column(name = "GSSD_UPDATE_DATE")
    private String gssdUpdateDate;

}
