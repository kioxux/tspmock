package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductBasicImage;
import com.gys.common.base.BaseMapper;

public interface GaiaProductBasicImageMapper extends BaseMapper<GaiaProductBasicImage> {
}