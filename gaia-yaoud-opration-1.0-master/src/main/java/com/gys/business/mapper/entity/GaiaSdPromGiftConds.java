package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 赠品类促销条件商品设置表
 */
@Table(name = "GAIA_SD_PROM_GIFT_CONDS")
@Data
public class GaiaSdPromGiftConds implements Serializable {
    private static final long serialVersionUID = 7793250989729732516L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPGC_VOUCHER_ID")
    private String gspgcVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPGC_SERIAL")
    private String gspgcSerial;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSPGC_PRO_ID")
    private String gspgcProId;

    /**
     * 系列编码
     */
    @Column(name = "GSPGC_SERIES_ID")
    private String gspgcSeriesId;

    /**
     * 是否会员
     */
    @Column(name = "GSPGC_MEM_FLAG")
    private String gspgcMemFlag;

    /**
     * 是否积分
     */
    @Column(name = "GSPGC_INTE_FLAG")
    private String gspgcInteFlag;

    /**
     * 积分倍率
     */
    @Column(name = "GSPGC_INTE_RATE")
    private String gspgcInteRate;

    /**
     * 会员等级
     */
    @Column(name = "GSPGC_MEMBERSHIP_GRADE")
    private String gspgcMembershipGrade;

}
