//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromAssoResult;
import com.gys.business.service.data.PromAssoResultOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdPromAssoResultMapper extends BaseMapper<GaiaSdPromAssoResult> {
    List<PromAssoResultOutData> getDetail(PromoteInData inData);
}
