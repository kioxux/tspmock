package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "GAIA_SD_PRINT_SALE")
public class GaiaSdPrintSale implements Serializable {
    private static final long serialVersionUID = -2253354600588667035L;

    @ApiModelProperty(value = "加盟商")
    @Id
    @Column( name = "CLIENT")
    private String clientId;

    @ApiModelProperty(value = "店号")
    @Id
    @Column(name = "GSPS_BR_ID")
    private String gspsBrId;


    @ApiModelProperty(value = "方案 01:方案一 / 02:方案二 /03:方案三")
    @Column(name = "GSPS_LOAD_PROJECT")
    private String gspsLoadProject;


    @ApiModelProperty(value = "模块一")
    @Column(name = "GSPS_MODULE1")
    private String gspsModule1;

    @ApiModelProperty(value = "模块五")
    @Column(name = "GSPS_MODULE5")
    private String gspsModule5;

    @ApiModelProperty(value = "模块六")
    @Column(name = "GSPS_MODULE6")
    private String gspsModule6;

}
