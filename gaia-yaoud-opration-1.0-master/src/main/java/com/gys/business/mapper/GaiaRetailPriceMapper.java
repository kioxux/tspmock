package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaRetailPrice;
import com.gys.business.service.data.GaiaRetailPriceInData;
import com.gys.common.base.BaseMapper;

public interface GaiaRetailPriceMapper extends BaseMapper<GaiaRetailPrice> {
    String afterSplitePrice(GaiaRetailPriceInData inData);
}
