package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 疾病大类-疾病中类关系表
 * </p>
 *
 * @author flynn
 * @since 2021-09-02
 */
@Data
@Table(name = "GAIA_OAS_SICKNESS_CODING_TAG")
public class OasSicknessCodingTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "KEY自增")
    @Id
    @Column(name = "ID")
    private Long id;

    @ApiModelProperty(value = "疾病标签")
    @Column(name = "SIO_SICKNESS_TAG")
    private String sioSicknessTag;

    @ApiModelProperty(value = "疾病编码")
    @Column(name = "SIO_SICKNESS_CODING")
    private String sioSicknessCoding;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "SIO_CJRQ")
    private LocalDate sioCjrq;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "SIO_CJSJ")
    private LocalDateTime sioCjsj;

    @ApiModelProperty(value = "创建人")
    @Column(name = "SIO_CJR")
    private String sioCjr;

    @ApiModelProperty(value = "修改日期")
    @Column(name = "SIO_XGRQ")
    private LocalDate sioXgrq;

    @ApiModelProperty(value = "修改时间")
    @Column(name = "SIO_XGSJ")
    private LocalDateTime sioXgsj;

    @ApiModelProperty(value = "修改人")
    @Column(name = "SIO_XGR")
    private String sioXgr;


}
