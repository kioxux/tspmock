package com.gys.business.mapper;

import com.gys.business.service.data.GaiaProductSpecialChange;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductSpecialChangeMapper {

    int deleteByPrimaryKey(Long id);

    int insert(GaiaProductSpecialChange record);

    int insertSelective(GaiaProductSpecialChange record);

    GaiaProductSpecialChange selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaProductSpecialChange record);

    int updateByPrimaryKey(GaiaProductSpecialChange record);

    int batchInsert(@Param("list") List<GaiaProductSpecialChange> list);

    int batchReplaceInsert(@Param("list") List<GaiaProductSpecialChange> list);
}