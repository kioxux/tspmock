package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRechargeCard;
import com.gys.business.mapper.entity.GaiaSdRechargeCardChange;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdRechargeCardChangeMapper extends BaseMapper<GaiaSdRechargeCardChange> {
    String selectNextVoucherId(@Param("client") String client, @Param("codePre") String codePre);

    List<String> determineWhetherTheTableExists(@Param("tableName") String tableName);

    int createTableBasic(@Param("tableName") String tableName);

    int createTableMemberCard(@Param("tableName") String tableName);

    int createTableRecharge(@Param("tableName") String tableName);

    void insertBanch(@Param("list") List<GaiaSdRechargeCard> list,@Param("table") String table );

}
