package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaNewProductEvaluationD;
import com.gys.business.service.data.GaiaNewProductEvaluationOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaNewProductEvaluationDMapper extends BaseMapper<GaiaNewProductEvaluationD> {

    void insertDetailList(List<GaiaNewProductEvaluationD> list);

    void updateList(List<GaiaNewProductEvaluationD> details);

    Integer getUnDealCount(GaiaNewProductEvaluationOutData param);
}