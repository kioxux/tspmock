package com.gys.business.mapper.entity;

import com.gys.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/22 16:31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DcOutStockD extends BaseEntity {
    /**加盟商*/
    private String client;
    /**缺断货推送流水号*/
    private String voucherId;
    /**商品编码*/
    private String proSelfCode;
    /**通用名称*/
    private String proCommonName;
    /**商品单位*/
    private String proUnit;
    /**商品规格*/
    private String proSpecs;
    /**生产厂家*/
    private String factoryName;
    /**品类模型排名*/
    private Integer modelSort;
    /**月均销量*/
    private BigDecimal monthSaleNum;
    /**月均销售额*/
    private BigDecimal monthSaleAmount;
    /**月均毛利额*/
    private BigDecimal monthProfitAmount;
    /**状态：0-未处理 1-已处理 2-已淘汰*/
    private Integer status;
    /**版本号*/
    private Integer version;
}
