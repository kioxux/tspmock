package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWmsMenDianHuoWeiHao;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface GaiaWmsMenDianHuoWeiHaoMapper {


    GaiaWmsMenDianHuoWeiHao selectOne(@Param("client") String client, @Param("gssdBrId") String gssdBrId, @Param("gssdProId") String gssdProId);

    List<GaiaWmsMenDianHuoWeiHao> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);
}