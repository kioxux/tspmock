//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdReplenishPara;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdReplenishParaMapper extends BaseMapper<GaiaSdReplenishPara> {

    void batchReplenishSdParam(List<GaiaSdReplenishPara> list);
}
