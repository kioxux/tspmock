package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBill;
import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBillDetail;
import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBillRelation;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillDetailVO;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillInData;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillOutData;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 重点商品任务—主表(GaiaKeyCommodityTaskBill)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-01 15:37:54
 */
public interface GaiaKeyCommodityTaskBillMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaKeyCommodityTaskBill queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaKeyCommodityTaskBill> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaKeyCommodityTaskBill 实例对象
     * @return 对象列表
     */
    List<GaiaKeyCommodityTaskBill> queryAll(GaiaKeyCommodityTaskBill gaiaKeyCommodityTaskBill);

    /**
     * 新增数据
     *
     * @param gaiaKeyCommodityTaskBill 实例对象
     * @return 影响行数
     */
    int insert(GaiaKeyCommodityTaskBill gaiaKeyCommodityTaskBill);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaKeyCommodityTaskBill> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaKeyCommodityTaskBill> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaKeyCommodityTaskBill> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaKeyCommodityTaskBill> entities);

    /**
     * 修改数据
     *
     * @param gaiaKeyCommodityTaskBill 实例对象
     * @return 影响行数
     */
    int update(GaiaKeyCommodityTaskBill gaiaKeyCommodityTaskBill);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    String getBillCode(GaiaKeyCommodityTaskBillInData param);

    List<GaiaKeyCommodityTaskBill> listBillInfo(GaiaKeyCommodityTaskBillInData inData);

    GaiaKeyCommodityTaskBillRelation getSalesInfo(@Param("client") String client, @Param("stoCodeList") List<String> stoCodeList
            , @Param("proSelfCode") String proSelfCode, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

    GaiaKeyCommodityTaskBill getBillInfoByCode(GaiaKeyCommodityTaskBill param);

    List<HashMap<String,Object>> listStores();

    GaiaKeyCommodityTaskBillDetailVO getThreeMonthSalesInfo(@Param("client")String client, @Param("stoCode")String stoCode);

    int updateStoreLevel(GaiaKeyCommodityTaskBillDetailVO salesInfo);

    HashMap<String,String> getImportTaskHBTime(GaiaKeyCommodityTaskBillInData inData);
}

