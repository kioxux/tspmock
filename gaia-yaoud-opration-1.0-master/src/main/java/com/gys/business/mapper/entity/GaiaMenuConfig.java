package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "GAIA_MENU_CONFIG")
public class GaiaMenuConfig implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 菜单编码
     */
    @Id
    @Column(name = "MENU_ID")
    private Long menuId;

    /**
     * 状态(1.启用 2.删除 )
     */
    @Column(name = "STATUS")
    private String status;

    /**
     * 创建人
     */
    @Column(name = "CREATOR")
    private String creator;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 更新人
     */
    @Column(name = "UPDATOR")
    private String updator;

    /**
     * 更新时间

     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取菜单编码
     *
     * @return MENU_ID - 菜单编码
     */
    public Long getMenuId() {
        return menuId;
    }

    /**
     * 设置菜单编码
     *
     * @param menuId 菜单编码
     */
    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    /**
     * 获取状态(1.启用 2.删除 )
     *
     * @return STATUS - 状态(1.启用 2.删除 )
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态(1.启用 2.删除 )
     *
     * @param status 状态(1.启用 2.删除 )
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取创建人
     *
     * @return CREATOR - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_DATE - 创建时间
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间
     *
     * @param createDate 创建时间
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新人
     *
     * @return UPDATOR - 更新人
     */
    public String getUpdator() {
        return updator;
    }

    /**
     * 设置更新人
     *
     * @param updator 更新人
     */
    public void setUpdator(String updator) {
        this.updator = updator;
    }

    /**
     * 获取更新时间

     *
     * @return UPDATE_DATE - 更新时间

     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间

     *
     * @param updateDate 更新时间

     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}