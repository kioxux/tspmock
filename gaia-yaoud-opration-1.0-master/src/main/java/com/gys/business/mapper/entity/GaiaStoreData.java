package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
@Data
@Table(name = "GAIA_STORE_DATA")
public class GaiaStoreData implements Serializable {
    private static final long serialVersionUID = 830472638959208973L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    @ApiModelProperty(value = "加盟商")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String client;

    /**
     * 门店编码
     */
    @Id
    @Column(name = "STO_CODE")
    @ApiModelProperty(value = "门店编码")
    private String stoCode;

    /**
     * 门店名称
     */
    @Column(name = "STO_NAME")
    @ApiModelProperty(value = "门店名称")
    private String stoName;

    /**
     * 助记码
     */
    @Column(name = "STO_PYM")
    @ApiModelProperty(value = "助记码")
    private String stoPym;

    /**
     * 门店简称
     */
    @Column(name = "STO_SHORT_NAME")
    @ApiModelProperty(value = "门店简称")
    private String stoShortName;

    /**
     * 门店属性
     */
    @Column(name = "STO_ATTRIBUTE")
    @ApiModelProperty(value = "门店属性")
    private String stoAttribute;

    /**
     * 配送方式
     */
    @Column(name = "STO_DELIVERY_MODE")
    @ApiModelProperty(value = "配送方式")
    private String stoDeliveryMode;

    /**
     * 关联门店
     */
    @Column(name = "STO_RELATION_STORE")
    @ApiModelProperty(value = "关联门店")
    private String stoRelationStore;

    /**
     * 门店状态
     */
    @Column(name = "STO_STATUS")
    @ApiModelProperty(value = "门店状态")
    private String stoStatus;

    /**
     * 经营面积
     */
    @Column(name = "STO_AREA")
    @ApiModelProperty(value = "经营面积")
    private String stoArea;

    /**
     * 开业日期
     */
    @Column(name = "STO_OPEN_DATE")
    @ApiModelProperty(value = "开业日期")
    private String stoOpenDate;

    /**
     * 关店日期
     */
    @Column(name = "STO_CLOSE_DATE")
    @ApiModelProperty(value = "关店日期")
    private String stoCloseDate;

    /**
     * 详细地址
     */
    @Column(name = "STO_ADD")
    @ApiModelProperty(value = "详细地址")
    private String stoAdd;

    /**
     * 省
     */
    @Column(name = "STO_PROVINCE")
    @ApiModelProperty(value = "省")
    private String stoProvince;

    /**
     * 城市
     */
    @Column(name = "STO_CITY")
    @ApiModelProperty(value = "城市")
    private String stoCity;

    /**
     * 区/县
     */
    @Column(name = "STO_DISTRICT")
    @ApiModelProperty(value = "区/县")
    private String stoDistrict;

    /**
     * 是否医保店
     */
    @Column(name = "STO_IF_MEDICALCARE")
    @ApiModelProperty(value = "是否医保店")
    private String stoIfMedicalcare;

    /**
     * DTP
     */
    @Column(name = "STO_IF_DTP")
    @ApiModelProperty(value = "DTP")
    private String stoIfDtp;

    /**
     * 税分类
     */
    @Column(name = "STO_TAX_CLASS")
    @ApiModelProperty(value = "税分类")
    private String stoTaxClass;

    /**
     * 委托配送公司
     */
    @Column(name = "STO_DELIVERY_COMPANY")
    @ApiModelProperty(value = "委托配送公司")
    private String stoDeliveryCompany;

    /**
     * 连锁总部
     */
    @Column(name = "STO_CHAIN_HEAD")
    @ApiModelProperty(value = "连锁总部")
    private String stoChainHead;

    /**
     * 纳税主体
     */
    @Column(name = "STO_TAX_SUBJECT")
    @ApiModelProperty(value = "纳税主体")
    private String stoTaxSubject;

    /**
     * 税率
     */
    @Column(name = "STO_TAX_RATE")
    @ApiModelProperty(value = "税率")
    private String stoTaxRate;

    /**
     * 统一社会信用代码
     */
    @Column(name = "STO_NO")
    @ApiModelProperty(value = "统一社会信用代码")
    private String stoNo;

    /**
     * 法人
     */
    @Column(name = "STO_LEGAL_PERSON")
    @ApiModelProperty(value = "法人")
    private String stoLegalPerson;

    /**
     * 质量负责人
     */
    @Column(name = "STO_QUA")
    @ApiModelProperty(value = "质量负责人")
    private String stoQua;

    /**
     * 创建日期
     */
    @Column(name = "STO_CRE_DATE")
    @ApiModelProperty(value = "创建日期")
    private String stoCreDate;

    /**
     * 创建时间
     */
    @Column(name = "STO_CRE_TIME")
    @ApiModelProperty(value = "创建时间")
    private String stoCreTime;

    /**
     * 创建人账号
     */
    @Column(name = "STO_CRE_ID")
    @ApiModelProperty(value = "创建人账号")
    private String stoCreId;

    /**
     * 修改日期
     */
    @Column(name = "STO_MODI_DATE")
    @ApiModelProperty(value = "修改日期")
    private String stoModiDate;

    /**
     * 修改时间
     */
    @Column(name = "STO_MODI_TIME")
    @ApiModelProperty(value = "修改时间")
    private String stoModiTime;

    /**
     * 修改人账号
     */
    @Column(name = "STO_MODI_ID")
    @ApiModelProperty(value = "修改人账号")
    private String stoModiId;

    /**
     * LOGO地址
     */
    @Column(name = "STO_LOGO")
    @ApiModelProperty(value = "LOGO地址")
    private String stoLogo;

    /**
     * 门店组编码
     */
    @Column(name = "STOG_CODE")
    @ApiModelProperty(value = "门店组编码")
    private String stogCode;

    /**
     * 店长
     */
    @Column(name = "STO_LEADER")
    @ApiModelProperty(value = "店长")
    private String stoLeader;

    /**
     * 配送中心
     */
    @Column(name = "STO_DC_CODE")
    @ApiModelProperty(value = "配送中心")
    private String stoDcCode;

    @Column(name = "STO_MD_SITE")
    private String stoMdSite;

    @Column(name = "PUBLIC_CODE")
    private String publicCode;

    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;

    @Column(name = "STO_BANK_ACCOUNT")
    private String stoBankAccount;

    @Column(name = "STO_BANK_NAME")
    private String stoBankName;

    @Column(name = "STO_TELEPHONE")
    private String stoTelephone;

    @Column(name = "LONGITUDE")
    private String longitude;

    @Column(name = "LATITUDE")
    private String latitude;

    @Column(name = "LEVEL")
    private String level;

    /**
     * 医保机构编码
     */
    @Column(name = "STO_YBCODE")
    private String stoYbcode;

    @Transient
    private String gsstDailyOpenDate;

    @Transient
    private String gsstDailyCloseDate;

    @Transient
    private String porvinceCn;

    @Transient
    private String cityCn;

    @Transient
    private String districtCn;
}