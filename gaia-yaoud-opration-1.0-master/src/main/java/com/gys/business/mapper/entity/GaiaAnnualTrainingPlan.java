package com.gys.business.mapper.entity;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@CsvRow
@Table(name = "GAIA_ANNUAL_TRAINING_PLAN")
public class GaiaAnnualTrainingPlan implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 单号
     */
    @Column(name = "GATP_VOUCHER_ID")
    private String gatpVoucherId;

    /**
     * 年份
     */
    @CsvCell(title = "年份",index = 1, fieldNo = 1)
    @Column(name = "GATP_YEAR")
    private String gatpYear;

    /**
     * 计划编号
     */
    @CsvCell(title = "计划编号",index = 2, fieldNo = 1)
    @Column(name = "GATP_PLAN_ID")
    private String gatpPlanId;

    /**
     * 培训目标
     */
    @CsvCell(title = "培训目标",index = 3, fieldNo = 1)
    @Column(name = "GATP_GOALS")
    private String gatpGoals;

    /**
     * 培训时间
     */
    @CsvCell(title = "培训时间",index = 4, fieldNo = 1)
    @Column(name = "GATP_TRAINING_DATE")
    private Date gatpTrainingDate;

    /**
     * 培训内容
     */
    @CsvCell(title = "培训内容",index = 5, fieldNo = 1)
    @Column(name = "GATP_CONTENT")
    private String gatpContent;

    /**
     * 听课部门
     */
    @CsvCell(title = "听课部门",index = 7, fieldNo = 1)
    @Column(name = "GATP_ATTENDANCE_DEP")
    private String gatpAttendanceDep;

    /**
     * 听课人员
     */
    @CsvCell(title = "听课人员",index = 8, fieldNo = 1)
    @Column(name = "GATP_ATTENDANCE_STAFF")
    private String gatpAttendanceStaff;

    /**
     * 听课费用
     */
    @CsvCell(title = "预算费用",index = 9, fieldNo = 1)
    @Column(name = "GATP_BUDGET_EXPENSES")
    private BigDecimal gatpBudgetExpenses;

    /**
     * 授课人
     */
    @CsvCell(title = "授课人",index = 6, fieldNo = 1)
    @Column(name = "GATP_LECTURER")
    private String gatpLecturer;

    /**
     * 负责人
     */
    @CsvCell(title = "负责人",index = 14, fieldNo = 1)
    @Column(name = "GATP_DIRECTOR")
    private String gatpDirector;

    /**
     * 审核人
     */
    @CsvCell(title = "审核人",index = 12, fieldNo = 1)
    @Column(name = "GATP_REVIEWER")
    private String gatpReviewer;

    /**
     * 审核时间
     */
    @CsvCell(title = "审核时间",index = 13, fieldNo = 1)
    @Column(name = "GATP_AUDIT_DATE")
    private Date gatpAuditDate;

    /**
     * 效果反馈
     */
    @CsvCell(title = "效果反馈",index = 15, fieldNo = 1)
    @Column(name = "GATP_EFFECT_FEEDBACK")
    private String gatpEffectFeedback;

    /**
     * 实施总结
     */
    @CsvCell(title = "实施总结",index = 16, fieldNo = 1)
    @Column(name = "GATP_EXECUTIVE_SUMMAR")
    private String gatpExecutiveSummar;

    /**
     * 创建人
     */
    @CsvCell(title = "制订人",index = 10, fieldNo = 1)
    @Column(name = "GATP_MAKER")
    private String gatpMaker;

    /**
     * 创建时间
     */
    @CsvCell(title = "制订时间",index = 11, fieldNo = 1)
    @Column(name = "GATP_MAKING_DATE")
    private Date gatpMakingDate;

    /**
     * 更新人
     */
    @Column(name = "GATP_UPDATE")
    private String gatpUpdate;

    /**
     * 更新时间
     */
    @Column(name = "GATP_UPDATE_DATE")
    private Date gatpUpdateDate;

    /**
     * 备注
     */
    @CsvCell(title = "备注",index = 17, fieldNo = 1)
    @Column(name = "GATP_REMARKS")
    private String gatpRemarks;

    /**
     * 删除标记 0：未删除、1：以删除
     */
    @Column(name = "GATP_IS_DELETE")
    private Integer gatpIsDelete;

    /**
     * 版本号
     */
    @Column(name = "GATP_VERSION")
    private String gatpVersion;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取单号
     *
     * @return GATP_VOUCHER_ID - 单号
     */
    public String getGatpVoucherId() {
        return gatpVoucherId;
    }

    /**
     * 设置单号
     *
     * @param gatpVoucherId 单号
     */
    public void setGatpVoucherId(String gatpVoucherId) {
        this.gatpVoucherId = gatpVoucherId;
    }

    /**
     * 获取年份
     *
     * @return GATP_YEAR - 年份
     */
    public String getGatpYear() {
        return gatpYear;
    }

    /**
     * 设置年份
     *
     * @param gatpYear 年份
     */
    public void setGatpYear(String gatpYear) {
        this.gatpYear = gatpYear;
    }

    /**
     * 获取计划编号
     *
     * @return GATP_PLAN_ID - 计划编号
     */
    public String getGatpPlanId() {
        return gatpPlanId;
    }

    /**
     * 设置计划编号
     *
     * @param gatpPlanId 计划编号
     */
    public void setGatpPlanId(String gatpPlanId) {
        this.gatpPlanId = gatpPlanId;
    }

    /**
     * 获取培训目标
     *
     * @return GATP_GOALS - 培训目标
     */
    public String getGatpGoals() {
        return gatpGoals;
    }

    /**
     * 设置培训目标
     *
     * @param gatpGoals 培训目标
     */
    public void setGatpGoals(String gatpGoals) {
        this.gatpGoals = gatpGoals;
    }

    /**
     * 获取培训时间
     *
     * @return GATP_TRAINING_DATE - 培训时间
     */
    public Date getGatpTrainingDate() {
        return gatpTrainingDate;
    }

    /**
     * 设置培训时间
     *
     * @param gatpTrainingDate 培训时间
     */
    public void setGatpTrainingDate(Date gatpTrainingDate) {
        this.gatpTrainingDate = gatpTrainingDate;
    }

    /**
     * 获取培训内容
     *
     * @return GATP_CONTENT - 培训内容
     */
    public String getGatpContent() {
        return gatpContent;
    }

    /**
     * 设置培训内容
     *
     * @param gatpContent 培训内容
     */
    public void setGatpContent(String gatpContent) {
        this.gatpContent = gatpContent;
    }

    /**
     * 获取听课部门
     *
     * @return GATP_ATTENDANCE_DEP - 听课部门
     */
    public String getGatpAttendanceDep() {
        return gatpAttendanceDep;
    }

    /**
     * 设置听课部门
     *
     * @param gatpAttendanceDep 听课部门
     */
    public void setGatpAttendanceDep(String gatpAttendanceDep) {
        this.gatpAttendanceDep = gatpAttendanceDep;
    }

    /**
     * 获取听课人员
     *
     * @return GATP_ATTENDANCE_STAFF - 听课人员
     */
    public String getGatpAttendanceStaff() {
        return gatpAttendanceStaff;
    }

    /**
     * 设置听课人员
     *
     * @param gatpAttendanceStaff 听课人员
     */
    public void setGatpAttendanceStaff(String gatpAttendanceStaff) {
        this.gatpAttendanceStaff = gatpAttendanceStaff;
    }

    /**
     * 获取听课费用
     *
     * @return GATP_BUDGET_EXPENSES - 听课费用
     */
    public BigDecimal getGatpBudgetExpenses() {
        return gatpBudgetExpenses;
    }

    /**
     * 设置听课费用
     *
     * @param gatpBudgetExpenses 听课费用
     */
    public void setGatpBudgetExpenses(BigDecimal gatpBudgetExpenses) {
        this.gatpBudgetExpenses = gatpBudgetExpenses;
    }

    /**
     * 获取授课人
     *
     * @return GATP_LECTURER - 授课人
     */
    public String getGatpLecturer() {
        return gatpLecturer;
    }

    /**
     * 设置授课人
     *
     * @param gatpLecturer 授课人
     */
    public void setGatpLecturer(String gatpLecturer) {
        this.gatpLecturer = gatpLecturer;
    }

    /**
     * 获取负责人
     *
     * @return GATP_DIRECTOR - 负责人
     */
    public String getGatpDirector() {
        return gatpDirector;
    }

    /**
     * 设置负责人
     *
     * @param gatpDirector 负责人
     */
    public void setGatpDirector(String gatpDirector) {
        this.gatpDirector = gatpDirector;
    }

    /**
     * 获取审核人
     *
     * @return GATP_REVIEWER - 审核人
     */
    public String getGatpReviewer() {
        return gatpReviewer;
    }

    /**
     * 设置审核人
     *
     * @param gatpReviewer 审核人
     */
    public void setGatpReviewer(String gatpReviewer) {
        this.gatpReviewer = gatpReviewer;
    }

    /**
     * 获取审核时间
     *
     * @return GATP_AUDIT_DATE - 审核时间
     */
    public Date getGatpAuditDate() {
        return gatpAuditDate;
    }

    /**
     * 设置审核时间
     *
     * @param gatpAuditDate 审核时间
     */
    public void setGatpAuditDate(Date gatpAuditDate) {
        this.gatpAuditDate = gatpAuditDate;
    }

    /**
     * 获取效果反馈
     *
     * @return GATP_EFFECT_FEEDBACK - 效果反馈
     */
    public String getGatpEffectFeedback() {
        return gatpEffectFeedback;
    }

    /**
     * 设置效果反馈
     *
     * @param gatpEffectFeedback 效果反馈
     */
    public void setGatpEffectFeedback(String gatpEffectFeedback) {
        this.gatpEffectFeedback = gatpEffectFeedback;
    }

    /**
     * 获取实施总结
     *
     * @return GATP_EXECUTIVE_SUMMAR - 实施总结
     */
    public String getGatpExecutiveSummar() {
        return gatpExecutiveSummar;
    }

    /**
     * 设置实施总结
     *
     * @param gatpExecutiveSummar 实施总结
     */
    public void setGatpExecutiveSummar(String gatpExecutiveSummar) {
        this.gatpExecutiveSummar = gatpExecutiveSummar;
    }

    /**
     * 获取创建人
     *
     * @return GATP_MAKER - 创建人
     */
    public String getGatpMaker() {
        return gatpMaker;
    }

    /**
     * 设置创建人
     *
     * @param gatpMaker 创建人
     */
    public void setGatpMaker(String gatpMaker) {
        this.gatpMaker = gatpMaker;
    }

    /**
     * 获取创建时间
     *
     * @return GATP_MAKING_DATE - 创建时间
     */
    public Date getGatpMakingDate() {
        return gatpMakingDate;
    }

    /**
     * 设置创建时间
     *
     * @param gatpMakingDate 创建时间
     */
    public void setGatpMakingDate(Date gatpMakingDate) {
        this.gatpMakingDate = gatpMakingDate;
    }

    /**
     * 获取更新人
     *
     * @return GATP_UPDATE - 更新人
     */
    public String getGatpUpdate() {
        return gatpUpdate;
    }

    /**
     * 设置更新人
     *
     * @param gatpUpdate 更新人
     */
    public void setGatpUpdate(String gatpUpdate) {
        this.gatpUpdate = gatpUpdate;
    }

    /**
     * 获取更新时间
     *
     * @return GATP_UPDATE_DATE - 更新时间
     */
    public Date getGatpUpdateDate() {
        return gatpUpdateDate;
    }

    /**
     * 设置更新时间
     *
     * @param gatpUpdateDate 更新时间
     */
    public void setGatpUpdateDate(Date gatpUpdateDate) {
        this.gatpUpdateDate = gatpUpdateDate;
    }

    /**
     * 获取备注
     *
     * @return GATP_REMARKS - 备注
     */
    public String getGatpRemarks() {
        return gatpRemarks;
    }

    /**
     * 设置备注
     *
     * @param gatpRemarks 备注
     */
    public void setGatpRemarks(String gatpRemarks) {
        this.gatpRemarks = gatpRemarks;
    }

    /**
     * 获取删除标记 0：未删除、1：以删除
     *
     * @return GATP_IS_DELETE - 删除标记 0：未删除、1：以删除
     */
    public Integer getGatpIsDelete() {
        return gatpIsDelete;
    }

    /**
     * 设置删除标记 0：未删除、1：以删除
     *
     * @param gatpIsDelete 删除标记 0：未删除、1：以删除
     */
    public void setGatpIsDelete(Integer gatpIsDelete) {
        this.gatpIsDelete = gatpIsDelete;
    }

    /**
     * 获取版本号
     *
     * @return GATP_VERSION - 版本号
     */
    public String getGatpVersion() {
        return gatpVersion;
    }

    /**
     * 设置版本号
     *
     * @param gatpVersion 版本号
     */
    public void setGatpVersion(String gatpVersion) {
        this.gatpVersion = gatpVersion;
    }
}