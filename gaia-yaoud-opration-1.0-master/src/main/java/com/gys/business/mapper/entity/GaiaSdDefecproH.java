package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(
        name = "GAIA_SD_DEFECPRO_H"
)
public class GaiaSdDefecproH {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "GSDH_BR_ID"
    )
    private String gsdhBrId;
    @Column(
            name = "GSDH_VOUCHER_ID"
    )
    private String gsdhVoucherId;
    @Column(
            name = "GSDH_DATE"
    )
    private String gsdhDate;
    @Column(
            name = "GSDH_STATUS"
    )
    private String gsdhStatus;
    @Column(
            name = "GSDH_EMP"
    )
    private String gsdhEmp;
    @Column(
            name = "GSDH_REMAKS"
    )
    private String gsdhRemarks;
    @Column(
            name = "GSDH_FLAG1"
    )
    private String gsdhFlag1;
    @Column(
            name = "GSDH_FLAG2"
    )
    private String gsdhFlag2;
    @Column(
            name = "GSDH_FLAG3"
    )
    private String gsdhFlag3;


}
