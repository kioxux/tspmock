package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPrintSale;
import com.gys.business.service.data.GaiaSdPrintSaleDto;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPrintSaleMapper extends BaseMapper<GaiaSdPrintSale> {

    List<GaiaSdPrintSale> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);


    GaiaSdPrintSale getPrintSaleByClientAndBrId(@Param("clientId") String clientId, @Param("brId") String brId);

    GaiaSdPrintSaleDto getPrintSale(@Param("client") String clientId, @Param("brId") String brId);
}
