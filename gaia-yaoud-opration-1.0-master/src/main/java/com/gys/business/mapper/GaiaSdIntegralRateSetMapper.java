package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaSdIntegralRateSet;
import com.gys.common.base.BaseMapper;
import com.gys.common.request.InsertProductSettingReq;
import com.gys.common.request.SelectCheckIfInDbReq;
import com.gys.common.request.SelectInteProductListReq;
import com.gys.common.response.SelectInteProductListResponse;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * <p>
 * 积分商品设置表 Mapper 接口
 * </p>
 *
 * @author zhangjiahao
 * @since 2021-07-28
 */
public interface GaiaSdIntegralRateSetMapper extends BaseMapper<GaiaSdIntegralRateSet> {

    @Options(useGeneratedKeys = true, keyProperty = "ID", keyColumn = "ID")
    void insertProductSetting(@Param(value = "list") List<InsertProductSettingReq.InsertProductSetting> reqList);

    List<SelectInteProductListResponse> selectProductPageList(@Param(value = "list")SelectInteProductListReq req);


    List<GaiaSdIntegralRateSet> getAllByClient(@Param("client")String client,@Param("brId") String depId, @Param("nowDate")String format);

    List<String> selectProIds(@Param("req")SelectCheckIfInDbReq req);


    List<GaiaSdIntegralRateSet> getAllPointsProducts(@Param("client")String clientId, @Param("brId")String brId,@Param("proIds") List<String> prolist);
}
