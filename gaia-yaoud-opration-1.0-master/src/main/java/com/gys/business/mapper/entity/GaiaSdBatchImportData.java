package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 门店手工流向数据表
 * </p>
 *
 * @author yifan.wang
 * @since 2021-08-04
 */
@Data
@Table(name = "GAIA_SD_BATCH_IMPORT_DATA")
public class GaiaSdBatchImportData implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "ID")
    private String client;

    @ApiModelProperty(value = "店号")
    @Column(name = "GSBC_BR_ID")
    private String gsbcBrId;

    @ApiModelProperty(value = "异动业务单号")
    @Column(name = "GSBC_VOUCHER_ID")
    private String gsbcVoucherId;

    @ApiModelProperty(value = "异动日期")
    @Column(name = "GSBC_DATE")
    private String gsbcDate;

    @ApiModelProperty(value = "行号")
    @Column(name = "GSBC_SERIAL")
    private String gsbcSerial;

    @ApiModelProperty(value = "商品")
    @Column(name = "GSBC_PRO_ID")
    private String gsbcProId;

    @ApiModelProperty(value = "批号")
    @Column(name = "GSBC_BATCH_NO")
    private String gsbcBatchNo;

    @ApiModelProperty(value = "批次")
    @Column(name = "GSBC_BATCH")
    private String gsbcBatch;

    @ApiModelProperty(value = "数量")
    @Column(name = "GSBC_QTY")
    private BigDecimal gsbcQty;

    @ApiModelProperty(value = "原库存数量")
    @Column(name = "GSBC_OLD_QTY")
    private BigDecimal gsbcOldQty;


}
