//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(
        name = "GAIA_SD_RECHARGE_CARD_PAYCHECK"
)
public class GaiaSdRechargeCardPaycheckOld implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSRCP_VOUCHER_ID"
    )
    private String gsrcpVoucherId;
    @Column(
            name = "GSRCP_CARD_ID"
    )
    private String gsrcpCardId;
    @Column(
            name = "GSRCP_BR_ID"
    )
    private String gsrcpBrId;
    @Column(
            name = "GSRCP_DATE"
    )
    private String gsrcpDate;
    @Column(
            name = "GSRCP_TIME"
    )
    private String gsrcpTime;
    @Column(
            name = "GSRCP_EMP"
    )
    private String gsrcpEmp;
    @Column(
            name = "GSRCP_BEFORE_AMT"
    )
    private BigDecimal gsrcpBeforeAmt;
    @Column(
            name = "GSRCP_PAY_AMT"
    )
    private BigDecimal gsrcpPayAmt;
    @Column(
            name = "GSRCP_PROPORTION"
    )
    private String gsrcpProportion;
    @Column(
            name = "GSRCP_PROMOTION"
    )
    private String gsrcpPromotion;
    @Column(
            name = "GSRCP_AFTER_AMT"
    )
    private BigDecimal gsrcpAfterAmt;
    @Column(
            name = "GSRCP_RECHARGE_AMT"
    )
    private BigDecimal gsrcpRechargeAmt;
    @Column(
            name = "GSRCP_YS_AMT"
    )
    private BigDecimal gsrcpYsAmt;
    @Column(
            name = "GSRCP_RMB_ZL_AMT"
    )
    private BigDecimal gsrcpRmbZlAmt;
    @Column(
            name = "GSRCP_RMB_AMT"
    )
    private BigDecimal gsrcpRmbAmt;
    @Column(
            name = "GSRCP_PAYMENT_NO1"
    )
    private String gsrcpPaymentNo1;
    @Column(
            name = "GSRCP_PAYMENT_AMT1"
    )
    private BigDecimal gsrcpPaymentAmt1;
    @Column(
            name = "GSRCP_PAYMENT_NO2"
    )
    private String gsrcpPaymentNo2;
    @Column(
            name = "GSRCP_PAYMENT_AMT2"
    )
    private BigDecimal gsrcpPaymentAmt2;
    @Column(
            name = "GSRCP_PAYMENT_NO3"
    )
    private String gsrcpPaymentNo3;
    @Column(
            name = "GSRCP_PAYMENT_AMT3"
    )
    private BigDecimal gsrcpPaymentAmt3;
    @Column(
            name = "GSRCP_PAYMENT_NO4"
    )
    private String gsrcpPaymentNo4;
    @Column(
            name = "GSRCP_PAYMENT_AMT4"
    )
    private BigDecimal gsrcpPaymentAmt4;
    private static final long serialVersionUID = 1L;
}
