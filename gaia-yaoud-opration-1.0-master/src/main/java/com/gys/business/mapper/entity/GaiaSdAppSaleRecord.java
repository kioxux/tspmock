package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@ApiModel(value="com-gys-business-mapper-entity-GaiaSdAppSaleRecord")
@Data
public class GaiaSdAppSaleRecord {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 年份
    */
    @ApiModelProperty(value="年份")
    private String years;

    /**
    * 月份
    */
    @ApiModelProperty(value="月份")
    private String months;

    /**
    * 1-药品 2-OTC 3-中药 888-药品小计 999-非药品
    */
    @ApiModelProperty(value="1-药品 2-OTC 3-中药 888-药品小计 999-非药品")
    private Integer productType;

    /**
    * 动销品项数
    */
    @ApiModelProperty(value="动销品项数")
    private String movProductCount;

    /**
    * 毛利率
    */
    @ApiModelProperty(value="毛利率")
    private BigDecimal profitRate;

    /**
    * 销售额
    */
    @ApiModelProperty(value="销售额")
    private BigDecimal saleAmt;

    /**
    * 推送时间
    */
    @ApiModelProperty(value="推送时间")
    private Date createTime;

}