package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaStoreDiscount;
import com.gys.business.mapper.entity.GaiaStoreDiscountData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaStoreDiscountMapper {

    List<String> findStoTaxRateList(@Param("client") String client);

    List<Map<String, Object>> findStoCodeList(Map<String,Object> inData);

    List<Map<String,Object>> findSalesSummaryByBrId(GaiaStoreDiscountData gaiaStoreDiscountData);

    GaiaStoreDiscountData findselectStoCood(GaiaStoreDiscountData gaiaStoreDiscountData);

    int insertStoreList(GaiaStoreDiscount gauaStoreDiscount);

    void updateGsdStoRebate(GaiaStoreDiscountData gaiaStoreDiscountData);
}
