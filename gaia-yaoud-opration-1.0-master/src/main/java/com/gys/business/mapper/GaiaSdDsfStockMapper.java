package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdDsfStock;
import com.gys.common.base.BaseMapper;

import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/16 14:03
 */
public interface GaiaSdDsfStockMapper extends BaseMapper<GaiaSdDsfStock> {
    List<String> getProIdCodeList(String client);
    void updateByDsfId(GaiaSdDsfStock request);
}
