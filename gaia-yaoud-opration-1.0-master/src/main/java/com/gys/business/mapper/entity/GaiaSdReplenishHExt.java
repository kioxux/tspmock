package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class GaiaSdReplenishHExt extends GaiaSdReplenishH implements Serializable {

    private String client;
}
