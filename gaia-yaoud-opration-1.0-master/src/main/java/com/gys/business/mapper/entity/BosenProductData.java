package com.gys.business.mapper.entity;

import com.gys.common.excel.StringUtil;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/9/18 10:08
 */
@Data
public class BosenProductData {
    private String centerCode;
    private String proCommonName;
    private String proEnName;
    private String proCategory;
    private String proForm;
    private String proSpec;
    private String factoryName;
    private String proRegisterNo;
    private String proRegisterDate;
    private String proBasicCode;

    public String getCenterCode() {
        return centerCode;
    }

    public String getProCommonName() {
        return proCommonName;
    }

    public String getProCategory() {
        return proCategory;
    }

    public String getProForm() {
        return StringUtil.isNotEmpty(proForm) && proForm.length() > 100 ? proForm.substring(0, 99) : proForm;
    }

    public String getFactoryName() {
        return StringUtil.isNotEmpty(factoryName) && factoryName.length() > 100 ? factoryName.substring(0, 99) : factoryName;
    }

    public String getProBasicCode() {
        return StringUtil.isNotEmpty(proBasicCode) && proBasicCode.length() > 50 ? proBasicCode.substring(0, 49) : proBasicCode;
    }

    public String getProSpec() {
        return StringUtil.isNotEmpty(proSpec) && proSpec.length() > 100 ? proSpec.substring(0, 99) : proSpec;
    }

    public String getProEnName() {
        return StringUtil.isNotEmpty(proEnName) && proEnName.length() > 100 ? proEnName.substring(0, 99) : proEnName;
    }
}
