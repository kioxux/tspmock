package com.gys.business.mapper;

import com.gys.business.mapper.entity.TichengProplanBasic;
import com.gys.business.service.data.percentageplan.PercentageInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 单品提成方案主表v5版 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-11-11
 */
@Mapper
public interface TichengProplanBasicMapper extends BaseMapper<TichengProplanBasic> {

    void deleteProPlan(@Param("id") Long planId);

    PercentageInData selectTichengZProDetail(@Param("tichengId") Long planId);

    void approveProPlan(@Param("id") Long planId, @Param("planStatus") String planStatus, @Param("planReason") String planReason);

    void stopProPlan(@Param("id") Long planId, @Param("planStatus") String planStatus, @Param("planStopDate") String planStopDate, @Param("planReason") String planReason);

    List<Map<String, Object>> selectProPlanDetailByClientAndTime(@Param("client") String client, @Param("startDate") String startDate, @Param("endDate") String endDate);

    List<Map<String, Object>> selectProPlanDetailByClientAndPid(@Param("client") String client, @Param("planId") Long planId);

    /**
     * run only once,only used to synchronize history data
     *
     * @return List<TichengProplanBasic>
     */
    List<TichengProplanBasic> selectSyncData();

    List<TichengProplanBasic> checkDate(PercentageInData inData);

    int insertBatch(List<TichengProplanBasic> tichengProplanBasics);

}
