package com.gys.business.mapper;

import com.gys.business.service.data.GspExpConfData;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface GspExpConfMapper {
    Integer insertGspExpConf(List<GspExpConfData> inData);
    Integer deleteGspExpConf(GspExpConfData inData);
    List<GspExpConfData> selectGspExpConfByWarnDays(GspExpConfData inData);
    List<GspExpConfData> selectGspExpConf(GspExpConfData inData);
}
