package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdChinmedRelateD;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdChinmedRelateDMapper extends BaseMapper<GaiaSdChinmedRelateD> {
    void insertBatch(List<GaiaSdChinmedRelateD> inData);
}
