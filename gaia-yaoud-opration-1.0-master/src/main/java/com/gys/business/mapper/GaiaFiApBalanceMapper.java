package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaFiApBalance;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 供应商应付期末余额 Mapper 接口
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Mapper
public interface GaiaFiApBalanceMapper extends BaseMapper<GaiaFiApBalance> {
    List<GaiaFiApBalance> getbeginPeriodBillOfAp(@Param("client")String client,@Param("year")String year, @Param("monthValue")String monthValue);

}
