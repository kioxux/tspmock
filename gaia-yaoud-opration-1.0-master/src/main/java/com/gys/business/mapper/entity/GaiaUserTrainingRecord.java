package com.gys.business.mapper.entity;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@CsvRow
@Table(name = "GAIA_USER_TRAINING_RECORD")
public class GaiaUserTrainingRecord implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 单号
     */
    @Column(name = "GUTR_VOUCHER_ID")
    private String gutrVoucherId;

    /**
     * 店号
     */
    @Column(name = "GUTR_BR_ID")
    private String gutrBrId;

    /**
     * 店名
     */
    @CsvCell(title = "部门",index = 1, fieldNo = 1)
    @Column(name = "GUTR_BR_NAME")
    private String gutrBrName;

    /**
     * 员工姓名
     */
    @CsvCell(title = "姓名",index = 2, fieldNo = 1)
    @Column(name = "GUTR_USER_NAME")
    private String gutrUserName;

    /**
     * 起始时间
     */
    @CsvCell(title = "起始日期",index = 3, fieldNo = 1)
    @Column(name = "GUTR_DATE_START")
    private Date gutrDateStart;

    /**
     * 终止时间
     */
    @CsvCell(title = "终止日期",index = 4, fieldNo = 1)
    @Column(name = "GUTR_DATE_END")
    private Date gutrDateEnd;

    /**
     * 培训类别
     */
    @CsvCell(title = "培训类别",index = 5, fieldNo = 1)
    @Column(name = "GUTR_CATEGORY")
    private String gutrCategory;

    /**
     * 培训类型
     */
    @CsvCell(title = "培训类型",index = 6, fieldNo = 1)
    @Column(name = "GUTR_TYPE")
    private String gutrType;

    /**
     * 培训内容
     */
    @CsvCell(title = "培训内容",index = 7, fieldNo = 1)
    @Column(name = "GUTR_CONTENT")
    private String gutrContent;

    /**
     * 主办单位
     */
    @CsvCell(title = "主办单位",index = 8, fieldNo = 1)
    @Column(name = "GUTR_ORG")
    private String gutrOrg;

    /**
     * 培训教师
     */
    @CsvCell(title = "培训教师",index = 9, fieldNo = 1)
    @Column(name = "GUTR_TRAINER")
    private String gutrTrainer;

    /**
     * 课时
     */
    @CsvCell(title = "课时",index = 10, fieldNo = 1)
    @Column(name = "GUTR_CLASS_HOUR")
    private String gutrClassHour;

    /**
     * 培训地点
     */
    @CsvCell(title = "培训地点",index = 11, fieldNo = 1)
    @Column(name = "GUTR_LOCATION")
    private String gutrLocation;

    /**
     * 成绩
     */
    @CsvCell(title = "成绩",index = 12, fieldNo = 1)
    @Column(name = "GUTR_SCORE")
    private String gutrScore;

    /**
     * 性别
     */
    @CsvCell(title = "性别",index = 13, fieldNo = 1)
    @Column(name = "GUTR_SEX")
    private String gutrSex;

    /**
     * 职务
     */
    @CsvCell(title = "职务",index = 14, fieldNo = 1)
    @Column(name = "GUTR_POST")
    private String gutrPost;

    /**
     * 职称
     */
    @CsvCell(title = "职称",index = 19, fieldNo = 1)
    @Column(name = "GUTR_TITLE")
    private String gutrTitle;

    /**
     * 岗位
     */
    @CsvCell(title = "岗位",index = 18, fieldNo = 1)
    @Column(name = "GUTR_JOB")
    private String gutrJob;

    /**
     * 学历
     */
    @CsvCell(title = "学历",index = 15, fieldNo = 1)
    @Column(name = "GUTR_EDUCATION")
    private String gutrEducation;

    /**
     * 资格证书
     */
    @CsvCell(title = "资格证书",index = 20, fieldNo = 1)
    @Column(name = "GUTR_QUALIFICATION")
    private String gutrQualification;

    /**
     * 出生日期
     */
    @CsvCell(title = "出生日期",index = 16, fieldNo = 1)
    @Column(name = "GUTR_BIRTHDAY")
    private Date gutrBirthday;

    /**
     * 入司日期
     */
    @CsvCell(title = "入司日期",index = 17, fieldNo = 1)
    @Column(name = "GUTR_JOIN_DATE")
    private Date gutrJoinDate;

    /**
     * 创建人
     */
    @Column(name = "GUTR_CREATE")
    private String gutrCreate;

    /**
     * 创建时间
     */
    @Column(name = "GUTR_CREATE_DATE")
    private Date gutrCreateDate;

    /**
     * 更新人
     */
    @Column(name = "GUTR_UPDATE")
    private String gutrUpdate;

    /**
     * 更新时间
     */
    @Column(name = "GUTR_UPDATE_DATE")
    private Date gutrUpdateDate;

    /**
     * 删除标记 0：未删除、1：以删除
     */
    @Column(name = "GUTR_IS_DELETE")
    private Integer gutrIsDelete;

    /**
     * 版本号
     */
    @Column(name = "GUTR_VERSION")
    private String gutrVersion;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取单号
     *
     * @return GUTR_VOUCHER_ID - 单号
     */
    public String getGutrVoucherId() {
        return gutrVoucherId;
    }

    /**
     * 设置单号
     *
     * @param gutrVoucherId 单号
     */
    public void setGutrVoucherId(String gutrVoucherId) {
        this.gutrVoucherId = gutrVoucherId;
    }

    /**
     * 获取店号
     *
     * @return GUTR_BR_ID - 店号
     */
    public String getGutrBrId() {
        return gutrBrId;
    }

    /**
     * 设置店号
     *
     * @param gutrBrId 店号
     */
    public void setGutrBrId(String gutrBrId) {
        this.gutrBrId = gutrBrId;
    }

    /**
     * 获取店名
     *
     * @return GUTR_BR_NAME - 店名
     */
    public String getGutrBrName() {
        return gutrBrName;
    }

    /**
     * 设置店名
     *
     * @param gutrBrName 店名
     */
    public void setGutrBrName(String gutrBrName) {
        this.gutrBrName = gutrBrName;
    }

    /**
     * 获取员工姓名
     *
     * @return GUTR_USER_NAME - 员工姓名
     */
    public String getGutrUserName() {
        return gutrUserName;
    }

    /**
     * 设置员工姓名
     *
     * @param gutrUserName 员工姓名
     */
    public void setGutrUserName(String gutrUserName) {
        this.gutrUserName = gutrUserName;
    }

    /**
     * 获取起始时间
     *
     * @return GUTR_DATE_START - 起始时间
     */
    public Date getGutrDateStart() {
        return gutrDateStart;
    }

    /**
     * 设置起始时间
     *
     * @param gutrDateStart 起始时间
     */
    public void setGutrDateStart(Date gutrDateStart) {
        this.gutrDateStart = gutrDateStart;
    }

    /**
     * 获取终止时间
     *
     * @return GUTR_DATE_END - 终止时间
     */
    public Date getGutrDateEnd() {
        return gutrDateEnd;
    }

    /**
     * 设置终止时间
     *
     * @param gutrDateEnd 终止时间
     */
    public void setGutrDateEnd(Date gutrDateEnd) {
        this.gutrDateEnd = gutrDateEnd;
    }

    /**
     * 获取培训类别
     *
     * @return GUTR_CATEGORY - 培训类别
     */
    public String getGutrCategory() {
        return gutrCategory;
    }

    /**
     * 设置培训类别
     *
     * @param gutrCategory 培训类别
     */
    public void setGutrCategory(String gutrCategory) {
        this.gutrCategory = gutrCategory;
    }

    /**
     * 获取培训类型
     *
     * @return GUTR_TYPE - 培训类型
     */
    public String getGutrType() {
        return gutrType;
    }

    /**
     * 设置培训类型
     *
     * @param gutrType 培训类型
     */
    public void setGutrType(String gutrType) {
        this.gutrType = gutrType;
    }

    /**
     * 获取培训内容
     *
     * @return GUTR_CONTENT - 培训内容
     */
    public String getGutrContent() {
        return gutrContent;
    }

    /**
     * 设置培训内容
     *
     * @param gutrContent 培训内容
     */
    public void setGutrContent(String gutrContent) {
        this.gutrContent = gutrContent;
    }

    /**
     * 获取主办单位
     *
     * @return GUTR_ORG - 主办单位
     */
    public String getGutrOrg() {
        return gutrOrg;
    }

    /**
     * 设置主办单位
     *
     * @param gutrOrg 主办单位
     */
    public void setGutrOrg(String gutrOrg) {
        this.gutrOrg = gutrOrg;
    }

    /**
     * 获取培训教师
     *
     * @return GUTR_TRAINER - 培训教师
     */
    public String getGutrTrainer() {
        return gutrTrainer;
    }

    /**
     * 设置培训教师
     *
     * @param gutrTrainer 培训教师
     */
    public void setGutrTrainer(String gutrTrainer) {
        this.gutrTrainer = gutrTrainer;
    }

    /**
     * 获取课时
     *
     * @return GUTR_CLASS_HOUR - 课时
     */
    public String getGutrClassHour() {
        return gutrClassHour;
    }

    /**
     * 设置课时
     *
     * @param gutrClassHour 课时
     */
    public void setGutrClassHour(String gutrClassHour) {
        this.gutrClassHour = gutrClassHour;
    }

    /**
     * 获取培训地点
     *
     * @return GUTR_LOCATION - 培训地点
     */
    public String getGutrLocation() {
        return gutrLocation;
    }

    /**
     * 设置培训地点
     *
     * @param gutrLocation 培训地点
     */
    public void setGutrLocation(String gutrLocation) {
        this.gutrLocation = gutrLocation;
    }

    /**
     * 获取成绩
     *
     * @return GUTR_SCORE - 成绩
     */
    public String getGutrScore() {
        return gutrScore;
    }

    /**
     * 设置成绩
     *
     * @param gutrScore 成绩
     */
    public void setGutrScore(String gutrScore) {
        this.gutrScore = gutrScore;
    }

    /**
     * 获取性别
     *
     * @return GUTR_SEX - 性别
     */
    public String getGutrSex() {
        return gutrSex;
    }

    /**
     * 设置性别
     *
     * @param gutrSex 性别
     */
    public void setGutrSex(String gutrSex) {
        this.gutrSex = gutrSex;
    }

    /**
     * 获取职务
     *
     * @return GUTR_POST - 职务
     */
    public String getGutrPost() {
        return gutrPost;
    }

    /**
     * 设置职务
     *
     * @param gutrPost 职务
     */
    public void setGutrPost(String gutrPost) {
        this.gutrPost = gutrPost;
    }

    /**
     * 获取职称
     *
     * @return GUTR_TITLE - 职称
     */
    public String getGutrTitle() {
        return gutrTitle;
    }

    /**
     * 设置职称
     *
     * @param gutrTitle 职称
     */
    public void setGutrTitle(String gutrTitle) {
        this.gutrTitle = gutrTitle;
    }

    /**
     * 获取岗位
     *
     * @return GUTR_JOB - 岗位
     */
    public String getGutrJob() {
        return gutrJob;
    }

    /**
     * 设置岗位
     *
     * @param gutrJob 岗位
     */
    public void setGutrJob(String gutrJob) {
        this.gutrJob = gutrJob;
    }

    /**
     * 获取学历
     *
     * @return GUTR_EDUCATION - 学历
     */
    public String getGutrEducation() {
        return gutrEducation;
    }

    /**
     * 设置学历
     *
     * @param gutrEducation 学历
     */
    public void setGutrEducation(String gutrEducation) {
        this.gutrEducation = gutrEducation;
    }

    /**
     * 获取资格证书
     *
     * @return GUTR_QUALIFICATION - 资格证书
     */
    public String getGutrQualification() {
        return gutrQualification;
    }

    /**
     * 设置资格证书
     *
     * @param gutrQualification 资格证书
     */
    public void setGutrQualification(String gutrQualification) {
        this.gutrQualification = gutrQualification;
    }

    /**
     * 获取出生日期
     *
     * @return GUTR_BIRTHDAY - 出生日期
     */
    public Date getGutrBirthday() {
        return gutrBirthday;
    }

    /**
     * 设置出生日期
     *
     * @param gutrBirthday 出生日期
     */
    public void setGutrBirthday(Date gutrBirthday) {
        this.gutrBirthday = gutrBirthday;
    }

    /**
     * 获取入司日期
     *
     * @return GUTR_JOIN_DATE - 入司日期
     */
    public Date getGutrJoinDate() {
        return gutrJoinDate;
    }

    /**
     * 设置入司日期
     *
     * @param gutrJoinDate 入司日期
     */
    public void setGutrJoinDate(Date gutrJoinDate) {
        this.gutrJoinDate = gutrJoinDate;
    }

    /**
     * 获取创建人
     *
     * @return GUTR_CREATE - 创建人
     */
    public String getGutrCreate() {
        return gutrCreate;
    }

    /**
     * 设置创建人
     *
     * @param gutrCreate 创建人
     */
    public void setGutrCreate(String gutrCreate) {
        this.gutrCreate = gutrCreate;
    }

    /**
     * 获取创建时间
     *
     * @return GUTR_CREATE_DATE - 创建时间
     */
    public Date getGutrCreateDate() {
        return gutrCreateDate;
    }

    /**
     * 设置创建时间
     *
     * @param gutrCreateDate 创建时间
     */
    public void setGutrCreateDate(Date gutrCreateDate) {
        this.gutrCreateDate = gutrCreateDate;
    }

    /**
     * 获取更新人
     *
     * @return GUTR_UPDATE - 更新人
     */
    public String getGutrUpdate() {
        return gutrUpdate;
    }

    /**
     * 设置更新人
     *
     * @param gutrUpdate 更新人
     */
    public void setGutrUpdate(String gutrUpdate) {
        this.gutrUpdate = gutrUpdate;
    }

    /**
     * 获取更新时间
     *
     * @return GUTR_UPDATE_DATE - 更新时间
     */
    public Date getGutrUpdateDate() {
        return gutrUpdateDate;
    }

    /**
     * 设置更新时间
     *
     * @param gutrUpdateDate 更新时间
     */
    public void setGutrUpdateDate(Date gutrUpdateDate) {
        this.gutrUpdateDate = gutrUpdateDate;
    }

    /**
     * 获取删除标记 0：未删除、1：以删除
     *
     * @return GUTR_IS_DELETE - 删除标记 0：未删除、1：以删除
     */
    public Integer getGutrIsDelete() {
        return gutrIsDelete;
    }

    /**
     * 设置删除标记 0：未删除、1：以删除
     *
     * @param gutrIsDelete 删除标记 0：未删除、1：以删除
     */
    public void setGutrIsDelete(Integer gutrIsDelete) {
        this.gutrIsDelete = gutrIsDelete;
    }

    /**
     * 获取版本号
     *
     * @return GUTR_VERSION - 版本号
     */
    public String getGutrVersion() {
        return gutrVersion;
    }

    /**
     * 设置版本号
     *
     * @param gutrVersion 版本号
     */
    public void setGutrVersion(String gutrVersion) {
        this.gutrVersion = gutrVersion;
    }
}