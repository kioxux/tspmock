package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 系列类促销条件商品设置表
 */
@Table(name = "GAIA_SD_PROM_SERIES_CONDS")
@Data
public class GaiaSdPromSeriesConds implements Serializable {
    private static final long serialVersionUID = 353989360213010645L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPSC_VOUCHER_ID")
    private String gspscVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPSC_SERIAL")
    private String gspscSerial;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSPSC_PRO_ID")
    private String gspscProId;

    /**
     * 系列编码
     */
    @Column(name = "GSPSC_SERIES_ID")
    private String gspscSeriesId;

    /**
     * 是否会员
     */
    @Column(name = "GSPSC_MEM_FLAG")
    private String gspscMemFlag;

    /**
     * 是否积分
     */
    @Column(name = "GSPSC_INTE_FLAG")
    private String gspscInteFlag;

    /**
     * 积分倍率
     */
    @Column(name = "GSPSC_INTE_RATE")
    private String gspscInteRate;

    /**
     * 会员等级
     */
    @Column(name = "GSPSC_MEMBERSHIP_GRADE")
    private String gspscMembershipGrade;
}
