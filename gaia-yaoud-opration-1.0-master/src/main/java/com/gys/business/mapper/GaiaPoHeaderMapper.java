//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaPoHeader;
import com.gys.business.service.data.GetAcceptInData;
import com.gys.business.service.data.GetPoOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface GaiaPoHeaderMapper extends BaseMapper<GaiaPoHeader> {
    List<GetPoOutData> poList(GetAcceptInData inData);

    String selectNextVoucherId(@Param("clientId") String clientId);

    String selectNextVoucherId1(@Param("clientId") String clientId);

    void insertBatch(List<GaiaPoHeader> poHeaderList);

    List<Map<String,String>> selectSupplierList(GetAcceptInData inData);
}
