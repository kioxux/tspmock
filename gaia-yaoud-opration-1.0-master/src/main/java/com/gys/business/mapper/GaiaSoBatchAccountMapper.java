package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSoBatchAccount;

import java.util.List;

public interface GaiaSoBatchAccountMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSoBatchAccount record);

    int insertSelective(GaiaSoBatchAccount record);

    GaiaSoBatchAccount selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSoBatchAccount record);

    int updateByPrimaryKey(GaiaSoBatchAccount record);

    List<GaiaSoBatchAccount> findAllOpen();

    GaiaSoBatchAccount findByClient(String client);
}