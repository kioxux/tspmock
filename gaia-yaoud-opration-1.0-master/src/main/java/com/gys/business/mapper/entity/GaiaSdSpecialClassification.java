package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "GAIA_SD_ SPECIAL CLASSIFICATION")
@Data
public class GaiaSdSpecialClassification {
    private static final long serialVersionUID = 7507079604873502475L;


    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 销售单号
     */
    @Id
    @Column(name = "GSSC_BILL_NO")
    private String gsscBillNo;

    /**
     * 店名
     */
    @Id
    @Column(name = "GSSC_BR_ID")
    private String gsscBrId;


    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSSC_PRO_ID")
    private String gsscProId;

    /**
     * 销售日期
     */
    @Id
    @Column(name = "GSSC_DATE")
    private String gsscDate;

    /**
     *行号
     */
    @Id
    @Column(name = "GSSC_SERIAL")
    private Integer gsscSerial;

    /**
     * 特殊分类药品类别
     */
    @Column(name = "GSSC_TYPE")
    private Integer gsscType;

    /**
     * 身份证
     */
    @Column(name = "GSSC_IDCARD")
    private String gsscIdcard;

    /**
     * 顾客姓名
     */
    @Column(name = "GSSC_NAME")
    private String gsscName;

    /**
     * 顾客性别
     */
    @Column(name = "GSSC_SEX")
    private String gsscSex;

    /**
     * 出生年月
     */
    @Column(name = "GSSC_BIRTHDAY")
    private String gsscBirthday;

    /**
     * 手机号
     */
    @Column(name = "GSSC_MOBILE")
    private String gsscMobile;

    /**
     * 地址
     */
    @Column(name = "GSSC_ADDRESS")
    private String gsscAddress;

    /**
     * 购买人监管码
     */
    @Column(name = "GSSC_ECODE")
    private String gsscEcode;


}
