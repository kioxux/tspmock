package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(
        name = "GAIA_BATCH_INFO"
)
@Data
public class GaiaBatchInfo implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "BAT_PRO_CODE"
    )
    private String batProCode;
    @Id
    @Column(
            name = "BAT_SITE_CODE"
    )
    private String batSiteCode;
    @Id
    @Column(
            name = "BAT_BATCH"
    )
    private String batBatch;
    @Column(
            name = "BAT_PO_ID"
    )
    private String batPoId;
    @Column(
            name = "BAT_PO_LINENO"
    )
    private String batPoLineno;
    @Column(
            name = "BAT_PRODUCT_DATE"
    )
    private String batProductDate;
    @Column(
            name = "BAT_EXPIRY_DATE"
    )
    private String batExpiryDate;
    @Column(
            name = "BAT_FACTORY_CODE"
    )
    private String batFactoryCode;
    @Column(
            name = "BAT_FACTORY_NAME"
    )
    private String batFactoryName;
    @Column(
            name = "BAT_SUPPLIER_CODE"
    )
    private String batSupplierCode;
    @Column(
            name = "BAT_SUPPLIER_NAME"
    )
    private String batSupplierName;
    @Column(
            name = "BAT_PAY_TYPE"
    )
    private String batPayType;
    @Column(
            name = "BAT_PO_PRICE"
    )
    private BigDecimal batPoPrice;
    @Column(
            name = "BAT_RECEIVE_QTY"
    )
    private BigDecimal batReceiveQty;
    @Column(
            name = "BAT_BATCH_NO"
    )
    private String batBatchNo;
    @Column(
            name = "BAT_PRO_PLACE"
    )
    private String batProPlace;
    @Column(
            name = "BAT_CREATE_DATE"
    )
    private String batCreateDate;
    @Column(
            name = "BAT_CREATE_TIME"
    )
    private String batCreateTime;
    @Column(
            name = "BAT_CREATE_USER"
    )
    private String batCreateUser;
    @Column(
            name = "BAT_CHANGE_DATE"
    )
    private String batChangeDate;
    @Column(
            name = "BAT_CHANGE_TIME"
    )
    private String batChangeTime;
    @Column(
            name = "BAT_CHANGE_USER"
    )
    private String batChangeUser;

    @Column(
            name = "BAT_SOURCE_NO"
    )
    private String batSourceNo;
}
