package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.service.data.*;
import com.gys.business.service.data.ReportForms.InventoryInquiryByClientAndBatchNoOutData;
import com.gys.business.service.data.ReportForms.InventoryInquiryByClientAndSiteOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaInventoryInquiryMapper extends BaseMapper<GaiaSdStockBatch> {
    List<InventoryInquiryOutData> getInventoryInquiryList(InventoryInquiryInData inData);

    List<InventoryInquiryOutData> getInventoryInquiryList2(InventoryInquiryInData inData);

    List<InventoryInquiryOutData> getInventoryInquiryToStock(InventoryInquiryInData inData);
    //按加盟商按批号查库存
    List<InventoryInquiryByClientAndBatchNoOutData> inventoryInquiryListByClientAndSiteAndBatchNo(InventoryInquiryInData inData);
    //按加盟商按商品查库存
    List<InventoryInquiryByClientAndBatchNoOutData> inventoryInquiryListByClient(InventoryInquiryInData inData);
    //按加盟商按地点查询
    List<InventoryInquiryByClientAndSiteOutData> inventoryInquiryListByStoreAndDc(InventoryInquiryInData inData);
    //按商品搜索的时候 没有商品信息的门店也要带出来
    List<InventoryInquiryByClientAndSiteOutData> inventoryInquiryListByAllStoreAndDc(InventoryInquiryInData inData);
    //按批号查询各个地点下的库存
    List<InventoryInquiryByClientAndSiteOutData> inventoryInquiryListByStoreAndDcAndBatchNo(InventoryInquiryInData inData);

    List<Map<String,Object>> selectBlackListByBatchNo(@Param("client") String client,@Param("brId") String brId);

    //查询条件
    List<MemberSaleClass> findSaleClass(InventoryInquiryInData inData);
    List<MemberSalePosition> findSalePosition(InventoryInquiryInData inData);
    List<MemberProsClass> findProsClass(InventoryInquiryInData inData);
    List<MemberPurchase> findPurchase(InventoryInquiryInData inData);
    List<MemberZDY1> findZDY1(InventoryInquiryInData inData);
    List<MemberZDY2> findZDY2(InventoryInquiryInData inData);
    List<MemberZDY3> findZDY3(InventoryInquiryInData inData);
    List<MemberZDY4> findZDY4(InventoryInquiryInData inData);
    List<MemberZDY5> findZDY5(InventoryInquiryInData inData);

    ZDYNameOutData zdyName(InventoryInquiryInData inData);
}
