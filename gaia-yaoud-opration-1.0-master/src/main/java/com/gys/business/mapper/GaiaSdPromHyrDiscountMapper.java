package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromHyrDiscount;
import com.gys.business.service.data.PromHyrDiscountOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromHyrDiscountMapper extends BaseMapper<GaiaSdPromHyrDiscount> {
    List<PromHyrDiscountOutData> getDetail(PromoteInData inData);

    List<GaiaSdPromHyrDiscount> getAllByClient(@Param("client") String client, @Param("nowDate") String nowDate,@Param("brId")String brId);
}
