package com.gys.business.mapper.entity;

import com.gys.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/22 16:29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DcOutStockH extends BaseEntity {
    /**加盟商*/
    private String client;
    /**缺断货推送流水号*/
    private String voucherId;
    /**配送中心编码*/
    private String dcCode;
    /**配送中心名称*/
    private String dcName;
    /**配送中心简称*/
    private String dcShortName;
    /**缺断货品项数*/
    private Integer proNum;
    /**月均销售额*/
    private BigDecimal monthSaleAmount;
    /**月均毛利额*/
    private BigDecimal monthProfitAmount;
    /**推送日期*/
    private String pushDate;
    /**版本号*/
    private Integer version;
}
