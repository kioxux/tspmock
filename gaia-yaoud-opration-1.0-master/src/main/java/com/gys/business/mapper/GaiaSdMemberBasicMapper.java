package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMemberBasic;
import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import com.gys.business.mapper.entity.GaiaSdSaleD;
import com.gys.business.service.data.*;
import com.gys.business.service.data.integralExchange.MemberCardData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface GaiaSdMemberBasicMapper extends BaseMapper<GaiaSdMemberBasic> {
    List<GetMemberOutData> selectMembersByCondition(GetMemberInData inData);

    GaiaSdMemberCardData selectMembersByMemerId(@Param("client") String client, @Param("brId") String brId, @Param("memberId") String memberId, @Param("gmbCardId") String gmbCardId);

    GetMemberOutData selectMemberByPrimary(GetMemberInData inData);

    List<GetMemberCardReportOutData> queryReport(GetMemberCardReportInData inData);

    GetQueryMemberOutData queryMember(GetQueryMemberInData inData);

    List<GetQueryMemberOutData> queryAppMember(GetQueryMemberInData inData);

    List<MemberCardOutData> selectMemberCardList(MemberCardInData inData);

    String getIncrMemberCard(@Param("prefix") String prefix, @Param("clientId") String clientId);

    /**
     * 根据时间查询 会员销售汇总查询
     *
     * @param inData
     * @return
     */
    List<MemberSaleSummaryOutData> findMemberSalesSummary(MemberSaleSummaryInData inData);

    MemberSaleSummaryOutDataTotal findMemberSalesSummaryTotal(MemberSaleSummaryInData inData);

    /**
     * 根据会员id 查询 名称
     *
     * @param gsshHykNo
     * @return
     */
    MemberNameByCardIdData findMemberNameByCardId(@Param("gsshHykNo") String gsshHykNo, @Param("client") String client, @Param("brId") String brId);

    /**
     * 根据会员卡查信息
     *
     * @param gsshHykNo
     * @return
     */
    GaiaSdMemberCardData findMemberNameByCardId2(@Param("gsshHykNo") String gsshHykNo, @Param("client") String client, @Param("brId") String brId);

    MemberCardData findMemberNameByCardId3(@Param("gsshHykNo") String gsshHykNo, @Param("client") String client, @Param("brId") String brId);

    List<Map<String,Object>> queryMemberCardList(GetSalesSummaryOfSalesmenReportInData inData);

    List<Map<String, Object>> selecthykList(Map<String, Object> inData);

    /**
     * 批量插入
     *
     * @param list
     */
    void insertBanch(@Param("list") List<GaiaSdMemberBasic> list, @Param("table") String table);


    void insertToCardBanch(@Param("list") List<GaiaSdMemberCardData> list, @Param("table") String table);

    List<MemberShipTypeData> membershipCardType(String client);

    /**
     * 根据会员卡 查询会员卡
     *
     * @param clientId
     * @param brId
     * @param gsshHykNo
     * @return
     */
    GaiaSdMemberBasic selectBasicByCardId(@Param("client") String clientId, @Param("brId") String brId, @Param("gsshHykNo") String gsshHykNo);


    /**
     * 根据会员卡 查询会员卡
     *
     * @param clientId
     * @param brId
     * @param gsshHykNo
     * @return
     */
    GaiaSdMemberCardToData selectByCardId(@Param("clientId") String clientId, @Param("brId") String brId, @Param("memberId") String memberId, @Param("gsshHykNo") String gsshHykNo);

    /**
     * 修改会员
     *
     * @param cardData
     */
    void updateByCardId(GaiaSdMemberCardToData cardData);

    /**
     * 修改会员
     *
     * @param cardData
     */
    void updateByCardData(GaiaSdMemberCardData cardData);

    CurrentMembersOutData checkCurrentMembers(GaiaSdSaleD inData);

    /**
     * 查询该店下的所有信息
     * @param client
     * @return
     */
    List<GaiaSdMemberBasic> getAllByClientAndBrId(@Param("client") String client);

    List<GaiaSdMemberCardData> getCardByClientAndBrId(@Param("client") String client, @Param("depId") String depId);

    List<Map> getMemberByClientOrBrId(@Param("client") String client, @Param("depId") String depId);

    String getMaxCardId(@Param("clientId") String clientId);

    List<GetQueryMemberOutData> queryMemberByCondition(GetQueryMemberOutData getQueryMemberOutData);

    int insertMember(GaiaSdMemberBasic gaiaSdMemberBasic);

    int updateMember(GaiaSdMemberBasic gaiaSdMemberBasic);

    GaiaSdMemberCardData getMembershipCardInformation(@Param("client")String clientId, @Param("brId") String brId, @Param("gsshHykNo") String gsshHykNo);

    List<MemberListOutData> getMemberListByCondition(MemberListInData inData);

    GaiaSdMemberBasic getMemberByClientAndPhoneAndCardId(GetMemberInData inData);

    HashMap<String, Object> listVIPParam(@Param("client") String client, @Param("brId")String depId);

    List<MemberListOutData> getMemberListByConditionExt(MemberConditionInData inData);

    MemberListOutData getMemberById(@Param("client") String client,@Param("memberId") String memberId);

    void updateCurrIntegralByCondition(GaiaSdMemberCardToData cardData);

    GaiaSdMemberCardData getCurrIntegralByCondition(GaiaSdMemberCardToData cardData);

    List<GaiaSdSaleD> getControlProNumByMember(GetMemberInData param);

   //会员卡消费汇总表（明细）
    List<LinkedHashMap<String,Object>> selectMemberList(MemberSalesInData inData);
    //会员卡消费汇总表（按门店）
    List<LinkedHashMap<String,Object>> selectMemberStoreList(MemberSalesInData inData);

    //会员商品消费明细
    List<LinkedHashMap<String,Object>> selectMemberPro(MemberSalesInData inData);

    //会员商品消费明细（门店）
    List<LinkedHashMap<String,Object>> selectMemberProSto(MemberSalesInData inData);

    //查询条件：门店编码
    List<MemberSalesOutData> findStoreId(MemberSalesInData inData);

    //查询条件：销售等级
    List<MemberSaleClass> findSaleClass(MemberSalesInData inData);

    //查询条件：商品定位
    List<MemberSalePosition> findSalePosition(MemberSalesInData inData);

    //查询条件：自定义1
    List<MemberProClass> findProClass(MemberSalesInData inData);

    //查询条件：自定义1
    List<MemberZDY1> findZDY1(MemberSalesInData inData);

    //查询条件：自定义2
    List<MemberZDY2> findZDY2(MemberSalesInData inData);

    //查询条件：自定义3
    List<MemberZDY3> findZDY3(MemberSalesInData inData);

    //查询条件：自定义4
    List<MemberZDY4> findZDY4(MemberSalesInData inData);

    //查询条件：自定义5
    List<MemberZDY5> findZDY5(MemberSalesInData inData);

    void updateCardStatus(MemberCardLoseInData inData);

    List<GaiaSdMemberCardData> getMemberCardList(@Param("client")String client, @Param("cardId") String cardId);

    //获取basic总数
    int getBasicCountByClient(@Param("client")String client);

    //获取card总数
    int getCardCountByClient(@Param("client")String client);

    List<GaiaSdMemberBasic> getBasicAllByClientLimit(@Param("client") String client,@Param("index") int index,@Param("num") int num);

    List<GaiaSdMemberCardData> getCardAllByClientLimit(@Param("client") String client,@Param("index") int index,@Param("num") int num);

    List<GetQueryMemberOutData> memberOfTheQuery(Map<String, Object> map);

    GaiaSdMemberBasic getMemberByMembrId(@Param("client")String client,@Param("memberId") String memberId);

    GaiaSdMemberCardData getMemberCardByMembrId(@Param("client")String client,@Param("memberId") String memberId);
}