package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel(value = "月销售任务维护 用户修改值", description = "")
@Data
public class MonthlyPlanVo {
    @ApiModelProperty(value = "门店简称")
    private String brName;
    @ApiModelProperty(value = "门店编码")
    private String BrId;

    @ApiModelProperty(value = "任务月份")
    private String monthPlan;

    @ApiModelProperty(value = "天数")
    private String heaven;

    @ApiModelProperty(value = "营业额 本期日均计划")
    private String turnoverPlan;

    @ApiModelProperty(value = "毛利额 本期日均计划")
    private String grossProfitPlay;

    @ApiModelProperty(value = "本期日均计划")
    private String thisPeriod;

    @ApiModelProperty(value = "营业额同比增长率")
    @NotNull(message = "营业额同比增长率不可为空!")
    private String yoySaleAmt;

    @ApiModelProperty(value = "营业额环比增长率")
    @NotNull(message = "营业额环比增长率不可为空!")
    private String momSaleAmt;

    @ApiModelProperty(value = "毛利额同比增长率")
    @NotNull(message = "毛利额同比增长率不可为空!")
    private String yoySaleGross;

    @ApiModelProperty(value = "毛利额环比增长率")
    @NotNull(message = "毛利额环比增长率不可为空!")
    private String momSaleGross;

    @ApiModelProperty(value = "会员卡同比增长率")
    @NotNull(message = "会员卡同比增长率不可为空!")
    private String yoyMcardQty;

    @ApiModelProperty(value = "会员卡环比增长率")
    @NotNull(message = "会员卡环比增长率不可为空!")
    private String momMcardQty;
}
