package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSaletaskHplanSystem;
import com.gys.business.service.data.saleTask.SalePlanPushInData;
import com.gys.business.service.data.saleTask.SaleTaskSystemInData;
import com.gys.business.service.data.saleTask.SaleTaskSystemOutData;
import com.gys.business.service.data.saleTask.TaskSystemPushData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SaleTaskSystemMapper extends BaseMapper<GaiaSaletaskHplanSystem> {
    int staskTaskNameByName(@Param("staskTaskName") String staskTaskName , @Param("monthPlan") String monthPlan,@Param("staskTaskId") String staskTaskId);

    String selectNextVoucherId();

    List<SaleTaskSystemOutData> selectSalePlanList(SaleTaskSystemInData inData);

    GaiaSaletaskHplanSystem selectSystemPlanDetail(@Param("staskTaskId") String staskTaskId);

    int selectCountByClient(@Param("client") String client,@Param("taskId") String staskTaskId);

    void batchPushUser(List<SalePlanPushInData> list);

    void modifySystemPlanPushStatus(TaskSystemPushData inData);

    List<String> selectUserIdList(@Param("client") String client);

    List<String> selectClientByTaskId(@Param("taskId") String staskTaskId);

    List<GaiaSaletaskHplanSystem> selectNoPushPlan();

    void deleteTask(@Param("taskId") String staskTaskId);
}
