package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSoBatchD;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSoBatchDMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSoBatchD record);

    int insertSelective(GaiaSoBatchD record);

    GaiaSoBatchD selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSoBatchD record);

    int updateByPrimaryKey(GaiaSoBatchD record);

    List<GaiaSoBatchD> selectAll();

    void insertList(List<GaiaSoBatchD> list);

    List<GaiaSoBatchD> selectByOrderId(@Param("soOrderid") String soOrderid, @Param("client") String client);
}