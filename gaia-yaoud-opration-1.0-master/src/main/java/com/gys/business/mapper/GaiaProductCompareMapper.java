package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductCompare;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductCompareMapper {

    List<GaiaProductCompare> getAllByClient(@Param("client") String client);
}
