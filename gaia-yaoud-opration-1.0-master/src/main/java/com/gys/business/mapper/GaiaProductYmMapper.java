package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductYm;
import com.gys.business.service.data.GaiaProductYmInData;
import com.gys.business.service.data.GaiaProductYmOutData;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * 药盟商品信息(GaiaProductYm)表数据库访问层
 *
 * @author XIAOZY
 * @since 2021-11-11 20:26:18
 */
public interface GaiaProductYmMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaProductYm queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaProductYm 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaProductYm> queryAllByLimit(GaiaProductYm gaiaProductYm, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaProductYm 查询条件
     * @return 总行数
     */
    long count(GaiaProductYm gaiaProductYm);

    /**
     * 新增数据
     *
     * @param gaiaProductYm 实例对象
     * @return 影响行数
     */
    int insert(GaiaProductYm gaiaProductYm);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaProductYm> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaProductYm> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaProductYm> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaProductYm> entities);

    /**
     * 修改数据
     *
     * @param gaiaProductYm 实例对象
     * @return 影响行数
     */
    int update(GaiaProductYm gaiaProductYm);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    List<GaiaProductYmOutData> listProInfo(GaiaProductYmInData param);

    List<GaiaProductYmOutData> listYMProInfo(GaiaProductYmInData param);
}

