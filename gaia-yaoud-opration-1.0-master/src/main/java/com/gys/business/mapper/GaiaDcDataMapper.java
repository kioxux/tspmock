package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDcData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/7 19:18
 **/
public interface GaiaDcDataMapper {

    List<GaiaDcData> findList(GaiaDcData cond);

    List<GaiaDcData> getDcList();

    /**
     * 获取批发仓库
     * 禅道1746 如果该加盟商下GAIA_COMPADM_WMS 取不到值，就取GAIA_DC_DATA.DC_CODE作为批发仓地点
     * @param client
     * @return
     */
    List<GaiaDcData> getWholeSaleDC(@Param("client") String client);
}
