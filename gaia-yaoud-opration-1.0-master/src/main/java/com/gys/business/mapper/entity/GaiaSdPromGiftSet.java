package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 赠品类促销系列编码设置表
 */
@Table(name = "GAIA_SD_PROM_GIFT_SET")
@Data
public class GaiaSdPromGiftSet implements Serializable {
    private static final long serialVersionUID = -7145226920606668688L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPGS_VOUCHER_ID")
    private String gspgsVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPGS_SERIAL")
    private String gspgsSerial;

    /**
     * 系列编码
     */
    @Id
    @Column(name = "GSPGS_SERIES_PRO_ID")
    private String gspgsSeriesProId;

    /**
     * 达到数量1
     */
    @Column(name = "GSPGS_REACH_QTY1")
    private String gspgsReachQty1;

    /**
     * 达到金额1
     */
    @Column(name = "GSPGS_REACH_AMT1")
    private BigDecimal gspgsReachAmt1;

    /**
     * 赠送数量1
     */
    @Column(name = "GSPGS_RESULT_QTY1")
    private String gspgsResultQty1;

    /**
     * 达到数量2
     */
    @Column(name = "GSPGS_REACH_QTY2")
    private String gspgsReachQty2;

    /**
     * 达到金额2
     */
    @Column(name = "GSPGS_REACH_AMT2")
    private BigDecimal gspgsReachAmt2;

    /**
     * 赠送数量2
     */
    @Column(name = "GSPGS_RESULT_QTY2")
    private String gspgsResultQty2;

    /**
     * 达到数量3
     */
    @Column(name = "GSPGS_REACH_QTY3")
    private String gspgsReachQty3;

    /**
     * 达到金额3
     */
    @Column(name = "GSPGS_REACH_AMT3")
    private BigDecimal gspgsReachAmt3;

    /**
     * 赠送数量3
     */
    @Column(name = "GSPGS_RESULT_QTY3")
    private String gspgsResultQty3;

}
