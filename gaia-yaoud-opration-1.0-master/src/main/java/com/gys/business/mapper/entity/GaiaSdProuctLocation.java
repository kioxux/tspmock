package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "GAIA_SD_PROUCT_LOCATION")
@Data
public class GaiaSdProuctLocation implements Serializable {
    private static final long serialVersionUID = -1894376284826479552L;
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSPL_BR_ID")
    private String gsplBrId;

    @Id
    @Column(name = "GSPL_PRO_ID")
    private String gsplProId;

    @Column(name = "GSPL_AREA")
    private String gsplArea;

    @Column(name = "GSPL_GROUP")
    private String gsplGroup;

    @Column(name = "GSPL_SHELF")
    private String gsplShelf;

    @Column(name = "GSPL_STOREY")
    private String gsplStorey;

    @Column(name = "GSPL_SEAT")
    private String gsplSeat;
}
