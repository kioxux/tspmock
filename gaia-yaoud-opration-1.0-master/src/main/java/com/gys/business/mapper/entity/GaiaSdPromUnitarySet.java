package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 单品类促销设置表
 */
@Table(name = "GAIA_SD_PROM_UNITARY_SET")
@Data
public class GaiaSdPromUnitarySet implements Serializable {
    private static final long serialVersionUID = 3648156396458072748L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPUS_VOUCHER_ID")
    private String gspusVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPUS_SERIAL")
    private String gspusSerial;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSPUS_PRO_ID")
    private String gspusProId;

    /**
     * 效期天数
     */
    @Column(name = "GSPUS_VAILD")
    private String gspusVaild;

    /**
     * 达到数量1
     */
    @Column(name = "GSPUS_QTY1")
    private String gspusQty1;

    /**
     * 生成促销价1
     */
    @Column(name = "GSPUS_PRC1")
    private BigDecimal gspusPrc1;

    /**
     * 生成促销折扣1
     */
    @Column(name = "GSPUS_REBATE1")
    private String gspusRebate1;

    /**
     * 达到数量2
     */
    @Column(name = "GSPUS_QTY2")
    private String gspusQty2;

    /**
     * 生成促销价2
     */
    @Column(name = "GSPUS_PRC2")
    private BigDecimal gspusPrc2;

    /**
     * 生成促销折扣2
     */
    @Column(name = "GSPUS_REBATE2")
    private String gspusRebate2;

    /**
     * 达到数量3
     */
    @Column(name = "GSPUS_QTY3")
    private String gspusQty3;

    /**
     * 生成促销价3
     */
    @Column(name = "GSPUS_PRC3")
    private BigDecimal gspusPrc3;

    /**
     * 生成促销折扣3
     */
    @Column(name = "GSPUS_REBATE3")
    private String gspusRebate3;

    /**
     * 是否会员
     */
    @Column(name = "GSPUS_MEM_FLAG")
    private String gspusMemFlag;

    /**
     * 是否积分
     */
    @Column(name = "GSPUS_INTE_FLAG")
    private String gspusInteFlag;

    /**
     * 积分倍率
     */
    @Column(name = "GSPUS_INTE_RATE")
    private String gspusInteRate;

    /**
     * 是否限价
     */
    @Column(name = "GSPUS_LIMIT_FLAG")
    private String gspusLimitFlag;

    /**
     * 限价类型
     */
    @Column(name = "GSPUS_LIMIT_TYPE")
    private String gspusLimitType;

    /**
     * 会员等级
     */
    @Column(name = "GSPMS_MEMBERSHIP_GRADE")
    private String gspusMembershipGrade;
}
