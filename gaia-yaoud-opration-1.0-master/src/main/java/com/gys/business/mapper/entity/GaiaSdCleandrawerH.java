//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_CLEANDRAWER_H"
)
public class GaiaSdCleandrawerH implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSCH_VOUCHER_ID"
    )
    private String gschVoucherId;
    @Column(
            name = "GSCH_BR_ID"
    )
    private String gschBrId;
    @Column(
            name = "GSCH_DATE"
    )
    private String gschDate;
    @Column(
            name = "GSCH_EXA_VOUCHER_ID"
    )
    private String gschExaVoucherId;
    @Column(
            name = "GSCH_STATUS"
    )
    private String gschStatus;
    @Column(
            name = "GSCH_EMP"
    )
    private String gschEmp;
    @Column(
            name = "GSCH_REMAKS"
    )
    private String gschRemaks;
    private static final long serialVersionUID = 1L;

    public GaiaSdCleandrawerH() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGschVoucherId() {
        return this.gschVoucherId;
    }

    public void setGschVoucherId(String gschVoucherId) {
        this.gschVoucherId = gschVoucherId;
    }

    public String getGschBrId() {
        return this.gschBrId;
    }

    public void setGschBrId(String gschBrId) {
        this.gschBrId = gschBrId;
    }

    public String getGschDate() {
        return this.gschDate;
    }

    public void setGschDate(String gschDate) {
        this.gschDate = gschDate;
    }

    public String getGschExaVoucherId() {
        return this.gschExaVoucherId;
    }

    public void setGschExaVoucherId(String gschExaVoucherId) {
        this.gschExaVoucherId = gschExaVoucherId;
    }

    public String getGschStatus() {
        return this.gschStatus;
    }

    public void setGschStatus(String gschStatus) {
        this.gschStatus = gschStatus;
    }

    public String getGschEmp() {
        return this.gschEmp;
    }

    public void setGschEmp(String gschEmp) {
        this.gschEmp = gschEmp;
    }

    public String getGschRemaks() {
        return this.gschRemaks;
    }

    public void setGschRemaks(String gschRemaks) {
        this.gschRemaks = gschRemaks;
    }
}
