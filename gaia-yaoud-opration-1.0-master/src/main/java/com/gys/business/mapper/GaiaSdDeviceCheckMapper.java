//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdDeviceCheck;
import com.gys.business.service.data.GetDeviceCheckInData;
import com.gys.business.service.data.GetDeviceCheckOutData;
import com.gys.business.service.data.device.MaintenanceForm;
import com.gys.common.base.BaseMapper;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdDeviceCheckMapper extends BaseMapper<GaiaSdDeviceCheck> {
    List<GetDeviceCheckOutData> selectList(GetDeviceCheckInData inData);

    String selectNextVoucherId(@Param("clientId") String clientId, @Param("codePre") String codePre);

    List<GetDeviceCheckOutData> findList(MaintenanceForm maintenanceForm);

}
