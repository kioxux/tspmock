//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_DEVICE_CHECK"
)
public class GaiaSdDeviceCheck implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSDC_VOUCHER_ID"
    )
    private String gsdcVoucherId;
    @Column(
            name = "GSDC_BR_ID"
    )
    private String gsdcBrId;
    @Column(
            name = "GSDC_BR_NAME"
    )
    private String gsdcBrName;
    @Column(
            name = "GSDC_DEVICE_ID"
    )
    private String gsdcDeviceId;
    @Column(
            name = "GSDC_DEVICE_NAME"
    )
    private String gsdcDeviceName;
    @Column(
            name = "GSDC_DEVICE_MODEL"
    )
    private String gsdcDeviceModel;
    @Column(
            name = "GSDC_CHECK_REMARKS"
    )
    private String gsdcCheckRemarks;
    @Column(
            name = "GSDC_CHECK_RESULT"
    )
    private String gsdcCheckResult;
    @Column(
            name = "GSDC_CHECK_STEP"
    )
    private String gsdcCheckStep;
    @Column(
            name = "GSDC_UPDATE_EMP"
    )
    private String gsdcUpdateEmp;
    @Column(
            name = "GSDC_UPDATE_DATE"
    )
    private String gsdcUpdateDate;
    @Column(
            name = "GSDC_UPDATE_TIME"
    )
    private String gsdcUpdateTime;
    @Column(
            name = "GSDC_STATUS"
    )
    private String gsdcStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdDeviceCheck() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsdcVoucherId() {
        return this.gsdcVoucherId;
    }

    public void setGsdcVoucherId(String gsdcVoucherId) {
        this.gsdcVoucherId = gsdcVoucherId;
    }

    public String getGsdcBrId() {
        return this.gsdcBrId;
    }

    public void setGsdcBrId(String gsdcBrId) {
        this.gsdcBrId = gsdcBrId;
    }

    public String getGsdcBrName() {
        return this.gsdcBrName;
    }

    public void setGsdcBrName(String gsdcBrName) {
        this.gsdcBrName = gsdcBrName;
    }

    public String getGsdcDeviceId() {
        return this.gsdcDeviceId;
    }

    public void setGsdcDeviceId(String gsdcDeviceId) {
        this.gsdcDeviceId = gsdcDeviceId;
    }

    public String getGsdcDeviceName() {
        return this.gsdcDeviceName;
    }

    public void setGsdcDeviceName(String gsdcDeviceName) {
        this.gsdcDeviceName = gsdcDeviceName;
    }

    public String getGsdcDeviceModel() {
        return this.gsdcDeviceModel;
    }

    public void setGsdcDeviceModel(String gsdcDeviceModel) {
        this.gsdcDeviceModel = gsdcDeviceModel;
    }

    public String getGsdcCheckRemarks() {
        return this.gsdcCheckRemarks;
    }

    public void setGsdcCheckRemarks(String gsdcCheckRemarks) {
        this.gsdcCheckRemarks = gsdcCheckRemarks;
    }

    public String getGsdcCheckResult() {
        return this.gsdcCheckResult;
    }

    public void setGsdcCheckResult(String gsdcCheckResult) {
        this.gsdcCheckResult = gsdcCheckResult;
    }

    public String getGsdcCheckStep() {
        return this.gsdcCheckStep;
    }

    public void setGsdcCheckStep(String gsdcCheckStep) {
        this.gsdcCheckStep = gsdcCheckStep;
    }

    public String getGsdcUpdateEmp() {
        return this.gsdcUpdateEmp;
    }

    public void setGsdcUpdateEmp(String gsdcUpdateEmp) {
        this.gsdcUpdateEmp = gsdcUpdateEmp;
    }

    public String getGsdcUpdateDate() {
        return this.gsdcUpdateDate;
    }

    public void setGsdcUpdateDate(String gsdcUpdateDate) {
        this.gsdcUpdateDate = gsdcUpdateDate;
    }

    public String getGsdcUpdateTime() {
        return this.gsdcUpdateTime;
    }

    public void setGsdcUpdateTime(String gsdcUpdateTime) {
        this.gsdcUpdateTime = gsdcUpdateTime;
    }

    public String getGsdcStatus() {
        return this.gsdcStatus;
    }

    public void setGsdcStatus(String gsdcStatus) {
        this.gsdcStatus = gsdcStatus;
    }
}
