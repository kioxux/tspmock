package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.*;

@Data
@Table(name = "MIB_DRUG_INFO")
public class MibDrugInfo implements Serializable {
    private static final long serialVersionUID = 5976376909201615221L;
    @Id
    @Column(name = "CLIENT")
    private String client;

    @Id
    @Column(name = "BR_ID")
    private String brId;

    @Id
    @Column(name = "BILL_NO")
    private String billNo;

    @Id
    @Column(name = "MSG_ID")
    private String msgId;

    @Column(name = "PSN_NO")
    private String psnNo;

    @Column(name = "PSN_NAME")
    private String psnName;

    @Column(name = "MDTRT_CERT_TYPE")
    private String mdtrtCertType;

    @Column(name = "MDTRT_CERT_NO")
    private String mdtrtCertNo;

    @Column(name = "BEGNTIME")
    private String begntime;

    @Column(name = "MEDFEE_SUMAMT")
    private String medfeeSumamt;

    @Column(name = "INVONO")
    private String invono;

    @Column(name = "INSUTYPE")
    private String insutype;

    @Column(name = "DISE_CODG")
    private String diseCodg;

    @Column(name = "DISE_NAME")
    private String diseName;

    @Column(name = "ACCT_USED_FLAG")
    private String acctUsedFlag;

    @Column(name = "MED_TYPE")
    private String medType;

    @Column(name = "OMSG_ID")
    private String omsgId;

    @Column(name = "OINF_NO")
    private String oinfNo;

    @Column(name = "RIGHTING")
    private String righting;

    @Column(name = "CZ_DATE")
    private String czDate;

    @Column(name = "CZ_FLAG")
    private String czFlag;

    @Column(name = "CZ_USER")
    private String czUser;

    @Column(name = "FAIL_MSG")
    private String failMsg;

    @Column(name = "INSUPLC_ADMDVS_PSN")
    private String insuplcAdmdvsPsn;

}