package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaStoreData;
import org.apache.ibatis.annotations.Param;

/**
 * @Author ：gyx
 * @Date ：Created in 9:56 2021/11/5
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
public interface GaiaWmsYangHuMapper {

    Integer selectStoreMaintenData(@Param("client") String client,@Param("stoCode") String stoCode,@Param("currentMonth") String currentMonth);
}
