package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "GAIA_PRODUCT_BASIC")
@Data
public class GaiaProductBasic implements Serializable {

    private static final long serialVersionUID = -4300872673365866644L;

    @Id
    @Column(name = "PRO_CODE")
    private String proCode;

    @Column(name = "PRO_COMMONNAME")
    private String proCommonname;

    @Column(name = "PRO_DEPICT")
    private String proDepict;

    @Column(name = "PRO_PYM")
    private String proPym;

    @Column(name = "PRO_NAME")
    private String proName;

    @Column(name = "PRO_SPECS")
    private String proSpecs;

    @Column(name = "PRO_UNIT")
    private String proUnit;

    @Column(name = "PRO_FORM")
    private String proForm;

    @Column(name = "PRO_PARTFORM")
    private String proPartform;

    @Column(name = "PRO_MINDOSE")
    private String proMindose;

    @Column(name = "PRO_TOTALDOSE")
    private String proTotaldose;

    @Column(name = "PRO_BARCODE")
    private String proBarcode;

    @Column(name = "PRO_BARCODE2")
    private String proBarcode2;

    @Column(name = "PRO_REGISTER_CLASS")
    private String proRegisterClass;

    @Column(name = "PRO_REGISTER_NO")
    private String proRegisterNo;

    @Column(name = "PRO_REGISTER_DATE")
    private String proRegisterDate;

    @Column(name = "PRO_REGISTER_EXDATE")
    private String proRegisterExdate;

    @Column(name = "PRO_CLASS")
    private String proClass;

    @Column(name = "PRO_CLASS_NAME")
    private String proClassName;

    @Column(name = "PRO_COMPCLASS")
    private String proCompclass;

    @Column(name = "PRO_COMPCLASS_NAME")
    private String proCompclassName;

    @Column(name = "PRO_PRESCLASS")
    private String proPresclass;

    @Column(name = "PRO_FACTORY_CODE")
    private String proFactoryCode;

    @Column(name = "PRO_FACTORY_NAME")
    private String proFactoryName;

    @Column(name = "PRO_MARK")
    private String proMark;

    @Column(name = "PRO_BRAND")
    private String proBrand;

    @Column(name = "PRO_BRAND_CLASS")
    private String proBrandClass;

    @Column(name = "PRO_LIFE")
    private String proLife;

    @Column(name = "PRO_LIFE_UNIT")
    private String proLifeUnit;

    @Column(name = "PRO_HOLDER")
    private String proHolder;

    @Column(name = "PRO_INPUT_TAX")
    private String proInputTax;

    @Column(name = "PRO_OUTPUT_TAX")
    private String proOutputTax;

    @Column(name = "PRO_BASIC_CODE")
    private String proBasicCode;

    @Column(name = "PRO_TAX_CLASS")
    private String proTaxClass;

    @Column(name = "PRO_CONTROL_CLASS")
    private String proControlClass;

    @Column(name = "PRO_PRODUCE_CLASS")
    private String proProduceClass;

    @Column(name = "PRO_STORAGE_CONDITION")
    private String proStorageCondition;

    @Column(name = "PRO_STORAGE_AREA")
    private String proStorageArea;

    @Column(name = "PRO_LONG")
    private String proLong;

    @Column(name = "PRO_WIDE")
    private String proWide;

    @Column(name = "PRO_HIGH")
    private String proHigh;

    @Column(name = "PRO_MID_PACKAGE")
    private String proMidPackage;

    @Column(name = "PRO_BIG_PACKAGE")
    private String proBigPackage;

    @Column(name = "PRO_ELECTRONIC_CODE")
    private String proElectronicCode;

    @Column(name = "PRO_QS_CODE")
    private String proQsCode;

    @Column(name = "PRO_MAX_SALES")
    private String proMaxSales;

    @Column(name = "PRO_INSTRUCTION_CODE")
    private String proInstructionCode;

    @Column(name = "PRO_INSTRUCTION")
    private String proInstruction;

    @Column(name = "PRO_MED_PRODCT")
    private String proMedProdct;

    @Column(name = "PRO_MED_PRODCTCODE")
    private String proMedProdctcode;

    @Column(name = "PRO_COUNTRY")
    private String proCountry;

    @Column(name = "PRO_PLACE")
    private String proPlace;

    @Column(name = "PRO_STATUS")
    private String proStatus;

    @Column(name = "PRO_TAKE_DAYS")
    private String proTakeDays;

    @Column(
            name = "PRO_USAGE"
    )
    private String proUsage;

    @Column(name = "PRO_CONTRAINDICATION")
    private String proContraindication;

    @Column(name = "PRO_MED_LISTNUM")
    private String proMedListNum;

    @Column(name = "PRO_MED_LISTNAME")
    private String proMedListName;

    @Column(name = "PRO_MED_LISTFORM")
    private String proMedListForm;

    @Column(name = "PRO_NOTICE_THINGS")
    private String proNoticeThings;

    /**
     * 创建加盟商
     */
    @Column(name = "CREATE_CLIENT")
    private String createClient;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    private String createTime;

    /**
     * 修改加盟商
     */
    @Column(name = "UPDATE_CLIENT")
    private String updateClient;

    /**
     * 修改人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 修改时间
     */
    @Column(name = "UPDATE_TIME")
    private String updateTime;

}
