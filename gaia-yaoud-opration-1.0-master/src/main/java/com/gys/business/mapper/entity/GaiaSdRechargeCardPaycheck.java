package com.gys.business.mapper.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "GAIA_SD_RECHARGE_CARD_PAYCHECK")
public class GaiaSdRechargeCardPaycheck implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private String client;

    /**
     * 储值卡账户
     */
    @Id
    @Column(name = "GSRCP_ACCOUNT_ID")
    private String gsrcpAccountId;

    /**
     * 充值单号
     */
    @Id
    @Column(name = "GSRCP_VOUCHER_ID")
    private String gsrcpVoucherId;

    /**
     * 充值卡号
     */
    @Id
    @Column(name = "GSRCP_CARD_ID")
    private String gsrcpCardId;

    /**
     * 充值门店
     */
    @Id
    @Column(name = "GSRCP_BR_ID")
    private String gsrcpBrId;

    /**
     * 充值日期
     */
    @Id
    @Column(name = "GSRCP_DATE")
    private String gsrcpDate;

    /**
     * 充值时间
     */
    @Column(name = "GSRCP_TIME")
    private String gsrcpTime;

    /**
     * 充值人员
     */
    @Column(name = "GSRCP_EMP")
    private String gsrcpEmp;

    /**
     * 应收金额
     */
    @Column(name = "GSRCP_YS_AMT")
    private BigDecimal gsrcpYsAmt;

    /**
     * 充值金额
     */
    @Column(name = "GSRCP_AFTER_AMT")
    private BigDecimal gsrcpAfterAmt;

    /**
     * 是否日结 0/空为否1为是
     */
    @Column(name = "GSRCP_DAYREPORT")
    private String gsrcpDayreport;

    /**
     * 原充值单号
     */
    @Column(name = "GSRCP_RETURN")
    private String gsrcpReturn;

    /**
     * 充值类型（0或空为正常充值，1为退款）
     */
    @Column(name = "GSRCP_FLAG")
    private String gsrcpFlag;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取储值卡账户
     *
     * @return GSRCP_ACCOUNT_ID - 储值卡账户
     */
    public String getGsrcpAccountId() {
        return gsrcpAccountId;
    }

    /**
     * 设置储值卡账户
     *
     * @param gsrcpAccountId 储值卡账户
     */
    public void setGsrcpAccountId(String gsrcpAccountId) {
        this.gsrcpAccountId = gsrcpAccountId;
    }

    /**
     * 获取充值单号
     *
     * @return GSRCP_VOUCHER_ID - 充值单号
     */
    public String getGsrcpVoucherId() {
        return gsrcpVoucherId;
    }

    /**
     * 设置充值单号
     *
     * @param gsrcpVoucherId 充值单号
     */
    public void setGsrcpVoucherId(String gsrcpVoucherId) {
        this.gsrcpVoucherId = gsrcpVoucherId;
    }

    /**
     * 获取充值卡号
     *
     * @return GSRCP_CARD_ID - 充值卡号
     */
    public String getGsrcpCardId() {
        return gsrcpCardId;
    }

    /**
     * 设置充值卡号
     *
     * @param gsrcpCardId 充值卡号
     */
    public void setGsrcpCardId(String gsrcpCardId) {
        this.gsrcpCardId = gsrcpCardId;
    }

    /**
     * 获取充值门店
     *
     * @return GSRCP_BR_ID - 充值门店
     */
    public String getGsrcpBrId() {
        return gsrcpBrId;
    }

    /**
     * 设置充值门店
     *
     * @param gsrcpBrId 充值门店
     */
    public void setGsrcpBrId(String gsrcpBrId) {
        this.gsrcpBrId = gsrcpBrId;
    }

    /**
     * 获取充值日期
     *
     * @return GSRCP_DATE - 充值日期
     */
    public String getGsrcpDate() {
        return gsrcpDate;
    }

    /**
     * 设置充值日期
     *
     * @param gsrcpDate 充值日期
     */
    public void setGsrcpDate(String gsrcpDate) {
        this.gsrcpDate = gsrcpDate;
    }

    /**
     * 获取充值时间
     *
     * @return GSRCP_TIME - 充值时间
     */
    public String getGsrcpTime() {
        return gsrcpTime;
    }

    /**
     * 设置充值时间
     *
     * @param gsrcpTime 充值时间
     */
    public void setGsrcpTime(String gsrcpTime) {
        this.gsrcpTime = gsrcpTime;
    }

    /**
     * 获取充值人员
     *
     * @return GSRCP_EMP - 充值人员
     */
    public String getGsrcpEmp() {
        return gsrcpEmp;
    }

    /**
     * 设置充值人员
     *
     * @param gsrcpEmp 充值人员
     */
    public void setGsrcpEmp(String gsrcpEmp) {
        this.gsrcpEmp = gsrcpEmp;
    }

    /**
     * 获取应收金额
     *
     * @return GSRCP_YS_AMT - 应收金额
     */
    public BigDecimal getGsrcpYsAmt() {
        return gsrcpYsAmt;
    }

    /**
     * 设置应收金额
     *
     * @param gsrcpYsAmt 应收金额
     */
    public void setGsrcpYsAmt(BigDecimal gsrcpYsAmt) {
        this.gsrcpYsAmt = gsrcpYsAmt;
    }

    /**
     * 获取充值金额
     *
     * @return GSRCP_AFTER_AMT - 充值金额
     */
    public BigDecimal getGsrcpAfterAmt() {
        return gsrcpAfterAmt;
    }

    /**
     * 设置充值金额
     *
     * @param gsrcpAfterAmt 充值金额
     */
    public void setGsrcpAfterAmt(BigDecimal gsrcpAfterAmt) {
        this.gsrcpAfterAmt = gsrcpAfterAmt;
    }

    /**
     * 获取是否日结 0/空为否1为是
     *
     * @return GSRCP_DAYREPORT - 是否日结 0/空为否1为是
     */
    public String getGsrcpDayreport() {
        return gsrcpDayreport;
    }

    /**
     * 设置是否日结 0/空为否1为是
     *
     * @param gsrcpDayreport 是否日结 0/空为否1为是
     */
    public void setGsrcpDayreport(String gsrcpDayreport) {
        this.gsrcpDayreport = gsrcpDayreport;
    }

    /**
     * 获取原充值单号
     *
     * @return GSRCP_RETURN - 原充值单号
     */
    public String getGsrcpReturn() {
        return gsrcpReturn;
    }

    /**
     * 设置原充值单号
     *
     * @param gsrcpReturn 原充值单号
     */
    public void setGsrcpReturn(String gsrcpReturn) {
        this.gsrcpReturn = gsrcpReturn;
    }

    /**
     * 获取充值类型（0或空为正常充值，1为退款）
     *
     * @return GSRCP_FLAG - 充值类型（0或空为正常充值，1为退款）
     */
    public String getGsrcpFlag() {
        return gsrcpFlag;
    }

    /**
     * 设置充值类型（0或空为正常充值，1为退款）
     *
     * @param gsrcpFlag 充值类型（0或空为正常充值，1为退款）
     */
    public void setGsrcpFlag(String gsrcpFlag) {
        this.gsrcpFlag = gsrcpFlag;
    }
}