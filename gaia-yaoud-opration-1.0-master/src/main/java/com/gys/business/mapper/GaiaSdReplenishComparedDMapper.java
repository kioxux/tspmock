package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdReplenishComparedD;
import com.gys.business.service.data.GetReplenishInData;
import com.gys.business.service.data.ReplenishDiffSumInData;
import com.gys.business.service.data.ReplenishDiffSumOutData;
import com.gys.common.base.BaseMapper;

import java.util.HashMap;
import java.util.List;

public interface GaiaSdReplenishComparedDMapper extends BaseMapper<GaiaSdReplenishComparedD> {
    void insertLists(List<GaiaSdReplenishComparedD> comparedDList);

    List<HashMap<String, Object>> listDifferentReplenish(GetReplenishInData inData);

    List<HashMap<String, Object>> listDifferentReplenishDetail(GetReplenishInData inData);

    List<HashMap<String, Object>> listOriReplenishDetail(GetReplenishInData inData);

    HashMap<String, Object> getDifferentReplenish(GetReplenishInData inData);

}