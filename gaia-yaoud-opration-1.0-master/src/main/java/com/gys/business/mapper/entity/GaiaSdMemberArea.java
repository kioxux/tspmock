package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="com-gys-business-mapper-entity-GaiaSdMemberArea")
public class GaiaSdMemberArea {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 门店编号
    */
    @ApiModelProperty(value="门店编号")
    private String brId;

    /**
    * 商圈编号
    */
    @ApiModelProperty(value="商圈编号")
    private String cbdId;

    /**
    * 商圈名称
    */
    @ApiModelProperty(value="商圈名称")
    private String cbdName;
}