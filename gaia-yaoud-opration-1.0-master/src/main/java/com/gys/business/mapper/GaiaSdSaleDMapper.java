package com.gys.business.mapper;

import com.gys.business.bosen.form.InDataForm;
import com.gys.business.controller.app.form.PushForm;
import com.gys.business.controller.app.vo.DcPushVO;
import com.gys.business.mapper.entity.GaiaSdSaleD;
import com.gys.business.mapper.entity.GaiaSynchronousAuditExt;
import com.gys.business.mapper.entity.GrossConstruction;
import com.gys.business.service.data.*;
import com.gys.business.service.data.ReportForms.*;
import com.gys.business.service.data.WebService.PrescripMaterial;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.SupplierInfoDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.ResultHandler;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface GaiaSdSaleDMapper extends BaseMapper<GaiaSdSaleD> {
    List<GetSaleReturnDetailOutData> detailList(GetSaleReturnInData inData);

    List<GetSaleReturnDetailOutData> detailReturnList(GetSaleReturnInData inData);

    List<String> getCompclassList(GetServerInData inData);

    List<String> getProIdList(GetServerInData inData);

    List<String> getPartformList(GetServerInData inData);

    List<SalesInquireDetailOutData> getSalesInquireDetailList(SalesInquireInData inData);

    List<GaiaSdSaleD> selectSyncList(@Param("client") String client, @Param("brId") String brId, @Param("startDate") String startDate);

    List<GetSalesSummaryOfSalesmenReportOutData> querySalesSummaryOfSalesmenReport(GetSalesSummaryOfSalesmenReportInData inData);

    void insertLists(List<GaiaSdSaleD> list);

    String getSalesNum(SalesInquireInData inData);

    List<SalesInquireDetailOutData> getSalesInfoList(SalesInquireInData inData);

    List<StoreRateSellOutData> selectStoreRateSellDetailPage(StoreRateSellInData inData);

    List<Map<String,Object>>querySalesSummaryOfSalesmenList(GetSalesSummaryOfSalesmenReportInData inData);

    List<StoreRateSellOutData> selectStoreRateSellPage(StoreRateSellInData inData);

    List<Map<String,Object>> selectSellerList(Map<String,Object> inData);

    List<GetSaleReturnDetailOutData> queryDjSaleOrderDetail(QueryParam inData);

    List<PrescripMaterial> queryMaterialFromSaleHByBillNo(@Param("client") String client, @Param("brId") String brId, @Param("billNo") String billNo);

    List<GaiaSdSaleD> getAllByBillNo(@Param("billNo")String billNo,@Param("clientId") String clientId,@Param("brId") String brId);

    List<Map<String,Object>> selectStoreSaleByDay(StoreSaleDayInData inData);

    Map<String, Object> selectStoreSaleByDayTotal(StoreSaleDayInData inData);

    void updateList(@Param("list")List<MaterialDocumentReturnData> saleDList);

    List<SalespersonsSalesDetailsOutData> getSalespersonsSalesDetails(GetSalesSummaryOfSalesmenReportInData inData);

    void getSalespersonsSalesDetails(GetSalesSummaryOfSalesmenReportInData inData, ResultHandler<SalespersonsSalesDetailsOutData> resultHandler);

    List<SalespersonsSalesDetailsOutData> getSalespersonsSalesByPro(GetSalesSummaryOfSalesmenReportInData inData);

    List<SalespersonsSalesDetailsOutData> getSalespersonsSalesByUser(GetSalesSummaryOfSalesmenReportInData inData);

    List<SalespersonsSalesDetailsOutData> getProductSalesBySupplier(ProductSalesBySupplierInData inData);

    GetSalesSummaryOfSalesmenReportOutData queryCountSalesSummar(GetSalesSummaryOfSalesmenReportInData inData);

    SalespersonsSalesDetailsOutTotal getSalespersonsSalesDetailsTotal(GetSalesSummaryOfSalesmenReportInData inData);

    List<Map<String, Object>> selectStoreSaleByDate(StoreSaleDateInData inData);

    Map<String,Object> selectStoreSaleByTatol(StoreSaleDateInData inData);

    List<StoreProductSaleClientOutData> selectProductSaleByClient(StoreProductSaleClientInData inData);
    StoreProductSaleClientOutTotal selectProductSaleByClientTatol(StoreProductSaleClientInData inData);


    List<StoreProductSaleStoreOutData> selectProductSaleByProAllStore(StoreProductSaleStoreInData inData);

    StoreProductSaleStoreOutTotal selectProductSaleByProAllStoreTatol(StoreProductSaleStoreInData inData);

    List<SalespersonsSalesDetailsOutData> getSalespersonsSalesByDoctor(GetSalesSummaryOfSalesmenReportInData inData);

    List<StoreProductSaleStoreOutData> selectProductSaleByStore(StoreProductSaleStoreInData inData);

    StoreProductSaleStoreOutTotal selectProductSaleByStoreTatol(StoreProductSaleStoreInData inData);

    List<MonthPushMoneyBySalespersonOutData> selectMonthSalesBySales(MonthPushMoneyBySalespersonInData inData);

    List<MonthPushMoneyBySalespersonOutData> selectMonthSalesByPro(MonthPushMoneyBySalespersonInData inData);

    List<MonthPushMoneyByStoreOutData> selectMonthSalesByStoreAndPro(MonthPushMoneyByStoreInData inData);

    List<MonthPushMoneyByStoreOutData> selectMonthSalesByStoreAndSales(MonthPushMoneyByStoreInData inData);

    List<GaiaSynchronousAuditExt> selectAuditByDay(StartDayAndEndDayInData inData);

    List<MemberPurchaseRecordOutData> getClientPurchaseRecordsByMember(@Param("client") String client,@Param("hykNo") String hykNo);

    BigDecimal getSaleAmtByDateRange(@Param("client") String client, @Param("brId") String brId, @Param("flag") String flag);

    BigDecimal getSaleAmtByDateRangeExt(@Param("client") String client, @Param("flag") String flag);

    /**
     * 会员历史销售查询
     * @param map
     * @return
     */
    List<MemberPurchaseRecordOutData> memberPurchaseRecord(Map<String, String> map);

    List<StoreDistributaionDataOutData> getDistributionDataByOutSide(StoreDistributaionDataInData inData);

    DcPushVO getOutOfStockAmt(PushForm pushForm);

    List<SaleDRebornData> getByProdIdAndOrderNo(OrderDetailBean bean);

    List<StoreDistributaionDataOutData> getDistributionDataByInSide(StoreDistributaionDataInData inData);

    List<Map<String, Object>> getDcCode(String client);

    List<Map<String, Object>> getCusList(@Param("client") String client,@Param("dcCode") String dcCode);

    List<Map<String, Object>> getCusOnlyList(@Param("client") String client,@Param("dcCode") String dcCode);

    List<GetSalesSummaryOfSalesmenReportOutDataUnion> querySalesSummaryOfSalesmenReportUnion(GetSalesSummaryOfSalesmenReportInData inData);

    List<GetSalesSummaryOfSalesmenReportOutDataUnion> selectUnionSaleCountAndAmtByUserId(RelatedSaleEjectInData relatedSaleEjectInData);

    List<GetSalesSummaryOfSalesmenReportOutDataUnion> selectUnionSaleOrderByUserId(RelatedSaleEjectInData relatedSaleEjectInData);

    BigDecimal getSaleAmtByCompClass(@Param("client") String client, @Param("flag") String flag);

    List<SaleAmtByBigCodeOutData> getSaleAmtByCompClassList(@Param("client") String client);

    List<GrossConstruction> selectProBigClassByClientAndMonth(@Param("clients") List<String> clients,@Param("startDate") String startDate, @Param("endDate") String endDate);

    List<GrossConstruction> selectProMidClassByClientAndMonth(@Param("clients") List<String> clients,@Param("startDate") String startDate, @Param("endDate") String endDate);

    List<SaleDDetail> retypeItInvoice(Map<String, String> inData);

    List<HealthCareQueryDto> healthCareQuery(Map<String, Object> map);

    List<RestOrderDetailOutData> listControlGoodsQtyByIdCard(@Param("client") String client,@Param("idCardList") List<String> idCardList);

    List<SupplierInfoDTO> getSupByClient(@Param("client")String client);

    List<GetQueryProductOutData> listOtherStoreProduct(HashMap<String,String> param);

    List<GetPdAndExpOutData> queryStockAndExpGroupByArea(HashMap<String, String> param);

    List<GetPdAndExpOutData> queryStockAndExpGroupByBatchNo(HashMap<String, String> param);

    GetSalesReceiptsTableOutData queryOtherProductDetail(HashMap<String, String> param);

    /**
     * 获取门店出库数据
     * @param dataForm
     * @return
     */
    List<LinkedHashMap<String, Object>> getOutData(InDataForm dataForm);

    List<GetSaleReturnToData> listOriSaleDetail(@Param("clientId") String clientId,@Param("brId") String brId,@Param("billNo") String billNo,@Param("serialList") List<String> serialList);

    List<SaleAmtByMidCodeOutData> getSaleAmtByCompMidClassList(ProductMidTypeSaleInData inData);

    List<GetQueryProductOutData> listOtherStoreProductBatch(HashMap<String, String> param);
}
