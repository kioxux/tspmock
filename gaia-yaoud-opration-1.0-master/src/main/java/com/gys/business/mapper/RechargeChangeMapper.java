package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRechargeChange;
import com.gys.business.service.data.RechargeChangeInData;
import com.gys.business.service.data.RechargeChangeOutData;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface RechargeChangeMapper {

    void insertOne(GaiaSdRechargeChange gaiaSdRechargeChange);

    List<RechargeChangeOutData> rechargeChangePage(RechargeChangeInData inData);
}
