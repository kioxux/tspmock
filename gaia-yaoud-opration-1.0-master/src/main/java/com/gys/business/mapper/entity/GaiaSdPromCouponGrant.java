//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 电子券类促销送券设置表
 */
@Table(name = "GAIA_SD_PROM_COUPON_GRANT")
@Data
public class GaiaSdPromCouponGrant implements Serializable {
    private static final long serialVersionUID = 7613073201467927646L;

    /**
     * 加盟店
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPCG_VOUCHER_ID")
    private String gspcgVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPCG_SERIAL")
    private String gspcgSerial;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSPCG_PRO_ID")
    private String gspcgProId;

    /**
     * 电子券活动号
     */
    @Id
    @Column(name = "GSPCG_ACT_NO")
    private String gspcgActNo;

    /**
     * 达到数量1
     */
    @Column(name = "GSPCG_REACH_QTY1")
    private String gspcgReachQty1;

    /**
     * 达到金额1
     */
    @Column(name = "GSPCG_REACH_AMT1")
    private BigDecimal gspcgReachAmt1;

    /**
     * 送券数量1
     */
    @Column(name = "GSPCG_RESULT_QTY1")
    private String gspcgResultQty1;

    /**
     * 达到数量2
     */
    @Column(name = "GSPCG_REACH_QTY2")
    private String gspcgReachQty2;

    /**
     * 达到金额2
     */
    @Column(name = "GSPCG_REACH_AMT2")
    private BigDecimal gspcgReachAmt2;

    /**
     * 送券数量2
     */
    @Column(name = "GSPCG_RESULT_QTY2")
    private String gspcgResultQty2;

    /**
     * 达到数量3
     */
    @Column(name = "GSPCG_REACH_QTY3")
    private String gspcgReachQty3;

    /**
     * 达到金额3
     */
    @Column(name = "GSPCG_REACH_AMT3")
    private BigDecimal gspcgReachAmt3;

    /**
     * 送券数量3
     */
    @Column(name = "GSPCG_RESULT_QTY3")
    private String gspcgResultQty3;

    /**
     * 是否会员
     */
    @Column(name = "GSPCG_MEM_FLAG")
    private String gspcgMemFlag;

    /**
     * 是否积分
     */
    @Column(name = "GSPCG_INTE_FLAG")
    private String gspcgInteFlag;

    /**
     * 积分倍率
     */
    @Column(name = "GSPCG_INTE_RATE")
    private String gspcgInteRate;


}
