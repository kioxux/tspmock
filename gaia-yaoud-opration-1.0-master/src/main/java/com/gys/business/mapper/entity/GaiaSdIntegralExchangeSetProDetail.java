package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName GAIA_SD_INTEGRAL_EXCHANGE_SET_PRO_DETAIL
 */
@Data
public class GaiaSdIntegralExchangeSetProDetail implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 兑换日期
     */
    private String exchangeDate;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建者
     */
    private String createUser;
}