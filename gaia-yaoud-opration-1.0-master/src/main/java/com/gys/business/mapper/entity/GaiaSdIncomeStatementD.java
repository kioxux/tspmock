package com.gys.business.mapper.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "GAIA_SD_INCOME_STATEMENT_D")
public class GaiaSdIncomeStatementD implements Serializable {
    /**
     * 损溢单号
     */
    @Id
    @Column(name = "GSISD_VOUCHER_ID")
    private String gsisdVoucherId;

    /**
     * 门店
     */
    @Id
    @Column(name = "GSISD_BR_ID")
    private String gsisdBrId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSISD_SERIAL")
    private String gsisdSerial;

    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String client;

    /**
     * 生成日期
     */
    @Column(name = "GSISD_DATE")
    private String gsisdDate;

    /**
     * 编码
     */
    @Column(name = "GSISD_PRO_ID")
    private String gsisdProId;

    /**
     * 批号
     */
    @Column(name = "GSISD_BATCH_NO")
    private String gsisdBatchNo;

    /**
     * 批次
     */
    @Column(name = "GSISD_BATCH")
    private String gsisdBatch;

    /**
     * 库存数量
     */
    @Column(name = "GSISD_STOCK_QTY")
    private BigDecimal gsisdStockQty;

    /**
     * 实际数量
     */
    @Column(name = "GSISD_REAL_QTY")
    private BigDecimal gsisdRealQty;

    /**
     * 损益原因
     */
    @Column(name = "GSISD_IS_CAUSE")
    private String gsisdIsCause;

    /**
     * 损益数量
     */
    @Column(name = "GSISD_IS_QTY")
    private BigDecimal gsisdIsQty;

    /**
     * 库存状态
     */
    @Column(name = "GSISD_STOCK_STATUS")
    private String gsisdStockStatus;

    @Column(name = "GSISD_VALID_UNTIL")
    private String gsisdValidUntil;
    @Column(name = "DELETE_FLAG")
    private Integer deleteFlag;

    private static final long serialVersionUID = 1L;

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * 获取损益单号
     *
     * @return GSISD_VOUCHER_ID - 损益单号
     */
    public String getGsisdVoucherId() {
        return gsisdVoucherId;
    }

    /**
     * 设置损益单号
     *
     * @param gsisdVoucherId 损益单号
     */
    public void setGsisdVoucherId(String gsisdVoucherId) {
        this.gsisdVoucherId = gsisdVoucherId;
    }

    /**
     * 获取门店
     *
     * @return GSISD_BR_ID - 门店
     */
    public String getGsisdBrId() {
        return gsisdBrId;
    }

    /**
     * 设置门店
     *
     * @param gsisdBrId 门店
     */
    public void setGsisdBrId(String gsisdBrId) {
        this.gsisdBrId = gsisdBrId;
    }

    /**
     * 获取行号
     *
     * @return GSISD_SERIAL - 行号
     */
    public String getGsisdSerial() {
        return gsisdSerial;
    }

    /**
     * 设置行号
     *
     * @param gsisdSerial 行号
     */
    public void setGsisdSerial(String gsisdSerial) {
        this.gsisdSerial = gsisdSerial;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取生成日期
     *
     * @return GSISD_DATE - 生成日期
     */
    public String getGsisdDate() {
        return gsisdDate;
    }

    /**
     * 设置生成日期
     *
     * @param gsisdDate 生成日期
     */
    public void setGsisdDate(String gsisdDate) {
        this.gsisdDate = gsisdDate;
    }

    /**
     * 获取编码
     *
     * @return GSISD_PRO_ID - 编码
     */
    public String getGsisdProId() {
        return gsisdProId;
    }

    /**
     * 设置编码
     *
     * @param gsisdProId 编码
     */
    public void setGsisdProId(String gsisdProId) {
        this.gsisdProId = gsisdProId;
    }

    /**
     * 获取批号
     *
     * @return GSISD_BATCH_NO - 批号
     */
    public String getGsisdBatchNo() {
        return gsisdBatchNo;
    }

    /**
     * 设置批号
     *
     * @param gsisdBatchNo 批号
     */
    public void setGsisdBatchNo(String gsisdBatchNo) {
        this.gsisdBatchNo = gsisdBatchNo;
    }

    /**
     * 获取批次
     *
     * @return GSISD_BATCH - 批次
     */
    public String getGsisdBatch() {
        return gsisdBatch;
    }

    /**
     * 设置批次
     *
     * @param gsisdBatch 批次
     */
    public void setGsisdBatch(String gsisdBatch) {
        this.gsisdBatch = gsisdBatch;
    }

    /**
     * 获取库存数量
     *
     * @return GSISD_STOCK_QTY - 库存数量
     */
    public BigDecimal getGsisdStockQty() {
        return gsisdStockQty;
    }

    /**
     * 设置库存数量
     *
     * @param gsisdStockQty 库存数量
     */
    public void setGsisdStockQty(BigDecimal gsisdStockQty) {
        this.gsisdStockQty = gsisdStockQty;
    }

    /**
     * 获取实际数量
     *
     * @return GSISD_REAL_QTY - 实际数量
     */
    public BigDecimal getGsisdRealQty() {
        return gsisdRealQty;
    }

    /**
     * 设置实际数量
     *
     * @param gsisdRealQty 实际数量
     */
    public void setGsisdRealQty(BigDecimal gsisdRealQty) {
        this.gsisdRealQty = gsisdRealQty;
    }

    /**
     * 获取损益原因
     *
     * @return GSISD_IS_CAUSE - 损益原因
     */
    public String getGsisdIsCause() {
        return gsisdIsCause;
    }

    /**
     * 设置损益原因
     *
     * @param gsisdIsCause 损益原因
     */
    public void setGsisdIsCause(String gsisdIsCause) {
        this.gsisdIsCause = gsisdIsCause;
    }

    /**
     * 获取损益数量
     *
     * @return GSISD_IS_QTY - 损益数量
     */
    public BigDecimal getGsisdIsQty() {
        return gsisdIsQty;
    }

    /**
     * 设置损益数量
     *
     * @param gsisdIsQty 损益数量
     */
    public void setGsisdIsQty(BigDecimal gsisdIsQty) {
        this.gsisdIsQty = gsisdIsQty;
    }

    /**
     * 获取库存状态
     *
     * @return GSISD_STOCK_STATUS - 库存状态
     */
    public String getGsisdStockStatus() {
        return gsisdStockStatus;
    }

    /**
     * 设置库存状态
     *
     * @param gsisdStockStatus 库存状态
     */
    public void setGsisdStockStatus(String gsisdStockStatus) {
        this.gsisdStockStatus = gsisdStockStatus;
    }

    /**
     * @return GSISD_VALID_UNTIL
     */
    public String getGsisdValidUntil() {
        return gsisdValidUntil;
    }

    /**
     * @param gsisdValidUntil
     */
    public void setGsisdValidUntil(String gsisdValidUntil) {
        this.gsisdValidUntil = gsisdValidUntil;
    }
}