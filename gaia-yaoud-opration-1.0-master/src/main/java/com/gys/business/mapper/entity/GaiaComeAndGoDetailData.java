package com.gys.business.mapper.entity;

import com.gys.util.csv.annotation.CsvCell;
import com.gys.util.csv.annotation.CsvRow;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author ：guoyuxi.
 * @Date ：Created in 15:29 2021/7/21
 * @Description：往来管理明细数据
 * @Modified By：guoyuxi.
 * @Version:
 */
@Data
@CsvRow
public class GaiaComeAndGoDetailData {



    @ApiModelProperty(value = "仓库编码，一个加盟商对应一个或者多个dc编码，即一个加盟商有一个或多个仓库")
    @CsvCell(title = "仓库编码",index = 1,fieldNo = 1)
    private String dcCode;

    @ApiModelProperty(value = "门店编码")
    @CsvCell(title = "门店编码",index = 3,fieldNo = 1)
    private String stoCode;

    @ApiModelProperty(value = "支付方式编码")
    @CsvCell(title = "支付方式编码",index = 5,fieldNo = 1)
    private String paymentId;

    @ApiModelProperty(value = "支付金额")
    @CsvCell(title = "支付金额",index = 7,fieldNo = 1)
    private BigDecimal amountOfPayment;

    @ApiModelProperty(value = "扣率")
    @CsvCell(title = "扣率",index = 8,fieldNo = 1)
    private String amountRate;

    @ApiModelProperty(value = "支付日期")
    @CsvCell(title = "支付日期",index = 10,fieldNo = 1)
    private String paymentDate;

    @ApiModelProperty(value = "备注")
    @CsvCell(title = "备注",index = 11,fieldNo = 1)
    private String remarks;

    @ApiModelProperty(value = "仓库名称，一个加盟商对应一个或者多个dc编码，即一个加盟商有一个或多个仓库")
    @CsvCell(title = "仓库名称",index = 2,fieldNo = 1)
    private String dcName;

    @ApiModelProperty(value = "门店名称")
    @CsvCell(title = "门店名称",index = 4,fieldNo = 1)
    private String stoName;

    @ApiModelProperty(value = "支付方式名称")
    @CsvCell(title = "支付方式名称",index = 6,fieldNo = 1)
    private String paymentName;

    @ApiModelProperty(value = "实际支付金额")
    @CsvCell(title = "实际支付金额",index = 9,fieldNo = 1)
    private BigDecimal amountOfPaymentRealistic;

    @ApiModelProperty(value = "上传日期")
    private String uploadDate;

    @ApiModelProperty(value = "上传时间")
    private String uploadTime;

    private String amt;

    private String client;
}
