package com.gys.business.mapper.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:40 2021/10/14
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
@Table(name = "GAIA_REGIONAL_PLANNING_MODEL")
@AllArgsConstructor
@NoArgsConstructor
public class GaiaRegionalPlanningModel implements Serializable {
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 区域编码
     */
    @Column(name = "REGIONAL_CODE")
    private String regionalCode;

    /**
     * 区域名称
     */
    @Column(name = "REGIONAL_NAME")
    private String regionalName;

    /**
     * 区域级别 3：三级区域、2：二级区域、1：一级区域
     */
    @Column(name = "LEVEL")
    private Integer level;

    /**
     * 排序编码
     */
    @Column(name = "SORT_NUM")
    private Integer sortNum;

    /**
     * 上级区域ID
     */
    @Column(name = "PARENT_ID")
    private Long parentId;

    /**
     * 创建人
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    private Date createTime;

    /**
     * 更新人
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 更新时间
     */
    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    /**
     * 是否删除 0：否、1：是
     */
    @Column(name = "DELETE_FLAG")
    private String deleteFlag;

    /**
     * 是否为默认 0：否、1：是
     */
    @Column(name = "IS_DEFAULT")
    private String isDefault;

    public static GaiaRegionalPlanningModel getDefaultThreeLevel(String client) {
        return new GaiaRegionalPlanningModel(null, client, "00", "默认", 3, 1, 0l, "system", new Date(), null, null, "0", "1");
    }

    public static GaiaRegionalPlanningModel getDefaultTwoLevel(String client, Long parentId) {
        return new GaiaRegionalPlanningModel(null, client, "000", "默认", 2, 1, parentId, "system", new Date(), null, null, "0", "1");
    }

    public static GaiaRegionalPlanningModel getDefaultOneLevel(String client, Long parentId) {
        return new GaiaRegionalPlanningModel(null, client, "0000", "默认", 1, 1, parentId, "system", new Date(), null, null, "0", "1");
    }
}
