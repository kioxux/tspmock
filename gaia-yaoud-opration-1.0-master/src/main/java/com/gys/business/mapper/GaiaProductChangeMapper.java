package com.gys.business.mapper;

import com.gys.business.service.data.GaiaProductChangeInData;

public interface GaiaProductChangeMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(GaiaProductChangeInData record);

    int insertSelective(GaiaProductChangeInData record);

    GaiaProductChangeInData selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaProductChangeInData record);

    int updateByPrimaryKey(GaiaProductChangeInData record);
}