package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @author xiaoyuan
 */
@Data
public class GaiaSdDoctorAdd implements  Serializable {
    private static final long serialVersionUID = -380527077298413432L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 医生ID
     */
    private String gsdaDrId;
    /**
     * 加点率
     */
    private String gsdaRate;

    /**
     * 加点金额
     */
    private BigDecimal gsdaAmt;

    /**
     * 修改日期
     */
    private String gsdaUpdateDate;

    /**
     * 修改时间
     */
    private String gsdaUpdateTime;

    /**
     * 修改人
     */
    private String gsdaUpdateEmp;

}