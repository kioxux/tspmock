package com.gys.business.mapper.takeAway;

import java.util.List;
import java.util.Map;

public interface BaseMapperPTS {
    //查询门店基础信息
    List<Map <String, Object>> selectShopInfoByMap(String buid);

    //查询商品基础信息
    List<Map<String,Object>> queryMedicine(String shop_no);

    List<Map<String,Object>> queryMedicineStock(Map<String, Object> param);

    List<Map<String,Object>> queryMedicineStockByUpc(Map<String, Object> param);

    //查询有变化的商品基础信息
    List<Map<String,Object>> queryModifyMedicine(Map<String, Object> param);

    //更新PTS内数据的读写标记，标识数据已经被处理了
    void updateMedicineO2OStatus(Map<String, Object> param);
}
