package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MemberPromotionInData implements Serializable {
    private String client;
    private String brId;
    private List<String> proList;
    private String proId;
    private String gsspmCardNo;
}
