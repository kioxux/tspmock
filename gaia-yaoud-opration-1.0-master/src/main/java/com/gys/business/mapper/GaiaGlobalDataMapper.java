package com.gys.business.mapper;

import org.apache.ibatis.annotations.Param;

public interface GaiaGlobalDataMapper {
    String globalType(@Param("client") String client, @Param("stoCode") String stoCode,@Param("globalId") String globalId);
}
