package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 促销主表
 */
@Table(name = "GAIA_SD_PROM_HEAD")
@Data
public class GaiaSdPromHead implements Serializable {
    private static final long serialVersionUID = 6508090414100407848L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 门店
     */
    @Id
    @Column(name = "GSPH_BR_ID")
    private String gsphBrId;

    /**
     * 促销单号
     */
    @Id
    @Column(name = "GSPH_VOUCHER_ID")
    private String gsphVoucherId;

    /**
     * 促销主题
     */
    @Column(name = "GSPH_THEME")
    private String gsphTheme;

    /**
     * 活动名称
     */
    @Column(name = "GSPH_NAME")
    private String gsphName;

    /**
     * 活动说明
     */
    @Column(name = "GSPH_REMARKS")
    private String gsphRemarks;

    /**
     * 状态
     */
    @Column(name = "GSPH_STATUS")
    private String gsphStatus;

    /**
     * 起始日期
     */
    @Column(name = "GSPH_BEGIN_DATE")
    private String gsphBeginDate;

    /**
     * 结束日期
     */
    @Column(name = "GSPH_END_DATE")
    private String gsphEndDate;

    /**
     * 日期频率
     */
    @Column(name = "GSPH_DATE_FREQUENCY")
    private String gsphDateFrequency;

    /**
     * 星期频率
     */
    @Column(name = "GSPH_WEEK_FREQUENCY")
    private String gsphWeekFrequency;

    /**
     * 起始时间
     */
    @Column(name = "GSPH_BEGIN_TIME")
    private String gsphBeginTime;

    /**
     * 结束时间
     */
    @Column(name = "GSPH_END_TIME")
    private String gsphEndTime;

    /**
     * 促销类型
     */
    @Column(name = "GSPH_TYPE")
    private String gsphType;

    /**
     * 阶梯
     */
    @Column(name = "GSPH_PART")
    private String gsphPart;

    /**
     * 参数1
     */
    @Column(name = "GSPH_PARA1")
    private String gsphPara1;

    /**
     * 参数2
     */
    @Column(name = "GSPH_PARA2")
    private String gsphPara2;

    /**
     * 参数3
     */
    @Column(name = "GSPH_PARA3")
    private String gsphPara3;

    /**
     * 参数4
     */
    @Column(name = "GSPH_PARA4")
    private String gsphPara4;

    /**
     * 互斥条件
     */
    @Column(name = "GSPH_EXCLUSION")
    private String gsphExclusion;

    /**
     * 营销主键
     */
    @Column(name = "GSPH_MARKETID")
    private String gsphMarketid;


    /**
     * 是否传输
     */
    @Column(name = "GSPH_FLAG")
    private String gsphFlag;

    /**
     * 农历  1-农历
     */
    @Column(name = "GSPH_NL_FLAG")
    private String gsphNlFlag;

}
