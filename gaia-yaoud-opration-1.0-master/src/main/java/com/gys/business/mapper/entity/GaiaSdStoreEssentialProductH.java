package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value="com-gys-business-mapper-entity-GaiaSdStoreEssentialProductH")
public class GaiaSdStoreEssentialProductH {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 必备品铺货单号
    */
    @ApiModelProperty(value="必备品铺货单号")
    private String orderId;

    /**
    * 必备商品铺货品次数
    */
    @ApiModelProperty(value="必备商品铺货品次数")
    private Integer distributionProCount;

    /**
    * 必备商品铺货门店数
    */
    @ApiModelProperty(value="必备商品铺货门店数")
    private Integer distributionStoreCount;

    /**
    * 数据来源日期
    */
    @ApiModelProperty(value="数据来源日期")
    private String sourceDate;

    /**
    * 单据状态 0-已保存 1-已铺货
    */
    @ApiModelProperty(value="单据状态 0-已保存 1-已铺货")
    private String status;

    /**
    * 创建时间(保存时间)
    */
    @ApiModelProperty(value="创建时间(保存时间)")
    private Date createTime;

    /**
    * 创建人(保存人)
    */
    @ApiModelProperty(value="创建人(保存人)")
    private String createUser;

    /**
     * 更新时间(修改时间)
     */
    @ApiModelProperty(value="更新时间(修改时间)")
    private Date updateTime;

    /**
     * 更新人(修改人)
     */
    @ApiModelProperty(value="更新人(修改人)")
    private String updateUser;

    /**
    * 确认时间(铺货时间)
    */
    @ApiModelProperty(value="确认时间(铺货时间)")
    private Date confirmTime;

    /**
    * 确认人(铺货人)
    */
    @ApiModelProperty(value="确认人(铺货人)")
    private String confirmUser;
}