package com.gys.business.mapper;

import com.gys.business.service.data.GaiaHideCommonRemark;

import java.util.HashMap;
import java.util.List;

/**
 * @Author ：gyx
 * @Date ：Created in 17:16 2022/2/9
 * @Description：
 * @Modified By：gyx
 * @Version:
 */
public interface GaiaHideCommonRemarkMapper {


    void updateHideCommonRemark(List<HashMap<String, String>> param);

    List<GaiaHideCommonRemark> listHideCommonRemark(HashMap<String, String> param);

    void saveHideRemark(GaiaHideCommonRemark param);
}
