package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/14 14:38
 */
@Data
@Table(name = "GAIA_SD_WTPSD_D")
public class GaiaSdWtpsdD implements Serializable {
    private static final long serialVersionUID = 2518741337174308585L;
    /**
     * 加盟商
     */
    @Id
    @Column( name = "CLIENT")
    private String client;
    /**
     * 供应商编码
     */
    @Id
    @Column( name = "GSWD_SUP_ID")
    private String gswdSupId;
    /**
     * 配送单号
     */
    @Id
    @Column( name = "GSWD_VOUCHER_ID")
    private String gswdVoucherId;
    /**
     * 第三方门店编码
     */
    @Id
    @Column( name = "GSWD_DSF_BR_ID")
    private String gswdDsfBrId;
    /**
     * 开单日期
     */
    @Id
    @Column( name = "GSWD_DATE")
    private String gswdDate;
    /**
     * 行号
     */
    @Id
    @Column( name = "GSWD_SERIAL")
    private String gswdSerial;
    /**
     * 第三方商品编码
     */
    @Id
    @Column( name = "GSWD_PRO_DSF_ID")
    private String gswdProDsfId;
    /**
     * 药德商品编码
     */
    @Id
    @Column( name = "GSWD_PRO_ID")
    private String gswdProId;
    /**
     * 生产日期
     */
    @Id
    @Column( name = "GSWD_MADE_DATE")
    private String gswdMadeDate;
    /**
     * 生产批号
     */
    @Id
    @Column( name = "GSWD_BATCH_NO")
    private String gswdBatchNo;
    /**
     * 有效期
     */
    @Id
    @Column( name = "GSWD_EXPIRY_DATE")
    private String gswdExpiryDate;
    /**
     * 批次
     */
    @Id
    @Column( name = "GSWD_BATCH")
    private String gswdBatch;
    /**
     * 数量
     */
    @Id
    @Column( name = "GSWD_QTY")
    private BigDecimal gswdQTY;
    /**
     * 单价
     */
    @Id
    @Column( name = "GSWD_PRC")
    private BigDecimal gswdPRC;
    /**
     * 行金额
     */
    @Id
    @Column( name = "GSWD_AMT")
    private BigDecimal gswdAMT;
    /**
     * 商品名称
     */
    @Id
    @Column( name = "GSWD_NAME")
    private String gswdName;
    /**
     * 通用名
     */
    @Id
    @Column( name = "GSWD_COMMONNAME")
    private String gswdCommonName;
    /**
     * 国际条形码
     */
    @Id
    @Column( name = "GSWD_BARCODE")
    private String gswdBarcode;
    /**
     * 规格
     */
    @Id
    @Column( name = "GSWD_SPECS")
    private String gswdSpecs;
    /**
     * 单位
     */
    @Id
    @Column( name = "GSWD_UNIT")
    private String gswdUnit;
    /**
     * 厂家
     */
    @Id
    @Column( name = "GSWD_FACTORY_NAME")
    private String gswdFactoryName;
    /**
     * 产地
     */
    @Id
    @Column( name = "GSWD_PLACE")
    private String gswdPlace;
    /**
     * 剂型
     */
    @Id
    @Column( name = "GSWD_PORM")
    private String gswdPorm;
    /**
     * 批准文号
     */
    @Id
    @Column( name = "GSWD_REGISTER_NO")
    private String gswdRegisterNo;
    /**
     * 税率
     */
    @Id
    @Column( name = "GSWD_TAX")
    private String gswdTAX;
    /**
     * 是否医保
     */
    @Id
    @Column( name = "GSWD_IF_MED")
    private String gswdIfMed;
    /**
     * 备注
     */
    @Id
    @Column( name = "GSWD_REMARKS")
    private String gswdRemarks;
    /**
     * 创建日期
     */
    @Id
    @Column( name = "GSWD_CRE_DATE")
    private String gswdCreDate;
    /**
     * 创建时间
     */
    @Id
    @Column( name = "GSWD_CRE_TIME")
    private String gswdCreTime;
    /**
     * 创建人
     */
    @Id
    @Column( name = "GSWD_CRE_EMP")
    private String gswdCreEMP;
    /**
     * 修改日期
     */
    @Id
    @Column( name = "GSWD_UPDATE_DATE")
    private String gswdUpdateDate;
    /**
     * 修改时间
     */
    @Id
    @Column( name = "GSWD_UPDATE_TIME")
    private String gswdUpdateTime;
    /**
     * 修改人
     */
    @Id
    @Column( name = "GSWD_UPDATE_EMP")
    private String gswdUpdateEMP;
}
