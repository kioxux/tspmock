package com.gys.business.mapper;

import com.gys.business.service.data.GaiaProductSpecialParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductSpecialParamMapper {

    int deleteByPrimaryKey(@Param("proProv") String proProv, @Param("proCity") String proCity, @Param("proName") String proName);

    int insert(GaiaProductSpecialParam record);

    int insertSelective(GaiaProductSpecialParam record);

    GaiaProductSpecialParam selectByPrimaryKey(@Param("proProv") String proProv, @Param("proCity") String proCity, @Param("proName") String proName);

    GaiaProductSpecialParam selectByCondition(GaiaProductSpecialParam record);

    int updateByPrimaryKeySelective(GaiaProductSpecialParam record);

    int updateByPrimaryKey(GaiaProductSpecialParam record);

    int batchInsert(@Param("list") List<GaiaProductSpecialParam> list);

    int batchReplaceInsert(@Param("list") List<GaiaProductSpecialParam> list);

    List<GaiaProductSpecialParam> getProductSpecialParamList(GaiaProductSpecialParam inData);
}