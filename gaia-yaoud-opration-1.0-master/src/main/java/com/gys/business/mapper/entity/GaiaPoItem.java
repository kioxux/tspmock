//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_PO_ITEM"
)
public class GaiaPoItem implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "PO_ID"
    )
    private String poId;
    @Id
    @Column(
            name = "PO_LINE_NO"
    )
    private String poLineNo;
    @Column(
            name = "PO_PRO_CODE"
    )
    private String poProCode;
    @Column(
            name = "PO_QTY"
    )
    private BigDecimal poQty;
    @Column(
            name = "PO_UNIT"
    )
    private String poUnit;
    @Column(
            name = "PO_PRICE"
    )
    private BigDecimal poPrice;
    @Column(
            name = "PO_LINE_AMT"
    )
    private BigDecimal poLineAmt;
    @Column(
            name = "PO_SITE_CODE"
    )
    private String poSiteCode;
    @Column(
            name = "PO_LOCATION_CODE"
    )
    private String poLocationCode;
    @Column(
            name = "PO_BATCH"
    )
    private String poBatch;
    @Column(
            name = "PO_RATE"
    )
    private String poRate;
    @Column(
            name = "PO_DELIVERY_DATE"
    )
    private String poDeliveryDate;
    @Column(
            name = "PO_LINE_REMARK"
    )
    private String poLineRemark;
    @Column(
            name = "PO_LINE_DELETE"
    )
    private String poLineDelete;
    @Column(
            name = "PO_COMPLETE_FLAG"
    )
    private String poCompleteFlag;
    @Column(
            name = "PO_DELIVERED_QTY"
    )
    private BigDecimal poDeliveredQty;
    @Column(
            name = "PO_DELIVERED_AMT"
    )
    private BigDecimal poDeliveredAmt;
    @Column(
            name = "PO_INVOICE_QTY"
    )
    private BigDecimal poInvoiceQty;
    @Column(
            name = "PO_INVOICE_AMT"
    )
    private BigDecimal poInvoiceAmt;
    @Column(
            name = "PO_COMPLETE_INVOICE"
    )
    private String poCompleteInvoice;
    @Column(
            name = "PO_COMPLETE_PAY"
    )
    private String poCompletePay;
    @Column(
            name = "PO_PAY_QTY"
    )
    private BigDecimal poPayQty;
    @Column(
            name = "PO_PAY_AMT"
    )
    private BigDecimal poPayAmt;
    private static final long serialVersionUID = 1L;

    public GaiaPoItem() {
    }

    public String getClient() {
        return this.client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getPoId() {
        return this.poId;
    }

    public void setPoId(String poId) {
        this.poId = poId;
    }

    public String getPoLineNo() {
        return this.poLineNo;
    }

    public void setPoLineNo(String poLineNo) {
        this.poLineNo = poLineNo;
    }

    public String getPoProCode() {
        return this.poProCode;
    }

    public void setPoProCode(String poProCode) {
        this.poProCode = poProCode;
    }

    public BigDecimal getPoQty() {
        return this.poQty;
    }

    public void setPoQty(BigDecimal poQty) {
        this.poQty = poQty;
    }

    public String getPoUnit() {
        return this.poUnit;
    }

    public void setPoUnit(String poUnit) {
        this.poUnit = poUnit;
    }

    public BigDecimal getPoPrice() {
        return this.poPrice;
    }

    public void setPoPrice(BigDecimal poPrice) {
        this.poPrice = poPrice;
    }

    public BigDecimal getPoLineAmt() {
        return this.poLineAmt;
    }

    public void setPoLineAmt(BigDecimal poLineAmt) {
        this.poLineAmt = poLineAmt;
    }

    public String getPoSiteCode() {
        return this.poSiteCode;
    }

    public void setPoSiteCode(String poSiteCode) {
        this.poSiteCode = poSiteCode;
    }

    public String getPoLocationCode() {
        return this.poLocationCode;
    }

    public void setPoLocationCode(String poLocationCode) {
        this.poLocationCode = poLocationCode;
    }

    public String getPoBatch() {
        return this.poBatch;
    }

    public void setPoBatch(String poBatch) {
        this.poBatch = poBatch;
    }

    public String getPoRate() {
        return this.poRate;
    }

    public void setPoRate(String poRate) {
        this.poRate = poRate;
    }

    public String getPoDeliveryDate() {
        return this.poDeliveryDate;
    }

    public void setPoDeliveryDate(String poDeliveryDate) {
        this.poDeliveryDate = poDeliveryDate;
    }

    public String getPoLineRemark() {
        return this.poLineRemark;
    }

    public void setPoLineRemark(String poLineRemark) {
        this.poLineRemark = poLineRemark;
    }

    public String getPoLineDelete() {
        return this.poLineDelete;
    }

    public void setPoLineDelete(String poLineDelete) {
        this.poLineDelete = poLineDelete;
    }

    public String getPoCompleteFlag() {
        return this.poCompleteFlag;
    }

    public void setPoCompleteFlag(String poCompleteFlag) {
        this.poCompleteFlag = poCompleteFlag;
    }

    public BigDecimal getPoDeliveredQty() {
        return this.poDeliveredQty;
    }

    public void setPoDeliveredQty(BigDecimal poDeliveredQty) {
        this.poDeliveredQty = poDeliveredQty;
    }

    public BigDecimal getPoDeliveredAmt() {
        return this.poDeliveredAmt;
    }

    public void setPoDeliveredAmt(BigDecimal poDeliveredAmt) {
        this.poDeliveredAmt = poDeliveredAmt;
    }

    public BigDecimal getPoInvoiceQty() {
        return this.poInvoiceQty;
    }

    public void setPoInvoiceQty(BigDecimal poInvoiceQty) {
        this.poInvoiceQty = poInvoiceQty;
    }

    public BigDecimal getPoInvoiceAmt() {
        return this.poInvoiceAmt;
    }

    public void setPoInvoiceAmt(BigDecimal poInvoiceAmt) {
        this.poInvoiceAmt = poInvoiceAmt;
    }

    public String getPoCompleteInvoice() {
        return this.poCompleteInvoice;
    }

    public void setPoCompleteInvoice(String poCompleteInvoice) {
        this.poCompleteInvoice = poCompleteInvoice;
    }

    public String getPoCompletePay() {
        return this.poCompletePay;
    }

    public void setPoCompletePay(String poCompletePay) {
        this.poCompletePay = poCompletePay;
    }

    public BigDecimal getPoPayQty() {
        return this.poPayQty;
    }

    public void setPoPayQty(BigDecimal poPayQty) {
        this.poPayQty = poPayQty;
    }

    public BigDecimal getPoPayAmt() {
        return this.poPayAmt;
    }

    public void setPoPayAmt(BigDecimal poPayAmt) {
        this.poPayAmt = poPayAmt;
    }
}
