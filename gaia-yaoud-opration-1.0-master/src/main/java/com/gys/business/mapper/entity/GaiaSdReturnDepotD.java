//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_RETURN_DEPOT_D"
)
public class GaiaSdReturnDepotD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSRDD_VOUCHER_ID"
    )
    private String gsrddVoucherId;
    @Column(
            name = "GSRDD_BR_ID"
    )
    private String gsrddBrId;
    @Id
    @Column(
            name = "GSRDD_DATE"
    )
    private String gsrddDate;
    @Id
    @Column(
            name = "GSRDD_SERIAL"
    )
    private String gsrddSerial;
    @Column(
            name = "GSRDD_PRO_ID"
    )
    private String gsrddProId;
    @Column(
            name = "GSRDD_BATCH_NO"
    )
    private String gsrddBatchNo;
    @Column(
            name = "GSRDD_BATCH"
    )
    private String gsrddBatch;
    @Column(
            name = "GSRDD_VALID_DATE"
    )
    private String gsrddValidDate;
    @Column(
            name = "GSRDD_STOCK_QTY"
    )
    private String gsrddStockQty;
    @Column(
            name = "GSRDD_RETDEP_QTY"
    )
    private String gsrddRetdepQty;
    @Column(
            name = "GSRDD_RETDEP_CAUSE"
    )
    private String gsrddRetdepCause;
    @Column(
            name = "GSRDD_START_QTY"
    )
    private String gsrddStartQty;
    @Column(
            name = "GSRDD_REVISE_QTY"
    )
    private String gsrddReviseQty;
    @Column(
            name = "GSRDD_STOCK_STATUS"
    )
    private String gsrddStockStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdReturnDepotD() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsrddVoucherId() {
        return this.gsrddVoucherId;
    }

    public void setGsrddVoucherId(String gsrddVoucherId) {
        this.gsrddVoucherId = gsrddVoucherId;
    }

    public String getGsrddBrId() {
        return gsrddBrId;
    }

    public void setGsrddBrId(String gsrddBrId) {
        this.gsrddBrId = gsrddBrId;
    }

    public String getGsrddDate() {
        return this.gsrddDate;
    }

    public void setGsrddDate(String gsrddDate) {
        this.gsrddDate = gsrddDate;
    }

    public String getGsrddSerial() {
        return this.gsrddSerial;
    }

    public void setGsrddSerial(String gsrddSerial) {
        this.gsrddSerial = gsrddSerial;
    }

    public String getGsrddProId() {
        return this.gsrddProId;
    }

    public void setGsrddProId(String gsrddProId) {
        this.gsrddProId = gsrddProId;
    }

    public String getGsrddBatchNo() {
        return this.gsrddBatchNo;
    }

    public void setGsrddBatchNo(String gsrddBatchNo) {
        this.gsrddBatchNo = gsrddBatchNo;
    }

    public String getGsrddBatch() {
        return this.gsrddBatch;
    }

    public void setGsrddBatch(String gsrddBatch) {
        this.gsrddBatch = gsrddBatch;
    }

    public String getGsrddValidDate() {
        return this.gsrddValidDate;
    }

    public void setGsrddValidDate(String gsrddValidDate) {
        this.gsrddValidDate = gsrddValidDate;
    }

    public String getGsrddStockQty() {
        return this.gsrddStockQty;
    }

    public void setGsrddStockQty(String gsrddStockQty) {
        this.gsrddStockQty = gsrddStockQty;
    }

    public String getGsrddRetdepQty() {
        return this.gsrddRetdepQty;
    }

    public void setGsrddRetdepQty(String gsrddRetdepQty) {
        this.gsrddRetdepQty = gsrddRetdepQty;
    }

    public String getGsrddRetdepCause() {
        return this.gsrddRetdepCause;
    }

    public void setGsrddRetdepCause(String gsrddRetdepCause) {
        this.gsrddRetdepCause = gsrddRetdepCause;
    }

    public String getGsrddStartQty() {
        return this.gsrddStartQty;
    }

    public void setGsrddStartQty(String gsrddStartQty) {
        this.gsrddStartQty = gsrddStartQty;
    }

    public String getGsrddReviseQty() {
        return this.gsrddReviseQty;
    }

    public void setGsrddReviseQty(String gsrddReviseQty) {
        this.gsrddReviseQty = gsrddReviseQty;
    }

    public String getGsrddStockStatus() {
        return this.gsrddStockStatus;
    }

    public void setGsrddStockStatus(String gsrddStockStatus) {
        this.gsrddStockStatus = gsrddStockStatus;
    }
}
