package com.gys.business.mapper;

import com.gys.business.service.data.GaiaSdReplenishHistoryInData;
import java.util.List;

import com.gys.business.service.data.OutOfStockInData;
import com.gys.business.service.data.OutOfStockOutListData;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdReplenishHistoryMapper {

    List<OutOfStockOutListData> getOutOfStockHistoryList(OutOfStockInData inData);

    int batchInsert(@Param("list") List<GaiaSdReplenishHistoryInData> list);
}