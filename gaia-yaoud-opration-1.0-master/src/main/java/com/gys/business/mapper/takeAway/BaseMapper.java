package com.gys.business.mapper.takeAway;

import com.gys.common.data.BaseOrderInfo;
import com.gys.common.data.BaseOrderProInfo;
import com.gys.common.data.PtsOrderInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BaseMapper {
    //接口操作信息记录到日志表
    void insertOperatelogWithMap(Map<String, Object> param);

    // 插入外卖订单基础信息
    void insertOrderInfoWithMap(BaseOrderInfo param);

    // 更新外卖订单信息
    void updateOrderInfoWithEntity(BaseOrderInfo param);

    // 插入订单历史变更信息
    void insertOrderHistoryInfoWithEntity(BaseOrderInfo param);

    // 插入订单申请售后记录
    void insertOrderAfterServiceWithEntity(BaseOrderInfo param);

    // 订单申请售后记录表更新（走的是存储过程）
    void orderAfterServiceWithEntity(BaseOrderInfo param);

    // 插入订单历史变更信息
    void insertOrderHistoryInfoWithMap(Map<String, Object> param);

    // 插入外卖订单商品明细
    void insertOrderProInfoWithList(List<BaseOrderProInfo> param);

    // 更新订单明细信息
    void updateOrderProInfoWithList(List<BaseOrderProInfo> param);

    // 插入门店外卖订单在途商品明细
    void insertOrderProLockInfoWithList(List<BaseOrderProInfo> param);

    // 更新订单库存锁定信息
    void updateOrderProLockInfoWithList(List<BaseOrderProInfo> param);

    // 更新订单基础信息
    int updateOrderInfoWithMap(Map<String, Object> param);

    // 更新订单配送员信息
    int updateOrderDispatchInfoWithMap(Map<String, Object> param);

    int updateStockWithMap(List<Map<String, Object>> param);

    void updateShopOpenWithMap(Map<String, Object> param);

    int updateOrderStatus(List<Map<String, Object>> param);

    int updateSingleOrderStatus(Map<String, Object> param);

    // 获取订单基础信息
    Map<String, Object> selectOrderInfoByMap(Map<String, Object> param);

    // 查询平台订单ID信息
    Map<String, Object> selectPlatformOrderIdInfoByMap(Map<String, Object> param);

    // 获取门店可用库存
    int selectStoreProStockByMap(Map<String, Object> param);

    // 查询每家门店某一种药品的库存锁定数量
    List<Map<String,Object>> selectStoreMedicineStockLockByMap(Map<String, Object> param);

    // 删除门店在途明细
    void deleteOrderProLockById(String order_id);

    PtsOrderInfo GetOrderInfo(Map<String, Object> param);

    //查询所有门店药品信息
    List<Map <String, Object>> queryMedicine(Map<String,Object> param);

    //插入门店药品信息
    void insertMedicine(Map<String,Object> param);

    //更新门店药品信息
    void updateMedicine(Map<String,Object> param);

    //更新门店药品状态
    void updateMedicineStatus(Map<String, Object> param);

    //插入药品平台价格信息
    void insertMedicinePlatforms(Map<String,Object> param);

    //更新药品平台价格信息
    void updateMedicinePlatforms(Map<String, Object> param);

    //更新药品平台状态结果
    void updateMedicinePlatformsStatus(Map<String, Object> param);

    void updateMedicineCate(List<Map<String, Object>> param);

    //加载已有药品分类信息
    List<Map<String,Object>> load_medicine_category_info();

    //插入药品分类信息
    void insert_medicine_category_info(List<Map<String,Object>> param);

    //查询所有门店id信息
    List<Map<String,Object>> queryShop();

    //加载配置信息
    List<Map<String,Object>> loadConfig();

    List<Map<String,Object>> loadAppSecretForJd();

    List<Map<String,Object>> loadAppSecretForJdByIndex();

    //加载门店配置信息
    List<Map<String,Object>> loadShop(Map<String, Object> param);

    //查询门店药品库存锁定信息
    List<Map<String,Object>> selectMedicineLockStockInfo(Map<String,Object> param);

    //查询所有分部信息
    List<Map<String,Object>> querySubCompany(Map<String, Object> param);

    //查询开关设置信息
    List<Map<String,Object>> selectSwitchInfo(Map<String, Object> param);

    //设置京东token自动更新
    void updateJDConfig_token(Map<String, Object> param);

    //查询待处理消息队列
    List<Map<String,Object>> selectMessageQueue(Map<String, Object> param);

    //更新事务队列
    void updateMessageQueue(Map<String, Object> param);

    //插入新的事务进事务队列
    void insertMessageQueue(Map<String, Object> param);

    //插入排他信息
    void insertSyncObj(Map<String, Object> param);

    //删除排他信息
    void deleteSyncObj(Map<String, Object> param);

    //查询商品组合清单
    List<Map<String, Object>> selectGroupMedicine(Map<String, Object> param);

    //批量查询门店商品清单
    List<Map<String, Object>> queryMedicineByList(Map<String, Object> param);

    //定量查询待处理商品清单
    List<Map<String, Object>> queryUnExecMedicine(Map<String, Object> param);

    Integer queryByPlatformOrderId(String platformOrderId);

    void updateRecipientPhone(List<Map<String, String>> list);

    void ddkyUpdateShippingFee(@Param("platformOrderId") String platformOrderId, @Param("shippingFee") String shippingFee);

}
