package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaReturnApproval;
import com.gys.business.service.data.DelegationReturn.ExcelReturnApprovalData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaReturnApprovalMapper extends BaseMapper<GaiaReturnApproval> {
    List<ExcelReturnApprovalData> selectByExport();

    void insertBatch(List<GaiaReturnApproval> returnApprovalList);
}
