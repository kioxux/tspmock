package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWfRecord;
import com.gys.business.service.data.GetWorkflowInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaWfRecordMapper extends BaseMapper<GaiaWfRecord> {
   String selectNextWfCode(@Param("client") String client, @Param("codePre") String codePre);

   List<GaiaWfRecord> fetchByOrder(GetWorkflowInData inData);
}
