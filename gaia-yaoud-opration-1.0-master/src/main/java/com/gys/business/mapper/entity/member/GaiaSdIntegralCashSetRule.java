package com.gys.business.mapper.entity.member;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 积分抵现规则
 *
 * @author wu mao yin
 * @date 2021/12/8 17:44
 */
@Data
@Table(name = "GAIA_SD_INTEGRAL_CASH_SET_RULE")
public class GaiaSdIntegralCashSetRule implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 规则内容代号
     */
    @Column(name = "GSICSR_CODE")
    private String gsicsrCode;

    /**
     * 序号
     */
    @Column(name = "GSICSR_SERIAL")
    private Integer gsicsrSerial;

    /**
     * 需要积分
     */
    @Column(name = "GSICSR_NEED_JF")
    private BigDecimal gsicsrNeedJf;

    /**
     * 抵用金额
     */
    @Column(name = "GSICSR_DY_AMT")
    private BigDecimal gsicsrDyAmt;

    /**
     * 抵用比率
     */
    @Column(name = "GSICSR_DY_RATE")
    private BigDecimal gsicsrDyRate;

    private static final long serialVersionUID = 1L;

}