package com.gys.business.mapper.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/7 15:02
 **/
@Data
public class WmKuCun {
    private Long id;
    private String client;
    private String proSite;
    private String wmSpBm;
    private String wmPch;
    private String wmHwh;
    private String wmKcztBh;
    private String wmLydh;
    private BigDecimal wmKcsl;
    private BigDecimal wmKysl;
    private String wmShr;
    private Long wmShrId;
    private String wmShrq;
    private String wmShsj;
    private String wmYkcztBh;
    private Date wmCjsj;
}
