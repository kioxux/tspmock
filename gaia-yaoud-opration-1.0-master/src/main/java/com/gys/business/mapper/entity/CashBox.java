package com.gys.business.mapper.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 02:08
 */
@Data
public class CashBox {
    private Long id;
    private String client;
    private String storeId;
    private String storeName;
    private String machineNo;
    private String machineName;
    private BigDecimal initAmount;
    private BigDecimal lastAmount;
    private BigDecimal changeAmount;
    private BigDecimal currentAmount;
    private String remark;
    private Integer deleteFlag;
    private String createUser;
    private Date createTime;
    private String updateUser;
    private Date updateTime;
}
