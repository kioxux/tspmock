package com.gys.business.mapper;

import com.gys.business.mapper.entity.member.GaiaSdIntegralCash;
import com.gys.business.mapper.entity.member.GaiaSdIntegralCashConds;
import com.gys.business.mapper.entity.member.GaiaSdIntegralCashSto;
import com.gys.business.mapper.entity.member.GaiaSdIntegralCashTimeInterval;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.member.PointCashOutActivitySearchDTO;
import com.gys.common.data.member.PointCashOutActivityVO;
import com.gys.common.data.member.PointProSearchDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 积分抵现活动
 *
 * @author wu mao yin
 * @date 2021/12/9 15:15
 */
public interface GaiaSdIntegralCashMapper extends BaseMapper<GaiaSdIntegralCash> {

    /**
     * 查询规则代号是否被抵现活动使用
     *
     * @param client client
     * @param planId planId
     * @return Integer
     */
    Integer checkPlanIdIsInUse(@Param("client") String client, @Param("planId") String planId);

    /**
     * 保存参与抵现活动的商品
     *
     * @param gaiaSdIntegralCashConds gaiaSdIntegralCashConds
     * @return int
     */
    int saveProWithCash(List<GaiaSdIntegralCashConds> gaiaSdIntegralCashConds);

    /**
     * 删除参与抵现活动的商品
     *
     * @param client    client
     * @param voucherId voucherId
     * @return int
     */
    int deleteProWithCash(@Param("client") String client, @Param("voucherId") String voucherId);

    /**
     * 删除抵现活动
     *
     * @param client    client
     * @param voucherId voucherId
     * @return int
     */
    int deleteCash(@Param("client") String client, @Param("voucherId") String voucherId);

    /**
     * 保存参与抵现活动的门店
     *
     * @param gaiaSdIntegralCashStos gaiaSdIntegralCashStos
     * @return int
     */
    int saveStoWithCash(List<GaiaSdIntegralCashSto> gaiaSdIntegralCashStos);

    /**
     * 删除参与抵现活动的商品
     *
     * @param client    client
     * @param voucherId voucherId
     * @return int
     */
    int deleteStoWithCash(@Param("client") String client, @Param("voucherId") String voucherId);

    /**
     * 保存参与抵现活动的时间段
     *
     * @param gaiaSdIntegralCashTimeIntervals gaiaSdIntegralCashTimeIntervals
     * @return int
     */
    int saveTimeIntervalsWithCash(List<GaiaSdIntegralCashTimeInterval> gaiaSdIntegralCashTimeIntervals);

    /**
     * 删除参与抵现活动的商品
     *
     * @param client    client
     * @param voucherId voucherId
     * @return int
     */
    int deleteTimeIntervalsWithCash(@Param("client") String client, @Param("voucherId") String voucherId);

    /**
     * 检查同一门店、同一会员卡别、同一时间，是否有多种积分设置被应用
     *
     * @param client          client
     * @param voucherId       voucherId
     * @param memberCardType memberCardType
     * @param stoCodes        stoCodes
     * @return List<Map < String, String>>
     */
    List<Map<String, String>> checkSameRuleInUse(@Param("client") String client,
                                                 @Param("voucherId") String voucherId,
                                                 @Param("memberCardType") String memberCardType,
                                                 @Param("stoCodes") List<String> stoCodes);

    /**
     * 获取抵现活动时段
     *
     * @param client    client
     * @param voucherId voucherId
     * @return List<Map < String, String>>
     */
    List<Map<String, String>> selectTimeIntervals(@Param("client") String client, @Param("voucherId") String voucherId);

    /**
     * 获取最新积分抵现活动单号
     *
     * @param client client
     * @return String
     */
    String selectLastVoucherId(@Param("client") String client);

    /**
     * 根据单号修改
     *
     * @param gaiaSdIntegralCash gaiaSdIntegralCash
     * @return int
     */
    int updateIntegralCash(GaiaSdIntegralCash gaiaSdIntegralCash);

    /**
     * 根据单号修改结束时间
     *
     * @param gaiaSdIntegralCash gaiaSdIntegralCash
     * @return int
     */
    int updateIntegralCashEndTime(GaiaSdIntegralCash gaiaSdIntegralCash);

    /**
     * 根据单号审批
     *
     * @param gaiaSdIntegralCash gaiaSdIntegralCash
     * @return int
     */
    int reviewActivity(GaiaSdIntegralCash gaiaSdIntegralCash);

    /**
     * 根据单号查看关联门店
     *
     * @param client    加盟商
     * @param voucherId 单号
     * @return List<GaiaSdIntegralCashSto>
     */
    List<GaiaSdIntegralCashSto> selectStoListByVoucherId(@Param("client") String client, @Param("voucherId") String voucherId);

    /**
     * 根据单号查看关联商品
     *
     * @param pointProSearchDTO pointProSearchDTO
     * @return List<GaiaSdIntegralCashConds>
     */
    List<GaiaSdIntegralCashConds> selectProListByVoucherId(PointProSearchDTO pointProSearchDTO);

    /**
     * 查询积分抵现活动列表
     *
     * @param pointCashOutActivitySearchDTO pointCashOutActivitySearchDTO
     * @return List<PointCashOutActivityVO>
     */
    List<PointCashOutActivityVO> selectIntegralCashActivity(PointCashOutActivitySearchDTO pointCashOutActivitySearchDTO);

}