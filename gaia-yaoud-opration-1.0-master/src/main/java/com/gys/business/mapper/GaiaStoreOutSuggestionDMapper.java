package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaStoreOutSuggestionD;
import com.gys.business.service.data.GaiaStoreOutSuggestionDOutData;
import com.gys.business.service.data.GaiaStoreOutSuggestionInData;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionD)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-28 10:53:43
 */
public interface GaiaStoreOutSuggestionDMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaStoreOutSuggestionD queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaStoreOutSuggestionD 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaStoreOutSuggestionD> queryAllByLimit(GaiaStoreOutSuggestionD gaiaStoreOutSuggestionD, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaStoreOutSuggestionD 查询条件
     * @return 总行数
     */
    long count(GaiaStoreOutSuggestionD gaiaStoreOutSuggestionD);

    /**
     * 新增数据
     *
     * @param gaiaStoreOutSuggestionD 实例对象
     * @return 影响行数
     */
    int insert(GaiaStoreOutSuggestionD gaiaStoreOutSuggestionD);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaStoreOutSuggestionD> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaStoreOutSuggestionD> entities);

    /**
     * 修改数据
     *
     * @param gaiaStoreOutSuggestionD 实例对象
     * @return 影响行数
     */
    int update(GaiaStoreOutSuggestionD gaiaStoreOutSuggestionD);

    List<GaiaStoreOutSuggestionDOutData> listDetail(GaiaStoreOutSuggestionInData inData);

    int updateByCode(GaiaStoreOutSuggestionD suggestionD);

}

