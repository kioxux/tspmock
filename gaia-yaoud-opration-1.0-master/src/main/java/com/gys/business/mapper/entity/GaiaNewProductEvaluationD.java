package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_NEW_PRODUCT_EVALUATION_D")
public class GaiaNewProductEvaluationD implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 商品评估单号
     */
    @Column(name = "BILL_CODE")
    private String billCode;

    /**
     * 商品编码
     */
    @Column(name = "PRO_SELF_CODE")
    private String proSelfCode;

    /**
     * 门店ID
     */
    @Column(name = "STORE_ID")
    private String storeId;

    /**
     * 销售额
     */
    @Column(name = "SALES_AMT")
    private BigDecimal salesAmt;

    /**
     * 销量
     */
    @Column(name = "SALES_QUANTITY")
    private BigDecimal salesQuantity;

    /**
     * 毛利额
     */
    @Column(name = "GROSS")
    private BigDecimal gross;

    /**
     * 商品状态：0-淘汰 1-转正常
     */
    @Column(name = "STATUS")
    private Integer status;

    /**
     * 删除标记:0-正常 1-删除
     */
    @Column(name = "IS_DELETE")
    private Integer isDelete;

    /**
     * 处理状态：0-未处理 1-已处理
     */
    @Column(name = "DEAL_STATUS")
    private Integer dealStatus;

    /**
     * 建议处置状态：0-淘汰 1-转正常
     */
    @Column(name = "SUGGESTED_STATUS")
    private Integer suggestedStatus;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    private Date createTime;

    /**
     * 创建者
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 更新时间
     */
    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    /**
     * 更新者
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    private static final long serialVersionUID = 1L;

    public Integer getDealStatus() {
        return dealStatus;
    }

    public void setDealStatus(Integer dealStatus) {
        this.dealStatus = dealStatus;
    }

    public Integer getSuggestedStatus() {
        return suggestedStatus;
    }

    public void setSuggestedStatus(Integer suggestedStatus) {
        this.suggestedStatus = suggestedStatus;
    }

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取商品评估单号
     *
     * @return BILL_CODE - 商品评估单号
     */
    public String getBillCode() {
        return billCode;
    }

    /**
     * 设置商品评估单号
     *
     * @param billCode 商品评估单号
     */
    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    /**
     * 获取商品编码
     *
     * @return PRO_SELF_CODE - 商品编码
     */
    public String getProSelfCode() {
        return proSelfCode;
    }

    /**
     * 设置商品编码
     *
     * @param proSelfCode 商品编码
     */
    public void setProSelfCode(String proSelfCode) {
        this.proSelfCode = proSelfCode;
    }

    /**
     * 获取门店ID
     *
     * @return STORE_ID - 门店ID
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * 设置门店ID
     *
     * @param storeId 门店ID
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     * 获取销售额
     *
     * @return SALES_AMT - 销售额
     */
    public BigDecimal getSalesAmt() {
        return salesAmt;
    }

    /**
     * 设置销售额
     *
     * @param salesAmt 销售额
     */
    public void setSalesAmt(BigDecimal salesAmt) {
        this.salesAmt = salesAmt;
    }

    /**
     * 获取销量
     *
     * @return SALES_QUANTITY - 销量
     */
    public BigDecimal getSalesQuantity() {
        return salesQuantity;
    }

    /**
     * 设置销量
     *
     * @param salesQuantity 销量
     */
    public void setSalesQuantity(BigDecimal salesQuantity) {
        this.salesQuantity = salesQuantity;
    }

    /**
     * 获取毛利额
     *
     * @return GROSS - 毛利额
     */
    public BigDecimal getGross() {
        return gross;
    }

    /**
     * 设置毛利额
     *
     * @param gross 毛利额
     */
    public void setGross(BigDecimal gross) {
        this.gross = gross;
    }

    /**
     * 获取商品状态：0-淘汰 1-转正常
     *
     * @return STATUS - 商品状态：0-淘汰 1-转正常
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置商品状态：0-淘汰 1-转正常
     *
     * @param status 商品状态：0-淘汰 1-转正常
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取删除标记:0-正常 1-删除
     *
     * @return IS_DELETE - 删除标记:0-正常 1-删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置删除标记:0-正常 1-删除
     *
     * @param isDelete 删除标记:0-正常 1-删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_TIME - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建者
     *
     * @return CREATE_USER - 创建者
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建者
     *
     * @param createUser 创建者
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * 获取更新时间
     *
     * @return UPDATE_TIME - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新者
     *
     * @return UPDATE_USER - 更新者
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置更新者
     *
     * @param updateUser 更新者
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
}