package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdElectronBasic;
import com.gys.business.service.data.ElectronBasicInData;
import com.gys.business.service.data.ElectronBasicOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdElectronBasicMapper {

    int insertSelective(GaiaSdElectronBasic record);

    GaiaSdElectronBasic selectByPrimaryKey(GaiaSdElectronBasic key);

    int updateByPrimaryKey(GaiaSdElectronBasic record);

    int batchInsert(@Param("list") List<GaiaSdElectronBasic> list);

    List<ElectronBasicOutData> getElectronThemeSetByList(ElectronBasicInData inData);

    String getGsebIdToElectronThemeSet(@Param("client")String client);

    List<ElectronBasicOutData> getElectronBasic();

    List<GaiaSdElectronBasic> getAllByClient(@Param("client") String client);
}