package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaTaxCode;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaTaxCodeMapper extends BaseMapper<GaiaTaxCode> {

    List<GaiaTaxCode> getAll();

}
