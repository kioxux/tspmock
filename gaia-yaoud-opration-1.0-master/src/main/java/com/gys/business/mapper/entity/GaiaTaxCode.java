package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author
 */
@Table(name = "GAIA_TAX_CODE")
@Data
public class GaiaTaxCode implements Serializable {
    private static final long serialVersionUID = -8288515467842709079L;
    @Id
    @Column(name = "TAX_CODE")
    @ApiModelProperty(value = "税率编码")
    private String taxCode;

    @Column(name = "TAX_CODE_NAME")
    @ApiModelProperty(value = "税率名称")
    private String taxCodeName;

    @Column(name = "TAX_CODE_CLASS")
    @ApiModelProperty(value = "税率类型")
    private String taxCodeClass;

    @Column(name = "TAX_CODE_VALUE")
    @ApiModelProperty(value = "税率值")
    private String taxCodeValue;

    @Column(name = "TAX_CODE_STATUS")
    @ApiModelProperty(value = "税码状态")
    private String taxCodeStatus;

}
