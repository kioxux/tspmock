package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_SD_SALE_PAY_MSG_LOCAL")
public class GaiaSdSalePayMsgLocal implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 店号
     */
    @Id
    @Column(name = "GSSPM_BR_ID")
    private String gsspmBrId;

    /**
     * 日期
     */
    @Id
    @Column(name = "GSSPM_DATE")
    private String gsspmDate;

    /**
     * 销售单号
     */
    @Id
    @Column(name = "GSSPM_BILL_NO")
    private String gsspmBillNo;

    /**
     * 支付编码
     */
    @Id
    @Column(name = "GSSPM_ID")
    private String gsspmId;

    /**
     * 支付类型 
1.销售支付 2.储值卡充值 3.挂号支付 4.代销售
     */
    @Id
    @Column(name = "GSSPM_TYPE")
    private String gsspmType;

    /**
     * 支付名称
     */
    @Column(name = "GSSPM_NAME")
    private String gsspmName;

    /**
     * 支付卡号
     */
    @Column(name = "GSSPM_CARD_NO")
    private String gsspmCardNo;

    /**
     * 支付金额
     */
    @Column(name = "GSSPM_AMT")
    private BigDecimal gsspmAmt;

    /**
     * RMB收款金额
     */
    @Column(name = "GSSPM_RMB_AMT")
    private BigDecimal gsspmRmbAmt;

    /**
     * 找零金额
     */
    @Column(name = "GSSPM_ZL_AMT")
    private BigDecimal gsspmZlAmt;

    /**
     * 是否日结 0/空为否1为是
     */
    @Column(name = "GSSPM_DAYREPORT")
    private String gsspmDayreport;

    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;

    /**
     * 医保卡号
     */
    @Column(name = "GSSPM_MEDICAL_CARD")
    private String gsspmMedicalCard;

    /**
     * 医保姓名
     */
    @Column(name = "GSSPM_MEDICAL_NAME")
    private String gsspmMedicalName;

    /**
     * 医保账户余额
     */
    @Column(name = "GSSPM_ACCOUNT_BALANCE")
    private String gsspmAccountBalance;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取店号
     *
     * @return GSSPM_BR_ID - 店号
     */
    public String getGsspmBrId() {
        return gsspmBrId;
    }

    /**
     * 设置店号
     *
     * @param gsspmBrId 店号
     */
    public void setGsspmBrId(String gsspmBrId) {
        this.gsspmBrId = gsspmBrId;
    }

    /**
     * 获取日期
     *
     * @return GSSPM_DATE - 日期
     */
    public String getGsspmDate() {
        return gsspmDate;
    }

    /**
     * 设置日期
     *
     * @param gsspmDate 日期
     */
    public void setGsspmDate(String gsspmDate) {
        this.gsspmDate = gsspmDate;
    }

    /**
     * 获取销售单号
     *
     * @return GSSPM_BILL_NO - 销售单号
     */
    public String getGsspmBillNo() {
        return gsspmBillNo;
    }

    /**
     * 设置销售单号
     *
     * @param gsspmBillNo 销售单号
     */
    public void setGsspmBillNo(String gsspmBillNo) {
        this.gsspmBillNo = gsspmBillNo;
    }

    /**
     * 获取支付编码
     *
     * @return GSSPM_ID - 支付编码
     */
    public String getGsspmId() {
        return gsspmId;
    }

    /**
     * 设置支付编码
     *
     * @param gsspmId 支付编码
     */
    public void setGsspmId(String gsspmId) {
        this.gsspmId = gsspmId;
    }

    /**
     * 获取支付类型 
1.销售支付 2.储值卡充值 3.挂号支付 4.代销售
     *
     * @return GSSPM_TYPE - 支付类型 
1.销售支付 2.储值卡充值 3.挂号支付 4.代销售
     */
    public String getGsspmType() {
        return gsspmType;
    }

    /**
     * 设置支付类型 
1.销售支付 2.储值卡充值 3.挂号支付 4.代销售
     *
     * @param gsspmType 支付类型 
1.销售支付 2.储值卡充值 3.挂号支付 4.代销售
     */
    public void setGsspmType(String gsspmType) {
        this.gsspmType = gsspmType;
    }

    /**
     * 获取支付名称
     *
     * @return GSSPM_NAME - 支付名称
     */
    public String getGsspmName() {
        return gsspmName;
    }

    /**
     * 设置支付名称
     *
     * @param gsspmName 支付名称
     */
    public void setGsspmName(String gsspmName) {
        this.gsspmName = gsspmName;
    }

    /**
     * 获取支付卡号
     *
     * @return GSSPM_CARD_NO - 支付卡号
     */
    public String getGsspmCardNo() {
        return gsspmCardNo;
    }

    /**
     * 设置支付卡号
     *
     * @param gsspmCardNo 支付卡号
     */
    public void setGsspmCardNo(String gsspmCardNo) {
        this.gsspmCardNo = gsspmCardNo;
    }

    /**
     * 获取支付金额
     *
     * @return GSSPM_AMT - 支付金额
     */
    public BigDecimal getGsspmAmt() {
        return gsspmAmt;
    }

    /**
     * 设置支付金额
     *
     * @param gsspmAmt 支付金额
     */
    public void setGsspmAmt(BigDecimal gsspmAmt) {
        this.gsspmAmt = gsspmAmt;
    }

    /**
     * 获取RMB收款金额
     *
     * @return GSSPM_RMB_AMT - RMB收款金额
     */
    public BigDecimal getGsspmRmbAmt() {
        return gsspmRmbAmt;
    }

    /**
     * 设置RMB收款金额
     *
     * @param gsspmRmbAmt RMB收款金额
     */
    public void setGsspmRmbAmt(BigDecimal gsspmRmbAmt) {
        this.gsspmRmbAmt = gsspmRmbAmt;
    }

    /**
     * 获取找零金额
     *
     * @return GSSPM_ZL_AMT - 找零金额
     */
    public BigDecimal getGsspmZlAmt() {
        return gsspmZlAmt;
    }

    /**
     * 设置找零金额
     *
     * @param gsspmZlAmt 找零金额
     */
    public void setGsspmZlAmt(BigDecimal gsspmZlAmt) {
        this.gsspmZlAmt = gsspmZlAmt;
    }

    /**
     * 获取是否日结 0/空为否1为是
     *
     * @return GSSPM_DAYREPORT - 是否日结 0/空为否1为是
     */
    public String getGsspmDayreport() {
        return gsspmDayreport;
    }

    /**
     * 设置是否日结 0/空为否1为是
     *
     * @param gsspmDayreport 是否日结 0/空为否1为是
     */
    public void setGsspmDayreport(String gsspmDayreport) {
        this.gsspmDayreport = gsspmDayreport;
    }

    /**
     * @return LAST_UPDATE_TIME
     */
    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    /**
     * @param lastUpdateTime
     */
    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    /**
     * 获取医保卡号
     *
     * @return GSSPM_MEDICAL_CARD - 医保卡号
     */
    public String getGsspmMedicalCard() {
        return gsspmMedicalCard;
    }

    /**
     * 设置医保卡号
     *
     * @param gsspmMedicalCard 医保卡号
     */
    public void setGsspmMedicalCard(String gsspmMedicalCard) {
        this.gsspmMedicalCard = gsspmMedicalCard;
    }

    /**
     * 获取医保姓名
     *
     * @return GSSPM_MEDICAL_NAME - 医保姓名
     */
    public String getGsspmMedicalName() {
        return gsspmMedicalName;
    }

    /**
     * 设置医保姓名
     *
     * @param gsspmMedicalName 医保姓名
     */
    public void setGsspmMedicalName(String gsspmMedicalName) {
        this.gsspmMedicalName = gsspmMedicalName;
    }

    /**
     * 获取医保账户余额
     *
     * @return GSSPM_ACCOUNT_BALANCE - 医保账户余额
     */
    public String getGsspmAccountBalance() {
        return gsspmAccountBalance;
    }

    /**
     * 设置医保账户余额
     *
     * @param gsspmAccountBalance 医保账户余额
     */
    public void setGsspmAccountBalance(String gsspmAccountBalance) {
        this.gsspmAccountBalance = gsspmAccountBalance;
    }
}