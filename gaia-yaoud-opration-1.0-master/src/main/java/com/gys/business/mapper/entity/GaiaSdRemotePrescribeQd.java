package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/18 10:13
 */

@Table(name = "GAIA_SD_REMOTE_PRESCRIBE_QD")
@Data
public class GaiaSdRemotePrescribeQd implements Serializable {
    @Id
    @Column(name = "ID")
    private String iD;
    @Column(name = "CLIENT")
    private String client;
    @Column(name = "TYPE")
    private String type;
    @Column(name = "DATE")
    private String date;
    @Column(name = "TIME")
    private String time;
    @Column(name = "EMP")
    private String emp;
    @Column(name = "NAME")
    private String name;
}
