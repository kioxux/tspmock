package com.gys.business.mapper;

import com.gys.business.mapper.entity.GAIAYaoshibangStore;
import com.gys.business.service.data.yaoshibang.CustormerQueryFrom;
import com.gys.common.base.BaseMapper;

import java.util.HashMap;
import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/12/22 14:16
 */
public interface GAIAYaoshibangStoreMapper extends BaseMapper<GAIAYaoshibangStore> {
    List<HashMap<String,String>> getYaoshibangStoreByKey(CustormerQueryFrom queryFrom);
    int updateByKey(GAIAYaoshibangStore model);
}
