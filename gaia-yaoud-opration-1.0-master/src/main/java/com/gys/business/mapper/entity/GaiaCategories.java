package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 商品分类实体
 *
 * @author xiaoyuan on 2020/7/26
 */
@Data
public class GaiaCategories implements Serializable {
    private static final long serialVersionUID = 1892279901076066564L;

    /**
     * 商品id
     */
    private String proClass;

    /**
     * 商品名称
     */
    private String proClassName;
}
