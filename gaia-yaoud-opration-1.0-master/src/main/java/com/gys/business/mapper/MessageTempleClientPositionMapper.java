package com.gys.business.mapper;

import com.gys.business.mapper.entity.MessageTempleClientPosition;
import com.gys.business.service.data.MessageTemplateChooseUserDb;
import com.gys.business.service.data.MessageTemplateChooseUserReq;
import com.gys.business.service.data.MessageTemplateChooseUserRes;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 消息模版-门店-职位关系表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-09-10
 */
@Mapper
public interface MessageTempleClientPositionMapper extends BaseMapper<MessageTempleClientPosition> {
    List<MessageTemplateChooseUserDb> selectStoSettingInfo(MessageTemplateChooseUserReq req);
}
