package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMessage;
import com.gys.business.service.data.SysParamOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author xiaoyuan
 */
public interface GaiaSdMessageMapper {

    int insertSelective(GaiaSdMessage record);

    List<GaiaSdMessage> selectByExample(@Param("client") String client, @Param("brId") String brId,@Param("gsmFlag") String gsmFlag);

    int updateByPrimaryKeySelective(@Param("record") GaiaSdMessage record);

    List<SysParamOutData>selectParamListByClass(@Param("paramClass") String paramClass,@Param("flag") String flag);

    String selectNextVoucherId(@Param("clientId") String clientId,@Param("type") String type);

    String selectNextVoucherIdFX(@Param("clientId") String clientId,@Param("type") String type);

    void deleteMessageByClient(@Param("msPlatForm") String msPlatForm);

    List<Map<String,Object>> selectStoreExpiryCount(@Param("clientId") String clientId,@Param("expiryDay") String expiryDay);

    List<SysParamOutData>selectDcCodeByClient(@Param("clientId") String clientId);

    SysParamOutData selectExpiryDayByClient(@Param("clientId") String clientId,@Param("dcCode") String dcCode);

    List<Map<String,Object>> selectStockExpiry(@Param("clientId") String clientId,@Param("expiryDay") String expiryDay);

    List<Map<String,Object>> selectProExpiry(@Param("clientId") String clientId,@Param("expiryDay") String expiryDay);

    List<SysParamOutData> selectExpiryListPro();

    List<SysParamOutData> selectExpiryListSto(@Param("clientId") String clientId);

    void batchMessageSto(List<GaiaSdMessage> list);

    void deleteMessageByType(@Param("page") String page);

    List<GaiaSdMessage> selectListByNow(GaiaSdMessage inData);

    void deleteLicenseMessage(String web);
}