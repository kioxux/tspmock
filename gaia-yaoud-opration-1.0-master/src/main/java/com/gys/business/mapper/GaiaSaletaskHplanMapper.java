package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSaletaskHplan;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.business.service.data.SaleTaskOutData;
import com.gys.business.service.data.SaleTaskStroeOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSaletaskHplanMapper extends BaseMapper<GaiaSaletaskHplan> {
    List<SaleTaskOutData> selectPage(String client);

    List<SaleTaskStroeOutData> getStoreBySaleTask(GaiaStoreData storeData);
}