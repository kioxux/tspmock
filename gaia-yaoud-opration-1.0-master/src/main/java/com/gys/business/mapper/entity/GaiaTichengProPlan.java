package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(
        name = "GAIA_TICHENG_PROPLAN_Z"
)
@Data
public class GaiaTichengProPlan implements Serializable {
    @Id
    @Column(name = "ID")
    @ApiModelProperty(name = "主键ID")
    private Integer id;

    @ApiModelProperty(name = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @Column(name = "PLAN_CODE")
    @ApiModelProperty(name = "方案编码")
    private String planCode;

    @Column(name = "PLAN_NAME")
    @ApiModelProperty(name = "方案名称")
    private String planName;

    @Column(name = "PLAN_START_DATE")
    @ApiModelProperty(name = "起始日期")
    private String planStartDate;

    @Column(name = "PLAN_END_DATE")
    @ApiModelProperty(name = "结束日期")
    private String planEndDate;

    @Column(name = "PLAN_PRODUCT_WAY")
    @ApiModelProperty(name = "单品提成方式 0 不参与销售提成 1 参与销售提成")
    private String planProductWay;

    @Column(name = "PLAN_TYPE")
    @ApiModelProperty(name = "提成类型 1 销售 2 单品")
    private String planType;

    @Column(name = "PLAN_SCALE_STO")
    @ApiModelProperty(name = "门店分配比例(默认0)")
    private String planScaleSto;

    @Column(name = "PLAN_SCALE_SALER")
    @ApiModelProperty(name = "门店分配比例(默认100)")
    private String planScaleSaler;

    @Column(name = "PLAN_STATUS")
    @ApiModelProperty(name = "审核状态 0 未审核 1 已审核")
    private String planStatus;

    @Column(name = "DELETE_FLAG")
    @ApiModelProperty(name = "操作状态 0 未删除 1 已删除 2 暂存（试算）")
    private String deleteFlag;

    @Column(name = "PLAN_CREATER")
    @ApiModelProperty(name = "创建人")
    private String planCreater;

    @Column(name = "PLAN_CREATER_ID")
    @ApiModelProperty(name = "创建人编码")
    private String planCreaterId;

    @Column(name = "PLAN_CREATE_TIME")
    @ApiModelProperty(name = "创建时间")
    private String planCreateTime;

    @Column(name = "PLAN_UPDATE_ID")
    @ApiModelProperty(name = "操作人编码")
    private String planUpdateId;

    @Column(name = "PLAN_UPDATER")
    @ApiModelProperty(name = "操作人")
    private String planUpdater;

    @Column(name = "PLAN_UPDATE_DATETIME")
    @ApiModelProperty(name = "操作人时间")
    private String planUpdateDateTime;

    @Column(name = "PLAN_REASON")
    @ApiModelProperty(name = "已审核提醒内容/修改原因提醒/停用原因")
    private String planReason;

    @Column(name = "PLAN_STOP_DATE")
    @ApiModelProperty(name = "停用时间")
    private String planStopDate;
}
