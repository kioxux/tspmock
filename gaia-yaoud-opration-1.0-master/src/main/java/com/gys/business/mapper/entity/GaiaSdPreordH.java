package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SD_PREORD_H")
@Data
public class GaiaSdPreordH implements Serializable {
    private static final long serialVersionUID = 1877488674993319749L;
    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 订购门店
     */
    @Column(name = "GSPH_BR_ID")
    private String gsphBrId;

    /**
     * 订购单号
     */
    @Column(name = "GSPH_VOUCHER_ID")
    private String gsphVoucherId;

    /**
     * 订购日期
     */
    @Column(name = "GSPH_DATE")
    private String gsphDate;

    /**
     * 订购时间
     */
    @Column(name = "GSPH_TIME")
    private String gsphTime;

    /**
     * 订购金额
     */
    @Column(name = "GSPH_AMT")
    private BigDecimal gsphAmt;

    /**
     * 顾客姓名
     */
    @Column(name = "GSPH_COSTOER_NAME")
    private String gsphCostoerName;

    /**
     * 顾客手机
     */
    @Column(name = "GSPH_COSTOER_MOBILE")
    private String gsphCostoerMobile;

    /**
     * 顾客身份证
     */
    @Column(name = "GSPH_COSTOER_ID")
    private String gsphCostoerId;
    /**
     * 顾客备注
     */
    @Column(name = "GSPH_COSTOER_REMARKS")
    private String gsphcostoerRemarks;

    /**
     * 会员卡号
     */
    @Column(name = "GSPH_MEMBER_ID")
    private String gsphMemberId;

    /**
     * 录入员工
     */
    @Column(name = "GSPH_EMP")
    private String gsphEmp;

    /**
     * 是否调用
     */
    @Column(name = "GSPH_FLAG")
    private String gsphFlag;

    /**
     * 调用日期
     */
    @Column(name = "GSPH_UPDATE_DATE")
    private String gsphUpdateDate;

    /**
     * 调用时间
     */
    @Column(name = "GSPH_UPDATE_TIME")
    private String gsphUpdateTime;

    /**
     * 销售单号
     */
    @Column(name = "GSPH_BILL_NO")
    private String gsphBillNo;

    /**
     * 单据类型：DG:订购单；HX:核销单
     */
    @Column(name = "GSPH_TYPE")
    private String gsphType;

    /**
     * 订购单状态：1正常，2退款，3核销
     */
    @Column(name = "GSPH_STATUS")
    private String gsphStatus;



}