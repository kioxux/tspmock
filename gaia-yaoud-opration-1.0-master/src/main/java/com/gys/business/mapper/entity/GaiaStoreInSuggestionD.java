package com.gys.business.mapper.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

/**
 * 门店调入建议-明细表(GaiaStoreInSuggestionD)实体类
 *
 * @author makejava
 * @since 2021-10-28 10:52:57
 */
@Data
public class GaiaStoreInSuggestionD implements Serializable {
    private static final long serialVersionUID = -63100790498154899L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店编码
     */
    private String stoCode;
    /**
     * 门店名称
     */
    private String stoName;
    /**
     * 商品调库单号
     */
    private String billCode;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 规格
     */
    private String proSpces;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 月均销量
     */
    private BigDecimal averageSalesQty;
    /**
     * 库存量
     */
    private BigDecimal inventoryQty;
    /**
     * 确认调剂量
     */
    private BigDecimal confirmQty;
    /**
     * 最终调出量
     */
    private BigDecimal finalQty;
    /**
     * 建议调出数量
     */
    private BigDecimal suggestionOutQty;
    /**
     * 批号
     */
    private String batchNo;
    /**
     * 有效期至
     */
    private String validDay;
    /**
     * 月均销售额
     */
    private BigDecimal averageSalesAmt;
    /**
     * 成分分类
     */
    private String proCompclass;
    /**
     * 商品分类
     */
    private String proClass;
    /**
     * 备注
     */
    private String remark;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;

}

