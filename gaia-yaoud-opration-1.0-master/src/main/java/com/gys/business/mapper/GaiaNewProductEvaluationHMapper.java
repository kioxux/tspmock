package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaNewProductEvaluationD;
import com.gys.business.mapper.entity.GaiaNewProductEvaluationH;
import com.gys.business.service.data.GaiaNewProductEvaluationInData;
import com.gys.business.service.data.GaiaNewProductEvaluationOutData;
import com.gys.business.service.data.GaiaProductEvaluationDetailOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface GaiaNewProductEvaluationHMapper extends BaseMapper<GaiaNewProductEvaluationH> {

    List<GaiaNewProductEvaluationD> listProductionInfo(GaiaNewProductEvaluationInData inData);

    String getBillCode(@Param("clientId") String clientId);

    GaiaNewProductEvaluationOutData getBillByClientAndStorId(GaiaNewProductEvaluationInData inData);

    void insertInfo(GaiaNewProductEvaluationH evaluationH);

    List<GaiaProductEvaluationDetailOutData> listBillDetail(GaiaNewProductEvaluationInData inData);

    void updateInfo(GaiaNewProductEvaluationH head);

    List<GaiaNewProductEvaluationOutData> listBillInfo(GaiaNewProductEvaluationOutData param);

    List<HashMap<String, String>> listBigClass(GaiaNewProductEvaluationOutData param);

    List<GaiaProductEvaluationDetailOutData > listUnCheckBill();

    HashMap<String, Object> getEntInfo(GaiaNewProductEvaluationInData inData);
}