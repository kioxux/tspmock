package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "GAIA_BASICS_GROUP_INFO")
public class GaiaBasicsGroupInfo implements Serializable {
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private String id;

    /**
     * 编码
     */
    @Column(name = "CODE")
    private String code;

    /**
     * 名称
     */
    @Column(name = "NAME")
    private String name;

    /**
     * 模块ID
     */
    @Column(name = "MODEL")
    private String model;

    /**
     * 是否后台角色(Y.删除  N.不删除)
     */
    @Column(name = "ISADMIN")
    private String isadmin;

    /**
     * 是否门店角色(Y.删除  N.不删除)
     */
    @Column(name = "ISSTO")
    private String issto;

    /**
     * 是否APP角色(Y.删除  N.不删除)
     */
    @Column(name = "ISAPP")
    private String isapp;

    /**
     * 备注
     */
    @Column(name = "REMARK")
    private String remark;

    /**
     * 删除(Y.删除  N.不删除)
     */
    @Column(name = "DELETE_FLAG")
    private String deleteFlag;

    /**
     * 创建日期
     */
    @Column(name = "CRE_DATE")
    private String creDate;

    /**
     * 创建时间
     */
    @Column(name = "CRE_TIME")
    private String creTime;

    /**
     * 创建人
     */
    @Column(name = "CRE_ID")
    private String creId;

    /**
     * 修改日期
     */
    @Column(name = "MOD_DATE")
    private String modDate;

    /**
     * 修改时间
     */
    @Column(name = "MOD_TIME")
    private String modTime;

    /**
     * 修改人
     */
    @Column(name = "MOD_ID")
    private String modId;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public String getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取编码
     *
     * @return CODE - 编码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置编码
     *
     * @param code 编码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取名称
     *
     * @return NAME - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取模块ID
     *
     * @return MODEL - 模块ID
     */
    public String getModel() {
        return model;
    }

    /**
     * 设置模块ID
     *
     * @param model 模块ID
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * 获取是否后台角色(Y.删除  N.不删除)
     *
     * @return ISADMIN - 是否后台角色(Y.删除  N.不删除)
     */
    public String getIsadmin() {
        return isadmin;
    }

    /**
     * 设置是否后台角色(Y.删除  N.不删除)
     *
     * @param isadmin 是否后台角色(Y.删除  N.不删除)
     */
    public void setIsadmin(String isadmin) {
        this.isadmin = isadmin;
    }

    /**
     * 获取是否门店角色(Y.删除  N.不删除)
     *
     * @return ISSTO - 是否门店角色(Y.删除  N.不删除)
     */
    public String getIssto() {
        return issto;
    }

    /**
     * 设置是否门店角色(Y.删除  N.不删除)
     *
     * @param issto 是否门店角色(Y.删除  N.不删除)
     */
    public void setIssto(String issto) {
        this.issto = issto;
    }

    /**
     * 获取是否APP角色(Y.删除  N.不删除)
     *
     * @return ISAPP - 是否APP角色(Y.删除  N.不删除)
     */
    public String getIsapp() {
        return isapp;
    }

    /**
     * 设置是否APP角色(Y.删除  N.不删除)
     *
     * @param isapp 是否APP角色(Y.删除  N.不删除)
     */
    public void setIsapp(String isapp) {
        this.isapp = isapp;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取删除(Y.删除  N.不删除)
     *
     * @return DELETE_FLAG - 删除(Y.删除  N.不删除)
     */
    public String getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * 设置删除(Y.删除  N.不删除)
     *
     * @param deleteFlag 删除(Y.删除  N.不删除)
     */
    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * 获取创建日期
     *
     * @return CRE_DATE - 创建日期
     */
    public String getCreDate() {
        return creDate;
    }

    /**
     * 设置创建日期
     *
     * @param creDate 创建日期
     */
    public void setCreDate(String creDate) {
        this.creDate = creDate;
    }

    /**
     * 获取创建时间
     *
     * @return CRE_TIME - 创建时间
     */
    public String getCreTime() {
        return creTime;
    }

    /**
     * 设置创建时间
     *
     * @param creTime 创建时间
     */
    public void setCreTime(String creTime) {
        this.creTime = creTime;
    }

    /**
     * 获取创建人
     *
     * @return CRE_ID - 创建人
     */
    public String getCreId() {
        return creId;
    }

    /**
     * 设置创建人
     *
     * @param creId 创建人
     */
    public void setCreId(String creId) {
        this.creId = creId;
    }

    /**
     * 获取修改日期
     *
     * @return MOD_DATE - 修改日期
     */
    public String getModDate() {
        return modDate;
    }

    /**
     * 设置修改日期
     *
     * @param modDate 修改日期
     */
    public void setModDate(String modDate) {
        this.modDate = modDate;
    }

    /**
     * 获取修改时间
     *
     * @return MOD_TIME - 修改时间
     */
    public String getModTime() {
        return modTime;
    }

    /**
     * 设置修改时间
     *
     * @param modTime 修改时间
     */
    public void setModTime(String modTime) {
        this.modTime = modTime;
    }

    /**
     * 获取修改人
     *
     * @return MOD_ID - 修改人
     */
    public String getModId() {
        return modId;
    }

    /**
     * 设置修改人
     *
     * @param modId 修改人
     */
    public void setModId(String modId) {
        this.modId = modId;
    }
}