package com.gys.business.mapper.entity;

import java.util.ArrayList;
import java.util.List;

public class GaiaThirdPaymentTonglianExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GaiaThirdPaymentTonglianExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andClientIsNull() {
            addCriterion("CLIENT is null");
            return (Criteria) this;
        }

        public Criteria andClientIsNotNull() {
            addCriterion("CLIENT is not null");
            return (Criteria) this;
        }

        public Criteria andClientEqualTo(String value) {
            addCriterion("CLIENT =", value, "client");
            return (Criteria) this;
        }

        public Criteria andClientNotEqualTo(String value) {
            addCriterion("CLIENT <>", value, "client");
            return (Criteria) this;
        }

        public Criteria andClientGreaterThan(String value) {
            addCriterion("CLIENT >", value, "client");
            return (Criteria) this;
        }

        public Criteria andClientGreaterThanOrEqualTo(String value) {
            addCriterion("CLIENT >=", value, "client");
            return (Criteria) this;
        }

        public Criteria andClientLessThan(String value) {
            addCriterion("CLIENT <", value, "client");
            return (Criteria) this;
        }

        public Criteria andClientLessThanOrEqualTo(String value) {
            addCriterion("CLIENT <=", value, "client");
            return (Criteria) this;
        }

        public Criteria andClientLike(String value) {
            addCriterion("CLIENT like", value, "client");
            return (Criteria) this;
        }

        public Criteria andClientNotLike(String value) {
            addCriterion("CLIENT not like", value, "client");
            return (Criteria) this;
        }

        public Criteria andClientIn(List<String> values) {
            addCriterion("CLIENT in", values, "client");
            return (Criteria) this;
        }

        public Criteria andClientNotIn(List<String> values) {
            addCriterion("CLIENT not in", values, "client");
            return (Criteria) this;
        }

        public Criteria andClientBetween(String value1, String value2) {
            addCriterion("CLIENT between", value1, value2, "client");
            return (Criteria) this;
        }

        public Criteria andClientNotBetween(String value1, String value2) {
            addCriterion("CLIENT not between", value1, value2, "client");
            return (Criteria) this;
        }

        public Criteria andStoCodeIsNull() {
            addCriterion("STO_CODE is null");
            return (Criteria) this;
        }

        public Criteria andStoCodeIsNotNull() {
            addCriterion("STO_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andStoCodeEqualTo(String value) {
            addCriterion("STO_CODE =", value, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeNotEqualTo(String value) {
            addCriterion("STO_CODE <>", value, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeGreaterThan(String value) {
            addCriterion("STO_CODE >", value, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeGreaterThanOrEqualTo(String value) {
            addCriterion("STO_CODE >=", value, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeLessThan(String value) {
            addCriterion("STO_CODE <", value, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeLessThanOrEqualTo(String value) {
            addCriterion("STO_CODE <=", value, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeLike(String value) {
            addCriterion("STO_CODE like", value, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeNotLike(String value) {
            addCriterion("STO_CODE not like", value, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeIn(List<String> values) {
            addCriterion("STO_CODE in", values, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeNotIn(List<String> values) {
            addCriterion("STO_CODE not in", values, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeBetween(String value1, String value2) {
            addCriterion("STO_CODE between", value1, value2, "stoCode");
            return (Criteria) this;
        }

        public Criteria andStoCodeNotBetween(String value1, String value2) {
            addCriterion("STO_CODE not between", value1, value2, "stoCode");
            return (Criteria) this;
        }

        public Criteria andBusinessidIsNull() {
            addCriterion("BusinessId is null");
            return (Criteria) this;
        }

        public Criteria andBusinessidIsNotNull() {
            addCriterion("BusinessId is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessidEqualTo(String value) {
            addCriterion("BusinessId =", value, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidNotEqualTo(String value) {
            addCriterion("BusinessId <>", value, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidGreaterThan(String value) {
            addCriterion("BusinessId >", value, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidGreaterThanOrEqualTo(String value) {
            addCriterion("BusinessId >=", value, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidLessThan(String value) {
            addCriterion("BusinessId <", value, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidLessThanOrEqualTo(String value) {
            addCriterion("BusinessId <=", value, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidLike(String value) {
            addCriterion("BusinessId like", value, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidNotLike(String value) {
            addCriterion("BusinessId not like", value, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidIn(List<String> values) {
            addCriterion("BusinessId in", values, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidNotIn(List<String> values) {
            addCriterion("BusinessId not in", values, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidBetween(String value1, String value2) {
            addCriterion("BusinessId between", value1, value2, "businessid");
            return (Criteria) this;
        }

        public Criteria andBusinessidNotBetween(String value1, String value2) {
            addCriterion("BusinessId not between", value1, value2, "businessid");
            return (Criteria) this;
        }

        public Criteria andTranstypeIsNull() {
            addCriterion("TransType is null");
            return (Criteria) this;
        }

        public Criteria andTranstypeIsNotNull() {
            addCriterion("TransType is not null");
            return (Criteria) this;
        }

        public Criteria andTranstypeEqualTo(String value) {
            addCriterion("TransType =", value, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeNotEqualTo(String value) {
            addCriterion("TransType <>", value, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeGreaterThan(String value) {
            addCriterion("TransType >", value, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeGreaterThanOrEqualTo(String value) {
            addCriterion("TransType >=", value, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeLessThan(String value) {
            addCriterion("TransType <", value, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeLessThanOrEqualTo(String value) {
            addCriterion("TransType <=", value, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeLike(String value) {
            addCriterion("TransType like", value, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeNotLike(String value) {
            addCriterion("TransType not like", value, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeIn(List<String> values) {
            addCriterion("TransType in", values, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeNotIn(List<String> values) {
            addCriterion("TransType not in", values, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeBetween(String value1, String value2) {
            addCriterion("TransType between", value1, value2, "transtype");
            return (Criteria) this;
        }

        public Criteria andTranstypeNotBetween(String value1, String value2) {
            addCriterion("TransType not between", value1, value2, "transtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeIsNull() {
            addCriterion("CardType is null");
            return (Criteria) this;
        }

        public Criteria andCardtypeIsNotNull() {
            addCriterion("CardType is not null");
            return (Criteria) this;
        }

        public Criteria andCardtypeEqualTo(String value) {
            addCriterion("CardType =", value, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeNotEqualTo(String value) {
            addCriterion("CardType <>", value, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeGreaterThan(String value) {
            addCriterion("CardType >", value, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeGreaterThanOrEqualTo(String value) {
            addCriterion("CardType >=", value, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeLessThan(String value) {
            addCriterion("CardType <", value, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeLessThanOrEqualTo(String value) {
            addCriterion("CardType <=", value, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeLike(String value) {
            addCriterion("CardType like", value, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeNotLike(String value) {
            addCriterion("CardType not like", value, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeIn(List<String> values) {
            addCriterion("CardType in", values, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeNotIn(List<String> values) {
            addCriterion("CardType not in", values, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeBetween(String value1, String value2) {
            addCriterion("CardType between", value1, value2, "cardtype");
            return (Criteria) this;
        }

        public Criteria andCardtypeNotBetween(String value1, String value2) {
            addCriterion("CardType not between", value1, value2, "cardtype");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNull() {
            addCriterion("Operator is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIsNotNull() {
            addCriterion("Operator is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorEqualTo(String value) {
            addCriterion("Operator =", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotEqualTo(String value) {
            addCriterion("Operator <>", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThan(String value) {
            addCriterion("Operator >", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorGreaterThanOrEqualTo(String value) {
            addCriterion("Operator >=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThan(String value) {
            addCriterion("Operator <", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLessThanOrEqualTo(String value) {
            addCriterion("Operator <=", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorLike(String value) {
            addCriterion("Operator like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotLike(String value) {
            addCriterion("Operator not like", value, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorIn(List<String> values) {
            addCriterion("Operator in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotIn(List<String> values) {
            addCriterion("Operator not in", values, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorBetween(String value1, String value2) {
            addCriterion("Operator between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andOperatorNotBetween(String value1, String value2) {
            addCriterion("Operator not between", value1, value2, "operator");
            return (Criteria) this;
        }

        public Criteria andStorenumberIsNull() {
            addCriterion("StoreNumber is null");
            return (Criteria) this;
        }

        public Criteria andStorenumberIsNotNull() {
            addCriterion("StoreNumber is not null");
            return (Criteria) this;
        }

        public Criteria andStorenumberEqualTo(String value) {
            addCriterion("StoreNumber =", value, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberNotEqualTo(String value) {
            addCriterion("StoreNumber <>", value, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberGreaterThan(String value) {
            addCriterion("StoreNumber >", value, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberGreaterThanOrEqualTo(String value) {
            addCriterion("StoreNumber >=", value, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberLessThan(String value) {
            addCriterion("StoreNumber <", value, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberLessThanOrEqualTo(String value) {
            addCriterion("StoreNumber <=", value, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberLike(String value) {
            addCriterion("StoreNumber like", value, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberNotLike(String value) {
            addCriterion("StoreNumber not like", value, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberIn(List<String> values) {
            addCriterion("StoreNumber in", values, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberNotIn(List<String> values) {
            addCriterion("StoreNumber not in", values, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberBetween(String value1, String value2) {
            addCriterion("StoreNumber between", value1, value2, "storenumber");
            return (Criteria) this;
        }

        public Criteria andStorenumberNotBetween(String value1, String value2) {
            addCriterion("StoreNumber not between", value1, value2, "storenumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberIsNull() {
            addCriterion("PosNumber is null");
            return (Criteria) this;
        }

        public Criteria andPosnumberIsNotNull() {
            addCriterion("PosNumber is not null");
            return (Criteria) this;
        }

        public Criteria andPosnumberEqualTo(String value) {
            addCriterion("PosNumber =", value, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberNotEqualTo(String value) {
            addCriterion("PosNumber <>", value, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberGreaterThan(String value) {
            addCriterion("PosNumber >", value, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberGreaterThanOrEqualTo(String value) {
            addCriterion("PosNumber >=", value, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberLessThan(String value) {
            addCriterion("PosNumber <", value, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberLessThanOrEqualTo(String value) {
            addCriterion("PosNumber <=", value, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberLike(String value) {
            addCriterion("PosNumber like", value, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberNotLike(String value) {
            addCriterion("PosNumber not like", value, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberIn(List<String> values) {
            addCriterion("PosNumber in", values, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberNotIn(List<String> values) {
            addCriterion("PosNumber not in", values, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberBetween(String value1, String value2) {
            addCriterion("PosNumber between", value1, value2, "posnumber");
            return (Criteria) this;
        }

        public Criteria andPosnumberNotBetween(String value1, String value2) {
            addCriterion("PosNumber not between", value1, value2, "posnumber");
            return (Criteria) this;
        }

        public Criteria andTipsIsNull() {
            addCriterion("Tips is null");
            return (Criteria) this;
        }

        public Criteria andTipsIsNotNull() {
            addCriterion("Tips is not null");
            return (Criteria) this;
        }

        public Criteria andTipsEqualTo(String value) {
            addCriterion("Tips =", value, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsNotEqualTo(String value) {
            addCriterion("Tips <>", value, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsGreaterThan(String value) {
            addCriterion("Tips >", value, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsGreaterThanOrEqualTo(String value) {
            addCriterion("Tips >=", value, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsLessThan(String value) {
            addCriterion("Tips <", value, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsLessThanOrEqualTo(String value) {
            addCriterion("Tips <=", value, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsLike(String value) {
            addCriterion("Tips like", value, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsNotLike(String value) {
            addCriterion("Tips not like", value, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsIn(List<String> values) {
            addCriterion("Tips in", values, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsNotIn(List<String> values) {
            addCriterion("Tips not in", values, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsBetween(String value1, String value2) {
            addCriterion("Tips between", value1, value2, "tips");
            return (Criteria) this;
        }

        public Criteria andTipsNotBetween(String value1, String value2) {
            addCriterion("Tips not between", value1, value2, "tips");
            return (Criteria) this;
        }

        public Criteria andTotalIsNull() {
            addCriterion("Total is null");
            return (Criteria) this;
        }

        public Criteria andTotalIsNotNull() {
            addCriterion("Total is not null");
            return (Criteria) this;
        }

        public Criteria andTotalEqualTo(String value) {
            addCriterion("Total =", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotEqualTo(String value) {
            addCriterion("Total <>", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalGreaterThan(String value) {
            addCriterion("Total >", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalGreaterThanOrEqualTo(String value) {
            addCriterion("Total >=", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLessThan(String value) {
            addCriterion("Total <", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLessThanOrEqualTo(String value) {
            addCriterion("Total <=", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLike(String value) {
            addCriterion("Total like", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotLike(String value) {
            addCriterion("Total not like", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalIn(List<String> values) {
            addCriterion("Total in", values, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotIn(List<String> values) {
            addCriterion("Total not in", values, "total");
            return (Criteria) this;
        }

        public Criteria andTotalBetween(String value1, String value2) {
            addCriterion("Total between", value1, value2, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotBetween(String value1, String value2) {
            addCriterion("Total not between", value1, value2, "total");
            return (Criteria) this;
        }

        public Criteria andAmountIsNull() {
            addCriterion("Amount is null");
            return (Criteria) this;
        }

        public Criteria andAmountIsNotNull() {
            addCriterion("Amount is not null");
            return (Criteria) this;
        }

        public Criteria andAmountEqualTo(String value) {
            addCriterion("Amount =", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotEqualTo(String value) {
            addCriterion("Amount <>", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThan(String value) {
            addCriterion("Amount >", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThanOrEqualTo(String value) {
            addCriterion("Amount >=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThan(String value) {
            addCriterion("Amount <", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThanOrEqualTo(String value) {
            addCriterion("Amount <=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLike(String value) {
            addCriterion("Amount like", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotLike(String value) {
            addCriterion("Amount not like", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountIn(List<String> values) {
            addCriterion("Amount in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotIn(List<String> values) {
            addCriterion("Amount not in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountBetween(String value1, String value2) {
            addCriterion("Amount between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotBetween(String value1, String value2) {
            addCriterion("Amount not between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountIsNull() {
            addCriterion("BalanceAmount is null");
            return (Criteria) this;
        }

        public Criteria andBalanceamountIsNotNull() {
            addCriterion("BalanceAmount is not null");
            return (Criteria) this;
        }

        public Criteria andBalanceamountEqualTo(String value) {
            addCriterion("BalanceAmount =", value, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountNotEqualTo(String value) {
            addCriterion("BalanceAmount <>", value, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountGreaterThan(String value) {
            addCriterion("BalanceAmount >", value, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountGreaterThanOrEqualTo(String value) {
            addCriterion("BalanceAmount >=", value, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountLessThan(String value) {
            addCriterion("BalanceAmount <", value, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountLessThanOrEqualTo(String value) {
            addCriterion("BalanceAmount <=", value, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountLike(String value) {
            addCriterion("BalanceAmount like", value, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountNotLike(String value) {
            addCriterion("BalanceAmount not like", value, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountIn(List<String> values) {
            addCriterion("BalanceAmount in", values, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountNotIn(List<String> values) {
            addCriterion("BalanceAmount not in", values, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountBetween(String value1, String value2) {
            addCriterion("BalanceAmount between", value1, value2, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andBalanceamountNotBetween(String value1, String value2) {
            addCriterion("BalanceAmount not between", value1, value2, "balanceamount");
            return (Criteria) this;
        }

        public Criteria andPostracenumberIsNull() {
            addCriterion("PosTraceNumber is null");
            return (Criteria) this;
        }

        public Criteria andPostracenumberIsNotNull() {
            addCriterion("PosTraceNumber is not null");
            return (Criteria) this;
        }

        public Criteria andPostracenumberEqualTo(String value) {
            addCriterion("PosTraceNumber =", value, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberNotEqualTo(String value) {
            addCriterion("PosTraceNumber <>", value, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberGreaterThan(String value) {
            addCriterion("PosTraceNumber >", value, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberGreaterThanOrEqualTo(String value) {
            addCriterion("PosTraceNumber >=", value, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberLessThan(String value) {
            addCriterion("PosTraceNumber <", value, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberLessThanOrEqualTo(String value) {
            addCriterion("PosTraceNumber <=", value, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberLike(String value) {
            addCriterion("PosTraceNumber like", value, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberNotLike(String value) {
            addCriterion("PosTraceNumber not like", value, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberIn(List<String> values) {
            addCriterion("PosTraceNumber in", values, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberNotIn(List<String> values) {
            addCriterion("PosTraceNumber not in", values, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberBetween(String value1, String value2) {
            addCriterion("PosTraceNumber between", value1, value2, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andPostracenumberNotBetween(String value1, String value2) {
            addCriterion("PosTraceNumber not between", value1, value2, "postracenumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberIsNull() {
            addCriterion("CardNumber is null");
            return (Criteria) this;
        }

        public Criteria andCardnumberIsNotNull() {
            addCriterion("CardNumber is not null");
            return (Criteria) this;
        }

        public Criteria andCardnumberEqualTo(String value) {
            addCriterion("CardNumber =", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberNotEqualTo(String value) {
            addCriterion("CardNumber <>", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberGreaterThan(String value) {
            addCriterion("CardNumber >", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberGreaterThanOrEqualTo(String value) {
            addCriterion("CardNumber >=", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberLessThan(String value) {
            addCriterion("CardNumber <", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberLessThanOrEqualTo(String value) {
            addCriterion("CardNumber <=", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberLike(String value) {
            addCriterion("CardNumber like", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberNotLike(String value) {
            addCriterion("CardNumber not like", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberIn(List<String> values) {
            addCriterion("CardNumber in", values, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberNotIn(List<String> values) {
            addCriterion("CardNumber not in", values, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberBetween(String value1, String value2) {
            addCriterion("CardNumber between", value1, value2, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberNotBetween(String value1, String value2) {
            addCriterion("CardNumber not between", value1, value2, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberIsNull() {
            addCriterion("OldTraceNumber is null");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberIsNotNull() {
            addCriterion("OldTraceNumber is not null");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberEqualTo(String value) {
            addCriterion("OldTraceNumber =", value, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberNotEqualTo(String value) {
            addCriterion("OldTraceNumber <>", value, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberGreaterThan(String value) {
            addCriterion("OldTraceNumber >", value, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberGreaterThanOrEqualTo(String value) {
            addCriterion("OldTraceNumber >=", value, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberLessThan(String value) {
            addCriterion("OldTraceNumber <", value, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberLessThanOrEqualTo(String value) {
            addCriterion("OldTraceNumber <=", value, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberLike(String value) {
            addCriterion("OldTraceNumber like", value, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberNotLike(String value) {
            addCriterion("OldTraceNumber not like", value, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberIn(List<String> values) {
            addCriterion("OldTraceNumber in", values, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberNotIn(List<String> values) {
            addCriterion("OldTraceNumber not in", values, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberBetween(String value1, String value2) {
            addCriterion("OldTraceNumber between", value1, value2, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andOldtracenumberNotBetween(String value1, String value2) {
            addCriterion("OldTraceNumber not between", value1, value2, "oldtracenumber");
            return (Criteria) this;
        }

        public Criteria andExpiredateIsNull() {
            addCriterion("ExpireDate is null");
            return (Criteria) this;
        }

        public Criteria andExpiredateIsNotNull() {
            addCriterion("ExpireDate is not null");
            return (Criteria) this;
        }

        public Criteria andExpiredateEqualTo(String value) {
            addCriterion("ExpireDate =", value, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateNotEqualTo(String value) {
            addCriterion("ExpireDate <>", value, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateGreaterThan(String value) {
            addCriterion("ExpireDate >", value, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateGreaterThanOrEqualTo(String value) {
            addCriterion("ExpireDate >=", value, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateLessThan(String value) {
            addCriterion("ExpireDate <", value, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateLessThanOrEqualTo(String value) {
            addCriterion("ExpireDate <=", value, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateLike(String value) {
            addCriterion("ExpireDate like", value, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateNotLike(String value) {
            addCriterion("ExpireDate not like", value, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateIn(List<String> values) {
            addCriterion("ExpireDate in", values, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateNotIn(List<String> values) {
            addCriterion("ExpireDate not in", values, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateBetween(String value1, String value2) {
            addCriterion("ExpireDate between", value1, value2, "expiredate");
            return (Criteria) this;
        }

        public Criteria andExpiredateNotBetween(String value1, String value2) {
            addCriterion("ExpireDate not between", value1, value2, "expiredate");
            return (Criteria) this;
        }

        public Criteria andBatchnumberIsNull() {
            addCriterion("BatchNumber is null");
            return (Criteria) this;
        }

        public Criteria andBatchnumberIsNotNull() {
            addCriterion("BatchNumber is not null");
            return (Criteria) this;
        }

        public Criteria andBatchnumberEqualTo(String value) {
            addCriterion("BatchNumber =", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberNotEqualTo(String value) {
            addCriterion("BatchNumber <>", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberGreaterThan(String value) {
            addCriterion("BatchNumber >", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberGreaterThanOrEqualTo(String value) {
            addCriterion("BatchNumber >=", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberLessThan(String value) {
            addCriterion("BatchNumber <", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberLessThanOrEqualTo(String value) {
            addCriterion("BatchNumber <=", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberLike(String value) {
            addCriterion("BatchNumber like", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberNotLike(String value) {
            addCriterion("BatchNumber not like", value, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberIn(List<String> values) {
            addCriterion("BatchNumber in", values, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberNotIn(List<String> values) {
            addCriterion("BatchNumber not in", values, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberBetween(String value1, String value2) {
            addCriterion("BatchNumber between", value1, value2, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andBatchnumberNotBetween(String value1, String value2) {
            addCriterion("BatchNumber not between", value1, value2, "batchnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberIsNull() {
            addCriterion("MerchantNumber is null");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberIsNotNull() {
            addCriterion("MerchantNumber is not null");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberEqualTo(String value) {
            addCriterion("MerchantNumber =", value, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberNotEqualTo(String value) {
            addCriterion("MerchantNumber <>", value, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberGreaterThan(String value) {
            addCriterion("MerchantNumber >", value, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberGreaterThanOrEqualTo(String value) {
            addCriterion("MerchantNumber >=", value, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberLessThan(String value) {
            addCriterion("MerchantNumber <", value, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberLessThanOrEqualTo(String value) {
            addCriterion("MerchantNumber <=", value, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberLike(String value) {
            addCriterion("MerchantNumber like", value, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberNotLike(String value) {
            addCriterion("MerchantNumber not like", value, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberIn(List<String> values) {
            addCriterion("MerchantNumber in", values, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberNotIn(List<String> values) {
            addCriterion("MerchantNumber not in", values, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberBetween(String value1, String value2) {
            addCriterion("MerchantNumber between", value1, value2, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnumberNotBetween(String value1, String value2) {
            addCriterion("MerchantNumber not between", value1, value2, "merchantnumber");
            return (Criteria) this;
        }

        public Criteria andMerchantnameIsNull() {
            addCriterion("MerchantName is null");
            return (Criteria) this;
        }

        public Criteria andMerchantnameIsNotNull() {
            addCriterion("MerchantName is not null");
            return (Criteria) this;
        }

        public Criteria andMerchantnameEqualTo(String value) {
            addCriterion("MerchantName =", value, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameNotEqualTo(String value) {
            addCriterion("MerchantName <>", value, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameGreaterThan(String value) {
            addCriterion("MerchantName >", value, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameGreaterThanOrEqualTo(String value) {
            addCriterion("MerchantName >=", value, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameLessThan(String value) {
            addCriterion("MerchantName <", value, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameLessThanOrEqualTo(String value) {
            addCriterion("MerchantName <=", value, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameLike(String value) {
            addCriterion("MerchantName like", value, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameNotLike(String value) {
            addCriterion("MerchantName not like", value, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameIn(List<String> values) {
            addCriterion("MerchantName in", values, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameNotIn(List<String> values) {
            addCriterion("MerchantName not in", values, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameBetween(String value1, String value2) {
            addCriterion("MerchantName between", value1, value2, "merchantname");
            return (Criteria) this;
        }

        public Criteria andMerchantnameNotBetween(String value1, String value2) {
            addCriterion("MerchantName not between", value1, value2, "merchantname");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberIsNull() {
            addCriterion("TerminalNumber is null");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberIsNotNull() {
            addCriterion("TerminalNumber is not null");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberEqualTo(String value) {
            addCriterion("TerminalNumber =", value, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberNotEqualTo(String value) {
            addCriterion("TerminalNumber <>", value, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberGreaterThan(String value) {
            addCriterion("TerminalNumber >", value, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberGreaterThanOrEqualTo(String value) {
            addCriterion("TerminalNumber >=", value, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberLessThan(String value) {
            addCriterion("TerminalNumber <", value, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberLessThanOrEqualTo(String value) {
            addCriterion("TerminalNumber <=", value, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberLike(String value) {
            addCriterion("TerminalNumber like", value, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberNotLike(String value) {
            addCriterion("TerminalNumber not like", value, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberIn(List<String> values) {
            addCriterion("TerminalNumber in", values, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberNotIn(List<String> values) {
            addCriterion("TerminalNumber not in", values, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberBetween(String value1, String value2) {
            addCriterion("TerminalNumber between", value1, value2, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andTerminalnumberNotBetween(String value1, String value2) {
            addCriterion("TerminalNumber not between", value1, value2, "terminalnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberIsNull() {
            addCriterion("HostSerialNumber is null");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberIsNotNull() {
            addCriterion("HostSerialNumber is not null");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberEqualTo(String value) {
            addCriterion("HostSerialNumber =", value, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberNotEqualTo(String value) {
            addCriterion("HostSerialNumber <>", value, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberGreaterThan(String value) {
            addCriterion("HostSerialNumber >", value, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberGreaterThanOrEqualTo(String value) {
            addCriterion("HostSerialNumber >=", value, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberLessThan(String value) {
            addCriterion("HostSerialNumber <", value, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberLessThanOrEqualTo(String value) {
            addCriterion("HostSerialNumber <=", value, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberLike(String value) {
            addCriterion("HostSerialNumber like", value, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberNotLike(String value) {
            addCriterion("HostSerialNumber not like", value, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberIn(List<String> values) {
            addCriterion("HostSerialNumber in", values, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberNotIn(List<String> values) {
            addCriterion("HostSerialNumber not in", values, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberBetween(String value1, String value2) {
            addCriterion("HostSerialNumber between", value1, value2, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andHostserialnumberNotBetween(String value1, String value2) {
            addCriterion("HostSerialNumber not between", value1, value2, "hostserialnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberIsNull() {
            addCriterion("AuthNumber is null");
            return (Criteria) this;
        }

        public Criteria andAuthnumberIsNotNull() {
            addCriterion("AuthNumber is not null");
            return (Criteria) this;
        }

        public Criteria andAuthnumberEqualTo(String value) {
            addCriterion("AuthNumber =", value, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberNotEqualTo(String value) {
            addCriterion("AuthNumber <>", value, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberGreaterThan(String value) {
            addCriterion("AuthNumber >", value, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberGreaterThanOrEqualTo(String value) {
            addCriterion("AuthNumber >=", value, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberLessThan(String value) {
            addCriterion("AuthNumber <", value, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberLessThanOrEqualTo(String value) {
            addCriterion("AuthNumber <=", value, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberLike(String value) {
            addCriterion("AuthNumber like", value, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberNotLike(String value) {
            addCriterion("AuthNumber not like", value, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberIn(List<String> values) {
            addCriterion("AuthNumber in", values, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberNotIn(List<String> values) {
            addCriterion("AuthNumber not in", values, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberBetween(String value1, String value2) {
            addCriterion("AuthNumber between", value1, value2, "authnumber");
            return (Criteria) this;
        }

        public Criteria andAuthnumberNotBetween(String value1, String value2) {
            addCriterion("AuthNumber not between", value1, value2, "authnumber");
            return (Criteria) this;
        }

        public Criteria andRejcodeIsNull() {
            addCriterion("RejCode is null");
            return (Criteria) this;
        }

        public Criteria andRejcodeIsNotNull() {
            addCriterion("RejCode is not null");
            return (Criteria) this;
        }

        public Criteria andRejcodeEqualTo(String value) {
            addCriterion("RejCode =", value, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeNotEqualTo(String value) {
            addCriterion("RejCode <>", value, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeGreaterThan(String value) {
            addCriterion("RejCode >", value, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RejCode >=", value, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeLessThan(String value) {
            addCriterion("RejCode <", value, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeLessThanOrEqualTo(String value) {
            addCriterion("RejCode <=", value, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeLike(String value) {
            addCriterion("RejCode like", value, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeNotLike(String value) {
            addCriterion("RejCode not like", value, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeIn(List<String> values) {
            addCriterion("RejCode in", values, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeNotIn(List<String> values) {
            addCriterion("RejCode not in", values, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeBetween(String value1, String value2) {
            addCriterion("RejCode between", value1, value2, "rejcode");
            return (Criteria) this;
        }

        public Criteria andRejcodeNotBetween(String value1, String value2) {
            addCriterion("RejCode not between", value1, value2, "rejcode");
            return (Criteria) this;
        }

        public Criteria andIssnumberIsNull() {
            addCriterion("IssNumber is null");
            return (Criteria) this;
        }

        public Criteria andIssnumberIsNotNull() {
            addCriterion("IssNumber is not null");
            return (Criteria) this;
        }

        public Criteria andIssnumberEqualTo(String value) {
            addCriterion("IssNumber =", value, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberNotEqualTo(String value) {
            addCriterion("IssNumber <>", value, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberGreaterThan(String value) {
            addCriterion("IssNumber >", value, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberGreaterThanOrEqualTo(String value) {
            addCriterion("IssNumber >=", value, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberLessThan(String value) {
            addCriterion("IssNumber <", value, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberLessThanOrEqualTo(String value) {
            addCriterion("IssNumber <=", value, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberLike(String value) {
            addCriterion("IssNumber like", value, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberNotLike(String value) {
            addCriterion("IssNumber not like", value, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberIn(List<String> values) {
            addCriterion("IssNumber in", values, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberNotIn(List<String> values) {
            addCriterion("IssNumber not in", values, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberBetween(String value1, String value2) {
            addCriterion("IssNumber between", value1, value2, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnumberNotBetween(String value1, String value2) {
            addCriterion("IssNumber not between", value1, value2, "issnumber");
            return (Criteria) this;
        }

        public Criteria andIssnameIsNull() {
            addCriterion("IssName is null");
            return (Criteria) this;
        }

        public Criteria andIssnameIsNotNull() {
            addCriterion("IssName is not null");
            return (Criteria) this;
        }

        public Criteria andIssnameEqualTo(String value) {
            addCriterion("IssName =", value, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameNotEqualTo(String value) {
            addCriterion("IssName <>", value, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameGreaterThan(String value) {
            addCriterion("IssName >", value, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameGreaterThanOrEqualTo(String value) {
            addCriterion("IssName >=", value, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameLessThan(String value) {
            addCriterion("IssName <", value, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameLessThanOrEqualTo(String value) {
            addCriterion("IssName <=", value, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameLike(String value) {
            addCriterion("IssName like", value, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameNotLike(String value) {
            addCriterion("IssName not like", value, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameIn(List<String> values) {
            addCriterion("IssName in", values, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameNotIn(List<String> values) {
            addCriterion("IssName not in", values, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameBetween(String value1, String value2) {
            addCriterion("IssName between", value1, value2, "issname");
            return (Criteria) this;
        }

        public Criteria andIssnameNotBetween(String value1, String value2) {
            addCriterion("IssName not between", value1, value2, "issname");
            return (Criteria) this;
        }

        public Criteria andTransdateIsNull() {
            addCriterion("TransDate is null");
            return (Criteria) this;
        }

        public Criteria andTransdateIsNotNull() {
            addCriterion("TransDate is not null");
            return (Criteria) this;
        }

        public Criteria andTransdateEqualTo(String value) {
            addCriterion("TransDate =", value, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateNotEqualTo(String value) {
            addCriterion("TransDate <>", value, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateGreaterThan(String value) {
            addCriterion("TransDate >", value, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateGreaterThanOrEqualTo(String value) {
            addCriterion("TransDate >=", value, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateLessThan(String value) {
            addCriterion("TransDate <", value, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateLessThanOrEqualTo(String value) {
            addCriterion("TransDate <=", value, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateLike(String value) {
            addCriterion("TransDate like", value, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateNotLike(String value) {
            addCriterion("TransDate not like", value, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateIn(List<String> values) {
            addCriterion("TransDate in", values, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateNotIn(List<String> values) {
            addCriterion("TransDate not in", values, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateBetween(String value1, String value2) {
            addCriterion("TransDate between", value1, value2, "transdate");
            return (Criteria) this;
        }

        public Criteria andTransdateNotBetween(String value1, String value2) {
            addCriterion("TransDate not between", value1, value2, "transdate");
            return (Criteria) this;
        }

        public Criteria andTranstimeIsNull() {
            addCriterion("TransTime is null");
            return (Criteria) this;
        }

        public Criteria andTranstimeIsNotNull() {
            addCriterion("TransTime is not null");
            return (Criteria) this;
        }

        public Criteria andTranstimeEqualTo(String value) {
            addCriterion("TransTime =", value, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeNotEqualTo(String value) {
            addCriterion("TransTime <>", value, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeGreaterThan(String value) {
            addCriterion("TransTime >", value, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeGreaterThanOrEqualTo(String value) {
            addCriterion("TransTime >=", value, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeLessThan(String value) {
            addCriterion("TransTime <", value, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeLessThanOrEqualTo(String value) {
            addCriterion("TransTime <=", value, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeLike(String value) {
            addCriterion("TransTime like", value, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeNotLike(String value) {
            addCriterion("TransTime not like", value, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeIn(List<String> values) {
            addCriterion("TransTime in", values, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeNotIn(List<String> values) {
            addCriterion("TransTime not in", values, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeBetween(String value1, String value2) {
            addCriterion("TransTime between", value1, value2, "transtime");
            return (Criteria) this;
        }

        public Criteria andTranstimeNotBetween(String value1, String value2) {
            addCriterion("TransTime not between", value1, value2, "transtime");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainIsNull() {
            addCriterion("RejCodeExplain is null");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainIsNotNull() {
            addCriterion("RejCodeExplain is not null");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainEqualTo(String value) {
            addCriterion("RejCodeExplain =", value, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainNotEqualTo(String value) {
            addCriterion("RejCodeExplain <>", value, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainGreaterThan(String value) {
            addCriterion("RejCodeExplain >", value, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainGreaterThanOrEqualTo(String value) {
            addCriterion("RejCodeExplain >=", value, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainLessThan(String value) {
            addCriterion("RejCodeExplain <", value, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainLessThanOrEqualTo(String value) {
            addCriterion("RejCodeExplain <=", value, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainLike(String value) {
            addCriterion("RejCodeExplain like", value, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainNotLike(String value) {
            addCriterion("RejCodeExplain not like", value, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainIn(List<String> values) {
            addCriterion("RejCodeExplain in", values, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainNotIn(List<String> values) {
            addCriterion("RejCodeExplain not in", values, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainBetween(String value1, String value2) {
            addCriterion("RejCodeExplain between", value1, value2, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andRejcodeexplainNotBetween(String value1, String value2) {
            addCriterion("RejCodeExplain not between", value1, value2, "rejcodeexplain");
            return (Criteria) this;
        }

        public Criteria andCardbackIsNull() {
            addCriterion("CardBack is null");
            return (Criteria) this;
        }

        public Criteria andCardbackIsNotNull() {
            addCriterion("CardBack is not null");
            return (Criteria) this;
        }

        public Criteria andCardbackEqualTo(String value) {
            addCriterion("CardBack =", value, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackNotEqualTo(String value) {
            addCriterion("CardBack <>", value, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackGreaterThan(String value) {
            addCriterion("CardBack >", value, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackGreaterThanOrEqualTo(String value) {
            addCriterion("CardBack >=", value, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackLessThan(String value) {
            addCriterion("CardBack <", value, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackLessThanOrEqualTo(String value) {
            addCriterion("CardBack <=", value, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackLike(String value) {
            addCriterion("CardBack like", value, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackNotLike(String value) {
            addCriterion("CardBack not like", value, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackIn(List<String> values) {
            addCriterion("CardBack in", values, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackNotIn(List<String> values) {
            addCriterion("CardBack not in", values, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackBetween(String value1, String value2) {
            addCriterion("CardBack between", value1, value2, "cardback");
            return (Criteria) this;
        }

        public Criteria andCardbackNotBetween(String value1, String value2) {
            addCriterion("CardBack not between", value1, value2, "cardback");
            return (Criteria) this;
        }

        public Criteria andMemoIsNull() {
            addCriterion("Memo is null");
            return (Criteria) this;
        }

        public Criteria andMemoIsNotNull() {
            addCriterion("Memo is not null");
            return (Criteria) this;
        }

        public Criteria andMemoEqualTo(String value) {
            addCriterion("Memo =", value, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoNotEqualTo(String value) {
            addCriterion("Memo <>", value, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoGreaterThan(String value) {
            addCriterion("Memo >", value, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoGreaterThanOrEqualTo(String value) {
            addCriterion("Memo >=", value, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoLessThan(String value) {
            addCriterion("Memo <", value, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoLessThanOrEqualTo(String value) {
            addCriterion("Memo <=", value, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoLike(String value) {
            addCriterion("Memo like", value, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoNotLike(String value) {
            addCriterion("Memo not like", value, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoIn(List<String> values) {
            addCriterion("Memo in", values, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoNotIn(List<String> values) {
            addCriterion("Memo not in", values, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoBetween(String value1, String value2) {
            addCriterion("Memo between", value1, value2, "memo");
            return (Criteria) this;
        }

        public Criteria andMemoNotBetween(String value1, String value2) {
            addCriterion("Memo not between", value1, value2, "memo");
            return (Criteria) this;
        }

        public Criteria andTranscheckIsNull() {
            addCriterion("TransCheck is null");
            return (Criteria) this;
        }

        public Criteria andTranscheckIsNotNull() {
            addCriterion("TransCheck is not null");
            return (Criteria) this;
        }

        public Criteria andTranscheckEqualTo(String value) {
            addCriterion("TransCheck =", value, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckNotEqualTo(String value) {
            addCriterion("TransCheck <>", value, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckGreaterThan(String value) {
            addCriterion("TransCheck >", value, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckGreaterThanOrEqualTo(String value) {
            addCriterion("TransCheck >=", value, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckLessThan(String value) {
            addCriterion("TransCheck <", value, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckLessThanOrEqualTo(String value) {
            addCriterion("TransCheck <=", value, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckLike(String value) {
            addCriterion("TransCheck like", value, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckNotLike(String value) {
            addCriterion("TransCheck not like", value, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckIn(List<String> values) {
            addCriterion("TransCheck in", values, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckNotIn(List<String> values) {
            addCriterion("TransCheck not in", values, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckBetween(String value1, String value2) {
            addCriterion("TransCheck between", value1, value2, "transcheck");
            return (Criteria) this;
        }

        public Criteria andTranscheckNotBetween(String value1, String value2) {
            addCriterion("TransCheck not between", value1, value2, "transcheck");
            return (Criteria) this;
        }

        public Criteria andIsofflineIsNull() {
            addCriterion("IsOffline is null");
            return (Criteria) this;
        }

        public Criteria andIsofflineIsNotNull() {
            addCriterion("IsOffline is not null");
            return (Criteria) this;
        }

        public Criteria andIsofflineEqualTo(String value) {
            addCriterion("IsOffline =", value, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineNotEqualTo(String value) {
            addCriterion("IsOffline <>", value, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineGreaterThan(String value) {
            addCriterion("IsOffline >", value, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineGreaterThanOrEqualTo(String value) {
            addCriterion("IsOffline >=", value, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineLessThan(String value) {
            addCriterion("IsOffline <", value, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineLessThanOrEqualTo(String value) {
            addCriterion("IsOffline <=", value, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineLike(String value) {
            addCriterion("IsOffline like", value, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineNotLike(String value) {
            addCriterion("IsOffline not like", value, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineIn(List<String> values) {
            addCriterion("IsOffline in", values, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineNotIn(List<String> values) {
            addCriterion("IsOffline not in", values, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineBetween(String value1, String value2) {
            addCriterion("IsOffline between", value1, value2, "isoffline");
            return (Criteria) this;
        }

        public Criteria andIsofflineNotBetween(String value1, String value2) {
            addCriterion("IsOffline not between", value1, value2, "isoffline");
            return (Criteria) this;
        }

        public Criteria andCupsIsNull() {
            addCriterion("CUPS is null");
            return (Criteria) this;
        }

        public Criteria andCupsIsNotNull() {
            addCriterion("CUPS is not null");
            return (Criteria) this;
        }

        public Criteria andCupsEqualTo(String value) {
            addCriterion("CUPS =", value, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsNotEqualTo(String value) {
            addCriterion("CUPS <>", value, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsGreaterThan(String value) {
            addCriterion("CUPS >", value, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsGreaterThanOrEqualTo(String value) {
            addCriterion("CUPS >=", value, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsLessThan(String value) {
            addCriterion("CUPS <", value, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsLessThanOrEqualTo(String value) {
            addCriterion("CUPS <=", value, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsLike(String value) {
            addCriterion("CUPS like", value, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsNotLike(String value) {
            addCriterion("CUPS not like", value, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsIn(List<String> values) {
            addCriterion("CUPS in", values, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsNotIn(List<String> values) {
            addCriterion("CUPS not in", values, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsBetween(String value1, String value2) {
            addCriterion("CUPS between", value1, value2, "cups");
            return (Criteria) this;
        }

        public Criteria andCupsNotBetween(String value1, String value2) {
            addCriterion("CUPS not between", value1, value2, "cups");
            return (Criteria) this;
        }

        public Criteria andTransidIsNull() {
            addCriterion("TransId is null");
            return (Criteria) this;
        }

        public Criteria andTransidIsNotNull() {
            addCriterion("TransId is not null");
            return (Criteria) this;
        }

        public Criteria andTransidEqualTo(String value) {
            addCriterion("TransId =", value, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidNotEqualTo(String value) {
            addCriterion("TransId <>", value, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidGreaterThan(String value) {
            addCriterion("TransId >", value, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidGreaterThanOrEqualTo(String value) {
            addCriterion("TransId >=", value, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidLessThan(String value) {
            addCriterion("TransId <", value, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidLessThanOrEqualTo(String value) {
            addCriterion("TransId <=", value, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidLike(String value) {
            addCriterion("TransId like", value, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidNotLike(String value) {
            addCriterion("TransId not like", value, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidIn(List<String> values) {
            addCriterion("TransId in", values, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidNotIn(List<String> values) {
            addCriterion("TransId not in", values, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidBetween(String value1, String value2) {
            addCriterion("TransId between", value1, value2, "transid");
            return (Criteria) this;
        }

        public Criteria andTransidNotBetween(String value1, String value2) {
            addCriterion("TransId not between", value1, value2, "transid");
            return (Criteria) this;
        }

        public Criteria andPrintcontentIsNull() {
            addCriterion("PrintContent is null");
            return (Criteria) this;
        }

        public Criteria andPrintcontentIsNotNull() {
            addCriterion("PrintContent is not null");
            return (Criteria) this;
        }

        public Criteria andPrintcontentEqualTo(String value) {
            addCriterion("PrintContent =", value, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentNotEqualTo(String value) {
            addCriterion("PrintContent <>", value, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentGreaterThan(String value) {
            addCriterion("PrintContent >", value, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentGreaterThanOrEqualTo(String value) {
            addCriterion("PrintContent >=", value, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentLessThan(String value) {
            addCriterion("PrintContent <", value, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentLessThanOrEqualTo(String value) {
            addCriterion("PrintContent <=", value, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentLike(String value) {
            addCriterion("PrintContent like", value, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentNotLike(String value) {
            addCriterion("PrintContent not like", value, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentIn(List<String> values) {
            addCriterion("PrintContent in", values, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentNotIn(List<String> values) {
            addCriterion("PrintContent not in", values, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentBetween(String value1, String value2) {
            addCriterion("PrintContent between", value1, value2, "printcontent");
            return (Criteria) this;
        }

        public Criteria andPrintcontentNotBetween(String value1, String value2) {
            addCriterion("PrintContent not between", value1, value2, "printcontent");
            return (Criteria) this;
        }

        public Criteria andDiscountamtIsNull() {
            addCriterion("DiscountAmt is null");
            return (Criteria) this;
        }

        public Criteria andDiscountamtIsNotNull() {
            addCriterion("DiscountAmt is not null");
            return (Criteria) this;
        }

        public Criteria andDiscountamtEqualTo(String value) {
            addCriterion("DiscountAmt =", value, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtNotEqualTo(String value) {
            addCriterion("DiscountAmt <>", value, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtGreaterThan(String value) {
            addCriterion("DiscountAmt >", value, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtGreaterThanOrEqualTo(String value) {
            addCriterion("DiscountAmt >=", value, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtLessThan(String value) {
            addCriterion("DiscountAmt <", value, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtLessThanOrEqualTo(String value) {
            addCriterion("DiscountAmt <=", value, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtLike(String value) {
            addCriterion("DiscountAmt like", value, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtNotLike(String value) {
            addCriterion("DiscountAmt not like", value, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtIn(List<String> values) {
            addCriterion("DiscountAmt in", values, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtNotIn(List<String> values) {
            addCriterion("DiscountAmt not in", values, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtBetween(String value1, String value2) {
            addCriterion("DiscountAmt between", value1, value2, "discountamt");
            return (Criteria) this;
        }

        public Criteria andDiscountamtNotBetween(String value1, String value2) {
            addCriterion("DiscountAmt not between", value1, value2, "discountamt");
            return (Criteria) this;
        }

        public Criteria andActualpaymentIsNull() {
            addCriterion("ActualPayment is null");
            return (Criteria) this;
        }

        public Criteria andActualpaymentIsNotNull() {
            addCriterion("ActualPayment is not null");
            return (Criteria) this;
        }

        public Criteria andActualpaymentEqualTo(String value) {
            addCriterion("ActualPayment =", value, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentNotEqualTo(String value) {
            addCriterion("ActualPayment <>", value, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentGreaterThan(String value) {
            addCriterion("ActualPayment >", value, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentGreaterThanOrEqualTo(String value) {
            addCriterion("ActualPayment >=", value, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentLessThan(String value) {
            addCriterion("ActualPayment <", value, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentLessThanOrEqualTo(String value) {
            addCriterion("ActualPayment <=", value, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentLike(String value) {
            addCriterion("ActualPayment like", value, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentNotLike(String value) {
            addCriterion("ActualPayment not like", value, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentIn(List<String> values) {
            addCriterion("ActualPayment in", values, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentNotIn(List<String> values) {
            addCriterion("ActualPayment not in", values, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentBetween(String value1, String value2) {
            addCriterion("ActualPayment between", value1, value2, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andActualpaymentNotBetween(String value1, String value2) {
            addCriterion("ActualPayment not between", value1, value2, "actualpayment");
            return (Criteria) this;
        }

        public Criteria andOrdernumberIsNull() {
            addCriterion("OrderNumber is null");
            return (Criteria) this;
        }

        public Criteria andOrdernumberIsNotNull() {
            addCriterion("OrderNumber is not null");
            return (Criteria) this;
        }

        public Criteria andOrdernumberEqualTo(String value) {
            addCriterion("OrderNumber =", value, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberNotEqualTo(String value) {
            addCriterion("OrderNumber <>", value, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberGreaterThan(String value) {
            addCriterion("OrderNumber >", value, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberGreaterThanOrEqualTo(String value) {
            addCriterion("OrderNumber >=", value, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberLessThan(String value) {
            addCriterion("OrderNumber <", value, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberLessThanOrEqualTo(String value) {
            addCriterion("OrderNumber <=", value, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberLike(String value) {
            addCriterion("OrderNumber like", value, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberNotLike(String value) {
            addCriterion("OrderNumber not like", value, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberIn(List<String> values) {
            addCriterion("OrderNumber in", values, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberNotIn(List<String> values) {
            addCriterion("OrderNumber not in", values, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberBetween(String value1, String value2) {
            addCriterion("OrderNumber between", value1, value2, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andOrdernumberNotBetween(String value1, String value2) {
            addCriterion("OrderNumber not between", value1, value2, "ordernumber");
            return (Criteria) this;
        }

        public Criteria andSubacctIsNull() {
            addCriterion("SubAcct is null");
            return (Criteria) this;
        }

        public Criteria andSubacctIsNotNull() {
            addCriterion("SubAcct is not null");
            return (Criteria) this;
        }

        public Criteria andSubacctEqualTo(String value) {
            addCriterion("SubAcct =", value, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctNotEqualTo(String value) {
            addCriterion("SubAcct <>", value, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctGreaterThan(String value) {
            addCriterion("SubAcct >", value, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctGreaterThanOrEqualTo(String value) {
            addCriterion("SubAcct >=", value, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctLessThan(String value) {
            addCriterion("SubAcct <", value, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctLessThanOrEqualTo(String value) {
            addCriterion("SubAcct <=", value, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctLike(String value) {
            addCriterion("SubAcct like", value, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctNotLike(String value) {
            addCriterion("SubAcct not like", value, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctIn(List<String> values) {
            addCriterion("SubAcct in", values, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctNotIn(List<String> values) {
            addCriterion("SubAcct not in", values, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctBetween(String value1, String value2) {
            addCriterion("SubAcct between", value1, value2, "subacct");
            return (Criteria) this;
        }

        public Criteria andSubacctNotBetween(String value1, String value2) {
            addCriterion("SubAcct not between", value1, value2, "subacct");
            return (Criteria) this;
        }

        public Criteria andTrxidIsNull() {
            addCriterion("TrxId is null");
            return (Criteria) this;
        }

        public Criteria andTrxidIsNotNull() {
            addCriterion("TrxId is not null");
            return (Criteria) this;
        }

        public Criteria andTrxidEqualTo(String value) {
            addCriterion("TrxId =", value, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidNotEqualTo(String value) {
            addCriterion("TrxId <>", value, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidGreaterThan(String value) {
            addCriterion("TrxId >", value, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidGreaterThanOrEqualTo(String value) {
            addCriterion("TrxId >=", value, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidLessThan(String value) {
            addCriterion("TrxId <", value, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidLessThanOrEqualTo(String value) {
            addCriterion("TrxId <=", value, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidLike(String value) {
            addCriterion("TrxId like", value, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidNotLike(String value) {
            addCriterion("TrxId not like", value, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidIn(List<String> values) {
            addCriterion("TrxId in", values, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidNotIn(List<String> values) {
            addCriterion("TrxId not in", values, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidBetween(String value1, String value2) {
            addCriterion("TrxId between", value1, value2, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxidNotBetween(String value1, String value2) {
            addCriterion("TrxId not between", value1, value2, "trxid");
            return (Criteria) this;
        }

        public Criteria andTrxstatusIsNull() {
            addCriterion("TrxStatus is null");
            return (Criteria) this;
        }

        public Criteria andTrxstatusIsNotNull() {
            addCriterion("TrxStatus is not null");
            return (Criteria) this;
        }

        public Criteria andTrxstatusEqualTo(String value) {
            addCriterion("TrxStatus =", value, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusNotEqualTo(String value) {
            addCriterion("TrxStatus <>", value, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusGreaterThan(String value) {
            addCriterion("TrxStatus >", value, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusGreaterThanOrEqualTo(String value) {
            addCriterion("TrxStatus >=", value, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusLessThan(String value) {
            addCriterion("TrxStatus <", value, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusLessThanOrEqualTo(String value) {
            addCriterion("TrxStatus <=", value, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusLike(String value) {
            addCriterion("TrxStatus like", value, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusNotLike(String value) {
            addCriterion("TrxStatus not like", value, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusIn(List<String> values) {
            addCriterion("TrxStatus in", values, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusNotIn(List<String> values) {
            addCriterion("TrxStatus not in", values, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusBetween(String value1, String value2) {
            addCriterion("TrxStatus between", value1, value2, "trxstatus");
            return (Criteria) this;
        }

        public Criteria andTrxstatusNotBetween(String value1, String value2) {
            addCriterion("TrxStatus not between", value1, value2, "trxstatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}