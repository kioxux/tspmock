package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * @author wu mao yin
 * @description: 连锁公司
 * @date 2021/12/3 15:08
 */
@Data
@Table(name = "GAIA_COMPADM")
public class GaiaCompAdm implements Serializable {
    /**
     * 加盟商ID
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 连锁总部ID
     */
    @Id
    @Column(name = "COMPADM_ID")
    private String compadmId;

    /**
     * 连锁总部名称
     */
    @Column(name = "COMPADM_NAME")
    private String compadmName;

    /**
     * 统一社会信用代码
     */
    @Column(name = "COMPADM_NO")
    private String compadmNo;

    /**
     * 法人/负责人
     */
    @Column(name = "COMPADM_LEGAL_PERSON")
    private String compadmLegalPerson;

    /**
     * 质量负责人
     */
    @Column(name = "COMPADM_QUA")
    private String compadmQua;

    /**
     * 详细地址
     */
    @Column(name = "COMPADM_ADDR")
    private String compadmAddr;

    /**
     * 创建日期
     */
    @Column(name = "COMPADM_CRE_DATE")
    private String compadmCreDate;

    /**
     * 创建时间
     */
    @Column(name = "COMPADM_CRE_TIME")
    private String compadmCreTime;

    /**
     * 创建人账号
     */
    @Column(name = "COMPADM_CRE_ID")
    private String compadmCreId;

    /**
     * 修改日期
     */
    @Column(name = "COMPADM_MODI_DATE")
    private String compadmModiDate;

    /**
     * 修改时间
     */
    @Column(name = "COMPADM_MODI_TIME")
    private String compadmModiTime;

    /**
     * 修改人账号
     */
    @Column(name = "COMPADM_MODI_ID")
    private String compadmModiId;

    /**
     * LOGO地址
     */
    @Column(name = "COMPADM_LOGO")
    private String compadmLogo;

    /**
     * 连锁总部状态 0停用 1启用
     */
    @Column(name = "COMPADM_STATUS")
    private String compadmStatus;

    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;

    private static final long serialVersionUID = 1L;

}