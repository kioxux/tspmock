package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdAppSaleRecord;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdAppSaleRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdAppSaleRecord record);

    int insertSelective(GaiaSdAppSaleRecord record);

    GaiaSdAppSaleRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdAppSaleRecord record);

    int updateByPrimaryKey(GaiaSdAppSaleRecord record);

    int updateBatch(List<GaiaSdAppSaleRecord> list);

    int batchInsert(@Param("list") List<GaiaSdAppSaleRecord> list);

    void deleteDataByYear();

    void deleteDataByYearAndMonth(@Param("years") String years);
}