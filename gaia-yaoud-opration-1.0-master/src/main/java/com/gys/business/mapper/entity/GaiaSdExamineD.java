//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(
        name = "GAIA_SD_EXAMINE_D"
)
@Data
public class GaiaSdExamineD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSED_VOUCHER_ID"
    )
    private String gsedVoucherId;
    @Column(
            name = "GSED_BR_ID"
    )
    private String gsedBrId;
    @Column(
            name = "GSED_DATE"
    )
    private String gsedDate;
    @Id
    @Column(
            name = "GSED_SERIAL"
    )
    private String gsedSerial;
    @Column(
            name = "GSED_PRO_ID"
    )
    private String gsedProId;
    @Column(
            name = "GSED_BATCH_NO"
    )
//    private String gsedMadeDate;
//    @Column(
//            name = "GSAD_MADE_DATE"
//    )
    private String gsedBatchNo;
    @Column(
            name = "GSED_BATCH"
    )
    private String gsedBatch;
    @Column(
            name = "GSED_VALID_DATE"
    )
    private String gsedValidDate;
    @Column(
            name = "GSED_RECIPIENT_QTY"
    )
    private String gsedRecipientQty;
    @Column(
            name = "GSED_QUALIFIED_QTY"
    )
    private String gsedQualifiedQty;
    @Column(
            name = "GSED_UNQUALIFIED_QTY"
    )
    private String gsedUnqualifiedQty;
    @Column(
            name = "GSED_UNQUALIFIED_CAUSE"
    )
    private String gsedUnqualifiedCause;
    @Column(
            name = "GSED_EMP"
    )
    private String gsedEmp;
    @Column(
            name = "GSED_RESULT"
    )
    private String gsedResult;
    @Column(
            name = "GSED_STOCK_STATUS"
    )
    private String gsedStockStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdExamineD() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsedVoucherId() {
        return this.gsedVoucherId;
    }

    public void setGsedVoucherId(String gsedVoucherId) {
        this.gsedVoucherId = gsedVoucherId;
    }

    public String getGsedDate() {
        return this.gsedDate;
    }

    public void setGsedDate(String gsedDate) {
        this.gsedDate = gsedDate;
    }

    public String getGsedSerial() {
        return this.gsedSerial;
    }

    public void setGsedSerial(String gsedSerial) {
        this.gsedSerial = gsedSerial;
    }

    public String getGsedProId() {
        return this.gsedProId;
    }

    public void setGsedProId(String gsedProId) {
        this.gsedProId = gsedProId;
    }

    public String getGsedBatchNo() {
        return this.gsedBatchNo;
    }

    public void setGsedBatchNo(String gsedBatchNo) {
        this.gsedBatchNo = gsedBatchNo;
    }

    public String getGsedBatch() {
        return this.gsedBatch;
    }

    public void setGsedBatch(String gsedBatch) {
        this.gsedBatch = gsedBatch;
    }

    public String getGsedValidDate() {
        return this.gsedValidDate;
    }

    public void setGsedValidDate(String gsedValidDate) {
        this.gsedValidDate = gsedValidDate;
    }

    public String getGsedRecipientQty() {
        return this.gsedRecipientQty;
    }

    public void setGsedRecipientQty(String gsedRecipientQty) {
        this.gsedRecipientQty = gsedRecipientQty;
    }

    public String getGsedQualifiedQty() {
        return this.gsedQualifiedQty;
    }

    public void setGsedQualifiedQty(String gsedQualifiedQty) {
        this.gsedQualifiedQty = gsedQualifiedQty;
    }

    public String getGsedUnqualifiedQty() {
        return this.gsedUnqualifiedQty;
    }

    public void setGsedUnqualifiedQty(String gsedUnqualifiedQty) {
        this.gsedUnqualifiedQty = gsedUnqualifiedQty;
    }

    public String getGsedUnqualifiedCause() {
        return this.gsedUnqualifiedCause;
    }

    public void setGsedUnqualifiedCause(String gsedUnqualifiedCause) {
        this.gsedUnqualifiedCause = gsedUnqualifiedCause;
    }

    public String getGsedEmp() {
        return this.gsedEmp;
    }

    public void setGsedEmp(String gsedEmp) {
        this.gsedEmp = gsedEmp;
    }

    public String getGsedResult() {
        return this.gsedResult;
    }

    public void setGsedResult(String gsedResult) {
        this.gsedResult = gsedResult;
    }

    public String getGsedStockStatus() {
        return this.gsedStockStatus;
    }

    public void setGsedStockStatus(String gsedStockStatus) {
        this.gsedStockStatus = gsedStockStatus;
    }

//    public String getGsedMadeDate() {
//        return gsedMadeDate;
//    }
//
//    public void setGsedMadeDate(String gsedMadeDate) {
//        this.gsedMadeDate = gsedMadeDate;
//    }
}
