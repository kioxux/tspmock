package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "GAIA_SD_ISH_FEE_DETAIL")
public class GaiaSdIshFeeDetail implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     *
     门店号
     */
    @Id
    @Column(name = "ISH_BR_ID")
    private String ishBrId;

    /**
     *
     销售日期
     */
    @Id
    @Column(name = "ISH_DATE")
    private Date ishDate;

    /**
     *
     销售单号
     */
    @Id
    @Column(name = "ISH_BILL_NO")
    private String ishBillNo;

    /**
     *
     交易流水号
     */
    @Id
    @Column(name = "ISH_DEAL_SERIAL")
    private String ishDealSerial;
    /**
     *
     序号
     */
    @Id
    @Column(name = "ISH_SERIAL")
    private Long ishSerial;
    /**
     * 
	销售时间
     */
    @Column(name = "ISH_TIME")
    private String ishTime;

    /**
     * 
	就诊流水号（住院(或门诊)号）
     */
    @Column(name = "ISH_JZBH")
    private String ishJzbh;

    /**
     * 
	商品编码
     */
    @Column(name = "ISH_PRO_ID")
    private String ishProId;

    /**
     * 
	医保商品编码
     */
    @Column(name = "ISH_SFXMBM")
    private String ishSfxmbm;

    /**
     * 
	自付比例
     */
    @Column(name = "ISH_ZF_PERCENT")
    private BigDecimal ishZfPercent;

    /**
     * 
	项目单价
     */
    @Column(name = "ISH_PRO_PRICE")
    private BigDecimal ishProPrice;

    /**
     * 
	数量
     */
    @Column(name = "ISH_QTY")
    private BigDecimal ishQty;

    /**
     * 
	项目费用总额
     */
    @Column(name = "ISH_FS_AMT")
    private BigDecimal ishFsAmt;

    /**
     * 
	标准单价
     */
    @Column(name = "ISH_SB_AMT")
    private BigDecimal ishSbAmt;

    /**
     * 
	自付金额
     */
    @Column(name = "ISH_QZF_AMT")
    private BigDecimal ishQzfAmt;

    /**
     * 
	自费金额
     */
    @Column(name = "ISH_ZF_AMT")
    private BigDecimal ishZfAmt;

    /**
     * 
	审批标记|
     */
    @Column(name = "ISH_SSF_FLAG")
    private String ishSsfFlag;

    /**
     * 
	审批规则
     */
    @Column(name = "ISH_LJZF_FLAG")
    private String ishLjzfFlag;

    /**
     * 
	项目等级
     */
    @Column(name = "ISH_TJDM_FLAG")
    private String ishTjdmFlag;

    @Column(name = "ISH_ZFYP_FLAG")
    private String ishZfypFlag;

    @Column(name = "ISH_QZF_FLAG")
    private String ishQzfFlag;

    @Column(name = "ISH_YBSP_CLASS")
    private String ishYbspClass;

    @Column(name = "ISH_OTC_FLAG")
    private String ishOtcFlag;

    @Column(name = "ISH_PERSONAL_CLASS")
    private String ishPersonalClass;

    @Column(name = "ISH_YPPJH")
    private String ishYppjh;

    @Column(name = "ISH_JX")
    private String ishJx;

    @Column(name = "ISH_USING_METHOD")
    private String ishUsingMethod;

    @Column(name = "ISH_USING_QTY")
    private String ishUsingQty;

    @Column(name = "ISH_YYTS")
    private Integer ishYyts;

    @Column(name = "ISH_YBPJ_FLAG")
    private String ishYbpjFlag;

    @Column(name = "ISH_YPPJH_FPDM")
    private String ishYppjhFpdm;

    private static final long serialVersionUID = 1L;
    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取
     门店号
     *
     * @return ISH_BR_ID -
    门店号
     */
    public String getIshBrId() {
        return ishBrId;
    }

    /**
     * 设置
     门店号
     *
     * @param ishBrId
    门店号
     */
    public void setIshBrId(String ishBrId) {
        this.ishBrId = ishBrId;
    }

    /**
     * 获取
     销售日期
     *
     * @return ISH_DATE -
    销售日期
     */
    public Date getIshDate() {
        return ishDate;
    }

    /**
     * 设置
     销售日期
     *
     * @param ishDate
    销售日期
     */
    public void setIshDate(Date ishDate) {
        this.ishDate = ishDate;
    }

    /**
     * 获取
     销售单号
     *
     * @return ISH_BILL_NO -
    销售单号
     */
    public String getIshBillNo() {
        return ishBillNo;
    }

    /**
     * 设置
     销售单号
     *
     * @param ishBillNo
    销售单号
     */
    public void setIshBillNo(String ishBillNo) {
        this.ishBillNo = ishBillNo;
    }

    /**
     * 获取
     交易流水号
     *
     * @return ISH_DEAL_SERIAL -
    交易流水号
     */
    public String getIshDealSerial() {
        return ishDealSerial;
    }

    /**
     * 设置
     交易流水号
     *
     * @param ishDealSerial
    交易流水号
     */
    public void setIshDealSerial(String ishDealSerial) {
        this.ishDealSerial = ishDealSerial;
    }

    /**
     * 获取
     序号
     *
     * @return ISH_SERIAL -
    序号
     */
    public Long getIshSerial() {
        return ishSerial;
    }

    /**
     * 设置
     序号
     *
     * @param ishSerial
    序号
     */
    public void setIshSerial(Long ishSerial) {
        this.ishSerial = ishSerial;
    }
    /**
     * 获取
	销售时间
     *
     * @return ISH_TIME - 
	销售时间
     */
    public String getIshTime() {
        return ishTime;
    }

    /**
     * 设置
	销售时间
     *
     * @param ishTime 
	销售时间
     */
    public void setIshTime(String ishTime) {
        this.ishTime = ishTime;
    }

    /**
     * 获取
	就诊流水号（住院(或门诊)号）
     *
     * @return ISH_JZBH - 
	就诊流水号（住院(或门诊)号）
     */
    public String getIshJzbh() {
        return ishJzbh;
    }

    /**
     * 设置
	就诊流水号（住院(或门诊)号）
     *
     * @param ishJzbh 
	就诊流水号（住院(或门诊)号）
     */
    public void setIshJzbh(String ishJzbh) {
        this.ishJzbh = ishJzbh;
    }

    /**
     * 获取
	商品编码
     *
     * @return ISH_PRO_ID - 
	商品编码
     */
    public String getIshProId() {
        return ishProId;
    }

    /**
     * 设置
	商品编码
     *
     * @param ishProId 
	商品编码
     */
    public void setIshProId(String ishProId) {
        this.ishProId = ishProId;
    }

    /**
     * 获取
	医保商品编码
     *
     * @return ISH_SFXMBM - 
	医保商品编码
     */
    public String getIshSfxmbm() {
        return ishSfxmbm;
    }

    /**
     * 设置
	医保商品编码
     *
     * @param ishSfxmbm 
	医保商品编码
     */
    public void setIshSfxmbm(String ishSfxmbm) {
        this.ishSfxmbm = ishSfxmbm;
    }

    /**
     * 获取
	自付比例
     *
     * @return ISH_ZF_PERCENT - 
	自付比例
     */
    public BigDecimal getIshZfPercent() {
        return ishZfPercent;
    }

    /**
     * 设置
	自付比例
     *
     * @param ishZfPercent 
	自付比例
     */
    public void setIshZfPercent(BigDecimal ishZfPercent) {
        this.ishZfPercent = ishZfPercent;
    }

    /**
     * 获取
	项目单价
     *
     * @return ISH_PRO_PRICE - 
	项目单价
     */
    public BigDecimal getIshProPrice() {
        return ishProPrice;
    }

    /**
     * 设置
	项目单价
     *
     * @param ishProPrice 
	项目单价
     */
    public void setIshProPrice(BigDecimal ishProPrice) {
        this.ishProPrice = ishProPrice;
    }

    /**
     * 获取
	数量
     *
     * @return ISH_QTY - 
	数量
     */
    public BigDecimal getIshQty() {
        return ishQty;
    }

    /**
     * 设置
	数量
     *
     * @param ishQty 
	数量
     */
    public void setIshQty(BigDecimal ishQty) {
        this.ishQty = ishQty;
    }

    /**
     * 获取
	项目费用总额
     *
     * @return ISH_FS_AMT - 
	项目费用总额
     */
    public BigDecimal getIshFsAmt() {
        return ishFsAmt;
    }

    /**
     * 设置
	项目费用总额
     *
     * @param ishFsAmt 
	项目费用总额
     */
    public void setIshFsAmt(BigDecimal ishFsAmt) {
        this.ishFsAmt = ishFsAmt;
    }

    /**
     * 获取
	标准单价
     *
     * @return ISH_SB_AMT - 
	标准单价
     */
    public BigDecimal getIshSbAmt() {
        return ishSbAmt;
    }

    /**
     * 设置
	标准单价
     *
     * @param ishSbAmt 
	标准单价
     */
    public void setIshSbAmt(BigDecimal ishSbAmt) {
        this.ishSbAmt = ishSbAmt;
    }

    /**
     * 获取
	自付金额
     *
     * @return ISH_QZF_AMT - 
	自付金额
     */
    public BigDecimal getIshQzfAmt() {
        return ishQzfAmt;
    }

    /**
     * 设置
	自付金额
     *
     * @param ishQzfAmt 
	自付金额
     */
    public void setIshQzfAmt(BigDecimal ishQzfAmt) {
        this.ishQzfAmt = ishQzfAmt;
    }

    /**
     * 获取
	自费金额
     *
     * @return ISH_ZF_AMT - 
	自费金额
     */
    public BigDecimal getIshZfAmt() {
        return ishZfAmt;
    }

    /**
     * 设置
	自费金额
     *
     * @param ishZfAmt 
	自费金额
     */
    public void setIshZfAmt(BigDecimal ishZfAmt) {
        this.ishZfAmt = ishZfAmt;
    }

    /**
     * 获取
	审批标记|
     *
     * @return ISH_SSF_FLAG - 
	审批标记|
     */
    public String getIshSsfFlag() {
        return ishSsfFlag;
    }

    /**
     * 设置
	审批标记|
     *
     * @param ishSsfFlag 
	审批标记|
     */
    public void setIshSsfFlag(String ishSsfFlag) {
        this.ishSsfFlag = ishSsfFlag;
    }

    /**
     * 获取
	审批规则
     *
     * @return ISH_LJZF_FLAG - 
	审批规则
     */
    public String getIshLjzfFlag() {
        return ishLjzfFlag;
    }

    /**
     * 设置
	审批规则
     *
     * @param ishLjzfFlag 
	审批规则
     */
    public void setIshLjzfFlag(String ishLjzfFlag) {
        this.ishLjzfFlag = ishLjzfFlag;
    }

    /**
     * 获取
	项目等级
     *
     * @return ISH_TJDM_FLAG - 
	项目等级
     */
    public String getIshTjdmFlag() {
        return ishTjdmFlag;
    }

    /**
     * 设置
	项目等级
     *
     * @param ishTjdmFlag 
	项目等级
     */
    public void setIshTjdmFlag(String ishTjdmFlag) {
        this.ishTjdmFlag = ishTjdmFlag;
    }

    /**
     * @return ISH_ZFYP_FLAG
     */
    public String getIshZfypFlag() {
        return ishZfypFlag;
    }

    /**
     * @param ishZfypFlag
     */
    public void setIshZfypFlag(String ishZfypFlag) {
        this.ishZfypFlag = ishZfypFlag;
    }

    /**
     * @return ISH_QZF_FLAG
     */
    public String getIshQzfFlag() {
        return ishQzfFlag;
    }

    /**
     * @param ishQzfFlag
     */
    public void setIshQzfFlag(String ishQzfFlag) {
        this.ishQzfFlag = ishQzfFlag;
    }

    /**
     * @return ISH_YBSP_CLASS
     */
    public String getIshYbspClass() {
        return ishYbspClass;
    }

    /**
     * @param ishYbspClass
     */
    public void setIshYbspClass(String ishYbspClass) {
        this.ishYbspClass = ishYbspClass;
    }

    /**
     * @return ISH_OTC_FLAG
     */
    public String getIshOtcFlag() {
        return ishOtcFlag;
    }

    /**
     * @param ishOtcFlag
     */
    public void setIshOtcFlag(String ishOtcFlag) {
        this.ishOtcFlag = ishOtcFlag;
    }

    /**
     * @return ISH_PERSONAL_CLASS
     */
    public String getIshPersonalClass() {
        return ishPersonalClass;
    }

    /**
     * @param ishPersonalClass
     */
    public void setIshPersonalClass(String ishPersonalClass) {
        this.ishPersonalClass = ishPersonalClass;
    }

    /**
     * @return ISH_YPPJH
     */
    public String getIshYppjh() {
        return ishYppjh;
    }

    /**
     * @param ishYppjh
     */
    public void setIshYppjh(String ishYppjh) {
        this.ishYppjh = ishYppjh;
    }

    /**
     * @return ISH_JX
     */
    public String getIshJx() {
        return ishJx;
    }

    /**
     * @param ishJx
     */
    public void setIshJx(String ishJx) {
        this.ishJx = ishJx;
    }

    /**
     * @return ISH_USING_METHOD
     */
    public String getIshUsingMethod() {
        return ishUsingMethod;
    }

    /**
     * @param ishUsingMethod
     */
    public void setIshUsingMethod(String ishUsingMethod) {
        this.ishUsingMethod = ishUsingMethod;
    }

    /**
     * @return ISH_USING_QTY
     */
    public String getIshUsingQty() {
        return ishUsingQty;
    }

    /**
     * @param ishUsingQty
     */
    public void setIshUsingQty(String ishUsingQty) {
        this.ishUsingQty = ishUsingQty;
    }

    /**
     * @return ISH_YYTS
     */
    public Integer getIshYyts() {
        return ishYyts;
    }

    /**
     * @param ishYyts
     */
    public void setIshYyts(Integer ishYyts) {
        this.ishYyts = ishYyts;
    }

    /**
     * @return ISH_YBPJ_FLAG
     */
    public String getIshYbpjFlag() {
        return ishYbpjFlag;
    }

    /**
     * @param ishYbpjFlag
     */
    public void setIshYbpjFlag(String ishYbpjFlag) {
        this.ishYbpjFlag = ishYbpjFlag;
    }

    /**
     * @return ISH_YPPJH_FPDM
     */
    public String getIshYppjhFpdm() {
        return ishYppjhFpdm;
    }

    /**
     * @param ishYppjhFpdm
     */
    public void setIshYppjhFpdm(String ishYppjhFpdm) {
        this.ishYppjhFpdm = ishYppjhFpdm;
    }
}