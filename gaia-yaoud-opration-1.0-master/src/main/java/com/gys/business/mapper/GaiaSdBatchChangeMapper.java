package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdBatchChange;
import com.gys.business.service.data.InvoicingInData;
import com.gys.business.service.data.InvoicingOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdBatchChangeMapper extends BaseMapper<GaiaSdBatchChange> {
    /**
     * 进销存查询
     * @param inData
     * @return
     */
    List<InvoicingOutData> getInventoryList(InvoicingInData inData);

    /**
     * 批量插入
     * @param changeList
     */
    void insertLists(List<GaiaSdBatchChange> changeList);

    List<InvoicingOutData> getBrIdInventoryList(InvoicingInData inData);

    GaiaSdBatchChange getOneByVoucherId(GaiaSdBatchChange cond);
}
