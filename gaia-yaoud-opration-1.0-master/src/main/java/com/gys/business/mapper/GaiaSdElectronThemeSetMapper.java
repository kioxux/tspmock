package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdElectronThemeSet;
import com.gys.business.service.data.ElectronicAllData;
import com.gys.business.service.data.ElectronicSubjectQueryInData;
import com.gys.business.service.data.ElectronicSubjectQueryOutData;
import com.gys.business.service.data.ThemeSettingOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface GaiaSdElectronThemeSetMapper {

    int insertSelective(GaiaSdElectronThemeSet record);

    GaiaSdElectronThemeSet selectByPrimaryKey(GaiaSdElectronThemeSet key);

    int batchInsert(@Param("list") List<GaiaSdElectronThemeSet> list);

    String getGsebIdToGaiaSdElectronThemeSet(@Param("client") String client);

    List<ThemeSettingOutData> getthemeSettingListById(@Param("client") String client,@Param("gsetsFlag") String gsetsFlag);

    GaiaSdElectronThemeSet getAllByClient(@Param("client")String client,@Param("gsetsId") String gsetsId);

    void update(GaiaSdElectronThemeSet electronThemeSet);


    List<ElectronicSubjectQueryOutData> subjectQuery(ElectronicSubjectQueryInData inVo);

    int getUseCount(@Param("gsebIdSet") Set<String> gsebIdSet, @Param("client") String client, @Param("gsetsId") String gsetsId);
}