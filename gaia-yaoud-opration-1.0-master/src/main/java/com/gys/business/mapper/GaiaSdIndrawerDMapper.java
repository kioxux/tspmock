//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIndrawerD;
import com.gys.common.base.BaseMapper;

public interface GaiaSdIndrawerDMapper extends BaseMapper<GaiaSdIndrawerD> {
    void updateExa(GaiaSdIndrawerD inData);
}
