package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaRegionalPlanningModel;
import com.gys.business.service.data.RegionalOutData;
import com.gys.business.service.data.SaveRegionalInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 13:52 2021/10/14
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
public interface GaiaRegionalPlanningModelMapper extends BaseMapper<GaiaRegionalPlanningModel> {
    List<RegionalOutData> getRegionalByClientAndParentId(@Param("client") String client,@Param("parentId") Long parentId);

    void saveRegionalPlanningModel(GaiaRegionalPlanningModel threeLevel);

    GaiaRegionalPlanningModel selectByClientAndCodeExcludeId(@Param("client") String client, @Param("regionalCode") String regionalCode,@Param("id") Long id);

    GaiaRegionalPlanningModel selectByClientAndNameExcludeId(@Param("client") String client,@Param("regionalName") String regionalName,@Param("id") Long id);

    GaiaRegionalPlanningModel selectById(Long id);

    List<GaiaRegionalPlanningModel> selectByParentId(Long parentId);

    void updateSortNum(@Param("id") Long id,@Param("sortNum") Integer sortNum);

    void updateParentSort(@Param("client") String client, @Param("parentId") Long parentId, @Param("sortNum") Integer sortNum);

    void updateRegional(SaveRegionalInData inData);

    GaiaRegionalPlanningModel selectOneLevelDefaultRegionalByClient(String client);

    void removeRegional(@Param("updateUser") String updateUser,@Param("updateTime") Date updateTime, @Param("ids") List<Long> ids);

    GaiaRegionalPlanningModel selectMaxRegionalCodeByClientAndLevel(@Param("client") String client,@Param("level") Integer level);

    void updateDefault(@Param("client") String client,@Param("level") Integer level,@Param("regionalCode") String regionalCode,@Param("regionalName") String regionalName);
}
