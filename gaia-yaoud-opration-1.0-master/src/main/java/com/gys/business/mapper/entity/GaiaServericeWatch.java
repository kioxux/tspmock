package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2022/1/7 13:49
 */
@Data
@Table(name = "GAIA_SERVERICE_WATCH")
public class GaiaServericeWatch implements Serializable {
    private static final long serialVersionUID = 2518741337174308536L;
    /**
     * ID
     */
    @Id
    @Column( name = "ID")
    private Long id;
    /**
     * 加盟商
     */
    @Column( name = "CLIENT")
    private String client;
    /**
     * 药德门店编码
     */
    @Column( name = "GSW_SITE")
    private String gswSite;
    /**
     * IP地址
     */
    @Column( name = "GSW_IP")
    private String gswIp;
    /**
     * 请求日期
     */
    @Column( name = "GSW_REQUEST_DATE")
    private String gswRequestDate;
    /**
     * 请求服务/接口名
     */
    @Column( name = "GSW_NAME")
    private String gswName;
    /**
     * 请求服务/接口描述(接口注释)
     */
    @Column( name = "GSW_COMMENT")
    private String gswComment;
    /**
     * 接口耗时(毫秒)
     */
    @Column( name = "GSW_COMSUME_SECOND")
    private Integer gswComsumeSecond;
    /**
     * 监控类型(1服务，2接口)
     */
    @Column( name = "GSW_WATCH_TYPE")
    private Integer gswWatchType;
    /**
     * 备注
     */
    @Column( name = "GSW_REMARKS")
    private String gswRemarks;
    /**
     * 创建日期
     */
    @Column( name = "GSW_CREATE_DATE")
    private String gswCreateDate;
    /**
     * 删除标识(0未删除，1删除)
     */
    @Column( name = "GSW_ISDELETE")
    private Integer gswIsDelete;
}
