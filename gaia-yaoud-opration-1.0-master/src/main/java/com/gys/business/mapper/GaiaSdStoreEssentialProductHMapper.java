package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdStoreEssentialProductH;
import java.util.List;
import java.util.Map;

import com.gys.business.service.data.EssentialProductCalculationInData;
import com.gys.business.service.data.EssentialProductOrderOutData;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdStoreEssentialProductHMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdStoreEssentialProductH record);

    int insertSelective(GaiaSdStoreEssentialProductH record);

    GaiaSdStoreEssentialProductH selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdStoreEssentialProductH record);

    int updateByPrimaryKey(GaiaSdStoreEssentialProductH record);

    int updateBatch(List<GaiaSdStoreEssentialProductH> list);

    int batchInsert(@Param("list") List<GaiaSdStoreEssentialProductH> list);

    String getNextOrderId(@Param("client") String client);

    void updateStatusByOrderId(GaiaSdStoreEssentialProductH record);

    List<EssentialProductOrderOutData> getEssentialProductOrderList(EssentialProductCalculationInData inData);

    List<Map<String, Object>> getSourceDateList(@Param("client") String client);

    List<Map<String, Object>> getStoreList(EssentialProductCalculationInData inData);

    GaiaSdStoreEssentialProductH getOrderInfo(GaiaSdStoreEssentialProductH inData);

    Integer getDisableFlag(@Param("client") String client);
}