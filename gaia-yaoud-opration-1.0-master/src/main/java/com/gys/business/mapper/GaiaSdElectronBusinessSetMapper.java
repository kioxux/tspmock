package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaSdElectronBusinessSet;
import com.gys.business.service.data.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdElectronBusinessSetMapper {

    int insertSelective(GaiaSdElectronBusinessSet record);

    int batchInsert(@Param("list") List<GaiaSdElectronBusinessSet> list);

    int batchUpdate(@Param("list") List<GaiaSdElectronBusinessSet> list);

    String getElectronBusinessBySQId(@Param("client") String client);

    String getElectronBusinessByYQId(@Param("client") String client);

    List<GaiaSdElectronBusinessSet> getAll(@Param("client") String client, @Param("gsetsId") String gsetsId, @Param("gsebsType") String gsebsType);

    List<EVoucherQueryOutData> eVoucherQuery(EVoucherQueryInData inData);

    List<GaiaSdElectronBusinessSet> detailedView(ElectronicDetailedViewInData inData);

    List<GaiaSdElectronBusinessSet> getGaiaSdElectronBusinessSetByGsebsId(@Param("client") String client, @Param("brIds") List<String> brIds);

    List<GaiaSdElectronBusinessSet> determineWhetherTheTimeIsRepeated(@Param("client") String client, @Param("brIds") List<String> brIds, @Param("beginDate") String beginDate, @Param("endData") String endData, @Param("list") List<String> gsebIds, @Param("gsebsType") String gsebsType);

    List<GaiaSdElectronBusinessSet> getAllByClientAndGsetsId(@Param("client") String client, @Param("gsetsId") String gsetsId);

    List<GaiaSdElectronBusinessSet> getAllByClientAndGsetsIdAndStatus(@Param("client") String client, @Param("gsetsId") String gsetsId, @Param("status") String status);

    List<GaiaSdElectronBusinessSet> getBusinessSetByGsebsId(@Param("client") String client, @Param("gsetsId") String gsetsId);

    List<GaiaSdElectronBusinessSet> getAllByGsetsIdAndType(@Param("client") String client, @Param("gsetsId") String gsetsId, @Param("type") String type);

    List<GaiaSdElectronBusinessSet> getAllByClient(@Param("client") String client);

    List<GaiaSdElectronBusinessSet> determineWhetherTheTimeIsRepeated2(@Param("client") String client, @Param("brIds") List<String> brIds, @Param("beginDate") String beginDate, @Param("endData") String endData, @Param("gsebsType") String gsebsType);

    List<ElectronBasicOutData> listUnusedElectronBasic(ElectronBasicInData inData);

    List<GaiaSdElectronBusinessSet> getBusinessSetByStatus(@Param("client")String client, @Param("gsetsId")String gsetsId);
}