package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSaleHLocal;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdSaleHLocalMapper extends BaseMapper<GaiaSdSaleHLocal> {
    void insertOrUpdate(List<GaiaSdSaleHLocal> saleHList);
}