//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdAllotMutualH;
import com.gys.business.service.data.GetAllotInData;
import com.gys.business.service.data.GetAllotOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdAllotMutualHMapper extends BaseMapper<GaiaSdAllotMutualH> {
    List<GetAllotOutData> selectList(GetAllotInData inData);

    String selectNextVoucherId(@Param("clientId") String clientId);
}
