package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 
 * 中药类促销设置表
 */
@Data
public class GaiaSdPromChmedSet implements Serializable {
    private static final long serialVersionUID = -1846868308025630565L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String gspmsVoucherId;

    /**
     * 行号
     */
    private String gspmsSerial;

    /**
     * 达到数量1
     */
    private String gspmsQty1;

    /**
     * 生成促销折扣1
     */
    private String gspmsRebate1;

    /**
     * 达到数量2
     */
    private String gspmsQty2;

    /**
     * 生成促销折扣2
     */
    private String gspmsRebate2;

    /**
     * 达到数量3
     */
    private String gspmsQty3;

    /**
     * 生成促销折扣3
     */
    private String gspmsRebate3;

    /**
     * 是否会员
     */
    private String gspmsMemFlag;

    /**
     * 是否积分
     */
    private String gspmsInteFlag;

    /**
     * 积分倍率
     */
    private String gspmsInteRate;

    /**
     * 会员等级
     */
    private String gspmsMembershipGrade;
}