//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdCleandrawerD;
import com.gys.common.base.BaseMapper;

public interface GaiaSdCleandrawerDMapper extends BaseMapper<GaiaSdCleandrawerD> {
    void updateExa(GaiaSdCleandrawerD inData);
}
