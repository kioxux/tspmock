package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 
 * 电脑信息
 */
@Data
public class GaiaComputerInformation implements Serializable {
    private static final long serialVersionUID = 449636688414765701L;
    /**
     * ID
     */
    private String id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String brId;

    /**
     * 门店标识
     */
    private String storeLogo;

    /**
     * 版本信息
     */
    private String versionInformation;

    /**
     * 登陆时间
     */
    private String loginTime;

    /**
     * 电脑主机名
     */
    private String hostName;

    /**
     * 电脑IP
     */
    private String hostIp;

    /**
     * 电脑系统
     */
    private String system;

    /**
     * 登陆人员
     */
    private String loginEmp;

    /**
     * 是否绑定
     */
    private String bind;

    /**
     * 备用1
     */
    private String spare1;

    /**
     * 备用2
     */
    private String spare2;

    /**
     * 备用3
     */
    private String spare3;

    /**
     * 备用4
     */
    private String spare4;

    /**
     * 备用5
     */
    private String spare5;
}