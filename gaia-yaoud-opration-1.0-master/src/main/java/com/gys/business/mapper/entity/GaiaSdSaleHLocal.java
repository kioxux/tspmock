package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_SD_SALE_H_LOCAL")
public class GaiaSdSaleHLocal implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 销售单号
     */
    @Id
    @Column(name = "GSSH_BILL_NO")
    private String gsshBillNo;

    /**
     * 店名
     */
    @Id
    @Column(name = "GSSH_BR_ID")
    private String gsshBrId;

    /**
     * 销售日期
     */
    @Id
    @Column(name = "GSSH_DATE")
    private String gsshDate;

    /**
     * 销售时间
     */
    @Column(name = "GSSH_TIME")
    private String gsshTime;

    /**
     * 收银工号
     */
    @Column(name = "GSSH_EMP")
    private String gsshEmp;

    /**
     * 发票号
     */
    @Column(name = "GSSH_TAX_NO")
    private String gsshTaxNo;

    /**
     * 会员卡号
     */
    @Column(name = "GSSH_HYK_NO")
    private String gsshHykNo;

    /**
     * 零售价格
     */
    @Column(name = "GSSH_NORMAL_AMT")
    private BigDecimal gsshNormalAmt;

    /**
     * 折扣金额
     */
    @Column(name = "GSSH_ZK_AMT")
    private BigDecimal gsshZkAmt;

    /**
     * 应收金额
     */
    @Column(name = "GSSH_YS_AMT")
    private BigDecimal gsshYsAmt;

    /**
     * 现金找零
     */
    @Column(name = "GSSH_RMB_ZL_AMT")
    private BigDecimal gsshRmbZlAmt;

    /**
     * 现金收银金额
     */
    @Column(name = "GSSH_RMB_AMT")
    private BigDecimal gsshRmbAmt;

    /**
     * 抵用券卡号
     */
    @Column(name = "GSSH_DYQ_NO")
    private String gsshDyqNo;

    /**
     * 抵用券金额
     */
    @Column(name = "GSSH_DYQ_AMT")
    private BigDecimal gsshDyqAmt;

    /**
     * 抵用原因
     */
    @Column(name = "GSSH_DYQ_TYPE")
    private String gsshDyqType;

    /**
     * 储值卡号
     */
    @Column(name = "GSSH_RECHARGE_CARD_NO")
    private String gsshRechargeCardNo;

    /**
     * 储值卡消费金额
     */
    @Column(name = "GSSH_RECHARGE_CARD_AMT")
    private BigDecimal gsshRechargeCardAmt;

    /**
     * 电子券充值卡号1
     */
    @Column(name = "GSSH_DZQCZ_ACTNO1")
    private String gsshDzqczActno1;

    /**
     * 电子券充值金额1
     */
    @Column(name = "GSSH_DZQCZ_AMT1")
    private BigDecimal gsshDzqczAmt1;

    /**
     * 电子券抵用卡号1
     */
    @Column(name = "GSSH_DZQDY_ACTNO1")
    private String gsshDzqdyActno1;

    /**
     * 电子券抵用金额1
     */
    @Column(name = "GSSH_DZQDY_AMT1")
    private BigDecimal gsshDzqdyAmt1;

    /**
     * 增加积分
     */
    @Column(name = "GSSH_INTEGRAL_ADD")
    private String gsshIntegralAdd;

    /**
     * 兑换积分
     */
    @Column(name = "GSSH_INTEGRAL_EXCHANGE")
    private String gsshIntegralExchange;

    /**
     * 	加价兑换积分金额
     */
    @Column(name = "GSSH_INTEGRAL_EXCHANGE_AMT")
    private BigDecimal gsshIntegralExchangeAmt;

    /**
     * 抵现积分
     */
    @Column(name = "GSSH_INTEGRAL_CASH")
    private String gsshIntegralCash;

    /**
     * 抵现金额
     */
    @Column(name = "GSSH_INTEGRAL_CASH_AMT")
    private BigDecimal gsshIntegralCashAmt;

    /**
     * 支付方式1
     */
    @Column(name = "GSSH_PAYMENT_NO1")
    private String gsshPaymentNo1;

    /**
     * 支付金额1
     */
    @Column(name = "GSSH_PAYMENT_AMT1")
    private BigDecimal gsshPaymentAmt1;

    /**
     * 退货原销售单号
     */
    @Column(name = "GSSH_BILL_NO_RETURN")
    private String gsshBillNoReturn;

    /**
     * 退货审核人员
     */
    @Column(name = "GSSH_EMP_RETURN")
    private String gsshEmpReturn;

    /**
     * 参加促销活动类型1
     */
    @Column(name = "GSSH_PROMOTION_TYPE1")
    private String gsshPromotionType1;

    /**
     * 参加促销活动类型2
     */
    @Column(name = "GSSH_PROMOTION_TYPE2")
    private String gsshPromotionType2;

    /**
     * 参加促销活动类型3
     */
    @Column(name = "GSSH_PROMOTION_TYPE3")
    private String gsshPromotionType3;

    /**
     * 参加促销活动类型4
     */
    @Column(name = "GSSH_PROMOTION_TYPE4")
    private String gsshPromotionType4;

    /**
     * 参加促销活动类型5
     */
    @Column(name = "GSSH_PROMOTION_TYPE5")
    private String gsshPromotionType5;

    /**
     * 移动收银单号/外卖单号
     */
    @Column(name = "GSSH_REGISTER_VOUCHER_ID")
    private String gsshRegisterVoucherId;

    /**
     * 代销售店号
     */
    @Column(name = "GSSH_REPLACE_BR_ID")
    private String gsshReplaceBrId;

    /**
     * 代销售营业员
     */
    @Column(name = "GSSH_REPLACE_SALER_ID")
    private String gsshReplaceSalerId;

    /**
     * 是否挂起 1挂起 0不挂起
默认不挂起 2移动收银
     */
    @Column(name = "GSSH_HIDE_FLAG")
    private String gsshHideFlag;

    /**
     * 是否允许调出销售 1为是，0为否
     */
    @Column(name = "GSSH_CALL_ALLOW")
    private String gsshCallAllow;

    /**
     * 移动销售码
     */
    @Column(name = "GSSH_EMP_GROUP_NAME")
    private String gsshEmpGroupName;

    /**
     * 调用次数
     */
    @Column(name = "GSSH_CALL_QTY")
    private BigDecimal gsshCallQty;

    /**
     * 支付方式2
     */
    @Column(name = "GSSH_PAYMENT_NO2")
    private String gsshPaymentNo2;

    /**
     * 支付金额2
     */
    @Column(name = "GSSH_PAYMENT_AMT2")
    private BigDecimal gsshPaymentAmt2;

    /**
     * 支付方式3
     */
    @Column(name = "GSSH_PAYMENT_NO3")
    private String gsshPaymentNo3;

    /**
     * 支付金额3
     */
    @Column(name = "GSSH_PAYMENT_AMT3")
    private BigDecimal gsshPaymentAmt3;

    /**
     * 支付方式4
     */
    @Column(name = "GSSH_PAYMENT_NO4")
    private String gsshPaymentNo4;

    /**
     * 支付金额4
     */
    @Column(name = "GSSH_PAYMENT_AMT4")
    private BigDecimal gsshPaymentAmt4;

    /**
     * 支付方式5
     */
    @Column(name = "GSSH_PAYMENT_NO5")
    private String gsshPaymentNo5;

    /**
     * 支付金额5
     */
    @Column(name = "GSSH_PAYMENT_AMT5")
    private BigDecimal gsshPaymentAmt5;

    /**
     * 电子券抵用卡号2
     */
    @Column(name = "GSSH_DZQDY_ACTNO2")
    private String gsshDzqdyActno2;

    /**
     * 电子券抵用金额2
     */
    @Column(name = "GSSH_DZQDY_AMT2")
    private BigDecimal gsshDzqdyAmt2;

    /**
     * 电子券抵用卡号3
     */
    @Column(name = "GSSH_DZQDY_ACTNO3")
    private String gsshDzqdyActno3;

    /**
     * 电子券抵用金额3
     */
    @Column(name = "GSSH_DZQDY_AMT3")
    private BigDecimal gsshDzqdyAmt3;

    /**
     * 电子券抵用卡号4
     */
    @Column(name = "GSSH_DZQDY_ACTNO4")
    private String gsshDzqdyActno4;

    /**
     * 电子券抵用金额4
     */
    @Column(name = "GSSH_DZQDY_AMT4")
    private BigDecimal gsshDzqdyAmt4;

    /**
     * 销售备注
     */
    @Column(name = "GSSH_DZQDY_ACTNO5")
    private String gsshDzqdyActno5;

    /**
     * 电子券抵用金额5
     */
    @Column(name = "GSSH_DZQDY_AMT5")
    private BigDecimal gsshDzqdyAmt5;

    /**
     * 是否日结 0/空为否1为是
     */
    @Column(name = "GSSH_DAYREPORT")
    private String gsshDayreport;

    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;

    /**
     * 委托代煎供应商编号
     */
    @Column(name = "GSSH_CR_FLAG")
    private String gsshCrFlag;

    /**
     * 代煎状态 0未开方，1已完成，2已退货
     */
    @Column(name = "GSSH_CR_STATUS")
    private String gsshCrStatus;

    /**
     * 订单退货状态：0：已退货，1：医保退货完成, 2:医保退货失败
     */
    @Column(name = "GSSH_RETURN_STATUS")
    private String gsshReturnStatus;


    /**
     * 是否关联销售标识1表示是0表示否
     */
    @Column(name = "GSSH_IF_RELATED_SALE")
    private String gsshIfRelatedSale;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取销售单号
     *
     * @return GSSH_BILL_NO - 销售单号
     */
    public String getGsshBillNo() {
        return gsshBillNo;
    }

    /**
     * 设置销售单号
     *
     * @param gsshBillNo 销售单号
     */
    public void setGsshBillNo(String gsshBillNo) {
        this.gsshBillNo = gsshBillNo;
    }

    /**
     * 获取店名
     *
     * @return GSSH_BR_ID - 店名
     */
    public String getGsshBrId() {
        return gsshBrId;
    }

    /**
     * 设置店名
     *
     * @param gsshBrId 店名
     */
    public void setGsshBrId(String gsshBrId) {
        this.gsshBrId = gsshBrId;
    }

    /**
     * 获取销售日期
     *
     * @return GSSH_DATE - 销售日期
     */
    public String getGsshDate() {
        return gsshDate;
    }

    /**
     * 设置销售日期
     *
     * @param gsshDate 销售日期
     */
    public void setGsshDate(String gsshDate) {
        this.gsshDate = gsshDate;
    }

    /**
     * 获取销售时间
     *
     * @return GSSH_TIME - 销售时间
     */
    public String getGsshTime() {
        return gsshTime;
    }

    /**
     * 设置销售时间
     *
     * @param gsshTime 销售时间
     */
    public void setGsshTime(String gsshTime) {
        this.gsshTime = gsshTime;
    }

    /**
     * 获取收银工号
     *
     * @return GSSH_EMP - 收银工号
     */
    public String getGsshEmp() {
        return gsshEmp;
    }

    /**
     * 设置收银工号
     *
     * @param gsshEmp 收银工号
     */
    public void setGsshEmp(String gsshEmp) {
        this.gsshEmp = gsshEmp;
    }

    /**
     * 获取发票号
     *
     * @return GSSH_TAX_NO - 发票号
     */
    public String getGsshTaxNo() {
        return gsshTaxNo;
    }

    /**
     * 设置发票号
     *
     * @param gsshTaxNo 发票号
     */
    public void setGsshTaxNo(String gsshTaxNo) {
        this.gsshTaxNo = gsshTaxNo;
    }

    /**
     * 获取会员卡号
     *
     * @return GSSH_HYK_NO - 会员卡号
     */
    public String getGsshHykNo() {
        return gsshHykNo;
    }

    /**
     * 设置会员卡号
     *
     * @param gsshHykNo 会员卡号
     */
    public void setGsshHykNo(String gsshHykNo) {
        this.gsshHykNo = gsshHykNo;
    }

    /**
     * 获取零售价格
     *
     * @return GSSH_NORMAL_AMT - 零售价格
     */
    public BigDecimal getGsshNormalAmt() {
        return gsshNormalAmt;
    }

    /**
     * 设置零售价格
     *
     * @param gsshNormalAmt 零售价格
     */
    public void setGsshNormalAmt(BigDecimal gsshNormalAmt) {
        this.gsshNormalAmt = gsshNormalAmt;
    }

    /**
     * 获取折扣金额
     *
     * @return GSSH_ZK_AMT - 折扣金额
     */
    public BigDecimal getGsshZkAmt() {
        return gsshZkAmt;
    }

    /**
     * 设置折扣金额
     *
     * @param gsshZkAmt 折扣金额
     */
    public void setGsshZkAmt(BigDecimal gsshZkAmt) {
        this.gsshZkAmt = gsshZkAmt;
    }

    /**
     * 获取应收金额
     *
     * @return GSSH_YS_AMT - 应收金额
     */
    public BigDecimal getGsshYsAmt() {
        return gsshYsAmt;
    }

    /**
     * 设置应收金额
     *
     * @param gsshYsAmt 应收金额
     */
    public void setGsshYsAmt(BigDecimal gsshYsAmt) {
        this.gsshYsAmt = gsshYsAmt;
    }

    /**
     * 获取现金找零
     *
     * @return GSSH_RMB_ZL_AMT - 现金找零
     */
    public BigDecimal getGsshRmbZlAmt() {
        return gsshRmbZlAmt;
    }

    /**
     * 设置现金找零
     *
     * @param gsshRmbZlAmt 现金找零
     */
    public void setGsshRmbZlAmt(BigDecimal gsshRmbZlAmt) {
        this.gsshRmbZlAmt = gsshRmbZlAmt;
    }

    /**
     * 获取现金收银金额
     *
     * @return GSSH_RMB_AMT - 现金收银金额
     */
    public BigDecimal getGsshRmbAmt() {
        return gsshRmbAmt;
    }

    /**
     * 设置现金收银金额
     *
     * @param gsshRmbAmt 现金收银金额
     */
    public void setGsshRmbAmt(BigDecimal gsshRmbAmt) {
        this.gsshRmbAmt = gsshRmbAmt;
    }

    /**
     * 获取抵用券卡号
     *
     * @return GSSH_DYQ_NO - 抵用券卡号
     */
    public String getGsshDyqNo() {
        return gsshDyqNo;
    }

    /**
     * 设置抵用券卡号
     *
     * @param gsshDyqNo 抵用券卡号
     */
    public void setGsshDyqNo(String gsshDyqNo) {
        this.gsshDyqNo = gsshDyqNo;
    }

    /**
     * 获取抵用券金额
     *
     * @return GSSH_DYQ_AMT - 抵用券金额
     */
    public BigDecimal getGsshDyqAmt() {
        return gsshDyqAmt;
    }

    /**
     * 设置抵用券金额
     *
     * @param gsshDyqAmt 抵用券金额
     */
    public void setGsshDyqAmt(BigDecimal gsshDyqAmt) {
        this.gsshDyqAmt = gsshDyqAmt;
    }

    /**
     * 获取抵用原因
     *
     * @return GSSH_DYQ_TYPE - 抵用原因
     */
    public String getGsshDyqType() {
        return gsshDyqType;
    }

    /**
     * 设置抵用原因
     *
     * @param gsshDyqType 抵用原因
     */
    public void setGsshDyqType(String gsshDyqType) {
        this.gsshDyqType = gsshDyqType;
    }

    /**
     * 获取储值卡号
     *
     * @return GSSH_RECHARGE_CARD_NO - 储值卡号
     */
    public String getGsshRechargeCardNo() {
        return gsshRechargeCardNo;
    }

    /**
     * 设置储值卡号
     *
     * @param gsshRechargeCardNo 储值卡号
     */
    public void setGsshRechargeCardNo(String gsshRechargeCardNo) {
        this.gsshRechargeCardNo = gsshRechargeCardNo;
    }

    /**
     * 获取储值卡消费金额
     *
     * @return GSSH_RECHARGE_CARD_AMT - 储值卡消费金额
     */
    public BigDecimal getGsshRechargeCardAmt() {
        return gsshRechargeCardAmt;
    }

    /**
     * 设置储值卡消费金额
     *
     * @param gsshRechargeCardAmt 储值卡消费金额
     */
    public void setGsshRechargeCardAmt(BigDecimal gsshRechargeCardAmt) {
        this.gsshRechargeCardAmt = gsshRechargeCardAmt;
    }

    /**
     * 获取电子券充值卡号1
     *
     * @return GSSH_DZQCZ_ACTNO1 - 电子券充值卡号1
     */
    public String getGsshDzqczActno1() {
        return gsshDzqczActno1;
    }

    /**
     * 设置电子券充值卡号1
     *
     * @param gsshDzqczActno1 电子券充值卡号1
     */
    public void setGsshDzqczActno1(String gsshDzqczActno1) {
        this.gsshDzqczActno1 = gsshDzqczActno1;
    }

    /**
     * 获取电子券充值金额1
     *
     * @return GSSH_DZQCZ_AMT1 - 电子券充值金额1
     */
    public BigDecimal getGsshDzqczAmt1() {
        return gsshDzqczAmt1;
    }

    /**
     * 设置电子券充值金额1
     *
     * @param gsshDzqczAmt1 电子券充值金额1
     */
    public void setGsshDzqczAmt1(BigDecimal gsshDzqczAmt1) {
        this.gsshDzqczAmt1 = gsshDzqczAmt1;
    }

    /**
     * 获取电子券抵用卡号1
     *
     * @return GSSH_DZQDY_ACTNO1 - 电子券抵用卡号1
     */
    public String getGsshDzqdyActno1() {
        return gsshDzqdyActno1;
    }

    /**
     * 设置电子券抵用卡号1
     *
     * @param gsshDzqdyActno1 电子券抵用卡号1
     */
    public void setGsshDzqdyActno1(String gsshDzqdyActno1) {
        this.gsshDzqdyActno1 = gsshDzqdyActno1;
    }

    /**
     * 获取电子券抵用金额1
     *
     * @return GSSH_DZQDY_AMT1 - 电子券抵用金额1
     */
    public BigDecimal getGsshDzqdyAmt1() {
        return gsshDzqdyAmt1;
    }

    /**
     * 设置电子券抵用金额1
     *
     * @param gsshDzqdyAmt1 电子券抵用金额1
     */
    public void setGsshDzqdyAmt1(BigDecimal gsshDzqdyAmt1) {
        this.gsshDzqdyAmt1 = gsshDzqdyAmt1;
    }

    /**
     * 获取增加积分
     *
     * @return GSSH_INTEGRAL_ADD - 增加积分
     */
    public String getGsshIntegralAdd() {
        return gsshIntegralAdd;
    }

    /**
     * 设置增加积分
     *
     * @param gsshIntegralAdd 增加积分
     */
    public void setGsshIntegralAdd(String gsshIntegralAdd) {
        this.gsshIntegralAdd = gsshIntegralAdd;
    }

    /**
     * 获取兑换积分
     *
     * @return GSSH_INTEGRAL_EXCHANGE - 兑换积分
     */
    public String getGsshIntegralExchange() {
        return gsshIntegralExchange;
    }

    /**
     * 设置兑换积分
     *
     * @param gsshIntegralExchange 兑换积分
     */
    public void setGsshIntegralExchange(String gsshIntegralExchange) {
        this.gsshIntegralExchange = gsshIntegralExchange;
    }

    /**
     * 获取	加价兑换积分金额
     *
     * @return GSSH_INTEGRAL_EXCHANGE_AMT - 	加价兑换积分金额
     */
    public BigDecimal getGsshIntegralExchangeAmt() {
        return gsshIntegralExchangeAmt;
    }

    /**
     * 设置	加价兑换积分金额
     *
     * @param gsshIntegralExchangeAmt 	加价兑换积分金额
     */
    public void setGsshIntegralExchangeAmt(BigDecimal gsshIntegralExchangeAmt) {
        this.gsshIntegralExchangeAmt = gsshIntegralExchangeAmt;
    }

    /**
     * 获取抵现积分
     *
     * @return GSSH_INTEGRAL_CASH - 抵现积分
     */
    public String getGsshIntegralCash() {
        return gsshIntegralCash;
    }

    /**
     * 设置抵现积分
     *
     * @param gsshIntegralCash 抵现积分
     */
    public void setGsshIntegralCash(String gsshIntegralCash) {
        this.gsshIntegralCash = gsshIntegralCash;
    }

    /**
     * 获取抵现金额
     *
     * @return GSSH_INTEGRAL_CASH_AMT - 抵现金额
     */
    public BigDecimal getGsshIntegralCashAmt() {
        return gsshIntegralCashAmt;
    }

    /**
     * 设置抵现金额
     *
     * @param gsshIntegralCashAmt 抵现金额
     */
    public void setGsshIntegralCashAmt(BigDecimal gsshIntegralCashAmt) {
        this.gsshIntegralCashAmt = gsshIntegralCashAmt;
    }

    /**
     * 获取支付方式1
     *
     * @return GSSH_PAYMENT_NO1 - 支付方式1
     */
    public String getGsshPaymentNo1() {
        return gsshPaymentNo1;
    }

    /**
     * 设置支付方式1
     *
     * @param gsshPaymentNo1 支付方式1
     */
    public void setGsshPaymentNo1(String gsshPaymentNo1) {
        this.gsshPaymentNo1 = gsshPaymentNo1;
    }

    /**
     * 获取支付金额1
     *
     * @return GSSH_PAYMENT_AMT1 - 支付金额1
     */
    public BigDecimal getGsshPaymentAmt1() {
        return gsshPaymentAmt1;
    }

    /**
     * 设置支付金额1
     *
     * @param gsshPaymentAmt1 支付金额1
     */
    public void setGsshPaymentAmt1(BigDecimal gsshPaymentAmt1) {
        this.gsshPaymentAmt1 = gsshPaymentAmt1;
    }

    /**
     * 获取退货原销售单号
     *
     * @return GSSH_BILL_NO_RETURN - 退货原销售单号
     */
    public String getGsshBillNoReturn() {
        return gsshBillNoReturn;
    }

    /**
     * 设置退货原销售单号
     *
     * @param gsshBillNoReturn 退货原销售单号
     */
    public void setGsshBillNoReturn(String gsshBillNoReturn) {
        this.gsshBillNoReturn = gsshBillNoReturn;
    }

    /**
     * 获取退货审核人员
     *
     * @return GSSH_EMP_RETURN - 退货审核人员
     */
    public String getGsshEmpReturn() {
        return gsshEmpReturn;
    }

    /**
     * 设置退货审核人员
     *
     * @param gsshEmpReturn 退货审核人员
     */
    public void setGsshEmpReturn(String gsshEmpReturn) {
        this.gsshEmpReturn = gsshEmpReturn;
    }

    /**
     * 获取参加促销活动类型1
     *
     * @return GSSH_PROMOTION_TYPE1 - 参加促销活动类型1
     */
    public String getGsshPromotionType1() {
        return gsshPromotionType1;
    }

    /**
     * 设置参加促销活动类型1
     *
     * @param gsshPromotionType1 参加促销活动类型1
     */
    public void setGsshPromotionType1(String gsshPromotionType1) {
        this.gsshPromotionType1 = gsshPromotionType1;
    }

    /**
     * 获取参加促销活动类型2
     *
     * @return GSSH_PROMOTION_TYPE2 - 参加促销活动类型2
     */
    public String getGsshPromotionType2() {
        return gsshPromotionType2;
    }

    /**
     * 设置参加促销活动类型2
     *
     * @param gsshPromotionType2 参加促销活动类型2
     */
    public void setGsshPromotionType2(String gsshPromotionType2) {
        this.gsshPromotionType2 = gsshPromotionType2;
    }

    /**
     * 获取参加促销活动类型3
     *
     * @return GSSH_PROMOTION_TYPE3 - 参加促销活动类型3
     */
    public String getGsshPromotionType3() {
        return gsshPromotionType3;
    }

    /**
     * 设置参加促销活动类型3
     *
     * @param gsshPromotionType3 参加促销活动类型3
     */
    public void setGsshPromotionType3(String gsshPromotionType3) {
        this.gsshPromotionType3 = gsshPromotionType3;
    }

    /**
     * 获取参加促销活动类型4
     *
     * @return GSSH_PROMOTION_TYPE4 - 参加促销活动类型4
     */
    public String getGsshPromotionType4() {
        return gsshPromotionType4;
    }

    /**
     * 设置参加促销活动类型4
     *
     * @param gsshPromotionType4 参加促销活动类型4
     */
    public void setGsshPromotionType4(String gsshPromotionType4) {
        this.gsshPromotionType4 = gsshPromotionType4;
    }

    /**
     * 获取参加促销活动类型5
     *
     * @return GSSH_PROMOTION_TYPE5 - 参加促销活动类型5
     */
    public String getGsshPromotionType5() {
        return gsshPromotionType5;
    }

    /**
     * 设置参加促销活动类型5
     *
     * @param gsshPromotionType5 参加促销活动类型5
     */
    public void setGsshPromotionType5(String gsshPromotionType5) {
        this.gsshPromotionType5 = gsshPromotionType5;
    }

    /**
     * 获取移动收银单号/外卖单号
     *
     * @return GSSH_REGISTER_VOUCHER_ID - 移动收银单号/外卖单号
     */
    public String getGsshRegisterVoucherId() {
        return gsshRegisterVoucherId;
    }

    /**
     * 设置移动收银单号/外卖单号
     *
     * @param gsshRegisterVoucherId 移动收银单号/外卖单号
     */
    public void setGsshRegisterVoucherId(String gsshRegisterVoucherId) {
        this.gsshRegisterVoucherId = gsshRegisterVoucherId;
    }

    /**
     * 获取代销售店号
     *
     * @return GSSH_REPLACE_BR_ID - 代销售店号
     */
    public String getGsshReplaceBrId() {
        return gsshReplaceBrId;
    }

    /**
     * 设置代销售店号
     *
     * @param gsshReplaceBrId 代销售店号
     */
    public void setGsshReplaceBrId(String gsshReplaceBrId) {
        this.gsshReplaceBrId = gsshReplaceBrId;
    }

    /**
     * 获取代销售营业员
     *
     * @return GSSH_REPLACE_SALER_ID - 代销售营业员
     */
    public String getGsshReplaceSalerId() {
        return gsshReplaceSalerId;
    }

    /**
     * 设置代销售营业员
     *
     * @param gsshReplaceSalerId 代销售营业员
     */
    public void setGsshReplaceSalerId(String gsshReplaceSalerId) {
        this.gsshReplaceSalerId = gsshReplaceSalerId;
    }

    /**
     * 获取是否挂起 1挂起 0不挂起
默认不挂起 2移动收银
     *
     * @return GSSH_HIDE_FLAG - 是否挂起 1挂起 0不挂起
默认不挂起 2移动收银
     */
    public String getGsshHideFlag() {
        return gsshHideFlag;
    }

    /**
     * 设置是否挂起 1挂起 0不挂起
默认不挂起 2移动收银
     *
     * @param gsshHideFlag 是否挂起 1挂起 0不挂起
默认不挂起 2移动收银
     */
    public void setGsshHideFlag(String gsshHideFlag) {
        this.gsshHideFlag = gsshHideFlag;
    }

    /**
     * 获取是否允许调出销售 1为是，0为否
     *
     * @return GSSH_CALL_ALLOW - 是否允许调出销售 1为是，0为否
     */
    public String getGsshCallAllow() {
        return gsshCallAllow;
    }

    /**
     * 设置是否允许调出销售 1为是，0为否
     *
     * @param gsshCallAllow 是否允许调出销售 1为是，0为否
     */
    public void setGsshCallAllow(String gsshCallAllow) {
        this.gsshCallAllow = gsshCallAllow;
    }

    /**
     * 获取移动销售码
     *
     * @return GSSH_EMP_GROUP_NAME - 移动销售码
     */
    public String getGsshEmpGroupName() {
        return gsshEmpGroupName;
    }

    /**
     * 设置移动销售码
     *
     * @param gsshEmpGroupName 移动销售码
     */
    public void setGsshEmpGroupName(String gsshEmpGroupName) {
        this.gsshEmpGroupName = gsshEmpGroupName;
    }

    /**
     * 获取调用次数
     *
     * @return GSSH_CALL_QTY - 调用次数
     */
    public BigDecimal getGsshCallQty() {
        return gsshCallQty;
    }

    /**
     * 设置调用次数
     *
     * @param gsshCallQty 调用次数
     */
    public void setGsshCallQty(BigDecimal gsshCallQty) {
        this.gsshCallQty = gsshCallQty;
    }

    /**
     * 获取支付方式2
     *
     * @return GSSH_PAYMENT_NO2 - 支付方式2
     */
    public String getGsshPaymentNo2() {
        return gsshPaymentNo2;
    }

    /**
     * 设置支付方式2
     *
     * @param gsshPaymentNo2 支付方式2
     */
    public void setGsshPaymentNo2(String gsshPaymentNo2) {
        this.gsshPaymentNo2 = gsshPaymentNo2;
    }

    /**
     * 获取支付金额2
     *
     * @return GSSH_PAYMENT_AMT2 - 支付金额2
     */
    public BigDecimal getGsshPaymentAmt2() {
        return gsshPaymentAmt2;
    }

    /**
     * 设置支付金额2
     *
     * @param gsshPaymentAmt2 支付金额2
     */
    public void setGsshPaymentAmt2(BigDecimal gsshPaymentAmt2) {
        this.gsshPaymentAmt2 = gsshPaymentAmt2;
    }

    /**
     * 获取支付方式3
     *
     * @return GSSH_PAYMENT_NO3 - 支付方式3
     */
    public String getGsshPaymentNo3() {
        return gsshPaymentNo3;
    }

    /**
     * 设置支付方式3
     *
     * @param gsshPaymentNo3 支付方式3
     */
    public void setGsshPaymentNo3(String gsshPaymentNo3) {
        this.gsshPaymentNo3 = gsshPaymentNo3;
    }

    /**
     * 获取支付金额3
     *
     * @return GSSH_PAYMENT_AMT3 - 支付金额3
     */
    public BigDecimal getGsshPaymentAmt3() {
        return gsshPaymentAmt3;
    }

    /**
     * 设置支付金额3
     *
     * @param gsshPaymentAmt3 支付金额3
     */
    public void setGsshPaymentAmt3(BigDecimal gsshPaymentAmt3) {
        this.gsshPaymentAmt3 = gsshPaymentAmt3;
    }

    /**
     * 获取支付方式4
     *
     * @return GSSH_PAYMENT_NO4 - 支付方式4
     */
    public String getGsshPaymentNo4() {
        return gsshPaymentNo4;
    }

    /**
     * 设置支付方式4
     *
     * @param gsshPaymentNo4 支付方式4
     */
    public void setGsshPaymentNo4(String gsshPaymentNo4) {
        this.gsshPaymentNo4 = gsshPaymentNo4;
    }

    /**
     * 获取支付金额4
     *
     * @return GSSH_PAYMENT_AMT4 - 支付金额4
     */
    public BigDecimal getGsshPaymentAmt4() {
        return gsshPaymentAmt4;
    }

    /**
     * 设置支付金额4
     *
     * @param gsshPaymentAmt4 支付金额4
     */
    public void setGsshPaymentAmt4(BigDecimal gsshPaymentAmt4) {
        this.gsshPaymentAmt4 = gsshPaymentAmt4;
    }

    /**
     * 获取支付方式5
     *
     * @return GSSH_PAYMENT_NO5 - 支付方式5
     */
    public String getGsshPaymentNo5() {
        return gsshPaymentNo5;
    }

    /**
     * 设置支付方式5
     *
     * @param gsshPaymentNo5 支付方式5
     */
    public void setGsshPaymentNo5(String gsshPaymentNo5) {
        this.gsshPaymentNo5 = gsshPaymentNo5;
    }

    /**
     * 获取支付金额5
     *
     * @return GSSH_PAYMENT_AMT5 - 支付金额5
     */
    public BigDecimal getGsshPaymentAmt5() {
        return gsshPaymentAmt5;
    }

    /**
     * 设置支付金额5
     *
     * @param gsshPaymentAmt5 支付金额5
     */
    public void setGsshPaymentAmt5(BigDecimal gsshPaymentAmt5) {
        this.gsshPaymentAmt5 = gsshPaymentAmt5;
    }

    /**
     * 获取电子券抵用卡号2
     *
     * @return GSSH_DZQDY_ACTNO2 - 电子券抵用卡号2
     */
    public String getGsshDzqdyActno2() {
        return gsshDzqdyActno2;
    }

    /**
     * 设置电子券抵用卡号2
     *
     * @param gsshDzqdyActno2 电子券抵用卡号2
     */
    public void setGsshDzqdyActno2(String gsshDzqdyActno2) {
        this.gsshDzqdyActno2 = gsshDzqdyActno2;
    }

    /**
     * 获取电子券抵用金额2
     *
     * @return GSSH_DZQDY_AMT2 - 电子券抵用金额2
     */
    public BigDecimal getGsshDzqdyAmt2() {
        return gsshDzqdyAmt2;
    }

    /**
     * 设置电子券抵用金额2
     *
     * @param gsshDzqdyAmt2 电子券抵用金额2
     */
    public void setGsshDzqdyAmt2(BigDecimal gsshDzqdyAmt2) {
        this.gsshDzqdyAmt2 = gsshDzqdyAmt2;
    }

    /**
     * 获取电子券抵用卡号3
     *
     * @return GSSH_DZQDY_ACTNO3 - 电子券抵用卡号3
     */
    public String getGsshDzqdyActno3() {
        return gsshDzqdyActno3;
    }

    /**
     * 设置电子券抵用卡号3
     *
     * @param gsshDzqdyActno3 电子券抵用卡号3
     */
    public void setGsshDzqdyActno3(String gsshDzqdyActno3) {
        this.gsshDzqdyActno3 = gsshDzqdyActno3;
    }

    /**
     * 获取电子券抵用金额3
     *
     * @return GSSH_DZQDY_AMT3 - 电子券抵用金额3
     */
    public BigDecimal getGsshDzqdyAmt3() {
        return gsshDzqdyAmt3;
    }

    /**
     * 设置电子券抵用金额3
     *
     * @param gsshDzqdyAmt3 电子券抵用金额3
     */
    public void setGsshDzqdyAmt3(BigDecimal gsshDzqdyAmt3) {
        this.gsshDzqdyAmt3 = gsshDzqdyAmt3;
    }

    /**
     * 获取电子券抵用卡号4
     *
     * @return GSSH_DZQDY_ACTNO4 - 电子券抵用卡号4
     */
    public String getGsshDzqdyActno4() {
        return gsshDzqdyActno4;
    }

    /**
     * 设置电子券抵用卡号4
     *
     * @param gsshDzqdyActno4 电子券抵用卡号4
     */
    public void setGsshDzqdyActno4(String gsshDzqdyActno4) {
        this.gsshDzqdyActno4 = gsshDzqdyActno4;
    }

    /**
     * 获取电子券抵用金额4
     *
     * @return GSSH_DZQDY_AMT4 - 电子券抵用金额4
     */
    public BigDecimal getGsshDzqdyAmt4() {
        return gsshDzqdyAmt4;
    }

    /**
     * 设置电子券抵用金额4
     *
     * @param gsshDzqdyAmt4 电子券抵用金额4
     */
    public void setGsshDzqdyAmt4(BigDecimal gsshDzqdyAmt4) {
        this.gsshDzqdyAmt4 = gsshDzqdyAmt4;
    }

    /**
     * 获取销售备注
     *
     * @return GSSH_DZQDY_ACTNO5 - 销售备注
     */
    public String getGsshDzqdyActno5() {
        return gsshDzqdyActno5;
    }

    /**
     * 设置销售备注
     *
     * @param gsshDzqdyActno5 销售备注
     */
    public void setGsshDzqdyActno5(String gsshDzqdyActno5) {
        this.gsshDzqdyActno5 = gsshDzqdyActno5;
    }

    /**
     * 获取电子券抵用金额5
     *
     * @return GSSH_DZQDY_AMT5 - 电子券抵用金额5
     */
    public BigDecimal getGsshDzqdyAmt5() {
        return gsshDzqdyAmt5;
    }

    /**
     * 设置电子券抵用金额5
     *
     * @param gsshDzqdyAmt5 电子券抵用金额5
     */
    public void setGsshDzqdyAmt5(BigDecimal gsshDzqdyAmt5) {
        this.gsshDzqdyAmt5 = gsshDzqdyAmt5;
    }

    /**
     * 获取是否日结 0/空为否1为是
     *
     * @return GSSH_DAYREPORT - 是否日结 0/空为否1为是
     */
    public String getGsshDayreport() {
        return gsshDayreport;
    }

    /**
     * 设置是否日结 0/空为否1为是
     *
     * @param gsshDayreport 是否日结 0/空为否1为是
     */
    public void setGsshDayreport(String gsshDayreport) {
        this.gsshDayreport = gsshDayreport;
    }

    /**
     * @return LAST_UPDATE_TIME
     */
    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    /**
     * @param lastUpdateTime
     */
    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    /**
     * 获取委托代煎供应商编号
     *
     * @return GSSH_CR_FLAG - 委托代煎供应商编号
     */
    public String getGsshCrFlag() {
        return gsshCrFlag;
    }

    /**
     * 设置委托代煎供应商编号
     *
     * @param gsshCrFlag 委托代煎供应商编号
     */
    public void setGsshCrFlag(String gsshCrFlag) {
        this.gsshCrFlag = gsshCrFlag;
    }

    /**
     * 获取代煎状态 0未开方，1已完成，2已退货
     *
     * @return GSSH_CR_STATUS - 代煎状态 0未开方，1已完成，2已退货
     */
    public String getGsshCrStatus() {
        return gsshCrStatus;
    }

    /**
     * 设置代煎状态 0未开方，1已完成，2已退货
     *
     * @param gsshCrStatus 代煎状态 0未开方，1已完成，2已退货
     */
    public void setGsshCrStatus(String gsshCrStatus) {
        this.gsshCrStatus = gsshCrStatus;
    }

    /**
     * 获取订单退货状态：0：已退货，1：医保退货完成, 2:医保退货失败
     *
     * @return GSSH_RETURN_STATUS - 订单退货状态：0：已退货，1：医保退货完成, 2:医保退货失败
     */
    public String getGsshReturnStatus() {
        return gsshReturnStatus;
    }

    /**
     * 设置订单退货状态：0：已退货，1：医保退货完成, 2:医保退货失败
     *
     * @param gsshReturnStatus 订单退货状态：0：已退货，1：医保退货完成, 2:医保退货失败
     */
    public void setGsshReturnStatus(String gsshReturnStatus) {
        this.gsshReturnStatus = gsshReturnStatus;
    }

    /**
     * 获取是否关联销售标识1表示是0表示否
     *
     * @return GSSH_IF_RELATED_SALE - 是否关联销售标识1表示是0表示否
     */
    public String getGsshIfRelatedSale() {
        return gsshIfRelatedSale;
    }

    /**
     * 设置是否关联销售标识1表示是0表示否
     *
     * @param gsshIfRelatedSale 是否关联销售标识1表示是0表示否
     */
    public void setGsshIfRelatedSale(String gsshIfRelatedSale) {
        this.gsshIfRelatedSale = gsshIfRelatedSale;
    }
}