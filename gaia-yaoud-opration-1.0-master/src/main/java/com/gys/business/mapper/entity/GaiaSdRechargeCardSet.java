//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(
        name = "GAIA_SD_RECHARGE_CARD_SET"
)
public class GaiaSdRechargeCardSet implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSRCS_BR_ID"
    )
    private String gsrcsBrId;
    @Column(
            name = "GSRCS_CARD_PREFIX"
    )
    private String gsrcsCardPrefix;
    @Column(
            name = "GSRCS_PASSWORD"
    )
    private String gsrcsPassword;
//    @Column(
//            name = "GSRCS_PAYMENT_METHOD"
//    )
//    private String gsrcsPaymentMethod;
    @Column(
            name = "GSRCS_PW_NEED_OPT"
    )
    private String gsrcsPwNeedOpt;
//    @Column(
//            name = "GSRCS_MIN_AMT_OPT"
//    )
//    private String gsrcsMinAmtOpt;
//    @Column(
//            name = "GSRCS_MAX_AMT_OPT"
//    )
//    private String gsrcsMaxAmtOpt;
//    @Column(
//            name = "GSRCS_FIRST_PW_OPT"
//    )
//    private String gsrcsFirstPwOpt;
    @Column(
            name = "GSRCS_RECOVERPW_OPT"
    )
    private String gsrcsRecoverpwOpt;
    @Column(
            name = "GSRCS_PROPORTION1"
    )
    private String gsrcsProportion1;
    @Column(
            name = "GSRCS_MIN_AMT1"
    )
    private BigDecimal gsrcsMinAmt1;
    @Column(
            name = "GSRCS_PROPORTION2"
    )
    private String gsrcsProportion2;
    @Column(
            name = "GSRCS_MIN_AMT2"
    )
    private BigDecimal gsrcsMinAmt2;
    @Column(
            name = "GSRCS_PROMOTION2"
    )
    private String gsrcsPromotion2;
    @Column(
            name = "GSRCS_REPEAT2"
    )
    private String gsrcsRepeat2;
    @Column(
            name = "GSRCS_PROPORTION3"
    )
    private String gsrcsProportion3;
    @Column(
            name = "GSRCS_MIN_AMT3"
    )
    private BigDecimal gsrcsMinAmt3;
    @Column(
            name = "GSRCS_PROMOTION3"
    )
    private String gsrcsPromotion3;
    @Column(
            name = "GSRCS_REPEAT3"
    )
    private String gsrcsRepeat3;
    @Column(
            name = "GSRCS_UPDATE_DATE"
    )
    private String gsrcsUpdateDate;
    @Column(
            name = "GSRCS_UPDATE_EMP"
    )
    private String gsrcsUpdateEmp;
    private static final long serialVersionUID = 1L;
}
