package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductYmDm;
import com.gys.business.service.data.GaiaProductYmInData;
import com.gys.business.service.data.GaiaProductYmOutData;
import com.gys.util.yaomeng.YaomengDrugDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * 药盟对码表(GaiaProductYmDm)表数据库访问层
 *
 * @author XIAOZY
 * @since 2021-11-11 20:26:42
 */
public interface GaiaProductYmDmMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaProductYmDm queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaProductYmDm 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaProductYmDm> queryAllByLimit(GaiaProductYmDm gaiaProductYmDm, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaProductYmDm 查询条件
     * @return 总行数
     */
    long count(GaiaProductYmDm gaiaProductYmDm);

    /**
     * 新增数据
     *
     * @param gaiaProductYmDm 实例对象
     * @return 影响行数
     */
    int insert(GaiaProductYmDm gaiaProductYmDm);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaProductYmDm> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaProductYmDm> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaProductYmDm> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaProductYmDm> entities);

    /**
     * 修改数据
     *
     * @param gaiaProductYmDm 实例对象
     * @return 影响行数
     */
    int update(GaiaProductYmDm gaiaProductYmDm);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

//    GaiaProductYmDm getDMDetail(GaiaProductYmInData param);

//    List<GaiaProductYmOutData> listDMInfo(GaiaProductYmInData param);

    List<YaomengDrugDto> listYMRelation(@Param("proSelfCodeList") List<String> proSelfCodeList,@Param("client") String client,@Param("stoCode") String stoCode);
}

