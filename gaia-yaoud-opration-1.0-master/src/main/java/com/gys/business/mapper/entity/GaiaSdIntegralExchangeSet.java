package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "GAIA_SD_INTEGRAL_EXCHANGE_SET")
@Data
public class GaiaSdIntegralExchangeSet implements Serializable {
    private static final long serialVersionUID = -5872962753749283395L;
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSIES_VOUCHER_ID")
    private String gsiesVoucherId;

    @Column(name = "GSIES_BR_ID")
    private String gsiesBrId;

    @Column(name = "GSIES_BR_NAME")
    private String gsiesBrName;

    @Column(name = "GSIES_STORE_GROUP")
    private String gsiesStoreGroup;

    @Column(name = "GSIES_MEMBER_CLASS")
    private String gsiesMemberClass;

    @Column(name = "GSIES_DATE_START")
    private String gsiesDateStart;

    @Column(name = "GSIES_DATE_END")
    private String gsiesDateEnd;

    @Column(name = "GSIES_EXCHANGE_PRO_ID")
    private String gsiesExchangeProId;

    @Column(name = "GSIES_EXCHANGE_INTEGRA")
    private String gsiesExchangeIntegra;

    @Column(name = "GSIES_EXCHANGE_AMT")
    private BigDecimal gsiesExchangeAmt;

    @Column(name = "GSIES_EXCHANGE_QTY")
    private BigDecimal gsiesExchangeQty;

    @Column(name = "GSIES_EXCHANGE_REPEAT")
    private String gsiesExchangeRepeat;

    @Column(name = "GSIES_INTEGRAL_ADD_SET")
    private String gsiesIntegralAddSet;

    @Column(name = "GSIES_STATUS")
    private String gsiesStatus;

}
