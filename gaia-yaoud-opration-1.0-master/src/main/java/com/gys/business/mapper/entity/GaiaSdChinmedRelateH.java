package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "GAIA_SD_CHINMED_RELATE_H")
public class GaiaSdChinmedRelateH {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "BR_ID"
    )
    private String brId;
    @Id
    @Column(
            name = "PID"
    )
    private String pid;
    @Column(
            name = "PATIENT"
    )
    private String patient;
    @Column(
            name = "GENDER"
    )
    private String gender;
    @Column(
            name = "AGE"
    )
    private String age;
    @Column(
            name = "PHONE"
    )
    private String phone;
    @Column(
            name = "ADDRESS"
    )
    private String address;
    @Column(
            name = "DIAGNOSIS"
    )
    private String diagnosis;
    @Column(
            name = "DOCTOR"
    )
    private String doctor;
    @Column(
            name = "DRUGNAME"
    )
    private String drugName;
    @Column(
            name = "FORMS"
    )
    private String forms;
    @Column(
            name = "QUANTITY"
    )
    private String quantity;
    @Column(
            name = "DOSAGE"
    )
    private String dosage;
    @Column(
            name = "DATE"
    )
    private String date;
    @Column(
            name = "SHIP"
    )
    private String ship;
    @Column(
            name = "TYPE"
    )
    private String type;
    @Column(
            name = "PRICE"
    )
    private String price;
    @Column(
            name = "STATUS"
    )
    private String status;
    @Column(
            name = "REMARKS"
    )
    private String remarks;
    @Column(
            name = "TCM_USE"
    )
    private String tcmUse;
    @Column(
            name = "OP_EM_HP_NO"
    )
    private String opEmHpNo;
    @Column(
            name = "RX_CODE"
    )
    private String rxCode;
    @Column(
            name = "TCM_DECOCT"
    )
    private String tcmDecoct;
    @Column(
            name = "DOC_IDCARD"
    )
    private String docIdCard;
    @Column(
            name = "SALE_ID"
    )
    private String saleId;

}
