package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSaleHandH;
import com.gys.business.service.data.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdSaleHandHMapper {

    void insert(GaiaSdSaleHandH handH);

    GaiaSdSaleHandH getSaleHByBillNo(@Param("gsshBillNo") String gsshBillNo, @Param("clientId") String clientId, @Param("gsshBrId") String gsshBrId);

    List<GetRestOrderInfoOutData> getSaleHand(@Param("client") String client, @Param("depId") String depId, @Param("gsshDate") String gsshDate, @Param("billNo") String billNo);

    List<GetRestOrderInfoOutData> getAllSaleHand(@Param("client") String client, @Param("depId") String depId, @Param("startDate") String startDate, @Param("endDate") String endDate);

    void update(GaiaSdSaleHandH handH);

    String getMaxByBrId(@Param("client") String clientId, @Param("brId") String brId, @Param("nowDate") String nowDate);

    /**
     * app销售查询
     *
     * @param inData
     * @return
     */
    List<AppParameterDto> getAllListByApp(AppParameterVo inData);

    GaiaSdSaleHandH selectByBillNo(@Param("billNo") String billNo, @Param("clientId") String clientId, @Param("brId") String brId);

    void updateByAppPrimaryKey(GaiaSdSaleHandH sdSaleH);

    List<RebornData> getAllListByAppBillNo(AppParameterVo inData);

    List<AppParameterDetailsDto> getAppByDetails(AppParameterVo inData);

    List<GetRestOrderInfoOutData> listSaleHand(@Param("client") String client, @Param("depId") String depId, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("gsshHideFlag") String gsshHideFlag,
                                               @Param("gsshBillType") String gsshBillType,@Param("gsshOrderSource") String gsshOrderSource,@Param("gsshBillOrderId") String gsshBillOrderId);

    List<GetRestOrderInfoOutData> getSaleHandForSaleOther(@Param("client") String client, @Param("depId") String depId, @Param("gsshDate") String gsshDate, @Param("billNo") String billNo, @Param("gsshHideFlag") String gsshHideFlag);

    List<GetRestOrderInfoOutData> getHisHand(@Param("client") String client, @Param("brId") String brId, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("billNo") String billNo,@Param("proCode") String proCode);

    GaiaSdSaleHandH getHandByRegisterVoucher(@Param("gsshBillNo") String gsshBillNo, @Param("clientId") String clientId, @Param("dsfOrderCode") String dsfOrderCode);

    void updateSaleHandStatus(@Param("client") String client, @Param("stoCode") String depId, @Param("saleBillNo") String billNo, @Param("gsshBdFlag") String gsshBdFlag);

}
