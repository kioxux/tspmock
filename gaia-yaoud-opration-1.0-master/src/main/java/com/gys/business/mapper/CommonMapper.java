package com.gys.business.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CommonMapper {
    /**
     * 生成最大编码 如：PD + 年 + 000001
     * clientId 加盟商编码 type 类型（PD：补货 Y 新批次） num 为年后面拼接位数
     * 如要使用该方法第一次需要在CODE_VAL_PD维护最大编码（非新建数据）
     * @param clientId
     * @param type
     * @param num
     * @return
     */
    String selectNextCode(@Param("clientId") String clientId,@Param("type") String type,@Param("num") Integer num);

    String selectNextCodeByBatch(@Param("clientId") String clientId,@Param("type") String type);//获取最大批次编号

    void addBatchToCode(@Param("clientId") String clientId,@Param("codeVal") String codeVal,@Param("type") String type);

    String selectNextCodeByReplenish(@Param("clientId") String clientId,@Param("type") String type);//获取最大补货单号

    String selectNextCodeByCancel(@Param("clientId") String clientId,@Param("type") String type);//获取最大KD退库单号
}
