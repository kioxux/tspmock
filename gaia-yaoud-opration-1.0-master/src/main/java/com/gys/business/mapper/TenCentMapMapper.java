package com.gys.business.mapper;

import java.util.List;
import java.util.Map;

public interface TenCentMapMapper {



    List<Map<String,String>> getAddressList(Map<String, Object> inData);

    void updateLatAndLngByAddress(Map<String,String> map);
}
