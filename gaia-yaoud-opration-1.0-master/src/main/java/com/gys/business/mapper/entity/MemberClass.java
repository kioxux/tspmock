package com.gys.business.mapper.entity;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.poi.hpsf.Decimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 会员卡类型表
 */
@Table(name = "GAIA_SD_MEMBER_CLASS")
@Data
public class MemberClass implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    @ApiModelProperty(value = "加盟商")
    private String clientId;

    /**
     * 卡类型编号
     */
    @Id
    @Column(name = "GSMC_ID")
    @ApiModelProperty(value = "卡类型编号")
    private String gsmcId;

    /**
     * 卡类型名称
     */
    @Column(name = "GSMC_NAME")
    @ApiModelProperty(value = "卡类型名称")
    private String gsmcName;

    /**
     * 卡类型折率
     */
    @Column(name = "GSMC_DISCOUNT_RATE")
    @ApiModelProperty(value = "卡类型折率")
    private String gsmcDiscountRate;

    @Column(name = "GSMC_DISCOUNT_RATE2")
    @ApiModelProperty(value = "折上折")
    private String gsmcDiscountRate2;

    /**
     * 消费金额
     */
    @Column(name = "GSMC_AMT_SALE")
    @ApiModelProperty(value = "消费金额")
    private BigDecimal gsmcAmtSale;

    /**
     * 积分数值
     */
    @Column(name = "GSMC_INTEGRAL_SALE")
    @ApiModelProperty(value = "积分数值")
    private String gsmcIntegralSale;

    /**
     * 达到积分
     */
    @Column(name = "GSMC_UPGRADE_INTE")
    @ApiModelProperty(value = "达到积分")
    private String gsmcUpgradeInte;

    /**
     * 达到储值
     */
    @Column(name = "GSMC_UPGRADE_RECH")
    @ApiModelProperty(value = "达到储值")
    private String gsmcUpgradeRech;

    /**
     * 升级条件
     */
    @Column(name = "GSMC_UPGRADE_SET")
    @ApiModelProperty(value = "升级条件")
    private String gsmcUpgradeSet;

    /**
     * 新增日期
     */
    @Column(name = "GSMC_CRE_DATE")
    @ApiModelProperty(value = "新增日期")
    private String gsmcCreDate;


    /**
     * 新增人员
     */
    @Column(name = "GSMC_CRE_EMP")
    @ApiModelProperty(value = "新增人员")
    private String gsmcCreEmp;

    /**
     * 更新日期
     */
    @Column(name = "GSMC_UPDATE_DATE")
    @ApiModelProperty(value = "更新日期")
    private String gsmcUpdateDate;

    /**
     * 更新人员
     */
    @Column(name = "GSMC_UPDATE_EMP")
    @ApiModelProperty(value = "更新人员")
    private String gsmcUpdateEmp;




}
