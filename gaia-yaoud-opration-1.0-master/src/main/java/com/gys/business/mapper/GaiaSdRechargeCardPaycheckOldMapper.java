//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRechargeCardPaycheckOld;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdRechargeCardPaycheckOldMapper extends BaseMapper<GaiaSdRechargeCardPaycheckOld> {
    String selectNextVoucherId(@Param("client") String client, @Param("codePre") String codePre);
}
