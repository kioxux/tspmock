package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author ：gyx
 * @Date ：Created in 14:01 2021/11/19
 * @Description：会员活跃度
 * @Modified By：gyx
 * @Version:
 */
@Data
@Table(name = "GAIA_SD_MEMBER_GRADE_LIST")
public class GaiaSdMemberGrade {
    //加盟商
    @Column(name = "CLIENT")
    private String client;
    //会员卡号
    @Column(name = "GSMGL_CARD_ID")
    private String gsmglCardId;
    //分级编号
    @Column(name = "GSMGL_GRADE_ID")
    private String gsmglGradeId;
    //周期内消费总额
    @Column(name = "GSMGL_AMT")
    private String gsmglAmt;
    //周期内消费次数
    @Column(name = "GSMGL_SALE_TIMES")
    private String gsmglSaleTimes;
    //评估周期
    @Column(name = "GSMGL_CYCLE")
    private String gsmglCycle;
    //最后一次交易时间
    @Column(name = "GSMGL_LAST_SALE")
    private String gsmglLastSale;
    //计算月份
    @Column(name = "GSMGL_CALC_MONTH")
    private String gsmglCalcMonth;
    //更新日期
    @Column(name = "GSMGL_UPDATE_DATE")
    private String gsmglUpdateDate;
}
