package com.gys.business.mapper;

import com.gys.common.data.GaiaSdIntegralExchangeRecord;
import com.gys.common.data.NewExchangeTotalNumBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdIntegralExchangeRecordDao {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdIntegralExchangeRecord record);

    int insertSelective(GaiaSdIntegralExchangeRecord record);

    GaiaSdIntegralExchangeRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdIntegralExchangeRecord record);

    int updateByPrimaryKey(GaiaSdIntegralExchangeRecord record);

    void insertBatch(List<GaiaSdIntegralExchangeRecord> list);

    List<GaiaSdIntegralExchangeRecord> exchangeTotalNum(NewExchangeTotalNumBean bean);

    List<GaiaSdIntegralExchangeRecord> getBySaleBillNoAndProId(@Param("client") String client, @Param("stoCode") String stoCode,
                                                               @Param("saleBillNo") String saleBillNo, @Param("list") List<String> list);
}