//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(
        name = "GAIA_SD_ACCEPT_H"
)
@Data
public class GaiaSdAcceptH implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSAH_VOUCHER_ID"
    )
    private String gsahVoucherId;
    @Column(
            name = "GSAH_BR_ID"
    )
    private String gsahBrId;
    @Column(
            name = "GSAH_DATE"
    )
    private String gsahDate;
    @Column(
            name = "GSAH_PS_VOUCHER_ID"
    )
    private String gsahPsVoucherId;
    @Column(
            name = "GSAH_POID"
    )
    private String gsahPoid;
    @Column(
            name = "GSAH_DEPARTURE_PLACE"
    )
    private String gsahDeparturePlace;
    @Column(
            name = "GSAH_DEPARTURE_DATE"
    )
    private String gsahDepartureDate;
    @Column(
            name = "GSAH_DEPARTURE_TIME"
    )
    private String gsahDepartureTime;
    @Column(
            name = "GSAH_DEPARTURE_TEMPERATURE"
    )
    private String gsahDepartureTemperature;
    @Column(
            name = "GSAH_ARRIVE_DATE"
    )
    private String gsahArriveDate;
    @Column(
            name = "GSAH_ARRIVE_TIME"
    )
    private String gsahArriveTime;
    @Column(
            name = "GSAH_ARRIVE_TEMPERATURE"
    )
    private String gsahArriveTemperature;
    @Column(
            name = "GSAH_TRANSPORT_ORGANIZATION"
    )
    private String gsahTransportOrganization;
    @Column(
            name = "GSAH_TRANSPORT_MODE"
    )
    private String gsahTransportMode;
    @Column(
            name = "GSAH_FROM"
    )
    private String gsahFrom;
    @Column(
            name = "GSAH_TO"
    )
    private String gsahTo;
    @Column(
            name = "GSAH_TYPE"
    )
    private String gsahType;
    @Column(
            name = "GSAH_STATUS"
    )
    private String gsahStatus;
    @Column(
            name = "GSAH_TOTAL_AMT"
    )
    private BigDecimal gsahTotalAmt;
    @Column(
            name = "GSAH_TOTAL_QTY"
    )
    private String gsahTotalQty;
    @Column(
            name = "GSAH_EMP"
    )
    private String gsahEmp;
    @Column(
            name = "GSAH_REMAKS"
    )
    private String gsahRemaks;
    @Column(
            name = "GSAH_ACCEPT_TYPE"
    )
    private String gsahAcceptType;
    @Column(
            name = "GSAH_COLDCHAIN_FLAG"
    )
    private String gsahColdchainFlag;
    @Column(
            name = "GSAH_BOX_QTY"
    )
    private BigDecimal gsahBoxQty;
    @Column(
            name = "GSAH_INSULATION_MODE"
    )
    private String gsahInsulationMode;
    @Column(
            name = "GSAH_TX_FOLLOW"
    )
    private String gsahTxFollow;
    @Column(
            name = "GSAH_TEMP_FIT"
    )
    private String gsahTempFit;
    @Column(
            name = "GSAH_TRANSPORT_EMP"
    )
    private String gsahTransportEmp;

    @Column(
            name = "GSAH_BUSINESS_EMP"
    )
    private String gsahBusinessEmp;

    @Column(
            name = "GSAH_TRANSPORT_CARDID"
    )
    private  String  gsahTransportCardId;

    @Column(
            name = "GSAH_TRANSPORT_TOOL"
    )
    private  String  gsahTransportTool;

    @Column(
            name = "GSAH_TRANSPORT_TIME"
    )
    private  String  gsahTransportTime;

    @Column(
            name = "GSAH_TEMP_TYPE"
    )
    private  String  gsahTempType;

    private static final long serialVersionUID = 1L;
}
