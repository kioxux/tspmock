package com.gys.business.mapper.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 重点商品任务—主表(GaiaKeyCommodityTaskBill)实体类
 *
 * @author makejava
 * @since 2021-09-01 15:37:53
 */
public class GaiaKeyCommodityTaskBill implements Serializable {
    private static final long serialVersionUID = -93074848282309802L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 任务单号
     */
    private String billCode;
    /**
     * 任务名称
     */
    private String billName;
    /**
     * 开始日期
     */
    private Date startTime;
    /**
     * 结束日期
     */
    private Date endTime;
    /**
     * 任务天数
     */
    private Integer validQty;
    /**
     * 任务门店数
     */
    private Integer storeQty;
    /**
     * 销售量
     */
    private Double salesQty;
    /**
     * 销售额
     */
    private Double salesAmt;
    /**
     * 单据状态：1-已保存 1-已审核 2-停用
     */
    private Integer status;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;
    private String startTimeStr;
    private String endTimeStr;

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getBillName() {
        return billName;
    }

    public void setBillName(String billName) {
        this.billName = billName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getValidQty() {
        return validQty;
    }

    public void setValidQty(Integer validQty) {
        this.validQty = validQty;
    }

    public Integer getStoreQty() {
        return storeQty;
    }

    public void setStoreQty(Integer storeQty) {
        this.storeQty = storeQty;
    }

    public Double getSalesQty() {
        return salesQty;
    }

    public void setSalesQty(Double salesQty) {
        this.salesQty = salesQty;
    }

    public Double getSalesAmt() {
        return salesAmt;
    }

    public void setSalesAmt(Double salesAmt) {
        this.salesAmt = salesAmt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}
