package com.gys.business.mapper;

import com.gys.business.service.data.PointsDetailsQueryInData;
import com.gys.business.service.data.PointsDetailsQueryOutData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author xiaoyuan
 */
@Mapper
public interface GaiaPointsDetailsQueryMapper {
    /**
     * 根据会员id 会员名称 时间查询
     * @param queryInData
     * @return
     */
    List<PointsDetailsQueryOutData> selectPointsDetailsQueryByNameAndIdAndDate(PointsDetailsQueryInData queryInData);
}
