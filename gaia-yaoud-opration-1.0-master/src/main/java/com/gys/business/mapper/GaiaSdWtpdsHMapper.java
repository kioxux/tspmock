package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdWtpdsH;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/14 19:20
 */
public interface GaiaSdWtpdsHMapper extends BaseMapper<GaiaSdWtpdsH> {

    GaiaSdWtpdsH selectByClientAndVoucherAndDsfBrId(@Param("client") String client,@Param("voucherId") String voucherId,@Param("dsfBrId") String dsfBrId);

    void updateAMT(@Param("client") String client,@Param("voucherId") String voucherId,@Param("dsfBrId") String dsfBrId);
}
