package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaPaymentApplications;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-26
 */
@Mapper
public interface GaiaPaymentApplicationsMapper extends BaseMapper<GaiaPaymentApplications> {
    List<Map<String,Object>> selectprepaidDocuments(@Param("gspApprovalDateStart") String startDate, @Param("gspApprovalDateEnd")String  endDate, @Param("client")String client, @Param("supCode")String supSelfCode);
}
