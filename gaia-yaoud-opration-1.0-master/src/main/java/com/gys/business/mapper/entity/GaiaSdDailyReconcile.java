package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "GAIA_SD_PAY_DAYREPORT_H")
public class GaiaSdDailyReconcile implements Serializable {

    private static final long serialVersionUID = 5862981376139007004L;

    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSPDH_VOUCHER_ID")
    private String gpdhVoucherId;

    @Column(name = "GSPDH_BR_ID")
    private String gpdhBrId;

    @Column(name = "GSPDH_SALE_DATE")
    private String gpdhSaleDate;

    @Column(name = "GSPDH_CHECK_DATE")
    private String gpdhCheckDate;

    @Column(name = "GSPDH_CHECK_TIME")
    private String gpdhCheckTime;

    @Column(name = "GSPDH_EMP")
    private String gpdhEmp;

    @Column(name = "GSPDHTOTAL_SALES_AMT")
    private String gpdhtotalSalesAmt;

    @Column(name = "GSPDHTOTAL_INPUT_AMT")
    private String gpdhtotalInputAmt;

    @Column(name = "GSPDH_STATUS")
    private String gpdhStatus;

    @Column(name = "GSPDHTOTAL_CARD_AMT")
    private String gspdhtotalCardAmt;

    @Column(name = "GSPDHTOTAL_DG_AMT")
    private String gspdhtotalDgAmt;


}
