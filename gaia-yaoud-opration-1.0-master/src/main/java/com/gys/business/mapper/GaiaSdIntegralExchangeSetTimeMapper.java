package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralExchangeSetTime;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author RULAISZ
* @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_TIME(门店积分换购商品设置时间段)】的数据库操作Mapper
* @createDate 2021-11-29 09:49:26
* @Entity com.gys.business.mapper.entity.GaiaSdIntegralExchangeSetTime
*/
public interface GaiaSdIntegralExchangeSetTimeMapper {


    List<GaiaSdIntegralExchangeSetTime> getByClientAndVoucherId(@Param("client") String client,@Param("voucherId") String voucherId);

    void batchInsert(@Param("times") List<GaiaSdIntegralExchangeSetTime> times);

    void deleteByClientAndVoucherId(@Param("client") String client,@Param("voucherId") String voucherId);
    List<GaiaSdIntegralExchangeSetTime> getEffectiveTime(@Param("client") String client, @Param("voucherId") String voucherId);

    List<GaiaSdIntegralExchangeSetTime> getAllByClient(@Param("client") String client);
}
