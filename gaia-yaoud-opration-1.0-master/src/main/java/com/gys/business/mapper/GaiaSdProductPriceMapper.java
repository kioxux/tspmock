package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralRateSto;
import com.gys.business.mapper.entity.GaiaSdProductPrice;
import com.gys.business.service.data.CommodityCostDto;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdProductPriceMapper extends BaseMapper<GaiaSdProductPrice> {

    List<GaiaSdProductPrice> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);

    CommodityCostDto commodityCost(Map<String, String> map);

    List<GaiaSdProductPrice> getListByClientAndBrId(@Param("client") String client, @Param("depId") String depId);



    List<GaiaSdIntegralRateSto>  selectList(@Param("client")String client, @Param("proIds") List<String> proId);
}
