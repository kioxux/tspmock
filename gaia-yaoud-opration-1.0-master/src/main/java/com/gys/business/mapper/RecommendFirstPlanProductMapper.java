package com.gys.business.mapper;

import com.gys.business.mapper.entity.RecommendFirstPlanProduct;
import com.gys.business.mapper.entity.RecommendFirstPlanProductRes;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 首推商品明细表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-08-04
 */
@Mapper
public interface RecommendFirstPlanProductMapper extends BaseMapper<RecommendFirstPlanProduct> {
    List<RecommendFirstPlanProductRes> selectProductInfo(@Param( value = "client") String client,@Param( value = "content") String content,@Param( value = "proIds") List<String> proIds);
}
