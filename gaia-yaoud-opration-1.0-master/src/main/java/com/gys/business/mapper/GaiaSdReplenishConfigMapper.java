package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdReplenishConfig;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 门店补货参数 Mapper 接口
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-17
 */
@Mapper
public interface GaiaSdReplenishConfigMapper extends BaseMapper<GaiaSdReplenishConfig> {

    void batchReplenishConfig(List<GaiaSdReplenishConfig> list);

}
