package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_MATERIAL_DOC_LOG_H")
@Data
public class GaiaMaterialDocLogH implements Serializable {
    /**
     * 主键ID
     */
    @Id
    @Column(name = "GUID")
    private String guid;

    /**
     * 时间
     */
    @Column(name = "GMDLH_TIME")
    private String gmdlhTime;

    /**
     * 是否成功，0：未成功，1：成功
     */
    @Column(name = "GMDLH_SUCESS")
    private Integer gmdlhSucess;

    /**
     * 内容
     */
    @Column(name = "GMDLH_CONTENT")
    private String gmdlhContent;

    private static final long serialVersionUID = 1L;

}