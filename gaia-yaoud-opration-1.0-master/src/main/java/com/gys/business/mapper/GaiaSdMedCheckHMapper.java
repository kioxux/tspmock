//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMedCheckH;
import com.gys.business.service.data.GetMedCheckInData;
import com.gys.business.service.data.GetMedCheckOutData;
import com.gys.business.service.data.UserInData;
import com.gys.business.service.data.UserOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdMedCheckHMapper extends BaseMapper<GaiaSdMedCheckH> {
    List<GetMedCheckOutData> selectMedCheckList(GetMedCheckInData inData);

    String selectNextVoucherId(@Param("client") String client, @Param("codePre") String codePre);

    List<UserOutData> selectPharmacistByBrId(UserInData inData);

}
