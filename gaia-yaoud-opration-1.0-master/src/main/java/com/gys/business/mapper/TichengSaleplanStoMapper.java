package com.gys.business.mapper;

import com.gys.business.mapper.entity.TichengSaleplanSto;
import com.gys.business.service.data.percentageplan.PercentageStoInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 销售提成方案门店表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-11-15
 */
@Mapper
public interface TichengSaleplanStoMapper extends BaseMapper<TichengSaleplanSto> {

    void deleteStoreByPid(@Param("pid") Integer pid);

    void addPlanStores(List<PercentageStoInData> list);

    List<Map<String,String>> selectTichengSaleStoList(@Param("tichengId") Long planId);

    List<Map<String,String>> selectSaleStoList(@Param("tichengId") Long planId);
}
