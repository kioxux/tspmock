package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaTichengProPlan;
import com.gys.business.mapper.entity.GaiaTichengProplanZN;
import com.gys.business.mapper.entity.GaiaTichengSalePlan;
import com.gys.business.service.data.percentageplan.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PercentagePlanV3Mapper {
    List<PercentageOutData> selectTichengZList(PercentageInData inData);

    int checkStartDate(PercentageInData inData);

    int checkEndDate(PercentageInData inData);

    int checkEndDate(PercentageInTrialData inData);

    void updateTichengSaleZ(GaiaTichengSalePlan salePlan);

    String selectNextPlanCode(String clientId);

    Long addPlanMain(GaiaTichengSalePlan inData);

    void deleteStoreByPid(@Param("pid") Integer pid);

    void deleteSalePlanByPid(@Param("pid") Integer tichengId);

    void deleteProPlanByPid(@Param("pid") Integer tichengId);

    void batchSaleTichengList(List<PercentageSaleInData> list);

    void batchSaleRejectProList(List<PercentageProInData> list);

    void addPlanStores(List<PercentageStoInData> list);

    void batchPlanRejectClass(List<PlanRejectClass> list);

    void deleteClassByPid(@Param("pid") Integer pid);

    int checkProStartDate(PercentageInData inData);

    int checkProEndDate(PercentageInData inData);

    int checkProEndDate(PercentageInTrialData inData);

    String selectNextProPlanCode(String clientId);

    Long addPlanProMain(GaiaTichengProplanZN inData);

    void updateTichengProZ(GaiaTichengProplanZN proPlan);

    void deleteProStoreByPid(@Param("pid") Integer pid);

    void addPlanProStores(List<PercentageStoInData> list);

    void batchProTichengList(List<PercentageProInData> list);

//    List<PercentageOutData> selectTichengZList(PercentageInData inData);

    List<Map<String,String>> selectTichengSaleStoList(@Param("tichengId") Long planId);

    PercentageInData selectTichengZSaleDetail(@Param("tichengId") Long planId);

    List<PercentageSaleInData> selectTichengSaleList(@Param("tichengId") Long planId);

    List<PercentageProInData> selectTichengProList(@Param("tichengId") Long planId);

    List<Map<String,String>> selectRejectClass(@Param("tichengId") Long planId);

    List<PercentageProInData> selectRejectProList(@Param("tichengId") Long planId);

    List<Map<String,String>> selectTichengProStoList(@Param("tichengId") Long planId);

    PercentageInData selectTichengZProDetail(@Param("tichengId") Long planId);

    void deleteRejectProByPid(@Param("pid") Integer pid);

    void approveSalePlan(@Param("id") Long planId,@Param("planStatus") String planStatus,@Param("planReason") String planReason);

    void approveProPlan(@Param("id") Long planId,@Param("planStatus") String planStatus,@Param("planReason") String planReason);

    void deleteSalePlan(@Param("id") Long planId);

    void deleteProPlan(@Param("id") Long planId);

    PercentageProInData selectProductByClient(@Param("client") String clientId,@Param("proCode") String proCode);

    String selectMovPriceByDcCode(@Param("client") String clientId,@Param("dcCode") String dcCode,@Param("proCode") String proCode);

    List<Map<String,String>> selectMovPriceByDcCodeProCodes(@Param("client") String clientId,@Param("dcCode") String dcCode,@Param("proCodes") List<String> proCodes);

    List<PercentageProInData> selectProductByProCodes(ImportProInData inData);

    List<Map<String,String>>selectMovPriceListByDcCode(ImportProInData inData);

    List<Map<String,String>> selectSaleStoList(@Param("tichengId") Long planId);

    List<Map<String,String>> selectProStoList(@Param("tichengId") Long planId);

    List<String> selectUserIdList(PercentageInData inData);

    void stopSalePlan(@Param("id") Long planId,@Param("planStatus") String planStatus,@Param("planStopDate") String planStopDate,@Param("planReason") String planReason);

    void stopProPlan(@Param("id") Long planId,@Param("planStatus") String planStatus,@Param("planStopDate") String planStopDate,@Param("planReason") String planReason);

    List<PercentageOutData> selectPlanStopList();

    List<Map<String,Object>> selectProPlanDetailByClientAndTime(@Param("client")String client,@Param("startDate")String startDate,@Param("endDate")String endDate);

    List<PercentageProInData> selectProductByClientProCodes(String client, List<String> proCodes);
}
