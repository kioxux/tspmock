package com.gys.business.mapper.entity;

import cn.hutool.core.collection.CollUtil;
import com.gys.util.ysb.YsbDrugInfo;
import com.gys.util.ysb.YsbOrderDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * GAIA_SO_BATCH_H
 *
 * @author
 */
@Data
@NoArgsConstructor
public class GaiaSoBatchH implements Serializable {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商")
    private String client;

    /**
     * 订单编码
     */
    @ApiModelProperty(value = "订单编码")
    private String soOrderid;

    /**
     * 下单时间
     */
    @ApiModelProperty(value = "下单时间")
    private String soOrdertime;

    /**
     * 客户ID
     */
    @ApiModelProperty(value = "客户ID")
    private String soCustomerid;

    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String soDrugstorename;

    /**
     * 订单总金额
     */
    @ApiModelProperty(value = "订单总金额")
    private BigDecimal soTotalcost;

    /**
     * 批发订单ID
     */
    @ApiModelProperty(value = "批发订单ID")
    private String soBatchOrderId;

    /**
     * 发票类型
     */
    @ApiModelProperty(value = "发票类型")
    private String soInvoicetype;

    /**
     * 付款时间
     */
    @ApiModelProperty(value = "付款时间")
    private String soNewpaytime;

    /**
     * 优惠金额
     */
    @ApiModelProperty(value = "优惠金额")
    private BigDecimal soCouponpayprov;

    /**
     * 收货人信息
     */
    @ApiModelProperty(value = "收货人信息")
    private String soShipinfo;

    /**
     * 省份
     */
    @ApiModelProperty(value = "省份")
    private String soProvincename;

    /**
     * 城市
     */
    @ApiModelProperty(value = "城市")
    private String soCityname;

    /**
     * 区域
     */
    @ApiModelProperty(value = "区域")
    private String soDistrictname;

    /**
     * 配送地址
     */
    @ApiModelProperty(value = "配送地址")
    private String soAddress;

    /**
     * 运费
     */
    @ApiModelProperty(value = "运费")
    private BigDecimal soDeliverfee;

    /**
     * 配送商家
     */
    @ApiModelProperty(value = "配送商家")
    private String soProvidename;

    /**
     * 委托商商家名称
     */
    @ApiModelProperty(value = "委托商商家名称")
    private String soDrugprovidename;

    /**
     * 税码登记证号
     */
    @ApiModelProperty(value = "税码登记证号")
    private String soTaxno;

    /**
     * 注册地址
     */
    @ApiModelProperty(value = "注册地址")
    private String soRegaddress;

    /**
     * 注册电话
     */
    @ApiModelProperty(value = "注册电话")
    private String soRegphone;

    /**
     * 开户行
     */
    @ApiModelProperty(value = "开户行")
    private String soBankname;

    /**
     * 开户行账号
     */
    @ApiModelProperty(value = "开户行账号")
    private String soBankcardno;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String soRemark;

    /**
     * 客户留言信息和资质需求信息
     */
    @ApiModelProperty(value = "客户留言信息和资质需求信息")
    private String soOriginalremark;

    /**
     * 订单状态：1-正在开单2-拣货完成，待配送 3-订单交与拣货,分捡中4正在配送 5客户已签收，配送完成（药师帮定义的状态）6 客户不存在 7 客户首营审批中 8 客户首营成功 9 创建成功。
     */
    @ApiModelProperty(value = "订单状态")
    private String soStatus;

    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    private String soCreDate;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private String soCreTime;

    /**
     * 创建人账号
     */
    @ApiModelProperty(value = "创建人账号")
    private String soCreId;

    /**
     * 修改日期
     */
    @ApiModelProperty(value = "修改日期")
    private String soModiDate;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private String soModiTime;

    /**
     * 修改人账号
     */
    @ApiModelProperty(value = "修改人账号")
    private String soModiId;

    @Transient
    private List<GaiaSoBatchD> batchDList;

    @Transient
    @ApiModelProperty(value = "订单状态")
    private String orderStatus;
    @Transient
    @ApiModelProperty(value = "发票类型")
    private String invoiceType;

    public GaiaSoBatchH(YsbOrderDto dto) {
        this.soOrderid = dto.getOrderId();
        this.soOrdertime = dto.getOrderTime();
        this.soCustomerid = dto.getCustomerId();
        this.soDrugstorename = dto.getDrugstoreName();
        this.soTotalcost = new BigDecimal(dto.getTotalCost());
        this.soInvoicetype = dto.getInvoiceType();
        this.soNewpaytime = dto.getNewPayTime();
        this.soCouponpayprov = new BigDecimal(dto.getCouponPayProv());
        this.soShipinfo = dto.getShipInfo();
        this.soProvincename = dto.getProvinceName();
        this.soCityname = dto.getCityName();
        this.soDistrictname = dto.getDistrictName();
        this.soAddress = dto.getAddress();
        this.soDeliverfee = new BigDecimal(dto.getDeliverFee());
        this.soProvidename = dto.getProviderName();
        this.soDrugprovidename = dto.getDrugProviderName();
        this.soRegaddress = dto.getRegAddress();
        this.soRegphone = dto.getRegPhone();
        this.soBankname = dto.getBankName();
        this.soBankcardno = dto.getBankCardNo();
        this.soRemark = dto.getRemark();
        this.soOriginalremark = dto.getOriginalRemark();
        this.client = dto.getClient();

        List<YsbDrugInfo> drugInfo = dto.getDrugInfo();
        if (CollUtil.isNotEmpty(drugInfo)) {
            List<GaiaSoBatchD> batchDList = drugInfo.stream().map(GaiaSoBatchD::new).collect(Collectors.toList());
            batchDList.forEach(r -> {
                r.setSoOrderid(dto.getOrderId());
                r.setClient(dto.getClient());
            });
            this.batchDList = batchDList;
        }
    }
}