//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_DEPOSIT_H"
)
public class GaiaSdCashPayment implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSDH_VOUCHER_ID"
    )
    private String gsdhVoucherId;
    @Column(
            name = "GSDH_BR_ID"
    )
    private String gsdhBrId;
    @Column(
            name = "GSDH_CHECK_DATE"
    )
    private String gsdhCheckDate;
    @Column(
            name = "GSDH_DEPOSIT_ID"
    )
    private String gsdhDepositId;
    @Column(
            name = "GSDH_BANK_ID"
    )
    private String gsdhBankId;
    @Column(
            name = "GSDH_BANK_ACCOUNT"
    )
    private String gsdhBankAccount;
    @Column(
            name = "GSDH_DEPOSIT_DATE"
    )
    private String gsdhDepositDate;
    @Column(
            name = "GSDH_DEPOSIT_AMT"
    )
    private BigDecimal gsdhDepositAmt;
    @Column(
            name = "GSDH_STATUS"
    )
    private String gsdhStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdCashPayment() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsdhVoucherId() {
        return this.gsdhVoucherId;
    }

    public void setGsdhVoucherId(String gsdhVoucherId) {
        this.gsdhVoucherId = gsdhVoucherId;
    }

    public String getGsdhBrId() {
        return this.gsdhBrId;
    }

    public void setGsdhBrId(String gsdhBrId) {
        this.gsdhBrId = gsdhBrId;
    }

    public String getGsdhCheckDate() {
        return this.gsdhCheckDate;
    }

    public void setGsdhCheckDate(String gsdhCheckDate) {
        this.gsdhCheckDate = gsdhCheckDate;
    }

    public String getGsdhDepositId() {
        return this.gsdhDepositId;
    }

    public void setGsdhDepositId(String gsdhDepositId) {
        this.gsdhDepositId = gsdhDepositId;
    }

    public String getGsdhBankId() {
        return this.gsdhBankId;
    }

    public void setGsdhBankId(String gsdhBankId) {
        this.gsdhBankId = gsdhBankId;
    }

    public String getGsdhBankAccount() {
        return this.gsdhBankAccount;
    }

    public void setGsdhBankAccount(String gsdhBankAccount) {
        this.gsdhBankAccount = gsdhBankAccount;
    }

    public String getGsdhDepositDate() {
        return this.gsdhDepositDate;
    }

    public void setGsdhDepositDate(String gsdhDepositDate) {
        this.gsdhDepositDate = gsdhDepositDate;
    }

    public BigDecimal getGsdhDepositAmt() {
        return this.gsdhDepositAmt;
    }

    public void setGsdhDepositAmt(BigDecimal gsdhDepositAmt) {
        this.gsdhDepositAmt = gsdhDepositAmt;
    }

    public String getGsdhStatus() {
        return this.gsdhStatus;
    }

    public void setGsdhStatus(String gsdhStatus) {
        this.gsdhStatus = gsdhStatus;
    }
}
