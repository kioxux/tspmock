package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 组合类促销赠送商品设置表
 */
@Table(name = "GAIA_SD_PROM_ASSO_RESULT")
@Data
public class GaiaSdPromAssoResult implements Serializable {
    private static final long serialVersionUID = -5192544637061381078L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPAR_VOUCHER_ID")
    private String gsparVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPAR_SERIAL")
    private String gsparSerial;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSPAR_PRO_ID")
    private String gsparProId;

    /**
     * 系列编码
     */
    @Column(name = "GSPAR_SERIES_ID")
    private String gsparSeriesId;

    /**
     * 赠品单品价格
     */
    @Column(name = "GSPAR_GIFT_PRC")
    private BigDecimal gsparGiftPrc;

    /**
     * 赠品单品折扣
     */
    @Column(name = "GSPAR_GIFT_REBATE")
    private String gsparGiftRebate;

}
