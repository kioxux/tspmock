package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromGiftConds;
import com.gys.business.mapper.entity.GaiaSdPromHead;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromHeadMapper extends BaseMapper<GaiaSdPromHead> {
    String selectMaxId(@Param("client") String client);

    List<PromoteOutData> getList(PromoteInData inData);

    List<GetPromByProCodeOutData> promByProCode(GetQueryProductInData inData);

    List<GetPromByProCodeOutData> promCombinByProCode(GetQueryProductInData inData);

    List<GetPromByProCodeOutData> giveSelect(GetQueryProductInData inData);

    List<GiftCardOutData> giftCardByProCode(GetQueryProductInData inData);

    List<GetPromByProCodeOutData> promByProCodeMember(GetQueryProductInData inData);

    List<GetPromByProCodeOutData> promByProCodeMemberDay(GetQueryProductInData inData);

    List<GetPromByProCodeOutData> promByProCodeMemberDayU(GetQueryProductInData inData);

    List<GetPromByProCodeOutData> selectSaleType(GetDisPriceInData inData);

    NumberOfPromotionalMethodsData findSingleProductPromotionByCode(PromotionMethodInData inData);

    /***
     * 查询出此商品所包含的所有促销活动
     * @param inData
     * @return
     */
    List<PromotionalConditionData> findProductPromotionByCode(PromotionMethodInData inData);

    List<PromotionalConditionData> findProductPromotionByCode2(PromotionMethodInData inData);

    List<GetDisPriceOutData> findPromotionById(@Param("inData") List<FindPromotionInData> inData);

    List<PromotionalConditionData> findProductPromotionByIdAndArgs(@Param("inData") List<FindPromotionInData> inData);

    void insertToList(List<GaiaSdPromHead> dataList);

    List<GaiaSdPromHead> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId, @Param("nowDate") String nowDate);

    /**
     * 查询当前门店赠品促销活动（不互斥）
     * @param inData
     * @return
     */
    List<PromotionalConditionData> findAllGiftPromotionByExclusion(PromotionMethodInData inData);

    /**
     * 查询当前活动的赠品列表
     * @param client
     * @param brId
     * @param voucherId
     * @return
     */
    List<GiftPromOutData> findAllGiftSendProductByExclusion(@Param("client")String client,@Param("brId")String brId,@Param("voucherId") String voucherId,@Param("seriesId") String seriesId);

    /**
     * 查询当前活动的条件列表
     * @param client
     * @param brId
     * @param voucherId
     * @return
     */
    List<GaiaSdPromGiftConds> findAllGiftConditionProductByExclusion(@Param("client")String client, @Param("brId")String brId, @Param("voucherId") String voucherId,@Param("seriesId") String seriesId,@Param("isMember") String isMember);

    /**
     * app会员促销查询
     * @param inData
     * @return
     */
    List<GetPromByProCodeOutData> promByProCodeMemberAll(GetQueryProductInData inData);

    /**
     * app 促销查询
     * @param inData
     * @return
     */
    List<PromotionalConditionData> findProductPromotionByAppCode(PromotionMethodInData inData);
}
