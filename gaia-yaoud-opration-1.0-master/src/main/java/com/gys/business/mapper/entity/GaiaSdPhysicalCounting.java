//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_PHYSICAL_COUNTING"
)
public class GaiaSdPhysicalCounting implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSPC_VOUCHER_ID"
    )
    private String gspcVoucherId;
    @Id
    @Column(
            name = "GSPC_BR_ID"
    )
    private String gspcBrId;
    @Id
    @Column(
            name = "GSPC_DATE"
    )
    private String gspcDate;
    @Id
    @Column(
            name = "GSPC_TYPE"
    )
    private String gspcType;
    @Id
    @Column(
            name = "GSPC_ROW_NO"
    )
    private String gspcRowNo;
    @Column(
            name = "GSPC_TIME"
    )
    private String gspcTime;
    @Column(
            name = "GSPC_PRO_ID"
    )
    private String gspcProId;
    @Column(
            name = "GSPC_BATCH_NO"
    )
    private String gspcBatchNo;
    @Column(
            name = "GSPC_STOCK_QTY"
    )
    private String gspcStockQty;
    @Column(
            name = "GSPC_QTY"
    )
    private String gspcQty;
    @Column(
            name = "GSPC_EXAMINE_DATE"
    )
    private String gspcExamineDate;
    @Column(
            name = "GSPC_EXAMINE_TIME"
    )
    private String gspcExamineTime;
    @Column(
            name = "GSPC_EXAMINE_EMP"
    )
    private String gspcExamineEmp;
    @Column(
            name = "GSPC_STATUS"
    )
    private String gspcStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdPhysicalCounting() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGspcVoucherId() {
        return this.gspcVoucherId;
    }

    public void setGspcVoucherId(String gspcVoucherId) {
        this.gspcVoucherId = gspcVoucherId;
    }

    public String getGspcBrId() {
        return this.gspcBrId;
    }

    public void setGspcBrId(String gspcBrId) {
        this.gspcBrId = gspcBrId;
    }

    public String getGspcDate() {
        return this.gspcDate;
    }

    public void setGspcDate(String gspcDate) {
        this.gspcDate = gspcDate;
    }

    public String getGspcType() {
        return this.gspcType;
    }

    public void setGspcType(String gspcType) {
        this.gspcType = gspcType;
    }

    public String getGspcRowNo() {
        return this.gspcRowNo;
    }

    public void setGspcRowNo(String gspcRowNo) {
        this.gspcRowNo = gspcRowNo;
    }

    public String getGspcTime() {
        return this.gspcTime;
    }

    public void setGspcTime(String gspcTime) {
        this.gspcTime = gspcTime;
    }

    public String getGspcProId() {
        return this.gspcProId;
    }

    public void setGspcProId(String gspcProId) {
        this.gspcProId = gspcProId;
    }

    public String getGspcBatchNo() {
        return this.gspcBatchNo;
    }

    public void setGspcBatchNo(String gspcBatchNo) {
        this.gspcBatchNo = gspcBatchNo;
    }

    public String getGspcStockQty() {
        return this.gspcStockQty;
    }

    public void setGspcStockQty(String gspcStockQty) {
        this.gspcStockQty = gspcStockQty;
    }

    public String getGspcQty() {
        return this.gspcQty;
    }

    public void setGspcQty(String gspcQty) {
        this.gspcQty = gspcQty;
    }

    public String getGspcExamineDate() {
        return this.gspcExamineDate;
    }

    public void setGspcExamineDate(String gspcExamineDate) {
        this.gspcExamineDate = gspcExamineDate;
    }

    public String getGspcExamineTime() {
        return this.gspcExamineTime;
    }

    public void setGspcExamineTime(String gspcExamineTime) {
        this.gspcExamineTime = gspcExamineTime;
    }

    public String getGspcExamineEmp() {
        return this.gspcExamineEmp;
    }

    public void setGspcExamineEmp(String gspcExamineEmp) {
        this.gspcExamineEmp = gspcExamineEmp;
    }

    public String getGspcStatus() {
        return this.gspcStatus;
    }

    public void setGspcStatus(String gspcStatus) {
        this.gspcStatus = gspcStatus;
    }
}
