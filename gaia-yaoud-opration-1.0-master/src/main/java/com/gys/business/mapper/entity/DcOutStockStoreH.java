package com.gys.business.mapper.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/25 14:46
 */
@Data
public class DcOutStockStoreH {
    private Long id;
    private String client;
    private String voucherId;
    private Integer storeNum;
    private Integer productNum;
    private BigDecimal monthSaleAmount;
    private BigDecimal monthProfitAmount;
    private Integer deleteFlag;
    private Date createTime;
}
