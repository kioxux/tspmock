package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSytAccount;
import com.gys.common.base.BaseMapper;

public interface GaiaSdSytAccountMapper extends BaseMapper<GaiaSdSytAccount> {
}