package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdEIMS;
import com.gys.business.yaojian.dto.InBoundRecordDTO;
import com.gys.business.yaojian.dto.InventoryRecordDTO;
import com.gys.business.yaojian.dto.OutboundRecordDTO;
import com.gys.business.yaojian.form.EIMSForm;
import com.gys.common.base.BaseMapper;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/19 15:35
 */
public interface GaiaSdEIMSMapper  extends BaseMapper<GaiaSdEIMS> {
    /**
     * 批量添加前一天的入库数据
     * @param client
     */
    void batchInsertInData(String client);

    /**
     * 批量添加库存数据
     * @param client
     */

    void batchInsertStockData(String client);

    /**
     * 批量更新库存信息，库存数量
     * @param client
     */

    void batchUpdateStockData(String client);

    /**
     * 批量添加前一天的出库数据
     * @param client
     */
    void batchInsertOutData(String client);

    List<GaiaSdEIMS> search(EIMSForm eimsForm);
    List<LinkedHashMap<String, Object>> export(EIMSForm eimsForm);

    List<InBoundRecordDTO> getInDataById(Integer id);
    List<InventoryRecordDTO> getStockDataById(Integer id);
    List<OutboundRecordDTO> getOutDataById(Integer id);

    void updateStatus(GaiaSdEIMS model);

    List<LinkedHashMap<String, Object>> getUpdateID();
}
