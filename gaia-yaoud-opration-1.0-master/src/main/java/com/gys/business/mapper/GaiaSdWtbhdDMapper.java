package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdReplenishD;
import com.gys.business.mapper.entity.GaiaSdWtbhdD;
import com.gys.business.mapper.entity.GaiaSdWtbhdDKey;
import com.gys.business.mapper.entity.GaiaSdWtbhdH;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/13 16:03
 **/
public interface GaiaSdWtbhdDMapper {

    GaiaSdWtbhdD selectByPrimaryKey(GaiaSdWtbhdDKey gaiaSdWtbhdDKey);

    int add(GaiaSdWtbhdD gaiaSdWtbhdD);

    int update(GaiaSdWtbhdD gaiaSdWtbhdD);

    List<GaiaSdWtbhdD> findList(GaiaSdWtbhdH wtbhdH);

    int getCurrentSerialNo(GaiaSdWtbhdD cond);
}
