//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdCashPaymentDetail;
import com.gys.business.service.data.CashPaymentDetailOutData;
import com.gys.business.service.data.CashPaymentOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaCashPaymentDetailMapper extends BaseMapper<GaiaSdCashPaymentDetail> {
    List<CashPaymentDetailOutData> getCashPaymentDetailList(CashPaymentOutData inData);
}
