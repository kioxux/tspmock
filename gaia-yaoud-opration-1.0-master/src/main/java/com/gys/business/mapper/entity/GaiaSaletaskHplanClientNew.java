package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SALETASK_HPLAN_CLIENT_NEW")
@Data
public class GaiaSaletaskHplanClientNew implements Serializable {
    /**
     * 客户ID
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 计划月份
     */
    @Id
    @Column(name = "MONTH_PLAN")
    private String monthPlan;

    /**
     * 任务ID
     */
    @Id
    @Column(name = "STASK_TASK_ID")
    private String staskTaskId;

    /**
     * 任务名称
     */
    @Column(name = "STASK_TASK_NAME")
    private String staskTaskName;

    /**
     * 起始日期
     */
    @Column(name = "STASK_START_DATE")
    private String staskStartDate;

    /**
     * 结束日期
     */
    @Column(name = "STASK_END_DATE")
    private String staskEndDate;

    /**
     * 同比月份
     */
    @Column(name = "YOY_MONTH")
    private String yoyMonth;

    /**
     * 环比月份
     */
    @Column(name = "MOM_MONTH")
    private String momMonth;

    /**
     * 期间天数
     */
    @Column(name = "MONTH_DAYS")
    private Integer monthDays;

    /**
     * 营业额同比增长率
     */
    @Column(name = "YOY_SALE_AMT")
    private BigDecimal yoySaleAmt;

    /**
     * 营业额环比增长率
     */
    @Column(name = "MOM_SALE_AMT")
    private BigDecimal momSaleAmt;

    /**
     * 毛利额同比增长率
     */
    @Column(name = "YOY_SALE_GROSS")
    private BigDecimal yoySaleGross;

    /**
     * 毛利额环比增长率
     */
    @Column(name = "MOM_SALE_GROSS")
    private BigDecimal momSaleGross;

    /**
     * 会员卡同比增长率
     */
    @Column(name = "YOY_MCARD_QTY")
    private BigDecimal yoyMcardQty;

    /**
     * 会员卡环比增长率
     */
    @Column(name = "MOM_MCARD_QTY")
    private BigDecimal momMcardQty;

    /**
     * 任务状态(P待处理；S已保存；A：已审批；D已停用；I已忽略)
     */
    @Column(name = "STASK_SPLAN_STA")
    private String staskSplanSta;

    /**
     * 创建人账号
     */
    @Column(name = "STASK_CRE_ID")
    private String staskCreId;

    /**
     * 创建日期
     */
    @Column(name = "STASK_CRE_DATE")
    private String staskCreDate;

    /**
     * 创建时间
     */
    @Column(name = "STASK_CRE_TIME")
    private String staskCreTime;

    /**
     * 修改人
     */
    @Column(name = "STASK_MD_ID")
    private String staskMdId;

    /**
     * 修改日期
     */
    @Column(name = "STASK_MD_DATE")
    private String staskMdDate;

    /**
     * 修改时间
     */
    @Column(name = "STASK_MD_TIME")
    private String staskMdTime;

    /**
     * 删除 0 未删除 1已删除
     */
    @Column(name = "STASK_TYPE")
    private String staskType;

    /**
     * 营业额
     */
    @Column(name = "SALE_AMT")
    private BigDecimal saleAmt;

    /**
     * 毛利额
     */
    @Column(name = "SALE_GROSS")
    private BigDecimal saleGross;

    /**
     * 会员卡
     */
    @Column(name = "MCARD_QTY")
    private BigDecimal mcardQty;
    /**
     * 门店总数
     */
    @Column(name = "STO_QTY")
    private int stoQty;

    @Column(name = "STASK_APPROVED_ID")
    private String staskApprovedId;//审核人id
    @Column(name = "STASK_APPROVED_DATE")
    private String staskApprovedDate;//审核日期
    @Column(name = "STASK_APPROVED_TIME")
    private String staskApprovedTime;//审核时间


    private static final long serialVersionUID = 1L;

    /**
     * 获取客户ID
     *
     * @return CLIENT - 客户ID
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置客户ID
     *
     * @param client 客户ID
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取计划月份
     *
     * @return MONTH_PLAN - 计划月份
     */
    public String getMonthPlan() {
        return monthPlan;
    }

    /**
     * 设置计划月份
     *
     * @param monthPlan 计划月份
     */
    public void setMonthPlan(String monthPlan) {
        this.monthPlan = monthPlan;
    }

    /**
     * 获取任务ID
     *
     * @return STASK_TASK_ID - 任务ID
     */
    public String getStaskTaskId() {
        return staskTaskId;
    }

    /**
     * 设置任务ID
     *
     * @param staskTaskId 任务ID
     */
    public void setStaskTaskId(String staskTaskId) {
        this.staskTaskId = staskTaskId;
    }

    /**
     * 获取任务名称
     *
     * @return STASK_TASK_NAME - 任务名称
     */
    public String getStaskTaskName() {
        return staskTaskName;
    }

    /**
     * 设置任务名称
     *
     * @param staskTaskName 任务名称
     */
    public void setStaskTaskName(String staskTaskName) {
        this.staskTaskName = staskTaskName;
    }

    /**
     * 获取起始日期
     *
     * @return STASK_START_DATE - 起始日期
     */
    public String getStaskStartDate() {
        return staskStartDate;
    }

    /**
     * 设置起始日期
     *
     * @param staskStartDate 起始日期
     */
    public void setStaskStartDate(String staskStartDate) {
        this.staskStartDate = staskStartDate;
    }

    /**
     * 获取结束日期
     *
     * @return STASK_END_DATE - 结束日期
     */
    public String getStaskEndDate() {
        return staskEndDate;
    }

    /**
     * 设置结束日期
     *
     * @param staskEndDate 结束日期
     */
    public void setStaskEndDate(String staskEndDate) {
        this.staskEndDate = staskEndDate;
    }

    /**
     * 获取同比月份
     *
     * @return YOY_MONTH - 同比月份
     */
    public String getYoyMonth() {
        return yoyMonth;
    }

    /**
     * 设置同比月份
     *
     * @param yoyMonth 同比月份
     */
    public void setYoyMonth(String yoyMonth) {
        this.yoyMonth = yoyMonth;
    }

    /**
     * 获取环比月份
     *
     * @return MOM_MONTH - 环比月份
     */
    public String getMomMonth() {
        return momMonth;
    }

    /**
     * 设置环比月份
     *
     * @param momMonth 环比月份
     */
    public void setMomMonth(String momMonth) {
        this.momMonth = momMonth;
    }

    /**
     * 获取期间天数
     *
     * @return MONTH_DAYS - 期间天数
     */
    public Integer getMonthDays() {
        return monthDays;
    }

    /**
     * 设置期间天数
     *
     * @param monthDays 期间天数
     */
    public void setMonthDays(Integer monthDays) {
        this.monthDays = monthDays;
    }

    /**
     * 获取营业额同比增长率
     *
     * @return YOY_SALE_AMT - 营业额同比增长率
     */
    public BigDecimal getYoySaleAmt() {
        return yoySaleAmt;
    }

    /**
     * 设置营业额同比增长率
     *
     * @param yoySaleAmt 营业额同比增长率
     */
    public void setYoySaleAmt(BigDecimal yoySaleAmt) {
        this.yoySaleAmt = yoySaleAmt;
    }

    /**
     * 获取营业额环比增长率
     *
     * @return MOM_SALE_AMT - 营业额环比增长率
     */
    public BigDecimal getMomSaleAmt() {
        return momSaleAmt;
    }

    /**
     * 设置营业额环比增长率
     *
     * @param momSaleAmt 营业额环比增长率
     */
    public void setMomSaleAmt(BigDecimal momSaleAmt) {
        this.momSaleAmt = momSaleAmt;
    }

    /**
     * 获取毛利额同比增长率
     *
     * @return YOY_SALE_GROSS - 毛利额同比增长率
     */
    public BigDecimal getYoySaleGross() {
        return yoySaleGross;
    }

    /**
     * 设置毛利额同比增长率
     *
     * @param yoySaleGross 毛利额同比增长率
     */
    public void setYoySaleGross(BigDecimal yoySaleGross) {
        this.yoySaleGross = yoySaleGross;
    }

    /**
     * 获取毛利额环比增长率
     *
     * @return MOM_SALE_GROSS - 毛利额环比增长率
     */
    public BigDecimal getMomSaleGross() {
        return momSaleGross;
    }

    /**
     * 设置毛利额环比增长率
     *
     * @param momSaleGross 毛利额环比增长率
     */
    public void setMomSaleGross(BigDecimal momSaleGross) {
        this.momSaleGross = momSaleGross;
    }

    /**
     * 获取会员卡同比增长率
     *
     * @return YOY_MCARD_QTY - 会员卡同比增长率
     */
    public BigDecimal getYoyMcardQty() {
        return yoyMcardQty;
    }

    /**
     * 设置会员卡同比增长率
     *
     * @param yoyMcardQty 会员卡同比增长率
     */
    public void setYoyMcardQty(BigDecimal yoyMcardQty) {
        this.yoyMcardQty = yoyMcardQty;
    }

    /**
     * 获取会员卡环比增长率
     *
     * @return MOM_MCARD_QTY - 会员卡环比增长率
     */
    public BigDecimal getMomMcardQty() {
        return momMcardQty;
    }

    /**
     * 设置会员卡环比增长率
     *
     * @param momMcardQty 会员卡环比增长率
     */
    public void setMomMcardQty(BigDecimal momMcardQty) {
        this.momMcardQty = momMcardQty;
    }

    /**
     * 获取任务状态(P待处理；S已保存；A：已审批；D已停用；I已忽略)
     *
     * @return STASK_SPLAN_STA - 任务状态(P待处理；S已保存；A：已审批；D已停用；I已忽略)
     */
    public String getStaskSplanSta() {
        return staskSplanSta;
    }

    /**
     * 设置任务状态(P待处理；S已保存；A：已审批；D已停用；I已忽略)
     *
     * @param staskSplanSta 任务状态(P待处理；S已保存；A：已审批；D已停用；I已忽略)
     */
    public void setStaskSplanSta(String staskSplanSta) {
        this.staskSplanSta = staskSplanSta;
    }

    /**
     * 获取创建人账号
     *
     * @return STASK_CRE_ID - 创建人账号
     */
    public String getStaskCreId() {
        return staskCreId;
    }

    /**
     * 设置创建人账号
     *
     * @param staskCreId 创建人账号
     */
    public void setStaskCreId(String staskCreId) {
        this.staskCreId = staskCreId;
    }

    /**
     * 获取创建日期
     *
     * @return STASK_CRE_DATE - 创建日期
     */
    public String getStaskCreDate() {
        return staskCreDate;
    }

    /**
     * 设置创建日期
     *
     * @param staskCreDate 创建日期
     */
    public void setStaskCreDate(String staskCreDate) {
        this.staskCreDate = staskCreDate;
    }

    /**
     * 获取创建时间
     *
     * @return STASK_CRE_TIME - 创建时间
     */
    public String getStaskCreTime() {
        return staskCreTime;
    }

    /**
     * 设置创建时间
     *
     * @param staskCreTime 创建时间
     */
    public void setStaskCreTime(String staskCreTime) {
        this.staskCreTime = staskCreTime;
    }

    /**
     * 获取修改人
     *
     * @return STASK_MD_ID - 修改人
     */
    public String getStaskMdId() {
        return staskMdId;
    }

    /**
     * 设置修改人
     *
     * @param staskMdId 修改人
     */
    public void setStaskMdId(String staskMdId) {
        this.staskMdId = staskMdId;
    }

    /**
     * 获取修改日期
     *
     * @return STASK_MD_DATE - 修改日期
     */
    public String getStaskMdDate() {
        return staskMdDate;
    }

    /**
     * 设置修改日期
     *
     * @param staskMdDate 修改日期
     */
    public void setStaskMdDate(String staskMdDate) {
        this.staskMdDate = staskMdDate;
    }

    /**
     * 获取修改时间
     *
     * @return STASK_MD_TIME - 修改时间
     */
    public String getStaskMdTime() {
        return staskMdTime;
    }

    /**
     * 设置修改时间
     *
     * @param staskMdTime 修改时间
     */
    public void setStaskMdTime(String staskMdTime) {
        this.staskMdTime = staskMdTime;
    }

    /**
     * 获取删除 0 未删除 1已删除
     *
     * @return STASK_TYPE - 删除 0 未删除 1已删除
     */
    public String getStaskType() {
        return staskType;
    }

    /**
     * 设置删除 0 未删除 1已删除
     *
     * @param staskType 删除 0 未删除 1已删除
     */
    public void setStaskType(String staskType) {
        this.staskType = staskType;
    }

    public BigDecimal getSaleAmt() {
        return saleAmt;
    }

    public void setSaleAmt(BigDecimal saleAmt) {
        this.saleAmt = saleAmt;
    }

    public BigDecimal getSaleGross() {
        return saleGross;
    }

    public void setSaleGross(BigDecimal saleGross) {
        this.saleGross = saleGross;
    }



    public int getStoQty() {
        return stoQty;
    }

    public void setStoQty(int stoQty) {
        this.stoQty = stoQty;
    }

    public BigDecimal getMcardQty() {
        return mcardQty;
    }

    public void setMcardQty(BigDecimal mcardQty) {
        this.mcardQty = mcardQty;
    }
}