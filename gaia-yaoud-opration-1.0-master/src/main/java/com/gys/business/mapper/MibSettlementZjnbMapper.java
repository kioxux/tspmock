package com.gys.business.mapper;

import com.gys.business.mapper.entity.MibSettlementZjnb;
import com.gys.common.base.BaseMapper;

import java.util.List;
import java.util.Map;

public interface MibSettlementZjnbMapper extends BaseMapper<MibSettlementZjnb> {
    void insertSaleInfo(Map<String, Object> inData);

    List<MibSettlementZjnb> selectSaleInfo(Map<String, Object> inData);

    MibSettlementZjnb selectSaleInfoSum(Map<String, Object> inData);

    MibSettlementZjnb accessToHealthCareInformation(Map<String, String> inData);
    MibSettlementZjnb accessToHealthCareInformation2(Map<String, String> inData);

    void insertDetail(MibSettlementZjnb zjnb);
}