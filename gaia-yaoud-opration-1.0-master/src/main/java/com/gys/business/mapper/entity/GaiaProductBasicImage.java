package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_PRODUCT_BASIC_IMAGE")
@Data
public class GaiaProductBasicImage implements Serializable {
    /**
     * 商品编码
     */
    @Id
    @Column(name = "GPBI_PRO_CODE")
    private String gpbiProCode;

    /**
     * 序号
     */
    @Id
    @Column(name = "GPBI_SORT")
    private Integer gpbiSort;

    /**
     * 文件名
     */
    @Column(name = "GPBI_FILE_NAME")
    private String gpbiFileName;

    /**
     * 图片上传地址
     */
    @Column(name = "GPBI_UPLOAD_PATH")
    private String gpbiUploadPath;

    /**
     * 上传日期
     */
    @Column(name = "GPBI_CREATE_TIME")
    private Date gpbiCreateTime;

    /**
     * 修改日期
     */
    @Column(name = "GPBI_MODIFY_TIME")
    private Date gpbiModifyTime;

    /**
     * 删除状态:0未删除,1删除
     */
    @Column(name = "GPBI_DELETED")
    private String gpbiDeleted;

    private static final long serialVersionUID = 1L;

    /**
     * 获取商品编码
     *
     * @return GPBI_PRO_CODE - 商品编码
     */
    public String getGpbiProCode() {
        return gpbiProCode;
    }

    /**
     * 设置商品编码
     *
     * @param gpbiProCode 商品编码
     */
    public void setGpbiProCode(String gpbiProCode) {
        this.gpbiProCode = gpbiProCode;
    }

    /**
     * 获取序号
     *
     * @return GPBI_SORT - 序号
     */
    public Integer getGpbiSort() {
        return gpbiSort;
    }

    /**
     * 设置序号
     *
     * @param gpbiSort 序号
     */
    public void setGpbiSort(Integer gpbiSort) {
        this.gpbiSort = gpbiSort;
    }

    /**
     * 获取文件名
     *
     * @return GPBI_FILE_NAME - 文件名
     */
    public String getGpbiFileName() {
        return gpbiFileName;
    }

    /**
     * 设置文件名
     *
     * @param gpbiFileName 文件名
     */
    public void setGpbiFileName(String gpbiFileName) {
        this.gpbiFileName = gpbiFileName;
    }

    /**
     * 获取图片上传地址
     *
     * @return GPBI_UPLOAD_PATH - 图片上传地址
     */
    public String getGpbiUploadPath() {
        return gpbiUploadPath;
    }

    /**
     * 设置图片上传地址
     *
     * @param gpbiUploadPath 图片上传地址
     */
    public void setGpbiUploadPath(String gpbiUploadPath) {
        this.gpbiUploadPath = gpbiUploadPath;
    }

    /**
     * 获取上传日期
     *
     * @return GPBI_CREATE_TIME - 上传日期
     */
    public Date getGpbiCreateTime() {
        return gpbiCreateTime;
    }

    /**
     * 设置上传日期
     *
     * @param gpbiCreateTime 上传日期
     */
    public void setGpbiCreateTime(Date gpbiCreateTime) {
        this.gpbiCreateTime = gpbiCreateTime;
    }

    /**
     * 获取修改日期
     *
     * @return GPBI_MODIFY_TIME - 修改日期
     */
    public Date getGpbiModifyTime() {
        return gpbiModifyTime;
    }

    /**
     * 设置修改日期
     *
     * @param gpbiModifyTime 修改日期
     */
    public void setGpbiModifyTime(Date gpbiModifyTime) {
        this.gpbiModifyTime = gpbiModifyTime;
    }

    /**
     * 获取删除状态:0未删除,1删除
     *
     * @return GPBI_DELETED - 删除状态:0未删除,1删除
     */
    public String getGpbiDeleted() {
        return gpbiDeleted;
    }

    /**
     * 设置删除状态:0未删除,1删除
     *
     * @param gpbiDeleted 删除状态:0未删除,1删除
     */
    public void setGpbiDeleted(String gpbiDeleted) {
        this.gpbiDeleted = gpbiDeleted;
    }
}