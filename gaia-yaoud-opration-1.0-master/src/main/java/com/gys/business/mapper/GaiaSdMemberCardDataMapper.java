package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMemberCardData;
import com.gys.business.service.data.GaiaSdMemberCard;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author xiaoyuan
 */
public interface GaiaSdMemberCardDataMapper extends BaseMapper<GaiaSdMemberCardData> {

    // 根据会员卡号查出对应会员卡
    GaiaSdMemberCard queryMemberCardByCardId(@Param("client") String client, @Param("cardId") String cardId);

    // 修改会员卡信息表 卡状态
    void updateMemberCardStatus(@Param("status") String status,@Param("client") String client, @Param("cardId") String cardId);

}
