package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_CUSTOMER_BUSINESS")
public class GaiaCustomerBusiness implements Serializable {
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 客户编码
     */
    @Id
    @Column(name = "CUS_CODE")
    private String cusCode;

    /**
     * 地点
     */
    @Id
    @Column(name = "CUS_SITE")
    private String cusSite;

    /**
     * 供应商自编码
     */
    @Id
    @Column(name = "CUS_SELF_CODE")
    private String cusSelfCode;

    /**
     * 供应商状态
     */
    @Column(name = "CUS_STATUS")
    private String cusStatus;

    /**
     * 禁止销售
     */
    @Column(name = "CUS_NO_SALE")
    private String cusNoSale;

    /**
     * 禁止退货
     */
    @Column(name = "CUS_NO_RETURN")
    private String cusNoReturn;

    /**
     * 付款条件
     */
    @Column(name = "CUS_PAY_TERM")
    private String cusPayTerm;

    /**
     * 业务联系人
     */
    @Column(name = "CUS_BUSSINESS_CONTACT")
    private String cusBussinessContact;

    /**
     * 联系人电话
     */
    @Column(name = "CUS_CONTACT_TEL")
    private String cusContactTel;

    /**
     * 收货地址
     */
    @Column(name = "CUS_DELIVERY_ADD")
    private String cusDeliveryAdd;

    @Column(name = "CUS_CREDIT_FLAG")
    private String cusCreditFlag;

    /**
     * 信用额度
     */
    @Column(name = "CUS_CREDIT_QUOTA")
    private BigDecimal cusCreditQuota;

    @Column(name = "CUS_CREDIT_CHECK")
    private String cusCreditCheck;

    @Column(name = "CUS_BANK_CODE")
    private String cusBankCode;

    @Column(name = "CUS_BANK_NAME")
    private String cusBankName;

    @Column(name = "CUS_ACCOUNT_PERSON")
    private String cusAccountPerson;

    @Column(name = "CUS_BANK_ACCOUNT")
    private String cusBankAccount;

    @Column(name = "CUS_PAY_MODE")
    private String cusPayMode;

    @Column(name = "CUS_CREDIT_AMT")
    private String cusCreditAmt;
    @Column(name = "CUS_AR_AMT")
    private BigDecimal cusArAmt;

    public BigDecimal getCusArAmt() {
        return cusArAmt;
    }

    public void setCusArAmt(BigDecimal cusArAmt) {
        this.cusArAmt = cusArAmt;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 1L;

    /**
     * @return CLIENT
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取客户编码
     *
     * @return CUS_CODE - 客户编码
     */
    public String getCusCode() {
        return cusCode;
    }

    /**
     * 设置客户编码
     *
     * @param cusCode 客户编码
     */
    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    /**
     * 获取地点
     *
     * @return CUS_SITE - 地点
     */
    public String getCusSite() {
        return cusSite;
    }

    /**
     * 设置地点
     *
     * @param cusSite 地点
     */
    public void setCusSite(String cusSite) {
        this.cusSite = cusSite;
    }

    /**
     * 获取供应商自编码
     *
     * @return CUS_SELF_CODE - 供应商自编码
     */
    public String getCusSelfCode() {
        return cusSelfCode;
    }

    /**
     * 设置供应商自编码
     *
     * @param cusSelfCode 供应商自编码
     */
    public void setCusSelfCode(String cusSelfCode) {
        this.cusSelfCode = cusSelfCode;
    }

    /**
     * 获取供应商状态
     *
     * @return CUS_STATUS - 供应商状态
     */
    public String getCusStatus() {
        return cusStatus;
    }

    /**
     * 设置供应商状态
     *
     * @param cusStatus 供应商状态
     */
    public void setCusStatus(String cusStatus) {
        this.cusStatus = cusStatus;
    }

    /**
     * 获取禁止销售
     *
     * @return CUS_NO_SALE - 禁止销售
     */
    public String getCusNoSale() {
        return cusNoSale;
    }

    /**
     * 设置禁止销售
     *
     * @param cusNoSale 禁止销售
     */
    public void setCusNoSale(String cusNoSale) {
        this.cusNoSale = cusNoSale;
    }

    /**
     * 获取禁止退货
     *
     * @return CUS_NO_RETURN - 禁止退货
     */
    public String getCusNoReturn() {
        return cusNoReturn;
    }

    /**
     * 设置禁止退货
     *
     * @param cusNoReturn 禁止退货
     */
    public void setCusNoReturn(String cusNoReturn) {
        this.cusNoReturn = cusNoReturn;
    }

    /**
     * 获取付款条件
     *
     * @return CUS_PAY_TERM - 付款条件
     */
    public String getCusPayTerm() {
        return cusPayTerm;
    }

    /**
     * 设置付款条件
     *
     * @param cusPayTerm 付款条件
     */
    public void setCusPayTerm(String cusPayTerm) {
        this.cusPayTerm = cusPayTerm;
    }

    /**
     * 获取业务联系人
     *
     * @return CUS_BUSSINESS_CONTACT - 业务联系人
     */
    public String getCusBussinessContact() {
        return cusBussinessContact;
    }

    /**
     * 设置业务联系人
     *
     * @param cusBussinessContact 业务联系人
     */
    public void setCusBussinessContact(String cusBussinessContact) {
        this.cusBussinessContact = cusBussinessContact;
    }

    /**
     * 获取联系人电话
     *
     * @return CUS_CONTACT_TEL - 联系人电话
     */
    public String getCusContactTel() {
        return cusContactTel;
    }

    /**
     * 设置联系人电话
     *
     * @param cusContactTel 联系人电话
     */
    public void setCusContactTel(String cusContactTel) {
        this.cusContactTel = cusContactTel;
    }

    /**
     * 获取收货地址
     *
     * @return CUS_DELIVERY_ADD - 收货地址
     */
    public String getCusDeliveryAdd() {
        return cusDeliveryAdd;
    }

    /**
     * 设置收货地址
     *
     * @param cusDeliveryAdd 收货地址
     */
    public void setCusDeliveryAdd(String cusDeliveryAdd) {
        this.cusDeliveryAdd = cusDeliveryAdd;
    }

    /**
     * @return CUS_CREDIT_FLAG
     */
    public String getCusCreditFlag() {
        return cusCreditFlag;
    }

    /**
     * @param cusCreditFlag
     */
    public void setCusCreditFlag(String cusCreditFlag) {
        this.cusCreditFlag = cusCreditFlag;
    }

    /**
     * 获取信用额度
     *
     * @return CUS_CREDIT_QUOTA - 信用额度
     */
    public BigDecimal getCusCreditQuota() {
        return cusCreditQuota;
    }

    /**
     * 设置信用额度
     *
     * @param cusCreditQuota 信用额度
     */
    public void setCusCreditQuota(BigDecimal cusCreditQuota) {
        this.cusCreditQuota = cusCreditQuota;
    }

    /**
     * @return CUS_CREDIT_CHECK
     */
    public String getCusCreditCheck() {
        return cusCreditCheck;
    }

    /**
     * @param cusCreditCheck
     */
    public void setCusCreditCheck(String cusCreditCheck) {
        this.cusCreditCheck = cusCreditCheck;
    }

    /**
     * @return CUS_BANK_CODE
     */
    public String getCusBankCode() {
        return cusBankCode;
    }

    /**
     * @param cusBankCode
     */
    public void setCusBankCode(String cusBankCode) {
        this.cusBankCode = cusBankCode;
    }

    /**
     * @return CUS_BANK_NAME
     */
    public String getCusBankName() {
        return cusBankName;
    }

    /**
     * @param cusBankName
     */
    public void setCusBankName(String cusBankName) {
        this.cusBankName = cusBankName;
    }

    /**
     * @return CUS_ACCOUNT_PERSON
     */
    public String getCusAccountPerson() {
        return cusAccountPerson;
    }

    /**
     * @param cusAccountPerson
     */
    public void setCusAccountPerson(String cusAccountPerson) {
        this.cusAccountPerson = cusAccountPerson;
    }

    /**
     * @return CUS_BANK_ACCOUNT
     */
    public String getCusBankAccount() {
        return cusBankAccount;
    }

    /**
     * @param cusBankAccount
     */
    public void setCusBankAccount(String cusBankAccount) {
        this.cusBankAccount = cusBankAccount;
    }

    /**
     * @return CUS_PAY_MODE
     */
    public String getCusPayMode() {
        return cusPayMode;
    }

    /**
     * @param cusPayMode
     */
    public void setCusPayMode(String cusPayMode) {
        this.cusPayMode = cusPayMode;
    }

    /**
     * @return CUS_CREDIT_AMT
     */
    public String getCusCreditAmt() {
        return cusCreditAmt;
    }

    /**
     * @param cusCreditAmt
     */
    public void setCusCreditAmt(String cusCreditAmt) {
        this.cusCreditAmt = cusCreditAmt;
    }
}