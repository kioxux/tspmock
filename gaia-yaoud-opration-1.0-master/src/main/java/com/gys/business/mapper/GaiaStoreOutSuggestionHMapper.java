package com.gys.business.mapper;

import com.gys.business.entity.dto.CallUpOrInStoreDetailDTO;
import com.gys.business.entity.vo.CallUpStoreDetailExportVO;
import com.gys.business.entity.vo.CallUpStoreDetailVO;
import com.gys.business.mapper.entity.GaiaStoreOutSuggestionD;
import com.gys.business.mapper.entity.GaiaStoreOutSuggestionH;
import com.gys.business.service.data.GaiaStoreInSuggestionDData;
import com.gys.business.service.data.GaiaStoreOutSuggestionDOutData;
import com.gys.business.service.data.GaiaStoreOutSuggestionInData;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionH)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-28 10:54:04
 */
public interface GaiaStoreOutSuggestionHMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaStoreOutSuggestionH queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaStoreOutSuggestionH 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaStoreOutSuggestionH> queryAllByLimit(GaiaStoreOutSuggestionH gaiaStoreOutSuggestionH, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaStoreOutSuggestionH 查询条件
     * @return 总行数
     */
    long count(GaiaStoreOutSuggestionH gaiaStoreOutSuggestionH);

    /**
     * 新增数据
     *
     * @param gaiaStoreOutSuggestionH 实例对象
     * @return 影响行数
     */
    int insert(GaiaStoreOutSuggestionH gaiaStoreOutSuggestionH);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaStoreOutSuggestionH> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaStoreOutSuggestionH> entities);

    /**
     * 修改数据
     *
     * @param gaiaStoreOutSuggestionH 实例对象
     * @return 影响行数
     */
    int update(GaiaStoreOutSuggestionH gaiaStoreOutSuggestionH);

    List<GaiaStoreOutSuggestionDOutData> listWaitGoods(GaiaStoreOutSuggestionInData inData);

    List<GaiaStoreOutSuggestionDOutData> listBill(GaiaStoreOutSuggestionInData inData);

    int isAllow(GaiaStoreOutSuggestionInData inData);

    String getId(GaiaStoreOutSuggestionH suggestionH);

    List<GaiaStoreOutSuggestionH> listInvalidBill();

    List<CallUpStoreDetailVO> queryDetail(CallUpOrInStoreDetailDTO storeDetailDTO);

    List<CallUpStoreDetailExportVO> queryDetailExport(CallUpOrInStoreDetailDTO storeDetailDTO);

    List<String> queryType();

    List<Map<String,String>> queryBillCode(@Param("startDate") String startDate ,@Param("endDate") String endDate);

}

