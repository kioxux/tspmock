package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdReplenishOriD;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdReplenishOriDMapper extends BaseMapper<GaiaSdReplenishOriD> {
    void insertLists(List<GaiaSdReplenishOriD> spOriDList);
}