package com.gys.business.mapper;

import com.gys.common.data.newProductsOrder.NewProductsOrderInfoDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductsOrderServiceMapper {

    /**
     * 获取订单列表
     * @param client
     * @param gsphBrIdList
     * @param startDate
     * @param endDate
     * @return
     */
    List<NewProductsOrderInfoDTO> queryOrderList(@Param("client") String client, @Param("gsphBrIdList") List<String> gsphBrIdList, @Param("startDate") String startDate, @Param("endDate") String endDate);
}
