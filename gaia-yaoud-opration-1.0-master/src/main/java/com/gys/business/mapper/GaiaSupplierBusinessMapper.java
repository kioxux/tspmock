package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSupplierBusiness;
import com.gys.business.service.data.GetAcceptInData;
import com.gys.business.service.data.GetPlaceOrderInData;
import com.gys.business.service.data.GetSupOutData;
import com.gys.business.service.data.SupPriceRatioOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSupplierBusinessMapper extends BaseMapper<GaiaSupplierBusiness> {
    List<GetSupOutData> getSup(GetAcceptInData inData);

    List<GetSupOutData> getSupByList(GetPlaceOrderInData inData);

    List<GetSupOutData> getSupByBrIds(GetAcceptInData inData);

    List<SupPriceRatioOutData> getRatioSupPrice(GetAcceptInData inData);

    List<GaiaSupplierBusiness> queryThirdSupplier(String client, String brId);

    List<GaiaSupplierBusiness> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);

    List<GetSupOutData> getSupByContent(GetAcceptInData inData);

    List<GaiaSupplierBusiness> findList(GaiaSupplierBusiness cond);
}
