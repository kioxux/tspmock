package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value="com-gys-business-mapper-entity-GaiaSdStoreEssentialProductD")
public class GaiaSdStoreEssentialProductD {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 必备品铺货单号
    */
    @ApiModelProperty(value="必备品铺货单号")
    private String orderId;

    /**
     * 补货单号   确认铺货后回写才会存在
     */
    @ApiModelProperty(value="补货单号    确认铺货后回写才会存在")
    private String replenishOrderId;

    /**
    * 门店号
    */
    @ApiModelProperty(value="门店号")
    private String storeId;

    /**
    * 商品自编码
    */
    @ApiModelProperty(value="商品自编码")
    private String proSelfCode;

    /**
     * 仓库成本价
     */
    @ApiModelProperty(value="仓库成本价")
    private BigDecimal wmsCostPrice;

    /**
     * 门店属性编码
     */
    @ApiModelProperty(value="门店属性编码")
    private String storeAttributeCode;


    /**
     * 门店属性
     */
    @ApiModelProperty(value="门店属性")
    private String storeAttribute;

    /**
     * 店效级别编码
     */
    @ApiModelProperty(value="店效级别编码")
    private String storeLevelCode;

    /**
    * 店效级别
    */
    @ApiModelProperty(value="店效级别")
    private String storeLevel;

    /**
     * 是否被过滤 0-没被过滤 1-被过滤
     */
    @ApiModelProperty(value="是否被过滤 0-没被过滤 1-被过滤")
    private String isRule;

    /**
    * 建议铺货量
    */
    @ApiModelProperty(value="建议铺货量")
    private Integer adviseNumber;

    /**
    * 确认铺货量
    */
    @ApiModelProperty(value="确认铺货量")
    private Integer resultNumber;

    /**
    * 创建时间
    */
    @ApiModelProperty(value="创建时间")
    private Date createTime;

    /**
    * 创建人
    */
    @ApiModelProperty(value="创建人")
    private String createUser;

    /**
    * 更新时间
    */
    @ApiModelProperty(value="更新时间")
    private Date updateTime;

    /**
    * 更新人
    */
    @ApiModelProperty(value="更新人")
    private String updateUser;

    /**
     * 更新标志 0-未更新 1-更新
     */
    @ApiModelProperty(value="更新标志 0-未更新 1-更新")
    private String updateFlag;
}