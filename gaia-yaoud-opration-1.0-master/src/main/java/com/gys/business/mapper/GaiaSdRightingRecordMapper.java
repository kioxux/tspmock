package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRightingRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdRightingRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdRightingRecord record);

    int insertSelective(GaiaSdRightingRecord record);

    GaiaSdRightingRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdRightingRecord record);

    int updateByPrimaryKey(GaiaSdRightingRecord record);

    int updateBatch(List<GaiaSdRightingRecord> list);

    int batchInsert(@Param("list") List<GaiaSdRightingRecord> list);
}