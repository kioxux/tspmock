package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaMenuConfig;
import com.gys.business.service.data.MenuConfig.MenuConfigInput;
import com.gys.business.service.data.MenuConfig.MenuConfigOutput;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaMenuConfigMapper extends BaseMapper<GaiaMenuConfig> {
    List<MenuConfigOutput> selectMenuConfig(MenuConfigInput inData);
}