package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaUserUsemap;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface GaiaUserUsemapMapper {

    int insertSelective(GaiaUserUsemap record);

    int batchInsert(@Param("list") List<GaiaUserUsemap> list);

    List<GaiaUserUsemap> getAllByClient(String client);
}