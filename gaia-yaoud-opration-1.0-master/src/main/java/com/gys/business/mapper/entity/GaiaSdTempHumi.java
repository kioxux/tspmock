//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_TEMP_HUMI"
)
public class GaiaSdTempHumi implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSTH_VOUCHER_ID"
    )
    private String gsthVoucherId;
    @Column(
            name = "GSTH_BR_ID"
    )
    private String gsthBrId;
    @Column(
            name = "GSTH_BR_NAME"
    )
    private String gsthBrName;
    @Column(
            name = "GSTH_AREA"
    )
    private String gsthArea;
    @Column(
            name = "GSTH_DATE"
    )
    private String gsthDate;
    @Column(
            name = "GSTH_TIME"
    )
    private String gsthTime;
    @Column(
            name = "GSTH_TEMP"
    )
    private String gsthTemp;
    @Column(
            name = "GSTH_HUMI"
    )
    private String gsthHumi;
    @Column(
            name = "GSTH_CHECK_STEP"
    )
    private String gsthCheckStep;
    @Column(
            name = "GSTH_CHECK_TEMP"
    )
    private String gsthCheckTemp;
    @Column(
            name = "GSTH_CHECK_HUMI"
    )
    private String gsthCheckHumi;
    @Column(
            name = "GSTH_UPDATE_EMP"
    )
    private String gsthUpdateEmp;
    @Column(
            name = "GSTH_UPDATE_DATE"
    )
    private String gsthUpdateDate;
    @Column(
            name = "GSTH_UPDATE_TIME"
    )
    private String gsthUpdateTime;
    @Column(
            name = "GSTH_STATUS"
    )
    private String gsthStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdTempHumi() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsthVoucherId() {
        return this.gsthVoucherId;
    }

    public void setGsthVoucherId(String gsthVoucherId) {
        this.gsthVoucherId = gsthVoucherId;
    }

    public String getGsthBrId() {
        return this.gsthBrId;
    }

    public void setGsthBrId(String gsthBrId) {
        this.gsthBrId = gsthBrId;
    }

    public String getGsthBrName() {
        return this.gsthBrName;
    }

    public void setGsthBrName(String gsthBrName) {
        this.gsthBrName = gsthBrName;
    }

    public String getGsthArea() {
        return this.gsthArea;
    }

    public void setGsthArea(String gsthArea) {
        this.gsthArea = gsthArea;
    }

    public String getGsthDate() {
        return this.gsthDate;
    }

    public void setGsthDate(String gsthDate) {
        this.gsthDate = gsthDate;
    }

    public String getGsthTime() {
        return this.gsthTime;
    }

    public void setGsthTime(String gsthTime) {
        this.gsthTime = gsthTime;
    }

    public String getGsthTemp() {
        return this.gsthTemp;
    }

    public void setGsthTemp(String gsthTemp) {
        this.gsthTemp = gsthTemp;
    }

    public String getGsthHumi() {
        return this.gsthHumi;
    }

    public void setGsthHumi(String gsthHumi) {
        this.gsthHumi = gsthHumi;
    }

    public String getGsthCheckStep() {
        return this.gsthCheckStep;
    }

    public void setGsthCheckStep(String gsthCheckStep) {
        this.gsthCheckStep = gsthCheckStep;
    }

    public String getGsthCheckTemp() {
        return this.gsthCheckTemp;
    }

    public void setGsthCheckTemp(String gsthCheckTemp) {
        this.gsthCheckTemp = gsthCheckTemp;
    }

    public String getGsthCheckHumi() {
        return this.gsthCheckHumi;
    }

    public void setGsthCheckHumi(String gsthCheckHumi) {
        this.gsthCheckHumi = gsthCheckHumi;
    }

    public String getGsthUpdateEmp() {
        return this.gsthUpdateEmp;
    }

    public void setGsthUpdateEmp(String gsthUpdateEmp) {
        this.gsthUpdateEmp = gsthUpdateEmp;
    }

    public String getGsthUpdateDate() {
        return this.gsthUpdateDate;
    }

    public void setGsthUpdateDate(String gsthUpdateDate) {
        this.gsthUpdateDate = gsthUpdateDate;
    }

    public String getGsthUpdateTime() {
        return this.gsthUpdateTime;
    }

    public void setGsthUpdateTime(String gsthUpdateTime) {
        this.gsthUpdateTime = gsthUpdateTime;
    }

    public String getGsthStatus() {
        return this.gsthStatus;
    }

    public void setGsthStatus(String gsthStatus) {
        this.gsthStatus = gsthStatus;
    }
}
