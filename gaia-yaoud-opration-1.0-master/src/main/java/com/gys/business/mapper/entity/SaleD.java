package com.gys.business.mapper.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
//@JSONType(orders={"amt","batch","batchNo","billNo","brId","client","date","doctorId","lastUpdateTime","prc1","prc2","proId","qty","salerId","serial","stCode","validDate","zkAmt"})
public class SaleD {
    /**
     * 加盟商
     */
    @NotNull
    @JSONField(ordinal=6)
    private String client;

    /**
     * 销售单号
     */
    @NotNull
    @JSONField(ordinal=4)
    private String billNo;

    /**
     * 店名
     */
    @NotNull
    @JSONField(ordinal=5)
    private String brId;

    /**
     * 销售日期
     */
    @NotNull
    @JSONField(ordinal=7)
    private String date;

    /**
     * 行号
     */
    @NotNull
    @JSONField(ordinal=15)
    private String serial;

    /**
     * 商品编码
     */
    @NotNull
    @JSONField(ordinal=12)
    private String proId;

    /**
     * 商品批号
     */
    @JSONField(ordinal=3)
    private String batchNo;

    /**
     * 商品批次
     */
    @JSONField(ordinal=2)
    private String batch;

    /**
     * 商品有效期
     */
    @JSONField(ordinal=17)
    private String validDate;

    /**
     * 监管码/条形码
     */
    @JSONField(ordinal=16)
    private String stCode;

    /**
     * 单品零售价
     */
    @NotNull
    @JSONField(ordinal=10)
    private BigDecimal prc1;

    /**
     * 单品应收价
     */
    @NotNull
    @JSONField(ordinal=11)
    private BigDecimal prc2;

    /**
     * 数量
     */
    @NotNull
    @JSONField(ordinal=13)
    private BigDecimal qty;

    /**
     * 汇总应收金额
     */
    @NotNull
    @JSONField(ordinal=1)
    private BigDecimal amt;

    /**
     * 商品折扣总金额
     */
    @JSONField(ordinal=18)
    private BigDecimal zkAmt;

    /**
     * 	营业员编号
     */
    @JSONField(ordinal=14)
    private String salerId;

    /**
     * 医生编号
     */
    @JSONField(ordinal=8)
    private String doctorId;

    @JSONField(ordinal=9,serialzeFeatures = {SerializerFeature.WriteMapNullValue})
    private String lastUpdateTime;

    private static final long serialVersionUID = 1L;



//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder("{");
//        sb.append(",\"amt\":")
//                .append(amt);
//        sb.append(",\"batch\":\"")
//                .append(batch).append('\"');
//        sb.append(",\"batchNo\":\"")
//                .append(batchNo).append('\"');
//        sb.append(",\"billNo\":\"")
//                .append(billNo).append('\"');
//        sb.append(",\"brId\":\"")
//                .append(brId).append('\"');
//        sb.append("\"client\":\"")
//                .append(client).append('\"');
//        sb.append(",\"date\":\"")
//                .append(date).append('\"');
//        sb.append(",\"doctorId\":\"")
//                .append(doctorId).append('\"');
//        sb.append(",\"lastUpdateTime\":\"")
//                .append(lastUpdateTime).append('\"');
//        sb.append(",\"prc1\":")
//                .append(prc1);
//        sb.append(",\"prc2\":")
//                .append(prc2);
//        sb.append(",\"proId\":\"")
//                .append(proId).append('\"');
//        sb.append(",\"qty\":")
//                .append(qty);
//        sb.append(",\"salerId\":\"")
//                .append(salerId).append('\"');
//        sb.append(",\"serial\":\"")
//                .append(serial).append('\"');
//        sb.append(",\"stCode\":\"")
//                .append(stCode).append('\"');
//        sb.append(",\"validDate\":\"")
//                .append(validDate).append('\"');
//        sb.append(",\"zkAmt\":")
//                .append(zkAmt);
//        sb.append('}');
//        return sb.toString();
//    }
}