package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(  name = "GAIA_SUPPLIER_BUSINESS")
@Data
public class GaiaSupplierBusiness implements Serializable {
    private static final long serialVersionUID = -5966282044194302826L;
    @Id
    @Column( name = "CLIENT" )
    private String client;

    @Id
    @Column( name = "SUP_CODE"  )
    private String supCode;

    @Id
    @Column( name = "SUP_SITE" )
    private String supSite;

    @Id
    @Column( name = "SUP_SELF_CODE" )
    private String supSelfCode;

    @Column(name = "SUP_MATCH_STATUS")
    private String supMatchStatus;

    @Column(name = "SUP_PYM")
    private String supPym;

    @Column(name = "SUP_NAME")
    private String supName;

    @Column(name = "SUP_STATUS")
    private String supStatus;

    @Column(name = "SUP_NO_PURCHASE")
    private String supNoPurchase;

    @Column(name = "SUP_NO_SUPPLIER")
    private String supNoSupplier;

    @Column(name = "SUP_PAY_TERM")
    private String supPayTerm;

    @Column(name = "SUP_BUSSINESS_CONTACT")
    private String supBussinessContact;

    @Column(name = "SUP_CONTACT_TEL" )
    private String supContactTel;

    @Column(name = "SUP_LEAD_TIME")
    private String supLeadTime;

    @Column(name = "SUP_BANK_CODE")
    private String supBankCode;

    @Column(name = "SUP_BANK_NAME")
    private String supBankName;

    @Column(name = "SUP_ACCOUNT_PERSON")
    private String supAccountPerson;

    @Column(name = "SUP_BANK_ACCOUNT")
    private String supBankAccount;

    @Column(name = "SUP_PAY_MODE")
    private String supPayMode;

    @Column(name = "SUP_CREDIT_AMT")
    private String supCreditAmt;

    @Column(name = "SUP_CLASS")
    private String supClass;

    @Column(name = "SUP_CREDIT_CODE")
    private String supCreditCode;

    @Column(name = "SUP_CREDIT_DATE")
    private String supCreditDate;

    @Column(name = "SUP_LEGAL_PERSON")
    private String supLegalPerson;

    @Column(name = "SUP_REG_ADD")
    private String supRegAdd;

    @Column(name = "SUP_LICENCE_NO")
    private String supLicenceNo;

    @Column(name = "SUP_LICENCE_DATE")
    private String supLicenceDate;

    @Column(name = "SUP_LICENCE_VALID")
    private String supLicenceValid;

    @Column(name = "SUP_SCOPE")
    private String supScope;

    @Column(name = "SUP_MIX_AMT")
    private BigDecimal supMixAmt;

    @Column(name = "SUP_FRWTS")
    private String supFrwts;

    @Column(name = "SUP_FRWTSXQ")
    private String supFrwtsxq;

    @Column(name = "SUP_ZLFZR")
    private String supZlfzr;

    @Column(name = "SUP_CKDZ")
    private String supCkdz;

    @Column(name = "SUP_LOGIN_ADD")
    private String supLoginAdd;

    @Column(name = "SUP_LOGIN_USER")
    private String supLoginUser;

    @Column(name = "SUP_LOGIN_PASSWORD")
    private String supLoginPassword;

    @Column(name = "SUP_SYS_PARM")
    private String supSysParm;

    @Column(name = "SUP_DELIVERY_MODE")
    private String supDeliveryMode;

    @Column(name = "SUP_CARRIER")
    private String supCarrier;

    @Column(name = "SUP_DELIVERY_TIMES")
    private String supDeliveryTimes;

    @Column(name = "SUP_PICTURE_SHTXD")
    private String supPictureShtxd;
}
