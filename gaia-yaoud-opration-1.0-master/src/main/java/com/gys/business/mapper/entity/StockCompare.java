package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 月度库存表
 *
 * </p>
 *
 * @author flynn
 * @since 2021-07-13
 */
@Data
@Table(
        name = "GAIA_STOCK_COMPARE"
)
public class StockCompare implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "比对任务号")
    @Column(name = "GSC_TASK_NO")
    private String taskNo;

    @ApiModelProperty(value = "比对任务行号")
    @Column(name = "GSC_TASK_ITEM")
    private String taskItem;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "地点（门店/仓库）")
    @Column(name = "GSC_SITE_CODE")
    private String gscSiteCode;

    @ApiModelProperty(value = "对比条目数")
    @Column(name = "GSC_COMPARE_ROW")
    private BigDecimal gscCompareRow;

    @ApiModelProperty(value = "差异条目数")
    @Column(name = "GSC_DIFF_ROW")
    private BigDecimal gscDiffRow;

    @ApiModelProperty(value = "实时数量")
    @Column(name = "GSC_STOCK_QTY")
    private BigDecimal gscStockQty;

    @ApiModelProperty(value = "推算数量")
    @Column(name = "GSC_COUNT_QTY")
    private BigDecimal gscCountQty;

    @ApiModelProperty(value = "差异数量")
    @Column(name = "GSC_DIFF_QTY")
    private BigDecimal gscDiffQty;

    @ApiModelProperty(value = "实时金额")
    @Column(name = "GSC_STOCK_AMT")
    private BigDecimal gscStockAmt;

    @ApiModelProperty(value = "推算金额")
    @Column(name = "GSC_COUNT_AMT")
    private BigDecimal gscCountAmt;

    @ApiModelProperty(value = "差异金额")
    @Column(name = "GSC_DIFF_AMT")
    private BigDecimal gscDiffAmt;

    @ApiModelProperty(value = "实时税金")
    @Column(name = "GSC_TAX_AMT")
    private BigDecimal gscTaxAmt;

    @ApiModelProperty(value = "推算税金")
    @Column(name = "GSC_COUNT_TAX")
    private BigDecimal gscCountTax;

    @ApiModelProperty(value = "差异金额")
    @Column(name = "GSC_DIFF_TAX")
    private BigDecimal gscDiffTax;

    @ApiModelProperty(value = "创建日期")
    @Column(name = "GSC_CREATE_DATE")
    private String gscCreateDate;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "GSC_CREATE_TIME")
    private String gscCreateTime;

    @ApiModelProperty(value = "创建人")
    @Column(name = "GSC_CREATE_BY")
    private String gscCreateBy;

    @ApiModelProperty(value = "最后更新时间")
    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;


}
