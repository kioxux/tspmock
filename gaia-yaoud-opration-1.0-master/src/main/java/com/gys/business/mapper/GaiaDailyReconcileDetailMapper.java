//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdDailyReconcileDetail;
import com.gys.business.service.data.DailyReconcileDetailOutData;
import com.gys.business.service.data.DailyReconcileOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaDailyReconcileDetailMapper extends BaseMapper<GaiaSdDailyReconcileDetail> {
    List<DailyReconcileDetailOutData> getDailyReconcileDetailList(DailyReconcileOutData inData);

    void insertLists(List<GaiaSdDailyReconcileDetail> list);
}
