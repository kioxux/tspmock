//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRechargeCardLose;
import com.gys.business.service.data.GetCardLoseOrChangeInData;
import com.gys.business.service.data.GetCardLoseOrChangeOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdRechargeCardLoseMapper extends BaseMapper<GaiaSdRechargeCardLose> {
    String selectNextVoucherId(@Param("client") String client, @Param("codePre") String codePre);

    List<GaiaSdRechargeCardLose> getAllLostDataByCardNo(@Param("client") String client, @Param("cardNo") String cardNo);

    List<GetCardLoseOrChangeOutData> queryLostOrChange(GetCardLoseOrChangeInData inData);
}
