package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaMaterialDocLog;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaMaterialDocLogMapper extends BaseMapper<GaiaMaterialDocLog> {
    int insertBatchList(@Param("list") List<GaiaMaterialDocLog> list);

}