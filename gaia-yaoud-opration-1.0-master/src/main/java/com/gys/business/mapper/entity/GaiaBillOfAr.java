package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 明细清单表(GaiaBillOfAr)实体类
 *
 * @author makejava
 * @since 2021-09-18 15:13:11
 */
public class GaiaBillOfAr implements Serializable {
    private static final long serialVersionUID = -38101044084295338L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 回款单号
     */
    private String billCode;
    /**
     * 仓库编码
     */
    private String dcCode;
    /**
     * 应收方式编码
     */
    private String arMethodId;
    /**
     * 发生金额
     */
    private BigDecimal arAmt;
    /**
     * 核销金额
     */
    private BigDecimal hxAmt;
    /**
     * 备注
     */
    private String remark;
    /**
     * 发生日期
     */
    private Date billDate;
    /**
     * 客户自编码
     */
    private String cusSelfCode;
    /**
     * 客户名称
     */
    private String cusName;


    /**
     * 销售员编号
     */
    private String gwsCode;

    /**
     * 销售员姓名
     */
    private String gwsName;
    /**
     * 核销状态 0-未核销 1-核销中 2-已核销
     */
    private Integer status;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;

    public String getGwsCode() {
        return gwsCode;
    }

    public void setGwsCode(String gwsCode) {
        this.gwsCode = gwsCode;
    }

    public String getGwsName() {
        return gwsName;
    }

    public void setGwsName(String gwsName) {
        this.gwsName = gwsName;
    }

    public BigDecimal getArAmt() {
        return arAmt;
    }

    public void setArAmt(BigDecimal arAmt) {
        this.arAmt = arAmt;
    }

    public BigDecimal getHxAmt() {
        return hxAmt;
    }

    public void setHxAmt(BigDecimal hxAmt) {
        this.hxAmt = hxAmt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getDcCode() {
        return dcCode;
    }

    public void setDcCode(String dcCode) {
        this.dcCode = dcCode;
    }

    public String getArMethodId() {
        return arMethodId;
    }

    public void setArMethodId(String arMethodId) {
        this.arMethodId = arMethodId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public String getCusSelfCode() {
        return cusSelfCode;
    }

    public void setCusSelfCode(String cusSelfCode) {
        this.cusSelfCode = cusSelfCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}
