package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class GaiaMaterialControl implements Serializable {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String brId;

    /**
     * 商品编码
     */
    private String proId;

    /**
     * 物资编码
     */
    private String materialId;
}
