package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaTOrderProInfo;
import com.gys.common.base.BaseMapper;

public interface GaiaTOrderProInfoMapper extends BaseMapper<GaiaTOrderProInfo> {
}
