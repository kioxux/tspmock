//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_RECHARGE_CARD_LOSE"
)
public class GaiaSdRechargeCardLose implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSRCL_VOUCHER_ID"
    )
    private String gsrclVoucherId;
    @Column(
            name = "GSRCL_STATUS"
    )
    private String gsrclStatus;
    @Column(
            name = "GSRCL_BR_ID"
    )
    private String gsrclBrId;
    @Column(
            name = "GSRCL_DATE"
    )
    private String gsrclDate;
    @Column(
            name = "GSRCL_TIME"
    )
    private String gsrclTime;
    @Column(
            name = "GSRCL_EMP"
    )
    private String gsrclEmp;
    @Column(
            name = "GSRCL_CARD_ID"
    )
    private String gsrclCardId;
    @Column(
            name = "GSRCL_REASON"
    )
    private String gsrclReason;
    private static final long serialVersionUID = 1L;

    public GaiaSdRechargeCardLose() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsrclVoucherId() {
        return this.gsrclVoucherId;
    }

    public void setGsrclVoucherId(String gsrclVoucherId) {
        this.gsrclVoucherId = gsrclVoucherId;
    }

    public String getGsrclStatus() {
        return this.gsrclStatus;
    }

    public void setGsrclStatus(String gsrclStatus) {
        this.gsrclStatus = gsrclStatus;
    }

    public String getGsrclBrId() {
        return this.gsrclBrId;
    }

    public void setGsrclBrId(String gsrclBrId) {
        this.gsrclBrId = gsrclBrId;
    }

    public String getGsrclDate() {
        return this.gsrclDate;
    }

    public void setGsrclDate(String gsrclDate) {
        this.gsrclDate = gsrclDate;
    }

    public String getGsrclTime() {
        return this.gsrclTime;
    }

    public void setGsrclTime(String gsrclTime) {
        this.gsrclTime = gsrclTime;
    }

    public String getGsrclEmp() {
        return this.gsrclEmp;
    }

    public void setGsrclEmp(String gsrclEmp) {
        this.gsrclEmp = gsrclEmp;
    }

    public String getGsrclCardId() {
        return this.gsrclCardId;
    }

    public void setGsrclCardId(String gsrclCardId) {
        this.gsrclCardId = gsrclCardId;
    }

    public String getGsrclReason() {
        return this.gsrclReason;
    }

    public void setGsrclReason(String gsrclReason) {
        this.gsrclReason = gsrclReason;
    }
}
