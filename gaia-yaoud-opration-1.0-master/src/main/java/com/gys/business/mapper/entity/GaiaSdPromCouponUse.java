//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 电子券类促销用券设置表
 */
@Table(name = "GAIA_SD_PROM_COUPON_USE")
@Data
public class GaiaSdPromCouponUse implements Serializable {
    private static final long serialVersionUID = 3295300081353467535L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPCU_VOUCHER_ID")
    private String gspcuVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPCU_SERIAL")
    private String gspcuSerial;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSPCU_PRO_ID")
    private String gspcuProId;

    /**
     * 电子券活动号
     */
    @Id
    @Column(name = "GSPCU_ACT_NO")
    private String gspcuActNo;

    /**
     * 达到数量1
     */
    @Column(name = "GSPCU_REACH_QTY1")
    private String gspcuReachQty1;

    /**
     * 达到金额1
     */
    @Column(name = "GSPCU_REACH_AMT1")
    private BigDecimal gspcuReachAmt1;

    /**
     * 用券数量1
     */
    @Column(name = "GSPCU_RESULT_QTY1")
    private String gspcuResultQty1;

    /**
     * 达到数量2
     */
    @Column(name = "GSPCU_REACH_QTY2")
    private String gspcuReachQty2;

    /**
     * 达到金额2
     */
    @Column(name = "GSPCU_REACH_AMT2")
    private BigDecimal gspcuReachAmt2;

    /**
     * 用券数量2
     */
    @Column(name = "GSPCU_RESULT_QTY2")
    private String gspcuResultQty2;

    /**
     * 达到数量3
     */
    @Column(name = "GSPCU_REACH_QTY3")
    private String gspcuReachQty3;

    /**
     * 达到金额3
     */
    @Column(name = "GSPCU_REACH_AMT3")
    private BigDecimal gspcuReachAmt3;

    /**
     * 用券数量3
     */
    @Column(name = "GSPCU_RESULT_QTY3")
    private String gspcuResultQty3;

    /**
     * 是否会员
     */
    @Column(name = "GSPCU_MEM_FLAG")
    private String gspcuMemFlag;

    /**
     *  是否积分
     */
    @Column(name = "GSPCU_INTE_FLAG")
    private String gspcuInteFlag;

    /**
     * 积分倍率
     */
    @Column(name = "GSPCU_INTE_RATE")
    private String gspcuInteRate;
}
