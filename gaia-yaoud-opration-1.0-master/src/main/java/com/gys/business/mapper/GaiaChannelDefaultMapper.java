package com.gys.business.mapper;

import com.gys.business.service.data.GaiaChannelDefault;

import java.util.HashMap;
import java.util.List;

import com.gys.business.service.data.GaiaChannelProductInData;
import com.gys.business.service.data.GaiaChannelProductOutData;
import org.apache.ibatis.annotations.Param;

public interface GaiaChannelDefaultMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaChannelDefault record);

    int insertSelective(GaiaChannelDefault record);

    GaiaChannelDefault selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaChannelDefault record);

    int updateByPrimaryKey(GaiaChannelDefault record);

    int batchInsert(@Param("list") List<GaiaChannelDefault> list);

    List<HashMap<String,Object>> listChannelDefault(GaiaChannelProductInData inData);
}