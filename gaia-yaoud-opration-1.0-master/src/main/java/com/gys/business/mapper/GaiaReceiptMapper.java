package com.gys.business.mapper;

import com.gys.business.service.data.ReceiptInData;
import com.gys.business.service.data.ReceiptOutData;
import com.gys.business.service.data.ReceiptPayOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface GaiaReceiptMapper extends BaseMapper<ReceiptOutData> {

    /**
     * 根据单号获取 交易数据
     * @param inData
     * @return
     */
    ReceiptOutData findReceipt(ReceiptInData inData);


    /**
     * 根据单号查询支付类型
     * @param inData
     * @return
     */
    List<ReceiptPayOutData> findTypeByOrderNumber(ReceiptInData inData);
}
