package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**

 * @author xiayuan
 * 商品组表
 */
@Data
public class GaiaSdElectronicProductsGroup implements Serializable {
    private static final long serialVersionUID = 4860206383753613212L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 商品组名
     */
    private String gspgId;

    /**
     * 商品编码
     */
    private String gspgProId;

    /**
     * 是否有效
     */
    private String gspgValid;
}