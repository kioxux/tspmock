package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaPriceGet;
import com.gys.business.mapper.entity.GaiaRepPrice;
import com.gys.business.service.data.PriceGetOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaPriceGetMapper extends BaseMapper<GaiaPriceGet> {
    List<PriceGetOutData> getReplishList(GaiaRepPrice gaiaRepPrice);
    List<PriceGetOutData> getChainReplishList(GaiaRepPrice gaiaRepPrice);
    void insertBatch(List<GaiaPriceGet> inData);
}
