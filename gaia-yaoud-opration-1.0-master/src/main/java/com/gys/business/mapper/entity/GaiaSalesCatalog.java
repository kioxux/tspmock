package com.gys.business.mapper.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

/**
 * 销售目录维护表(GaiaSalesCatalog)实体类
 *
 * @author makejava
 * @since 2021-08-25 10:51:47
 */
public class GaiaSalesCatalog extends BaseRowModel implements Serializable {
    private static final long serialVersionUID = 643579019570930620L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 商品编码
     */
    @ExcelProperty(index = 0)
    private String proSelfCode;
    /**
     * 商品通用名
     */
    private String proCommonname;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 厂家
     */
    private String proFactoryName;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 状态 1:正常隐藏 2:彻底隐藏
     */
    private Integer status;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 创建日期
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String updateUser;
    /**
     * 更新日期
     */
    private Date updateTime;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getProSelfCode() {
        return proSelfCode;
    }

    public void setProSelfCode(String proSelfCode) {
        this.proSelfCode = proSelfCode;
    }

    public String getProCommonname() {
        return proCommonname;
    }

    public void setProCommonname(String proCommonname) {
        this.proCommonname = proCommonname;
    }

    public String getProSpecs() {
        return proSpecs;
    }

    public void setProSpecs(String proSpecs) {
        this.proSpecs = proSpecs;
    }

    public String getProFactoryName() {
        return proFactoryName;
    }

    public void setProFactoryName(String proFactoryName) {
        this.proFactoryName = proFactoryName;
    }

    public String getProUnit() {
        return proUnit;
    }

    public void setProUnit(String proUnit) {
        this.proUnit = proUnit;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

}
