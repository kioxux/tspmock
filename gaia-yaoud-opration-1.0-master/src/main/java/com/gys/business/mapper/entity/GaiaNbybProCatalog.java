package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_NBYB_PRO_CATALOG")
public class GaiaNbybProCatalog implements Serializable {
    @Column(name = "NBYB_PRO_CODE")
    private String nbybProCode;

    @Column(name = "CITY_PRO_CODE")
    private String cityProCode;

    @Column(name = "PRO_NAME")
    private String proName;

    @Column(name = "JX")
    private String jx;

    @Column(name = "YBFL")
    private String ybfl;

    @Column(name = "XZFW")
    private String xzfw;

    @Column(name = "CFYBZ")
    private String cfybz;

    @Column(name = "GRZFLNZH")
    private String grzflnzh;

    @Column(name = "FYTJML")
    private String fytjml;

    @Column(name = "XZYYBZ")
    private String xzyybz;

    @Column(name = "YLKZBZ")
    private String ylkzbz;

    @Column(name = "ZYYPQFBZ")
    private String zyypqfbz;

    @Column(name = "ZYYPCLBZ")
    private String zyypclbz;

    @Column(name = "PROVINCE_PRO_CODE")
    private String provinceProCode;

    private static final long serialVersionUID = 1L;

    /**
     * @return NBYB_PRO_CODE
     */
    public String getNbybProCode() {
        return nbybProCode;
    }

    /**
     * @param nbybProCode
     */
    public void setNbybProCode(String nbybProCode) {
        this.nbybProCode = nbybProCode;
    }

    /**
     * @return CITY_PRO_CODE
     */
    public String getCityProCode() {
        return cityProCode;
    }

    /**
     * @param cityProCode
     */
    public void setCityProCode(String cityProCode) {
        this.cityProCode = cityProCode;
    }

    /**
     * @return PRO_NAME
     */
    public String getProName() {
        return proName;
    }

    /**
     * @param proName
     */
    public void setProName(String proName) {
        this.proName = proName;
    }

    /**
     * @return JX
     */
    public String getJx() {
        return jx;
    }

    /**
     * @param jx
     */
    public void setJx(String jx) {
        this.jx = jx;
    }

    /**
     * @return YBFL
     */
    public String getYbfl() {
        return ybfl;
    }

    /**
     * @param ybfl
     */
    public void setYbfl(String ybfl) {
        this.ybfl = ybfl;
    }

    /**
     * @return XZFW
     */
    public String getXzfw() {
        return xzfw;
    }

    /**
     * @param xzfw
     */
    public void setXzfw(String xzfw) {
        this.xzfw = xzfw;
    }

    /**
     * @return CFYBZ
     */
    public String getCfybz() {
        return cfybz;
    }

    /**
     * @param cfybz
     */
    public void setCfybz(String cfybz) {
        this.cfybz = cfybz;
    }

    /**
     * @return GRZFLNZH
     */
    public String getGrzflnzh() {
        return grzflnzh;
    }

    /**
     * @param grzflnzh
     */
    public void setGrzflnzh(String grzflnzh) {
        this.grzflnzh = grzflnzh;
    }

    /**
     * @return FYTJML
     */
    public String getFytjml() {
        return fytjml;
    }

    /**
     * @param fytjml
     */
    public void setFytjml(String fytjml) {
        this.fytjml = fytjml;
    }

    /**
     * @return XZYYBZ
     */
    public String getXzyybz() {
        return xzyybz;
    }

    /**
     * @param xzyybz
     */
    public void setXzyybz(String xzyybz) {
        this.xzyybz = xzyybz;
    }

    /**
     * @return YLKZBZ
     */
    public String getYlkzbz() {
        return ylkzbz;
    }

    /**
     * @param ylkzbz
     */
    public void setYlkzbz(String ylkzbz) {
        this.ylkzbz = ylkzbz;
    }

    /**
     * @return ZYYPQFBZ
     */
    public String getZyypqfbz() {
        return zyypqfbz;
    }

    /**
     * @param zyypqfbz
     */
    public void setZyypqfbz(String zyypqfbz) {
        this.zyypqfbz = zyypqfbz;
    }

    /**
     * @return ZYYPCLBZ
     */
    public String getZyypclbz() {
        return zyypclbz;
    }

    /**
     * @param zyypclbz
     */
    public void setZyypclbz(String zyypclbz) {
        this.zyypclbz = zyypclbz;
    }

    /**
     * @return PROVINCE_PRO_CODE
     */
    public String getProvinceProCode() {
        return provinceProCode;
    }

    /**
     * @param provinceProCode
     */
    public void setProvinceProCode(String provinceProCode) {
        this.provinceProCode = provinceProCode;
    }
}