package com.gys.business.mapper.entity;

import lombok.Data;

/**
 * @author XIAOZY
 * @date 2022/01/12
 */
@Data
public class GaiaSdSendElectron {

    /**
     * 加盟商
     */
    private String client;

    /**
     *会员卡号
     */
    private String gsecMemberId;

    /**
     * 电子券活动号
     */
    private String gsebId;

    /**
     * 电子券金额
     */
    private String gsecAmt;

    /**
     * 电子券描述
     */
    private String gsebName;

    /**
     * 途径  0为线下，1为线上
     */
    private String gsecFlag;

    /**
     * 送券销售单号
     */
    private String gsecGiveBillNo;
    /**
     *创建日期
     */
    private String gsecCreateDate;
    /**
     *失效日期
     */
    private String gsecFailDate;
    /**
     *用券截止日期
     */
    private String gsecUseExpirationDate;
    /**
     *赠送电子券数量
     */
    private String gsecSendQty;
    /**
     * 送券门店
     */
    private String gsecGiveBrId;
    /**
     * 送券日期
     */
    private String gsecGiveSaleDate;



}
