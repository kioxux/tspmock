package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("会员卡解挂信息")
public class UncouplingMemberCardInfo {

    @ApiModelProperty(value = "挂失门店号")
    private String brId;

    @ApiModelProperty(value = "挂失门店名称")
    private String brName;

    @ApiModelProperty(value = "挂失时间")
    private Date updateTime;

    @ApiModelProperty(value = "挂失办理人id")
    private String empId;

    @ApiModelProperty(value = "挂失办理人名称")
    private String empName;
}
