//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_MED_CHECK_D"
)
public class GaiaSdMedCheckD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSMCD_VOUCHER_ID"
    )
    private String gsmcdVoucherId;
    @Id
    @Column(
            name = "GSMCD_BR_ID"
    )
    private String gsmcdBrId;
    @Id
    @Column(
            name = "GSMCD_SERIAL"
    )
    private String gsmcdSerial;
    @Column(
            name = "GSMCD_PRO_ID"
    )
    private String gsmcdProId;
    @Column(
            name = "GSMCD_BATCH_NO"
    )
    private String gsmcdBatchNo;
    @Column(
            name = "GSMCD_QTY"
    )
    private String gsmcdQty;
    @Column(
            name = "GSMCD_RESULT"
    )
    private String gsmcdResult;
    private static final long serialVersionUID = 1L;

    public GaiaSdMedCheckD() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsmcdVoucherId() {
        return this.gsmcdVoucherId;
    }

    public void setGsmcdVoucherId(String gsmcdVoucherId) {
        this.gsmcdVoucherId = gsmcdVoucherId;
    }

    public String getGsmcdBrId() {
        return this.gsmcdBrId;
    }

    public void setGsmcdBrId(String gsmcdBrId) {
        this.gsmcdBrId = gsmcdBrId;
    }

    public String getGsmcdSerial() {
        return this.gsmcdSerial;
    }

    public void setGsmcdSerial(String gsmcdSerial) {
        this.gsmcdSerial = gsmcdSerial;
    }

    public String getGsmcdProId() {
        return this.gsmcdProId;
    }

    public void setGsmcdProId(String gsmcdProId) {
        this.gsmcdProId = gsmcdProId;
    }

    public String getGsmcdBatchNo() {
        return this.gsmcdBatchNo;
    }

    public void setGsmcdBatchNo(String gsmcdBatchNo) {
        this.gsmcdBatchNo = gsmcdBatchNo;
    }

    public String getGsmcdQty() {
        return this.gsmcdQty;
    }

    public void setGsmcdQty(String gsmcdQty) {
        this.gsmcdQty = gsmcdQty;
    }

    public String getGsmcdResult() {
        return this.gsmcdResult;
    }

    public void setGsmcdResult(String gsmcdResult) {
        this.gsmcdResult = gsmcdResult;
    }
}
