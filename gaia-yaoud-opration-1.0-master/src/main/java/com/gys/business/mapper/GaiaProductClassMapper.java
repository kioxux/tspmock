package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductClass;
import com.gys.business.service.data.SelectData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaProductClassMapper extends BaseMapper<GaiaProductClass> {
    List<SelectData> getProClass();
    List<SelectData> getProBigClass();
    List<SelectData> getProMidClass();
}