package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 库存审计(GaiaSynchronousAuditStock)实体类
 *
 * @author makejava
 * @since 2021-07-01 13:43:46
 */
@Data
public class GaiaSynchronousAuditStock implements Serializable {
    private static final long serialVersionUID = -70698907210161115L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店ID
     */
    private String storeId;
    /**
     * 同步日期
     */
    private Date billDate;
    /**
     * 主表总数量
     */
    private BigDecimal headQty;
    /**
     * 明细表总数量
     */
    private BigDecimal detailQty;
    /**
     * 来源 timer:定时任务 fx: fx上传
     */
    private String source;
    /**
     * 门店电脑标识(主板)
     */
    private String computerLogo;
}
