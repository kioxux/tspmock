package com.gys.business.mapper;

import com.gys.business.mapper.entity.MibReconciliation;
import com.gys.common.base.BaseMapper;


public interface MibReconciliationMapper extends BaseMapper<MibReconciliation> {

}