package com.gys.business.mapper;

import com.gys.business.service.data.GaiaDoctorAddPointInData;
import com.gys.business.service.data.GaiaDoctorAddPointQueryData;

import java.util.List;
import java.util.Map;

public interface FXDoctorAddPointMapper {
    void saveDoctorAddPoint(GaiaDoctorAddPointInData inData);

    GaiaDoctorAddPointInData findAddPointByDoctorId(GaiaDoctorAddPointInData inData);

    void updateDoctorAddPoint(GaiaDoctorAddPointInData inData);

    List<GaiaDoctorAddPointInData> findDoctorAddPointPageByClient(GaiaDoctorAddPointQueryData queryData);

    List<Map<String, String>> findDoctorListByClient(String client);
}
