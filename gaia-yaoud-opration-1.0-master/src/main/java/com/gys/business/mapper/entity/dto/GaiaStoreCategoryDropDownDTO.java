package com.gys.business.mapper.entity.dto;

import com.gys.business.mapper.entity.GaiaStoreCategoryAttr;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wu mao yin
 * @Title: 门店分类下拉入参
 * @date 2021/10/2113:33
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GaiaStoreCategoryDropDownDTO extends GaiaStoreCategoryAttr {

    @ApiModelProperty("门店编号")
    private String stoCode;

    @ApiModelProperty(value = "分类类型")
    private String gssgType;

    @ApiModelProperty("加盟商id")
    private String clientId;

}
