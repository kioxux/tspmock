package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_WMS_TUIGONG_M")
public class GaiaWmsTuigongM implements Serializable {
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 地点
     */
    @Column(name = "PRO_SITE")
    private String proSite;

    /**
     * 退供应商单号
     */
    @Column(name = "WM_TGYSDH")
    private String wmTgysdh;

    /**
     * 商品编码
     */
    @Column(name = "WM_SP_BM")
    private String wmSpBm;

    /**
     * 批次号
     */
    @Column(name = "WM_PCH")
    private String wmPch;

    /**
     * 货位号
     */
    @Column(name = "WM_HWH")
    private String wmHwh;

    /**
     * 库存状态编号
     */
    @Column(name = "WM_KCZT_BH")
    private String wmKcztBh;

    /**
     * 出库价
     */
    @Column(name = "WM_CKJ")
    private BigDecimal wmCkj;

    /**
     * 出库原数量
     */
    @Column(name = "WM_CKYSL")
    private BigDecimal wmCkysl;

    /**
     * 过帐数量
     */
    @Column(name = "WM_GZSL")
    private BigDecimal wmGzsl;

    /**
     * 备注
     */
    @Column(name = "WM_BZ")
    private String wmBz;

    /**
     * 修改日期
     */
    @Column(name = "WM_XGRQ")
    private String wmXgrq;

    /**
     * 修改时间
     */
    @Column(name = "WM_XGSJ")
    private String wmXgsj;

    /**
     * 修改人
     */
    @Column(name = "WM_XGR")
    private String wmXgr;

    /**
     * 修改人姓名
     */
    @Column(name = "WM_XGRXM")
    private String wmXgrxm;

    /**
     * po明细表行号
     */
    @Column(name = "PO_LINE_NO")
    private String poLineNo;

    private static final long serialVersionUID = 1L;

    /**
     * 获取ID
     *
     * @return ID - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取地点
     *
     * @return PRO_SITE - 地点
     */
    public String getProSite() {
        return proSite;
    }

    /**
     * 设置地点
     *
     * @param proSite 地点
     */
    public void setProSite(String proSite) {
        this.proSite = proSite;
    }

    /**
     * 获取退供应商单号
     *
     * @return WM_TGYSDH - 退供应商单号
     */
    public String getWmTgysdh() {
        return wmTgysdh;
    }

    /**
     * 设置退供应商单号
     *
     * @param wmTgysdh 退供应商单号
     */
    public void setWmTgysdh(String wmTgysdh) {
        this.wmTgysdh = wmTgysdh;
    }

    /**
     * 获取商品编码
     *
     * @return WM_SP_BM - 商品编码
     */
    public String getWmSpBm() {
        return wmSpBm;
    }

    /**
     * 设置商品编码
     *
     * @param wmSpBm 商品编码
     */
    public void setWmSpBm(String wmSpBm) {
        this.wmSpBm = wmSpBm;
    }

    /**
     * 获取批次号
     *
     * @return WM_PCH - 批次号
     */
    public String getWmPch() {
        return wmPch;
    }

    /**
     * 设置批次号
     *
     * @param wmPch 批次号
     */
    public void setWmPch(String wmPch) {
        this.wmPch = wmPch;
    }

    /**
     * 获取货位号
     *
     * @return WM_HWH - 货位号
     */
    public String getWmHwh() {
        return wmHwh;
    }

    /**
     * 设置货位号
     *
     * @param wmHwh 货位号
     */
    public void setWmHwh(String wmHwh) {
        this.wmHwh = wmHwh;
    }

    /**
     * 获取库存状态编号
     *
     * @return WM_KCZT_BH - 库存状态编号
     */
    public String getWmKcztBh() {
        return wmKcztBh;
    }

    /**
     * 设置库存状态编号
     *
     * @param wmKcztBh 库存状态编号
     */
    public void setWmKcztBh(String wmKcztBh) {
        this.wmKcztBh = wmKcztBh;
    }

    /**
     * 获取出库价
     *
     * @return WM_CKJ - 出库价
     */
    public BigDecimal getWmCkj() {
        return wmCkj;
    }

    /**
     * 设置出库价
     *
     * @param wmCkj 出库价
     */
    public void setWmCkj(BigDecimal wmCkj) {
        this.wmCkj = wmCkj;
    }

    /**
     * 获取出库原数量
     *
     * @return WM_CKYSL - 出库原数量
     */
    public BigDecimal getWmCkysl() {
        return wmCkysl;
    }

    /**
     * 设置出库原数量
     *
     * @param wmCkysl 出库原数量
     */
    public void setWmCkysl(BigDecimal wmCkysl) {
        this.wmCkysl = wmCkysl;
    }

    /**
     * 获取过帐数量
     *
     * @return WM_GZSL - 过帐数量
     */
    public BigDecimal getWmGzsl() {
        return wmGzsl;
    }

    /**
     * 设置过帐数量
     *
     * @param wmGzsl 过帐数量
     */
    public void setWmGzsl(BigDecimal wmGzsl) {
        this.wmGzsl = wmGzsl;
    }

    /**
     * 获取备注
     *
     * @return WM_BZ - 备注
     */
    public String getWmBz() {
        return wmBz;
    }

    /**
     * 设置备注
     *
     * @param wmBz 备注
     */
    public void setWmBz(String wmBz) {
        this.wmBz = wmBz;
    }

    /**
     * 获取修改日期
     *
     * @return WM_XGRQ - 修改日期
     */
    public String getWmXgrq() {
        return wmXgrq;
    }

    /**
     * 设置修改日期
     *
     * @param wmXgrq 修改日期
     */
    public void setWmXgrq(String wmXgrq) {
        this.wmXgrq = wmXgrq;
    }

    /**
     * 获取修改时间
     *
     * @return WM_XGSJ - 修改时间
     */
    public String getWmXgsj() {
        return wmXgsj;
    }

    /**
     * 设置修改时间
     *
     * @param wmXgsj 修改时间
     */
    public void setWmXgsj(String wmXgsj) {
        this.wmXgsj = wmXgsj;
    }

    /**
     * 获取修改人
     *
     * @return WM_XGR - 修改人
     */
    public String getWmXgr() {
        return wmXgr;
    }

    /**
     * 设置修改人
     *
     * @param wmXgr 修改人
     */
    public void setWmXgr(String wmXgr) {
        this.wmXgr = wmXgr;
    }

    /**
     * 获取修改人姓名
     *
     * @return WM_XGRXM - 修改人姓名
     */
    public String getWmXgrxm() {
        return wmXgrxm;
    }

    /**
     * 设置修改人姓名
     *
     * @param wmXgrxm 修改人姓名
     */
    public void setWmXgrxm(String wmXgrxm) {
        this.wmXgrxm = wmXgrxm;
    }

    /**
     * 获取po明细表行号
     *
     * @return PO_LINE_NO - po明细表行号
     */
    public String getPoLineNo() {
        return poLineNo;
    }

    /**
     * 设置po明细表行号
     *
     * @param poLineNo po明细表行号
     */
    public void setPoLineNo(String poLineNo) {
        this.poLineNo = poLineNo;
    }
}