//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdServerTips;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdServerTipsMapper extends BaseMapper<GaiaSdServerTips> {
    List<GaiaSdServerTips> selectTipList(@Param("clientId") String clientId, @Param("storeCode") String storeCode, @Param("compclassList") List<String> compclassList, @Param("proIdList") List<String> proIdList, @Param("partformList") List<String> partformList);
}
