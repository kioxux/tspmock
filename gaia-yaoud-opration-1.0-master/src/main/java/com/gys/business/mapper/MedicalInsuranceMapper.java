package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdBatchChange;
import com.gys.business.service.data.GetSaleReturnToData;
import com.gys.business.service.data.RebornData;
import com.gys.business.service.data.StockChangeInfo;
import com.gys.common.YiLianDaModel.GYSXXGL.Gysxxlb;
import com.gys.common.YiLianDaModel.YPXXGL.Ypxxlb;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MedicalInsuranceMapper {
    RebornData selectMedProList(@Param("client")String client,@Param("brId") String brId,@Param("inData") RebornData inData);
    RebornData selectMedProList2(@Param("client")String client,@Param("brId") String brId,@Param("inData") RebornData inData);
    List<Ypxxlb> selectPushProduct(@Param("client")String client,@Param("brId") String brId,@Param("startDate")String startDate,@Param("endDate") String endDate);
    List<Gysxxlb> selectPushSupplier(@Param("client")String client,@Param("brId") String brId,@Param("startDate")String startDate,@Param("endDate") String endDate);
    List<RebornData> matchMedProList(@Param("client") String client,@Param("brId") String brId, @Param("inData") List<GaiaSdBatchChange> inData);
    List<StockChangeInfo> selectReturnBatchChange(@Param("client") String client, @Param("brId") String brId,@Param("billNo") String billNo, @Param("inData") List<GetSaleReturnToData> inData);
}
