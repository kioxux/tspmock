package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.*;

@Data
@Table(name = "MIB_SETL_DETAIL")
public class MibSetlDetail implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店
     */
    @Column(name = "BR_ID")
    private String brId;

    @Column(name = "BILL_NO")
    private String billNo;

    @Column(name = "MSG_ID")
    private String msgId;

    @Column(name = "FUND_PAY_TYPE")
    private String fundPayType;

    @Column(name = "INSCP_SCP_AMT")
    private String inscpScpAmt;

    @Column(name = "CRT_PAYB_LMT_AMT")
    private String crtPaybLmtAmt;

    @Column(name = "FUND_PAYAMT")
    private String fundPayamt;

    @Column(name = "FUND_PAY_TYPE_NAME")
    private String fundPayTypeName;

    @Column(name = "SETL_PROC_INFO")
    private String setlProcInfo;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取门店
     *
     * @return BR_ID - 门店
     */
    public String getBrId() {
        return brId;
    }

    /**
     * 设置门店
     *
     * @param brId 门店
     */
    public void setBrId(String brId) {
        this.brId = brId;
    }

    /**
     * @return BILL_NO
     */
    public String getBillNo() {
        return billNo;
    }

    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * @return MSG_ID
     */
    public String getMsgId() {
        return msgId;
    }

    /**
     * @param msgId
     */
    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    /**
     * @return FUND_PAY_TYPE
     */
    public String getFundPayType() {
        return fundPayType;
    }

    /**
     * @param fundPayType
     */
    public void setFundPayType(String fundPayType) {
        this.fundPayType = fundPayType;
    }

    /**
     * @return INSCP_SCP_AMT
     */
    public String getInscpScpAmt() {
        return inscpScpAmt;
    }

    /**
     * @param inscpScpAmt
     */
    public void setInscpScpAmt(String inscpScpAmt) {
        this.inscpScpAmt = inscpScpAmt;
    }

    /**
     * @return CRT_PAYB_LMT_AMT
     */
    public String getCrtPaybLmtAmt() {
        return crtPaybLmtAmt;
    }

    /**
     * @param crtPaybLmtAmt
     */
    public void setCrtPaybLmtAmt(String crtPaybLmtAmt) {
        this.crtPaybLmtAmt = crtPaybLmtAmt;
    }

    /**
     * @return FUND_PAYAMT
     */
    public String getFundPayamt() {
        return fundPayamt;
    }

    /**
     * @param fundPayamt
     */
    public void setFundPayamt(String fundPayamt) {
        this.fundPayamt = fundPayamt;
    }

    /**
     * @return FUND_PAY_TYPE_NAME
     */
    public String getFundPayTypeName() {
        return fundPayTypeName;
    }

    /**
     * @param fundPayTypeName
     */
    public void setFundPayTypeName(String fundPayTypeName) {
        this.fundPayTypeName = fundPayTypeName;
    }

    /**
     * @return SETL_PROC_INFO
     */
    public String getSetlProcInfo() {
        return setlProcInfo;
    }

    /**
     * @param setlProcInfo
     */
    public void setSetlProcInfo(String setlProcInfo) {
        this.setlProcInfo = setlProcInfo;
    }
}