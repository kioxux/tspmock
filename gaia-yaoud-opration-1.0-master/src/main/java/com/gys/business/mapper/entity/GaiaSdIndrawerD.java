//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_INDRAWER_D"
)
public class GaiaSdIndrawerD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSID_VOUCHER_ID"
    )
    private String gsidVoucherId;
    @Id
    @Column(
            name = "GSID_DATE"
    )
    private String gsidDate;
    @Id
    @Column(
            name = "GSID_SERIAL"
    )
    private String gsidSerial;
    @Column(
            name = "GSID_PRO_ID"
    )
    private String gsidProId;
    @Column(
            name = "GSID_BATCH_NO"
    )
    private String gsidBatchNo;
    @Column(
            name = "GSID_BATCH"
    )
    private String gsidBatch;
    @Column(
            name = "GSID_VALID_DATE"
    )
    private String gsidValidDate;
    @Column(
            name = "GSID_QTY"
    )
    private String gsidQty;
    @Column(
            name = "GSID_STATUS"
    )
    private String gsidStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdIndrawerD() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsidVoucherId() {
        return this.gsidVoucherId;
    }

    public void setGsidVoucherId(String gsidVoucherId) {
        this.gsidVoucherId = gsidVoucherId;
    }

    public String getGsidDate() {
        return this.gsidDate;
    }

    public void setGsidDate(String gsidDate) {
        this.gsidDate = gsidDate;
    }

    public String getGsidSerial() {
        return this.gsidSerial;
    }

    public void setGsidSerial(String gsidSerial) {
        this.gsidSerial = gsidSerial;
    }

    public String getGsidProId() {
        return this.gsidProId;
    }

    public void setGsidProId(String gsidProId) {
        this.gsidProId = gsidProId;
    }

    public String getGsidBatchNo() {
        return this.gsidBatchNo;
    }

    public void setGsidBatchNo(String gsidBatchNo) {
        this.gsidBatchNo = gsidBatchNo;
    }

    public String getGsidBatch() {
        return this.gsidBatch;
    }

    public void setGsidBatch(String gsidBatch) {
        this.gsidBatch = gsidBatch;
    }

    public String getGsidValidDate() {
        return this.gsidValidDate;
    }

    public void setGsidValidDate(String gsidValidDate) {
        this.gsidValidDate = gsidValidDate;
    }

    public String getGsidQty() {
        return this.gsidQty;
    }

    public void setGsidQty(String gsidQty) {
        this.gsidQty = gsidQty;
    }

    public String getGsidStatus() {
        return this.gsidStatus;
    }

    public void setGsidStatus(String gsidStatus) {
        this.gsidStatus = gsidStatus;
    }
}
