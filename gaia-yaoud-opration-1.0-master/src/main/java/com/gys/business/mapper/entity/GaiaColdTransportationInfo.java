package com.gys.business.mapper.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

/**
 * 委托配送冷藏运输信息表(GaiaColdTransportationInfo)实体类
 *
 * @author makejava
 * @since 2021-10-08 11:17:31
 */
@Data
public class GaiaColdTransportationInfo implements Serializable {

    /**
     * 冷藏_车辆类型
     */
    private String gsahTransportMode;
    /**
     * 冷藏_保温方式
     */
    private String gsahInsulationMode;
    /**
     * 冷藏_起运时间
     */
    private String gsahDepartureTime;
    /**
     * 冷藏_起运日期
     */
    private String gsahDepartureDate;
    /**
     * 冷藏_到货时间
     */
    private String gsahArriveDate;
    /**
     * 冷藏_运输人
     */
    private String gsahTransportEmp;
    /**
     * 冷藏_来货件数
     */
    private BigDecimal gsahBoxQty;
    /**
     * 冷藏_随货同行单票
     */
    private String gsahTxFollow;
    /**
     * 冷藏_途中温度达标
     */
    private String gsahTempFit;
    /**
     * 冷藏_起运温度
     */
    private String gsahDepartureTemperature;
    /**
     * 冷藏_到货温度
     */
    private String gsahArriveTemperature;

}
