package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_SD_SYT_OPERATION")
@Data
public class GaiaSdSytOperation implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CLIENT")
    private String client;

    @Column(name = "BR_ID")
    private String brId;

    /**
     * 渠道编号
     */
    @Column(name = "CHANNEL")
    private String channel;

    /**
     * 商户号
     */
    @Column(name = "MER_ID")
    private String merId;

    /**
     * 商户名称
     */
    @Column(name = "MCHT_NAME")
    private String mchtName;

    /**
     * 收单流水号，供后续退货或撤销使用
     */
    @Column(name = "TXN_SEQ_ID")
    private String txnSeqId;

    /**
     * 平台交易日期
     */
    @Column(name = "TXN_DATE")
    private String txnDate;

    /**
     * 平台交易时间
     */
    @Column(name = "TXN_TIME")
    private String txnTime;

    /**
     * 交易类型 01:消费，34：退款
     */
    @Column(name = "TRANS_TYPE")
    private String transType;

    /**
     * 支付类型 15：微信被扫，20：支付宝被扫
     */
    @Column(name = "PAY_TYPE")
    private String payType;

    /**
     * 订单金额
     */
    @Column(name = "ORDER_AMOUNT")
    private BigDecimal orderAmount;

    /**
     * 设备信息
     */
    @Column(name = "MACHINE_INFO")
    private String machineInfo;

    /**
     * 订单编号(外部)
     */
    @Column(name = "OUTER_NUMBER")
    private String outerNumber;

    /**
     * 订单时间
     */
    @Column(name = "OUTER_DT_TM")
    private String outerDtTm;

    /**
     * 币种 156：人民币
     */
    @Column(name = "CURRENCY_CODE")
    private String currencyCode;

    /**
     * 订单编号（内部）
     */
    @Column(name = "BILL_NO")
    private String billNo;

    /**
     * 操作名称
     */
    @Column(name = "OPERATION")
    private String operation;

    /**
     * 交易结果 0：失败，1：成功
     */
    @Column(name = "RESULT")
    private String result;

    /**
     * 结果内容
     */
    @Column(name = "RESULT_CONTENT")
    private String resultContent;

    /**
     * 实付金额
     */
    @Column(name = "REAL_PAY_AMOUNT")
    private BigDecimal realPayAmount;
    /**
     * 折扣金额
     */
    @Column(name = "DISCOUNT_AMOUNT")
    private BigDecimal discountAmount;

    /**
     * 实付金额
     */
    @Column(name = "TXN_STA")
    private String txnSta;

    /**
     * 退货原交易流水号
     */
    @Column(name = "INIT_ORDER_NUMBER")
    private String initOrderNumber;

    @Column(name = "UPDATE_DATE")
    private Date updateDate;
    @Column(name = "UPDATE_EMP")
    private String updateEmp;
    @Column(name = "CREATE_DATE")
    private Date createDate;
    @Column(name = "CREATE_EMP")
    private String createEmp;

    private static final long serialVersionUID = 1L;
}