package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSaletaskHplanNew;
import com.gys.business.mapper.entity.GaiaSaletaskHplanSystem;
import com.gys.business.mapper.entity.GaiaSaletaskPlanSto;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSaletaskPlanStoMapper extends BaseMapper<GaiaSaletaskPlanSto> {

    List<GaiaSaletaskPlanSto> selectSaleStorePlanByClient(GaiaSaletaskHplanNew inData);
}