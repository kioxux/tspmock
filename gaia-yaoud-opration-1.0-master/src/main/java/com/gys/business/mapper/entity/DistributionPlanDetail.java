package com.gys.business.mapper.entity;

import com.gys.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 13:58
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DistributionPlanDetail extends BaseEntity {
    /**加盟商*/
    private String client;
    /**铺货计划单号*/
    private String planCode;
    /**商品编码*/
    private String proSelfCode;
    /**门店ID*/
    private String storeId;
    /**门店名称*/
    private String storeName;
    /**计划数量*/
    private BigDecimal planQuantity;
    /**实际数量*/
    private BigDecimal actualQuantity;
    /**执行状态：0-未执行 1-已执行*/
    private Integer status;
}
