package com.gys.business.mapper.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description 零钱转入
 * @Author huxinxin
 * @Date 2021/7/29 14:06
 * @Version 1.0.0
 **/
@Data
@AllArgsConstructor
public class GaiaIntoMemberCard {
    /**
     * 零钱转入储值卡号
     */
    private String intoMemberCard;

    /**
     * 零钱转入金额
     */
    private BigDecimal intoMemberPrice;
}
