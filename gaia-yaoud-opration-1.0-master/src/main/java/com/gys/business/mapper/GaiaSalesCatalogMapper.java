package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSalesCatalog;
import com.gys.business.service.data.GaiaSalesCatalogInData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 销售目录维护表(GaiaSalesCatalog)表数据库访问层
 *
 * @author makejava
 * @since 2021-08-25 10:51:54
 */
public interface GaiaSalesCatalogMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaSalesCatalog queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaSalesCatalog> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaSalesCatalog 实例对象
     * @return 对象列表
     */
    List<GaiaSalesCatalog> queryAll(GaiaSalesCatalog gaiaSalesCatalog);

    /**
     * 新增数据
     *
     * @param gaiaSalesCatalog 实例对象
     * @return 影响行数
     */
    int insert(GaiaSalesCatalog gaiaSalesCatalog);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaSalesCatalog> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaSalesCatalog> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaSalesCatalog> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaSalesCatalog> entities);

    /**
     * 修改数据
     *
     * @param gaiaSalesCatalog 实例对象
     * @return 影响行数
     */
    int update(GaiaSalesCatalog gaiaSalesCatalog);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    int checkHas(GaiaSalesCatalog inData);

    List<GaiaSalesCatalog> list(GaiaSalesCatalogInData inData);

    GaiaSalesCatalog getProductInfo(GaiaSalesCatalogInData catalog);
}

