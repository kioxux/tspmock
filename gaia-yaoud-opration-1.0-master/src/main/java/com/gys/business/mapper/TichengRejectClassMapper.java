package com.gys.business.mapper;

import com.gys.business.mapper.entity.TichengRejectClass;
import com.gys.business.service.data.percentageplan.PlanRejectClass;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 销售提成方案剔除商品分类表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-11-12
 */
@Mapper
public interface TichengRejectClassMapper extends BaseMapper<TichengRejectClass> {
    void deleteClassByPid(@Param("pid") Integer pid);

    void batchPlanRejectClass(List<PlanRejectClass> list);

    int updateClientByPlanId();

}
