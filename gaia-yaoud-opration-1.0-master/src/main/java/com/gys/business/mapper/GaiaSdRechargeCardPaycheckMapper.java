package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRechargeCardPaycheck;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdRechargeCardPaycheckMapper extends BaseMapper<GaiaSdRechargeCardPaycheck> {
    String selectNextVoucherId(@Param("client") String client);

    /**
     * 根据日期 和 门店 查询 储值卡订单编码
     * @param map
     * @return
     */
    List<String> getAllByBillAndDate(Map<String, Object> map);

    void updateList(@Param("client") String client,@Param("brId") String depId,@Param("list") List<String> cardNo);

    List<String> verifyWhetherToReview(Map<String, Object> map);
}