package com.gys.business.mapper;

import com.gys.business.bosen.dto.BsInData;
import com.gys.business.bosen.dto.BsStockData;
import com.gys.business.bosen.form.InDataForm;
import com.gys.business.mapper.entity.WmKuCun;
import org.apache.ibatis.annotations.Param;

import java.util.LinkedHashMap;
import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/7 15:03
 **/
public interface GaiaWmKuCunMapper {

    WmKuCun getById(Long id);

    WmKuCun getUnique(WmKuCun cond);

    WmKuCun getKuCunInfo(WmKuCun cond);

    List<String> getDcCodeList(@Param("client") String client);

    /**
     * 获取当前库存数据
     */
    List<BsStockData> getCurrentStock(WmKuCun cond);

    /**
     * 获取仓库入库数据
     */
    List<BsInData> getDcInData(InDataForm dataForm);

    /**
     * 获取门店入库数据
     */
    List<BsInData> getStoreInData(InDataForm dataForm);

    /**
     * 获取仓库退供数据
     */
    List<BsInData> getDcTGData(InDataForm cond);

    /**
     * 获取出库销售数据
     */
    List<BsInData> getSaleData(InDataForm cond);

    /**
     * 获取仓库入库数据（万盛堂）
     * */
    List<LinkedHashMap<String, Object>> getCKInData(InDataForm dataForm);

    /**
     * 获取仓库库存数据（万盛堂）
     * */
    List<LinkedHashMap<String, Object>> getCKStockData(InDataForm dataForm);

    /**
     * 获取仓库销售数据（万盛堂）
     * */
    List<LinkedHashMap<String, Object>> getCKOutData(InDataForm dataForm);

    /**
     * 获取库存数量，按照商品sum WM_KYSL
     * 加盟商+地点+存储类型（A04+A05）+库存状态1000+可用数量
     * EXT_VALUE = 1  GAIA_BUSINESS_PRODUCT.PRO_YSB_CODE = 1的商品
     * */
    List<LinkedHashMap<String, Object>> getKYSL(@Param("client") String client, @Param("proSite") String proSite, @Param("config") Integer config);

    List<LinkedHashMap<String, Object>> getWMSConfig(@Param("client") String client, @Param("proSite") String proSite,@Param("configName") String configName);
}
