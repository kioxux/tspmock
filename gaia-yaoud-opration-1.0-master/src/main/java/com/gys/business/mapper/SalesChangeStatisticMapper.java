package com.gys.business.mapper;

import com.gys.business.controller.app.form.SalesChangeStatisticForm;
import com.gys.business.controller.app.vo.SalesChangeStatisticVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SalesChangeStatisticMapper {

    List<SalesChangeStatisticVO> salesChangeStatisticTop(@Param("queryForm") SalesChangeStatisticForm inData);

    List<SalesChangeStatisticVO> queryProductBusinessInfo(@Param("queryForm") SalesChangeStatisticForm inData);

    List<SalesChangeStatisticVO> querySaleData(@Param("queryForm") SalesChangeStatisticForm inData);

}