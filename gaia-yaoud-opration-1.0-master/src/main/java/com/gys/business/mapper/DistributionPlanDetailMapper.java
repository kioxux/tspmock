package com.gys.business.mapper;

import com.gys.business.mapper.entity.DistributionCountInfo;
import com.gys.business.mapper.entity.DistributionPlanDetail;
import com.gys.business.mapper.entity.NewDistributionPlan;

import java.util.List;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 14:24
 */
public interface DistributionPlanDetailMapper {

    DistributionPlanDetail getById(Long id);

    int add(DistributionPlanDetail distributionPlanDetail);

    int update(DistributionPlanDetail distributionPlanDetail);

    List<DistributionPlanDetail> findList(DistributionPlanDetail distributionPlanDetail);

    DistributionCountInfo getPlanCountInfo(NewDistributionPlan cond);

}
