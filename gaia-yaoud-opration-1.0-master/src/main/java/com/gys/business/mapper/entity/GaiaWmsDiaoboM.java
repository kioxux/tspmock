package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(
        name = "GAIA_WMS_DIAOBO_M"
)
public class GaiaWmsDiaoboM implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "WM_PSDH"
    )
    private String wmPsdh;
    @Column(
            name = "WM_SP_BM"
    )
    private String wmSpBm;
    @Column(
            name = "WM_PCH"
    )
    private String wmPch;
    @Column(
            name = "WM_KCZT_BH"
    )
    private String wmKcztBh;
    @Column(
            name = "WM_PSDFH"
    )
    private String wmPsdfh;
    @Column(
            name = "WM_CKJ"
    )
    private BigDecimal wmCkj;
    @Column(
            name = "WM_CKYSL"
    )
    private BigDecimal wmCkysl;
    @Column(
            name = "WM_GZSL"
    )
    private BigDecimal wmGzsl;
    @Column(
            name = "WM_XGRQ"
    )
    private String wmXgrq;
    @Column(
            name = "WM_XGSJ"
    )
    private String wmXgsj;
    @Column(
            name = "WM_XGR"
    )
    private String wmXgr;
    @Column(
            name = "PRO_SITE"
    )
    private String proSite;
    private static final long serialVersionUID = 1L;

}
