package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author xiayuan
 * 电子券业务条件设置表
 */
@Data
public class GaiaSdElectronConditionSet implements Serializable {
    private static final long serialVersionUID = -1928274062327265234L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 主题单号
     */
    private String gsetsId;

    /**
     * 业务单号
     */
    private String gsebsId;

    /**
     * 电子券活动号
     */
    private String gsebId;

    /**
     * 达到数量1
     */
    private String gsecsReachQty1;

    /**
     * 达到金额1
     */
    private String gsecsReachAmt1;

    /**
     * 送券数量1
     */
    private String gsecsResultQty1;

    /**
     * 达到数量2
     */
    private String gsecsReachQty2;

    /**
     * 达到金额2
     */
    private BigDecimal gsecsReachAmt2;

    /**
     * 送券数量2
     */
    private String gsecsResultQty2;

    /**
     * 是否重复   N为否，Y为是
     */
    private String gsecsFlag1;

    /**
     * 是否全场商品   N为否，Y为是
     */
    private String gsecsFlag2;

    /**
     * 商品组名
     */
    private String gsecsProGroup;

    /**
     * 创建日期
     */
    private String gsecsCreateDate;

    /**
     * 创建时间
     */
    private String gsecsCreateTime;

    /**
     * 是否有效
     */
    private String gsecsValid;


}