//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromCouponGrant;
import com.gys.business.service.data.PromCouponGrantOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdPromCouponGrantMapper extends BaseMapper<GaiaSdPromCouponGrant> {
    List<PromCouponGrantOutData> getDetail(PromoteInData inData);
}
