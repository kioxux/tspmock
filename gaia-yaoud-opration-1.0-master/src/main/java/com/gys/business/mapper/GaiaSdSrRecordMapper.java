package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSrRecord;
import com.gys.business.mapper.entity.SpecialClassOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdSrRecordMapper extends BaseMapper<GaiaSdSrRecord> {


    void insertBatchs(List<GaiaSdSrRecord> list);

    void batchSpecialClassPro(List<SpecialClassOutData> list);

}
