package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_NBYB_CATALOGUE")
public class GaiaNbybCatalogue implements Serializable {
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    private String VALUE1;

    private String VALUE2;

    private String VALUE3;

    private String VALUE4;

    private String VALUE5;

    private String VALUE6;

    private String VALUE7;

    private String VALUE8;

    private String VALUE9;

    private String VALUE10;

    private String VALUE11;

    private String VALUE12;

    private static final long serialVersionUID = 1L;

    /**
     * 获取id
     *
     * @return id - id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return VALUE1
     */
    public String getVALUE1() {
        return VALUE1;
    }

    /**
     * @param VALUE1
     */
    public void setVALUE1(String VALUE1) {
        this.VALUE1 = VALUE1;
    }

    /**
     * @return VALUE2
     */
    public String getVALUE2() {
        return VALUE2;
    }

    /**
     * @param VALUE2
     */
    public void setVALUE2(String VALUE2) {
        this.VALUE2 = VALUE2;
    }

    /**
     * @return VALUE3
     */
    public String getVALUE3() {
        return VALUE3;
    }

    /**
     * @param VALUE3
     */
    public void setVALUE3(String VALUE3) {
        this.VALUE3 = VALUE3;
    }

    /**
     * @return VALUE4
     */
    public String getVALUE4() {
        return VALUE4;
    }

    /**
     * @param VALUE4
     */
    public void setVALUE4(String VALUE4) {
        this.VALUE4 = VALUE4;
    }

    /**
     * @return VALUE5
     */
    public String getVALUE5() {
        return VALUE5;
    }

    /**
     * @param VALUE5
     */
    public void setVALUE5(String VALUE5) {
        this.VALUE5 = VALUE5;
    }

    /**
     * @return VALUE6
     */
    public String getVALUE6() {
        return VALUE6;
    }

    /**
     * @param VALUE6
     */
    public void setVALUE6(String VALUE6) {
        this.VALUE6 = VALUE6;
    }

    /**
     * @return VALUE7
     */
    public String getVALUE7() {
        return VALUE7;
    }

    /**
     * @param VALUE7
     */
    public void setVALUE7(String VALUE7) {
        this.VALUE7 = VALUE7;
    }

    /**
     * @return VALUE8
     */
    public String getVALUE8() {
        return VALUE8;
    }

    /**
     * @param VALUE8
     */
    public void setVALUE8(String VALUE8) {
        this.VALUE8 = VALUE8;
    }

    /**
     * @return VALUE9
     */
    public String getVALUE9() {
        return VALUE9;
    }

    /**
     * @param VALUE9
     */
    public void setVALUE9(String VALUE9) {
        this.VALUE9 = VALUE9;
    }

    /**
     * @return VALUE10
     */
    public String getVALUE10() {
        return VALUE10;
    }

    /**
     * @param VALUE10
     */
    public void setVALUE10(String VALUE10) {
        this.VALUE10 = VALUE10;
    }

    /**
     * @return VALUE11
     */
    public String getVALUE11() {
        return VALUE11;
    }

    /**
     * @param VALUE11
     */
    public void setVALUE11(String VALUE11) {
        this.VALUE11 = VALUE11;
    }

    /**
     * @return VALUE12
     */
    public String getVALUE12() {
        return VALUE12;
    }

    /**
     * @param VALUE12
     */
    public void setVALUE12(String VALUE12) {
        this.VALUE12 = VALUE12;
    }
}