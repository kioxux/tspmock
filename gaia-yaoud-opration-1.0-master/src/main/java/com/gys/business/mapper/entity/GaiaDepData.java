//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_DEP_DATA"
)
public class GaiaDepData implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "DEP_ID"
    )
    private String depId;
    @Column(
            name = "DEP_NAME"
    )
    private String depName;
    @Column(
            name = "DEP_HEAD_ID"
    )
    private String depHeadId;
    @Column(
            name = "DEP_HEAD_NAM"
    )
    private String depHeadNam;
    @Column(
            name = "DEP_CRE_DATE"
    )
    private String depCreDate;
    @Column(
            name = "DEP_CRE_TIME"
    )
    private String depCreTime;
    @Column(
            name = "DEP_CRE_ID"
    )
    private String depCreId;
    @Column(
            name = "DEP_MODI_DATE"
    )
    private String depModiDate;
    @Column(
            name = "DEP_MODI_TIME"
    )
    private String depModiTime;
    @Column(
            name = "DEP_MODI_ID"
    )
    private String depModiId;
    @Column(
            name = "DEP_DIS_DATE"
    )
    private String depDisDate;
    @Column(
            name = "STO_CHAIN_HEAD"
    )
    private String stoChainHead;
    @Column(
            name = "DEP_TYPE"
    )
    private String depType;

    @Column(
            name = "DEP_CLS"
    )
    private String depCls;


    @Column(
            name = "DEP_STATUS"
    )
    private String depStatus;


    private static final long serialVersionUID = 1L;

    public GaiaDepData() {
    }

    public String getClient() {
        return this.client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDepId() {
        return this.depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    public String getDepName() {
        return this.depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public String getDepHeadId() {
        return this.depHeadId;
    }

    public void setDepHeadId(String depHeadId) {
        this.depHeadId = depHeadId;
    }

    public String getDepHeadNam() {
        return this.depHeadNam;
    }

    public void setDepHeadNam(String depHeadNam) {
        this.depHeadNam = depHeadNam;
    }

    public String getDepCreDate() {
        return this.depCreDate;
    }

    public void setDepCreDate(String depCreDate) {
        this.depCreDate = depCreDate;
    }

    public String getDepCreTime() {
        return this.depCreTime;
    }

    public void setDepCreTime(String depCreTime) {
        this.depCreTime = depCreTime;
    }

    public String getDepCreId() {
        return this.depCreId;
    }

    public void setDepCreId(String depCreId) {
        this.depCreId = depCreId;
    }

    public String getDepModiDate() {
        return this.depModiDate;
    }

    public void setDepModiDate(String depModiDate) {
        this.depModiDate = depModiDate;
    }

    public String getDepModiTime() {
        return this.depModiTime;
    }

    public void setDepModiTime(String depModiTime) {
        this.depModiTime = depModiTime;
    }

    public String getDepModiId() {
        return this.depModiId;
    }

    public void setDepModiId(String depModiId) {
        this.depModiId = depModiId;
    }

    public String getDepDisDate() {
        return this.depDisDate;
    }

    public void setDepDisDate(String depDisDate) {
        this.depDisDate = depDisDate;
    }

    public String getStoChainHead() {
        return this.stoChainHead;
    }

    public void setStoChainHead(String stoChainHead) {
        this.stoChainHead = stoChainHead;
    }

    public String getDepType() {
        return this.depType;
    }

    public void setDepType(String depType) {
        this.depType = depType;
    }

    public String getDepCls() {
        return depCls;
    }

    public void setDepCls(String depCls) {
        this.depCls = depCls;
    }

    public String getDepStatus() {
        return depStatus;
    }

    public void setDepStatus(String depStatus) {
        this.depStatus = depStatus;
    }
}
