package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SpecialClassOutData implements Serializable {
    private String clientId;
    private String billNo;
    private String brId;
    private String gsscProId;
    private String gsscDate;
    private String gsscSerial;
    private String gsscBillSerial;
    private String specialClassType;
    private String cardId;
    private String name;
    private String sex;
    private String birthDate;
    private String phone;
    private String address;
    private String controllerCode;
}
