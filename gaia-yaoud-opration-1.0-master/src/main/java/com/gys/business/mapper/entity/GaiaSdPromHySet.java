package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 会员类促销会员价设置表
 */
@Table(name = "GAIA_SD_PROM_HY_SET")
@Data
public class GaiaSdPromHySet implements Serializable {
    private static final long serialVersionUID = -4085643919104050589L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPHS_VOUCHER_ID")
    private String gsphsVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPHS_SERIAL")
    private String gsphsSerial;

    /**
     * 编码
     */
    @Id
    @Column(name = "GSPHS_PRO_ID")
    private String gsphsProId;

    /**
     * 促销价
     */
    @Column(name = "GSPHS_PRICE")
    private BigDecimal gsphsPrice;

    /**
     * 促销折扣
     */
    @Column(name = "GSPHS_REBATE")
    private String gsphsRebate;

    /**
     * 是否积分
     */
    @Column(name = "GSPHS_INTE_FLAG")
    private String gsphsInteFlag;

    /**
     * 积分倍率
     */
    @Column(name = "GSPHS_INTE_RATE")
    private String gsphsInteRate;

}
