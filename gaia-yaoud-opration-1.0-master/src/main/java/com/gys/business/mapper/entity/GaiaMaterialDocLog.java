package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_MATERIAL_DOC_LOG")
public class GaiaMaterialDocLog implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 主键ID
     */
    @Id
    @Column(name = "GUID")
    private String guid;

    /**
     * 序号
     */
    @Id
    @Column(name = "GUID_NO")
    private String guidNo;

    /**
     * 业务单号
     */
    @Id
    @Column(name = "MAT_DN_ID")
    private String matDnId;

    /**
     * 业务类型
     */
    @Column(name = "MAT_TYPE")
    private String matType;

    /**
     * 过账日期
     */
    @Column(name = "MAT_POST_DATE")
    private String matPostDate;

    /**
     * 抬头备注
     */
    @Column(name = "MAT_HEAD_REMARK")
    private String matHeadRemark;

    /**
     * 商品编码
     */
    @Column(name = "MAT_PRO_CODE")
    private String matProCode;

    /**
     * 配送门店
     */
    @Column(name = "MAT_STO_CODE")
    private String matStoCode;

    /**
     * 地点
     */
    @Column(name = "MAT_SITE_CODE")
    private String matSiteCode;

    /**
     * 库存地点
     */
    @Column(name = "MAT_LOCATION_CODE")
    private String matLocationCode;

    /**
     * 转移库存地点
     */
    @Column(name = "MAT_LOCATION_TO")
    private String matLocationTo;

    /**
     * 批次
     */
    @Column(name = "MAT_BATCH")
    private String matBatch;

    /**
     * 交易数量
     */
    @Column(name = "MAT_QTY")
    private BigDecimal matQty;

    /**
     * 交易金额
     */
    @Column(name = "MAT_AMT")
    private BigDecimal matAmt;

    /**
     * 交易价格
     */
    @Column(name = "MAT_PRICE")
    private BigDecimal matPrice;

    /**
     * 基本计量单位
     */
    @Column(name = "MAT_UINT")
    private String matUint;

    /**
     * 借/贷标识
     */
    @Column(name = "MAT_DEBIT_CREDIT")
    private String matDebitCredit;

    /**
     * 订单号
     */
    @Column(name = "MAT_PO_ID")
    private String matPoId;

    /**
     * 订单行号
     */
    @Column(name = "MAT_PO_LINENO")
    private String matPoLineno;

    /**
     * 业务单行号
     */
    @Column(name = "MAT_DN_LINENO")
    private String matDnLineno;

    /**
     * 物料凭证行备注
     */
    @Column(name = "MAT_LINE_REMARK")
    private String matLineRemark;

    /**
     * 创建人
     */
    @Column(name = "MAT_CREATE_BY")
    private String matCreateBy;

    /**
     * 创建日期
     */
    @Column(name = "MAT_CREATE_DATE")
    private String matCreateDate;

    /**
     * 创建时间
     */
    @Column(name = "MAT_CREATE_TIME")
    private String matCreateTime;

    /**
     * 物料凭证号
     */
    @Column(name = "MAT_ID")
    private String matId;

    /**
     * 物料凭证年份
     */
    @Column(name = "MAT_YEAR")
    private String matYear;

    /**
     * 物料凭证行号
     */
    @Column(name = "MAT_LINE_NO")
    private String matLineNo;

    /**
     * 物料凭证生成状态（0-未处理，1-成功，2-失败）
     */
    @Column(name = "MAT_STATUS")
    private String matStatus;

    /**
     * 重处理人
     */
    @Column(name = "MAT_UPDATE_BY")
    private String matUpdateBy;

    /**
     * 重处理日期
     */
    @Column(name = "MAT_UPDATE_DATE")
    private String matUpdateDate;

    /**
     * 重处理时间
     */
    @Column(name = "MAT_UPDATE_TIME")
    private String matUpdateTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取主键ID
     *
     * @return GUID - 主键ID
     */
    public String getGuid() {
        return guid;
    }

    /**
     * 设置主键ID
     *
     * @param guid 主键ID
     */
    public void setGuid(String guid) {
        this.guid = guid;
    }

    /**
     * 获取序号
     *
     * @return GUID_NO - 序号
     */
    public String getGuidNo() {
        return guidNo;
    }

    /**
     * 设置序号
     *
     * @param guidNo 序号
     */
    public void setGuidNo(String guidNo) {
        this.guidNo = guidNo;
    }

    /**
     * 获取业务单号
     *
     * @return MAT_DN_ID - 业务单号
     */
    public String getMatDnId() {
        return matDnId;
    }

    /**
     * 设置业务单号
     *
     * @param matDnId 业务单号
     */
    public void setMatDnId(String matDnId) {
        this.matDnId = matDnId;
    }

    /**
     * 获取业务类型
     *
     * @return MAT_TYPE - 业务类型
     */
    public String getMatType() {
        return matType;
    }

    /**
     * 设置业务类型
     *
     * @param matType 业务类型
     */
    public void setMatType(String matType) {
        this.matType = matType;
    }

    /**
     * 获取过账日期
     *
     * @return MAT_POST_DATE - 过账日期
     */
    public String getMatPostDate() {
        return matPostDate;
    }

    /**
     * 设置过账日期
     *
     * @param matPostDate 过账日期
     */
    public void setMatPostDate(String matPostDate) {
        this.matPostDate = matPostDate;
    }

    /**
     * 获取抬头备注
     *
     * @return MAT_HEAD_REMARK - 抬头备注
     */
    public String getMatHeadRemark() {
        return matHeadRemark;
    }

    /**
     * 设置抬头备注
     *
     * @param matHeadRemark 抬头备注
     */
    public void setMatHeadRemark(String matHeadRemark) {
        this.matHeadRemark = matHeadRemark;
    }

    /**
     * 获取商品编码
     *
     * @return MAT_PRO_CODE - 商品编码
     */
    public String getMatProCode() {
        return matProCode;
    }

    /**
     * 设置商品编码
     *
     * @param matProCode 商品编码
     */
    public void setMatProCode(String matProCode) {
        this.matProCode = matProCode;
    }

    /**
     * 获取配送门店
     *
     * @return MAT_STO_CODE - 配送门店
     */
    public String getMatStoCode() {
        return matStoCode;
    }

    /**
     * 设置配送门店
     *
     * @param matStoCode 配送门店
     */
    public void setMatStoCode(String matStoCode) {
        this.matStoCode = matStoCode;
    }

    /**
     * 获取地点
     *
     * @return MAT_SITE_CODE - 地点
     */
    public String getMatSiteCode() {
        return matSiteCode;
    }

    /**
     * 设置地点
     *
     * @param matSiteCode 地点
     */
    public void setMatSiteCode(String matSiteCode) {
        this.matSiteCode = matSiteCode;
    }

    /**
     * 获取库存地点
     *
     * @return MAT_LOCATION_CODE - 库存地点
     */
    public String getMatLocationCode() {
        return matLocationCode;
    }

    /**
     * 设置库存地点
     *
     * @param matLocationCode 库存地点
     */
    public void setMatLocationCode(String matLocationCode) {
        this.matLocationCode = matLocationCode;
    }

    /**
     * 获取转移库存地点
     *
     * @return MAT_LOCATION_TO - 转移库存地点
     */
    public String getMatLocationTo() {
        return matLocationTo;
    }

    /**
     * 设置转移库存地点
     *
     * @param matLocationTo 转移库存地点
     */
    public void setMatLocationTo(String matLocationTo) {
        this.matLocationTo = matLocationTo;
    }

    /**
     * 获取批次
     *
     * @return MAT_BATCH - 批次
     */
    public String getMatBatch() {
        return matBatch;
    }

    /**
     * 设置批次
     *
     * @param matBatch 批次
     */
    public void setMatBatch(String matBatch) {
        this.matBatch = matBatch;
    }

    /**
     * 获取交易数量
     *
     * @return MAT_QTY - 交易数量
     */
    public BigDecimal getMatQty() {
        return matQty;
    }

    /**
     * 设置交易数量
     *
     * @param matQty 交易数量
     */
    public void setMatQty(BigDecimal matQty) {
        this.matQty = matQty;
    }

    /**
     * 获取交易金额
     *
     * @return MAT_AMT - 交易金额
     */
    public BigDecimal getMatAmt() {
        return matAmt;
    }

    /**
     * 设置交易金额
     *
     * @param matAmt 交易金额
     */
    public void setMatAmt(BigDecimal matAmt) {
        this.matAmt = matAmt;
    }

    /**
     * 获取交易价格
     *
     * @return MAT_PRICE - 交易价格
     */
    public BigDecimal getMatPrice() {
        return matPrice;
    }

    /**
     * 设置交易价格
     *
     * @param matPrice 交易价格
     */
    public void setMatPrice(BigDecimal matPrice) {
        this.matPrice = matPrice;
    }

    /**
     * 获取基本计量单位
     *
     * @return MAT_UINT - 基本计量单位
     */
    public String getMatUint() {
        return matUint;
    }

    /**
     * 设置基本计量单位
     *
     * @param matUint 基本计量单位
     */
    public void setMatUint(String matUint) {
        this.matUint = matUint;
    }

    /**
     * 获取借/贷标识
     *
     * @return MAT_DEBIT_CREDIT - 借/贷标识
     */
    public String getMatDebitCredit() {
        return matDebitCredit;
    }

    /**
     * 设置借/贷标识
     *
     * @param matDebitCredit 借/贷标识
     */
    public void setMatDebitCredit(String matDebitCredit) {
        this.matDebitCredit = matDebitCredit;
    }

    /**
     * 获取订单号
     *
     * @return MAT_PO_ID - 订单号
     */
    public String getMatPoId() {
        return matPoId;
    }

    /**
     * 设置订单号
     *
     * @param matPoId 订单号
     */
    public void setMatPoId(String matPoId) {
        this.matPoId = matPoId;
    }

    /**
     * 获取订单行号
     *
     * @return MAT_PO_LINENO - 订单行号
     */
    public String getMatPoLineno() {
        return matPoLineno;
    }

    /**
     * 设置订单行号
     *
     * @param matPoLineno 订单行号
     */
    public void setMatPoLineno(String matPoLineno) {
        this.matPoLineno = matPoLineno;
    }

    /**
     * 获取业务单行号
     *
     * @return MAT_DN_LINENO - 业务单行号
     */
    public String getMatDnLineno() {
        return matDnLineno;
    }

    /**
     * 设置业务单行号
     *
     * @param matDnLineno 业务单行号
     */
    public void setMatDnLineno(String matDnLineno) {
        this.matDnLineno = matDnLineno;
    }

    /**
     * 获取物料凭证行备注
     *
     * @return MAT_LINE_REMARK - 物料凭证行备注
     */
    public String getMatLineRemark() {
        return matLineRemark;
    }

    /**
     * 设置物料凭证行备注
     *
     * @param matLineRemark 物料凭证行备注
     */
    public void setMatLineRemark(String matLineRemark) {
        this.matLineRemark = matLineRemark;
    }

    /**
     * 获取创建人
     *
     * @return MAT_CREATE_BY - 创建人
     */
    public String getMatCreateBy() {
        return matCreateBy;
    }

    /**
     * 设置创建人
     *
     * @param matCreateBy 创建人
     */
    public void setMatCreateBy(String matCreateBy) {
        this.matCreateBy = matCreateBy;
    }

    /**
     * 获取创建日期
     *
     * @return MAT_CREATE_DATE - 创建日期
     */
    public String getMatCreateDate() {
        return matCreateDate;
    }

    /**
     * 设置创建日期
     *
     * @param matCreateDate 创建日期
     */
    public void setMatCreateDate(String matCreateDate) {
        this.matCreateDate = matCreateDate;
    }

    /**
     * 获取创建时间
     *
     * @return MAT_CREATE_TIME - 创建时间
     */
    public String getMatCreateTime() {
        return matCreateTime;
    }

    /**
     * 设置创建时间
     *
     * @param matCreateTime 创建时间
     */
    public void setMatCreateTime(String matCreateTime) {
        this.matCreateTime = matCreateTime;
    }

    /**
     * 获取物料凭证号
     *
     * @return MAT_ID - 物料凭证号
     */
    public String getMatId() {
        return matId;
    }

    /**
     * 设置物料凭证号
     *
     * @param matId 物料凭证号
     */
    public void setMatId(String matId) {
        this.matId = matId;
    }

    /**
     * 获取物料凭证年份
     *
     * @return MAT_YEAR - 物料凭证年份
     */
    public String getMatYear() {
        return matYear;
    }

    /**
     * 设置物料凭证年份
     *
     * @param matYear 物料凭证年份
     */
    public void setMatYear(String matYear) {
        this.matYear = matYear;
    }

    /**
     * 获取物料凭证行号
     *
     * @return MAT_LINE_NO - 物料凭证行号
     */
    public String getMatLineNo() {
        return matLineNo;
    }

    /**
     * 设置物料凭证行号
     *
     * @param matLineNo 物料凭证行号
     */
    public void setMatLineNo(String matLineNo) {
        this.matLineNo = matLineNo;
    }

    /**
     * 获取物料凭证生成状态（0-未处理，1-成功，2-失败）
     *
     * @return MAT_STATUS - 物料凭证生成状态（0-未处理，1-成功，2-失败）
     */
    public String getMatStatus() {
        return matStatus;
    }

    /**
     * 设置物料凭证生成状态（0-未处理，1-成功，2-失败）
     *
     * @param matStatus 物料凭证生成状态（0-未处理，1-成功，2-失败）
     */
    public void setMatStatus(String matStatus) {
        this.matStatus = matStatus;
    }

    /**
     * 获取重处理人
     *
     * @return MAT_UPDATE_BY - 重处理人
     */
    public String getMatUpdateBy() {
        return matUpdateBy;
    }

    /**
     * 设置重处理人
     *
     * @param matUpdateBy 重处理人
     */
    public void setMatUpdateBy(String matUpdateBy) {
        this.matUpdateBy = matUpdateBy;
    }

    /**
     * 获取重处理日期
     *
     * @return MAT_UPDATE_DATE - 重处理日期
     */
    public String getMatUpdateDate() {
        return matUpdateDate;
    }

    /**
     * 设置重处理日期
     *
     * @param matUpdateDate 重处理日期
     */
    public void setMatUpdateDate(String matUpdateDate) {
        this.matUpdateDate = matUpdateDate;
    }

    /**
     * 获取重处理时间
     *
     * @return MAT_UPDATE_TIME - 重处理时间
     */
    public String getMatUpdateTime() {
        return matUpdateTime;
    }

    /**
     * 设置重处理时间
     *
     * @param matUpdateTime 重处理时间
     */
    public void setMatUpdateTime(String matUpdateTime) {
        this.matUpdateTime = matUpdateTime;
    }
}