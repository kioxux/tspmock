package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaBasicsGroupInfo;
import com.gys.common.base.BaseMapper;


public interface GaiaBasicsGroupInfoMapper extends BaseMapper<GaiaBasicsGroupInfo> {

}