package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaTAccount;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaTAccountMapper extends BaseMapper<GaiaTAccount> {

    List<GaiaTAccount> findByClientAndStoCode(@Param("client")   String client,   @Param("stoCode") String stoCode);

    String getJdSecret(String appKey);

    GaiaTAccount selectStoCodeByPlatformsAndPublicCodeAndAppId(@Param("platforms") String platforms, @Param("publicCode") String publicCode, @Param("appId") String appId);

    String getJdTokenByVenderId(String venderId);

    void updateJdToken(@Param("venderId") String venderId, @Param("token") String token);

    GaiaTAccount getAccountByVenderId(String venderId);

    String getAppSecretByToken(String token);
}
