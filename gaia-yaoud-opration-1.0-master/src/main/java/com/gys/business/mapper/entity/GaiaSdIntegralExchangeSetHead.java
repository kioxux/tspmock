package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 门店积分换购商品设置主表
 * @TableName GAIA_SD_INTEGRAL_EXCHANGE_SET_HEAD
 */
@Data
public class GaiaSdIntegralExchangeSetHead implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 门店ID
     */
    private String stoCode;

    /**
     * 门店简称或名称
     */
    private String stoName;

    /**
     * 门店类别
     */
    private String stoGroup;

    /**
     * 会员卡等级
     */
    private String memberClass;

    /**
     * 开始日期
     */
    private Date startDate;

    /**
     * 结束日期
     */
    private Date endDate;

    /**
     * 日期频率
     */
    private String dateFrequency;

    /**
     * 星期频率
     */
    private String weekFrequency;

    /**
     * 是否设置时间段：0-否 1-是
     */
    private Integer timeInterval;

    /**
     * 状态：0-未审核 1-已审核
     */
    private Integer status;

    /**
     * 删除标记：0-未删除 1-已删除
     */
    private Integer deleteFlag;

    /**
     * 创建者
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateUser;

    /**
     * 更新时间
     */
    private Date updateTime;

}