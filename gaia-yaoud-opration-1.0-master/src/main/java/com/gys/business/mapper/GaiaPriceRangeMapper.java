package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaPriceRange;
import com.gys.business.service.data.PriceRangeInData;
import com.gys.business.service.data.PriceRangeOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaPriceRangeMapper extends BaseMapper<GaiaPriceRange> {
    List<Map<String, String>> getAreaList();

    List<PriceRangeOutData> getPriceRangeByClient(PriceRangeInData priceRangeInData);

    List<PriceRangeOutData> getClient();

    void savePriceRange(GaiaPriceRange gaiaPriceRange);

    void insertPriceRange(GaiaPriceRange priceRange);

    String selectUserName(@Param("client") String client,@Param("userId") String userId);

    GaiaPriceRange getPriceRange(@Param("client") String client,@Param("rangeLevel") String rangeLevel);
}