package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaAuthconfiData;
import com.gys.business.service.data.CompadmDcOutData;
import com.gys.business.service.data.GetDoctorOutData;
import com.gys.business.service.data.GetEmpOutData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface GaiaAuthconfiDataMapper extends BaseMapper<GaiaAuthconfiData> {
    List<GetEmpOutData> queryEmp(GetLoginOutData inData);

    List<GetDoctorOutData> queryDoctor(GetLoginOutData inData);

    List<GetDoctorOutData> queryDoctorByClient(Map map);

    List<GetDoctorOutData> queryDruggist(GetLoginOutData inData);

    List<CompadmDcOutData> selectAuthCompadmList(@Param("clientId") String clientId, @Param("userId") String userId);

    CompadmDcOutData selectCompadmById(@Param("clientId") String clientId, @Param("stoChineHead") String stoChineHead);

    List<GaiaAuthconfiData> getAllByClient(@Param("client") String client);

    List<GetEmpOutData> queryAllEmp(@Param("client")String client,@Param("content") String content);

    List<GetEmpOutData> pharmacist(GetLoginOutData inData);

    List<Map<String,Object>> selectStaff(@Param("client")String client, @Param("brId")String brId,@Param("staff") ArrayList staff);

    List<GaiaAuthconfiData> getDcInfoRole(@Param("client") String client, @Param("groupId") String groupId);

    List<Map<String,Object>> getGroupOrScManagerList(@Param("client") String client, @Param("userId") String userId, @Param("groupId") String groupId);
}
