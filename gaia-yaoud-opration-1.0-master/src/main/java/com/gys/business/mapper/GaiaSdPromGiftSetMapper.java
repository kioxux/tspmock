package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromGiftSet;
import com.gys.business.service.data.PromGiftSetOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromGiftSetMapper extends BaseMapper<GaiaSdPromGiftSet> {
    List<PromGiftSetOutData> getDetail(PromoteInData inData);

    List<GaiaSdPromGiftSet> getAllByClient(@Param("client") String client, @Param("nowDate") String nowDate,@Param("brId")String brId);
}
