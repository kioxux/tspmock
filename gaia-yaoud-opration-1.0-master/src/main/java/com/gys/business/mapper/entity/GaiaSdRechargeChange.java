package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xiaoyuan on 2021/7/14
 */
@Data
public class GaiaSdRechargeChange implements Serializable {
    private static final long serialVersionUID = -8311090297746287457L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 储值卡卡号
     */
    private String gsrcAccountId;

    /**
     * 异动门店
     */
    private String gsrcBrId;

    /**
     * 异动单号
     */
    private String gsrcVoucherId;

    /**
     * 异动日期
     */
    private String gsrcDate;

    /**
     * 异动时间
     */
    private String gsrcTime;

    /**
     * 异动类型 1充值，2退款，3消费，4退货
     */
    private String gsrcType;

    /**
     * 储值卡初始金额
     */
    private BigDecimal gsrcInitialAmt;

    /**
     * 储值卡异动金额
     */
    private BigDecimal gsrcChangeAmt;

    /**
     * 储值卡结果金额
     */
    private BigDecimal gsrcResultAmt;

    /**
     * 异动人员
     */
    private String gsrcEmp;
}
