package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_WMS_HUDIAO_Z")
public class GaiaWmsHudiaoZ implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 调入门店
     */
    @Column(name = "WM_DRMD")
    private String wmDrmd;

    /**
     * 调出门店
     */
    @Column(name = "WM_DQMD")
    private String wmDqmd;

    /**
     * 调剂单号
     */
    @Column(name = "WM_TJDH")
    private String wmTjdh;

    /**
     * 创建日期
     */
    @Column(name = "WM_CJRQ")
    private String wmCjrq;

    /**
     * 创建时间
     */
    @Column(name = "WM_CJSJ")
    private String wmCjsj;

    /**
     * 创建人
     */
    @Column(name = "WM_CJR")
    private String wmCjr;

    /**
     * 调出确认日期
     */
    @Column(name = "WM_DQRQ")
    private String wmDqrq;

    /**
     * 调出确认时间
     */
    @Column(name = "WM_DQSJ")
    private String wmDqsj;

    /**
     * 调出确认人
     */
    @Column(name = "WM_DQR")
    private String wmDqr;

    /**
     * 调入确认日期
     */
    @Column(name = "WM_DRRQ")
    private String wmDrrq;

    /**
     * 调入确认时间
     */
    @Column(name = "WM_DRSJ")
    private String wmDrsj;

    /**
     * 调入确认人
     */
    @Column(name = "WM_DRR")
    private String wmDrr;

    /**
     * 关闭日期
     */
    @Column(name = "WM_GBRQ")
    private String wmGbrq;

    /**
     * 关闭时间
     */
    @Column(name = "WM_GBSJ")
    private String wmGbsj;

    /**
     * 关闭人
     */
    @Column(name = "WM_GBR")
    private String wmGbr;

    /**
     * 单据状态
     */
    @Column(name = "WM_DJZT")
    private String wmDjzt;

    /**
     * 审核人
     */
    @Column(name = "WM_SHR")
    private String wmShr;

    private static final long serialVersionUID = 1L;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取调入门店
     *
     * @return WM_DRMD - 调入门店
     */
    public String getWmDrmd() {
        return wmDrmd;
    }

    /**
     * 设置调入门店
     *
     * @param wmDrmd 调入门店
     */
    public void setWmDrmd(String wmDrmd) {
        this.wmDrmd = wmDrmd;
    }

    /**
     * 获取调出门店
     *
     * @return WM_DQMD - 调出门店
     */
    public String getWmDqmd() {
        return wmDqmd;
    }

    /**
     * 设置调出门店
     *
     * @param wmDqmd 调出门店
     */
    public void setWmDqmd(String wmDqmd) {
        this.wmDqmd = wmDqmd;
    }

    /**
     * 获取调剂单号
     *
     * @return WM_TJDH - 调剂单号
     */
    public String getWmTjdh() {
        return wmTjdh;
    }

    /**
     * 设置调剂单号
     *
     * @param wmTjdh 调剂单号
     */
    public void setWmTjdh(String wmTjdh) {
        this.wmTjdh = wmTjdh;
    }

    /**
     * 获取创建日期
     *
     * @return WM_CJRQ - 创建日期
     */
    public String getWmCjrq() {
        return wmCjrq;
    }

    /**
     * 设置创建日期
     *
     * @param wmCjrq 创建日期
     */
    public void setWmCjrq(String wmCjrq) {
        this.wmCjrq = wmCjrq;
    }

    /**
     * 获取创建时间
     *
     * @return WM_CJSJ - 创建时间
     */
    public String getWmCjsj() {
        return wmCjsj;
    }

    /**
     * 设置创建时间
     *
     * @param wmCjsj 创建时间
     */
    public void setWmCjsj(String wmCjsj) {
        this.wmCjsj = wmCjsj;
    }

    /**
     * 获取创建人
     *
     * @return WM_CJR - 创建人
     */
    public String getWmCjr() {
        return wmCjr;
    }

    /**
     * 设置创建人
     *
     * @param wmCjr 创建人
     */
    public void setWmCjr(String wmCjr) {
        this.wmCjr = wmCjr;
    }

    /**
     * 获取调出确认日期
     *
     * @return WM_DQRQ - 调出确认日期
     */
    public String getWmDqrq() {
        return wmDqrq;
    }

    /**
     * 设置调出确认日期
     *
     * @param wmDqrq 调出确认日期
     */
    public void setWmDqrq(String wmDqrq) {
        this.wmDqrq = wmDqrq;
    }

    /**
     * 获取调出确认时间
     *
     * @return WM_DQSJ - 调出确认时间
     */
    public String getWmDqsj() {
        return wmDqsj;
    }

    /**
     * 设置调出确认时间
     *
     * @param wmDqsj 调出确认时间
     */
    public void setWmDqsj(String wmDqsj) {
        this.wmDqsj = wmDqsj;
    }

    /**
     * 获取调出确认人
     *
     * @return WM_DQR - 调出确认人
     */
    public String getWmDqr() {
        return wmDqr;
    }

    /**
     * 设置调出确认人
     *
     * @param wmDqr 调出确认人
     */
    public void setWmDqr(String wmDqr) {
        this.wmDqr = wmDqr;
    }

    /**
     * 获取调入确认日期
     *
     * @return WM_DRRQ - 调入确认日期
     */
    public String getWmDrrq() {
        return wmDrrq;
    }

    /**
     * 设置调入确认日期
     *
     * @param wmDrrq 调入确认日期
     */
    public void setWmDrrq(String wmDrrq) {
        this.wmDrrq = wmDrrq;
    }

    /**
     * 获取调入确认时间
     *
     * @return WM_DRSJ - 调入确认时间
     */
    public String getWmDrsj() {
        return wmDrsj;
    }

    /**
     * 设置调入确认时间
     *
     * @param wmDrsj 调入确认时间
     */
    public void setWmDrsj(String wmDrsj) {
        this.wmDrsj = wmDrsj;
    }

    /**
     * 获取调入确认人
     *
     * @return WM_DRR - 调入确认人
     */
    public String getWmDrr() {
        return wmDrr;
    }

    /**
     * 设置调入确认人
     *
     * @param wmDrr 调入确认人
     */
    public void setWmDrr(String wmDrr) {
        this.wmDrr = wmDrr;
    }

    /**
     * 获取关闭日期
     *
     * @return WM_GBRQ - 关闭日期
     */
    public String getWmGbrq() {
        return wmGbrq;
    }

    /**
     * 设置关闭日期
     *
     * @param wmGbrq 关闭日期
     */
    public void setWmGbrq(String wmGbrq) {
        this.wmGbrq = wmGbrq;
    }

    /**
     * 获取关闭时间
     *
     * @return WM_GBSJ - 关闭时间
     */
    public String getWmGbsj() {
        return wmGbsj;
    }

    /**
     * 设置关闭时间
     *
     * @param wmGbsj 关闭时间
     */
    public void setWmGbsj(String wmGbsj) {
        this.wmGbsj = wmGbsj;
    }

    /**
     * 获取关闭人
     *
     * @return WM_GBR - 关闭人
     */
    public String getWmGbr() {
        return wmGbr;
    }

    /**
     * 设置关闭人
     *
     * @param wmGbr 关闭人
     */
    public void setWmGbr(String wmGbr) {
        this.wmGbr = wmGbr;
    }

    /**
     * 获取单据状态
     *
     * @return WM_DJZT - 单据状态
     */
    public String getWmDjzt() {
        return wmDjzt;
    }

    /**
     * 设置单据状态
     *
     * @param wmDjzt 单据状态
     */
    public void setWmDjzt(String wmDjzt) {
        this.wmDjzt = wmDjzt;
    }

    /**
     * 获取审核人
     *
     * @return WM_SHR - 审核人
     */
    public String getWmShr() {
        return wmShr;
    }

    /**
     * 设置审核人
     *
     * @param wmShr 审核人
     */
    public void setWmShr(String wmShr) {
        this.wmShr = wmShr;
    }
}