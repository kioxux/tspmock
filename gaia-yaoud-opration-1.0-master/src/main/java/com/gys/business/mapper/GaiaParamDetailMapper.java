package com.gys.business.mapper;

import com.gys.business.mapper.entity.ParamDetailData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GaiaParamDetailMapper extends BaseMapper<ParamDetailData> {

    /**
     * 获取当前加盟商下的所有门店的设置条件
     * @param client
     * @return
     */
    List<ParamDetailData> getAllByClient(@Param("client") String client);
}
