package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromChmedSet;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author xiaoyuan
 */
public interface GaiaSdPromChmedSetMapper {

    List<GaiaSdPromChmedSet> getAllByClientAndBrId(@Param("client") String client, @Param("brId") String depId,@Param("nowDate") String nowDate);
}
