package com.gys.business.mapper;

import com.gys.business.mapper.entity.StockDiff;
import com.gys.business.service.data.StockCompareRes;
import com.gys.business.service.data.StockDiffCondition;
import com.gys.business.service.data.StockDiffRes;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 月度库存表
 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-07-13
 */
@Mapper
public interface StockDiffMapper extends BaseMapper<StockDiff> {
    List<StockDiffRes> selectByCondition(StockDiffCondition inData);

//    List<StockMouthRes> selectByCondition(StockMouthCaculateCondition inData);
}

