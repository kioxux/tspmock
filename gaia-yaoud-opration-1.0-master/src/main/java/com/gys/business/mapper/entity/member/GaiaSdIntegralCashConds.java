package com.gys.business.mapper.entity.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import javax.persistence.*;

/**
 * 积分抵现活动前提商品
 *
 * @author wu mao yin
 * @date 2021/12/9 15:16
 */
@Data
public class GaiaSdIntegralCashConds implements Serializable {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 积分抵现活动单号
     */
    private String gsiccVoucherId;

    /**
     * 门店
     */
    private String gsiccSto;

    /**
     * 序号
     */
    private Long gsiccSerial;

    /**
     * 可抵现商品编码
     */
    private String gsiccProId;

    @ApiModelProperty(value = "商品编码")
    private String proCode;

    @ApiModelProperty(value = "商品名称")
    private String proName;

    @ApiModelProperty(value = "规格")
    private String proSpecs;

    @ApiModelProperty(value = "生产企业")
    private String proFactoryName;

    @ApiModelProperty(value = "计量单位")
    private String proUnit;

    private static final long serialVersionUID = 1L;

}