package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaYbStockBatch;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaYbStockBatchMapper extends BaseMapper<GaiaYbStockBatch> {
    void insertBatch( List<GaiaYbStockBatch> inData);
}
