//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdMedCheckD;
import com.gys.business.service.data.GetMedCheckDOutData;
import com.gys.business.service.data.GetMedCheckInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdMedCheckDMapper extends BaseMapper<GaiaSdMedCheckD> {
    List<GetMedCheckDOutData> selectMedCheckDs(GetMedCheckInData inData);
}
