package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**

 * @author xiayuan
 * @date 2021-01-10
 */
@Data
public class GaiaUserUsemap implements Serializable {

    private static final long serialVersionUID = -3329302406237869066L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 员工编号
     */
    private String userId;
    /**
     * 最后登录店号
     */
    private String lastStoreCode;

    /**
     * 最后登录日期
     */
    private String lastDate;

    /**
     * 最后登录时间
     */
    private String lastTime;


}