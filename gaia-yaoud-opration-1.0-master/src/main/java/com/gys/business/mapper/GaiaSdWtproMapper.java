package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdWtbhdD;
import com.gys.business.mapper.entity.GaiaSdWtpro;
import com.gys.business.service.data.GetAcceptInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/16 14:13
 */
public interface GaiaSdWtproMapper extends BaseMapper<GaiaSdWtpro> {
    List<String> getProSelfCodeList(String client);
    void  updateByproCode(GaiaSdWtpro request);

    List<HashMap<String, Object>> listUnMatchedProSelfCode();

    List<HashMap<String, Object>> listWTPSDInfo(GetAcceptInData inData);
    GaiaSdWtpro  getProInfo(GaiaSdWtpro request);

    HashMap<String, Object> getSdWtstoInfo(@Param("clientId") String clientId,@Param("gsrhBrId") String gsrhBrId);

    int updateSendSendStatus(GaiaSdWtbhdD gaiaSdWtbhdD);

    List<GaiaSdWtpro> getGaiaSdWtproByClientAndProDsfCode(GaiaSdWtpro gaiaSdWtpro);

}
