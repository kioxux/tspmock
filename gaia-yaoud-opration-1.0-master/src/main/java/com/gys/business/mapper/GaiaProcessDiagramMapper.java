package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProcessDiagram;
import com.gys.business.service.data.Menu.ProcessDiagramBO;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaProcessDiagramMapper extends BaseMapper<GaiaProcessDiagram> {
    List<ProcessDiagramBO> getProcessDiagram(@Param("nType")String nType);

    Integer checkUserAuth(Map map);
}