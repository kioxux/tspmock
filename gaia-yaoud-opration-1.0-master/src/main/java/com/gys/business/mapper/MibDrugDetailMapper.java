package com.gys.business.mapper;

import com.gys.business.mapper.entity.MibDrugDetail;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wavesen.shen
 */
@Mapper
public interface MibDrugDetailMapper extends BaseMapper<MibDrugDetail> {
}