package com.gys.business.mapper.yaomeng;

import com.gys.common.data.yaomeng.GaiaPrescribeDetail;

import java.util.List;

public interface GaiaPrescribeDetailDao {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaPrescribeDetail record);

    int insertList(List<GaiaPrescribeDetail> list);

    int insertSelective(GaiaPrescribeDetail record);

    GaiaPrescribeDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaPrescribeDetail record);

    int updateByPrimaryKey(GaiaPrescribeDetail record);

    List<GaiaPrescribeDetail> getBySaleBillNo(String saleBillNo);

    int deleteByPreBillNo(String preBillNo);
}