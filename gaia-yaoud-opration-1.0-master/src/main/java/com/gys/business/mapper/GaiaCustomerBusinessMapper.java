package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaBillOfAr;
import com.gys.business.mapper.entity.GaiaBillOfArCancel;
import com.gys.business.mapper.entity.GaiaCustomerBusiness;
import com.gys.business.service.data.SelectData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GaiaBillOfArInData;
import com.gys.common.data.GaiaBillOfArOutData;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface GaiaCustomerBusinessMapper extends BaseMapper<GaiaCustomerBusiness> {
    List<SelectData> getCustomer(Map<String, String> map);

    List<Map<String, Object>> listCustomer(GaiaBillOfArInData inData);

    List<Map<String, Object>> listDc(GaiaBillOfArInData inData);

    void updateCusArAmt(GaiaBillOfArInData inData,@Param("arAmt") BigDecimal arAmt);

    void UpdateByCusArAmt(GaiaBillOfArCancel cancel,@Param("cusArAmt") BigDecimal cusArAmt);

    GaiaCustomerBusiness selectByCusArAmt(GaiaBillOfArCancel cancel);

    GaiaCustomerBusiness selectBillByCusArAmt(GaiaBillOfAr billInfo);

    void UpdateBillByCusArAmt(GaiaBillOfAr billInfo,@Param("cusArAmt") BigDecimal cusArAmt);
}