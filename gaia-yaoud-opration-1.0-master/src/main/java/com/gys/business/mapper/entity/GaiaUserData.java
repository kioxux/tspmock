package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_USER_DATA"
)
@Data
public class GaiaUserData implements Serializable {
    private static final long serialVersionUID = -6011181432816261853L;
    @Id
    @Column(
            name = "USER_ID"
    )
    @ApiModelProperty(value = "员工编号")
    private String userId;
    @Id
    @Column(
            name = "CLIENT"
    )
    @ApiModelProperty(value = "加盟商")
    private String client;
    @Column(
            name = "USER_PASSWORD"
    )
    @ApiModelProperty(value = "密码")
    private String userPassword;
    @Column(
            name = "USER_NAM"
    )
    @ApiModelProperty(value = "姓名")
    private String userNam;
    @Column(
            name = "USER_SEX"
    )
    @ApiModelProperty(value = "性别 0女1男")
    private String userSex;
    @Column(
            name = "USER_TEL"
    )
    @ApiModelProperty(value = "手机号")
    private String userTel;
    @Column(
            name = "USER_IDC"
    )
    @ApiModelProperty(value = "身份证号")
    private String userIdc;
    @Column(
            name = "USER_YS_ID"
    )
    @ApiModelProperty(value = "注册证号")
    private String userYsId;
    @Column(
            name = "USER_YS_ZYFW"
    )
    @ApiModelProperty(value = "职业范围")
    private String userYsZyfw;
    @Column(
            name = "USER_YS_ZYLB"
    )
    @ApiModelProperty(value = "职业类型")
    private String userYsZylb;
    @Column(
            name = "DEP_ID"
    )
    @ApiModelProperty(value = "部门OD")
    private String depId;
    @Column(
            name = "DEP_NAME"
    )
    @ApiModelProperty(value = "部门名称")
    private String depName;
    @Column(
            name = "USER_ADDR"
    )
    @ApiModelProperty(value = "地址")
    private String userAddr;
    @Column(
            name = "USER_STA"
    )
    @ApiModelProperty(value = "在职状态:0，在职；1离职；2停用，默认0")
    private String userSta;
    @Column(
            name = "USER_CRE_DATE"
    )
    @ApiModelProperty(value = "创建日期")
    private String userCreDate;
    @Column(
            name = "USER_CRE_TIME"
    )
    @ApiModelProperty(value = "创建时间")
    private String userCreTime;
    @Column(
            name = "USER_CRE_ID"
    )
    @ApiModelProperty(value = "创建人账号")
    private String userCreId;
    @Column(
            name = "USER_MODI_DATE"
    )
    @ApiModelProperty(value = "修改日期")
    private String userModiDate;
    @Column(
            name = "USER_MODI_TIME"
    )
    @ApiModelProperty(value = "修改时间")
    private String userModiTime;
    @Column(
            name = "USER_MODI_ID"
    )
    @ApiModelProperty(value = "修改人账号")
    private String userModiId;
    @Column(
            name = "USER_JOIN_DATE"
    )
    @ApiModelProperty(value = "入职时间")
    private String userJoinDate;
    @Column(
            name = "USER_DIS_DATE"
    )
    @ApiModelProperty(value = "停用时间")
    private String userDisDate;
    @Column(
            name = "USER_EMAIL"
    )
    @ApiModelProperty(value = "邮箱")
    private String userEmail;
    @Column(
            name = "USER_LOGIN_STA"
    )
    @ApiModelProperty(value = "首次登录 0-首次登录 1-非首次登录")
    private String userLoginSta;

    @Column(name = "STO_CHAIN_HEAD")
    private String stoChainHead;

    @Column(name = "USER_PROVINCE")
    private String userProvince;

    @Column(name = "USER_PDC_ID")
    private String userPdcId;

    @Column(name = "USER_PREDEP")
    private String userPredep;

    @Column(name = "USER_IDCPHO_ADDS")
    private String userIdcphoAdds;

    @Column(name = "USER_IDCPHO_STATUS")
    private String userIdcphoStatus;

    @Column(name = "USER_PDCIDPHO_ADDS")
    private String userPdcidphoAdds;

    @Column(name = "USER_PDCIDCPHO_STATUS")
    private String userPdcidcphoStatus;

    @Column(name = "USER_YBLPA_ID")
    private String userYblpaId;

    @Column(name = "USER_SIGN_ADDS")
    private String userSignAdds;
}
