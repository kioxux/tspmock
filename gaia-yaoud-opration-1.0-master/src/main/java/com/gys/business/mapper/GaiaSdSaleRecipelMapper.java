//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSaleRecipel;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import com.gys.common.data.remoteReview.dto.QueryReviewParamDTO;
import com.gys.common.data.yaomeng.RecipelInfoQueryBean;
import com.gys.common.data.yaomeng.RecipelInfoQueryDto;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface GaiaSdSaleRecipelMapper extends BaseMapper<GaiaSdSaleRecipel> {
    List<GaiaSdSaleRecipelOutData> getAuditedSaleRecipelList(GaiaSdSaleRecipelInData inData);

    List<GaiaSdSaleRecipelRecordOutData> getSaleRecipeRecordlList(GaiaSdSaleRecipelRecordInData inData);

    String selectNextVoucherId();

    Integer checkRecipelCount(GaiaSdSaleRecipel gaiaSdSaleRecipel);

    List<ControlledDrugsOutData> getControlledDrugsManage(ControlledDrugsInData inData);

    /**
     * 批量添加
     * @param list
     */
    void insertLists(List<GaiaSdSaleRecipel> list);

    /**
     * 查询处方信息列表
     **/
    ArrayList<GaiaSdSaleRecipel> queryReviewList(@Param("client") String client, @Param("queryReviewParamDTO") QueryReviewParamDTO queryReviewParamDTO);

    //获取上传门店
    ArrayList<StoreListOutData> storeListQry(@Param("client") String client);

    // 根据销售单号修改处方数据
    Integer updateReviewStatus(@Param("client") String client, @Param("gssrSaleBillNo") String gssrSaleBillNo, @Param("code") String code, @Param("userId") String userId, @Param("loginName") String loginName, @Param("formatDate") String formatDate, @Param("formatTime") String formatTime, @Param("newUrl") String newUrl, @Param("gssrTime") String gssrTime, @Param("gssrDate") String gssrDate);

    /**
     * 根据销售单号查询处方信息
     **/
    ArrayList<GaiaSdSaleRecipel> queryReviewBySaleBillNo(@Param("client")String client, @Param("gssrSaleBillNo")String gssrSaleBillNo);


    ArrayList<GaiaSdSaleRecipel> getEleRecipelInfoByGssrSaleBillNo(@Param("client")String client, @Param("gssrSaleBillNo")String gssrSaleBillNo);


    /**
     * 获取易慧请求云南监管系统的处方药销售明细
     * @return
     */
    List<YunNanSalesDetailInData> getYunNanSalesDetail();

    List<RecipelInfoQueryDto> recipelInfoQuery(RecipelInfoQueryBean bean);

    List<GaiaSdSaleRecipel> getBySaleBillNo(String saleBillNo);

    void saveRecipelIdAndFileUrl(@Param("saleBillNo") String saleBillNo,@Param("recipelId") String recipelId, @Param("fileUrl") String fileUrl);

    List<RebornData> getRecipelProFromSaleD(@Param("client") String client,@Param("stoCode") String stoCode, @Param("saleBillNo") String saleBillNo);


    int deleteBySaleBillNo(String gssrSaleBillNo);

    void savePhoneTypeAndHospital(GaiaSdSaleRecipel recipel);

    void saveVoucherId(List<GaiaSdSaleRecipel> list);

    List<SalePecipelProductOutData> getProductByClientAndBillNo(@Param("client") String client, @Param("saleBillNo") String saleBillNo);

    int savePictureUrl(@Param("pictureUrl") String pictureUrl, @Param("client") String client, @Param("gssrVoucherId") String gssrVoucherId);

    List<String> getVoucherId(@Param("client") String client, @Param("gssrSaleBillNo") String gssrSaleBillNo);

    GetLoginOutData getDefoultPharmacist(@Param("client") String client,@Param("depId") String depId);
}
