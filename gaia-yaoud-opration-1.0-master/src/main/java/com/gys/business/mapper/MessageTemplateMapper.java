package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaBasicsGroupInfo;
import com.gys.business.mapper.entity.MessageTemplate;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-09-07
 */
@Mapper
public interface MessageTemplateMapper extends BaseMapper<MessageTemplate> {
    List<Map<String, String>> selectAllStoInUse();



}
