//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_INDRAWER_H"
)
public class GaiaSdIndrawerH implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSIH_VOUCHER_ID"
    )
    private String gsihVoucherId;
    @Column(
            name = "GSIH_BR_ID"
    )
    private String gsihBrId;
    @Column(
            name = "GSIH_DATE"
    )
    private String gsihDate;
    @Column(
            name = "GSIH_CLE_VOUCHER_ID"
    )
    private String gsihCleVoucherId;
    @Column(
            name = "GSIH_STATUS"
    )
    private String gsihStatus;
    @Column(
            name = "GSIH_EMP"
    )
    private String gsihEmp;
    @Column(
            name = "GSIH_REMAKS"
    )
    private String gsihRemaks;
    private static final long serialVersionUID = 1L;

    public GaiaSdIndrawerH() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsihVoucherId() {
        return this.gsihVoucherId;
    }

    public void setGsihVoucherId(String gsihVoucherId) {
        this.gsihVoucherId = gsihVoucherId;
    }

    public String getGsihBrId() {
        return this.gsihBrId;
    }

    public void setGsihBrId(String gsihBrId) {
        this.gsihBrId = gsihBrId;
    }

    public String getGsihDate() {
        return this.gsihDate;
    }

    public void setGsihDate(String gsihDate) {
        this.gsihDate = gsihDate;
    }

    public String getGsihCleVoucherId() {
        return this.gsihCleVoucherId;
    }

    public void setGsihCleVoucherId(String gsihCleVoucherId) {
        this.gsihCleVoucherId = gsihCleVoucherId;
    }

    public String getGsihStatus() {
        return this.gsihStatus;
    }

    public void setGsihStatus(String gsihStatus) {
        this.gsihStatus = gsihStatus;
    }

    public String getGsihEmp() {
        return this.gsihEmp;
    }

    public void setGsihEmp(String gsihEmp) {
        this.gsihEmp = gsihEmp;
    }

    public String getGsihRemaks() {
        return this.gsihRemaks;
    }

    public void setGsihRemaks(String gsihRemaks) {
        this.gsihRemaks = gsihRemaks;
    }
}
