
package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author xiaoyuan
 */
@Data
@Table(name = "GAIA_RETAIL_PRICE")
public class GaiaRetailPrice implements Serializable {
    private static final long serialVersionUID = -1846313672255512L;

    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店
     */
    @Id
    @Column(name = "PRC_STORE")
    private String prcStore;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "PRC_PRODUCT")
    private String prcProduct;

    /**
     * 价格类型
     */
    @Id
    @Column(name = "PRC_CLASS")
    private String prcClass;

    /**
     * 掉价单号
     */
    @Id
    @Column(name = "PRC_MODFIY_NO")
    private String prcModfiyNo;

    /**
     * 金额
     */
    @Column(name = "PRC_AMOUNT")
    private BigDecimal prcAmount;

    /**
     * 有效日期
     */
    @Column(name = "PRC_EFFECT_DATE")
    private String prcEffectDate;

    /**
     * 修改前价格
     */
    @Column(name = "PRC_AMOUNT_BEFORE")
    private BigDecimal prcAmountBefore;

    /**
     * 单位
     */
    @Column(name = "PRC_UNIT")
    private String prcUnit;

    /**
     * 不允许积分
     */
    @Column(name = "PRC_NO_INTEGRAL")
    private String prcNoIntegral;

    /**
     * 不允许会员卡打折
     */
    @Column(name = "PRC_NO_DISCOUNT")
    private String prcNoDiscount;

    /**
     * 不允许积分兑换
     */
    @Column(name = "PRC_NO_EXCHANGE")
    private String prcNoExchange;

    /**
     * 限购数量
     */
    @Column(name = "PRC_LIMIT_AMOUNT")
    private String prcLimitAmount;

    /**
     * 审批状态
     */
    @Column(name = "PRC_APPROVAL_SUATUS")
    private String prcApprovalSuatus;

    /**
     * 创建日期
     */
    @Column(name = "PRC_CREATE_DATE")
    private String prcCreateDate;

    /**
     * 创建时间
     */
    @Column(name = "PRC_CREATE_TIME")
    private String prcCreateTime;

    /**
     * 审批日期
     */
    @Column(name = "PRC_APPROVAL_DATE")
    private String prcApprovalDate;

    /**
     * 审批时间
     */
    @Column(name = "PRC_APPROVAL_TIME")
    private String prcApprovalTime;

    /**
     * 调价来源
     */
    @Column(name = "PRC_SOURCE")
    private String prcSource;

    /**
     * 总部调价
     */
    @Column(name = "PRC_HEAD_PRICE")
    private String prcHeadPrice;


}
