//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_MED_CHECK_H"
)
public class GaiaSdMedCheckH implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSMCH_VOUCHER_ID"
    )
    private String gsmchVoucherId;
    @Column(
            name = "GSMCH_BR_ID"
    )
    private String gsmchBrId;
    @Column(
            name = "GSMCH_BR_NAME"
    )
    private String gsmchBrName;
    @Column(
            name = "GSMCH_TYPE"
    )
    private String gsmchType;
    @Column(
            name = "GSMCH_DATE"
    )
    private String gsmchDate;
    @Column(
            name = "GSMCH_EMP"
    )
    private String gsmchEmp;
    @Column(
            name = "GSMCH_EXAMINE_EMP"
    )
    private String gsmchExamineEmp;
    @Column(
            name = "GSMCH_EXAMINE_DATE"
    )
    private String gsmchExamineDate;
    @Column(
            name = "GSMCH_STATUS"
    )
    private String gsmchStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdMedCheckH() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsmchVoucherId() {
        return this.gsmchVoucherId;
    }

    public void setGsmchVoucherId(String gsmchVoucherId) {
        this.gsmchVoucherId = gsmchVoucherId;
    }

    public String getGsmchBrId() {
        return this.gsmchBrId;
    }

    public void setGsmchBrId(String gsmchBrId) {
        this.gsmchBrId = gsmchBrId;
    }

    public String getGsmchBrName() {
        return this.gsmchBrName;
    }

    public void setGsmchBrName(String gsmchBrName) {
        this.gsmchBrName = gsmchBrName;
    }

    public String getGsmchType() {
        return this.gsmchType;
    }

    public void setGsmchType(String gsmchType) {
        this.gsmchType = gsmchType;
    }

    public String getGsmchDate() {
        return this.gsmchDate;
    }

    public void setGsmchDate(String gsmchDate) {
        this.gsmchDate = gsmchDate;
    }

    public String getGsmchEmp() {
        return this.gsmchEmp;
    }

    public void setGsmchEmp(String gsmchEmp) {
        this.gsmchEmp = gsmchEmp;
    }

    public String getGsmchExamineEmp() {
        return this.gsmchExamineEmp;
    }

    public void setGsmchExamineEmp(String gsmchExamineEmp) {
        this.gsmchExamineEmp = gsmchExamineEmp;
    }

    public String getGsmchExamineDate() {
        return this.gsmchExamineDate;
    }

    public void setGsmchExamineDate(String gsmchExamineDate) {
        this.gsmchExamineDate = gsmchExamineDate;
    }

    public String getGsmchStatus() {
        return this.gsmchStatus;
    }

    public void setGsmchStatus(String gsmchStatus) {
        this.gsmchStatus = gsmchStatus;
    }
}
