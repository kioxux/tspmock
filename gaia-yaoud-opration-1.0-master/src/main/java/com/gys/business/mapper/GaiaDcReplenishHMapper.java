package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDcReplenishH;
import com.gys.business.service.data.DcReplenishForm;
import com.gys.business.service.data.DcReplenishVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/21 15:49
 */
public interface GaiaDcReplenishHMapper {

    GaiaDcReplenishH getById(Long id);

    int add(GaiaDcReplenishH gaiaDcReplenishH);

    int update(GaiaDcReplenishH gaiaDcReplenishH);

    String getNextVoucherId(@Param("client") String client);

    List<DcReplenishVO> replenishList(DcReplenishForm replenishForm);

    GaiaDcReplenishH getUnique(GaiaDcReplenishH cond);
}
