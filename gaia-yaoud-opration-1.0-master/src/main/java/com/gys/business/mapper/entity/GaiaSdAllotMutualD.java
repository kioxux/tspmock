//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_ALLOT_MUTUAL_D"
)
public class GaiaSdAllotMutualD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSAMD_VOUCHER_ID"
    )
    private String gsamdVoucherId;
    @Id
    @Column(
            name = "GSAMD_DATE"
    )
    private String gsamdDate;
    @Id
    @Column(
            name = "GSAMD_SERIAL"
    )
    private String gsamdSerial;
    @Column(
            name = "GSAMD_PRO_ID"
    )
    private String gsamdProId;
    @Column(
            name = "GSAMD_BATCH_NO"
    )
    private String gsamdBatchNo;
    @Column(
            name = "GSAMD_BATCH"
    )
    private String gsamdBatch;
    @Column(
            name = "GSAMD_VALID_DATE"
    )
    private String gsamdValidDate;
    @Column(
            name = "GSAMD_STOCK_QTY"
    )
    private String gsamdStockQty;
    @Column(
            name = "GSAMD_RECIPIENT_QTY"
    )
    private String gsamdRecipientQty;
    @Column(
            name = "GSAMD_STOCK_STATUS"
    )
    private String gsamdStockStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdAllotMutualD() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsamdVoucherId() {
        return this.gsamdVoucherId;
    }

    public void setGsamdVoucherId(String gsamdVoucherId) {
        this.gsamdVoucherId = gsamdVoucherId;
    }

    public String getGsamdDate() {
        return this.gsamdDate;
    }

    public void setGsamdDate(String gsamdDate) {
        this.gsamdDate = gsamdDate;
    }

    public String getGsamdSerial() {
        return this.gsamdSerial;
    }

    public void setGsamdSerial(String gsamdSerial) {
        this.gsamdSerial = gsamdSerial;
    }

    public String getGsamdProId() {
        return this.gsamdProId;
    }

    public void setGsamdProId(String gsamdProId) {
        this.gsamdProId = gsamdProId;
    }

    public String getGsamdBatchNo() {
        return this.gsamdBatchNo;
    }

    public void setGsamdBatchNo(String gsamdBatchNo) {
        this.gsamdBatchNo = gsamdBatchNo;
    }

    public String getGsamdBatch() {
        return this.gsamdBatch;
    }

    public void setGsamdBatch(String gsamdBatch) {
        this.gsamdBatch = gsamdBatch;
    }

    public String getGsamdValidDate() {
        return this.gsamdValidDate;
    }

    public void setGsamdValidDate(String gsamdValidDate) {
        this.gsamdValidDate = gsamdValidDate;
    }

    public String getGsamdStockQty() {
        return this.gsamdStockQty;
    }

    public void setGsamdStockQty(String gsamdStockQty) {
        this.gsamdStockQty = gsamdStockQty;
    }

    public String getGsamdRecipientQty() {
        return this.gsamdRecipientQty;
    }

    public void setGsamdRecipientQty(String gsamdRecipientQty) {
        this.gsamdRecipientQty = gsamdRecipientQty;
    }

    public String getGsamdStockStatus() {
        return this.gsamdStockStatus;
    }

    public void setGsamdStockStatus(String gsamdStockStatus) {
        this.gsamdStockStatus = gsamdStockStatus;
    }
}
