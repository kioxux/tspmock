package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaBillOfAp;
import com.gys.business.mapper.entity.GaiaPaymentApplications;
import com.gys.business.service.data.billOfAp.GaiaBillOfApParam;
import com.gys.business.service.data.billOfAp.GaiaBillOfApResponse;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GaiaBillOfApData;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 供应商应付明细清单表 Mapper 接口
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Mapper
public interface GaiaBillOfApMapper extends BaseMapper<GaiaBillOfAp> {
    List<Map<String, Object>> getClientStore();

    List<GaiaBillOfAp> getSelectBillOfList(@Param("request") GaiaBillOfApParam gaiaBillOfApParam, @Param("startTime") String startTime, @Param("endTime") String endTime);



    List<Map<String, Object>> supplierSalesman(@Param("user")GetLoginOutData userInfo,@Param("depId")String depId,@Param("stoCode")String stoCode,@Param("supSelfCode")String supSelfCode,@Param("name")String name);



    List<GaiaBillOfAp> getList(@Param("client") String client, @Param("lastDay") String lastDay);

    List<Map<String, Object>> getStoreList();

    List<GaiaBillOfAp> selectGroupBy(@Param("type") String type);
    //-----------------------------------------供应商业务员------------------------------------------------
    List<GaiaBillOfAp> assistantList(@Param("response") GaiaBillOfApResponse response);

    List<GaiaBillOfAp> getListOfAp(@Param("client")String client,@Param("dcCode") String dcCode, @Param("supSelfCode")String supSelfCode, @Param("lastDay")String lastDay,@Param("salesmanId")String salesmanId);


    List<Map<String,Object>> checkSalesman(@Param("client")String client, @Param("supSelfCode")String supSelfCode, @Param("dcCode")String dcCode,@Param("name")String name);

    List<Map<String,Object>> selectAmt(@Param("data")GaiaPaymentApplications datum);

    List<String> getName(@Param("client")String client, @Param("supSite")String supSite, @Param("supSelfCode")String supSelfCode, @Param("salesmanId")String salesmanId);

    List<Map<String, Object>> getDocumentsAdvance(@Param("data")GaiaPaymentApplications datum);

    List<GaiaBillOfAp> selectBillOfApGroupBy();

    List<GaiaBillOfAp> getAmt(@Param("client")String client,@Param("supCode") String supCode, @Param("siteCode")String siteCode, @Param("date")String date);

    List<GaiaBillOfAp> getBillOfAp(@Param("client")String client,@Param("supCode") String supCode, @Param("siteCode")String siteCode, @Param("date")String date);

    void updateSupplierBusiness(@Param("client")String client, @Param("supCode") String supCode, @Param("siteCode")String siteCode,@Param("amountOfPayment") BigDecimal amountOfPayment);

    GaiaBillOfAp getBillOf(@Param("client")String client, @Param("supCode")String supCode, @Param("siteCode") String siteCode, @Param("yestDay") String yestDay);



    List<GaiaBillOfAp> selectEndingBalance(@Param("date")String date);

    void updateEndingBalance(@Param("billOfAp")GaiaBillOfAp billOfAp);

    List<GaiaBillOfApData> getBeginAmount(@Param("data")GaiaBillOfApParam gaiaBillOfApParam, @Param("beginTime")String beginTime,@Param("dcCode")String dcCode);

    List<GaiaBillOfAp> getbeginPeriodAmt(@Param("client")String client);
}
