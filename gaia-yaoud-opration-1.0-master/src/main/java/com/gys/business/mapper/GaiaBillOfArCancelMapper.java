package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaBillOfArCancel;
import com.gys.common.data.GaiaBillOfArDetailOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 应收核销清单表(GaiaBillOfArCancel)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-18 15:20:50
 */
public interface GaiaBillOfArCancelMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaBillOfArCancel queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaBillOfArCancel> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaBillOfArCancel 实例对象
     * @return 对象列表
     */
    List<GaiaBillOfArCancel> queryAll(GaiaBillOfArCancel gaiaBillOfArCancel);

    /**
     * 新增数据
     *
     * @param gaiaBillOfArCancel 实例对象
     * @return 影响行数
     */
    int insert(GaiaBillOfArCancel gaiaBillOfArCancel);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaBillOfArCancel> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaBillOfArCancel> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaBillOfArCancel> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaBillOfArCancel> entities);

    /**
     * 修改数据
     *
     * @param gaiaBillOfArCancel 实例对象
     * @return 影响行数
     */
    int update(GaiaBillOfArCancel gaiaBillOfArCancel);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    void updateDiaoBoZ(GaiaBillOfArDetailOutData bill);

    void updateTKZ(GaiaBillOfArDetailOutData bill);
}

