//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_DEVICE_INFO"
)
public class GaiaSdDeviceInfo implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSDI_ID"
    )
    private String gsdiId;
    @Column(
            name = "GSDI_NAME"
    )
    private String gsdiName;
    @Column(
            name = "GSTH_BR_ID"
    )
    private String gsthBrId;
    @Column(
            name = "GSTH_BR_NAME"
    )
    private String gsthBrName;
    @Column(
            name = "GSDI_QTY"
    )
    private String gsdiQty;
    @Column(
            name = "GSDI_MODEL"
    )
    private String gsdiModel;
    @Column(
            name = "GSDI_FACTORY"
    )
    private String gsdiFactory;
    @Column(
            name = "GSDI_START_DATE"
    )
    private String gsdiStartDate;
    @Column(
            name = "GSDI_UPDATE_DAT"
    )
    private String gsdiUpdateDat;
    @Column(
            name = "GSTH_UPDATE_EMP"
    )
    private String gsthUpdateEmp;
    @Column(
            name = "GSDI_INSTRUCTION"
    )
    private String gsdiInstruction;
    @Column(
            name = "GSDI_QUALIFY"
    )
    private String gsdiQualify;
    @Column(
            name = "GSDI_CHECK"
    )
    private String gsdiCheck;
    private static final long serialVersionUID = 1L;

    public GaiaSdDeviceInfo() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsdiId() {
        return this.gsdiId;
    }

    public void setGsdiId(String gsdiId) {
        this.gsdiId = gsdiId;
    }

    public String getGsdiName() {
        return this.gsdiName;
    }

    public void setGsdiName(String gsdiName) {
        this.gsdiName = gsdiName;
    }

    public String getGsthBrId() {
        return this.gsthBrId;
    }

    public void setGsthBrId(String gsthBrId) {
        this.gsthBrId = gsthBrId;
    }

    public String getGsthBrName() {
        return this.gsthBrName;
    }

    public void setGsthBrName(String gsthBrName) {
        this.gsthBrName = gsthBrName;
    }

    public String getGsdiQty() {
        return this.gsdiQty;
    }

    public void setGsdiQty(String gsdiQty) {
        this.gsdiQty = gsdiQty;
    }

    public String getGsdiModel() {
        return this.gsdiModel;
    }

    public void setGsdiModel(String gsdiModel) {
        this.gsdiModel = gsdiModel;
    }

    public String getGsdiFactory() {
        return this.gsdiFactory;
    }

    public void setGsdiFactory(String gsdiFactory) {
        this.gsdiFactory = gsdiFactory;
    }

    public String getGsdiStartDate() {
        return this.gsdiStartDate;
    }

    public void setGsdiStartDate(String gsdiStartDate) {
        this.gsdiStartDate = gsdiStartDate;
    }

    public String getGsdiUpdateDat() {
        return this.gsdiUpdateDat;
    }

    public void setGsdiUpdateDat(String gsdiUpdateDat) {
        this.gsdiUpdateDat = gsdiUpdateDat;
    }

    public String getGsthUpdateEmp() {
        return this.gsthUpdateEmp;
    }

    public void setGsthUpdateEmp(String gsthUpdateEmp) {
        this.gsthUpdateEmp = gsthUpdateEmp;
    }

    public String getGsdiInstruction() {
        return this.gsdiInstruction;
    }

    public void setGsdiInstruction(String gsdiInstruction) {
        this.gsdiInstruction = gsdiInstruction;
    }

    public String getGsdiQualify() {
        return this.gsdiQualify;
    }

    public void setGsdiQualify(String gsdiQualify) {
        this.gsdiQualify = gsdiQualify;
    }

    public String getGsdiCheck() {
        return this.gsdiCheck;
    }

    public void setGsdiCheck(String gsdiCheck) {
        this.gsdiCheck = gsdiCheck;
    }
}
