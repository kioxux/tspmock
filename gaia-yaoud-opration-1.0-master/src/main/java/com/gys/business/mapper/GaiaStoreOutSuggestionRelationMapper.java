package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaStoreOutSuggestionH;
import com.gys.business.mapper.entity.GaiaStoreOutSuggestionRelation;
import com.gys.business.service.data.GaiaStoreOutSuggestionInData;
import com.gys.business.service.data.GaiaStoreOutSuggestionRelationOutData;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * 门店调出建议关系表(GaiaStoreOutSuggestionRelation)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-28 10:54:23
 */
public interface GaiaStoreOutSuggestionRelationMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaStoreOutSuggestionRelation queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaStoreOutSuggestionRelation 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaStoreOutSuggestionRelation> queryAllByLimit(GaiaStoreOutSuggestionRelation gaiaStoreOutSuggestionRelation, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaStoreOutSuggestionRelation 查询条件
     * @return 总行数
     */
    long count(GaiaStoreOutSuggestionRelation gaiaStoreOutSuggestionRelation);

    /**
     * 新增数据
     *
     * @param gaiaStoreOutSuggestionRelation 实例对象
     * @return 影响行数
     */
    int insert(GaiaStoreOutSuggestionRelation gaiaStoreOutSuggestionRelation);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaStoreOutSuggestionRelation> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaStoreOutSuggestionRelation> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaStoreOutSuggestionRelation> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaStoreOutSuggestionRelation> entities);

    /**
     * 修改数据
     *
     * @param gaiaStoreOutSuggestionRelation 实例对象
     * @return 影响行数
     */
    int update(GaiaStoreOutSuggestionRelation gaiaStoreOutSuggestionRelation);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    List<GaiaStoreOutSuggestionRelationOutData> listDetail(GaiaStoreOutSuggestionInData inData);

    int updateByCode(GaiaStoreOutSuggestionRelation update);
}

