package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaCommodityInventoryD;
import com.gys.business.service.data.CommodityAllocationPushInData;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * 商品调库-明细表(GaiaCommodityInventoryD)表数据库访问层
 *
 * @author makejava
 * @since 2021-10-19 16:01:15
 */
public interface GaiaCommodityInventoryDMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaCommodityInventoryD queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param gaiaCommodityInventoryD 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<GaiaCommodityInventoryD> queryAllByLimit(GaiaCommodityInventoryD gaiaCommodityInventoryD, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param gaiaCommodityInventoryD 查询条件
     * @return 总行数
     */
    long count(GaiaCommodityInventoryD gaiaCommodityInventoryD);

    /**
     * 新增数据
     *
     * @param gaiaCommodityInventoryD 实例对象
     * @return 影响行数
     */
    int insert(GaiaCommodityInventoryD gaiaCommodityInventoryD);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaCommodityInventoryD> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaCommodityInventoryD> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaCommodityInventoryD> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaCommodityInventoryD> entities);

    /**
     * 修改数据
     *
     * @param gaiaCommodityInventoryD 实例对象
     * @return 影响行数
     */
    int update(GaiaCommodityInventoryD gaiaCommodityInventoryD);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    List<String> getStoCode(@Param("client") String client,@Param("xlxqs") List<String> xlxqs);

    List<GaiaCommodityInventoryD> getCommodityDetail(CommodityAllocationPushInData inData);

    List<String> getChainCompany(CommodityAllocationPushInData inData);

    void batchUpdateStatusAndRemark(@Param("InventoryList") List<GaiaCommodityInventoryD> InventoryList);

    List<GaiaCommodityInventoryD> getByBillNo(String billNo);
}

