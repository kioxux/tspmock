package com.gys.business.mapper.entity.member;

import lombok.Data;

import java.io.Serializable;

/**
 * 积分抵现活动门店
 *
 * @author wu mao yin
 * @date 2021/12/9 15:16
 */
@Data
public class GaiaSdIntegralCashSto implements Serializable {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 积分抵现活动单号
     */
    private String gsicsVoucherId;

    /**
     * 门店
     */
    private String stoCode;

    /**
     * 名称
     */
    private String stoName;

    private static final long serialVersionUID = 1L;

}