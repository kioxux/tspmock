//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SO_HEADER"
)
public class GaiaSoHeader implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "SO_ID"
    )
    private String soId;
    @Column(
            name = "SO_TYPE"
    )
    private String soType;
    @Column(
            name = "SO_CUSTOMER_ID"
    )
    private String soCustomerId;
    @Column(
            name = "SO_COMPANY_CODE"
    )
    private String soCompanyCode;
    @Column(
            name = "SO_DATE"
    )
    private String soDate;
    @Column(
            name = "SO_PAYMENT_ID"
    )
    private String soPaymentId;
    @Column(
            name = "SO_HEAD_REMARK"
    )
    private String soHeadRemark;
    @Column(
            name = "SO_CREATE_BY"
    )
    private String soCreateBy;
    @Column(
            name = "SO_CREATE_DATE"
    )
    private String soCreateDate;
    @Column(
            name = "SO_CREATE_TIME"
    )
    private String soCreateTime;
    @Column(
            name = "SO_APPROVE_STATUS"
    )
    private String soApproveStatus;
    @Column(
            name = "SO_UPDATE_BY"
    )
    private String soUpdateBy;
    @Column(
            name = "SO_UPDATE_DATE"
    )
    private String soUpdateDate;
    @Column(
            name = "SO_UPDATE_TIME"
    )
    private String soUpdateTime;
    @Column(
            name = "GWS_CODE"
    )
    private String gwsCode;

    public String getGwsCode() {
        return gwsCode;
    }

    public void setGwsCode(String gwsCode) {
        this.gwsCode = gwsCode;
    }

    private static final long serialVersionUID = 1L;

    public GaiaSoHeader() {
    }

    public String getClient() {
        return this.client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getSoId() {
        return this.soId;
    }

    public void setSoId(String soId) {
        this.soId = soId;
    }

    public String getSoType() {
        return this.soType;
    }

    public void setSoType(String soType) {
        this.soType = soType;
    }

    public String getSoCustomerId() {
        return this.soCustomerId;
    }

    public void setSoCustomerId(String soCustomerId) {
        this.soCustomerId = soCustomerId;
    }

    public String getSoCompanyCode() {
        return this.soCompanyCode;
    }

    public void setSoCompanyCode(String soCompanyCode) {
        this.soCompanyCode = soCompanyCode;
    }

    public String getSoDate() {
        return this.soDate;
    }

    public void setSoDate(String soDate) {
        this.soDate = soDate;
    }

    public String getSoPaymentId() {
        return this.soPaymentId;
    }

    public void setSoPaymentId(String soPaymentId) {
        this.soPaymentId = soPaymentId;
    }

    public String getSoHeadRemark() {
        return this.soHeadRemark;
    }

    public void setSoHeadRemark(String soHeadRemark) {
        this.soHeadRemark = soHeadRemark;
    }

    public String getSoCreateBy() {
        return this.soCreateBy;
    }

    public void setSoCreateBy(String soCreateBy) {
        this.soCreateBy = soCreateBy;
    }

    public String getSoCreateDate() {
        return this.soCreateDate;
    }

    public void setSoCreateDate(String soCreateDate) {
        this.soCreateDate = soCreateDate;
    }

    public String getSoCreateTime() {
        return this.soCreateTime;
    }

    public void setSoCreateTime(String soCreateTime) {
        this.soCreateTime = soCreateTime;
    }

    public String getSoApproveStatus() {
        return this.soApproveStatus;
    }

    public void setSoApproveStatus(String soApproveStatus) {
        this.soApproveStatus = soApproveStatus;
    }

    public String getSoUpdateBy() {
        return this.soUpdateBy;
    }

    public void setSoUpdateBy(String soUpdateBy) {
        this.soUpdateBy = soUpdateBy;
    }

    public String getSoUpdateDate() {
        return this.soUpdateDate;
    }

    public void setSoUpdateDate(String soUpdateDate) {
        this.soUpdateDate = soUpdateDate;
    }

    public String getSoUpdateTime() {
        return this.soUpdateTime;
    }

    public void setSoUpdateTime(String soUpdateTime) {
        this.soUpdateTime = soUpdateTime;
    }
}
