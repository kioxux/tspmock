package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdStoreEssentialProductD;
import java.util.List;

import com.gys.business.service.data.*;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdStoreEssentialProductDMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSdStoreEssentialProductD record);

    int insertSelective(GaiaSdStoreEssentialProductD record);

    GaiaSdStoreEssentialProductD selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSdStoreEssentialProductD record);

    int updateByPrimaryKey(GaiaSdStoreEssentialProductD record);

    int batchInsert(@Param("list") List<GaiaSdStoreEssentialProductD> list);

    List<StoreEssentialProductRankOutData> getProductRankList(@Param("client") String client);

    List<StoreStockCountOutData> getDirectListByCondition(EssentialProductCalculationInData inData);

    List<String> getStockProListByStore(@Param("client") String client, @Param("brId") String brId, @Param("rank") Integer rank);

    String getLastMonth();

    int updateProductByCondition(GaiaSdStoreEssentialProductD record);

    List<String> getNotAllowList(@Param("client") String client);

    List<StoreJyfwOutData> getStoreJyfwByClient(@Param("client") String client);

    List<StoreProJylbOutData> getProductJylbByClient(@Param("client") String client);

    List<String> getProductListByClient(@Param("client") String client);

    GaiaSdStoreEssentialProductD getStoreEssentialProduct(@Param("client") String client, @Param("orderId") String orderId, @Param("storeId") String storeId, @Param("proSelfCode") String proSelfCode);

    List<StoreEssentialProductOutData> getStoreEssentialProductByOrderId(@Param("client") String client, @Param("orderId") String orderId);

    List<CostPriceAndTaxOutData> getProCostPriceByProList(@Param("client") String client, @Param("list") List<GaiaSdStoreEssentialProductD> distributionList);

    void updateReplenishOrderId(GaiaSdStoreEssentialProductD record);

    List<EssentialProductDetailOutData> getOrderDetailList(EssentialProductCalculationInData inData);
}