package com.gys.business.mapper.cancelStock;

import com.gys.business.service.data.cancelStock.CancelStockData;
import com.gys.business.service.data.cancelStock.CancelStockDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CancelStockMapper {
    void addCancelStock(CancelStockData inData);

    void batchCancelDetail(List<CancelStockDetail> list);
}
