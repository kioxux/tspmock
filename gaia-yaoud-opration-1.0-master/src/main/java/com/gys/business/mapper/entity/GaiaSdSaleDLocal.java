package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_SD_SALE_D_LOCAL")
public class GaiaSdSaleDLocal implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 销售单号
     */
    @Id
    @Column(name = "GSSD_BILL_NO")
    private String gssdBillNo;

    /**
     * 店名
     */
    @Id
    @Column(name = "GSSD_BR_ID")
    private String gssdBrId;

    /**
     * 销售日期
     */
    @Id
    @Column(name = "GSSD_DATE")
    private String gssdDate;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSSD_SERIAL")
    private String gssdSerial;

    /**
     * 商品编码
     */
    @Column(name = "GSSD_PRO_ID")
    private String gssdProId;

    /**
     * 商品批号
     */
    @Column(name = "GSSD_BATCH_NO")
    private String gssdBatchNo;

    /**
     * 商品批次
     */
    @Column(name = "GSSD_BATCH")
    private String gssdBatch;

    /**
     * 商品有效期
     */
    @Column(name = "GSSD_VALID_DATE")
    private String gssdValidDate;

    /**
     * 监管码/条形码
     */
    @Column(name = "GSSD_ST_CODE")
    private String gssdStCode;

    /**
     * 单品零售价
     */
    @Column(name = "GSSD_PRC1")
    private BigDecimal gssdPrc1;

    /**
     * 单品应收价
     */
    @Column(name = "GSSD_PRC2")
    private BigDecimal gssdPrc2;

    /**
     * 数量
     */
    @Column(name = "GSSD_QTY")
    private BigDecimal gssdQty;

    /**
     * 汇总应收金额
     */
    @Column(name = "GSSD_AMT")
    private BigDecimal gssdAmt;

    /**
     * 兑换单个积分
     */
    @Column(name = "GSSD_INTEGRAL_EXCHANGE_SINGLE")
    private String gssdIntegralExchangeSingle;

    /**
     * 兑换积分汇总
     */
    @Column(name = "GSSD_INTEGRAL_EXCHANGE_COLLECT")
    private String gssdIntegralExchangeCollect;

    /**
     * 单个加价兑换积分金额
     */
    @Column(name = "GSSD_INTEGRAL_EXCHANGE_AMT_SINGLE")
    private BigDecimal gssdIntegralExchangeAmtSingle;

    /**
     * 加价兑换积分金额汇总
     */
    @Column(name = "GSSD_INTEGRAL_EXCHANGE_AMT_COLLECT")
    private BigDecimal gssdIntegralExchangeAmtCollect;

    /**
     * 商品折扣总金额
     */
    @Column(name = "GSSD_ZK_AMT")
    private BigDecimal gssdZkAmt;

    /**
     * 积分兑换折扣金额
     */
    @Column(name = "GSSD_ZK_JFDH")
    private BigDecimal gssdZkJfdh;

    /**
     * 积分抵现折扣金额
     */
    @Column(name = "GSSD_ZK_JFDX")
    private BigDecimal gssdZkJfdx;

    /**
     * 电子券折扣金额
     */
    @Column(name = "GSSD_ZK_DZQ")
    private BigDecimal gssdZkDzq;

    /**
     * 抵用券折扣金额
     */
    @Column(name = "GSSD_ZK_DYQ")
    private BigDecimal gssdZkDyq;

    /**
     * 促销折扣金额
     */
    @Column(name = "GSSD_ZK_PM")
    private BigDecimal gssdZkPm;

    /**
     * 营业员编号
     */
    @Column(name = "GSSD_SALER_ID")
    private String gssdSalerId;

    /**
     * 医生编号
     */
    @Column(name = "GSSD_DOCTOR_ID")
    private String gssdDoctorId;

    /**
     * 参加促销主题编号
     */
    @Column(name = "GSSD_PM_SUBJECT_ID")
    private String gssdPmSubjectId;

    /**
     * 参加促销类型编号
     */
    @Column(name = "GSSD_PM_ID")
    private String gssdPmId;

    /**
     * 参加促销类型说明
     */
    @Column(name = "GSSD_PM_CONTENT")
    private String gssdPmContent;

    /**
     * 参加促销编号
     */
    @Column(name = "GSSD_PM_ACTIVITY_ID")
    private String gssdPmActivityId;

    /**
     * 参加促销活动名称
     */
    @Column(name = "GSSD_PM_ACTIVITY_NAME")
    private String gssdPmActivityName;

    /**
     * 参加促销活动参数
     */
    @Column(name = "GSSD_PM_ACTIVITY_FLAG")
    private String gssdPmActivityFlag;

    /**
     * 是否登记处方信息 0否1是
     */
    @Column(name = "GSSD_RECIPEL_FLAG")
    private String gssdRecipelFlag;

    /**
     * 是否为关联推荐
     */
    @Column(name = "GSSD_RELATION_FLAG")
    private String gssdRelationFlag;

    /**
     * 特殊药品购买人身份证
     */
    @Column(name = "GSSD_SPECIALMED_IDCARD")
    private String gssdSpecialmedIdcard;

    /**
     * 特殊药品购买人姓名
     */
    @Column(name = "GSSD_SPECIALMED_NAME")
    private String gssdSpecialmedName;

    /**
     * 特殊药品购买人性别
     */
    @Column(name = "GSSD_SPECIALMED_SEX")
    private String gssdSpecialmedSex;

    /**
     * 特殊药品购买人出生日期
     */
    @Column(name = "GSSD_SPECIALMED_BIRTHDAY")
    private String gssdSpecialmedBirthday;

    /**
     * 特殊药品购买人手机
     */
    @Column(name = "GSSD_SPECIALMED_MOBILE")
    private String gssdSpecialmedMobile;

    /**
     * 特殊药品购买人地址
     */
    @Column(name = "GSSD_SPECIALMED_ADDRESS")
    private String gssdSpecialmedAddress;

    /**
     * 特殊药品电子监管码
     */
    @Column(name = "GSSD_SPECIALMED_ECODE")
    private String gssdSpecialmedEcode;

    /**
     * 价格状态
     */
    @Column(name = "GSSD_PRO_STATUS")
    private String gssdProStatus;

    /**
     * 批次成本价
     */
    @Column(name = "GSSD_BATCH_COST")
    private BigDecimal gssdBatchCost;

    /**
     * 批次成本税额
     */
    @Column(name = "GSSD_BATCH_TAX")
    private BigDecimal gssdBatchTax;

    /**
     * 移动平均单价
     */
    @Column(name = "GSSD_MOV_PRICE")
    private BigDecimal gssdMovPrice;

    /**
     * 移动平均总价（实际未税成本）
     */
    @Column(name = "GSSD_MOV_PRICES")
    private BigDecimal gssdMovPrices;

    /**
     * 税率
     */
    @Column(name = "GSSD_MOV_TAX")
    private BigDecimal gssdMovTax;

    /**
     * 是否生成凭证 1-是 0-否
     */
    @Column(name = "GSSD_AD_FLAG")
    private String gssdAdFlag;

    /**
     * 初始数量
     */
    @Column(name = "GSSD_ORI_QTY")
    private BigDecimal gssdOriQty;

    /**
     * 帖数
     */
    @Column(name = "GSSD_DOSE")
    private String gssdDose;

    /**
     * 移动税额（实际成本税额）
     */
    @Column(name = "GSSD_TAX_RATE")
    private BigDecimal gssdTaxRate;

    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;

    /**
     * 加点后金额（加点后未税成本）
     */
    @Column(name = "GSSD_ADD_AMT")
    private BigDecimal gssdAddAmt;

    /**
     * 加点后税金（加点后成本税额）
     */
    @Column(name = "GSSD_ADD_TAX")
    private BigDecimal gssdAddTax;

    /**
     * 是否为负库存 0 :否 / 1 :是
     */
    @Column(name = "GSSD_IF_FKC")
    private String gssdIFkc;

    /**
     * 是否关联销售标识1表示是0表示否
     */
    @Column(name = "GSSD_IF_RELATED_SALE")
    private String gssdIfRelatedSale;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取销售单号
     *
     * @return GSSD_BILL_NO - 销售单号
     */
    public String getGssdBillNo() {
        return gssdBillNo;
    }

    /**
     * 设置销售单号
     *
     * @param gssdBillNo 销售单号
     */
    public void setGssdBillNo(String gssdBillNo) {
        this.gssdBillNo = gssdBillNo;
    }

    /**
     * 获取店名
     *
     * @return GSSD_BR_ID - 店名
     */
    public String getGssdBrId() {
        return gssdBrId;
    }

    /**
     * 设置店名
     *
     * @param gssdBrId 店名
     */
    public void setGssdBrId(String gssdBrId) {
        this.gssdBrId = gssdBrId;
    }

    /**
     * 获取销售日期
     *
     * @return GSSD_DATE - 销售日期
     */
    public String getGssdDate() {
        return gssdDate;
    }

    /**
     * 设置销售日期
     *
     * @param gssdDate 销售日期
     */
    public void setGssdDate(String gssdDate) {
        this.gssdDate = gssdDate;
    }

    /**
     * 获取行号
     *
     * @return GSSD_SERIAL - 行号
     */
    public String getGssdSerial() {
        return gssdSerial;
    }

    /**
     * 设置行号
     *
     * @param gssdSerial 行号
     */
    public void setGssdSerial(String gssdSerial) {
        this.gssdSerial = gssdSerial;
    }

    /**
     * 获取商品编码
     *
     * @return GSSD_PRO_ID - 商品编码
     */
    public String getGssdProId() {
        return gssdProId;
    }

    /**
     * 设置商品编码
     *
     * @param gssdProId 商品编码
     */
    public void setGssdProId(String gssdProId) {
        this.gssdProId = gssdProId;
    }

    /**
     * 获取商品批号
     *
     * @return GSSD_BATCH_NO - 商品批号
     */
    public String getGssdBatchNo() {
        return gssdBatchNo;
    }

    /**
     * 设置商品批号
     *
     * @param gssdBatchNo 商品批号
     */
    public void setGssdBatchNo(String gssdBatchNo) {
        this.gssdBatchNo = gssdBatchNo;
    }

    /**
     * 获取商品批次
     *
     * @return GSSD_BATCH - 商品批次
     */
    public String getGssdBatch() {
        return gssdBatch;
    }

    /**
     * 设置商品批次
     *
     * @param gssdBatch 商品批次
     */
    public void setGssdBatch(String gssdBatch) {
        this.gssdBatch = gssdBatch;
    }

    /**
     * 获取商品有效期
     *
     * @return GSSD_VALID_DATE - 商品有效期
     */
    public String getGssdValidDate() {
        return gssdValidDate;
    }

    /**
     * 设置商品有效期
     *
     * @param gssdValidDate 商品有效期
     */
    public void setGssdValidDate(String gssdValidDate) {
        this.gssdValidDate = gssdValidDate;
    }

    /**
     * 获取监管码/条形码
     *
     * @return GSSD_ST_CODE - 监管码/条形码
     */
    public String getGssdStCode() {
        return gssdStCode;
    }

    /**
     * 设置监管码/条形码
     *
     * @param gssdStCode 监管码/条形码
     */
    public void setGssdStCode(String gssdStCode) {
        this.gssdStCode = gssdStCode;
    }

    /**
     * 获取单品零售价
     *
     * @return GSSD_PRC1 - 单品零售价
     */
    public BigDecimal getGssdPrc1() {
        return gssdPrc1;
    }

    /**
     * 设置单品零售价
     *
     * @param gssdPrc1 单品零售价
     */
    public void setGssdPrc1(BigDecimal gssdPrc1) {
        this.gssdPrc1 = gssdPrc1;
    }

    /**
     * 获取单品应收价
     *
     * @return GSSD_PRC2 - 单品应收价
     */
    public BigDecimal getGssdPrc2() {
        return gssdPrc2;
    }

    /**
     * 设置单品应收价
     *
     * @param gssdPrc2 单品应收价
     */
    public void setGssdPrc2(BigDecimal gssdPrc2) {
        this.gssdPrc2 = gssdPrc2;
    }

    /**
     * 获取数量
     *
     * @return GSSD_QTY - 数量
     */
    public BigDecimal getGssdQty() {
        return gssdQty;
    }

    /**
     * 设置数量
     *
     * @param gssdQty 数量
     */
    public void setGssdQty(BigDecimal gssdQty) {
        this.gssdQty = gssdQty;
    }

    /**
     * 获取汇总应收金额
     *
     * @return GSSD_AMT - 汇总应收金额
     */
    public BigDecimal getGssdAmt() {
        return gssdAmt;
    }

    /**
     * 设置汇总应收金额
     *
     * @param gssdAmt 汇总应收金额
     */
    public void setGssdAmt(BigDecimal gssdAmt) {
        this.gssdAmt = gssdAmt;
    }

    /**
     * 获取兑换单个积分
     *
     * @return GSSD_INTEGRAL_EXCHANGE_SINGLE - 兑换单个积分
     */
    public String getGssdIntegralExchangeSingle() {
        return gssdIntegralExchangeSingle;
    }

    /**
     * 设置兑换单个积分
     *
     * @param gssdIntegralExchangeSingle 兑换单个积分
     */
    public void setGssdIntegralExchangeSingle(String gssdIntegralExchangeSingle) {
        this.gssdIntegralExchangeSingle = gssdIntegralExchangeSingle;
    }

    /**
     * 获取兑换积分汇总
     *
     * @return GSSD_INTEGRAL_EXCHANGE_COLLECT - 兑换积分汇总
     */
    public String getGssdIntegralExchangeCollect() {
        return gssdIntegralExchangeCollect;
    }

    /**
     * 设置兑换积分汇总
     *
     * @param gssdIntegralExchangeCollect 兑换积分汇总
     */
    public void setGssdIntegralExchangeCollect(String gssdIntegralExchangeCollect) {
        this.gssdIntegralExchangeCollect = gssdIntegralExchangeCollect;
    }

    /**
     * 获取单个加价兑换积分金额
     *
     * @return GSSD_INTEGRAL_EXCHANGE_AMT_SINGLE - 单个加价兑换积分金额
     */
    public BigDecimal getGssdIntegralExchangeAmtSingle() {
        return gssdIntegralExchangeAmtSingle;
    }

    /**
     * 设置单个加价兑换积分金额
     *
     * @param gssdIntegralExchangeAmtSingle 单个加价兑换积分金额
     */
    public void setGssdIntegralExchangeAmtSingle(BigDecimal gssdIntegralExchangeAmtSingle) {
        this.gssdIntegralExchangeAmtSingle = gssdIntegralExchangeAmtSingle;
    }

    /**
     * 获取加价兑换积分金额汇总
     *
     * @return GSSD_INTEGRAL_EXCHANGE_AMT_COLLECT - 加价兑换积分金额汇总
     */
    public BigDecimal getGssdIntegralExchangeAmtCollect() {
        return gssdIntegralExchangeAmtCollect;
    }

    /**
     * 设置加价兑换积分金额汇总
     *
     * @param gssdIntegralExchangeAmtCollect 加价兑换积分金额汇总
     */
    public void setGssdIntegralExchangeAmtCollect(BigDecimal gssdIntegralExchangeAmtCollect) {
        this.gssdIntegralExchangeAmtCollect = gssdIntegralExchangeAmtCollect;
    }

    /**
     * 获取商品折扣总金额
     *
     * @return GSSD_ZK_AMT - 商品折扣总金额
     */
    public BigDecimal getGssdZkAmt() {
        return gssdZkAmt;
    }

    /**
     * 设置商品折扣总金额
     *
     * @param gssdZkAmt 商品折扣总金额
     */
    public void setGssdZkAmt(BigDecimal gssdZkAmt) {
        this.gssdZkAmt = gssdZkAmt;
    }

    /**
     * 获取积分兑换折扣金额
     *
     * @return GSSD_ZK_JFDH - 积分兑换折扣金额
     */
    public BigDecimal getGssdZkJfdh() {
        return gssdZkJfdh;
    }

    /**
     * 设置积分兑换折扣金额
     *
     * @param gssdZkJfdh 积分兑换折扣金额
     */
    public void setGssdZkJfdh(BigDecimal gssdZkJfdh) {
        this.gssdZkJfdh = gssdZkJfdh;
    }

    /**
     * 获取积分抵现折扣金额
     *
     * @return GSSD_ZK_JFDX - 积分抵现折扣金额
     */
    public BigDecimal getGssdZkJfdx() {
        return gssdZkJfdx;
    }

    /**
     * 设置积分抵现折扣金额
     *
     * @param gssdZkJfdx 积分抵现折扣金额
     */
    public void setGssdZkJfdx(BigDecimal gssdZkJfdx) {
        this.gssdZkJfdx = gssdZkJfdx;
    }

    /**
     * 获取电子券折扣金额
     *
     * @return GSSD_ZK_DZQ - 电子券折扣金额
     */
    public BigDecimal getGssdZkDzq() {
        return gssdZkDzq;
    }

    /**
     * 设置电子券折扣金额
     *
     * @param gssdZkDzq 电子券折扣金额
     */
    public void setGssdZkDzq(BigDecimal gssdZkDzq) {
        this.gssdZkDzq = gssdZkDzq;
    }

    /**
     * 获取抵用券折扣金额
     *
     * @return GSSD_ZK_DYQ - 抵用券折扣金额
     */
    public BigDecimal getGssdZkDyq() {
        return gssdZkDyq;
    }

    /**
     * 设置抵用券折扣金额
     *
     * @param gssdZkDyq 抵用券折扣金额
     */
    public void setGssdZkDyq(BigDecimal gssdZkDyq) {
        this.gssdZkDyq = gssdZkDyq;
    }

    /**
     * 获取促销折扣金额
     *
     * @return GSSD_ZK_PM - 促销折扣金额
     */
    public BigDecimal getGssdZkPm() {
        return gssdZkPm;
    }

    /**
     * 设置促销折扣金额
     *
     * @param gssdZkPm 促销折扣金额
     */
    public void setGssdZkPm(BigDecimal gssdZkPm) {
        this.gssdZkPm = gssdZkPm;
    }

    /**
     * 获取营业员编号
     *
     * @return GSSD_SALER_ID - 营业员编号
     */
    public String getGssdSalerId() {
        return gssdSalerId;
    }

    /**
     * 设置营业员编号
     *
     * @param gssdSalerId 营业员编号
     */
    public void setGssdSalerId(String gssdSalerId) {
        this.gssdSalerId = gssdSalerId;
    }

    /**
     * 获取医生编号
     *
     * @return GSSD_DOCTOR_ID - 医生编号
     */
    public String getGssdDoctorId() {
        return gssdDoctorId;
    }

    /**
     * 设置医生编号
     *
     * @param gssdDoctorId 医生编号
     */
    public void setGssdDoctorId(String gssdDoctorId) {
        this.gssdDoctorId = gssdDoctorId;
    }

    /**
     * 获取参加促销主题编号
     *
     * @return GSSD_PM_SUBJECT_ID - 参加促销主题编号
     */
    public String getGssdPmSubjectId() {
        return gssdPmSubjectId;
    }

    /**
     * 设置参加促销主题编号
     *
     * @param gssdPmSubjectId 参加促销主题编号
     */
    public void setGssdPmSubjectId(String gssdPmSubjectId) {
        this.gssdPmSubjectId = gssdPmSubjectId;
    }

    /**
     * 获取参加促销类型编号
     *
     * @return GSSD_PM_ID - 参加促销类型编号
     */
    public String getGssdPmId() {
        return gssdPmId;
    }

    /**
     * 设置参加促销类型编号
     *
     * @param gssdPmId 参加促销类型编号
     */
    public void setGssdPmId(String gssdPmId) {
        this.gssdPmId = gssdPmId;
    }

    /**
     * 获取参加促销类型说明
     *
     * @return GSSD_PM_CONTENT - 参加促销类型说明
     */
    public String getGssdPmContent() {
        return gssdPmContent;
    }

    /**
     * 设置参加促销类型说明
     *
     * @param gssdPmContent 参加促销类型说明
     */
    public void setGssdPmContent(String gssdPmContent) {
        this.gssdPmContent = gssdPmContent;
    }

    /**
     * 获取参加促销编号
     *
     * @return GSSD_PM_ACTIVITY_ID - 参加促销编号
     */
    public String getGssdPmActivityId() {
        return gssdPmActivityId;
    }

    /**
     * 设置参加促销编号
     *
     * @param gssdPmActivityId 参加促销编号
     */
    public void setGssdPmActivityId(String gssdPmActivityId) {
        this.gssdPmActivityId = gssdPmActivityId;
    }

    /**
     * 获取参加促销活动名称
     *
     * @return GSSD_PM_ACTIVITY_NAME - 参加促销活动名称
     */
    public String getGssdPmActivityName() {
        return gssdPmActivityName;
    }

    /**
     * 设置参加促销活动名称
     *
     * @param gssdPmActivityName 参加促销活动名称
     */
    public void setGssdPmActivityName(String gssdPmActivityName) {
        this.gssdPmActivityName = gssdPmActivityName;
    }

    /**
     * 获取参加促销活动参数
     *
     * @return GSSD_PM_ACTIVITY_FLAG - 参加促销活动参数
     */
    public String getGssdPmActivityFlag() {
        return gssdPmActivityFlag;
    }

    /**
     * 设置参加促销活动参数
     *
     * @param gssdPmActivityFlag 参加促销活动参数
     */
    public void setGssdPmActivityFlag(String gssdPmActivityFlag) {
        this.gssdPmActivityFlag = gssdPmActivityFlag;
    }

    /**
     * 获取是否登记处方信息 0否1是
     *
     * @return GSSD_RECIPEL_FLAG - 是否登记处方信息 0否1是
     */
    public String getGssdRecipelFlag() {
        return gssdRecipelFlag;
    }

    /**
     * 设置是否登记处方信息 0否1是
     *
     * @param gssdRecipelFlag 是否登记处方信息 0否1是
     */
    public void setGssdRecipelFlag(String gssdRecipelFlag) {
        this.gssdRecipelFlag = gssdRecipelFlag;
    }

    /**
     * 获取是否为关联推荐
     *
     * @return GSSD_RELATION_FLAG - 是否为关联推荐
     */
    public String getGssdRelationFlag() {
        return gssdRelationFlag;
    }

    /**
     * 设置是否为关联推荐
     *
     * @param gssdRelationFlag 是否为关联推荐
     */
    public void setGssdRelationFlag(String gssdRelationFlag) {
        this.gssdRelationFlag = gssdRelationFlag;
    }

    /**
     * 获取特殊药品购买人身份证
     *
     * @return GSSD_SPECIALMED_IDCARD - 特殊药品购买人身份证
     */
    public String getGssdSpecialmedIdcard() {
        return gssdSpecialmedIdcard;
    }

    /**
     * 设置特殊药品购买人身份证
     *
     * @param gssdSpecialmedIdcard 特殊药品购买人身份证
     */
    public void setGssdSpecialmedIdcard(String gssdSpecialmedIdcard) {
        this.gssdSpecialmedIdcard = gssdSpecialmedIdcard;
    }

    /**
     * 获取特殊药品购买人姓名
     *
     * @return GSSD_SPECIALMED_NAME - 特殊药品购买人姓名
     */
    public String getGssdSpecialmedName() {
        return gssdSpecialmedName;
    }

    /**
     * 设置特殊药品购买人姓名
     *
     * @param gssdSpecialmedName 特殊药品购买人姓名
     */
    public void setGssdSpecialmedName(String gssdSpecialmedName) {
        this.gssdSpecialmedName = gssdSpecialmedName;
    }

    /**
     * 获取特殊药品购买人性别
     *
     * @return GSSD_SPECIALMED_SEX - 特殊药品购买人性别
     */
    public String getGssdSpecialmedSex() {
        return gssdSpecialmedSex;
    }

    /**
     * 设置特殊药品购买人性别
     *
     * @param gssdSpecialmedSex 特殊药品购买人性别
     */
    public void setGssdSpecialmedSex(String gssdSpecialmedSex) {
        this.gssdSpecialmedSex = gssdSpecialmedSex;
    }

    /**
     * 获取特殊药品购买人出生日期
     *
     * @return GSSD_SPECIALMED_BIRTHDAY - 特殊药品购买人出生日期
     */
    public String getGssdSpecialmedBirthday() {
        return gssdSpecialmedBirthday;
    }

    /**
     * 设置特殊药品购买人出生日期
     *
     * @param gssdSpecialmedBirthday 特殊药品购买人出生日期
     */
    public void setGssdSpecialmedBirthday(String gssdSpecialmedBirthday) {
        this.gssdSpecialmedBirthday = gssdSpecialmedBirthday;
    }

    /**
     * 获取特殊药品购买人手机
     *
     * @return GSSD_SPECIALMED_MOBILE - 特殊药品购买人手机
     */
    public String getGssdSpecialmedMobile() {
        return gssdSpecialmedMobile;
    }

    /**
     * 设置特殊药品购买人手机
     *
     * @param gssdSpecialmedMobile 特殊药品购买人手机
     */
    public void setGssdSpecialmedMobile(String gssdSpecialmedMobile) {
        this.gssdSpecialmedMobile = gssdSpecialmedMobile;
    }

    /**
     * 获取特殊药品购买人地址
     *
     * @return GSSD_SPECIALMED_ADDRESS - 特殊药品购买人地址
     */
    public String getGssdSpecialmedAddress() {
        return gssdSpecialmedAddress;
    }

    /**
     * 设置特殊药品购买人地址
     *
     * @param gssdSpecialmedAddress 特殊药品购买人地址
     */
    public void setGssdSpecialmedAddress(String gssdSpecialmedAddress) {
        this.gssdSpecialmedAddress = gssdSpecialmedAddress;
    }

    /**
     * 获取特殊药品电子监管码
     *
     * @return GSSD_SPECIALMED_ECODE - 特殊药品电子监管码
     */
    public String getGssdSpecialmedEcode() {
        return gssdSpecialmedEcode;
    }

    /**
     * 设置特殊药品电子监管码
     *
     * @param gssdSpecialmedEcode 特殊药品电子监管码
     */
    public void setGssdSpecialmedEcode(String gssdSpecialmedEcode) {
        this.gssdSpecialmedEcode = gssdSpecialmedEcode;
    }

    /**
     * 获取价格状态
     *
     * @return GSSD_PRO_STATUS - 价格状态
     */
    public String getGssdProStatus() {
        return gssdProStatus;
    }

    /**
     * 设置价格状态
     *
     * @param gssdProStatus 价格状态
     */
    public void setGssdProStatus(String gssdProStatus) {
        this.gssdProStatus = gssdProStatus;
    }

    /**
     * 获取批次成本价
     *
     * @return GSSD_BATCH_COST - 批次成本价
     */
    public BigDecimal getGssdBatchCost() {
        return gssdBatchCost;
    }

    /**
     * 设置批次成本价
     *
     * @param gssdBatchCost 批次成本价
     */
    public void setGssdBatchCost(BigDecimal gssdBatchCost) {
        this.gssdBatchCost = gssdBatchCost;
    }

    /**
     * 获取批次成本税额
     *
     * @return GSSD_BATCH_TAX - 批次成本税额
     */
    public BigDecimal getGssdBatchTax() {
        return gssdBatchTax;
    }

    /**
     * 设置批次成本税额
     *
     * @param gssdBatchTax 批次成本税额
     */
    public void setGssdBatchTax(BigDecimal gssdBatchTax) {
        this.gssdBatchTax = gssdBatchTax;
    }

    /**
     * 获取移动平均单价
     *
     * @return GSSD_MOV_PRICE - 移动平均单价
     */
    public BigDecimal getGssdMovPrice() {
        return gssdMovPrice;
    }

    /**
     * 设置移动平均单价
     *
     * @param gssdMovPrice 移动平均单价
     */
    public void setGssdMovPrice(BigDecimal gssdMovPrice) {
        this.gssdMovPrice = gssdMovPrice;
    }

    /**
     * 获取移动平均总价（实际未税成本）
     *
     * @return GSSD_MOV_PRICES - 移动平均总价（实际未税成本）
     */
    public BigDecimal getGssdMovPrices() {
        return gssdMovPrices;
    }

    /**
     * 设置移动平均总价（实际未税成本）
     *
     * @param gssdMovPrices 移动平均总价（实际未税成本）
     */
    public void setGssdMovPrices(BigDecimal gssdMovPrices) {
        this.gssdMovPrices = gssdMovPrices;
    }

    /**
     * 获取税率
     *
     * @return GSSD_MOV_TAX - 税率
     */
    public BigDecimal getGssdMovTax() {
        return gssdMovTax;
    }

    /**
     * 设置税率
     *
     * @param gssdMovTax 税率
     */
    public void setGssdMovTax(BigDecimal gssdMovTax) {
        this.gssdMovTax = gssdMovTax;
    }

    /**
     * 获取是否生成凭证 1-是 0-否
     *
     * @return GSSD_AD_FLAG - 是否生成凭证 1-是 0-否
     */
    public String getGssdAdFlag() {
        return gssdAdFlag;
    }

    /**
     * 设置是否生成凭证 1-是 0-否
     *
     * @param gssdAdFlag 是否生成凭证 1-是 0-否
     */
    public void setGssdAdFlag(String gssdAdFlag) {
        this.gssdAdFlag = gssdAdFlag;
    }

    /**
     * 获取初始数量
     *
     * @return GSSD_ORI_QTY - 初始数量
     */
    public BigDecimal getGssdOriQty() {
        return gssdOriQty;
    }

    /**
     * 设置初始数量
     *
     * @param gssdOriQty 初始数量
     */
    public void setGssdOriQty(BigDecimal gssdOriQty) {
        this.gssdOriQty = gssdOriQty;
    }

    /**
     * 获取帖数
     *
     * @return GSSD_DOSE - 帖数
     */
    public String getGssdDose() {
        return gssdDose;
    }

    /**
     * 设置帖数
     *
     * @param gssdDose 帖数
     */
    public void setGssdDose(String gssdDose) {
        this.gssdDose = gssdDose;
    }

    /**
     * 获取移动税额（实际成本税额）
     *
     * @return GSSD_TAX_RATE - 移动税额（实际成本税额）
     */
    public BigDecimal getGssdTaxRate() {
        return gssdTaxRate;
    }

    /**
     * 设置移动税额（实际成本税额）
     *
     * @param gssdTaxRate 移动税额（实际成本税额）
     */
    public void setGssdTaxRate(BigDecimal gssdTaxRate) {
        this.gssdTaxRate = gssdTaxRate;
    }

    /**
     * @return LAST_UPDATE_TIME
     */
    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    /**
     * @param lastUpdateTime
     */
    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    /**
     * 获取加点后金额（加点后未税成本）
     *
     * @return GSSD_ADD_AMT - 加点后金额（加点后未税成本）
     */
    public BigDecimal getGssdAddAmt() {
        return gssdAddAmt;
    }

    /**
     * 设置加点后金额（加点后未税成本）
     *
     * @param gssdAddAmt 加点后金额（加点后未税成本）
     */
    public void setGssdAddAmt(BigDecimal gssdAddAmt) {
        this.gssdAddAmt = gssdAddAmt;
    }

    /**
     * 获取加点后税金（加点后成本税额）
     *
     * @return GSSD_ADD_TAX - 加点后税金（加点后成本税额）
     */
    public BigDecimal getGssdAddTax() {
        return gssdAddTax;
    }

    /**
     * 设置加点后税金（加点后成本税额）
     *
     * @param gssdAddTax 加点后税金（加点后成本税额）
     */
    public void setGssdAddTax(BigDecimal gssdAddTax) {
        this.gssdAddTax = gssdAddTax;
    }

    /**
     * 获取是否为负库存 0 :否 / 1 :是
     *
     * @return GSSD_IF_FKC - 是否为负库存 0 :否 / 1 :是
     */
    public String getGssdIFkc() {
        return gssdIFkc;
    }

    /**
     * 设置是否为负库存 0 :否 / 1 :是
     *
     * @param gssdIFkc 是否为负库存 0 :否 / 1 :是
     */
    public void setGssdIFkc(String gssdIFkc) {
        this.gssdIFkc = gssdIFkc;
    }

    /**
     * 获取是否关联销售标识1表示是0表示否
     *
     * @return GSSD_IF_RELATED_SALE - 是否关联销售标识1表示是0表示否
     */
    public String getGssdIfRelatedSale() {
        return gssdIfRelatedSale;
    }

    /**
     * 设置是否关联销售标识1表示是0表示否
     *
     * @param gssdIfRelatedSale 是否关联销售标识1表示是0表示否
     */
    public void setGssdIfRelatedSale(String gssdIfRelatedSale) {
        this.gssdIfRelatedSale = gssdIfRelatedSale;
    }
}