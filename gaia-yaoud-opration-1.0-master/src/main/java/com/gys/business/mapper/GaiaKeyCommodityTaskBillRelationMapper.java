package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBill;
import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBillRelation;
import com.gys.business.mapper.entity.GaiaSalesCatalog;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillInData;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillRelationInData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 重点商品任务—关系表(GaiaKeyCommodityTaskBillRelation)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-01 15:38:59
 */
public interface GaiaKeyCommodityTaskBillRelationMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaKeyCommodityTaskBillRelation queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaKeyCommodityTaskBillRelation> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaKeyCommodityTaskBillRelation 实例对象
     * @return 对象列表
     */
    List<GaiaKeyCommodityTaskBillRelation> queryAll(GaiaKeyCommodityTaskBillRelation gaiaKeyCommodityTaskBillRelation);

    /**
     * 新增数据
     *
     * @param gaiaKeyCommodityTaskBillRelation 实例对象
     * @return 影响行数
     */
    int insert(GaiaKeyCommodityTaskBillRelation gaiaKeyCommodityTaskBillRelation);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaKeyCommodityTaskBillRelation> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaKeyCommodityTaskBillRelation> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaKeyCommodityTaskBillRelation> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaKeyCommodityTaskBillRelation> entities);

    /**
     * 修改数据
     *
     * @param gaiaKeyCommodityTaskBillRelation 实例对象
     * @return 影响行数
     */
    int update(GaiaKeyCommodityTaskBillRelation gaiaKeyCommodityTaskBillRelation);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    int getCount(GaiaKeyCommodityTaskBillInData inData);


    GaiaKeyCommodityTaskBillRelation getRelationInfoByCode(GaiaKeyCommodityTaskBillRelationInData param);

    GaiaKeyCommodityTaskBill getPlanInfo(GaiaKeyCommodityTaskBillRelationInData inData);

    List<GaiaKeyCommodityTaskBillRelation> listInfo(GaiaKeyCommodityTaskBillInData inData);

    GaiaKeyCommodityTaskBillRelation getProductInfo(GaiaKeyCommodityTaskBillInData param);
}

