package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Data
@Table(name = "GAIA_AL_PL_QJMD")
public class GaiaAlPlQjmd implements Serializable {
    /**
     * 模型
     */
    @Id
    @Column(name = "APL_MODEL")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String aplModel;

    /**
     * 区域
     */
    @Id
    @Column(name = "APL_AREA")
    private String aplArea;

    /**
     * 成分
     */
    @Id
    @Column(name = "APL_COMP")
    private String aplComp;

    /**
     * 区间段编号
     */
    @Id
    @Column(name = "APL_QJD_CODE")
    private String aplQjdCode;

    /**
     * 价格上限
     */
    @Column(name = "APL_PRICE_UP")
    private BigDecimal aplPriceUp;

    /**
     * 价格下限
     */
    @Column(name = "APL_PRICE_LOW")
    private BigDecimal aplPriceLow;

    /**
     * 销售量
     */
    @Column(name = "APL_SALES_QTY")
    private BigDecimal aplSalesQty;

    /**
     * 销售额
     */
    @Column(name = "APL_SALES_AMT")
    private BigDecimal aplSalesAmt;

    /**
     * 毛利额
     */
    @Column(name = "APL_GROSS_AMT")
    private BigDecimal aplGrossAmt;

    /**
     * 动销品种数
     */
    @Column(name = "APL_MS_PRO")
    private BigDecimal aplMsPro;

    /**
     * 动销小票计数
     */
    @Column(name = "APL_MS_BILL")
    private BigDecimal aplMsBill;

    /**
     * 每个品种的月动销门店数之和
     */
    @Column(name = "APL_MS_BRS")
    private BigDecimal aplMsBrs;

    /**
     * 每个品种的月动销天数之和
     */
    @Column(name = "APL_MS_DAYS")
    private BigDecimal aplMsDays;

    /**
     * 有效期起
     */
    @Column(name = "APL_DATE_FROM")
    private String aplDateFrom;

    /**
     * 有效期止
     */
    @Column(name = "APL_DATE_END")
    private String aplDateEnd;

    /**
     * 更新日期
     */
    @Column(name = "APL_UPDATE_DATE")
    private String aplUpdateDate;

    /**
     * 更新时间
     */
    @Column(name = "APL_UPDATE_TIME")
    private String aplUpdateTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取模型
     *
     * @return APL_MODEL - 模型
     */
    public String getAplModel() {
        return aplModel;
    }

    /**
     * 设置模型
     *
     * @param aplModel 模型
     */
    public void setAplModel(String aplModel) {
        this.aplModel = aplModel;
    }

    /**
     * 获取区域
     *
     * @return APL_AREA - 区域
     */
    public String getAplArea() {
        return aplArea;
    }

    /**
     * 设置区域
     *
     * @param aplArea 区域
     */
    public void setAplArea(String aplArea) {
        this.aplArea = aplArea;
    }

    /**
     * 获取成分
     *
     * @return APL_COMP - 成分
     */
    public String getAplComp() {
        return aplComp;
    }

    /**
     * 设置成分
     *
     * @param aplComp 成分
     */
    public void setAplComp(String aplComp) {
        this.aplComp = aplComp;
    }

    /**
     * 获取区间段编号
     *
     * @return APL_QJD_CODE - 区间段编号
     */
    public String getAplQjdCode() {
        return aplQjdCode;
    }

    /**
     * 设置区间段编号
     *
     * @param aplQjdCode 区间段编号
     */
    public void setAplQjdCode(String aplQjdCode) {
        this.aplQjdCode = aplQjdCode;
    }

    /**
     * 获取价格上限
     *
     * @return APL_PRICE_UP - 价格上限
     */
    public BigDecimal getAplPriceUp() {
        return aplPriceUp;
    }

    /**
     * 设置价格上限
     *
     * @param aplPriceUp 价格上限
     */
    public void setAplPriceUp(BigDecimal aplPriceUp) {
        this.aplPriceUp = aplPriceUp;
    }

    /**
     * 获取价格下限
     *
     * @return APL_PRICE_LOW - 价格下限
     */
    public BigDecimal getAplPriceLow() {
        return aplPriceLow;
    }

    /**
     * 设置价格下限
     *
     * @param aplPriceLow 价格下限
     */
    public void setAplPriceLow(BigDecimal aplPriceLow) {
        this.aplPriceLow = aplPriceLow;
    }

    /**
     * 获取销售量
     *
     * @return APL_SALES_QTY - 销售量
     */
    public BigDecimal getAplSalesQty() {
        return aplSalesQty;
    }

    /**
     * 设置销售量
     *
     * @param aplSalesQty 销售量
     */
    public void setAplSalesQty(BigDecimal aplSalesQty) {
        this.aplSalesQty = aplSalesQty;
    }

    /**
     * 获取销售额
     *
     * @return APL_SALES_AMT - 销售额
     */
    public BigDecimal getAplSalesAmt() {
        return aplSalesAmt;
    }

    /**
     * 设置销售额
     *
     * @param aplSalesAmt 销售额
     */
    public void setAplSalesAmt(BigDecimal aplSalesAmt) {
        this.aplSalesAmt = aplSalesAmt;
    }

    /**
     * 获取毛利额
     *
     * @return APL_GROSS_AMT - 毛利额
     */
    public BigDecimal getAplGrossAmt() {
        return aplGrossAmt;
    }

    /**
     * 设置毛利额
     *
     * @param aplGrossAmt 毛利额
     */
    public void setAplGrossAmt(BigDecimal aplGrossAmt) {
        this.aplGrossAmt = aplGrossAmt;
    }

    /**
     * 获取动销品种数
     *
     * @return APL_MS_PRO - 动销品种数
     */
    public BigDecimal getAplMsPro() {
        return aplMsPro;
    }

    /**
     * 设置动销品种数
     *
     * @param aplMsPro 动销品种数
     */
    public void setAplMsPro(BigDecimal aplMsPro) {
        this.aplMsPro = aplMsPro;
    }

    /**
     * 获取动销小票计数
     *
     * @return APL_MS_BILL - 动销小票计数
     */
    public BigDecimal getAplMsBill() {
        return aplMsBill;
    }

    /**
     * 设置动销小票计数
     *
     * @param aplMsBill 动销小票计数
     */
    public void setAplMsBill(BigDecimal aplMsBill) {
        this.aplMsBill = aplMsBill;
    }

    /**
     * 获取每个品种的月动销门店数之和
     *
     * @return APL_MS_BRS - 每个品种的月动销门店数之和
     */
    public BigDecimal getAplMsBrs() {
        return aplMsBrs;
    }

    /**
     * 设置每个品种的月动销门店数之和
     *
     * @param aplMsBrs 每个品种的月动销门店数之和
     */
    public void setAplMsBrs(BigDecimal aplMsBrs) {
        this.aplMsBrs = aplMsBrs;
    }

    /**
     * 获取每个品种的月动销天数之和
     *
     * @return APL_MS_DAYS - 每个品种的月动销天数之和
     */
    public BigDecimal getAplMsDays() {
        return aplMsDays;
    }

    /**
     * 设置每个品种的月动销天数之和
     *
     * @param aplMsDays 每个品种的月动销天数之和
     */
    public void setAplMsDays(BigDecimal aplMsDays) {
        this.aplMsDays = aplMsDays;
    }

    /**
     * 获取有效期起
     *
     * @return APL_DATE_FROM - 有效期起
     */
    public String getAplDateFrom() {
        return aplDateFrom;
    }

    /**
     * 设置有效期起
     *
     * @param aplDateFrom 有效期起
     */
    public void setAplDateFrom(String aplDateFrom) {
        this.aplDateFrom = aplDateFrom;
    }

    /**
     * 获取有效期止
     *
     * @return APL_DATE_END - 有效期止
     */
    public String getAplDateEnd() {
        return aplDateEnd;
    }

    /**
     * 设置有效期止
     *
     * @param aplDateEnd 有效期止
     */
    public void setAplDateEnd(String aplDateEnd) {
        this.aplDateEnd = aplDateEnd;
    }

    /**
     * 获取更新日期
     *
     * @return APL_UPDATE_DATE - 更新日期
     */
    public String getAplUpdateDate() {
        return aplUpdateDate;
    }

    /**
     * 设置更新日期
     *
     * @param aplUpdateDate 更新日期
     */
    public void setAplUpdateDate(String aplUpdateDate) {
        this.aplUpdateDate = aplUpdateDate;
    }

    /**
     * 获取更新时间
     *
     * @return APL_UPDATE_TIME - 更新时间
     */
    public String getAplUpdateTime() {
        return aplUpdateTime;
    }

    /**
     * 设置更新时间
     *
     * @param aplUpdateTime 更新时间
     */
    public void setAplUpdateTime(String aplUpdateTime) {
        this.aplUpdateTime = aplUpdateTime;
    }
}