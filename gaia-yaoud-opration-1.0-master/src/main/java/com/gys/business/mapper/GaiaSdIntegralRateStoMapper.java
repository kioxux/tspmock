package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralRateSto;
import com.gys.common.base.BaseMapper;
import com.gys.common.request.InsertProductStortReq;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface GaiaSdIntegralRateStoMapper extends BaseMapper<GaiaSdIntegralRateSto> {
    /**
     * 修改会员积分商品设置
     * @param productStortList
     * @param client
     */
    void update(@Param("list") List<InsertProductStortReq> productStortList,@Param("client") String client);

    List<InsertProductStortReq> selectStorts(@Param("id")String id,@Param("client") String client);

    List<GaiaSdIntegralRateSto> getAllByClientAndBrId(@Param("client")String client, @Param("brId") String depId);

}
