package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaAlPlQjmd;

import java.util.HashMap;
import java.util.List;

public interface GaiaAlPlExecMapper {
    List<GaiaAlPlQjmd> getSaleSummaryByArea(GaiaAlPlQjmd inData);

    List<GaiaAlPlQjmd> getSaleInfoByComp(HashMap<String,Object> inHashMap);
}
