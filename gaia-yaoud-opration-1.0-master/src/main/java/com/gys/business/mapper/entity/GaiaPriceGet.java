package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "GAIA_PRICE_GET")
@Data
public class GaiaPriceGet implements Serializable {
    private static final long serialVersionUID = 7340912316728521606L;
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "PRG_SITE_CODE"
    )
    private String prgSiteCode;
    @Id
    @Column(
            name = "PRG_PRO_CODE"
    )
    private String prgProCode;
    @Id
    @Column(
            name = "PRG_SUPPLIER_CODE"
    )
    private String prgSupplierCode;
    @Column(
            name = "PRG_PRICE"
    )
    private BigDecimal prgPrice;
    @Column(
            name = "PRG_PRODUCT_DATE"
    )
    private String prgProductDate;
    @Column(
            name = "PRG_EXPIRY_DATE"
    )
    private String prgExpiryDate;
    @Column(
            name = "PRG_BATCH_NO"
    )
    private String prgBatchNo;
    @Column(
            name = "PRG_GET_DATE"
    )
    private String prgGetDate;
    @Column(
            name = "GSRD_VOUCHER_ID"
    )
    private String gsrdVoucherId;
    @Column(
            name = "GSRD_SERIAL"
    )
    private String gsrdSerial;
    @Column(
            name = "PRG_CREATE_BY"
    )
    private String prgCreateBy;
    @Column(
            name = "PRG_CREATE_DATE"
    )
    private String prgCreateDate;
    @Column(
            name = "PRG_CREATE_TIME"
    )
    private String prgCreateTime;
    @Column(
            name = "PRG_UPDATE_BY"
    )
    private String prgUpdateBy;
    @Column(
            name = "PRG_UPDATE_DATE"
    )
    private String prgUpdateDate;
    @Column(
            name = "PRG_UPDATE_TIME"
    )
    private String prgUpdateTime;
}
