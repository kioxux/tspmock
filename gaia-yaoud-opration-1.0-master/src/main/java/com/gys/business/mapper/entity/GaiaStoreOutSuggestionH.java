package com.gys.business.mapper.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 门店调出建议-主表(GaiaStoreOutSuggestionH)实体类
 *
 * @author makejava
 * @since 2021-10-28 10:54:04
 */
public class GaiaStoreOutSuggestionH implements Serializable {
    private static final long serialVersionUID = -38256365616900798L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店编码
     */
    private String stoCode;
    /**
     * 门店名称
     */
    private String stoName;
    /**
     * 商品调出单号
     */
    private String billCode;
    /**
     * 计算周期 week-周 month-月
     */
    private String type;
    /**
     * 单据日期
     */
    private Date billDate;
    /**
     * 商品调库失效日期
     */
    private Date invalidDate;
    /**
     * 单据状态：0-待处理 1-已完成 2-已失效
     */
    private Integer status;
    /**
     * 商品调库品项数
     */
    private Integer itemsQty;
    /**
     * 完成日期
     */
    private Date finishTime;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getStoCode() {
        return stoCode;
    }

    public void setStoCode(String stoCode) {
        this.stoCode = stoCode;
    }

    public String getStoName() {
        return stoName;
    }

    public void setStoName(String stoName) {
        this.stoName = stoName;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public Date getInvalidDate() {
        return invalidDate;
    }

    public void setInvalidDate(Date invalidDate) {
        this.invalidDate = invalidDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getItemsQty() {
        return itemsQty;
    }

    public void setItemsQty(Integer itemsQty) {
        this.itemsQty = itemsQty;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}

