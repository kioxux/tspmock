//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdCleandrawerH;
import com.gys.business.service.data.ClearBoxInfoOutData;
import com.gys.business.service.data.ExamineTipOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdCleandrawerHMapper extends BaseMapper<GaiaSdCleandrawerH> {
    String selectMaxgschVoucherId();

    void insertData(ExamineTipOutData inData);

    List<ClearBoxInfoOutData> getClearBox(ClearBoxInfoOutData inData);
}
