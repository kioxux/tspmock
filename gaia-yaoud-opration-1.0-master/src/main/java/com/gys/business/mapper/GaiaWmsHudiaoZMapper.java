package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWmsHudiaoZ;
import com.gys.business.service.data.GaiaWmsHudiaoZOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaWmsHudiaoZMapper extends BaseMapper<GaiaWmsHudiaoZ> {
    List<GaiaWmsHudiaoZOutData> listAdjustBill(GaiaWmsHudiaoZOutData map);

    List<GaiaWmsHudiaoZOutData> listAdjustBillDetail(GaiaWmsHudiaoZOutData param);
}
