package com.gys.business.mapper;

import com.gys.business.mapper.entity.SystemPara;
import com.gys.business.mapper.entity.ReplenishParam;
import com.gys.business.service.data.replenishParam.ReplenishParamInData;
import com.gys.business.service.data.replenishParam.ReplenishParamOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 系统模块参数表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-08-13
 */
@Mapper
public interface SystemParaMapper extends BaseMapper<SystemPara> {
    List<ReplenishParamOutData> selectReplenishParamList(ReplenishParamInData inData);

    void batchAddReplenishParam(List<ReplenishParam> inData);
}
