package com.gys.business.mapper;

import com.gys.business.service.data.AcceptOrderInData;
import com.gys.business.service.data.AcceptOrderOutData;
import com.gys.business.service.data.AcceptOrderTotalOutData;
import com.gys.business.service.data.PoOrderOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaAcceptMapper {
    List<AcceptOrderOutData> selectAcceptProduct(AcceptOrderInData inData);

    AcceptOrderTotalOutData selectAcceptTotal(AcceptOrderInData inData);

    List<PoOrderOutData> selectOrderList(AcceptOrderInData inData);

    List<Map<String, String>> getBusEmpListBySupSelfCode(@Param("client") String client, @Param("brId") String brId, @Param("supSelfCode") String supSelfCode);

    String getBusEmpNameListByEmpCode(@Param("client") String client, @Param("brId") String brId,
                                      @Param("supSelfCode") String supSelfCode, @Param("empCode") String empCode);
}
