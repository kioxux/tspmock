package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
    * 会员卡变动表
    */
@AllArgsConstructor
@ApiModel(value="com-gys-business-mapper-entity-GaiaSdMemberCardChange")
@Data
public class GaiaSdMemberCardChange implements Serializable {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 操作门店
    */
    @ApiModelProperty(value="操作门店")
    private String brId;

    /**
    * 会员ID（原会员编号）
    */
    @ApiModelProperty(value="会员ID（原会员编号）")
    private String oldMemberId;

    /**
    * 新会员ID
    */
    @ApiModelProperty(value="新会员ID")
    private String newMemberId;

    /**
    * 原卡号
    */
    @ApiModelProperty(value="原卡号")
    private String oldCardId;

    /**
    * 初始卡号
    */
    @ApiModelProperty(value="初始卡号")
    private String initCardId;

    /**
    * 新卡号
    */
    @ApiModelProperty(value="新卡号")
    private String newCardId;

    /**
    * 卡类型 1-挂失 2-解挂 3-换卡（注销）
    */
    @ApiModelProperty(value="卡类型 1-挂失 2-解挂 3-换卡（注销）")
    private String cardType;

    /**
    * 操作原因
    */
    @ApiModelProperty(value="操作原因")
    private String remark;

    /**
    * 操作人员编号
    */
    @ApiModelProperty(value="操作人员编号")
    private String empId;

    /**
    * 操作时间
    */
    @ApiModelProperty(value="操作时间")
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBrId() {
        return brId;
    }

    public void setBrId(String brId) {
        this.brId = brId;
    }

    public String getOldMemberId() {
        return oldMemberId;
    }

    public void setOldMemberId(String oldMemberId) {
        this.oldMemberId = oldMemberId;
    }

    public String getNewMemberId() {
        return newMemberId;
    }

    public void setNewMemberId(String newMemberId) {
        this.newMemberId = newMemberId;
    }

    public String getOldCardId() {
        return oldCardId;
    }

    public void setOldCardId(String oldCardId) {
        this.oldCardId = oldCardId;
    }

    public String getInitCardId() {
        return initCardId;
    }

    public void setInitCardId(String initCardId) {
        this.initCardId = initCardId;
    }

    public String getNewCardId() {
        return newCardId;
    }

    public void setNewCardId(String newCardId) {
        this.newCardId = newCardId;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }


    public GaiaSdMemberCardChange() {}
}