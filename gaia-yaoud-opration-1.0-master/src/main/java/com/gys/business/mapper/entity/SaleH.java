package com.gys.business.mapper.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
//@JSONType(orders={"billNo","billNoReturn","brId","client","date","emp","emp","hykNo","integralAdd","lastUpdateTime","time","ysAmt","zkAmt"})
public class SaleH {
    /**
     * 加盟商
     */
    @NotNull
    @JSONField(ordinal=4)
    private String client;

    /**
     * 销售单号
     */
    @NotNull
    @JSONField(ordinal=1)
    private String billNo;

    /**
     * 店名
     */
    @NotNull
    @JSONField(ordinal=3)
    private String brId;

    /**
     * 销售日期
     */
    @NotNull
    @JSONField(ordinal=5)
    private String date;

    /**
     * 销售时间
     */
    @NotNull
    @JSONField(ordinal=11)
    private String time;

    /**
     * 收银工号
     */
    @JSONField(ordinal=6)
    private String emp;

    /**
     * 会员卡号
     */
    @JSONField(ordinal=7)
    private String hykNo;

    /**
     * 零售价格
     */
    @NotNull
    @JSONField(ordinal=10)
    private BigDecimal normalAmt;

    /**
     * 折扣金额
     */
    @JSONField(ordinal=13)
    private BigDecimal zkAmt;

    /**
     * 应收金额
     */
    @NotNull
    @JSONField(ordinal=12)
    private BigDecimal ysAmt;

    /**
     * 增加积分
     */
    @JSONField(ordinal=8)
    private BigDecimal integralAdd;

    /**
     * 退货原销售单号
     */
    @JSONField(ordinal=2)
    private String billNoReturn;

    @JSONField(ordinal=9,serialzeFeatures = {SerializerFeature.WriteMapNullValue})
    private String lastUpdateTime;

    private static final long serialVersionUID = 1L;

//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder("{");
//        sb.append(",\"billNo\":\"")
//                .append(billNo).append('\"');
//        sb.append(",\"billNoReturn\":\"")
//                .append(billNoReturn).append('\"');
//        sb.append(",\"brId\":\"")
//                .append(brId).append('\"');
//        sb.append("\"client\":\"")
//                .append(client).append('\"');
//        sb.append(",\"date\":\"")
//                .append(date).append('\"');
//        sb.append(",\"emp\":\"")
//                .append(emp).append('\"');
//        sb.append(",\"hykNo\":\"")
//                .append(hykNo).append('\"');
//        sb.append(",\"integralAdd\":\"")
//                .append(integralAdd).append('\"');
//        sb.append(",\"lastUpdateTime\":\"")
//                .append(lastUpdateTime).append('\"');
//        sb.append(",\"normalAmt\":")
//                .append(normalAmt);
//        sb.append(",\"time\":\"")
//                .append(time).append('\"');
//        sb.append(",\"ysAmt\":")
//                .append(ysAmt);
//        sb.append(",\"zkAmt\":")
//                .append(zkAmt);
//        sb.append('}');
//        return sb.toString();
//    }
}