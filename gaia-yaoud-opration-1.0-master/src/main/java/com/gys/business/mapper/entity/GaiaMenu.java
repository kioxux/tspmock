package com.gys.business.mapper.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "GAIA_MENU")
public class GaiaMenu implements Serializable {
    /**
     * 序号
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 菜单编码
     */
    @Column(name = "CODE")
    private String code;

    /**
     * 名称
     */
    @Column(name = "NAME")
    private String name;

    /**
     * 标题
     */
    @Column(name = "TITLE")
    private String title;

    /**
     * 路由/跳转地址
     */
    @Column(name = "PATH")
    private String path;

    /**
     * 页面地址
     */
    @Column(name = "PAGE_PATH")
    private String pagePath;

    /**
     * 图标
     */
    @Column(name = "ICON")
    private String icon;

    /**
     * 类型（1模块 2页面）(废弃)
     */
    @Column(name = "TYPE")
    private Integer type;

    /**
     * 父节点（空为最高节点）
     */
    @Column(name = "PARENT_ID")
    private Long parentId;

    /**
     * 节点数组（从一级开始）
     */
    @Column(name = "PARENT_ID_LIST")
    private String parentIdList;

    /**
     * 1缓存  0不缓存
     */
    @Column(name = "KEEP_ALIVE")
    private Boolean keepAlive;

    /**
     * 1隐藏 0不隐藏
     */
    @Column(name = "DETAIS_PAGE")
    private Boolean detaisPage;

    /**
     * 状态( 1.启用 2.删除 )
     */
    @Column(name = "STATUS")
    private String status;

    /**
     * 排序
     */
    @Column(name = "SEQ")
    private Integer seq;

    /**
     * 1.web 
     */
    @Column(name = "MODULE")
    private String module;

    /**
     * 创建人
     */
    @Column(name = "CREATOR")
    private String creator;

    /**
     * 创建时间

     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 更新人
     */
    @Column(name = "UPDATOR")
    private String updator;

    /**
     * 更新时间

     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    private static final long serialVersionUID = 1L;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取名称
     *
     * @return NAME - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取标题
     *
     * @return TITLE - 标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取路由/跳转地址
     *
     * @return PATH - 路由/跳转地址
     */
    public String getPath() {
        return path;
    }

    /**
     * 设置路由/跳转地址
     *
     * @param path 路由/跳转地址
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * 获取页面地址
     *
     * @return PAGE_PATH - 页面地址
     */
    public String getPagePath() {
        return pagePath;
    }

    /**
     * 设置页面地址
     *
     * @param pagePath 页面地址
     */
    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    /**
     * 获取图标
     *
     * @return ICON - 图标
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 设置图标
     *
     * @param icon 图标
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 获取类型（1模块 2页面）(废弃)
     *
     * @return TYPE - 类型（1模块 2页面）(废弃)
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置类型（1模块 2页面）(废弃)
     *
     * @param type 类型（1模块 2页面）(废弃)
     */
    public void setType(Integer type) {
        this.type = type;
    }


    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取节点数组（从一级开始）
     *
     * @return PARENT_ID_LIST - 节点数组（从一级开始）
     */
    public String getParentIdList() {
        return parentIdList;
    }

    /**
     * 设置节点数组（从一级开始）
     *
     * @param parentIdList 节点数组（从一级开始）
     */
    public void setParentIdList(String parentIdList) {
        this.parentIdList = parentIdList;
    }

    /**
     * 获取1缓存  0不缓存
     *
     * @return KEEP_ALIVE - 1缓存  0不缓存
     */
    public Boolean getKeepAlive() {
        return keepAlive;
    }

    /**
     * 设置1缓存  0不缓存
     *
     * @param keepAlive 1缓存  0不缓存
     */
    public void setKeepAlive(Boolean keepAlive) {
        this.keepAlive = keepAlive;
    }

    /**
     * 获取1隐藏 0不隐藏
     *
     * @return DETAIS_PAGE - 1隐藏 0不隐藏
     */
    public Boolean getDetaisPage() {
        return detaisPage;
    }

    /**
     * 设置1隐藏 0不隐藏
     *
     * @param detaisPage 1隐藏 0不隐藏
     */
    public void setDetaisPage(Boolean detaisPage) {
        this.detaisPage = detaisPage;
    }

    /**
     * 获取状态( 1.启用 2.删除 )
     *
     * @return STATUS - 状态( 1.启用 2.删除 )
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态( 1.启用 2.删除 )
     *
     * @param status 状态( 1.启用 2.删除 )
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取排序
     *
     * @return SEQ - 排序
     */
    public Integer getSeq() {
        return seq;
    }

    /**
     * 设置排序
     *
     * @param seq 排序
     */
    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    /**
     * 获取1.web 
     *
     * @return MODULE - 1.web 
     */
    public String getModule() {
        return module;
    }

    /**
     * 设置1.web 
     *
     * @param module 1.web 
     */
    public void setModule(String module) {
        this.module = module;
    }

    /**
     * 获取创建人
     *
     * @return CREATOR - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取创建时间

     *
     * @return CREATE_DATE - 创建时间

     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间

     *
     * @param createDate 创建时间

     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新人
     *
     * @return UPDATOR - 更新人
     */
    public String getUpdator() {
        return updator;
    }

    /**
     * 设置更新人
     *
     * @param updator 更新人
     */
    public void setUpdator(String updator) {
        this.updator = updator;
    }

    /**
     * 获取更新时间

     *
     * @return UPDATE_DATE - 更新时间

     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间

     *
     * @param updateDate 更新时间

     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}