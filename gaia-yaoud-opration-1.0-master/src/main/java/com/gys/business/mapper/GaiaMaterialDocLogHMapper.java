package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaMaterialDocLogH;
import com.gys.common.base.BaseMapper;

public interface GaiaMaterialDocLogHMapper extends BaseMapper<GaiaMaterialDocLogH> {
}