package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "GAIA_SD_INCOME_STATEMENT_H")
public class GaiaSdIncomeStatementH implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 损益单号
     */
    @Id
    @Column(name = "GSISH_VOUCHER_ID")
    private String gsishVoucherId;

    /**
     * 盘点单号
     */
    @Column(name = "GSISH_PC_VOUCHER_ID")
    private String gsishPcVoucherId;

    /**
     * 门店
     */
    @Id
    @Column(name = "GSISH_BR_ID")
    private String gsishBrId;

    /**
     * 生成日期
     */
    @Column(name = "GSISH_DATE")
    private String gsishDate;

    /**
     * 损益类型 1盘点转入 2批号养护 3门店领用 4门店报损
     */
    @Column(name = "GSISH_IS_TYPE")
    private String gsishIsType;

    /**
     * 单据类型
     */
    @Column(name = "GSISH_TABLE_TYPE")
    private String gsishTableType;

    /**
     * 合计数量
     */
    @Column(name = "GSISH_TOTAL_QTY")
    private String gsishTotalQty;

    /**
     * 合计金额
     */
    @Column(name = "GSISH_TOTAL_AMT")
    private BigDecimal gsishTotalAmt;

    /**
     * 备注
     */
    @Column(name = "GSISH_REMARK")
    private String gsishRemark;

    /**
     * 审核日期
     */
    @Column(name = "GSISH_EXAMINE_DATE")
    private String gsishExamineDate;

    /**
     * 审核人员
     */
    @Column(name = "GSISH_EXAMINE_EMP")
    private String gsishExamineEmp;

    /**
     * 审核状态 0-未审核 1-已审核（通过） 2-已退回（不通过）
     */
    @Column(name = "GSISH_STATUS")
    private String gsishStatus;

    /**
      * 添加附件
     */
    @Column(name = "GSISH_FJ")
    private String gsishFj;

    /**
      领用部门
     */
    @Column(name = "GSISH_BRANCH")
    private String gsishBranch;

    private static final long serialVersionUID = 1L;

    public String getGsishFj() {
        return gsishFj;
    }

    public void setGsishFj(String gsishFj) {
        this.gsishFj = gsishFj;
    }

    public String getGsishBranch() {
        return gsishBranch;
    }

    public void setGsishBranch(String gsishBranch) {
        this.gsishBranch = gsishBranch;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT_ID - 加盟商
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * 设置加盟商
     *
     * @param clientId 加盟商
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * 获取损益单号
     *
     * @return GSISH_VOUCHER_ID - 损益单号
     */
    public String getGsishVoucherId() {
        return gsishVoucherId;
    }

    public String getGsishPcVoucherId() {
        return gsishPcVoucherId;
    }

    public void setGsishPcVoucherId(String gsishPcVoucherId) {
        this.gsishPcVoucherId = gsishPcVoucherId;
    }

    /**
     * 设置损益单号
     *
     * @param gsishVoucherId 损益单号
     */
    public void setGsishVoucherId(String gsishVoucherId) {
        this.gsishVoucherId = gsishVoucherId;
    }

    /**
     * 获取门店
     *
     * @return GSISH_BR_ID - 门店
     */
    public String getGsishBrId() {
        return gsishBrId;
    }

    /**
     * 设置门店
     *
     * @param gsishBrId 门店
     */
    public void setGsishBrId(String gsishBrId) {
        this.gsishBrId = gsishBrId;
    }

    /**
     * 获取生成日期
     *
     * @return GSISH_DATE - 生成日期
     */
    public String getGsishDate() {
        return gsishDate;
    }

    /**
     * 设置生成日期
     *
     * @param gsishDate 生成日期
     */
    public void setGsishDate(String gsishDate) {
        this.gsishDate = gsishDate;
    }

    /**
     * 获取损益类型 1-盘点差异、2-批号调整、3-低值易耗品领用、4-门店耗用
     *
     * @return GSISH_IS_TYPE - 损益类型 1-盘点差异、2-批号调整、3-低值易耗品领用、4-门店耗用
     */
    public String getGsishIsType() {
        return gsishIsType;
    }

    /**
     * 设置损益类型 1-盘点差异、2-批号调整、3-低值易耗品领用、4-门店耗用
     *
     * @param gsishIsType 损益类型 1-盘点差异、2-批号调整、3-低值易耗品领用、4-门店耗用
     */
    public void setGsishIsType(String gsishIsType) {
        this.gsishIsType = gsishIsType;
    }

    /**
     * 获取单据类型
     *
     * @return GSISH_TABLE_TYPE - 单据类型
     */
    public String getGsishTableType() {
        return gsishTableType;
    }

    /**
     * 设置单据类型
     *
     * @param gsishTableType 单据类型
     */
    public void setGsishTableType(String gsishTableType) {
        this.gsishTableType = gsishTableType;
    }

    /**
     * 获取合计数量
     *
     * @return GSISH_TOTAL_QTY - 合计数量
     */
    public String getGsishTotalQty() {
        return gsishTotalQty;
    }

    /**
     * 设置合计数量
     *
     * @param gsishTotalQty 合计数量
     */
    public void setGsishTotalQty(String gsishTotalQty) {
        this.gsishTotalQty = gsishTotalQty;
    }

    /**
     * 获取合计金额
     *
     * @return GSISH_TOTAL_AMT - 合计金额
     */
    public BigDecimal getGsishTotalAmt() {
        return gsishTotalAmt;
    }

    /**
     * 设置合计金额
     *
     * @param gsishTotalAmt 合计金额
     */
    public void setGsishTotalAmt(BigDecimal gsishTotalAmt) {
        this.gsishTotalAmt = gsishTotalAmt;
    }

    /**
     * 获取备注
     *
     * @return GSISH_REMARK - 备注
     */
    public String getGsishRemark() {
        return gsishRemark;
    }

    /**
     * 设置备注
     *
     * @param gsishRemark 备注
     */
    public void setGsishRemark(String gsishRemark) {
        this.gsishRemark = gsishRemark;
    }

    /**
     * 获取审核日期
     *
     * @return GSISH_EXAMINE_DATE - 审核日期
     */
    public String getGsishExamineDate() {
        return gsishExamineDate;
    }

    /**
     * 设置审核日期
     *
     * @param gsishExamineDate 审核日期
     */
    public void setGsishExamineDate(String gsishExamineDate) {
        this.gsishExamineDate = gsishExamineDate;
    }

    /**
     * 获取审核人员
     *
     * @return GSISH_EXAMINE_EMP - 审核人员
     */
    public String getGsishExamineEmp() {
        return gsishExamineEmp;
    }

    /**
     * 设置审核人员
     *
     * @param gsishExamineEmp 审核人员
     */
    public void setGsishExamineEmp(String gsishExamineEmp) {
        this.gsishExamineEmp = gsishExamineEmp;
    }

    /**
     * 获取审核状态 0-未审核 1-已审核（通过） 2-已退回（不通过）
     *
     * @return GSISH_STATUS - 审核状态 0-未审核 1-已审核（通过） 2-已退回（不通过）
     */
    public String getGsishStatus() {
        return gsishStatus;
    }

    /**
     * 设置审核状态 0-未审核 1-已审核（通过） 2-已退回（不通过）
     *
     * @param gsishStatus 审核状态 0-未审核 1-已审核（通过） 2-已退回（不通过）
     */
    public void setGsishStatus(String gsishStatus) {
        this.gsishStatus = gsishStatus;
    }
}