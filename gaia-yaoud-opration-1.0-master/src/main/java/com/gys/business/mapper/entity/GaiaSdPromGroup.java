package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * GAIA_SD_PROM_GROUP
 * @author 
 */
@Data
public class GaiaSdPromGroup  implements Serializable {




    /**
     * 加盟商ID
     */
    private String client;

    /**
     * 分组ID
     */
    private String groupId;

    /**
     * 分组名
     */
    private String groupName;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建日期
     */
    private String createDate;

    /**
     * 创建时间
     */
    private String createTiem;

    /**
     * 标记
     */
    private String groupFlag;

    private static final long serialVersionUID = 1L;
}