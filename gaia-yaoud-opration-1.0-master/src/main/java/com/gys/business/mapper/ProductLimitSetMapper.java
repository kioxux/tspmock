package com.gys.business.mapper;

import com.gys.business.mapper.entity.ProductLimitSet;
import com.gys.business.service.data.ProductLimitCheckRes;
import com.gys.business.service.data.ProductLimitRes;
import com.gys.business.service.data.ProductLimitSetSearchVo;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ProductLimitSetMapper extends BaseMapper<ProductLimitSet> {
    List<ProductLimitRes> selectByCondition(ProductLimitSetSearchVo inData,String client);

    List<ProductLimitCheckRes> selectCountNumber(String client, List<String> proIds);

    List<ProductLimitSet> getAllByPros(@Param("client") String client,@Param("brId") String depId,@Param("proIds") List<String> proIds);

    List<ProductLimitSet> getAllByClient(@Param("client")String client);
}
