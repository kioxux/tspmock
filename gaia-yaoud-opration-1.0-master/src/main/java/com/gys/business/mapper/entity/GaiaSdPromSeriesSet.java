package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 系列类促销系列编码设置表
 */
@Table(name = "GAIA_SD_PROM_SERIES_SET")
@Data
public class GaiaSdPromSeriesSet implements Serializable {
    private static final long serialVersionUID = -1659874970995053606L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPSS_VOUCHER_ID")
    private String gspssVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPSS_SERIAL")
    private String gspssSerial;

    /**
     * 系列编码
     */
    @Id
    @Column(name = "GSPSS_SERIES_ID")
    private String gspssSeriesId;

    /**
     * 达到数量1
     */
    @Column(name = "GSPSS_REACH_QTY1")
    private String gspssReachQty1;

    /**
     * 达到金额1
     */
    @Column(name = "GSPSS_REACH_AMT1")
    private BigDecimal gspssReachAmt1;

    /**
     * 结果减额1
     */
    @Column(name = "GSPSS_RESULT_AMT1")
    private BigDecimal gspssResultAmt1;

    /**
     * 结果折扣1
     */
    @Column(name = "GSPSS_RESULT_REBATE1")
    private String gspssResultRebate1;

    /**
     * 达到数量2
     */
    @Column(name = "GSPSS_REACH_QTY2")
    private String gspssReachQty2;

    /**
     * 达到金额2
     */
    @Column(name = "GSPSS_REACH_AMT2")
    private BigDecimal gspssReachAmt2;

    /**
     * 结果减额2
     */
    @Column(name = "GSPSS_RESULT_AMT2")
    private BigDecimal gspssResultAmt2;

    /**
     * 结果折扣2
     */
    @Column(name = "GSPSS_RESULT_REBATE2")
    private String gspssResultRebate2;

    /**
     * 达到数量3
     */
    @Column(name = "GSPSS_REACH_QTY3")
    private String gspssReachQty3;

    /**
     * 达到金额3
     */
    @Column(name = "GSPSS_REACH_AMT3")
    private BigDecimal gspssReachAmt3;

    /**
     * 结果减额3
     */
    @Column(name = "GSPSS_RESULT_AMT3")
    private BigDecimal gspssResultAmt3;

    /**
     * 结果折扣3
     */
    @Column(name = "GSPSS_RESULT_REBATE3")
    private String gspssResultRebate3;
}
