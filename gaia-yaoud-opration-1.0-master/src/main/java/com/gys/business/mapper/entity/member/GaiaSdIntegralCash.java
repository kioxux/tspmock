package com.gys.business.mapper.entity.member;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * 积分抵现活动
 *
 * @author wu mao yin
 * @date 2021/12/9 15:15
 */
@Data
@Table(name = "GAIA_SD_INTEGRAL_CASH")
public class GaiaSdIntegralCash implements Serializable {

    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 积分抵现活动单号
     */
    @Column(name = "GSIC_VOUCHER_ID")
    private String gsicVoucherId;

    /**
     * 积分抵现活动描述
     */
    @Column(name = "GSIC_NAME")
    private String gsicName;

    /**
     * 积分抵现规则代号
     */
    @Column(name = "GSIC_PLAN_ID")
    private String gsicPlanId;

    /**
     * 开始日期
     */
    @Column(name = "GSIC_START_DATE")
    private String gsicStartDate;

    /**
     * 结束日期
     */
    @Column(name = "GSIC_END_DATE")
    private String gsicEndDate;

    /**
     * 频率类型 1:日期，2:星期
     */
    @Column(name = "GSIC_FREQUENCY_TYPE")
    private Integer gsicFrequencyType;

    /**
     * 日期频率
     */
    @Column(name = "GSIC_DATE_FREQUENCY")
    private String gsicDateFrequency;

    /**
     * 星期频率
     */
    @Column(name = "GSIC_WEEK_FREQUENCY")
    private String gsicWeekFrequency;

    /**
     * 开始时间
     */
    @Column(name = "GSIC_START_TIME")
    private String gsicStartTime;

    /**
     * 结束时间
     */
    @Column(name = "GSIC_END_TIME")
    private String gsicEndTime;

    /**
     * 抵用商品范围 0-所有，1-部分
     */
    @Column(name = "GSIC_PRO_RANGE")
    private Integer gsicProRange;

    /**
     * 单据状态 0-保存，1-审核
     */
    @Column(name = "GSIC_STATUS")
    private Integer gsicStatus;

    /**
     * 审核人
     */
    @Column(name = "GSIC_REVIEWER")
    private String gsicReviewer;

    /**
     * 审核日期
     */
    @Column(name = "GSIC_REVIEW_DATE")
    private Date gsicReviewDate;

    /**
     * 创建人
     */
    @Column(name = "GSIC_CREATE_USER")
    private String gsicCreateUser;

    /**
     * 创建日期
     */
    @Column(name = "GSIC_CREATE_DATE")
    private Date gsicCreateDate;

    /**
     * 更新人
     */
    @Column(name = "GSIC_UPDATE_USER")
    private String gsicUpdateUser;

    /**
     * 更新日期
     */
    @Column(name = "GSIC_UPDATE_DATE")
    private Date gsicUpdateDate;

    private static final long serialVersionUID = 1L;

}