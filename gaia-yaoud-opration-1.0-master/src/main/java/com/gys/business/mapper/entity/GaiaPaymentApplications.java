package com.gys.business.mapper.entity;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-26
 */
@Data
@Accessors(chain = true)
@Table( name = "GAIA_PAYMENT_APPLICATIONS")
public class GaiaPaymentApplications implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @Id
    @Column(name = "ID")
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "供应商")
    @Column(name = "SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "地点")
    @Column(name = "PRO_SITE")
    private String proSite;

    @ApiModelProperty(value = "申请日期")
    @Column(name = "PAYMENT_ORDER_DATE")
    private String paymentOrderDate;

    @ApiModelProperty(value = "类别")
    @Column(name = "TYPE")
    private Integer type;

    @ApiModelProperty(value = "本次付款金额")
    @Column(name = "INVOICE_AMOUNT_OF_THIS_PAYMENT")
    private BigDecimal invoiceAmountOfThisPayment;

    @ApiModelProperty(value = "申请状态")
    @Column(name = "APPLICATION_STATUS")
    private Integer applicationStatus;

    @ApiModelProperty(value = "审批状态")
    @Column(name = "APPROVAL_STATUS")
    private Integer approvalStatus;

    @ApiModelProperty(value = "工作流编码")
    @Column(name = "APPROVAL_FLOW_NO")
    private String approvalFlowNo;

    @Column(name = "PAYMENT_ORDER_NO")
    private String paymentOrderNo;

    @ApiModelProperty(value = "备注")
    @Column(name = "GPA_REMARKS")
    private String gpaRemarks;

    @ApiModelProperty(value = "审批完成日期")
    @Column(name = "GPA_APPROVAL_DATE")
    private String gpaApprovalDate;


    /**
     * 核销金额
     */
    //private BigDecimal writeOffAmount;

    public static final String ID = "ID";

    public static final String CLIENT = "CLIENT";

    public static final String SUP_CODE = "SUP_CODE";

    public static final String PRO_SITE = "PRO_SITE";

    public static final String PAYMENT_ORDER_DATE = "paymentOrderDate";

    public static final String TYPE = "TYPE";

    public static final String INVOICE_AMOUNT_OF_THIS_PAYMENT = "INVOICE_AMOUNT_OF_THIS_PAYMENT";

    public static final String APPLICATION_STATUS = "APPLICATION_STATUS";

    public static final String APPROVAL_STATUS = "APPROVAL_STATUS";

    public static final String APPROVAL_FLOW_NO = "APPROVAL_FLOW_NO";

    public static final String PAYMENT_ORDER_NO = "PAYMENT_ORDER_NO";

    public static final String GPA_REMARKS = "GPA_REMARKS";

}
