package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdWTSTO;
import com.gys.common.base.BaseMapper;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/15 21:44
 */
public interface GaiaSdWTSTOMapper extends BaseMapper<GaiaSdWTSTO> {
}
