//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_REMOTE_AUDIT_H"
)
public class GaiaSdRemoteAuditH implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSRAH_VOUCHER_ID"
    )
    private String gsrahVoucherId;
    @Column(
            name = "GSRAH_BR_ID"
    )
    private String gsrahBrId;
    @Column(
            name = "GSRAH_BILL_NO"
    )
    private String gsrahBillNo;
    @Column(
            name = "GSRAH_BR_NAME"
    )
    private String gsrahBrName;
    @Column(
            name = "GSRAH_UPLOAD_DATE"
    )
    private String gsrahUploadDate;
    @Column(
            name = "GSRAH_UPLOAD_TIME"
    )
    private String gsrahUploadTime;
    @Column(
            name = "GSRAH_UPLOAD_EMP"
    )
    private String gsrahUploadEmp;
    @Column(
            name = "GSRAH_UPLOAD_FLAG"
    )
    private String gsrahUploadFlag;
    @Column(
            name = "GSRAH_DATE"
    )
    private String gsrahDate;
    @Column(
            name = "GSRAH_TIME"
    )
    private String gsrahTime;
    @Column(
            name = "GSRAH_RESULT"
    )
    private String gsrahResult;
    @Column(
            name = "GSRAH_PHARMACIST_ID"
    )
    private String gsrahPharmacistId;
    @Column(
            name = "GSRAH_PHARMACIST_NAME"
    )
    private String gsrahPharmacistName;
    @Column(
            name = "GSRAH_CUST_NAME"
    )
    private String gsrahCustName;
    @Column(
            name = "GSRAH_CUST_SEX"
    )
    private String gsrahCustSex;
    @Column(
            name = "GSRAH_CUST_AGE"
    )
    private String gsrahCustAge;
    @Column(
            name = "GSRAH_CUST_IDCARD"
    )
    private String gsrahCustIdcard;
    @Column(
            name = "GSRAH_CUST_MOBILE"
    )
    private String gsrahCustMobile;
    @Column(
            name = "GSRAH_STATUS"
    )
    private String gsrahStatus;
    @Column(
            name = "GSRAH_RECIPEL_PIC_ADDRESS"
    )
    private String gsrahRecipelPicAddress;
    private static final long serialVersionUID = 1L;

    public GaiaSdRemoteAuditH() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsrahVoucherId() {
        return this.gsrahVoucherId;
    }

    public void setGsrahVoucherId(String gsrahVoucherId) {
        this.gsrahVoucherId = gsrahVoucherId;
    }

    public String getGsrahBrId() {
        return this.gsrahBrId;
    }

    public void setGsrahBrId(String gsrahBrId) {
        this.gsrahBrId = gsrahBrId;
    }

    public String getGsrahBrName() {
        return this.gsrahBrName;
    }

    public void setGsrahBrName(String gsrahBrName) {
        this.gsrahBrName = gsrahBrName;
    }

    public String getGsrahUploadDate() {
        return this.gsrahUploadDate;
    }

    public void setGsrahUploadDate(String gsrahUploadDate) {
        this.gsrahUploadDate = gsrahUploadDate;
    }

    public String getGsrahUploadTime() {
        return this.gsrahUploadTime;
    }

    public void setGsrahUploadTime(String gsrahUploadTime) {
        this.gsrahUploadTime = gsrahUploadTime;
    }

    public String getGsrahUploadEmp() {
        return this.gsrahUploadEmp;
    }

    public void setGsrahUploadEmp(String gsrahUploadEmp) {
        this.gsrahUploadEmp = gsrahUploadEmp;
    }

    public String getGsrahUploadFlag() {
        return this.gsrahUploadFlag;
    }

    public void setGsrahUploadFlag(String gsrahUploadFlag) {
        this.gsrahUploadFlag = gsrahUploadFlag;
    }

    public String getGsrahDate() {
        return this.gsrahDate;
    }

    public void setGsrahDate(String gsrahDate) {
        this.gsrahDate = gsrahDate;
    }

    public String getGsrahTime() {
        return this.gsrahTime;
    }

    public void setGsrahTime(String gsrahTime) {
        this.gsrahTime = gsrahTime;
    }

    public String getGsrahResult() {
        return this.gsrahResult;
    }

    public void setGsrahResult(String gsrahResult) {
        this.gsrahResult = gsrahResult;
    }

    public String getGsrahPharmacistId() {
        return this.gsrahPharmacistId;
    }

    public void setGsrahPharmacistId(String gsrahPharmacistId) {
        this.gsrahPharmacistId = gsrahPharmacistId;
    }

    public String getGsrahPharmacistName() {
        return this.gsrahPharmacistName;
    }

    public void setGsrahPharmacistName(String gsrahPharmacistName) {
        this.gsrahPharmacistName = gsrahPharmacistName;
    }

    public String getGsrahCustName() {
        return this.gsrahCustName;
    }

    public void setGsrahCustName(String gsrahCustName) {
        this.gsrahCustName = gsrahCustName;
    }

    public String getGsrahCustSex() {
        return this.gsrahCustSex;
    }

    public void setGsrahCustSex(String gsrahCustSex) {
        this.gsrahCustSex = gsrahCustSex;
    }

    public String getGsrahCustAge() {
        return this.gsrahCustAge;
    }

    public void setGsrahCustAge(String gsrahCustAge) {
        this.gsrahCustAge = gsrahCustAge;
    }

    public String getGsrahCustIdcard() {
        return this.gsrahCustIdcard;
    }

    public void setGsrahCustIdcard(String gsrahCustIdcard) {
        this.gsrahCustIdcard = gsrahCustIdcard;
    }

    public String getGsrahCustMobile() {
        return this.gsrahCustMobile;
    }

    public void setGsrahCustMobile(String gsrahCustMobile) {
        this.gsrahCustMobile = gsrahCustMobile;
    }

    public String getGsrahStatus() {
        return this.gsrahStatus;
    }

    public void setGsrahStatus(String gsrahStatus) {
        this.gsrahStatus = gsrahStatus;
    }

    public String getGsrahRecipelPicAddress() {
        return this.gsrahRecipelPicAddress;
    }

    public void setGsrahRecipelPicAddress(String gsrahRecipelPicAddress) {
        this.gsrahRecipelPicAddress = gsrahRecipelPicAddress;
    }

    public String getGsrahBillNo() {
        return this.gsrahBillNo;
    }

    public void setGsrahBillNo(String gsrahBillNo) {
        this.gsrahBillNo = gsrahBillNo;
    }
}
