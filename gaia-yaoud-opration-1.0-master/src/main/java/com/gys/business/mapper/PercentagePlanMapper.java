package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaTichengPlan;
import com.gys.business.service.data.ReportForms.MonthPushMoneyByRule;
import com.gys.business.service.data.ReportForms.MonthPushMoneyBySalespersonInData;
import com.gys.business.service.data.percentageplan.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PercentagePlanMapper extends BaseMapper<GaiaTichengPlan> {

    List<Map<String, String>> selectStoreByClient(@Param("clientId") String clientId);

    void addPlanStores(List<PercentageStoInData> list);

    void deleteStoreByPid(@Param("pid") Integer pid);

    void batchSaleTichengList(List<PercentageSaleInData> list);

    void batchProTichengList(List<PercentageProInData> list);

    void updateTichengZ(GaiaTichengPlan inData);

    List<PercentageOutData> selectTichengZList(PercentageInData inData);

    PercentageInData selectTichengZDetail(@Param("tichengId") Long planId);

    List<Map<String,String>> selectTichengStoList(@Param("tichengId") Long planId);

    List<PercentageSaleInData> selectTichengSaleList(@Param("tichengId") Long planId);

    List<PercentageProInData> selectTichengProList(@Param("tichengId") Long planId);

    void deleteSalePlan(@Param("id") Long saleId);

    void deleteSalePlanByPid(@Param("pid") Integer tichengId);

    void deleteProPlan(@Param("id") Long proId);

    String selectNextPlanCode(String clientId);

    void approvePlan(@Param("id") Long planId,@Param("planStatus") String planStatus);

    void deletePlan(@Param("id") Long planId);

    Long selectIdByCode(@Param("client") String clientId,@Param("planCode") String planCode);

    Long addPlanMain(GaiaTichengPlan inData);

    PercentageProInData selectProductByClient(@Param("client") String clientId,@Param("proCode") String proCode);

    String selectMovPriceByDcCode(@Param("client") String clientId,@Param("dcCode") String dcCode,@Param("proCode") String proCode);

    List<PercentageProInData> selectProductByProCodes(ImportProInData inData);

    List<Map<String,String>>selectMovPriceListByDcCode(ImportProInData inData);

    int checkStartDate(PercentageInData inData);

    int checkEndDate(PercentageInData inData);
}

