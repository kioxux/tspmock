package com.gys.business.mapper;

import com.gys.business.mapper.entity.StockCompare;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 月度库存表
 Mapper 接口
 * </p>
 *
 * @author sy
 * @since 2021-07-13
 */
@Mapper
public interface StockCompareMapper extends BaseMapper<StockCompare> {
    List<StockCompareKuCen> selectKuCenList(String client, List<String> sites);

    List<StockCompareRes> selectByCondition(StockCompareCondition inData);

//    List<StockMouthRes> selectByCondition(StockMouthCaculateCondition inData);
}
