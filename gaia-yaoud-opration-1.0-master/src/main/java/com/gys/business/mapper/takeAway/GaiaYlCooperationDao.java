package com.gys.business.mapper.takeAway;

import com.gys.common.data.yaolian.GaiaYlCooperation;

public interface GaiaYlCooperationDao {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaYlCooperation record);

    int insertSelective(GaiaYlCooperation record);

    GaiaYlCooperation selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaYlCooperation record);

    int updateByPrimaryKey(GaiaYlCooperation record);

    String getCooperationByClient(String client);
}