package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_GROSS_MARGIN_INTERVAL")
public class GaiaGrossMarginInterval implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 毛利等级：ABCDE
     */
    @Column(name = "INTERVAL_TYPE")
    private String intervalType;

    /**
     * 起始毛利
     */
    @Column(name = "START_VALUE")
    private BigDecimal startValue;

    /**
     * 截止毛利
     */
    @Column(name = "END_VALUE")
    private BigDecimal endValue;

    /**
     * 是否删除：0-正常 1-删除
     */
    @Column(name = "DELETE_FLAG")
    private Integer deleteFlag;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    private Date createTime;

    /**
     * 创建者
     */
    @Column(name = "CREATE_USER")
    private String createUser;

    /**
     * 更新时间
     */
    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    /**
     * 更新者
     */
    @Column(name = "UPDATE_USER")
    private String updateUser;

    /**
     * 版本
     */
    @Column(name = "VERSION")
    private Integer version;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return ID - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取毛利等级：ABCDE
     *
     * @return INTERVAL_TYPE - 毛利等级：ABCDE
     */
    public String getIntervalType() {
        return intervalType;
    }

    /**
     * 设置毛利等级：ABCDE
     *
     * @param intervalType 毛利等级：ABCDE
     */
    public void setIntervalType(String intervalType) {
        this.intervalType = intervalType;
    }

    /**
     * 获取起始毛利
     *
     * @return START_VALUE - 起始毛利
     */
    public BigDecimal getStartValue() {
        return startValue;
    }

    /**
     * 设置起始毛利
     *
     * @param startValue 起始毛利
     */
    public void setStartValue(BigDecimal startValue) {
        this.startValue = startValue;
    }

    /**
     * 获取截止毛利
     *
     * @return END_VALUE - 截止毛利
     */
    public BigDecimal getEndValue() {
        return endValue;
    }

    /**
     * 设置截止毛利
     *
     * @param endValue 截止毛利
     */
    public void setEndValue(BigDecimal endValue) {
        this.endValue = endValue;
    }

    /**
     * 获取是否删除：0-正常 1-删除
     *
     * @return DELETE_FLAG - 是否删除：0-正常 1-删除
     */
    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * 设置是否删除：0-正常 1-删除
     *
     * @param deleteFlag 是否删除：0-正常 1-删除
     */
    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_TIME - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建者
     *
     * @return CREATE_USER - 创建者
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * 设置创建者
     *
     * @param createUser 创建者
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * 获取更新时间
     *
     * @return UPDATE_TIME - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新者
     *
     * @return UPDATE_USER - 更新者
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置更新者
     *
     * @param updateUser 更新者
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * 获取版本
     *
     * @return VERSION - 版本
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 设置版本
     *
     * @param version 版本
     */
    public void setVersion(Integer version) {
        this.version = version;
    }
}