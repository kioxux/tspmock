package com.gys.business.mapper;

import com.gys.business.mapper.entity.OasSicknessCodingElement;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 疾病中类-成分细类关系表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-09-02
 */
@Mapper
public interface OasSicknessCodingElementMapper extends BaseMapper<OasSicknessCodingElement> {
    List<Map<String, Object>> selectMingx();

}
