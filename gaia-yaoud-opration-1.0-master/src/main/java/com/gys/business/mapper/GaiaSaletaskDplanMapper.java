package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSaletaskDplan;
import com.gys.business.service.data.GetAllotOutData;
import com.gys.business.service.data.SaleTaskInData;
import com.gys.business.service.data.Saletask;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSaletaskDplanMapper extends BaseMapper<GaiaSaletaskDplan> {
    List<Saletask> selectSaletaskById(SaleTaskInData saletask);

    void insertBatch(List<GaiaSaletaskDplan> dplansInsert);

    List<Saletask> selectSaleTaskReachPage(Saletask saletask);

    void updateBatch(List<GaiaSaletaskDplan> dplansUpdate);

    GaiaSaletaskDplan selectBySum(SaleTaskInData saletasks);
}