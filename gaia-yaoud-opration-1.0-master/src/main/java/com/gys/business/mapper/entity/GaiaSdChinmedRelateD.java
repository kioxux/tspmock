package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "GAIA_SD_CHINMED_RELATE_D")
public class GaiaSdChinmedRelateD {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "BR_ID"
    )
    private String brId;
    @Id
    @Column(
            name = "PID"
    )
    private String pid;
    @Id
    @Column(
            name = "SERIAL"
    )
    private String serial;
    @Column(
            name = "CLTITMID"
    )
    private String cltItmId;
    @Column(
            name = "ITMID"
    )
    private String itmId;
    @Column(
            name = "ITMNAME"
    )
    private String itmName;
    @Column(
            name = "ITMSPEC"
    )
    private String itmSpec;
    @Column(
            name = "PLACE"
    )
    private String place;
    @Column(
            name = "UNIT"
    )
    private String unit;
    @Column(
            name = "QUANTITY"
    )
    private String quantity;
    @Column(
            name = "PRICE"
    )
    private String price;
    @Column(
            name = "FREETXT"
    )
    private String freeTxt;
}
