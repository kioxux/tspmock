package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_SALETASK_HPLAN")
public class GaiaSaletaskHplan implements Serializable {
    /**
     * 客户ID
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 计划月份
     */
    @Id
    @Column(name = "MONTH_PLAN")
    private String monthPlan;

    /**
     * 计划天数
     */
    @Column(name = "DAYS_PLAN")
    private Integer daysPlan;

    /**
     * 门店数
     */
    @Column(name = "T_COUNT")
    private Integer tCount;

    /**
     * 总计划营业额
     */
    @Column(name = "TH_SALE_AMT")
    private BigDecimal thSaleAmt;

    /**
     * 总计划营业额增长比率
     */
    @Column(name = "GPH_SALE_AMT")
    private BigDecimal gphSaleAmt;

    /**
     * 总计划毛利额
     */
    @Column(name = "TH_SALE_GROSS")
    private BigDecimal thSaleGross;

    /**
     * 总计划毛利额增长比率
     */
    @Column(name = "GPH_SALE_GROSS")
    private BigDecimal gphSaleGross;

    /**
     * 总计划会员卡
     */
    @Column(name = "TH_MCARD_QTY")
    private BigDecimal thMcardQty;

    /**
     * 总计划会员卡增长比率
     */
    @Column(name = "GPH_MCARD_QTY")
    private BigDecimal gphMcardQty;

    /**
     * 创建人账号
     */
    @Column(name = "CRE_ID")
    private String creId;

    /**
     * 创建年月日时分秒
     */
    @Column(name = "CRE_DATE")
    private String creDate;

    /**
     * 任务状态（S:保存,P:审核通过）
     */
    @Column(name = "SPLAN_STA")
    private String splanSta;

    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取客户ID
     *
     * @return CLIENT - 客户ID
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置客户ID
     *
     * @param client 客户ID
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取计划月份
     *
     * @return MONTH_PLAN - 计划月份
     */
    public String getMonthPlan() {
        return monthPlan;
    }

    /**
     * 设置计划月份
     *
     * @param monthPlan 计划月份
     */
    public void setMonthPlan(String monthPlan) {
        this.monthPlan = monthPlan;
    }

    /**
     * 获取计划天数
     *
     * @return DAYS_PLAN - 计划天数
     */
    public Integer getDaysPlan() {
        return daysPlan;
    }

    /**
     * 设置计划天数
     *
     * @param daysPlan 计划天数
     */
    public void setDaysPlan(Integer daysPlan) {
        this.daysPlan = daysPlan;
    }

    /**
     * 获取门店数
     *
     * @return T_COUNT - 门店数
     */
    public Integer gettCount() {
        return tCount;
    }

    /**
     * 设置门店数
     *
     * @param tCount 门店数
     */
    public void settCount(Integer tCount) {
        this.tCount = tCount;
    }

    /**
     * 获取总计划营业额
     *
     * @return TH_SALE_AMT - 总计划营业额
     */
    public BigDecimal getThSaleAmt() {
        return thSaleAmt;
    }

    /**
     * 设置总计划营业额
     *
     * @param thSaleAmt 总计划营业额
     */
    public void setThSaleAmt(BigDecimal thSaleAmt) {
        this.thSaleAmt = thSaleAmt;
    }

    /**
     * 获取总计划营业额增长比率
     *
     * @return GPH_SALE_AMT - 总计划营业额增长比率
     */
    public BigDecimal getGphSaleAmt() {
        return gphSaleAmt;
    }

    /**
     * 设置总计划营业额增长比率
     *
     * @param gphSaleAmt 总计划营业额增长比率
     */
    public void setGphSaleAmt(BigDecimal gphSaleAmt) {
        this.gphSaleAmt = gphSaleAmt;
    }

    /**
     * 获取总计划毛利额
     *
     * @return TH_SALE_GROSS - 总计划毛利额
     */
    public BigDecimal getThSaleGross() {
        return thSaleGross;
    }

    /**
     * 设置总计划毛利额
     *
     * @param thSaleGross 总计划毛利额
     */
    public void setThSaleGross(BigDecimal thSaleGross) {
        this.thSaleGross = thSaleGross;
    }

    /**
     * 获取总计划毛利额增长比率
     *
     * @return GPH_SALE_GROSS - 总计划毛利额增长比率
     */
    public BigDecimal getGphSaleGross() {
        return gphSaleGross;
    }

    /**
     * 设置总计划毛利额增长比率
     *
     * @param gphSaleGross 总计划毛利额增长比率
     */
    public void setGphSaleGross(BigDecimal gphSaleGross) {
        this.gphSaleGross = gphSaleGross;
    }

    /**
     * 获取总计划会员卡
     *
     * @return TH_MCARD_QTY - 总计划会员卡
     */
    public BigDecimal getThMcardQty() {
        return thMcardQty;
    }

    /**
     * 设置总计划会员卡
     *
     * @param thMcardQty 总计划会员卡
     */
    public void setThMcardQty(BigDecimal thMcardQty) {
        this.thMcardQty = thMcardQty;
    }

    /**
     * 获取总计划会员卡增长比率
     *
     * @return GPH_MCARD_QTY - 总计划会员卡增长比率
     */
    public BigDecimal getGphMcardQty() {
        return gphMcardQty;
    }

    /**
     * 设置总计划会员卡增长比率
     *
     * @param gphMcardQty 总计划会员卡增长比率
     */
    public void setGphMcardQty(BigDecimal gphMcardQty) {
        this.gphMcardQty = gphMcardQty;
    }

    /**
     * 获取创建人账号
     *
     * @return CRE_ID - 创建人账号
     */
    public String getCreId() {
        return creId;
    }

    /**
     * 设置创建人账号
     *
     * @param creId 创建人账号
     */
    public void setCreId(String creId) {
        this.creId = creId;
    }

    /**
     * 获取创建年月日时分秒
     *
     * @return CRE_DATE - 创建年月日时分秒
     */
    public String getCreDate() {
        return creDate;
    }

    /**
     * 设置创建年月日时分秒
     *
     * @param creDate 创建年月日时分秒
     */
    public void setCreDate(String creDate) {
        this.creDate = creDate;
    }

    /**
     * 获取任务状态（S:保存,P:审核通过）
     *
     * @return SPLAN_STA - 任务状态（S:保存,P:审核通过）
     */
    public String getSplanSta() {
        return splanSta;
    }

    /**
     * 设置任务状态（S:保存,P:审核通过）
     *
     * @param splanSta 任务状态（S:保存,P:审核通过）
     */
    public void setSplanSta(String splanSta) {
        this.splanSta = splanSta;
    }

    /**
     * @return LAST_UPDATE_TIME
     */
    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    /**
     * @param lastUpdateTime
     */
    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
}