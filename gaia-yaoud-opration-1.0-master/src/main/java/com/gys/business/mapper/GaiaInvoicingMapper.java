package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.service.data.InvoicingInData;
import com.gys.business.service.data.InvoicingOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaInvoicingMapper extends BaseMapper<GaiaSdStockBatch> {
    List<InvoicingOutData> getInvoicingList(InvoicingInData inData);

    List<InvoicingOutData> getInvoicingList2(InvoicingInData inData);

    List<InvoicingOutData> getInvoicingDetailList(InvoicingInData inData);

    List<InvoicingOutData> getIncomeStatementList(InvoicingInData inData);
}
