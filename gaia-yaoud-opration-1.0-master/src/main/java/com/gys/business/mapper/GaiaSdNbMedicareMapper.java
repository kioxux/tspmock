package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdNbMedicare;
import com.gys.business.service.data.GetProductInfoQueryInData;
import com.gys.business.service.data.zjnb.MibProductMatchDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdNbMedicareMapper {

    List<GaiaSdNbMedicare> getAllByClient(@Param("client") String client);


    List<GaiaSdNbMedicare> getNBYBProCode(GetProductInfoQueryInData inData);

    List<MibProductMatchDto> getNBMibCodeByCode(@Param("client") String client,
                                                @Param("site") String site,
                                                @Param("status")String status,
                                                @Param("nameOrCode") String nameOrCode);
    List<MibProductMatchDto> getNBMibCodeByCode2(@Param("client") String client,
                                                @Param("site") String site,
                                                @Param("status")String status,
                                                @Param("nameOrCode") String nameOrCode);
    int insertOrUpdateMedPrd(List<MibProductMatchDto> list);
    int insertOrUpdateMedPrd2(@Param("site") String site,@Param("list") List<MibProductMatchDto> list);

    List<MibProductMatchDto> getNBMibCodeByWzbmCode(@Param("client") String client,
                                                @Param("site") String site,
                                                @Param("status")String status,
                                                @Param("nameOrCode") String nameOrCode);

    void saveNBMibStockCode(@Param("client") String client,
                           @Param("site") String site,
                           @Param("list") List<MibProductMatchDto> list);

    void update(@Param("site") String site,  @Param("list")List<MibProductMatchDto> matchList);
}
