package com.gys.business.mapper;

import com.gys.business.mapper.entity.GrossConstruction;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface GrossConstructionMapper {
    void batchInsert(@Param("insertList") List<GrossConstruction> insertList);

    List<GrossConstruction> bigClassTotalList(@Param("client") String client, @Param("time") String time);

    List<GrossConstruction> selectByBigClassCode(@Param("client") String client, @Param("time") String time, @Param("bigClassCode") String bigClassCode, @Param("type") Integer type);

    List<GrossConstruction> selectBigGroupByBigClassCodes(@Param("client") String client, @Param("time") String time, @Param("classCodes") List<String> classCodes);

    List<GrossConstruction> selectMidByBigClassCode(@Param("client") String client, @Param("time") String time, @Param("bigClassCodes") ArrayList<String> bigClassCodes);

    List<GrossConstruction> selectMidByMidClassCode(@Param("client") String client, @Param("time") String time, @Param("midClassCode") String midClassCode);

    void deleteByClientAndMonth(@Param("clients") List<String> clients, @Param("time") String time, @Param("type") String type);

    List<String> findMonthByClient(String client);

    List<GrossConstruction> selectMidGroupByBigClassCode(@Param("client") String client, @Param("time") String time, @Param("bigClassCodes") ArrayList<String> bigClassCodes);

    List<GrossConstruction> selectMidGroupByMidClassCode(@Param("client") String client, @Param("time") String time, @Param("bigClassCodes") List<String> bigClassCodes, @Param("midClassCode") String midClassCode);
}