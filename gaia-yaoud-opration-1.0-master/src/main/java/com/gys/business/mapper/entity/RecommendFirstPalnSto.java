package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 首推商品方案门店设置表
 * </p>
 *
 * @author flynn
 * @since 2021-08-04
 */
@Data
@Table( name = "GAIA_SD_RECOMMEND_FIRST_PALN_STO")
public class RecommendFirstPalnSto implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "PLAN_ID")
    private Long planId;

    @ApiModelProperty(value = "加盟店")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "门店id")
    @Column(name = "BR_ID")
    private String brId;

    @ApiModelProperty(value = "门店名称")
    @Column(name = "BR_NAME")
    private String brName;


}
