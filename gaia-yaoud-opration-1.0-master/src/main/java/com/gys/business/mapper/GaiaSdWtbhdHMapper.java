package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdWtbhdH;
import com.gys.business.mapper.entity.GaiaSdWtbhdHKey;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/13 16:03
 **/
public interface GaiaSdWtbhdHMapper {

    GaiaSdWtbhdH selectByPrimaryKey(GaiaSdWtbhdHKey gaiaSdWtbhdHKey);

    int add(GaiaSdWtbhdH gaiaSdWtbhdH);

    int update(GaiaSdWtbhdH gaiaSdWtbhdH);

    GaiaSdWtbhdH selectByPid(Long id);
}
