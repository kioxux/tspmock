package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSrSaleRecord;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import tk.mybatis.mapper.provider.SpecialProvider;

import java.util.List;

public interface GaiaSdSrSaleRecordMapper extends BaseMapper<GaiaSdSrSaleRecord> {
    void saleRecordInsert(List<GaiaSdSrSaleRecord> list);
}
