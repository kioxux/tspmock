package com.gys.business.mapper.yaomeng;

import com.gys.common.data.yaomeng.GaiaPrescribeInfo;

import java.util.List;

public interface GaiaPrescribeInfoDao {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaPrescribeInfo record);

    int insertList(List<GaiaPrescribeInfo> list);

    int insertSelective(GaiaPrescribeInfo record);

    GaiaPrescribeInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaPrescribeInfo record);

    int updateByPrimaryKey(GaiaPrescribeInfo record);

    GaiaPrescribeInfo getPrescribeBySaleOrderIdLimit(String saleOrderId);

    List<GaiaPrescribeInfo> getBySaleBillNo(String saleBillNo);

    int deleteByPreBillNo(String preBillNo);
}