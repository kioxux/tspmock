package com.gys.business.mapper;

import com.gys.common.data.GetLoginOutData;
import com.gys.common.yinHaiModel.*;

import java.util.List;

public interface YinHaiMedicalInsuranceMapper {

    List<CancelStockInput> getCancelSupplierStockInfo(GetLoginOutData inData);

    List<CancelStockDetailInput> getCancelSupplierStockDetail(GetLoginOutData inData);

    List<CancelStockInput> getCancelStorageInfo(GetLoginOutData inData);

    List<CancelStockDetailInput> getCancelStorageDetail(GetLoginOutData inData);

    List<AddOrderInput> getOrderInfo(GetLoginOutData inData);

    List<AddOrderDetailInput> getOrderDetail(GetLoginOutData inData);

    List<InventoryManagerOrderInput> getInventoryManagerOrders(GetLoginOutData inData);

    List<InventoryManagerOrderDetailInput> getInventoryManagerOrderDetail(GetLoginOutData inData);

    OrderModifyInput getOrderModifyInfo(GetLoginOutData inData);

    List<ClientInfoInput> listSendClientInfo(ClientInfoInput inData);

    List<GoodsInfoInput> listSendGoodsInfo(GoodsInfoInput inData);

    List<StockDetailInfoInput> listSendStockDetailInfo(StockDetailInfoInput inData);

    List<TransforStockDetailnput> listSendTransforStockDetailInfo(TransforStockInfoInput inData);
}
