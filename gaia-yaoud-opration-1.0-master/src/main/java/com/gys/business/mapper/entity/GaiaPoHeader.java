package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(
        name = "GAIA_PO_HEADER"
)
public class GaiaPoHeader implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "PO_ID"
    )
    private String poId;
    @Column(
            name = "PO_TYPE"
    )
    private String poType;
    @Column(
            name = "PO_SUPPLIER_ID"
    )
    private String poSupplierId;
    @Column(
            name = "PO_COMPANY_CODE"
    )
    private String poCompanyCode;
    @Column(
            name = "PO_SUBJECT_TYPE"
    )
    private String poSubjectType;
    @Column(
            name = "PO_DATE"
    )
    private String poDate;
    @Column(
            name = "PO_PAYMENT_ID"
    )
    private String poPaymentId;
    @Column(
            name = "PO_HEAD_REMARK"
    )
    private String poHeadRemark;
    @Column(
            name = "PO_DELIVERY_TYPE"
    )
    private String poDeliveryType;
    @Column(
            name = "PO_DELIVERY_TYPE_STORE"
    )
    private String poDeliveryTypeStore;
    @Column(
            name = "PO_PRE_POID"
    )
    private String poPrePoid;
    @Column(
            name = "PO_REQ_ID"
    )
    private String poReqId;
    @Column(
            name = "PO_CREATE_BY"
    )
    private String poCreateBy;
    @Column(
            name = "PO_CREATE_DATE"
    )
    private String poCreateDate;
    @Column(
            name = "PO_CREATE_TIME"
    )
    private String poCreateTime;
    @Column(
            name = "PO_APPROVE_STATUS"
    )
    private String poApproveStatus;
    @Column(
            name = "PO_UPDATE_BY"
    )
    private String poUpdateBy;
    @Column(
            name = "PO_UPDATE_DATE"
    )
    private String poUpdateDate;
    @Column(
            name = "PO_UPDATE_TIME"
    )
    private String poUpdateTime;
    @Column(
            name = "PO_REQ_TYPE"
    )
    private String poReqType;
    @Column(
            name = "PO_FLOW_NO"
    )
    private String poFlowNo;
    private static final long serialVersionUID = 1L;
}
