package com.gys.business.mapper.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GaiaSdWtbhdD extends GaiaSdWtbhdDKey {
    private Long id;
    private Long pid;
    private String gswbdProId;
    private String gswbdBatch;
    private String gswbdBatchNo;
    private String gswbdGrossMargin;
    private BigDecimal gswbdSalesDays1;
    private BigDecimal gswbdSalesDays2;
    private BigDecimal gswbdSalesDays3;
    private BigDecimal gswbdProposeQty;
    private String gswbdStockStore;
    private String gswbdStockDepot;
    private String gswbdDisplayMin;
    private String gswbdPackMidsize;
    private String gswbdLastSupid;
    private BigDecimal gswbdLastPrice;
    private BigDecimal gswbdEditPrice;
    private BigDecimal gswbdNeedQty;
    private BigDecimal gswbdVoucherAmt;
    private BigDecimal gswbdAverageCost;
    private String gswbdTaxRate;
    private BigDecimal gswbdDnQty;
    private BigDecimal gsspPriceNormal;
    private String gswbdFlag;
    private String gswbdHwh;
    private String gswbdBz;
    private String gswbdProDsfCode;
    private String gswbdProDsfName;
    private String gswbdDate;
    private String gswbdSerial;
    private Integer dsfStatus;
}