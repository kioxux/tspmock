package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
@Table(
        name = "GAIA_SD_DEFECPRO_D"
)
@Data
public class GaiaSdDefecproD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "GSDD_BR_ID"
    )
    private String gsddBrId;
    @Column(
            name = "GSDD_VOUCHER_ID"
    )
    private String gsddVoucherId;
    @Column(
            name = "GSDD_DATE"
    )
    private String gsddDate;
    @Column(
            name = "GSDD_SERIAL"
    )
    private String gsddSerial;
    @Column(
            name = "GSDD_PRO_ID"
    )
    private String gsddProId;
    @Column(
            name = "GSDD_BATCH_NO"
    )
    private String gsddBatchNo;
    @Column(
            name = "GSDD_VALID_DATE"
    )
    private String gsddValidDate;
    @Column(
            name = "GSDD_BATCH"
    )
    private String gsddBatch;
    @Column(
            name = "GSDD_QTY"
    )
    private String gsddQty;

    @Column(
            name = "GSDD_REMAKS"
    )
    private String gsddRemaks;

    private String stockQty;

    private String gsddEmp;
}
