package com.gys.business.mapper;

import com.gys.business.mapper.entity.MibSetlInfo;
import com.gys.business.service.data.CommenData.InData;
import com.gys.business.service.data.mib.*;
import com.gys.business.service.data.shanxi.SaleCountReportVO;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MibSetlInfoMapper extends BaseMapper<MibSetlInfo> {
    List<SaleCountReportVO> saleCountReport(InData inData);

    MibSXXZJSFormInfoOutData getMibSXXZJSForm(MibRePrintFormInData inData);

    InsuranceSettleTableOutData getMibJSSplitForm(MibRePrintFormInData inData);

    MibSXSZJSFormData getMibSXSZJSForm(MibRePrintFormInData inData);

    List<MibSXSZJSProListData> getMibSXSZJSProList(MibRePrintFormInData inData);
	List<MibInfoOutData> getSeltInfo(InData inData);



    List<MibInfoOutData> getSeltInfo3202(InData inData);


    void updateCzFlag(Map<String, Object> map );


    List<PaymentOut> getSeltInfoList(InData inData);

    List<MibInfoOutData> getSeltInfoSum(InData inData);
    List<MibInfoOutData> getSeltInfoSumByHlj(InData inData);

    void updateSetlInfoState(Map<String, Object> map);
}