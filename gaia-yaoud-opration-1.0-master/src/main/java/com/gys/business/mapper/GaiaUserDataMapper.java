package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaUserData;
import com.gys.business.service.data.GaiaRetailPriceInData;
import com.gys.business.service.data.GetEmpOutData;
import com.gys.business.service.data.HandoverReportOutData;
import com.gys.business.service.data.check.UserVO;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaUserDataMapper extends BaseMapper<GaiaUserData> {
    List<GetEmpOutData> queryEmp(GetLoginOutData inData);

    List<GetEmpOutData> getListByClientId(@Param("clientId") String clientId);

    int selectMaxId();

    List<String> getAuthCode(@Param("client") String client, @Param("userId") String userId);

    List<GaiaUserData> getAllByClient(@Param("client") String client);

    List<GaiaUserData> getAllByClientById(@Param("client") String client,@Param("userId") String userId);

    HandoverReportOutData handoverReport(GaiaRetailPriceInData inData);

    List<GaiaUserData> getAllByClientAndDepId(GaiaUserData gaiaUserData);

    List<GaiaUserData> getAllByClientAndDepIdNew(GaiaUserData gaiaUserData);

    List<UserVO> userList(@Param("client") String userId);

    List<GaiaUserData> selectStoreUser(GaiaUserData gaiaUserData);
}
