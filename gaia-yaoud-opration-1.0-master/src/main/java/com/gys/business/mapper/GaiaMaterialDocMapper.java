package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaMaterialDoc;
import com.gys.business.service.data.ReportForms.*;
import com.gys.business.service.data.StockMouthCaculateCondition;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.FuzzyQueryForConditionInData;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface GaiaMaterialDocMapper extends BaseMapper<GaiaMaterialDoc> {

    List<SupplierDealOutData> selectSupplierDealPageBySiteCode(SupplierDealInData inData);

    List<SupplierDealOutData> selectSupplierDealDetailPageBySiteCode(SupplierDealInData inData);

    List<WholesaleSaleOutData> selectWholesaleSalePage(WholesaleSaleInData inData);

    List<WholesaleSaleOutData> selectWholesaleSaleDetailPage(WholesaleSaleInData inData);

    List<BusinessDocumentOutData> selectBusinessDocumentPage(BusinessDocumentInData inData);

    List<BusinessDocumentOutData> selectBusinessDocumentDetailPage(BusinessDocumentInData inData);

    SupplierDealOutTotal selectSupplierDealByTotal(SupplierDealInData inData);

    SupplierDealOutTotal selectSupplierDealDetailByTotal(SupplierDealInData inData);
    List<InventoryChangeSummaryOutData> selectInventoryChangeSummary(InventoryChangeSummaryInData inData);

    List<InventoryChangeSummaryDetailOutData> selectInventoryChangeSummaryDetail(InventoryChangeSummaryInData inData);

    BigDecimal countSelectInventoryChangeSummaryDetail(InventoryChangeSummaryInData startDateInData);

    BusinessDocumentOutTotal selectBusinessDocumentTotal(BusinessDocumentInData inData);

    BusinessDocumentOutTotal selectBusinessDocumentDetailTotal(BusinessDocumentInData inData);

    InventoryChangeSummaryOutTotal selectInventoryChangeSummaryTotal(InventoryChangeSummaryInData inData);

    List<SupplierDealOutData> selectSupplierDealDetailPage(SupplierDealInData inData);

    List<GaiaMaterialDoc> selectListByCondition(StockMouthCaculateCondition inData);


    Integer getClientGcCount(String client);






    List<String> selectDcSiteCodeListByClient(String client);
    List<String> selectStoSiteCodeListByClient(String client);

    List<InventoryChangeCheckOutData> selectInventoryStockCheckAmtStoStartList(InventoryChangeCheckInData data);
    List<InventoryChangeCheckOutData> selectInventoryStockCheckStoBetweenAmt(InventoryChangeCheckInData data);

    InventoryChangeCheckOutData selectInventoryStockCheckAmtDcStartList(InventoryChangeCheckInData data);
    InventoryChangeCheckOutData selectInventoryStockCheckDcBetweenAmt(InventoryChangeCheckInData data);


    List<Map<String, String>> findQcDateByClient(InventoryChangeCheckInData data);
    List<Map<String, Object>> selectSalesmanName(@Param("data") SupplierDealOutData  supplierDealInData);

    List<SelectSupplierDealData> selectList(SupplierDealInData supplierDealInData);

    /**
     * 获取供应商业务员姓名
     * @param list
     * @return
     */
    List<Map<String, Object>> selectSupplierList(@Param("list")List<SelectSupplierDetailPageListResponse> list);

    List<SelectSupplierDealData> getGdList(@Param("data")SupplierDealInData supplierDealInData);

    List<SupplierDealOutData> getBatSupplier(@Param("list") SupplierDealOutData list);

    List<InventoryChangeSummaryOutData> getStartDataBySto(InventoryChangeSummaryInData inData);

    List<InventoryChangeSummaryOutData> getStartDataByDc(InventoryChangeSummaryInData inData);

    List<InventoryChangeSummaryOutData> getDcQcAmt(@Param("client") String client,@Param("dcCode") String dcCode);

    List<InventoryChangeSummaryOutData> getStoQcAmt(@Param("client") String client,@Param("stoCodeList") List<String> stoCodeList);

    String findDcNameByDcCode(@Param("client")String client,@Param("dcCode") String dcCode);

    List<InventoryChangeSummaryOutData> selectInventoryChangeSummaryByGYX(InventoryChangeSummaryInData inData);

    List<InventoryChangeSummaryOutData> selectDistributionAndAdjustSummaryData(InventoryChangeSummaryInData inData);

    List<Map<String, Object>> selectSupplierName(@Param("inData")SupplierDealInData inData);
    List<InventoryChangeSummaryOutData> getStartDataAll(InventoryChangeSummaryInData inData);

    List<WholesaleSaleOutData> selectWholesaleSalemanList(WholesaleSaleInData inData);

    List<String> selectSoHeadRemark(FuzzyQueryForConditionInData conditionQuery);

    List<WholesaleSaleOutData> selectWholesaleDetailSalemanList(WholesaleSaleInData inData);

    List<WholesaleSaleOutData> selectDistributionInvoiceByBatch(WholesaleSaleInData inData);

//    List<WholesaleSaleOutData> selectWholesaleSalemanList(WholesaleSaleInData inData);

    List<WholesaleSaleOutData> selectDistributionInvoiceDetailByBatch(WholesaleSaleInData inData);

//    List<WholesaleSaleOutData> selectWholesaleDetailSalemanList(WholesaleSaleInData inData);
}