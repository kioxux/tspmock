package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromGroup;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

public interface GaiaSdPromGroupMapper {

    int insertSelective(GaiaSdPromGroup record);

    int updateByPrimaryKeySelective(GaiaSdPromGroup record);


    List<GaiaSdPromGroup> selectAll(@Param("client") String client);
}