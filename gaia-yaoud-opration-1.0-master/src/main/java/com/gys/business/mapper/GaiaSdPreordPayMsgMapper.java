package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPreordPayMsg;
import com.gys.business.service.data.OrderInData;
import com.gys.business.service.data.OrderPayOutData;
import com.gys.business.service.data.PreordPayMsgDetails;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPreordPayMsgMapper extends BaseMapper<GaiaSdPreordPayMsg> {

    /**
     * 获取支付方式
     *
     * @param userInfo
     * @return
     */
    List<OrderPayOutData> getPayList(GetLoginOutData userInfo);

    /**
     * 获取当前支付方式
     *
     * @param inData
     * @return
     */
    List<PreordPayMsgDetails> getOrderDetailsList(OrderInData inData);

    /**
     * 批量插入
     *
     * @param payMsgs
     */
    void insertLists(@Param("list") List<GaiaSdPreordPayMsg> payMsgs);
}
