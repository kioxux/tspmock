package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdBatchChangeLocal;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdBatchChangeLocalMapper extends BaseMapper<GaiaSdBatchChangeLocal> {
    void insertOrUpdate(List<GaiaSdBatchChangeLocal> batchChangeList);
}