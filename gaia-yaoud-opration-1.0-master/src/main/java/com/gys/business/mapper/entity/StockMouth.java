package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 月度库存表
 *
 * </p>
 *
 * @author sy
 * @since 2021-07-13
 */
@Data
@Table(
        name = "GAIA_STOCK_MOUTH"
)
public class StockMouth implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "商品编码")
    @Column(name = "GSM_PRO_CODE")
    private String gsmProCode;

    @ApiModelProperty(value = "地点（门店/仓库）")
    @Column(name = "GSM_SITE_CODE")
    private String gsmSiteCode;

    @ApiModelProperty(value = "年月")
    @Column(name = "GSM_YEAR_MONTH")
    private String gsmYearMonth;

    @ApiModelProperty(value = "批次")
    @Column(name = "GSM_BATCH")
    private String gsmBatch;

    @ApiModelProperty(value = "库存数量")
    @Column(name = "GSM_STOCK_QTY")
    private BigDecimal gsmStockQty;

    @ApiModelProperty(value = "库存金额")
    @Column(name = "GSM_STOCK_AMT")
    private BigDecimal gsmStockAmt;

    @ApiModelProperty(value = "税金")
    @Column(name = "GSM_TAX_AMT")
    private BigDecimal gsmTaxAmt;

    @ApiModelProperty(value = "最后更新时间")
    @Column(name = "LAST_UPDATE_TIME")
    private Date lastUpdateTime;


}
