package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_SALETASK_DPLAN")
@ApiModel
public class GaiaSaletaskDplan implements Serializable {
    /**
     * 客户ID
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 月份
     */
    @Id
    @Column(name = "MONTH_PLAN")
    @ApiModelProperty("月份")
    private String monthPlan;

    /**
     * 门店编号
     */
    @Id
    @Column(name = "STO_CODE")
    @ApiModelProperty("门店编号")
    private String stoCode;

    /**
     * 总计划营业额
     */
    @Column(name = "T_SALE_AMT")
    @ApiModelProperty("总计划营业额")
    private BigDecimal tSaleAmt;

    /**
     * 总计划营业额增长比率
     */
    @Column(name = "GP_SALE_AMT")
    private BigDecimal gpSaleAmt;

    /**
     * 总计划毛利额
     */
    @Column(name = "T_SALE_GROSS")
    @ApiModelProperty("总计划毛利额")
    private BigDecimal tSaleGross;

    /**
     * 总计划毛利额增长比率
     */
    @Column(name = "GP_SALE_GROSS")
    private BigDecimal gpSaleGross;

    /**
     * 总计划会员卡
     */
    @Column(name = "T_MCARD_QTY")
    @ApiModelProperty("总计划会员卡")
    private BigDecimal tMcardQty;

    /**
     * 总计划会员卡增长比率
     */
    @Column(name = "GP_MCARD_QTY")
    private BigDecimal gpMcardQty;

    /**
     * 日均计划营业额
     */
    @Column(name = "A_SALE_AMT")
    @ApiModelProperty("日均计划营业额")
    private BigDecimal aSaleAmt;

    /**
     * 日均计划营业额
     */
    @Column(name = "A_SALE_GROSS")
    @ApiModelProperty("毛利额")
    private BigDecimal aSaleGross;

    /**
     * 日均计划会员卡
     */
    @Column(name = "A_MCARD_QTY")
    @ApiModelProperty("日均计划会员卡")
    private BigDecimal aMcardQty;

    private static final long serialVersionUID = 1L;

    /**
     * 获取客户ID
     *
     * @return CLIENT - 客户ID
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置客户ID
     *
     * @param client 客户ID
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取月份
     *
     * @return MONTH_PLAN - 月份
     */
    public String getMonthPlan() {
        return monthPlan;
    }

    /**
     * 设置月份
     *
     * @param monthPlan 月份
     */
    public void setMonthPlan(String monthPlan) {
        this.monthPlan = monthPlan;
    }

    /**
     * 获取门店编号
     *
     * @return STO_CODE - 门店编号
     */
    public String getStoCode() {
        return stoCode;
    }

    /**
     * 设置门店编号
     *
     * @param stoCode 门店编号
     */
    public void setStoCode(String stoCode) {
        this.stoCode = stoCode;
    }

    /**
     * 获取总计划营业额
     *
     * @return T_SALE_AMT - 总计划营业额
     */
    public BigDecimal gettSaleAmt() {
        return tSaleAmt;
    }

    /**
     * 设置总计划营业额
     *
     * @param tSaleAmt 总计划营业额
     */
    public void settSaleAmt(BigDecimal tSaleAmt) {
        this.tSaleAmt = tSaleAmt;
    }

    /**
     * 获取总计划营业额增长比率
     *
     * @return GP_SALE_AMT - 总计划营业额增长比率
     */
    public BigDecimal getGpSaleAmt() {
        return gpSaleAmt;
    }

    /**
     * 设置总计划营业额增长比率
     *
     * @param gpSaleAmt 总计划营业额增长比率
     */
    public void setGpSaleAmt(BigDecimal gpSaleAmt) {
        this.gpSaleAmt = gpSaleAmt;
    }

    /**
     * 获取总计划毛利额
     *
     * @return T_SALE_GROSS - 总计划毛利额
     */
    public BigDecimal gettSaleGross() {
        return tSaleGross;
    }

    /**
     * 设置总计划毛利额
     *
     * @param tSaleGross 总计划毛利额
     */
    public void settSaleGross(BigDecimal tSaleGross) {
        this.tSaleGross = tSaleGross;
    }

    /**
     * 获取总计划毛利额增长比率
     *
     * @return GP_SALE_GROSS - 总计划毛利额增长比率
     */
    public BigDecimal getGpSaleGross() {
        return gpSaleGross;
    }

    /**
     * 设置总计划毛利额增长比率
     *
     * @param gpSaleGross 总计划毛利额增长比率
     */
    public void setGpSaleGross(BigDecimal gpSaleGross) {
        this.gpSaleGross = gpSaleGross;
    }

    /**
     * 获取总计划会员卡
     *
     * @return T_MCARD_QTY - 总计划会员卡
     */
    public BigDecimal gettMcardQty() {
        return tMcardQty;
    }

    /**
     * 设置总计划会员卡
     *
     * @param tMcardQty 总计划会员卡
     */
    public void settMcardQty(BigDecimal tMcardQty) {
        this.tMcardQty = tMcardQty;
    }

    /**
     * 获取总计划会员卡增长比率
     *
     * @return GP_MCARD_QTY - 总计划会员卡增长比率
     */
    public BigDecimal getGpMcardQty() {
        return gpMcardQty;
    }

    /**
     * 设置总计划会员卡增长比率
     *
     * @param gpMcardQty 总计划会员卡增长比率
     */
    public void setGpMcardQty(BigDecimal gpMcardQty) {
        this.gpMcardQty = gpMcardQty;
    }

    /**
     * 获取日均计划营业额
     *
     * @return A_SALE_AMT - 日均计划营业额
     */
    public BigDecimal getaSaleAmt() {
        return aSaleAmt;
    }

    /**
     * 设置日均计划营业额
     *
     * @param aSaleAmt 日均计划营业额
     */
    public void setaSaleAmt(BigDecimal aSaleAmt) {
        this.aSaleAmt = aSaleAmt;
    }

    /**
     * 获取日均计划营业额
     *
     * @return A_SALE_GROSS - 日均计划营业额
     */
    public BigDecimal getaSaleGross() {
        return aSaleGross;
    }

    /**
     * 设置日均计划营业额
     *
     * @param aSaleGross 日均计划营业额
     */
    public void setaSaleGross(BigDecimal aSaleGross) {
        this.aSaleGross = aSaleGross;
    }

    /**
     * 获取日均计划会员卡
     *
     * @return A_MCARD_QTY - 日均计划会员卡
     */
    public BigDecimal getaMcardQty() {
        return aMcardQty;
    }

    /**
     * 设置日均计划会员卡
     *
     * @param aMcardQty 日均计划会员卡
     */
    public void setaMcardQty(BigDecimal aMcardQty) {
        this.aMcardQty = aMcardQty;
    }
}