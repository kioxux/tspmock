package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaSdDefecproD;
import com.gys.business.mapper.entity.GaiaSdDefecproH;
import com.gys.business.service.data.GaiaSdDefecProDInData;
import com.gys.business.service.data.GaiaSdDefecProHInData;
import com.gys.business.service.data.Unqualified.UnqualifiedDto;
import com.gys.business.service.data.Unqualified.UnqualifiedInDataParam;
import com.gys.business.service.data.UnqualifiedInData;
import com.gys.business.service.data.UnqualifiedOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface UnqualifiedMapper extends BaseMapper<GaiaSdDefecproH> {

    String selectNextVoucherId(@Param("clientId") String clientId);

    String selectNextSYDPVoucherId(@Param("clientId") String clientId);

    List<UnqualifiedOutData> selectUnqualifiedList(UnqualifiedInData inData);

    //查询 批号有效期选择
    List<GaiaSdDefecProDInData> checkSubstandard(GaiaSdDefecProDInData vo);

    // 添加 GAIA_SD_DEFECPRO_D
    void addSubstandardList(List<GaiaSdDefecProDInData> list);
    //修改
    void modifyTheSubstandardListD(List<GaiaSdDefecproD> list);

    //添加 GAIA_SD_DEFECPRO_H
    void addSubstandardLists(GaiaSdDefecproH gaiaSdDefecproH);

    List<GaiaSdDefecproD>selectDefecProDList(@Param("client") String clientId, @Param("stoCode") String brId, @Param("gsddVoucherId") String gsddVoucherId);

    void batchDefecProDList(List<GaiaSdDefecproD> list);

    void batchStockBatch(List<GaiaSdDefecproD> list);

    void batchStock(List<GaiaSdDefecproD> list);

    void removeDefecProD(GaiaSdDefecProDInData inData);

    List<UnqualifiedDto> listUnqualified(UnqualifiedInDataParam inData);

    List<UnqualifiedDto> listUnqualifiedHead(UnqualifiedInDataParam inData);

    List<UnqualifiedDto> listUnqualifiedDetailByBillNo(UnqualifiedInDataParam inData);

    void updateGsdhFlag1(GaiaSdDefecproH defecproH);

    List<GaiaSdDefecProDInData>selectDefecProDetail(@Param("client") String clientId, @Param("stoCode") String brId, @Param("gsddVoucherId") String gsddVoucherId);

    void updateLossStatus(@Param("client") String clientId, @Param("lossStatus") String lossStatus, @Param("voucherId") String gsddVoucherId);

    GaiaSdDefecProHInData selectDefecProH(@Param("clientId") String clientId, @Param("stoCode") String brId, @Param("gsdhVoucherId") String gsddVoucherId);

    int selectDefecProHCount(@Param("clientId") String clientId,@Param("gsdhVoucherId") String gsddVoucherId);
}
