package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSytOperation;
import com.gys.business.service.data.thirdPay.SytOperationInData;
import com.gys.business.service.data.thirdPay.SytOperationOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdSytOperationMapper extends BaseMapper<GaiaSdSytOperation> {
    List<SytOperationOutData> querySytOperation(SytOperationInData inData);
}