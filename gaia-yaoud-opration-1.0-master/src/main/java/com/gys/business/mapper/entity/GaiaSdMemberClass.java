package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Table( name = "GAIA_SD_MEMBER_CLASS" )
public class GaiaSdMemberClass implements Serializable {
    private static final long serialVersionUID = 4821211826251654608L;
    @Id
    @Column( name = "CLIENT" )
    private String clientId;

    @Id
    @Column( name = "GSMC_ID" )
    private String gsmcId;

    @Column( name = "GSMC_NAME" )
    private String gsmcName;

    @Column( name = "GSMC_DISCOUNT_RATE" )
    private String gsmcDiscountRate;

    @Column( name = "GSMC_DISCOUNT_RATE2" )
    private String gsmcDiscountRate2;

    @Column( name = "GSMC_AMT_SALE" )
    private BigDecimal gsmcAmtSale;

    @Column( name = "GSMC_INTEGRAL_SALE" )
    private String gsmcIntegralSale;

    @Column( name = "GSMC_UPGRADE_INTE" )
    private String gsmcUpgradeInte;

    @Column( name = "GSMC_UPGRADE_RECH" )
    private String gsmcUpgradeRech;

    @Column( name = "GSMC_UPGRADE_SET" )
    private String gsmcUpgradeSet;

    @Column(name = "GSMC_CRE_DATE")
    private String gsmcCreDate;

    @Column(name = "GSMC_CRE_EMP")
    private String gsmcCreEmp;

    @Column(name = "GSMC_UPDATE_DATE")
    private String gsmcUpdateDate;

    @Column(name = "GSMC_UPDATE_EMP")
    private String gsmcUpdateEmp;
}
