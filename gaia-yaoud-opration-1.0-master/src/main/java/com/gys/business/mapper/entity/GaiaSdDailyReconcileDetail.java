package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "GAIA_SD_PAY_DAYREPORT_D")
@Data
public class GaiaSdDailyReconcileDetail implements Serializable {
    private static final long serialVersionUID = -6049358371185857173L;
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSPDD_VOUCHER_ID")
    private String gspddVoucherId;

    @Column(name = "GSPDD_BR_ID")
    private String gspddBrId;

    @Column(name = "GSPDD_SERIAL")
    private String gspddSerial;

    @Column(name = "GSPDD_PAY_TYPE")
    private String gspddPayType;

    @Column(name = "GSPDD_SALE_PAYMETHOD_ID")
    private String gspddSalePaymethodId;

    @Column(name = "GSPDD_SALE_RECEIVABLE_AMT")
    private String gspddSaleReceivableAmt;

    @Column(name = "GSPDD_SALE_INPUT_AMT")
    private String gspddSaleInputAmt;

    @Column(name = "GSPDD_SALE_DIFFERENCE")
    private String gspddSaleDifference;

    @Column(name = "GSPDD_SALE_CARD_AMT")
    private String  gspddSaleCardAmt;

    @Column(name = "GSPDD_SALE_DG_AMT")
    private String  gspddSaleDgAmt;
}
