package com.gys.business.mapper.entity;

import lombok.Data;

@Data
public class ReplenishParam {
    private String clientId;
    private String stoCode;
    private String flag1;//冷链 0 否 1 是
    private String flag2;//含麻 0 否 1 是
    private String flag3;//二类精神 0 否 1 是
    private String flag4;//三类器械 0 否 1 是
    private String flag5;//散装中药 0 否 1 是
    private String flag6;//精致饮片 0 否 1 是
    private String flag7;//税率
    private String updateEmp;//更新人
    private String updateDate;//更新日期
    private String updateTime;//更新时间
}
