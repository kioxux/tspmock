package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class GaiaSdNbMedicare implements Serializable {
    private static final long serialVersionUID = 5895027290995170750L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String brId;

    /**
     * 商品编码
     */
    private String proId;

    /**
     * 医保对照编码
     */
    private String contrastId;

    /**
     * 医保唯一码
     */
    private String onlyId;

    /**
     * 1药品 2材料
     */
    private String proType;
    /**
     * 医保商品编码
     */
    private String medicareProId;


    private String type;


    private String medForm;

    private String medFormName;

    private String placeType;

    private String placeName;

    private String priceUnit;

    private String priceUnitName;
    private String ylUnil;

    private String yfBz;

    private String medCateCode;

    private String medCateName;

    private String commonCode;

    private String zxzq;
    private String mcyl;
    private String xzyybz;
}
