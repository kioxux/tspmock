package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "GAIA_SD_PHYSICAL_HEADER")
public class GaiaSdPhysicalHeader implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 盘点门店
     */
    @Id
    @Column(name = "GSPH_BR_ID")
    private String gsphBrId;

    /**
     * 盘点日期
     */
    @Id
    @Column(name = "GSPH_DATE")
    private String gsphDate;

    /**
     * 盘点单号
     */
    @Id
    @Column(name = "GSPH_VOUCHER_ID")
    private String gsphVoucherId;

    /**
     * 盘点时间
     */
    @Column(name = "GSPH_TIME")
    private String gsphTime;

    /**
     * 是否全盘, 0:部分盘点, 1:全盘
     */
    @Column(name = "GSPH_FLAG1")
    private String gsphFlag1;

    /**
     * 是否显示库存, 0:明盘, 1:盲盘
     */
    @Column(name = "GSPH_FLAG2")
    private String gsphFlag2;

    /**
     * 是否批号, 0:数量盘点, 1:批号盘点
     */
    @Column(name = "GSPH_FLAG3")
    private String gsphFlag3;

    /**
     * 更新日期
     */
    @Column(name = "GSPH_UPDATE_DATE")
    private String gsphUpdateDate;

    /**
     * 更新人员
     */
    @Column(name = "GSPH_UPDATE_EMP")
    private String gsphUpdateEmp;

    /**
     * 盘点进度, 0:初始, 1:初盘完成, 2:APP复盘完成
     */
    @Column(name = "GSPH_SCHEDULE")
    private String gsphSchedule;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取盘点门店
     *
     * @return GSPH_BR_ID - 盘点门店
     */
    public String getGsphBrId() {
        return gsphBrId;
    }

    /**
     * 设置盘点门店
     *
     * @param gsphBrId 盘点门店
     */
    public void setGsphBrId(String gsphBrId) {
        this.gsphBrId = gsphBrId;
    }

    /**
     * 获取盘点日期
     *
     * @return GSPH_DATE - 盘点日期
     */
    public String getGsphDate() {
        return gsphDate;
    }

    /**
     * 设置盘点日期
     *
     * @param gsphDate 盘点日期
     */
    public void setGsphDate(String gsphDate) {
        this.gsphDate = gsphDate;
    }

    /**
     * 获取盘点单号
     *
     * @return GSPH_VOUCHER_ID - 盘点单号
     */
    public String getGsphVoucherId() {
        return gsphVoucherId;
    }

    /**
     * 设置盘点单号
     *
     * @param gsphVoucherId 盘点单号
     */
    public void setGsphVoucherId(String gsphVoucherId) {
        this.gsphVoucherId = gsphVoucherId;
    }

    /**
     * 获取盘点时间
     *
     * @return GSPH_TIME - 盘点时间
     */
    public String getGsphTime() {
        return gsphTime;
    }

    /**
     * 设置盘点时间
     *
     * @param gsphTime 盘点时间
     */
    public void setGsphTime(String gsphTime) {
        this.gsphTime = gsphTime;
    }

    /**
     * 获取是否全盘, 0:部分盘点, 1:全盘
     *
     * @return GSPH_FLAG1 - 是否全盘, 0:部分盘点, 1:全盘
     */
    public String getGsphFlag1() {
        return gsphFlag1;
    }

    /**
     * 设置是否全盘, 0:部分盘点, 1:全盘
     *
     * @param gsphFlag1 是否全盘, 0:部分盘点, 1:全盘
     */
    public void setGsphFlag1(String gsphFlag1) {
        this.gsphFlag1 = gsphFlag1;
    }

    /**
     * 获取是否显示库存, 0:明盘, 1:盲盘
     *
     * @return GSPH_FLAG2 - 是否显示库存, 0:明盘, 1:盲盘
     */
    public String getGsphFlag2() {
        return gsphFlag2;
    }

    /**
     * 设置是否显示库存, 0:明盘, 1:盲盘
     *
     * @param gsphFlag2 是否显示库存, 0:明盘, 1:盲盘
     */
    public void setGsphFlag2(String gsphFlag2) {
        this.gsphFlag2 = gsphFlag2;
    }

    /**
     * 获取是否批号, 0:数量盘点, 1:批号盘点
     *
     * @return GSPH_FLAG3 - 是否批号, 0:数量盘点, 1:批号盘点
     */
    public String getGsphFlag3() {
        return gsphFlag3;
    }

    /**
     * 设置是否批号, 0:数量盘点, 1:批号盘点
     *
     * @param gsphFlag3 是否批号, 0:数量盘点, 1:批号盘点
     */
    public void setGsphFlag3(String gsphFlag3) {
        this.gsphFlag3 = gsphFlag3;
    }

    /**
     * 获取更新日期
     *
     * @return GSPH_UPDATE_DATE - 更新日期
     */
    public String getGsphUpdateDate() {
        return gsphUpdateDate;
    }

    /**
     * 设置更新日期
     *
     * @param gsphUpdateDate 更新日期
     */
    public void setGsphUpdateDate(String gsphUpdateDate) {
        this.gsphUpdateDate = gsphUpdateDate;
    }

    /**
     * 获取更新人员
     *
     * @return GSPH_UPDATE_EMP - 更新人员
     */
    public String getGsphUpdateEmp() {
        return gsphUpdateEmp;
    }

    /**
     * 设置更新人员
     *
     * @param gsphUpdateEmp 更新人员
     */
    public void setGsphUpdateEmp(String gsphUpdateEmp) {
        this.gsphUpdateEmp = gsphUpdateEmp;
    }

    /**
     * 获取盘点进度, 0:初始, 1:初盘完成, 2:APP复盘完成
     *
     * @return GSPH_SCHEDULE - 盘点进度, 0:初始, 1:初盘完成, 2:APP复盘完成
     */
    public String getGsphSchedule() {
        return gsphSchedule;
    }

    /**
     * 设置盘点进度, 0:初始, 1:初盘完成, 2:APP复盘完成
     *
     * @param gsphSchedule 盘点进度, 0:初始, 1:初盘完成, 2:APP复盘完成
     */
    public void setGsphSchedule(String gsphSchedule) {
        this.gsphSchedule = gsphSchedule;
    }
}