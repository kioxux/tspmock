package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralExchangeSetHead;
import com.gys.business.service.data.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author RULAISZ
* @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_HEAD(门店积分换购商品设置主表)】的数据库操作Mapper
* @createDate 2021-11-29 09:40:14
* @Entity com.gys.business.mapper.entity.GaiaSdIntegralExchangeSetHead
*/
public interface GaiaSdIntegralExchangeSetHeadMapper {
    List<IntegralOutData> getIntegralExchangeList(IntegralInData inData);

    List<IntegralExchangeStore> getStoreList(IntegralInData inData);

    GaiaSdIntegralExchangeSetHead getByClientAndVoucherIdAndStoCode(@Param("client") String client,@Param("voucherId") String voucherId,@Param("stoCode") String stoCode);

    void batchInsert(@Param("headList") List<GaiaSdIntegralExchangeSetHead> headList);

    List<GaiaSdIntegralExchangeSetHead> getByClientAndVoucherId(@Param("client") String client,@Param("voucherId") String voucherId);

    void deleteByClientAndVoucherId(@Param("client") String client,@Param("voucherId") String voucherId);

    void updateStatus(AuditIntegralExchangeInData inData);
    CopyIntegralExchangeOutData getActivityData(@Param("client") String client, @Param("voucherId") String voucherId);

    void updateEndDate(SetEndTimeIntegralExchangeInData inData);

    List<GaiaSdIntegralExchangeSetHead> getAllByClient(@Param("client") String client, @Param("stoCode") String stoCode);
}
