package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDiseaseMedication;
import com.gys.business.service.data.disease.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaDiseaseMedicationMapper extends BaseMapper<GaiaDiseaseMedication> {
    List<SchemeBO> getFirstRecommendProduct(RelevanceProductInData map1);
    List<SchemeBO> getRelevanceRecommendProduct(RelevanceProductInData inData);

    List<RelevanceProductOutData> selectDisease(RelevanceProductInData inData);

    List<MemberLabelResponse> getMemberLabel(@Param("yesterday") String yesterday,@Param("client")String client,@Param("store")String store);

    List<Map<String, String>> getClientStore();

    List<MemberTagListResponse> getMemberTagList(@Param("cardId")String cardId,@Param("diseaseCode")String diseaseCode,@Param("client") String client,@Param("date") String date);

    void update(@Param("memberTag")MemberTagListResponse memberTag);

    void insertMemberTag(@Param("memberTag")MemberTagListResponse memberTag);

    void deleteMemberTag();

    List<SelectCardTagResponse> getCardTag(SelectCardTagRequest request);

}