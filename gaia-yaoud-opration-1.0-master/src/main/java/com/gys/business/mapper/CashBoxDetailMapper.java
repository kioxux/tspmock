package com.gys.business.mapper;

import com.gys.business.cashbox.form.CashBoxForm;
import com.gys.business.cashbox.vo.CashBoxDetailVO;
import com.gys.business.mapper.entity.CashBoxDetail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2022/1/27 02:15
 */
@Mapper
public interface CashBoxDetailMapper {

    CashBoxDetail getById(Long id);

    int add(CashBoxDetail cashBoxDetail);

    List<CashBoxDetail> findList(CashBoxDetail cond);

    List<CashBoxDetailVO> getChangeRecord(CashBoxForm cashBoxForm);
}
