package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "GAIA_SD_SERVER_RELEVANCY")
public class GaiaSdServerRelevancy implements Serializable {

    private static final long serialVersionUID = 202818966754443390L;
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSSR_BR_ID")
    private String gssrBrId;

    @Id
    @Column(name = "GSSR_ID")
    private String gssrId;

    @Column(name = "GSSR_COMPCLASS")
    private String gssrCompclass;

    @Column(name = "GSSR_COMPCLASS_REMARK")
    private String gssrCompclassRemark;

    @Column(name = "GSSR_COMPCLASS_NAME")
    private String gssrCompclassName;

    @Column(name = "GSSR_RELE_COMPCLASS")
    private String gssrReleCompclass;

    @Column(name = "GSSR_RELE_COMPCLASS_REMARK")
    private String gssrReleCompclassRemark;

    @Column(name = "GSSR_RELE_COMPCLASS_NAME")
    private String gssrReleCompclassName;

    @Column(name = "GSSR_PRIORITY1")
    private Short gssrPriority1;

    @Column(name = "GSSR_PRIORITY2")
    private Short gssrPriority2;

    @Column(name = "GSSR_PRIORITY3")
    private Short gssrPriority3;

    @Column(name = "GSSR_PROBABILITY1")
    private String gssrProbability1;

    @Column(name = "GSSR_PROBABILITY2")
    private String gssrProbability2;

    @Column(name = "GSSR_EXPLAIN1")
    private String gssrExplain1;

    @Column(name = "GSSR_EXPLAIN2")
    private String gssrExplain2;

    @Column(name = "GSSR_EXPLAIN3")
    private String gssrExplain3;

    @Column(name = "GSSR_EXPLAIN4")
    private String gssrExplain4;

    @Column(name = "GSSR_EXPLAIN5")
    private String gssrExplain5;

    @Column(name = "GSSR_UPDATE_DATE")
    private String gssrUpdateDate;

    @Column(name = "GSSR_BEGIN_DATE")
    private String gssrBeginDate;

    @Column(name = "GSSR_END_DATE")
    private String gssrEndDate;
}
