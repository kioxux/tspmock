package com.gys.business.mapper;

import com.gys.business.service.data.NewProductDistributionPlanDto;
import com.gys.business.service.data.NewProductDistributionPlanVo;
import com.gys.business.service.data.NewProductDto;
import com.gys.common.data.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Mapper
public interface NewProductDistributionPlanMapper {

    /**
     * 根据查询参数获取新品铺货详情（商品主数据业务信息表相关字段除外）
     * @param planDto
     * @return
     */
    List<NewProductDistributionPlanVo> plan(NewProductDistributionPlanDto planDto);

    /**
     * 获取新品铺货计划主表所有的client和proSelfCode（只用到这两个字段，其他业务可添加字段）
     * @return
     */
    List<Map<String, String>> getAllClientAndProSelfCode();

    /**
     * 根据省、市获取加盟商信息表的client、clientName、prov和city（只用到这四个字段，其他业务可添加字段）
     * @param planDto
     * @return
     */
    List<Map<String, String>> getByProvAndCity(NewProductDistributionPlanDto planDto);

    /**
     * 根据client列表和proSelfCode列表获取商品主数据业务信息表的相关信息
     * @param clients
     * @param proSelfCodes
     * @return
     */
    List<NewProductDistributionPlanVo> getProductDetailByClientAndProSelfCode(@Param("clients") Set<String> clients, @Param("proSelfCodes") Set<String> proSelfCodes);

    List<Map<String, Object>> getProSelfCodeByCondition(NewProductDto dto);
    List<Map<String, String>> findAllClientAndProCode(NewProductDistributionNoPlanDto dto);

    List<GaiaNewProductDistributionNoPlanVo> findNoPlanResult(NewProductDistributionNoPlanDto dto);

    List<Map<String, String>> findNoPlanClientList(NewProductDistributionNoPlanClientDto noPlanClientDto);

    List<Map<String,String>> findNoPlanProductCodeList(NewProductDistributionNoPlanProductCodeDto noPlanProductCodeDto);

    List<Map<String, String>> findProvList(NewProductDistributionNoPlanProvAndCityDto noPlanProvAndCityDto);

    List<Map<String, String>> findCityList(NewProductDistributionNoPlanProvAndCityDto noPlanProvAndCityDto);

    List<Map<String, String>> findProDetailByProCode(@Param("clientSet") Set<String> clientSet,
                                               @Param("proCodeSet") Set<String> proCodeSet,
                                               @Param("dto") NewProductDistributionNoPlanProductCodeDto dto);

    Integer findMaxLength();
}
