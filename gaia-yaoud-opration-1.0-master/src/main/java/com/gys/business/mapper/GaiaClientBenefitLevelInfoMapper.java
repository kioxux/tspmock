package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaClientBenefitLevelInfo;
import com.gys.common.data.GaiaClientBenefitLevelInfoExcelData;
import com.gys.common.vo.GaiaClientBenefitLevelInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaClientBenefitLevelInfoMapper {

    int insert(GaiaClientBenefitLevelInfo record);

    GaiaClientBenefitLevelInfo selectByPrimaryKey(Long id);

    int update(GaiaClientBenefitLevelInfo record);
    List<GaiaClientBenefitLevelInfoVo> selectByClient(@Param("clientIds") List<String> clientIds);

    void insertList(@Param("gaiaClientLevelInfos") List<GaiaClientBenefitLevelInfo> gaiaClientLevelInfos);

    List<GaiaClientBenefitLevelInfoExcelData>  getClientLevelExcelData(@Param("client") String client);
}