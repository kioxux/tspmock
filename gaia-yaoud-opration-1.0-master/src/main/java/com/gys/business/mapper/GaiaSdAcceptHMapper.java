//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdAcceptH;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdAcceptHMapper extends BaseMapper<GaiaSdAcceptH> {
    List<GetAcceptOutData> selectList(GetAcceptInData inData);

    List<GetAcceptOutData> supplierAcceptList(GetAcceptInData inData);

    List<GetAcceptOutData> acceptList(GetExamineInData inData);

    String selectNextVoucherId(@Param("clientId") String clientId, @Param("codePre") String codePre);

    void rejectAcceptOrder(@Param("clientId") String clientId, @Param("gsehBrId") String gsehBrId,@Param("gsahVoucherId") String gsahVoucherId,@Param("gsahStatus") String gsahStatus);

    void updateColdchainInfo(@Param("info") ColdchainInfo info,@Param("psdh") List<String> psdh);

    List<String> getPsdh(ColdchainInfo info);

    ColdchainInfo getColdchainInfo(AcceptWareHouseInData inData);

    String getEmpName(@Param("client") String client,@Param("userId") String userId);

    List<String> getGsahPsVoucherId(@Param("client") String client,@Param("stoCode") String stoCode,@Param("jhdh") String jhdh);

    String getAcceptStatus(@Param("client") String client,@Param("stoCode") String stoCode,@Param("jhdh") String jhdh);

    List<GetAcceptOutData> acceptCSList(GetExamineInData inData);

    List<PoPriceProductHistoryOutData> getPoPriceProductHistory(PoPriceProductHistoryInData inData);

    String getProductCreateDate(PoPriceProductHistoryInData inData);

    String getProductValidityDate(PoPriceProductHistoryInData inData);

    ColdchainInfo getStoreReceiptCsClodChainInfo(@Param("client") String client, @Param("stoCode") String stoCode, @Param("voucherId") String voucherId);
}
