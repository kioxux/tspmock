package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaKeyCommodityTaskBillStore;
import com.gys.business.service.data.GaiaKeyCommodityTaskBillInData;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * 重点商品任务—参与门店表(GaiaKeyCommodityTaskBillStore)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-01 15:39:15
 */
public interface GaiaKeyCommodityTaskBillStoreMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaKeyCommodityTaskBillStore queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaKeyCommodityTaskBillStore> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaKeyCommodityTaskBillStore 实例对象
     * @return 对象列表
     */
    List<GaiaKeyCommodityTaskBillStore> queryAll(GaiaKeyCommodityTaskBillStore gaiaKeyCommodityTaskBillStore);

    /**
     * 新增数据
     *
     * @param gaiaKeyCommodityTaskBillStore 实例对象
     * @return 影响行数
     */
    int insert(GaiaKeyCommodityTaskBillStore gaiaKeyCommodityTaskBillStore);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaKeyCommodityTaskBillStore> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaKeyCommodityTaskBillStore> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaKeyCommodityTaskBillStore> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaKeyCommodityTaskBillStore> entities);

    /**
     * 修改数据
     *
     * @param gaiaKeyCommodityTaskBillStore 实例对象
     * @return 影响行数
     */
    int update(GaiaKeyCommodityTaskBillStore gaiaKeyCommodityTaskBillStore);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    int getCount(@Param("client") String client, @Param("billCode") String billCode, @Param("stoCode") String stoCode);

    List<String> getStoCode(@Param("client") String client, @Param("billCode")String billCode, @Param("stoCode")String stoCode);

    int updateByStoCode(GaiaKeyCommodityTaskBillStore store);

    Integer getStoreLevel(@Param("client") String client, @Param("stoCode")String stoCode);

    List<String> listStoreInfo(GaiaKeyCommodityTaskBillStore param);
}

