package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_SD_SYT_ACCOUNT")
public class GaiaSdSytAccount implements Serializable {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "CLIENT")
    private String client;

    @Column(name = "BR_ID")
    private String brId;

    /**
     * 渠道编号
     */
    @Column(name = "CHANNEL")
    private String channel;

    /**
     * 商户号
     */
    @Column(name = "MER_ID")
    private String merId;

    /**
     * MD5加密要素
     */
    @Column(name = "MD5_KEY")
    private String md5Key;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBrId() {
        return brId;
    }

    public void setBrId(String brId) {
        this.brId = brId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMerId() {
        return merId;
    }

    public void setMerId(String merId) {
        this.merId = merId;
    }

    public String getMd5Key() {
        return md5Key;
    }

    public void setMd5Key(String md5Key) {
        this.md5Key = md5Key;
    }
}