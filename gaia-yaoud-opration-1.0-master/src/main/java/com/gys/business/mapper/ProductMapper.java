//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.Product;
import com.gys.business.service.data.GetProductOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface ProductMapper extends BaseMapper<Product> {
    List<GetProductOutData> selectProductsByCondition();
}
