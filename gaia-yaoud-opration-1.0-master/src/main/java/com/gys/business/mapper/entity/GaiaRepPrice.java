package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "GAIA_REP_PRICE")
@Data
public class GaiaRepPrice implements Serializable {
    private static final long serialVersionUID = 7340912316728121606L;
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "REP_BR_ID"
    )
    private String repBrId;
    @Column(
            name = "REP_BR_TYPE"
    )
    private String repBrType;
    @Id
    @Column(
            name = "REP_PG_ID"
    )
    private String repPgId;
    @Column(
            name = "REP_PG_ITEM"
    )
    private String repPgItem;
    @Column(
            name = "REP_DATE"
    )
    private String repDate;
    @Id
    @Column(
            name = "REP_PRO_ID"
    )
    private String repProId;
    @Column(
            name = "REP_NEED_QTY"
    )
    private String repNeedQty;
    @Column(
            name = "REP_STATUS"
    )
    private String repStatus;
    @Column(
            name = "REP_CREATE_DATE"
    )
    private String repCreateDate;
    @Column(
            name = "REP_CREATE_TIME"
    )
    private String repCreateTime;
    @Column(
            name = "REP_CREATE_USER"
    )
    private String repCreateUser;
}
