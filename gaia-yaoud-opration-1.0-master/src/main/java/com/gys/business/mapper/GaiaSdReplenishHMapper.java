//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.mapper.entity.GaiaSdReplenishHExt;
import com.gys.business.service.data.GetReplenishInData;
import com.gys.business.service.data.GetReplenishOutData;
import com.gys.business.service.data.ReplenishHOnWayOutData;
import com.gys.business.service.data.check.CallOrderForm;
import com.gys.business.service.data.check.OrderCheckForm;
import com.gys.business.service.data.check.RequestOrderVO;
import com.gys.business.service.data.replenishParam.ReplenishPart;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.storeorder.ProSummaryVO;
import com.gys.common.data.storeorder.StoreDetailedVO;
import com.gys.common.data.storeorder.StoreOrderDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdReplenishHMapper extends BaseMapper<GaiaSdReplenishH> {
    List<GetReplenishOutData> selectList(GetReplenishInData inData);

    String selectNextVoucherId(GaiaSdReplenishH inData);

    void update(GaiaSdReplenishH replenishH);

//    String queryNextVoucherId(GaiaSdReplenishH inData);

    int selectReplenishCount(@Param("clientId") String clientId, @Param("gsrhBrId") String gsrhBrId, @Param("gsrhDate") String gsrhDate);

    List<RequestOrderVO> getCallList(CallOrderForm callOrderForm);

    List<RequestOrderVO> orderList(OrderCheckForm orderCheckForm);

    GaiaSdReplenishH getUnique(GaiaSdReplenishH cond);

    GetReplenishOutData selectReplenishOrder(@Param("clientId") String clientId, @Param("stoCode") String stoCode, @Param("gsrhVoucherId") String gsrhVoucherId);

    List<ReplenishHOnWayOutData> getProductOnWayList(@Param("client") String client, @Param("brId") String brId, @Param("replenishDate") String replenishDate);

    /**
     * 获取最大铺货单号
     *
     * @param client
     * @return
     */
    String getMaxVoucherId(@Param("client") String client);

    void saveBatch(@Param("replenishHList") List<GaiaSdReplenishHExt> replenishHList);

    /**
     * 查询门店明细清单
     *
     * @param storeOrderDTO storeOrderDTO
     * @return List<StoreDetailedVO>
     */
    List<StoreDetailedVO> selectStoreDetailedList(StoreOrderDTO storeOrderDTO);

    /**
     * 查询商品汇总清单
     *
     * @param storeOrderDTO storeOrderDTO
     * @return List<ProSummaryVO>
     */
    List<ProSummaryVO> selectProSummaryList(StoreOrderDTO storeOrderDTO);

    /**
     * 补货分单设置查询
     * @param clientId
     * @param stoCode
     * @return
     */
    ReplenishPart selectReplenishPart(@Param("clientId") String clientId,@Param("stoCode") String stoCode);

}
