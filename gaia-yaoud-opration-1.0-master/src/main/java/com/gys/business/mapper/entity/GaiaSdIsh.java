package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "GAIA_SD_ISH")
public class GaiaSdIsh  implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店号
     */
    @Id
    @Column(name = "ISH_BR_ID")
    private String ishBrId;

    /**
     * 销售日期
     */
    @Id
    @Column(name = "ISH_DATE")
    private Date ishDate;

    /**
     * 销售单号
     */
    @Id
    @Column(name = "ISH_BILL_NO")
    private String ishBillNo;

    /**
     * 交易流水号
     */
    @Id
    @Column(name = "ISH_DEAL_SERIAL")
    private String ishDealSerial;

    /**
     * 销售时间
     */
    @Column(name = "ISH_TIME")
    private String ishTime;

    @Column(name = "ISH_COMPUTE_SERIAL")
    private String ishComputeSerial;

    @Column(name = "ISH_PERSONAL_INFORMATION")
    private String ishPersonalInformation;

    /**
     * 医疗费总额
     */
    @Column(name = "ISH_TOTAL_AMT")
    private BigDecimal ishTotalAmt;

    /**
     * 帐户支付
     */
    @Column(name = "ISH_PERSONAL_CURR_PAY")
    private BigDecimal ishPersonalCurrPay;

    /**
     * 公务员补助
     */
    @Column(name = "ISH_PERSONAL_TOTAL_PAY")
    private BigDecimal ishPersonalTotalPay;

    @Column(name = "ISH_CASH_PAY_A")
    private BigDecimal ishCashPayA;

    @Column(name = "ISH_CASH_PAY_B")
    private BigDecimal ishCashPayB;

    @Column(name = "ISH_CASH_PAY_C")
    private BigDecimal ishCashPayC;

    /**
     * 大额理赔金额
     */
    @Column(name = "ISH_CASH_PAY_D")
    private BigDecimal ishCashPayD;

    @Column(name = "ISH_PUBFOUND_PAY")
    private BigDecimal ishPubfoundPay;

    /**
     * 统筹支付
     */
    @Column(name = "ISH_FOUND_PAY")
    private BigDecimal ishFoundPay;

    @Column(name = "ISH_HELP_PAY")
    private BigDecimal ishHelpPay;

    /**
     * 现金支付
     */
    @Column(name = "ISH_CASH_PAY")
    private BigDecimal ishCashPay;

    @Column(name = "ISH_SOCIETY_HELP_PAY")
    private BigDecimal ishSocietyHelpPay;

    @Column(name = "ISH_SELF_BEFORE")
    private BigDecimal ishSelfBefore;

    @Column(name = "ISH_SELF_AFTER")
    private BigDecimal ishSelfAfter;

    @Column(name = "ISH_PUBLIC_BEFORE")
    private BigDecimal ishPublicBefore;

    @Column(name = "ISH_PUBLIC_AFTER")
    private BigDecimal ishPublicAfter;

    /**
     * 帐户余额
     */
    @Column(name = "ISH_PERSONAL_CURR_REMIAN")
    private BigDecimal ishPersonalCurrRemian;

    @Column(name = "ISH_PERSONAL_TOTA_REMIAN")
    private BigDecimal ishPersonalTotaRemian;

    /**
     * 交易类型00是收款01退款
     */
    @Column(name = "ISH_TX_CODE")
    private String ishTxCode;

    /**
     * 民族
     */
    @Column(name = "ISH_CARD_INFORM")
    private String ishCardInform;

    /**
     * 医疗:参保类别/工伤:参保状态/生育:
     */
    @Column(name = "ISH_CARD_TYPE")
    private String ishCardType;

    /**
     * 社保卡卡号(社会保障识别号)
     */
    @Column(name = "ISH_YBH")
    private String ishYbh;

    /**
     * 姓名
     */
    @Column(name = "ISH_XM")
    private String ishXm;

    /**
     * 医院id
     */
    @Column(name = "ISH_YYID")
    private String ishYyid;

    /**
     * 就诊流水号（住院（或门诊）号）
     */
    @Column(name = "ISH_JZBH")
    private String ishJzbh;

    /**
     * 医生
     */
    @Column(name = "ISH_DOCTOR")
    private String ishDoctor;

    /**
     * 城区号(六城区离休和子女) 杭州/药师编码
     */
    @Column(name = "ISH_YSID")
    private String ishYsid;

    /**
     * 原销售单退货标志
     */
    @Column(name = "ISH_THFLAG")
    private String ishThflag;

    /**
     * 险种类别
     */
    @Column(name = "ISH_STATUS")
    private String ishStatus;

    /**
     * 是否享受公务员待遇
     */
    @Column(name = "ISH_GWY_FLAG")
    private String ishGwyFlag;

    /**
     * 行政区划编码
     */
    @Column(name = "ISH_XZQH_ID")
    private String ishXzqhId;

    /**
     * 封锁状况
     */
    @Column(name = "ISH_FSZK")
    private String ishFszk;

    /**
     * 封锁原因
     */
    @Column(name = "ISH_FSYY")
    private String ishFsyy;

    /**
     * 人员变更类型
     */
    @Column(name = "ISH_RYBG")
    private String ishRybg;

    /**
     * 人员类型变更时间
     */
    @Column(name = "ISH_RYBG_DATE")
    private Date ishRybgDate;

    /**
     * 医疗,工伤：待遇封锁开始时间/生育: 享受待遇实际开始日期
     */
    @Column(name = "ISH_DYFS_BEGIN")
    private Date ishDyfsBegin;

    /**
     * 医疗,工伤：待遇封锁终止时间/生育: 享受待遇实际开始日期
     */
    @Column(name = "ISH_DYFS_END")
    private Date ishDyfsEnd;

    /**
     * 民政人员类别
     */
    @Column(name = "ISH_MZRY_TYPE")
    private String ishMzryType;

    /**
     * 居民缴费档次
     */
    @Column(name = "ISH_JMJFDC")
    private String ishJmjfdc;

    /**
     * 患者联系电话
     */
    @Column(name = "ISH_HZLXDH")
    private String ishHzlxdh;

    /**
     * 当前扶贫人员类别
     */
    @Column(name = "ISH_FPRY_TYPE")
    private String ishFpryType;

    /**
     * 工伤:工伤参保时间/生育:生育参保时间
     */
    @Column(name = "ISH_GSCA_DATE")
    private Date ishGscaDate;

    /**
     * 工伤:转院转诊/生育:不能享受就诊原因
     */
    @Column(name = "ISH_ZYZZ")
    private String ishZyzz;

    /**
     * 工伤:辅助器具超标审批/生育:可否享受就诊标志
     */
    @Column(name = "ISH_FZQJCBSP")
    private String ishFzqjcbsp;

    /**
     * 就医登记机构编码
     */
    @Column(name = "ISH_JZYLJG_ID")
    private String ishJzyljgId;

    /**
     * 并发症标志
     */
    @Column(name = "ISH_BFZ_FLAG")
    private String ishBfzFlag;

    /**
     *  
     */
    @Column(name = "ISH_BILL_SERIAL")
    private String ishBillSerial;

    /**
     * 性别
     */
    @Column(name = "ISH_ENTERPRISE_CHARACTER")
    private String ishEnterpriseCharacter;

    /**
     * 人员类别
     */
    @Column(name = "ISH_PERSONAL_CLASS")
    private String ishPersonalClass;

    /**
     * 单位名称
     */
    @Column(name = "ISH_PERSONAL_CHARACTER")
    private String ishPersonalCharacter;

    /**
     * 历史起付线公务员返 还
     */
    @Column(name = "ISH_QFZF_AMT")
    private BigDecimal ishQfzfAmt;

    /**
     * 单病种定点医疗机构 垫支
     */
    @Column(name = "ISH_QFBZACCOUNT_AMT")
    private BigDecimal ishQfbzaccountAmt;

    /**
     * 民政救助金额
     */
    @Column(name = "ISH_QFBZCASH_AMT")
    private BigDecimal ishQfbzcashAmt;

    /**
     * 民政救助门诊余额
     */
    @Column(name = "ISH_FDZL_AMTF")
    private BigDecimal ishFdzlAmtf;

    /**
     * 耐多药项目支付金额
     */
    @Column(name = "ISH_QFYSACCOUNT_AMT")
    private BigDecimal ishQfysaccountAmt;

    /**
     * 一般诊疗支付数
     */
    @Column(name = "ISH_QFYSCASH_AMT")
    private BigDecimal ishQfyscashAmt;

    /**
     * 神华救助基金支付数
     */
    @Column(name = "ISH_CGFDXGRZF_AMT")
    private BigDecimal ishCgfdxgrzfAmt;

    /**
     * 本年统筹支付累计
     */
    @Column(name = "ISH_ZLCWF_AMT")
    private BigDecimal ishZlcwfAmt;

    /**
     * 本年大额支付累计
     */
    @Column(name = "ISH_GWYBCMZQFX_AMT")
    private BigDecimal ishGwybcmzqfxAmt;

    /**
     * 特病起付线支付累计
     */
    @Column(name = "ISH_GWYMZQFXLNZH_AMT")
    private BigDecimal ishGwymzqfxlnzhAmt;

    /**
     * 耐多药项目累计
     */
    @Column(name = "ISH_GWYMZFDZF_AMT")
    private BigDecimal ishGwymzfdzfAmt;

    /**
     * 本年民政救助住院支 付累计
     */
    @Column(name = "ISH_FHJBYLBXFY_AMT")
    private BigDecimal ishFhjbylbxfyAmt;

    /**
     * 本次起付线支付金额
     */
    @Column(name = "ISH_LXRYJJ_AMT")
    private BigDecimal ishLxryjjAmt;

    /**
     * 本次进入医保范围费 用
     */
    @Column(name = "ISH_TXMZTCJJ_AMT")
    private BigDecimal ishTxmztcjjAmt;

    /**
     * 药事服务支付数
     */
    @Column(name = "ISH_ZNTCJJ_AMT")
    private BigDecimal ishZntcjjAmt;

    /**
     * 医院超标扣款金额
     */
    @Column(name = "ISH_LFJJ_AMT")
    private BigDecimal ishLfjjAmt;

    /**
     *  实足年龄
     */
    @Column(name = "ISH_LXJSJJ_AMT")
    private Short ishLxjsjjAmt;

    /**
     * 生育基金支付
     */
    @Column(name = "ISH_JSQZHYE_AMT")
    private BigDecimal ishJsqzhyeAmt;

    /**
     * 生育现金支付
     */
    @Column(name = "ISH_JSQBNZHYE_AMT")
    private BigDecimal ishJsqbnzhyeAmt;

    /**
     * 工伤基金支付
     */
    @Column(name = "ISH_JSQLNZHYE_AMT")
    private BigDecimal ishJsqlnzhyeAmt;

    /**
     * 工伤现金支付
     */
    @Column(name = "ISH_JSHZHYE_AMT")
    private BigDecimal ishJshzhyeAmt;

    @Column(name = "ISH_KNJZZJ_AMT")
    private BigDecimal ishKnjzzjAmt;

    /**
     * 工伤单病种机构垫支
     */
    @Column(name = "ISH_QYZZMZTCJJ_AMT")
    private BigDecimal ishQyzzmztcjjAmt;

    /**
     * 其他补助
     */
    @Column(name = "ISH_LJMJJ_AMT")
    private BigDecimal ishLjmjjAmt;

    /**
     * 生育账户支付
     */
    @Column(name = "ISH_SNETJJ_AMT")
    private BigDecimal ishSnetjjAmt;

    /**
     * 工伤账户支付
     */
    @Column(name = "ISH_NMGJJ_AMT")
    private BigDecimal ishNmgjjAmt;

    /**
     * 健康扶贫医疗基金
     */
    @Column(name = "ISH_NMGLJJF_MONTHS")
    private BigDecimal ishNmgljjfMonths;

    /**
     * 住院次数
     */
    @Column(name = "ISH_JTBC_DAYS")
    private Long ishJtbcDays;

    /**
     * 工伤个人编号|
     */
    @Column(name = "ISH_MZREGISTER_ID")
    private String ishMzregisterId;

    /**
     * 工伤单位编号
     */
    @Column(name = "ISH_ENTERPRISE_ID")
    private String ishEnterpriseId;

    /**
     * 身份证号   
     */
    @Column(name = "ISH_PERSONAL_ID")
    private String ishPersonalId;

    /**
     * 住址
     */
    @Column(name = "ISH_PERSONAL")
    private String ishPersonal;

    /**
     * 精准脱贫保险金额
     */
    @Column(name = "ISH_MZQFBZLJ")
    private BigDecimal ishMzqfbzlj;

    /**
     * 其他扶贫报销金额
     */
    @Column(name = "ISH_GFKZPEJJ_AMT")
    private BigDecimal ishGfkzpejjAmt;

    @Column(name = "ISH_GFJFPEJJ_AMT")
    private BigDecimal ishGfjfpejjAmt;

    @Column(name = "ISH_LFKZPEJJ_AMT")
    private BigDecimal ishLfkzpejjAmt;

    @Column(name = "ISH_LFJFPEJJ_AMT")
    private BigDecimal ishLfjfpejjAmt;

    @Column(name = "ISH_FSJJ_AMT")
    private BigDecimal ishFsjjAmt;

    @Column(name = "ISH_FSJJJ_AMT")
    private BigDecimal ishFsjjjAmt;

    @Column(name = "ISH_FTJJJ_AMT")
    private BigDecimal ishFtjjjAmt;

    @Column(name = "ISH_FJJJJ_AMT")
    private BigDecimal ishFjjjjAmt;

    @Column(name = "ISH_FCYXJJ_AMT")
    private BigDecimal ishFcyxjjAmt;

    /**
     * 中心结算时间
     */
    @Column(name = "ISH_JYDATE")
    private Date ishJydate;

    @Column(name = "ISH_CQZNJJ_AMT")
    private BigDecimal ishCqznjjAmt;

    @Column(name = "ISH_CQGFLXJJ_AMT")
    private BigDecimal ishCqgflxjjAmt;

    @Column(name = "ISH_CQLFJJ_AMT")
    private BigDecimal ishCqlfjjAmt;

    @Column(name = "ISH_XNHJJ_AMT")
    private BigDecimal ishXnhjjAmt;

    @Column(name = "ISH_STRUC1")
    private String ishStruc1;

    @Column(name = "ISH_TJJJ_AMT")
    private BigDecimal ishTjjjAmt;

    @Column(name = "ISH_DXSJJ_AMT")
    private BigDecimal ishDxsjjAmt;

    @Column(name = "ISH_AMT1")
    private BigDecimal ishAmt1;

    @Column(name = "ISH_AMT2")
    private BigDecimal ishAmt2;

    @Column(name = "ISH_AMT3")
    private BigDecimal ishAmt3;

    @Column(name = "ISH_AMT4")
    private BigDecimal ishAmt4;

    @Column(name = "ISH_AMT5")
    private BigDecimal ishAmt5;

    @Column(name = "ISH_AMT6")
    private BigDecimal ishAmt6;

    @Column(name = "ISH_STR1")
    private String ishStr1;

    @Column(name = "ISH_STR2")
    private String ishStr2;

    @Column(name = "ISH_STR3")
    private String ishStr3;

    @Column(name = "ISH_STR4")
    private String ishStr4;

    @Column(name = "ISH_STR5")
    private String ishStr5;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取门店号
     *
     * @return ISH_BR_ID - 门店号
     */
    public String getIshBrId() {
        return ishBrId;
    }

    /**
     * 设置门店号
     *
     * @param ishBrId 门店号
     */
    public void setIshBrId(String ishBrId) {
        this.ishBrId = ishBrId;
    }

    /**
     * 获取销售日期
     *
     * @return ISH_DATE - 销售日期
     */
    public Date getIshDate() {
        return ishDate;
    }

    /**
     * 设置销售日期
     *
     * @param ishDate 销售日期
     */
    public void setIshDate(Date ishDate) {
        this.ishDate = ishDate;
    }

    /**
     * 获取销售单号
     *
     * @return ISH_BILL_NO - 销售单号
     */
    public String getIshBillNo() {
        return ishBillNo;
    }

    /**
     * 设置销售单号
     *
     * @param ishBillNo 销售单号
     */
    public void setIshBillNo(String ishBillNo) {
        this.ishBillNo = ishBillNo;
    }

    /**
     * 获取交易流水号
     *
     * @return ISH_DEAL_SERIAL - 交易流水号
     */
    public String getIshDealSerial() {
        return ishDealSerial;
    }

    /**
     * 设置交易流水号
     *
     * @param ishDealSerial 交易流水号
     */
    public void setIshDealSerial(String ishDealSerial) {
        this.ishDealSerial = ishDealSerial;
    }

    /**
     * 获取销售时间
     *
     * @return ISH_TIME - 销售时间
     */
    public String getIshTime() {
        return ishTime;
    }

    /**
     * 设置销售时间
     *
     * @param ishTime 销售时间
     */
    public void setIshTime(String ishTime) {
        this.ishTime = ishTime;
    }

    /**
     * @return ISH_COMPUTE_SERIAL
     */
    public String getIshComputeSerial() {
        return ishComputeSerial;
    }

    /**
     * @param ishComputeSerial
     */
    public void setIshComputeSerial(String ishComputeSerial) {
        this.ishComputeSerial = ishComputeSerial;
    }

    /**
     * @return ISH_PERSONAL_INFORMATION
     */
    public String getIshPersonalInformation() {
        return ishPersonalInformation;
    }

    /**
     * @param ishPersonalInformation
     */
    public void setIshPersonalInformation(String ishPersonalInformation) {
        this.ishPersonalInformation = ishPersonalInformation;
    }

    /**
     * 获取医疗费总额
     *
     * @return ISH_TOTAL_AMT - 医疗费总额
     */
    public BigDecimal getIshTotalAmt() {
        return ishTotalAmt;
    }

    /**
     * 设置医疗费总额
     *
     * @param ishTotalAmt 医疗费总额
     */
    public void setIshTotalAmt(BigDecimal ishTotalAmt) {
        this.ishTotalAmt = ishTotalAmt;
    }

    /**
     * 获取帐户支付
     *
     * @return ISH_PERSONAL_CURR_PAY - 帐户支付
     */
    public BigDecimal getIshPersonalCurrPay() {
        return ishPersonalCurrPay;
    }

    /**
     * 设置帐户支付
     *
     * @param ishPersonalCurrPay 帐户支付
     */
    public void setIshPersonalCurrPay(BigDecimal ishPersonalCurrPay) {
        this.ishPersonalCurrPay = ishPersonalCurrPay;
    }

    /**
     * 获取公务员补助
     *
     * @return ISH_PERSONAL_TOTAL_PAY - 公务员补助
     */
    public BigDecimal getIshPersonalTotalPay() {
        return ishPersonalTotalPay;
    }

    /**
     * 设置公务员补助
     *
     * @param ishPersonalTotalPay 公务员补助
     */
    public void setIshPersonalTotalPay(BigDecimal ishPersonalTotalPay) {
        this.ishPersonalTotalPay = ishPersonalTotalPay;
    }

    /**
     * @return ISH_CASH_PAY_A
     */
    public BigDecimal getIshCashPayA() {
        return ishCashPayA;
    }

    /**
     * @param ishCashPayA
     */
    public void setIshCashPayA(BigDecimal ishCashPayA) {
        this.ishCashPayA = ishCashPayA;
    }

    /**
     * @return ISH_CASH_PAY_B
     */
    public BigDecimal getIshCashPayB() {
        return ishCashPayB;
    }

    /**
     * @param ishCashPayB
     */
    public void setIshCashPayB(BigDecimal ishCashPayB) {
        this.ishCashPayB = ishCashPayB;
    }

    /**
     * @return ISH_CASH_PAY_C
     */
    public BigDecimal getIshCashPayC() {
        return ishCashPayC;
    }

    /**
     * @param ishCashPayC
     */
    public void setIshCashPayC(BigDecimal ishCashPayC) {
        this.ishCashPayC = ishCashPayC;
    }

    /**
     * 获取大额理赔金额
     *
     * @return ISH_CASH_PAY_D - 大额理赔金额
     */
    public BigDecimal getIshCashPayD() {
        return ishCashPayD;
    }

    /**
     * 设置大额理赔金额
     *
     * @param ishCashPayD 大额理赔金额
     */
    public void setIshCashPayD(BigDecimal ishCashPayD) {
        this.ishCashPayD = ishCashPayD;
    }

    /**
     * @return ISH_PUBFOUND_PAY
     */
    public BigDecimal getIshPubfoundPay() {
        return ishPubfoundPay;
    }

    /**
     * @param ishPubfoundPay
     */
    public void setIshPubfoundPay(BigDecimal ishPubfoundPay) {
        this.ishPubfoundPay = ishPubfoundPay;
    }

    /**
     * 获取统筹支付
     *
     * @return ISH_FOUND_PAY - 统筹支付
     */
    public BigDecimal getIshFoundPay() {
        return ishFoundPay;
    }

    /**
     * 设置统筹支付
     *
     * @param ishFoundPay 统筹支付
     */
    public void setIshFoundPay(BigDecimal ishFoundPay) {
        this.ishFoundPay = ishFoundPay;
    }

    /**
     * @return ISH_HELP_PAY
     */
    public BigDecimal getIshHelpPay() {
        return ishHelpPay;
    }

    /**
     * @param ishHelpPay
     */
    public void setIshHelpPay(BigDecimal ishHelpPay) {
        this.ishHelpPay = ishHelpPay;
    }

    /**
     * 获取现金支付
     *
     * @return ISH_CASH_PAY - 现金支付
     */
    public BigDecimal getIshCashPay() {
        return ishCashPay;
    }

    /**
     * 设置现金支付
     *
     * @param ishCashPay 现金支付
     */
    public void setIshCashPay(BigDecimal ishCashPay) {
        this.ishCashPay = ishCashPay;
    }

    /**
     * @return ISH_SOCIETY_HELP_PAY
     */
    public BigDecimal getIshSocietyHelpPay() {
        return ishSocietyHelpPay;
    }

    /**
     * @param ishSocietyHelpPay
     */
    public void setIshSocietyHelpPay(BigDecimal ishSocietyHelpPay) {
        this.ishSocietyHelpPay = ishSocietyHelpPay;
    }

    /**
     * @return ISH_SELF_BEFORE
     */
    public BigDecimal getIshSelfBefore() {
        return ishSelfBefore;
    }

    /**
     * @param ishSelfBefore
     */
    public void setIshSelfBefore(BigDecimal ishSelfBefore) {
        this.ishSelfBefore = ishSelfBefore;
    }

    /**
     * @return ISH_SELF_AFTER
     */
    public BigDecimal getIshSelfAfter() {
        return ishSelfAfter;
    }

    /**
     * @param ishSelfAfter
     */
    public void setIshSelfAfter(BigDecimal ishSelfAfter) {
        this.ishSelfAfter = ishSelfAfter;
    }

    /**
     * @return ISH_PUBLIC_BEFORE
     */
    public BigDecimal getIshPublicBefore() {
        return ishPublicBefore;
    }

    /**
     * @param ishPublicBefore
     */
    public void setIshPublicBefore(BigDecimal ishPublicBefore) {
        this.ishPublicBefore = ishPublicBefore;
    }

    /**
     * @return ISH_PUBLIC_AFTER
     */
    public BigDecimal getIshPublicAfter() {
        return ishPublicAfter;
    }

    /**
     * @param ishPublicAfter
     */
    public void setIshPublicAfter(BigDecimal ishPublicAfter) {
        this.ishPublicAfter = ishPublicAfter;
    }

    /**
     * 获取帐户余额
     *
     * @return ISH_PERSONAL_CURR_REMIAN - 帐户余额
     */
    public BigDecimal getIshPersonalCurrRemian() {
        return ishPersonalCurrRemian;
    }

    /**
     * 设置帐户余额
     *
     * @param ishPersonalCurrRemian 帐户余额
     */
    public void setIshPersonalCurrRemian(BigDecimal ishPersonalCurrRemian) {
        this.ishPersonalCurrRemian = ishPersonalCurrRemian;
    }

    /**
     * @return ISH_PERSONAL_TOTA_REMIAN
     */
    public BigDecimal getIshPersonalTotaRemian() {
        return ishPersonalTotaRemian;
    }

    /**
     * @param ishPersonalTotaRemian
     */
    public void setIshPersonalTotaRemian(BigDecimal ishPersonalTotaRemian) {
        this.ishPersonalTotaRemian = ishPersonalTotaRemian;
    }

    /**
     * 获取交易类型00是收款01退款
     *
     * @return ISH_TX_CODE - 交易类型00是收款01退款
     */
    public String getIshTxCode() {
        return ishTxCode;
    }

    /**
     * 设置交易类型00是收款01退款
     *
     * @param ishTxCode 交易类型00是收款01退款
     */
    public void setIshTxCode(String ishTxCode) {
        this.ishTxCode = ishTxCode;
    }

    /**
     * 获取民族
     *
     * @return ISH_CARD_INFORM - 民族
     */
    public String getIshCardInform() {
        return ishCardInform;
    }

    /**
     * 设置民族
     *
     * @param ishCardInform 民族
     */
    public void setIshCardInform(String ishCardInform) {
        this.ishCardInform = ishCardInform;
    }

    /**
     * 获取医疗:参保类别/工伤:参保状态/生育:
     *
     * @return ISH_CARD_TYPE - 医疗:参保类别/工伤:参保状态/生育:
     */
    public String getIshCardType() {
        return ishCardType;
    }

    /**
     * 设置医疗:参保类别/工伤:参保状态/生育:
     *
     * @param ishCardType 医疗:参保类别/工伤:参保状态/生育:
     */
    public void setIshCardType(String ishCardType) {
        this.ishCardType = ishCardType;
    }

    /**
     * 获取社保卡卡号(社会保障识别号)
     *
     * @return ISH_YBH - 社保卡卡号(社会保障识别号)
     */
    public String getIshYbh() {
        return ishYbh;
    }

    /**
     * 设置社保卡卡号(社会保障识别号)
     *
     * @param ishYbh 社保卡卡号(社会保障识别号)
     */
    public void setIshYbh(String ishYbh) {
        this.ishYbh = ishYbh;
    }

    /**
     * 获取姓名
     *
     * @return ISH_XM - 姓名
     */
    public String getIshXm() {
        return ishXm;
    }

    /**
     * 设置姓名
     *
     * @param ishXm 姓名
     */
    public void setIshXm(String ishXm) {
        this.ishXm = ishXm;
    }

    /**
     * 获取医院id
     *
     * @return ISH_YYID - 医院id
     */
    public String getIshYyid() {
        return ishYyid;
    }

    /**
     * 设置医院id
     *
     * @param ishYyid 医院id
     */
    public void setIshYyid(String ishYyid) {
        this.ishYyid = ishYyid;
    }

    /**
     * 获取就诊流水号（住院（或门诊）号）
     *
     * @return ISH_JZBH - 就诊流水号（住院（或门诊）号）
     */
    public String getIshJzbh() {
        return ishJzbh;
    }

    /**
     * 设置就诊流水号（住院（或门诊）号）
     *
     * @param ishJzbh 就诊流水号（住院（或门诊）号）
     */
    public void setIshJzbh(String ishJzbh) {
        this.ishJzbh = ishJzbh;
    }

    /**
     * 获取医生
     *
     * @return ISH_DOCTOR - 医生
     */
    public String getIshDoctor() {
        return ishDoctor;
    }

    /**
     * 设置医生
     *
     * @param ishDoctor 医生
     */
    public void setIshDoctor(String ishDoctor) {
        this.ishDoctor = ishDoctor;
    }

    /**
     * 获取城区号(六城区离休和子女) 杭州/药师编码
     *
     * @return ISH_YSID - 城区号(六城区离休和子女) 杭州/药师编码
     */
    public String getIshYsid() {
        return ishYsid;
    }

    /**
     * 设置城区号(六城区离休和子女) 杭州/药师编码
     *
     * @param ishYsid 城区号(六城区离休和子女) 杭州/药师编码
     */
    public void setIshYsid(String ishYsid) {
        this.ishYsid = ishYsid;
    }

    /**
     * 获取原销售单退货标志
     *
     * @return ISH_THFLAG - 原销售单退货标志
     */
    public String getIshThflag() {
        return ishThflag;
    }

    /**
     * 设置原销售单退货标志
     *
     * @param ishThflag 原销售单退货标志
     */
    public void setIshThflag(String ishThflag) {
        this.ishThflag = ishThflag;
    }

    /**
     * 获取险种类别
     *
     * @return ISH_STATUS - 险种类别
     */
    public String getIshStatus() {
        return ishStatus;
    }

    /**
     * 设置险种类别
     *
     * @param ishStatus 险种类别
     */
    public void setIshStatus(String ishStatus) {
        this.ishStatus = ishStatus;
    }

    /**
     * 获取是否享受公务员待遇
     *
     * @return ISH_GWY_FLAG - 是否享受公务员待遇
     */
    public String getIshGwyFlag() {
        return ishGwyFlag;
    }

    /**
     * 设置是否享受公务员待遇
     *
     * @param ishGwyFlag 是否享受公务员待遇
     */
    public void setIshGwyFlag(String ishGwyFlag) {
        this.ishGwyFlag = ishGwyFlag;
    }

    /**
     * 获取行政区划编码
     *
     * @return ISH_XZQH_ID - 行政区划编码
     */
    public String getIshXzqhId() {
        return ishXzqhId;
    }

    /**
     * 设置行政区划编码
     *
     * @param ishXzqhId 行政区划编码
     */
    public void setIshXzqhId(String ishXzqhId) {
        this.ishXzqhId = ishXzqhId;
    }

    /**
     * 获取封锁状况
     *
     * @return ISH_FSZK - 封锁状况
     */
    public String getIshFszk() {
        return ishFszk;
    }

    /**
     * 设置封锁状况
     *
     * @param ishFszk 封锁状况
     */
    public void setIshFszk(String ishFszk) {
        this.ishFszk = ishFszk;
    }

    /**
     * 获取封锁原因
     *
     * @return ISH_FSYY - 封锁原因
     */
    public String getIshFsyy() {
        return ishFsyy;
    }

    /**
     * 设置封锁原因
     *
     * @param ishFsyy 封锁原因
     */
    public void setIshFsyy(String ishFsyy) {
        this.ishFsyy = ishFsyy;
    }

    /**
     * 获取人员变更类型
     *
     * @return ISH_RYBG - 人员变更类型
     */
    public String getIshRybg() {
        return ishRybg;
    }

    /**
     * 设置人员变更类型
     *
     * @param ishRybg 人员变更类型
     */
    public void setIshRybg(String ishRybg) {
        this.ishRybg = ishRybg;
    }

    /**
     * 获取人员类型变更时间
     *
     * @return ISH_RYBG_DATE - 人员类型变更时间
     */
    public Date getIshRybgDate() {
        return ishRybgDate;
    }

    /**
     * 设置人员类型变更时间
     *
     * @param ishRybgDate 人员类型变更时间
     */
    public void setIshRybgDate(Date ishRybgDate) {
        this.ishRybgDate = ishRybgDate;
    }

    /**
     * 获取医疗,工伤：待遇封锁开始时间/生育: 享受待遇实际开始日期
     *
     * @return ISH_DYFS_BEGIN - 医疗,工伤：待遇封锁开始时间/生育: 享受待遇实际开始日期
     */
    public Date getIshDyfsBegin() {
        return ishDyfsBegin;
    }

    /**
     * 设置医疗,工伤：待遇封锁开始时间/生育: 享受待遇实际开始日期
     *
     * @param ishDyfsBegin 医疗,工伤：待遇封锁开始时间/生育: 享受待遇实际开始日期
     */
    public void setIshDyfsBegin(Date ishDyfsBegin) {
        this.ishDyfsBegin = ishDyfsBegin;
    }

    /**
     * 获取医疗,工伤：待遇封锁终止时间/生育: 享受待遇实际开始日期
     *
     * @return ISH_DYFS_END - 医疗,工伤：待遇封锁终止时间/生育: 享受待遇实际开始日期
     */
    public Date getIshDyfsEnd() {
        return ishDyfsEnd;
    }

    /**
     * 设置医疗,工伤：待遇封锁终止时间/生育: 享受待遇实际开始日期
     *
     * @param ishDyfsEnd 医疗,工伤：待遇封锁终止时间/生育: 享受待遇实际开始日期
     */
    public void setIshDyfsEnd(Date ishDyfsEnd) {
        this.ishDyfsEnd = ishDyfsEnd;
    }

    /**
     * 获取民政人员类别
     *
     * @return ISH_MZRY_TYPE - 民政人员类别
     */
    public String getIshMzryType() {
        return ishMzryType;
    }

    /**
     * 设置民政人员类别
     *
     * @param ishMzryType 民政人员类别
     */
    public void setIshMzryType(String ishMzryType) {
        this.ishMzryType = ishMzryType;
    }

    /**
     * 获取居民缴费档次
     *
     * @return ISH_JMJFDC - 居民缴费档次
     */
    public String getIshJmjfdc() {
        return ishJmjfdc;
    }

    /**
     * 设置居民缴费档次
     *
     * @param ishJmjfdc 居民缴费档次
     */
    public void setIshJmjfdc(String ishJmjfdc) {
        this.ishJmjfdc = ishJmjfdc;
    }

    /**
     * 获取患者联系电话
     *
     * @return ISH_HZLXDH - 患者联系电话
     */
    public String getIshHzlxdh() {
        return ishHzlxdh;
    }

    /**
     * 设置患者联系电话
     *
     * @param ishHzlxdh 患者联系电话
     */
    public void setIshHzlxdh(String ishHzlxdh) {
        this.ishHzlxdh = ishHzlxdh;
    }

    /**
     * 获取当前扶贫人员类别
     *
     * @return ISH_FPRY_TYPE - 当前扶贫人员类别
     */
    public String getIshFpryType() {
        return ishFpryType;
    }

    /**
     * 设置当前扶贫人员类别
     *
     * @param ishFpryType 当前扶贫人员类别
     */
    public void setIshFpryType(String ishFpryType) {
        this.ishFpryType = ishFpryType;
    }

    /**
     * 获取工伤:工伤参保时间/生育:生育参保时间
     *
     * @return ISH_GSCA_DATE - 工伤:工伤参保时间/生育:生育参保时间
     */
    public Date getIshGscaDate() {
        return ishGscaDate;
    }

    /**
     * 设置工伤:工伤参保时间/生育:生育参保时间
     *
     * @param ishGscaDate 工伤:工伤参保时间/生育:生育参保时间
     */
    public void setIshGscaDate(Date ishGscaDate) {
        this.ishGscaDate = ishGscaDate;
    }

    /**
     * 获取工伤:转院转诊/生育:不能享受就诊原因
     *
     * @return ISH_ZYZZ - 工伤:转院转诊/生育:不能享受就诊原因
     */
    public String getIshZyzz() {
        return ishZyzz;
    }

    /**
     * 设置工伤:转院转诊/生育:不能享受就诊原因
     *
     * @param ishZyzz 工伤:转院转诊/生育:不能享受就诊原因
     */
    public void setIshZyzz(String ishZyzz) {
        this.ishZyzz = ishZyzz;
    }

    /**
     * 获取工伤:辅助器具超标审批/生育:可否享受就诊标志
     *
     * @return ISH_FZQJCBSP - 工伤:辅助器具超标审批/生育:可否享受就诊标志
     */
    public String getIshFzqjcbsp() {
        return ishFzqjcbsp;
    }

    /**
     * 设置工伤:辅助器具超标审批/生育:可否享受就诊标志
     *
     * @param ishFzqjcbsp 工伤:辅助器具超标审批/生育:可否享受就诊标志
     */
    public void setIshFzqjcbsp(String ishFzqjcbsp) {
        this.ishFzqjcbsp = ishFzqjcbsp;
    }

    /**
     * 获取就医登记机构编码
     *
     * @return ISH_JZYLJG_ID - 就医登记机构编码
     */
    public String getIshJzyljgId() {
        return ishJzyljgId;
    }

    /**
     * 设置就医登记机构编码
     *
     * @param ishJzyljgId 就医登记机构编码
     */
    public void setIshJzyljgId(String ishJzyljgId) {
        this.ishJzyljgId = ishJzyljgId;
    }

    /**
     * 获取并发症标志
     *
     * @return ISH_BFZ_FLAG - 并发症标志
     */
    public String getIshBfzFlag() {
        return ishBfzFlag;
    }

    /**
     * 设置并发症标志
     *
     * @param ishBfzFlag 并发症标志
     */
    public void setIshBfzFlag(String ishBfzFlag) {
        this.ishBfzFlag = ishBfzFlag;
    }

    /**
     * 获取 
     *
     * @return ISH_BILL_SERIAL -  
     */
    public String getIshBillSerial() {
        return ishBillSerial;
    }

    /**
     * 设置 
     *
     * @param ishBillSerial  
     */
    public void setIshBillSerial(String ishBillSerial) {
        this.ishBillSerial = ishBillSerial;
    }

    /**
     * 获取性别
     *
     * @return ISH_ENTERPRISE_CHARACTER - 性别
     */
    public String getIshEnterpriseCharacter() {
        return ishEnterpriseCharacter;
    }

    /**
     * 设置性别
     *
     * @param ishEnterpriseCharacter 性别
     */
    public void setIshEnterpriseCharacter(String ishEnterpriseCharacter) {
        this.ishEnterpriseCharacter = ishEnterpriseCharacter;
    }

    /**
     * 获取人员类别
     *
     * @return ISH_PERSONAL_CLASS - 人员类别
     */
    public String getIshPersonalClass() {
        return ishPersonalClass;
    }

    /**
     * 设置人员类别
     *
     * @param ishPersonalClass 人员类别
     */
    public void setIshPersonalClass(String ishPersonalClass) {
        this.ishPersonalClass = ishPersonalClass;
    }

    /**
     * 获取单位名称
     *
     * @return ISH_PERSONAL_CHARACTER - 单位名称
     */
    public String getIshPersonalCharacter() {
        return ishPersonalCharacter;
    }

    /**
     * 设置单位名称
     *
     * @param ishPersonalCharacter 单位名称
     */
    public void setIshPersonalCharacter(String ishPersonalCharacter) {
        this.ishPersonalCharacter = ishPersonalCharacter;
    }

    /**
     * 获取历史起付线公务员返 还
     *
     * @return ISH_QFZF_AMT - 历史起付线公务员返 还
     */
    public BigDecimal getIshQfzfAmt() {
        return ishQfzfAmt;
    }

    /**
     * 设置历史起付线公务员返 还
     *
     * @param ishQfzfAmt 历史起付线公务员返 还
     */
    public void setIshQfzfAmt(BigDecimal ishQfzfAmt) {
        this.ishQfzfAmt = ishQfzfAmt;
    }

    /**
     * 获取单病种定点医疗机构 垫支
     *
     * @return ISH_QFBZACCOUNT_AMT - 单病种定点医疗机构 垫支
     */
    public BigDecimal getIshQfbzaccountAmt() {
        return ishQfbzaccountAmt;
    }

    /**
     * 设置单病种定点医疗机构 垫支
     *
     * @param ishQfbzaccountAmt 单病种定点医疗机构 垫支
     */
    public void setIshQfbzaccountAmt(BigDecimal ishQfbzaccountAmt) {
        this.ishQfbzaccountAmt = ishQfbzaccountAmt;
    }

    /**
     * 获取民政救助金额
     *
     * @return ISH_QFBZCASH_AMT - 民政救助金额
     */
    public BigDecimal getIshQfbzcashAmt() {
        return ishQfbzcashAmt;
    }

    /**
     * 设置民政救助金额
     *
     * @param ishQfbzcashAmt 民政救助金额
     */
    public void setIshQfbzcashAmt(BigDecimal ishQfbzcashAmt) {
        this.ishQfbzcashAmt = ishQfbzcashAmt;
    }

    /**
     * 获取民政救助门诊余额
     *
     * @return ISH_FDZL_AMTF - 民政救助门诊余额
     */
    public BigDecimal getIshFdzlAmtf() {
        return ishFdzlAmtf;
    }

    /**
     * 设置民政救助门诊余额
     *
     * @param ishFdzlAmtf 民政救助门诊余额
     */
    public void setIshFdzlAmtf(BigDecimal ishFdzlAmtf) {
        this.ishFdzlAmtf = ishFdzlAmtf;
    }

    /**
     * 获取耐多药项目支付金额
     *
     * @return ISH_QFYSACCOUNT_AMT - 耐多药项目支付金额
     */
    public BigDecimal getIshQfysaccountAmt() {
        return ishQfysaccountAmt;
    }

    /**
     * 设置耐多药项目支付金额
     *
     * @param ishQfysaccountAmt 耐多药项目支付金额
     */
    public void setIshQfysaccountAmt(BigDecimal ishQfysaccountAmt) {
        this.ishQfysaccountAmt = ishQfysaccountAmt;
    }

    /**
     * 获取一般诊疗支付数
     *
     * @return ISH_QFYSCASH_AMT - 一般诊疗支付数
     */
    public BigDecimal getIshQfyscashAmt() {
        return ishQfyscashAmt;
    }

    /**
     * 设置一般诊疗支付数
     *
     * @param ishQfyscashAmt 一般诊疗支付数
     */
    public void setIshQfyscashAmt(BigDecimal ishQfyscashAmt) {
        this.ishQfyscashAmt = ishQfyscashAmt;
    }

    /**
     * 获取神华救助基金支付数
     *
     * @return ISH_CGFDXGRZF_AMT - 神华救助基金支付数
     */
    public BigDecimal getIshCgfdxgrzfAmt() {
        return ishCgfdxgrzfAmt;
    }

    /**
     * 设置神华救助基金支付数
     *
     * @param ishCgfdxgrzfAmt 神华救助基金支付数
     */
    public void setIshCgfdxgrzfAmt(BigDecimal ishCgfdxgrzfAmt) {
        this.ishCgfdxgrzfAmt = ishCgfdxgrzfAmt;
    }

    /**
     * 获取本年统筹支付累计
     *
     * @return ISH_ZLCWF_AMT - 本年统筹支付累计
     */
    public BigDecimal getIshZlcwfAmt() {
        return ishZlcwfAmt;
    }

    /**
     * 设置本年统筹支付累计
     *
     * @param ishZlcwfAmt 本年统筹支付累计
     */
    public void setIshZlcwfAmt(BigDecimal ishZlcwfAmt) {
        this.ishZlcwfAmt = ishZlcwfAmt;
    }

    /**
     * 获取本年大额支付累计
     *
     * @return ISH_GWYBCMZQFX_AMT - 本年大额支付累计
     */
    public BigDecimal getIshGwybcmzqfxAmt() {
        return ishGwybcmzqfxAmt;
    }

    /**
     * 设置本年大额支付累计
     *
     * @param ishGwybcmzqfxAmt 本年大额支付累计
     */
    public void setIshGwybcmzqfxAmt(BigDecimal ishGwybcmzqfxAmt) {
        this.ishGwybcmzqfxAmt = ishGwybcmzqfxAmt;
    }

    /**
     * 获取特病起付线支付累计
     *
     * @return ISH_GWYMZQFXLNZH_AMT - 特病起付线支付累计
     */
    public BigDecimal getIshGwymzqfxlnzhAmt() {
        return ishGwymzqfxlnzhAmt;
    }

    /**
     * 设置特病起付线支付累计
     *
     * @param ishGwymzqfxlnzhAmt 特病起付线支付累计
     */
    public void setIshGwymzqfxlnzhAmt(BigDecimal ishGwymzqfxlnzhAmt) {
        this.ishGwymzqfxlnzhAmt = ishGwymzqfxlnzhAmt;
    }

    /**
     * 获取耐多药项目累计
     *
     * @return ISH_GWYMZFDZF_AMT - 耐多药项目累计
     */
    public BigDecimal getIshGwymzfdzfAmt() {
        return ishGwymzfdzfAmt;
    }

    /**
     * 设置耐多药项目累计
     *
     * @param ishGwymzfdzfAmt 耐多药项目累计
     */
    public void setIshGwymzfdzfAmt(BigDecimal ishGwymzfdzfAmt) {
        this.ishGwymzfdzfAmt = ishGwymzfdzfAmt;
    }

    /**
     * 获取本年民政救助住院支 付累计
     *
     * @return ISH_FHJBYLBXFY_AMT - 本年民政救助住院支 付累计
     */
    public BigDecimal getIshFhjbylbxfyAmt() {
        return ishFhjbylbxfyAmt;
    }

    /**
     * 设置本年民政救助住院支 付累计
     *
     * @param ishFhjbylbxfyAmt 本年民政救助住院支 付累计
     */
    public void setIshFhjbylbxfyAmt(BigDecimal ishFhjbylbxfyAmt) {
        this.ishFhjbylbxfyAmt = ishFhjbylbxfyAmt;
    }

    /**
     * 获取本次起付线支付金额
     *
     * @return ISH_LXRYJJ_AMT - 本次起付线支付金额
     */
    public BigDecimal getIshLxryjjAmt() {
        return ishLxryjjAmt;
    }

    /**
     * 设置本次起付线支付金额
     *
     * @param ishLxryjjAmt 本次起付线支付金额
     */
    public void setIshLxryjjAmt(BigDecimal ishLxryjjAmt) {
        this.ishLxryjjAmt = ishLxryjjAmt;
    }

    /**
     * 获取本次进入医保范围费 用
     *
     * @return ISH_TXMZTCJJ_AMT - 本次进入医保范围费 用
     */
    public BigDecimal getIshTxmztcjjAmt() {
        return ishTxmztcjjAmt;
    }

    /**
     * 设置本次进入医保范围费 用
     *
     * @param ishTxmztcjjAmt 本次进入医保范围费 用
     */
    public void setIshTxmztcjjAmt(BigDecimal ishTxmztcjjAmt) {
        this.ishTxmztcjjAmt = ishTxmztcjjAmt;
    }

    /**
     * 获取药事服务支付数
     *
     * @return ISH_ZNTCJJ_AMT - 药事服务支付数
     */
    public BigDecimal getIshZntcjjAmt() {
        return ishZntcjjAmt;
    }

    /**
     * 设置药事服务支付数
     *
     * @param ishZntcjjAmt 药事服务支付数
     */
    public void setIshZntcjjAmt(BigDecimal ishZntcjjAmt) {
        this.ishZntcjjAmt = ishZntcjjAmt;
    }

    /**
     * 获取医院超标扣款金额
     *
     * @return ISH_LFJJ_AMT - 医院超标扣款金额
     */
    public BigDecimal getIshLfjjAmt() {
        return ishLfjjAmt;
    }

    /**
     * 设置医院超标扣款金额
     *
     * @param ishLfjjAmt 医院超标扣款金额
     */
    public void setIshLfjjAmt(BigDecimal ishLfjjAmt) {
        this.ishLfjjAmt = ishLfjjAmt;
    }

    /**
     * 获取 实足年龄
     *
     * @return ISH_LXJSJJ_AMT -  实足年龄
     */
    public Short getIshLxjsjjAmt() {
        return ishLxjsjjAmt;
    }

    /**
     * 设置 实足年龄
     *
     * @param ishLxjsjjAmt  实足年龄
     */
    public void setIshLxjsjjAmt(Short ishLxjsjjAmt) {
        this.ishLxjsjjAmt = ishLxjsjjAmt;
    }

    /**
     * 获取生育基金支付
     *
     * @return ISH_JSQZHYE_AMT - 生育基金支付
     */
    public BigDecimal getIshJsqzhyeAmt() {
        return ishJsqzhyeAmt;
    }

    /**
     * 设置生育基金支付
     *
     * @param ishJsqzhyeAmt 生育基金支付
     */
    public void setIshJsqzhyeAmt(BigDecimal ishJsqzhyeAmt) {
        this.ishJsqzhyeAmt = ishJsqzhyeAmt;
    }

    /**
     * 获取生育现金支付
     *
     * @return ISH_JSQBNZHYE_AMT - 生育现金支付
     */
    public BigDecimal getIshJsqbnzhyeAmt() {
        return ishJsqbnzhyeAmt;
    }

    /**
     * 设置生育现金支付
     *
     * @param ishJsqbnzhyeAmt 生育现金支付
     */
    public void setIshJsqbnzhyeAmt(BigDecimal ishJsqbnzhyeAmt) {
        this.ishJsqbnzhyeAmt = ishJsqbnzhyeAmt;
    }

    /**
     * 获取工伤基金支付
     *
     * @return ISH_JSQLNZHYE_AMT - 工伤基金支付
     */
    public BigDecimal getIshJsqlnzhyeAmt() {
        return ishJsqlnzhyeAmt;
    }

    /**
     * 设置工伤基金支付
     *
     * @param ishJsqlnzhyeAmt 工伤基金支付
     */
    public void setIshJsqlnzhyeAmt(BigDecimal ishJsqlnzhyeAmt) {
        this.ishJsqlnzhyeAmt = ishJsqlnzhyeAmt;
    }

    /**
     * 获取工伤现金支付
     *
     * @return ISH_JSHZHYE_AMT - 工伤现金支付
     */
    public BigDecimal getIshJshzhyeAmt() {
        return ishJshzhyeAmt;
    }

    /**
     * 设置工伤现金支付
     *
     * @param ishJshzhyeAmt 工伤现金支付
     */
    public void setIshJshzhyeAmt(BigDecimal ishJshzhyeAmt) {
        this.ishJshzhyeAmt = ishJshzhyeAmt;
    }

    /**
     * @return ISH_KNJZZJ_AMT
     */
    public BigDecimal getIshKnjzzjAmt() {
        return ishKnjzzjAmt;
    }

    /**
     * @param ishKnjzzjAmt
     */
    public void setIshKnjzzjAmt(BigDecimal ishKnjzzjAmt) {
        this.ishKnjzzjAmt = ishKnjzzjAmt;
    }

    /**
     * 获取工伤单病种机构垫支
     *
     * @return ISH_QYZZMZTCJJ_AMT - 工伤单病种机构垫支
     */
    public BigDecimal getIshQyzzmztcjjAmt() {
        return ishQyzzmztcjjAmt;
    }

    /**
     * 设置工伤单病种机构垫支
     *
     * @param ishQyzzmztcjjAmt 工伤单病种机构垫支
     */
    public void setIshQyzzmztcjjAmt(BigDecimal ishQyzzmztcjjAmt) {
        this.ishQyzzmztcjjAmt = ishQyzzmztcjjAmt;
    }

    /**
     * 获取其他补助
     *
     * @return ISH_LJMJJ_AMT - 其他补助
     */
    public BigDecimal getIshLjmjjAmt() {
        return ishLjmjjAmt;
    }

    /**
     * 设置其他补助
     *
     * @param ishLjmjjAmt 其他补助
     */
    public void setIshLjmjjAmt(BigDecimal ishLjmjjAmt) {
        this.ishLjmjjAmt = ishLjmjjAmt;
    }

    /**
     * 获取生育账户支付
     *
     * @return ISH_SNETJJ_AMT - 生育账户支付
     */
    public BigDecimal getIshSnetjjAmt() {
        return ishSnetjjAmt;
    }

    /**
     * 设置生育账户支付
     *
     * @param ishSnetjjAmt 生育账户支付
     */
    public void setIshSnetjjAmt(BigDecimal ishSnetjjAmt) {
        this.ishSnetjjAmt = ishSnetjjAmt;
    }

    /**
     * 获取工伤账户支付
     *
     * @return ISH_NMGJJ_AMT - 工伤账户支付
     */
    public BigDecimal getIshNmgjjAmt() {
        return ishNmgjjAmt;
    }

    /**
     * 设置工伤账户支付
     *
     * @param ishNmgjjAmt 工伤账户支付
     */
    public void setIshNmgjjAmt(BigDecimal ishNmgjjAmt) {
        this.ishNmgjjAmt = ishNmgjjAmt;
    }

    /**
     * 获取健康扶贫医疗基金
     *
     * @return ISH_NMGLJJF_MONTHS - 健康扶贫医疗基金
     */
    public BigDecimal getIshNmgljjfMonths() {
        return ishNmgljjfMonths;
    }

    /**
     * 设置健康扶贫医疗基金
     *
     * @param ishNmgljjfMonths 健康扶贫医疗基金
     */
    public void setIshNmgljjfMonths(BigDecimal ishNmgljjfMonths) {
        this.ishNmgljjfMonths = ishNmgljjfMonths;
    }

    /**
     * 获取住院次数
     *
     * @return ISH_JTBC_DAYS - 住院次数
     */
    public Long getIshJtbcDays() {
        return ishJtbcDays;
    }

    /**
     * 设置住院次数
     *
     * @param ishJtbcDays 住院次数
     */
    public void setIshJtbcDays(Long ishJtbcDays) {
        this.ishJtbcDays = ishJtbcDays;
    }

    /**
     * 获取工伤个人编号|
     *
     * @return ISH_MZREGISTER_ID - 工伤个人编号|
     */
    public String getIshMzregisterId() {
        return ishMzregisterId;
    }

    /**
     * 设置工伤个人编号|
     *
     * @param ishMzregisterId 工伤个人编号|
     */
    public void setIshMzregisterId(String ishMzregisterId) {
        this.ishMzregisterId = ishMzregisterId;
    }

    /**
     * 获取工伤单位编号
     *
     * @return ISH_ENTERPRISE_ID - 工伤单位编号
     */
    public String getIshEnterpriseId() {
        return ishEnterpriseId;
    }

    /**
     * 设置工伤单位编号
     *
     * @param ishEnterpriseId 工伤单位编号
     */
    public void setIshEnterpriseId(String ishEnterpriseId) {
        this.ishEnterpriseId = ishEnterpriseId;
    }

    /**
     * 获取身份证号   
     *
     * @return ISH_PERSONAL_ID - 身份证号   
     */
    public String getIshPersonalId() {
        return ishPersonalId;
    }

    /**
     * 设置身份证号   
     *
     * @param ishPersonalId 身份证号   
     */
    public void setIshPersonalId(String ishPersonalId) {
        this.ishPersonalId = ishPersonalId;
    }

    /**
     * 获取住址
     *
     * @return ISH_PERSONAL - 住址
     */
    public String getIshPersonal() {
        return ishPersonal;
    }

    /**
     * 设置住址
     *
     * @param ishPersonal 住址
     */
    public void setIshPersonal(String ishPersonal) {
        this.ishPersonal = ishPersonal;
    }

    /**
     * 获取精准脱贫保险金额
     *
     * @return ISH_MZQFBZLJ - 精准脱贫保险金额
     */
    public BigDecimal getIshMzqfbzlj() {
        return ishMzqfbzlj;
    }

    /**
     * 设置精准脱贫保险金额
     *
     * @param ishMzqfbzlj 精准脱贫保险金额
     */
    public void setIshMzqfbzlj(BigDecimal ishMzqfbzlj) {
        this.ishMzqfbzlj = ishMzqfbzlj;
    }

    /**
     * 获取其他扶贫报销金额
     *
     * @return ISH_GFKZPEJJ_AMT - 其他扶贫报销金额
     */
    public BigDecimal getIshGfkzpejjAmt() {
        return ishGfkzpejjAmt;
    }

    /**
     * 设置其他扶贫报销金额
     *
     * @param ishGfkzpejjAmt 其他扶贫报销金额
     */
    public void setIshGfkzpejjAmt(BigDecimal ishGfkzpejjAmt) {
        this.ishGfkzpejjAmt = ishGfkzpejjAmt;
    }

    /**
     * @return ISH_GFJFPEJJ_AMT
     */
    public BigDecimal getIshGfjfpejjAmt() {
        return ishGfjfpejjAmt;
    }

    /**
     * @param ishGfjfpejjAmt
     */
    public void setIshGfjfpejjAmt(BigDecimal ishGfjfpejjAmt) {
        this.ishGfjfpejjAmt = ishGfjfpejjAmt;
    }

    /**
     * @return ISH_LFKZPEJJ_AMT
     */
    public BigDecimal getIshLfkzpejjAmt() {
        return ishLfkzpejjAmt;
    }

    /**
     * @param ishLfkzpejjAmt
     */
    public void setIshLfkzpejjAmt(BigDecimal ishLfkzpejjAmt) {
        this.ishLfkzpejjAmt = ishLfkzpejjAmt;
    }

    /**
     * @return ISH_LFJFPEJJ_AMT
     */
    public BigDecimal getIshLfjfpejjAmt() {
        return ishLfjfpejjAmt;
    }

    /**
     * @param ishLfjfpejjAmt
     */
    public void setIshLfjfpejjAmt(BigDecimal ishLfjfpejjAmt) {
        this.ishLfjfpejjAmt = ishLfjfpejjAmt;
    }

    /**
     * @return ISH_FSJJ_AMT
     */
    public BigDecimal getIshFsjjAmt() {
        return ishFsjjAmt;
    }

    /**
     * @param ishFsjjAmt
     */
    public void setIshFsjjAmt(BigDecimal ishFsjjAmt) {
        this.ishFsjjAmt = ishFsjjAmt;
    }

    /**
     * @return ISH_FSJJJ_AMT
     */
    public BigDecimal getIshFsjjjAmt() {
        return ishFsjjjAmt;
    }

    /**
     * @param ishFsjjjAmt
     */
    public void setIshFsjjjAmt(BigDecimal ishFsjjjAmt) {
        this.ishFsjjjAmt = ishFsjjjAmt;
    }

    /**
     * @return ISH_FTJJJ_AMT
     */
    public BigDecimal getIshFtjjjAmt() {
        return ishFtjjjAmt;
    }

    /**
     * @param ishFtjjjAmt
     */
    public void setIshFtjjjAmt(BigDecimal ishFtjjjAmt) {
        this.ishFtjjjAmt = ishFtjjjAmt;
    }

    /**
     * @return ISH_FJJJJ_AMT
     */
    public BigDecimal getIshFjjjjAmt() {
        return ishFjjjjAmt;
    }

    /**
     * @param ishFjjjjAmt
     */
    public void setIshFjjjjAmt(BigDecimal ishFjjjjAmt) {
        this.ishFjjjjAmt = ishFjjjjAmt;
    }

    /**
     * @return ISH_FCYXJJ_AMT
     */
    public BigDecimal getIshFcyxjjAmt() {
        return ishFcyxjjAmt;
    }

    /**
     * @param ishFcyxjjAmt
     */
    public void setIshFcyxjjAmt(BigDecimal ishFcyxjjAmt) {
        this.ishFcyxjjAmt = ishFcyxjjAmt;
    }

    /**
     * 获取中心结算时间
     *
     * @return ISH_JYDATE - 中心结算时间
     */
    public Date getIshJydate() {
        return ishJydate;
    }

    /**
     * 设置中心结算时间
     *
     * @param ishJydate 中心结算时间
     */
    public void setIshJydate(Date ishJydate) {
        this.ishJydate = ishJydate;
    }

    /**
     * @return ISH_CQZNJJ_AMT
     */
    public BigDecimal getIshCqznjjAmt() {
        return ishCqznjjAmt;
    }

    /**
     * @param ishCqznjjAmt
     */
    public void setIshCqznjjAmt(BigDecimal ishCqznjjAmt) {
        this.ishCqznjjAmt = ishCqznjjAmt;
    }

    /**
     * @return ISH_CQGFLXJJ_AMT
     */
    public BigDecimal getIshCqgflxjjAmt() {
        return ishCqgflxjjAmt;
    }

    /**
     * @param ishCqgflxjjAmt
     */
    public void setIshCqgflxjjAmt(BigDecimal ishCqgflxjjAmt) {
        this.ishCqgflxjjAmt = ishCqgflxjjAmt;
    }

    /**
     * @return ISH_CQLFJJ_AMT
     */
    public BigDecimal getIshCqlfjjAmt() {
        return ishCqlfjjAmt;
    }

    /**
     * @param ishCqlfjjAmt
     */
    public void setIshCqlfjjAmt(BigDecimal ishCqlfjjAmt) {
        this.ishCqlfjjAmt = ishCqlfjjAmt;
    }

    /**
     * @return ISH_XNHJJ_AMT
     */
    public BigDecimal getIshXnhjjAmt() {
        return ishXnhjjAmt;
    }

    /**
     * @param ishXnhjjAmt
     */
    public void setIshXnhjjAmt(BigDecimal ishXnhjjAmt) {
        this.ishXnhjjAmt = ishXnhjjAmt;
    }

    /**
     * @return ISH_STRUC1
     */
    public String getIshStruc1() {
        return ishStruc1;
    }

    /**
     * @param ishStruc1
     */
    public void setIshStruc1(String ishStruc1) {
        this.ishStruc1 = ishStruc1;
    }

    /**
     * @return ISH_TJJJ_AMT
     */
    public BigDecimal getIshTjjjAmt() {
        return ishTjjjAmt;
    }

    /**
     * @param ishTjjjAmt
     */
    public void setIshTjjjAmt(BigDecimal ishTjjjAmt) {
        this.ishTjjjAmt = ishTjjjAmt;
    }

    /**
     * @return ISH_DXSJJ_AMT
     */
    public BigDecimal getIshDxsjjAmt() {
        return ishDxsjjAmt;
    }

    /**
     * @param ishDxsjjAmt
     */
    public void setIshDxsjjAmt(BigDecimal ishDxsjjAmt) {
        this.ishDxsjjAmt = ishDxsjjAmt;
    }

    /**
     * @return ISH_AMT1
     */
    public BigDecimal getIshAmt1() {
        return ishAmt1;
    }

    /**
     * @param ishAmt1
     */
    public void setIshAmt1(BigDecimal ishAmt1) {
        this.ishAmt1 = ishAmt1;
    }

    /**
     * @return ISH_AMT2
     */
    public BigDecimal getIshAmt2() {
        return ishAmt2;
    }

    /**
     * @param ishAmt2
     */
    public void setIshAmt2(BigDecimal ishAmt2) {
        this.ishAmt2 = ishAmt2;
    }

    /**
     * @return ISH_AMT3
     */
    public BigDecimal getIshAmt3() {
        return ishAmt3;
    }

    /**
     * @param ishAmt3
     */
    public void setIshAmt3(BigDecimal ishAmt3) {
        this.ishAmt3 = ishAmt3;
    }

    /**
     * @return ISH_AMT4
     */
    public BigDecimal getIshAmt4() {
        return ishAmt4;
    }

    /**
     * @param ishAmt4
     */
    public void setIshAmt4(BigDecimal ishAmt4) {
        this.ishAmt4 = ishAmt4;
    }

    /**
     * @return ISH_AMT5
     */
    public BigDecimal getIshAmt5() {
        return ishAmt5;
    }

    /**
     * @param ishAmt5
     */
    public void setIshAmt5(BigDecimal ishAmt5) {
        this.ishAmt5 = ishAmt5;
    }

    /**
     * @return ISH_AMT6
     */
    public BigDecimal getIshAmt6() {
        return ishAmt6;
    }

    /**
     * @param ishAmt6
     */
    public void setIshAmt6(BigDecimal ishAmt6) {
        this.ishAmt6 = ishAmt6;
    }

    /**
     * @return ISH_STR1
     */
    public String getIshStr1() {
        return ishStr1;
    }

    /**
     * @param ishStr1
     */
    public void setIshStr1(String ishStr1) {
        this.ishStr1 = ishStr1;
    }

    /**
     * @return ISH_STR2
     */
    public String getIshStr2() {
        return ishStr2;
    }

    /**
     * @param ishStr2
     */
    public void setIshStr2(String ishStr2) {
        this.ishStr2 = ishStr2;
    }

    /**
     * @return ISH_STR3
     */
    public String getIshStr3() {
        return ishStr3;
    }

    /**
     * @param ishStr3
     */
    public void setIshStr3(String ishStr3) {
        this.ishStr3 = ishStr3;
    }

    /**
     * @return ISH_STR4
     */
    public String getIshStr4() {
        return ishStr4;
    }

    /**
     * @param ishStr4
     */
    public void setIshStr4(String ishStr4) {
        this.ishStr4 = ishStr4;
    }

    /**
     * @return ISH_STR5
     */
    public String getIshStr5() {
        return ishStr5;
    }

    /**
     * @param ishStr5
     */
    public void setIshStr5(String ishStr5) {
        this.ishStr5 = ishStr5;
    }
}