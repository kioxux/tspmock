package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaMaterialControl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaMaterialControlMapper {

    List<GaiaMaterialControl> getAllByClientAndBrId(@Param("client") String client,@Param("brId") String brId);
}