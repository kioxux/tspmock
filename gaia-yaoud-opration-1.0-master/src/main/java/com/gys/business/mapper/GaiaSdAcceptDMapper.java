package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdAcceptD;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdAcceptDMapper extends BaseMapper<GaiaSdAcceptD> {
    List<GetAcceptDetailOutData> detailList(GetAcceptInData inData);

    /**
     * 查询报表
     * @param inData
     * @return
     */
    List<SupplierRecordOutData> findReportRecord(SupplierRecordInData inData);

    List<GetPoDetailInData> selectAcceptDetail(GetAcceptInData inData);

    GetPoInData selectAcceptTotal(GetAcceptInData inData);
}
