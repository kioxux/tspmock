package com.gys.business.mapper;

import com.gys.business.service.data.ProCampaignsOutData;
import com.gys.business.service.data.SalesSummaryData;

import java.util.List;
import java.util.Map;

public interface GaiaProCampaignsMapper {
    List<ProCampaignsOutData> selectCampainsProDetails(SalesSummaryData inData);

    List<ProCampaignsOutData>selectCampainsProTotal(SalesSummaryData inData);

    List<Map<String,String>> selectSXList(SalesSummaryData inData);
}
