//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 电子券类促销设置表
 */
@Table(name = "GAIA_SD_PROM_COUPON_SET")
@Data
public class GaiaSdPromCouponSet implements Serializable {
    private static final long serialVersionUID = 6496469446201545681L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPCS_VOUCHER_ID")
    private String gspcsVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPCS_SERIAL")
    private String gspcsSerial;

    /**
     * 电子券活动号
     */
    @Id
    @Column(name = "GSPCS_ACT_NO")
    private String gspcsActNo;

    /**
     * 电子券类型
     */
    @Column(name = "GSPCS_COUPON_TYPE")
    private String gspcsCouponType;

    /**
     * 电子券描述
     */
    @Column(name = "GSPCS_COUPON_REMARKS")
    private String gspcsCouponRemarks;

    /**
     * 使用起始日期
     */
    @Column(name = "GSPCS_BEGIN_DATE")
    private String gspcsBeginDate;

    /**
     * 使用结束日期
     */
    @Column(name = "GSPCS_END_DATE")
    private String gspcsEndDate;

    /**
     * 使用起始时间
     */
    @Column(name = "GSPCS_BEGIN_TIME")
    private String gspcsBeginTime;

    /**
     * 使用结束时间
     */
    @Column(name = "GSPCS_END_TIME")
    private String gspcsEndTime;

    /**
     * 整体赠送余额上限
     */
    @Column(name = "GSPCS_TOTAL_PAYCHECK_AMT")
    private BigDecimal gspcsTotalPaycheckAmt;

    /**
     * 单次充值金额
     */
    @Column(name = "GSPCS_SINGLE_PAYCHECK_AMT")
    private BigDecimal gspcsSinglePaycheckAmt;

    /**
     * 单次使用最小金额
     */
    @Column(name = "GSPCS_SINGLE_USE_AMT")
    private BigDecimal gspcsSingleUseAmt;
}
