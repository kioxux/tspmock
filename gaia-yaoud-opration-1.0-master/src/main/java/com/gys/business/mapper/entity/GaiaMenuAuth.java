package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Table(name = "GAIA_MENU_AUTH")
@ApiModel
public class GaiaMenuAuth implements Serializable {
    /**
     * 序号
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 权限编号
     */
    @NotNull
    @Column(name = "CODE")
    @ApiModelProperty(name = "权限编号")
    private String code;

    /**
     * 权限名称
     */
    @Column(name = "NAME")
    @ApiModelProperty(name = "权限名称")
    private String name;

    /**
     * 菜单编码
     */
    @Column(name = "MENU_ID")
    @ApiModelProperty(name = "菜单编码")
    private Long menuId;

    /**
     * 前置权限
     */
    @Column(name = "PRE_PERMISSION")
    @ApiModelProperty(name = "前置权限")
    private String prePermission;

    /**
     * 状态(0.禁用 1.启用 2.删除 )
     */
    @Column(name = "STATUS")
    @ApiModelProperty(name = " 状态(0.禁用 1.启用 2.删除 )")
    private String status;

    /**
     * 创建人
     */
    @Column(name = "CREATOR")
    private String creator;

    /**
     * 创建时间

     */
    @Column(name = "CREATE_DATE")
    private Date createDate;

    /**
     * 更新人
     */
    @Column(name = "UPDATOR")
    private String updator;

    /**
     * 更新时间

     */
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取权限名称
     *
     * @return NAME - 权限名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置权限名称
     *
     * @param name 权限名称
     */
    public void setName(String name) {
        this.name = name;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    /**
     * 获取前置权限
     *
     * @return PRE_PERMISSION - 前置权限
     */
    public String getPrePermission() {
        return prePermission;
    }

    /**
     * 设置前置权限
     *
     * @param prePermission 前置权限
     */
    public void setPrePermission(String prePermission) {
        this.prePermission = prePermission;
    }

    /**
     * 获取状态(0.禁用 1.启用 2.删除 )
     *
     * @return STATUS - 状态(0.禁用 1.启用 2.删除 )
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态(0.禁用 1.启用 2.删除 )
     *
     * @param status 状态(0.禁用 1.启用 2.删除 )
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取创建人
     *
     * @return CREATOR - 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置创建人
     *
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取创建时间

     *
     * @return CREATE_DATE - 创建时间

     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 设置创建时间

     *
     * @param createDate 创建时间

     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 获取更新人
     *
     * @return UPDATOR - 更新人
     */
    public String getUpdator() {
        return updator;
    }

    /**
     * 设置更新人
     *
     * @param updator 更新人
     */
    public void setUpdator(String updator) {
        this.updator = updator;
    }

    /**
     * 获取更新时间

     *
     * @return UPDATE_DATE - 更新时间

     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 设置更新时间

     *
     * @param updateDate 更新时间

     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}