package com.gys.business.mapper.entity;

import io.swagger.models.auth.In;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/7/16 14:10
 */
@Data
@Table(name = "GAIA_SD_WTPRO")
public class GaiaSdWtpro implements Serializable {
    private static final long serialVersionUID = 2518741337174308386L;
    /**
     * ID
     */
    @Id
    @Column( name = "ID")
    private Long id;
    /**
     * 加盟商
     */
    @Column( name = "CLIENT")
    private String client;
    /**
     * 药德商品自编码
     */
    @Column( name = "PRO_SELF_CODE")
    private String proSelfCode;
    /**
     * 第三方商品编码
     */
    @Column( name = "PRO_DSF_CODE")
    private String proDsfCode;
    /**
     * 第三方商品名
     */
    @Column( name = "PRO_DSF_NAME")
    private String proDsfName;
    /**
     * 药德/原始内码
     */
    @Column( name = "PRO_NM")
    private String proNm;
    /**
     * 状态 0：审核中 1：已审核
     */
    @Column( name = "STATUS")
    private int status;
    /**
     * 同步数据标识(0未同步，1已同步)
     */
    @Column( name = "PRO_SYNCFlAG")
    private int proSyncflag;
    /**
     * 转换比率
     */
    @Column( name = "PRO_UPDATE_DATE")
    private String proUpdateDate;
    /**
     * 转换比率
     */
    @Column( name = "PRO_UPDATE_TIME")
    private String proUpdateTime;
    /**
     * 转换比率
     */
    @Column( name = "PRO_RATE")
    private BigDecimal proRate;
}
