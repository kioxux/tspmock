package com.gys.business.mapper;

import com.gys.business.service.data.GaiaSdIntegralChange;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdIntegralChangeMapper {
    int deleteByPrimaryKey(@Param("client") String client, @Param("gsicBrId") String gsicBrId, @Param("gsicVoucherId") String gsicVoucherId, @Param("gsicFlag") String gsicFlag, @Param("gsicDate") String gsicDate, @Param("gsicType") String gsicType);

    int insert(GaiaSdIntegralChange record);

    int insertSelective(GaiaSdIntegralChange record);

    GaiaSdIntegralChange selectByPrimaryKey(@Param("client") String client, @Param("gsicBrId") String gsicBrId, @Param("gsicVoucherId") String gsicVoucherId, @Param("gsicFlag") String gsicFlag, @Param("gsicDate") String gsicDate, @Param("gsicType") String gsicType);

    int updateByPrimaryKeySelective(GaiaSdIntegralChange record);

    int updateByPrimaryKey(GaiaSdIntegralChange record);

    int updateBatch(List<GaiaSdIntegralChange> list);

    int batchInsert(@Param("list") List<GaiaSdIntegralChange> list);

    String getNextVoucherId(@Param("client") String client);

    int getNextVoucherIdByJFSJ(@Param("client") String client);

}