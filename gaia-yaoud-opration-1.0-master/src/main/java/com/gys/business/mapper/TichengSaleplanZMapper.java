package com.gys.business.mapper;

import com.gys.business.mapper.entity.TichengSaleplanZ;
import com.gys.business.service.data.CompadmOutData;
import com.gys.business.service.data.GetStoListOutData;
import com.gys.business.service.data.StoreInData;
import com.gys.business.service.data.percentageplan.*;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 销售提成方案主表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-11-11
 */
@Mapper
public interface TichengSaleplanZMapper extends BaseMapper<TichengSaleplanZ> {
    String selectNextPlanCode(String clientId);

    List<PercentageOutData> selectTichengZList(PercentageSearchInData inData);

    void deleteSalePlan(@Param("id") Long planId);

    List<PercentageProInData> selectProductByProCodes(ImportProInData inData);

    List<Map<String,String>>selectMovPriceListByDcCode(ImportProInData inData);

    PercentageInData selectTichengZSaleDetail(@Param("tichengId") Integer tichengId);

    void approveSalePlan(@Param("id") Long planId,@Param("planStatus") String planStatus,@Param("planReason") String planReason);

    List<Map<String,String>> selectSaleStoList(@Param("tichengId") Long planId);

    List<String> selectUserIdList(PercentageInData inData);

    List<Map<String,String>> selectProStoList(@Param("tichengId") Long planId);

    PercentageProInData selectProductByClient(@Param("client") String clientId,@Param("proCode") String proCode);

    String selectMovPriceByDcCode(@Param("client") String clientId,@Param("dcCode") String dcCode,@Param("proCode") String proCode);

    void stopSalePlan(@Param("id") Long planId,@Param("planStatus") String planStatus,@Param("planStopDate") String planStopDate,@Param("planReason") String planReason);

    List<PercentageOutData> selectPlanStopList();

    List<CompadmOutData> compadmInfoByClientId(@Param("client") String client, @Param("compadmId")String compadmId);

    List<GetStoListOutData> getStoListByClientId(StoreInData inData);

    List<Map<String, String>> selectMaxPlanCodeByClient();

    List<PercentageProInData> selectProductByClientProCodes(String client, List<String> proCodes);

    List<Map<String, Object>> selectMovPriceByDcCodeProCodes(@Param("client") String clientId, @Param("dcCode") String dcCode, @Param("proCodes") List<String> proCodes);


    List<String> getSaleClass(String client);

    List<String> getProPosition(String client);


    int checkStartDate(PercentageInData inData);

    int checkEndDate(PercentageInData inData);

    List<TichengSaleplanZ> checkDate(PercentageInData inData);
}
