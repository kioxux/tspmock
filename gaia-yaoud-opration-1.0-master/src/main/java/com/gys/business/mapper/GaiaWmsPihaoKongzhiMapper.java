package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWmsPihaoKongzhi;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaWmsPihaoKongzhiMapper {


    int batchInsert(@Param("list") List<GaiaWmsPihaoKongzhi> list);

    /**
     * 查询所有
     * @param client
     * @param depId
     * @return
     */
    List<GaiaWmsPihaoKongzhi> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);
}