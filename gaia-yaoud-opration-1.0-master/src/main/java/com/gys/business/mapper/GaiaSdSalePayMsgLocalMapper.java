package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdSalePayMsgLocal;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaSdSalePayMsgLocalMapper extends BaseMapper<GaiaSdSalePayMsgLocal> {
    void insertOrUpdate(List<GaiaSdSalePayMsgLocal> salePayMsgList);
}