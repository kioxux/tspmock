//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDepData;
import com.gys.business.service.data.DcOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaDepDataMapper extends BaseMapper<GaiaDepData> {
    List<DcOutData> selectDepList(String client);

    List<DcOutData> selectDcAndStore(String client);
}
