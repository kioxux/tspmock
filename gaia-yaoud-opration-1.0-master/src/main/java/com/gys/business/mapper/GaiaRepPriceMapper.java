package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaPriceGet;
import com.gys.business.mapper.entity.GaiaRepPrice;
import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.service.data.PriceGetOutData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaRepPriceMapper extends BaseMapper<GaiaRepPrice> {
    List<PriceGetOutData> getReplishList(GaiaSdReplenishH gaiaSdReplenishH);
    void insertBatch(List<GaiaPriceGet> inData);
}
