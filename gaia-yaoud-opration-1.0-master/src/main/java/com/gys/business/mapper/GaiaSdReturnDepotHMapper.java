//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdReturnDepotH;
import com.gys.business.service.data.DelegationReturn.DelegationReturnInData;
import com.gys.business.service.data.DelegationReturn.DelegationReturnOutData;
import com.gys.business.service.data.DelegationReturn.RetrunApprovalInData;
import com.gys.business.service.data.GetDepotInData;
import com.gys.business.service.data.GetDepotOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface GaiaSdReturnDepotHMapper extends BaseMapper<GaiaSdReturnDepotH> {
    List<GetDepotOutData> selectList(GetDepotInData inData);

    String selectNextVoucherId(@Param("clientId") String clientId);

    String selectNextVoucherId1(@Param("clientId") String clientId);

    List<DelegationReturnOutData> selectDelegationList(DelegationReturnInData data);

    List<RetrunApprovalInData> remainReturnD(Map<String, Object> map);

    void updateStatusBatch(@Param("rejectList")List<RetrunApprovalInData> rejectList,@Param("clientId") String clientId);

    void updateBatch(List<GaiaSdReturnDepotH> returnDepotHList);
}
