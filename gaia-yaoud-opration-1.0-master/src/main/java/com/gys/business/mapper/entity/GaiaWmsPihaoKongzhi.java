package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**

 * @author xiayuan
 * @date 2021-01-12
 */
@Data
public class GaiaWmsPihaoKongzhi implements Serializable {

    private static final long serialVersionUID = -763088426305917397L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String batSiteCode;

    /**
     * 商品编码
     */
    private String wmSpBm;

    /**
     * 批号
     */
    private String wmPh;

    /**
     * 创建日期
     */
    private String wmCjrq;

    /**
     * 创建时间
     */
    private String wmCjsj;

    /**
     * 创建人
     */
    private String wmCjr;

    /**
     * 修改日期
     */
    private String wmXgrq;

    /**
     * 修改时间
     */
    private String wmXgsj;

    /**
     * 修改人
     */
    private String wmXgr;

    /**
     * 禁止销售 0-否，1-是
     */
    private String proNoRetail;

    /**
     * 禁止采购 0-否，1-是
     */
    private String proNoPurchase;

    /**
     * 禁止配送 0-否，1-是
     */
    private String proNoDistributed;

    /**
     * 禁止原因
     */
    private String proJzyy;

    /**
     * 解禁原因
     */
    private String proJjyy;

}