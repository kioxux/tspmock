package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSynchronousAudit;
import com.gys.business.service.data.StartDayAndEndDayInData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author xiaoyuan
 */
public interface GaiaSynchronousAuditMapper extends BaseMapper<GaiaSynchronousAudit> {


    void insertLists(List<GaiaSynchronousAudit> list);

    String getAuditMaxDate(@Param("client") String client, @Param("brId") String brId,@Param("computerLogo") String computerLogo);

    List<GaiaSynchronousAudit> selectAuditListByDay(StartDayAndEndDayInData startDayAndEndDayInData);
}
