package com.gys.business.mapper.entity.member;

import lombok.Data;

import java.io.Serializable;

/**
 * 积分抵现活动时间段
 *
 * @author wu mao yin
 * @date 2021/12/9 15:16
 */
@Data
public class GaiaSdIntegralCashTimeInterval implements Serializable {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 积分抵现活动单号
     */
    private String gsictiVoucherId;

    /**
     * 开始日期
     */
    private String gsictiStartDate;

    /**
     * 开始时间
     */
    private String gsictiStartTime;

    /**
     * 结束日期
     */
    private String gsictiEndDate;

    /**
     * 结束时间
     */
    private String gsictiEndTime;

    private static final long serialVersionUID = 1L;

}