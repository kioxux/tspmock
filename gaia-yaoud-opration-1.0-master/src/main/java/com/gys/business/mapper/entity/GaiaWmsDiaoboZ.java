package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(
        name = "GAIA_WMS_DIAOBO_Z"
)
public class GaiaWmsDiaoboZ implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "WM_PSDH"
    )
    private String wmPsdh;
    @Column(
            name = "WM_DDH"
    )
    private String wmDdh;
    @Column(
            name = "WM_KH_BH"
    )
    private String wmKhBh;
    @Column(
            name = "WM_XQLX"
    )
    private String wmXqlx;
    @Column(
            name = "WM_CJRQ"
    )
    private String wmCjrq;
    @Column(
            name = "WM_CJSJ"
    )
    private String wmCjsj;
    @Column(
            name = "WM_CJR"
    )
    private String wmCjr;
    @Column(
            name = "WM_SFSQ"
    )
    private String wmSfsq;
    @Column(
            name = "WM_SFSF"
    )
    private String wmSfsf;
    @Column(
            name = "WM_XGRQ"
    )
    private String wmXgrq;
    @Column(
            name = "WM_XGSJ"
    )
    private String wmXgsj;
    @Column(
            name = "WM_XGR"
    )
    private String wmXgr;
    private static final long serialVersionUID = 1L;
}
