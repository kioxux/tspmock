package com.gys.business.mapper;


import com.gys.business.service.data.ReplenishmentRecordInData;
import com.gys.business.service.data.ReplenishmentRecordOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author xiaoyuan
 */
@Mapper
public interface DocumentManagementMapper {

    /**
     * 查询补货记录sql
     * @param inData
     * @return
     */
    List<ReplenishmentRecordOutData> getReplenishmentRecord(ReplenishmentRecordInData inData);
}
