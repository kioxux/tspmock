package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "GAIA_PURCHASE_REMIND")
public class GaiaPurchaseRemind implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name = "CLIENT")
    private String client;

    /**
     * 门店
     */
    @Column(name = "BR_ID")
    private String brId;

    /**
     * 单号
     */
    @Column(name = "BILL_NO")
    private String billNo;

    /**
     * 会员卡号
     */
    @Column(name = "MEMBER_CARD")
    private String memberCard;

    /**
     * 手机号码
     */
    @Column(name = "TEL")
    private String tel;

    /**
     * 商品编码
     */
    @Column(name = "PRO_ID")
    private String proId;

    /**
     * 药品名称
     */
    @Column(name = "CH_TRADE_NAME")
    private String chTradeName;

    /**
     * 购药时间
     */
    @Column(name = "PURCHASE_TIME")
    private Date purchaseTime;

    /**
     * 购药提醒日期
     */
    @Column(name = "REMIND_DATE")
    private Date remindDate;

    /**
     * 是否已发送 默认N
     */
    @Column(name = "IS_SEND")
    private String isSend;

    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    private Date createTime;

    private static final long serialVersionUID = 1L;

    /**
     * @return ID
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取门店
     *
     * @return BR_ID - 门店
     */
    public String getBrId() {
        return brId;
    }

    /**
     * 设置门店
     *
     * @param brId 门店
     */
    public void setBrId(String brId) {
        this.brId = brId;
    }

    /**
     * 获取单号
     *
     * @return BILL_NO - 单号
     */
    public String getBillNo() {
        return billNo;
    }

    /**
     * 设置单号
     *
     * @param billNo 单号
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * 获取会员卡号
     *
     * @return MEMBER_CARD - 会员卡号
     */
    public String getMemberCard() {
        return memberCard;
    }

    /**
     * 设置会员卡号
     *
     * @param memberCard 会员卡号
     */
    public void setMemberCard(String memberCard) {
        this.memberCard = memberCard;
    }

    /**
     * 获取手机号码
     *
     * @return TEL - 手机号码
     */
    public String getTel() {
        return tel;
    }

    /**
     * 设置手机号码
     *
     * @param tel 手机号码
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 获取商品编码
     *
     * @return PRO_ID - 商品编码
     */
    public String getProId() {
        return proId;
    }

    /**
     * 设置商品编码
     *
     * @param proId 商品编码
     */
    public void setProId(String proId) {
        this.proId = proId;
    }

    /**
     * 获取药品名称
     *
     * @return CH_TRADE_NAME - 药品名称
     */
    public String getChTradeName() {
        return chTradeName;
    }

    /**
     * 设置药品名称
     *
     * @param chTradeName 药品名称
     */
    public void setChTradeName(String chTradeName) {
        this.chTradeName = chTradeName;
    }

    /**
     * 获取购药时间
     *
     * @return PURCHASE_TIME - 购药时间
     */
    public Date getPurchaseTime() {
        return purchaseTime;
    }

    /**
     * 设置购药时间
     *
     * @param purchaseTime 购药时间
     */
    public void setPurchaseTime(Date purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    /**
     * 获取购药提醒日期
     *
     * @return REMIND_DATE - 购药提醒日期
     */
    public Date getRemindDate() {
        return remindDate;
    }

    /**
     * 设置购药提醒日期
     *
     * @param remindDate 购药提醒日期
     */
    public void setRemindDate(Date remindDate) {
        this.remindDate = remindDate;
    }

    /**
     * 获取是否已发送 默认N
     *
     * @return IS_SEND - 是否已发送 默认N
     */
    public String getIsSend() {
        return isSend;
    }

    /**
     * 设置是否已发送 默认N
     *
     * @param isSend 是否已发送 默认N
     */
    public void setIsSend(String isSend) {
        this.isSend = isSend;
    }

    /**
     * 获取创建时间
     *
     * @return CREATE_TIME - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}