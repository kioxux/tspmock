package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 组合类促销条件商品设置表
 */
@Table(name = "GAIA_SD_PROM_ASSO_CONDS")
@Data
public class GaiaSdPromAssoConds implements Serializable {
    private static final long serialVersionUID = 8516728886216051725L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 单号
     */
    @Id
    @Column(name = "GSPAC_VOUCHER_ID")
    private String gspacVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPAC_SERIAL")
    private String gspacSerial;

    /**
     * 商品编码
     */
    @Id
    @Column(name = "GSPAC_PRO_ID")
    private String gspacProId;

    /**
     * 数量
     */
    @Column(name = "GSPAC_QTY")
    private String gspacQty;

    /**
     * 系列编码
     */
    @Column(name = "GSPAC_SERIES_ID")
    private String gspacSeriesId;

    /**
     * 是否会员
     */
    @Column(name = "GSPAC_MEM_FLAG")
    private String gspacMemFlag;

    /**
     * 是否积分
     */
    @Column(name = "GSPAC_INTE_FLAG")
    private String gspacInteFlag;

    /**
     * 积分倍率
     */
    @Column(name = "GSPAC_INTE_RATE")
    private String gspacInteRate;

    /**
     * 会员等级
     */
    @Column(name = "GSPAC_MEMBERSHIP_GRADE")
    private String gspacMembershipGrade;
}
