package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 销售提成方案明细表
 * </p>
 *
 * @author flynn
 * @since 2021-11-12
 */
@Data
@Table( name = "GAIA_TICHENG_SALEPLAN_M")
public class TichengSaleplanM implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键ID")
    @Id
    @Column(name = "ID")
    private Long id;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "销售提成方案主表主键ID")
    @Column(name = "PID")
    private Long pid;

    @ApiModelProperty(value = "最小日均销售额")
    @Column(name = "MIN_DAILY_SALE_AMT")
    private BigDecimal minDailySaleAmt;

    @ApiModelProperty(value = "最大日均销售额")
    @Column(name = "MAX_DAILY_SALE_AMT")
    private BigDecimal maxDailySaleAmt;

    @ApiModelProperty(value = "毛利级别")
    @Column(name = "PRO_SALE_CLASS")
    private String proSaleClass;

    @ApiModelProperty(value = "最小毛利率")
    @Column(name = "MIN_PRO_MLL")
    private BigDecimal minProMll;

    @ApiModelProperty(value = "最大毛利率")
    @Column(name = "MAX_PRO_MLL")
    private BigDecimal maxProMll;

    @ApiModelProperty(value = "提成比例")
    @Column(name = "TICHENG_SCALE")
    private BigDecimal tichengScale;

    @ApiModelProperty(value = "操作状态 0 未删除 1 已删除")
    @Column(name = "DELETE_FLAG")
    private String deleteFlag;

    @Column(name = "LAST_UPDATE_TIME")
    private LocalDateTime lastUpdateTime;


}
