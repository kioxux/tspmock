package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdStoreData;
import com.gys.business.mapper.entity.MemberClass;
import com.gys.business.service.data.StoreInData;
import com.gys.business.service.data.StoreOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdStoreDataMapper extends BaseMapper<GaiaSdStoreData> {
    List<StoreOutData> getStoreData(StoreInData inData);

    List<GaiaSdStoreData> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);

    void addMemberClass(List<MemberClass> list);

    int countMemberClass(@Param("client")String client);

    List<String> getAllBrIdByClient(String client);

    List<GaiaSdStoreData> getStoreByClientAndStoCodes(@Param("client") String client,@Param("stoCodeList") List<String> stoCodeList);
}
