package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaDataSynchronized;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author xiaoyuan
 */
public interface GaiaDataSynchronizedMapper {

    int insertSelective(GaiaDataSynchronized record);

    int batchInsert(@Param("list") List<GaiaDataSynchronized> list);

    List<GaiaDataSynchronized> getAll();

    Map<String, Object> getEveryOne(@Param("tableName") String tableName, @Param("map") Map<String, Object> map);

    GaiaDataSynchronized getAllByVersion(@Param("version") String version);

    List<GaiaDataSynchronized> getAllByTime(GaiaDataSynchronized aSynchronized);

    String getMaxDateTime(@Param("client") String client, @Param("depId") String depId);

    List<GaiaDataSynchronized> getTheDataYouNeed(GaiaDataSynchronized aSynchronized);
}