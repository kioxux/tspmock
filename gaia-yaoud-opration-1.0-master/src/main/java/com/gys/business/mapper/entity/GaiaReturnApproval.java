package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "GAIA_RETURN_APPROVAL")
public class GaiaReturnApproval implements Serializable {
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 店号
     */
    @Id
    @Column(name = "GSRDH_BR_ID")
    private String gsrdhBrId;

    /**
     * 退库单号
     */
    @Id
    @Column(name = "GSRDH_VOUCHER_ID")
    private String gsrdhVoucherId;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSRDD_SERIAL")
    private String gsrddSerial;

    /**
     * 退库日期
     */
    @Column(name = "GSRDH_DATE")
    private String gsrdhDate;

    /**
     * 单据类型
     */
    @Column(name = "GSRDH_TYPE")
    private String gsrdhType;

    /**
     * 退库状态
     */
    @Column(name = "GSRDH_STATUS")
    private String gsrdhStatus;

    /**
     * 退库人员
     */
    @Column(name = "GSRDH_EMP")
    private String gsrdhEmp;

    /**
     * 备注
     */
    @Column(name = "GSRDH_REMAKS")
    private String gsrdhRemaks;

    /**
     * 退库方式
     */
    @Column(name = "GSRDH_PATTERN")
    private String gsrdhPattern;

    /**
     * 商品编码
     */
    @Column(name = "GSRDD_PRO_ID")
    private String gsrddProId;

    /**
     * 批次
     */
    @Column(name = "GSRDD_BATCH")
    private String gsrddBatch;

    /**
     * 退库数量
     */
    @Column(name = "GSRDD_RETDEP_QTY")
    private BigDecimal gsrddRetdepQty;

    /**
     * 修正数量
     */
    @Column(name = "GSRDD_REVISE_QTY")
    private BigDecimal gsrddReviseQty;

    /**
     * 审批结果
     */
    @Column(name = "GSRDD_APP_RES")
    private String gsrddAppRes;

    /**
     * 审批人
     */
    @Column(name = "GSRDD_APP_PEO")
    private String gsrddAppPeo;

    /**
     * 审批日期
     */
    @Column(name = "GSRDD_APP_DATE")
    private String gsrddAppDate;

    /**
     * 审批时间
     */
    @Column(name = "GSRDD_APP_TIME")
    private String gsrddAppTime;

    /**
     * 采购订单
     */
    @Column(name = "GSRDD_PO_ID")
    private String gsrddPoId;

    /**
     * 采购订单行
     */
    @Column(name = "GSRDD_PO_LINE")
    private String gsrddPoLine;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商
     *
     * @return CLIENT - 加盟商
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商
     *
     * @param client 加盟商
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取店号
     *
     * @return GSRDH_BR_ID - 店号
     */
    public String getGsrdhBrId() {
        return gsrdhBrId;
    }

    /**
     * 设置店号
     *
     * @param gsrdhBrId 店号
     */
    public void setGsrdhBrId(String gsrdhBrId) {
        this.gsrdhBrId = gsrdhBrId;
    }

    /**
     * 获取退库单号
     *
     * @return GSRDH_VOUCHER_ID - 退库单号
     */
    public String getGsrdhVoucherId() {
        return gsrdhVoucherId;
    }

    /**
     * 设置退库单号
     *
     * @param gsrdhVoucherId 退库单号
     */
    public void setGsrdhVoucherId(String gsrdhVoucherId) {
        this.gsrdhVoucherId = gsrdhVoucherId;
    }

    /**
     * 获取行号
     *
     * @return GSRDD_SERIAL - 行号
     */
    public String getGsrddSerial() {
        return gsrddSerial;
    }

    /**
     * 设置行号
     *
     * @param gsrddSerial 行号
     */
    public void setGsrddSerial(String gsrddSerial) {
        this.gsrddSerial = gsrddSerial;
    }

    /**
     * 获取退库日期
     *
     * @return GSRDH_DATE - 退库日期
     */
    public String getGsrdhDate() {
        return gsrdhDate;
    }

    /**
     * 设置退库日期
     *
     * @param gsrdhDate 退库日期
     */
    public void setGsrdhDate(String gsrdhDate) {
        this.gsrdhDate = gsrdhDate;
    }

    /**
     * 获取单据类型
     *
     * @return GSRDH_TYPE - 单据类型
     */
    public String getGsrdhType() {
        return gsrdhType;
    }

    /**
     * 设置单据类型
     *
     * @param gsrdhType 单据类型
     */
    public void setGsrdhType(String gsrdhType) {
        this.gsrdhType = gsrdhType;
    }

    /**
     * 获取退库状态
     *
     * @return GSRDH_STATUS - 退库状态
     */
    public String getGsrdhStatus() {
        return gsrdhStatus;
    }

    /**
     * 设置退库状态
     *
     * @param gsrdhStatus 退库状态
     */
    public void setGsrdhStatus(String gsrdhStatus) {
        this.gsrdhStatus = gsrdhStatus;
    }

    /**
     * 获取退库人员
     *
     * @return GSRDH_EMP - 退库人员
     */
    public String getGsrdhEmp() {
        return gsrdhEmp;
    }

    /**
     * 设置退库人员
     *
     * @param gsrdhEmp 退库人员
     */
    public void setGsrdhEmp(String gsrdhEmp) {
        this.gsrdhEmp = gsrdhEmp;
    }

    /**
     * 获取备注
     *
     * @return GSRDH_REMAKS - 备注
     */
    public String getGsrdhRemaks() {
        return gsrdhRemaks;
    }

    /**
     * 设置备注
     *
     * @param gsrdhRemaks 备注
     */
    public void setGsrdhRemaks(String gsrdhRemaks) {
        this.gsrdhRemaks = gsrdhRemaks;
    }

    /**
     * 获取退库方式
     *
     * @return GSRDH_PATTERN - 退库方式
     */
    public String getGsrdhPattern() {
        return gsrdhPattern;
    }

    /**
     * 设置退库方式
     *
     * @param gsrdhPattern 退库方式
     */
    public void setGsrdhPattern(String gsrdhPattern) {
        this.gsrdhPattern = gsrdhPattern;
    }

    /**
     * 获取商品编码
     *
     * @return GSRDD_PRO_ID - 商品编码
     */
    public String getGsrddProId() {
        return gsrddProId;
    }

    /**
     * 设置商品编码
     *
     * @param gsrddProId 商品编码
     */
    public void setGsrddProId(String gsrddProId) {
        this.gsrddProId = gsrddProId;
    }

    /**
     * 获取批次
     *
     * @return GSRDD_BATCH - 批次
     */
    public String getGsrddBatch() {
        return gsrddBatch;
    }

    /**
     * 设置批次
     *
     * @param gsrddBatch 批次
     */
    public void setGsrddBatch(String gsrddBatch) {
        this.gsrddBatch = gsrddBatch;
    }

    /**
     * 获取退库数量
     *
     * @return GSRDD_RETDEP_QTY - 退库数量
     */
    public BigDecimal getGsrddRetdepQty() {
        return gsrddRetdepQty;
    }

    /**
     * 设置退库数量
     *
     * @param gsrddRetdepQty 退库数量
     */
    public void setGsrddRetdepQty(BigDecimal gsrddRetdepQty) {
        this.gsrddRetdepQty = gsrddRetdepQty;
    }

    /**
     * 获取修正数量
     *
     * @return GSRDD_REVISE_QTY - 修正数量
     */
    public BigDecimal getGsrddReviseQty() {
        return gsrddReviseQty;
    }

    /**
     * 设置修正数量
     *
     * @param gsrddReviseQty 修正数量
     */
    public void setGsrddReviseQty(BigDecimal gsrddReviseQty) {
        this.gsrddReviseQty = gsrddReviseQty;
    }

    /**
     * 获取审批结果
     *
     * @return GSRDD_APP_RES - 审批结果
     */
    public String getGsrddAppRes() {
        return gsrddAppRes;
    }

    /**
     * 设置审批结果
     *
     * @param gsrddAppRes 审批结果
     */
    public void setGsrddAppRes(String gsrddAppRes) {
        this.gsrddAppRes = gsrddAppRes;
    }

    /**
     * 获取审批人
     *
     * @return GSRDD_APP_PEO - 审批人
     */
    public String getGsrddAppPeo() {
        return gsrddAppPeo;
    }

    /**
     * 设置审批人
     *
     * @param gsrddAppPeo 审批人
     */
    public void setGsrddAppPeo(String gsrddAppPeo) {
        this.gsrddAppPeo = gsrddAppPeo;
    }

    /**
     * 获取审批日期
     *
     * @return GSRDD_APP_DATE - 审批日期
     */
    public String getGsrddAppDate() {
        return gsrddAppDate;
    }

    /**
     * 设置审批日期
     *
     * @param gsrddAppDate 审批日期
     */
    public void setGsrddAppDate(String gsrddAppDate) {
        this.gsrddAppDate = gsrddAppDate;
    }

    /**
     * 获取审批时间
     *
     * @return GSRDD_APP_TIME - 审批时间
     */
    public String getGsrddAppTime() {
        return gsrddAppTime;
    }

    /**
     * 设置审批时间
     *
     * @param gsrddAppTime 审批时间
     */
    public void setGsrddAppTime(String gsrddAppTime) {
        this.gsrddAppTime = gsrddAppTime;
    }

    /**
     * 获取采购订单
     *
     * @return GSRDD_PO_ID - 采购订单
     */
    public String getGsrddPoId() {
        return gsrddPoId;
    }

    /**
     * 设置采购订单
     *
     * @param gsrddPoId 采购订单
     */
    public void setGsrddPoId(String gsrddPoId) {
        this.gsrddPoId = gsrddPoId;
    }

    /**
     * 获取采购订单行
     *
     * @return GSRDD_PO_LINE - 采购订单行
     */
    public String getGsrddPoLine() {
        return gsrddPoLine;
    }

    /**
     * 设置采购订单行
     *
     * @param gsrddPoLine 采购订单行
     */
    public void setGsrddPoLine(String gsrddPoLine) {
        this.gsrddPoLine = gsrddPoLine;
    }
}