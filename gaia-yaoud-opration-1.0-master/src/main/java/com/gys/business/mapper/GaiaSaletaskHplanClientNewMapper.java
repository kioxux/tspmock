package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSaletaskHplanClientNew;
import com.gys.business.mapper.entity.GaiaSaletaskHplanNew;
import com.gys.business.service.data.MonthOutData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.GetLoginOutData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface GaiaSaletaskHplanClientNewMapper extends BaseMapper<GaiaSaletaskHplanClientNew> {

    void insertMonthlySalesPlan(List<GaiaSaletaskHplanClientNew> list);//添加
    //用户管理后台 月销售任务列表
    String selectNextVoucherIdS();
    int staskTaskNameByName(@Param("staskTaskName") String staskTaskName ,@Param("monthPlan") String monthPlan);
}