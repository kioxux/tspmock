package com.gys.business.mapper;

import com.gys.business.mapper.entity.MessageTempleStoPosition;
import com.gys.business.service.MessageTemplateStoQuery;
import com.gys.business.service.data.MessageTemplateChooseUserDb;
import com.gys.business.service.data.MessageTemplateQueryUserInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 消息模版—门店级别-职位关系表（门店人员设定） Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-09-14
 */
@Mapper
public interface MessageTempleStoPositionMapper extends BaseMapper<MessageTempleStoPosition> {
    List<MessageTemplateChooseUserDb> selectAllStoSetting(MessageTemplateStoQuery query);

    List<MessageTemplateChooseUserDb> selectAllStoUsers(MessageTemplateQueryUserInData inData);

}
