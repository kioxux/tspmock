package com.gys.business.mapper;

import com.gys.business.controller.app.form.PushForm;
import com.gys.business.controller.app.vo.DcPushVO;
import com.gys.business.mapper.entity.DcOutStockD;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/22 16:39
 */
public interface DcOutStockDMapper {

    DcOutStockD getById(Long id);

    int add(DcOutStockD dcOutStockD);

    int update(DcOutStockD dcOutStockD);

    List<DcOutStockD> findList(DcOutStockD cond);

    DcOutStockD getUnique(DcOutStockD cond);

    DcPushVO getPushAmount(PushForm pushForm);
}
