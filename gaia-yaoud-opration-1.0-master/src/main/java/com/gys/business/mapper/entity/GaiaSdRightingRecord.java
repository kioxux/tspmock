package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="com-gys-business-mapper-entity-GaiaSdRightingRecord")
public class GaiaSdRightingRecord {
    /**
    * 主键
    */
    @ApiModelProperty(value="主键")
    private Long id;

    /**
    * 加盟商
    */
    @ApiModelProperty(value="加盟商")
    private String client;

    /**
    * 门店编码
    */
    @ApiModelProperty(value="门店编码")
    private String brId;

    /**
    * 结算ID
    */
    @ApiModelProperty(value="结算ID")
    private String setlId;

    /**
    * 就诊ID
    */
    @ApiModelProperty(value="就诊ID")
    private String mdtrtId;

    /**
    * 人员编号
    */
    @ApiModelProperty(value="人员编号")
    private String psnNo;

    /**
    * 人员姓名
    */
    @ApiModelProperty(value="人员姓名")
    private String psnName;

    /**
    * 人员证件类型
    */
    @ApiModelProperty(value="人员证件类型")
    private String psnCertType;

    /**
    * 证件号码
    */
    @ApiModelProperty(value="证件号码")
    private String certno;

    /**
    * 性别
    */
    @ApiModelProperty(value="性别")
    private String gend;

    /**
    * 民族
    */
    @ApiModelProperty(value="民族")
    private String naty;

    /**
    * 出生日期
    */
    @ApiModelProperty(value="出生日期")
    private String brdy;

    /**
    * 年龄
    */
    @ApiModelProperty(value="年龄")
    private String age;

    /**
    * 险种类型
    */
    @ApiModelProperty(value="险种类型")
    private String insutype;

    /**
    * 人员类别
    */
    @ApiModelProperty(value="人员类别")
    private String psnType;

    /**
    * 公务员标志
    */
    @ApiModelProperty(value="公务员标志")
    private String cvlservFlag;

    /**
    * 灵活就业标志
    */
    @ApiModelProperty(value="灵活就业标志")
    private String flxempeFlag;

    /**
    * 新生儿标志
    */
    @ApiModelProperty(value="新生儿标志")
    private String nwbFlag;

    /**
    * 参保机构医保区划
    */
    @ApiModelProperty(value="参保机构医保区划")
    private String insuOptins;

    /**
    * 单位名称
    */
    @ApiModelProperty(value="单位名称")
    private String empName;

    /**
    * 支付地点类别
    */
    @ApiModelProperty(value="支付地点类别")
    private String payLoc;

    /**
    * 定点医药机构编号
    */
    @ApiModelProperty(value="定点医药机构编号")
    private String fixmedinsCode;

    /**
    * 定点医药机构名称
    */
    @ApiModelProperty(value="定点医药机构名称")
    private String fixmedinsName;

    /**
    * 医院等级
    */
    @ApiModelProperty(value="医院等级")
    private String hospLv;

    /**
    * 定点归属机构
    */
    @ApiModelProperty(value="定点归属机构")
    private String fixmedinsPoolarea;

    /**
    * 限价医院等级
    */
    @ApiModelProperty(value="限价医院等级")
    private String lmtpricHospLv;

    /**
    * 起付线医院等级
    */
    @ApiModelProperty(value="起付线医院等级")
    private String dedcHospLv;

    /**
    * 开始日期
    */
    @ApiModelProperty(value="开始日期")
    private String begndate;

    /**
    * 结束日期
    */
    @ApiModelProperty(value="结束日期")
    private String enddate;

    /**
    * 结算时间
    */
    @ApiModelProperty(value="结算时间")
    private String setlTime;

    /**
    * 就诊凭证类型
    */
    @ApiModelProperty(value="就诊凭证类型")
    private String mdtrtCertType;

    /**
    * 医疗类别
    */
    @ApiModelProperty(value="医疗类别")
    private String medType;

    /**
    * 清算类别
    */
    @ApiModelProperty(value="清算类别")
    private String clrType;

    /**
    * 清算方式
    */
    @ApiModelProperty(value="清算方式")
    private String clrWay;

    /**
    * 清算经办机构
    */
    @ApiModelProperty(value="清算经办机构")
    private String clrOptins;

    /**
    * 医疗费总额
    */
    @ApiModelProperty(value="医疗费总额")
    private String medfeeSumamt;

    /**
    * 全自费金额
    */
    @ApiModelProperty(value="全自费金额")
    private String fulamtOwnpayAmt;

    /**
    * 超限价自费费用
    */
    @ApiModelProperty(value="超限价自费费用")
    private String overlmtSelfpay;

    /**
    * 先行自付金额
    */
    @ApiModelProperty(value="先行自付金额")
    private String preselfpayAmt;

    /**
    * 符合政策范围金额
    */
    @ApiModelProperty(value="符合政策范围金额")
    private String inscpScpAmt;

    /**
    * 实际支付起付线
    */
    @ApiModelProperty(value="实际支付起付线")
    private String actPayDedc;

    /**
    * 基本医疗保险统筹基金支出
    */
    @ApiModelProperty(value="基本医疗保险统筹基金支出")
    private String hifpPay;

    /**
    * 基本医疗保险统筹基金支付比例
    */
    @ApiModelProperty(value="基本医疗保险统筹基金支付比例")
    private String poolPropSelfpay;

    /**
    * 公务员医疗补助资金支出
    */
    @ApiModelProperty(value="公务员医疗补助资金支出")
    private String cvlservPay;

    /**
    * 企业补充医疗保险基金支出
    */
    @ApiModelProperty(value="企业补充医疗保险基金支出")
    private String hifesPay;

    /**
    * 居民大病保险资金支出
    */
    @ApiModelProperty(value="居民大病保险资金支出")
    private String hifmiPay;

    /**
    * 职工大额医疗费用补助基金支出
    */
    @ApiModelProperty(value="职工大额医疗费用补助基金支出")
    private String hifobPay;

    /**
    * 医疗救助基金支出
    */
    @ApiModelProperty(value="医疗救助基金支出")
    private String mafPay;

    /**
    * 其他支出
    */
    @ApiModelProperty(value="其他支出")
    private String othPay;

    /**
    * 基金支付总额
    */
    @ApiModelProperty(value="基金支付总额")
    private String fundPaySumamt;

    /**
    * 个人支付金额
    */
    @ApiModelProperty(value="个人支付金额")
    private String psnPay;

    /**
    * 个人账户支出
    */
    @ApiModelProperty(value="个人账户支出")
    private String acctPay;

    /**
    * 现金支付金额
    */
    @ApiModelProperty(value="现金支付金额")
    private String cashPayamt;

    /**
    * 余额
    */
    @ApiModelProperty(value="余额")
    private String balc;

    /**
    * 个人账户共济支付金额
    */
    @ApiModelProperty(value="个人账户共济支付金额")
    private String acctMulaidPay;

    /**
    * 医药机构结算ID
    */
    @ApiModelProperty(value="医药机构结算ID")
    private String medinsSetlId;

    /**
    * 退费结算标志
    */
    @ApiModelProperty(value="退费结算标志")
    private String refdSetlFlag;

    /**
    * 年度
    */
    @ApiModelProperty(value="年度")
    private String year;

    /**
    * 病种编码
    */
    @ApiModelProperty(value="病种编码")
    private String diseCodg;

    /**
    * 病种名称
    */
    @ApiModelProperty(value="病种名称")
    private String diseName;

    /**
    * 发票号
    */
    @ApiModelProperty(value="发票号")
    private String invono;

    /**
    * 经办人ID
    */
    @ApiModelProperty(value="经办人ID")
    private String opterId;

    /**
    * 经办人姓名
    */
    @ApiModelProperty(value="经办人姓名")
    private String opterName;

    /**
    * 经办时间
    */
    @ApiModelProperty(value="经办时间")
    private String optTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBrId() {
        return brId;
    }

    public void setBrId(String brId) {
        this.brId = brId;
    }

    public String getSetlId() {
        return setlId;
    }

    public void setSetlId(String setlId) {
        this.setlId = setlId;
    }

    public String getMdtrtId() {
        return mdtrtId;
    }

    public void setMdtrtId(String mdtrtId) {
        this.mdtrtId = mdtrtId;
    }

    public String getPsnNo() {
        return psnNo;
    }

    public void setPsnNo(String psnNo) {
        this.psnNo = psnNo;
    }

    public String getPsnName() {
        return psnName;
    }

    public void setPsnName(String psnName) {
        this.psnName = psnName;
    }

    public String getPsnCertType() {
        return psnCertType;
    }

    public void setPsnCertType(String psnCertType) {
        this.psnCertType = psnCertType;
    }

    public String getCertno() {
        return certno;
    }

    public void setCertno(String certno) {
        this.certno = certno;
    }

    public String getGend() {
        return gend;
    }

    public void setGend(String gend) {
        this.gend = gend;
    }

    public String getNaty() {
        return naty;
    }

    public void setNaty(String naty) {
        this.naty = naty;
    }

    public String getBrdy() {
        return brdy;
    }

    public void setBrdy(String brdy) {
        this.brdy = brdy;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getInsutype() {
        return insutype;
    }

    public void setInsutype(String insutype) {
        this.insutype = insutype;
    }

    public String getPsnType() {
        return psnType;
    }

    public void setPsnType(String psnType) {
        this.psnType = psnType;
    }

    public String getCvlservFlag() {
        return cvlservFlag;
    }

    public void setCvlservFlag(String cvlservFlag) {
        this.cvlservFlag = cvlservFlag;
    }

    public String getFlxempeFlag() {
        return flxempeFlag;
    }

    public void setFlxempeFlag(String flxempeFlag) {
        this.flxempeFlag = flxempeFlag;
    }

    public String getNwbFlag() {
        return nwbFlag;
    }

    public void setNwbFlag(String nwbFlag) {
        this.nwbFlag = nwbFlag;
    }

    public String getInsuOptins() {
        return insuOptins;
    }

    public void setInsuOptins(String insuOptins) {
        this.insuOptins = insuOptins;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getPayLoc() {
        return payLoc;
    }

    public void setPayLoc(String payLoc) {
        this.payLoc = payLoc;
    }

    public String getFixmedinsCode() {
        return fixmedinsCode;
    }

    public void setFixmedinsCode(String fixmedinsCode) {
        this.fixmedinsCode = fixmedinsCode;
    }

    public String getFixmedinsName() {
        return fixmedinsName;
    }

    public void setFixmedinsName(String fixmedinsName) {
        this.fixmedinsName = fixmedinsName;
    }

    public String getHospLv() {
        return hospLv;
    }

    public void setHospLv(String hospLv) {
        this.hospLv = hospLv;
    }

    public String getFixmedinsPoolarea() {
        return fixmedinsPoolarea;
    }

    public void setFixmedinsPoolarea(String fixmedinsPoolarea) {
        this.fixmedinsPoolarea = fixmedinsPoolarea;
    }

    public String getLmtpricHospLv() {
        return lmtpricHospLv;
    }

    public void setLmtpricHospLv(String lmtpricHospLv) {
        this.lmtpricHospLv = lmtpricHospLv;
    }

    public String getDedcHospLv() {
        return dedcHospLv;
    }

    public void setDedcHospLv(String dedcHospLv) {
        this.dedcHospLv = dedcHospLv;
    }

    public String getBegndate() {
        return begndate;
    }

    public void setBegndate(String begndate) {
        this.begndate = begndate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getSetlTime() {
        return setlTime;
    }

    public void setSetlTime(String setlTime) {
        this.setlTime = setlTime;
    }

    public String getMdtrtCertType() {
        return mdtrtCertType;
    }

    public void setMdtrtCertType(String mdtrtCertType) {
        this.mdtrtCertType = mdtrtCertType;
    }

    public String getMedType() {
        return medType;
    }

    public void setMedType(String medType) {
        this.medType = medType;
    }

    public String getClrType() {
        return clrType;
    }

    public void setClrType(String clrType) {
        this.clrType = clrType;
    }

    public String getClrWay() {
        return clrWay;
    }

    public void setClrWay(String clrWay) {
        this.clrWay = clrWay;
    }

    public String getClrOptins() {
        return clrOptins;
    }

    public void setClrOptins(String clrOptins) {
        this.clrOptins = clrOptins;
    }

    public String getMedfeeSumamt() {
        return medfeeSumamt;
    }

    public void setMedfeeSumamt(String medfeeSumamt) {
        this.medfeeSumamt = medfeeSumamt;
    }

    public String getFulamtOwnpayAmt() {
        return fulamtOwnpayAmt;
    }

    public void setFulamtOwnpayAmt(String fulamtOwnpayAmt) {
        this.fulamtOwnpayAmt = fulamtOwnpayAmt;
    }

    public String getOverlmtSelfpay() {
        return overlmtSelfpay;
    }

    public void setOverlmtSelfpay(String overlmtSelfpay) {
        this.overlmtSelfpay = overlmtSelfpay;
    }

    public String getPreselfpayAmt() {
        return preselfpayAmt;
    }

    public void setPreselfpayAmt(String preselfpayAmt) {
        this.preselfpayAmt = preselfpayAmt;
    }

    public String getInscpScpAmt() {
        return inscpScpAmt;
    }

    public void setInscpScpAmt(String inscpScpAmt) {
        this.inscpScpAmt = inscpScpAmt;
    }

    public String getActPayDedc() {
        return actPayDedc;
    }

    public void setActPayDedc(String actPayDedc) {
        this.actPayDedc = actPayDedc;
    }

    public String getHifpPay() {
        return hifpPay;
    }

    public void setHifpPay(String hifpPay) {
        this.hifpPay = hifpPay;
    }

    public String getPoolPropSelfpay() {
        return poolPropSelfpay;
    }

    public void setPoolPropSelfpay(String poolPropSelfpay) {
        this.poolPropSelfpay = poolPropSelfpay;
    }

    public String getCvlservPay() {
        return cvlservPay;
    }

    public void setCvlservPay(String cvlservPay) {
        this.cvlservPay = cvlservPay;
    }

    public String getHifesPay() {
        return hifesPay;
    }

    public void setHifesPay(String hifesPay) {
        this.hifesPay = hifesPay;
    }

    public String getHifmiPay() {
        return hifmiPay;
    }

    public void setHifmiPay(String hifmiPay) {
        this.hifmiPay = hifmiPay;
    }

    public String getHifobPay() {
        return hifobPay;
    }

    public void setHifobPay(String hifobPay) {
        this.hifobPay = hifobPay;
    }

    public String getMafPay() {
        return mafPay;
    }

    public void setMafPay(String mafPay) {
        this.mafPay = mafPay;
    }

    public String getOthPay() {
        return othPay;
    }

    public void setOthPay(String othPay) {
        this.othPay = othPay;
    }

    public String getFundPaySumamt() {
        return fundPaySumamt;
    }

    public void setFundPaySumamt(String fundPaySumamt) {
        this.fundPaySumamt = fundPaySumamt;
    }

    public String getPsnPay() {
        return psnPay;
    }

    public void setPsnPay(String psnPay) {
        this.psnPay = psnPay;
    }

    public String getAcctPay() {
        return acctPay;
    }

    public void setAcctPay(String acctPay) {
        this.acctPay = acctPay;
    }

    public String getCashPayamt() {
        return cashPayamt;
    }

    public void setCashPayamt(String cashPayamt) {
        this.cashPayamt = cashPayamt;
    }

    public String getBalc() {
        return balc;
    }

    public void setBalc(String balc) {
        this.balc = balc;
    }

    public String getAcctMulaidPay() {
        return acctMulaidPay;
    }

    public void setAcctMulaidPay(String acctMulaidPay) {
        this.acctMulaidPay = acctMulaidPay;
    }

    public String getMedinsSetlId() {
        return medinsSetlId;
    }

    public void setMedinsSetlId(String medinsSetlId) {
        this.medinsSetlId = medinsSetlId;
    }

    public String getRefdSetlFlag() {
        return refdSetlFlag;
    }

    public void setRefdSetlFlag(String refdSetlFlag) {
        this.refdSetlFlag = refdSetlFlag;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDiseCodg() {
        return diseCodg;
    }

    public void setDiseCodg(String diseCodg) {
        this.diseCodg = diseCodg;
    }

    public String getDiseName() {
        return diseName;
    }

    public void setDiseName(String diseName) {
        this.diseName = diseName;
    }

    public String getInvono() {
        return invono;
    }

    public void setInvono(String invono) {
        this.invono = invono;
    }

    public String getOpterId() {
        return opterId;
    }

    public void setOpterId(String opterId) {
        this.opterId = opterId;
    }

    public String getOpterName() {
        return opterName;
    }

    public void setOpterName(String opterName) {
        this.opterName = opterName;
    }

    public String getOptTime() {
        return optTime;
    }

    public void setOptTime(String optTime) {
        this.optTime = optTime;
    }
}