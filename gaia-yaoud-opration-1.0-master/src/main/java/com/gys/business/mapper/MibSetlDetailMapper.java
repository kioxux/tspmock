package com.gys.business.mapper;

import com.gys.business.mapper.entity.MibSetlDetail;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wavesen.shen
 */
@Mapper
public interface MibSetlDetailMapper extends BaseMapper<MibSetlDetail> {
}