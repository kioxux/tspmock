package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 消息模版—门店级别-职位关系表（门店人员设定）
 * </p>
 *
 * @author flynn
 * @since 2021-09-14
 */
@Data
@Table( name = "GAIA_MESSAGE_TEMPLE_STO_POSITION")
public class MessageTempleStoPosition implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    @Id
    @Column(name = "ID")
    private Integer id;

    @ApiModelProperty(value = "消息模版id")
    @Column(name = "GMT_ID")
    private String gmtId;

    @ApiModelProperty(value = "加盟商ID")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "门店ID")
    @Column(name = "SITE")
    private String site;

    @ApiModelProperty(value = "门店名")
    @Column(name = "SITE_NAME")
    private String siteName;

    @ApiModelProperty(value = "职位名")
    @Column(name = "POSITION_NAME")
    private String positionName;

    @ApiModelProperty(value = "职位id")
    @Column(name = "POSITION_ID")
    private String positionId;

    @ApiModelProperty(value = "消息接收人id")
    @Column(name = "RECEIVE_USER_ID")
    private String receiveUserId;

    @ApiModelProperty(value = "门店人员是否显示毛利数据1表示打勾 0表示未打勾")
    @Column(name = "GMT_IF_SHOW_MAO")
    private String gmtIfShowMao;

    @ApiModelProperty(value = "模块编码")
    @Column(name = "MODULE_CODE")
    private String moduleCode;

}
