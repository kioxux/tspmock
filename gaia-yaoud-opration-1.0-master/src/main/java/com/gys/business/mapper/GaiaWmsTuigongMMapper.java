package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaWmsTuigongM;
import com.gys.business.service.data.DtYb.ReturnExamime.ReturnExamimeVO;
import com.gys.business.service.data.YbInterfaceQueryData;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaWmsTuigongMMapper extends BaseMapper<GaiaWmsTuigongM> {
    List<ReturnExamimeVO> queryTuiGongByYLZ(YbInterfaceQueryData inData);
}