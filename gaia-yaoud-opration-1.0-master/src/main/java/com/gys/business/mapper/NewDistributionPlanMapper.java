package com.gys.business.mapper;

import com.gys.business.mapper.entity.NewDistributionPlan;
import com.gys.business.service.data.DistributionPlanForm;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 11:15
 */
public interface NewDistributionPlanMapper {

    NewDistributionPlan getById(Long id);

    int add(NewDistributionPlan newDistributionPlan);

    int update(NewDistributionPlan newDistributionPlan);

    List<NewDistributionPlan> findList(NewDistributionPlan cond);

    List<NewDistributionPlan> findPage(DistributionPlanForm distributionPlanForm);

    NewDistributionPlan getUnique(NewDistributionPlan cond);

    List<NewDistributionPlan> getExpireList(@Param("currentTime") String currentTime);

}
