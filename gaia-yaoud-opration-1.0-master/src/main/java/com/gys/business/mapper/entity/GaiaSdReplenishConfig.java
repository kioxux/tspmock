package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import javax.persistence.Transient;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 门店补货参数
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-18
 */
@Data
@Accessors(chain = true)
@Table(name = "GAIA_SD_REPLENISH_CONFIG")
public class GaiaSdReplenishConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    @Id
    @Column(name = "CLIENT")
    private String client;

    @Id
    @ApiModelProperty(value = "门店编码")
    @Column(name = "BR_ID")
    private String brId;

    @ApiModelProperty(value = "最小补货量验证，0为否，1为是")
    @Column(name = "MIN_QTY_FLAG")
    private String minQtyFlag;

    @ApiModelProperty(value = "最小补货量")
    @Column(name = "MIN_QTY")
    private BigDecimal minQty;

    @ApiModelProperty(value = "最大补货量验证，0为否，1为是")
    @Column(name = "MAX_QTY_FLAG")
    private String maxQtyFlag;

    @ApiModelProperty(value = "最大补货量")
    @Column(name = "MAX_QTY")
    private BigDecimal maxQty;

    @ApiModelProperty(value = "散装中药饮片是否按系统中包装补货，0为否，1为是")
    @Column(name = "MIDSIZE_MED_FLAG")
    private String midsizeMedFlag;

    @ApiModelProperty(value = "正常商品中包装系数")
    @Column(name = "MIDSIZE_NORM_RATIO")
    private String midsizeNormRatio;

    @ApiModelProperty(value = "null/0不控制，1删除行数百分比，2删除行数")
    @Column(name = "DEL_LINE_FLAG")
    private String delLineFlag;

    @ApiModelProperty(value = "删除行数百分比")
    @Column(name = "DEL_LINE_RATE")
    private String delLineRate;

    @ApiModelProperty(value = "删除行数")
    @Column(name = "DEL_LINE_QTY")
    private BigDecimal delLineQty;

    @Column(name = "ADD_LINE_RATE")
    private String addLineRate;

    @ApiModelProperty(value = "null/0不控制，1修改行数百分比，2修改行数")
    @Column(name = "REVISE_LINE_FLAG")
    private String reviseLineFlag;

    @ApiModelProperty(value = "修改行数百分比")
    @Column(name = "REVISE_LINE_RATE")
    private String reviseLineRate;

    @ApiModelProperty(value = "修改行数")
    @Column(name = "REVISE_LINE_QTY")
    private BigDecimal reviseLineQty;

    @ApiModelProperty(value = "null/0不控制，1修改单行数量百分比，2修改单行可减少数量")
    @Column(name = "REVISE_SINGLE_FLAG")
    private String reviseSingleFlag;

    @ApiModelProperty(value = "修改单行数量百分比")
    @Column(name = "REVISE_SINGLE_RATE")
    private String reviseSingleRate;

    @ApiModelProperty(value = "修改单行可减少数量")
    @Column(name = "REVISE_SINGLE_QTY")
    private BigDecimal reviseSingleQty;

    @Column(name = "REVISE_SINGLE_ADD_RATE")
    private String reviseSingleAddRate;

    @Transient
    private String gsrhVoucherId;


    public static final String GAIA_SD_REPLENISH_CONFIG_REMAIN = "GaiaSdReplenishConfigRemain";

    public static final String CLIENT = "client";

    public static final String BR_ID = "brId";

    public static final String MIN_QTY_FLAG = "MIN_QTY_FLAG";

    public static final String MIN_QTY = "MIN_QTY";

    public static final String MAX_QTY_FLAG = "MAX_QTY_FLAG";

    public static final String MAX_QTY = "MAX_QTY";

    public static final String MIDSIZE_MED_FLAG = "MIDSIZE_MED_FLAG";

    public static final String MIDSIZE_NORM_RATIO = "MIDSIZE_NORM_RATIO";

    public static final String DEL_LINE_FLAG = "DEL_LINE_FLAG";

    public static final String DEL_LINE_RATE = "DEL_LINE_RATE";

    public static final String DEL_LINE_QTY = "DEL_LINE_QTY";

    public static final String REVISE_LINE_FLAG = "REVISE_LINE_FLAG";

    public static final String REVISE_LINE_RATE = "REVISE_LINE_RATE";

    public static final String REVISE_LINE_QTY = "REVISE_LINE_QTY";

    public static final String REVISE_SINGLE_FLAG = "REVISE_SINGLE_FLAG";

    public static final String REVISE_SINGLE_RATE = "REVISE_SINGLE_RATE";

    public static final String REVISE_SINGLE_QTY = "REVISE_SINGLE_QTY";

    public static final String ADD_LINE_RATE = "ADD_LINE_RATE";

    public static final String REVISE_SINGLE_ADD_RATE = "REVISE_SINGLE_ADD_RATE";

}
