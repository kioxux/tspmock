//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRemoteAuditD;
import com.gys.business.service.data.PrescriptionDetailInfoOutData;
import com.gys.business.service.data.PrescriptionInfoInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdRemoteAuditDMapper extends BaseMapper<GaiaSdRemoteAuditD> {
    List<PrescriptionDetailInfoOutData> getPrescriptionDetailList(PrescriptionInfoInData inData);
}
