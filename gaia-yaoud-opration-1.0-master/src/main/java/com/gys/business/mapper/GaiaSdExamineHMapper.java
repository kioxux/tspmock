//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdExamineH;
import com.gys.business.service.data.GetExamineInData;
import com.gys.business.service.data.GetExamineOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdExamineHMapper extends BaseMapper<GaiaSdExamineH> {
    List<GetExamineOutData> selectList(GetExamineInData inData);

    String selectNextVoucherId(@Param("clientId") String clientId);

    List<GetExamineOutData>selectAcceptList(GetExamineInData inData);

    int selectExCount(@Param("clientId") String clientId,@Param("gsehVoucherAcceptId") String gsehVoucherAcceptId);
}
