package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "GAIA_SD_SERVER_TIPS")
@Data
public class GaiaSdServerTips implements Serializable {
    private static final long serialVersionUID = -5052722608378245455L;

    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSST_BR_ID")
    private String gsstBrId;

    @Id
    @Column(name = "GSST_ID")
    private String gsstId;

    @Column(name = "GSST_TPYE")
    private String gsstTpye;

    @Column(name = "GSST_REMARK")
    private String gsstRemark;

    @Column(name = "GSST_NAME")
    private String gsstName;

    @Column(name = "GSST_RELE_TPYE")
    private String gsstReleTpye;

    @Column(name = "GSST_FLAGE")
    private String gsstFlage;

    @Column(name = "GSST_FACTOR")
    private String gsstFactor;

    @Column(name = "GSST_PRIORITY")
    private Short gsstPriority;

    @Column(name = "GSST_EXPLAIN1")
    private String gsstExplain1;

    @Column(name = "GSST_EXPLAIN2")
    private String gsstExplain2;

    @Column(name = "GSST_EXPLAIN3")
    private String gsstExplain3;

    @Column(name = "GSST_EXPLAIN4")
    private String gsstExplain4;

    @Column(name = "GSST_EXPLAIN5")
    private String gsstExplain5;

    @Column(name = "GSST_UPDATE_DATE")
    private String gsstUpdateDate;

}
