package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
@Table(name = "GAIA_SD_PREORD_D")
@Data
public class GaiaSdPreordD implements Serializable {
    private static final long serialVersionUID = 2856758345472282776L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 订购门店
     */
    @Id
    @Column(name = "GSPD_BR_ID")
    private String gspdBrId;

    /**
     * 订购单号
     */
    @Id
    @Column(name = "GSPD_VOUCHER_ID")
    private String gspdVoucherId;

    /**
     * 订购日期
     */
    @Id
    @Column(name = "GSPD_DATE")
    private String gspdDate;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSPD_SERIL")
    private String gspdSeril;

    /**
     * 商品编码
     */
    @Column(name = "GSPD_PRO_ID")
    private String gspdProId;

    /**
     * 商品名称
     */
    @Column(name = "GSPD_PRO_NAME")
    private String gspdProName;

    /**
     * 通用名
     */
    @Column(name = "GSPD_PRO_COMMONNAME")
    private String gspdProCommonname;

    /**
     * 规格
     */
    @Column(name = "GSPD_PRO_SPECS")
    private String gspdProSpecs;

    /**
     * 厂家
     */
    @Column(name = "GSPD_PRO_FACTORY_NAME")
    private String gspdProFactoryName;

    /**
     * 单位
     */
    @Column(name = "GSPD_PRO_UNIT")
    private String gspdProUnit;

    /**
     * 条形码
     */
    @Column(name = "GSPD_PRO_BARCODE")
    private String gspdProBarcode;

    /**
     * 批准文号
     */
    @Column(name = "GSPD_PRO_REGISTER_NO")
    private String gspdProRegisterNo;

    /**
     * 参考零售价
     */
    @Column(name = "GSPD_PRO_LSJ")
    private BigDecimal gspdProLsj;

    /**
     * 数量
     */
    @Column(name = "GSPD_QTY")
    private BigDecimal gspdQty;

}