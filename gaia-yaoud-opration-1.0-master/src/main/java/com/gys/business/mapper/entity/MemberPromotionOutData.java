package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class MemberPromotionOutData implements Serializable {
    private String  voucherId;
    private String  type;
    private String  proId;
    private BigDecimal price;
    private String  rebate;
    private String  inteFlag;
    private String  inteRate;
}
