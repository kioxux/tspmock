//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_CLEANDRAWER_D"
)
public class GaiaSdCleandrawerD implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSCD_VOUCHER_ID"
    )
    private String gscdVoucherId;
    @Id
    @Column(
            name = "GSCD_DATE"
    )
    private String gscdDate;
    @Id
    @Column(
            name = "GSCD_SERIAL"
    )
    private String gscdSerial;
    @Column(
            name = "GSCD_PRO_ID"
    )
    private String gscdProId;
    @Column(
            name = "GSCD_BATCH_NO"
    )
    private String gscdBatchNo;
    @Column(
            name = "GSCD_BATCH"
    )
    private String gscdBatch;
    @Column(
            name = "GSCD_VALID_DATE"
    )
    private String gscdValidDate;
    @Column(
            name = "GSCD_QTY"
    )
    private String gscdQty;
    @Column(
            name = "GSCD_STATUS"
    )
    private String gscdStatus;
    private static final long serialVersionUID = 1L;

    public GaiaSdCleandrawerD() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGscdVoucherId() {
        return this.gscdVoucherId;
    }

    public void setGscdVoucherId(String gscdVoucherId) {
        this.gscdVoucherId = gscdVoucherId;
    }

    public String getGscdDate() {
        return this.gscdDate;
    }

    public void setGscdDate(String gscdDate) {
        this.gscdDate = gscdDate;
    }

    public String getGscdSerial() {
        return this.gscdSerial;
    }

    public void setGscdSerial(String gscdSerial) {
        this.gscdSerial = gscdSerial;
    }

    public String getGscdProId() {
        return this.gscdProId;
    }

    public void setGscdProId(String gscdProId) {
        this.gscdProId = gscdProId;
    }

    public String getGscdBatchNo() {
        return this.gscdBatchNo;
    }

    public void setGscdBatchNo(String gscdBatchNo) {
        this.gscdBatchNo = gscdBatchNo;
    }

    public String getGscdBatch() {
        return this.gscdBatch;
    }

    public void setGscdBatch(String gscdBatch) {
        this.gscdBatch = gscdBatch;
    }

    public String getGscdValidDate() {
        return this.gscdValidDate;
    }

    public void setGscdValidDate(String gscdValidDate) {
        this.gscdValidDate = gscdValidDate;
    }

    public String getGscdQty() {
        return this.gscdQty;
    }

    public void setGscdQty(String gscdQty) {
        this.gscdQty = gscdQty;
    }

    public String getGscdStatus() {
        return this.gscdStatus;
    }

    public void setGscdStatus(String gscdStatus) {
        this.gscdStatus = gscdStatus;
    }
}
