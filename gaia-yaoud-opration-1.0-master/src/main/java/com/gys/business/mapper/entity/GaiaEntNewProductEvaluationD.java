package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 公司-新品评估-明细表(GaiaEntNewProductEvaluationD)实体类
 *
 * @author makejava
 * @since 2021-07-19 10:16:25
 */
public class GaiaEntNewProductEvaluationD implements Serializable {
    private static final long serialVersionUID = 796644975143025513L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 商品评估单号
     */
    private String billCode;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 销售额
     */
    private Double salesAmt;
    /**
     * 销量
     */
    private Double salesQuantity;
    /**
     * 毛利额
     */
    private Double gross;
    /**
     * 铺货门店数
     */
    private int storeQty;
    /**
     * 动销门店数
     */
    private int salesStoreQty;
    /**
     * 在库门店数
     */
    private int invStoreQty;
    /**
     * 建议处置状态：0-淘汰 1-转正常
     */
    private Integer suggestedStatus;
    /**
     * 商品状态：0-淘汰 1-转正常
     */
    private Integer status;
    /**
     * 处理状态：0-未处理 1-已处理
     */
    private Integer dealStatus;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;

    public int getInvStoreQty() {
        return invStoreQty;
    }

    public void setInvStoreQty(int invStoreQty) {
        this.invStoreQty = invStoreQty;
    }

    public int getStoreQty() {
        return storeQty;
    }

    public void setStoreQty(int storeQty) {
        this.storeQty = storeQty;
    }

    public int getSalesStoreQty() {
        return salesStoreQty;
    }

    public void setSalesStoreQty(int salesStoreQty) {
        this.salesStoreQty = salesStoreQty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getProSelfCode() {
        return proSelfCode;
    }

    public void setProSelfCode(String proSelfCode) {
        this.proSelfCode = proSelfCode;
    }

    public Double getSalesAmt() {
        return salesAmt;
    }

    public void setSalesAmt(Double salesAmt) {
        this.salesAmt = salesAmt;
    }

    public Double getSalesQuantity() {
        return salesQuantity;
    }

    public void setSalesQuantity(Double salesQuantity) {
        this.salesQuantity = salesQuantity;
    }

    public Double getGross() {
        return gross;
    }

    public void setGross(Double gross) {
        this.gross = gross;
    }

    public Integer getSuggestedStatus() {
        return suggestedStatus;
    }

    public void setSuggestedStatus(Integer suggestedStatus) {
        this.suggestedStatus = suggestedStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDealStatus() {
        return dealStatus;
    }

    public void setDealStatus(Integer dealStatus) {
        this.dealStatus = dealStatus;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}
