package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromSeriesConds;
import com.gys.business.service.data.PromSeriesCondsOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromSeriesCondsMapper extends BaseMapper<GaiaSdPromSeriesConds> {
    List<PromSeriesCondsOutData> getDetail(PromoteInData inData);

    void insertToList(List<GaiaSdPromSeriesConds> condsList);

    List<GaiaSdPromSeriesConds> getAllByClient(@Param("client") String client,@Param("nowDate") String nowDate,@Param("brId")String brId);
}
