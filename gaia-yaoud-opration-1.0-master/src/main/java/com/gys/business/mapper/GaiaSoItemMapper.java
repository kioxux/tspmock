//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSoItem;
import com.gys.business.service.data.GetReplenishCountInData;
import com.gys.business.service.data.GetReplenishInData;
import com.gys.business.service.data.GetReplenishOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSoItemMapper extends BaseMapper<GaiaSoItem> {
    List<GetReplenishOutData> selectCountByDate(GetReplenishInData inData);

    List<GetReplenishOutData> selectCountList(GetReplenishInData inData);

    List<GetReplenishOutData>selectCountByProIds(GetReplenishCountInData inData);

    List<GetReplenishOutData>selectCountByDcCode(GetReplenishCountInData inData);
}
