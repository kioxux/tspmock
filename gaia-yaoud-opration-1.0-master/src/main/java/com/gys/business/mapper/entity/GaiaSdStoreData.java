package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "GAIA_SD_STORE_DATA")
@Data
public class GaiaSdStoreData implements Serializable {
    private static final long serialVersionUID = 2991508159180589786L;
    @Id
    @Column(name = "CLIENT")
    private String clientId;
    @Id
    @Column(name = "GSST_BR_ID")
    private String gsstBrId;

    @Column(name = "GSST_BR_NAME")
    private String gsstBrName;

    @Column(name = "GSST_SALE_MAX_QTY")
    private String gsstSaleMaxQty;

    @Column(name = "GSST_LAST_DAY")
    private String gsstLastDay;

    @Column(name = "GSST_PD_START_DATE")
    private String gsstPdStartDate;

    @Column(name = "GSST_VERSION")
    private String gsstVersion;

    @Column(name = "GSST_MIN_DISCOUNT_RATE")
    private String gsstMinDiscountRate;

    @Column(name = "GSST_DYQ_MAX_AMT")
    private BigDecimal gsstDyqMaxAmt;

    @Column(name = "GSST_DAILY_OPEN_DATE")
    private String gsstDailyOpenDate;

    @Column(name = "GSST_DAILY_CHANGE_DATE")
    private String gsstDailyChangeDate;

    @Column(name = "GSST_DAILY_CLOSE_DATE")
    private String gsstDailyCloseDate;

    @Column(name = "GSST_JF_QLDATE")
    private String gsstJfQldate;

    @Column(name = "GSST_MEM_NEW_MIX")
    private String gsstMemNewMix;

    @Column(name = "GSST_MEN_NEW_AUTO")
    private String gsstMenNewAuto;

    @Column(name = "GSST_PS_DATE")
    private String gsstPsDate;

    @Column(name = "GSST_ORG_TYPE")
    private String gsstOrgType;

    @Column(name = "GSST_ORG_ID")
    private String gsstOrgId;

}
