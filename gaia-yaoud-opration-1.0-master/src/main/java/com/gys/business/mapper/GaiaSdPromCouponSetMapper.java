//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromCouponSet;
import com.gys.business.service.data.PromCouponSetOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdPromCouponSetMapper extends BaseMapper<GaiaSdPromCouponSet> {
    List<PromCouponSetOutData> getDetail(PromoteInData inData);
}
