package com.gys.business.mapper.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhangdong
 * @date 2021/3/31 18:34
 */
@AllArgsConstructor
@Data
public class DateTimeDto implements Serializable {
    private String dateTime;
}