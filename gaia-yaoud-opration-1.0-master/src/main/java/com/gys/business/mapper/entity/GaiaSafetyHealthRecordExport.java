package com.gys.business.mapper.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * GAIA_SAFETY_HEALTH_RECORD
 * @author 
 */
@Data
public class GaiaSafetyHealthRecordExport {

    /**
     * 序号
     */
    @ExcelProperty(value = "序号",index = 0)
    private Integer seq;

    /**
     * 状态：0-未完成 1-完成
     */
    @ExcelProperty(value = "状态",index = 1)
    private String status;

    /**
     * 检查日期
     */
    @ExcelProperty(value = "日期",index = 2)
    private String checkDate;

    /**
     * 健康检查情况
     */
    @ExcelProperty(value = "卫生检查情况",index = 3)
    private String healthCheck;

    /**
     * 安全检查情况
     */
    @ExcelProperty(value = "安全检查情况",index = 4)
    private String safetyCheck;

    /**
     * 负责人姓名
     */
    @ExcelProperty(value = "负责人",index = 5)
    private String masterName;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注",index = 6)
    private String remark;
}