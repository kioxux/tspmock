//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SUPPLIER_BASIC"
)
public class GaiaSupplierBasic implements Serializable {
    @Id
    @Column(
            name = "SUP_CODE"
    )
    private String supCode;
    @Column(
            name = "SUP_PYM"
    )
    private String supPym;
    @Column(
            name = "SUP_NAME"
    )
    private String supName;
    @Column(
            name = "SUP_CREDIT_CODE"
    )
    private String supCreditCode;
    @Column(
            name = "SUP_CREDIT_DATE"
    )
    private String supCreditDate;
    @Column(
            name = "SUP_CLASS"
    )
    private String supClass;
    @Column(
            name = "SUP_LEGAL_PERSON"
    )
    private String supLegalPerson;
    @Column(
            name = "SUP_REG_ADD"
    )
    private String supRegAdd;
    @Column(
            name = "SUP_STATUS"
    )
    private String supStatus;
    @Column(
            name = "SUP_LICENCE_NO"
    )
    private String supLicenceNo;
    @Column(
            name = "SUP_LICENCE_DATE"
    )
    private String supLicenceDate;
    @Column(
            name = "SUP_LICENCE_VALID"
    )
    private String supLicenceValid;
    @Column(
            name = "SUP_SCOPE"
    )
    private String supScope;
    private static final long serialVersionUID = 1L;

    public GaiaSupplierBasic() {
    }

    public String getSupCode() {
        return this.supCode;
    }

    public void setSupCode(String supCode) {
        this.supCode = supCode;
    }

    public String getSupPym() {
        return this.supPym;
    }

    public void setSupPym(String supPym) {
        this.supPym = supPym;
    }

    public String getSupName() {
        return this.supName;
    }

    public void setSupName(String supName) {
        this.supName = supName;
    }

    public String getSupCreditCode() {
        return this.supCreditCode;
    }

    public void setSupCreditCode(String supCreditCode) {
        this.supCreditCode = supCreditCode;
    }

    public String getSupCreditDate() {
        return this.supCreditDate;
    }

    public void setSupCreditDate(String supCreditDate) {
        this.supCreditDate = supCreditDate;
    }

    public String getSupClass() {
        return this.supClass;
    }

    public void setSupClass(String supClass) {
        this.supClass = supClass;
    }

    public String getSupLegalPerson() {
        return this.supLegalPerson;
    }

    public void setSupLegalPerson(String supLegalPerson) {
        this.supLegalPerson = supLegalPerson;
    }

    public String getSupRegAdd() {
        return this.supRegAdd;
    }

    public void setSupRegAdd(String supRegAdd) {
        this.supRegAdd = supRegAdd;
    }

    public String getSupStatus() {
        return this.supStatus;
    }

    public void setSupStatus(String supStatus) {
        this.supStatus = supStatus;
    }

    public String getSupLicenceNo() {
        return this.supLicenceNo;
    }

    public void setSupLicenceNo(String supLicenceNo) {
        this.supLicenceNo = supLicenceNo;
    }

    public String getSupLicenceDate() {
        return this.supLicenceDate;
    }

    public void setSupLicenceDate(String supLicenceDate) {
        this.supLicenceDate = supLicenceDate;
    }

    public String getSupLicenceValid() {
        return this.supLicenceValid;
    }

    public void setSupLicenceValid(String supLicenceValid) {
        this.supLicenceValid = supLicenceValid;
    }

    public String getSupScope() {
        return this.supScope;
    }

    public void setSupScope(String supScope) {
        this.supScope = supScope;
    }
}
