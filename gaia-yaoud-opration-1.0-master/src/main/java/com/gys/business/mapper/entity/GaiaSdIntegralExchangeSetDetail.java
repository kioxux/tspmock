package com.gys.business.mapper.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @TableName GAIA_SD_INTEGRAL_EXCHANGE_SET_DETAIL
 */
@Data
public class GaiaSdIntegralExchangeSetDetail implements Serializable {
    /**
     * 
     */
    @ExcelProperty(value = "序号",index = 1)
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String voucherId;

    /**
     * 门店ID
     */
    private String stoCode;

    /**
     * 商品编码
     */
    @ExcelProperty(value = "商品编码",index = 2)
    private String proSelfCode;

    /**
     * 拼音码
     */
    private String pym;

    /**
     * 买满金额
     */
    @ExcelProperty(value = "买满金额",index = 3)
    private BigDecimal buyAmt;

    /**
     * 所需积分
     */
    @ExcelProperty(value = "所需积分",index = 4)
    private BigDecimal exchangeIntegra;

    /**
     * 换购金额
     */
    @ExcelProperty(value = "换购金额",index = 5)
    private BigDecimal exchangeAmt;

    /**
     * 兑换数量
     */
    @ExcelProperty(value = "兑换数量",index = 6)
    private BigDecimal exchangeQty;

    /**
     * 限兑数量
     */
    @ExcelProperty(value = "限兑数量",index = 7)
    private BigDecimal periodMaxQty;

    /**
     * 期间限兑（购）天数
     */
    @ExcelProperty(value = "期间限兑（购）天数",index = 8)
    private BigDecimal periodDays;

    /**
     * 本商品是否积分（1-是、0-否）
     */
    @ExcelProperty(value = "本商品是否积分（1-是、0-否）",index = 9)
    private String integralFlag;

    /**
     * 是否删除（1-是、0-否）
     */
    @ExcelProperty(value = "是否删除（1-是、0-否）",index = 10)
    private String deleteFlag;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateUser;
}