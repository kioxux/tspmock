package com.gys.business.mapper.entity;

import lombok.Data;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/15 14:57
 **/
@Data
public class AllotPrice {
    private Long id;
    /**收货地点*/
    private String alpReceiveSite;
    /**商品编码*/
    private String alpProCode;
    /**加盟商*/
    private String client;
    /**加价金额*/
    private BigDecimal alpAddAmt;
    /**加价比例*/
    private BigDecimal alpAddRate;
    /**目录价*/
    private BigDecimal alpCataloguePrice;
    private String alpCreateBy;
    private String alpCreateDate;
    private String alpCreateTime;
    private String alpUpdateBy;
    private String alpUpdateDate;
    private String alpUpdateTime;
}
