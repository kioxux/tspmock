//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdEmpGroup;
import com.gys.business.service.data.GetLoginStoreInData;
import com.gys.business.service.data.GetSaleScheduleInData;
import com.gys.business.service.data.GetSaleScheduleOutData;
import com.gys.business.service.data.GetSaleScheduleQueryInData;
import com.gys.common.base.BaseMapper;
import java.util.List;

public interface GaiaSdEmpGroupMapper extends BaseMapper<GaiaSdEmpGroup> {
    List<GetSaleScheduleOutData> selectByPage(GetSaleScheduleQueryInData inData);

    List<GetSaleScheduleOutData> selectAllEmpGroup(GetSaleScheduleQueryInData inData);

    void update(GetSaleScheduleInData inData);

    void deleteData(GetSaleScheduleInData inData);

    void addSave(GetSaleScheduleInData inData);

    List<String> checkChange(GetLoginStoreInData inData);

    String selectMaxId(GetSaleScheduleInData inData);
}
