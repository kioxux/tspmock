package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromHySet;
import com.gys.business.mapper.entity.MemberPromotionInData;
import com.gys.business.mapper.entity.MemberPromotionOutData;
import com.gys.business.service.data.PromHySetOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromHySetMapper extends BaseMapper<GaiaSdPromHySet> {

    List<PromHySetOutData> getDetail(PromoteInData inData);

    List<MemberPromotionOutData> findMemberPromotion(MemberPromotionInData inData);

    List<GaiaSdPromHySet> getAllByClient(@Param("client") String client, @Param("nowDate") String nowDate,@Param("brId")String brId);
}
