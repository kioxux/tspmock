package com.gys.business.mapper;

import com.gys.business.mapper.entity.DcOutStockStoreH;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/25 15:26
 */
public interface DcOutStockStoreHMapper {

    DcOutStockStoreH getById(Long id);

    int add(DcOutStockStoreH dcOutStockStoreH);

    DcOutStockStoreH getUnique(DcOutStockStoreH cond);
}
