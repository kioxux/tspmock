package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "GAIA_SD_REPLENISH_D")
@Data
public class GaiaSdReplenishD implements Serializable {
    private static final long serialVersionUID = 7507079604892002475L;

    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    /**
     * 补货单号
     */
    @Id
    @Column(name = "GSRD_VOUCHER_ID")
    private String gsrdVoucherId;

    /**
     * 补货日期
     */
    @Id
    @Column(name = "GSRD_DATE")
    private String gsrdDate;

    /**
     * 行号
     */
    @Id
    @Column(name = "GSRD_SERIAL")
    private String gsrdSerial;

    /**
     * 补货门店
     */
    @Id
    @Column(name = "GSRD_BR_ID")
    private String gsrdBrId;

    /**
     * 商品编码
     */
    @Column(name = "GSRD_PRO_ID")
    private String gsrdProId;

    /**
     * 商品批次
     */
    @Column(name = "GSRD_BATCH")
    private String gsrdBatch;

    /**
     * 商品批号
     */
    @Column(name ="GSRD_BATCH_NO")
    private String gsrdBatchNo;

    /**
     * 毛利率
     */
    @Column(name = "GSRD_GROSS_MARGIN")
    private String gsrdGrossMargin;

    /**
     * 90天销量
     */
    @Column(name = "GSRD_SALES_DAYS1")
    private String gsrdSalesDays1;

    /**
     * 30天销量
     */
    @Column(name = "GSRD_SALES_DAYS2")
    private String gsrdSalesDays2;

    /**
     * 7天销量
     */
    @Column(name = "GSRD_SALES_DAYS3")
    private String gsrdSalesDays3;

    /**
     * 建议补货量
     */
    @Column(name = "GSRD_PROPOSE_QTY")
    private String gsrdProposeQty;

    /**
     * 本店库存
     */
    @Column(name = "GSRD_STOCK_STORE")
    private String gsrdStockStore;

    /**
     * 仓库库存
     */
    @Column(name = "GSRD_STOCK_DEPOT")
    private String gsrdStockDepot;

    /**
     * 最小陈列
     */
    @Column(name = "GSRD_DISPLAY_MIN")
    private String gsrdDisplayMin;

    /**
     * 中包装
     */
    @Column(name = "GSRD_PACK_MIDSIZE")
    private String gsrdPackMidsize;

    /**
     * 最后一次供应商
     */
    @Column(name = "GSRD_LAST_SUPID")
    private String gsrdLastSupid;

    /**
     * 最后一次价格
     */
    @Column(name = "GSRD_LAST_PRICE")
    private String gsrdLastPrice;

    /**
     * 修改后价格
     */
    @Column(name = "GSRD_EDIT_PRICE")
    private String gsrdEditPrice;

    /**
     * 补货量
     */
    @Column(name = "GSRD_NEED_QTY")
    private String gsrdNeedQty;

    /**
     * 订单价
     */
    @Column(name = "GSRD_VOUCHER_AMT")
    private BigDecimal gsrdVoucherAmt;

    /**
     * 移动平均价
     */
    @Column(name = "GSRD_AVERAGE_COST")
    private BigDecimal gsrdAverageCost;

    /**
     * 税率
     */
    @Column(name = "GSRD_TAX_RATE")
    private String gsrdTaxRate;

    @Column(name = "GSRD_FLAG")
    private String gsrdFlag;

    @Column(name = "GSSP_PRICE_NORMAL")
    private BigDecimal gsrdProPrice;

    @Column(name = "GSRD_DELETE_FLAG")
    private Integer gsrdDeleteFlag;

    @Column(name = "GSRD_SEND_PRICE")
    private BigDecimal gsrdSendPrice;

    @Column(name = "GSRD_IS_EDIT_ORIGINAL")
    private Boolean gsrdIsEditOriginal;

    @Column(name = "GSRD_IS_GIFT")
    private String gsrdIsGift;

}
