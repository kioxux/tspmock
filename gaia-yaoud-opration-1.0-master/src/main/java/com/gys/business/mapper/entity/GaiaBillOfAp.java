package com.gys.business.mapper.entity;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 供应商应付明细清单表
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Data
@Accessors(chain = true)
@Table( name = "GAIA_BILL_OF_AP")
public class GaiaBillOfAp implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "加盟商")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "DC编码")
    @Column(name = "DC_CODE")
    private String dcCode;

    @ApiModelProperty(value = "应付方式编码")
    @Column(name = "PAYMENT_ID")
    private String paymentId;

    @ApiModelProperty(value = "发生金额")
    @Column(name = "AMOUNT_OF_PAYMENT")
    private BigDecimal amountOfPayment;

    @Transient
    private String amountOfPaymentStr;

    public String getAmountOfPaymentStr() {
        String res = "0.00";
        if(amountOfPayment!=null){
            res = amountOfPayment.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
        }
        return res;
    }

    public BigDecimal getAmountOfPayment() {
        BigDecimal a = BigDecimal.ZERO;
        if(amountOfPayment!=null){
            a = amountOfPayment.setScale(2, BigDecimal.ROUND_HALF_UP);
        }
        return a;
    }

    @ApiModelProperty(value = "去税金额")
    @Column(name = "AMOUNT_EXCLUDING_TAX")
    private BigDecimal amountExcludingTax;

    @ApiModelProperty(value = "税额")
    @Column(name = "AMOUNT_OF_TAX")
    private BigDecimal amountOfTax;

    @ApiModelProperty(value = "备注")
    @Column(name = "REMARKS")
    private String remarks;

    @ApiModelProperty(value = "发生日期")
    @Column(name = "PAYMENT_DATE")
    private String paymentDate;

    @ApiModelProperty(value = "供应商自编码")
    @Column(name = "SUP_SELF_CODE")
    private String supSelfCode;

    @ApiModelProperty(value = "供应商编码")
    @Column(name = "SUP_CODE")
    private String supCode;

    @ApiModelProperty(value = "供应商名称")
    @Column(name = "SUP_NAME")
    private String supName;

    @ApiModelProperty(value = "业务员")
    @Column(name = "SALESMAN_NAME")
    private String salesmanName;

    @ApiModelProperty(value = "业务员ID")
    @Column(name = "SALESMAN_ID")
    private String salesmanId;


    public static final String CLIENT = "CLIENT";

    public static final String DC_CODE = "DC_CODE";

    public static final String PAYMENT_ID = "PAYMENT_ID";

    public static final String AMOUNT_OF_PAYMENT = "AMOUNT_OF_PAYMENT";

    public static final String AMOUNT_EXCLUDING_TAX = "AMOUNT_EXCLUDING_TAX";

    public static final String AMOUNT_OF_TAX = "AMOUNT_OF_TAX";

    public static final String REMARKS = "REMARKS";

    public static final String PAYMENT_DATE = "PAYMENT_DATE";

    public static final String SUP_SELF_CODE = "SUP_SELF_CODE";

    public static final String SUP_CODE = "SUP_CODE";

    public static final String SUP_NAME = "SUP_NAME";

}
