package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**

 * @author xiayuan
 * 电子券主题设置表
 */
@Data
public class GaiaSdElectronThemeSet implements Serializable {

    private static final long serialVersionUID = 7988169360945750677L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 主题单号
     */
    private String gsetsId;

    /**
     * 主题属性
     */
    private String gsetsFlag;

    /**
     * 主题描述
     */
    private String gsetsName;

    /**
     * 创建日期
     */
    private String gsetsCreateDate;

    /**
     * 创建时间
     */
    private String gsetsCreateTime;

    /**
     * 创建人
     */
    private String gsetsCreateEmp;

    /**
     * 最大赠送次数
     */
    private String gsetsMaxQty;

    /**
     * 已赠送次数
     */
    private String gsetsQty;

    /**
     * 是否有效
     */
    private String gsetsValid;

}