//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdDailyReconcile;
import com.gys.business.service.data.*;
import com.gys.common.base.BaseMapper;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface GaiaDailyReconcileMapper extends BaseMapper<GaiaSdDailyReconcile> {
    List<DailyReconcileOutData> getDailyReconcile(DailyReconcileInData inData);

    String selectNextVoucherId(@Param("clientId") String clientId);

    List<SaleReturnOutData> getSaleInfoList(DailyReconcileInData inData);

    List<RechargeCardPaycheckOutData> getRechargeInfoList(DailyReconcileInData inData);

//    List<DailyReconcileDetailNewOutData> querySaleAndRechargeInfoList(DailyReconcileInData inData);

    List<DailyReconcileDetailNewOutData>getDailyInfoList(DailyReconcileInData inData);

    void inserts(GaiaSdDailyReconcile reconcile);
}
