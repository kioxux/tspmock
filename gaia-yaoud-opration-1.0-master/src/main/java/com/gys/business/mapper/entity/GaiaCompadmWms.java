package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "GAIA_COMPADM_WMS")
public class GaiaCompadmWms implements Serializable {
    /**
     * 加盟商ID
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 批发公司ID
     */
    @Id
    @Column(name = "COMPADM_ID")
    private String compadmId;

    /**
     * 批发公司名称
     */
    @Column(name = "COMPADM_NAME")
    private String compadmName;

    /**
     * 统一社会信用代码
     */
    @Column(name = "COMPADM_NO")
    private String compadmNo;

    /**
     * 法人/负责人
     */
    @Column(name = "COMPADM_LEGAL_PERSON")
    private String compadmLegalPerson;

    /**
     * 质量负责人
     */
    @Column(name = "COMPADM_QUA")
    private String compadmQua;

    /**
     * 详细地址
     */
    @Column(name = "COMPADM_ADDR")
    private String compadmAddr;

    /**
     * 创建日期
     */
    @Column(name = "COMPADM__CRE_DATE")
    private String compadmCreDate;

    /**
     * 创建时间
     */
    @Column(name = "COMPADM__CRE_TIME")
    private String compadmCreTime;

    /**
     * 创建人账号
     */
    @Column(name = "COMPADM__CRE_ID")
    private String compadmCreId;

    /**
     * 修改日期
     */
    @Column(name = "COMPADM__MODI_DATE")
    private String compadmModiDate;

    /**
     * 修改时间
     */
    @Column(name = "COMPADM__MODI_TIME")
    private String compadmModiTime;

    /**
     * 修改人账号
     */
    @Column(name = "COMPADM__MODI_ID")
    private String compadmModiId;

    /**
     * LOGO地址
     */
    @Column(name = "COMPADM__LOGO")
    private String compadmLogo;

    /**
     * 批发公司状态 N停用 Y启用
     */
    @Column(name = "COMPADM_STATUS")
    private String compadmStatus;

    private static final long serialVersionUID = 1L;

    /**
     * 获取加盟商ID
     *
     * @return CLIENT - 加盟商ID
     */
    public String getClient() {
        return client;
    }

    /**
     * 设置加盟商ID
     *
     * @param client 加盟商ID
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * 获取批发公司ID
     *
     * @return COMPADM_ID - 批发公司ID
     */
    public String getCompadmId() {
        return compadmId;
    }

    /**
     * 设置批发公司ID
     *
     * @param compadmId 批发公司ID
     */
    public void setCompadmId(String compadmId) {
        this.compadmId = compadmId;
    }

    /**
     * 获取批发公司名称
     *
     * @return COMPADM_NAME - 批发公司名称
     */
    public String getCompadmName() {
        return compadmName;
    }

    /**
     * 设置批发公司名称
     *
     * @param compadmName 批发公司名称
     */
    public void setCompadmName(String compadmName) {
        this.compadmName = compadmName;
    }

    /**
     * 获取统一社会信用代码
     *
     * @return COMPADM_NO - 统一社会信用代码
     */
    public String getCompadmNo() {
        return compadmNo;
    }

    /**
     * 设置统一社会信用代码
     *
     * @param compadmNo 统一社会信用代码
     */
    public void setCompadmNo(String compadmNo) {
        this.compadmNo = compadmNo;
    }

    /**
     * 获取法人/负责人
     *
     * @return COMPADM_LEGAL_PERSON - 法人/负责人
     */
    public String getCompadmLegalPerson() {
        return compadmLegalPerson;
    }

    /**
     * 设置法人/负责人
     *
     * @param compadmLegalPerson 法人/负责人
     */
    public void setCompadmLegalPerson(String compadmLegalPerson) {
        this.compadmLegalPerson = compadmLegalPerson;
    }

    /**
     * 获取质量负责人
     *
     * @return COMPADM_QUA - 质量负责人
     */
    public String getCompadmQua() {
        return compadmQua;
    }

    /**
     * 设置质量负责人
     *
     * @param compadmQua 质量负责人
     */
    public void setCompadmQua(String compadmQua) {
        this.compadmQua = compadmQua;
    }

    /**
     * 获取详细地址
     *
     * @return COMPADM_ADDR - 详细地址
     */
    public String getCompadmAddr() {
        return compadmAddr;
    }

    /**
     * 设置详细地址
     *
     * @param compadmAddr 详细地址
     */
    public void setCompadmAddr(String compadmAddr) {
        this.compadmAddr = compadmAddr;
    }

    /**
     * 获取创建日期
     *
     * @return COMPADM__CRE_DATE - 创建日期
     */
    public String getCompadmCreDate() {
        return compadmCreDate;
    }

    /**
     * 设置创建日期
     *
     * @param compadmCreDate 创建日期
     */
    public void setCompadmCreDate(String compadmCreDate) {
        this.compadmCreDate = compadmCreDate;
    }

    /**
     * 获取创建时间
     *
     * @return COMPADM__CRE_TIME - 创建时间
     */
    public String getCompadmCreTime() {
        return compadmCreTime;
    }

    /**
     * 设置创建时间
     *
     * @param compadmCreTime 创建时间
     */
    public void setCompadmCreTime(String compadmCreTime) {
        this.compadmCreTime = compadmCreTime;
    }

    /**
     * 获取创建人账号
     *
     * @return COMPADM__CRE_ID - 创建人账号
     */
    public String getCompadmCreId() {
        return compadmCreId;
    }

    /**
     * 设置创建人账号
     *
     * @param compadmCreId 创建人账号
     */
    public void setCompadmCreId(String compadmCreId) {
        this.compadmCreId = compadmCreId;
    }

    /**
     * 获取修改日期
     *
     * @return COMPADM__MODI_DATE - 修改日期
     */
    public String getCompadmModiDate() {
        return compadmModiDate;
    }

    /**
     * 设置修改日期
     *
     * @param compadmModiDate 修改日期
     */
    public void setCompadmModiDate(String compadmModiDate) {
        this.compadmModiDate = compadmModiDate;
    }

    /**
     * 获取修改时间
     *
     * @return COMPADM__MODI_TIME - 修改时间
     */
    public String getCompadmModiTime() {
        return compadmModiTime;
    }

    /**
     * 设置修改时间
     *
     * @param compadmModiTime 修改时间
     */
    public void setCompadmModiTime(String compadmModiTime) {
        this.compadmModiTime = compadmModiTime;
    }

    /**
     * 获取修改人账号
     *
     * @return COMPADM__MODI_ID - 修改人账号
     */
    public String getCompadmModiId() {
        return compadmModiId;
    }

    /**
     * 设置修改人账号
     *
     * @param compadmModiId 修改人账号
     */
    public void setCompadmModiId(String compadmModiId) {
        this.compadmModiId = compadmModiId;
    }

    /**
     * 获取LOGO地址
     *
     * @return COMPADM__LOGO - LOGO地址
     */
    public String getCompadmLogo() {
        return compadmLogo;
    }

    /**
     * 设置LOGO地址
     *
     * @param compadmLogo LOGO地址
     */
    public void setCompadmLogo(String compadmLogo) {
        this.compadmLogo = compadmLogo;
    }

    /**
     * 获取批发公司状态 N停用 Y启用
     *
     * @return COMPADM_STATUS - 批发公司状态 N停用 Y启用
     */
    public String getCompadmStatus() {
        return compadmStatus;
    }

    /**
     * 设置批发公司状态 N停用 Y启用
     *
     * @param compadmStatus 批发公司状态 N停用 Y启用
     */
    public void setCompadmStatus(String compadmStatus) {
        this.compadmStatus = compadmStatus;
    }
}