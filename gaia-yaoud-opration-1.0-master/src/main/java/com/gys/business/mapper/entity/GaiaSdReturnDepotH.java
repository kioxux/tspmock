//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_RETURN_DEPOT_H"
)
public class GaiaSdReturnDepotH implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSRDH_VOUCHER_ID"
    )
    private String gsrdhVoucherId;
    @Column(
            name = "GSRDH_BR_ID"
    )
    private String gsrdhBrId;
    @Column(
            name = "GSRDH_DATE"
    )
    private String gsrdhDate;
    @Column(
            name = "GSRDH_FINISH_DATE"
    )
    private String gsrdhFinishDate;
    @Column(
            name = "GSRDH_FROM"
    )
    private String gsrdhFrom;
    @Column(
            name = "GSRDH_TO"
    )
    private String gsrdhTo;
    @Column(
            name = "GSRDH_TYPE"
    )
    private String gsrdhType;
    @Column(
            name = "GSRDH_STATUS"
    )
    private String gsrdhStatus;
    @Column(
            name = "GSRDH_TOTAL_AMT"
    )
    private BigDecimal gsrdhTotalAmt;
    @Column(
            name = "GSRDH_TOTAL_QTY"
    )
    private String gsrdhTotalQty;
    @Column(
            name = "GSRDH_EMP"
    )
    private String gsrdhEmp;
    @Column(
            name = "GSRDH_REMAKS"
    )
    private String gsrdhRemaks;
    @Column(
            name = "GSRDH_PATTERN"
    )
    private String gsrdhPattern;
    @Column(
            name = "GSRDH_PROCEDURE"
    )
    private String gsrdhProcedure;
    @Column(
            name = "GSRDH_RECALL_VOUCHER_ID"
    )
    private String gsrdhRecallVoucherId;
    private static final long serialVersionUID = 1L;

    public GaiaSdReturnDepotH() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsrdhVoucherId() {
        return this.gsrdhVoucherId;
    }

    public void setGsrdhVoucherId(String gsrdhVoucherId) {
        this.gsrdhVoucherId = gsrdhVoucherId;
    }

    public String getGsrdhBrId() {
        return this.gsrdhBrId;
    }

    public void setGsrdhBrId(String gsrdhBrId) {
        this.gsrdhBrId = gsrdhBrId;
    }

    public String getGsrdhDate() {
        return this.gsrdhDate;
    }

    public void setGsrdhDate(String gsrdhDate) {
        this.gsrdhDate = gsrdhDate;
    }

    public String getGsrdhFinishDate() {
        return this.gsrdhFinishDate;
    }

    public void setGsrdhFinishDate(String gsrdhFinishDate) {
        this.gsrdhFinishDate = gsrdhFinishDate;
    }

    public String getGsrdhFrom() {
        return this.gsrdhFrom;
    }

    public void setGsrdhFrom(String gsrdhFrom) {
        this.gsrdhFrom = gsrdhFrom;
    }

    public String getGsrdhTo() {
        return this.gsrdhTo;
    }

    public void setGsrdhTo(String gsrdhTo) {
        this.gsrdhTo = gsrdhTo;
    }

    public String getGsrdhType() {
        return this.gsrdhType;
    }

    public void setGsrdhType(String gsrdhType) {
        this.gsrdhType = gsrdhType;
    }

    public String getGsrdhStatus() {
        return this.gsrdhStatus;
    }

    public void setGsrdhStatus(String gsrdhStatus) {
        this.gsrdhStatus = gsrdhStatus;
    }

    public BigDecimal getGsrdhTotalAmt() {
        return this.gsrdhTotalAmt;
    }

    public void setGsrdhTotalAmt(BigDecimal gsrdhTotalAmt) {
        this.gsrdhTotalAmt = gsrdhTotalAmt;
    }

    public String getGsrdhTotalQty() {
        return this.gsrdhTotalQty;
    }

    public void setGsrdhTotalQty(String gsrdhTotalQty) {
        this.gsrdhTotalQty = gsrdhTotalQty;
    }

    public String getGsrdhEmp() {
        return this.gsrdhEmp;
    }

    public void setGsrdhEmp(String gsrdhEmp) {
        this.gsrdhEmp = gsrdhEmp;
    }

    public String getGsrdhRemaks() {
        return this.gsrdhRemaks;
    }

    public void setGsrdhRemaks(String gsrdhRemaks) {
        this.gsrdhRemaks = gsrdhRemaks;
    }

    public String getGsrdhPattern() {
        return gsrdhPattern;
    }

    public void setGsrdhPattern(String gsrdhPattern) {
        this.gsrdhPattern = gsrdhPattern;
    }

    public String getGsrdhProcedure() {
        return this.gsrdhProcedure;
    }

    public void setGsrdhProcedure(String gsrdhProcedure) {
        this.gsrdhProcedure = gsrdhProcedure;
    }

    public String getGsrdhRecallVoucherId() {
        return this.gsrdhRecallVoucherId;
    }

    public void setGsrdhRecallVoucherId(String gsrdhRecallVoucherId) {
        this.gsrdhRecallVoucherId = gsrdhRecallVoucherId;
    }
}
