package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaComeAndGoDetailData;
import com.gys.business.mapper.entity.GaiaComeAndGoResidualData;
import com.gys.business.service.data.GaiaComeAndGoQueryData;
import com.gys.business.service.data.GaiaComeAndGoSavePaymentInData;
import com.gys.business.service.data.GaiaComeAndGoSummeryQueryData;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ComeAndGoManageMapper {

    void batchSaveComeAndGoDetail(List<GaiaComeAndGoDetailData> comeAndGoDetailDataList);

    List<String> findPaymentIdsByClient(String clientId);

    List<Map<String, String>> findPaymentMethodsByClient(String client);

    List<Map<String,String>> getDcListByClient(String client);

    void batchSavePayment(List<GaiaComeAndGoSavePaymentInData> inData);

    void batchRemovePayment(@Param("client") String client,@Param("paymentIds") List<String> paymentIds);

    List<GaiaComeAndGoDetailData> getComeAndGoDetailList(GaiaComeAndGoQueryData inData);

    List<Map<String,Object>> getComeAndGoSummaryList(GaiaComeAndGoSummeryQueryData inData);

    List<GaiaComeAndGoResidualData> findComeAndGoDetailSummeryByDate(String yesterdayDate);

    void batchSaveComeAndGoResidualData(List<GaiaComeAndGoResidualData> residualList);

    List<Map<String,Object>> getResidualAmtByStartDate(GaiaComeAndGoSummeryQueryData inData);


    List<Map<String,Object>> getResidualAmtByEndDate(GaiaComeAndGoSummeryQueryData inData);

    List<Map<String, String>> getStoListByClient(String client,String queryString);

    String selectMinDateDetailTableBySto(@Param("client") String client,
                                    @Param("dcCode") String dcCode,
                                    @Param("stoCode") String stoCde);

    String selectMinDateDetailTable(String client);
    String selectMinDateResidualTable();

    GaiaComeAndGoResidualData findHistoryResidual(@Param("date") String date,
                                                        @Param("client") String client,
                                                        @Param("dcCode") String dcCode,
                                                        @Param("stoCode") String stoCode);

    void batchUpdateComeAndGoSummeryData(GaiaComeAndGoResidualData updateList);

    List<GaiaComeAndGoDetailData> findYesterDayUploadDetail(String yesterDay);



    List<GaiaComeAndGoDetailData> findStoSummaryDetail(GaiaComeAndGoSummeryQueryData inData);

    List<GaiaComeAndGoDetailData> findComeAndGoDetailSummeryByDateBefore(@Param("paymentDate") String paymentDate,
                                                                         @Param("client") String client,
                                                                         @Param("dcCode") String dcCode,
                                                                         @Param("stoCode") String stoCode);

    void batchRemoveResidualByStoAndPayDate(@Param("paymentDate") String paymentDate,
                                            @Param("client") String client,
                                            @Param("dcCode") String dcCode,
                                            @Param("stoCode") String stoCode);

    void batchUpdateComeAndGoResidualData(GaiaComeAndGoDetailData beforeYesterDayDetail);

    void batchUpdateComeAndGoResidualDataNotEqualDate(GaiaComeAndGoDetailData detailData);


    List<GaiaComeAndGoDetailData> findComeAndGoDetailByDate(GaiaComeAndGoSummeryQueryData inData);

    String findStoNameByClientAndStoCode(@Param("client") String client,
                                         @Param("stoCode") String stoCode);

    String findDcNameByClientAndDcCode(@Param("client") String client,
                                       @Param("dcCode") String dcCode);

    List<GaiaComeAndGoResidualData> findRecentResidualByStartDate(GaiaComeAndGoSummeryQueryData inData);

    GaiaComeAndGoDetailData findRecentDateToStartDateDetailSummaryData(@Param("recentDate") String recentDate, @Param("startDate") String startDate,
                                                    @Param("client") String client,@Param("dcCode") String dcCode, @Param("stoCode") String stoCode);

    List<Map<String, Object>> findComeAndGoSummaryData(@Param("paymentMethods") List<Map<String, String>> paymentMethods,
                                                       @Param("inData") GaiaComeAndGoSummeryQueryData inData);

    List<Map<String, Object>> autoComputeDistributionData(@Param("client") String client,
                                                          @Param("dcCode") String dcCode,
                                                          @Param("stoCode") String stoCode,
                                                          @Param("date") String date);

    List<String> getClientList();

    List<String> getDcCodeListByClient(String client);

    void savePayment(GaiaComeAndGoSavePaymentInData payment);

    Date find8888(@Param("client") String client,
                  @Param("dcCode") String dcCode,
                  @Param("stoCode") String stoCode);

    List<String> findStoListByClient(String client);

    Integer findIsFirst(@Param("client") String client,
                        @Param("dcCode") String dcCode,
                        @Param("stoCode") String stoCode,
                        @Param("startDate") Date startDate);

    Integer findIsFirstWithPayWay666(@Param("client") String client,
                        @Param("dcCode") String dcCode,
                        @Param("stoCode") String stoCode,
                        @Param("startDate") Date startDate);

    List<Map<String, Object>> autoComputeDistributionDataPT(@Param("client")String client, @Param("dcCode")String dcCode, @Param("stoCode")String stoCode,@Param("date") String yyyyMMdd);
}
