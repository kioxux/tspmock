package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author
 * 同步审计表
 */
@Data
public class GaiaSynchronousAuditExt implements Serializable {

    private static final long serialVersionUID = 2908794375996710682L;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String brId;

    /**
     * 提交人ID
     */
    private String userId;

    /**
     * 销售日期
     */
    private String saleDate;

    /**
     * 提交日期
     */
    private Date submissionDate;

    /**
     * 服务器总交易单数
     */
    private String serverTotalBill;

    /**
     * 服务器总交易金额
     */
    private BigDecimal serverTotalBillAmount;

    /**
     * 服务器销售笔数(不包含退货)
     */
    private String serverBillNumber;

    /**
     * 服务器销售金额(不包含退货)
     */
    private BigDecimal serverBillAmount;

    /**
     * 服务器退货笔数
     */
    private String serverRetreatBill;

    /**
     * 服务器退货金额
     */
    private BigDecimal serverRetreatBillAmount;

    /**
     * 本地总交易单数
     */
    private String localTotalBill;

    /**
     * 本地总交易金额
     */
    private BigDecimal localTotalBillAmount;

    /**
     * 本地销售笔数(不包含退货)
     */
    private String localBillNumber;

    /**
     * 本地销售金额(不包含退货)
     */
    private BigDecimal localBillAmount;

    /**
     * 本地退货笔数
     */
    private String localRetreatBill;

    /**
     * 本地退货金额
     */
    private BigDecimal localRetreatBillAmount;


}
