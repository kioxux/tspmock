package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author xiaoyuan
 */
@Table(name = "GAIA_AUTHCONFI_DATA")
@Data
public class GaiaAuthconfiData implements Serializable {
    private static final long serialVersionUID = 2027270598974871218L;
    @Id
    @Column( name = "CLIENT")
    private String client;

    @Id
    @Column(name = "AUTH_GROUP_ID")
    private String authGroupId;

    @Column(name = "AUTH_GROUP_NAME")
    private String authGroupName;

    @Id
    @Column(name = "AUTHOBJ_SITE")
    private String authobjSite;

    @Id
    @Column(name = "AUTHCONFI_USER")
    private String authconfiUser;
}
