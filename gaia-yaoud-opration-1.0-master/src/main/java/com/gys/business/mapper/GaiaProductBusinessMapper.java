package com.gys.business.mapper;

import com.gys.business.controller.app.form.OutStockForm;
import com.gys.business.controller.app.vo.ProductInfoVO;
import com.gys.business.mapper.entity.GaiaFranchisee;
import com.gys.business.mapper.entity.GaiaProductBusiness;
import com.gys.business.service.data.*;
import com.gys.business.service.data.DelegationReturn.DelegationProductOutData;
import com.gys.business.service.data.DelegationReturn.DelegationReturnInData;
import com.gys.business.service.data.coldStorage.ProductQueryCondition;
import com.gys.business.service.data.coldStorage.ProductQueryVo;
import com.gys.business.service.data.productBasic.GetProductBasicInData;
import com.gys.business.service.data.productBasic.GetProductBasicOutData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.goodspurchase.ClientProSite;
import com.gys.common.data.goodspurchase.ProductRelationMaintainListDTO;
import com.gys.common.data.goodspurchase.ProductRelationMaintainListVO;
import com.gys.common.data.goodspurchase.SearchSameProductDTO;
import com.gys.util.takeAway.ddky.DdkyStockUpdateDetailBean;
import com.gys.util.takeAway.yl.Drugs;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface GaiaProductBusinessMapper extends BaseMapper<GaiaProductBusiness> {
    GiveProData selectGiftInfo(GetQueryProductInData inData);

    List<GetQueryProVoOutData> queryProVo(GetProductInData inData);

    List<GetQueryProVoOutData> queryPro(GetProductInData inData);

    List<GetQueryProductOutData> queryProduct(GetQueryProductInData inData);

    List<GetQueryProductOutData> queryProduct2(GetQueryProductInData inData);

    List<GetPdAndExpOutData> queryBatchNoAndExp(GetQueryProductInData inData);

    List<GetPdAndExpOutData> queryStockAndExpGroupByBatchNo(GetQueryProductInData inData);

    GetSalesReceiptsTableOutData queryProductDetail(GetQueryProductInData inData);

    List<GetQueryProVoOutData> queryPrice(GetProductInData inData);

    List<GetProOutData> selectProductByProCodes(@Param("proCodes") List<String> gspgProIds, @Param("client") String clientId);

    List<GetReplenishDetailOutData> queryProList(GetProductInData inData);

    List<DelegationProductOutData> getDelegationProductList(DelegationReturnInData data);

    List<GetQueryProductOutData> queryProductDetailByList(GetPlaceOrderInData inData);

    List<GaiaProductBusiness> getAll(@Param("client") String client);

    List<GetPdAndExpOutData> queryStockAndExpGroupByArea(GetQueryProductInData inData);

    List<GetPdAndExpOutData> queryStockAndExpGroupByThird(GetQueryProductInData inData);

//    Map queryProNew(GetProductNewInData inData);

    List<GetProductNewOutData> queryProByStore(GetProductNewInData inData);

    List<GetProductNewOutData> queryProByClient(GetProductNewInData inData);

    List<GaiaProductBusiness> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);

    List<GetProductThirdlyOutData> queryProThirdlyByStore(GetProductThirdlyInData inData);

    List<GetProductThirdlyOutData> queryProThirdlyByClinet(GetProductThirdlyInData inData);

    GetProductThirdlyOutData queryProThirdlyByALLot(GetProductThirdlyInData inData);

    List<GetProductThirdlyOutData> queryProFourthlyByStore(GetProductThirdlyInData inData);

    List<GetProductThirdlyOutData> queryProFourthlyByClinet(GetProductThirdlyInData inData);

    List<GetProductThirdlyOutData> queryProFourthlyByClinetNoPrice(GetProductThirdlyInData inData);

    List<GetQueryProductOutData> queryProductApp(GetQueryProductInData inData);

    List<GetProductThirdlyOutData> queryProFourthlyByClinetNoPriceForTC(GetProductThirdlyInForTCData inData);


    List<GetProductThirdlyOutData> queryProFourthlyS(GetProductThirdlyInData inData);//不合格5.0查询

    List<GetProductThirdlyOutData> queryProFourthlySs(GetProductThirdlyInData inData); //不合格5.0查询

    OutOfStockOutData getOutOfStockExplain(OutOfStockInData inData);

    /**
     * 缺断货情况查询
     *
     * @param inData
     * @return
     */
    List<OutOfStockOutListData> getOutOfStockExplainList(OutOfStockInData inData);

    /**
     * 缺断货补货历史查询  弃用
     *
     * @param inData
     * @return
     */
    List<OutOfStockOutListData> getOutOfStockHistoryList(OutOfStockInData inData);

    int updateByProductId(InvalidOutOfStockInData inData);

    List<GaiaProductBusiness> findList(GaiaProductBusiness cond);

    List<ProductInfoVO> getOutOfStockList(OutStockForm outStockForm);

    CompanyOutOfStockInfoOutData getCompanyOutOfStockInfo(OutOfStockInData inData);

    CompanyOutOfStockInfoOutData getCompanyDcOutOfStockInfo(OutOfStockInData inData);

    List<CompanyOutOfStockDetailOutData> getCompanyOutOfStockList(OutOfStockInData inData);

    List<CompanyOutOfStockDetailOutData> getCompanyDcOutOfStockList(OutOfStockInData inData);

    int updatePosition(GaiaProductBusiness gaiaProductBusiness);

    List<GaiaProductBusiness> getListByProCode(@Param("client") String client, @Param("proMap") Map<String, Integer> proMap);

    List<GetProductThirdlyOutData> queryProForColdChain(GetProductThirdlyInData inData);

    GaiaProductBusiness selectByClientAndProCode(@Param("client") String client, @Param("proId") String proId);

    List<GaiaProductBusiness> selectByClientAndStoCodeAndProCode(@Param("client") String client, @Param("stoCode") String stoCode, @Param("proId") List<String> proId);

    List<GetProductThirdlyOutData> queryBasicProduct(@Param("content") String content);

    List<Drugs> getAllProductByClientAndBrId(@Param("client") String client, @Param("brId") String brId);

    List<DdkyStockUpdateDetailBean> getDdkyProductByClientAndBrId(@Param("client") String client, @Param("brId") String brId, @Param("proId") String proId);

    /**
     * 查询客户商品编码列表
     *
     * @param getProductBasicInData getProductBasicInData
     * @return List<GetProductBasicOutData>
     */
    List<GetProductBasicOutData> selectCustomerProCodeList(GetProductBasicInData getProductBasicInData);

    /**
     * 查询客户 client 集合
     *
     * @return List<GaiaFranchisee>
     */
    List<GaiaFranchisee> selectCustomerClientList();

    /**
     * 根据客户获取地点集合
     *
     * @param client client
     * @return List<String>
     */
    List<Map<String, String>> selectCustomerProSiteList(String client);

    /**
     * 根据客户获取仓库集合
     *
     * @param client client
     * @return List<String>
     */
    List<Map<String, String>> selectRepoSiteList(String client);

    /**
     * 查询商品对应关系维护列表总数
     *
     * @param productRelationMaintainListDTO productRelationMaintainListDTO
     * @return List<ProductRelationMaintainListVO>
     */
    Integer selectProductRelationMaintainCount(ProductRelationMaintainListDTO productRelationMaintainListDTO);

    /**
     * 查询商品对应关系维护列表
     *
     * @param productRelationMaintainListDTO productRelationMaintainListDTO
     * @return List<ProductRelationMaintainListVO>
     */
    List<ProductRelationMaintainListVO> selectProductRelationMaintainList(ProductRelationMaintainListDTO productRelationMaintainListDTO);

    /**
     * 根据 client 地点 商品自编码更新客户商品编码信息
     *
     * @param gaiaProductBusiness gaiaProductBusiness
     * @return int
     */
    int updateProductBusinessByProSelfCode(GaiaProductBusiness gaiaProductBusiness);

    /**
     * 批量根据 client 地点 商品自编码更新客户商品编码信息
     *
     * @param gaiaProductBusinessList gaiaProductBusinessList
     * @return int
     */
    int updateProductBusinessByProSelfCodeBatch( @Param("list") List<GaiaProductBusiness> gaiaProductBusinessList, @Param("updateClient") String updateClient, @Param("updateUser") String updateUser, @Param("updateTime") String updateTime );

    /**
     * 获取客户商品编码
     *
     * @param client      client
     * @param proSite     proSite
     * @param proSelfCode proSelfCode
     * @return List<ProductRelationMaintainListVO>
     */
    List<ProductRelationMaintainListVO> selectProductByRepoCode(@Param("client") String client, @Param("proSite") String proSite, @Param("proSelfCode") String proSelfCode);

    /**
     * 获取加盟商下仓库
     * @param client client
     * @return List<ClientProSite>
     */
    List<ClientProSite> selectRepoByClient(@Param("client") String client);

    /**
     * 获取加盟商下门店 PRO_SITE
     * @param client client
     * @return List<ClientProSite>
     */
    List<ClientProSite> selectProSiteByClient(@Param("client") String client);


    List<String> getListZdy1(@Param("client") String client );
    List<String> getListZdy2(@Param("client") String client );
    List<String> getListZdy3(@Param("client") String client );
    List<String> getListZdy4(@Param("client") String client );
    List<String> getListZdy5(@Param("client") String client );

    List<ProductQueryVo> findAllList(ProductQueryCondition condition);

    List<LinkedHashMap<String, Object>> getPrice_YSJ1(@Param("client") String client, @Param("proSite") String proSite,@Param("priceType") Integer priceType);
    List<LinkedHashMap<String, Object>> getPrice_BAT_PO_PRICE(@Param("client") String client, @Param("proSite") String proSite,@Param("config") Integer config);
    /**
     * 获取药师帮价格上传设置
     * @param client
     * @param configName
     * @return
     */
    List<LinkedHashMap<String, Object>> getSystemConfig(@Param("client") String client,@Param("configName") String configName);

    /** add by jinwencheng on 2021-12-30 13:50:13 新建药德商品编码页面，新建前先查询是否有符合条件的商品 **/
    List<GetProductBasicOutData> getSameProduct(SearchSameProductDTO searchSameProductDTO);
    /** add end **/

}
