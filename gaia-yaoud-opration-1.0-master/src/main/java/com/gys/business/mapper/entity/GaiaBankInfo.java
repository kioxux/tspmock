//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SD_BANK_SET"
)
public class GaiaBankInfo implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Column(
            name = "GSBS_BR_ID"
    )
    private String gsbsBrId;
    @Id
    @Column(
            name = "GSBS_BANK_ID"
    )
    private String gsbsBankId;
    @Column(
            name = "GSBS_BANK_NAME"
    )
    private String gsbsBankName;
    @Column(
            name = "GSBS_BANK_ACCOUNT"
    )
    private String gsbsBankAccount;

    public GaiaBankInfo() {
    }

    public String getClientId() {
        return this.clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getGsbsBrId() {
        return this.gsbsBrId;
    }

    public void setGsbsBrId(String gsbsBrId) {
        this.gsbsBrId = gsbsBrId;
    }

    public String getGsbsBankId() {
        return this.gsbsBankId;
    }

    public void setGsbsBankId(String gsbsBankId) {
        this.gsbsBankId = gsbsBankId;
    }

    public String getGsbsBankName() {
        return this.gsbsBankName;
    }

    public void setGsbsBankName(String gsbsBankName) {
        this.gsbsBankName = gsbsBankName;
    }

    public String getGsbsBankAccount() {
        return this.gsbsBankAccount;
    }

    public void setGsbsBankAccount(String gsbsBankAccount) {
        this.gsbsBankAccount = gsbsBankAccount;
    }
}
