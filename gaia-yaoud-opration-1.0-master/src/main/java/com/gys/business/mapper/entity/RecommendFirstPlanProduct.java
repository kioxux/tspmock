package com.gys.business.mapper.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>
 * 首推商品明细表
 * </p>
 *
 * @author flynn
 * @since 2021-08-04
 */
@Data
@Table( name = "GAIA_SD_RECOMMEND_FIRST_PLAN_PRODUCT")
public class RecommendFirstPlanProduct implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "PLAN_ID")
    private Long planId;

    @ApiModelProperty(value = "加盟店")
    @Column(name = "CLIENT")
    private String client;

    @ApiModelProperty(value = "商品编码")
    @Column(name = "PRO_ID")
    private String proId;


    @ApiModelProperty(value = "商品编码")
    @Column(name = "PRO_COMP_CLASS")
    private String proCompClass;

}
