package com.gys.business.mapper;


import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationD;
import com.gys.business.mapper.entity.GaiaEntNewProductEvaluationH;
import com.gys.business.service.data.GaiaEntNewProductEvaluationDOutData;
import com.gys.business.service.data.GaiaEntNewProductEvaluationInData;
import com.gys.business.service.data.GaiaNewProductEvaluationInData;
import com.gys.business.service.data.GaiaNewProductEvaluationOutData;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * 公司级-新品评估-主表(GaiaEntNewProductEvaluationH)表数据库访问层
 *
 * @author makejava
 * @since 2021-07-19 10:23:11
 */
public interface GaiaEntNewProductEvaluationHMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaEntNewProductEvaluationH queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaEntNewProductEvaluationH> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaEntNewProductEvaluationH 实例对象
     * @return 对象列表
     */
    List<GaiaEntNewProductEvaluationH> queryAll(GaiaEntNewProductEvaluationH gaiaEntNewProductEvaluationH);

    /**
     * 新增数据
     *
     * @param gaiaEntNewProductEvaluationH 实例对象
     * @return 影响行数
     */
    int insert(GaiaEntNewProductEvaluationH gaiaEntNewProductEvaluationH);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaEntNewProductEvaluationH> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaEntNewProductEvaluationH> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaEntNewProductEvaluationH> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaEntNewProductEvaluationH> entities);

    /**
     * 修改数据
     *
     * @param gaiaEntNewProductEvaluationH 实例对象
     * @return 影响行数
     */
    int update(GaiaEntNewProductEvaluationH gaiaEntNewProductEvaluationH);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    List<GaiaEntNewProductEvaluationDOutData> listWaitEvalustion(@Param("param") String param,@Param("client") String client);

    GaiaEntNewProductEvaluationDOutData getCommentStstus(GaiaEntNewProductEvaluationDOutData waitGood);

    List<String> getCommentClass(GaiaEntNewProductEvaluationDOutData waitGood);

    GaiaEntNewProductEvaluationDOutData getAveragePrice(GaiaEntNewProductEvaluationDOutData waitGood);

    String getBillNo(@Param("client") String client);

    GaiaEntNewProductEvaluationDOutData getGrossAndStoreQty(GaiaEntNewProductEvaluationDOutData waitGood);

    List<GaiaEntNewProductEvaluationDOutData> listEntProductBill(GaiaEntNewProductEvaluationInData param);

    List<GaiaEntNewProductEvaluationDOutData> listEntProductBillDetail(GaiaEntNewProductEvaluationInData param);

    GaiaEntNewProductEvaluationDOutData getHeadInfo(GaiaEntNewProductEvaluationInData param);

    List<HashMap<String, Object>> listEntInfo(GaiaEntNewProductEvaluationInData param);

    List<HashMap<String, Object>> listCK(GaiaEntNewProductEvaluationInData param);

    List<GaiaEntNewProductEvaluationD> listUnDealList(GaiaEntNewProductEvaluationH param);

    GaiaEntNewProductEvaluationDOutData getHintInfo(GaiaEntNewProductEvaluationH param);

    List<GaiaEntNewProductEvaluationDOutData> listEntProductionDetail(GaiaEntNewProductEvaluationInData param);

    GaiaNewProductEvaluationOutData getBillByClientAndStorId(GaiaNewProductEvaluationInData inData);

    List<String> listClient();

    HashMap<String, Object> getClientParam(@Param("client") String client);

    GaiaNewProductEvaluationOutData listEntProductionResult(GaiaEntNewProductEvaluationInData param);

    GaiaNewProductEvaluationOutData getBillInfo(GaiaEntNewProductEvaluationInData param);

    List<String> listBillCodeByProSelfCode(GaiaEntNewProductEvaluationInData param);

    List<GaiaEntNewProductEvaluationDOutData> listPGQty(GaiaEntNewProductEvaluationDOutData outData);
}

