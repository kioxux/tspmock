package com.gys.business.mapper;

import com.gys.business.mapper.entity.OasSicknessElement;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 成分细类表 Mapper 接口
 * </p>
 *
 * @author flynn
 * @since 2021-09-02
 */
@Mapper
public interface OasSicknessElementMapper extends BaseMapper<OasSicknessElement> {
}
