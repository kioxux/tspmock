package com.gys.business.mapper;

import com.gys.business.bosen.dto.BsStockData;
import com.gys.business.bosen.form.InDataForm;
import com.gys.business.mapper.entity.GaiaSdStockBatch;
import com.gys.business.service.data.*;
import com.gys.business.service.data.DtYb.Inventory.InventoryVO;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public interface GaiaSdStockBatchMapper extends BaseMapper<GaiaSdStockBatch> {
    /**
     * 根据销售单号查询
     * @return
     */
    List<GaiaSdStockBatch> selectAllByBillNo(@Param("client") String client, @Param("brId") String brId, @Param("proList") List<String> proList );

    /**
     * 批量更新
     * @param batchList
     */
    void updateList(List<GaiaSdStockBatch> batchList);

    /**
     * 批量减库存
     * @param batchList
     */
    void updateQtyList(List<StockInData> batchList);

    List<IncomeStatementStoreDeficitOutDataResult> fetchProductStockDetail(GetQueryProductInData inData);

    List<GaiaSdStockBatch> getAllByClientAndBrId(@Param("client")String client,@Param("depId") String depId);

    List<GaiaSdStockBatch> getAllByIds(@Param("client")String client,@Param("ids") List<String> ids);

    /**
     * 查询当前商品的 批号总库存
     * @param clientId
     * @param gssdBrId
     * @param gssdProId
     * @param gssdBatchNo
     * @return
     */
    GaiaSdStockBatch getAllByBatchNo(@Param("client")String clientId,@Param("gssdBrId") String gssdBrId, @Param("gssdProId")String gssdProId, @Param("gssdBatchNo")String gssdBatchNo);

    List<InventoryVO> queryInventory(YbInterfaceQueryData inData);

    ValidProductOutData getValidProductInfo(ValidProductInData inData);

    List<ValidProductListOutData> getValidProductList(ValidProductInData inData);

    CompanyOutOfStockOutData getCompanyExpiryProInfo(String client);

    List<ValidProductListOutData> getCompanyExpiryProList(ValidProductInData inData);

    List<CompanyExpiryProChartOutData> getCompanyExpiryProChart(CompanyExpiryProChartInData inData);

    List<StoreStockReportOutData> getClientStockList(@Param("clientId")String clientId, @Param("type") String type);

    StockReportHeadOutData getTotalHeadInfo(@Param("clientId")String clientId);

    List<ProductMidTypeOutData> getProductMidTypeReport(ProductMidTypeSaleInData inData);

    List<StoreStockOutData> getStoreStockList(AppStoreSaleInData inData);

    List<BsStockData> getCurrentStock(@Param("client") String client);

    List<AppNoMoveOutData> getBigTypeList(AppNoMoveInData inData);

    List<AppNoMoveStoreOutData> getNoMoveStoreList(NoMoveProInData inData);

    List<AppNoMoveOutData> getMidTypeList(AppNoMoveInData inData);

    List<GaiaSdStockBatch> getByClientAndBrIdAndProId(@Param("client") String client,@Param("stoCodes") List<String> stoCode,@Param("proSelfCodes") List<String> proSelfCode);

    /**
     * 获取最大批号
     * @param map
     * @return
     */
    List<GaiaSdStockBatch> selectAllByBillNo2(Map<String, Object> map);

    /**
     * 获取门店库存
     * @param dataForm
     * @return
     */
    List<LinkedHashMap<String, Object>> getStockData(InDataForm dataForm);

    List<ProductMidTypeOutData> getProductMidTypeReportByClient(ProductMidTypeSaleInData inData);

    List<ProductMidTypeOutData> getProductMidTypeReportByDc(ProductMidTypeSaleInData inData);

    List<ProductMidTypeOutData> getProductMidTypeReportByStore(ProductMidTypeSaleInData inData);

    List<RestOrderChangeDto> getChangeByBillNo(Map<String, String> map);
}
