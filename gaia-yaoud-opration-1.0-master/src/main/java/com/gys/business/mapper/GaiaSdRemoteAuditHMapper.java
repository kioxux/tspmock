//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdRemoteAuditH;
import com.gys.business.service.data.PrescriptionInfoInData;
import com.gys.business.service.data.PrescriptionInfoOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdRemoteAuditHMapper extends BaseMapper<GaiaSdRemoteAuditH> {
    String selectNextVoucherId(@Param("client") String client, @Param("codePre") String codePre);

    List<PrescriptionInfoOutData> getPrescriptionList(PrescriptionInfoInData inData);
}
