package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPhysicalCounting;
import com.gys.business.service.data.GetPhysicalCountInData;
import com.gys.business.service.data.GetPhysicalCountOutData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPhysicalCountingMapper extends BaseMapper<GaiaSdPhysicalCounting> {
    String selectNextVoucherId(@Param("client") String client, @Param("codePre") String codePre);

    List<GetPhysicalCountOutData> selectPhysical(GetPhysicalCountInData inData);
}
