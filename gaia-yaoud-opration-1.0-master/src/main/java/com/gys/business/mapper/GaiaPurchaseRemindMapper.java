package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaPurchaseRemind;
import com.gys.common.base.BaseMapper;

import java.util.List;

public interface GaiaPurchaseRemindMapper extends BaseMapper<GaiaPurchaseRemind> {
    int insertList(List<GaiaPurchaseRemind> list);
}