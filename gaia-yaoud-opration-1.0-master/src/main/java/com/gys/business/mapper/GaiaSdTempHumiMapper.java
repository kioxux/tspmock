//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdTempHumi;
import com.gys.business.service.data.TempHumiInData;
import com.gys.business.service.data.TempHumiOutData;
import com.gys.common.base.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdTempHumiMapper extends BaseMapper<GaiaSdTempHumi> {
    List<TempHumiOutData> getTempHumiList(TempHumiInData inData);

    String selectNextVoucherId(@Param("client") String client, @Param("codePre") String codePre);

    void aduitTempHumi(TempHumiInData inData);

    String selectCurrentVoucherId(@Param("client") String client, @Param("codePre") String codePre);

    List<String> getDistinctDayList(@Param("clientId") String clientId,
                                    @Param("brId") String brId,
                                    @Param("startDate") String startDate,
                                    @Param("endDate") String endDate);

    void batchInsert(List<TempHumiInData> list);
}
