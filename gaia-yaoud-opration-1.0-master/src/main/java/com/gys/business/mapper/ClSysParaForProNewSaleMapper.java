package com.gys.business.mapper;

import org.apache.ibatis.annotations.Param;

public interface ClSysParaForProNewSaleMapper {
    String getStatus(String client);

    void saveNewClientStatus(String client);

    void changeStatus(@Param("client") String client,@Param("status") String status);
}
