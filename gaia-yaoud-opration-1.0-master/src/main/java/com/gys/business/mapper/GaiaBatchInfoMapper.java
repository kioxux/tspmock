//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaBatchInfo;
import com.gys.business.service.data.GetAcceptInData;
import com.gys.business.service.data.PoPriceProductHistoryInData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.storeorder.LastSupplierPrice;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaBatchInfoMapper extends BaseMapper<GaiaBatchInfo> {
    String selectNextVoucherId(@Param("clientId") String clientId);

    String selectPoid(GetAcceptInData inData);

    List<Map<String, String>> selectSupplierListByStoCode(PoPriceProductHistoryInData inData);

    /**
     * 查询商品末次供应商信息
     *
     * @param client   client
     * @param proCodes proCodes
     * @return List<LastSupplierPrice>
     */
    List<LastSupplierPrice> selectLastSupplier(@Param("client") String client, @Param("proCodes") List<String> proCodes);

}
