package com.gys.business.mapper.entity;


import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * 积分商品设置表
 * </p>
 *
 * @author zhangjiahao
 * @since 2021-07-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Table(name = "GAIA_SD_INTEGRAL_RATE_SET")
@ToString
public class GaiaSdIntegralRateSet implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */

    @Id
    @Column(name = "ID")
    private Long id;

    /**
     * 加盟商
     */
    @Column(name ="CLIENT")
    private String client;

    /**
     * 商品编码
     */
    @Column(name ="PRO_ID")
    private String proId;

    /**
     * 是否积分	0：否，1：是
     */
    @Column(name ="IF_INTE")
    private String ifInte;

    /**
     * 积分倍率
     */
    @Column(name ="INTE_RATE")
    private String inteRate;

    /**
     * 0：否，1：是
     */
    @Column(name ="IF_ALLTIME")
    private String ifAlltime;

    /**
     * 起始日期
     */
    @Column(name ="BEGIN_DATE")
    private String beginDate;

    /**
     * 结束日期
     */
    @Column(name ="END_DATE")
    private String endDate;

    /**
     * 星期频率
     */
    @Column(name ="WEEK_FREQUENCY")
    private String weekFrequency;

    /**
     * 日期频率	
     */
    @Column(name ="DATE_FREQUENCY")
    private String dateFrequency;

    /**
     * 是否所有门店 0：否，1：是
     */
    @Column(name = "IF_ALLSTO")
    private String ifAllsto;


}
