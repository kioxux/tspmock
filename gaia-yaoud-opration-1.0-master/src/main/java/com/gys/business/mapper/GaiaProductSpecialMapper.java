package com.gys.business.mapper;

import com.gys.business.service.data.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductSpecialMapper {

    int deleteByPrimaryKey(String proCode);

    int insert(GaiaProductSpecial record);

    int replaceInsert(GaiaProductSpecial record);

    int insertSelective(GaiaProductSpecial record);

    GaiaProductSpecial selectByPrimaryKey(GaiaProductSpecial inData);

    int updateByPrimaryKeySelective(GaiaProductSpecial record);

    int updateByPrimaryKey(GaiaProductSpecial record);

    int batchInsert(@Param("list") List<GaiaProductSpecial> list);

    int batchReplaceInsert(@Param("list") List<GaiaProductSpecial> list);

    List<AreaOutData> getAreaList();

    List<SelectFrameFlagOutData> getProFlagList(@Param("queryType") String queryType);

    List<ProductComponentClassOutData> getClassList();

    List<String> getProductCodeList();

    List<GaiaProductSpecialOutData> getProductSpecialList(GaiaProductSpecialInData inData);

    int invalidProductByCodeList(@Param("productList") List<GaiaProductSpecial> productList, @Param("inData") GaiaProductSpecial inData);

    List<GaiaProductSpecialRecordOutData> getProductSpecialRecordList(GaiaProductSpecialEditRecordInData inData);

    List<ProvCityRelationshipOutData> getProvCityRelationship();

    List<SpecialClassifcationOutData> getclassifcation(SpecialClassifcationInData inData);
}