package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductBusiness;
import com.gys.business.mapper.entity.GaiaStoreData;
import com.gys.common.data.remoteReview.GaiaSdRemoteAuditD;
import com.gys.common.data.remoteReview.GaiaSdRemoteAuditH;
import com.gys.common.data.remoteReview.dto.QueryReviewDetailsOutDataDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface RemoteReviewMapper {
    /**
     * 新增处方明细
     **/
    int insertD(GaiaSdRemoteAuditD gaiaSdRemoteAuditD);

    /**
     * 刪除
     **/
    int delete(int id);

    /**
     * 更新
     **/
    int update(GaiaSdRemoteAuditD gaiaSdRemoteAuditD);

    /**
     * 新增处方信息
     **/
    int insertH(GaiaSdRemoteAuditH gaiaSdRemoteAuditH);

    /**
     * 查询处方明细
     **/
    ArrayList<QueryReviewDetailsOutDataDTO> queryReviewDetailList(@Param("client") String client, @Param("gssrSaleBillNo") String gssrSaleBillNo);

    /**
     * 获取审核人员校验方式 0：密码；1：指纹；2：密码和指纹
     * @param client
     * @return
     */
    Integer queryAuthenticationType(@Param("client") String client);

    /**
     * 通过加盟商，用户id，密码统计用户个数
     * @param client
     * @param userId
     * @param password
     * @return
     */
    Integer checkRegisterByPassword(@Param("client") String client, @Param("userId") String userId, @Param("password") String password);


    /**
     * 根据加盟商，店号，商品编码获取商品明细
     * @param gssrBrId
     * @param gssrProId
     * @return
     */
    GaiaProductBusiness queryProductBySelfCode(@Param("client") String client, @Param("gssrBrId") String gssrBrId, @Param("gssrProId") String gssrProId);

    /**
     * 根据加盟商，店号，获取门店明细
     * @param client
     * @param gssrBrId
     * @return
     */
    GaiaStoreData queryStoreData(@Param("client") String client, @Param("gssrBrId") String gssrBrId);

    /**
     * 审核结束后，修改挂单表状态
     *
     * @param status
     * @param clientId
     * @param gssrSaleBillNo
     * @param gssrBrId
     * @param gssrSaleDate
     */
    void updateHandStatus(@Param("status") String status, @Param("client") String clientId, @Param("gssrSaleBillNo") String gssrSaleBillNo, @Param("gssrBrId") String gssrBrId, @Param("gssrSaleDate") String gssrSaleDate);

}