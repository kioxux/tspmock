package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaProductRelate;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductRelateMapper extends BaseMapper<GaiaProductRelate> {

    GaiaProductRelate queryProductPrice(String client,String brId, String proCode,String supCode);

    List<GaiaProductRelate> getAllByClientAndBrId(@Param("client") String client, @Param("depId") String depId);
}
