package com.gys.business.mapper;

import com.gys.business.mapper.entity.DcOutStockH;
import org.apache.ibatis.annotations.Param;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/22 16:38
 */
public interface DcOutStockHMapper {

    DcOutStockH getById(Long id);

    int add(DcOutStockH dcOutStockH);

    int update(DcOutStockH dcOutStockH);

    DcOutStockH getLatestH(@Param("client") String client);
}
