package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员卡关联表
 *
 * @author xiaoyuan on 2020/8/5
 */
@Data
@Table(name = "GAIA_SD_MEMBER_CARD")
public class GaiaSdMemberCardData implements Serializable {
    private static final long serialVersionUID = 1790627152270932691L;
    /**
     * 加盟商
     */
    @Id
    @Column(name = "CLIENT")
    private String client;

    /**
     * 会员ID
     */
    @Id
    @Column(name = "GSMBC_MEMBER_ID")
    private String gsmbcMemberId;

    /**
     * 会员卡号
     */
    @Id
    @Column(name = "GSMBC_CARD_ID")
    private String gsmbcCardId;

    /**
     * 所属店号
     */
    @Column(name = "GSMBC_BR_ID")
    private String gsmbcBrId;

    /**
     * 渠道
     */
    @Column(name = "GSMBC_CHANNEL")
    private String gsmbcChannel;

    /**
     * 卡类型
     */
    @Column(name = "GSMBC_CLASS_ID")
    private String gsmbcClassId;

    /**
     * 当前积分
     */
    @Column(name = "GSMBC_INTEGRAL")
    private String gsmbcIntegral;

    /**
     * 最后积分日期
     */
    @Column(name = "GSMBC_INTEGRAL_LASTDATE")
    private String gsmbcIntegralLastdate;

    /**
     * 清零积分日期
     */
    @Column(name = "GSMBC_ZERO_DATE")
    private String gsmbcZeroDate;

    /**
     * 新卡创建日期
     */
    @Column(name = "GSMBC_CREATE_DATE")
    private String gsmbcCreateDate;

    /**
     * 类型
     */
    @Column(name = "GSMBC_TYPE")
    private String gsmbcType;

    /**
     * openID
     */
    @Column(name = "GSMBC_OPEN_ID")
    private String gsmbcOpenId;

    /**
     * 卡状态
     */
    @Column(name = "GSMBC_STATUS")
    private String gsmbcStatus;

    /**
     * 开卡人员
     */
    @Column(name = "GSMBC_CREATE_SALER")
    private String gsmbcCreateSaler;

    /**
     * 开卡门店
     */
    @Column(name = "GSMBC_OPEN_CARD")
    private String gsmbcOpenCard;

    /**
     * 所属组织，单体：店号，连锁：连锁编码
     */
    @Column(name = "GSMBC_ORG_ID")
    private String gsmbcOrgId;


    /**
     * 累计积分
     */
    @Column(name = "GSMBC_TOTAL_INTEGRAL")
    private String gsmbcTotalIntegral;

    /**
     * 累计金额
     */
    @Column(name = "GSMBC_TOTAL_AMT")
    private BigDecimal gsmbcTotalAmt;

    /**
     * 当年累计积分
     */
    @Column(name = "GSMBC_YEAR_INTEGRAL")
    private String gsmbcYearIntegral;

    /**
     * 当年累计金额
     */
    @Column(name = "GSMBC_YEAR_AMT")
    private BigDecimal gsmbcYearAmt;

    /**
     * 所属组织类型，单体：1，连锁：2
     */
    @Column(name = "GSMBC_ORG_TYPE")
    private String gsmbcOrgType;

    /**
     * 会员组编码
     */
    @Column(name = "GSMBC_ORG_GROUP")
    private String gsmbcOrgGroup;

    /**
     * 最后修改时间
     */
    @Column(name = "LAST_UPDATE_TIME")
    private String lastUpdateTime;

    /**
     * 企微绑定状态，0、null：未绑定，1：已绑定
     */
    @Column(name = "GSMBC_WORKWECHAT_BIND_STATUS")
    private String gsmbcWorkWeChatBindStatus;

    /**
     * 0:维持现有模式不变，即免费注册后立即生效，1: 收费注册需线下激活转正，2:收费注册线上支付后激活
     */
    @Column(name = "GSMBC_STATE")
    private String gsmbcState;

    /**
     * 激活人工号
     */
    @Column(name = "GSMBC_JH_USER")
    private String gsmbcJhUser;

    /**
     * 激活日期
     */
    @Column(name = "GSMBC_JH_DATE")
    private String gsmbcJhDate;

    /**
     * 激活时间
     */
    @Column(name = "GSMBC_JH_TIME")
    private String gsmbcJhTime;
}

