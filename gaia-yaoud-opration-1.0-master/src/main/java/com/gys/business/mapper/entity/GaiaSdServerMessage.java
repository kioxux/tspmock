package com.gys.business.mapper.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name="GAIA_SD_SERVER_MESSAGE")
public class GaiaSdServerMessage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(
            name = "GSSD_ID"
    )
    private Integer messageId;
    @Column(
            name = "GSSD_CONTENT"
    )
    private String content;
    @Column(
            name = "GSSD_FONT_SIZE"
    )
    private Integer size;
    @Column(
            name = "GSSD_FONT_COLOR"
    )
    private String color;
    @Column(
            name = "GSSD_FONT_FAMILY"
    )
    private String famliy;
    @Column(
            name = "GSSD_IS_BOLD"
    )
    private Integer isBold;
    @Column(
            name = "GSSD_IS_ITALICS"
    )
    private Integer isItalics;
    @Column(
            name = "GSSD_MARGIN_TOP"
    )
    private Float marginTop;
    @Column(
            name = "GSSD_MARGIN_BUTTOM"
    )
    private Float marginButton;
    @Column(
            name = "GSSD_MARGIN_LEFT"
    )
    private Float marginLeft;
    @Column(
            name = "GSSD_MARGIN_RIGHT"
    )
    private Float marginRight;
    @Column(
            name = "GSSD_IS_BREAK"
    )
    private Integer isBreak;

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFamliy() {
        return famliy;
    }

    public void setFamliy(String famliy) {
        this.famliy = famliy;
    }

    public Integer getIsBold() {
        return isBold;
    }

    public void setIsBold(Integer isBold) {
        this.isBold = isBold;
    }

    public Integer getIsItalics() {
        return isItalics;
    }

    public void setIsItalics(Integer isItalics) {
        this.isItalics = isItalics;
    }

    public Float getMarginTop() {
        return marginTop;
    }

    public void setMarginTop(Float marginTop) {
        this.marginTop = marginTop;
    }

    public Float getMarginButton() {
        return marginButton;
    }

    public void setMarginButton(Float marginButton) {
        this.marginButton = marginButton;
    }

    public Float getMarginLeft() {
        return marginLeft;
    }

    public void setMarginLeft(Float marginLeft) {
        this.marginLeft = marginLeft;
    }

    public Float getMarginRight() {
        return marginRight;
    }

    public void setMarginRight(Float marginRight) {
        this.marginRight = marginRight;
    }

    public Integer getIsBreak() {
        return isBreak;
    }

    public void setIsBreak(Integer isBreak) {
        this.isBreak = isBreak;
    }
}
