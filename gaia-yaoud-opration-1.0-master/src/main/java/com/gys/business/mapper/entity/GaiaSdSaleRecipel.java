package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author xiaoyuan
 */
@Table(name = "GAIA_SD_SALE_RECIPEL")
@Data
public class GaiaSdSaleRecipel implements Serializable {
    private static final long serialVersionUID = -600857379956171880L;
    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column( name = "GSSR_VOUCHER_ID")
    private String gssrVoucherId;

    @Column(name = "GSSR_TYPE")
    private String gssrType;

    @Column(name = "GSSR_BR_ID")
    private String gssrBrId;

    @Column(name = "GSSR_BR_NAME")
    private String gssrBrName;

    @Column(name = "GSSR_DATE")
    private String gssrDate;

    @Column(name = "GSSR_TIME")
    private String gssrTime;

    @Column(name = "GSSR_EMP")
    private String gssrEmp;

    @Column(name = "GSSR_UPLOAD_FLAG")
    private String gssrUploadFlag;

    @Column(name = "GSSR_SALE_BR_ID")
    private String gssrSaleBrId;

    @Column(name = "GSSR_SALE_DATE")
    private String gssrSaleDate;

    @Column(name = "GSSR_SALE_BILL_NO")
    private String gssrSaleBillNo;

    @Column(name = "GSSR_CUST_NAME")
    private String gssrCustName;

    @Column(name = "GSSR_CUST_SEX")
    private String gssrCustSex;

    @Column(name = "GSSR_CUST_AGE")
    private String gssrCustAge;

    @Column(name = "GSSR_CUST_IDCARD")
    private String gssrCustIdcard;

    @Column(name = "GSSR_CUST_MOBILE")
    private String gssrCustMobile;

    @Column(name = "GSSR_PHARMACIST_ID")
    private String gssrPharmacistId;

    @Column(name = "GSSR_PHARMACIST_NAME")
    private String gssrPharmacistName;

    @Column(name = "GSSR_CHECK_DATE")
    private String gssrCheckDate;

    @Column(name = "GSSR_CHECK_TIME")
    private String gssrCheckTime;

    @Column(name = "GSSR_CHECK_STATUS")
    private String gssrCheckStatus;

    @Column(name = "GSSR_PRO_ID")
    private String gssrProId;

    @Column(name = "GSSR_BATCH_NO")
    private String gssrBatchNo;

    @Column(name = "GSSR_QTY")
    private String gssrQty;

    @Column(name = "GSSR_RECIPEL_ID")
    private String gssrRecipelId;

    @Column(name = "GSSR_RECIPEL_HOSPITAL")
    private String gssrRecipelHospital;

    @Column(name = "GSSR_RECIPEL_DEPARTMENT")
    private String gssrRecipelDepartment;

    @Column(name = "GSSR_RECIPEL_DOCTOR")
    private String gssrRecipelDoctor;

    @Column(name = "GSSR_SYMPTOM")
    private String gssrSymptom;

    @Column(name = "GSSR_DIAGNOSE")
    private String gssrDiagnose;

    @Column(name = "GSSR_RECIPEL_PIC_ID")
    private String gssrRecipelPicId;

    @Column(name = "GSSR_RECIPEL_PIC_NAME")
    private String gssrRecipelPicName;

    @Column(name = "GSSR_RECIPEL_PIC_ADDRESS")
    private String gssrRecipelPicAddress;

    @Column(name = "GSSR_FLAG")
    private String gssrFlag;

    @Column(name ="GSSR_DATE_TIME" )
    private String gssrDateTime;

    @Column(name = "GSSR_YMFLAG")
    private String gssrYmflag;
}
