package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdElectronicProductsGroup;
import com.gys.business.service.data.FuzzyQueryOfProductGroupInData;
import com.gys.business.service.data.FuzzyQueryOfProductGroupOutData;
import com.gys.business.service.data.ProductGroupDetailsViewInData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdElectronicProductsGroupMapper {


    int insertSelective(GaiaSdElectronicProductsGroup record);

    int updateByPrimaryKeySelective(@Param("record") GaiaSdElectronicProductsGroup record);

    int batchInsert(@Param("list") List<GaiaSdElectronicProductsGroup> list);

    String getElectronicProductsGroupId(@Param("client") String client);

    List<GaiaSdElectronicProductsGroup> getAll(@Param("client") String client);

    List<String> getElectronicProductsGroupIdList(@Param("client") String client);

    List<FuzzyQueryOfProductGroupOutData> fuzzyQueryOfProductGroup(FuzzyQueryOfProductGroupInData inData);

    List<FuzzyQueryOfProductGroupOutData> productGroupDetailsView(@Param("client") String client,@Param("list") List<String> proCodes);

    List<FuzzyQueryOfProductGroupOutData> importExcel(@Param("list") List<String> proCode, @Param("client") String client, @Param("brId") String brId);

    List<GaiaSdElectronicProductsGroup> getAllByClientAndProGroupIds( @Param("client")String client,@Param("list") List<String> gsecsProGroupIds);

    void batchUpdate(@Param("list") List<GaiaSdElectronicProductsGroup> groupList);

}