package com.gys.business.mapper.entity;

import com.gys.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/22 16:35
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DcOutStockStore extends BaseEntity {
    /**加盟商*/
    private String client;
    /**缺断货流水号*/
    private String voucherId;
    /**门店ID*/
    private String storeId;
    /**门店名称*/
    private String storeName;
    /**月均销售额*/
    private BigDecimal monthSaleAmount;
    /**月均毛利额*/
    private BigDecimal monthProfitAmount;
    /**缺断货品项数*/
    private Integer proNum;
}
