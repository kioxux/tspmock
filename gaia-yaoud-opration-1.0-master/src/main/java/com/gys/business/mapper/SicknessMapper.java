package com.gys.business.mapper;

import java.util.List;
import java.util.Map;

public interface SicknessMapper {
    void saveTag(Map<String, String> importData);

    void saveCoding(Map<String, String> importData);

    void saveElement(Map<String, String> importData);

    void saveTagCoding(Map<String, String> importData);

    void saveElementCoding(Map<String, String> importData);

    Integer findTag(Map<String, String> importData);

    Integer findCoding(Map<String, String> importData);

    Integer findElement(Map<String, String> importData);

    Integer findTagCoding(Map<String, String> importData);

    Integer findElementCoding(Map<String, String> importData);
}
