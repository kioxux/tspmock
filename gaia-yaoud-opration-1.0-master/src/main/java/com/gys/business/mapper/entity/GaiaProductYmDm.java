package com.gys.business.mapper.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 药盟对码表(GaiaProductYmDm)实体类
 *
 * @author makejava
 * @since 2021-11-11 20:26:42
 */
public class GaiaProductYmDm implements Serializable {
    private static final long serialVersionUID = -18712030949667672L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店
     */
    private String dmBrId;
    /**
     * 药德编码
     */
    private String proCode;
    /**
     * 用户编码
     */
    private String proSelfCode;
    /**
     * 药盟商品编码
     */
    private String ymProCode;
    /**
     * 是否删除：0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新者
     */
    private Date updateTime;
    /**
     * 更新时间
     */
    private String updateUser;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDmBrId() {
        return dmBrId;
    }

    public void setDmBrId(String dmBrId) {
        this.dmBrId = dmBrId;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getProSelfCode() {
        return proSelfCode;
    }

    public void setProSelfCode(String proSelfCode) {
        this.proSelfCode = proSelfCode;
    }

    public String getYmProCode() {
        return ymProCode;
    }

    public void setYmProCode(String ymProCode) {
        this.ymProCode = ymProCode;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

}

