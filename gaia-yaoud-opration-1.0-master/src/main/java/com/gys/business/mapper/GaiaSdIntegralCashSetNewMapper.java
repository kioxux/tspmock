package com.gys.business.mapper;

import com.gys.business.mapper.entity.member.GaiaSdIntegralCashSetNew;
import com.gys.business.service.data.SelectData;
import com.gys.common.base.BaseMapper;
import com.gys.common.data.member.PointSettingSearchDTO;
import com.gys.common.data.member.PointSettingVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 积分抵现内容设置
 *
 * @author wu mao yin
 * @date 2021/12/8 17:53
 */
public interface GaiaSdIntegralCashSetNewMapper extends BaseMapper<GaiaSdIntegralCashSetNew> {

    /**
     * 根据主键修改
     *
     * @param gaiaSdIntegralCashSetNew gaiaSdIntegralCashSetNew
     * @return int
     */
    int updateCashSetByKey(GaiaSdIntegralCashSetNew gaiaSdIntegralCashSetNew);

    /**
     * 获取最新的规则代号
     *
     * @param client client
     * @return String
     */
    String selectLastPlanId(String client);

    /**
     * 获取最新的规则内容代号
     *
     * @param client client
     * @return String
     */
    String selectLastCode(String client);

    /**
     * 批量删除
     *
     * @param planIdList planIdList
     * @return int
     */
    int deleteCashSetBatch(List<String> planIdList);

    /**
     * 查询积分规则设置列表
     *
     * @param pointSettingSearchDTO pointSettingSearchDTO
     * @return List<PointSettingVO>
     */
    List<PointSettingVO> selectPointRuleSettingList(PointSettingSearchDTO pointSettingSearchDTO);

    /**
     * 根据加盟商获取规则下拉列表
     *
     * @param client client
     * @return List<SelectData>
     */
    List<SelectData> selectPlanIdList(@Param("client") String client);

}