package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Table(name = "GAIA_SD_REPLENISH_H")
@Data
public class GaiaSdReplenishH implements Serializable {
    private static final long serialVersionUID = 7340912316728529606L;
    @Id
    @Column(
            name = "CLIENT"
    )
    private String clientId;
    @Id
    @Column(
            name = "GSRH_VOUCHER_ID"
    )
    private String gsrhVoucherId;
    @Id
    @Column(
            name = "GSRH_BR_ID"
    )
    private String gsrhBrId;
    @Id
    @Column(
            name = "GSRH_DATE"
    )
    private String gsrhDate;
    @Column(
            name = "GSRH_TYPE"
    )
    private String gsrhType;
    @Column(
            name = "GSRH_TOTAL_AMT"
    )
    private BigDecimal gsrhTotalAmt;
    @Column(
            name = "GSRH_TOTAL_QTY"
    )
    private String gsrhTotalQty;
    @Column(
            name = "GSRH_EMP"
    )
    private String gsrhEmp;
    @Column(
            name = "GSRH_STATUS"
    )
    private String gsrhStatus;
    @Column(
            name = "GSRH_FLAG"
    )
    private String gsrhFlag;
    @Column(
            name = "GSRH_POID"
    )
    private String gsrhPoid;
    @Column(
            name = "GSRH_PATTERN"
    )
    private String gsrhPattern;
    @Column(
            name = "GSRH_DN_BY"
    )
    private String gsrhDnBy;
    @Column(
            name = "GSRH_DN_DATE"
    )
    private String gsrhDnDate;
    @Column(
            name = "GSRH_DN_TIME"
    )
    private String gsrhDnTime;
    @Column(
            name = "GSRH_DN_FLAG"
    )
    private String gsrhDnFlag;
    @Column(
            name = "GSRH_GET_STATUS"
    )
    private String gsrhGetStatus;
    @Column(
            name = "GSRH_ADDR"
    )
    private String gsrhAddr;
    @Column(
            name = "GSRH_TIME"
    )
    private String gsrhTime;

    @Column(
            name = "GSRH_SPECIAL"
    )
    private String gsrhSpecial;

    @Column(
            name = "GSRH_CRE_TIME"
    )
    private String gsrhCreateTime;

    @Column(
            name = "GSRH_CHECK_DATE"
    )
    private String gsrhCheckTime;

    @Column(
            name = "GSRH_SOURCE"
    )
    private String gsrhSource;

    @Column(
            name = "GSRH_PART"
    )
    private String gsrhPart;
}
