package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;

/**

 * @author xiayuan
 * @date 2020-11-27
 */
@Data
public class GaiaWmsMenDianHuoWeiHao implements Serializable {

    private static final long serialVersionUID = 2312351290481169167L;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点（门店）
     */
    private String batSiteCode;

    /**
     * 商品编码
     */
    private String wmSpBm;

    /**
     * 货位号编号
     */
    private String wmHwhBm;

    /**
     * 货位号名称
     */
    private String wmHwhMc;

    /**
     * 创建日期
     */
    private String wmCjrq;

    /**
     * 创建时间
     */
    private String wmCjsj;

    /**
     * 创建人
     */
    private String wmCjr;

    /**
     * 修改日期
     */
    private String wmXgrq;

    /**
     * 修改时间
     */
    private String wmXgsj;

    /**
     * 修改人
     */
    private String wmXgr;


}