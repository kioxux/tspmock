package com.gys.business.mapper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author li_haixia@gov-info.cn
 * @desc 描述
 * @date 2021/11/19 15:31
 */
@Data
@Table(name = "GAIA_SD_EIMS")
public class GaiaSdEIMS  implements Serializable {
    private static final long serialVersionUID = 4605117814476359157L;
    @Id
    @Column( name= "ID")
    private Integer id;
    @Column( name ="CLIENT")
    private String client;
    @Column( name ="Data_Site")
    private String dataSite;
    @Column( name ="Data_Type")
    private Integer dataType;
    @Column( name ="Data_BillNo")
    private String dataBillNo;
    @Column( name ="Data_Date")
    private String dataDate;
    @Column( name ="Pro_Id")
    private String proId;
    @Column( name ="Pro_Spec")
    private String proSpec;
    @Column( name ="Pro_BatchNo")
    private String proBatchNo;
    @Column( name ="Pro_ValidDate")
    private String proValidDate;
    @Column( name ="Pro_ProduceDate")
    private String proProduceDate;
    @Column( name ="Pro_Unit")
    private String proUnit;
    @Column( name ="Pro_Manufacturer")
    private String proManufacturer;
    @Column( name ="Pro_Place")
    private String proPlace;
    @Column( name ="Data_InType")
    private String dataInType;
    @Column( name ="Data_OutType")
    private Integer dataOutType;
    @Column( name ="Quantity")
    private BigDecimal quantity;
    @Column( name ="Company_Name")
    private String companyName;
    @Column( name ="Reason")
    private String reason;
    @Column( name ="Relative_No")
    private String relativeNo;
    @Column( name ="Operator")
    private String operator;
    @Column( name ="Remarks")
    private String remarks;
    @Column( name ="Process_Type")
    private String processType;
    @Column( name ="Prescription_Source")
    private String prescriptionSource;
    @Column( name ="Patient_Name")
    private String patientName;
    @Column( name ="Patient_IdCard")
    private String patientIdCard;
    @Column( name ="Conclusion")
    private String conclusion;
    @Column( name ="Upload_Status")
    private Integer uploadStatus;
    @Column( name ="Fail_Reason")
    private String failReason;
    @Column( name ="Upload_Date")
    private String uploadDate;
    @Column( name ="Upload_By")
    private String uploadBy;
    @Column( name ="Upload_Id")
    private String uploadId;
}
