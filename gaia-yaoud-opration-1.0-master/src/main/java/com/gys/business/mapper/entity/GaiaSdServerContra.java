package com.gys.business.mapper.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "GAIA_SD_SERVER_CONTRA")
public class GaiaSdServerContra implements Serializable {
    private static final long serialVersionUID = -8034068921414227023L;

    @Id
    @Column(name = "CLIENT")
    private String clientId;

    @Id
    @Column(name = "GSSC_BR_ID")
    private String gsscBrId;

    @Id
    @Column(name = "GSSC_ID")
    private String gsscId;

    @Column(name = "GSSC_COMPCLASS")
    private String gsscCompclass;

    @Column(name = "GSSC_COMPCLASS_REMARK")
    private String gsscCompclassRemark;

    @Column(name = "GSSC_COMPCLASS_NAME")
    private String gsscCompclassName;

    @Column(name = "GSSC_PRIORITY")
    private Short gsscPriority;

    @Column(name = "GSSC_EXPLAIN1")
    private String gsscExplain1;

    @Column(name = "GSSC_EXPLAIN2")
    private String gsscExplain2;

    @Column(name = "GSSC_EXPLAIN3")
    private String gsscExplain3;

    @Column(name = "GSSC_EXPLAIN4")
    private String gsscExplain4;

    @Column(name = "GSSC_EXPLAIN5")
    private String gsscExplain5;

    @Column(name = "GSSC_UPDATE_DATE")
    private String gsscUpdateDate;

}
