package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdIntegralExchangeSetProDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
* @author RULAISZ
* @description 针对表【GAIA_SD_INTEGRAL_EXCHANGE_SET_PRO_DETAIL】的数据库操作Mapper
* @createDate 2021-11-29 09:48:10
* @Entity com.gys.business.mapper.entity.GaiaSdIntegralExchangeSetProDetail
*/
public interface GaiaSdIntegralExchangeSetProDetailMapper {


    List<GaiaSdIntegralExchangeSetProDetail> getByClientAndProIdAndExchangeDates(@Param("client") String client,@Param("proSelfCode") String proSelfCode,@Param("days") Set<String> days);

    void batchInsert(@Param("proDetailList") List<GaiaSdIntegralExchangeSetProDetail> proDetailList);

    List<GaiaSdIntegralExchangeSetProDetail> getByClientAndProIdAndExchangeDatesWithOutVoucherId(@Param("client") String client,@Param("voucherId") String voucherId,@Param("proSelfCode") String proSelfCode,@Param("days") Set<String> days);

    void deleteByClientAndVoucherId(@Param("client") String client,@Param("voucherId") String voucherId);

    List<GaiaSdIntegralExchangeSetProDetail> getAllByClient(@Param("client") String client, @Param("stoCode") String stoCode);
}
