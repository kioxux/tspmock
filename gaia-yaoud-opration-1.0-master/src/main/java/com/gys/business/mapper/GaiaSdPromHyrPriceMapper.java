package com.gys.business.mapper;

import com.gys.business.mapper.entity.GaiaSdPromHyrPrice;
import com.gys.business.service.data.PromHyrPriceOutData;
import com.gys.business.service.data.PromoteInData;
import com.gys.common.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdPromHyrPriceMapper extends BaseMapper<GaiaSdPromHyrPrice> {
    List<PromHyrPriceOutData> getDetail(PromoteInData inData);

    List<GaiaSdPromHyrPrice> getAllByClient(@Param("client") String client,@Param("nowDate") String nowDate,@Param("brId")String brId);
}
