//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.gys.business.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(
        name = "GAIA_SO_ITEM"
)
public class GaiaSoItem implements Serializable {
    @Id
    @Column(
            name = "CLIENT"
    )
    private String client;
    @Id
    @Column(
            name = "SO_ID"
    )
    private String soId;
    @Id
    @Column(
            name = "SO_LINE_NO"
    )
    private String soLineNo;
    @Column(
            name = "SO_PRO_CODE"
    )
    private String soProCode;
    @Column(
            name = "SO_QTY"
    )
    private BigDecimal soQty;
    @Column(
            name = "SO_UNIT"
    )
    private String soUnit;
    @Column(
            name = "SO_PRICE"
    )
    private BigDecimal soPrice;
    @Column(
            name = "SO_LINE_AMT"
    )
    private BigDecimal soLineAmt;
    @Column(
            name = "SO_SITE_CODE"
    )
    private String soSiteCode;
    @Column(
            name = "SO_LOCATION_CODE"
    )
    private String soLocationCode;
    @Column(
            name = "SO_BATCH"
    )
    private String soBatch;
    @Column(
            name = "SO_RATE"
    )
    private String soRate;
    @Column(
            name = "SO_DELIVERY_DATE"
    )
    private String soDeliveryDate;
    @Column(
            name = "SO_LINE_REMARK"
    )
    private String soLineRemark;
    @Column(
            name = "SO_LINE_DELETE"
    )
    private String soLineDelete;
    @Column(
            name = "SO_COMPLETE_FLAG"
    )
    private String soCompleteFlag;
    @Column(
            name = "SO_DELIVERED_QTY"
    )
    private BigDecimal soDeliveredQty;
    @Column(
            name = "SO_DELIVERED_AMT"
    )
    private BigDecimal soDeliveredAmt;
    @Column(
            name = "SO_INVOICE_QTY"
    )
    private BigDecimal soInvoiceQty;
    @Column(
            name = "SO_INVOICE_AMT"
    )
    private BigDecimal soInvoiceAmt;
    @Column(
            name = "SO_REFER_ORDER"
    )
    private String soReferOrder;
    @Column(
            name = "SO_REFER_ORDER_LINENO"
    )
    private String soReferOrderLineno;
    private static final long serialVersionUID = 1L;

    public GaiaSoItem() {
    }

    public String getClient() {
        return this.client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getSoId() {
        return this.soId;
    }

    public void setSoId(String soId) {
        this.soId = soId;
    }

    public String getSoLineNo() {
        return this.soLineNo;
    }

    public void setSoLineNo(String soLineNo) {
        this.soLineNo = soLineNo;
    }

    public String getSoProCode() {
        return this.soProCode;
    }

    public void setSoProCode(String soProCode) {
        this.soProCode = soProCode;
    }

    public BigDecimal getSoQty() {
        return this.soQty;
    }

    public void setSoQty(BigDecimal soQty) {
        this.soQty = soQty;
    }

    public String getSoUnit() {
        return this.soUnit;
    }

    public void setSoUnit(String soUnit) {
        this.soUnit = soUnit;
    }

    public BigDecimal getSoPrice() {
        return this.soPrice;
    }

    public void setSoPrice(BigDecimal soPrice) {
        this.soPrice = soPrice;
    }

    public BigDecimal getSoLineAmt() {
        return this.soLineAmt;
    }

    public void setSoLineAmt(BigDecimal soLineAmt) {
        this.soLineAmt = soLineAmt;
    }

    public String getSoSiteCode() {
        return this.soSiteCode;
    }

    public void setSoSiteCode(String soSiteCode) {
        this.soSiteCode = soSiteCode;
    }

    public String getSoLocationCode() {
        return this.soLocationCode;
    }

    public void setSoLocationCode(String soLocationCode) {
        this.soLocationCode = soLocationCode;
    }

    public String getSoBatch() {
        return this.soBatch;
    }

    public void setSoBatch(String soBatch) {
        this.soBatch = soBatch;
    }

    public String getSoRate() {
        return this.soRate;
    }

    public void setSoRate(String soRate) {
        this.soRate = soRate;
    }

    public String getSoDeliveryDate() {
        return this.soDeliveryDate;
    }

    public void setSoDeliveryDate(String soDeliveryDate) {
        this.soDeliveryDate = soDeliveryDate;
    }

    public String getSoLineRemark() {
        return this.soLineRemark;
    }

    public void setSoLineRemark(String soLineRemark) {
        this.soLineRemark = soLineRemark;
    }

    public String getSoLineDelete() {
        return this.soLineDelete;
    }

    public void setSoLineDelete(String soLineDelete) {
        this.soLineDelete = soLineDelete;
    }

    public String getSoCompleteFlag() {
        return this.soCompleteFlag;
    }

    public void setSoCompleteFlag(String soCompleteFlag) {
        this.soCompleteFlag = soCompleteFlag;
    }

    public BigDecimal getSoDeliveredQty() {
        return this.soDeliveredQty;
    }

    public void setSoDeliveredQty(BigDecimal soDeliveredQty) {
        this.soDeliveredQty = soDeliveredQty;
    }

    public BigDecimal getSoDeliveredAmt() {
        return this.soDeliveredAmt;
    }

    public void setSoDeliveredAmt(BigDecimal soDeliveredAmt) {
        this.soDeliveredAmt = soDeliveredAmt;
    }

    public BigDecimal getSoInvoiceQty() {
        return this.soInvoiceQty;
    }

    public void setSoInvoiceQty(BigDecimal soInvoiceQty) {
        this.soInvoiceQty = soInvoiceQty;
    }

    public BigDecimal getSoInvoiceAmt() {
        return this.soInvoiceAmt;
    }

    public void setSoInvoiceAmt(BigDecimal soInvoiceAmt) {
        this.soInvoiceAmt = soInvoiceAmt;
    }

    public String getSoReferOrder() {
        return this.soReferOrder;
    }

    public void setSoReferOrder(String soReferOrder) {
        this.soReferOrder = soReferOrder;
    }

    public String getSoReferOrderLineno() {
        return this.soReferOrderLineno;
    }

    public void setSoReferOrderLineno(String soReferOrderLineno) {
        this.soReferOrderLineno = soReferOrderLineno;
    }
}
