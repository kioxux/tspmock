package com.gys.feign;


import com.gys.business.service.data.MessageParams;
import com.gys.common.config.FeignServiceConfiguration;
import com.gys.feign.fallback.OperateFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        value = "gys-operate",
        configuration= FeignServiceConfiguration.class,
        fallback = OperateFallback.class
//                ,url = "http://172.19.1.120:10113"
)
public interface OperateService {
    @PostMapping(value = "/push/sendMessageList")
    String sendMessageList(@RequestBody MessageParams messageParams);
}
