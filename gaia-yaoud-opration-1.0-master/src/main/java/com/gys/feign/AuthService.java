package com.gys.feign;

import com.gys.business.service.data.GetWfAuditInData;
import com.gys.business.service.data.GetWfCreateInData;
import com.gys.common.config.FeignServiceConfiguration;
import com.gys.feign.fallback.AuthFallback;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        value = "gys-auth",
        configuration= FeignServiceConfiguration.class,
        fallback = AuthFallback.class
//                ,url = "http://127.0.0.1:20103"
)
public interface AuthService {
    @PostMapping({"/createWorkflow"})
    @Headers("Content-Type: application/json")
    String createWorkflow(@RequestBody GetWfCreateInData param);

    /**
     * 获取工作流审批状态
     * @return
     */
    @PostMapping("/workflow/getByClientAndTitle")
    String getByClientAndTitle(@RequestBody GetWfAuditInData param);
}
