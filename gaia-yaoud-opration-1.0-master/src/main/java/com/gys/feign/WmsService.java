package com.gys.feign;

import com.gys.business.service.data.BatchInfoInData;
import com.gys.business.service.data.SaleProxyReplenishmentsInputData;
import com.gys.common.config.FeignServiceConfiguration;
import com.gys.feign.fallback.WmsFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        value = "wms",
        configuration= FeignServiceConfiguration.class,
        fallback = WmsFallback.class
//        url = "http://172.19.1.121:10109"
)
public interface WmsService {
    @PostMapping({"/web/api/v1/thirdPartyManager/external/savebatchInfo"})
    String saveBatchInfo(@RequestBody BatchInfoInData inData);
}
