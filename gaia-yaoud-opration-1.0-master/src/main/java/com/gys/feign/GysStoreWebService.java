package com.gys.feign;

import com.gys.business.service.data.RecallForm;
import com.gys.business.service.data.SaleProxyReplenishmentsInputData;
import com.gys.common.config.FeignServiceConfiguration;
import com.gys.feign.fallback.GysStoreWebFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(
        value = "gys-store-web",
        configuration= FeignServiceConfiguration.class,
        fallback = GysStoreWebFallback.class
//       , url = "http://172.19.1.120:10114"
)
public interface GysStoreWebService {
    @PostMapping({"/external/saleProxyReplenishments"})
    String saleProxyReplenishments(@RequestBody SaleProxyReplenishmentsInputData inData);

    /**
     * 连锁公司
     * @return
     */
    @PostMapping("/gaiarecallinfo/getChainCompany")
    String getChainCompany();

    /**
     * 商品调库-下发确认
     * @param recallForm
     * @return
     */
    @PostMapping("/gaiarecallinfo/recall")
    String recall(@RequestBody RecallForm recallForm, @RequestHeader(name = "X-Token",required = true) String token);
}
