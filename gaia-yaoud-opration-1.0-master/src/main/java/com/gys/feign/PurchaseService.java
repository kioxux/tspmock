package com.gys.feign;

import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.mapper.entity.GaiaSdReplenishHExt;
import com.gys.business.mapper.entity.NewDistributionPlan;
import com.gys.business.service.data.MaterialDocRequestDto;
import com.gys.business.service.data.yaoshibang.CustomerInfoBean;
import com.gys.business.service.data.yaoshibang.GaiaSoHeader;
import com.gys.business.service.data.yaoshibang.GoodsQueryBean;
import com.gys.common.config.FeignServiceConfiguration;
import com.gys.common.data.JsonResult;
import com.gys.feign.fallback.PurchaseFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "gys-purchase", configuration = FeignServiceConfiguration.class, fallbackFactory = PurchaseFallback.class
//,url = "http://172.19.10.36:10108"
)
public interface PurchaseService {
    /**
     * 物料凭证
     * @param inData
     */
    @PostMapping({"/api/insertMaterialDoc"})
    String insertMaterialDoc(@RequestBody List<MaterialDocRequestDto> inData);
    @PostMapping("/api/distributionPlan/confirmCall")
    String confirmCall(NewDistributionPlan distributionPlan);

    @PostMapping("/api/distributionPlan/confirmReplenish")
    String confirmReplenish(NewDistributionPlan distributionPlan);

    /**
     * 批发销售订单创建
     */
    @PostMapping({"/salesOrder/saveSalesOrder"})
    String saveSalesOrder(@RequestBody GaiaSoHeader gaiaSoHeader);

    /**
     * 销售主体查询
     */
    @GetMapping({"/base/getDcDataList"})
    String getDcDataList(@RequestParam(value = "client") String client);

    /**
     * 送达方查询
     */
    @GetMapping({"/purchase/customerInfo"})
    String customerInfo(@RequestBody CustomerInfoBean bean);
    /**
     * 商品查询
     */
    @PostMapping({"/salesOrder/queryGoodsList"})
    String queryGoodsList(@RequestBody GoodsQueryBean bean);

    /**
     * 自动开单
     */
    @PostMapping({"/storeNeed/automaticBilling"})
    JsonResult automaticBilling(@RequestBody List<GaiaSdReplenishHExt> replenishHList);

}
