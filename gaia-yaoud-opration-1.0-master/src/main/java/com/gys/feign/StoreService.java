package com.gys.feign;

import com.gys.business.service.data.GetWfApproveInData;
import com.gys.common.config.FeignServiceConfiguration;
import com.gys.feign.fallback.StoreFallback;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        value = "gys-store-app",
        configuration= FeignServiceConfiguration.class,
        fallback = StoreFallback.class
//        ,url = "172.19.1.120:10112"
)
public interface StoreService {
    @PostMapping({"/approval/callback"})
    @Headers("Content-Type: application/json")
    String approval(@RequestBody GetWfApproveInData param);
}
