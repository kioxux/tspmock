package com.gys.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.data.BatchInfoInData;
import com.gys.business.service.data.SaleProxyReplenishmentsInputData;
import com.gys.feign.WmsService;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

@Component
public class WmsFallback implements WmsService {
    @Override
    public String saveBatchInfo(@RequestBody BatchInfoInData inData){
        JSONObject json = new JSONObject();
        json.put("code", "-1");
        json.put("message", "调用接口失败,wms.saveBatchInfo()");
        return json.toJSONString();
    }

}
