package com.gys.feign.fallback;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.data.GetWfApproveInData;
import com.gys.feign.StoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class StoreFallback implements StoreService {
    @Override
    public String approval(GetWfApproveInData param) {
        JSONObject json = new JSONObject();
        json.put("code", "-1");
        json.put("message", "调用接口失败,gys-store-app.approval()");
        return json.toJSONString();
    }
}
