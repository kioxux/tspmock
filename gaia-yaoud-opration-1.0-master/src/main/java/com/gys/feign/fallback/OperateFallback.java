package com.gys.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.data.MessageParams;
import com.gys.feign.OperateService;
import org.springframework.stereotype.Component;

@Component
public class OperateFallback implements OperateService {
    @Override
    public String sendMessageList(MessageParams messageParams) {
        JSONObject json = new JSONObject();
        json.put("code", "-1");
        json.put("message", "调用接口失败,gys-operate.sendMessageList()");
        return json.toJSONString();
    }
}
