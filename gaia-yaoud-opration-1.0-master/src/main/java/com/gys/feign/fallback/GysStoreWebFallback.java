package com.gys.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.data.RecallForm;
import com.gys.business.service.data.SaleProxyReplenishmentsInputData;
import com.gys.feign.GysStoreWebService;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

@Component
public class GysStoreWebFallback implements GysStoreWebService {
    @Override
    public String saleProxyReplenishments(@RequestBody SaleProxyReplenishmentsInputData inData){
        JSONObject json = new JSONObject();
        json.put("code", "-1");
        json.put("message", "调用接口失败,gys-store-web.saleProxyReplenishments()");
        return json.toJSONString();
    }

    @Override
    public String getChainCompany() {
        JSONObject json = new JSONObject();
        json.put("code", "-1");
        json.put("message", "调用接口失败,gys-store-web.getChainCompany()");
        return json.toJSONString();
    }

    @Override
    public String recall(@RequestBody RecallForm recallForm,String token) {
        JSONObject json = new JSONObject();
        json.put("code", "-1");
        json.put("message", "调用接口失败,gys-store-web.recall()");
        return json.toJSONString();
    }
}
