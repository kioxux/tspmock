package com.gys.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gys.common.data.CategoryModelForm;
import com.gys.feign.ReportService;
import org.springframework.stereotype.Component;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/28 16:04
 **/
@Component
public class ReportFallback implements ReportService {

    @Override
    public String getCategoryModelRank(CategoryModelForm categoryModelForm) {
        JSONObject result = new JSONObject();
        result.put("code", -1);
        result.put("message", "查询品类模型排名异常");
        return result.toJSONString();
    }
}
