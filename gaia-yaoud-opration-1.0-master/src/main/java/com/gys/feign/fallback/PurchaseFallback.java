package com.gys.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gys.business.mapper.entity.GaiaSdReplenishH;
import com.gys.business.mapper.entity.GaiaSdReplenishHExt;
import com.gys.business.mapper.entity.NewDistributionPlan;
import com.gys.business.service.data.MaterialDocRequestDto;
import com.gys.business.service.data.yaoshibang.CustomerInfoBean;
import com.gys.business.service.data.yaoshibang.GaiaSoHeader;
import com.gys.business.service.data.yaoshibang.GoodsQueryBean;
import com.gys.common.data.JsonResult;
import com.gys.feign.PurchaseService;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PurchaseFallback implements FallbackFactory<PurchaseService> {
    private static final Logger logger = LoggerFactory.getLogger(PurchaseFallback.class);


    @Override
    public PurchaseService create(Throwable throwable) {
        return new PurchaseService() {
            @Override
            public String insertMaterialDoc(List<MaterialDocRequestDto> inData) {
                logger.error("insertMaterialDoc fegin fallback Exception:",throwable);
                JSONObject json = new JSONObject();
                json.put("code", "-1");
                json.put("message", "调用接口失败,gys-purchase.insertMaterialDoc()");
                return json.toJSONString();
            }
            @Override
            public String confirmCall(NewDistributionPlan distributionPlan) {
                logger.error("confirmCall fegin fallback Exception:",throwable);
                JSONObject json = new JSONObject();
                json.put("code", "-1");
                json.put("message", "调用接口失败,gys-purchase.confirmCall()");
                return json.toJSONString();
            }

            @Override
            public String confirmReplenish(NewDistributionPlan distributionPlan) {
                logger.error("confirmReplenish fegin fallback Exception:",throwable);
                JSONObject json = new JSONObject();
                json.put("code", "-1");
                json.put("message", "调用接口失败,gys-purchase.confirmReplenish()");
                return json.toJSONString();
            }

            @Override
            public String saveSalesOrder(GaiaSoHeader gaiaSoHeader) {
                logger.error("saveSalesOrder fegin fallback Exception:",throwable);
                return null;
            }

            @Override
            public String getDcDataList(String client) {
                logger.error("getDcDataList fegin fallback Exception:",throwable);
                return null;
            }

            @Override
            public String customerInfo(CustomerInfoBean bean) {
                logger.error("customerInfo fegin fallback Exception:",throwable);
                return null;
            }

            @Override
            public String queryGoodsList(GoodsQueryBean bean){
                logger.error("queryGoodsList fegin fallback Exception:",throwable);
                return null;
            }

            @Override
            public JsonResult automaticBilling(List<GaiaSdReplenishHExt> replenishHList) {
                logger.error("queryGoodsList fegin fallback Exception:",throwable);
                return null;
            }
        };
    }
}
