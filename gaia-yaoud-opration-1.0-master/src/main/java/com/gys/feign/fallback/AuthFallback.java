package com.gys.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gys.business.service.data.GetWfAuditInData;
import com.gys.business.service.data.GetWfCreateInData;
import com.gys.feign.AuthService;
import org.springframework.stereotype.Component;

@Component
public class AuthFallback implements AuthService {
    @Override
    public String createWorkflow(GetWfCreateInData param) {
        JSONObject json = new JSONObject();
        json.put("code", "-1");
        json.put("message", "创建工作流失败");
        return json.toJSONString();
    }

    @Override
    public String getByClientAndTitle(GetWfAuditInData param) {
        return null;
    }
}
