package com.gys.feign;


import com.gys.common.config.FeignServiceConfiguration;
import com.gys.common.data.CategoryModelForm;
import com.gys.feign.fallback.OperateFallback;
import com.gys.feign.fallback.ReportFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        value = "gys-report",
        configuration= FeignServiceConfiguration.class,
        fallback = ReportFallback.class
)
public interface ReportService {

    @PostMapping(value = "/product/analyse/productAnalyseList")
    String getCategoryModelRank(@RequestBody CategoryModelForm categoryModelForm);

}
