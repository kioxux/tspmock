package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductUpdateItemKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 字段
     */
    private String gpuiField;
}