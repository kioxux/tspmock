package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaDcReplenishExclud extends GaiaDcReplenishExcludKey {
    /**
     * 补货量
     */
    private Integer gdreReplenishNum;
}