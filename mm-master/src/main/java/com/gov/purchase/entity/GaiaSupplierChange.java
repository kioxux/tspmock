package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSupplierChange {
    /**
     * 唯一值
     */
    private Integer id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String supSite;

    /**
     * 供应商自编码
     */
    private String supSelfCode;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 统一社会信用代码
     */
    private String supCreditCode;

    /**
     * 变更字段(JAVA实体属性名)
     */
    private String supChangeField;

    /**
     * 变更字段(DB字段名)
     */
    private String supChangeColumn;

    /**
     * 变更字段名(DB字段注释)
     */
    private String supChangeColumnDes;

    /**
     * 变更前值
     */
    private String supChangeFrom;

    /**
     * 变更后值
     */
    private String supChangeTo;

    /**
     * 变更人
     */
    private String supChangeUser;

    /**
     * 变更日期
     */
    private String supChangeDate;

    /**
     * 变更时间
     */
    private String supChangeTime;

    /**
     * 工作流编号
     */
    private String supFlowNo;

    /**
     * 是否审批结束
     */
    private String supSfsp;

    /**
     * 审批状态
     */
    private String supSfty;

    /**
     * 备注
     */
    private String supRemarks;
}