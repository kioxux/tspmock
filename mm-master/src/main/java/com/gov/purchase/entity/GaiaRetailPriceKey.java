package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaRetailPriceKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String prcStore;

    /**
     * 商品编码
     */
    private String prcProduct;

    /**
     * 价格类型
     */
    private String prcClass;

    /**
     * 调价单号
     */
    private String prcModfiyNo;
}