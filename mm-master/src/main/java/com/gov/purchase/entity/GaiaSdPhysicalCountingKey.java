package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdPhysicalCountingKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 盘点单号
     */
    private String gspcVoucherId;
}