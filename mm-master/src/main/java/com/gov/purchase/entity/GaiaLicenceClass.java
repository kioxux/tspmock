package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaLicenceClass {
    /**
     * 证照编码
     */
    private String perCode;

    /**
     * 证照名称
     */
    private String perCodeName;

    /**
     * 证照状态
     */
    private String perCodeStatus;
}