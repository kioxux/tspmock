package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaInvoicePayingBasic extends GaiaInvoicePayingBasicKey {
    /**
     * 纳税主体编码
     */
    private String ficoTaxSubjectCode;


    /**
     * 纳税主体名称
     */
    private String ficoTaxSubjectName;

    /**
     * 发票类型
     */
    private String ficoInvoiceType;

    /**
     * 统一社会信用代码
     */
    private String ficoSocialCreditCode;

    /**
     * 开户行名称
     */
    private String ficoBankName;

    /**
     * 开户行账号
     */
    private String ficoBankNumber;

    /**
     * 地址
     */
    private String ficoCompanyAddress;

    /**
     * 电话号码
     */
    private String ficoCompanyPhoneNumber;

    /**
     * 付款银行名称
     */
    private String ficoPayingbankName;

    /**
     * 付款银行账号
     */
    private String ficoPayingbankNumber;

    /**
     * 纳税属性
     */
    private Integer ficoTaxAttributes;
}