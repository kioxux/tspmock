package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaJyfwData {
    /**
     * id
     */
    private String id;

    /**
     * 加盟商ID
     */
    private String client;

    /**
     * 机构编码
     */
    private String jyfwOrgid;

    /**
     * 机构名称
     */
    private String jyfwOrgname;

    /**
     * 经营范围编码
     */
    private String jyfwId;

    /**
     * 经营范围描述
     */
    private String jyfwName;

    /**
     * 创建日期
     */
    private String jyfwCreDate;

    /**
     * 创建时间
     */
    private String jyfwCreTime;

    /**
     * 创建人账号
     */
    private String jyfwCreId;

    /**
     * 修改日期
     */
    private String jyfwModiDate;

    /**
     * 修改时间
     */
    private String jyfwModiTime;

    /**
     * 修改人账号
     */
    private String jyfwModiId;

    /**
     * 地点
     */
    private String jyfwSite;

    /**
     * 类型 1：供应商 2：客户 3：其他（连锁、门店、批发）
     */
    private Integer jyfwType;
}