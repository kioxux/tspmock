package com.gov.purchase.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GaiaSdReplenishH extends GaiaSdReplenishHKey {
    /**
     * 补货日期
     */
    private String gsrhDate;

    /**
     * 创建时间
     */
    private String gsrhCreTime;

    /**
     * 补货类型
     */
    private String gsrhType;

    /**
     * 补货地点
     */
    private String gsrhAddr;

    /**
     * 补货金额
     */
    private BigDecimal gsrhTotalAmt;

    /**
     * 补货数量
     */
    private BigDecimal gsrhTotalQty;

    /**
     * 补货人员
     */
    private String gsrhEmp;

    /**
     * 补货状态是否审核
     */
    private String gsrhStatus;

    /**
     * 是否转采购订单
     */
    private String gsrhFlag;

    /**
     * 采购单号
     */
    private String gsrhPoid;

    /**
     * 补货方式 0正常补货，1紧急补货,2铺货,3互调
     */
    private String gsrhPattern;

    /**
     * 开单人
     */
    private String gsrhDnBy;

    /**
     * 开单日期
     */
    private String gsrhDnDate;

    /**
     * 开单时间
     */
    private String gsrhDnTime;

    /**
     * 开单标记 0未开单，1已开单
     */
    private String gsrhDnFlag;

    /**
     * 比价获取状态 0获取中, 1待获取，2已获取
     */
    private String gsrhGetStatus;

    private String gsrhSource;
}