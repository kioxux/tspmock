package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaUnitKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 计量单位编码
     */
    private String unitCode;
}