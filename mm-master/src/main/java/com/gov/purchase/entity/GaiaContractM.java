package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaContractM extends GaiaContractMKey {
    /**
     * 商品编码
     */
    private String conProCode;

    /**
     * 数量
     */
    private BigDecimal conQty;

    /**
     * 含税单价
     */
    private BigDecimal conPrice;

    /**
     * 行金额
     */
    private BigDecimal conLineAmt;

    /**
     * 税率
     */
    private String conProInputTax;

    /**
     * 行备注
     */
    private String conRemark;

    /**
     * 创建日期
     */
    private String conCreateDate;

    /**
     * 创建人
     */
    private String conCreateBy;

    /**
     * 创建时间
     */
    private String conCreateTime;

    /**
     * 更新日期
     */
    private String conUpdateDate;

    /**
     * 更新人
     */
    private String conUpdateBy;

    /**
     * 更新时间
     */
    private String conUpdateTime;
}