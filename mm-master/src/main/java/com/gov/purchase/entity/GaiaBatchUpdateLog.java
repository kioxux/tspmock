package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBatchUpdateLog {
    /**
     * 批量操作编码
     */
    private String bulUpdateCode;

    /**
     * 数据类型
     */
    private String bulDateType;

    /**
     * 操作类型
     */
    private String bulUpdateType;

    /**
     * 操作状态
     */
    private String bulUpdateStatus;

    /**
     * 文件地址
     */
    private String bulRouteAddress;

    /**
     * 创建人
     */
    private String bulCreateBy;

    /**
     * 创建日期
     */
    private String bulCreateDate;

    /**
     * 创建时间
     */
    private String bulCreateTime;
}