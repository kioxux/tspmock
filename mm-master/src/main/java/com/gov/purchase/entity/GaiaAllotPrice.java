package com.gov.purchase.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GaiaAllotPrice {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 收货地点
     */
    private String alpReceiveSite;

    /**
     * 商品编码
     */
    private String alpProCode;

    /**
     * 加价金额
     */
    private BigDecimal alpAddAmt;

    /**
     * 加价比例
     */
    private BigDecimal alpAddRate;

    /**
     * 创建人
     */
    private String alpCreateBy;

    /**
     * 创建日期
     */
    private String alpCreateDate;

    /**
     * 创建时间
     */
    private String alpCreateTime;

    /**
     * 更新人
     */
    private String alpUpdateBy;

    /**
     * 更新日期
     */
    private String alpUpdateDate;

    /**
     * 更新时间
     */
    private String alpUpdateTime;

    /**
     * 目录价
     */
    private BigDecimal alpCataloguePrice;
}