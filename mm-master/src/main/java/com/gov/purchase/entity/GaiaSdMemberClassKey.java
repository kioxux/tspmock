package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMemberClassKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 卡类型编号
     */
    private String gmcId;
}