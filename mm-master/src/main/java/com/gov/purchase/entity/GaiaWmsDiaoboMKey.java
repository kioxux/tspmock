package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWmsDiaoboMKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 配送单号
     */
    private String wmPsdh;

    /**
     * 
     */
    private Long id;
}