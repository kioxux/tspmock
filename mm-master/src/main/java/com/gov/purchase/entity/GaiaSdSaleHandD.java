package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleHandD extends GaiaSdSaleHandDKey {
    /**
     * 店号
     */
    private String gshdBrId;

    /**
     * 销售日期
     */
    private String gshdDate;

    /**
     * 行号
     */
    private String gshdSerial;

    /**
     * 商品编码
     */
    private String gshdProId;

    /**
     * 商品批号
     */
    private String gshdBatch;

    /**
     * 商品有效期
     */
    private String gshdValidDate;

    /**
     * 监管码/条形码
     */
    private String gshdStCode;

    /**
     * 单品零售价
     */
    private String gshdPrc1;

    /**
     * 单品应收价
     */
    private String gshdPrc2;

    /**
     * 数量
     */
    private String gshdQty;

    /**
     * 汇总应收金额
     */
    private String gshdAmt;

    /**
     * 兑换单个积分
     */
    private String gshdIntegralExchange;

    /**
     * 兑换积分合计
     */
    private String gshdIntegralExchangeAmt;

    /**
     * 兑换积分数量
     */
    private String gshdIntegralExchangeQty;

    /**
     * 换购使用积分
     */
    private String gshdIntegralHg;

    /**
     * 抵现使用积分
     */
    private String gshdIntegralDy;

    /**
     * 商品折扣金额
     */
    private String gshdZkAmt;

    /**
     * 积分兑换折扣金额
     */
    private String gshdZkJf;

    /**
     * 促销折扣金额
     */
    private String gshdZkPm;

    /**
     * 营业员编号
     */
    private String gshdSalerId;

    /**
     * 医生编号
     */
    private String gshdDoctorId;

    /**
     * 参加促销类型编号
     */
    private String gshdPmId;

    /**
     * 参加促销类型说明
     */
    private String gshdPmContent;
}