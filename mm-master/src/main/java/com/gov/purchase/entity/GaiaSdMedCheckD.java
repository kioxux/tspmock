package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMedCheckD extends GaiaSdMedCheckDKey {
    /**
     * 门店
     */
    private String gsmcdBrId;

    /**
     * 店名
     */
    private String gsmcdBrName;

    /**
     * 商品编码
     */
    private String gsmcdProId;

    /**
     * 批号
     */
    private String gsmcdBatchNo;

    /**
     * 养护数量
     */
    private String gsmcdQty;

    /**
     * 养护结论
     */
    private String gsmcdResult;
}