package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaWmsDiaoboM extends GaiaWmsDiaoboMKey {
    /**
     * 商品编码
     */
    private String wmSpBm;

    /**
     * 批次号
     */
    private String wmPch;

    /**
     * 库存状态编号
     */
    private String wmKcztBh;

    /**
     * 配送单副号
     */
    private String wmPsdfh;

    /**
     * 出库价
     */
    private BigDecimal wmCkj;

    /**
     * 出库原数量
     */
    private BigDecimal wmCkysl;

    /**
     * 过帐数量
     */
    private BigDecimal wmGzsl;

    /**
     * 修改日期
     */
    private String wmXgrq;

    /**
     * 修改时间
     */
    private String wmXgsj;

    /**
     * 修改人
     */
    private String wmXgr;
}