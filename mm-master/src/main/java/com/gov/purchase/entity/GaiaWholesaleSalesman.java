package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWholesaleSalesman {
    /**
     * 主键
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商地点
     */
    private String supSite;

    /**
     * 销售员编号
     */
    private String gwsCode;

    /**
     * 销售员姓名
     */
    private String gssName;

    /**
     * 业务员编号
     */
    private String gssCode;

    /**
     * 创建人
     */
    private String gwsCreateUser;

    /**
     * 创建日期
     */
    private String gwsCreateDate;

    /**
     * 创建时间
     */
    private String gwsCreateTime;

    /**
     * 更新人
     */
    private String gwsUpdateUser;

    /**
     * 更新日期
     */
    private String gwsUpdateDate;

    /**
     * 更新时间
     */
    private String gwsUpdateTime;
}