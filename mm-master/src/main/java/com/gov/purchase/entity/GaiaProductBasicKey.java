package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductBasicKey {
    private String client;

    private String proCode;
}