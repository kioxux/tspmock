package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaMaterialDocKey {
    /**
     * 
     */
    private String client;

    /**
     * 物料凭证号
     */
    private String matId;

    /**
     * 物料凭证年份
     */
    private String matYear;

    /**
     * 物料凭证行号
     */
    private String matLineNo;
}