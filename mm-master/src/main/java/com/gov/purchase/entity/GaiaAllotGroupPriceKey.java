package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaAllotGroupPriceKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地址
     */
    private String gagpSite;

    /**
     * 类型
     */
    private Integer gagpType;

    /**
     * 价格组编号
     */
    private String gagpCode;

    /**
     * 客户
     */
    private String gagpCus;
}