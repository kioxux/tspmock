package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdReturnDepotHKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 退库单号
     */
    private String gsrdhVoucherId;
}