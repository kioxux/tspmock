package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaCompadmKey {
    /**
     * 加盟商ID
     */
    private String client;

    /**
     * 连锁总部ID
     */
    private String compadmId;
}