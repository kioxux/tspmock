package com.gov.purchase.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class GaiaProductBusiness extends GaiaProductBusinessKey {
    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 匹配状态 0-未匹配，1-部分匹配，2-完全匹配
     */
    private String proMatchStatus;

    /**
     * 通用名称
     */
    private String proCommonname;

    /**
     * 商品描述
     */
    private String proDepict;

    /**
     * 助记码
     */
    private String proPym;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 剂型
     */
    private String proForm;

    /**
     * 细分剂型
     */
    private String proPartform;

    /**
     * 最小剂量（以mg/ml计算）
     */
    private String proMindose;

    /**
     * 总剂量（以mg/ml计算）
     */
    private String proTotaldose;

    /**
     * 国际条形码1
     */
    private String proBarcode;

    /**
     * 国际条形码2
     */
    private String proBarcode2;

    /**
     * 批准文号分类 1-国产药品，2-进口药品，3-国产器械，4-进口器械，5-中药饮片，6-特殊化妆品，7-消毒用品，8-保健食品，9-QS商品，10-其它
     */
    private String proRegisterClass;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 批准文号批准日期
     */
    private String proRegisterDate;

    /**
     * 批准文号失效日期
     */
    private String proRegisterExdate;

    /**
     * 商品分类
     */
    private String proClass;

    /**
     * 商品分类描述
     */
    private String proClassName;

    /**
     * 成分分类
     */
    private String proCompclass;

    /**
     * 成分分类描述
     */
    private String proCompclassName;

    /**
     * 处方类别 1-处方药，2-甲类OTC，3-乙类OTC，4-双跨处方
     */
    private String proPresclass;

    /**
     * 生产企业代码
     */
    private String proFactoryCode;

    /**
     * 生产企业
     */
    private String proFactoryName;

    /**
     * 商标
     */
    private String proMark;

    /**
     * 品牌标识名
     */
    private String proBrand;

    /**
     * 品牌区分 1-全国品牌，2-省份品牌，3-地区品牌，4-本地品牌，5-其它
     */
    private String proBrandClass;

    /**
     * 保质期
     */
    private String proLife;

    /**
     * 保质期单位 1-天，2-月，3-年
     */
    private String proLifeUnit;

    /**
     * 上市许可持有人
     */
    private String proHolder;

    /**
     * 进项税率
     */
    private String proInputTax;

    /**
     * 销项税率
     */
    private String proOutputTax;

    /**
     * 药品本位码
     */
    private String proBasicCode;

    /**
     * 税务分类编码
     */
    private String proTaxClass;

    /**
     * 管制特殊分类 1-毒性药品，2-麻醉药品，3-一类精神药品，4-二类精神药品，5-易制毒药品（麻黄碱），6-放射性药品，7-生物制品（含胰岛素），8-兴奋剂（除胰岛素），9-第一类器械，10-第二类器械，11-第三类器械，12-其它管制
     */
    private String proControlClass;

    /**
     * 生产类别 1-辅料，2-化学药品，3-生物制品，4-中药，5-器械
     */
    private String proProduceClass;

    /**
     * 贮存条件 1-常温，2-阴凉，3-冷藏
     */
    private String proStorageCondition;

    /**
     * 商品仓储分区 1-内服药品，2-外用药品，3-中药饮片区，4-精装中药区，5-针剂，6-二类精神药品，7-含麻药品，8-冷藏商品，9-外用非药，10-医疗器械，11-食品，12-保健食品，13-易燃商品区
     */
    private String proStorageArea;

    /**
     * 长（以MM计算）
     */
    private String proLong;

    /**
     * 宽（以MM计算）
     */
    private String proWide;

    /**
     * 高（以MM计算）
     */
    private String proHigh;

    /**
     * 中包装量
     */
    private String proMidPackage;

    /**
     * 大包装量
     */
    private String proBigPackage;

    /**
     * 启用电子监管码 0-否，1-是
     */
    private String proElectronicCode;

    /**
     * 生产经营许可证号
     */
    private String proQsCode;

    /**
     * 最大销售量
     */
    private String proMaxSales;

    /**
     * 说明书代码
     */
    private String proInstructionCode;

    /**
     * 说明书内容
     */
    private String proInstruction;

    /**
     * 国家医保品种 0-否，1-是
     */
    private String proMedProdct;

    /**
     * 商品状态 0 可用 1不可用
     */
    private String proStatus;

    /**
     * 国家医保品种编码
     */
    private String proMedProdctcode;

    /**
     * 生产国家
     */
    private String proCountry;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 可服用天数
     */
    private String proTakeDays;

    /**
     * 用法用量
     */
    private String proUsage;

    /**
     * 禁忌说明
     */
    private String proContraindication;

    /**
     * 商品定位
     */
    private String proPosition;

    /**
     * 禁止销售
     */
    private String proNoRetail;

    /**
     * 禁止采购
     */
    private String proNoPurchase;

    /**
     * 禁止配送
     */
    private String proNoDistributed;

    /**
     * 禁止退厂
     */
    private String proNoSupplier;

    /**
     * 禁止退仓
     */
    private String proNoDc;

    /**
     * 禁止调剂
     */
    private String proNoAdjust;

    /**
     * 禁止批发
     */
    private String proNoSale;

    /**
     * 禁止请货
     */
    private String proNoApply;

    /**
     * 是否拆零 1是0否
     */
    private String proIfpart;

    /**
     * 拆零单位
     */
    private String proPartUint;

    /**
     * 拆零比例
     */
    private String proPartRate;

    /**
     * 采购单位
     */
    private String proPurchaseUnit;

    /**
     * 采购员
     */
    private String proPurchaseRate;

    /**
     * 采购单位
     */
    private String proSaleUnit;

    /**
     * 采购比例
     */
    private String proSaleRate;

    /**
     * 最小订货量
     */
    private BigDecimal proMinQty;

    /**
     * 是否医保
     */
    private String proIfMed;

    /**
     * 销售级别
     */
    private String proSlaeClass;

    /**
     * 限购数量
     */
    private BigDecimal proLimitQty;

    /**
     * 特殊药品每单最大配货量
     */
    private BigDecimal proMaxQty;

    /**
     * 按中包装配货
     */
    private String proPackageFlag;

    /**
     * 重点养护
     */
    private String proKeyCare;

    /**
     * 中药规格
     */
    private String proTcmSpecs;

    /**
     * 中药批准文号
     */
    private String proTcmRegisterNo;

    /**
     * 中药生产企业代码
     */
    private String proTcmFactoryCode;

    /**
     * 中药产地
     */
    private String proTcmPlace;

    /**
     * 固定货位
     */
    private String proFixBin;

    /**
     * 创建日期
     */
    private String proCreateDate;

    /**
     * 参考进货价
     */
    private BigDecimal proCgj;

    /**
     * 参考零售价
     */
    private BigDecimal proLsj;

    /**
     * 参考毛利率
     */
    private BigDecimal proMll;

    /**
     * 国家医保目录编号
     */
    private String proMedListnum;

    /**
     * 国家医保目录名称
     */
    private String proMedListname;

    /**
     * 国家医保医保目录剂型
     */
    private String proMedListform;

    /**
     * 外包装图片
     */
    private String proPictureWbz;

    /**
     * 说明书图片
     */
    private String proPictureSms;

    /**
     * 批件图片
     */
    private String proPicturePj;

    /**
     * 内包装图片
     */
    private String proPictureNbz;

    /**
     * 生产企业证照图片
     */
    private String proPictureZz;

    /**
     * 其他图片
     */
    private String proPictureQt;

    /**
     * 自定义字段1
     */
    private String proZdy1;

    /**
     * 自定义字段2
     */
    private String proZdy2;

    /**
     * 自定义字段3
     */
    private String proZdy3;

    /**
     * 自定义字段4
     */
    private String proZdy4;

    /**
     * 自定义字段5
     */
    private String proZdy5;

    /**
     * 商品自分类
     */
    private String proSclass;

    /**
     * 医保刷卡数量
     */
    private String proMedQty;

    /**
     * 医保编码
     */
    private String proMedId;

    /**
     * 不打折商品
     */
    private String proBdz;

    /**
     * 特殊属性：1-防疫
     */
    private String proTssx;

    /**
     * 医保类型：1-甲类 2-乙类
     */
    private String proYblx;

    /**
     * 批文注册证号
     */
    private String proPwzcz;

    /**
     * 质量标准
     */
    private String proZlbz;

    /**
     *
     */
    private String proOut;

    /**
     * 是否批发：0-否 1-是
     */
    private String proIfWholesale;

    /**
     * 经营类别
     */
    private String proJylb;

    /**
     * 更新人
     */
    private String proUpdateUser;

    /**
     * 特殊药品每月最大配货量
     */
    private BigDecimal proMaxQtyMouth;

    /**
     * 医疗器械注册人
     */
    private String proYlqxZcr;

    /**
     * 医疗器械备案人
     */
    private String proYlqxBar;

    /**
     * 医疗器械受托生产企业
     */
    private String proYlqxScqy;

    /**
     * 医疗器械受托生产企业许可证号
     */
    private String proYlqxScqyxkz;

    /**
     * 医疗器械备案凭证号
     */
    private String proYlqxBapzh;

    /**
     * 备注
     */
    private String proBeizhu;

    /**
     * 是否发送购药提醒：0-否 1-是
     */
    private String proIfSms;

    /**
     * 服药剂量单位
     */
    private String proDoseUnit;

    /**
     * 经营管理类别
     */
    private String proJygllb;

    /**
     * 档案号
     */
    private String proDah;

    /**
     * 会员价
     */
    private BigDecimal proHyj;

    /**
     * 国批价
     */
    private BigDecimal proGpj;

    /**
     * 国零价
     */
    private BigDecimal proGlj;

    /**
     * 预设售价1
     */
    private BigDecimal proYsj1;

    /**
     * 预设售价2
     */
    private BigDecimal proYsj2;

    /**
     * 预设售价3
     */
    private BigDecimal proYsj3;

    /**
     * 煎煮属性 多值则以/隔开
     */
    private String proJzsx;

    /**
     * 生产经营许可证有效期
     */
    private String proScjyxkzyxq;

    /**
     * 是否虚拟商品 0-否，1-是
     */
    private String proXnp;

    /**
     * 国家医保支付价
     */
    private BigDecimal proGjybzfj;

    /**
     * 主治功能
     */
    private String proFunction;
    /**
     * 有效期
     */
    private String proLifeDate;
    /**
     * 供货单位名称
     */
    private String proSupplyName;
    /**
     * 供货单位地址
     */
    private String proSupplyAdd;
    /**
     * 供货单位联系人
     */
    private String proSupplyContact;
    /**
     * 供货单位联系电话
     */
    private String proSupplyTel;
    /**
     * 是否维价商品：0-否，1-是
     */
    private Integer isWjsp;

    /**
     * 备案人/注册人住所
     */
    private String proBarzcrzs;

    /**
     * 改价最低价格
     */
    private BigDecimal proLowerPrice;

    /**
     * 是否上传药师帮
     */
    private String proYsbCode;

    /**
     * 销售计算排除
     */
    private String proQueryOut;

    /**
     * HIS剂量
     */
    private String proHisJl;
    /**
     * HIS制剂
     */
    private String proHisZj;
    /**
     * HIS制剂单位
     */
    private String proHisZjdw;
    /**
     * HIS拆零价
     */
    private BigDecimal proHisClj;
    /**
     * HIS智能发药
     */
    private String proHisZnfy;
    /**
     * HIS初诊成本价
     */
    private BigDecimal proHisCzcbj;
    /**
     * HIS初诊销售价
     */
    private BigDecimal proHisCzxsj;
    /**
     * HIS复诊成本价
     */
    private BigDecimal proHisFzcbj;
    /**
     * HIS复诊销售价
     */
    private BigDecimal proHisFzxsj;
    /**
     * HIS成本价
     */
    private BigDecimal proHisCbj;
    /**
     * HIS销售价
     */
    private BigDecimal proHisXsj;
    /**
     * HIS样本类型
     */
    private String proHisYblx;
    /**
     * HIS部位
     */
    private String proHisBw;
    /**
     * HIS执行划扣
     */
    private String proHisZxhk;
    /**
     * 二级分类
     */
    private String proHisEjfl;
    /**
     * 自定义6
     */
    private String proZdy6;
    /**
     * 自定义7
     */
    private String proZdy7;
    /**
     * 自定义9
     */
    private String proZdy8;
    /**
     * 自定义9
     */
    private String proZdy9;
    /**
     * 自定义10
     */
    private String proZdy10;
    /**
     * 是否煎药
     */
    private String proSfjy;
    /**
     * 门诊计量单位和计量单位的换算比
     */
    private BigDecimal proHisHsb;
    /**
     * 提前停售天数
     */
    private String proTqtsts;
    /**
     * HIS是否皮试,0:否，1:是
     */
    private String proHisSfps;
}