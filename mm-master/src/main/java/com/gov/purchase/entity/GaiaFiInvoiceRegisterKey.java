package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaFiInvoiceRegisterKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 公司代码
     */
    private String poCompanyCode;

    /**
     * 会计凭证号码
     */
    private String belnr;

    /**
     * 会计凭证号行号
     */
    private String ivLineNo;

    /**
     * 发票号码
     */
    private String invoiceNum;
}