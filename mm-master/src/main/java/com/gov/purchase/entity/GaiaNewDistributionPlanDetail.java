package com.gov.purchase.entity;

import com.gov.purchase.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.omg.CORBA.PRIVATE_MEMBER;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @desc:
 * @author: xiaozy
 * @createTime: 2021/5/31 11:04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GaiaNewDistributionPlanDetail extends BaseEntity {
    /**加盟商*/
    private String client;
    /**铺货计划单号*/
    private String planCode;
    /**商品编码*/
    private String proSelfCode;
    /**门店ID*/
    private String storeId;
    /**门店名称*/
    private String storeName;
    /**计划铺货量*/
    private BigDecimal planQuantity;
    /**实际铺货量*/
    private BigDecimal actualQuantity;
    /**完成日期*/
    private Date finishDate;
    /**执行状态：0-未执行 1-已执行*/
    private Integer status;
    /**本次需要铺货数量*/
    private BigDecimal needQuantity;
}
