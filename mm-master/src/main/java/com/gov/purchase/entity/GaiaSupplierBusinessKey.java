package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSupplierBusinessKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String supSite;

    /**
     * 供应商自编码
     */
    private String supSelfCode;
}