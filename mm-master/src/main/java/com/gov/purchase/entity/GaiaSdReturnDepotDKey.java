package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdReturnDepotDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 退库单号
     */
    private String gsrddVoucherId;

    /**
     * 退库日期
     */
    private String gsrddDate;

    /**
     * 行号
     */
    private String gsrddSerial;
}