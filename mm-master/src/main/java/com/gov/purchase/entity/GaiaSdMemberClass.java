package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMemberClass extends GaiaSdMemberClassKey {
    /**
     * 卡类型名称
     */
    private String gmcName;

    /**
     * 卡类型折率
     */
    private String gmcDiscountRate;

    /**
     * 消费金额
     */
    private String gmcAmtSale;

    /**
     * 积分数值
     */
    private String gmcIntegralSale;
}