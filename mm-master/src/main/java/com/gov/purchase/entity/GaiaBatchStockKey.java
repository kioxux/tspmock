package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBatchStockKey {
    /**
     * 
     */
    private String client;

    /**
     * 商品编码
     */
    private String batProCode;

    /**
     * 地点
     */
    private String batSiteCode;

    /**
     * 库存地点
     */
    private String batLocationCode;

    /**
     * 批次
     */
    private String batBatch;
}