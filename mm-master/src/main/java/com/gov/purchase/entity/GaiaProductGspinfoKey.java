package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductGspinfoKey {
    /**
     * 
     */
    private String client;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 地点
     */
    private String proSite;

    /**
     * 首营流程编号
     */
    private String proFlowNo;
}