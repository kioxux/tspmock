package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaOrderType {
    /**
     * 订单类型
     */
    private String ordType;

    /**
     * 订单类型描述
     */
    private String ordTypeDesc;

    /**
     * 分类
     */
    private String ordTypeClass;

    /**
     * 状态
     */
    private String ordTypeStatus;

    /**
     * 创建日期
     */
    private String ordTypeCreateDate;

    /**
     * 创建时间
     */
    private String ordTypeCreateTime;

    /**
     * 创建人
     */
    private String ordTypeCreateUser;

    /**
     * 更新日期
     */
    private String ordTypeChangeDate;

    /**
     * 更新时间
     */
    private String ordTypeChangeTime;

    /**
     * 更新人
     */
    private String ordTypeChangeUser;
}