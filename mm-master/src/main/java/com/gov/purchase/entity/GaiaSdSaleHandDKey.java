package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleHandDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 销售单号
     */
    private String gshdBillNo;
}