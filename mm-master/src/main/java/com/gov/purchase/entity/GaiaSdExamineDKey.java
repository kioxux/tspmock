package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdExamineDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 收货单号
     */
    private String gedVoucherId;
}