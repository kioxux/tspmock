package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWholesaleChannelDetailKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 仓库编码
     */
    private String gwcdDcCode;

    /**
     * 渠道编码
     */
    private String gwcdChaCode;

    /**
     * 类别：0-商品，1-客户
     */
    private String gwcdType;

    /**
     * 自编码
     */
    private String gwcdSelfCode;
}