package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStoresGroupSetKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 分类类型编码
     */
    private String gssgType;

    /**
     * 分类编码
     */
    private String gssgId;
}