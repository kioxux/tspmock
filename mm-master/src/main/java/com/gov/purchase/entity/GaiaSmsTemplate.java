package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSmsTemplate {
    /**
     * 短信模板ID
     */
    private String smsId;

    /**
     * 模板ID
     */
    private String smsTemplateId;

    /**
     * 短信类别
     */
    private String smsType;

    /**
     * 模板主题
     */
    private String smsTheme;

    /**
     * 签名
     */
    private String smsSgin;

    /**
     * 模板内容
     */
    private String smsContent;

    /**
     * 模板状态
     */
    private String smsStatus;

    /**
     * 
     */
    private String smsParams;

    /**
     * 审批流程编号
     */
    private String smsFlowNo;

    /**
     * 审批状态 0-审批中，1-已审批，2-已拒绝
     */
    private String smsGspStatus;

    /**
     * 创建日期
     */
    private String userCreDate;

    /**
     * 创建时间
     */
    private String userCreTime;

    /**
     * 创建人账号
     */
    private String userCreId;

    /**
     * 修改日期
     */
    private String userModiDate;

    /**
     * 修改时间
     */
    private String userModiTime;

    /**
     * 修改人账号
     */
    private String userModiId;
}