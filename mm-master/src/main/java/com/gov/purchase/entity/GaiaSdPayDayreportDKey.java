package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdPayDayreportDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String gpddVoucherId;
}