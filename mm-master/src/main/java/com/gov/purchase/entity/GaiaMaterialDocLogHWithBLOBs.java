package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaMaterialDocLogHWithBLOBs extends GaiaMaterialDocLogH {
    /**
     * 内容
     */
    private String gmdlhContent;

    /**
     * 错误消息
     */
    private String gmdlhErrorResult;
}