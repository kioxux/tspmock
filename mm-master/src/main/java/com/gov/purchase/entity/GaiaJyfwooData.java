package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaJyfwooData {
    /**
     * 经营范围编码
     */
    private String jyfwId;

    /**
     * 经营范围描述
     */
    private String jyfwName;

    /**
     * 经营范围类别（1-经营企业，2-生产企业，3-客户）
     */
    private String jyfwClass;

    /**
     * 创建日期
     */
    private String jyfwCreDate;

    /**
     * 创建时间
     */
    private String jyfwCreTime;

    /**
     * 创建人账号
     */
    private String jyfwCreId;

    /**
     * 修改日期
     */
    private String jyfwModiDate;

    /**
     * 修改时间
     */
    private String jyfwModiTime;

    /**
     * 修改人账号
     */
    private String jyfwModiId;
}