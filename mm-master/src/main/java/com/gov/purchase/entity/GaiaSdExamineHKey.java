package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdExamineHKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 验收单号
     */
    private String gehVoucherId;
}