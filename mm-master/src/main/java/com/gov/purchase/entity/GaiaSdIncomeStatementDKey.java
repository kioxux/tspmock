package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdIncomeStatementDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 损益单号
     */
    private String gsisdVoucherId;
}