package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdIncomeStatementH extends GaiaSdIncomeStatementHKey {
    /**
     * 门店
     */
    private String gsishBrId;

    /**
     * 生成日期
     */
    private String gsishDate;

    /**
     * 损益类型
     */
    private String gsishIsType;

    /**
     * 单据类型
     */
    private String gsishTableType;

    /**
     * 合计金额
     */
    private BigDecimal gsishTotalQty;

    /**
     * 合计数量
     */
    private String gsishTotalAmt;

    /**
     * 备注
     */
    private String gsishRemark;

    /**
     * 审核日期
     */
    private String gsishExamineDate;

    /**
     * 审核人员
     */
    private String gsishExamineEmp;

    /**
     * 审核状态
     */
    private String gsishStatus;
}