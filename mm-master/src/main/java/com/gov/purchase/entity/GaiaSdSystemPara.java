package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSystemPara extends GaiaSdSystemParaKey {
    /**
     * 参数描述
     */
    private String gsspName;

    /**
     * 值描述
     */
    private String gsspParaRemark;

    /**
     * 值
     */
    private String gsspPara;

    /**
     * 修改人员
     */
    private String gsspUpdateEmp;

    /**
     * 修改日期
     */
    private String gsspUpdateDate;
}