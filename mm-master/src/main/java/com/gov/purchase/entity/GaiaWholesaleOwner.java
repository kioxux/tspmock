package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWholesaleOwner extends GaiaWholesaleOwnerKey {
    /**
     * 货主名称
     */
    private String gwoOwnerName;

    /**
     * 手机号码
     */
    private String gwoOwnerTel;

    /**
     * 销售员
     */
    private String gwoOwnerSaleman;

    /**
     * 状态    0-正常，1-停用
     */
    private String gwoStatus;

    /**
     * 创建日期
     */
    private String gwoCreateDate;

    /**
     * 创建时间
     */
    private String gwoCreateTime;

    /**
     * 创建人
     */
    private String gwoCreateBy;

    /**
     * 修改日期
     */
    private String gwoChangeDate;

    /**
     * 修改时间
     */
    private String gwoChangeTime;

    /**
     * 修改人
     */
    private String gwoChangeBy;
}