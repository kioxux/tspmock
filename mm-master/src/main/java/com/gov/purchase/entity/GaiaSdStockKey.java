package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStockKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String gssBrId;
}