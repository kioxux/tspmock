package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdAcceptH extends GaiaSdAcceptHKey {
    /**
     * 收货日期
     */
    private String gahDate;
}