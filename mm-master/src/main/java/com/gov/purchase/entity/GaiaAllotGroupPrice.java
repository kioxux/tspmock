package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaAllotGroupPrice extends GaiaAllotGroupPriceKey {
    /**
     * 创建人
     */
    private String gagpCreateUser;

    /**
     * 创建日期
     */
    private String gagpCreateDate;

    /**
     * 创建时间
     */
    private String gagpCreateTime;

    /**
     * 更新人
     */
    private String gagpUpdateUser;

    /**
     * 更新日期
     */
    private String gagpUpdateDate;

    /**
     * 更新时间
     */
    private String gagpUpdateTime;

    /**
     * 行号
     */
    private Integer gagpIndex;
}