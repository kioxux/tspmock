package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSoItemKey {
    /**
     * 
     */
    private String client;

    /**
     * 销售订单号
     */
    private String soId;

    /**
     * 订单行号
     */
    private String soLineNo;
}