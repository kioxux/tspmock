package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaDcDataKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * DC编码
     */
    private String dcCode;
}