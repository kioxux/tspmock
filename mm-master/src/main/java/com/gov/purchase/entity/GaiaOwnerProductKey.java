package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaOwnerProductKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String gopSite;

    /**
     * 货主编码
     */
    private String gopOwner;

    /**
     * 商品编码
     */
    private String gopProCode;
}