package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaUserData extends GaiaUserDataKey {
    /**
     * 姓名
     */
    private String userNam;

    /**
     * 性别
     */
    private String userSex;

    /**
     * 手机号
     */
    private String userTel;

    /**
     * 身份证号
     */
    private String userIdc;

    /**
     * 注册证号
     */
    private String userYsId;

    /**
     * 职业范围
     */
    private String userYsZyfw;

    /**
     * 职业范围
     */
    private String userYsZylb;

    /**
     * 部门ID
     */
    private String depId;

    /**
     * 部门名称
     */
    private String depName;

    /**
     * 地址
     */
    private String userAddr;

    /**
     * 在职状态
     */
    private String userSta;

    /**
     * 创建日期
     */
    private String userCreDate;

    /**
     * 创建时间
     */
    private String userCreTime;

    /**
     * 创建人账号
     */
    private String userCreId;

    /**
     * 修改日期
     */
    private String userModiDate;

    /**
     * 修改时间
     */
    private String userModiTime;

    /**
     * 修改人账号
     */
    private String userModiId;

    /**
     * 入职日期
     */
    private String userJoinDate;

    /**
     * 停用日期
     */
    private String userDisDate;

    /**
     * 登陆密码
     */
    private String userPassword;

    /**
     * 连锁总部
     */
    private String stoChainHead;

    /**
     * 所在地
     */
    private String userProvince;

    /**
     * 邮箱
     */
    private String userEmail;

    /**
     * 执业医生证号
     */
    private String userPdcId;

    /**
     * 科室
     */
    private String userPredep;

    /**
     * 身份证照片地址
     */
    private String userIdcphoAdds;

    /**
     * 身份证照片审核
     */
    private String userIdcphoStatus;

    /**
     * 执业医生证照片地址
     */
    private String userPdcidphoAdds;

    /**
     * 执业医生证照片审核
     */
    private String userPdcidcphoStatus;
}