package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaFieldRequiredDataKey {
    /**
     * 类型（1-商品 2-供应商 3-客户）
     */
    private String gfrdType;

    /**
     * 字段名
     */
    private String gfrdField;
}