package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaRetailPrice extends GaiaRetailPriceKey {
    /**
     * 金额
     */
    private BigDecimal prcAmount;

    /**
     * 有效期起
     */
    private String prcEffectDate;

    /**
     * 修改前价格
     */
    private BigDecimal prcAmountBefore;

    /**
     * 单位
     */
    private String prcUnit;

    /**
     * 不允许积分
     */
    private String prcNoIntegral;

    /**
     * 不允许会员卡打折
     */
    private String prcNoDiscount;

    /**
     * 不允许积分兑换
     */
    private String prcNoExchange;

    /**
     * 限购数量
     */
    private String prcLimitAmount;

    /**
     * 审批状态
     */
    private String prcApprovalSuatus;

    /**
     * 创建日期
     */
    private String prcCreateDate;

    /**
     * 创建时间
     */
    private String prcCreateTime;

    /**
     * 审批日期
     */
    private String prcApprovalDate;

    /**
     * 审批时间
     */
    private String prcApprovalTime;

    /**
     * 
     */
    private String clent;

    /**
     * 调价来源
     */
    private String prcSource;

    /**
     * 总部调价
     */
    private String prcHeadPrice;

    /**
     * 调价原因
     */
    private String prcReason;

    /**
     * 发起门店
     */
    private String prcInitStore;

    /**
     * 审批人
     */
    private String prcApprovalUser;

    /**
     * 创建人
     */
    private String prcCreateUser;

    /**
     * 销售级别
     */
    private String prcSlaeClass;

    /**
     * 自定义字段1
     */
    private String prcZdy1;

    /**
     * 自定义字段2
     */
    private String prcZdy2;

    /**
     * 自定义字段3
     */
    private String prcZdy3;

    /**
     * 自定义字段4
     */
    private String prcZdy4;

    /**
     * 自定义字段5
     */
    private String prcZdy5;
}