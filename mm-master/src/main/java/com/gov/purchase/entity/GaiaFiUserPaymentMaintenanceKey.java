package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaFiUserPaymentMaintenanceKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 付款主体编码
     */
    private String ficoPayingstoreCode;
}