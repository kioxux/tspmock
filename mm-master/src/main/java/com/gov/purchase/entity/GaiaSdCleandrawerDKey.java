package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdCleandrawerDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 清斗单号
     */
    private String gcdVoucherId;
}