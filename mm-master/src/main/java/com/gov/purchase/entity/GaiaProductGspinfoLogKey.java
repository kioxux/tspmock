package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductGspinfoLogKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 地点
     */
    private String proSite;

    /**
     * 行号
     */
    private Integer proLine;

    /**
     * 首营流程编号
     */
    private String proFlowNo;
}