package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPaymentType {
    /**
     * 付款条件
     */
    private String payType;

    /**
     * 付款条件描述
     */
    private String payTypeDesc;

    /**
     * 状态
     */
    private String payTypeStatus;

    /**
     * 
     */
    private String payTypeDay;

    /**
     * 创建日期
     */
    private String payTypeCreateDate;

    /**
     * 创建时间
     */
    private String payTypeCreateTime;

    /**
     * 创建人
     */
    private String payTypeCreateUser;

    /**
     * 更新日期
     */
    private String payTypeChangeDate;

    /**
     * 更新时间
     */
    private String payTypeChangeTime;

    /**
     * 更新人
     */
    private String payTypeChangeUser;
}