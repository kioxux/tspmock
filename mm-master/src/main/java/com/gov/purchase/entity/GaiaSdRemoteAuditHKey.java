package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRemoteAuditHKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 远程审方单号
     */
    private String gsrahVoucherId;
}