package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductFormKey {
    /**
     * 商品剂型
     */
    private String proForm;

    /**
     * 细分剂型
     */
    private String proFormPart;

    /**
     * 商品剂型状态
     */
    private String proFormStatus;
}