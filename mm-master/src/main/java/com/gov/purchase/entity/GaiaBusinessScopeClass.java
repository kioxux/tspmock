package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBusinessScopeClass {
    /**
     * 经营范围编码
     */
    private String bscCode;

    /**
     * 经营范围名称
     */
    private String bscCodeName;

    /**
     * 经营范围状态
     */
    private String bscCodeStatus;
}