package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdRechargeCard extends GaiaSdRechargeCardKey {
    /**
     * 创建门店
     */
    private String gsrcBrId;

    /**
     * 创建日期
     */
    private String gsrcDate;

    /**
     * 创建人员
     */
    private String gsrcEmp;

    /**
     * 可用门店
     */
    private String gsrcUsableBrId;

    /**
     * 可用门店分类
     */
    private String gsrcUsableStoreClass;

    /**
     * 状态
     */
    private String gsrcStatus;

    /**
     * 姓名
     */
    private String gsrcName;

    /**
     * 性别
     */
    private String gsrcSex;

    /**
     * 手机
     */
    private String gsrcMobile;

    /**
     * 电话
     */
    private String gsrcTel;

    /**
     * 地址
     */
    private String gsrcAddress;

    /**
     * 密码
     */
    private String gsrcPassword;

    /**
     * 当前余额
     */
    private BigDecimal gsrcAmt;

    /**
     * 修改门店
     */
    private String gsrcUpdateBrId;

    /**
     * 修改日期
     */
    private String gsrcUpdateDate;

    /**
     * 修改人员
     */
    private String gsrcUpdateEmp;
}