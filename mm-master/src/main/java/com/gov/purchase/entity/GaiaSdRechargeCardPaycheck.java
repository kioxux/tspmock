package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdRechargeCardPaycheck extends GaiaSdRechargeCardPaycheckKey {
    /**
     * 充值卡号
     */
    private String gsrcpCardId;

    /**
     * 充值门店
     */
    private String gsrcpBrId;

    /**
     * 充值日期
     */
    private String gsrcpDate;

    /**
     * 充值时间
     */
    private String gsrcpTime;

    /**
     * 充值人员
     */
    private String gsrcpEmp;

    /**
     * 充值前余额
     */
    private BigDecimal gsrcpBeforeAmt;

    /**
     * 付款金额
     */
    private BigDecimal gsrcpPayAmt;

    /**
     * 充值比例
     */
    private String gsrcpProportion;

    /**
     * 充值活动
     */
    private String gsrcpPromotion;

    /**
     * 充值金额
     */
    private BigDecimal gsrcpAfterAmt;

    /**
     * 充值后余额
     */
    private BigDecimal gsrcpRechargeAmt;

    /**
     * 应收金额
     */
    private BigDecimal gsrcpYsAmt;

    /**
     * 现金找零
     */
    private BigDecimal gsrcpRmbZlAmt;

    /**
     * 现金收银金额
     */
    private BigDecimal gsrcpRmbAmt;

    /**
     * 支付方式1
     */
    private String gsrcpPaymentNo1;

    /**
     * 支付金额1
     */
    private BigDecimal gsrcpPaymentAmt1;

    /**
     * 支付方式2
     */
    private String gsrcpPaymentNo2;

    /**
     * 支付金额2
     */
    private BigDecimal gsrcpPaymentAmt2;

    /**
     * 支付方式3
     */
    private String gsrcpPaymentNo3;

    /**
     * 支付金额3
     */
    private BigDecimal gsrcpPaymentAmt3;

    /**
     * 支付方式4
     */
    private String gsrcpPaymentNo4;

    /**
     * 支付金额4
     */
    private BigDecimal gsrcpPaymentAmt4;
}