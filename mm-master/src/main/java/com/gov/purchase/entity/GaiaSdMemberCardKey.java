package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMemberCardKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 会员ID
     */
    private String gsmbcMemberId;

    /**
     * 会员卡号
     */
    private String gsmbcCardId;

    /**
     * 所属店号
     */
    private String gsmbcBrId;
}