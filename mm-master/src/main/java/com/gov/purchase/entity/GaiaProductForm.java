package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductForm {
    /**
     * 商品剂型编码
     */
    private String proFormCode;

    /**
     * 商品剂型名称
     */
    private String proFormName;

    /**
     * 商品剂型状态
     */
    private String proFormStatus;

    /**
     * 是否成分细分
     */
    private String proFormPart;

    /**
     * 剂型细分
     */
    private String proFormPartName;
}