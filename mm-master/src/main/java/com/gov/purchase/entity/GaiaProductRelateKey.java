package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductRelateKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String proSite;

    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 供应商编码
     */
    private String supSelfCode;
}