package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaInvoicePayingBasicKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 组织
     */
    private String ficoCompanyCode;
}