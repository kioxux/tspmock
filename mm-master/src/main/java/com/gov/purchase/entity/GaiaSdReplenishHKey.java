package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdReplenishHKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 补货单号
     */
    private String gsrhVoucherId;

    /**
     * 补货门店
     */
    private String gsrhBrId;
}