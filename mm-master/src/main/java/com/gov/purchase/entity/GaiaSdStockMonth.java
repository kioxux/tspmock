package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStockMonth extends GaiaSdStockMonthKey {
    /**
     * 日期
     */
    private String gssmDate;

    /**
     * 商品编码
     */
    private String gssmProId;

    /**
     * 批号
     */
    private String gssmBatchNo;

    /**
     * 批次
     */
    private String gssmBatch;

    /**
     * 数量
     */
    private String gssmQty;

    /**
     * 效期
     */
    private String gssmVaildDate;

    /**
     * 更新日期
     */
    private String gssmUpdateDate;

    /**
     * 更新人员编号
     */
    private String gssmUpdateEmp;
}