package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaMaterialAssessH extends GaiaMaterialAssessHKey {
    /**
     * 移动平均价
     */
    private BigDecimal matMovPrice;
}