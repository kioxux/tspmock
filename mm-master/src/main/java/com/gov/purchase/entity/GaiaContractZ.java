package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaContractZ extends GaiaContractZKey {
    /**
     * 业务员
     */
    private String conSalesmanCode;

    /**
     * 合同日期
     */
    private String conDate;

    /**
     * 合同状态(0-待审核，1-已审核，2-修改待审核，3-已拒绝)
     */
    private String conType;

    /**
     * 有效期起
     */
    private String conExpiryDateFrom;

    /**
     * 有效期至
     */
    private String conExpiryDateTo;

    /**
     * 付款条件
     */
    private String conPaymentTerms;

    /**
     * 合同备注
     */
    private String conRemark;

    /**
     * 创建日期
     */
    private String conCreateDate;

    /**
     * 创建人
     */
    private String conCreateBy;

    /**
     * 创建时间
     */
    private String conCreateTime;

    /**
     * 审批日期
     */
    private String conApproveDate;

    /**
     * 审批人
     */
    private String conApproveBy;

    /**
     * 审批时间
     */
    private String conApproveTime;

    /**
     * 更新日期
     */
    private String conUpdateDate;

    /**
     * 更新人
     */
    private String conUpdateBy;

    /**
     * 更新时间
     */
    private String conUpdateTime;
}