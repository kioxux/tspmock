package com.gov.purchase.entity;

import com.gov.purchase.common.entity.PriceCompareBase;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class GaiaSspPriceCompareMessage extends PriceCompareBase {
    /**
     * 比价单ID
     */
    private Long priceCompareId;

    /**
     * 报价截止日期
     */
    private LocalDateTime offerEndTime;

    /**
     * 用戶ID
     */
    private String userId;

    /**
     * 是否已查看（0：未查看   1：已查看）
     */
    private Integer isRead;

    private String supplierCreditCode;
}