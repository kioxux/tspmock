package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdReplenishDKey {
    /**
     * 加盟商
     */
    protected String client;

    /**
     * 补货门店
     */
    private String gsrdBrId;

    /**
     * 补货单号
     */
    private String gsrdVoucherId;

    /**
     * 行号
     */
    private String gsrdSerial;

    /**
     * 商品编码
     */
    protected String gsrdProId;
}