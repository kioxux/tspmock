package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBank {
    /**
     * 银行代码
     */
    private String bankCode;

    /**
     * 银行名称
     */
    private String bankCodeName;

    /**
     * 银行状态
     */
    private String bankCodeStatus;
}