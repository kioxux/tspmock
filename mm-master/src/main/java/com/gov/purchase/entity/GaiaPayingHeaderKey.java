package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPayingHeaderKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 付款订单号
     */
    private String ficoId;
}