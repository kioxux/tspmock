package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdExamineD extends GaiaSdExamineDKey {
    /**
     * 收货日期
     */
    private String gedDate;

    /**
     * 编码
     */
    private String gedProId;

    /**
     * 批号
     */
    private String gedBatchNo;

    /**
     * 批次
     */
    private String gedBatch;

    /**
     * 有效期
     */
    private String gedValidDate;

    /**
     * 收货数量
     */
    private String gedRecipientQty;

    /**
     * 合格数量
     */
    private String gedQualifiedQty;

    /**
     * 不合格数量
     */
    private String gedUnqualifiedQty;

    /**
     * 不合格事项
     */
    private String gedUnqualifiedCause;

    /**
     * 验收人员
     */
    private String gedEmp;

    /**
     * 验收结论
     */
    private String gedResult;

    /**
     * 库存状态
     */
    private String gedStockStatus;
}