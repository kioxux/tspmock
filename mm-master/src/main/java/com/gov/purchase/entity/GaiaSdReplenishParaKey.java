package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdReplenishParaKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 参数ID
     */
    private String gepId;
}