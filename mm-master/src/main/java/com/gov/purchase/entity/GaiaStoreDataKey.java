package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaStoreDataKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String stoCode;
}