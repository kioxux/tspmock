package com.gov.purchase.entity;

import lombok.Data;

import java.math.BigDecimal;

/***
 * @desc: 入库验收实体
 * @author: ryan
 * @createTime: 2021/6/3 16:35
 **/
@Data
public class WmsRkys {
    private Long id;
    /***/
    private String client;
    /***/
    private String wmRkdh;
    /***/
    private String wmCgddh;
    /***/
    private String wmDdxh;
    /***/
    private String wmSpBm;
    /***/
    private String wmPh;
    /***/
    private String wmScrq;
    /***/
    private String wmYxq;
    /***/
    private BigDecimal wmDdj;
    /***/
    private BigDecimal wmDhj;
    /***/
    private String wmSccj;
    /***/
    private String wmCd;
    /***/
    private BigDecimal wmShsl;
    /***/
    private BigDecimal wmJssl;
    /***/
    private String wmJsyy;
    /***/
    private String wmKcztBh;
    /***/
    private String wmFph;
    /***/
    private String wmYsjl;
    /***/
    private String wmXgrq;
    /***/
    private String wmXgsj;
    /***/
    private String wmXgrId;
    /***/
    private String wmXgr;
    /***/
    private String wmYsrq;
    /***/
    private String wmYssj;
    /***/
    private String wmYsrId;
    /***/
    private String wmYsr;
    /***/
    private String batchno;
    /***/
    private String proSite;
    /***/
    private String wmGysBh;
    /***/
    private String remark;
    /***/
    private Integer state;
    /***/
    private Long acceptedQuantity;
    /***/
    private String consignee;
    /***/
    private String receivingDate;
    /***/
    private String receivingTime;
    /***/
    private Long kucenId;
    /***/
    private String buyerName;
    /***/
    private String poCreateDate;
    /***/
    private String consigneeId;
    /***/
    private String recommendedCargono;
    /***/
    private String wmLqr;
    /***/
    private String wmLqrId;
    /***/
    private String wmLqsj;
    /***/
    private String poPaymentId;
    /***/
    private String approvalComments;
    /**查询条件：验收开始日期*/
    private String startDate;
    /**查询条件：验收截止日期*/
    private String endDate;
}
