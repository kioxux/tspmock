package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductComponent {
    /**
     * 成分分类编码
     */
    private String proCompCode;

    /**
     * 成分分类名称
     */
    private String proCompName;

    /**
     * 成分分类状态
     */
    private String proCompStatus;
}