package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdExamineH extends GaiaSdExamineHKey {
    /**
     * 验收日期
     */
    private String gehDate;

    /**
     * 收货单号
     */
    private String gahVoucherId;

    /**
     * 发货地点
     */
    private String gddhFrom;

    /**
     * 收货地点
     */
    private String gddhTo;

    /**
     * 单据类型
     */
    private String gddhType;

    /**
     * 验收状态
     */
    private String gddhStatus;

    /**
     * 验收金额
     */
    private String gddhTotalAmt;

    /**
     * 验收数量
     */
    private String gddhTotalQty;

    /**
     * 备注
     */
    private String remaks;
}