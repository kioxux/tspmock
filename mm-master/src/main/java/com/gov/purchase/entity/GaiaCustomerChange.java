package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaCustomerChange {
    /**
     * 唯一值
     */
    private Integer id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String cusSite;

    /**
     * 客户自编码
     */
    private String cusSelfCode;

    /**
     * 客户名称
     */
    private String cusName;

    /**
     * 统一社会信用代码
     */
    private String cusCreditCode;

    /**
     * 变更字段(JAVA实体属性名)
     */
    private String cusChangeField;

    /**
     * 变更字段(DB字段名)
     */
    private String cusChangeColumn;

    /**
     * 变更字段名(DB字段注释)
     */
    private String cusChangeColumnDes;

    /**
     * 变更前值
     */
    private String cusChangeFrom;

    /**
     * 变更后值
     */
    private String cusChangeTo;

    /**
     * 变更人
     */
    private String cusChangeUser;

    /**
     * 变更日期
     */
    private String cusChangeDate;

    /**
     * 变更时间
     */
    private String cusChangeTime;

    /**
     * 工作流编号
     */
    private String cusFlowNo;

    /**
     * 是否审批结束
     */
    private String cusSfsp;

    /**
     * 审批状态
     */
    private String cusSfty;

    /**
     * 备注
     */
    private String cusRemarks;
}