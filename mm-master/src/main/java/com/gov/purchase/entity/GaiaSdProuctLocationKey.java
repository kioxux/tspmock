package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdProuctLocationKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String gsplBrId;
}