package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleHandH extends GaiaSdSaleHandHKey {
    /**
     * 店名
     */
    private String gshhBrId;

    /**
     * 销售日期
     */
    private String gshhDate;

    /**
     * 销售时间
     */
    private String gshhTime;

    /**
     * 收银工号
     */
    private String gshhEmp;

    /**
     * 发票号
     */
    private String gshhTaxNo;

    /**
     * 会员卡号
     */
    private String gshhHykNo;

    /**
     * 折扣金额
     */
    private String gshhZkAmt;

    /**
     * 应收金额
     */
    private String gshhYsAmt;

    /**
     * 现金找零
     */
    private String gshhRmbZlAmt;

    /**
     * 现金收银
     */
    private String gshhRmbAmt;

    /**
     * 抵用券卡号
     */
    private String gshhDyqNo;

    /**
     * 抵用券金额
     */
    private String gshhDyqAmt;

    /**
     * 抵用原因
     */
    private String gshhDyqType;

    /**
     * 电子券充值卡号1
     */
    private String gshhDzqczActno1;

    /**
     * 电子券充值金额1
     */
    private String gshhDzqczAmt1;

    /**
     * 电子券充值卡号2
     */
    private String gshhDzqczActno2;

    /**
     * 电子券充值金额2
     */
    private String gshhDzqczAmt2;

    /**
     * 电子券充值卡号3
     */
    private String gshhDzqczActno3;

    /**
     * 电子券充值金额3
     */
    private String gshhDzqczAmt3;

    /**
     * 电子券充值卡号4
     */
    private String gshhDzqczActno4;

    /**
     * 电子券充值金额4
     */
    private String gshhDzqczAmt4;

    /**
     * 电子券充值卡号5
     */
    private String gshhDzqczActno5;

    /**
     * 电子券充值金额5
     */
    private String gshhDzqczAmt5;

    /**
     * 电子券抵用卡号1
     */
    private String gshhDzqdyActno1;

    /**
     * 电子券抵用金额1
     */
    private String gshhDzqdyAmt1;

    /**
     * 电子券抵用卡号2
     */
    private String gshhDzqdyActno2;

    /**
     * 电子券抵用金额2
     */
    private String gshhDzqdyAmt2;

    /**
     * 电子券抵用卡号3
     */
    private String gshhDzqdyActno3;

    /**
     * 电子券抵用金额3
     */
    private String gshhDzqdyAmt3;

    /**
     * 电子券抵用卡号4
     */
    private String gshhDzqdyActno4;

    /**
     * 电子券抵用金额4
     */
    private String gshhDzqdyAmt4;

    /**
     * 电子券抵用卡号5
     */
    private String gshhDzqdyActno5;

    /**
     * 电子券抵用金额5
     */
    private String gshhDzqdyAmt5;

    /**
     * 积分兑换单号
     */
    private String gshhIntegralExchangeNo;

    /**
     * 兑换积分
     */
    private String gshhIntegralExchange;

    /**
     * 抵现积分
     */
    private String gshhIntegralSale;

    /**
     * 支付方式1
     */
    private String gshhPaymentNo1;

    /**
     * 支付金额1
     */
    private String gshhPaymentAmt1;

    /**
     * 支付方式2
     */
    private String gshhPaymentNo2;

    /**
     * 支付金额2
     */
    private String gshhPaymentAmt2;

    /**
     * 支付方式3
     */
    private String gshhPaymentNo3;

    /**
     * 支付金额3
     */
    private String gshhPaymentAmt3;

    /**
     * 支付方式4
     */
    private String gshhPaymentNo4;

    /**
     * 支付金额4
     */
    private String gshhPaymentAmt4;

    /**
     * 支付方式5
     */
    private String gshhPaymentNo5;

    /**
     * 支付金额5
     */
    private String gshhPaymentAmt5;

    /**
     * 退货原销售单号
     */
    private String gshhBillNoReturn;

    /**
     * 退货审核人员
     */
    private String gshhEmpReturn;

    /**
     * 参加促销活动类型1
     */
    private String gshhPromotionType1;

    /**
     * 参加促销活动类型2
     */
    private String gshhPromotionType2;

    /**
     * 参加促销活动类型3
     */
    private String gshhPromotionType3;

    /**
     * 参加促销活动类型4
     */
    private String gshhPromotionType4;

    /**
     * 参加促销活动类型5
     */
    private String gshhPromotionType5;

    /**
     * 调用次数
     */
    private String gshhCallQty;
}