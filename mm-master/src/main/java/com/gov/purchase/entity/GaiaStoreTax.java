package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaStoreTax extends GaiaStoreTaxKey {
    /**
     * 税分类
     */
    private String stoTaxClass;

    /**
     * 纳税主体
     */
    private String stoTaxSubject;

    /**
     * 税率
     */
    private String stoTaxRate;

    /**
     * 开始日期
     */
    private String stoTaxStartdate;

    /**
     * 结束日期
     */
    private String stoTaxEnddate;
}