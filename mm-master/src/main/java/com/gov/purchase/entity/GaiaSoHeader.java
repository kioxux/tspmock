package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSoHeader extends GaiaSoHeaderKey {
    /**
     * 销售订单类型
     */
    private String soType;

    /**
     * 客户编码
     */
    private String soCustomerId;

    /**
     * 公司代码
     */
    private String soCompanyCode;

    /**
     * 凭证日期
     */
    private String soDate;

    /**
     * 付款条款
     */
    private String soPaymentId;

    /**
     * 抬头备注
     */
    private String soHeadRemark;

    /**
     * 创建人
     */
    private String soCreateBy;

    /**
     * 创建日期
     */
    private String soCreateDate;

    /**
     * 创建时间
     */
    private String soCreateTime;

    /**
     * 审批状态
     */
    private String soApproveStatus;

    /**
     * 更新人
     */
    private String soUpdateBy;

    /**
     * 更新日期
     */
    private String soUpdateDate;

    /**
     * 更新时间
     */
    private String soUpdateTime;

    /**
     * 开单标记
     */
    private String soDnFlag;

    /**
     * 开单人
     */
    private String soDnBy;

    /**
     * 开单日期
     */
    private String soDnDate;

    /**
     * 开单时间
     */
    private String soDnTime;

    /**
     * 客户2
     */
    private String soCustomer2;

    /**
     * 销售员编号
     */
    private String gwsCode;

    /**
     * 审批人
     */
    private String soApproveBy;

    /**
     * 审批日期
     */
    private String soApproveDate;

    /**
     * 货主
     */
    private String soOwner;

    /**
     * 前序单号
     */
    private String soReferOrder;
}