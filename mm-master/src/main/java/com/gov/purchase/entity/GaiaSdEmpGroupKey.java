package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdEmpGroupKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 分类编号
     */
    private String gsegId;
}