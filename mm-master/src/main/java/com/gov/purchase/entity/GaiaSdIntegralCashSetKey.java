package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdIntegralCashSetKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String gicsVoucherId;
}