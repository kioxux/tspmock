package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdDepositH extends GaiaSdDepositHKey {
    /**
     * 缴款门店
     */
    private String gdhBrId;

    /**
     * 审核日期
     */
    private String gdhCheckDate;

    /**
     * 存款单号
     */
    private String gdhDepositId;

    /**
     * 存款银行
     */
    private String gdhBankId;

    /**
     * 银行账号
     */
    private String gdhBankAccount;

    /**
     * 存款日期
     */
    private String gdhDepositDate;

    /**
     * 存款金额
     */
    private BigDecimal gdhDepositAmt;

    /**
     * 审核状态
     */
    private String gdhStatus;
}