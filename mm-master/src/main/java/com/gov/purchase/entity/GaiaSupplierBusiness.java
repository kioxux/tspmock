package com.gov.purchase.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class GaiaSupplierBusiness extends GaiaSupplierBusinessKey {
    /**
     * 供应商编码
     */
    private String supCode;

    /**
     * 匹配状态
     */
    private String supMatchStatus;

    /**
     * 助记码
     */
    private String supPym;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 统一社会信用代码
     */
    private String supCreditCode;

    /**
     * 营业期限
     */
    private String supCreditDate;

    /**
     * 供应商分类
     */
    private String supClass;

    /**
     * 法人
     */
    private String supLegalPerson;

    /**
     * 注册地址
     */
    private String supRegAdd;

    /**
     * 供应商状态
     */
    private String supStatus;

    /**
     *
     */
    private String supLicenceNo;

    /**
     * 发证日期
     */
    private String supLicenceDate;

    /**
     * 有效期至
     */
    private String supLicenceValid;

    /**
     * 生产或经营范围
     */
    private String supScope;

    /**
     * 禁止采购
     */
    private String supNoPurchase;

    /**
     * 禁止退厂
     */
    private String supNoSupplier;

    /**
     * 付款条件
     */
    private String supPayTerm;

    /**
     * 业务联系人
     */
    private String supBussinessContact;

    /**
     * 联系人电话
     */
    private String supContactTel;

    /**
     * 送货前置期
     */
    private String supLeadTime;

    /**
     * 银行代码
     */
    private String supBankCode;

    /**
     * 银行名称
     */
    private String supBankName;

    /**
     * 账户持有人
     */
    private String supAccountPerson;

    /**
     * 银行账号
     */
    private String supBankAccount;

    /**
     * 支付方式
     */
    private String supPayMode;

    /**
     * 铺底授信额度
     */
    private String supCreditAmt;

    /**
     * 起订金额
     */
    private BigDecimal supMixAmt;

    /**
     * 法人委托书
     */
    private String supFrwts;

    /**
     * 法人委托书效期
     */
    private String supFrwtsxq;

    /**
     * 质量负责人
     */
    private String supZlfzr;

    /**
     * 仓库地址
     */
    private String supCkdz;

    /**
     * 网页登陆地址
     */
    private String supLoginAdd;

    /**
     * 登陆用户名
     */
    private String supLoginUser;

    /**
     * 登陆密码
     */
    private String supLoginPassword;

    /**
     * 系统参数
     */
    private String supSysParm;

    /**
     * 配送方式
     */
    private String supDeliveryMode;

    /**
     * 承运单位
     */
    private String supCarrier;

    /**
     * 随货同行单图片
     */
    private String supPictureShtxd;

    /**
     * 质量保证协议
     */
    private String supQuality;

    /**
     * 质保协议有效期
     */
    private String supQualityDate;

    /**
     * GSP证书
     */
    private String supGsp;

    /**
     * GSP有效期
     */
    private String supGspDate;

    /**
     * GMP证书
     */
    private String supGmp;

    /**
     * GMP有效期
     */
    private String supGmpDate;

    /**
     * 法人委托书图片
     */
    private String supPictureFrwts;

    /**
     * GSP证书图片
     */
    private String supPictureGsp;

    /**
     * GMP证书图片
     */
    private String supPictureGmp;

    /**
     * 其他图片
     */
    private String supPictureQt;

    /**
     * 运输时间
     */
    private String supDeliveryTimes;

    /**
     * 生产许可证书图片
     */
    private String supPictureScxkz;

    /**
     * 生产许可证书
     */
    private String supScxkz;

    /**
     * 生产许可证有效期
     */
    private String supScxkzDate;

    /**
     * 食品许可证书图片
     */
    private String supPictureSpxkz;

    /**
     * 食品许可证书
     */
    private String supSpxkz;

    /**
     * 食品许可证有效期
     */
    private String supSpxkzDate;

    /**
     * 医疗器械经营许可证图片
     */
    private String supPictureYlqxxkz;

    /**
     * 医疗器械经营许可证书
     */
    private String supYlqxxkz;

    /**
     * 医疗器械经营许可证在有效期
     */
    private String supYlqxxkzDate;

    /**
     * 法人委托书身份证
     */
    private String supFrwtsSfz;

    /**
     * 在途关闭天数
     */
    private String supCloseDate;

    /**
     * 创建日期
     */
    private String supCreateDate;

    /**
     * 样章
     */
    private String supSampleSeal;

    /**
     * 样章图片
     */
    private String supSampleSealImage;

    /**
     * 样票
     */
    private String supSampleTicket;

    /**
     * 样票图片
     */
    private String supSampleTicketImage;

    /**
     * 样膜
     */
    private String supSampleMembrane;

    /**
     * 样膜图片
     */
    private String supSampleMembraneImage;

    /**
     * 档案号
     */
    private String supDah;

    /**
     * 备注
     */
    private String supBeizhu;

    /**
     * 年度报告有效期
     */
    private String supNdbgyxq;

    /**
     * 购销合同
     */
    private String supGxht;

    /**
     * 购销合同有效期
     */
    private String supGxhtyxq;

    /**
     * 财务编码
     */
    private String supCwbm;
    /**
     * 发票类型 增值税专用发票、普通发票或普票
     */
    private String supFplx;
    /**
     * 发照机关
     */
    private String supFzjg;
    /**
     * 企业负责人
     */
    private String supQyfzr;
    /**
     * 注册资本
     */
    private String supZczb;
    /**
     * GSP类型 I类医疗器械 II类医疗器械
     */
    private String supGspgklx;

    /**
     * 第二类医疗器械备案凭证
     */
    private String supDrlylqxbapz;

    /**
     * 第二类医疗器械备案凭证有效期
     */
    private String supDrlylqxbapzDate;

    /**
     * 第二类医疗器械备案凭证图片
     */
    private String supDrlylqxbapzImg;

    /**
     * 应付余额
     */
    private BigDecimal supApAmt;

    /**
     * 采购员（供应商）
     */
    private String supCgy;
}