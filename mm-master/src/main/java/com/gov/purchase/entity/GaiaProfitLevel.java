package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaProfitLevel extends GaiaProfitLevelKey {
    /**
     * 毛利区间起
     */
    private BigDecimal gplProfitStart;

    /**
     * 毛利区间至
     */
    private BigDecimal gplProfitEnd;

    /**
     * 不打折商品
     */
    private String gplBdz;
}