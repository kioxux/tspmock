package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaContractZLogKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 采购主体
     */
    private String conCompanyCode;

    /**
     * 供应商
     */
    private String conSupplierId;

    /**
     * 合同编号
     */
    private String conId;

    /**
     * 合同编辑次数
     */
    private Integer conCompileIndex;
}