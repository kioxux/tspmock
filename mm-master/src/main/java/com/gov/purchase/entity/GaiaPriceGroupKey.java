package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPriceGroupKey {
    /**
     * 
     */
    private String client;

    /**
     * 价格组
     */
    private String prcGroupId;

    /**
     * 门店编码
     */
    private String prcStore;
}