package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWholesaleChannelDetail extends GaiaWholesaleChannelDetailKey {

    /**
     * 创建日期
     */
    private String gwcdCreDate;

    /**
     * 创建时间
     */
    private String gwcdCreTime;

    /**
     * 创建人账号
     */
    private String gwcdCreId;

    /**
     * 修改日期
     */
    private String gwcdModiDate;

    /**
     * 修改时间
     */
    private String gwcdModiTime;

    /**
     * 修改人账号
     */
    private String gwcdModiId;
}