package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleReturnD extends GaiaSdSaleReturnDKey {
    /**
     * 店号
     */
    private String gsrdBrId;

    /**
     * 退货销售单号
     */
    private String gsrdRetno;

    /**
     * 退货销售日期
     */
    private String gsrdRetdate;

    /**
     * 行号
     */
    private String gsrdSerial;

    /**
     * 商品编码
     */
    private String gsrdProId;

    /**
     * 商品批号
     */
    private String gsrdBatch;

    /**
     * 商品有效期
     */
    private String gsrdValidDate;

    /**
     * 监管码/条形码
     */
    private String gsrdStCode;

    /**
     * 单品零售价
     */
    private String gsrdPrc1;

    /**
     * 单品应收价
     */
    private String gsrdPrc2;

    /**
     * 数量
     */
    private String gsrdQty;

    /**
     * 汇总应收金额
     */
    private String gsrdAmt;

    /**
     * 兑换单个积分
     */
    private String gsrdIntegralExchangeSingle;

    /**
     * 兑换积分汇总
     */
    private String gsrdIntegralExchangeCollect;

    /**
     * 单个加价兑换积分金额
     */
    private String gsrdIntegralExchangeAmtSingle;

    /**
     * 加价兑换积分金额汇总
     */
    private String gsrdIntegralExchangeAmtCollect;

    /**
     * 商品折扣总金额
     */
    private String gsrdZkAmt;

    /**
     * 积分兑换折扣金额
     */
    private String gsrdZkJfdh;

    /**
     * 积分抵现折扣金额
     */
    private String gsrdZkJfdx;

    /**
     * 电子券折扣金额
     */
    private String gsrdZkDzq;

    /**
     * 抵用券折扣金额
     */
    private String gsrdZkDyq;

    /**
     * 促销折扣金额
     */
    private String gsrdZkPm;

    /**
     * 营业员编号
     */
    private String gsrdSalerId;

    /**
     * 医生编号
     */
    private String gsrdDoctorId;

    /**
     * 参加促销主题编号
     */
    private String gsrdPmSubjectId;

    /**
     * 参加促销类型编号
     */
    private String gsrdPmId;

    /**
     * 参加促销类型说明
     */
    private String gsrdPmContent;
}