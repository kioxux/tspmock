package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProfitLevelKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 毛利等级
     */
    private String gplLevel;
}