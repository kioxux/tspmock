package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaGlobalGspConfigureKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String gggcSite;

    /**
     * 配置项key
     */
    private String gggcCode;
}