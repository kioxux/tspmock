package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdIntegralExchangeSet extends GaiaSdIntegralExchangeSetKey {
    /**
     * 店号
     */
    private String giesBrId;

    /**
     * 店名
     */
    private String giesBrName;
}