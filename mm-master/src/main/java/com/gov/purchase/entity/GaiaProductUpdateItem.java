package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductUpdateItem extends GaiaProductUpdateItemKey {
    /**
     * 字段名
     */
    private String gpuiName;

    /**
     * 说明
     */
    private String gpuiNote;

    /**
     * 字段值域
     */
    private String gpuiValue;

    /**
     * 字段类型 1:STRING,2:INT,3:decimal
     */
    private Integer gpuiType;

    /**
     * 长度
     */
    private Integer gpuiLength;

    /**
     * 状态 0:启用，1:停用
     */
    private Integer gpuiStatus;

    /**
     * 排序
     */
    private Integer gpuiSort;

    /**
     * 可空  0:可空,1:非空
     */
    private Integer gpuiEmpty;
}