package com.gov.purchase.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-09-30
 */
@Data
@ApiModel(value = "SalaryProgram对象", description = "")
public class SalaryProgram {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Integer gspId;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "系数类型")
    private Integer gspTypeSalary;

    @ApiModelProperty(value = "基本工资")
    private BigDecimal gspBasicSalary;

    @ApiModelProperty(value = "岗位工资")
    private BigDecimal gspPostSalary;

    @ApiModelProperty(value = "绩效工资 ")
    private BigDecimal gspPerformanceSalary;

    @ApiModelProperty(value = "考评工资")
    private BigDecimal gspEvaluationSalary;

    @ApiModelProperty(value = "方案名称")
    private String gspName;

    @ApiModelProperty(value = "生效日期")
    private String gspEffectiveDate;

    @ApiModelProperty(value = "目的")
    private String gspPurpose;

    @ApiModelProperty(value = "当前索引")
    private Integer gspIndex;

    @ApiModelProperty(value = "营业额权重")
    private BigDecimal gspTurnover;

    @ApiModelProperty(value = "毛利额权重")
    private BigDecimal gspGrossProfit;

    @ApiModelProperty(value = "新店综合达成率计算")
    private Integer gspIsNewStore;

    @ApiModelProperty(value = "开业前月综合达成率计算月分")
    private Integer gspOpenBeforeMonth;

    @ApiModelProperty(value = "新店综合达成率")
    private BigDecimal gspNewAchieveRate;

    @ApiModelProperty(value = "首月整月天数")
    private Integer gspFirstmonthDays;

    @ApiModelProperty(value = "月基本工资关联达成系数")
    private Integer gspSalaryReach;

    @ApiModelProperty(value = "月基岗位资关联达成系数")
    private Integer gspPostReach;

    @ApiModelProperty(value = "销售提成 ")
    private Integer gspSalesCommission;

    @ApiModelProperty(value = "毛利提成")
    private Integer gspGrossMargin;

    @ApiModelProperty(value = "负毛利率是否提成")
    private Integer gspNegativeGrossMargin;

    @ApiModelProperty(value = "单品不参与销售提成")
    private Integer gspProReach;

    @ApiModelProperty(value = "月绩效关联达成系数")
    private Integer gspSalesReach;

    @ApiModelProperty(value = "考评工资关联达成系数")
    private Integer gspAppraisalSalary;

    @ApiModelProperty(value = "季度奖提成")
    private Integer gspQuarteQuarterlyAwards;

    @ApiModelProperty(value = "不参与季度奖提成")
    private Integer gspQuarteComputeAchievRate;

    @ApiModelProperty(value = "不参与季度奖提成最大值")
    private BigDecimal gspQuarteAchievRateMin;

    @ApiModelProperty(value = "关联季度员工个人系数")
    private Integer gspQuarterKpi;

    @ApiModelProperty(value = "关联季度综合达成系数")
    private Integer gspQuarterAppraisalSalary;

    @ApiModelProperty(value = "年终奖提成")
    private Integer gspYearQuarterlyAwards;

    @ApiModelProperty(value = "不参与年终奖提成")
    private Integer gspYearComputeAchievRate;

    @ApiModelProperty(value = "不参与年终奖提成最大值")
    private BigDecimal gspYearAchievRateMin;

    @ApiModelProperty(value = "关联年度员工个人系数")
    private Integer gspYearKpi;

    @ApiModelProperty(value = "关联年度综合达成系数")
    private Integer gspYearAppraisalSalary;

    @ApiModelProperty(value = "试算报表")
    private String gspTrialReport;

    @ApiModelProperty(value = "试算薪资")
    private String gspTrialSalary;

    @ApiModelProperty(value = "说明版本")
    private String gspDescription;

    @ApiModelProperty(value = "工作流编号")
    private String gspFlowNo;

    @ApiModelProperty(value = "审批状态")
    private Integer gspApprovalStatus;

    @ApiModelProperty(value = "审批日期")
    private String gspApprovalDate;

    @ApiModelProperty(value = "审批时间")
    private String gspApprovalTime;

    @ApiModelProperty(value = "创建日期")
    private String gspCreateDate;

    @ApiModelProperty(value = "创建时间")
    private String gspCreateTime;

    @ApiModelProperty(value = "创建人")
    private String gspCreateUser;

    @ApiModelProperty(value = "修改日期")
    private String gspChangeDate;

    @ApiModelProperty(value = "修改时间")
    private String gspChangeTime;

    @ApiModelProperty(value = "修改人")
    private String gspChangeUser;

    @ApiModelProperty(value = "负毛利商品不参与季度奖提成")
    private Integer gspQuarterNegativeCommission;

    @ApiModelProperty(value = "负毛利商品不参与年终奖提成")
    private Integer gspYearNegativeCommission;
}
