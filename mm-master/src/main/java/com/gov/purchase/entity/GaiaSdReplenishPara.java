package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdReplenishPara extends GaiaSdReplenishParaKey {
    /**
     * 中药标记
     */
    private String gepChnmedFlag;

    /**
     * 30天销售
     */
    private String gepSalesMonth;

    /**
     * 参数1
     */
    private BigDecimal gepPara1;

    /**
     * 参数1
     */
    private BigDecimal gepPara2;

    /**
     * 参数1
     */
    private BigDecimal gepPara3;
}