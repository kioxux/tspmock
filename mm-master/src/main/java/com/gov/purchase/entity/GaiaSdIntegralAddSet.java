package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdIntegralAddSet extends GaiaSdIntegralAddSetKey {
    /**
     * 店名
     */
    private String giasBrName;

    /**
     * 每单最小积分金额
     */
    private String giasIntegralMinAmt;

    /**
     * 每单最大积分
     */
    private String giasIntegralMax;

    /**
     * 零头是否积分
     */
    private String giasChangeSet;

    /**
     * 消费金额
     */
    private String giasAmtSale;

    /**
     * 积分数值
     */
    private String giasIntegralSale;

    /**
     * 促销类型1金额
     */
    private String giasAmtPm1;

    /**
     * 积分数值1
     */
    private String giasIntegralPm1;

    /**
     * 促销类型2金额
     */
    private String giasAmtPm2;

    /**
     * 积分数值2
     */
    private String giasIntegralPm2;

    /**
     * 促销类型3金额
     */
    private String giasAmtPm3;

    /**
     * 积分数值3
     */
    private String giasIntegralPm3;

    /**
     * 促销类型4金额
     */
    private String giasAmtPm4;

    /**
     * 积分数值4
     */
    private String giasIntegralPm4;

    /**
     * 促销类型5金额
     */
    private String giasAmtPm5;

    /**
     * 积分数值5
     */
    private String giasIntegralPm5;

    /**
     * 促销类型6金额
     */
    private String giasAmtPm6;

    /**
     * 积分数值6
     */
    private String giasIntegralPm6;

    /**
     * 促销类型7金额
     */
    private String giasAmtPm7;

    /**
     * 积分数值7
     */
    private String giasIntegralPm7;

    /**
     * 促销类型8金额
     */
    private String giasAmtPm8;

    /**
     * 积分数值8
     */
    private String giasIntegralPm8;

    /**
     * 促销类型9金额
     */
    private String giasAmtPm9;

    /**
     * 积分数值9
     */
    private String giasIntegralPm9;
}