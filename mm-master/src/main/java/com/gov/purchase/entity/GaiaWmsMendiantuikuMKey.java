package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWmsMendiantuikuMKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 退库门店
     */
    private String wmTkmd;

    /**
     * 退库订单号
     */
    private String wmTkddh;

    /**
     * 序号
     */
    private String wmXh;
}