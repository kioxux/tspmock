package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdSaleD extends GaiaSdSaleDKey {
    /**
     * 商品编码
     */
    private String gssdProId;

    /**
     * 商品批号
     */
    private String gssdBatchNo;

    /**
     * 商品批次
     */
    private String gssdBatch;

    /**
     * 商品有效期
     */
    private String gssdValidDate;

    /**
     * 监管码/条形码
     */
    private String gssdStCode;

    /**
     * 单品零售价
     */
    private BigDecimal gssdPrc1;

    /**
     * 单品应收价
     */
    private BigDecimal gssdPrc2;

    /**
     * 数量
     */
    private BigDecimal gssdQty;

    /**
     * 汇总应收金额
     */
    private BigDecimal gssdAmt;

    /**
     * 兑换单个积分
     */
    private String gssdIntegralExchangeSingle;

    /**
     * 兑换积分汇总
     */
    private String gssdIntegralExchangeCollect;

    /**
     * 单个加价兑换积分金额
     */
    private BigDecimal gssdIntegralExchangeAmtSingle;

    /**
     * 加价兑换积分金额汇总
     */
    private BigDecimal gssdIntegralExchangeAmtCollect;

    /**
     * 商品折扣总金额
     */
    private BigDecimal gssdZkAmt;

    /**
     * 积分兑换折扣金额
     */
    private BigDecimal gssdZkJfdh;

    /**
     * 积分抵现折扣金额
     */
    private BigDecimal gssdZkJfdx;

    /**
     * 电子券折扣金额
     */
    private BigDecimal gssdZkDzq;

    /**
     * 抵用券折扣金额
     */
    private BigDecimal gssdZkDyq;

    /**
     * 促销折扣金额
     */
    private BigDecimal gssdZkPm;

    /**
     * 	营业员编号
     */
    private String gssdSalerId;

    /**
     * 医生编号
     */
    private String gssdDoctorId;

    /**
     * 参加促销主题编号
     */
    private String gssdPmSubjectId;

    /**
     * 参加促销类型编号
     */
    private String gssdPmId;

    /**
     * 参加促销类型说明
     */
    private String gssdPmContent;

    /**
     * 参加促销编号
     */
    private String gssdPmActivityId;

    /**
     * 参加促销活动名称
     */
    private String gssdPmActivityName;

    /**
     * 参加促销活动参数
     */
    private String gssdPmActivityFlag;

    /**
     * 是否登记处方信息 0否1是
     */
    private String gssdRecipelFlag;

    /**
     * 是否为关联推荐
     */
    private String gssdRelationFlag;

    /**
     * 特殊药品购买人身份证
     */
    private String gssdSpecialmedIdcard;

    /**
     * 特殊药品购买人姓名
     */
    private String gssdSpecialmedName;

    /**
     * 特殊药品购买人性别
     */
    private String gssdSpecialmedSex;

    /**
     * 特殊药品购买人出生日期
     */
    private String gssdSpecialmedBirthday;

    /**
     * 特殊药品购买人手机
     */
    private String gssdSpecialmedMobile;

    /**
     * 特殊药品购买人地址
     */
    private String gssdSpecialmedAddress;

    /**
     * 特殊药品电子监管码
     */
    private String gssdSpecialmedEcode;

    /**
     * 价格状态
     */
    private String gssdProStatus;

    /**
     * 批次成本价
     */
    private BigDecimal gssdBatchCost;

    /**
     * 批次成本税额
     */
    private BigDecimal gssdBatchTax;

    /**
     * 移动平均单价
     */
    private BigDecimal gssdMovPrice;

    /**
     * 移动平均总价
     */
    private BigDecimal gssdMovPrices;

    /**
     * 税率
     */
    private BigDecimal gssdMovTax;

    /**
     * 是否生成凭证 1-是 0-否
     */
    private String gssdAdFlag;

    /**
     * 移动税额
     */
    private BigDecimal gssdTaxRate;

    /**
     * 加点后金额
     */
    private BigDecimal gssdAddAmt;

    /**
     * 加点后税金
     */
    private BigDecimal gssdAddTax;
}