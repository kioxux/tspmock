package com.gov.purchase.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author staxc
 * @Date 2020/10/28 16:03
 * @desc
 */
@Data
public class GaiaInvoice extends GaiaInvoiceKey {

    /**
     * 发票订单号
     */
    private String ficoOrderNo;


    /**
     * 纳税主体名称
     */
    private String ficoTaxSubjectName;

    /**
     * 统一社会信用代码
     */
    private String ficoSocialCreditCode;

    /**
     * 开户行名称
     */
    private String ficoBankName;

    /**
     * 开户行账号
     */
    private String ficoBankNumber;

    /**
     * 地址
     */
    private String ficoCompanyAddress;

    /**
     * 电话号码
     */
    private String ficoCompanyPhoneNumber;

    /**
     * 发票请求流水号
     */
    private String ficoFpqqlsh;

    /**
     * 发票pdf路径
     */
    private String ficoPdfUrl;

    /**
     * 发票号码
     */
    private String ficoFphm;

    /**
     * 发票代码
     */
    private String ficoFpdm;

    /**
     * 发票开票日期
     */
    private String ficoKprq;

    /**
     * 发票金额
     */
    private BigDecimal ficoInvoiceAmount;

    /**
     * 发票状态 0未开票 1开票中 2已开票 3开票失败
     */
    private String ficoInvoiceStatus;

    /**
     * 发票类型
     */
    private String ficoInvoiceType;

    /**
     * 开票返回结果
     */
    private String ficoInvoiceResult;

    /**
     * 发票序列
     */
    private Integer ficoIndex;
}
