package com.gov.purchase.entity;

import lombok.Data;

import java.util.List;

@Data
public class GaiaDcData extends GaiaDcDataKey {
    /**
     * DC名称
     */
    private String dcName;

    /**
     * 助记码
     */
    private String dcPym;

    /**
     * DC简称
     */
    private String dcShortName;

    /**
     * DC地址
     */
    private String dcAdd;

    /**
     * DC电话
     */
    private String dcTel;

    /**
     * DC状态
     */
    private String dcStatus;

    /**
     * 虚拟仓标记
     */
    private String dcInvent;

    /**
     * 是否有批发资质
     */
    private String dcWholesale;

    /**
     * 连锁总部
     */
    private String dcChainHead;

    /**
     * 配送平均天数
     */
    private String dcDeliveryDays;

    /**
     * 20-连锁 30-批发公司
     */
    private String dcType;

    /**
     * 纳税主体
     */
    private String dcTaxSubject;

    /**
     * 部门负责人ID
     */
    private String dcHeadId;

    /**
     * 部门负责人姓名
     */
    private String dcHeadNam;

    /**
     * 创建日期
     */
    private String dcCreDate;

    /**
     * 创建时间
     */
    private String dcCreTime;

    /**
     * 创建人账号
     */
    private String dcCreId;

    /**
     * 修改日期
     */
    private String dcModiDate;

    /**
     * 修改时间
     */
    private String dcModiTime;

    /**
     * 修改人账号
     */
    private String dcModiId;

    /**
     * 统一社会信用代码
     */
    private String dcNo;

    /**
     * 法人/负责人
     */
    private String dcLegalPerson;

    /**
     * 质量负责人
     */
    private String dcQua;

    /**
     * 是否管控经营范围：1-是
     */
    private String dcJyfwgk;

    /**
     * 是否管控证照效期：1-是
     */
    private String dcZzxqgk;

    /**
     * 修改是是否发起工作流：1-是
     */
    private String dcXgsfwf;

    /**
     * 商品配送范围字符串 逗号分割
     */
    private String dcSppsfw;

    /**
     * 商品配送范围列表
     */
    private List<String> dcSppsfwList;

    /**
     * 委托配送中心
     */
    private String dcWtdc;
}