package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMedCheckH extends GaiaSdMedCheckHKey {
    /**
     * 门店
     */
    private String gsmchBrId;

    /**
     * 店名
     */
    private String gsmchBrName;

    /**
     * 养护日期
     */
    private String gsmchDate;

    /**
     * 养护人员
     */
    private String gsmchEmp;

    /**
     * 审核人员
     */
    private String gsmchExamineEmp;

    /**
     * 审核日期
     */
    private String gsmchExamineDate;

    /**
     * 审核状态
     */
    private String gsmchStatus;
}