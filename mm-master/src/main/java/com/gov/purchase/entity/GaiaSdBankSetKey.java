package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdBankSetKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String gpsBrId;
}