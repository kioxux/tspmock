package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdCleandrawerD extends GaiaSdCleandrawerDKey {
    /**
     * 清斗日期
     */
    private String gcdDate;

    /**
     * 商品编码
     */
    private String gcdProId;

    /**
     * 批号
     */
    private String gcdBatchNo;

    /**
     * 批次
     */
    private String gcdBatch;

    /**
     * 有效期
     */
    private String gcdValidDate;

    /**
     * 清斗数量
     */
    private String gcdQty;

    /**
     * 清斗状态
     */
    private String gcdStatus;
}