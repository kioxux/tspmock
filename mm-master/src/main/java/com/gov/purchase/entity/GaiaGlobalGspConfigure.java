package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaGlobalGspConfigure extends GaiaGlobalGspConfigureKey {
    /**
     * 配置项值
     */
    private String gggcValue;

    /**
     * 注释
     */
    private String gggcComment;
}