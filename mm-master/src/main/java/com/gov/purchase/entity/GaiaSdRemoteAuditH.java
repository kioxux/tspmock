package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRemoteAuditH extends GaiaSdRemoteAuditHKey {
    /**
     * 店号
     */
    private String gsrahBrId;

    /**
     * 店名
     */
    private String gsrahBrName;

    /**
     * 上传日期
     */
    private String gsrahUploadDate;

    /**
     * 上传时间
     */
    private String gsrahUploadTime;

    /**
     * 上传人员
     */
    private String gsrahUploadEmp;

    /**
     * 上传状态
     */
    private String gsrahUploadFlag;

    /**
     * 审方日期
     */
    private String gsrahDate;

    /**
     * 审方时间
     */
    private String gsrahTime;

    /**
     * 审方结论
     */
    private String gsrahResult;

    /**
     * 药师编号
     */
    private String gsrahPharmacistId;

    /**
     * 药师名称
     */
    private String gsrahPharmacistName;

    /**
     * 顾客姓名
     */
    private String gsrahCustName;

    /**
     * 顾客性别
     */
    private String gsrahCustSex;

    /**
     * 顾客年龄
     */
    private String gsrahCustAge;

    /**
     * 顾客身份证
     */
    private String gsrahCustIdcard;

    /**
     * 顾客手机
     */
    private String gsrahCustMobile;

    /**
     * 状态
     */
    private String gsrahStatus;

    /**
     * 处方图片地址
     */
    private String gsrahRecipelPicAddress;
}