package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdDepositHKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 缴款单号
     */
    private String gdhVoucherId;
}