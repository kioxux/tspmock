package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaLabelApplyKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String labStore;

    /**
     * 价签申请单号
     */
    private String labApplyNo;

    /**
     * 商品编码
     */
    private String labProduct;
}