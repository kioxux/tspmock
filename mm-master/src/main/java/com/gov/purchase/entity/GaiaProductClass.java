package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductClass {
    /**
     * 商品分类编码
     */
    private String proClassCode;

    /**
     * 商品分类名称
     */
    private String proClassName;

    /**
     * 商品分类状态
     */
    private String proClassStatus;
}