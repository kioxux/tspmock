package com.gov.purchase.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class GaiaSdReplenishD extends GaiaSdReplenishDKey {
    /**
     * 补货日期
     */
    private String gsrdDate;

    /**
     * 商品批次
     */
    private String gsrdBatch;

    /**
     * 商品批号
     */
    private String gsrdBatchNo;

    /**
     * 毛利率
     */
    private String gsrdGrossMargin;

    /**
     * 90天销量
     */
    private BigDecimal gsrdSalesDays1;

    /**
     * 30天销量
     */
    private BigDecimal gsrdSalesDays2;

    /**
     * 7天销量
     */
    private BigDecimal gsrdSalesDays3;

    /**
     * 建议补货量
     */
    private BigDecimal gsrdProposeQty;

    /**
     * 本店库存
     */
    private String gsrdStockStore;

    /**
     * 仓库库存
     */
    private String gsrdStockDepot;

    /**
     * 最小陈列
     */
    private String gsrdDisplayMin;

    /**
     * 中包装
     */
    private String gsrdPackMidsize;

    /**
     * 最后一次供应商
     */
    private String gsrdLastSupid;

    /**
     * 最后一次价格
     */
    private BigDecimal gsrdLastPrice;

    /**
     * 修改后价格
     */
    private BigDecimal gsrdEditPrice;

    /**
     * 补货量
     */
    private String gsrdNeedQty;

    /**
     * 订单价
     */
    private BigDecimal gsrdVoucherAmt;

    /**
     * 移动平均价
     */
    private BigDecimal gsrdAverageCost;

    /**
     * 税率
     */
    private String gsrdTaxRate;

    /**
     * 货位号
     */
    private String gsrdHwh;

    /**
     * 备注
     */
    private String gsrdBz;

    /**
     * 手工标记
     */
    private String gsrdFlag;
}