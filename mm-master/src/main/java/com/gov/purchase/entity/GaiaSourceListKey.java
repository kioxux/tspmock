package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSourceListKey {
    /**
     * 
     */
    private String client;

    /**
     * 商品编码
     */
    private String souProCode;

    /**
     * 地点
     */
    private String souSiteCode;

    /**
     * 
     */
    private String souListCode;
}