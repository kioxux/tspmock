package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSupplierGspinfo extends GaiaSupplierGspinfoKey {
    /**
     * 
     */
    private String supCode;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 营业执照编号
     */
    private String supCreditCode;

    /**
     * 营业期限
     */
    private String supCreditDate;

    /**
     * 法人
     */
    private String supLegalPerson;

    /**
     * 注册地址
     */
    private String supRegAdd;

    /**
     * 
     */
    private String supLicenceNo;

    /**
     * 发证单位
     */
    private String supLicenceOrgan;

    /**
     * 发证日期
     */
    private String supLicenceDate;

    /**
     * 有效期至
     */
    private String supLicenceValid;

    /**
     * 邮政编码
     */
    private String supPostalCode;

    /**
     * 税务登记证
     */
    private String supTaxCard;

    /**
     * 组织机构代码证
     */
    private String supOrgCard;

    /**
     * 开户户名
     */
    private String supAccountName;

    /**
     * 开户银行
     */
    private String supAccountBank;

    /**
     * 银行账号
     */
    private String supBankAccount;

    /**
     * 随货同行单（票）情况
     */
    private String supDocState;

    /**
     * 企业印章情况
     */
    private String supSealState;

    /**
     * 生产或经营范围
     */
    private String supScope;

    /**
     * 经营方式
     */
    private String supOperationMode;

    /**
     * 首营日期
     */
    private String supGspDate;

    /**
     * 首营流程编号
     */
    private String supFlowNo;

    /**
     * 
     */
    private String supGspStatus;

    /**
     * 供应商分类
     */
    private String supClass;

    /**
     * 质量负责人
     */
    private String supZlfzr;

    /**
     * 业务联系人
     */
    private String supBussinessContact;

    /**
     * 送货前置期
     */
    private String supLeadTime;

    /**
     * 起订金额
     */
    private BigDecimal supMixAmt;

    /**
     * 匹配状态
     */
    private String supMatchStatus;

    /**
     * 助记码
     */
    private String supPym;

    /**
     * 供应商状态
     */
    private String supStatus;

    /**
     * 禁止采购
     */
    private String supNoPurchase;

    /**
     * 禁止退厂
     */
    private String supNoSupplier;

    /**
     * 付款条件
     */
    private String supPayTerm;

    /**
     * 联系人电话
     */
    private String supContactTel;

    /**
     * 银行代码
     */
    private String supBankCode;

    /**
     * 银行名称
     */
    private String supBankName;

    /**
     * 账户持有人
     */
    private String supAccountPerson;

    /**
     * 支付方式
     */
    private String supPayMode;

    /**
     * 铺底授信额度
     */
    private String supCreditAmt;

    /**
     * 法人委托书
 
     */
    private String supFrwts;

    /**
     * 法人委托书效期
 
     */
    private String supFrwtsxq;

    /**
     * 仓库地址
 
     */
    private String supCkdz;

    /**
     * 网页登陆地址
 
     */
    private String supLoginAdd;

    /**
     * 登陆用户名
 
     */
    private String supLoginUser;

    /**
     * 登陆密码
 
     */
    private String supLoginPassword;

    /**
     * 系统参数
 
     */
    private String supSysParm;

    /**
     * 法人委托书图片
     */
    private String supPictureFrwts;

    /**
     * GSP证书图片
     */
    private String supPictureGsp;

    /**
     * GSP证书
     */
    private String supGsp;

    /**
     * GMP证书图片
     */
    private String supPictureGmp;

    /**
     * GMP证书
     */
    private String supGmp;

    /**
     * GMP有效期
     */
    private String supGmpDate;

    /**
     * 随货同行单图片
     */
    private String supPictureShtxd;

    /**
     * 其他图片
     */
    private String supPictureQt;

    /**
     * 配送方式
     */
    private String supDeliveryMode;

    /**
     * 承运单位
     */
    private String supCarrier;

    /**
     * 运输时间
     */
    private String supDeliveryTimes;

    /**
     * 质量保证协议
     */
    private String supQuality;

    /**
     * 质保协议有效期
     */
    private String supQualityDate;

    /**
     * 生产许可证书图片
     */
    private String supPictureScxkz;

    /**
     * 生产许可证书
     */
    private String supScxkz;

    /**
     * 生产许可证有效期
     */
    private String supScxkzDate;

    /**
     * 食品许可证书图片
     */
    private String supPictureSpxkz;

    /**
     * 食品许可证书
     */
    private String supSpxkz;

    /**
     * 食品许可证有效期
     */
    private String supSpxkzDate;

    /**
     * 医疗器械经营许可证图片
     */
    private String supPictureYlqxxkz;

    /**
     * 医疗器械经营许可证书
     */
    private String supYlqxxkz;

    /**
     * 医疗器械经营许可证在有效期
     */
    private String supYlqxxkzDate;

    /**
     * 法人委托书身份证
     */
    private String supFrwtsSfz;

    /**
     * 在途关闭天数
     */
    private String supCloseDate;

    /**
     * GSP有效期
     */
    private String supGspDateValidity;

    /**
     * 样章
     */
    private String supSampleSeal;

    /**
     * 样章图片
     */
    private String supSampleSealImage;

    /**
     * 样票
     */
    private String supSampleTicket;

    /**
     * 样票图片
     */
    private String supSampleTicketImage;

    /**
     * 样膜
     */
    private String supSampleMembrane;

    /**
     * 样膜图片
     */
    private String supSampleMembraneImage;

    /**
     * 档案号
     */
    private String supDah;

    /**
     * 备注
     */
    private String supBeizhu;

    /**
     * 年度报告有效期
     */
    private String supNdbgyxq;

    /**
     * 首营日期
     */
    private String supCreateDate;

    /**
     * 购销合同
     */
    private String supGxht;

    /**
     * 购销合同有效期
     */
    private String supGxhtyxq;

    /**
     * 财务编码
     */
    private String supCwbm;
    /**
     * 发票类型 增值税专用发票、普通发票或普票
     */
    private String supFplx;
    /**
     * 发照机关
     */
    private String supFzjg;
    /**
     * 企业负责人
     */
    private String supQyfzr;
    /**
     * 注册资本
     */
    private String supZczb;
    /**
     * GSP类型 I类医疗器械 II类医疗器械
     */
    private String supGspgklx;

    /**
     * 第二类医疗器械备案凭证
     */
    private String supDrlylqxbapz;

    /**
     * 第二类医疗器械备案凭证有效期
     */
    private String supDrlylqxbapzDate;

    /**
     * 第二类医疗器械备案凭证图片
     */
    private String supDrlylqxbapzImg;

    /**
     * 采购员（供应商）
     */
    private String supCgy;
}