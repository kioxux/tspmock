package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaCustomerGspinfoKey {
    /**
     * 
     */
    private String client;

    /**
     * 客户自编码
     */
    private String cusSelfCode;

    /**
     * 地点
     */
    private String cusSite;
}