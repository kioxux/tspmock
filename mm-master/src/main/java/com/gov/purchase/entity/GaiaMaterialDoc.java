package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaMaterialDoc extends GaiaMaterialDocKey {
    /**
     * 物料凭证类型
     */
    private String matType;

    /**
     * 凭证日期
     */
    private String matDocDate;

    /**
     * 过账日期
     */
    private String matPostDate;

    /**
     * 抬头备注
     */
    private String matHeadRemark;

    /**
     * 商品编码
     */
    private String matProCode;

    /**
     * 地点
     */
    private String matSiteCode;

    /**
     * 库存地点
     */
    private String matLocationCode;

    /**
     * 批次
     */
    private String matBatch;

    /**
     * 物料凭证数量
     */
    private BigDecimal matQty;

    /**
     * 总金额（批次）
     */
    private BigDecimal matBatAmt;

    /**
     * 基本计量单位
     */
    private String matUint;

    /**
     * 总金额（移动）
     */
    private BigDecimal matMovAmt;

    /**
     * 税金（批次）
     */
    private BigDecimal matRateBat;

    /**
     * 税金（移动）
     */
    private BigDecimal matRateMov;

    /**
     * 借/贷标识
     */
    private String matDebitCredit;

    /**
     * 订单号
     */
    private String matPoId;

    /**
     * 订单行号
     */
    private String matPoLineno;

    /**
     * 业务单号
     */
    private String matDnId;

    /**
     * 业务单行号
     */
    private String matDnLineno;

    /**
     * 物料凭证行备注
     */
    private String matLineRemark;

    /**
     * 创建人
     */
    private String matCreateBy;

    /**
     * 创建日期
     */
    private String matCreateDate;

    /**
     * 创建时间
     */
    private String matCreateTime;

    /**
     * 加点后金额
     */
    private BigDecimal matAddAmt;

    /**
     * 加点后税金
     */
    private BigDecimal matAddTax;
}