package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaCustomerBasic {
    /**
     * 客户编码
     */
    private String cusCode;

    /**
     * 助记码
     */
    private String cusPym;

    /**
     * 客户名称
     */
    private String cusName;

    /**
     * 统一社会信用代码
     */
    private String cusCreditCode;

    /**
     * 营业期限
     */
    private String cusCreditDate;

    /**
     * 客户分类
     */
    private String cusClass;

    /**
     * 法人
     */
    private String cusLegalPerson;

    /**
     * 注册地址
     */
    private String cusRegAdd;

    /**
     * 客户状态
     */
    private String cusStatus;

    /**
     * 许可证编号
     */
    private String cusLicenceNo;

    /**
     * 发证日期
     */
    private String cusLicenceDate;

    /**
     * 有效期至
     */
    private String cusLicenceValid;

    /**
     * 生产或经营范围
     */
    private String cusScope;
}