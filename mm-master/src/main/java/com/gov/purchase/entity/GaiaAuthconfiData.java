package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaAuthconfiData extends GaiaAuthconfiDataKey {
    /**
     * 权限组(岗位)描述
     */
    private String authGroupName;
}