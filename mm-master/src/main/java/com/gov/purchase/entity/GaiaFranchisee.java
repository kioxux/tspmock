package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaFranchisee {
    /**
     * 
     */
    private String client;

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 统一社会信用代码
     */
    private String francNo;

    /**
     * 法人
     */
    private String francLegalPerson;

    /**
     * 质量负责人
     */
    private String francQua;

    /**
     * 详细地址
     */
    private String francAddr;

    /**
     * 创建日期
     */
    private String francCreDate;

    /**
     * 创建时间
     */
    private String francCreTime;

    /**
     * 创建人账号
     */
    private String francCreId;

    /**
     * 修改日期
     */
    private String francModiDate;

    /**
     * 修改时间
     */
    private String francModiTime;

    /**
     * 修改人账号
     */
    private String francModiId;

    /**
     * LOGO地址
     */
    private String francLogo;

    /**
     * 公司性质-单体店
     */
    private String francType1;

    /**
     * 公司性质-连锁公司
     */
    private String francType2;

    /**
     * 公司性质-批发公司
     */
    private String francType3;

    /**
     * 委托人账号
     */
    private String francAss;
}