package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaFiInvoiceRegister extends GaiaFiInvoiceRegisterKey {
    /**
     * 采购订单号
     */
    private String matPoId;

    /**
     * 供应商编码
     */
    private String poSupplierId;

    /**
     * 采购订单行号
     */
    private String poLineNo;

    /**
     * 商品编码
     */
    private String poProCode;

    /**
     * 订单行金额
     */
    private BigDecimal poLineAmt;

    /**
     * 过账日期
     */
    private String matPostDate;

    /**
     * 物料凭证号
     */
    private String matId;

    /**
     * 物料凭证行号
     */
    private String matLineNo;

    /**
     * 物料凭证年份
     */
    private String matYear;

    /**
     * 税金
     */
    private BigDecimal matRateAmt;

    /**
     * 已交货数量
     */
    private BigDecimal poDeliveredQty;

    /**
     * 已交货金额
     */
    private BigDecimal poDeliveredAmt;

    /**
     * 已发票登记数量
     */
    private BigDecimal poInvoiceQty;

    /**
     * 已发票登记金额
     */
    private BigDecimal poInvoiceAmt;

    /**
     * 采购订单数量
     */
    private BigDecimal poQty;
}