package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBussinessScopeKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 实体类型
     */
    private String perEntityClass;

    /**
     * 实体编码
     */
    private String perEntityId;

    /**
     * 经营范围编码
     */
    private String perCode;
}