package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdPhysicalCountDiff extends GaiaSdPhysicalCountDiffKey {
    /**
     * 门店
     */
    private String gspcdBrId;

    /**
     * 日期
     */
    private String gspcdDate;

    /**
     * 时间
     */
    private String gspcdTime;

    /**
     * 类型
     */
    private String gspcdType;

    /**
     * 编码
     */
    private String gspcdProId;

    /**
     * 批号
     */
    private String gspcdBatchNo;

    /**
     * 库存数量
     */
    private String gspcdStochQty;

    /**
     * 盘点数量
     */
    private String gspcdPcFirstQty;

    /**
     * 初盘差异
     */
    private String gspcdDiffFirstQty;

    /**
     * 初盘修改
     */
    private String gspcdPcSecondQty;

    /**
     * 复盘差异
     */
    private String gspcdDiffSecondQty;

    /**
     * 审核日期
     */
    private String gspcdExamineDate;

    /**
     * 审核工号
     */
    private String gspcdExamineEmp;

    /**
     * 状态
     */
    private String gspcdStatus;

    /**
     * 是否转损益单
     */
    private String gspcdIsFlag;
}