package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWmsDiaoboZKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 配送单号
     */
    private String wmPsdh;
}