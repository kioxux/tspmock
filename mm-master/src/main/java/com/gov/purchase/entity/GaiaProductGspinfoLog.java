package com.gov.purchase.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GaiaProductGspinfoLog extends GaiaProductGspinfoLogKey {
    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 通用名称
     */
    private String proCommonname;

    /**
     * 剂型
     */
    private String proForm;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 包装规格
     */
    private String proPackSpecs;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 供货单位名称
     */
    private String proFactoryName;

    /**
     * 是否通过GMP认证
     */
    private String proIfGmp;

    /**
     * 质量标准
     */
    private String proQs;

    /**
     * 功能主治
     */
    private String proFunction;

    /**
     * 有效期
     */
    private String proLifeDate;

    /**
     * 贮存条件
     */
    private String proStorageCondition;

    /**
     * 供货单位名称
     */
    private String proSupplyName;

    /**
     * 供货单位地址
     */
    private String proSupplyAdd;

    /**
     * 供货单位联系人
     */
    private String proSupplyContact;

    /**
     * 供货单位联系电话
     */
    private String proSupplyTel;

    /**
     * 首营日期
     */
    private String proGspDate;

    /**
     * 首营状态
     */
    private String proGspStatus;

    /**
     * 中药规格
     */
    private String proTcmSpecs;

    /**
     * 中药批准文号
     */
    private String proTcmRegisterNo;

    /**
     * 中药生产企业代码
     */
    private String proTcmFactoryCode;

    /**
     * 中药产地
     */
    private String proTcmPlace;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 进项税率
     */
    private String proInputTax;

    /**
     * 保质期
     */
    private String proLife;

    /**
     * 保质期单位
     */
    private String proLifeUnit;

    /**
     * 商品分类
     */
    private String proClass;

    /**
     * 批准文号分类
     */
    private String proRegisterClass;

    /**
     * 商品仓储分区
     */
    private String proStorageArea;

    /**
     * 中包装量
     */
    private String proMidPackage;

    /**
     * 生产厂家代码
     */
    private String proFactoryCode;

    /**
     * 参考进货价
     */
    private BigDecimal proCgj;

    /**
     * 参考零售价
     */
    private BigDecimal proLsj;

    /**
     * 参考毛利率
     */
    private BigDecimal proMll;

    /**
     * 国际条形码1
     */
    private String proBarcode;
}