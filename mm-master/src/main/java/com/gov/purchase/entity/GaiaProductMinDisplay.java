package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaProductMinDisplay extends GaiaProductMinDisplayKey {
    /**
     * 最小陈列量
     */
    private BigDecimal gpmdMinQty;

    /**
     * 创建人
     */
    private String gpmdCreateUser;

    /**
     * 创建日期
     */
    private String gpmdCreateDate;

    /**
     * 创建时间
     */
    private String gpmdCreateTime;

    /**
     * 变更人
     */
    private String gpmdChangeUser;

    /**
     * 变更日期
     */
    private String gpmdChangeDate;

    /**
     * 变更时间
     */
    private String gpmdChangeTime;
}