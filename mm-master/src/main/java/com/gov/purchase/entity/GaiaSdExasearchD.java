package com.gov.purchase.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GaiaSdExasearchD extends GaiaSdExasearchDKey {
    /**
     * 查询主题ID
     */
    private String gseTheid;

    /**
     * 会员名称
     */
    private String gseMenname;

    /**
     * 手机
     */
    private String gseMobile;

    /**
     * 性别
     */
    private String gseSex;

    /**
     * 年龄
     */
    private String gseAge;

    /**
     * 生日
     */
    private String gseBirth;

    /**
     * 当前积分
     */
    private BigDecimal gseIntegral;

    /**
     * 所属门店编号
     */
    private String gseBrId;

    /**
     * 所属门店名称
     */
    private String gseBrName;

    /**
     * 会员卡类型
     */
    private String gseClassId;

    /**
     * 最后积分日期
     */
    private String gseIntegralLastdate;

    /**
     * 会员级别
     */
    private String gseGlid;

    /**
     * 会员微信
     */
    private String gseWxid;

    /**
     * 是否发送短信
     */
    private String gseSms;

    /**
     * 是否发送微信
     */
    private String gseWx;

    /**
     * 信息创建时间
     */
    private String gseCretime;

    /**
     * 信息发送时间
     */
    private String gseSendtime;

    /**
     * 短信是否发送成功
     */
    private String gseStatus;

    /**
     * 营销活动id
     */
    private String gseMarketid;

    /**
     * 1-短信, 2-电话
     */
    private String gseNoticeType;

    /**
     * 模板ID
     */
    private String gseTempid;
}
