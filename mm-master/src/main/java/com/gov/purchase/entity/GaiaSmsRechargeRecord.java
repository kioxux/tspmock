package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSmsRechargeRecord {
    /**
     * ID
     */
    private Integer id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 购买时间
     */
    private String gsrrRechargeTime;

    /**
     * 单价
     */
    private Integer gsrrRechargePrice;

    /**
     * 条数
     */
    private Integer gsrrRechargeCount;

    /**
     * 金额
     */
    private Integer gsrrRechargeAmt;

    /**
     * 已用条数
     */
    private Integer gsrrRechargeUseCount;

    /**
     * 付款订单号
     */
    private String ficoId;

    /**
     * 付款状态
     */
    private Integer gsrrPayStatus;
}