package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaPayingItem extends GaiaPayingItemKey {


    /**
     * 付款主体名称
     */
    private String ficoPayingstoreName;

    /**
     * 付款方式
     */
    private String ficoPaymentMethod;

    /**
     * 付款金额
     */
    private BigDecimal ficoPaymentAmount;

    /**
     * 上期截止日期
     */
    private String ficoLastDeadline;

    /**
     * 本期开始日期
     */
    private String ficoStartingTime;

    /**
     * 本期截止日期
     */
    private String ficoEndingTime;

}