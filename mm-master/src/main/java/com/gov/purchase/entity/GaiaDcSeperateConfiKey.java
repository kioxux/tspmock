package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaDcSeperateConfiKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String gdscSite;
}