package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBhCsKey {
    /**
     * 
     */
    private String client;

    /**
     * 
     */
    private String bghSite;
}