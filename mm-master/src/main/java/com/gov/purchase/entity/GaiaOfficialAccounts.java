package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaOfficialAccounts {
    /**
     * 主键
     */
    private String id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 类型
     */
    private Integer goaType;

    /**
     * 对象编码
     */
    private String goaCode;

    /**
     * 参数
     */
    private String goaObjectParam;

    /**
     * 值
     */
    private String goaObjectValue;

    private String wechatMembershipCard;

    private String originalId;

    private String logo;

    private String cardId;

    private String appSecret;

    private String appId;

    private String stoName;
}