package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaBatchInfo extends GaiaBatchInfoKey {
    /**
     * 采购订单号
     */
    private String batPoId;

    /**
     * 采购订单行号
     */
    private String batPoLineno;

    /**
     * 生产日期
     */
    private String batProductDate;

    /**
     * 有效期至
     */
    private String batExpiryDate;

    /**
     * 生产厂家
     */
    private String batFactoryCode;

    /**
     * 生产厂家名称
     */
    private String batFactoryName;

    /**
     * 供应商编码
     */
    private String batSupplierCode;

    /**
     * 供应商名称
     */
    private String batSupplierName;

    /**
     * 付款条件编码
     */
    private String batPayType;

    /**
     * 采购单价
     */
    private BigDecimal batPoPrice;

    /**
     * 入库数量
     */
    private BigDecimal batReceiveQty;

    /**
     * 生产批号
     */
    private String batBatchNo;

    /**
     * 产地
     */
    private String batProPlace;

    /**
     * 创建日期
     */
    private String batCreateDate;

    /**
     * 创建时间
     */
    private String batCreateTime;

    /**
     * 创建人
     */
    private String batCreateUser;

    /**
     * 更新日期
     */
    private String batChangeDate;

    /**
     * 更新时间
     */
    private String batChangeTime;

    /**
     * 更新人
     */
    private String batChangeUser;
}