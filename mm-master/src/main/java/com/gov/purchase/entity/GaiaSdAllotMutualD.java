package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdAllotMutualD extends GaiaSdAllotMutualDKey {
    /**
     * 调剂日期
     */
    private String gamdDate;

    /**
     * 商品编码
     */
    private String gamdProId;

    /**
     * 批号
     */
    private String gamdBatchNo;

    /**
     * 批次
     */
    private String gamdBatch;

    /**
     * 有效期
     */
    private String gamdValidDate;

    /**
     * 批号库存数量
     */
    private String gamdStockQty;

    /**
     * 调剂数量
     */
    private String gamdRecipientQty;

    /**
     * 库存状态
     */
    private String gamdStockStatus;
}