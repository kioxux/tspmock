package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdReturnDepotD extends GaiaSdReturnDepotDKey {
    /**
     * 商品编码
     */
    private String gsrddProId;

    /**
     * 批号
     */
    private String gsrddBatchNo;

    /**
     * 批次
     */
    private String gsrddBatch;

    /**
     * 有效期
     */
    private String gsrddValidDate;

    /**
     * 批号库存数量
     */
    private String gsrddStockQty;

    /**
     * 退库数量
     */
    private String gsrddRetdepQty;

    /**
     * 退库原因
     */
    private String gsrddRetdepCause;

    /**
     * 发起数量
     */
    private String gsrddStartQty;

    /**
     * 修正数量
     */
    private String gsrddReviseQty;

    /**
     * 库存状态
     */
    private String gsrddStockStatus;
}