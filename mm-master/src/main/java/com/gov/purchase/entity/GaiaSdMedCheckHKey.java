package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMedCheckHKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 养护单号
     */
    private String gsmchVoucherId;
}