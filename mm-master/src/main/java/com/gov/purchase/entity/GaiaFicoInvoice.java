package com.gov.purchase.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class GaiaFicoInvoice extends GaiaFicoInvoiceKey {
    /**
     * 发票订单号
     */
    private String ficoOrderNo;

    /**
     * 纳税主体名称
     */
    private String ficoTaxSubjectName;

    /**
     * 发票类型
     */
    private String ficoInvoiceType;

    /**
     * 统一社会信用代码
     */
    private String ficoSocialCreditCode;

    /**
     * 开户行名称
     */
    private String ficoBankName;

    /**
     * 开户行账号
     */
    private String ficoBankNumber;

    /**
     * 地址
     */
    private String ficoCompanyAddress;

    /**
     * 电话号码
     */
    private String ficoCompanyPhoneNumber;

    /**
     * 付款金额
     */
    private BigDecimal ficoInvoiceAmount;

    /**
     * 发票回调成功PDF地址
     */
    private String ficoPdfUrl;

    /**
     * 发票请求流水号
     */
    private String ficoFpqqlsh;

    /**
     * 发票号码
     */
    private String ficoFphm;

    /**
     * 发票代码
     */
    private String ficoFpdm;

    /**
     * 发票日期
     */
    private String ficoKprq;

    /**
     * 发票状态 0未开票 1开票中 2已开票 3开票失败
     */
    private String ficoInvoiceStatus;

    /**
     * 申请开票返回结果
     */
    private String ficoInvoiceResult;

    /**
     * 发标唯一值
     */
    private Long ficoIndex;

    /**
     * 发票区分 1:电子发票，2:纸质发票
     */
    private Integer ficoType;

    /**
     * 发票业务类型 1：门店服务费发票，2：短信费发票
     */
    private Integer ficoClass;

    /**
     * 开票人
     */
    private String ficoOperator;
}