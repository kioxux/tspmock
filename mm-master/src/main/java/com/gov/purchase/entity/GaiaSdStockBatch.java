package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStockBatch extends GaiaSdStockBatchKey {
    /**
     * 商品编码
     */
    private String gssbProId;

    /**
     * 批号
     */
    private String gssbBatchNo;

    /**
     * 批次
     */
    private String gssbBatch;

    /**
     * 数量
     */
    private String gssbQty;

    /**
     * 效期
     */
    private String gssbVaildDate;

    /**
     * 更新日期
     */
    private String gssbUpdateDate;

    /**
     * 更新人员编号
     */
    private String gssbUpdateEmp;
}