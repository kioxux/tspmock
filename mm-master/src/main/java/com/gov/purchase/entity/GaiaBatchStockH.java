package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaBatchStockH extends GaiaBatchStockHKey {
    /**
     * 非限制使用库存
     */
    private BigDecimal batNormalQty;

    /**
     * 批次成本价
     */
    private BigDecimal batBatchCost;

    /**
     * 税金
     */
    private BigDecimal batRateAmt;
}