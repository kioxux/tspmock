package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaCustomerGspinfoLog extends GaiaCustomerGspinfoLogKey {
    /**
     * 客户编码
     */
    private String cusCode;

    /**
     * 客户名称
     */
    private String cusName;

    /**
     * 营业执照编号
     */
    private String cusCreditCode;

    /**
     * 营业期限
     */
    private String cusCreditDate;

    /**
     * 法人
     */
    private String cusLegalPerson;

    /**
     * 注册地址
     */
    private String cusRegAdd;

    /**
     * 许可证编号
     */
    private String cusLicenceNo;

    /**
     * 发证单位
     */
    private String cusLicenceOrgan;

    /**
     * 发证日期
     */
    private String cusLicenceDate;

    /**
     * 有效期至
     */
    private String cusLicenceValid;

    /**
     * 邮政编码
     */
    private String cusPostalCode;

    /**
     * 税务登记证
     */
    private String cusTaxCard;

    /**
     * 组织机构代码证
     */
    private String cusOrgCard;

    /**
     * 开户户名
     */
    private String cusAccountName;

    /**
     * 开户银行
     */
    private String cusAccountBank;

    /**
     * 银行账号
     */
    private String cusBankAccount;

    /**
     * 随货同行单（票）情况
     */
    private String cusDocState;

    /**
     * 企业印章情况
     */
    private String cusSealState;

    /**
     * 生产或经营范围
     */
    private String cusScope;

    /**
     * 经营方式
     */
    private String cusOperationMode;

    /**
     * 首营日期
     */
    private String cusGspDate;

    /**
     * 首营状态
     */
    private String cusGspStatus;
}