package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaMaterialAssess extends GaiaMaterialAssessKey {
    /**
     * 总库存
     */
    private BigDecimal matTotalQty;

    /**
     * 总金额
     */
    private BigDecimal matTotalAmt;

    /**
     * 移动平均价
     */
    private BigDecimal matMovPrice;

    /**
     * 税金
     */
    private BigDecimal matRateAmt;

    /**
     * 创建日期
     */
    private String batCreateDate;

    /**
     * 创建时间
     */
    private String batCreateTime;

    /**
     * 创建人
     */
    private String batCreateUser;

    /**
     * 更新日期
     */
    private String batChangeDate;

    /**
     * 更新时间
     */
    private String batChangeTime;

    /**
     * 更新人
     */
    private String batChangeUser;

    /**
     * 加点后金额
     */
    private BigDecimal matAddAmt;

    /**
     * 加点后税金
     */
    private BigDecimal matAddTax;
}