package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaMovementTypeKey {
    /**
     * 
     */
    private String client;

    /**
     * 移动类型
     */
    private String matMovType;
}