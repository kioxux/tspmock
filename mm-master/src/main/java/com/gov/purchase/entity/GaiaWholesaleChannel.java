package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWholesaleChannel extends GaiaWholesaleChannelKey {
    /**
     * 渠道名
     */
    private String gwcChaName;

    /**
     * 渠道状态, 0-正常
     */
    private String gwcStatus;

    /**
     * 创建日期
     */
    private String gwcCreDate;

    /**
     * 创建时间
     */
    private String gwcCreTime;

    /**
     * 创建人账号
     */
    private String gwcCreId;

    /**
     * 修改日期
     */
    private String gwcModiDate;

    /**
     * 修改时间
     */
    private String gwcModiTime;

    /**
     * 修改人账号
     */
    private String gwcModiId;
}