package com.gov.purchase.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @Author ：liuzhiwen.
 * @Date ：Created in 10:52 2021/8/24
 * @Description：
 * @Modified By：liuzhiwen.
 * @Version:
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GaiaNoplanMessage {
    /**
     * 主键id
     */
    private Long Id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 新品消息时间
     */
    private Date msgDate;

    /**
     * 新品消息是否查看
     */
    private String readFlag;

    /**
     * 是否是周新品消息
     */
    private String isWeekMsg;

    /**
     * 周新品消息是否查看
     */
    private String isWeekReadFlag;

    /**
     * 周新品消息时间
     */
    private Date weekMsgDate;

    /**
     * 创建日期
     */
    private Date createTime;
}
