package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaClientInvoiceInfo {
    /**
     * ID
     */
    private Integer id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 统一社会信用代码
     */
    private String ficoSocialCreditCode;

    /**
     * 纳税主体名称
     */
    private String ficoTaxSubjectName;

    /**
     * 开户行名称
     */
    private String ficoBankName;

    /**
     * 开户行账号
     */
    private String ficoBankNumber;
}