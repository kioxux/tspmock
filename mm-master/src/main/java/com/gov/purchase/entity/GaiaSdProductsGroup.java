package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdProductsGroup extends GaiaSdProductsGroupKey {
    /**
     * 分类名称
     */
    private String gspgName;

    /**
     * 商品编码
     */
    private String gspgProId;

    /**
     * 商品通用名
     */
    private String gspgProName;
}