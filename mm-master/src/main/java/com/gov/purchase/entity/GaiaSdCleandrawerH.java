package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdCleandrawerH extends GaiaSdCleandrawerHKey {
    /**
     * 清斗日期
     */
    private String gchDate;

    /**
     * 验收单号
     */
    private String gehVoucherId;

    /**
     * 清斗状态
     */
    private String gchStatus;

    /**
     * 清斗人员
     */
    private String gchEmp;

    /**
     * 备注
     */
    private String gchRemaks;
}