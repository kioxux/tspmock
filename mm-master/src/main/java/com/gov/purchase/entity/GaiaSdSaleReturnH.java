package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleReturnH extends GaiaSdSaleReturnHKey {
    /**
     * 店号
     */
    private String gsrhBrId;

    /**
     * 原销售单号
     */
    private String gsrhSaleno;

    /**
     * 原单支付金额
     */
    private String gsrhSaleamt;

    /**
     * 原单销售日期
     */
    private String gsrhSaledate;

    /**
     * 原单销售时间
     */
    private String gsrhSaletime;

    /**
     * 退货销售单号
     */
    private String gsrhRetno;

    /**
     * 退货支付金额
     */
    private String gsrhRetamt;

    /**
     * 退货销售日期
     */
    private String gsrhRetdate;

    /**
     * 退货销售时间
     */
    private String gsrhRettime;
}