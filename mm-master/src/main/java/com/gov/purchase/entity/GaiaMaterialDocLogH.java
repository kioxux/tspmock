package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaMaterialDocLogH {
    /**
     * 主键ID
     */
    private String guid;

    /**
     * 时间
     */
    private String gmdlhTime;

    /**
     * 是否成功，0：未成功，1：成功
     */
    private Integer gmdlhSucess;

    /**
     * 计划推送时间
     */
    private String gwthPlanPushTime;
}