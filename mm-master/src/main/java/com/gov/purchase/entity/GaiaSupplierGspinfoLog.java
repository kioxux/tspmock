package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSupplierGspinfoLog extends GaiaSupplierGspinfoLogKey {
    /**
     * 供应商编码
     */
    private String supCode;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 营业执照编号
     */
    private String supCreditCode;

    /**
     * 营业期限
     */
    private String supCreditDate;

    /**
     * 法人
     */
    private String supLegalPerson;

    /**
     * 注册地址
     */
    private String supRegAdd;

    /**
     * 许可证编号
     */
    private String supLicenceNo;

    /**
     * 发证单位
     */
    private String supLicenceOrgan;

    /**
     * 发证日期
     */
    private String supLicenceDate;

    /**
     * 有效期至
     */
    private String supLicenceValid;

    /**
     * 邮政编码
     */
    private String supPostalCode;

    /**
     * 税务登记证
     */
    private String supTaxCard;

    /**
     * 组织机构代码证
     */
    private String supOrgCard;

    /**
     * 开户户名
     */
    private String supAccountName;

    /**
     * 开户银行
     */
    private String supAccountBank;

    /**
     * 银行账号
     */
    private String supBankAccount;

    /**
     * 随货同行单（票）情况
     */
    private String supDocState;

    /**
     * 企业印章情况
     */
    private String supSealState;

    /**
     * 生产或经营范围
     */
    private String supScope;

    /**
     * 经营方式
     */
    private String supOperationMode;

    /**
     * 首营日期
     */
    private String supGspDate;

    /**
     * 首营状态
     */
    private String supGspStatus;
}