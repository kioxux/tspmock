package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStoresGroupKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 分类类型编码
     */
    private String gssgType;

    /**
     * 门店编码
     */
    private String gssgBrId;
}