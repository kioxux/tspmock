package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaOwnerCustomer extends GaiaOwnerCustomerKey {
    /**
     * 创建日期
     */
    private String gocCreateDate;

    /**
     * 创建时间
     */
    private String gocCreateTime;

    /**
     * 创建人
     */
    private String gocCreateBy;

    /**
     * 修改日期
     */
    private String gocChangeDate;

    /**
     * 修改时间
     */
    private String gocChangeTime;

    /**
     * 修改人
     */
    private String gocChangeBy;
}