package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaApproveRule {
    /**
     * 
     */
    private String client;

    /**
     * 采购主体
     */
    private String aprCompanyCode;

    /**
     * 单价调增值
     */
    private BigDecimal aprPriceAmt;

    /**
     * 单价调增比例
     */
    private BigDecimal aprPriceRate;

    /**
     * 存销比（百分比）
     */
    private BigDecimal aprStockUseRate;

    /**
     * 状态
     */
    private String aprTypeStatus;
}