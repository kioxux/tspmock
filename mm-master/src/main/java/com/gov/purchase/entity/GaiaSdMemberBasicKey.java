package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMemberBasicKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 会员ID
     */
    private String gsmbMemberId;
}