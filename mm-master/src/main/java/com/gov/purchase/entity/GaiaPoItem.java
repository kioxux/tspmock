package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaPoItem extends GaiaPoItemKey {
    /**
     * 商品编码
     */
    private String poProCode;

    /**
     * 采购订单数量
     */
    private BigDecimal poQty;

    /**
     * 订单单位
     */
    private String poUnit;

    /**
     * 采购订单单价
     */
    private BigDecimal poPrice;

    /**
     * 订单行金额
     */
    private BigDecimal poLineAmt;

    /**
     * 地点
     */
    private String poSiteCode;

    /**
     * 库存地点
     */
    private String poLocationCode;

    /**
     * 批次
     */
    private String poBatch;

    /**
     * 税率
     */
    private String poRate;

    /**
     * 计划交货日期
     */
    private String poDeliveryDate;

    /**
     * 订单行备注
     */
    private String poLineRemark;

    /**
     * 删除标记
     */
    private String poLineDelete;

    /**
     * 交货已完成标记
     */
    private String poCompleteFlag;

    /**
     * 已交货数量
     */
    private BigDecimal poDeliveredQty;

    /**
     * 已交货金额
     */
    private BigDecimal poDeliveredAmt;

    /**
     * 发票已完成标记
     */
    private String poCompleteInvoice;

    /**
     * 已发票校验数量
     */
    private BigDecimal poInvoiceQty;

    /**
     * 已发票校验金额
     */
    private BigDecimal poInvoiceAmt;

    /**
     * 付款已完成标记
     */
    private String poCompletePay;

    /**
     * 已付款数量
     */
    private BigDecimal poPayQty;

    /**
     * 已付款金额
     */
    private BigDecimal poPayAmt;

    /**
     * 前序订单单号
     */
    private String poPrePoid;

    /**
     * 前序订单行号
     */
    private String poPrePoitem;

    /**
     * 生产批号
     */
    private String poBatchNo;

    /**
     * 生产日期
     */
    private String poScrq;

    /**
     * 有效期
     */
    private String poYxq;

    /**
     * 拒收数量
     */
    private BigDecimal wmJssl;

    /**
     * 拒收原因
     */
    private String wmJsyy;

    /**
     * 发票价
     */
    private BigDecimal poFpj;

    /**
     * 供应商编码
     */
    private String poSupplierId;

    /**
     * 供应商名
     */
    private String supName;
}