package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPriceTag {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 模板名称
     */
    private String gptName;

    /**
     * 模板内容
     */
    private String gptTemp;

    /**
     * 模板类型
     */
    private Integer gptType;
}