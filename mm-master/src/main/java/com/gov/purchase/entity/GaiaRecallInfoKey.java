package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaRecallInfoKey {
    /**
     * 
     */
    private String client;

    /**
     * 连锁公司
     */
    private String recChainHead;

    /**
     * 召回单号
     */
    private String recNumber;

    /**
     * 召回单行号
     */
    private String recLineno;
}