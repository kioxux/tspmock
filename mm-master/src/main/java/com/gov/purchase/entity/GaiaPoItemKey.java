package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPoItemKey {
    /**
     * 
     */
    private String client;

    /**
     * 采购凭证号
     */
    private String poId;

    /**
     * 订单行号
     */
    private String poLineNo;
}