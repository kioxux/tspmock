package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaAllotProductPrice extends GaiaAllotProductPriceKey {
    /**
     * 加点金额
     */
    private BigDecimal gapgAddAmt;

    /**
     * 加点比例
     */
    private BigDecimal gapgAddRate;

    /**
     * 目录价
     */
    private BigDecimal gapgCataloguePrice;

    /**
     * 创建人
     */
    private String gapgCreateUser;

    /**
     * 创建日期
     */
    private String gapgCreateDate;

    /**
     * 创建时间
     */
    private String gapgCreateTime;

    /**
     * 更新人
     */
    private String gapgUpdateUser;

    /**
     * 更新日期
     */
    private String gapgUpdateDate;

    /**
     * 更新时间
     */
    private String gapgUpdateTime;

    /**
     * 行号
     */
    private Integer gapgIndex;
}