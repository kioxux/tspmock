package com.gov.purchase.entity;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class GaiaPriceAdjust extends GaiaPriceAdjustKey {
    /**
     * 有效期起
     */
    private String praStartDate;

    /**
     * 零售价
     */
    private BigDecimal praPriceNormal;

    /**
     * 调前零售价
     */
    private BigDecimal praPriceNormalBefore;

    /**
     * 会员价
     */
    private BigDecimal praPriceHy;

    /**
     * 调前会员价
     */
    private BigDecimal praPriceYhBefore;

    /**
     * 医保价
     */
    private BigDecimal praPriceYb;

    /**
     * 调前医保价
     */
    private BigDecimal praPriceYbBefore;

    /**
     * 拆零价
     */
    private BigDecimal praPriceCl;

    /**
     * 调前拆零价
     */
    private BigDecimal praPriceClBefore;

    /**
     * 网上零售价
     */
    private BigDecimal praPriceOln;

    /**
     * 调前网上零售价
     */
    private BigDecimal praPriceOlnBefore;

    /**
     * 网上会员价
     */
    private BigDecimal praPriceOlhy;

    /**
     * 调前网上会员价
     */
    private BigDecimal praPriceOlhyBefore;

    /**
     * 审批状态（1-待审批，2-已审批）
     */
    private String praApprovalStatus;

    /**
     * 同步状态（1-未同步，2-已同步，3-作废）
     */
    private String praSendStatus;

    /**
     * 工作流单号
     */
    private String praFlowNo;

    /**
     * 创建日期
     */
    private String praCreateDate;

    /**
     * 创建时间
     */
    private String praCreateTime;

    /**
     * 审批日期
     */
    private String praApprovalDate;

    /**
     * 审批时间
     */
    private String praApprovalTime;

    /**
     * 
     */
    private Date lastUpdateTime;
}