package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaJyfwDataGspinfoKey {
    /**
     * 加盟商ID
     */
    private String client;

    /**
     * 机构编码
     */
    private String jyfwOrgid;

    /**
     * 经营范围编码
     */
    private String jyfwId;

    /**
     * 首营流程编号
     */
    private String jyfwFlowNo;
}