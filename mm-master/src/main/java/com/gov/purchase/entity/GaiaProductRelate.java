package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaProductRelate extends GaiaProductRelateKey {
    /**
     * 关联商品编码
     */
    private String proRelateCode;

    /**
     * 采购价格
     */
    private BigDecimal proPrice;

    /**
     * 创建人
     */
    private String proCreateUser;

    /**
     * 创建日期
     */
    private String proCreateDate;

    /**
     * 创建时间
     */
    private String proCreateTime;

    /**
     * 变更人
     */
    private String proChangeUser;

    /**
     * 变更日期
     */
    private String proChangeDate;

    /**
     * 变更时间
     */
    private String proChangeTime;
}