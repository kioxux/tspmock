package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRechargeCardPaycheckKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 充值单号
     */
    private String gsrcpVoucherId;
}