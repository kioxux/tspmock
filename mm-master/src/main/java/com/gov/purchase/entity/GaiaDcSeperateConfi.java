package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaDcSeperateConfi extends GaiaDcSeperateConfiKey {
    /**
     * 分单字段
     */
    private String gdscField;
}