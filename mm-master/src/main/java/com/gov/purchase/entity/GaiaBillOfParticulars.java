package com.gov.purchase.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description 往来管理明细清单表
 * @author sunnyzhou
 * @date 2021-12-29
 */
@Data
@ApiModel("往来管理明细清单表")
public class GaiaBillOfParticulars implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户
     */
    @ApiModelProperty("用户")
    private String client;

    /**
     * 仓库编码
     */
    @ApiModelProperty("仓库编码")
    private String dcCode;

    /**
     * 仓库名称
     */
    @ApiModelProperty("仓库名称")
    private String dcName;

    /**
     * 门店编码
     */
    @ApiModelProperty("门店编码")
    private String stoCode;

    /**
     * 门店名称
     */
    @ApiModelProperty("门店名称")
    private String stoName;

    /**
     * 往来支付方式编码
     */
    @ApiModelProperty("往来支付方式编码")
    private String paymentId;

    /**
     * 实际支付金额
     */
    @ApiModelProperty("实际支付金额")
    private BigDecimal amountOfPaymentRealistic;

    /**
     * 扣率
     */
    @ApiModelProperty("扣率")
    private String amountRate;

    /**
     * 支付金额
     */
    @ApiModelProperty("支付金额")
    private BigDecimal amountOfPayment;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remarks;

    /**
     * 支付日期
     */
    @ApiModelProperty("支付日期")
    private String paymentDate;

    /**
     * 上传日期
     */
    @ApiModelProperty("上传日期")
    private String uploadDate;

    /**
     * 上传时间
     */
    @ApiModelProperty("上传时间")
    private String uploadTime;

    public GaiaBillOfParticulars() {}
}