package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaCompadm extends GaiaCompadmKey {
    /**
     * 连锁总部名称
     */
    private String compadmName;

    /**
     * 统一社会信用代码
     */
    private String compadmNo;

    /**
     * 法人/负责人
     */
    private String compadmLegalPerson;

    /**
     * 质量负责人
     */
    private String compadmQua;

    /**
     * 详细地址
     */
    private String compadmAddr;

    /**
     * 创建日期
     */
    private String compadmCreDate;

    /**
     * 创建时间
     */
    private String compadmCreTime;

    /**
     * 创建人账号
     */
    private String compadmCreId;

    /**
     * 修改日期
     */
    private String compadmModiDate;

    /**
     * 修改时间
     */
    private String compadmModiTime;

    /**
     * 修改人账号
     */
    private String compadmModiId;

    /**
     * LOGO地址
     */
    private String compadmLogo;

    /**
     * 类型 20 连锁  30 批发
     */
    private String type;
}