package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaMaterialAssessKey {
    /**
     * 
     */
    private String client;

    /**
     * 商品编码
     */
    private String matProCode;

    /**
     * 估价地点(门店)
     */
    private String matAssessSite;
}