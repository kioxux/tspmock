package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPriceAdjustKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 调价单号
     */
    private String praAdjustNo;

    /**
     * 门店编码
     */
    private String praStore;

    /**
     * 商品编码
     */
    private String praProduct;
}