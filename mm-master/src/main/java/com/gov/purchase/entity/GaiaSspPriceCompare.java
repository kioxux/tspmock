package com.gov.purchase.entity;

import com.gov.purchase.common.entity.PriceCompareBase;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class GaiaSspPriceCompare extends PriceCompareBase {
    /**
     * 单号（唯一）
     */
    private String priceCompareNo;

    /**
     * DC_编码
     */
    private String dcCode;

    /**
     * 商品范围（多选都好隔开）
     */
    private String range;

    /**
     * 报价截至时间
     */
    private LocalDateTime offerEndTime;

    /**
     * 比价模式： 1、全品比价
     */
    private Integer pattern;

    /**
     * 显示订单数量 (0: false 不显示) （1：true 显示）
     */
    private Integer showOrderAmount;

    /**
     * 失效日期
     */
    private LocalDate expiringDate;

    /**
     * 比价完成时间
     */
    private LocalDateTime finishTime;

    /**
     * 状态（1：  处理中 ，2已完成  3：已失效）
     */
    private Integer status;
}