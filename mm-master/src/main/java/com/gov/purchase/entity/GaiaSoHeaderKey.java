package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSoHeaderKey {
    /**
     * 
     */
    private String client;

    /**
     * 销售订单号
     */
    private String soId;
}