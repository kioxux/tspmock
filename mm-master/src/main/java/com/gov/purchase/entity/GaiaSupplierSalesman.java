package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSupplierSalesman {
    /**
     * 主键
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商地点
     */
    private String supSite;

    /**
     * 供应商自编码
     */
    private String supSelfCode;

    /**
     * 业务员编号
     */
    private String gssCode;

    /**
     * 业务员姓名
     */
    private String gssName;

    /**
     * 身份证号
     */
    private String gssIdCard;

    /**
     * 电话
     */
    private String gssPhone;

    /**
     * 经营范围
     */
    private String gssBusinessScope;

    /**
     * 首营流程编号
     */
    private String gssFlowNo;

    /**
     * 默认业务员
     */
    private Integer gssDefault;

    /**
     * 是否按照品种管控 1：是，0：否
     */
    private String isControl;

    /**
     * 授权委托书效期
     */
    private String gssWtsxq;
}