package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaFiUserPaymentMaintenance extends GaiaFiUserPaymentMaintenanceKey {
    /**
     * 付款主体名称
     */
    private String ficoPayingstoreName;

    /**
     * 生效日期
     */
    private String ficoEffectiveDate;

    /**
     * 收费标准
     */
    private BigDecimal ficoCurrentChargingStandard;
}