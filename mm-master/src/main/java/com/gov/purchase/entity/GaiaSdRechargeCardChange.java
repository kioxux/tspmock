package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRechargeCardChange extends GaiaSdRechargeCardChangeKey {
    /**
     * 类型
     */
    private String gsrccStatus;

    /**
     * 门店
     */
    private String gsrccBrId;

    /**
     * 日期
     */
    private String gsrccDate;

    /**
     * 时间
     */
    private String gsrccTime;

    /**
     * 工号
     */
    private String gsrccEmp;

    /**
     * 原储值卡号
     */
    private String gsrccOldCardId;

    /**
     * 新储值卡号
     */
    private String gsrccNewCardId;
}