package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductBusinessKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String proSite;

    /**
     * 商品自编码
     */
    private String proSelfCode;
}