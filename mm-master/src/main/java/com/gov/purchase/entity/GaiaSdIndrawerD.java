package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdIndrawerD extends GaiaSdIndrawerDKey {
    /**
     * 装斗日期
     */
    private String gidDate;

    /**
     * 商品编码
     */
    private String gidProId;

    /**
     * 批号
     */
    private String gidBatchNo;

    /**
     * 批次
     */
    private String gidBatch;

    /**
     * 有效期
     */
    private String gidValidDate;

    /**
     * 装斗数量
     */
    private String gidQty;

    /**
     * 装斗状态
     */
    private String gidStatus;
}