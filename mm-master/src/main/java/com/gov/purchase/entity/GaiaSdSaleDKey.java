package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 销售单号
     */
    private String gssdBillNo;

    /**
     * 店名
     */
    private String gssdBrId;

    /**
     * 销售日期
     */
    private String gssdDate;

    /**
     * 行号
     */
    private String gssdSerial;
}