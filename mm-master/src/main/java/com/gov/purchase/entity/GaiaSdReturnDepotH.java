package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdReturnDepotH extends GaiaSdReturnDepotHKey {
    /**
     * 退库日期
     */
    private String gsrdhDate;

    /**
     * 完成日期
     */
    private String gsrdhFinishDate;

    /**
     * 发货地点
     */
    private String gsrdhFrom;

    /**
     * 收货地点
     */
    private String gsrdhTo;

    /**
     * 单据类型
     */
    private String gsrdhType;

    /**
     * 退库状态
     */
    private String gsrdhStatus;

    /**
     * 退库金额
     */
    private String gsrdhTotalAmt;

    /**
     * 退库数量
     */
    private String gsrdhTotalQty;

    /**
     * 退库人员
     */
    private String gsrdhEmp;

    /**
     * 备注
     */
    private String gsrdhRemaks;

    /**
     * 退库步骤
     */
    private String gsrdhProcedure;

    /**
     * 召回单号
     */
    private String gsrdhRecallVoucherId;
}