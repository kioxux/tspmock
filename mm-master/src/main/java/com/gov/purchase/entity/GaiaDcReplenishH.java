package com.gov.purchase.entity;

import lombok.Data;

import java.util.Date;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/21 15:49
 */
@Data
public class GaiaDcReplenishH {
    private Long id;
    private String client;
    private String voucherId;
    private String proSite;
    private Integer status;
    private Date processTime;
    private Integer deleteFlag;
    private Date createTime;
    private String createUser;
    private Date updateTime;
    private String updateUser;
}
