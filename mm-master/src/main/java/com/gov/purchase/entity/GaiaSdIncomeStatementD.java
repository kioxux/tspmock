package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdIncomeStatementD extends GaiaSdIncomeStatementDKey {
    /**
     * 门店
     */
    private String gsisdBrId;

    /**
     * 生成日期
     */
    private String gsisdDate;

    /**
     * 编码
     */
    private String gsisdProId;

    /**
     * 批号
     */
    private String gsisdBatchNo;

    /**
     * 批次
     */
    private String gsisdBatch;

    /**
     * 库存数量
     */
    private String gsisdStockQty;

    /**
     * 实际数量
     */
    private String gsisdRealQty;

    /**
     * 损益原因
     */
    private String gsisdIsCause;

    /**
     * 损益数量
     */
    private String gsisdIsQty;

    /**
     * 库存状态
     */
    private String gsisdStockStatus;
}