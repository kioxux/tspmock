package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaAuthconfiDataKey {
    /**
     * 人员
     */
    private String authconfiUser;

    /**
     * SITE=门店/部门
     */
    private String authobjSite;

    /**
     * 权限组(岗位)编号
     */
    private String authGroupId;

    /**
     * 加盟商ID
     */
    private String client;
}