package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdProuctLocation extends GaiaSdProuctLocationKey {
    /**
     * 商品编码
     */
    private String gsplProId;

    /**
     * 区域
     */
    private String gsplArea;

    /**
     * 柜组
     */
    private String gsplGroup;

    /**
     * 货架
     */
    private String gsplShelf;

    /**
     * 层级
     */
    private String gsplStorey;

    /**
     * 货位
     */
    private String gsplSeat;
}