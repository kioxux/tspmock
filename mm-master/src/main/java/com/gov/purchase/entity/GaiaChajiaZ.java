package com.gov.purchase.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GaiaChajiaZ extends GaiaChajiaZKey {
    /**
     * 地点
     */
    private String cjSite;

    /**
     * 供应商编码
     */
    private String cjSupplierId;

    /**
     * 供应商业务员
     */
    private String cjSupplierSalesman;

    /**
     * 凭证日期
     */
    private String cjDate;

    /**
     * 抬头备注
     */
    private String cjHeadRemark;

    /**
     * 单据状态  0-待审批，1-已审批，2-已记账
     */
    private String cjStatus;

    /**
     * 创建人
     */
    private String cjCreateBy;

    /**
     * 创建日期
     */
    private String cjCreateDate;

    /**
     * 创建时间
     */
    private String cjCreateTime;

    /**
     * 更新人
     */
    private String cjUpdateBy;

    /**
     * 更新日期
     */
    private String cjUpdateDate;

    /**
     * 更新时间
     */
    private String cjUpdateTime;

    /**
     * 审核人
     */
    private String cjShBy;

    /**
     * 审核日期
     */
    private String cjShDate;

    /**
     * 审核时间
     */
    private String cjShTime;

    /**
     * 类型，明细表正CJ，明细负CJTZ
     */
    private String cjType;
}