package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdSaleH extends GaiaSdSaleHKey {
    /**
     * 店名
     */
    private String gshBrId;

    /**
     * 销售日期
     */
    private String gshDate;

    /**
     * 销售时间
     */
    private String gshTime;

    /**
     * 收银工号
     */
    private String gshEmp;

    /**
     * 发票号
     */
    private String gshTaxNo;

    /**
     * 会员卡号
     */
    private String gshHykNo;

    /**
     * 折扣金额
     */
    private BigDecimal gshZkAmt;

    /**
     * 应收金额
     */
    private BigDecimal gshYsAmt;

    /**
     * 现金找零
     */
    private BigDecimal gshRmbZlAmt;

    /**
     * 现金收银金额
     */
    private BigDecimal gshRmbAmt;

    /**
     * 抵用券卡号
     */
    private String gshDyqNo;

    /**
     * 抵用券金额
     */
    private BigDecimal gshDyqAmt;

    /**
     * 抵用原因
     */
    private String gshDyqType;

    /**
     * 储值卡号
     */
    private String gshRechargeCardNo;

    /**
     * 储值卡消费金额
     */
    private BigDecimal gshRechargeCardAmt;

    /**
     * 电子券充值卡号1
     */
    private String gshDzqczActno1;

    /**
     * 电子券充值金额1
     */
    private BigDecimal gshDzqczAmt1;

    /**
     * 电子券充值卡号2
     */
    private String gshDzqczActno2;

    /**
     * 电子券充值金额2
     */
    private BigDecimal gshDzqczAmt2;

    /**
     * 电子券充值卡号3
     */
    private String gshDzqczActno3;

    /**
     * 电子券充值金额3
     */
    private BigDecimal gshDzqczAmt3;

    /**
     * 电子券充值卡号4
     */
    private String gshDzqczActno4;

    /**
     * 电子券充值金额4
     */
    private BigDecimal gshDzqczAmt4;

    /**
     * 电子券充值卡号5
     */
    private String gshDzqczActno5;

    /**
     * 电子券充值金额5
     */
    private BigDecimal gshDzqczAmt5;

    /**
     * 电子券抵用卡号1
     */
    private String gshDzqdyActno1;

    /**
     * 电子券抵用金额1
     */
    private BigDecimal gshDzqdyAmt1;

    /**
     * 电子券抵用卡号2
     */
    private String gshDzqdyActno2;

    /**
     * 电子券抵用金额2
     */
    private BigDecimal gshDzqdyAmt2;

    /**
     * 电子券抵用卡号3
     */
    private String gshDzqdyActno3;

    /**
     * 电子券抵用金额3
     */
    private BigDecimal gshDzqdyAmt3;

    /**
     * 电子券抵用卡号4
     */
    private String gshDzqdyActno4;

    /**
     * 电子券抵用金额4
     */
    private BigDecimal gshDzqdyAmt4;

    /**
     * 电子券抵用卡号5
     */
    private String gshDzqdyActno5;

    /**
     * 电子券抵用金额5
     */
    private BigDecimal gshDzqdyAmt5;

    /**
     * 增加积分
     */
    private String gshIntegralAdd;

    /**
     * 兑换积分
     */
    private String gshIntegralExchange;

    /**
     * 加价兑换积分金额
     */
    private BigDecimal gshIntegralExchangeAmt;

    /**
     * 抵现积分
     */
    private String gshIntegralCash;

    /**
     * 抵现金额
     */
    private BigDecimal gshIntegralCashAmt;

    /**
     * 支付方式1
     */
    private String gshPaymentNo1;

    /**
     * 支付金额1
     */
    private BigDecimal gshPaymentAmt1;

    /**
     * 支付方式2
     */
    private String gshPaymentNo2;

    /**
     * 支付金额2
     */
    private BigDecimal gshPaymentAmt2;

    /**
     * 支付方式3
     */
    private String gshPaymentNo3;

    /**
     * 支付金额3
     */
    private BigDecimal gshPaymentAmt3;

    /**
     * 支付方式4
     */
    private String gshPaymentNo4;

    /**
     * 支付金额4
     */
    private BigDecimal gshPaymentAmt4;

    /**
     * 支付方式5
     */
    private String gshPaymentNo5;

    /**
     * 支付金额5
     */
    private BigDecimal gshPaymentAmt5;

    /**
     * 退货原销售单号
     */
    private String gshBillNoReturn;

    /**
     * 退货审核人员
     */
    private String gshEmpReturn;

    /**
     * 参加促销活动类型1
     */
    private String gshPromotionType1;

    /**
     * 参加促销活动类型2
     */
    private String gshPromotionType2;

    /**
     * 参加促销活动类型3
     */
    private String gshPromotionType3;

    /**
     * 参加促销活动类型4
     */
    private String gshPromotionType4;

    /**
     * 参加促销活动类型5
     */
    private String gshPromotionType5;
}