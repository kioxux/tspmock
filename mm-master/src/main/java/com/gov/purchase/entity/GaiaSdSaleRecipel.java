package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleRecipel extends GaiaSdSaleRecipelKey {
    /**
     * 审方类型
     */
    private String gssrType;

    /**
     * 登记店号
     */
    private String gssrBrId;

    /**
     * 登记店名
     */
    private String gssrBrName;

    /**
     * 登记日期
     */
    private String gssrDate;

    /**
     * 登记时间
     */
    private String gssrTime;

    /**
     * 登记人员
     */
    private String gssrEmp;

    /**
     * 上传状态
     */
    private String gssrUploadFlag;

    /**
     * 销售门店
     */
    private String gssrSaleBrId;

    /**
     * 销售日期
     */
    private String gssrSaleDate;

    /**
     * 销售单号
     */
    private String gssrSaleBillNo;

    /**
     * 顾客姓名
     */
    private String gssrCustName;

    /**
     * 顾客性别
     */
    private String gssrCustSex;

    /**
     * 顾客年龄
     */
    private String gssrCustAge;

    /**
     * 顾客身份证
     */
    private String gssrCustIdcard;

    /**
     * 顾客手机
     */
    private String gssrCustMobile;

    /**
     * 审方药师编号
     */
    private String gssrPharmacistId;

    /**
     * 审方药师姓名
     */
    private String gssrPharmacistName;

    /**
     * 审方日期
     */
    private String gssrCheckDate;

    /**
     * 审方时间
     */
    private String gssrCheckTime;

    /**
     * 审方状态
     */
    private String gssrCheckStatus;

    /**
     * 商品编码
     */
    private String gssrProId;

    /**
     * 商品批号
     */
    private String gssrBatchNo;

    /**
     * 数量
     */
    private String gssrQty;

    /**
     * 处方编号
     */
    private String gssrRecipelId;

    /**
     * 处方医院
     */
    private String gssrRecipelHospital;

    /**
     * 处方科室
     */
    private String gssrRecipelDepartment;

    /**
     * 处方医生
     */
    private String gssrRecipelDoctor;

    /**
     * 症状
     */
    private String gssrSymptom;

    /**
     * 诊断
     */
    private String gssrDiagnose;

    /**
     * 处方图片编号
     */
    private String gssrRecipelPicId;

    /**
     * 处方图片名称
     */
    private String gssrRecipelPicName;

    /**
     * 处方图片地址
     */
    private String gssrRecipelPicAddress;
}