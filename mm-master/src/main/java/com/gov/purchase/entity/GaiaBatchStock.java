package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaBatchStock extends GaiaBatchStockKey {
    /**
     * 非限制使用库存
     */
    private BigDecimal batNormalQty;

    /**
     * 批次成本价
     */
    private BigDecimal batBatchCost;

    /**
     * 冻结库存
     */
    private BigDecimal batLockQty;

    /**
     * 
     */
    private BigDecimal batBatchPrice;

    /**
     * 税金
     */
    private BigDecimal batRateAmt;

    /**
     * 创建日期
     */
    private String batCreateDate;

    /**
     * 创建时间
     */
    private String batCreateTime;

    /**
     * 创建人
     */
    private String batCreateUser;

    /**
     * 更新日期
     */
    private String batChangeDate;

    /**
     * 更新时间
     */
    private String batChangeTime;

    /**
     * 更新人
     */
    private String batChangeUser;
}