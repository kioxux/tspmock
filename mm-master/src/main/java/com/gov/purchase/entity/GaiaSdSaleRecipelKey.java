package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleRecipelKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 审方单号
     */
    private String gssrVoucherId;
}