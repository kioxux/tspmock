package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBussinessScope extends GaiaBussinessScopeKey {
    /**
     * 证照号
     */
    private String perId;

    /**
     * 有效期从
     */
    private Integer perDateStart;

    /**
     * 有效期至
     */
    private String perDateEnd;

    /**
     * 登记日期
     */
    private String perRecordDate;

    /**
     * 停用标志
     */
    private String perStopFlag;

    /**
     * 创建日期
     */
    private String perCreateDate;

    /**
     * 创建时间
     */
    private String perCreateTime;

    /**
     * 创建人
     */
    private String perCreateUser;

    /**
     * 更新日期
     */
    private String perChangeDate;

    /**
     * 更新时间
     */
    private String perChangeTime;

    /**
     * 更新人
     */
    private String perChangeUser;
}