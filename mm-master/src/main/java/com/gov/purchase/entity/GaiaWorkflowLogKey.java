package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWorkflowLogKey {
    /**
     * 
     */
    private String client;

    /**
     * 主键ID
     */
    private String flowGuid;
}