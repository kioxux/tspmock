package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaCustomerGspinfoLogKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 客户自编码
     */
    private String cusSelfCode;

    /**
     * 地点
     */
    private String cusSite;

    /**
     * 行号
     */
    private Integer cusLine;

    /**
     * 首营流程编号
     */
    private String cusFlowNo;
}