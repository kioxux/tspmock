package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRechargeCardLoseKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 挂失单号
     */
    private String gsrcsBrId;
}