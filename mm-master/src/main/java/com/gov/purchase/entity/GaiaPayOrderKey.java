package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPayOrderKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 采购主体
     */
    private String payCompanyCode;

    /**
     * 付款申请单号
     */
    private String payOrderId;

    /**
     * 付款申请单行号
     */
    private String payOrderLineno;
}