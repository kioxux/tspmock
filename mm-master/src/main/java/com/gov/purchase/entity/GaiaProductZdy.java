package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductZdy extends GaiaProductZdyKey {
    /**
     * 自定义1名称
     */
    private String proZdy1Name;

    /**
     * 自定义2名称
     */
    private String proZdy2Name;

    /**
     * 自定义3名称
     */
    private String proZdy3Name;

    /**
     * 自定义4名称
     */
    private String proZdy4Name;

    /**
     * 自定义5名称
     */
    private String proZdy5Name;

    /**
     * 自定义6名称
     */
    private String proZdy6Name;

    /**
     * 自定义7名称
     */
    private String proZdy7Name;

    /**
     * 自定义8名称
     */
    private String proZdy8Name;

    /**
     * 自定义9名称
     */
    private String proZdy9Name;

    /**
     * 自定义10名称
     */
    private String proZdy10Name;
}