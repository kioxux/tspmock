package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPayingItemKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 付款订单号
     */
    private String ficoId;

    /**
     * 付款主体编码
     */
    private String ficoPayingstoreCode;

    /**
     * 组织
     */
    private String ficoCompanyCode;
}