package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdDepositD extends GaiaSdDepositDKey {
    /**
     * 缴款门店
     */
    private String gpsBrId;

    /**
     * 销售日期
     */
    private String gpsSaleDate;

    /**
     * 班次
     */
    private String gpsEmpGroup;

    /**
     * 收银员
     */
    private String gpsEmp;

    /**
     * 现金实收金额
     */
    private BigDecimal gpsRmbAmt;
}