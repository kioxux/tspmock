package com.gov.purchase.entity;

import lombok.Data;

/**
 * @Author staxc
 * @Date 2020/10/28 16:03
 * @desc
 */
@Data
public class GaiaInvoiceKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 付款订单号
     */
    private String ficoId;

    /**
     * 纳税主体编码
     */
    private String ficoTaxSubjectCode;

    /**
     * 发票行号
     */
    private String ficoInvoceLineNum;


}
