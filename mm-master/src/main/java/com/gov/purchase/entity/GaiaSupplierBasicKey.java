package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSupplierBasicKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商编码
     */
    private String supCode;
}