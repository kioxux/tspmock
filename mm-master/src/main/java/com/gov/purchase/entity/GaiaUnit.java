package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaUnit {
    /**
     * 计量单位编码
     */
    private String unitCode;

    /**
     * 计量单位名称
     */
    private String unitName;

    /**
     * 状态
     */
    private String unitStatus;

    /**
     * 允许的小数位数
     */
    private String unitDecimalDigit;

    /**
     * 创建日期
     */
    private String unitCreateDate;

    /**
     * 创建时间
     */
    private String unitCreateTime;

    /**
     * 创建人
     */
    private String unitCreateUser;

    /**
     * 更新日期
     */
    private String unitChangeDate;

    /**
     * 更新时间
     */
    private String unitChangeTime;

    /**
     * 更新人
     */
    private String unitChangeUser;
}