package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaRecallInfo extends GaiaRecallInfoKey {
    /**
     * 召回门店
     */
    private String recStore;

    /**
     * 召回商品
     */
    private String recProCode;

    /**
     * 召回批号
     */
    private String recBatchNo;

    /**
     * 召回供应商
     */
    private String recSupplier;

    /**
     * 召回数量
     */
    private BigDecimal recQty;

    /**
     * 召回备注
     */
    private String recRemark;

    /**
     * 创建人
     */
    private String recCreateBy;

    /**
     * 创建日期
     */
    private String recCreateDate;

    /**
     * 创建时间
     */
    private String recCreateTime;
}