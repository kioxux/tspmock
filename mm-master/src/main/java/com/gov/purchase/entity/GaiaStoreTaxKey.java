package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaStoreTaxKey {
    /**
     * 
     */
    private String client;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 主键ID
     */
    private String stoGuid;
}