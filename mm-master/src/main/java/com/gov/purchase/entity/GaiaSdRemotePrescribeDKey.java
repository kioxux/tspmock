package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRemotePrescribeDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 开方单号
     */
    private String gsrpdVoucherId;
}