package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdExasearchDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 查询ID
     */
    private String gseSearchid;

    /**
     * 会员卡号
     */
    private String gseCardId;
}