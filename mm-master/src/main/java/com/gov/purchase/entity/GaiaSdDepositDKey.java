package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdDepositDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 缴款单号
     */
    private String gpsVoucherId;
}