package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPermitDataKey {
    /**
     * 
     */
    private String client;

    /**
     * 实体类型
     */
    private String perEntityClass;

    /**
     * 实体编码
     */
    private String perEntityId;

    /**
     * 证照编码
     */
    private String perCode;
}