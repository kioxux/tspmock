package com.gov.purchase.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class GaiaDcReplenishConf extends GaiaDcReplenishConfKey {
    /**
     * 是否实际送货天数
     */
    private Integer gdrcActualDeliveryDays;

    /**
     * 是否实际中包装量
     */
    private Integer gdrcActualMidPackage;

    /**
     * 非中药 月销量
     */
    private Integer gdrcMsUncm;

    /**
     * 非中药 月销量固定
     */
    private Integer gdrcMsUncmFixed;

    /**
     * 非中药 月销量固定数量
     */
    private BigDecimal gdrcMsUncmFixedNum;

    /**
     * 非中药 月销量单月销量
     */
    private Integer gdrcMsUncmAvg;

    /**
     * 中药 月销量
     */
    private Integer gdrcMsCm;

    /**
     * 中药 月销量固定
     */
    private Integer gdrcMsCmFixed;

    /**
     * 中药 月销量固定数量
     */
    private BigDecimal gdrcMsCmFixedNum;

    /**
     * 中药 月销量单月销量
     */
    private Integer gdrcMsCmAvg;

    /**
     * 总库存验证非中药
     */
    private Integer gdrcTqtyUncm;

    /**
     * 总库存数量非中药
     */
    private Integer gdrcTqtyUncmNum;

    /**
     * 仓库库存验证非中药
     */
    private Integer gdrcDtqyUncm;

    /**
     * 仓库库存数量非中药
     */
    private Integer gdrcDtqyUncmNum;

    /**
     * 门店库存验证非中药
     */
    private Integer gdrcStqyUncm;

    /**
     * 门店库存数量非中药
     */
    private Integer gdrcStqyUncmNum;

    /**
     * 仓库配送验证
     */
    private Integer gdrcDcDelivery;

    /**
     * 仓库配送天数
     */
    private Integer gdrcDcDeliveryDays;

    /**
     * 最小补货量验证
     */
    private Integer gdrcMinReplenish;

    /**
     * 最小补货量
     */
    private Integer gdrcMinReplenishNum;

    /**
     * 最大补货量验证
     */
    private Integer gdrcMaxReplenish;

    /**
     * 最大补货量
     */
    private Integer gdrcMaxReplenishNum;

    /**
     * 非中药 月销量单月销量
     */
    private BigDecimal gdrcMsUncmAvgNum;

    /**
     * 中药 月销量单月销量
     */
    private BigDecimal gdrcMsCmAvgNum;

    /**
     * 是否在途关闭天数
     */
    private Integer gdrcCloseDate;

    /**
     * 在途关闭天数
     */
    private Integer gdrcCloseDateDays;

    /**
     * 对外批发是否参与补货 0否1是
     */
    private Integer gdrcWholesaleFlg;

    /**
     * 对外批发是否参与补货
     */
    private Integer gdrcFranchiseeFlg;
}