package com.gov.purchase.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GaiaStoreData extends GaiaStoreDataKey {
    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 助记码
     */
    private String stoPym;

    /**
     * 门店简称
     */
    private String stoShortName;

    /**
     * 门店属性
     */
    private String stoAttribute;

    /**
     * 配送方式
     */
    private String stoDeliveryMode;

    /**
     * 关联门店
     */
    private String stoRelationStore;

    /**
     * 门店状态
     */
    private String stoStatus;

    /**
     * 经营面积
     */
    private String stoArea;

    /**
     * 开业日期
     */
    private String stoOpenDate;

    /**
     * 关店日期
     */
    private String stoCloseDate;

    /**
     * 详细地址
     */
    private String stoAdd;

    /**
     * 省
     */
    private String stoProvince;

    /**
     * 城市
     */
    private String stoCity;

    /**
     * 区/县
     */
    private String stoDistrict;

    /**
     * 是否医保店
     */
    private String stoIfMedicalcare;

    /**
     * DTP
     */
    private String stoIfDtp;

    /**
     * 税分类
     */
    private String stoTaxClass;

    /**
     * 委托配送公司
     */
    private String stoDeliveryCompany;

    /**
     * 连锁总部
     */
    private String stoChainHead;

    /**
     * 纳税主体
     */
    private String stoTaxSubject;

    /**
     * 税率
     */
    private String stoTaxRate;

    /**
     * 统一社会信用代码
     */
    private String stoNo;

    /**
     * 法人
     */
    private String stoLegalPerson;

    /**
     * 质量负责人
     */
    private String stoQua;

    /**
     * 创建日期
     */
    private String stoCreDate;

    /**
     * 创建时间
     */
    private String stoCreTime;

    /**
     * 创建人账号
     */
    private String stoCreId;

    /**
     * 修改日期
     */
    private String stoModiDate;

    /**
     * 修改时间
     */
    private String stoModiTime;

    /**
     * 修改人账号
     */
    private String stoModiId;

    /**
     * LOGO地址
     */
    private String stoLogo;

    /**
     * 门店组编码
     */
    private String stogCode;

    /**
     * 店长
     */
    private String stoLeader;

    /**
     * 主数据地点
     */
    private String stoMdSite;

    /**
     * 配送中心
     */
    private String stoDcCode;

    /**
     * 收货人签章图片
     */
    private String stoShrqxtp;

    /**
     * 运输时效（小时）
     */
    private String stoYssx;

    /**
     * 是否启用信用管理（0-否，1-是）
     */
    private String stoCreditFlag;

    /**
     *信用额度
     */
    private BigDecimal stoCreditQuota;

    /**
     * 账期类型（0-账期天数，1-固定账期日）
     */
    private String stoZqlx;

    /**
     * 固定账期天数
     */
    private Integer stoZqts;

    /**
     * 欠款标志,0-禁止补货或铺货，1-不禁止
     */
    private String stoArrearsFlag;

    /**
     * 应收余额
     */
    private BigDecimal stoArAmt;

    /**
     * 电商文案 1仓库所有合格品;2仅电商仓库
     */
    private String wmsDsfa;
}