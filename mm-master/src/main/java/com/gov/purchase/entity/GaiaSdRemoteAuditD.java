package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRemoteAuditD extends GaiaSdRemoteAuditDKey {
    /**
     * 店号
     */
    private String gsradBrId;

    /**
     * 商品编码
     */
    private String gsradProId;

    /**
     * 批号
     */
    private String gsradBatchNo;

    /**
     * 数量
     */
    private String gsradQty;
}