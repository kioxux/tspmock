package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMemberBasic extends GaiaSdMemberBasicKey {
    /**
     * 会员姓名
     */
    private String gsmbName;

    /**
     * 地址
     */
    private String gsmbAddress;

    /**
     * 手机
     */
    private String gsmbMobile;

    /**
     * 电话
     */
    private String gsmbTel;

    /**
     * 性别
     */
    private String gsmbSex;

    /**
     * 身份证
     */
    private String gsmbCredentials;

    /**
     * 年龄
     */
    private String gsmbAge;

    /**
     * 生日
     */
    private String gsmbBirth;

    /**
     * BB姓名
     */
    private String gsmbBbName;

    /**
     * BB性别
     */
    private String gsmbBbSex;

    /**
     * BB年龄
     */
    private String gsmbBbAge;

    /**
     * 慢病类型
     */
    private String gsmbChronicDiseaseType;

    /**
     * 是否接受本司信息
     */
    private String gsmbContactAllowed;

    /**
     * 会员状态
     */
    private String gsmbStatus;

    /**
     * 修改资料门店
     */
    private String gsmbUpdateBrId;

    /**
     * 修改资料人员
     */
    private String gsmbUpdateSaler;

    /**
     * 修改资料日期
     */
    private String gsmbUpdateDate;
}