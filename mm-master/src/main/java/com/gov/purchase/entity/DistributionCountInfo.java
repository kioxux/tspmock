package com.gov.purchase.entity;

import lombok.Data;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/7 15:20
 **/
@Data
public class DistributionCountInfo {
    private BigDecimal actualQuantity;
    private Integer actualStoreNum;
}
