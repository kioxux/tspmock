package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRemotePrescribeH extends GaiaSdRemotePrescribeHKey {
    /**
     * 店号
     */
    private String gsrphBrId;

    /**
     * 店名
     */
    private String gsrphBrName;

    /**
     * 请求日期
     */
    private String gsrphUploadDate;

    /**
     * 请求时间
     */
    private String gsrphUploadTime;

    /**
     * 请求人员
     */
    private String gsrphUploadEmp;

    /**
     * 请求状态
     */
    private String gsrphUploadFlag;

    /**
     * 顾客姓名
     */
    private String gsrphCustName;

    /**
     * 顾客性别
     */
    private String gsrphCustSex;

    /**
     * 顾客年龄
     */
    private String gsrphCustAge;

    /**
     * 顾客身份证
     */
    private String gsrphCustIdcard;

    /**
     * 顾客手机
     */
    private String gsrphCustMobile;

    /**
     * 症状
     */
    private String gsrphSymptom;

    /**
     * 开方日期
     */
    private String gsrphPrescribeDate;

    /**
     * 开方时间
     */
    private String gsrphPrescribeTime;

    /**
     * 处方编号
     */
    private String gsrphRecipelId;

    /**
     * 开方科室
     */
    private String gsrphPrescribeDepartment;

    /**
     * 开方医生编号
     */
    private String gsrphPrescribeDoctorId;

    /**
     * 开方医生姓名
     */
    private String gsrphPrescribeDoctorName;

    /**
     * 开方状态
     */
    private String gsrphStatus;
}