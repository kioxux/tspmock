package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaWmsTkshM {
    /**
     * ID
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 退库单号
     */
    private String wmTkdh;

    /**
     * 退库订单号
     */
    private String wmTkddh;

    /**
     * 订单序号
     */
    private String wmDdxh;

    /**
     * 商品编码
     */
    private String wmSpBm;

    /**
     * 批次号
     */
    private String wmPch;

    /**
     * 
     */
    private BigDecimal wmDhj;

    /**
     * 订单价
     */
    private BigDecimal wmDdj;

    /**
     * 收货数量
     */
    private BigDecimal wmShsl;

    /**
     * 拒收数量
     */
    private BigDecimal wmJssl;

    /**
     * 拒收原因
     */
    private String wmJsyy;

    /**
     * 库存状态编号
     */
    private String wmKcztBh;

    /**
     * 创建日期
     */
    private String wmCjrq;

    /**
     * 创建人ID
     */
    private String wmCjrId;

    /**
     * 创建人
     */
    private String wmCjr;

    /**
     * 修改日期
     */
    private String wmXgrq;

    /**
     * 修改时间
     */
    private String wmXgsj;

    /**
     * 修改人
     */
    private String wmXgr;

    /**
     * 修改人ID
     */
    private String wmXgrId;

    /**
     * 地点
     */
    private String proSite;

    /**
     * 创建时间
     */
    private String wmCjsj;

    /**
     * 生产日期
     */
    private String wmScrq;

    /**
     * 有效期
     */
    private String wmYxq;

    /**
     * 状态（1.已收货2.已验收）
     */
    private Integer state;

    /**
     * 客户编号
     */
    private String wmKhBh;

    /**
     * 商品批号
     */
    private String wmSpPh;

    /**
     * 退库原因
     */
    private String wmTkyy;
}