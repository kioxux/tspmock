package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWmsDiaoboZ extends GaiaWmsDiaoboZKey {
    /**
     * 订单号
     */
    private String wmDdh;

    /**
     * 客户号
     */
    private String wmKhBh;

    /**
     * 需求类型
     */
    private String wmXqlx;

    /**
     * 创建日期
     */
    private String wmCjrq;

    /**
     * 创建时间
     */
    private String wmCjsj;

    /**
     * 创建人
     */
    private String wmCjr;

    /**
     * 是否删除
     */
    private String wmSfsq;

    /**
     * 是否释放
     */
    private String wmSfsf;

    /**
     * 修改日期
     */
    private String wmXgrq;

    /**
     * 修改时间
     */
    private String wmXgsj;

    /**
     * 修改人
     */
    private String wmXgr;
}