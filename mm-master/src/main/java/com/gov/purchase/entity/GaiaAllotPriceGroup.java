package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaAllotPriceGroup extends GaiaAllotPriceGroupKey {
    /**
     * 价格组名称 
     */
    private String gapgName;

    /**
     * 创建人
     */
    private String gapgCreateUser;

    /**
     * 创建日期
     */
    private String gapgCreateDate;

    /**
     * 创建时间
     */
    private String gapgCreateTime;

    /**
     * 更新人
     */
    private String gapgUpdateUser;

    /**
     * 更新日期
     */
    private String gapgUpdateDate;

    /**
     * 更新时间
     */
    private String gapgUpdateTime;
}