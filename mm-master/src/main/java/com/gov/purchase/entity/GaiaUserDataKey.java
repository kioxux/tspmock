package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaUserDataKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 员工编号
     */
    private String userId;
}