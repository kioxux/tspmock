package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStoresGroup extends GaiaSdStoresGroupKey {
    /**
     * 分类编号
     */
    private String gssgId;

    /**
     * 修改人
     */
    private String gssgUpdateEmp;

    /**
     * 修改日期
     */
    private String gssgUpdateDate;

    /**
     * 修改时间
     */
    private String gssgUpdateTime;

    /**
     * 分类名称
     */
    private String gssgName;
}