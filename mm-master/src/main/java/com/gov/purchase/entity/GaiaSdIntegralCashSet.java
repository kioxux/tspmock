package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdIntegralCashSet extends GaiaSdIntegralCashSetKey {
    /**
     * 店号
     */
    private String gicsBrId;

    /**
     * 店名
     */
    private String gicsBrName;

    /**
     * 会员卡类型
     */
    private String gicsMemberClass;

    /**
     * 开始日期
     */
    private String gicsDateStart;

    /**
     * 结束日期
     */
    private String gicsDateEnd;

    /**
     * 扣除积分
     */
    private String gicsIntegralDeduct;

    /**
     * 抵扣金额
     */
    private String gicsAmtOffset;

    /**
     * 每单最大抵现金额
     */
    private String gicsAmtOffsetMax;

    /**
     * 内容说明
     */
    private String gicsDescription;

    /**
     * 当笔交易是否积分
     */
    private String gicsIntegralAddSet;

    /**
     * 是否允许支付小数
     */
    private String gicsChangeSet;

    /**
     * 修改人员
     */
    private String gicsEmp;
}