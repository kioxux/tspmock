package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStock extends GaiaSdStockKey {
    /**
     * 商品编码
     */
    private String gssProId;

    /**
     * 数量
     */
    private String gssQty;

    /**
     * 更新日期
     */
    private String gssUpdateDate;

    /**
     * 更新人员编号
     */
    private String gssUpdateEmp;
}