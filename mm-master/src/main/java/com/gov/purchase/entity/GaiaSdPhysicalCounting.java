package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdPhysicalCounting extends GaiaSdPhysicalCountingKey {
    /**
     * 盘点门店
     */
    private String gspcBrId;

    /**
     * 盘点日期
     */
    private String gspcDate;

    /**
     * 盘点时间
     */
    private String gspcTime;

    /**
     * 盘点类型
     */
    private String gspcType;

    /**
     * 行序号
     */
    private String gspcRowNo;

    /**
     * 编码
     */
    private String gspcProId;

    /**
     * 批号
     */
    private String gspcBatchNo;

    /**
     * 库存数量
     */
    private String gspcStockQty;

    /**
     * 盘点数量
     */
    private String gspcQty;

    /**
     * 审核日期
     */
    private String gspcExamineDate;

    /**
     * 审核时间
     */
    private String gspcExamineTime;

    /**
     * 审核人
     */
    private String gspcExamineEmp;

    /**
     * 状态
     */
    private String gspcStatus;
}