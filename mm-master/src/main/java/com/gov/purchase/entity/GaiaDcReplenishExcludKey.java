package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaDcReplenishExcludKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String gdreSite;

    /**
     * 业务类型
     */
    private Integer gdreBtype;

    /**
     * 商品类型
     */
    private Integer gdrePtype;

    /**
     * 编号
     */
    private String gdreCode;
}