package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdMarketing extends GaiaSdMarketingKey {
    /**
     * 营销主题ID
     */
    private String gsmThenid;

    /**
     * 营销主题名称
     */
    private String gsmThename;

    /**
     * 营销活动简介
     */
    private String gsmTheintro;

    /**
     * 活动类型
     */
    private String gsmType;

    /**
     * 活动开始时间
     */
    private String gsmStartd;

    /**
     * 活动结束时间
     */
    private String gsmEndd;

    /**
     * 周期活动ID
     */
    private String gsmCycleid;

    /**
     * 销售额标值
     */
    private BigDecimal gsmIndexValue;

    /**
     * 销售额活动值
     */
    private BigDecimal gsmTargetValue;

    /**
     * 销售额增幅
     */
    private BigDecimal gsmSaleinc;

    /**
     * 客单价选择
     */
    private String gsmPricechoose;

    /**
     * 客单价
     */
    private BigDecimal gsmPriceinc;

    /**
     * 客单价活动值
     */
    private BigDecimal gsmActPriceinc;

    /**
     * 客单价对应交易次数修正值
     */
    private BigDecimal gsmPriceincSt;

    /**
     * 交易次数选择
     */
    private String gsmStchoose;

    /**
     * 交易次数
     */
    private BigDecimal gsmStinc;

    /**
     * 
     */
    private BigDecimal gsmActStinc;

    /**
     * 交易次数对应客单价修正值
     */
    private BigDecimal gsmStincPrice;

    /**
     * 毛利率
     */
    private BigDecimal gsmGrprofit;

    /**
     * 短信模板ID
     */
    private String gsmTempid;

    /**
     * 短信查询条件
     */
    private String gsmSmscondi;

    /**
     * 短信发送人数
     */
    private BigDecimal gsmSmstimes;

    /**
     * 电话通知查询条件
     */
    private String gsmTelcondi;

    /**
     * 电话通知人数
     */
    private BigDecimal gsmTeltimes;

    /**
     * DM单打印张数
     */
    private BigDecimal gsmDmtimes;

    /**
     * DM单发放数
     */
    private String gsmDmsend;

    /**
     * DM单成本
     */
    private BigDecimal gsmDmprice;

    /**
     * DM单回收数
     */
    private String gsmDmrecovery;

    /**
     * 是否下发
     */
    private String gsmRele;

    /**
     * 状态(0:未推送, 1:已推送, 2:立即执行, 3:待审批, 4:审批通过, 5:审批拒绝)
     */
    private String gsmImpl;

    /**
     * 推送时间
     */
    private String gsmReletime;

    /**
     * 执行时间
     */
    private String gsmImpltime;

    /**
     * 0-否，1-是
     */
    private String gsmDeleteFlag;

    /**
     * 审批流程编码
     */
    private String gsmFlowNo;

    /**
     * 选品条件
     */
    private String gsmProcondi;
}