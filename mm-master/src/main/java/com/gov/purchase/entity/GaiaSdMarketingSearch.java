package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMarketingSearch {
    /**
     * 查询ID
     */
    private String gsmsId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 查询名称
     */
    private String gsmsName;

    /**
     * 门店编码
     */
    private String gsmsStore;

    /**
     * 是否常用活动 0，常用查询，1,非常用查询
     */
    private String gsmsNormal;

    /**
     * 删除标记 0-否，1-是
     */
    private String gsmsDeleteFlag;

    /**
     * 1:短信，2：电话
     */
    private Integer gsmsType;

    /**
     * 查询明细
     */
    private String gsmsDetail;
}