package com.gov.purchase.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "批发销售查询")
public class WholesaleSaleOutData {
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "商品名称")
    private String proName;
    @ApiModelProperty(value = "商品通用名称")
    private String proCommonName;
    @ApiModelProperty(value = "规格")
    private String specs;
    @ApiModelProperty(value = "单位")
    private String unit;
    @ApiModelProperty(value = "生产厂家")
    private String factoryName;
    @ApiModelProperty(value = "业务类型")
    private String matType;
    @ApiModelProperty(value = "业务名称")
    private String matName;
    @ApiModelProperty(value = "发生时间")
    private String postDate;
    @ApiModelProperty(value = "发生时间")
    private String docDate;
    @ApiModelProperty(value = "业务单据号")
    private String dnId;
    @ApiModelProperty(value = "批次去税成本额")
    private BigDecimal batAmt;
    @ApiModelProperty(value = "批次去税税金")
    private BigDecimal rateBat;
    @ApiModelProperty(value = "批次含税成本额")
    private BigDecimal totalAmount;
    @ApiModelProperty(value = "税率编码")
    private String poRate;
    @ApiModelProperty(value = "税率值")
    private String rate;
    @ApiModelProperty(value = "调拨去税金额")
    private BigDecimal addAmt;
    @ApiModelProperty(value = "调拨税金")
    private BigDecimal addTax;
    @ApiModelProperty(value = "调拨金额")
    private BigDecimal addtotalAmount;
    @ApiModelProperty(value = "客户编码")
    private String stoCode;
    @ApiModelProperty(value = "客户名称")
    private String stoName;
    @ApiModelProperty(value = " 数量")
    private BigDecimal qty;
    @ApiModelProperty(value = " 单价")
    private BigDecimal addAmtRate;
    @ApiModelProperty(value = "供应商编码")
    private String supplierCode;
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    @ApiModelProperty(value = "移动去税成本额")
    private BigDecimal movAmt;
    @ApiModelProperty(value = "移动去税税金")
    private BigDecimal rateMov;
    @ApiModelProperty(value = "移动含税成本额")
    private BigDecimal movTotalAmount;
    @ApiModelProperty(value = "发生地点")
    private String siteCode;
    @ApiModelProperty(value = "发生地点")
    private String siteName;
    //税务分类编码
    private String taxClassCode;
    //销售员
    private String gwoOwnerSaleMan;
    //销售员编码
    private String gwoOwnerSaleManUserId;
    //货主
    private String gwoOwner;
    //抬头备注
    private String soHeaderRemark;

    //采购单价（入库批次价）
    private BigDecimal batPoPrice;

    //商品自分类
    private String prosClass;
    //销售级别
    private String saleClass;
    //商品定位
    private String proPosition;
    //禁止采购
    private String purchase;
    //自定义1
    private String zdy1;
    //自定义2
    private String zdy2;
    //自定义3
    private String zdy3;
    //自定义4
    private String zdy4;
    //自定义5
    private String zdy5;
    // 大类代码
    private String bigClassCode;
    // 大类名称
    private String bigClassName;
    // 中类代码
    private String midClassCode;
    // 中类名称
    private String midClassName;
    // 商品分类代码
    private String proClassCode;
    // 商品分类名称
    private String proClassName;
}
