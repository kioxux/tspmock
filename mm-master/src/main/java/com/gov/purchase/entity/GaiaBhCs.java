package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaBhCs extends GaiaBhCsKey {
    /**
     * 
     */
    private BigDecimal bghZxbhxs;

    /**
     * 
     */
    private BigDecimal bghZy90;

    /**
     * 
     */
    private BigDecimal bghZy30;

    /**
     * 
     */
    private BigDecimal bghFzy90;

    /**
     * 
     */
    private BigDecimal bghFzy30;

    private Integer bghSqxsts;
}