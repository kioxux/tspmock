package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWholesaleOwnerKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String gwoSite;

    /**
     * 货主编码
     */
    private String gwoOwner;

    /**
     * 客户规则  1-包含，2-排除
     */
    private String gwoCusRule;

    /**
     * 商品规则  1-包含，2-排除
     */
    private String gwoProRule;
}