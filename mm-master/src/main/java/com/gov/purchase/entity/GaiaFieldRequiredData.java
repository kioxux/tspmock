package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaFieldRequiredData extends GaiaFieldRequiredDataKey {
    /**
     * 中文名
     */
    private String gfrdName;
}