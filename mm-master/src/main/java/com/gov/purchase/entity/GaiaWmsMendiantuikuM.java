package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaWmsMendiantuikuM extends GaiaWmsMendiantuikuMKey {
    /**
     * 商品编码
     */
    private String wmSpBm;

    /**
     * 批次号
     */
    private String wmPch;

    /**
     * 批号
     */
    private String wmPh;

    /**
     * 税率
     */
    private String wmSl;

    /**
     * 申请数量
     */
    private BigDecimal wmSqsl;

    /**
     * 退库原因
     */
    private String wmTkyy;

    /**
     * 确认数量
     */
    private BigDecimal wmQrsl;

    /**
     * 仓库验收数量
     */
    private BigDecimal wmCkyssl;

    /**
     * 仓库拒收数量
     */
    private BigDecimal wmCkjssl;

    /**
     * 加权成本价
     */
    private BigDecimal wmJqcbj;

    /**
     * 委托退库价(退供应商用)
     */
    private BigDecimal wmWttkj;
}