package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdBankSet extends GaiaSdBankSetKey {
    /**
     * 银行编号
     */
    private String gpsBankId;

    /**
     * 银行名称
     */
    private String gpsBankName;

    /**
     * 银行账号
     */
    private String gpsBankAccount;
}