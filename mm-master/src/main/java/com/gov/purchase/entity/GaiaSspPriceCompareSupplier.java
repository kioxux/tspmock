package com.gov.purchase.entity;

import com.gov.purchase.common.entity.PriceCompareBase;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class GaiaSspPriceCompareSupplier extends PriceCompareBase {
    /**
     * 比较表主表ID
     */
    private Long priceCompareId;

    /**
     * 操作人ID
     */
    private Long userId;

    /**
     * 供应商CODE
     */
    private String supplierCode;

    /**
     * 供应商姓名
     */
    private String supplierName;

    /**
     * 统一社会信用代码
     */
    private String supplierCreditCode;

    /**
     * 完成时间
     */
    private LocalDateTime finishTime;

    /**
     * 已报价品项数
     */
    private Integer reportAmount;

    /**
     * 单据状态(1：草稿  2；正式价格)
     */
    private Integer billStatus;
}