package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaStogData extends GaiaStogDataKey {
    /**
     * 门店组名称
     */
    private String stogName;
}