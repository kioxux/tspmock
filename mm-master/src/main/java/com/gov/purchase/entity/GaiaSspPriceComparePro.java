package com.gov.purchase.entity;

import com.gov.purchase.common.entity.PriceCompareBase;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class GaiaSspPriceComparePro extends PriceCompareBase {
    /**
     * 比价表主表.ID
     */
    private Long priceCompareId;

    /**
     * 商品编号
     */
    private String proNo;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String specs;

    /**
     * 生成厂家
     */
    private String manufacturer;

    /**
     * 单位
     */
    private String unit;

    /**
     * 中包装
     */
    private String mediumPack;

    /**
     * 大包装
     */
    private String bigPack;

    /**
     * 批准文号
     */
    private String approvalNumber;

    /**
     * 国际条形码
     */
    private String internationalBarCode;

    /**
     * 建议采购量
     */
    private Integer adviseAmount;

    /**
     * 最终采购量
     */
    private Integer finalAmount;

    /**
     * 进项税率
     */
    private String proInputTax;

    /**
     * 是否已下单（0；未下单 ，1已下单）
     */
    private Integer isOrder;

    /**
     * 下单价格
     */
    private BigDecimal orderPrice;

    /**
     * 下单供应商自编码
     */
    private String orderSupplierCode;

    /**
     * 下单供应商名称
     */
    private String orderSupplierName;

    /**
     * 货主
     */
    private String poOwner;

    /**
     * 备注
     */
    private String remarks;
}