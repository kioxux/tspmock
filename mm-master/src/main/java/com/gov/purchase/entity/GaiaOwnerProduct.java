package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaOwnerProduct extends GaiaOwnerProductKey {
    /**
     * 创建日期
     */
    private String gopCreateDate;

    /**
     * 创建时间
     */
    private String gopCreateTime;

    /**
     * 创建人
     */
    private String gopCreateBy;

    /**
     * 修改日期
     */
    private String gopChangeDate;

    /**
     * 修改时间
     */
    private String gopChangeTime;

    /**
     * 修改人
     */
    private String gopChangeBy;
}