package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPoHeaderKey {
    /**
     * 
     */
    private String client;

    /**
     * 采购凭证号
     */
    private String poId;
}