package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleReturnDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 退货单单号
     */
    private String gsrdVoucherId;
}