package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSmsSend {
    /**
     * ID
     */
    private Integer id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 手机号
     */
    private String gssTel;

    /**
     * 发送时间
     */
    private String gssSendTime;

    /**
     * 发送内容
     */
    private String gssSendContent;

    /**
     * 发送单价
     */
    private Integer gssPrice;

    /**
     * 充值记录ID
     */
    private Integer gsrrId;

    /**
     * 是否已开票
     */
    private Integer gssInvoice;

    /**
     * 所属门店
     */
    private String gssStore;
}