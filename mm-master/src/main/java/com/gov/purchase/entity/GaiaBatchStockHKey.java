package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBatchStockHKey {
    /**
     * 
     */
    private String client;

    /**
     * 商品编码
     */
    private String batProCode;

    /**
     * 地点
     */
    private String batSiteCode;

    /**
     * 库存地点
     */
    private String batLocationCode;

    /**
     * 批次
     */
    private String batBatch;

    /**
     * 当前期间的会计年度
     */
    private String batStockYear;

    /**
     * 当前期间的月份
     */
    private String matStockMonth;
}