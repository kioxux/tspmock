package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaPayOrder extends GaiaPayOrderKey {
    /**
     * 出票方
     */
    private String payOrderPayer;

    /**
     * 付款日期
     */
    private String payOrderDate;

    /**
     * 付款总金额
     */
    private BigDecimal payOrderAmt;

    /**
     * 支付方式
     */
    private String payOrderMode;

    /**
     * 承兑天数
     */
    private String payAcceptDay;

    /**
     * 银行代码
     */
    private String payBankCode;

    /**
     * 银行账号
     */
    private String payBankAccount;

    /**
     * 情况说明
     */
    private String payOrderRemark;

    /**
     * 付款状态
     */
    private String payOrderStatus;

    /**
     * 发票凭证号码
     */
    private String payInvoiceDoc;

    /**
     * 发票凭证行号
     */
    private String payLineNo;

    /**
     * 付款行数量
     */
    private BigDecimal payLineQty;

    /**
     * 付款行金额
     */
    private BigDecimal payLineAmt;

    /**
     * 创建日期
     */
    private String payCreateDate;

    /**
     * 创建时间
     */
    private String payCreateTime;

    /**
     * 创建人
     */
    private String payCreateUser;

    /**
     * 更新日期
     */
    private String payChangeDate;

    /**
     * 更新时间
     */
    private String payChangeTime;

    /**
     * 更新人
     */
    private String payChangeUser;
}