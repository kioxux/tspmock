package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdEmpGroup extends GaiaSdEmpGroupKey {
    /**
     * 分类名称
     */
    private String gsegName;

    /**
     * 门店编码
     */
    private String gsegBrId;

    /**
     * 门店名称
     */
    private String gsegBrName;
}