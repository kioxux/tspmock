package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMarketingKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 营销活动ID
     */
    private String gsmMarketid;

    /**
     * 门店编码
     */
    private String gsmStore;
}