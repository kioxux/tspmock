package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSupplierBasic {
    /**
     * 供应商编码
     */
    private String supCode;

    /**
     * 助记码
     */
    private String supPym;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 统一社会信用代码
     */
    private String supCreditCode;

    /**
     * 营业期限
     */
    private String supCreditDate;

    /**
     * 供应商分类
     */
    private String supClass;

    /**
     * 法人
     */
    private String supLegalPerson;

    /**
     * 注册地址
     */
    private String supRegAdd;

    /**
     * 供应商状态
     */
    private String supStatus;

    /**
     * 许可证编号
     */
    private String supLicenceNo;

    /**
     * 发证日期
     */
    private String supLicenceDate;

    /**
     * 有效期至
     */
    private String supLicenceValid;

    /**
     * 生产或经营范围
     */
    private String supScope;
}