package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRemotePrescribeD extends GaiaSdRemotePrescribeDKey {
    /**
     * 店号
     */
    private String gsrpdBrId;

    /**
     * 商品编码
     */
    private String gsrpdBrName;

    /**
     * 批号
     */
    private String gsrpdBatch;

    /**
     * 数量
     */
    private String gsrpdQty;
}