package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdDeviceInfoKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 设备编号
     */
    private String gsdiId;
}