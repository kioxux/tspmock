package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaOwnerCustomerKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String gocSite;

    /**
     * 货主编码
     */
    private String gocOwner;

    /**
     * 客户编码
     */
    private String gocCusCode;
}