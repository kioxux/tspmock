package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWholesaleChannelKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 仓库编码
     */
    private String gwcDcCode;

    /**
     * 渠道编码
     */
    private String gwcChaCode;
}