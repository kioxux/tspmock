package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWorkflowLog extends GaiaWorkflowLogKey {
    /**
     * 工作流程编号
     */
    private String flowNo;

    /**
     * 审批角色
     */
    private String flowApproveRole;

    /**
     * 审批人
     */
    private String flowApproveName;

    /**
     * 审批组织
     */
    private String flowApproveOrg;

    /**
     * 审批状态
     */
    private String flowApproveState;

    /**
     * 审批日期
     */
    private String flowApproveDate;

    /**
     * 审批时间
     */
    private String flowApproveTime;

    /**
     * 审批备注
     */
    private String flowApproveRemark;
}