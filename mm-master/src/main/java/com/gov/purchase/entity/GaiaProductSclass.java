package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductSclass extends GaiaProductSclassKey {
    /**
     * 商品分类名称
     */
    private String proSclassName;

    /**
     * 商品分类状态
     */
    private String proSclassStatus;
}