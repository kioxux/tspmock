package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdIntegralAddSetKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String giasBrId;
}