package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaDcReplenishRatio {
    /**
     * ID
     */
    private Integer id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String gdrrSite;

    /**
     * 补货模式
     */
    private Integer gdrrType;

    /**
     * 月销下限
     */
    private Integer gdrrLnum;

    /**
     * 月销上限
     */
    private Integer gdrrHnum;

    /**
     * 起补点
     */
    private Integer gdrrLmake;

    /**
     * 补足库存天数
     */
    private Integer gdrrHmake;

    /**
     * 系数
     */
    private BigDecimal gdrrRatio;
}