package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdPaymentMethod extends GaiaSdPaymentMethodKey {
    /**
     * 名称
     */
    private String gpmName;

    /**
     * 类型
     */
    private String gpmType;

    /**
     * 储值卡充值是否可用
     */
    private String gpmRecharge;
}