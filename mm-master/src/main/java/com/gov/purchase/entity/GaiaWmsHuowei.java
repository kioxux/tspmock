package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWmsHuowei {
    /**
     * ID
     */
    private Long id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String proSite;

    /**
     * 货位号
     */
    private String wmHwh;

    /**
     * 存储区域编号
     */
    private String wmCcqyBh;

    /**
     * 存储类型编号
     */
    private String wmCclxBh;

    /**
     * 拣货区编号
     */
    private String wmJhqBh;

    /**
     * 创建日期
     */
    private String wmCjrq;

    /**
     * 创建时间
     */
    private String wmCjsj;

    /**
     * 创建人
     */
    private String wmCjr;

    /**
     * 创建人id
     */
    private String wmCjrId;

    /**
     * 修改日期
     */
    private String wmXgrq;

    /**
     * 修改时间
     */
    private String wmXgsj;

    /**
     * 修改人
     */
    private String wmXgr;

    /**
     * 是否允许混批种 0是/1否（默认是）
     */
    private String wmHwhPz;

    /**
     * 是否允许混批号 0是/1否（默认是）
     */
    private String wmHwhPh;

    /**
     * 当前状态(0.正常1.冻结2.停用）
     */
    private String wmHwhZt;

    /**
     * 冻结原因
     */
    private String wmHwhYy;

    /**
     * 是否系统预置 1是 0 否
     */
    private String wmSfyz;
}