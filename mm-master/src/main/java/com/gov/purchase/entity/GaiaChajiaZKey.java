package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaChajiaZKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 差价单号
     */
    private String cjId;
}