package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdAllotMutualH extends GaiaSdAllotMutualHKey {
    /**
     * 调剂日期
     */
    private String gamhDate;

    /**
     * 完成日期
     */
    private String gamhFinishDate;

    /**
     * 发货地点
     */
    private String gamhFrom;

    /**
     * 收货地点
     */
    private String gamhTo;

    /**
     * 单据类型
     */
    private String gamhType;

    /**
     * 调剂状态
     */
    private String gamhStatus;

    /**
     * 调剂金额
     */
    private String gamhTotalAmt;

    /**
     * 调剂数量
     */
    private String gamhTotalQty;

    /**
     * 调剂人员
     */
    private String gamhEmp;

    /**
     * 配送/退库单号
     */
    private String gamhInvoicesId;

    /**
     * 调剂步骤
     */
    private String gamhProcedure;
}