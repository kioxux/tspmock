package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMemberCard extends GaiaSdMemberCardKey {
    /**
     * 渠道
     */
    private String gsmbcChannel;

    /**
     * 卡类型
     */
    private String gsmbcClassId;

    /**
     * 当前积分
     */
    private String gsmbcIntegral;

    /**
     * 最后积分日期
     */
    private String gsmbcIntegralLastdate;

    /**
     * 清零积分日期
     */
    private String gsmbcZeroDate;

    /**
     * 新卡创建日期
     */
    private String gsmbcCreateDate;

    /**
     * 类型
     */
    private String gsmbcType;

    /**
     * openID
     */
    private String gsmbcOpenId;

    /**
     * 卡状态
     */
    private String gsmbcStatus;

    /**
     * 组织ID
     */
   private String gsmbcOrgId;
}