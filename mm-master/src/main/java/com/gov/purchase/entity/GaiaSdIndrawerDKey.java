package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdIndrawerDKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 装斗单号
     */
    private String gidVoucherId;
}