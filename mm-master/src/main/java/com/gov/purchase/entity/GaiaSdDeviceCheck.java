package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdDeviceCheck extends GaiaSdDeviceCheckKey {
    /**
     * 店号
     */
    private String gsdcBrId;

    /**
     * 店名
     */
    private String gsdcBrName;

    /**
     * 设备编号
     */
    private String gsdcDeviceId;

    /**
     * 设备名称
     */
    private String gsdcDeviceName;

    /**
     * 型号
     */
    private String gsdcDeviceModel;

    /**
     * 维护内容
     */
    private String gsdcCheckRemarks;

    /**
     * 检查情况
     */
    private String gsdcCheckResult;

    /**
     * 调控措施
     */
    private String gsdcCheckStep;

    /**
     * 维护人员
     */
    private String gsdcUpdateEmp;

    /**
     * 维护日期
     */
    private String gsdcUpdateDate;

    /**
     * 维护时间
     */
    private String gsdcUpdateTime;

    /**
     * 审核状态
     */
    private String gsdcStatus;
}