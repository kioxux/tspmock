package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSupplierGspinfoKey {
    /**
     * 
     */
    private String client;

    /**
     * 供应商自编码
     */
    private String supSelfCode;

    /**
     * 地点
     */
    private String supSite;
}