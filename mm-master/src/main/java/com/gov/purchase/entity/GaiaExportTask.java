package com.gov.purchase.entity;

import java.util.Date;
import lombok.Data;

@Data
public class GaiaExportTask {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 导出用户
     */
    private String getUser;

    /**
     * 状态
     */
    private Integer getStatus;

    /**
     * 文件类型
     */
    private Integer getType;

    /**
     * 导出时间
     */
    private Date getDate;

    /**
     * 文件路径
     */
    private String getPath;

    /**
     * 失败原因
     */
    private String getReason;
}