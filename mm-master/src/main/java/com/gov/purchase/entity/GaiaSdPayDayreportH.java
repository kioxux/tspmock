package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdPayDayreportH extends GaiaSdPayDayreportHKey {
    /**
     * 店号
     */
    private String gpdhBrId;

    /**
     * 销售日期
     */
    private String gpdhSaleDate;

    /**
     * 审核日期
     */
    private String gpdhCheckDate;

    /**
     * 审核时间
     */
    private String gpdhCheckTime;

    /**
     * 对账员工
     */
    private String gpdhEmp;

    /**
     * 应收金额汇总
     */
    private BigDecimal gpdhtotalSalesAmt;

    /**
     * 对账金额汇总
     */
    private BigDecimal gpdhtotalInputAmt;

    /**
     * 审核状态
     */
    private String gpdhStatus;
}