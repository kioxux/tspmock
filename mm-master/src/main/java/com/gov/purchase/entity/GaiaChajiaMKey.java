package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaChajiaMKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 差价单号
     */
    private String cjId;

    /**
     * 订单行号
     */
    private String cjLineNo;
}