package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdDeviceInfo extends GaiaSdDeviceInfoKey {
    /**
     * 设备名称
     */
    private String gsdiName;

    /**
     * 门店
     */
    private String gsdiBrId;

    /**
     * 店名
     */
    private String gsdiBrName;

    /**
     * 数量
     */
    private String gsdiQty;

    /**
     * 规格/型号
     */
    private String gsdiModel;

    /**
     * 生产企业
     */
    private String gsdiFactory;

    /**
     * 启用日期
     */
    private String gsdiStartDate;

    /**
     * 登记日期
     */
    private String gsdiUpdateDat;

    /**
     * 登记人员
     */
    private String gsdiUpdateEmp;

    /**
     * 是否有说明书
     */
    private String gsdiInstruction;

    /**
     * 是否检验合格
     */
    private String gsdiQualify;

    /**
     * 是否复核
     */
    private String gsdiCheck;
}