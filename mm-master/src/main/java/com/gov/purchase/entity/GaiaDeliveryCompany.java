package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaDeliveryCompany {
    /**
     * 委托配送公司编码
     */
    private String decCode;

    /**
     * 委托配送公司名称
     */
    private String decName;

    /**
     * 委托配送公司地址
     */
    private String decAdd;

    /**
     * 委托配送公司电话
     */
    private String decTel;

    /**
     * 委托配送公司状态
     */
    private String decStatus;
}