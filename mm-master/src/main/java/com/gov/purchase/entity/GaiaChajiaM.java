package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaChajiaM extends GaiaChajiaMKey {
    /**
     * 商品编码
     */
    private String cjProCode;

    /**
     * 单据数量
     */
    private BigDecimal cjQty;

    /**
     * 含税单价
     */
    private BigDecimal cjPrice;

    /**
     * 订单行金额
     */
    private BigDecimal cjLineAmt;

    /**
     * 进项税率代码
     */
    private String cjRateCode;

    /**
     * 订单行备注
     */
    private String cjLineRemark;

    /**
     * 类型，GAIA_CHAJIA_Z.CJ_TYPE
     */
    private String cjType;
}