package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaFicoInvoiceKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 付款订单号
     */
    private String ficoId;

    /**
     * 发票行号
     */
    private String ficoInvoceLineNum;

    /**
     * 纳税主体编码
     */
    private String ficoTaxSubjectCode;
}