package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdAllotMutualHKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 调剂单号
     */
    private String gamhVoucherId;
}