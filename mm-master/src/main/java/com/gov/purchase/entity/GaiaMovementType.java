package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaMovementType extends GaiaMovementTypeKey {
    /**
     * 移动类型描述
     */
    private String matMovDesc;

    /**
     * 借/贷标识
     */
    private String matDebitCredit;

    /**
     * 状态
     */
    private String matMovStatus;

    /**
     * 创建日期
     */
    private String matMovCreateDate;

    /**
     * 创建时间
     */
    private String matMovCreateTime;

    /**
     * 创建人
     */
    private String matMovCreateUser;

    /**
     * 更新日期
     */
    private String matMovChangeDate;

    /**
     * 更新时间
     */
    private String matMovChangeTime;

    /**
     * 更新人
     */
    private String matMovChangeUser;
}