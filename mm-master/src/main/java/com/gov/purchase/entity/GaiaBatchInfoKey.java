package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBatchInfoKey {
    /**
     * 
     */
    private String client;

    /**
     * 商品编码
     */
    private String batProCode;

    /**
     * 地点
     */
    private String batSiteCode;

    /**
     * 批次
     */
    private String batBatch;

    private String batBatchNo;
}