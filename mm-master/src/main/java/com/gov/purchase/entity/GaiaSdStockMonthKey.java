package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStockMonthKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 店号
     */
    private String gssmBrId;
}