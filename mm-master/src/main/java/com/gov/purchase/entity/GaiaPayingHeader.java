package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaPayingHeader extends GaiaPayingHeaderKey {
    /**
     * 本次付款总计
     */
    private BigDecimal ficoPaymentAmountTotal;

    /**
     * 下单时间
     */
    private String ficoOrderDate;

    /**
     * 操作者
     */
    private String ficoOperator;

    /**
     * 发票状态
     */
    private String ficoInvoiceStatus;

    /**
     * 付款状态
     */
    private String ficoPaymentStatus;

    /**
     * 流水号
     */
    private String ficoSerialNumber;

    /**
     * 二维码编号
     */
    private String ficoQrcode;

    /**
     * 付款时间
     */
    private String ficoPayingDate;

    /**
     * 支付结果
     */
    private String ficoPayResult;

    /**
     * 支付方式　　1:二维码，2:app银联支付，3：网关付款
     */
    private Integer ficoType;

    /**
     * 发票类型 1-增值税普通发票；2-增值税专用发票
     */
    private String ficoInvoiceType;

    /**
     * 订单类型/1：门店付款，2：短信充值
     */
    private Integer ficoOrderType;
}