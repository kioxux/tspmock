package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdTempHumi extends GaiaSdTempHumiKey {
    /**
     * 门店
     */
    private String gsthBrId;

    /**
     * 店名
     */
    private String gsthBrName;

    /**
     * 地点
     */
    private String gsthArea;

    /**
     * 检查日期
     */
    private String gsthDate;

    /**
     * 检查时间
     */
    private String gsthTime;

    /**
     * 库内温度
     */
    private String gsthTemp;

    /**
     * 库内湿度
     */
    private String gsthHumi;

    /**
     * 调控措施
     */
    private String gsthCheckStep;

    /**
     * 调控后温度
     */
    private String gsthCheckTemp;

    /**
     * 调控后湿度
     */
    private String gsthCheckHumi;

    /**
     * 修改人员
     */
    private String gsthUpdateEmp;

    /**
     * 修改日期
     */
    private String gsthUpdateDate;

    /**
     * 修改时间
     */
    private String gsthUpdateTime;

    /**
     * 审核状态
     */
    private String gsthStatus;
}