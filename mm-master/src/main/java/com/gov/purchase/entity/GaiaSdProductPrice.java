package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdProductPrice extends GaiaSdProductPriceKey {
    /**
     * 零售价
     */
    private BigDecimal gsppPriceNormal;

    /**
     * 会员价
     */
    private BigDecimal gsppPriceHy;

    /**
     * 医保价
     */
    private BigDecimal gsppPriceYb;

    /**
     * 拆零价
     */
    private BigDecimal gsppPriceCl;

    /**
     * 会员日价
     */
    private BigDecimal gsppPriceHyr;

    /**
     * 网上零售价
     */
    private BigDecimal gsppPriceOnlineNormal;

    /**
     * 网上会员价
     */
    private BigDecimal gsppPriceOnlineHy;
}