package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPriceGroup extends GaiaPriceGroupKey {
    /**
     * 价格组描述
     */
    private String prcGroupName;

    /**
     * 创建日期
     */
    private String prcCreateDate;

    /**
     * 创建时间
     */
    private String prcCreateTime;

    /**
     * 修改日期
     */
    private String prcChangeDate;

    /**
     * 修改时间
     */
    private String prcChangeTime;
}