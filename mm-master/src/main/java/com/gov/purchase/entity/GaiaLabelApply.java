package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaLabelApply extends GaiaLabelApplyKey {
    /**
     * 申请日期
     */
    private String labApplyDate;

    /**
     * 申请份数
     */
    private String labApplyQty;

    /**
     * 状态
     */
    private String labSuatus;

    /**
     * 创建日期
     */
    private String labCreateDate;

    /**
     * 创建时间
     */
    private String labCreateTime;

    /**
     * 打印次数
     */
    private Integer labPrintTimes;

    /**
     * 最后打印日期
     */
    private String labPrintDate;

    /**
     * 最后打印时间
     */
    private String labPrintTime;

    /**
     * 导出次数
     */
    private Integer labOutputTimes;

    /**
     * 最后导出日期
     */
    private String labOutputDate;

    /**
     * 最后导出时间
     */
    private String labOutputTime;

}