package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBusinessScope extends GaiaBusinessScopeKey {
    /**
     * 删除标志
     */
    private String bscDeleteFlag;

    /**
     * 创建日期
     */
    private String bscCreateDate;

    /**
     * 创建时间
     */
    private String bscCreateTime;

    /**
     * 创建人
     */
    private String bscCreateUser;

    /**
     * 更新日期
     */
    private String bscChangeDate;

    /**
     * 更新时间
     */
    private String bscChangeTime;

    /**
     * 更新人
     */
    private String bscChangeUser;
}