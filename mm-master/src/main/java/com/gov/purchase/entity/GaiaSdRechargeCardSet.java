package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdRechargeCardSet {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店
     */
    private String gsrcsBrId;

    /**
     * 门店分类
     */
    private String gsrcsStoreClass;

    /**
     * 前缀
     */
    private String gsrcsCardPrefix;

    /**
     * 初始密码
     */
    private String gsrcsPassword;

    /**
     * 对应支付方式
     */
    private String gsrcsPaymentMethod;

    /**
     * 支付必须输入密码
     */
    private String gsrcsPwNeedOpt;

    /**
     * 支付最低金额不能低于1元
     */
    private String gsrcsMinAmtOpt;

    /**
     * 不超过整单金额
     */
    private String gsrcsMaxAmtOpt;

    /**
     * 首次使用需要修改密码
     */
    private String gsrcsFirstPwOpt;

    /**
     * 恢复密码需店长权限
     */
    private String gsrcsRecoverpwOpt;

    /**
     * 充值比例1
     */
    private String gsrcsProportion1;

    /**
     * 起充金额1
     */
    private BigDecimal gsrcsMinAmt1;

    /**
     * 充值比例2
     */
    private String gsrcsProportion2;

    /**
     * 起充金额2
     */
    private BigDecimal gsrcsMinAmt2;

    /**
     * 是否活动充值2
     */
    private String gsrcsPromotion2;

    /**
     * 是否重复2
     */
    private String gsrcsRepeat2;

    /**
     * 充值比例3
     */
    private String gsrcsProportion3;

    /**
     * 起充金额3
     */
    private BigDecimal gsrcsMinAmt3;

    /**
     * 是否活动充值3
     */
    private String gsrcsPromotion3;

    /**
     * 是否重复3
     */
    private String gsrcsRepeat3;

    /**
     * 更新日期
     */
    private String gsrcsUpdateDate;

    /**
     * 更新工号
     */
    private String gsrcsUpdateEmp;
}