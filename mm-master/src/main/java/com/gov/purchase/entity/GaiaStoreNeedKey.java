package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaStoreNeedKey {
    /**
     * 
     */
    private String client;

    /**
     * 门店申请单号
     */
    private String stoNeedId;

    /**
     * 门店申请单行号
     */
    private String stoNeedLineno;
}