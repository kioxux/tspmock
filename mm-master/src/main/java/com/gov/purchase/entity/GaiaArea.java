package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaArea {
    /**
     * 地区Id
     */
    private String areaId;

    /**
     * 地区名
     */
    private String areaName;

    /**
     * 地区级别（1:省份province,2:市city,3:区县district,4:街道street）
     */
    private String level;

    /**
     * 地区父节点
     */
    private String parentId;
}