package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaPoHeader extends GaiaPoHeaderKey {
    /**
     * 采购订单类型
     */
    private String poType;

    /**
     * 供应商编码
     */
    private String poSupplierId;

    /**
     * 采购主体
     */
    private String poCompanyCode;

    /**
     * 主体类型
     */
    private String poSubjectType;

    /**
     * 凭证日期
     */
    private String poDate;

    /**
     * 付款条款
     */
    private String poPaymentId;

    /**
     * 抬头备注
     */
    private String poHeadRemark;

    /**
     * 物流模式
     */
    private String poDeliveryType;

    /**
     * 物流模式门店
     */
    private String poDeliveryTypeStore;

    /**
     * 前序订单单号
     */
    private String poPrePoid;

    /**
     * 门店请货单号
     */
    private String poReqId;

    /**
     * 创建人
     */
    private String poCreateBy;

    /**
     * 创建日期
     */
    private String poCreateDate;

    /**
     * 创建时间
     */
    private String poCreateTime;

    /**
     * 审批状态
     */
    private String poApproveStatus;

    /**
     * 审批人
     */
    private String poApproveBy;

    /**
     * 审批时间
     */
    private String poApproveDate;

    /**
     * 更新人
     */
    private String poUpdateBy;

    /**
     * 更新日期
     */
    private String poUpdateDate;

    /**
     * 更新时间
     */
    private String poUpdateTime;

    /**
     *
     */
    private String poFlowNo;

    /**
     *
     */
    private String poReqType;

    /**
     *
     */
    private String poDnFlag;

    /**
     *
     */
    private String poDnBy;

    /**
     *
     */
    private String poDnDate;

    /**
     *
     */
    private String poDnTime;

    /**
     * 送货前置期
     */
    private String poLeadTime;

    /**
     * 配送方式
     */
    private String poDeliveryMode;

    /**
     * 承运单位
     */
    private String poCarrier;

    /**
     * 运输时间
     */
    private String poDeliveryTimes;

    /**
     * 审核日期
     */
    private String poAuditDate;

    /**
     * 供应商业务员
     */
    private String poSupplierSalesman;

    /**
     * 货主
     */
    private String poOwner;

    private String poSoFlag;

    /**
     * 业务员姓名
     */
    private String poSalesmanName;

    /**
     * 货主姓名
     */
    private String poOwnerName;
}