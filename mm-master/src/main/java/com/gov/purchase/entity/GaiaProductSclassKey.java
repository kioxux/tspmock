package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductSclassKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String proSite;

    /**
     * 商品分类编码
     */
    private String proSclassCode;
}