package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRechargeCardKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 储值卡号
     */
    private String gsrcId;
}