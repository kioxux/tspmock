package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSystemParaKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String gsspBrId;

    /**
     * 参数名
     */
    private String gsspId;
}