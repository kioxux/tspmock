package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdDeviceCheckKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 维护单号
     */
    private String gsdcVoucherId;
}