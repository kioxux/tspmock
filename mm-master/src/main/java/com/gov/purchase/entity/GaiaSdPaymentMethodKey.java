package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdPaymentMethodKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 编号
     */
    private String gpmId;
}