package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRechargeCardChangeKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 换卡单号
     */
    private String gsrccVoucherId;
}