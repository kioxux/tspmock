package com.gov.purchase.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/24 22:29
 */
@Data
public class DcReplenishDetailVO {
    private Integer index;
    private String proSite;
    private String proSiteName;
    private String proSelfCode;
    private String proCommonName;
    private String proSpecs;
    private String proUnit;
    private String factoryName;
}
