package com.gov.purchase.entity;

import com.gov.purchase.common.entity.PriceCompareBase;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class GaiaSspPriceCompareInfo extends PriceCompareBase {
    /**
     * 客户比价表.ID
     */
    private Long priceCompareSupplierId;

    /**
     * 比价商品表ID
     */
    private Long priceCompareProId;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 最终价格
     */
    private BigDecimal finalPrice;

    /**
     * 备注
     */
    private String remarks;
}