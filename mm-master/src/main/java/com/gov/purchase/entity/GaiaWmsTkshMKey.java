package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaWmsTkshMKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 退库单号
     */
    private String wmTkdh;

    /**
     * 退库订单号
     */
    private String wmTkddh;

    /**
     * 订单序号
     */
    private String wmDdxh;
}