package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleReturnHKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 退货单单号
     */
    private String gsrhVoucherId;
}