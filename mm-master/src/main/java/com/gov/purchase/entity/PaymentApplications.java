package com.gov.purchase.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author sy
 * @since 2020-10-20
 */
@Data
public class PaymentApplications {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Integer id;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "供应商")
    private String supCode;

    @ApiModelProperty(value = "供应商名称")
    private String supName;

    @ApiModelProperty(value = "地点")
    private String proSite;

    @ApiModelProperty(value = "申请日期")
    private String paymentOrderDate;

    @ApiModelProperty(value = "类别")
    private Integer type;

    @ApiModelProperty(value = "本次付款金额")
    private BigDecimal invoiceAmountOfThisPayment;

    @ApiModelProperty(value = "申请状态")
    private Integer applicationStatus;

    @ApiModelProperty(value = "审批状态")
    private Integer approvalStatus;

    @ApiModelProperty(value = "工作流编码")
    private String approvalFlowNo;

    @ApiModelProperty(value = "单据号")
    private String paymentOrderNo;


}
