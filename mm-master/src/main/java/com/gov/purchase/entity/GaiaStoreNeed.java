package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaStoreNeed extends GaiaStoreNeedKey {
    /**
     * 业务类型
     */
    private String stoNeedType;

    /**
     * 物流模式
     */
    private String stoDeliveryType;

    /**
     * 需求到货日期
     */
    private String stoNeedArrivalDate;

    /**
     * 紧急请货标志
     */
    private String stoNeedUrgent;

    /**
     * 备注
     */
    private String stoNeedRemark;

    /**
     * 商品编码
     */
    private String stoNeedPro;

    /**
     * 门店
     */
    private String stoNeedStore;

    /**
     * 调入门店
     */
    private String stoTransferStore;

    /**
     * 数量
     */
    private BigDecimal stoNeedQty;

    /**
     * 批次
     */
    private String stoNeedBatch;

    /**
     * POS建议数量
     */
    private BigDecimal stoSuggestQty;

    /**
     * 数量修改原因
     */
    private BigDecimal stoQtyReason;

    /**
     * 退库审批标识
     */
    private String stoReApproval;

    /**
     * 退货处理意见
     */
    private String stoReOpinions;

    /**
     * 单据处理状态
     */
    private String stoNeedHandle;

    /**
     * 创建日期
     */
    private String stoNeedCreateDate;

    /**
     * 创建时间
     */
    private String stoNeedCreateTime;

    /**
     * 创建人
     */
    private String stoNeedCreateUser;

    /**
     * 更新日期
     */
    private String stoNeedChangeDate;

    /**
     * 更新时间
     */
    private String stoNeedChangeTime;

    /**
     * 更新人
     */
    private String stoNeedChangeUser;
}