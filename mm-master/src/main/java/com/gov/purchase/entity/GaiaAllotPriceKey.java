package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaAllotPriceKey {
    /**
     * 
     */
    private String client;

    /**
     * 条件类型
     */
    private String alpConditionType;

    /**
     * 供货地点
     */
    private String alpSupplySite;

    /**
     * 收货地点
     */
    private String alpReceiveSite;

    /**
     * 订单类型
     */
    private String alpOrderType;

    /**
     * 连锁总部
     */
    private String alpChainHead;

    /**
     * 商品编码
     */
    private String alpProCode;
}