package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductFactory {
    /**
     * 生产企业编码
     */
    private String proFactoryCode;

    /**
     * 生产企业名称
     */
    private String proFactoryName;

    /**
     * 生产企业状态
     */
    private String proFactoryStatus;
}