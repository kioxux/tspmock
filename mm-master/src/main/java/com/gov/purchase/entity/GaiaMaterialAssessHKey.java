package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaMaterialAssessHKey {
    /**
     * 
     */
    private String client;

    /**
     * 商品编码
     */
    private String matProCode;

    /**
     * 估价地点
     */
    private String matAssessSite;

    /**
     * 当前期间的会计年度
     */
    private String matAssessYear;

    /**
     * 当前期间的月份
     */
    private String matAssessMonth;
}