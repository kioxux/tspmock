package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdRechargeCardLose extends GaiaSdRechargeCardLoseKey {
    /**
     * 类型
     */
    private String gsrcsStoreClass;

    /**
     * 门店
     */
    private String gsrcsCardPrefix;

    /**
     * 日期
     */
    private String gsrcsPassword;

    /**
     * 时间
     */
    private String gsrcsPaymentMethod;

    /**
     * 工号
     */
    private String gsrcsPwNeedOpt;

    /**
     * 储值卡号
     */
    private String gsrcsMinAmtOpt;
}