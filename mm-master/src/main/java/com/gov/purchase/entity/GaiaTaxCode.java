package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaTaxCode {
    /**
     * 税率编码
     */
    private String taxCode;

    /**
     * 税率名称
     */
    private String taxCodeName;

    /**
     * 税率类型
     */
    private String taxCodeClass;

    /**
     * 税率值
     */
    private String taxCodeValue;

    /**
     * 税码状态
     */
    private String taxCodeStatus;
}