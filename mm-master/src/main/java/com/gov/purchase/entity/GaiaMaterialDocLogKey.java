package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaMaterialDocLogKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 主键ID
     */
    private String guid;

    /**
     * 序号
     */
    private String guidNo;
}