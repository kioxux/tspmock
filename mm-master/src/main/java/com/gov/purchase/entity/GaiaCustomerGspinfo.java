package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaCustomerGspinfo extends GaiaCustomerGspinfoKey {
    /**
     * 
     */
    private String cusCode;

    /**
     * 客户名称
     */
    private String cusName;

    /**
     * 营业执照编号
     */
    private String cusCreditCode;

    /**
     * 营业期限
     */
    private String cusCreditDate;

    /**
     * 法人
     */
    private String cusLegalPerson;

    /**
     * 注册地址
     */
    private String cusRegAdd;

    /**
     * 许可证编号
     */
    private String cusLicenceNo;

    /**
     * 发证单位
     */
    private String cusLicenceOrgan;

    /**
     * 发证日期
     */
    private String cusLicenceDate;

    /**
     * 有效期至
     */
    private String cusLicenceValid;

    /**
     * 邮政编码
     */
    private String cusPostalCode;

    /**
     * 税务登记证
     */
    private String cusTaxCard;

    /**
     * 组织机构代码证
     */
    private String cusOrgCard;

    /**
     * 开户户名
     */
    private String cusAccountName;

    /**
     * 开户银行
     */
    private String cusAccountBank;

    /**
     * 银行账号
     */
    private String cusBankAccount;

    /**
     * 随货同行单（票）情况
     */
    private String cusDocState;

    /**
     * 企业印章情况
     */
    private String cusSealState;

    /**
     * 生产或经营范围
     */
    private String cusScope;

    /**
     * 经营方式
     */
    private String cusOperationMode;

    /**
     * 首营日期
     */
    private String cusGspDate;

    /**
     * 首营流程编号
     */
    private String cusFlowNo;

    /**
     * 
     */
    private String cusGspStatus;

    /**
     * 收货地址
     */
    private String cusDeliveryAdd;

    /**
     * 质量负责人
     */
    private String cusQua;

    /**
     * 业务联系人
     */
    private String cusBussinessContact;

    /**
     * 医疗器械经营许可证有效期
     */
    private String cusYlqxxkzDate;

    /**
     * 医疗器械经营许可证书
     */
    private String cusYlqxxkz;

    /**
     * 医疗机构执业许可证书有效期
     */
    private String cusYljgxkzDate;

    /**
     * 医疗机构执业许可证书
     */
    private String cusYljgxkz;

    /**
     * 客户状态
     */
    private String cusStatus;

    /**
     * 食品许可证有效期
     */
    private String cusSpxkzDate;

    /**
     * 食品许可证书
     */
    private String cusSpxkz;

    /**
     * 助记码
     */
    private String cusPym;

    /**
     * 医疗器械经营许可证图片
     */
    private String cusPictureYlqxxkz;

    /**
     * 医疗机构执业许可证图片
     */
    private String cusPictureYljgxkz;

    /**
     * 食品许可证书图片
     */
    private String cusPictureSpxkz;

    /**
     * 随货同行单图片
     */
    private String cusPictureShtxd;

    /**
     * 其他图片
     */
    private String cusPictureQt;

    /**
     * GSP证书图片
     */
    private String cusPictureGsp;

    /**
     * GMP证书图片
     */
    private String cusPictureGmp;

    /**
     * 法人委托书图片
     */
    private String cusPictureFrwts;

    /**
     * 付款条件
     */
    private String cusPayTerm;

    /**
     * 支付方式
     */
    private String cusPayMode;

    /**
     * 禁止销售
     */
    private String cusNoSale;

    /**
     * 禁止退货
     */
    private String cusNoReturn;

    /**
     * 匹配状态
     */
    private String cusMatchStatus;

    /**
     * GMP有效期
     */
    private String cusGmpDate;

    /**
     * GMP证书
     */
    private String cusGmp;

    /**
     * 信用额度
     */
    private BigDecimal cusCreditQuota;

    /**
     * 是否启用信用管理
     */
    private String cusCreditFlag;

    /**
     * 信用检查点
     */
    private String cusCreditCheck;

    /**
     * 铺底授信额度
     */
    private String cusCreditAmt;

    /**
     * 联系人电话
     */
    private String cusContactTel;

    /**
     * 客户分类
     */
    private String cusClass;

    /**
     * 银行名称
     */
    private String cusBankName;

    /**
     * 银行代码
     */
    private String cusBankCode;

    /**
     * 账户持有人
     */
    private String cusAccountPerson;

    /**
     * GSP证书
     */
    private String cusGsp;

    /**
     * GSP有效期
     */
    private String cusGspDates;

    /**
     * 注册资本
     */
    private String cusZczb;

    /**
     * 仓库地址
     */
    private String cusCkdz;

    /**
     * 业务员（销售）
     */
    private String cusYwy;

    /**
     * 业务员（采购员）的身份证号码
     */
    private String cusYwysfz;

    /**
     * 业务员（采购员）授权日期
     */
    private String cusYwysqrq;

    /**
     * 业务员（采购员）结束日期
     */
    private String cusYwyjsrq;

    /**
     * 收货人/提货人姓名
     */
    private String cusShr;

    /**
     * 收货人/提货人身份证号
     */
    private String cusShrsfz;

    /**
     * 收货人/提货人授权日期
     */
    private String cusShrsqrq;

    /**
     * 收货人/提货人结束日期
     */
    private String cusShrjsrq;

    /**
     * 备注
     */
    private String cusRemarks;

    /**
     * 企业负责人
     */
    private String cusQyfzr;

    /**
     * 经营方式
     */
    private String cusJyfs;

    /**
     * 开户许可证号
     */
    private String cusKhxkzh;

    /**
     * 税号
     */
    private String cusSh;

    /**
     * 电话号码
     */
    private String cusDhhm;

    /**
     * 发票类型”及其选项（增值税专用发票、普通发票或普票）
     */
    private String cusFplx;

    /**
     * 医疗器械备案证
     */
    private String cusYlqxbaz;

    /**
     * 医疗器械类别”（选项：一类、二类、三类）
     */
    private String cusYlqxlb;

    /**
     * 质保协议期限
     */
    private String cusZbxyyxq;

    /**
     * 购销合同期限
     */
    private String cusGxhtyxq;

    /**
     * 法人委托书
     */
    private String cusFrwts;

    /**
     * 法人委托书效期
     */
    private String cusFrwtsxq;

    /**
     * 客户签收图片
     */
    private String cusKhqstp;

    /**
     * 运输方式
     */
    private String cusYsfs;

    /**
     * 运输时长
     */
    private String cusYssc;

    /**
     * GSP控制类型”及其选项（批发企业/医疗机构/连锁药店/其他）
     */
    private String cusGspgklx;

    /**
     * 经营许可证图片
     */
    private String cusLicenceImg;
}