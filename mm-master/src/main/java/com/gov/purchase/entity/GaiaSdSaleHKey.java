package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdSaleHKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 销售单号
     */
    private String gshBillNo;
}