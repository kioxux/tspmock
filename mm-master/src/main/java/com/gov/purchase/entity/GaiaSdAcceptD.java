package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdAcceptD extends GaiaSdAcceptDKey {
    /**
     * 收货日期
     */
    private String gadDate;

    /**
     * 商品编码
     */
    private String gadProId;

    /**
     * 批号
     */
    private String gadBatchNo;

    /**
     * 批次
     */
    private String gadBatch;

    /**
     * 有效期
     */
    private String gadValidDate;

    /**
     * 单据数量
     */
    private String gadInvoicesQty;

    /**
     * 到货数量
     */
    private String gadRecipientQty;

    /**
     * 拒收数量
     */
    private String gadRefuseQty;

    /**
     * 箱号
     */
    private String gadGadPackNo;

    /**
     * 库存状态
     */
    private String gadStockStatus;
}