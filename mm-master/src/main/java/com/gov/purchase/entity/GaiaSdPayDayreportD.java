package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaSdPayDayreportD extends GaiaSdPayDayreportDKey {
    /**
     * 支付方式
     */
    private String gpddPaymethodId;

    /**
     * 应收金额
     */
    private BigDecimal gpddSalesAmt;

    /**
     * 人工对账金额
     */
    private BigDecimal gpddInputAmt;
}