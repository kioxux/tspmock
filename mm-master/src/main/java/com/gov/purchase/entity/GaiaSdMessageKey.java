package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdMessageKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店/配送中心
     */
    private String gsmId;

    /**
     * 消息单号
     */
    private String gsmVoucherId;
}