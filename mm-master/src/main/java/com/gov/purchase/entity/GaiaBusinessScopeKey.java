package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaBusinessScopeKey {
    /**
     * 
     */
    private String client;

    /**
     * 实体类型
     */
    private String bscEntityClass;

    /**
     * 实体编码
     */
    private String bscEntityId;

    /**
     * 经营范围编码
     */
    private String bscCode;
}