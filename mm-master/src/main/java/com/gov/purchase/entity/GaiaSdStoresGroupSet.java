package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStoresGroupSet extends GaiaSdStoresGroupSetKey {
    /**
     * 分类类型名称
     */
    private String gssgTypeName;

    /**
     * 分类编码名称
     */
    private String gssgIdName;

    /**
     * 修改人
     */
    private String gssgUpdateEmp;

    /**
     * 修改日期
     */
    private String gssgUpdateDate;

    /**
     * 修改时间
     */
    private String gssgUpdateTime;
}