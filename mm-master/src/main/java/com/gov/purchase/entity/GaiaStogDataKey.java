package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaStogDataKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店组编码
     */
    private String stogCode;
}