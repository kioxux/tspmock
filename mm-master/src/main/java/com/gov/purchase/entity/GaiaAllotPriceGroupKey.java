package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaAllotPriceGroupKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地址
     */
    private String gapgSite;

    /**
     * 类型
     */
    private Integer gapgType;

    /**
     * 价格组编号
     */
    private String gapgCode;
}