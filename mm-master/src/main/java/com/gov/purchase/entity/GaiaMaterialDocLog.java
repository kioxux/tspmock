package com.gov.purchase.entity;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class GaiaMaterialDocLog extends GaiaMaterialDocLogKey {
    /**
     * 业务类型
     */
    private String matType;

    /**
     * 过账日期
     */
    private String matPostDate;

    /**
     * 抬头备注
     */
    private String matHeadRemark;

    /**
     * 商品编码
     */
    private String matProCode;

    /**
     * 配送门店
     */
    private String matStoCode;

    /**
     * 地点
     */
    private String matSiteCode;

    /**
     * 库存地点
     */
    private String matLocationCode;

    /**
     * 转移库存地点
     */
    private String matLocationTo;

    /**
     * 批次
     */
    private String matBatch;

    /**
     * 交易数量
     */
    private BigDecimal matQty;

    /**
     * 交易金额
     */
    private BigDecimal matAmt;

    /**
     * 交易价格
     */
    private BigDecimal matPrice;

    /**
     * 基本计量单位
     */
    private String matUint;

    /**
     * 借/贷标识
     */
    private String matDebitCredit;

    /**
     * 订单号
     */
    private String matPoId;

    /**
     * 订单行号
     */
    private String matPoLineno;

    /**
     * 业务单号
     */
    private String matDnId;

    /**
     * 业务单行号
     */
    private String matDnLineno;

    /**
     * 物料凭证行备注
     */
    private String matLineRemark;

    /**
     * 创建人
     */
    private String matCreateBy;

    /**
     * 创建日期
     */
    private String matCreateDate;

    /**
     * 创建时间
     */
    private String matCreateTime;

    /**
     * 物料凭证号
     */
    private String matId;

    /**
     * 物料凭证年份
     */
    private String matYear;

    /**
     * 物料凭证行号
     */
    private String matLineNo;

    /**
     * 物料凭证生成状态（0-未处理，1-成功，2-失败）
     */
    private String matStatus;

    /**
     * 重处理人
     */
    private String matUpdateBy;

    /**
     * 重处理日期
     */
    private String matUpdateDate;

    /**
     * 重处理时间
     */
    private String matUpdateTime;

    /**
     * 互调价
     */
    private BigDecimal matHdPrice;
}