package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSourceList extends GaiaSourceListKey {
    /**
     * 供应商编码
     */
    private String souSupplierId;

    /**
     * 有效期从
     */
    private String souEffectFrom;

    /**
     * 有效期至
     */
    private String souEffectEnd;

    /**
     * 是否主供应商
     */
    private String souMainSupplier;

    /**
     * 是否锁定供应商
     */
    private String souLockSupplier;

    /**
     * 冻结标志
     */
    private String souDeleteFlag;

    /**
     * 创建类型
     */
    private String souCreateType;

    /**
     * 创建人
     */
    private String souCreateBy;

    /**
     * 创建日期
     */
    private String souCreateDate;

    /**
     * 创建时间
     */
    private String souCreateTime;

    /**
     * 更新人
     */
    private String souUpdateBy;

    /**
     * 更新日期
     */
    private String souUpdateDate;

    /**
     * 更新时间
     */
    private String souUpdateTime;
}