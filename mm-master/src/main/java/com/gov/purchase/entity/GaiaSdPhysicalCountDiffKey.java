package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdPhysicalCountDiffKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 差异单号
     */
    private String gspcdVoucherId;
}