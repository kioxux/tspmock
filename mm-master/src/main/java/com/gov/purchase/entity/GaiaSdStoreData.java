package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSdStoreData extends GaiaSdStoreDataKey {
    /**
     * 店名
     */
    private String perEntityId;

    /**
     * 每单销售最大数量
     */
    private String perCode;

    /**
     * 最后登录系统日期
     */
    private String perId;

    /**
     * 全盘盘点开始日期
     */
    private String perDateStart;

    /**
     * 店型
     */
    private String perDateEnd;

    /**
     * 最低折率设置
     */
    private String perRecordDate;

    /**
     * 抵用券使用最大金额
     */
    private String perStopFlag;

    /**
     * 设置开店时间
     */
    private String perCreateDate;

    /**
     * 设置交班时间
     */
    private String perCreateTime;

    /**
     * 设置闭店时间
     */
    private String perCreateUser;

    /**
     * 积分清零日期
     */
    private String perChangeDate;
}