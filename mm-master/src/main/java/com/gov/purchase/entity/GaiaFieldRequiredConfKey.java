package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaFieldRequiredConfKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 类型（1-商品 2-供应商 3-客户）
     */
    private String gfrcType;

    /**
     * 字段名
     */
    private String gfrcField;

    /**
     * 中文名
     */
    private String gfrdName;
}