package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductBasic {
    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 通用名称
     */
    private String proCommonname;

    /**
     * 商品描述
     */
    private String proDepict;

    /**
     * 助记码
     */
    private String proPym;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 剂型
     */
    private String proForm;

    /**
     * 细分剂型
     */
    private String proPartform;

    /**
     * 最小剂量（以mg/ml计算）
     */
    private String proMindose;

    /**
     * 总剂量（以mg/ml计算）
     */
    private String proTotaldose;

    /**
     * 国际条形码1
     */
    private String proBarcode;

    /**
     * 国际条形码2
     */
    private String proBarcode2;

    /**
     * 批准文号分类
     */
    private String proRegisterClass;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 批准文号批准日期
     */
    private String proRegisterDate;

    /**
     * 批准文号失效日期
     */
    private String proRegisterExdate;

    /**
     * 商品分类
     */
    private String proClass;

    /**
     * 商品分类描述
     */
    private String proClassName;

    /**
     * 成分分类
     */
    private String proCompclass;

    /**
     * 成分分类描述
     */
    private String proCompclassName;

    /**
     * 处方类别
     */
    private String proPresclass;

    /**
     * 生产企业代码
     */
    private String proFactoryCode;

    /**
     * 生产企业
     */
    private String proFactoryName;

    /**
     * 商标
     */
    private String proMark;

    /**
     * 品牌标识名
     */
    private String proBrand;

    /**
     * 品牌区分
     */
    private String proBrandClass;

    /**
     * 保质期
     */
    private String proLife;

    /**
     * 保质期单位
     */
    private String proLifeUnit;

    /**
     * 上市许可持有人
     */
    private String proHolder;

    /**
     * 进项税率
     */
    private String proInputTax;

    /**
     * 销项税率
     */
    private String proOutputTax;

    /**
     * 药品本位码
     */
    private String proBasicCode;

    /**
     * 税务分类编码
     */
    private String proTaxClass;

    /**
     * 管制特殊分类
     */
    private String proControlClass;

    /**
     * 生产类别
     */
    private String proProduceClass;

    /**
     * 贮存条件
     */
    private String proStorageCondition;

    /**
     * 商品仓储分区
     */
    private String proStorageArea;

    /**
     * 长（以MM计算）
     */
    private String proLong;

    /**
     * 宽（以MM计算）
     */
    private String proWide;

    /**
     * 高（以MM计算）
     */
    private String proHigh;

    /**
     * 中包装量
     */
    private String proMidPackage;

    /**
     * 大包装量
     */
    private String proBigPackage;

    /**
     * 启用电子监管码
     */
    private String proElectronicCode;

    /**
     * 生产经营许可证号
     */
    private String proQsCode;

    /**
     * 最大销售量
     */
    private String proMaxSales;

    /**
     * 说明书代码
     */
    private String proInstructionCode;

    /**
     * 说明书内容
     */
    private String proInstruction;

    /**
     * 国家医保品种
     */
    private String proMedProdct;

    /**
     * 国家医保品种编码
     */
    private String proMedProdctcode;

    /**
     * 生产国家
     */
    private String proCountry;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 状态
     */
    private String proStatus;

    /**
     * 可服用天数
     */
    private String proTakeDays;

    /**
     * 用法用量
     */
    private String proUsage;

    /**
     * 禁忌说明
     */
    private String proContraindication;
}