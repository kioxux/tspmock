package com.gov.purchase.entity;

import com.gov.purchase.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @desc:
 * @author: xiaozy
 * @createTime: 2021/5/31 11:04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GaiaNewDistributionPlan extends BaseEntity {
    /**加盟商*/
    private String client;
    /**铺货计划单号*/
    private String planCode;
    /**商品编码*/
    private String proSelfCode;
    /**计划选择方式：1-门店属性 2-门店类型 3-门店*/
    private Integer planType;
    /**铺货计划天数*/
    private Integer planDays;
    /**有效期截止日期*/
    private Date expireDate;
    /**铺货方式：1-单次提取 2-多次提取*/
    private Integer distributionType;
    /**完成日期*/
    private Date finishDate;
    /**计划铺货数量*/
    private BigDecimal planQuantity;
    /**实际铺货数量*/
    private BigDecimal actualQuantity;
    /**计划铺货门店*/
    private Integer planStoreNum;
    /**实际铺货门店*/
    private Integer actualStoreNum;
    /**计划状态*/
    private Integer status;
    /**调取次数*/
    private Integer distributionNum;
    /**商品名称*/
    private String proSelfName;
    /**已使用的入库单号，防止重复铺货*/
    private String wmRkdh;
    /**是否需要补铺：0-否 1-是*/
    private Integer needReplenishFlag;
    /**是否执行补铺：0-否 1-是*/
    private Integer execReplenishFlag;
    /**首次调取时间*/
    private Date firstCallTime;
    /**最后调取时间*/
    private Date lastCallTime;
    /**是否自动执行：1-自动 2-手动*/
    private Integer autoFlag;
    /**是否关联加盟商 0或空：不包含、 1：包含*/
    private Integer isIncludeClient;
}
