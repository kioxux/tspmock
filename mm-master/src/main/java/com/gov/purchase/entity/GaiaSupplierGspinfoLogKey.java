package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaSupplierGspinfoLogKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商自编码
     */
    private String supSelfCode;

    /**
     * 地点
     */
    private String supSite;

    /**
     * 行号
     */
    private Integer supLine;

    /**
     * 首营流程编号
     */
    private String supFlowNo;
}