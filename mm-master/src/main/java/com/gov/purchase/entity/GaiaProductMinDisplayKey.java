package com.gov.purchase.entity;

import lombok.Data;

@Data
public class GaiaProductMinDisplayKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String gpmdBrId;

    /**
     * 商品编码
     */
    private String gpmdProId;
}