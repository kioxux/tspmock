package com.gov.purchase.exception;

import lombok.Data;

@Data
public class FeignResultException extends RuntimeException {

    private Integer code;

    public FeignResultException(Integer code, String message) {
        super(message);
        this.code = code;
    }

}
