package com.gov.purchase.common.diff;

import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.module.base.dto.DictionaryParams;
import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021/4/15 9:21
 */
@Data
public class DiffEntityInfo {
    /**
     * 属性名(修改前)
     */
    private String propertyFrom;
    /**
     * 属性名(修改后)
     */
    private String propertyTo;
    /**
     * 类型
     */
    private Class<?> type;
    /**
     * 数据库字段名
     */
    private String column;
    /**
     * 数据库字段描述
     */
    private String columnDes;
    /**
     * 否记录差异(0:否 1:是)
     */
    private Integer flagDiff;

    /**
     * 是否需要翻译(0:否 1:是)
     */
    private Integer flagNeedTranslate;

    /**
     * 静态字典
     */
    private CommonEnum.DictionaryStaticData dictionaryStaticData;

    /**
     * 动态字典
     */
    private CommonEnum.DictionaryData dictionaryData;
    /**
     * 动态字典请求参数
     */
    private DictionaryParams dictionaryParams;


    public DiffEntityInfo() {
    }

    /**
     * 构造器,没有字典配置项
     */
    public DiffEntityInfo(String propertyFrom, String propertyTo, Class<?> type, String column, String columnDes, Integer flagDiff, Integer flagNeedTranslate) {
        this.propertyFrom = propertyFrom;
        this.propertyTo = propertyTo;
        this.type = type;
        this.column = column;
        this.columnDes = columnDes;
        this.flagDiff = flagDiff;
        this.flagNeedTranslate = flagNeedTranslate;
    }

    /**
     * 静态字典
     */
    public DiffEntityInfo(String propertyFrom, String propertyTo, Class<?> type, String column, String columnDes, Integer flagDiff, Integer flagNeedTranslate, CommonEnum.DictionaryStaticData dictionaryStaticData) {
        this.propertyFrom = propertyFrom;
        this.propertyTo = propertyTo;
        this.type = type;
        this.column = column;
        this.columnDes = columnDes;
        this.flagDiff = flagDiff;
        this.flagNeedTranslate = flagNeedTranslate;
        this.dictionaryStaticData = dictionaryStaticData;
    }

    /**
     * 动态字典
     */
    public DiffEntityInfo(String propertyFrom, String propertyTo, Class<?> type, String column, String columnDes, Integer flagDiff, Integer flagNeedTranslate, CommonEnum.DictionaryData dictionaryData) {
        this.propertyFrom = propertyFrom;
        this.propertyTo = propertyTo;
        this.type = type;
        this.column = column;
        this.columnDes = columnDes;
        this.flagDiff = flagDiff;
        this.flagNeedTranslate = flagNeedTranslate;
        this.dictionaryData = dictionaryData;
    }

    /**
     * 动态字典+动态字典参数
     */
    public DiffEntityInfo(String propertyFrom, String propertyTo, Class<?> type, String column, String columnDes, Integer flagDiff, Integer flagNeedTranslate, CommonEnum.DictionaryData dictionaryData, DictionaryParams dictionaryParams) {
        this.propertyFrom = propertyFrom;
        this.propertyTo = propertyTo;
        this.type = type;
        this.column = column;
        this.columnDes = columnDes;
        this.flagDiff = flagDiff;
        this.flagNeedTranslate = flagNeedTranslate;
        this.dictionaryData = dictionaryData;
        this.dictionaryParams = dictionaryParams;
    }
}
