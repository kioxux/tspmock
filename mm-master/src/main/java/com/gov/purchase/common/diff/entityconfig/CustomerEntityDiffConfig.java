package com.gov.purchase.common.diff.entityconfig;

import com.gov.purchase.common.diff.DiffEntityInfo;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/22 10:45
 */
@Data
@Component
public class CustomerEntityDiffConfig {

    @Resource
    DictionaryParamsConfig dictionaryParamsConfig;

    public List<DiffEntityInfo> customerConfigInit() {
        return Arrays.asList(
                new DiffEntityInfo("client", "client", String.class, "CLIENT", "加盟商", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusSite", "cusSite", String.class, "CUS_SITE", "地点", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusSelfCode", "cusSelfCode", String.class, "CUS_SELF_CODE", "供应商自编码", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusCode", "cusCode", String.class, "CUS_CODE", "客户编码", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusMatchStatus", "cusMatchStatus", String.class, "CUS_MATCH_STATUS", "匹配状态", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.PRO_MATCH_STATUS),
                new DiffEntityInfo("cusPym", "cusPym", String.class, "CUS_PYM", "助记码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusName", "cusName", String.class, "CUS_NAME", "客户名称", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusCreditCode", "cusCreditCode", String.class, "CUS_CREDIT_CODE", "统一社会信用代码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusCreditDate", "cusCreditDate", String.class, "CUS_CREDIT_DATE", "营业执照期限", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusClass", "cusClass", String.class, "CUS_CLASS", "客户分类", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.CUS_CLASS),
                new DiffEntityInfo("cusLegalPerson", "cusLegalPerson", String.class, "CUS_LEGAL_PERSON", "法人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusRegAdd", "cusRegAdd", String.class, "CUS_REG_ADD", "注册地址", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusStatus", "cusStatus", String.class, "CUS_STATUS", "客户状态", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode(), CommonEnum.DictionaryStaticData.CUS_STATUS),
                new DiffEntityInfo("cusLicenceNo", "cusLicenceNo", String.class, "CUS_LICENCE_NO", "许可证编号", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusLicenceDate", "cusLicenceDate", String.class, "CUS_LICENCE_DATE", "发证日期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusLicenceValid", "cusLicenceValid", String.class, "CUS_LICENCE_VALID", "发证有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusScope", "cusScope", String.class, "CUS_SCOPE", "生产或经营范围", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusNoSale", "cusNoSale", String.class, "CUS_NO_SALE", "禁止销售", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.NO_YES),
                new DiffEntityInfo("cusNoReturn", "cusNoReturn", String.class, "CUS_NO_RETURN", "禁止退货", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.NO_YES),
                new DiffEntityInfo("cusPayTerm", "cusPayTerm", String.class, "CUS_PAY_TERM", "销售收款条件", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DB.getCode(), CommonEnum.DictionaryData.PAYMENT_TYPE),
                new DiffEntityInfo("cusBussinessContact", "cusBussinessContact", String.class, "CUS_BUSSINESS_CONTACT", "业务联系人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusContactTel", "cusContactTel", String.class, "CUS_CONTACT_TEL", "联系人电话", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusDeliveryAdd", "cusDeliveryAdd", String.class, "CUS_DELIVERY_ADD", "收货地址", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusCreditFlag", "cusCreditFlag", String.class, "CUS_CREDIT_FLAG", "是否信用管理", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.NO_YES),
                new DiffEntityInfo("cusCreditQuota", "cusCreditQuota", BigDecimal.class, "CUS_CREDIT_QUOTA", "信用额度", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusCreditCheck", "cusCreditCheck", String.class, "CUS_CREDIT_CHECK", "信用检查点", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusBankCode", "cusBankCode", String.class, "CUS_BANK_CODE", "银行代码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusBankName", "cusBankName", String.class, "CUS_BANK_NAME", "银行名称", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusAccountPerson", "cusAccountPerson", String.class, "CUS_ACCOUNT_PERSON", "账户持有人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusBankAccount", "cusBankAccount", String.class, "CUS_BANK_ACCOUNT", "银行账号", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusPayMode", "cusPayMode", String.class, "CUS_PAY_MODE", "支付方式", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.SUP_PAY_MODE),
                new DiffEntityInfo("cusCreditAmt", "cusCreditAmt", String.class, "CUS_CREDIT_AMT", "铺底授信额度", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusQua", "cusQua", String.class, "CUS_QUA", "质量负责人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusPictureFrwts", "cusPictureFrwts", String.class, "CUS_PICTURE_FRWTS", "法人委托书图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusPictureGsp", "cusPictureGsp", String.class, "CUS_PICTURE_GSP", "GSP证书图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusPictureGmp", "cusPictureGmp", String.class, "CUS_PICTURE_GMP", "GMP证书图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusPictureShtxd", "cusPictureShtxd", String.class, "CUS_PICTURE_SHTXD", "随货同行单图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusPictureQt", "cusPictureQt", String.class, "CUS_PICTURE_QT", "其他图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusPictureSpxkz", "cusPictureSpxkz", String.class, "CUS_PICTURE_SPXKZ", "食品许可证书图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusSpxkz", "cusSpxkz", String.class, "CUS_SPXKZ", "食品许可证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusSpxkzDate", "cusSpxkzDate", String.class, "CUS_SPXKZ_DATE", "食品许可证有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusPictureYlqxxkz", "cusPictureYlqxxkz", String.class, "CUS_PICTURE_YLQXXKZ", "医疗器械经营许可证图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYlqxxkz", "cusYlqxxkz", String.class, "CUS_YLQXXKZ", "医疗器械经营许可证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYlqxxkzDate", "cusYlqxxkzDate", String.class, "CUS_YLQXXKZ_DATE", "医疗器械经营许可证有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusGmp", "cusGmp", String.class, "CUS_GMP", "GMP证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusGmpDate", "cusGmpDate", String.class, "CUS_GMP_DATE", "GMP有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusPictureYljgxkz", "cusPictureYljgxkz", String.class, "CUS_PICTURE_YLJGXKZ", "医疗机构执业许可证图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYljgxkz", "cusYljgxkz", String.class, "CUS_YLJGXKZ", "医疗机构执业许可证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYljgxkzDate", "cusYljgxkzDate", String.class, "CUS_YLJGXKZ_DATE", "医疗机构执业许可证书有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusGsp", "cusGsp", String.class, "CUS_GSP", "GSP证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusGspDates", "cusGspDates", String.class, "CUS_GSP_DATES", "GSP有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusCreateDate", "cusCreateDate", String.class, "CUS_CREATE_DATE", "创建日期", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusZczb", "cusZczb", String.class, "CUS_ZCZB", "注册资本", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusCkdz", "cusCkdz", String.class, "CUS_CKDZ", "仓库地址", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYwy", "cusYwy", String.class, "CUS_YWY", "业务员（销售）", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYwysfz", "cusYwysfz", String.class, "CUS_YWYSFZ", "业务员（采购员）的身份证号码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYwysqrq", "cusYwysqrq", String.class, "CUS_YWYSQRQ", "业务员（采购员）授权日期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYwyjsrq", "cusYwyjsrq", String.class, "CUS_YWYJSRQ", "业务员（采购员）结束日期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusShr", "cusShr", String.class, "CUS_SHR", "收货人/提货人姓名", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusShrsfz", "cusShrsfz", String.class, "CUS_SHRSFZ", "收货人/提货人身份证号", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusShrsqrq", "cusShrsqrq", String.class, "CUS_SHRSQRQ", "收货人/提货人授权日期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusShrjsrq", "cusShrjsrq", String.class, "CUS_SHRJSRQ", "收货人/提货人结束日期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusRemarks", "cusRemarks", String.class, "CUS_REMARKS", "备注", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusQyfzr", "cusQyfzr", String.class, "CUS_QYFZR", "企业负责人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusJyfs", "cusJyfs", String.class, "CUS_JYFS", "经营方式", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusKhxkzh", "cusKhxkzh", String.class, "CUS_KHXKZH", "开户许可证号", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusSh", "cusSh", String.class, "CUS_SH", "税号", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusDhhm", "cusDhhm", String.class, "CUS_DHHM", "电话号码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusFplx", "cusFplx", String.class, "CUS_FPLX", "发票类型”及其选项（增值税专用发票、普通发票或普票）", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYlqxbaz", "cusYlqxbaz", String.class, "CUS_YLQXBAZ", "医疗器械备案证", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYlqxlb", "cusYlqxlb", String.class, "CUS_YLQXLB", "医疗器械类别”（选项：一类、二类、三类）", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusZbxyyxq", "cusZbxyyxq", String.class, "CUS_ZBXYYXQ", "质保协议期限", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusGxhtyxq", "cusGxhtyxq", String.class, "CUS_GXHTYXQ", "购销合同期限", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusFrwts", "cusFrwts", String.class, "CUS_FRWTS", "法人委托书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusFrwtsxq", "cusFrwtsxq", String.class, "CUS_FRWTSXQ", "法人委托书效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusKhqstp", "cusKhqstp", String.class, "CUS_KHQSTP", "客户签收图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYsfs", "cusYsfs", String.class, "CUS_YSFS", "运输方式", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusYssc", "cusYssc", String.class, "CUS_YSSC", "运输时长", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusGspgklx", "cusGspgklx", String.class, "CUS_GSPGKLX", "GSP控制类型”及其选项（批发企业/医疗机构/连锁药店/其他）", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("cusLicenceImg", "cusLicenceImg", String.class, "CUS_LICENCE_IMG", "经营许可证图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode())
        );
    }
}
