package com.gov.purchase.common.utils;

import com.gov.purchase.utils.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.19
 */
public class ListUtils {

    /**
     * 取最大自编码
     *
     * @param codeList
     * @return
     */
    public static String getMaxCode(List<String> codeList) {
        List<Map<String, Object>> sortCodeList = new ArrayList<>();
        for (String code : codeList) {
            if (StringUtils.isBlank(code)) {
                continue;
            }
            Map<String, Object> map = new HashMap<>();
            // 自编码
            map.put("selfCode", code);
            // 数字
            StringBuffer number = new StringBuffer("");
            String[] strArray = code.split("");
            for (int i = 0; i < strArray.length; i++) {
                if (NumberUtils.isNumber(strArray[i])) {
                    number.append(strArray[i]);
                }
            }
            if (StringUtils.isBlank(number.toString())) {
                continue;
            }
            int num = NumberUtils.toInt(number.toString(), -1);
            if (num != -1) {
                map.put("num", NumberUtils.toInt(number.toString(), -1));
            }
            String letter = code.replaceAll("([1-9]+[0-9]*|0)(\\.[\\d]+)?", "");
            if (StringUtils.isNotBlank(letter)) {
                // 非数字部分
                map.put("letter", code.replaceAll("([1-9]+[0-9]*|0)(\\.[\\d]+)?", ""));
            }
            sortCodeList.add(map);
        }
        if (org.springframework.util.CollectionUtils.isEmpty(sortCodeList)) {
            return null;
        }
        Collections.sort(sortCodeList, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                int result = 0;
                String letter1 = o1.get("letter") == null ? "" : o1.get("letter").toString();
                String letter2 = o2.get("letter") == null ? "" : o2.get("letter").toString();
                result = letter1.compareTo(letter2);
                if (result != 0)
                    return -result;
                int num1 = o1.get("num") == null ? -1 : NumberUtils.toInt(o1.get("num").toString(), -1);
                int num2 = o2.get("num") == null ? -1 : NumberUtils.toInt(o2.get("num").toString(), -1);
                result = num1 - num2;
                if (result != 0)
                    return -result;
                return result;
            }
        });
        return sortCodeList.get(0).get("selfCode").toString();
    }

    public static String getMaxCode(String maxCode) {
        if (StringUtils.isBlank(maxCode)) {
            return null;
        }
        long num = NumberUtils.toLong(maxCode, -1);
        if (num != -1) {
            num++;
            String result = StringUtils.leftPad(String.valueOf(num), maxCode.length(), "0");
            return result;
        } else {
            String ext = maxCode.replaceAll(".*[^\\d](?=(\\d+))", "");
            String left = maxCode.replaceAll(ext, "");
            num = NumberUtils.toLong(ext, -1);
            if (num == -1) {
                return null;
            } else {
                num++;
                return left.concat(StringUtils.leftPad(String.valueOf(num), ext.length(), "0"));
            }
        }
    }
}
