package com.gov.purchase.common.utils;

import com.gov.purchase.utils.StringUtils;

import java.math.BigDecimal;

public class DecimalUtils {

    public static BigDecimal stripTrailingZeros(BigDecimal num) {
        num = num == null ? BigDecimal.ZERO : num;
        return num.stripTrailingZeros();
    }

    /**
     * 类型转换
     *
     * @param object
     * @return
     */
    public static BigDecimal valueOf(Object object) {
        if (object == null) {
            return null;
        }
        if (StringUtils.isBlank(object.toString())) {
            return null;
        }
        return new BigDecimal(object.toString());
    }

    /**
     * 乘法
     *
     * @param decimals
     * @return
     */
    public static BigDecimal multiply(BigDecimal... decimals) {
        if (decimals == null || decimals.length == 0) {
            return BigDecimal.ZERO;
        }
        BigDecimal result = BigDecimal.ZERO;
        for (BigDecimal decimal : decimals) {
            if (decimal != null) {
                result = result.multiply(decimal);
            }
        }
        return result;
    }

    /**
     * 数值相加
     *
     * @param decimals
     * @return
     */
    public static BigDecimal add(BigDecimal... decimals) {
        if (decimals == null || decimals.length == 0) {
            return BigDecimal.ZERO;
        }
        BigDecimal result = BigDecimal.ZERO;
        for (BigDecimal decimal : decimals) {
            if (decimal != null) {
                result = result.add(decimal);
            }
        }
        return result;
    }

    /**
     * 数值相减
     *
     * @param bigDecimal1
     * @param bigDecimal2
     * @return
     */
    public static BigDecimal subtract(BigDecimal bigDecimal1, BigDecimal bigDecimal2) {
        if (bigDecimal1 == null) {
            bigDecimal1 = BigDecimal.ZERO;
        }
        if (bigDecimal2 == null) {
            bigDecimal2 = BigDecimal.ZERO;
        }
        return bigDecimal1.subtract(bigDecimal2);
    }
}
