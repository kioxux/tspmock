package com.gov.purchase.common.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author staxc
 * @Date 2020/10/23 9:52
 * @desc 发票处理配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "invoice")
public class InvoiceConfig {

    /**
     * 身份证
     */
    private String identity;

    /**
     * 开票请求地址
     */
    private String invoiceReqUrl;

    /**
     * 开票结果查询地址(fpqqlsh 和 orderno 只取其一作入参查询，不能同时作入参查询)
     * 入参   fpqqlsh--通过发票请求流水号查询
     * orderno--通过订单号查询
     */
    private String invoiceResUrl;

    /**
     * 下载前缀
     */
    private String downloadPath;

    /**
     * 短信签名
     */
    private String sgin;

    /**
     * 销方企业税号
     */
    private String saletaxnum;

    /**
     * 销方企业地址
     */
    private String saleaddress;

    /**
     * 销方企业银行开户行及账号
     */
    private String saleaccount;

    /**
     * 销方企业电话
     */
    private String salephone;

    /**
     * 开票员
     */
    private String clerk;

    /**
     * 推送手机号
     */
    private String phone;

}
