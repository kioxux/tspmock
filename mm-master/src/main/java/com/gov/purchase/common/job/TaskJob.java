package com.gov.purchase.common.job;

import com.gov.purchase.entity.GaiaMaterialDocLogHWithBLOBs;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.mapper.GaiaPoHeaderMapper;
import com.gov.purchase.mapper.GaiaSdStockMapper;
import com.gov.purchase.module.base.service.MaterialDocService;
import com.gov.purchase.module.priceCompare.service.PriceCompareService;
import com.gov.purchase.module.purchase.service.InvoicePayingService;
import com.gov.purchase.module.store.service.StoreProductService;
import com.gov.purchase.utils.JsonUtils;
import com.gov.purchase.utils.StringUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.12
 */
@Slf4j
@Component
public class TaskJob {

    @Resource
    private InvoicePayingService payingInvoiceService;

    @Autowired
    private StoreProductService storeProductService;

    @Resource
    private MaterialDocService materialDocService;

    @Resource
    private GaiaPoHeaderMapper gaiaPoHeaderMapper;

    @Resource
    private GaiaSdStockMapper gaiaSdStockMapper;

    @Resource
    private PriceCompareService priceCompareService;

    /**
     * 门店付款定时任务
     *
     * @throws Exception
     */
    @XxlJob("payCallbackHandler")
    public ReturnT<String> payCallback(String param) throws Exception {
        XxlJobHelper.log("门店付款 定时任务开始 ");
        payingInvoiceService.payCallback();
        XxlJobHelper.log("门店付款 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 生成发票 定时任务
     *
     * @throws Exception
     */
    @XxlJob("initInvoiceHandler")
    public ReturnT<String> initInvoice(String param) throws Exception {
        XxlJobHelper.log("生成发票 定时任务开始 ");
        payingInvoiceService.initInvoice();
        XxlJobHelper.log("生成发票 定时任务结束 ");
        return ReturnT.SUCCESS;
    }


    /**
     * 商品调价 定时任务
     *
     * @throws Exception
     */
    @XxlJob("saveTimingHandler")
    public ReturnT<String> saveTimingHandler(String param) throws Exception {
        XxlJobHelper.log("商品调价 定时任务开始 ");
        storeProductService.updateList();
        XxlJobHelper.log("商品调价 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 物料凭证定时任务
     *
     * @param param
     * @return
     * @throws Exception
     */
    @XxlJob("materialDocBatchTaskHandler")
    public ReturnT<String> materialDocBatchTask(String param) throws Exception {
        XxlJobHelper.log("物料凭证 定时任务开始 ");
        log.info("物料凭证 定时任务开始");
        List<GaiaMaterialDocLogHWithBLOBs> list = materialDocService.getMaterialDocBatchTask();
        if (CollectionUtils.isEmpty(list)) {
            log.info("物料凭证 定时任务结束 _ 无数据");
            XxlJobHelper.log("物料凭证 定时任务结束 ");
            return ReturnT.SUCCESS;
        }
        log.info("物料凭证 定时任务 _ {}条", list.size());
        for (int i = 0; i < list.size(); i++) {
            GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs = list.get(i);
            try {
                FeignResult feignResult = materialDocService.insertMaterialDocBatchTask(gaiaMaterialDocLogHWithBLOBs);
                if (feignResult.getCode() == 0) {
                    log.info("物料凭证 定时任务成功 _ 第{}条", i);
                    materialDocService.taskSucess(gaiaMaterialDocLogHWithBLOBs.getGuid());
                    XxlJobHelper.log("物料凭证定时任务执行数据_{}_成功", gaiaMaterialDocLogHWithBLOBs.getGuid());
                    continue;
                } else {
                    log.info("物料凭证 定时任务失败 _ 第{}条{}", i, JsonUtils.beanToJson(feignResult));
                    materialDocService.taskError(gaiaMaterialDocLogHWithBLOBs.getGuid(), JsonUtils.beanToJson(feignResult));
                    XxlJobHelper.log("物料凭证定时任务执行数据_{}_失败_{}", gaiaMaterialDocLogHWithBLOBs.getGuid(), JsonUtils.beanToJson(feignResult));
                    continue;
                }
            } catch (Exception e) {
                log.info("物料凭证 定时任务失败 _ 第{}条{}", i, e.getMessage());
                e.printStackTrace();
                try {
                    materialDocService.taskError(gaiaMaterialDocLogHWithBLOBs.getGuid(), JsonUtils.beanToJson(e));
                } catch (Exception ex) {
                    materialDocService.taskError(gaiaMaterialDocLogHWithBLOBs.getGuid(), ex.getMessage());
                }
                XxlJobHelper.log("物料凭证定时任务执行数据_{}_失败", gaiaMaterialDocLogHWithBLOBs.getGuid());
                continue;
            }
        }
        log.info("物料凭证 定时任务结束");
        XxlJobHelper.log("物料凭证 定时任务结束 ");
        return ReturnT.SUCCESS;
    }


    /**
     * 更新采购订单状态定时任务
     *
     * @param param
     * @return
     * @throws Exception
     */
    @XxlJob("closePoTaskHandler")
    public ReturnT<String> closePo(String param) throws Exception {
        XxlJobHelper.log("[更新采购订单状态] 定时任务开始 ");
        gaiaPoHeaderMapper.closePo();
        XxlJobHelper.log("[更新采购订单状态] 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * GAIA_SD_STOCK,GAIA_SD_STOCK_BATCH 库存表备份定时任务
     *
     * @param param
     * @return
     * @throws Exception
     */
    @XxlJob("gaiaSdStockBatchBackups")
    public ReturnT<String> gaiaSdStockBatchBackups(String param) throws Exception {
        XxlJobHelper.log("[GAIA_SD_STOCK,GAIA_SD_STOCK_BATCH 库存表备份] 定时任务开始 ");
        // 年份
        LocalDate lastMonth = LocalDate.now().minusMonths(1);
        String year = String.valueOf(lastMonth.getYear());
        String month = StringUtils.leftPad(String.valueOf(lastMonth.getMonth().getValue()), 2, "0");
        gaiaSdStockMapper.gaiaSdStockBackups(year, month);
        gaiaSdStockMapper.gaiaSdStockBatchBackups(year, month);
        XxlJobHelper.log("[GAIA_SD_STOCK,GAIA_SD_STOCK_BATCH 库存表备份] 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

    /**
     * 比价失效(0点以后执行)
     * @param param
     * @return
     * @throws Exception
     */
    @XxlJob("priceCompareInvalid")
    public ReturnT<String> priceCompareInvalid(String param) throws Exception {
        XxlJobHelper.log("[更新比价失效状态] 定时任务开始 ");
        priceCompareService.updateCompareInvalid();
        XxlJobHelper.log("[更新比价失效状态] 定时任务结束 ");
        return ReturnT.SUCCESS;
    }

}
