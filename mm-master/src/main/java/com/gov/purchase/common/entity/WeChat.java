package com.gov.purchase.common.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.04.08
 */
@Data
@Component
@ConfigurationProperties(prefix = "wechat")
public class WeChat {
    // 关注推文内容
    private String pushContent;
    // 关注推文url
    private String pushUrl;

    // 订阅URL
    private String subscribeUrl;

}
