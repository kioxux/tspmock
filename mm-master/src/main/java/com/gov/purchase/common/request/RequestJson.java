package com.gov.purchase.common.request;


import java.lang.annotation.*;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestJson {
    String value();

    boolean required() default true;

    String defaultValue() default "";

    String name() default "";
}
