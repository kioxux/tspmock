package com.gov.purchase.common.kylin;

import com.alibaba.fastjson.JSONObject;
import com.gov.purchase.common.kylin.entity.ColumnMetas;
import com.gov.purchase.common.kylin.entity.KylinQuery;
import com.gov.purchase.common.kylin.entity.KylinResult;
import com.gov.purchase.config.SpringContextUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.apache.commons.text.CaseUtils;

import javax.annotation.Resource;
import java.io.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.12.21
 */
@Slf4j
@Component
public class KylinHttpBasic {

    private static String auth;
    //    private static final String DEFAULT_ENCODING = "UTF-8";
//    private static String baseURL;
//    private static String project;
//    private String kylin;
    @Value("${kylin.UserName}")
    private String userName;
    @Value("${kylin.Password}")
    private String password;
    @Value("${kylin.Project}")
    private String project;
    @Value("${kylin.ExcuteUrl}")
    private String excuteUrl;

    private void login() throws IOException {
        String method = "POST";
        String para = "/user/authentication";
        byte[] key = (userName + ":" + password).getBytes();
        auth = new sun.misc.BASE64Encoder().encode(key);
//        excute(para, method, null);
    }

    public <T> List<T> queryList(KylinQuery kylinQuery, Class<T> c) throws InstantiationException, IllegalAccessException, IOException {
        login();
        kylinQuery.setProject(project);
        log.info("kylin参数:{}", JsonUtils.beanToJson(kylinQuery));
        String method = "POST";
        String para = "/query";
        String result = excute(para, method, JsonUtils.beanToJson(kylinQuery));
        log.info("kylin结果:{}", result);
        return excuteResult(result, c);
    }

    private <T> List<T> excuteResult(String result, Class<T> c) throws IllegalAccessException, InstantiationException {
        List<T> resultList = new ArrayList<>();
        if (!StringUtils.isNotBlank(result)) {
            return resultList;
        }
        KylinResult kylinResult = JsonUtils.jsonToBean(result, KylinResult.class);
        if (kylinResult.getResults() == null || kylinResult.getResults().length == 0) {
            return resultList;
        }
        for (int i = 0; i < kylinResult.getResults().length; i++) {
            Object[] row = kylinResult.getResults()[i];
            if (row == null || row.length == 0) {
                continue;
            }
            T t = c.newInstance();
            for (int j = 0; j < row.length; j++) {
                Object data = row[j];
                ColumnMetas columnMetas = kylinResult.getColumnMetas().get(j);
                String col = CaseUtils.toCamelCase(columnMetas.getName(), false, '_');
                setFieldValueByFieldName(col, t, data);
            }
            resultList.add(t);
        }
        return resultList;
    }

    private void setFieldValueByFieldName(String fieldName, Object object, Object value) {
        try {
            if (value == null || StringUtils.isBlank(value.toString())) {
                return;
            }
            if ("null".toLowerCase().equals(value.toString().toLowerCase())) {
                return;
            }
            // 获取obj类的字节文件对象
            Class c = object.getClass();
            // 获取该类的成员变量
            Field f = c.getDeclaredField(fieldName);
            // 取消语言访问检查
            f.setAccessible(true);
            // 给变量赋值
            if (f.getType().toString().endsWith("BigDecimal")) {
                f.set(object, new BigDecimal(value.toString()));
            } else {
                f.set(object, value.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException(ResultEnum.UNKNOWN_ERROR);
        }
    }

//    private String getKylin() throws IOException {
//        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("kylin-" + SpringContextUtil.getActiveProfile() + ".properties");
//        log.error("读取Kylin配置文件", "kylin-" + SpringContextUtil.getActiveProfile() + ".properties");
//
//        Properties prop = new Properties();
//        KylinSqlProperties properties = new KylinSqlProperties();
//        try {
//            prop.load(new InputStreamReader(inputStream, DEFAULT_ENCODING));
//            baseURL = prop.getProperty("ExcuteUrl");
//            project = prop.getProperty("Project");
//            return JsonUtils.beanToJson(prop);
//        } catch (IOException e) {
//            e.printStackTrace();
//            log.error("读取Kylin配置文件异常", e);
//            throw e;
//        } finally {
//            if (prop != null) {
//                prop.clear();
//            }
//        }
//    }

    private String excute(String para, String method, String body) {
        StringBuilder out = new StringBuilder();
        try {
            URL url = new URL(excuteUrl + para);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + auth);
            connection.setRequestProperty("Content-Type", "application/json");
            if (body != null) {
                byte[] outputInBytes = body.getBytes("UTF-8");
                OutputStream os = connection.getOutputStream();
                os.write(outputInBytes);
                os.close();
            }
            InputStream content = (InputStream) connection.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = in.readLine()) != null) {
                out.append(line);
            }
            in.close();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out.toString();
    }

}
