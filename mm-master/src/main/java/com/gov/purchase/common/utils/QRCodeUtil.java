package com.gov.purchase.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class QRCodeUtil {

//    public static void main(String[] args) throws Exception {
//        String appid = "wxdf3398c5d72246e1";
//        String secret = "77473ed7aaa56e0f376cb9433eee4c2f";
//        String accesstoken = getAccessToken(appid,secret);
//        System.out.print(accesstoken);
//        String param1 = "a";
//        String param2 = "b";
//        String param3 = "c";
//        String param = param1+","+param2+","+param3;
//        String qrCode = createQrcode(accesstoken,param);
//        System.out.print(qrCode);
//    }

    public String getAccessToken(String appid, String secret) {
        try {
            String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + secret;
            log.info("生成token参数{}", url);
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String resStr = EntityUtils.toString(entity, "UTF-8");
            log.info("生成生成token返回值{}", resStr);
            httpClient.close();
            response.close();
            String accesstoken = JSONObject.parseObject(resStr).getString("access_token");
            return accesstoken;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String createQrcode(String accessToken, String param) {
        try {
            String url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" + accessToken;
            String entity = "{\"action_name\": \"QR_LIMIT_STR_SCENE\", \"action_info\": {\"scene\": {\"scene_str\": \"" + param + "\"}}}";
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);
            StringEntity se = new StringEntity(entity, "UTF-8");
            se.setContentType("application/json");
            httpPost.setEntity(se);
            CloseableHttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity1 = response.getEntity();
            String resStr = EntityUtils.toString(entity1, "UTF-8");
            log.info("生成生成二维码返回值{}", resStr);
            httpClient.close();
            response.close();
            String ticket = JSONObject.parseObject(resStr).getString("ticket");
            String qrcodeURL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" + ticket;
            return qrcodeURL;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}