package com.gov.purchase.common.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.31
 */
@Data
@Component
@ConfigurationProperties(prefix = "apppay")
public class AppPay {

    private String appId;

    private String appKey;

    private String mid;

    private String tid;

    private String billNoPre;

    private String subAppId;

    /**
     * 微信接口
     */
    private String wechatUrl;

    /**
     * 支付宝接口
     */
    private String alipayUrl;

    /**
     * 云闪付接口
     */
    private String unionpayUrl;

    /**
     * 回调接口
     */
    private String notifyUrl;

    /**
     * 查询接口
     */
    private String queryUlr;

    /**
     * 过期时间
     */
    private Integer expireTime;

    /**
     * 首月单价
     */
    private Integer firstMonthPrice;

    /**
     * 月单价
     */
    private Integer monthPrice;

}
