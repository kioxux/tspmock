package com.gov.purchase.common.diff;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021/4/15 13:21
 */
@Data
@AllArgsConstructor
public class DiffEntityReuslt {
    /**
     * 属性名(修改前)
     */
    private String propertyFrom;
    /**
     * 属性名(修改后)
     */
    private String propertyTo;
    /**
     * 修改前的值
     */
    private String valueFrom;
    /**
     * 修改后的值
     */
    private String valueTo;
    /**
     * 数据库字段名
     */
    private String column;
    /**
     * 数据库字段描述
     */
    private String columnDes;
}
