package com.gov.purchase.common.kylin.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.12.21
 */
@Data
public class ColumnMetas {
    private Integer isNullable;
    private Integer displaySize;
    private String label;
    private String name;
    private String schemaName;
    private String catelogName;
    private String tableName;
    private Integer precision;
    private Integer scale;
    private Integer columnType;
    private String columnTypeName;
    private boolean autoIncrement;
    private boolean caseSensitive;
    private boolean currency;
    private boolean definitelyWritable;
    private boolean searchable;
    private boolean signed;
    private boolean readOnly;
    private boolean writable;
}
