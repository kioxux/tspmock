package com.gov.purchase.common.diff.entityconfig;

import com.gov.purchase.common.diff.DiffEntityInfo;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/22 10:42
 */
@Data
@Component
public class SupplierEntityDiffConfig {

    @Resource
    DictionaryParamsConfig dictionaryParamsConfig;


    public List<DiffEntityInfo> supplierConfigInit() {
        return Arrays.asList(
                new DiffEntityInfo("client", "client", String.class, "CLIENT", "加盟商", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSite", "supSite", String.class, "SUP_SITE", "地点", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSelfCode", "supSelfCode", String.class, "SUP_SELF_CODE", "供应商自编码", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supCode", "supCode", String.class, "SUP_CODE", "供应商编码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supMatchStatus", "supMatchStatus", String.class, "SUP_MATCH_STATUS", "匹配状态", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.PRO_MATCH_STATUS),
                new DiffEntityInfo("supPym", "supPym", String.class, "SUP_PYM", "助记码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supName", "supName", String.class, "SUP_NAME", "供应商名称", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supCreditCode", "supCreditCode", String.class, "SUP_CREDIT_CODE", "统一社会信用代码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supCreditDate", "supCreditDate", String.class, "SUP_CREDIT_DATE", "营业执照期限", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supClass", "supClass", String.class, "SUP_CLASS", "供应商分类", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.SUP_CLASS),
                new DiffEntityInfo("supLegalPerson", "supLegalPerson", String.class, "SUP_LEGAL_PERSON", "法人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supRegAdd", "supRegAdd", String.class, "SUP_REG_ADD", "注册地址", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supStatus", "supStatus", String.class, "SUP_STATUS", "供应商状态", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.SUP_STATUS),
                new DiffEntityInfo("supLicenceNo", "supLicenceNo", String.class, "SUP_LICENCE_NO", "许可证编号", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supLicenceDate", "supLicenceDate", String.class, "SUP_LICENCE_DATE", "发证日期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supLicenceValid", "supLicenceValid", String.class, "SUP_LICENCE_VALID", "发证有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supScope", "supScope", String.class, "SUP_SCOPE", "生产或经营范围", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supNoPurchase", "supNoPurchase", String.class, "SUP_NO_PURCHASE", "禁止采购", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.NO_YES),
                new DiffEntityInfo("supNoSupplier", "supNoSupplier", String.class, "SUP_NO_SUPPLIER", "禁止退厂", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.NO_YES),
                new DiffEntityInfo("supPayTerm", "supPayTerm", String.class, "SUP_PAY_TERM", "采购付款条件", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DB.getCode(), CommonEnum.DictionaryData.PAYMENT_TYPE),
                new DiffEntityInfo("supBussinessContact", "supBussinessContact", String.class, "SUP_BUSSINESS_CONTACT", "业务联系人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supContactTel", "supContactTel", String.class, "SUP_CONTACT_TEL", "联系人电话", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supLeadTime", "supLeadTime", String.class, "SUP_LEAD_TIME", "送货前置期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supBankCode", "supBankCode", String.class, "SUP_BANK_CODE", "银行代码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supBankName", "supBankName", String.class, "SUP_BANK_NAME", "银行名称", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supAccountPerson", "supAccountPerson", String.class, "SUP_ACCOUNT_PERSON", "账户持有人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supBankAccount", "supBankAccount", String.class, "SUP_BANK_ACCOUNT", "银行账号", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supPayMode", "supPayMode", String.class, "SUP_PAY_MODE", "支付方式", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.SUP_PAY_MODE),
                new DiffEntityInfo("supCreditAmt", "supCreditAmt", String.class, "SUP_CREDIT_AMT", "铺底授信额度", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supMixAmt", "supMixAmt", BigDecimal.class, "SUP_MIX_AMT", "起订金额", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supFrwts", "supFrwts", String.class, "SUP_FRWTS", "法人委托书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supFrwtsxq", "supFrwtsxq", String.class, "SUP_FRWTSXQ", "法人委托书有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supZlfzr", "supZlfzr", String.class, "SUP_ZLFZR", "质量负责人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supCkdz", "supCkdz", String.class, "SUP_CKDZ", "仓库地址", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supLoginAdd", "supLoginAdd", String.class, "SUP_LOGIN_ADD", "网页登录地址", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supLoginUser", "supLoginUser", String.class, "SUP_LOGIN_USER", "登录用户名", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supLoginPassword", "supLoginPassword", String.class, "SUP_LOGIN_PASSWORD", "登录密码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSysParm", "supSysParm", String.class, "SUP_SYS_PARM", "系统参数", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supDeliveryMode", "supDeliveryMode", String.class, "SUP_DELIVERY_MODE", "配送方式", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.DIC.getCode(), CommonEnum.DictionaryStaticData.STO_DELIVERY_MODE),
                new DiffEntityInfo("supCarrier", "supCarrier", String.class, "SUP_CARRIER", "承运单位", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supPictureShtxd", "supPictureShtxd", String.class, "SUP_PICTURE_SHTXD", "随货同行单图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supQuality", "supQuality", String.class, "SUP_QUALITY", "质量保证协议", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supQualityDate", "supQualityDate", String.class, "SUP_QUALITY_DATE", "质保协议有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supGsp", "supGsp", String.class, "SUP_GSP", "GSP证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supGspDate", "supGspDate", String.class, "SUP_GSP_DATE", "GSP有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supGmp", "supGmp", String.class, "SUP_GMP", "GMP证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supGmpDate", "supGmpDate", String.class, "SUP_GMP_DATE", "GMP有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supPictureFrwts", "supPictureFrwts", String.class, "SUP_PICTURE_FRWTS", "法人委托书图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supPictureGsp", "supPictureGsp", String.class, "SUP_PICTURE_GSP", "GSP证书图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supPictureGmp", "supPictureGmp", String.class, "SUP_PICTURE_GMP", "GMP证书图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supPictureQt", "supPictureQt", String.class, "SUP_PICTURE_QT", "其他图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supDeliveryTimes", "supDeliveryTimes", String.class, "SUP_DELIVERY_TIMES", "运输时间", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supPictureScxkz", "supPictureScxkz", String.class, "SUP_PICTURE_SCXKZ", "生产许可证书图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supScxkz", "supScxkz", String.class, "SUP_SCXKZ", "生产许可证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supScxkzDate", "supScxkzDate", String.class, "SUP_SCXKZ_DATE", "生产许可证有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supPictureSpxkz", "supPictureSpxkz", String.class, "SUP_PICTURE_SPXKZ", "食品许可证书图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSpxkz", "supSpxkz", String.class, "SUP_SPXKZ", "食品许可证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSpxkzDate", "supSpxkzDate", String.class, "SUP_SPXKZ_DATE", "食品许可证有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supPictureYlqxxkz", "supPictureYlqxxkz", String.class, "SUP_PICTURE_YLQXXKZ", "医疗器械经营许可证图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supYlqxxkz", "supYlqxxkz", String.class, "SUP_YLQXXKZ", "医疗器械经营许可证书", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supYlqxxkzDate", "supYlqxxkzDate", String.class, "SUP_YLQXXKZ_DATE", "医疗器械经营许可证在有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supFrwtsSfz", "supFrwtsSfz", String.class, "SUP_FRWTS_SFZ", "法人委托书身份证", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supCloseDate", "supCloseDate", String.class, "SUP_CLOSE_DATE", "在途关闭天数", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supCreateDate", "supCreateDate", String.class, "SUP_CREATE_DATE", "创建日期", CommonEnum.NoYesStatusInteger.NO.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSampleSeal", "supSampleSeal", String.class, "SUP_SAMPLE_SEAL", "样章", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSampleSealImage", "supSampleSealImage", String.class, "SUP_SAMPLE_SEAL_IMAGE", "样章图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSampleTicket", "supSampleTicket", String.class, "SUP_SAMPLE_TICKET", "样票", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSampleTicketImage", "supSampleTicketImage", String.class, "SUP_SAMPLE_TICKET_IMAGE", "样票图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSampleMembrane", "supSampleMembrane", String.class, "SUP_SAMPLE_MEMBRANE", "样膜", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supSampleMembraneImage", "supSampleMembraneImage", String.class, "SUP_SAMPLE_MEMBRANE_IMAGE", "样膜图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supDah", "supDah", String.class, "SUP_DAH", "档案号", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supBeizhu", "supBeizhu", String.class, "SUP_BEIZHU", "备注", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supNdbgyxq", "supNdbgyxq", String.class, "SUP_NDBGYXQ", "年度报告有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supGxht", "supGxht", String.class, "SUP_GXHT", "购销合同", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supGxhtyxq", "supGxhtyxq", String.class, "SUP_GXHTYXQ", "购销合同有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supCwbm", "supCwbm", String.class, "SUP_CWBM", "财务编码", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supFplx", "supFplx", String.class, "SUP_FPLX", "发票类型", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supFzjg", "supFzjg", String.class, "SUP_FZJG", "发照机关", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supQyfzr", "supQyfzr", String.class, "SUP_QYFZR", "企业负责人", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supZczb", "supZczb", String.class, "SUP_ZCZB", "注册资本", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supGspgklx", "supGspgklx", String.class, "SUP_GSPGKLX", "GSP类型", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supDrlylqxbapz", "supDrlylqxbapz", String.class, "SUP_DRLYLQXBAPZ", "第二类医疗器械备案凭证", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supDrlylqxbapzDate", "supDrlylqxbapzDate", String.class, "SUP_DRLYLQXBAPZ_DATE", "第二类医疗器械备案凭证有效期", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supDrlylqxbapzImg", "supDrlylqxbapzImg", String.class, "SUP_DRLYLQXBAPZ_IMG", "第二类医疗器械备案凭证图片", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode()),
                new DiffEntityInfo("supCgy", "supCgy", String.class, "SUP_CGY", "采购员（供应商）", CommonEnum.NoYesStatusInteger.YES.getCode(), CommonEnum.NeedTranslate.NO.getCode())
        );
    }
}
