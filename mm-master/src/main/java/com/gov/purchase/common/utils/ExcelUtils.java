package com.gov.purchase.common.utils;

import lombok.Data;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Data
public class ExcelUtils {


    public ExcelUtils(String fileName) {
        this.fileName = fileName;
        // 创建HSSFWorkbook对象(excel的文档对象)
        this.workbook = new HSSFWorkbook();
        // 建立新的sheet对象（excel的表单）
        this.sheet = this.workbook.createSheet(fileName);
        // 字体
        setHeadFont();
        setTitleFont();
        setContentFont();
        // 样式
        setHeadStyle();
        setTitleStyle();
        setTitleStyleB();
        setTitleStyleC();
        setContentStyle();
        setContentRightStyle();
        setContentLeftStyle();
        setContentRightBlueStyle();
        setContentRightRedStyle();
    }

    private String fileName;
    private HSSFWorkbook workbook;
    private HSSFSheet sheet;
    // 表头字体
    private HSSFFont headFont;
    private HSSFFont titleFont;
    private HSSFFont contentFont;
    // 样式
    private HSSFCellStyle headStyle;
    private HSSFCellStyle titleStyle;
    private HSSFCellStyle titleStyleB;
    private HSSFCellStyle titleStyleC;
    private HSSFCellStyle contentStyle;
    private HSSFCellStyle contentRightStyle;
    private HSSFCellStyle contentLeftStyle;
    private HSSFCellStyle contentRightBlueStyle;
    private HSSFCellStyle contentRightRedStyle;

    private Integer defaultRowHeight;


    public void setHeadFont(Short height) {
        this.sheet.setDefaultRowHeight(height);
    }


    public void setHeadFont() {
        HSSFFont headFont = this.workbook.createFont();
        headFont.setFontHeightInPoints((short) 20);
        headFont.setFontName("微软雅黑");
        headFont.setBold(true);
        this.headFont = headFont;
    }

    public void setTitleFont() {
        HSSFFont titleFont = this.workbook.createFont();
        titleFont.setFontHeightInPoints((short) 10);
        titleFont.setFontName("微软雅黑");
        titleFont.setBold(true);
        this.titleFont = titleFont;
    }

    public void setContentFont() {
        HSSFFont contentFont = this.workbook.createFont();
        contentFont.setFontHeightInPoints((short) 10);
        contentFont.setFontName("微软雅黑");
        this.contentFont = contentFont;
    }

    private void setHeadStyle() {
        HSSFCellStyle headStyle = this.workbook.createCellStyle();
        headStyle.setFont(headFont);// 调用字体样式对象
        headStyle.setWrapText(true);
        headStyle.setBorderBottom(BorderStyle.THIN);//下边框
        headStyle.setBorderLeft(BorderStyle.THIN);//左边框
        headStyle.setBorderTop(BorderStyle.THIN);//上边框
        headStyle.setBorderRight(BorderStyle.THIN);//右边框
        headStyle.setAlignment(HorizontalAlignment.CENTER);
        headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        this.headStyle = headStyle;
    }

    private void setTitleStyle() {
        //第二行的样式
        HSSFCellStyle titleStyle = this.workbook.createCellStyle();
        titleStyle.setFont(this.titleFont);// 调用字体样式对象
        titleStyle.setWrapText(true);
        titleStyle.setBorderBottom(BorderStyle.THIN);//下边框
        titleStyle.setBorderLeft(BorderStyle.THIN);//左边框
        titleStyle.setBorderTop(BorderStyle.THIN);//上边框
        titleStyle.setBorderRight(BorderStyle.THIN);//右边框
        titleStyle.setAlignment(HorizontalAlignment.CENTER);
        titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        titleStyle.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
        titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        this.titleStyle = titleStyle;
    }

    private void setTitleStyleB() {
        HSSFCellStyle titleStyle = this.workbook.createCellStyle();
        titleStyle.setFont(this.titleFont);// 调用字体样式对象
        titleStyle.setWrapText(true);
        titleStyle.setBorderBottom(BorderStyle.THIN);//下边框
        titleStyle.setBorderLeft(BorderStyle.THIN);//左边框
        titleStyle.setBorderTop(BorderStyle.THIN);//上边框
        titleStyle.setBorderRight(BorderStyle.THIN);//右边框
        titleStyle.setAlignment(HorizontalAlignment.CENTER);
        titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        this.titleStyleB = titleStyle;
    }

    private void setTitleStyleC() {
        HSSFCellStyle titleStyle = this.workbook.createCellStyle();
        titleStyle.setFont(this.titleFont);// 调用字体样式对象
        titleStyle.setWrapText(true);
        titleStyle.setBorderBottom(BorderStyle.THIN);//下边框
        titleStyle.setBorderLeft(BorderStyle.THIN);//左边框
        titleStyle.setBorderTop(BorderStyle.THIN);//上边框
        titleStyle.setBorderRight(BorderStyle.THIN);//右边框
        titleStyle.setAlignment(HorizontalAlignment.RIGHT);
        titleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        this.titleStyleC = titleStyle;
    }


    private void setContentStyle() {
        HSSFCellStyle contentStyle = this.workbook.createCellStyle();
        contentStyle.setVerticalAlignment(VerticalAlignment.CENTER);//设置居中
        contentStyle.setAlignment(HorizontalAlignment.CENTER);//设置居中
        contentStyle.setFont(this.contentFont);// 调用字体样式对象
        contentStyle.setWrapText(true);
        contentStyle.setBorderBottom(BorderStyle.THIN);//下边框
        contentStyle.setBorderLeft(BorderStyle.THIN);//左边框
        contentStyle.setBorderTop(BorderStyle.THIN);//上边框
        contentStyle.setBorderRight(BorderStyle.THIN);//右边框
        this.contentStyle = contentStyle;
    }


    private void setContentRightStyle() {
        HSSFCellStyle style = this.workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);//设置居中
        style.setAlignment(HorizontalAlignment.RIGHT);//设置居中
        style.setFont(this.contentFont);// 调用字体样式对象
        style.setWrapText(true);
        style.setBorderBottom(BorderStyle.THIN);//下边框
        style.setBorderLeft(BorderStyle.THIN);//左边框
        style.setBorderTop(BorderStyle.THIN);//上边框
        style.setBorderRight(BorderStyle.THIN);//右边框
        this.contentRightStyle = style;
    }

    private void setContentLeftStyle() {
        HSSFCellStyle style = this.workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);//设置居中
        style.setAlignment(HorizontalAlignment.LEFT);//设置居中
        style.setFont(this.contentFont);// 调用字体样式对象
        style.setWrapText(true);
        style.setBorderBottom(BorderStyle.THIN);//下边框
        style.setBorderLeft(BorderStyle.THIN);//左边框
        style.setBorderTop(BorderStyle.THIN);//上边框
        style.setBorderRight(BorderStyle.THIN);//右边框
        this.contentLeftStyle = style;
    }

    private void setContentRightBlueStyle() {
        HSSFFont font = this.workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("微软雅黑");
        font.setColor(HSSFColor.BLUE.index);
        HSSFCellStyle style = this.workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);//设置居中
        style.setAlignment(HorizontalAlignment.RIGHT);//设置居中
        style.setFont(font);// 调用字体样式对象
        style.setWrapText(true);
        style.setBorderBottom(BorderStyle.THIN);//下边框
        style.setBorderLeft(BorderStyle.THIN);//左边框
        style.setBorderTop(BorderStyle.THIN);//上边框
        style.setBorderRight(BorderStyle.THIN);//右边框
        this.contentRightBlueStyle = style;
    }

    private void setContentRightRedStyle() {
        HSSFFont font = this.workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("微软雅黑");
        font.setColor(HSSFColor.RED.index);
        HSSFCellStyle style = this.workbook.createCellStyle();
        style.setVerticalAlignment(VerticalAlignment.CENTER);//设置居中
        style.setAlignment(HorizontalAlignment.RIGHT);//设置居中
        style.setFont(font);// 调用字体样式对象
        style.setWrapText(true);
        style.setBorderBottom(BorderStyle.THIN);//下边框
        style.setBorderLeft(BorderStyle.THIN);//左边框
        style.setBorderTop(BorderStyle.THIN);//上边框
        style.setBorderRight(BorderStyle.THIN);//右边框
        this.contentRightRedStyle = style;
    }

    /**
     * 生成表头内容
     *
     * @param lastColumn 横向合并的最大列数
     */
    public void setHead(int lastColumn) {
        // 标题行
        HSSFRow row0 = this.getSheet().createRow(0);
        HSSFCell cell = row0.createCell(0);
        row0.setHeightInPoints(35);
        // 单元格内容
        cell.setCellStyle(this.getHeadStyle());
        cell.setCellValue(this.getFileName());
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, lastColumn));

    }


    public void exportOut(HttpServletResponse response) throws IOException {
        //输出Excel文件
        OutputStream outputStream = response.getOutputStream();
        response.reset();
        response.setHeader("Content-disposition", "attachment;filename=" + new String((this.fileName + ".xls").getBytes("gb2312"), "ISO8859-1"));
        response.setContentType("application/msexcel");
        this.workbook.write(outputStream);
        outputStream.close();
    }

    /**
     * 导出
     *
     * @param
     * @param response
     */
    // public static void exportList(String fileName, List<T> dataList, int type, List<String> titleKeys, HttpServletResponse response) throws IOException {
    //
    //     // 创建HSSFWorkbook对象(excel的文档对象)
    //     HSSFWorkbook wb = new HSSFWorkbook();
    //     // 建立新的sheet对象（excel的表单）
    //     HSSFSheet sheet = wb.createSheet(fileName);
    //     // 在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个
    //     HSSFRow row0 = sheet.createRow(0);
    //     // 创建单元格（excel的单元格，参数为列索引，可以是0～255之间的任何一个
    //     HSSFCell cell = row0.createCell(0);
    //
    //     row0.setHeightInPoints(35);
    //
    //     // todo 设置列宽
    //
    //     // 设置默认高度
    //     // sheet.setDefaultRowHeight((short) 400);
    //
    //
    //     // 单元格内容
    //     cell.setCellStyle(headStyle);
    //     cell.setCellValue(fileName);
    //     sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, type == 1 ? 13 : type == 2 ? 14 : 24));
    //
    //     //在sheet里创建第二行
    //     HSSFRow row2 = sheet.createRow(1);
    //     row2.setHeight((short) 600);
    //     //创建单元格并设置单元格内容及样式
    //
    //
    //     //输出Excel文件
    //     OutputStream outputStream = response.getOutputStream();
    //     response.reset();
    //     response.setHeader("Content-disposition", "attachment;filename=" + new String((fileName + ".xls").getBytes("gb2312"), "ISO8859-1"));
    //     response.setContentType("application/msexcel");
    //     wb.write(outputStream);
    //     outputStream.close();
    //
    // }

}
