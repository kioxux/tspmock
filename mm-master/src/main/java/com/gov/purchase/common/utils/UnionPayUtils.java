package com.gov.purchase.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.gov.purchase.common.entity.AppPay;
import com.gov.purchase.common.entity.UnionPay;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.31
 */
@Slf4j
@Component
public class UnionPayUtils {

    private static String authorization;

//    public static void main(String[] args) throws Exception {
//        AppPay appPay = new AppPay();
//        appPay.setAppId("10037e6f66f2d0f901672aa27d690006");
//        appPay.setAppKey("47ace12ae3b348fe93ab46cee97c6fde");
//        appPay.setTid("00000001");
//        appPay.setMid("898310148160568");
//        appPay.setBillNoPre("1017");
//        appPay.setWechatUrl("http://58.247.0.18:29015/v1/netpay/wx/unified-order");
//        appPay.setAlipayUrl("http://58.247.0.18:29015/v1/netpay/trade/app-pre-order");
//        appPay.setUnionpayUrl("http://58.247.0.18:29015/v1/netpay/uac/app-order");
//        appPay.setNotifyUrl("http://1172410739.uttcare.com:18008/purchase/invoicePaying/appPayResult");
//        appPay.setExpireTime(5);
//
//        UnionPay unionPay = new UnionPay();
//        unionPay.setPaymentAmount(1);
//        unionPay.setPayMethod(UnionPay.ALIPAY);
//
//        placeAnOrder(appPay, unionPay);
//    }

//    public static void main(String[] args) throws Exception {
//        AppPay appPay = new AppPay();
//        appPay.setAppId("10037e6f66f2d0f901672aa27d690006");
//        appPay.setAppKey("47ace12ae3b348fe93ab46cee97c6fde");
//        appPay.setTid("00000001");
//        appPay.setMid("898310148160568");
//        appPay.setBillNoPre("1017");
//        appPay.setQueryUlr("http://58.247.0.18:29015/v1/netpay/query");
//        String result = query(appPay, "1017202011101630316065556390");
//    }

    /**
     * 查询支付结果
     *
     * @return
     */
    public static String query(AppPay appPay, String billNo) throws Exception {
        /* post参数,格式:JSON */
        JSONObject json = new JSONObject();
        json.put("msgId", "001");   // 消息Id,原样返回
        json.put("requestTimestamp", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));    // 报文请求时间
        json.put("mid", appPay.getMid()); // 商户号
        json.put("tid", appPay.getTid());    // 终端号
        json.put("instMid", "APPDEFAULT"); // 业务类型
        json.put("merOrderId", billNo); // 商户订单号,支付交易生成的商户订单号
//        json.put("targetOrderId", "");
        log.info("APP查询支付结果参数：{}", json);
        System.out.println("请求报文:\n" + json);
        String send = send(appPay, appPay.getQueryUlr(), json.toString());
        System.out.println("返回结果:\n" + send);
        log.info("APP查询支付结果返回值：{}", send);
        return send;
    }

    /**
     * app下单
     *
     * @throws Exception
     */
    public static String placeAnOrder(AppPay appPay, UnionPay unionPay) throws Exception {
        /* post参数,格式:JSON */
        JSONObject json = new JSONObject();
        json.put("msgId", "001");   // 消息Id,原样返回
        json.put("requestTimestamp", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));    // 报文请求时间
        json.put("merOrderId", unionPay.getMerOrderId()); // 商户订单号
        json.put("srcReserve", unionPay.getSrcReserve()); // 请求系统预留字段
        json.put("subAppId", appPay.getSubAppId()); // 微信子商户appid
        json.put("mid", appPay.getMid()); // 商户号
        json.put("tid", appPay.getTid());    // 终端号
        json.put("instMid", "APPDEFAULT"); // 业务类型
        json.put("expireTime", DateFormatUtils.format(new Date().getTime() + appPay.getExpireTime() * 60 * 1000, "yyyy-MM-dd HH:mm:ss"));  // 订单过期时间
        json.put("orderDesc", "门店服务费");  // 账单描述
        json.put("totalAmount", unionPay.getPaymentAmount());      // 支付总金额
        json.put("notifyUrl", appPay.getNotifyUrl());  // 支付结果通知地址
        json.put("tradeType", "APP");  // 交易类型，微信必传
        log.info("APP支付请求：{}", json);
        System.out.println("请求报文:\n" + json);
        String url = "";
        if (UnionPay.WECHAT.equals(unionPay.getPayMethod())) {
            url = appPay.getWechatUrl();
        } else if (UnionPay.ALIPAY.equals(unionPay.getPayMethod())) {
            url = appPay.getAlipayUrl();
        } else if (UnionPay.UNIONPAY.equals(unionPay.getPayMethod())) {
            url = appPay.getUnionpayUrl();
        } else {
            throw new CustomResultException("请选择正确的支付方式");
        }
        log.info("APP支付请求URL：{}", url);
        System.out.println(url);
        String send = send(appPay, url, json.toString());
        log.info("APP支付请求结果：{}", send);
        System.out.println("返回结果:\n" + send);
        // APP支付返回结果
        JSONObject result = JSONObject.parseObject(send);
        // 支付申请结果
        String errCode = result.getString("errCode");
        String errMsg = result.getString("errMsg");
        String errInfo = result.getString("errInfo");
        if (!"SUCCESS".equals(errCode)) {
            if (StringUtils.isNotBlank(errMsg)) {
                throw new CustomResultException(errMsg);
            }
            if (StringUtils.isNotBlank(errInfo)) {
                throw new CustomResultException(errInfo);
            }
            throw new CustomResultException("支付错误，请稍后再试");
        }
        return send;
    }

    /**
     * 发送请求
     *
     * @param url    eg:http://58.247.0.18:29015/v2/poslink/transaction/pay
     * @param entity eg:{"merchantCode":"123456789900081","terminalCode":"00810001","transactionAmount":1,"transactionCurrencyCode":"156","merchantOrderId":"201905211550526234902343","merchantRemark":"商户备注","payMode":"CODE_SCAN","payCode":"285059979526414634"}
     * @return
     * @throws Exception
     */
    public static String send(AppPay appPay, String url, String entity) throws Exception {
        authorization = getOpenBodySig(appPay.getAppId(), appPay.getAppKey(), entity);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Authorization", authorization);
        StringEntity se = new StringEntity(entity, "UTF-8");
        se.setContentType("application/json");
        httpPost.setEntity(se);
        CloseableHttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity1 = response.getEntity();
        String resStr = null;
        if (entity1 != null) {
            resStr = EntityUtils.toString(entity1, "UTF-8");
        }
        httpClient.close();
        response.close();
        return resStr;
    }

    /**
     * open-body-sig方式获取到Authorization 的值
     *
     * @param appId  f0ec96ad2c3848b5b810e7aadf369e2f
     * @param appKey 775481e2556e4564985f5439a5e6a277
     * @param body   json字符串 String body = "{\"merchantCode\":\"123456789900081\",\"terminalCode\":\"00810001\",\"merchantOrderId\":\"20123333644493200\",\"transactionAmount\":\"1\",\"merchantRemark\":\"测试\",\"payMode\":\"CODE_SCAN\",\"payCode\":\"285668667587422761\",\"transactionCurrencyCode\":\"156\"}";
     * @return
     * @throws Exception
     */
    public static String getOpenBodySig(String appId, String appKey, String body) throws Exception {
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());   // eg:20190227113148
        String nonce = UUID.randomUUID().toString().replace("-", ""); // eg:be46cd581c9f46ecbd71b9858311ea12
        byte[] data = body.getBytes("UTF-8");
        System.out.println("data:\n" + body);
        InputStream is = new ByteArrayInputStream(data);
        String bodyDigest = testSHA256(is); // eg:d60bc3aedeb853e2a11c0c096baaf19954dd9b752e48dea8e919e5fb29a42a8d
        System.out.println("bodyDigest:\n" + bodyDigest);
        String str1_C = appId + timestamp + nonce + bodyDigest; // eg:f0ec96ad2c3848b5b810e7aadf369e2f + 20190227113148 + be46cd581c9f46ecbd71b9858311ea12 + d60bc3aedeb853e2a11c0c096baaf19954dd9b752e48dea8e919e5fb29a42a8d
        System.out.println("str1_C:" + str1_C);
//        System.out.println("appKey_D:\n" + appKey);
        byte[] localSignature = hmacSHA256(str1_C.getBytes(), appKey.getBytes());

        String localSignatureStr = Base64.encodeBase64String(localSignature);   // Signature
        System.out.println("Authorization:\n" + "OPEN-BODY-SIG AppId=" + "\"" + appId + "\"" + ", Timestamp=" + "\"" + timestamp + "\"" + ", Nonce=" + "\"" + nonce + "\"" + ", Signature=" + "\"" + localSignatureStr + "\"\n");
        return ("OPEN-BODY-SIG AppId=" + "\"" + appId + "\"" + ", Timestamp=" + "\"" + timestamp + "\"" + ", Nonce=" + "\"" + nonce + "\"" + ", Signature=" + "\"" + localSignatureStr + "\"");
    }

    /**
     * 进行加密
     *
     * @param is
     * @return 加密后的结果
     */
    private static String testSHA256(InputStream is) {
        try {
//            System.out.println(is.hashCode());
            return DigestUtils.sha256Hex(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param data
     * @param key
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public static byte[] hmacSHA256(byte[] data, byte[] key) throws NoSuchAlgorithmException, InvalidKeyException {
        String algorithm = "HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data);
    }

}
