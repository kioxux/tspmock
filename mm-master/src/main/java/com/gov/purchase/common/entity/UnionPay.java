package com.gov.purchase.common.entity;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.02
 */
@Data
public class UnionPay {

    /**
     * 微信
     */
    public static final String WECHAT = "WECHAT";

    /**
     * 支付宝
     */
    public static final String ALIPAY = "ALIPAY";

    /**
     * 云闪付
     */
    public static final String UNIONPAY = "UNIONPAY";

    /**
     * web支付
     */
    public static final String PAYCHANNEL_WEB = "PAYCHANNEL_WEB";

    /**
     * web支付
     */
    public static final String PAYCHANNEL_APP = "PAYCHANNEL_APP";


    /**
     * 请求系统预留字段
     */
    private String srcReserve;

    /**
     * 支付渠道 APP WEB
     */
    private String payChannel;

    /**
     * 支付方式 支付宝 微信 云闪付
     */
    private String payMethod;

    /**
     * 支付金额
     */
    private int paymentAmount;

    /**
     * 商户订单号
     */
    private String merOrderId;

}
