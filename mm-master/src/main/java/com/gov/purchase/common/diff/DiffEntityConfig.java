package com.gov.purchase.common.diff;

import com.gov.purchase.common.diff.entityconfig.CustomerEntityDiffConfig;
import com.gov.purchase.common.diff.entityconfig.ProductEntityDiffConfig;
import com.gov.purchase.common.diff.entityconfig.SupplierEntityDiffConfig;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.exception.CustomResultException;
import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author zhoushuai
 * @date 2021/4/15 9:37
 */
@Data
@Component
public class DiffEntityConfig {

    @Resource
    ProductEntityDiffConfig productEntityDiffConfig;
    @Resource
    SupplierEntityDiffConfig supplierEntityDiffConfig;
    @Resource
    CustomerEntityDiffConfig customerEntityDiffConfig;


    // 商品
    private List<DiffEntityInfo> productAllPropertyList;
    private List<DiffEntityInfo> productDiffPropertyList;
    private Map<String, DiffEntityInfo> productNeedTransPropertyMap;

    // 供应商
    private List<DiffEntityInfo> supplierAllPropertyList;
    private List<DiffEntityInfo> supplierDiffPropertyList;
    private Map<String, DiffEntityInfo> supplierNeedTransPropertyMap;

    // 客户
    private List<DiffEntityInfo> customerAllPropertyList;
    private List<DiffEntityInfo> customerDiffPropertyList;
    private Map<String, DiffEntityInfo> customerNeedTransPropertyMap;


    /**
     * 初始化
     */
    @PostConstruct
    private void configInit() {
        // 商品实体信息配置
        productAllPropertyList = productEntityDiffConfig.productConfigInit();
        productDiffPropertyList = productAllPropertyList.stream().filter(Objects::nonNull)
                .filter(item -> CommonEnum.NoYesStatusInteger.YES.getCode().equals(item.getFlagDiff()))
                .collect(Collectors.toList());
        productNeedTransPropertyMap = productDiffPropertyList.stream().filter(Objects::nonNull)
                .peek(item -> {
                    if (CommonEnum.NeedTranslate.DIC.getCode().equals(item.getFlagNeedTranslate()) && ObjectUtils.isEmpty(item.getDictionaryStaticData())) {
                        throw new CustomResultException(MessageFormat.format("商品配置,字段{0},需要配置静态字典", item.getPropertyTo()));
                    }
                    if (CommonEnum.NeedTranslate.DB.getCode().equals(item.getFlagNeedTranslate()) && ObjectUtils.isEmpty(item.getDictionaryData())) {
                        throw new CustomResultException(MessageFormat.format("商品配置,字段{0},需要配置动态字典", item.getPropertyTo()));
                    }
                })
                .filter(item -> !CommonEnum.NeedTranslate.NO.getCode().equals(item.getFlagNeedTranslate()))
                .collect(Collectors.toMap(DiffEntityInfo::getPropertyTo, item -> item));

        // 供应商实体信息配置
        supplierAllPropertyList = supplierEntityDiffConfig.supplierConfigInit();
        supplierDiffPropertyList = supplierAllPropertyList.stream().filter(Objects::nonNull)
                .filter(item -> CommonEnum.NoYesStatusInteger.YES.getCode().equals(item.getFlagDiff()))
                .collect(Collectors.toList());
        supplierNeedTransPropertyMap = supplierDiffPropertyList.stream().filter(Objects::nonNull)
                .peek(item -> {
                    if (CommonEnum.NeedTranslate.DIC.getCode().equals(item.getFlagNeedTranslate()) && ObjectUtils.isEmpty(item.getDictionaryStaticData())) {
                        throw new CustomResultException(MessageFormat.format("供应商配置,字段{0},需要配置静态字典", item.getPropertyTo()));
                    }
                    if (CommonEnum.NeedTranslate.DB.getCode().equals(item.getFlagNeedTranslate()) && ObjectUtils.isEmpty(item.getDictionaryData())) {
                        throw new CustomResultException(MessageFormat.format("供应商配置,字段{0},需要配置动态字典", item.getPropertyTo()));
                    }
                })
                .filter(item -> !CommonEnum.NeedTranslate.NO.getCode().equals(item.getFlagNeedTranslate()))
                .collect(Collectors.toMap(DiffEntityInfo::getPropertyTo, item -> item));

        // 客户实体信息配置
        customerAllPropertyList = customerEntityDiffConfig.customerConfigInit();
        customerDiffPropertyList = customerAllPropertyList.stream().filter(Objects::nonNull)
                .filter(item -> CommonEnum.NoYesStatusInteger.YES.getCode().equals(item.getFlagDiff()))
                .collect(Collectors.toList());
        customerNeedTransPropertyMap = customerDiffPropertyList.stream().filter(Objects::nonNull)
                .peek(item -> {
                    if (CommonEnum.NeedTranslate.DIC.getCode().equals(item.getFlagNeedTranslate()) && ObjectUtils.isEmpty(item.getDictionaryStaticData())) {
                        throw new CustomResultException(MessageFormat.format("客户配置,字段{0},需要配置静态字典", item.getPropertyTo()));
                    }
                    if (CommonEnum.NeedTranslate.DB.getCode().equals(item.getFlagNeedTranslate()) && ObjectUtils.isEmpty(item.getDictionaryData())) {
                        throw new CustomResultException(MessageFormat.format("客户配置,字段{0},需要配置动态字典", item.getPropertyTo()));
                    }
                })
                .filter(item -> !CommonEnum.NeedTranslate.NO.getCode().equals(item.getFlagNeedTranslate()))
                .collect(Collectors.toMap(DiffEntityInfo::getPropertyTo, item -> item));

    }





}
