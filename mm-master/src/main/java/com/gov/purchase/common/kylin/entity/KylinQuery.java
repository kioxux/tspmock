package com.gov.purchase.common.kylin.entity;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.12.21
 */
@Data
public class KylinQuery {

    public KylinQuery(String sql, String debugToggleHitCube) {
        this.sql = sql;
        if (StringUtils.isNotBlank(debugToggleHitCube)) {
            Map<String, String> backdoorToggles = new HashMap<>();
            backdoorToggles.put(KylinQuery.DEBUG_TOGGLE_HIT_CUBE, debugToggleHitCube);
            this.backdoorToggles = backdoorToggles;
        }
    }

    public final static String C_DAILY_SALE_ALL = "C_DAILY_SALE_ALL";

    public final static String C_D_STORE = "C_D_STORE";

    public final static String DEBUG_TOGGLE_HIT_CUBE = "DEBUG_TOGGLE_HIT_CUBE";

    private String sql;

    private Integer offset = 0;

    private Integer limit = 50000;

    private boolean acceptPartial;

    private String project;

    private Map<String, String> backdoorToggles;
}
