package com.gov.purchase.common.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "smsconfig")
public class SmsConfig {

    /**
     * 用户的企业编码
     */
    private String smsspcode;

    /**
     * 用户名
     */
    private String smsusername;

    /**
     * 密码
     */
    private String smspassword;

    /**
     * 接口地址
     */
    private String smsurl;

}
