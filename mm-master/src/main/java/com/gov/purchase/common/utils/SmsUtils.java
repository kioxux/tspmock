package com.gov.purchase.common.utils;

import com.gov.purchase.common.entity.SmsConfig;
import com.gov.purchase.common.entity.SmsEntity;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.19
 */
@Slf4j
@Component
public class SmsUtils {

    /**
     * 发票短信模板
     */
    public static final String SMS0000010 = "SMS0000010";

    @Resource
    SmsConfig smsConfig;

    /**
     * 营销短信发送
     */
    public Result sendMarketingSms(SmsEntity smsEntity) {
        return this.sendSms(smsEntity);
    }

    /**
     * 发送短信
     */
    private Result sendSms(SmsEntity smsEntity) {
        HttpClient httpclient = new HttpClient();
        PostMethod post = new PostMethod(smsConfig.getSmsurl());//
        post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "gbk");
        post.addParameter("SpCode", smsConfig.getSmsspcode());
        post.addParameter("LoginName", smsConfig.getSmsusername());
        post.addParameter("Password", smsConfig.getSmspassword());
        post.addParameter("MessageContent", smsEntity.getMsg());
        post.addParameter("UserNumber", smsEntity.getPhone());
        post.addParameter("SerialNumber", UUIDUtil.getNumUUID());
        post.addParameter("ScheduleTime", "");
        String info = null;
        try {
            log.info("短信发送参数：{}", JsonUtils.beanToJson(smsEntity));
            httpclient.executeMethod(post);
            info = new String(post.getResponseBody(), "gbk");
            log.info("短信发送结果：{}", info);
        } catch (IOException e) {
            throw new CustomResultException(ResultEnum.E0171);
        }
        if (null != info) {
            Map<String, String> infoMap = this.getResultParams(info);
            if ("0".equals(infoMap.get("result"))) {
                return ResultUtil.success();
            } else {
                throw new CustomResultException(ResultEnum.E0171, infoMap.get("description"));
            }
        }
        throw new CustomResultException(ResultEnum.E0171);
    }

    /**
     * 解析返回参数
     */
    private Map getResultParams(String result) {
        Map<String, String> map = new HashMap<>();
        if (result.split("&").length > 0) {
            String[] arr = result.split("&");
            for (String s : arr) {
                String[] items = s.split("=");
                if (items.length > 1) {
                    String key = s.split("=")[0];
                    String value = s.split("=")[1];
                    map.put(key, value);
                } else if (items.length == 1) {
                    map.put(s.split("=")[0], "");
                }
            }
            return map;
        } else {
            return map;
        }
    }

}
