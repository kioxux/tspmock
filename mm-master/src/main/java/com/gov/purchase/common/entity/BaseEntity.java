package com.gov.purchase.common.entity;

import lombok.Data;

import java.util.Date;

/**
 * @desc: 基础实体对象
 * @author: ryan
 * @createTime: 2021/5/31 10:20
 */
@Data
public class BaseEntity {
    /**主键*/
    protected Long id;
    /**删除标记：0-正常 1-删除*/
    protected Integer deleteFlag;
    /**创建时间*/
    protected Date createTime;
    /**创建者*/
    protected String createUser;
    /**更新时间*/
    protected Date updateTime;
    /**更新者*/
    protected String updateUser;
}
