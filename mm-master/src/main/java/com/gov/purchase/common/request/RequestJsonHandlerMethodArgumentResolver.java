package com.gov.purchase.common.request;

import com.gov.purchase.exception.CustomResultException;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;

public class RequestJsonHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
    private static final String JSON_REQUEST_BODY = "JSON_REQUEST_BODY";

    //判断是否支持要转换的参数类型
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(RequestJson.class);
    }

    //当支持后进行相应的转换
    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String body = getRequestBody(webRequest);
        Object val = null;

        try {
            val = JsonPath.read(body, parameter.getParameterAnnotation(RequestJson.class).value());
            if (parameter.getParameterAnnotation(RequestJson.class).required() && val == null) {
                String name = parameter.getParameterAnnotation(RequestJson.class).name();
                throw new CustomResultException(name + "不能为空");
            }
        } catch (Exception exception) {
            if (parameter.getParameterAnnotation(RequestJson.class).required()) {
                String name = parameter.getParameterAnnotation(RequestJson.class).name();
                throw new CustomResultException(name + "不能为空");
            }
        }
        if (parameter.getGenericParameterType().getTypeName().endsWith("String")) {
            if (val != null) {
                return val.toString();
            }
        }
        if (parameter.getGenericParameterType().getTypeName().endsWith("Long")) {
            if (val != null && StringUtils.isNotBlank(val.toString())) {
                return NumberUtils.toLong(val.toString());
            } else {
                return null;
            }
        }
        if (parameter.getGenericParameterType().getTypeName().endsWith("BigDecimal")) {
            if (val != null && StringUtils.isNotBlank(val.toString())) {
                return new BigDecimal(val.toString());
            } else {
                return null;
            }
        }
        if (parameter.getGenericParameterType().getTypeName().endsWith("Integer")) {
            if (val != null && StringUtils.isNotBlank(val.toString())) {
                return NumberUtils.toInt(val.toString());
            } else {
                return null;
            }
        }
        return val;
    }

    private String getRequestBody(NativeWebRequest webRequest) {
        HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
        String jsonBody = (String) servletRequest.getAttribute(JSON_REQUEST_BODY);
        if (jsonBody == null) {
            try {
                jsonBody = IOUtils.toString(servletRequest.getInputStream());
                servletRequest.setAttribute(JSON_REQUEST_BODY, jsonBody);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return jsonBody;

    }


}
