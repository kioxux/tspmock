package com.gov.purchase.common.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author li_haixia@gov-info.cn
 * @date 2021/8/18 15:09
 */
@Data
@Component
@ConfigurationProperties(prefix = "bsaobpay")
public class BsaobPay {
    private String b2b_appId;
    private String b2b_appKey;

    /**
     * 商户号
     */
    private String b2b_mid;

    /**
     * 终端号
     */
    private String b2b_tid;

    /**
     * 业务类型
     */
    private String b2b_instMid;

    /**
     * 商户订单号前缀
     */
    private String b2b_billNoPre;

    /**
     * 支付结果回调地址
     */
    private String b2b_notifyUrl;

    /**
     * 请求地址
     */
    private String b2b_url;
    /**
     * 下单请求地址
     */
    private String b2b_order;

    /**
     * 支付状态查询地址
     */
    private String b2b_queryUrl;

    /**
     * 单钱包和多钱包,多钱包为:MULTIPLE
     */
    private String b2b_walletOption;

    /**
     * 二维码过期时间
     */
    private Integer b2b_expireTime;
}
