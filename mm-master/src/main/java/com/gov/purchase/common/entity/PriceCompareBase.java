package com.gov.purchase.common.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @desc: 基础实体对象
 * @author: ryan
 * @createTime: 2021/5/31 10:20
 */
@Data
public class PriceCompareBase {
    /**
     * 主键
     */
    private Long id;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 加盟商
     */
    private String client;

}
