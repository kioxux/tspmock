package com.gov.purchase.common.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class WechatUtils {

    /**
     * 凭证获取
     */
    private static final String TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}";

    /**
     * 获取卡券激活url
     */
    private static final String GET_ACTIVE_URL = "https://api.weixin.qq.com/card/membercard/activate/geturl?access_token={0}";

    /**
     * 获取注册会员信息
     */
    private static final String GET_MEMBER_INFO = "https://api.weixin.qq.com/card/membercard/userinfo/get?access_token={0}";


    private static final String CUSTOM_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}";
    /**
     * 获取公众号 token
     *
     * @param weChatAppid
     * @param weChatSecret
     * @return
     */
    public String getAccessToken(String weChatAppid, String weChatSecret) {

        Mono<String> response = WebClient.create()
                .get()
                .uri(MessageFormat.format(TOKEN_URL, weChatAppid, weChatSecret))
                .retrieve()
                .bodyToMono(String.class);
        String result = response.block();

        log.info("公众号token:APPID:{},结果{}", weChatAppid, result);

        JSONObject json = JSONObject.parseObject(result);
        return json.getString("access_token");
    }


    /**
     * 获取卡券激活url
     * @param accessToken
     * @param cardId 会员卡id
     * @param outer_str 额外参数，激活后事件会返回
     * @return
     */
    public String getActiveUrl(String accessToken, String cardId, String outer_str) {
        Map<String, Object> map = new HashMap<>();
        map.put("card_id", cardId);
        map.put("outer_str", outer_str);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(GET_ACTIVE_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("获取卡券激活url接口,参数:{}, 返回：{}", map, result);
        JSONObject json = JSONObject.parseObject(result);
        return json.getString("url");
    }

    public String customTextImageSend(String accessToken, Map<String, Object> map) {
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(CUSTOM_SEND_URL, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("客服消息返回,参数:{}, 返回：{}", map, result);
        return result;
    }

    /**
     * 获取会员信息
     * @param accessToken
     * @param cardId
     * @param code
     * @return
     */
    public String getMemberInfoByCard(String accessToken, String cardId, String code) {
        Map<String, Object> map = new HashMap<>();
        map.put("card_id", cardId);
        map.put("code", code);
        Mono<String> response = WebClient.create().post()
                .uri(MessageFormat.format(GET_MEMBER_INFO, accessToken))
                .body(BodyInserters.fromObject(map))
                .retrieve().bodyToMono(String.class);
        String result = response.block();
        log.info("拉取会员信息接口,参数:{}, 返回：{}", map, result);
        return result;
    }
}
