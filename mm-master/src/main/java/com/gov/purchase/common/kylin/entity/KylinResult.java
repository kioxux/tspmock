package com.gov.purchase.common.kylin.entity;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.12.21
 */
@Data
public class KylinResult {
    /**
     * 字段组合
     */
    private List<ColumnMetas> columnMetas;
    /**
     * 数据
     */
    private Object[][] results;
    private String cube;
    private Integer affectedRowCount;
    private boolean isException;
    private String exceptionMessage;
    private Integer duration;
    private Integer totalScanCount;
    private Integer totalScanBytes;
    private boolean hitExceptionCache;
    private boolean storageCacheUsed;
    private String traceUrl;
    private boolean pushDown;
    private boolean partial;
}
