package com.gov.purchase.common.diff.entityconfig;

import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.module.base.dto.DictionaryParams;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * 动态字典参数配置
 *
 * @author zhoushuai
 * @date 2021/4/22 9:14
 */
@Data
@Component
public class DictionaryParamsConfig {

    private Map<String, DictionaryParams> dictionaryParamsMap = new HashMap<>();

    @PostConstruct
    public void init() {
        productSclassParams();
        proPartformParams();
    }

    /**
     * 商品 "商品自分类" 动态字典参数配置
     */
    private void productSclassParams() {
        DictionaryParams dictionaryParams = new DictionaryParams();
        dictionaryParams.setKey(CommonEnum.DictionaryData.PRODUCT_SCLASS.getCode());
        Map<String, String> exactParams = new HashMap<>();
        exactParams.put("CLIENT", "");
        exactParams.put("PRO_SITE", "");
        dictionaryParams.setExactParams(exactParams);
        dictionaryParamsMap.put(CommonEnum.DictionaryData.PRODUCT_SCLASS.getCode(), dictionaryParams);
    }

    /**
     * 商品 "细分剂型" 动态字典参数配置
     */
    private void proPartformParams() {
        DictionaryParams dictionaryParams = new DictionaryParams();
        dictionaryParams.setKey(CommonEnum.DictionaryData.PRO_PARTFORM.getCode());
        Map<String, String> exactParams = new HashMap<>();
        exactParams.put("PRO_FORM", "片剂");
        dictionaryParams.setExactParams(exactParams);
        dictionaryParamsMap.put(CommonEnum.DictionaryData.PRO_PARTFORM.getCode(), dictionaryParams);
    }
}
