package com.gov.purchase.common.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.HttpURLConnection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.05
 */
@Slf4j
@Component
public class GatewayPayUtils {

//        static String appid = "10037ca764636bbc01647d1ef4e10009";
//        static String appkey = "389d830dcc0c46a58b0ea1962041e497";
//        static String mid = "898310148160568";
//        static String tid ="E0209445";
//        static String url = "http://58.247.0.18:29015/v1/netpay/upg/order";
    static String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    static String nonce = UUID.randomUUID().toString().replace("-", "");

    static String url = "https://api-mop.chinaums.com/v1/netpay/upg/order";
    static String appid = "8a81c1be744ea995017567be1e810100";
    static String appkey = "27171dc56e6b4b49a84f1fea34692147";
    static String mid = "89832054816011H";
    static String tid = "E0209445";


//    public static void main(String[] args) throws Exception {
//
////        //1. 组建请求报文
////        UpgPayBody reqBody = new UpgPayBody();
////        reqBody.requestTimestamp = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
////        reqBody.merOrderId = "10C7"+DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(7);
////        reqBody.mid = mid;
////        reqBody.tid = tid;
////        reqBody.instMid = "H5DEFAULT";
////        reqBody.totalAmount = "1";
////
////        reqBody.transType = "UPG_BUSINESS";
////        reqBody.bizType = "100002";
////        reqBody.chnlNo = "03000000002";
////        reqBody.termType = "PC";
////        reqBody.chnlType = "PC";
////        reqBody.inAcctNo = Base64.encodeBase64String("01111111100000".getBytes("utf-8"));
////        reqBody.inAcctName = Base64.encodeBase64String("北京融防科技有限公司".getBytes("utf-8"));
////        reqBody.bankName = Base64.encodeBase64String("华夏银行".getBytes("utf-8"));
////
////        System.out.println("request body:\n" + reqBody);
////
////        //2. 获取认证报文
////        String authorization = "OPEN-FORM-PARAM";
////        System.out.println("authorization:\n" + authorization);
////        String signature = getSignature(appid, appkey, timestamp, nonce, reqBody.toString());
////        String URL = url + "?" + "authorization=" + authorization + "&appId=" + appid
////                + "&timestamp=" + timestamp + "&nonce=" + nonce + "&content=" + URLEncoder.encode(reqBody.toString(), "UTF-8")
////                + "&signature=" + URLEncoder.encode(signature, "UTF-8");
////        System.out.println("URL: \n" + URL);
////        //3. 发送http请求，并解析返回信息
//////        String response = request(URL);
//////        System.out.println("response:\n" + response);
//        payV1();
//    }

    /**
     * 发送httpGET请求
     *
     * @param url 请求url
     * @return response
     */
    static String request(String url) {
        StringBuffer sbf = new StringBuffer();
        String response = "";
        PrintWriter out = null;
        BufferedReader in = null;
        try {
            URL realUrl = new URL(url);
            URLConnection conn = realUrl.openConnection();
            HttpURLConnection httpUrlConnection = (HttpURLConnection) conn;
            httpUrlConnection.setRequestMethod("GET");
            httpUrlConnection.setReadTimeout(15000);
            httpUrlConnection.setReadTimeout(60000);
            httpUrlConnection.connect();
            in = new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                sbf.append(line);
                sbf.append("\r\n");
            }
            response = sbf.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return response;
    }

    /**
     * 获取签名头
     *
     * @param appid
     * @param appkey
     * @param timestamp 格式:"yyyyMMddHHmmss"
     * @param nonce     随机字符串，
     * @param body      请求体
     * @return authorization 认证报文
     * @throws Exception
     */
    static String getSignature(String appid, String appkey, String timestamp, String nonce, String body) throws Exception {
        byte[] data = body.getBytes("utf-8");
        InputStream is = new ByteArrayInputStream(data);
        String testSH = DigestUtils.sha256Hex(is);
        String s1 = appid+timestamp+nonce+testSH;
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(appkey.getBytes("utf-8"),"HmacSHA256"));
        byte[] localSignature = mac.doFinal(s1.getBytes("utf-8"));
        String localSignatureStr = Base64.encodeBase64String(localSignature);
        return localSignatureStr.trim();
    }

    public static void payV1() throws Exception {
        JSONObject json = new JSONObject();
        json.put("mid", mid);
        json.put("tid", tid);
        json.put("totalAmount", "1");
        json.put("instMid", "H5DEFAULT");
        json.put("transType", "UPG_BUSINESS");
//        json.put("inAcctNo", "aW5BY2N0TmFtZQ==");
//        json.put("inAcctName", "aW5BY2N0TmFtZQ==");
        json.put("merOrderId", "10VJ2021040614441448871405751");
        json.put("requestTimestamp", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        json.put("bankName","YmFua05hbWU=");
        json.put("bizType", "100003");
        json.put("chnlNo", "04100001001");
        json.put("termType", "PC");
        json.put("chnlType", "PC");

        //json.put("notifyUrl", "http://api.mall.dev.longdaoyun.com/front/payment/chinaumsGatewayPayment/notify");
        System.out.println("请求报文:\n"+json);
        //String signature = getOpenBodySigForNetpay(appId, appKey, timestamp, nonce, json.toString());

        String signature = getSignature(appid, appkey, timestamp, nonce, json.toString());
        System.out.println("signature:\n"+signature);
        String param = "authorization=OPEN-FORM-PARAM" + "&appId=" + appid + "&timestamp=" + timestamp + "&nonce=" + nonce + "&content=" + URLEncoder.encode(json.toString(), "UTF-8") + "&signature=" + URLEncoder.encode(signature, "UTF-8");
        String url = "http://api-mop.chinaums.com/v1/netpay/upg/order";
        System.out.println("URL:\n" + url + "?" + param);
    }

    static class UpgPayBody {
        //消息ID
        String msgId;
        //报文请求时间，格式yyyy-MM-ddHH:mm:ss
        String requestTimestamp;
        //商户订单号
        String merOrderId;
        //请求系统预留字段
        String srcReserve;
        //商户号
        String mid;
        //终端号
        String tid;
        //业务类型
        String instMid;
        //商品信息
        List<GoodsItem> goods;
        //商户附加数据
        String attachedData;
        //账单描述
        String orderDesc;
        //商品标记
        String goodsTag;
        //订单原始金额
        String originalAmount;
        //支付总金额
        String totalAmount;
        //订单过期时间
        String expireTime;
        //担保交易标识
        String secureTransaction;
        //支付结果通知地址
        String notifyUrl;
        //网页跳转地址
        String returnUrl;
        //系统ID
        String systemId;
        //业务应用类型
        String sceneType;
        //应用名称
        String merAppName;
        //应用标识
        String merAppId;
        //是否需要限制信用卡支付
        String limitCreditCard;
        //分账标记
        String divisionFlag;
        //平台商户分账金额
        String platformAmount;
        //子订单信息
        List<SubOrderItem> subOrders;
        //银联网关交易类型
        String transType;
        //银联网关业务种类
        String bizType;
        //网关渠道标识，用来标识发卡机构的网关电子渠道
        String chnlNo;
        //银联网关交易终端类型
        String termType;
        //银联网关交易受理渠道
        String chnlType;
        //银联网关交易结算账户号
        String inAcctNo;
        //银联网关交易结算账户名称
        String inAcctName;
        //银联网关交 易结算账户 开户行系统 行名称。若 对应银行开 户行名称， 应具体到分 支行
        String bankName;

        String toJson() {
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            if (this.msgId != null) sb.append("\"msgId\":\"" + this.msgId + "\",");
            if (this.requestTimestamp != null) sb.append("\"requestTimestamp\":\"" + this.requestTimestamp + "\",");
            if (this.merOrderId != null) sb.append("\"merOrderId\":\"" + this.merOrderId + "\",");
            if (this.srcReserve != null) sb.append("\"srcReserve\":\"" + this.srcReserve + "\",");
            if (this.mid != null) sb.append("\"mid\":\"" + this.mid + "\",");
            if (this.tid != null) sb.append("\"tid\":\"" + this.tid + "\",");
            if (this.instMid != null) sb.append("\"instMid\":\"" + this.instMid + "\",");
            if (this.goods != null && this.goods.size() > 0) {
                sb.append("\"goods\":[");
                for (int i = 0; i < goods.size(); i++) {
                    sb.append(goods.get(i));
                    sb.append(",");
                }
                if (sb.charAt(sb.length() - 1) == ',')
                    sb.deleteCharAt(sb.length() - 1);
                sb.append("],");
            }
            if (this.attachedData != null) sb.append("\"attachedData\":\"" + this.attachedData + "\",");
            if (this.orderDesc != null) sb.append("\"orderDesc\":\"" + this.orderDesc + "\",");
            if (this.goodsTag != null) sb.append("\"goodsTag\":\"" + this.goodsTag + "\",");
            if (this.originalAmount != null) sb.append("\"originalAmount\":\"" + this.originalAmount + "\",");
            if (this.totalAmount != null) sb.append("\"totalAmount\":\"" + this.totalAmount + "\",");
            if (this.expireTime != null) sb.append("\"expireTime\":\"" + this.expireTime + "\",");
            if (this.secureTransaction != null) sb.append("\"secureTransaction\":\"" + this.secureTransaction + "\",");
            if (this.notifyUrl != null) sb.append("\"notifyUrl\":\"" + this.notifyUrl + "\",");
            if (this.returnUrl != null) sb.append("\"returnUrl\":\"" + this.returnUrl + "\",");
            if (this.systemId != null) sb.append("\"systemId\":\"" + this.systemId + "\",");
            if (this.sceneType != null) sb.append("\"sceneType\":\"" + this.sceneType + "\",");
            if (this.merAppName != null) sb.append("\"merAppName\":\"" + this.merAppName + "\",");
            if (this.merAppId != null) sb.append("\"merAppId\":\"" + this.merAppId + "\",");
            if (this.limitCreditCard != null) sb.append("\"limitCreditCard\":\"" + this.limitCreditCard + "\",");
            if (this.divisionFlag != null) sb.append("\"divisionFlag\":\"" + this.divisionFlag + "\",");
            if (this.platformAmount != null) sb.append("\"platformAmount\":\"" + this.platformAmount + "\",");
            if (this.subOrders != null && this.subOrders.size() > 0) {
                sb.append("\"subOrders\":[");
                for (int i = 0; i < subOrders.size(); i++) {
                    sb.append(subOrders.get(i));
                    sb.append(",");
                }
                if (sb.charAt(sb.length() - 1) == ',')
                    sb.deleteCharAt(sb.length() - 1);
                sb.append("],");
            }

            if (this.transType != null) sb.append("\"transType\":\"" + this.transType + "\",");
            if (this.bizType != null) sb.append("\"bizType\":\"" + this.bizType + "\",");
            if (this.chnlNo != null) sb.append("\"chnlNo\":\"" + this.chnlNo + "\",");
            if (this.termType != null) sb.append("\"termType\":\"" + this.termType + "\",");
            if (this.chnlType != null) sb.append("\"chnlType\":\"" + this.chnlType + "\",");
            if (this.inAcctNo != null) sb.append("\"inAcctNo\":\"" + this.inAcctNo + "\",");
            if (this.inAcctName != null) sb.append("\"inAcctName\":\"" + this.inAcctName + "\",");
            if (this.bankName != null) sb.append("\"bankName\":\"" + this.bankName + "\",");
            if (sb.charAt(sb.length() - 1) == ',')
                sb.deleteCharAt(sb.length() - 1);
            sb.append("}");
            return sb.toString();
        }

        public String toString() {
            return this.toJson();
        }

        /* goods，商品信息 */
        static class GoodsItem {
            //商品ID
            String goodsId;
            //商品名称
            String goodsName;
            //商品数量
            String quantity;
            //商品单价（分）
            String price;
            //商品分类
            String goodsCategory;
            //商品说明
            String body;
            //子商户号
            String subMerchantId;
            // 商户子订单号
            String merOrderId;
            //子商户商品总额
            int subOrderAmount;

            String toJson() {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                if (this.goodsId != null) sb.append("\"goodsId\":\"" + this.goodsId + "\",");
                if (this.goodsName != null) sb.append("\"goodsName\":\"" + this.goodsName + "\",");

                if (this.quantity != null) {
                    sb.append("\"quantity\":\"" + this.quantity + "\",");
                }
                if (this.price != null) {
                    sb.append("\"price\":\"" + this.price + "\",");
                }
                if (this.goodsCategory != null) {
                    sb.append("\"goodsCategory\":\"" + this.goodsCategory + "\",");
                }
                if (this.body != null) {
                    sb.append("\"body\":\"" + this.body + "\",");
                }
                if (this.subMerchantId != null) {
                    sb.append("\"subMerchantId\":\"" + this.subMerchantId + "\",");
                }
                if (this.merOrderId != null) {
                    sb.append("\"merOrderId\":\"" + this.merOrderId + "\",");
                }
                if (this.subOrderAmount != 0) {
                    sb.append("\"subOrderAmount\":\"" + this.subOrderAmount + "\",");
                }
                if (sb.charAt(sb.length() - 1) == ',')
                    sb.deleteCharAt(sb.length() - 1);
                sb.append("}");
                return sb.toString();
            }

            public String toString() {
                return this.toJson();
            }
        }

        /* subOrders，子订单信息 */
        static class SubOrderItem {
            //子商户号
            String mid;
            //子商户分账金额
            int totalAmount;

            String toJson() {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                if (this.mid != null) {
                    sb.append("\"mid\":\"" + this.mid + "\",");
                }
                if (this.totalAmount != 0) {
                    sb.append("\"totalAmount\":\"" + this.totalAmount + "\",");
                }
                if (sb.charAt(sb.length() - 1) == ',')
                    sb.deleteCharAt(sb.length() - 1);
                sb.append("}");
                return sb.toString();
            }

            public String toString() {
                return this.toJson();
            }
        }

    }

}
