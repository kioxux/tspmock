package com.gov.purchase.common.diff;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.DictionaryParams;
import com.gov.purchase.module.base.service.DictionaryService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhoushuai
 * @date 2021/4/15 10:27
 */
@Slf4j
@Data
@Component
public class DiffEntityUtils {

    @Resource
    private DictionaryService dictionaryService;

    /**
     * 两个对象比对结果
     *
     * @param entityFrom              对象原始数据
     * @param entityTo                对象修改后的数据
     * @param productDiffPropertyList 需要比对的字段配置
     * @param <F>                     entityFrom类型
     * @param <T>                     entityTo类型
     * @return 两个对象比对结果集合
     */
    public <F, T> List<DiffEntityReuslt> diff(F entityFrom, T entityTo, List<DiffEntityInfo> productDiffPropertyList) {

        Map<String, PropertyDescriptor> entityFromPropertyMap = Stream.of(BeanUtils.getPropertyDescriptors(entityFrom.getClass()))
                .collect(Collectors.toMap(PropertyDescriptor::getName, item -> item));
        Map<String, PropertyDescriptor> entityToPropertyMap = Stream.of(BeanUtils.getPropertyDescriptors(entityTo.getClass()))
                .collect(Collectors.toMap(PropertyDescriptor::getName, item -> item));

        List<DiffEntityReuslt> difResultList = new ArrayList<>();
        for (DiffEntityInfo diffEntityInfo : productDiffPropertyList) {
            // 属性名(修改前)
            String propertyFrom = diffEntityInfo.getPropertyFrom();
            PropertyDescriptor propertyFromDescriptor = entityFromPropertyMap.get(propertyFrom);
            if (ObjectUtils.isEmpty(propertyFromDescriptor)) {
                log.info("比对的两个对象,对象1(更新前的对象)没有字段{}", propertyFrom);
                continue;
            }
            // 属性名(修改后)
            String propertyTo = diffEntityInfo.getPropertyTo();
            PropertyDescriptor propertyToDescriptor = entityToPropertyMap.get(propertyTo);
            if (ObjectUtils.isEmpty(propertyToDescriptor)) {
                log.info("比对的两个对象,对象2(更新后的对象)没有字段{}", propertyTo);
                continue;
            }
            // 反射获取值
            Object objFrom = null;
            try {
                objFrom = propertyFromDescriptor.getReadMethod().invoke(entityFrom);
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.info("比对的两个对象,反射获取对象1(更新后的对象)属性{}异常", propertyFrom);
                e.printStackTrace();
            }
            Object objTo = null;
            try {
                objTo = propertyToDescriptor.getReadMethod().invoke(entityTo);
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.info("比对的两个对象,反射获取对象2(更新后的对象)属性{}异常", propertyTo);
                e.printStackTrace();
            }
            // 值转为String类型
            String valueFrom = objToString(objFrom, propertyFromDescriptor.getPropertyType());
            String valueTo = objToString(objTo, propertyToDescriptor.getPropertyType());
            // 比对值(相同路过,不同则记录)
            if (StringUtils.isBlank(valueFrom) && StringUtils.isBlank(valueTo)) {
                continue;
            }
            if (StringUtils.isNotBlank(valueFrom) && !valueFrom.equals(valueTo)) {
                difResultList.add(new DiffEntityReuslt(propertyFrom, propertyTo, valueFrom, valueTo, diffEntityInfo.getColumn(), diffEntityInfo.getColumnDes()));
                continue;
            }
            if (StringUtils.isNotBlank(valueTo) && !valueTo.equals(valueFrom)) {
                difResultList.add(new DiffEntityReuslt(propertyFrom, propertyTo, valueFrom, valueTo, diffEntityInfo.getColumn(), diffEntityInfo.getColumnDes()));
            }

        }
        return difResultList;
    }

    public static String objToString(Object obj, Class<?> propertyType) {
        if (propertyType == String.class) {
            return (String) obj;
        }
        if (propertyType == Integer.class) {
            return String.valueOf(obj);
        }
        if (propertyType == Long.class) {
            return String.valueOf(obj);
        }
        if (propertyType == Short.class) {
            return String.valueOf(obj);
        }
        if (propertyType == BigDecimal.class) {
            return obj == null ? "" : ((BigDecimal) obj).stripTrailingZeros().toPlainString();
        }
        return obj == null ? "" : obj.toString();
    }

    /**
     * 值+字典 -->翻译
     *
     * @param diffEntityInfo 配置详情
     * @param value          值
     * @return 翻译结果
     */
    public String getLabelByValueFromDic(DiffEntityInfo diffEntityInfo, String value, Map<String, String> params, Map<String, String> exactParams, String type) {
        if (CommonEnum.NeedTranslate.DIC.getCode().equals(diffEntityInfo.getFlagNeedTranslate())) {
            // 静态字典
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(diffEntityInfo.getDictionaryStaticData(), value);
            if (!ObjectUtils.isEmpty(dictionary)) {
                return dictionary.getLabel();
            }
            return value;
        } else if (CommonEnum.NeedTranslate.DB.getCode().equals(diffEntityInfo.getFlagNeedTranslate())) {
            // 动态字典
            // 参数
            DictionaryParams dicParams = null;
            if (ObjectUtils.isEmpty(diffEntityInfo.getDictionaryParams())) {
                dicParams = new DictionaryParams();
                dicParams.setKey(diffEntityInfo.getDictionaryData().getCode());
            } else {
                dicParams = diffEntityInfo.getDictionaryParams();
                // 精确参数
                Map<String, String> exact = dicParams.getExactParams();
                if (!CollectionUtils.isEmpty(exact)) {
                    exact.forEach((key, val) -> {
                        String customerExactParam = exactParams.get(key);
                        // 如果有自定义参数，直接设置，如果没有，设置默认值
                        exact.put(key, StringUtils.isNotBlank(customerExactParam) ? customerExactParam : val);
                    });
                }
                // 模糊参数
                Map<String, String> indistinctParam = dicParams.getParams();
                if (!CollectionUtils.isEmpty(indistinctParam)) {
                    indistinctParam.forEach((key, val) -> {
                        String customerIndistinctParam = params.get(key);
                        // 如果有自定义参数，直接设置，如果没有，设置默认值
                        indistinctParam.put(key, StringUtils.isNotBlank(customerIndistinctParam) ? customerIndistinctParam : val);
                    });
                }
                // 转置
                dicParams.setType(StringUtils.isNotBlank(type) ? type : dicParams.getType());
            }
            // 查询
            Result result = dictionaryService.getDictionary(dicParams);
            // 结果Object强转 List<Dictionary>
            List<Dictionary> listDic = (List<Dictionary>) (result.getData());
            // 转 Map
            Map<String, String> valueLabelMap = listDic.stream().collect(Collectors.toMap(Dictionary::getValue, Dictionary::getLabel));
            return valueLabelMap.getOrDefault(value, value);
        } else {
            return value;
        }
    }

}
