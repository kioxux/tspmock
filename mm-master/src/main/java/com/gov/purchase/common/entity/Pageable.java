package com.gov.purchase.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;


@Getter
@Setter
@ApiModel(value = "分页信息")
public class Pageable {
    /**
     * 查询页面
     */
    @ApiModelProperty(value = "页码", name = "pageNum", example = "1" , required = true)
    @NotNull(message = "未发现查询参数页码")
    private Integer pageNum;

    /**
     * 分页信息
     */
    @ApiModelProperty(value = "分页", name = "pageSize", example = "10" , required = true)
    @NotNull(message = "未发现查询参数分页")
    private Integer pageSize;
}
