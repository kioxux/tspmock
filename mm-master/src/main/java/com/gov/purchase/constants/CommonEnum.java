package com.gov.purchase.constants;

import com.google.common.collect.Lists;
import com.gov.purchase.entity.GaiaWholesaleChannelDetail;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.DictionaryStatic;
import com.gov.purchase.module.base.dto.businessImport.*;
import com.gov.purchase.module.store.dto.store.StoreMinQtyImportDto;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 共通枚举
 */
public class CommonEnum {

    /**
     * 静态数据字典
     */
    public enum DictionaryStaticData {
        // 验证用
        DEFAULT("DEFAULT", new ArrayList<Dictionary>()),
        // 保质期单位
        PRO_LIFE_UNIT("PRO_LIFE_UNIT", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "天"));
            add(new Dictionary("2", "月"));
            add(new Dictionary("3", "年"));
        }}),
        // 批准文号分类
        PRO_REGISTER_CLASS("PRO_REGISTER_CLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "国产化学药品"));
            add(new Dictionary("2", "进口化学药品"));
            add(new Dictionary("3", "国产器械"));
            add(new Dictionary("4", "进口器械"));
            add(new Dictionary("5", "中药饮片"));
            add(new Dictionary("6", "特殊化妆品"));
            add(new Dictionary("7", "消毒用品"));
            add(new Dictionary("8", "保健食品"));
            add(new Dictionary("9", "QS商品"));
            add(new Dictionary("10", "其它"));
            add(new Dictionary("11", "国产中成药品"));
            add(new Dictionary("12", "进口中成药品"));
            add(new Dictionary("13", "生物制剂"));
            add(new Dictionary("14", "辅料"));
        }}),
        // 品牌区分
        PRO_BRAND_CLASS("PRO_BRAND_CLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "全国品牌"));
            add(new Dictionary("2", "省份品牌"));
            add(new Dictionary("3", "地区品牌"));
            add(new Dictionary("4", "本地品牌"));
            add(new Dictionary("5", "其它"));
        }}),
        // 贮存条件
        PRO_STORAGE_CONDITION("PRO_STORAGE_CONDITION", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "常温"));
            add(new Dictionary("2", "阴凉"));
            add(new Dictionary("3", "冷藏"));
        }}),
        // 商品仓储分区
        PRO_STORAGE_AREA("PRO_STORAGE_AREA", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "内服药品"));
            add(new Dictionary("2", "外用药品"));
            add(new Dictionary("3", "中药饮片区"));
            add(new Dictionary("4", "精装中药区"));
            add(new Dictionary("5", "针剂"));
            add(new Dictionary("6", "二类精神药品"));
            add(new Dictionary("7", "含麻药品"));
            add(new Dictionary("8", "冷藏商品"));
            add(new Dictionary("9", "外用非药"));
            add(new Dictionary("10", "医疗器械"));
            add(new Dictionary("11", "食品"));
            add(new Dictionary("12", "保健食品"));
            add(new Dictionary("13", "易燃商品区"));
            add(new Dictionary("14", "其他"));
        }}),
        // 生产类别
        PRO_PRODUCE_CLASS("PRO_PRODUCE_CLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "辅料"));
            add(new Dictionary("2", "化学药品"));
            add(new Dictionary("3", "生物制品"));
            add(new Dictionary("4", "中药"));
            add(new Dictionary("5", "器械"));
        }}),
        // 管制特殊分类
        PRO_CONTROL_CLASS("PRO_CONTROL_CLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "毒性药品"));
            add(new Dictionary("2", "麻醉药品"));
            add(new Dictionary("3", "一类精神药品"));
            add(new Dictionary("4", "二类精神药品"));
            add(new Dictionary("5", "易制毒药品（麻黄碱）"));
            add(new Dictionary("6", "放射性药品"));
            add(new Dictionary("7", "生物制品（含胰岛素）"));
            add(new Dictionary("8", "兴奋剂（除胰岛素）"));
            add(new Dictionary("9", "第一类器械"));
            add(new Dictionary("10", "第二类器械"));
            add(new Dictionary("11", "第三类器械"));
            add(new Dictionary("12", "其它管制"));
            add(new Dictionary("13", "含特殊药品复方制剂"));
            add(new Dictionary("14", "易燃易爆"));
        }}),
        // 供应商分类
        SUP_CLASS("SUP_CLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "生产企业"));
            add(new Dictionary("2", "商业公司"));
            add(new Dictionary("3", "非商品供应商"));
            add(new Dictionary("4", "财务专用"));
            add(new Dictionary("5", "食品经营企业"));
            add(new Dictionary("6", "药品经营企业"));
            add(new Dictionary("7", "器械经营企业"));
            add(new Dictionary("8", "药品生产企业"));
            add(new Dictionary("9", "器械生产企业"));
            add(new Dictionary("10", "食品生产企业"));
            add(new Dictionary("11", "其他经营企业"));
            add(new Dictionary("12", "委托配送供应商"));
            add(new Dictionary("13", "物流供应商"));
        }}),
        // 客户分类
        CUS_CLASS("CUS_CLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "外部客户"));
            add(new Dictionary("2", "配送中心"));
            add(new Dictionary("3", "门店"));
        }}),
        // 支付方式
        SUP_PAY_MODE("SUP_PAY_MODE", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "银行转账"));
            add(new Dictionary("2", "承兑汇票"));
            add(new Dictionary("3", "线下票据"));
            add(new Dictionary("4", "赊销"));
            add(new Dictionary("5", "电汇"));
        }}),
        // 处方类别
        PRO_PRESCLASS("PRO_PRESCLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "单轨处方"));
            add(new Dictionary("2", "甲类OTC"));
            add(new Dictionary("3", "乙类OTC"));
            add(new Dictionary("4", "双跨处方"));
            add(new Dictionary("5", "双轨处方"));
        }}),
        // 物流模式
        PO_DELIVERY_TYPE("PO_DELIVERY_TYPE", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "直送"));
            add(new Dictionary("2", "直通"));
        }}),
        // 库位
        PO_LOCATION_CODE("PO_LOCATION_CODE", new ArrayList<Dictionary>() {{
            add(new Dictionary("1000", "合格状态"));
            add(new Dictionary("2000", "待判定状态"));
            add(new Dictionary("3000", "退货状态"));
            add(new Dictionary("4000", "不合格状态"));
            add(new Dictionary("5000", "调拨状态"));
            add(new Dictionary("6000", "直通状态"));
            add(new Dictionary("7000", "冻结状态"));
        }}),
        // 商品状态
        PRO_STATUS("PRO_STATUS", new ArrayList<Dictionary>() {{
            add(new Dictionary("0", "正常"));
            add(new Dictionary("1", "停用"));
        }}),
        // 税分类
        STO_TAX_CLASS("STO_TAX_CLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "小规模纳税人"));
            add(new Dictionary("2", "一般纳税人"));
            add(new Dictionary("3", "个体工商户"));
        }}),
        // 供应商状态
        SUP_STATUS("SUP_STATUS", new ArrayList<Dictionary>() {{
            add(new Dictionary("0", "正常"));
            add(new Dictionary("1", "停用"));
        }}),
        // 客户状态
        CUS_STATUS("CUS_STATUS", new ArrayList<Dictionary>() {{
            add(new Dictionary("0", "正常"));
            add(new Dictionary("1", "停用"));
        }}),
        // DC状态
        DC_STATUS("DC_STATUS", new ArrayList<Dictionary>() {{
            add(new Dictionary("0", "正常"));
            add(new Dictionary("1", "停用"));
        }}),
        // 门店属性
        STO_ATTRIBUTE("STO_ATTRIBUTE", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "单体"));
            add(new Dictionary("2", "连锁"));
            add(new Dictionary("3", "加盟"));
            add(new Dictionary("4", "门诊"));
        }}),
        // 实体类型
        ENTITY_CLASS("ENTITY_CLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "供应商"));
            add(new Dictionary("2", "客户"));
            add(new Dictionary("3", "门店"));
            add(new Dictionary("4", "公司"));
        }}),
        // 配送方式
        STO_DELIVERY_MODE("STO_DELIVERY_MODE", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "自采"));
            add(new Dictionary("2", "总部配送"));
            add(new Dictionary("3", "委托配送"));
            add(new Dictionary("4", "内部批发"));
            add(new Dictionary("5", "内部委托配送"));
            add(new Dictionary("6", "全量委托配送"));
            add(new Dictionary("7", "第三方委托配送"));
            add(new Dictionary("8", "第三方委托配送2"));
            add(new Dictionary("9", "双仓配送"));
        }}),
        // 门店状态
        STO_STATUS("STO_STATUS", new ArrayList<Dictionary>() {{
            add(new Dictionary("0", "营业"));
            add(new Dictionary("1", "闭店"));
        }}),
        // 信用检查点
        CUS_CREDIT_CHECK("CUS_CREDIT_CHECK", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "销售订单"));
            add(new Dictionary("2", "发货"));
            add(new Dictionary("3", "开票"));
        }}),
        PRC_CLASS("PRC_CLASS", new ArrayList<Dictionary>() {{
            add(new Dictionary("P001", "P001-零售价"));
            add(new Dictionary("P002", "P002-会员价"));
            add(new Dictionary("P003", "P003-医保价"));
            add(new Dictionary("P004", "P004-会员日价"));
            add(new Dictionary("P005", "P005-拆零价"));
            add(new Dictionary("P006", "P006-网上零售价"));
            add(new Dictionary("P007", "P007-网上会员价"));
        }}),
        // 匹配状态
        PRO_MATCH_STATUS("PRO_MATCH_STATUS", new ArrayList<Dictionary>() {{
            add(new Dictionary("0", "未匹配"));
            add(new Dictionary("1", "部分匹配"));
            add(new Dictionary("2", "完全匹配"));
        }}),
        // 国家医保品种  启用电子监管码  禁止销售
        // 禁止采购      禁止销售        是否拆零
        // 禁止配送      禁止退厂        禁止退仓
        // 禁止调剂      禁止批发        禁止请货
        // 禁止退货      是否启用信用管理 按中包装量倍数配货
        NO_YES("NO_YES", new ArrayList<Dictionary>() {{
            add(new Dictionary("0", "否"));
            add(new Dictionary("1", "是"));
        }}),
        // 字段名称
        FIELD_NAME("FIELD_NAME", new ArrayList<Dictionary>() {{
            add(new Dictionary("proStatus", "商品状态"));
            add(new Dictionary("proPosition", "商品定位"));
            add(new Dictionary("proNoRetail", "禁止销售"));
            add(new Dictionary("proNoPurchase", "禁止采购"));
            add(new Dictionary("proNoDistributed", "禁止配送"));
            add(new Dictionary("proNoSupplier", "禁止退厂"));
            add(new Dictionary("proNoDc", "禁止退仓"));
            add(new Dictionary("proNoAdjust", "禁止调剂"));
            add(new Dictionary("proNoSale", "禁止批发"));
            add(new Dictionary("proNoApply", "禁止请货"));
            add(new Dictionary("proIfpart", "是否拆零"));
            add(new Dictionary("proPartUint", "拆零单位"));
            add(new Dictionary("proPartRate", "拆零比例"));
            add(new Dictionary("proPurchaseUnit", "采购单位"));
            add(new Dictionary("proPurchaseRate", "采购单位比例"));
            add(new Dictionary("proSaleUnit", "销售单位"));
            add(new Dictionary("proSaleRate", "销售单位比例"));
            add(new Dictionary("proMinQty", "最小订货量"));
            add(new Dictionary("proIfMed", "是否医保"));
            add(new Dictionary("proSlaeClass", "销售级别"));
            add(new Dictionary("proLimitQty", "限购数量"));
        }}),
        APPROVE_STATUS("APPROVE_STATUS", new ArrayList<Dictionary>() {{
            add(new Dictionary("0", "未审核"));
            add(new Dictionary("1", "已审核"));
            add(new Dictionary("2", "拒绝"));
        }}),
        INVIOCE_TYPE("INVIOCE_TYPE", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "增值税普通发票"));
            add(new Dictionary("2", "增值税专用发票"));
        }}),
        PRO_POSITION("PRO_POSITION", new ArrayList<Dictionary>() {{
            add(new Dictionary("T", "淘汰"));
            add(new Dictionary("Q", "清场"));
            add(new Dictionary("X", "新品"));
            add(new Dictionary("D", "订购"));
            add(new Dictionary("Z", "正常"));
            add(new Dictionary("H", "核心"));
        }}),
        FICO_TAX_ATTRIBUTES("FICO_TAX_ATTRIBUTES", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "一般纳税人"));
            add(new Dictionary("2", "小规模"));
            add(new Dictionary("3", "个体工商户"));
        }}),
        QUESTION_TYPE("QUESTION_TYPE", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "单选"));
            add(new Dictionary("2", "多选"));
            add(new Dictionary("3", "不启用"));
        }}),
        QUESTION_LEVEL("QUESTION_LEVEL", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "难"));
            add(new Dictionary("2", "中"));
            add(new Dictionary("3", "易"));
        }}),
        PRO_TSSX("PRO_TSSX", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "防疫"));
        }}),
        PRO_YBLX("PRO_YBLX", new ArrayList<Dictionary>() {{
            add(new Dictionary("1", "甲类"));
            add(new Dictionary("2", "乙类"));
        }}),
        // 0-不养护，1-重点养护，2-一般养护
        PRO_KEY_CARE("PRO_KEY_CARE", new ArrayList<Dictionary>() {{
            add(new Dictionary("0", "不养护"));
            add(new Dictionary("1", "重点养护"));
            add(new Dictionary("2", "一般养护"));
        }});

        private String code;
        private List<Dictionary> list;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public List<Dictionary> getList() {
            return list;
        }

        public void setList(List<Dictionary> list) {
            this.list = list;
        }

        DictionaryStaticData(String code, List<Dictionary> list) {
            this.code = code;
            this.list = list;
        }

        //通过value获取对应的枚举对象
        public static List<DictionaryStatic> getDictionary(String value) {
            List<DictionaryStatic> list = new ArrayList<>();
            for (DictionaryStaticData dictionaryStaticData : DictionaryStaticData.values()) {
                DictionaryStatic dictionaryStatic = new DictionaryStatic();
                dictionaryStatic.setKey(dictionaryStaticData.getCode());
                dictionaryStatic.setList(dictionaryStaticData.getList());
                if (StringUtils.isNotBlank(value)) {
                    if (value.equals(dictionaryStaticData.getCode())) {
                        list.add(dictionaryStatic);
                        break;
                    }
                } else {
                    list.add(dictionaryStatic);
                }
            }
            return list;
        }

        /**
         * 返回当前字典的明细数据
         *
         * @param dictionaryStaticData 字典对象
         * @param value                编码
         * @return
         */
        public static Dictionary getDictionaryByValue(DictionaryStaticData dictionaryStaticData, String value) {
            // 	字典对象
            List<Dictionary> list = dictionaryStaticData.getList();
            // 数据不存在
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            // 遍历当前字典数据
            for (Dictionary dictionary : list) {
                if (dictionary.getValue().equals(value)) {
                    return dictionary;
                }
            }
            return null;
        }

        /**
         * 返回当前字典的明细数据
         *
         * @param dictionaryStaticData 字典对象
         * @param label                表示值
         * @return
         */
        public static Dictionary getDictionaryByLabel(DictionaryStaticData dictionaryStaticData, String label) {
            // 	字典对象
            List<Dictionary> list = dictionaryStaticData.getList();
            // 数据不存在
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            // 遍历当前字典数据
            for (Dictionary dictionary : list) {
                if (dictionary.getLabel().equals(label)) {
                    return dictionary;
                }
            }
            return null;
        }

        /**
         * 字典数据查询
         *
         * @param dictionaryStaticData
         * @param label
         * @return
         */
        public static List<String> getDictionaryValByLikeLabel(DictionaryStaticData dictionaryStaticData, String label) {
            List<String> list = new ArrayList<>();
            // 	字典对象
            List<Dictionary> dictionaryList = dictionaryStaticData.getList();
            // 数据不存在
            if (CollectionUtils.isEmpty(dictionaryList)) {
                return list;
            }
            // 遍历当前字典数据
            for (Dictionary dictionary : dictionaryList) {
                if (dictionary.getLabel().indexOf(label) >= 0) {
                    list.add(dictionary.getValue());
                }
            }
            return list;
        }
    }

    /**
     * 数据字典表
     */
    public enum DictionaryData {
        // 成分分类配置表
        PRODUCT_COMPONENT("PRODUCT_COMPONENT", "GAIA_PRODUCT_COMPONENT", "成分分类配置表",
                "PRO_COMP_CODE AS value, PRO_COMP_NAME AS label",
                "PRO_COMP_CODE AS label, PRO_COMP_NAME AS value",
                "PRO_COMP_STATUS = '0'", ""),
        // 全部商品分类配置表
        PRODUCT_CLASS_ALL("PRODUCT_CLASS_ALL", "GAIA_PRODUCT_CLASS", "商品分类配置表",
                "PRO_CLASS_CODE AS value, PRO_CLASS_NAME AS label",
                "PRO_CLASS_CODE AS label, PRO_CLASS_NAME AS value",
                "PRO_CLASS_STATUS = '0'", ""),
        // 药品商品分类配置表
        PRODUCT_CLASS_DRUG("PRODUCT_CLASS_DRUG", "GAIA_PRODUCT_CLASS", "商品分类配置表",
                "PRO_CLASS_CODE AS value, PRO_CLASS_NAME AS label",
                "PRO_CLASS_CODE AS label, PRO_CLASS_NAME AS value",
                "PRO_CLASS_STATUS = '0' AND(LEFT (PRO_CLASS_CODE, 1) = '1' OR LEFT (PRO_CLASS_CODE, 1) = '2' OR LEFT (PRO_CLASS_CODE, 1) = '3' )", ""),
        // 非药品商品分类配置表
        PRODUCT_CLASS_NONDRUG("PRODUCT_CLASS_NONDRUG", "GAIA_PRODUCT_CLASS", "商品分类配置表",
                "PRO_CLASS_CODE AS value, PRO_CLASS_NAME AS label",
                "PRO_CLASS_CODE AS label, PRO_CLASS_NAME AS value",
                "PRO_CLASS_STATUS = '0' AND(LEFT (PRO_CLASS_CODE, 1) != '1' AND LEFT (PRO_CLASS_CODE, 1) != '2' AND LEFT (PRO_CLASS_CODE, 1) != '3' )", ""),
        // 经营范围类型配置表
        BUSINESS_SCOPE_CLASS("BUSINESS_SCOPE_CLASS", "GAIA_BUSINESS_SCOPE_CLASS", "经营范围类型配置表",
                "BSC_CODE AS value, BSC_CODE_NAME AS label",
                "BSC_CODE AS label, BSC_CODE_NAME AS value",
                "BSC_CODE_STATUS = '0'", ""),
        // 证照资质类型配置表
        LICENCE_CLASS("LICENCE_CLASS", "GAIA_LICENCE_CLASS", "证照资质类型配置表",
                "PER_CODE AS value, PER_CODE_NAME AS label",
                "PER_CODE AS label, PER_CODE_NAME AS value",
                "PER_CODE_STATUS = '0'", ""),
        // 商品剂型配置表
        PRODUCT_FORM("PRODUCT_FORM", "GAIA_PRODUCT_FORM", "商品剂型配置表",
                "DISTINCT PRO_FORM AS value, PRO_FORM AS label",
                "DISTINCT PRO_FORM AS label, PRO_FORM AS value",
                "PRO_FORM_STATUS = '0'", ""),
        // 细分剂型
        PRO_PARTFORM("PRO_PARTFORM", "GAIA_PRODUCT_FORM", "商品剂型配置表",
                "PRO_FORM_PART AS value, PRO_FORM_PART AS label",
                "PRO_FORM_PART AS label, PRO_FORM_PART AS value",
                "PRO_FORM_STATUS = '0'", ""),
        // 进项税率
        TAX_CODE1("TAX_CODE1", "GAIA_TAX_CODE", "税率配置表",
                "TAX_CODE AS value, TAX_CODE_NAME AS label, TAX_CODE_VALUE AS codeValue",
                "TAX_CODE AS label, TAX_CODE_NAME AS value, TAX_CODE_VALUE AS codeValue",
                "TAX_CODE_STATUS = '0' AND TAX_CODE_CLASS = '1'", "CAST(TAX_CODE_VALUE AS SIGNED INTEGER)"),
        // 销项税率
        TAX_CODE2("TAX_CODE2", "GAIA_TAX_CODE", "税率配置表",
                "TAX_CODE AS value, TAX_CODE_NAME AS label",
                "TAX_CODE AS label, TAX_CODE_NAME AS value",
                "TAX_CODE_STATUS = '0' AND TAX_CODE_CLASS = '2'", "CAST(TAX_CODE_VALUE AS SIGNED INTEGER)"),
        // 计量单位配置表
        UNIT("UNIT", "GAIA_UNIT", "计量单位配置表",
                "UNIT_CODE AS value, UNIT_NAME AS label",
                "UNIT_CODE AS label, UNIT_NAME AS value",
                "UNIT_STATUS = '0'", ""),
        // 银行主数据表
        BANK("BANK", "GAIA_BANK", "银行主数据表",
                "BANK_CODE AS value, BANK_CODE_NAME AS label",
                "BANK_CODE AS label, BANK_CODE_NAME AS value",
                "BANK_CODE_STATUS = '0'", ""),
        // 采购订单 订单类型配置表
        ORDER_TYPE1("ORDER_TYPE1", "GAIA_ORDER_TYPE", "订单类型配置表",
                "ORD_TYPE AS value, ORD_TYPE_DESC AS label",
                "ORD_TYPE AS label, ORD_TYPE_DESC AS value",
                "ORD_TYPE_STATUS = '0' AND ORD_TYPE_CLASS = '1'", ""),
        // 销售订单 订单类型配置表
        ORDER_TYPE2("ORDER_TYPE2", "GAIA_ORDER_TYPE", "订单类型配置表",
                "ORD_TYPE AS value, ORD_TYPE_DESC AS label",
                "ORD_TYPE AS label, ORD_TYPE_DESC AS value",
                "ORD_TYPE_STATUS = '0' AND ORD_TYPE_CLASS = '2'", ""),
        // 付款条件配置表
        PAYMENT_TYPE("PAYMENT_TYPE", "GAIA_PAYMENT_TYPE", "付款条件配置表",
                "PAY_TYPE AS value, PAY_TYPE_DESC AS label",
                "PAY_TYPE AS label, PAY_TYPE_DESC AS value",
                "PAY_TYPE_STATUS = '0'", ""),
        // 生产企业配置表
        PRODUCT_FACTORY("PRODUCT_FACTORY", "GAIA_PRODUCT_FACTORY", "生产企业配置表",
                "PRO_FACTORY_CODE AS value, PRO_FACTORY_NAME AS label",
                "PRO_FACTORY_CODE AS label, PRO_FACTORY_NAME AS value",
                "PRO_FACTORY_STATUS = '0'", ""),
        // 商品自分类配置表
        PRODUCT_SCLASS("PRODUCT_SCLASS", "GAIA_PRODUCT_SCLASS", "商品自分类",
                "PRO_SCLASS_CODE AS value, PRO_SCLASS_NAME AS label",
                "PRO_SCLASS_NAME AS label, PRO_SCLASS_CODE AS value",
                "PRO_SCLASS_STATUS = '0'", ""),
        // 经营类别
        PRO_JYLB("PRO_JYLB", "GAIA_JYFWOO_DATA", "经营类别",
                "JYFW_ID AS value, JYFW_NAME AS label",
                "JYFW_NAME AS label, JYFW_ID AS value",
                "", "");
        private String code;
        private String table;
        private String name;
        private String field1;
        private String field2;
        private String condition;
        private String sortFields;

        DictionaryData(String code, String table, String name, String field1, String field2, String condition, String sortFields) {
            this.code = code;
            this.table = table;
            this.name = name;
            this.field1 = field1;
            this.field2 = field2;
            this.condition = condition;
            this.sortFields = sortFields;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTable() {
            return table;
        }

        public void setTable(String table) {
            this.table = table;
        }

        public String getField1() {
            return field1;
        }

        public void setField1(String field1) {
            this.field1 = field1;
        }

        public String getField2() {
            return field2;
        }

        public void setField2(String field2) {
            this.field2 = field2;
        }

        public String getCondition() {
            return condition;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }

        public String getSortFields() {
            return sortFields;
        }

        public void setSortFields(String sortFields) {
            this.sortFields = sortFields;
        }

        //通过value获取对应的枚举对象
        public static DictionaryData getDictionary(String value) {
            for (DictionaryData dictionaryData : DictionaryData.values()) {
                if (value.equals(dictionaryData.getCode())) {
                    return dictionaryData;
                }
            }
            return null;
        }

        /**
         * 获取表示值对应的对象
         *
         * @param list
         * @param value
         * @return
         */
        public static Dictionary getDictionaryByValue(List<Dictionary> list, String value) {
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            for (Dictionary dictionary : list) {
                if (value.equals(dictionary.getValue())) {
                    return dictionary;
                }
            }
            return null;
        }
    }

    /**
     * 首营状态
     */
    public enum GspinfoStauts {

        APPROVAL("0", "审批中"),
        APPROVED("1", "已审批"),
        ABANDONED("2", "已废弃");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        GspinfoStauts(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 采购主体
     */
    public enum SaleSubject {

        SUBJECTVAL("1", "连锁"),
        SUBJECTVED("2", "门店");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        SaleSubject(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 供应商状态
     */
    public enum SupplierStauts {

        SUPPLIERNORMAL("0", "正常"),
        SUPPLIERDISNORMAL("1", "停用");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        SupplierStauts(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 商品状态
     */
    public enum GoodsStauts {

        GOODSNORMAL("0", "正常"),
        GOODSDEACTIVATE("1", "停用");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        GoodsStauts(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 客户状态
     */
    public enum CustomerStauts {

        CUSTOMERNORMAL("0", "正常"),
        CUSTOMERDISNORMAL("1", "停用");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        CustomerStauts(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 客户支付方式  (1-银行转账、2-承兑汇票、3-线下票据；"4", "赊销" "5", "电汇")
     */
    public enum CusPayMode {

        BANK("1", "银行转账"),
        TICKET("2", "承兑汇票"),
        OFFLINE("3", "线下票据"),
        SALEONCREDIT("4", "赊销"),
        WIRETRANSFER("5", "电汇");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        CusPayMode(String code, String name) {
            this.code = code;
            this.name = name;
        }

    }

    /**
     * 客户信用检查点(1-销售订单，2-发货过账，3-开票)
     */
    public enum CusCreditCheck {

        ORDER("1", "销售订单"),
        GOODS("2", "发货过账"),
        TICKET("3", "开票");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        CusCreditCheck(String code, String name) {
            this.code = code;
            this.name = name;
        }

    }

    /**
     * 禁止采购
     */
    public enum CusNoSale {

        FORBID("0", "禁止"),
        APPROVED("1", "同意");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        CusNoSale(String code, String name) {
            this.code = code;
            this.name = name;
        }

    }

    /**
     * 禁止退厂
     */
    public enum CusNoReturn {

        FORBID("0", "禁止"),
        APPROVED("1", "同意");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        CusNoReturn(String code, String name) {
            this.code = code;
            this.name = name;
        }

    }

    /**
     * 是否启用信用管理
     */
    public enum CusCreditFlag {

        FORBID("0", "禁止"),
        APPROVED("1", "同意");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        CusCreditFlag(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 验证类型
     */
    public enum ProofType {

        PROOFVAL("0", "新建"),
        PROOFVED("1", "编辑");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        ProofType(String code, String name) {
            this.code = code;
            this.name = name;
        }

    }

    /**
     * 客户分类
     */
    public enum CusClass {
        EXT("1", "外部客户"),
        DC("2", "配送中心"),
        STORE("3", "门店");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        CusClass(String code, String name) {
            this.code = code;
            this.name = name;
        }

        /**
         * 根据值取得第一个匹配的键
         *
         * @param code
         * @return String
         */
        public static String getEnumLabel(String code) {
            for (CusClass c : CusClass.values()) {
                if (StringUtils.isEqual(c.getCode(), code)) {
                    return c.name;
                }
            }
            return null;
        }

    }

    /**
     * 商品状态
     */
    public enum ProStatus {
        PROSTATUS("proStatus", "商品状态"),
        PROPOSITION("proPosition", "商品定位"),
        PRONORETAIL("proNoRetail", "禁止销售"),
        PRONOPURCHASE("proNoPurchase", "禁止采购"),
        PRONODISTRIBUTED("proNoDistributed", "禁止配送"),
        PRONOSUPPLIER("proNoSupplier", "禁止退厂"),
        PRONODC("proNoDc", "禁止退仓"),
        PRONOADJUST("proNoAdjust", "禁止调剂"),
        PRONOSALE("proNoSale", "禁止批发"),
        PRONOAPPLY("proNoApply", "禁止请货"),
        PROIFPART("proIfpart", "是否拆零"),
        PROPARTUINT("proPartUint", "拆零单位"),
        PROPARTRATE("proPartRate", "拆零比例"),
        PROPURCHASEUNIT("proPurchaseUnit", "采购单位"),
        PROPURCHASERATE("proPurchaseRate", "采购单位比例"),
        PROSALEUNIT("proSaleUnit", "销售单位"),
        PROSALERATE("proSaleRate", "销售单位比例"),
        PROMINQTY("proMinQty", "最小订货量"),
        PROIFMED("proIfMed", "是否医保"),
        PROSLAECLASS("proSlaeClass", "销售级别"),
        PROLIMITQTY("proLimitQty", "限购数量");

        private String fieldName;
        private String name;

        public String getFieldName() {
            return fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        ProStatus(String fieldName, String name) {
            this.fieldName = fieldName;
            this.name = name;
        }

        public static List toList() {
            List list = Lists.newArrayList();
            for (CommonEnum.ProStatus proStatus : CommonEnum.ProStatus.values()) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fieldName", proStatus.getFieldName());
                map.put("name", proStatus.getName());
                list.add(map);
            }
            return list;
        }
    }

    /**
     * 采购类型
     */
    public enum PurchaseClass {
        PURCHASECD("Z001", "CD"),
        PURCHASEGD("Z002", "GD"),
        PURCHASEPD("Z003", "PD"),
        PURCHASETD("Z004", "TD"),
        PURCHASEND("Z005", "ND"),
        PURCHASEMD("Z006", "MD"),
        PURCHASECD1("Z007", "CD"),
        PURCHASECD2("Z008", "CD");

        private String fieldName;
        private String name;

        public String getFieldName() {
            return fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        PurchaseClass(String fieldName, String name) {
            this.fieldName = fieldName;
            this.name = name;
        }

        public static List toList() {
            List list = Lists.newArrayList();
            for (CommonEnum.PurchaseClass purchaseClass : CommonEnum.PurchaseClass.values()) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("fieldName", purchaseClass.getFieldName());
                map.put("name", purchaseClass.getName());
                list.add(map);
            }
            return list;
        }
    }

    /**
     * 销售订单类型
     */
    public enum SoType {
        SOR("SOR", "XD"),
        SRE("SRE", "ED"),
        SCR("SCR", "CR"),
        SDR("SDR", "DR"),
        WOR("WOR", "OD"), //变更为OD开头
        ZJY("ZJY", "XD");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        SoType(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public static SoType getSoType(String code) {
            for (SoType soType : CommonEnum.SoType.values()) {
                if (soType.getCode().equals(code)) {
                    return soType;
                }
            }
            return null;
        }
    }

    /**
     * 实体类型
     */
    public enum StoreType {
        STORE("1", "门店"),
        SUPPLIER("2", "供应商"),
        CUSTOMER("3", "客户");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        StoreType(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 条件类型
     */
    public enum AlpConditionType {
        STORE("CT01", "加价%"),
        SUPPLIER("CT02", "加价￥"),
        CUSTOMER("CT03", "目录价");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        AlpConditionType(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 导入类型
     */
    public enum ImportType {
        CREATE_PRODUCT("1_1", "商品新增", "1", "1"),
        CREATE_SUPPLIER("1_2", "供应商新增", "1", "2"),
        CREATE_CUSTOMER("1_3", "客户新增", "1", "3"),
        CREATE_STORE("1_4", "门店新增", "1", "4"),
        EDIT_PRODUCT("2_1", "商品编辑", "2", "1"),
        EDIT_SUPPLIER("2_2", "供应商编辑", "2", "2"),
        EDIT_CUSTOMER("2_3", "客户编辑", "2", "3"),
        EDIT_STORE("2_4", "门店编辑", "2", "4");
//		EXTENT_PRODUCT("3_1", "商品扩展属性", "3", "1");

        private String code;

        private String name;

        private String bulDateType;

        private String bulUpdateType;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getBulDateType() {
            return bulDateType;
        }

        public void setBulDateType(String bulDateType) {
            this.bulDateType = bulDateType;
        }

        public String getBulUpdateType() {
            return bulUpdateType;
        }

        public void setBulUpdateType(String bulUpdateType) {
            this.bulUpdateType = bulUpdateType;
        }

        ImportType(String code, String name, String bulUpdateType, String bulDateType) {
            this.code = code;
            this.name = name;
            this.bulDateType = bulDateType;
            this.bulUpdateType = bulUpdateType;
        }

        public static ImportType getImportType(String code) {
            for (ImportType importType : CommonEnum.ImportType.values()) {
                if (importType.getCode().equals(code)) {
                    return importType;
                }
            }
            return null;
        }
    }

    /**
     * 批量操作 数据类型
     */
    public enum BulDateType {
        PRODUCT("1", "商品"),
        SUPPLIER("2", "供应商"),
        CUSTOMER("3", "客户"),
        STORE("4", "门店");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        BulDateType(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 批量操作 操作类型
     */
    public enum BulUpdateType {
        CREATE("1", "新增"),
        EDIT("2", "编辑"),
        EXTENT("3", "扩展企业");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        BulUpdateType(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 批量操作 操作状态
     */
    public enum BulUpdateStatus {
        WAIT("1", "待提交"),
        ERROR("2", "无合格数据"),
        COMPLETE("3", "已更新");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        BulUpdateStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 业务导入类型
     */
    public enum BusinessImportTypet {
        PRICEADJUSTMENT("1", "调价", 2, PriceAdjustment.class),
        GOODSSTATEUPDATE("2", "商品状态批量更新", 2, GoodsStateUpdate.class),
        PERMITDATAINFO("3", "编辑证照", 1, PermitData.class),
        PERMITADD("4", "新增证照", 1, PermitData.class),
        ADDBUSINESSSCOPE("5", "新增经营范围", 1, AddBusinessScope.class),
        BUSINESSSCOPEINFO("6", "编辑经营范围", 1, AddBusinessScope.class),
        PURCHASERETURNS("7", "采购退货单处理", 2, PurchaseReturns.class),
        DCREPLENISHMENT("8", "DC补货", 3, DcReplenishment.class),
        GOODSRECALL("9", "商品召回", 2, GoodsRecall.class),
        ALLOTPRICE("10", "调拨价格设置", 2, AllotPrice.class),
        SOURCELIST("11", "货源清单", 2, SourceList.class),
        STOREDISTRIBUTION("12", "门店铺货", 2, StoreDistribution.class),
        STOREREPLENISHMENT("13", "门店补货", 1, StoreReplenishment.class),
        INVOICEINFO("14", "发票维护", 2, InvioceInfo.class),
        PRODUCTRELATE("15", "煎药导入", 2, ProductRelate.class),
        COURSEQUESTION("16", "课件题目", 2, CourseQuestion.class),
        PAYMENTMAINTENANCE("17", "付款基础数据维护", 2, PaymentMaintenance.class),
        PRODUCTMINQTY("18", "最小陈列量", 2, StoreMinQtyImportDto.class),
        STORENEED("19", "门店铺货", 2, StoreNeedImportDTO.class),
        ALLOTPRICECUS("20", "客户调拨价格设置", 1, AllotPriceCus.class),
        PURCHASEPO("21", "采购订单导入", 2, PurchasePo.class),
        DCREPLENISIMP("22", "仓库补货参数导入", 2, DcreplenisImp.class),
        PRODUCTBATCHUPDATE("23", "批量修改商品主数据", 1, ProductBatchUpdate.class),
        NODCREPLENISHMENT("24", "无仓库补货导入", 2, NoDcReplenishment.class),
        PRICEGROUPPROD("25", "目录商品价格导入", 2, GroupPriceProduct.class),
        GROUPPRICE("26", "价格组目录组员导入", 2, GroupPrice.class),
        WHOLESALEPRODUCT("27", "渠道商品导入", 1, WholesalePro.class),
        WHOLESALECONSIGNORPRODUCT("28", "批发货主商品导入", 1, WholesaleConsignorPro.class),
        PRICEGROUPPRODUCT("29", "价格组商品导入", 1, PriceGroupProduct.class),
        SALESORDERPRODUCTS("30", "销售订单批量导入", 1, SalesOrderProducts.class),
        PURCHASECONTRACT("31", "采购合同导入", 2, PurchaseContract.class);

        private String code;

        private String name;

        private Integer lineNo;

        private Class<?> cs;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getLineNo() {
            return lineNo;
        }

        public void setLineNo(Integer lineNo) {
            this.lineNo = lineNo;
        }

        public Class<?> getCs() {
            return cs;
        }

        public void setCs(Class<?> cs) {
            this.cs = cs;
        }

        BusinessImportTypet(String code, String name, Integer lineNo, Class<?> cs) {
            this.code = code;
            this.name = name;
            this.lineNo = lineNo;
            this.cs = cs;
        }

        public static BusinessImportTypet getBusinessImportTypet(String code) {
            for (BusinessImportTypet businessImportTypet : CommonEnum.BusinessImportTypet.values()) {
                if (businessImportTypet.getCode().equals(code)) {
                    return businessImportTypet;
                }
            }
            return null;
        }
    }

    // 国家医保品种  启用电子监管码  禁止销售
    // 禁止采购      禁止销售        是否拆零
    // 禁止配送      禁止退厂        禁止退仓
    // 禁止调剂      禁止批发        禁止请货
    // 是否转采购订单
    public enum NoYesStatus {
        NO("0", "否"),
        YES("1", "是");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        NoYesStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * (是,否)状态Integer类型
     */
    public enum NoYesStatusInteger {
        NO(0, "否"),
        YES(1, "是");
        private Integer code;

        private String name;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        NoYesStatusInteger(Integer code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 采购订单审批状态
     */
    public enum PoApproveStatus {
        DRAFT("-10", "暂存"),
        NO_APPROVE("0", "不需审批"),
        APPROVAL("1", "未审批"),
        APPROVED("2", "已审批");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        PoApproveStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }


    /**
     * 采购订单审批状态
     */
    public enum GaiaRetailPrice_PrcClass {
        P001("P001", "P001-零售价"),
        P002("P002", "P002-会员价"),
        P003("P003", "P003-医保价"),
        P004("P004", "P004-会员日价"),
        P005("P005", "P005-拆零价"),
        P006("P006", "P006-网上零售价"),
        P007("P007", "P007-网上会员价");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        GaiaRetailPrice_PrcClass(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public static GaiaRetailPrice_PrcClass getGaiaRetailPrice_PrcClass(String code) {
            for (GaiaRetailPrice_PrcClass price_prcClass : CommonEnum.GaiaRetailPrice_PrcClass.values()) {
                if (price_prcClass.getCode().equals(code)) {
                    return price_prcClass;
                }
            }
            return null;
        }
    }

    /**
     * 匹配状态
     */
    public enum MatchStatus {
        DIDMATCH("0", "未匹配"),
        PARTIALMATCH("1", "部分匹配"),
        EXACTMATCH("2", "完全匹配");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        MatchStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 财务付款与开票组织类型
     */
    public enum FicoCompanyCode {

        STORE("0", "单体"),
        CHAIN("1", "连锁"),
        DC("2", "配送中心");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        FicoCompanyCode(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 付款方式
     */
    public enum PaymentMethod {

        MONTH("0", "月"),
        SEASON("1", "季"),
        HALF_YEAR("2", "半年"),
        YEAR("3", "半年");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        PaymentMethod(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 付款状态
     */
    public enum PaymentStatus {

        UNPAID("0", "未付款"),
        SUCCESS("1", "成功"),
        FAILE("2", "失败");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        PaymentStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 开票状态
     */
    public enum InvoiceStatus {

        UN_INVOICE("0", "未开票"),
        INVOICING("1", "开票中"),
        SUCCESS("2", "已开票"),
        FAILE("3", "开票失败");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        InvoiceStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 支付回调结果
     */
    public enum PayResult {

        TRADE_SUCCESS("TRADE_SUCCESS", "调用成功"),

        PAID("PAID", "支付成功");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        PayResult(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 门店属性
     */
    public enum StoAttribute {
        ATTRIBUTE1("1", "单体"),
        ATTRIBUTE2("2", "连锁"),
        ATTRIBUTE3("3", "加盟"),
        ATTRIBUTE4("4", "门诊");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        StoAttribute(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 短信模版审批状态 0-审批中，1-已审批，2-已拒绝
     * 门店调价审批状态
     */
    public enum GspStatus {
        PROCESS("0", "审批中"),
        FINISH("1", "已审批"),
        REFUSE("2", "已拒绝");

        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        GspStatus(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    /**
     * 状态(0:未推送, 1:已推送, 2:立即执行, 3:待审批, 4:审批通过, 5:审批拒绝)
     */
    public enum GsmImpl {
        NOT_PUSHED("0", "未推送"),
        PUSHED("1", "已推送"),
        IMMEDIATE_EXECUTION("2", "立即执行"),
        PENDING_APPROVAL("3", "待审批"),
        APPROVED("4", "审批通过"),
        REFUSE("5", "审批拒绝");

        private String code;
        private String message;

        GsmImpl(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    /**
     * 发票类型
     */
    public enum InvioceType {
        ORDINARY("1", "增值税普通发票"),
        SPECIAL("2", "增值税专用发票");

        private String code;
        private String message;

        InvioceType(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 支付方式
     */
    public enum PayType {
        SCAN(1, "扫码支付"),
        APP(2, "APP银联支付"),
        GATEWAY(3, "网关支付");

        private Integer code;
        private String message;

        PayType(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 订单类型
     */
    public enum ficoOrderType {
        STORESERVICE(1, "门店服务费"),
        SMSSERVICE(2, "短信充值");

        private Integer code;
        private String message;

        ficoOrderType(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 门店参数配置表GAIA_SD_SYSTEM_PARA
     */
    public enum GaiaSdSystemParaEnum {

        ADJUST_PRICE("ADJUST_PRICE", "门店调价自动审批");

        private String code;
        private String message;

        GaiaSdSystemParaEnum(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 经营范围类型 1：供应商 2：客户 3：其他（连锁、门店、批发）
     */
    public enum JyfwType {

        SUPPLIER(1, "供应商"),
        CUSTOMER(2, "客户"),
        OTHER(3, "其他(连锁、门店、批发)");

        private Integer code;
        private String message;

        JyfwType(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 商品、供应商、客户 主数据修改是否需要翻译
     */
    public enum NeedTranslate {

        NO(0, "不需要翻译"),
        DIC(1, "静态字典"),
        DB(2, "动态字典(调用DB)");

        private Integer code;
        private String message;

        NeedTranslate(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


    /**
     * 全局开头配置表
     */
    public enum GaiaGlobalGspConfigure {

        GSP_PO_APPROVE_STATUS("GSP_PO_APPROVE_STATUS", "采购订单审核时，填写审核日期（1:填写，0:不填写）"),
        GSP_PRO_LSJ("GSP_PRO_LSJ", "商品首营时，零售价是否必填(1:必填，0:非必填)");

        private String code;
        private String message;

        GaiaGlobalGspConfigure(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * wms配置表
     */
    public enum WmsConfrig {

        AUTO_APPOINT_BILL_CREATE_ORDER("AUTO_APPOINT_BILL_CREATE_ORDER", "指定批号,指定货位前置单据自动开单"),
        AUTO_REPLENISH_CREATE_ORDER("AUTO_REPLENISH_CREATE_ORDER", "铺货自动开单"),
        AUTO_SO_CREATE_ORDER("AUTO_SO_CREATE_ORDER", "销售自动开单");

        private String code;
        private String message;

        WmsConfrig(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 经营范围类别（1-经营企业，2-生产企业，3-客户）
     */
    public enum JyfwClass {

        OPERATING_ENTERPRISES("1", "经营企业"),
        MANUFACTURING_ENTERPRISE("2", "生产企业"),
        CUSTOMER("3", "客户");

        private String code;
        private String message;

        JyfwClass(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /**
     * 经营范围类别（1-经营企业，2-生产企业，3-客户）
     */
    public enum JyfwEditFlowField {

        CUSTOMER("editCusScope", "客户"),
        SUPPLIER("editSupScope", "供应商");

        private String code;
        private String message;

        JyfwEditFlowField(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public enum SalesPriceType {
        PRO_LSJ("1", "零售价"),
        PRO_HYJ("2", "会员价"),
        PRO_GPJ("3", "国批价"),
        PRO_GLJ("4", "国零价"),
        PRO_YSJ1("5", "预设售价1"),
        PRO_YSJ2("6", "预设售价2"),
        PRO_YSJ3("7", "预设售价3"),
        BAT_PO_PRICE("8", "最近进价");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        SalesPriceType(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    public enum ConType {
        ConType0("0", "待审核"),
        ConType1("1", "已审核"),
        ConType2("2", "修改待审核"),
        ConType3("3", "已拒绝");
        private String code;

        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        ConType(String code, String name) {
            this.code = code;
            this.name = name;
        }
    }

}
