package com.gov.purchase.constants;

/***
 * @desc: 消息枚举类
 * @author: ryan
 * @createTime: 2021/6/16 10:49
 **/
public enum SdMessageTypeEnum {

    NEW_DISTRIBUTION("GAIA_MM_010311", "newProductDistributionPlan", "新品铺货计划"),
    STORE_DISTRIBUTION("GAIA_MM_010309", "storeDistribution2", "门店铺货（新）"),;

    public final String code;
    public final String page;
    public final String remark;

    SdMessageTypeEnum(String code, String page,String remark) {
        this.code = code;
        this.page = page;
        this.remark = remark;
    }
}
