package com.gov.purchase.constants;

/***
 * @desc: 补货类型枚举
 * @author: ryan
 * @createTime: 2021/6/29 11:25
 **/
public enum DeliveryModeEnum {

    SELF_PURCHASE("1", "自采"),
    COMPANY_SEND("2", "公司配送"),
    ENTRUST_SEND("3", "委托配送"),
    INTERNAL_PIFA("4", "内部批发"),
    INTERNAL_ENTRUST_SEND("5", "内部委托配送"),
    DELIVERY_6("6", "全量委托配送"),
    DELIVERY_7("7", "第三方委托配送"),
    DELIVERY_8("8", "第三方委托配送2"),
    DELIVERY_9("9", "双仓配送");

    public final String type;
    public final String message;

    DeliveryModeEnum(String type, String message) {
        this.type = type;
        this.message = message;
    }
}
