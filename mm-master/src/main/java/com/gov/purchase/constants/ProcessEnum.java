package com.gov.purchase.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProcessEnum {
    GAIA_WF_014("GAIA_WF_014", "商品首营流程"),
    GAIA_WF_015("GAIA_WF_015", "供应商首营流程"),
    GAIA_WF_016("GAIA_WF_016", "批发客户首营流程"),
    GAIA_WF_017("GAIA_WF_017", "采购订单审批流程"),
    GAIA_WF_018("GAIA_WF_018", "采购付款申请审批流程"),
    GAIA_WF_027("GAIA_WF_027", "短信模板审批流程"),
    GAIA_WF_029("GAIA_WF_029", "营销任务审批流程"),
    GAIA_WF_031("GAIA_WF_031", "薪酬核算审批流程"),
    GAIA_WF_032("GAIA_WF_032", "供应商付款审批流程"),
    GAIA_WF_033("GAIA_WF_033", "商品调价审批流程"),
    GAIA_WF_041("GAIA_WF_041", "商品主数据修改审批流程"),
    GAIA_WF_044("GAIA_WF_044", "补充首营"),
    GAIA_WF_048("GAIA_WF_048", "供应商修改审批回调"),
    GAIA_WF_049("GAIA_WF_049", "客户修改审批回调");


    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static ProcessEnum getEnumByCode(String code) {
        for (ProcessEnum processEnum : ProcessEnum.values()) {
            if (processEnum.getCode().equals(code)) {
                return processEnum;
            }
        }
        return null;
    }
}
