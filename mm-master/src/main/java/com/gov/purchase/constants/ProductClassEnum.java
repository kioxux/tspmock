package com.gov.purchase.constants;

/**
 * 商品分类枚举
 * @author Zhangchi
 * @since 2021/11/09/16:21
 */
public enum ProductClassEnum {
    DRUGS("01","1"),
    HEALTH_FOOD("06","4"),
    APPARATUS("02","5");
    public final String code;
    public final String proClass;

    ProductClassEnum(String code, String proClass) {
        this.code = code;
        this.proClass = proClass;
    }
}
