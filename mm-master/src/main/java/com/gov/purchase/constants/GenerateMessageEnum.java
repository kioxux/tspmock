package com.gov.purchase.constants;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/21 11:11
 **/
public enum GenerateMessageEnum {

    NOT_ENOUGH_STOCK(1, "存在新品铺货计划，入库库存不足消息"),
    NOT_HAS_PLAN(2,"无新品铺货计划，可手动铺货消息提醒"),
    NOT_HAS_PLAN_WEEKLY(3, "无铺货计划，可手动铺货消息提醒（每周）"),
    EXIST_DISTRIBUTION_DIFF(4, "存在铺货差异，需要补铺消息提醒"),;

    public final Integer type;
    public final String message;

    GenerateMessageEnum(Integer type, String message) {
        this.type = type;
        this.message = message;
    }
}
