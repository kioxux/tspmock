package com.gov.purchase.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MatTypeEnum {
    CD("CD", "采购"),
    GD("GD", "退厂"),
    XD("XD", "销售"),
    ED("ED", "销退"),
    PD("PD", "配送"),
    TD("TD", "退库"),
    LS("LS", "零售"),
    MD("MD", "互调退库"),
    ND("ND", "互调配送"),
    BD("BD", "报损"),
    ZD("ZD", "自用"),
    SY("SY", "损益"),
    ZT("ZT", "移库"),
    LX("LX", "零售（代销）"),
    CX("CX", "采购（代销）"),
    PX("PX", "配送（代销）");

    private String code;

    private String label;

    public void setCode(String code) {
        this.code = code;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public static MatTypeEnum getEnumByCode(String code) {
        for (MatTypeEnum e : MatTypeEnum.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        return null;
    }
}
