package com.gov.purchase.constants;

import java.util.regex.Pattern;

/**
 * 通用常量
 */
public class CommonConstants {

    /*** 短信模板id*/
    public static final Pattern pattern = Pattern.compile("<(.+?)>");

    /**
     * 短信模板门店名称占位符
     **/
    public static final String SMS_COMTENT_PD_REGEX = "\\{}";

    /**
     * 营销活动 会员姓名占位符
     **/
    public static final String SMS_COMTENT_PD_NAME = "{KEY:MEMBERNAME}";

    /**
     * 营销活动 会员积分占位符
     **/
    public static final String SMS_COMTENT_PD_SCOPE = "{KEY:MEMBERSCOPE}";

    /**
     * 空字符串
     **/
    public static final String EMPTY_STRING = "";

    /**
     * 短信发送线程锁
     **/
    public static final String LOCK_SEND_MARKETING_SMS = "sendMarketingSms";

    /**
     * 营销任务短信发送线程锁
     **/
    public static final String LOCK_SEND_MARKET_TASK_SMS = "sendMarketTaskSms";

    /*** 目前发票额度千元版最大9999  */
    public static final String INVICE_MAX_QUOTA = "9999.0000";

    /**
     * 补货表
     */
    public static final String GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID = "GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID";

    /**
     * 消息表
     */
    public static final String GAIA_SD_MESSAGE_GSM_VOUCHER_ID = "GAIA_SD_MESSAGE_GSM_VOUCHER_ID_{}_{}_{}";

    /**
     * 采购表
     */
    public static final String GAIA_PO_HEADER_PO_ID = "GAIA_PO_HEADER_PO_ID";

    /**
     * 采购表
     */
    public static final String GAIA_SO_HEADER_SO_ID = "GAIA_SO_HEADER_SO_ID";

    /**
     * 合同表
     */
    public static final String GAIA_CONTRACT_Z = "GAIA_CONTRACT_Z_{}_{}";
}
