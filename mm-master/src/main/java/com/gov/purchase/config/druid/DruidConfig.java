package com.gov.purchase.config.druid;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author hl
 * @date : 2018/10/13 下午3:53
 * @description
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(DruidProperties.class)
public class DruidConfig {

    @Resource
    private DruidProperties druidProperties;

    @Bean
    public ServletRegistrationBean view() {
        return new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
    }

    @Bean
    public DataSource dataSource() {
        DruidDataSource druid = new DruidDataSource();
        druid.setEnable(true);
        druid.setKeepAlive(true);
        druid.setKillWhenSocketReadTimeout(true);
        druid.setPoolPreparedStatements(true);
        druid.setUseGlobalDataSourceStat(true);
        druid.setValidationQuery("select 1");
        druid.setDbType("mysql");
        druid.setUrl(druidProperties.getUrl());
        druid.setUsername(druidProperties.getUsername());
        druid.setPassword(druidProperties.getPassword());
        druid.setDriverClassName(druidProperties.getDriverClassName());

        druid.setInitialSize(druidProperties.getInitialSize());
        druid.setMinIdle(druidProperties.getMinIdle());
        druid.setMaxActive(druidProperties.getMaxActive());
        druid.setMaxWait(druidProperties.getMaxWait());
        druid.setTimeBetweenEvictionRunsMillis(druidProperties.getTimeBetweenEvictionRunsMillis());
        druid.setMinEvictableIdleTimeMillis(druidProperties.getMinEvictableIdleTimeMillis());
        druid.setTestWhileIdle(true);
        druid.setTestOnBorrow(false);
        druid.setTestOnReturn(false);
        druid.setConnectionProperties(druidProperties.getConnectionProperties());
        try {
            druid.setFilters(druidProperties.getFilters());
        } catch (SQLException e) {
            log.error("datasource配置filter失败", e);
        }

        return druid;
    }
}