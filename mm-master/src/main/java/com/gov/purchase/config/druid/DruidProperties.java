package com.gov.purchase.config.druid;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author hl
 * @date : 2018/10/13 下午4:28
 * @description
 */
@Data
@ConfigurationProperties("spring.datasource.druid")
public class DruidProperties {

    private String url;
    private String username;
    private String password;
    private String driverClassName = "com.mysql.jdbc.Driver";
    private int initialSize = 5;
    private int minIdle = 5;
    private int maxActive = 20;
    private int maxWait = 60000;
    private int timeBetweenEvictionRunsMillis = 60000;
    /**
     * 配置一个连接在池中最小生存的时间，单位是毫秒
     */
    private long minEvictableIdleTimeMillis = 300000;
    /**
     * 配置监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于防火墙
     */
    private String filters = "stat,slf4j";
    /**
     * 通过connectProperties属性来打开mergeSql功能；慢SQL记录
     */
    private String connectionProperties = "druid.stat.mergeSql=false;druid.stat.slowSqlMillis=5000";


}