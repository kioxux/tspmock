package com.gov.purchase.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.gov.purchase.common.request.RequestJsonHandlerMethodArgumentResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class GysConfig extends WebMvcConfigurationSupport {
//   @Bean
//   public SecurityInterceptor getSecurityInterceptor() {
//      return new SecurityInterceptor();
//   }

//   public void addInterceptors(InterceptorRegistry registry) {
//      List<String> url = new ArrayList();
//      registry.addInterceptor(this.getSecurityInterceptor()).addPathPatterns(new String[]{"/**"}).excludePathPatterns(url);
//      super.addInterceptors(registry);
//   }

   @Bean
   public CorsFilter corsFilter() {
      UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
      CorsConfiguration corsConfiguration = new CorsConfiguration();
      corsConfiguration.setAllowCredentials(true);
      corsConfiguration.addAllowedOrigin("*");
      corsConfiguration.addAllowedHeader("*");
      corsConfiguration.addAllowedMethod("*");
      urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
      return new CorsFilter(urlBasedCorsConfigurationSource);
   }

   public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
      super.configureMessageConverters(converters);
      FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
      FastJsonConfig fastJsonConfig = new FastJsonConfig();
      fastJsonConfig.setSerializerFeatures(new SerializerFeature[]{SerializerFeature.DisableCircularReferenceDetect, SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullStringAsEmpty});
      fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");
      List<MediaType> fastMediaTypes = new ArrayList();
      fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
      fastConverter.setSupportedMediaTypes(fastMediaTypes);
      fastConverter.setFastJsonConfig(fastJsonConfig);
      converters.add(fastConverter);
   }

   public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
      argumentResolvers.add(new RequestJsonHandlerMethodArgumentResolver());
   }
}
