package com.gov.purchase.config;

import com.gov.purchase.common.request.RequestJsonHandlerMethodArgumentResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 *
 */
@Configuration
public class WebAppConfigurer implements WebMvcConfigurer {

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new RequestJsonHandlerMethodArgumentResolver());
    }

    @Bean
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        //核心工作线程数
        threadPoolTaskExecutor.setCorePoolSize(1);
        //最大工作线程数
        threadPoolTaskExecutor.setMaxPoolSize(10);
        //允许线程空闲时间
        threadPoolTaskExecutor.setKeepAliveSeconds(10);
        //核心线程满之后，排队的队列大小
        threadPoolTaskExecutor.setQueueCapacity(20);
        //拒绝策略
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return threadPoolTaskExecutor;
    }
}
