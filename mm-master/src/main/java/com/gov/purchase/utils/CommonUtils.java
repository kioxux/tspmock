package com.gov.purchase.utils;

import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.constants.CommonConstants;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.entity.GaiaPoHeader;
import com.gov.purchase.mapper.GaiaPoHeaderMapper;
import com.gov.purchase.mapper.GaiaSoHeaderMapper;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

@Service
public class CommonUtils {

    /**
     * 采购凭证号生成
     *
     * @param oderType
     * @param year
     * @return
     */
    public String getPoId(String client, String oderType, String year, GaiaPoHeaderMapper gaiaPoHeaderMapper, RedisClient redisClient) {

        String NumName = "";

        if (oderType.equals(CommonEnum.PurchaseClass.PURCHASECD.getFieldName()) ||
                oderType.equals(CommonEnum.PurchaseClass.PURCHASECD1.getFieldName()) ||
                oderType.equals(CommonEnum.PurchaseClass.PURCHASECD2.getFieldName())) {
            NumName = CommonEnum.PurchaseClass.PURCHASECD.getName();
        } else if (oderType.equals(CommonEnum.PurchaseClass.PURCHASEGD.getFieldName())) {
            NumName = CommonEnum.PurchaseClass.PURCHASEGD.getName();
        } else if (oderType.equals(CommonEnum.PurchaseClass.PURCHASEPD.getFieldName())) {
            NumName = CommonEnum.PurchaseClass.PURCHASEPD.getName();
        } else if (oderType.equals(CommonEnum.PurchaseClass.PURCHASETD.getFieldName())) {
            NumName = CommonEnum.PurchaseClass.PURCHASETD.getName();
        } else if (oderType.equals(CommonEnum.PurchaseClass.PURCHASEND.getFieldName())) {
            NumName = CommonEnum.PurchaseClass.PURCHASEND.getName();
        } else if (oderType.equals(CommonEnum.PurchaseClass.PURCHASEMD.getFieldName())) {
            NumName = CommonEnum.PurchaseClass.PURCHASEMD.getName();
        }

        GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
        gaiaPoHeader.setPoId(NumName + year);
        gaiaPoHeader.setClient(client);

        // 搜索目前采购号
        String poId = gaiaPoHeaderMapper.getPoId(gaiaPoHeader);
        while (redisClient.exists(CommonConstants.GAIA_PO_HEADER_PO_ID + client + poId)) {
            String newId = StringUtils.leftPad(String.valueOf(NumberUtils.toLong(poId.replaceFirst(StringUtils.left(poId, 6), "")) + 1), 6, "0");
            poId = StringUtils.left(poId, 6) + newId;
        }
        redisClient.set(CommonConstants.GAIA_PO_HEADER_PO_ID + client + poId, poId, 1800);
        return poId;
    }

    /**
     * 销售凭证号
     *
     * @param oderType           销售类型
     * @param gaiaSoHeaderMapper 销售主表
     * @return
     */
    public String getPoId(String client, String oderType, GaiaSoHeaderMapper gaiaSoHeaderMapper, RedisClient redisClient) {
        if (!EnumUtils.contains(oderType, CommonEnum.SoType.class)) {
            return null;
        }
        // 号段
        String NumName = CommonEnum.SoType.getSoType(oderType).getName();
        String soId = gaiaSoHeaderMapper.getSoId(client, NumName + DateUtils.getCurrentDateStr("yyyy"));
        while (redisClient.exists(CommonConstants.GAIA_SO_HEADER_SO_ID + client + soId)) {
            String newId = StringUtils.leftPad(String.valueOf(NumberUtils.toLong(soId.replaceFirst(StringUtils.left(soId, 6), "")) + 1), 6, "0");
            soId = StringUtils.left(soId, 6) + newId;
        }
        redisClient.set(CommonConstants.GAIA_SO_HEADER_SO_ID + client + soId, soId, 1800);
        return soId;
    }
}
