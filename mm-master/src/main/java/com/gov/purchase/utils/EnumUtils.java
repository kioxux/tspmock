package com.gov.purchase.utils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class EnumUtils {
	public static <T> Boolean contains(Object value, Class<T> enumT, String... methodNames) {
		Map<Object, String> map = EnumToMap(enumT, methodNames);
		if (map != null && map.containsKey(value)) {
			return true;
		}
		return false;
	}

	/**
	 * 枚举转map结合value作为map的key,description作为map的value
	 * @param enumT
	 * @param methodNames
	 * @return enum mapcolloction
	 */
	public static <T> Map<Object, String> EnumToMap(Class<T> enumT,
													String... methodNames) {
		Map<Object, String> enummap = new HashMap<Object, String>();
		if (!enumT.isEnum()) {
			return enummap;
		}
		T[] enums = enumT.getEnumConstants();
		if (enums == null || enums.length <= 0) {
			return enummap;
		}
		int count = methodNames.length;
		String valueMathod = "getCode"; //默认接口value方法
		String desMathod = "getName";//默认接口description方法
		if (count >= 1 && !"".equals(methodNames[0])) { //扩展方法
			valueMathod = methodNames[0];
		}
		if (count == 2 && !"".equals(methodNames[1])) {
			desMathod = methodNames[1];
		}
		for (int i = 0, len = enums.length; i < len; i++) {
			T tobj = enums[i];
			try {
				Object resultValue = getMethodValue(valueMathod, tobj); //获取value值
				if ("".equals(resultValue)) {
					continue;
				}
				Object resultDes = getMethodValue(desMathod, tobj); //获取description描述值
				if ("".equals(resultDes)) { //如果描述不存在获取属性值
					resultDes = tobj;
				}
				enummap.put(resultValue, resultDes + "");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return enummap;
	}

	/**
	 * 根据反射，通过方法名称获取方法值，忽略大小写的
	 * @param methodName
	 * @param obj
	 * @param args
	 * @return return value
	 */
	private static <T> Object getMethodValue(String methodName, T obj,
											 Object... args) {
		Object result = "";
		// boolean isHas = false;
		try {
			/********************************* start *****************************************/
			Method[] methods = obj.getClass().getMethods(); //获取方法数组，这里只要共有的方法
			if (methods.length <= 0) {
				return result;
			}
			// String methodstr=Arrays.toString(obj.getClass().getMethods());
			// if(methodstr.indexOf(methodName)<0){ //只能粗略判断如果同时存在 getValue和getValues可能判断错误
			// return result;
			// }
			// List<Method> methodNamelist=Arrays.asList(methods); //这样似乎还要循环取出名称
			Method method = null;
			for (int i = 0, len = methods.length; i < len; i++) {
				if (methods[i].getName().equalsIgnoreCase(methodName)) { //忽略大小写取方法
					// isHas = true;
					methodName = methods[i].getName(); //如果存在，则取出正确的方法名称
					method = methods[i];
					break;
				}
			}
			// if(!isHas){
			// return result;
			// }
			/*************************** end ***********************************************/
			// Method method = obj.getClass().getDeclaredMethod(methodName); ///确定方法
			if (method == null) {
				return result;
			}
			result = method.invoke(obj, args); //方法执行
			if (result == null) {
				result = "";
			}
			return result; //返回结果
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
