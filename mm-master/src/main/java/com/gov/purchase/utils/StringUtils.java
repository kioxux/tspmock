package com.gov.purchase.utils;

import com.github.stuxuhai.jpinyin.PinyinHelper;
import org.springframework.util.DigestUtils;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author jiangt
 */

public class StringUtils extends org.apache.commons.lang3.StringUtils {


    /**
     * 判断是否不为空，不空返回true
     *
     * @param str
     * @return boolean
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 替换字串中所有空格
     *
     * @param str 字串
     * @return 处理后字串
     */
    public static String replaceAllSpace(String str) {
        return org.apache.commons.lang3.StringUtils.isBlank(str) ? null : str.replaceAll(" ", "");
    }

    /**
     * 替换字符串首尾空格
     *
     * @param str 字符串
     * @return 处理后字符串
     */
    public static String trimSpace(String str) {
        return org.apache.commons.lang3.StringUtils.isBlank(str) ? null : str.trim();
    }

    /**
     * md5转码
     *
     * @param str 字符串
     * @return 处理后字符串
     */
    public static String getMD5(String str) {
        return DigestUtils.md5DigestAsHex(str.getBytes());
    }

    /**
     * md5 16 位
     *
     * @param md5
     * @return
     */
    public static String getMD5TO16(String md5) {
        return md5.substring(8, 24);
    }

    public static String upperFirst(String str) {
        if (StringUtils.isBlank(str)) {
            return "";
        } else {
            return str.substring(0, 1).toUpperCase() + str.substring(1);
        }
    }

    /**
     * 判断两个String 是否相等， 如果都是null 也表示相等
     *
     * @param s1
     * @param s2
     * @return boolean
     */
    public static boolean isEqual(String s1, String s2) {
        if (s1 == null) {
            s1 = "";
        }
        if (s2 == null) {
            s2 = "";
        }

        return (s1.equals(s2));
    }


    /**
     * 判断是否是数字
     *
     * @param s String
     * @return boolean
     */
    public final static boolean isNumeric(String s) {
        if (s != null && !"".equals(s.trim()))
            return s.matches("^[0-9]*$");
        else
            return false;
    }

    /**
     * 字符串连接
     *
     * @param val
     * @return
     */
    public static String concat(String joiner, String... val) {
        StringBuilder result = new StringBuilder();
        if (val == null || val.length == 0) {
            return result.toString();
        }
        for (int i = 0; i < val.length; i++) {
            if (StringUtils.isNotBlank(val[i])) {
                result.append(joiner).append(val[i]);
            }
        }
        if (result.length() > 0) {
            result.deleteCharAt(0);
        }
        return result.toString();
    }

    /**
     * 日期格式化
     *
     * @param date
     * @return
     */
    public static String formatDate(String joiner, String date) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        if (date.length() != 8) {
            return date;
        }
        if (StringUtils.isBlank(joiner)) {
            return date;
        }
        StringBuilder sb = new StringBuilder(date);
        sb.insert(4, joiner);
        sb.insert(7, joiner);
        return sb.toString();
    }

    /**
     * 获取字符串拼音的第一个字母
     *
     * @param chinese
     * @return
     */
    public static String ToFirstChar(String chinese) {
        String pingyin = PinyinHelper.getShortPinyin(chinese).toUpperCase();
        pingyin = pingyin.replaceAll("[^A-Z]", "");
        return pingyin;
    }

    /**
     * 字符串传参
     *
     * @param text
     * @param args
     * @return
     */
    public static String parse(String text, Object... args) {
        return parse("{", "}", text, args);
    }

    /**
     * 将字符串text中由openToken和closeToken组成的占位符依次替换为args数组中的值
     *
     * @param openToken
     * @param closeToken
     * @param text
     * @param args
     * @return
     */
    private static String parse(String openToken, String closeToken, String text, Object... args) {
        if (args == null || args.length <= 0) {
            return text;
        }
        int argsIndex = 0;
        if (text == null || text.isEmpty()) {
            return "";
        }
        char[] src = text.toCharArray();
        int offset = 0;
        // search open token
        int start = text.indexOf(openToken, offset);
        if (start == -1) {
            return text;
        }
        final StringBuilder builder = new StringBuilder();
        StringBuilder expression = null;
        while (start > -1) {
            if (start > 0 && src[start - 1] == '\\') {
                // this open token is escaped. remove the backslash and continue.
                builder.append(src, offset, start - offset - 1).append(openToken);
                offset = start + openToken.length();
            } else {
                // found open token. let's search close token.
                if (expression == null) {
                    expression = new StringBuilder();
                } else {
                    expression.setLength(0);
                }
                builder.append(src, offset, start - offset);
                offset = start + openToken.length();
                int end = text.indexOf(closeToken, offset);
                while (end > -1) {
                    if (end > offset && src[end - 1] == '\\') {
                        // this close token is escaped. remove the backslash and continue.
                        expression.append(src, offset, end - offset - 1).append(closeToken);
                        offset = end + closeToken.length();
                        end = text.indexOf(closeToken, offset);
                    } else {
                        expression.append(src, offset, end - offset);
                        offset = end + closeToken.length();
                        break;
                    }
                }
                if (end == -1) {
                    // close token was not found.
                    builder.append(src, start, src.length - start);
                    offset = src.length;
                } else {
                    ///////////////////////////////////////仅仅修改了该else分支下的个别行代码////////////////////////
                    String value = (argsIndex <= args.length - 1) ?
                            (args[argsIndex] == null ? "" : args[argsIndex].toString()) : expression.toString();
                    builder.append(value);
                    offset = end + closeToken.length();
                    argsIndex++;
                    ////////////////////////////////////////////////////////////////////////////////////////////////
                }
            }
            start = text.indexOf(openToken, offset);
        }
        if (offset < src.length) {
            builder.append(src, offset, src.length - offset);
        }
        return builder.toString();
    }

    public static boolean equals(String var1, String var2) {
        if (isBlank(var1) && isBlank(var2)) {
            return true;
        }
        return org.apache.commons.lang3.StringUtils.equals(var1, var2);
    }

    /**
     * 下划线转驼峰
     */
    public static String lineToHump(String str) {
        Pattern linePattern = Pattern.compile("_(\\w)");
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 对象属性值
     *
     * @param fieldName 属性名
     * @param o         对象
     * @return 返回值
     */
    public static Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[]{});
            Object value = method.invoke(o, new Object[]{});
            return value;
        } catch (Exception e) {
            return null;
        }
    }
}
