package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaTaxCode;
import com.gov.purchase.module.base.dto.SearchValBasic;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaTaxCodeMapper {
    int deleteByPrimaryKey(String taxCode);

    int insert(GaiaTaxCode record);

    int insertSelective(GaiaTaxCode record);

    GaiaTaxCode selectByPrimaryKey(String taxCode);

    int updateByPrimaryKeySelective(GaiaTaxCode record);

    int updateByPrimaryKey(GaiaTaxCode record);

    /**
     * 取得进项税率
     */
    List<String> getProInputTax(@Param("searchValBasic") SearchValBasic searchValBasic);

    List<GaiaTaxCode> selectTaxList();
}