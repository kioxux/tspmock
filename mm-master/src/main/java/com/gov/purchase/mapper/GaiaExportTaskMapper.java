package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaExportTask;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaExportTaskMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GaiaExportTask record);

    int insertSelective(GaiaExportTask record);

    GaiaExportTask selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaExportTask record);

    int updateByPrimaryKeyWithBLOBs(GaiaExportTask record);

    int updateByPrimaryKey(GaiaExportTask record);

    List<GaiaExportTask> getExportTaskList(@Param("client") String client, @Param("userId") String userId, @Param("getStatus") Integer getStatus,
                                           @Param("getType") Integer getType, @Param("getDateStart") String getDateStart, @Param("getDateEnd") String getDateEnd);

    void delExportTaskList(@Param("ids") List<Integer> ids);
}