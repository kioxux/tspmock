package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPermitData;
import com.gov.purchase.entity.GaiaPermitDataKey;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.qa.dto.GaiaPermitDataExtendDto;
import com.gov.purchase.module.qa.dto.QueryPermitDataInfoRequestDto;
import com.gov.purchase.module.qa.dto.QueryPermitDataListRequestDto;
import com.gov.purchase.module.qa.dto.QueryPermitDataListResponseDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaPermitDataMapper {
    int deleteByPrimaryKey(GaiaPermitDataKey key);

    int insert(GaiaPermitData record);

    int insertSelective(GaiaPermitData record);

    GaiaPermitData selectByPrimaryKey(GaiaPermitDataKey key);

    int updateByPrimaryKeySelective(GaiaPermitData record);

    int updateByPrimaryKey(GaiaPermitData record);

    List<QueryPermitDataListResponseDto> queryPermitDataList(QueryPermitDataListRequestDto dto);

    /**
     * 加盟商 + 实体类型 + 实体编号 查询资质数据信息表（GAIA_PERMIT_DATA）
     *
     * @param dto
     * @return
     */
    List<GaiaPermitDataExtendDto> getPermitDataInfo(QueryPermitDataInfoRequestDto dto);

    /**
     * 批量查询
     *
     * @param searchValBasicMap
     * @return
     */
    List<GaiaPermitDataExtendDto> selectPermitBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);

    /**
     * 明细数据查询
     * @param searchValBasicMap
     * @return
     */
    List<GaiaPermitDataExtendDto> selectPermitDetailBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap,
                                                          @Param("client") String client,
                                                          @Param("per_entity_class") String per_entity_class,
                                                          @Param("per_entity_id") String per_entity_id
    );
}