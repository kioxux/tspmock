package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdReplenishPara;
import com.gov.purchase.entity.GaiaSdReplenishParaKey;

public interface GaiaSdReplenishParaMapper {
    int deleteByPrimaryKey(GaiaSdReplenishParaKey key);

    int insert(GaiaSdReplenishPara record);

    int insertSelective(GaiaSdReplenishPara record);

    GaiaSdReplenishPara selectByPrimaryKey(GaiaSdReplenishParaKey key);

    int updateByPrimaryKeySelective(GaiaSdReplenishPara record);

    int updateByPrimaryKey(GaiaSdReplenishPara record);
}