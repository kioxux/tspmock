package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaDcReplenishExcludKey;
import com.gov.purchase.module.replenishment.dto.GaiaDcReplenishExcludDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaDcReplenishExcludMapper {
    int deleteByPrimaryKey(GaiaDcReplenishExcludKey key);

    int insert(GaiaDcReplenishExcludKey record);

    int insertSelective(GaiaDcReplenishExcludKey record);

    int deleteByDc(@Param("client") String client, @Param("gdreSite") String gdreSite);

    int insertBatch(@Param("list") List<GaiaDcReplenishExcludDto> list);

    List<GaiaDcReplenishExcludDto> selectByDc(@Param("client") String client, @Param("gdreSite") String gdreSite,
                                           @Param("gdreBtype") Integer gdreBtype, @Param("gdrePtype") Integer gdrePtype
    );
}