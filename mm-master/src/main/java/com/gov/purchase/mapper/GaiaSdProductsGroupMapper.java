package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdProductsGroup;
import com.gov.purchase.entity.GaiaSdProductsGroupKey;

public interface GaiaSdProductsGroupMapper {
    int deleteByPrimaryKey(GaiaSdProductsGroupKey key);

    int insert(GaiaSdProductsGroup record);

    int insertSelective(GaiaSdProductsGroup record);

    GaiaSdProductsGroup selectByPrimaryKey(GaiaSdProductsGroupKey key);

    int updateByPrimaryKeySelective(GaiaSdProductsGroup record);

    int updateByPrimaryKey(GaiaSdProductsGroup record);
}