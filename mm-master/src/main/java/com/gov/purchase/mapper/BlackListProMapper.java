package com.gov.purchase.mapper;

import com.gov.purchase.module.blacklist.dto.BatchNoData;
import com.gov.purchase.module.blacklist.dto.ImportInData;

import java.util.List;

public interface BlackListProMapper {
    List<BatchNoData>selectBatchNo(BatchNoData inData);

    void insertBatchBlackList(List<ImportInData> list);

    void midifiedProOutList(List<ImportInData> list);
}
