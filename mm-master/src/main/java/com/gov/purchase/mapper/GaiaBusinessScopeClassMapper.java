package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBusinessScopeClass;

public interface GaiaBusinessScopeClassMapper {
    int insert(GaiaBusinessScopeClass record);

    int insertSelective(GaiaBusinessScopeClass record);
}