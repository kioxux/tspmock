package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdRemotePrescribeH;
import com.gov.purchase.entity.GaiaSdRemotePrescribeHKey;

public interface GaiaSdRemotePrescribeHMapper {
    int deleteByPrimaryKey(GaiaSdRemotePrescribeHKey key);

    int insert(GaiaSdRemotePrescribeH record);

    int insertSelective(GaiaSdRemotePrescribeH record);

    GaiaSdRemotePrescribeH selectByPrimaryKey(GaiaSdRemotePrescribeHKey key);

    int updateByPrimaryKeySelective(GaiaSdRemotePrescribeH record);

    int updateByPrimaryKey(GaiaSdRemotePrescribeH record);
}