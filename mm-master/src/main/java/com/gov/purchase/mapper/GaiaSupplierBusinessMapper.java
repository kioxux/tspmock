package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.entity.GaiaSupplierBusinessKey;
import com.gov.purchase.module.base.dto.MainStore;
import com.gov.purchase.module.purchase.dto.PurchaseReturnsRequestDto;
import com.gov.purchase.module.purchase.dto.SupplierListForReturnsRequestDto;
import com.gov.purchase.module.purchase.dto.SupplierListForReturnsResponseDto;
import com.gov.purchase.module.replenishment.dto.SupplierRequestDto;
import com.gov.purchase.module.replenishment.dto.SupplierResponseDto;
import com.gov.purchase.module.supplier.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSupplierBusinessMapper {
    int deleteByPrimaryKey(GaiaSupplierBusinessKey key);

    int insert(GaiaSupplierBusiness record);

    int insertSelective(GaiaSupplierBusiness record);

    GaiaSupplierBusiness selectByPrimaryKey(GaiaSupplierBusinessKey key);

    int updateByPrimaryKeySelective(GaiaSupplierBusiness record);

    int updateByPrimaryKey(GaiaSupplierBusiness record);

    GaiaSupplierBusiness selectByCode(QueryUnDrugSupCheckRequestDto dto);

    List<SupplierListResponseDto> selectQueryProInfo(SupplierListRequestDto dto);

    /**
     * 客户集合下拉框
     *
     * @param client
     * @return
     */
    List<MainStore> mainStore(@Param("client") String client);

    /**
     * 供应商列表
     */
    List<SupplierListForReturnsResponseDto> selectSupplierList(SupplierListForReturnsRequestDto dto);

    /**
     * 付款条款查询
     */
    List<GaiaSupplierBusiness> selectSupPayTerm(PurchaseReturnsRequestDto dto);

    /**
     * 送货前置期字段查询
     */
    List<GaiaSupplierBusiness> selectLeadTime(GaiaSupplierBusiness dto);

    /**
     * DC补货 选择供应商列表
     *
     * @param dto
     * @return
     */
    List<SupplierResponseDto> querySupplierList(SupplierRequestDto dto);

    List<SupplierListForReturnsResponseDto> selectSupplierListByClient(@Param("client") String client);

    /**
     * 供应商状态查询
     */
    GaiaSupplierBusiness selectSupplierStatus(@Param("client") String client, @Param("poSupplierId") String poSupplierId,
                                              @Param("poSiteCode") String poSiteCode);

    /**
     * 供应商状态查询
     */
    List<GaiaSupplierBusiness> selectSupplierListStatus(@Param("client") String client, @Param("supCodes") List<String> supCodes,
                                                        @Param("poSiteCode") String poSiteCode);

    List<GaiaSupplierBusiness> selectSupplierListStatusNoDC(@Param("client") String client, @Param("keyList") List<GaiaSupplierBusinessKey> gaiaProductBusinessKeyList);

    /**
     * 根据加盟商、自编码查询数据
     *
     * @param client      加盟商
     * @param supSelfCode 自编码
     * @return
     */
    List<GaiaSupplierBusiness> selfOnlyCheck(@Param("client") String client, @Param("supSelfCode") String supSelfCode, @Param("supSite") String supSite);

    /**
     * 自编码集合
     *
     * @param client
     * @param supSite
     * @return
     */
    List<String> getSelfCodeList(@Param("client") String client, @Param("supSite") String supSite, @Param("supCode") String supCode);

    /**
     * 第一步保存，进行数据比对
     * 据地点+对应行的统一社会信用代码 精确匹配
     */
    List<SaveSupList1ResponseDTO> getCompList(@Param("client") String client, @Param("list") List<GaiaSupplierBusinessInfo> list);


    /**
     * 供应商查询列表
     */
    List<GaiaSupplierBusinessDTO> getSupList(@Param("dto") GetSupListRequestDTO dto, @Param("stoList") List<String> stoList);

    /**
     * 批量更新
     *
     * @param supplierBusiness 更新内容
     * @param supplierKeyList  主健集合
     */
    void updateBatch(@Param("business") GaiaSupplierBusiness supplierBusiness, @Param("keyList") List<GaiaSupplierBusinessKey> supplierKeyList);

    /**
     * 根据复合主键集合查询集合
     *
     * @param supplierKeyList 主键集合
     * @return 供应商集合
     */
    List<GaiaSupplierBusiness> selectSupListByKeyList(@Param("keyList") List<GaiaSupplierBusinessKey> supplierKeyList);
}

