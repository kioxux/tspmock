package com.gov.purchase.mapper;

import com.gov.purchase.entity.DistributionPlanRelation;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 15:03
 */
public interface DistributionPlanRelationMapper {

    DistributionPlanRelation getById(Long id);

    int add(DistributionPlanRelation distributionPlanRelation);

}
