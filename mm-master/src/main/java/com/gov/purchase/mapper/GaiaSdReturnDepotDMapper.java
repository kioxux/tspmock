package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdReturnDepotD;
import com.gov.purchase.entity.GaiaSdReturnDepotDKey;

public interface GaiaSdReturnDepotDMapper {
    int deleteByPrimaryKey(GaiaSdReturnDepotDKey key);

    int insert(GaiaSdReturnDepotD record);

    int insertSelective(GaiaSdReturnDepotD record);

    GaiaSdReturnDepotD selectByPrimaryKey(GaiaSdReturnDepotDKey key);

    int updateByPrimaryKeySelective(GaiaSdReturnDepotD record);

    int updateByPrimaryKey(GaiaSdReturnDepotD record);
}