package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaChajiaM;
import com.gov.purchase.entity.GaiaChajiaMKey;
import com.gov.purchase.module.purchase.dto.PurchaseReceiptPriceDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaChajiaMMapper {
    int deleteByPrimaryKey(GaiaChajiaMKey key);

    int insert(GaiaChajiaM record);

    int insertSelective(GaiaChajiaM record);

    GaiaChajiaM selectByPrimaryKey(GaiaChajiaMKey key);

    int updateByPrimaryKeySelective(GaiaChajiaM record);

    int updateByPrimaryKey(GaiaChajiaM record);

    /**
     * 采购差价单明细
     *
     * @param client
     * @param cjId
     * @param proSite
     * @return
     */
    List<PurchaseReceiptPriceDetailVO> selectPurchaseReceiptPriceOrderDetail(@Param("client") String client, @Param("cjId") String cjId, @Param("proSite") String proSite);

    /**
     * 批量插入采购差价单明细
     *
     * @param list
     */
    void insertBatch(List<GaiaChajiaM> list);

    /**
     * 批量更新采购差价单明细
     *
     * @param list
     */
    void updateBatch(List<GaiaChajiaM> list);

    /**
     * 删除采购差价单明细
     *
     * @param client
     * @param cjId
     */
    void deleteByCjId(@Param("client") String client, @Param("cjId") String cjId);
}