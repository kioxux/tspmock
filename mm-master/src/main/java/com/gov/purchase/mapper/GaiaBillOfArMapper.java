package com.gov.purchase.mapper;


import com.gov.purchase.module.wholesale.dto.GaiaBillOfApVO;
import com.gov.purchase.module.wholesale.dto.GaiaBillOfArVO;
import com.gov.purchase.module.wholesale.dto.SupplierDealOutData;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 明细清单表(GaiaBillOfAr)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-18 15:13:12
 */
public interface GaiaBillOfArMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaBillOfArVO queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaBillOfArVO> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaBillOfAr 实例对象
     * @return 对象列表
     */
    List<GaiaBillOfArVO> queryAll(GaiaBillOfArVO gaiaBillOfAr);

    /**
     * 新增数据
     *
     * @param gaiaBillOfAr 实例对象
     * @return 影响行数
     */
    int insert(GaiaBillOfArVO gaiaBillOfAr);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaBillOfAr> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaBillOfArVO> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaBillOfAr> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaBillOfArVO> entities);

    String getBillCode(GaiaBillOfArVO inData);


    Integer getBillCount(GaiaBillOfArVO inData);

    double getLastAmt(GaiaBillOfArVO inData);

    int getOriBill(GaiaBillOfArVO param);

    Integer getBeginningOfPeriod(@Param("client")String client,@Param("supCode") String supCode,@Param("siteCode") String siteCode);

    List<GaiaBillOfApVO> getAmt(@Param("client")String client,@Param("supCode") String supCode, @Param("siteCode")String siteCode, @Param("date")String date);

    List<GaiaBillOfApVO> getBillOfAp(@Param("client")String client,@Param("supCode") String supCode, @Param("siteCode")String siteCode, @Param("date")String date);

    void updateSupplierBusiness(@Param("client")String client, @Param("supCode") String supCode, @Param("siteCode")String siteCode,@Param("amountOfPayment") BigDecimal amountOfPayment);

     GaiaBillOfApVO getBillOf(@Param("client")String client, @Param("supCode")String supCode, @Param("siteCode") String siteCode, @Param("yestDay") String yestDay);

    List<SupplierDealOutData> getBatSupplier(@Param("list") SupplierDealOutData list);

    List<String> getName(@Param("client")String client, @Param("supSite")String supSite, @Param("supSelfCode")String supSelfCode, @Param("salesmanId")String salesmanId);


    GaiaBillOfArVO selectSumByArAmt(GaiaBillOfArVO billOfAr);
}

