package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBusinessScope;
import com.gov.purchase.entity.GaiaBusinessScopeKey;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.qa.dto.GaiaBusinessScopeExtendDto;
import com.gov.purchase.module.qa.dto.QueryBusinessScopeInfoRequestDto;
import com.gov.purchase.module.qa.dto.QueryScopeListRequestDto;
import com.gov.purchase.module.qa.dto.QueryScopeListResponseDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaBusinessScopeMapper {
    int deleteByPrimaryKey(GaiaBusinessScopeKey key);

    int insert(GaiaBusinessScope record);

    int insertSelective(GaiaBusinessScope record);

    GaiaBusinessScope selectByPrimaryKey(GaiaBusinessScopeKey key);

    int updateByPrimaryKeySelective(GaiaBusinessScope record);

    int updateByPrimaryKey(GaiaBusinessScope record);

    List<QueryScopeListResponseDto> queryBusinessScopeList(QueryScopeListRequestDto dto);

    List<GaiaBusinessScopeExtendDto> getBusinessScopeInfo(QueryBusinessScopeInfoRequestDto dto);

    /**
     * 批量查询
     *
     * @param searchValBasicMap
     * @return
     */
    List<GaiaBusinessScopeExtendDto> selectScopeBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);

    /**
     * 明细数据
     * @param searchValBasicMap
     * @return
     */
    List<GaiaBusinessScopeExtendDto> selectScopeDetailBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap,
                                                            @Param("client") String client,
                                                            @Param("bsc_entity_class") String bsc_entity_class,
                                                            @Param("bsc_entity_id") String bsc_entity_id
    );
}