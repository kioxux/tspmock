package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductZdy;
import com.gov.purchase.entity.GaiaProductZdyKey;
import org.apache.ibatis.annotations.Param;

public interface GaiaProductZdyMapper {
    int deleteByPrimaryKey(GaiaProductZdyKey key);

    int insert(GaiaProductZdy record);

    int insertSelective(GaiaProductZdy record);

    GaiaProductZdy selectByPrimaryKey(GaiaProductZdyKey key);

    int updateByPrimaryKeySelective(GaiaProductZdy record);

    int updateByPrimaryKey(GaiaProductZdy record);

    GaiaProductZdy selectZdyName(@Param("client") String client);
}