package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaFranchisee;
import com.gov.purchase.module.base.dto.GaiaFranchiseeResponseDto;
import com.gov.purchase.module.base.dto.SearchValBasic;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaFranchiseeMapper {
    int deleteByPrimaryKey(String client);

    int insert(GaiaFranchisee record);

    int insertSelective(GaiaFranchisee record);

    GaiaFranchisee selectByPrimaryKey(String client);

    int updateByPrimaryKeySelective(GaiaFranchisee record);

    int updateByPrimaryKey(GaiaFranchisee record);

    /**
     * 加盟商集合
     * @param client
     * @return
     */
    List<GaiaFranchiseeResponseDto> franchiseeList(@Param("client") String client);

    /**
     * 取得加盟商
     */
    List<String> getFrancName(@Param("searchValBasic") SearchValBasic searchValBasic);
}