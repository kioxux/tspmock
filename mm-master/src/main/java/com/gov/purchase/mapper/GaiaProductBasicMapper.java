package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaProductBasic;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.base.dto.GetProductTranslationListDTO;
import com.gov.purchase.module.base.dto.ProductDto;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.goods.dto.*;
import com.gov.purchase.module.purchase.dto.GetProductListRequestDto;
import com.gov.purchase.module.purchase.dto.GetProductListResponseDto;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentListResponseDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaProductBasicMapper {
    int deleteByPrimaryKey(String proCode);

    int insert(GaiaProductBasic record);

    int insertSelective(GaiaProductBasic record);

    GaiaProductBasic selectByPrimaryKey(String proCode);

    int updateByPrimaryKeySelective(GaiaProductBasic record);

    int updateByPrimaryKey(GaiaProductBasic record);

    List<QueryProductBasicResponseDto> selectProductBasicInfo(QueryProductBasicRequestDto dto);

    List<QueryProductBasicResponseDto> selectGoodsInfo(QueryProductBasicRequestDto dto);

    List<GetProductListResponseDto> selectProductList(GetProductListRequestDto dto);

    /**
     * 商品编码生成
     */
    String getProCode();

    /**
     * 批量查询（通用）
     *
     * @param searchValBasicMap
     * @return
     */
    List<ProductDto> selectBasicBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);

    /**
     * 批量查询（通用+业务）
     *
     * @param searchValBasicMap
     * @return
     */
    List<ProductDto> selectBusinessBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);

    /**
     * 商品列表
     *
     * @param dto
     * @return
     */
    List<GetProductListResponseDto> getProductListForAdjustment(GetProductListRequestDto dto);


    /***********************************************************五期调整********************************************************/

    /**
     * 商品首营商品查询
     */
    List<GetProBasicListDTO> getProBasicList(@Param("dto") GetProBasicListRequestDTO dto, @Param("client") String client);

    /**
     * 若没有匹配上 商品库GAIA_PRODUCT_BASIC表 则匹配 商品业务表 GAIA_PRODUCT_BUSINESS
     */
    List<GetProBasicListDTO> getProBasicListByBusiness(@Param("dto") GetProBasicListRequestDTO dto, @Param("client") String client);

    /**
     * 如果精确定位直接带入列表的情况下，提示“该商品在本公司已经营，编码为XXXXX，是否继续新增。”
     */
    String getProHasBeenUsedCode(@Param("proCode") String proCode, @Param("client") String client);

    /**
     * 当前权限下 的 地点列表
     */
    List<String> getSiteList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 根据地点+对应行的国际条形码 精确匹配business表
     */
    List<GaiaProductBusiness> getCompBusinessList(@Param("client") String client,
                                                  @Param("barcodeList") List<String> barcodeList,
                                                  @Param("siteList") List<String> siteList);

    /**
     * 非连锁门店集合
     */
    List<String> getStoList(@Param("client") String client);

    /**
     * 配送中心（即为连锁总部）集合
     */
    List<String> getDcList(@Param("client") String client);

    /**
     * 供货单位列表
     */
    List<String> getSupplierList(@Param("client") String client);

    /**
     * 当前人员的连锁权限
     */
    List<GetDefaultSiteResponseDTO.GaiaCompadmDTO> getCompadmList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 当前人员的连锁权限
     */
    List<GetDefaultSiteResponseDTO.GaiaCompadmDTO> getCompadmList2(@Param("client") String client, @Param("userId") String userId);

    /**
     * 指定连锁总部下门店的集合
     */
    List<GaiaStoreData> getCompStoList(@Param("client") String client, @Param("compIdList") List<String> compIdList);

    /**
     * 当前权限下的门店集合
     */
    List<GaiaStoreData> getAuthStoList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 连锁总部下的门店列表
     */
    List<String> getStoListByComp(@Param("client") String client, @Param("compadmId") String compadmId);

    /**
     * 同一加盟商下国际条形码相同的商品
     */
    List<GaiaProductBusiness> getCompBarcode(@Param("client") String client, @Param("proBarcode") String proBarcode);

    /**
     * 批量更新前的数据
     */
    List<GaiaProductBusiness> getOriginalList(@Param("client") String client, @Param("business") List<GaiaProductBusiness> business);

    /**
     * DC列表
     *
     * @return
     */
    List<GaiaDcData> getDcListToUpdate(@Param("client") String client, @Param("userId") String userId);

    /**
     * 商品主数据更新时_有权限的_非连锁门店地点
     */
    List<String> getAuthStoListToUpdate(@Param("client") String client, @Param("userId") String userId);

    /**
     * 商品列表 [ALL全部地点都包含]
     *
     * @param client
     * @param proSelfCode
     * @return
     */
    List<GetProductListResponseDto> selectProductListSiteAll(@Param("client") String client, @Param("proSelfCode") String proSelfCode);

    /**
     * 指定 加盟商+地点 下商品自编码列表
     */
    List<String> getDistinctSiteList(@Param("client") String client, @Param("proSite") String proSite, @Param("selfCodeList") List<String> selfCodeList);

    /**
     *
     */
    List<GetProductTranslationListDTO> getProListForPurchasePo(@Param("client") String client,
                                                               @Param("proSite") String proSite,
                                                               @Param("proSelfCodeList") List<String> proSelfCodeList);

    /**
     * 商品列表_弹出框
     */
    List<GetProductListResponseDto> getProductListForAdjustmentPopup(GetProductListRequestDto dto);

    List<DistributionDetailDTO> listSto(@Param("client") String client, @Param("stoAttributesList") List<String> stoAttributesList,
                                        @Param("stoTypeList") List<String> stoTypeList, @Param("stoCodeList") List<String> stoCodeList,
                                        @Param("stoEffectList") List<String> stoEffectList,
                                        @Param("gssgType") String gssgType,
                                        @Param("isIncludeClient") String isIncludeClient);

    List<DcReplenishmentListResponseDto> selectSaleProByPro(@Param("client") String client, @Param("proSelfCodeList") List<String> proSelfCodeList);

    /**
     * 指定 加盟商 下商品自编码列表
     */
    List<String> getDistinctProductList(@Param("client") String client, @Param("selfCodeList") List<String> selfCodeList);

    /**
     * 查询门店是否存在该商品
     *
     * @param client
     * @param selfCode
     * @param stores
     * @return
     */
    int selectProInStore(@Param("client") String client, @Param("selfCode") String selfCode, @Param("stores") List<String> stores);

    List<GetProductListResponseDto> getBatchListByProCOde(@Param("client") String client, @Param("supCode") String supCode,
                                                          @Param("proCodeList") List<String> proCodeList);
}
