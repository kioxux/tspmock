package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSupplierGspinfo;
import com.gov.purchase.entity.GaiaSupplierGspinfoKey;
import com.gov.purchase.module.supplier.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSupplierGspinfoMapper {
    int deleteByPrimaryKey(GaiaSupplierGspinfoKey key);

    int insert(GaiaSupplierGspinfo record);

    int insertSelective(GaiaSupplierGspinfo record);

    GaiaSupplierGspinfo selectByPrimaryKey(GaiaSupplierGspinfoKey key);

    int updateByPrimaryKeySelective(GaiaSupplierGspinfo record);

    int updateByPrimaryKey(GaiaSupplierGspinfo record);

    List<GaiaSupplierGspinfo> selfOnlyCheck(@Param("client") String client, @Param("supSelfCode") String supSelfCode, @Param("supSite") String supSite);

    int insertGspinfoSubmit(GspinfoSubmitRequestDto dto);

    List<QueryGspListResponseDto> selectQueryGspinfo(QueryGspinfoListRequestDto dto);

    QueryGspListResponseDto selectByPrimary(GaiaSupplierRequestDto dto);

    /**
     * 根据流程号获取供应商首营信息
     * @param supFlowNo
     * @return
     */
    List<GaiaSupplierGspinfo> selectSupplierGspByFlowNo(@Param("client") String client,@Param("supFlowNo") String supFlowNo);

    /**
     * 根据查询条件导出表中所有内容
     * @param dto
     * @return
     */
    List<QueryGspListResponseDtoCSV> selectExportQueryGspinfo(QueryGspinfoListRequestDto dto);
}