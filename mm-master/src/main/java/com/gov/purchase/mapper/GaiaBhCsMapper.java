package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBhCs;
import com.gov.purchase.entity.GaiaBhCsKey;

public interface GaiaBhCsMapper {
    int deleteByPrimaryKey(GaiaBhCsKey key);

    int insert(GaiaBhCs record);

    int insertSelective(GaiaBhCs record);

    GaiaBhCs selectByPrimaryKey(GaiaBhCsKey key);

    int updateByPrimaryKeySelective(GaiaBhCs record);

    int updateByPrimaryKey(GaiaBhCs record);
}