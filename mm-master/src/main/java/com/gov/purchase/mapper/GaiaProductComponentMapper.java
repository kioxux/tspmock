package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductComponent;
import com.gov.purchase.module.base.dto.SearchValBasic;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductComponentMapper {
    int deleteByPrimaryKey(String proCompCode);

    int insert(GaiaProductComponent record);

    int insertSelective(GaiaProductComponent record);

    GaiaProductComponent selectByPrimaryKey(String proCompCode);

    int updateByPrimaryKeySelective(GaiaProductComponent record);

    int updateByPrimaryKey(GaiaProductComponent record);

    /**
     * 取得成分分类
     */
    List<String> getProComponent(@Param("searchValBasic") SearchValBasic searchValBasic);
}