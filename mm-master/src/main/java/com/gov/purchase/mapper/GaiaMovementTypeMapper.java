package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaMovementType;
import com.gov.purchase.entity.GaiaMovementTypeKey;

public interface GaiaMovementTypeMapper {
    int deleteByPrimaryKey(GaiaMovementTypeKey key);

    int insert(GaiaMovementType record);

    int insertSelective(GaiaMovementType record);

    GaiaMovementType selectByPrimaryKey(GaiaMovementTypeKey key);

    int updateByPrimaryKeySelective(GaiaMovementType record);

    int updateByPrimaryKey(GaiaMovementType record);
}