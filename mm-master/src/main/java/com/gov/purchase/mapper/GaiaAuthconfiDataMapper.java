package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaAuthconfiData;
import com.gov.purchase.entity.GaiaAuthconfiDataKey;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.base.dto.GaiaStoreDataForGspinfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaAuthconfiDataMapper {
    int deleteByPrimaryKey(GaiaAuthconfiDataKey key);

    int insert(GaiaAuthconfiData record);

    int insertSelective(GaiaAuthconfiData record);

    GaiaAuthconfiData selectByPrimaryKey(GaiaAuthconfiDataKey key);

    int updateByPrimaryKeySelective(GaiaAuthconfiData record);

    int updateByPrimaryKey(GaiaAuthconfiData record);

    /**
     * 根据用户查询地点集合
     * @param userId
     * @return
     */
    List<GaiaStoreData> getSiteList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 根据用户查询地点集合 过滤门店连锁的数据
     * @param userId
     * @return
     */
    List<GaiaStoreDataForGspinfo> siteListForGspinfo(@Param("client") String client, @Param("userId") String userId);

    /**
     * 查询角色是否配置权限
     * @param client
     * @param userId
     * @return
     */
    int selectUserAuthority(@Param("client") String client, @Param("groupId") String groupId, @Param("userId") String userId);
}