package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdStoresGroupSet;
import com.gov.purchase.entity.GaiaSdStoresGroupSetKey;
import com.gov.purchase.module.replenishment.dto.GaiaSdStoresGroupSetDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdStoresGroupSetMapper {
    int deleteByPrimaryKey(GaiaSdStoresGroupSetKey key);

    int insert(GaiaSdStoresGroupSet record);

    int insertSelective(GaiaSdStoresGroupSet record);

    GaiaSdStoresGroupSet selectByPrimaryKey(GaiaSdStoresGroupSetKey key);

    int updateByPrimaryKeySelective(GaiaSdStoresGroupSet record);

    int updateByPrimaryKey(GaiaSdStoresGroupSet record);

    List<GaiaSdStoresGroupSetDto> selectSdStoresGroupSetList(@Param("client") String client, @Param("type") String type);
}