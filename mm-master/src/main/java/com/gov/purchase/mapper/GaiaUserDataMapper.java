package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaUserData;
import com.gov.purchase.entity.GaiaUserDataKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaUserDataMapper {
    int deleteByPrimaryKey(GaiaUserDataKey key);

    int insert(GaiaUserData record);

    int insertSelective(GaiaUserData record);

    GaiaUserData selectByPrimaryKey(GaiaUserDataKey key);

    int updateByPrimaryKeySelective(GaiaUserData record);

    int updateByPrimaryKey(GaiaUserData record);

    List<GaiaUserData> selectByClient(@Param("client") String client);
}