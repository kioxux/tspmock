package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBillOfParticulars;
import com.gov.purchase.entity.WholesaleSaleOutData;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public interface GaiaBillOfParticularsMapper {

    // 判断GAIA_BILL_OF_PARTICULARS是否存在对应支付类型记录
    Integer countListByPaymentId(@Param("client") String client,@Param("matStoCode") String matStoCode,@Param("matSiteCode") String matSiteCode,@Param("paymentId") String paymentId,@Param("matCreateDate") String matCreateDate);

     // 新增
    int insert(GaiaBillOfParticulars gaiaBillOfParticulars);

    List<WholesaleSaleOutData> selectWholesaleSalePage(@Param("client") String client,@Param("siteCode") String siteCode,@Param("dnId") String dnId);

    // 根据code查询对应名称
    String queryDcNameByCode(@Param("client") String client, @Param("matSiteCode") String matSiteCode);

    ArrayList<String> queryAllStoCode(String client);

    BigDecimal sumInitComeAndGo(@Param("client")String client,@Param("stoCode") String stoCode,@Param("startDate") String startDate,@Param("endDate") String endDate);
}
