package com.gov.purchase.mapper;

import com.gov.purchase.module.blacklist.dto.BlackListRequestDto;
import com.gov.purchase.module.blacklist.dto.BlackListResponseDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProBatchHmdMapper {

    int save(BlackListRequestDto dto);

    /**
     * 批量更新 GAIA_PRODUCT_BUSINESS.PRO_OUT
     * flag 为null 置为null   flag为 1  置为 1
     * @param dto
     * @param flag
     * @return
     */
    int updateBusinessProOut(@Param("dtoList") List<BlackListRequestDto> dto, @Param("flag") String flag);

    List<BlackListResponseDto> listBlack(BlackListRequestDto dto);

    List<BlackListRequestDto> checkBlackExist(@Param("client") String client, @Param("proSite") String proSite, @Param("proSelfCode") String proSelfCode, @Param("proBatchNo") String proBatchNo, @Param("flag") String flag);

    void deleteBlackList(@Param("deleteList") List<BlackListRequestDto> deleteList);
}