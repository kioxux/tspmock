package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaWmsDiaoboZ;
import com.gov.purchase.entity.GaiaWmsDiaoboZKey;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

public interface GaiaWmsDiaoboZMapper {
    int deleteByPrimaryKey(GaiaWmsDiaoboZKey key);

    int insert(GaiaWmsDiaoboZ record);

    int insertSelective(GaiaWmsDiaoboZ record);

    GaiaWmsDiaoboZ selectByPrimaryKey(GaiaWmsDiaoboZKey key);

    int updateByPrimaryKeySelective(GaiaWmsDiaoboZ record);

    int updateByPrimaryKey(GaiaWmsDiaoboZ record);

    BigDecimal getZqAmount(@Param("client") String client, @Param("khbh") String khbh, @Param("zqDate") String zqDate);
}