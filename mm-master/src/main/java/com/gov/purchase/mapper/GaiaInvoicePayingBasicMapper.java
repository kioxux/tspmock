package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaInvoicePayingBasic;
import com.gov.purchase.entity.GaiaInvoicePayingBasicKey;
import com.gov.purchase.module.purchase.dto.InvoiceDTO;
import com.gov.purchase.module.purchase.dto.ListInvoiceRequestDTO;

import java.util.List;

public interface GaiaInvoicePayingBasicMapper {
    int deleteByPrimaryKey(GaiaInvoicePayingBasicKey key);

    int insert(GaiaInvoicePayingBasic record);

    int insertSelective(GaiaInvoicePayingBasic record);

    GaiaInvoicePayingBasic selectByPrimaryKey(GaiaInvoicePayingBasicKey key);

    int updateByPrimaryKeySelective(GaiaInvoicePayingBasic record);

    int updateByPrimaryKey(GaiaInvoicePayingBasic record);

    /**
     * 批量插入
     * @param list
     */
    void insertBatch(List<InvoiceDTO> list);
}