package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaFiUserPaymentMaintenance;
import com.gov.purchase.entity.GaiaFiUserPaymentMaintenanceKey;

public interface GaiaFiUserPaymentMaintenanceMapper {
    int deleteByPrimaryKey(GaiaFiUserPaymentMaintenanceKey key);

    int insert(GaiaFiUserPaymentMaintenance record);

    int insertSelective(GaiaFiUserPaymentMaintenance record);

    GaiaFiUserPaymentMaintenance selectByPrimaryKey(GaiaFiUserPaymentMaintenanceKey key);

    int updateByPrimaryKeySelective(GaiaFiUserPaymentMaintenance record);

    int updateByPrimaryKey(GaiaFiUserPaymentMaintenance record);
}