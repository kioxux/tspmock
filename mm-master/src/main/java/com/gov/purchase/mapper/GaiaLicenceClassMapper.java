package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaLicenceClass;

public interface GaiaLicenceClassMapper {
    int deleteByPrimaryKey(String perCode);

    int insert(GaiaLicenceClass record);

    int insertSelective(GaiaLicenceClass record);

    GaiaLicenceClass selectByPrimaryKey(String perCode);

    int updateByPrimaryKeySelective(GaiaLicenceClass record);

    int updateByPrimaryKey(GaiaLicenceClass record);
}