package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdAcceptH;
import com.gov.purchase.entity.GaiaSdAcceptHKey;

public interface GaiaSdAcceptHMapper {
    int deleteByPrimaryKey(GaiaSdAcceptHKey key);

    int insert(GaiaSdAcceptH record);

    int insertSelective(GaiaSdAcceptH record);

    GaiaSdAcceptH selectByPrimaryKey(GaiaSdAcceptHKey key);

    int updateByPrimaryKeySelective(GaiaSdAcceptH record);

    int updateByPrimaryKey(GaiaSdAcceptH record);
}