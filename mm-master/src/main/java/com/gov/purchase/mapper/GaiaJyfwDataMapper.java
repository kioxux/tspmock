package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaJyfwData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaJyfwDataMapper {
    int deleteByPrimaryKey(String id);

    int insert(GaiaJyfwData record);

    int insertSelective(GaiaJyfwData record);

    GaiaJyfwData selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(GaiaJyfwData record);

    int updateByPrimaryKey(GaiaJyfwData record);

    List<String> selectStoreJyfwBySto(@Param("client") String client, @Param("jyfwOrgid") String jyfwOrgid);

    // 经营范围类别中有3
    List<String> selectStoreJyfwByStoClass3(@Param("client") String client, @Param("jyfwOrgid") String jyfwOrgid);

    List<GaiaJyfwData> selectStoreJyfwByStoList(@Param("client") String client, @Param("list") List<String> list);

    // 经营范围类别中有3
    List<GaiaJyfwData> selectStoreJyfwByStoClass3List(@Param("client") String client, @Param("list") List<String> list);

    /**
     * 查询指定机构经营范围列表
     *
     * @return
     */
    List<String> getOrgJyfwIdList(@Param("client") String client, @Param("jyfwOrgid") String jyfwOrgid);

    // 经营范围类别中有3
    List<String> getOrgJyfwIdListClass3(@Param("client") String client, @Param("jyfwOrgid") String jyfwOrgid);


    /**
     * 其他（连锁、门店、批发）的管控范围
     *
     * @param client
     * @param jyfwOrgid
     * @return
     */
    List<String> getJyfwIdList(@Param("client") String client, @Param("jyfwOrgid") String jyfwOrgid);
}