package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSupplierSalesman;
import com.gov.purchase.module.supplier.dto.GaiaSupplierSalesmanDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSupplierSalesmanMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSupplierSalesman record);

    int insertSelective(GaiaSupplierSalesman record);

    GaiaSupplierSalesman selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSupplierSalesman record);

    int updateByPrimaryKey(GaiaSupplierSalesman record);

    /**
     * 批量新增
     */
    void saveSupplierSalesmanList(@Param("list") List<GaiaSupplierSalesman> supplierSalesmanList);

    /**
     * 指定供应商下的业务员列表
     */
    List<GaiaSupplierSalesmanDTO> getSupplierSalesmanList(@Param("client") String client,
                                                          @Param("supSite") String supSite,
                                                          @Param("supSelfCode") String supSelfCode);

    /**
     * 删除指定供应商下的业务员
     */
    void deleteBySup(@Param("client") String client, @Param("supSite") String supSite, @Param("supSelfCode") String supSelfCode);

    /**
     * 指定供应商(+审批流程编号)下的业务员列表
     */
    List<GaiaSupplierSalesman> getSupplierSalesmanListByFlowNo(@Param("client") String client,
                                                               @Param("supSite") String supSite,
                                                               @Param("supSelfCode") String supSelfCode,
                                                               @Param("gssFlowNo") String gssFlowNo);

    /**
     * 指定供应商下已经使用了的业务员
     */
    List<GaiaSupplierSalesman> getHasUsedSalesmanList(@Param("client") String client, @Param("supSite") String supSite, @Param("supSelfCode") String supSelfCode);

    List<GaiaSupplierSalesman> selectSupplierSalesman(@Param("client") String client);

    List<GaiaSupplierSalesman> supplierSalesmanList(@Param("client") String client, @Param("dcCode") String dcCode, @Param("supSelfCode") String supSelfCode);
}