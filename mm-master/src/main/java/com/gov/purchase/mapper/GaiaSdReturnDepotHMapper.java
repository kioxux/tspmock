package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdReturnDepotH;
import com.gov.purchase.entity.GaiaSdReturnDepotHKey;

public interface GaiaSdReturnDepotHMapper {
    int deleteByPrimaryKey(GaiaSdReturnDepotHKey key);

    int insert(GaiaSdReturnDepotH record);

    int insertSelective(GaiaSdReturnDepotH record);

    GaiaSdReturnDepotH selectByPrimaryKey(GaiaSdReturnDepotHKey key);

    int updateByPrimaryKeySelective(GaiaSdReturnDepotH record);

    int updateByPrimaryKey(GaiaSdReturnDepotH record);
}