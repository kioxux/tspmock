package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdDepositH;
import com.gov.purchase.entity.GaiaSdDepositHKey;

public interface GaiaSdDepositHMapper {
    int deleteByPrimaryKey(GaiaSdDepositHKey key);

    int insert(GaiaSdDepositH record);

    int insertSelective(GaiaSdDepositH record);

    GaiaSdDepositH selectByPrimaryKey(GaiaSdDepositHKey key);

    int updateByPrimaryKeySelective(GaiaSdDepositH record);

    int updateByPrimaryKey(GaiaSdDepositH record);
}