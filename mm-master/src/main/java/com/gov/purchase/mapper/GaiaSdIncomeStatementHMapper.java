package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdIncomeStatementH;
import com.gov.purchase.entity.GaiaSdIncomeStatementHKey;

public interface GaiaSdIncomeStatementHMapper {
    int deleteByPrimaryKey(GaiaSdIncomeStatementHKey key);

    int insert(GaiaSdIncomeStatementH record);

    int insertSelective(GaiaSdIncomeStatementH record);

    GaiaSdIncomeStatementH selectByPrimaryKey(GaiaSdIncomeStatementHKey key);

    int updateByPrimaryKeySelective(GaiaSdIncomeStatementH record);

    int updateByPrimaryKey(GaiaSdIncomeStatementH record);
}