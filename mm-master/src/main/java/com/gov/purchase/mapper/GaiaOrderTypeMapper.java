package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaOrderType;

public interface GaiaOrderTypeMapper {
    int insert(GaiaOrderType record);

    int insertSelective(GaiaOrderType record);
}