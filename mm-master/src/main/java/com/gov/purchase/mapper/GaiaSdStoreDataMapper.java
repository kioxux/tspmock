package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdStoreData;
import com.gov.purchase.entity.GaiaSdStoreDataKey;

public interface GaiaSdStoreDataMapper {
    int deleteByPrimaryKey(GaiaSdStoreDataKey key);

    int insert(GaiaSdStoreData record);

    int insertSelective(GaiaSdStoreData record);

    GaiaSdStoreData selectByPrimaryKey(GaiaSdStoreDataKey key);

    int updateByPrimaryKeySelective(GaiaSdStoreData record);

    int updateByPrimaryKey(GaiaSdStoreData record);
}