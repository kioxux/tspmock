package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdIncomeStatementD;
import com.gov.purchase.entity.GaiaSdIncomeStatementDKey;

public interface GaiaSdIncomeStatementDMapper {
    int deleteByPrimaryKey(GaiaSdIncomeStatementDKey key);

    int insert(GaiaSdIncomeStatementD record);

    int insertSelective(GaiaSdIncomeStatementD record);

    GaiaSdIncomeStatementD selectByPrimaryKey(GaiaSdIncomeStatementDKey key);

    int updateByPrimaryKeySelective(GaiaSdIncomeStatementD record);

    int updateByPrimaryKey(GaiaSdIncomeStatementD record);
}