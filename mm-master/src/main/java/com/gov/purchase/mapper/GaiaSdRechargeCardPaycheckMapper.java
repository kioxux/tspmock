package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdRechargeCardPaycheck;
import com.gov.purchase.entity.GaiaSdRechargeCardPaycheckKey;

public interface GaiaSdRechargeCardPaycheckMapper {
    int deleteByPrimaryKey(GaiaSdRechargeCardPaycheckKey key);

    int insert(GaiaSdRechargeCardPaycheck record);

    int insertSelective(GaiaSdRechargeCardPaycheck record);

    GaiaSdRechargeCardPaycheck selectByPrimaryKey(GaiaSdRechargeCardPaycheckKey key);

    int updateByPrimaryKeySelective(GaiaSdRechargeCardPaycheck record);

    int updateByPrimaryKey(GaiaSdRechargeCardPaycheck record);
}