package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdDeviceInfo;
import com.gov.purchase.entity.GaiaSdDeviceInfoKey;

public interface GaiaSdDeviceInfoMapper {
    int deleteByPrimaryKey(GaiaSdDeviceInfoKey key);

    int insert(GaiaSdDeviceInfo record);

    int insertSelective(GaiaSdDeviceInfo record);

    GaiaSdDeviceInfo selectByPrimaryKey(GaiaSdDeviceInfoKey key);

    int updateByPrimaryKeySelective(GaiaSdDeviceInfo record);

    int updateByPrimaryKey(GaiaSdDeviceInfo record);
}