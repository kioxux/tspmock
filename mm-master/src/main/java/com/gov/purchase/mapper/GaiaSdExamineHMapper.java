package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdExamineH;
import com.gov.purchase.entity.GaiaSdExamineHKey;

public interface GaiaSdExamineHMapper {
    int deleteByPrimaryKey(GaiaSdExamineHKey key);

    int insert(GaiaSdExamineH record);

    int insertSelective(GaiaSdExamineH record);

    GaiaSdExamineH selectByPrimaryKey(GaiaSdExamineHKey key);

    int updateByPrimaryKeySelective(GaiaSdExamineH record);

    int updateByPrimaryKey(GaiaSdExamineH record);
}