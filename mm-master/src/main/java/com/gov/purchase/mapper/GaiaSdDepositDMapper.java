package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdDepositD;
import com.gov.purchase.entity.GaiaSdDepositDKey;

public interface GaiaSdDepositDMapper {
    int deleteByPrimaryKey(GaiaSdDepositDKey key);

    int insert(GaiaSdDepositD record);

    int insertSelective(GaiaSdDepositD record);

    GaiaSdDepositD selectByPrimaryKey(GaiaSdDepositDKey key);

    int updateByPrimaryKeySelective(GaiaSdDepositD record);

    int updateByPrimaryKey(GaiaSdDepositD record);
}