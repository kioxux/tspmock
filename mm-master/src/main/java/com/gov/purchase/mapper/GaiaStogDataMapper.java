package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaStogData;
import com.gov.purchase.entity.GaiaStogDataKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaStogDataMapper {
    int deleteByPrimaryKey(GaiaStogDataKey key);

    int insert(GaiaStogData record);

    int insertSelective(GaiaStogData record);

    GaiaStogData selectByPrimaryKey(GaiaStogDataKey key);

    int updateByPrimaryKeySelective(GaiaStogData record);

    int updateByPrimaryKey(GaiaStogData record);

    /**
     * 删除门店组
     *
     * @param priceDtos
     */
    void deleteList(@Param("priceDtos") List<GaiaStogData> priceDtos);

    /**
     * 新增门店组
     *
     * @param priceDtos
     */
    void insertList(@Param("priceDtos") List<GaiaStogData> priceDtos);

}