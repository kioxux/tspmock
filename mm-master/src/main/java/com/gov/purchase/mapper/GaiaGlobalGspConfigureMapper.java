package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaGlobalGspConfigure;
import com.gov.purchase.entity.GaiaGlobalGspConfigureKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaGlobalGspConfigureMapper {
    int deleteByPrimaryKey(GaiaGlobalGspConfigureKey key);

    int insert(GaiaGlobalGspConfigure record);

    int insertSelective(GaiaGlobalGspConfigure record);

    GaiaGlobalGspConfigure selectByPrimaryKey(GaiaGlobalGspConfigureKey key);

    int updateByPrimaryKeySelective(GaiaGlobalGspConfigure record);

    int updateByPrimaryKey(GaiaGlobalGspConfigure record);

    /**
     * 全局GSP开关
     */
    List<GaiaGlobalGspConfigure> getCurrentClientConfigureList(@Param("client") String client,
                                                               @Param("gggcSite") String gggcSite,
                                                               @Param("gggcCode") String gggcCode);

    List<GaiaGlobalGspConfigure> getGspRequire(@Param("client") String client, @Param("gggcCode") String gggcCode);
}