package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdMemberCard;
import com.gov.purchase.entity.GaiaSdMemberCardKey;
import com.gov.purchase.module.base.dto.GetMemberListDTO;
import com.gov.purchase.module.base.dto.MemberDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdMemberCardMapper {
    int deleteByPrimaryKey(GaiaSdMemberCardKey key);

    int insert(GaiaSdMemberCard record);

    int insertSelective(GaiaSdMemberCard record);

    GaiaSdMemberCard selectByPrimaryKey(GaiaSdMemberCardKey key);

    int updateByPrimaryKeySelective(GaiaSdMemberCard record);

    int updateByPrimaryKey(GaiaSdMemberCard record);

    /**
     * 精准查询会员
     */
    List<MemberDTO> getMemberList(@Param("vo") GetMemberListDTO vo);

}
