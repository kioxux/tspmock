package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaAllotGroupPrice;
import com.gov.purchase.entity.GaiaAllotGroupPriceKey;
import com.gov.purchase.module.delivery.dto.GaiaAllotGroupPriceVO;
import com.gov.purchase.module.delivery.dto.GaiaAllotProductPriceVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaAllotGroupPriceMapper {
    int deleteByPrimaryKey(GaiaAllotGroupPriceKey key);

    int insert(GaiaAllotGroupPrice record);

    int insertSelective(GaiaAllotGroupPrice record);

    GaiaAllotGroupPrice selectByPrimaryKey(GaiaAllotGroupPriceKey key);

    int updateByPrimaryKeySelective(GaiaAllotGroupPrice record);

    int updateByPrimaryKey(GaiaAllotGroupPrice record);

    //extend
    List<GaiaAllotGroupPriceVO> queryAllotGroupPriceList(@Param("client") String client,
                                                         @Param("alpReceiveSite") String alpReceiveSite,
                                                         @Param("gapgCode") String gapgCode,
                                                         @Param("gapgType") String gapgType);

    int batchDelete(List<GaiaAllotGroupPrice> list);

    int batchInsert(List<GaiaAllotGroupPrice> list);

    int batchUpdate(List<GaiaAllotGroupPrice> list);

    Integer selectMaxIndex();

    List<GaiaAllotGroupPrice> selectByPrimaryKeyList(List<GaiaAllotGroupPriceKey> list);

    List<GaiaAllotProductPriceVO> selectAllotPriceListForGroup(@Param("client") String client,
                                                               @Param("cusList") List<String> cusList,
                                                               @Param("gapgType") Integer gapgType,
                                                               @Param("alpReceiveSite") String alpReceiveSite);

    List<GaiaAllotProductPriceVO> selectAllotPriceListByPro(@Param("client") String client,
                                                            @Param("proList") List<String> proList,
                                                            @Param("gapgType") Integer gapgType,
                                                            @Param("alpReceiveSite") String alpReceiveSite);
}