package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaContractZLog;
import com.gov.purchase.entity.GaiaContractZLogKey;
import com.gov.purchase.module.purchase.dto.PurchaseContractDTO;
import com.gov.purchase.module.purchase.dto.PurchaseContractVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaContractZLogMapper {
    int deleteByPrimaryKey(GaiaContractZLogKey key);

    int insert(GaiaContractZLog record);

    int insertSelective(GaiaContractZLog record);

    GaiaContractZLog selectByPrimaryKey(GaiaContractZLogKey key);

    int updateByPrimaryKeySelective(GaiaContractZLog record);

    int updateByPrimaryKey(GaiaContractZLog record);

    /**
     * 最新变更记录
     *
     * @param key
     * @return
     */
    GaiaContractZLog selectLog(GaiaContractZLogKey key);

    /**
     * 合同列表
     *
     * @param purchaseContractDTO
     * @return
     */
    List<PurchaseContractVO> queryContract(PurchaseContractDTO purchaseContractDTO);

    int deleteByConId(@Param("client") String client, @Param("conId") String conId);
}