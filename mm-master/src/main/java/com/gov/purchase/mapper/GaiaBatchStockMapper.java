package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBatchStock;
import com.gov.purchase.entity.GaiaBatchStockKey;
import com.gov.purchase.module.base.dto.businessImport.StoreDistributionRequestDto;
import com.gov.purchase.module.base.dto.businessImport.StoreDistributionResp;
import com.gov.purchase.module.delivery.dto.StockNumRequestDto;
import com.gov.purchase.module.qa.dto.SaveBatchStockInfoResquestDto;
import com.gov.purchase.module.replenishment.dto.BatchStockListResponseDto;
import com.gov.purchase.module.replenishment.dto.BatchStockRequestDto;
import org.apache.ibatis.annotations.Param;
import java.math.BigDecimal;
import java.util.List;

public interface GaiaBatchStockMapper {
    int deleteByPrimaryKey(GaiaBatchStockKey key);

    int insert(GaiaBatchStock record);

    int insertSelective(GaiaBatchStock record);

    GaiaBatchStock selectByPrimaryKey(GaiaBatchStockKey key);

    int updateByPrimaryKeySelective(GaiaBatchStock record);

    int updateByPrimaryKey(GaiaBatchStock record);

    int saveBatchStockInfo(List<SaveBatchStockInfoResquestDto> list);

    /**
     * 门店铺货列表
     *
     * @param dto
     * @return
     */
    List<BatchStockListResponseDto> selectStoreDistributionList(BatchStockRequestDto dto);

    /**
     * 门店铺货信息（导入用）
     * @param dto
     * @return
     */
    StoreDistributionResp selectStoreDistribution(StoreDistributionRequestDto dto);

}