package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductFactory;
import com.gov.purchase.module.base.dto.SearchValBasic;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductFactoryMapper {
    int deleteByPrimaryKey(String proFactoryCode);

    int insert(GaiaProductFactory record);

    int insertSelective(GaiaProductFactory record);

    GaiaProductFactory selectByPrimaryKey(String proFactoryCode);

    int updateByPrimaryKeySelective(GaiaProductFactory record);

    int updateByPrimaryKey(GaiaProductFactory record);

    /**
     * 取得生产企业
     */
    List<String> getProFactory(@Param("searchValBasic") SearchValBasic searchValBasic);
}