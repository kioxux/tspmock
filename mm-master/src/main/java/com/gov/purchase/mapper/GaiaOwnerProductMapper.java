package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaOwnerProduct;
import com.gov.purchase.entity.GaiaOwnerProductKey;
import com.gov.purchase.module.wholesale.dto.OwnerProductVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaOwnerProductMapper {
    int deleteByPrimaryKey(GaiaOwnerProductKey key);

    int insert(GaiaOwnerProduct record);

    int insertSelective(GaiaOwnerProduct record);

    GaiaOwnerProduct selectByPrimaryKey(GaiaOwnerProductKey key);

    int updateByPrimaryKeySelective(GaiaOwnerProduct record);

    int updateByPrimaryKey(GaiaOwnerProduct record);

    /**
     * 根据仓库地点查询货主商品
     *
     * @param client 加盟商
     * @param dcCode 仓库
     * @param owner  货主编码
     * @return
     */
    List<OwnerProductVO> selectProByDcCodeAndOwner(@Param("client") String client, @Param("dcCode") String dcCode, @Param("owner") String owner);

    /**
     * 删除货主商品
     *
     * @param client
     * @param dcCode
     * @param owner
     * @return
     */
    int deleteOwnerPro(@Param("client") String client, @Param("dcCode") String dcCode, @Param("owner") String owner);

    /**
     * 批量插入货主商品
     *
     * @param list
     */
    void insertBatchOwnerProduct(List<OwnerProductVO> list);


    /**
     * 批量删除
     *
     * @param client
     * @param dcCode
     * @param owner
     * @param list
     */
    void deleteBatch(@Param("client") String client,
                     @Param("dcCode") String dcCode,
                     @Param("owner") String owner,
                     @Param("list") List<String> list);
}