package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdSaleHandD;
import com.gov.purchase.entity.GaiaSdSaleHandDKey;

public interface GaiaSdSaleHandDMapper {
    int deleteByPrimaryKey(GaiaSdSaleHandDKey key);

    int insert(GaiaSdSaleHandD record);

    int insertSelective(GaiaSdSaleHandD record);

    GaiaSdSaleHandD selectByPrimaryKey(GaiaSdSaleHandDKey key);

    int updateByPrimaryKeySelective(GaiaSdSaleHandD record);

    int updateByPrimaryKey(GaiaSdSaleHandD record);
}