package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductFormKey;

public interface GaiaProductFormMapper {
    int deleteByPrimaryKey(GaiaProductFormKey key);

    int insert(GaiaProductFormKey record);

    int insertSelective(GaiaProductFormKey record);
}