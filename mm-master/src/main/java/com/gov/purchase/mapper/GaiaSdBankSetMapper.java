package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdBankSet;
import com.gov.purchase.entity.GaiaSdBankSetKey;

public interface GaiaSdBankSetMapper {
    int deleteByPrimaryKey(GaiaSdBankSetKey key);

    int insert(GaiaSdBankSet record);

    int insertSelective(GaiaSdBankSet record);

    GaiaSdBankSet selectByPrimaryKey(GaiaSdBankSetKey key);

    int updateByPrimaryKeySelective(GaiaSdBankSet record);

    int updateByPrimaryKey(GaiaSdBankSet record);
}