package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSmsRechargeRecord;
import org.apache.ibatis.annotations.Param;

public interface GaiaSmsRechargeRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GaiaSmsRechargeRecord record);

    int insertSelective(GaiaSmsRechargeRecord record);

    GaiaSmsRechargeRecord selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaSmsRechargeRecord record);

    int updateByPrimaryKey(GaiaSmsRechargeRecord record);

    GaiaSmsRechargeRecord selectByFicoId(@Param("ficoId") String ficoId);
}