package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPriceGroup;
import com.gov.purchase.entity.GaiaPriceGroupKey;
import com.gov.purchase.entity.GaiaSdProductPrice;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.store.dto.price.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaPriceGroupMapper {
    int deleteByPrimaryKey(GaiaPriceGroupKey key);

    int insert(GaiaPriceGroup record);

    int insertSelective(GaiaPriceGroup record);

    GaiaPriceGroup selectByPrimaryKey(GaiaPriceGroupKey key);

    int updateByPrimaryKeySelective(GaiaPriceGroup record);

    int updateByPrimaryKey(GaiaPriceGroup record);

    List<PriceGroupListReponseDto> selectPriceGroupList(PriceGroupListRequestDto dto);

    List<PriceStoreListReponseDto> queryPriceStoreList(PriceStoreListRequestDto dto);

    /**
     * 当前用户商品组以及权限下的门店列表
     */
    List<PriceStoreListReponseDto> getCurrentUserAuthStoreList(@Param("client") String client, @Param("userId") String userId, @Param("prcGroupId") String prcGroupId);

    /**
     * 删除价格组
     *
     * @param client
     * @param prcGroupId
     * @param storeIds
     */
    void deleteBatchPriceGroup(@Param("client") String client, @Param("prcGroupId") String prcGroupId, @Param("storeIds") List<String> storeIds);

    /**
     * 删除重复门店
     *
     * @param client
     * @param prcGroupId
     * @param storeIds
     */
    void deleteBatchPriceGroupByStore(@Param("client") String client, @Param("prcGroupId") String prcGroupId, @Param("storeIds") List<String> storeIds);

    /**
     * 删除价格组商品
     *
     * @param client
     * @param prcGroupId
     */
    void deleteBatchPricePro(@Param("client") String client, @Param("prcGroupId") String prcGroupId);

    /**
     * 价格组所有门店列表
     *
     * @param client
     * @param storeIds
     * @return
     */
    List<GaiaStoreData> queryAllPriceStore(@Param("client") String client, @Param("storeIds") List<String> storeIds);

    /**
     * 价格组所有商品列表
     *
     * @param client
     * @param groupId
     * @return
     */
    List<PriceGroupProductVO> selectProByPriceGroup(@Param("client") String client, @Param("groupId") String groupId);

    /**
     * 价格组所有列表
     *
     * @param client
     * @return
     */
    List<GaiaPriceGroup> selectStoreInPriceGroup(@Param("client") String client);

    /**
     * 价格组最大id
     *
     * @param client
     * @return
     */
    String selectMaxGroupId(@Param("client") String client);

    /**
     * 批量插入价格组
     *
     * @param list
     */
    void insertBatch(List<GaiaPriceGroup> list);

    /**
     * 插入，门店商品
     *
     * @param list
     */
    void insertStoreProBatch(List<GaiaSdProductPrice> list);


    List<GaiaSdProductPrice> selectStoreProBatch(@Param("client") String client, @Param("list") List<GaiaSdProductPrice> list);

    /**
     * 更新，门店商品
     *
     * @param list
     */
    void updateStoreProBatch(List<GaiaSdProductPrice> list);

    /**
     * 批量更新价格组
     *
     * @param list
     */
    void updateBatch(List<GaiaPriceGroup> list);

    /**
     * 价格组下的门店列表
     *
     * @param client
     * @param prcGroupId
     * @return
     */
    List<String> selectPriceGroupStore(@Param("client") String client, @Param("prcGroupId") String prcGroupId);


    /**
     * 查询门店商品
     *
     * @param client
     * @param store
     * @return
     */
    List<String> selectBatchStorePro(@Param("client") String client, @Param("store") String store);

    /**
     *
     *
     * @param client
     * @param proId
     * @return
     */
    List<String> selectBatchStore(@Param("client") String client, @Param("proId") String proId);

    /**
     * 价格组所有商品列表明细
     *
     * @param client
     * @param groupId
     * @return
     */
    List<PriceGroupProductVO> selectProByGroup(@Param("client") String client, @Param("groupId") String groupId);


    /**
     * 批量插入价格组商品
     *
     * @param list
     */
    void insertPriceProBatch(List<PriceGroupProductVO> list);

    /**
     * 批量更新价格组商品
     *
     * @param list
     */
    void updateGroupProBatch(List<PriceGroupProductVO> list);


}