package com.gov.purchase.mapper;

import com.gov.purchase.entity.*;
import com.gov.purchase.module.wholesale.dto.WholeSaleChannelDetailDTO;
import com.gov.purchase.module.wholesale.dto.WholeSaleChannelDetailVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaWholesaleChannelMapper {
    int deleteByPrimaryKey(GaiaWholesaleChannelKey key);

    int insert(GaiaWholesaleChannel record);

    int insertSelective(GaiaWholesaleChannel record);

    GaiaWholesaleChannel selectByPrimaryKey(GaiaWholesaleChannelKey key);

    int updateByPrimaryKeySelective(GaiaWholesaleChannel record);

    int updateByPrimaryKey(GaiaWholesaleChannel record);

    /**
     * 查询批发渠道列表
     *
     * @param client
     * @param dcCode
     * @param proCode
     * @param cusCode
     * @param channelName
     * @return
     */
    List<WholeSaleChannelDetailDTO> queryWholeSaleChannelData(
            @Param("client") String client,
            @Param("dcCode") String dcCode,
            @Param("proCode") String proCode,
            @Param("cusCode") String cusCode,
            @Param("channelName") String channelName
    );

    /**
     * @param client
     * @param dcCode
     * @param chaCode
     * @return
     */
    List<GaiaWholesaleChannel> queryWholeSaleChannelDataByChaCode(
            @Param("client") String client,
            @Param("dcCode") String dcCode,
            @Param("chaCode") String chaCode
    );

    /**
     * 查询渠道列表
     *
     * @param client
     * @return
     */
    List<GaiaWholesaleChannel> queryWholeSaleChannelChaCode(
            @Param("client") String client
    );

    /**
     * 根据自编码类型查询批发渠道明细（0 商品， 1 用户）
     *
     * @param client
     * @param dcCode
     * @param chaCode
     * @param type
     * @return
     */
    List<WholeSaleChannelDetailVo> queryWholeSaleChannelDetailByType(
            @Param("client") String client,
            @Param("dcCode") String dcCode,
            @Param("chaCode") String chaCode,
            @Param("type") String type
    );

    List<GaiaWholesaleChannelDetail> queryWholeSaleChannelDetail(
            @Param("client") String client,
            @Param("dcCode") String dcCode,
            @Param("chaCode") String chaCode,
            @Param("selfCode") String selfCode,
            @Param("type") String type
    );

    /**
     * 删除明细
     *
     * @param client
     * @param dcCode
     * @param chaCode
     * @param type
     */
    void deleteWholeSaleChannelDetail(@Param("client") String client,
                                      @Param("dcCode") String dcCode,
                                      @Param("chaCode") String chaCode,
                                      @Param("type") String type);

    /**
     * 批量删除
     *
     * @param client
     * @param dcCode
     * @param chaCode
     * @param type
     * @param list
     */
    void deleteBatch(@Param("client") String client,
                     @Param("dcCode") String dcCode,
                     @Param("chaCode") String chaCode,
                     @Param("type") String type,
                     @Param("list") List<String> list);

    /**
     * 批量插入
     *
     * @param list
     */
    void insertBatchWholeSaleChannelDetail(List<GaiaWholesaleChannelDetail> list);

    /**
     * 查询所有商品
     *
     * @param client
     * @param dcCode
     * @return
     */
    List<GaiaProductBusiness> queryProductBusiness(@Param("client") String client, @Param("dcCode") String dcCode);

    /**
     * 查询所有用户
     *
     * @param client
     * @param dcCode
     * @return
     */
    List<GaiaCustomerBusiness> queryCustomerBusiness(@Param("client") String client, @Param("dcCode") String dcCode);

    /**
     * 批发渠道渠道明细
     *
     * @param client
     * @param soCompanyCode
     * @return
     */
    List<GaiaWholesaleChannelDetail> selectDetailList(@Param("client") String client, @Param("gwcdDcCode") String soCompanyCode);
}