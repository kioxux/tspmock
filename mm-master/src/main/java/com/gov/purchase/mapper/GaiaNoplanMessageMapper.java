package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaNoplanMessage;
import org.apache.ibatis.annotations.Param;

public interface GaiaNoplanMessageMapper {
    void insert(GaiaNoplanMessage noplanMessage);

    GaiaNoplanMessage findIn121Day(@Param("client") String client,@Param("proCode") String proCode);

    void updateWeekMsg(GaiaNoplanMessage noplanMessage);
}
