package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaFiInvoiceRegister;
import com.gov.purchase.entity.GaiaFiInvoiceRegisterKey;
import com.gov.purchase.module.purchase.dto.*;

import java.util.List;

public interface GaiaFiInvoiceRegisterMapper {
    int deleteByPrimaryKey(GaiaFiInvoiceRegisterKey key);

    int insert(GaiaFiInvoiceRegister record);

    int insertSelective(GaiaFiInvoiceRegister record);

    GaiaFiInvoiceRegister selectByPrimaryKey(GaiaFiInvoiceRegisterKey key);

    int updateByPrimaryKeySelective(GaiaFiInvoiceRegister record);

    int updateByPrimaryKey(GaiaFiInvoiceRegister record);

    /**
     * 发票登记查询
     * @param dto InvoiceRegisterListRequestDto
     * @return InvoiceRegisterListResponseDto
     */
    List<InvoiceRegisterListResponseDto> selectInvoiceRegisterList(InvoiceRegisterListRequestDto dto);

    /**
     * 发票列表查询
     * @param dto InvoiceListRequestDto
     * @return InvoiceListResponseDto
     */
    List<InvoiceListResponseDto> selectInvoiceList(InvoiceListRequestDto dto);

    /**
     * 发票明细查询
     * @param dto InvoiceDetailsRequestDto
     * @return InvoiceDetailsDto
     */
    List<InvoiceDetailsDto> selectInvoiceDetails(InvoiceDetailsRequestDto dto);

    /**
     * 付款单明细
     */
    List<PayOrderDetailsResponseDto> selectPayOrderDetails(PayOrderDetailsRequestDto dto);
}