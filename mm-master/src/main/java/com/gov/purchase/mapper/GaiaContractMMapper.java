package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaContractM;
import com.gov.purchase.entity.GaiaContractMKey;
import com.gov.purchase.module.purchase.dto.PurchaseContractDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaContractMMapper {
    int deleteByPrimaryKey(GaiaContractMKey key);

    int insert(GaiaContractM record);

    int insertSelective(GaiaContractM record);

    GaiaContractM selectByPrimaryKey(GaiaContractMKey key);

    int updateByPrimaryKeySelective(GaiaContractM record);

    int updateByPrimaryKey(GaiaContractM record);

    /**
     * 作废合同
     *
     * @param conId
     * @param client
     * @return
     */
    int invalidPurchaseContract(@Param("client") String client, @Param("conId") String conId);

    /**
     * 合同明细列表
     *
     * @param client
     * @param conId
     * @return
     */
    List<PurchaseContractDetailVO> queryContractDetail(@Param("client") String client, @Param("conId") String conId);

    /**
     * 批量插入
     *
     * @param record
     */
    int insertBatch(@Param("list") List<GaiaContractM> record);

    /**
     * 审批明细
     *
     * @param client
     * @param conCompanyCode
     * @param conSupplierId
     * @param conId
     * @param conCompileIndex
     * @return
     */
    List<PurchaseContractDetailVO> queryPurchaseApproveContractDetails(String client, String conCompanyCode, String conSupplierId, String conId, Integer conCompileIndex);
}