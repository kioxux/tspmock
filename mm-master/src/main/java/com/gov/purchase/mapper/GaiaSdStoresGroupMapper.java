package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdStoresGroup;
import com.gov.purchase.entity.GaiaSdStoresGroupKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdStoresGroupMapper {
    int deleteByPrimaryKey(GaiaSdStoresGroupKey key);

    int insert(GaiaSdStoresGroup record);

    int insertSelective(GaiaSdStoresGroup record);

    GaiaSdStoresGroup selectByPrimaryKey(GaiaSdStoresGroupKey key);

    int updateByPrimaryKeySelective(GaiaSdStoresGroup record);

    int updateByPrimaryKey(GaiaSdStoresGroup record);

    List<GaiaSdStoresGroup> selectSdStoresGroupList(@Param("client") String client);
}