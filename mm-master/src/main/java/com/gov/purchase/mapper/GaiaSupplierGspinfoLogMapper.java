package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSupplierGspinfoLog;
import com.gov.purchase.entity.GaiaSupplierGspinfoLogKey;

public interface GaiaSupplierGspinfoLogMapper {
    int deleteByPrimaryKey(GaiaSupplierGspinfoLogKey key);

    int insert(GaiaSupplierGspinfoLog record);

    int insertSelective(GaiaSupplierGspinfoLog record);

    GaiaSupplierGspinfoLog selectByPrimaryKey(GaiaSupplierGspinfoLogKey key);

    int updateByPrimaryKeySelective(GaiaSupplierGspinfoLog record);

    int updateByPrimaryKey(GaiaSupplierGspinfoLog record);
}