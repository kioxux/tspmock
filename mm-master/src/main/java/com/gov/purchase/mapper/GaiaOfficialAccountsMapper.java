package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaOfficialAccounts;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaOfficialAccountsMapper {
    int deleteByPrimaryKey(String id);

    int insert(GaiaOfficialAccounts record);

    int insertSelective(GaiaOfficialAccounts record);

    GaiaOfficialAccounts selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(GaiaOfficialAccounts record);

    int updateByPrimaryKey(GaiaOfficialAccounts record);

    List<GaiaOfficialAccounts> selectConfig(@Param("client") String client, @Param("type") int type, @Param("code") String code);

    List<GaiaOfficialAccounts> selectListByOriginalId(@Param("originalId") String originalId);
}