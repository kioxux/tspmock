package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdRechargeCard;
import com.gov.purchase.entity.GaiaSdRechargeCardKey;

public interface GaiaSdRechargeCardMapper {
    int deleteByPrimaryKey(GaiaSdRechargeCardKey key);

    int insert(GaiaSdRechargeCard record);

    int insertSelective(GaiaSdRechargeCard record);

    GaiaSdRechargeCard selectByPrimaryKey(GaiaSdRechargeCardKey key);

    int updateByPrimaryKeySelective(GaiaSdRechargeCard record);

    int updateByPrimaryKey(GaiaSdRechargeCard record);
}