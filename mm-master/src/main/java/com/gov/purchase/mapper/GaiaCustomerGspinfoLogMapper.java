package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaCustomerGspinfoLog;
import com.gov.purchase.entity.GaiaCustomerGspinfoLogKey;

public interface GaiaCustomerGspinfoLogMapper {
    int deleteByPrimaryKey(GaiaCustomerGspinfoLogKey key);

    int insert(GaiaCustomerGspinfoLog record);

    int insertSelective(GaiaCustomerGspinfoLog record);

    GaiaCustomerGspinfoLog selectByPrimaryKey(GaiaCustomerGspinfoLogKey key);

    int updateByPrimaryKeySelective(GaiaCustomerGspinfoLog record);

    int updateByPrimaryKey(GaiaCustomerGspinfoLog record);
}