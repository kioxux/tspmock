package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaContractZ;
import com.gov.purchase.entity.GaiaContractZKey;
import com.gov.purchase.module.purchase.dto.GaiaContractZDetails;
import com.gov.purchase.module.purchase.dto.PurchaseContractDTO;
import com.gov.purchase.module.purchase.dto.PurchaseContractVO;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaContractZMapper {
    int deleteByPrimaryKey(GaiaContractZKey key);

    int insert(GaiaContractZ record);

    int insertSelective(GaiaContractZ record);

    GaiaContractZ selectByPrimaryKey(GaiaContractZKey key);

    int updateByPrimaryKeySelective(GaiaContractZ record);

    int updateByPrimaryKey(GaiaContractZ record);

    /**
     * 作废合同
     *
     * @param conId
     * @param client
     * @return
     */
    int invalidPurchaseContract(@Param("client") String client, @Param("conId") String conId);

    /**
     * 合同列表
     *
     * @param purchaseContractDTO
     * @return
     */
    List<PurchaseContractVO> queryContract(PurchaseContractDTO purchaseContractDTO);

    /**
     * 合同编号
     *
     * @param client
     * @param conId
     * @return
     */
    String getConId(@Param("client") String client, @Param("conId") String conId);

    /**
     * 合同列表
     *
     * @param client
     * @param conCompanyCode
     * @param conSupplierId
     * @param conSalesmanCode
     * @return
     */
    List<GaiaContractZDetails> selectQueryContractList(@Param("client") String client,
                                                       @Param("conCompanyCode") String conCompanyCode,
                                                       @Param("conSupplierId") String conSupplierId,
                                                       @Param("conSalesmanCode") String conSalesmanCode,
                                                       @Param("conId") String conId,
                                                       @Param("proSelfCode") String proSelfCode,
                                                       @Param("conType") String conType,
                                                       @Param("conCreateDateStart") String conCreateDateStart,
                                                       @Param("conCreateDateEnd") String conCreateDateEnd,
                                                       @Param("type") String type
    );

    GaiaContractZ selectByConId(@Param("client") String client, @Param("conId") String conId);

    /**
     * 合同审批列表
     *
     * @param client
     * @param conCompanyCode
     * @param conSupplierId
     * @param conSalesmanCode
     * @return
     */
    List<GaiaContractZDetails> queryPurchaseApproveContractList(@Param("client") String client, @Param("conCompanyCode") String conCompanyCode,
                                                                @Param("conSupplierId") String conSupplierId, @Param("conSalesmanCode") String conSalesmanCode);

    /**
     * 合同列表
     *
     * @param client
     * @param conIdList
     * @return
     */
    List<GaiaContractZ> selectGaiaContractZByConIds(@Param("client") String client, @Param("conIdList") List<String> conIdList);

}