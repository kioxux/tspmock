package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPoItem;
import com.gov.purchase.entity.GaiaPoItemKey;
import com.gov.purchase.module.purchase.dto.PoItemRequestDto;
import com.gov.purchase.module.purchase.dto.PurchaseItemResponseDto;
import com.gov.purchase.module.purchase.dto.PurchasePlanReviewDetailVo;
import com.gov.purchase.module.wholesale.dto.DrugPurchaseDTO;
import com.gov.purchase.module.wholesale.dto.DrugPurchaseVo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GaiaPoItemMapper {
    int deleteByPrimaryKey(GaiaPoItemKey key);

    int insert(GaiaPoItem record);

    int insertSelective(GaiaPoItem record);

    GaiaPoItem selectByPrimaryKey(GaiaPoItemKey key);

    int updateByPrimaryKeySelective(GaiaPoItem record);

    int updateByPrimaryKey(GaiaPoItem record);

    /**
     * 采购价格平均值
     *
     * @return
     */
    BigDecimal selectUnitPrice(@Param("client") String client, @Param("poSiteCode") String poSiteCode, @Param("poProCode") String poProCode);

    /**
     * 参数删除标记1-是的场合
     *
     * @return
     */
    int updatepoLineDelete(GaiaPoItem record);

    /**
     * 订单行号查询
     *
     * @return
     */
    String selectLineNo(@Param("poIdforItem") String poIdforItem, @Param("client") String client);

    /**
     * 采购次数获取
     *
     * @return
     */
    List<GaiaPoItem> selectSaleTime(@Param("client") String client, @Param("poSiteCode") String poSiteCode, @Param("poProCode") String poProCode);

    /**
     * 打印明细数据
     *
     * @param dto
     * @return
     */
    List<PurchaseItemResponseDto> getPoItemList(PoItemRequestDto dto);

    /**
     * 删除数据
     */
    void deleteByClientAndPoId(@Param("client") String client, @Param("poId") String poId);

    int updateByPrimaryKeySelective2(GaiaPoItem record);

    /**
     * 药品采购记录明细
     *
     * @param drugPurchaseDTO
     * @return
     */
    List<DrugPurchaseVo> queryDrugPurchaseRecord(DrugPurchaseDTO drugPurchaseDTO);

    /**
     * 药品到货数量
     *
     * @param client   加盟商
     * @param siteCode 地点
     * @param poID     采购单号
     * @param poLineNo 采购单行号
     * @param proCode  商品编码
     * @return
     */
    BigDecimal queryQtyCount(@Param("client") String client,
                             @Param("siteCode") String siteCode,
                             @Param("poID") String poID,
                             @Param("poLineNo") String poLineNo,
                             @Param("proCode") String proCode
    );


    /**
     * 查询采购员列表
     *
     * @param drugPurchaseDTO
     * @return
     */
    List<String> queryProPurchaseRate(DrugPurchaseDTO drugPurchaseDTO);

    /**
     * 采购计划明细总和
     *
     * @param client
     * @param poIds
     * @return
     */
    List<GaiaPoItem> queryPurchasePlanReviewItemSum(@Param("client") String client, @Param("poIds") List<String> poIds);

    /**
     * 采购计划明细
     *
     * @param client
     * @param poId
     * @return
     */
    List<PurchasePlanReviewDetailVo> queryPurchasePlanReviewDetailList(@Param("client") String client, @Param("poId") String poId);

    /**
     * 批量插入采购计划明细
     *
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<GaiaPoItem> list);

    /**
     * 检查同品在途订单
     *
     * @param client
     * @param proSelfCode
     * @param poCompanyCode
     * @return
     */
    List<GaiaPoItem> checkExistOrder(@Param("client") String client, @Param("poCompanyCode") String poCompanyCode, @Param("proSelfCode") String proSelfCode);
}