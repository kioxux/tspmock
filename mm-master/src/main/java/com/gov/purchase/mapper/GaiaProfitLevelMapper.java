package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProfitLevel;
import com.gov.purchase.entity.GaiaProfitLevelKey;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GaiaProfitLevelMapper {
    int deleteByPrimaryKey(GaiaProfitLevelKey key);

    int insert(GaiaProfitLevel record);

    int insertSelective(GaiaProfitLevel record);

    GaiaProfitLevel selectByPrimaryKey(GaiaProfitLevelKey key);

    int updateByPrimaryKeySelective(GaiaProfitLevel record);

    int updateByPrimaryKey(GaiaProfitLevel record);

    GaiaProfitLevel selectProfit(@Param("client") String client, @Param("profitRate") BigDecimal profitRate);

    List<GaiaProfitLevel> selectProfitList(@Param("client") String client);
}