package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaLabelApply;
import com.gov.purchase.entity.GaiaLabelApplyKey;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.store.dto.GaiaLabelApplyDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaLabelApplyMapper {
    int deleteByPrimaryKey(GaiaLabelApplyKey key);

    int insert(GaiaLabelApply record);

    int insertSelective(GaiaLabelApply record);

    GaiaLabelApply selectByPrimaryKey(GaiaLabelApplyKey key);

    int updateByPrimaryKeySelective(GaiaLabelApply record);

    int updateByPrimaryKey(GaiaLabelApply record);

    String selectLabApplyNo(@Param("client") String client);

    void insertList(@Param("labelApplyList") List<GaiaLabelApply> list);

    List<GaiaLabelApplyDto> getLabelList(@Param("client") String client, @Param("labApplyDate") String labApplyDate,
                                         @Param("labProduct") String labProduct, @Param("labSuatus") String labSuatus,
                                         @Param("labStore") String labStore, @Param("labApplyNo") String labApplyNo,
                                         @Param("labApplyDateStart") String labApplyDateStart, @Param("labApplyDateEnd") String labApplyDateEnd,
                                         @Param("labPrintTimes") Integer labPrintTimes, @Param("userId") String userId);

    List<GaiaLabelApplyDto> getLabelItemList(@Param("client") String client, @Param("labStore") String labStore, @Param("labApplyNo") String labApplyNo);

    void print(@Param("client") String client, @Param("labStore") String labStore, @Param("labApplyNo") String labApplyNo, @Param("proSelfCodeList") List<String> proSelfCodeList);

    void output(@Param("client") String client, @Param("labStore") String labStore, @Param("labApplyNo") String labApplyNo);

    void delLabel(@Param("client") String client, @Param("labStore") String labStore, @Param("labApplyNo") String labApplyNo);

    /**
     * 有权限的门店列表
     *
     * @param client
     * @param userId
     * @return
     */
    List<GaiaStoreData> getLabelStoList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 申请明细(不携带商品信息)
     */
    List<String> getLabelItemNoStoInfoList(@Param("client") String client, @Param("labApplyNo") String labApplyNo, @Param("labStore") String labStore);

    /**
     * 删除单条
     */
    void delLabelItem(@Param("client") String client, @Param("labStore") String labStore, @Param("labApplyNo") String labApplyNo, @Param("pro") String pro);

    /**
     * 编辑单条
     */
    void editLabelItem(@Param("client") String client, @Param("labStore") String labStore, @Param("labApplyNo") String labApplyNo,
                       @Param("pro") String pro, @Param("labApplyQty") String labApplyQty);

    /**
     * 申请单关闭
     *
     * @param client
     * @param labStore
     * @param labApplyNo
     */
    void closeLable(@Param("client") String client, @Param("labStore") String labStore, @Param("labApplyNo") String labApplyNo);

    /**
     * 申请商品集合
     * @param client
     * @param labStore
     * @param labApplyNo
     * @return
     */
    List<String> getLabelItems(@Param("client") String client, @Param("labStore") String labStore, @Param("labApplyNo") String labApplyNo);
}