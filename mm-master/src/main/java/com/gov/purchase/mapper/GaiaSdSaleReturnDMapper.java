package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdSaleReturnD;
import com.gov.purchase.entity.GaiaSdSaleReturnDKey;

public interface GaiaSdSaleReturnDMapper {
    int deleteByPrimaryKey(GaiaSdSaleReturnDKey key);

    int insert(GaiaSdSaleReturnD record);

    int insertSelective(GaiaSdSaleReturnD record);

    GaiaSdSaleReturnD selectByPrimaryKey(GaiaSdSaleReturnDKey key);

    int updateByPrimaryKeySelective(GaiaSdSaleReturnD record);

    int updateByPrimaryKey(GaiaSdSaleReturnD record);
}