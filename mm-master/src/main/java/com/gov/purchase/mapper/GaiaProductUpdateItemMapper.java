package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductUpdateItem;
import com.gov.purchase.entity.GaiaProductUpdateItemKey;
import com.gov.purchase.module.base.dto.Dictionary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductUpdateItemMapper {
    int deleteByPrimaryKey(GaiaProductUpdateItemKey key);

    int insert(GaiaProductUpdateItem record);

    int insertSelective(GaiaProductUpdateItem record);

    GaiaProductUpdateItem selectByPrimaryKey(GaiaProductUpdateItemKey key);

    int updateByPrimaryKeySelective(GaiaProductUpdateItem record);

    int updateByPrimaryKey(GaiaProductUpdateItem record);

    /**
     * 修改字段
     *
     * @param client
     * @return
     */
    List<GaiaProductUpdateItem> selectProductUpdateItemList(@Param("client") String client);

    /**
     * 修改地点
     *
     * @param client
     * @return
     */
    List<Dictionary> selectProductUpdateSiteList(@Param("client") String client);
}