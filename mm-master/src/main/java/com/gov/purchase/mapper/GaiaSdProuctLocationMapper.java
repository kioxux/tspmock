package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdProuctLocation;
import com.gov.purchase.entity.GaiaSdProuctLocationKey;

public interface GaiaSdProuctLocationMapper {
    int deleteByPrimaryKey(GaiaSdProuctLocationKey key);

    int insert(GaiaSdProuctLocation record);

    int insertSelective(GaiaSdProuctLocation record);

    GaiaSdProuctLocation selectByPrimaryKey(GaiaSdProuctLocationKey key);

    int updateByPrimaryKeySelective(GaiaSdProuctLocation record);

    int updateByPrimaryKey(GaiaSdProuctLocation record);
}