package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductSclass;
import com.gov.purchase.entity.GaiaProductSclassKey;

public interface GaiaProductSclassMapper {
    int deleteByPrimaryKey(GaiaProductSclassKey key);

    int insert(GaiaProductSclass record);

    int insertSelective(GaiaProductSclass record);

    GaiaProductSclass selectByPrimaryKey(GaiaProductSclassKey key);

    int updateByPrimaryKeySelective(GaiaProductSclass record);

    int updateByPrimaryKey(GaiaProductSclass record);
}