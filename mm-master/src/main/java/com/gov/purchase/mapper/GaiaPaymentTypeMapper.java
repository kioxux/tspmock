package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPaymentType;

import java.util.List;

public interface GaiaPaymentTypeMapper {
    int deleteByPrimaryKey(String payType);

    int insert(GaiaPaymentType record);

    int insertSelective(GaiaPaymentType record);

    GaiaPaymentType selectByPrimaryKey(String payType);

    int updateByPrimaryKeySelective(GaiaPaymentType record);

    int updateByPrimaryKey(GaiaPaymentType record);

    List<GaiaPaymentType> selectPaymentTypeList();
}