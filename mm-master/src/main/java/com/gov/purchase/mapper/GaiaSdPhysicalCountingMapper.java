package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdPhysicalCounting;
import com.gov.purchase.entity.GaiaSdPhysicalCountingKey;

public interface GaiaSdPhysicalCountingMapper {
    int deleteByPrimaryKey(GaiaSdPhysicalCountingKey key);

    int insert(GaiaSdPhysicalCounting record);

    int insertSelective(GaiaSdPhysicalCounting record);

    GaiaSdPhysicalCounting selectByPrimaryKey(GaiaSdPhysicalCountingKey key);

    int updateByPrimaryKeySelective(GaiaSdPhysicalCounting record);

    int updateByPrimaryKey(GaiaSdPhysicalCounting record);
}