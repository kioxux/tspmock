package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaMaterialDocLogH;
import com.gov.purchase.entity.GaiaMaterialDocLogHWithBLOBs;

import java.util.List;

public interface GaiaMaterialDocLogHMapper {
    int deleteByPrimaryKey(String guid);

    int insert(GaiaMaterialDocLogH record);

    int insertSelective(GaiaMaterialDocLogH record);

    GaiaMaterialDocLogH selectByPrimaryKey(String guid);

    int updateByPrimaryKeySelective(GaiaMaterialDocLogHWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(GaiaMaterialDocLogH record);

    int updateByPrimaryKey(GaiaMaterialDocLogH record);

    List<GaiaMaterialDocLogHWithBLOBs> selectGaiaMaterialDocLogHError();
}