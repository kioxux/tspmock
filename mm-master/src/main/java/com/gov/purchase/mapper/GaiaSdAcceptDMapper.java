package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdAcceptD;
import com.gov.purchase.entity.GaiaSdAcceptDKey;

public interface GaiaSdAcceptDMapper {
    int deleteByPrimaryKey(GaiaSdAcceptDKey key);

    int insert(GaiaSdAcceptD record);

    int insertSelective(GaiaSdAcceptD record);

    GaiaSdAcceptD selectByPrimaryKey(GaiaSdAcceptDKey key);

    int updateByPrimaryKeySelective(GaiaSdAcceptD record);

    int updateByPrimaryKey(GaiaSdAcceptD record);
}