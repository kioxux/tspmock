package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductClass;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.goods.dto.GoodsClassResponseDTO;
import com.gov.purchase.module.replenishment.dto.GaiaProductClassDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductClassMapper {
    int deleteByPrimaryKey(String proClassCode);

    int insert(GaiaProductClass record);

    int insertSelective(GaiaProductClass record);

    GaiaProductClass selectByPrimaryKey(String proClassCode);

    int updateByPrimaryKeySelective(GaiaProductClass record);

    int updateByPrimaryKey(GaiaProductClass record);

    /**
     * 取得商品分类
     */
    List<String> getProClass(@Param("searchValBasic") SearchValBasic searchValBasic);

    /**
     * 全部商品列表
     */
    List<GoodsClassResponseDTO> getClassList();

    /**
     * 商品大类
     */
    List<GaiaProductClassDTO> getProductClass();

    List<GaiaProductClass> selectGaiaProductClass();
}