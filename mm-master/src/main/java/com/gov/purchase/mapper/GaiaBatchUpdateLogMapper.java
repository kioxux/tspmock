package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBatchUpdateLog;
import com.gov.purchase.module.base.dto.BatchResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaBatchUpdateLogMapper {
    int deleteByPrimaryKey(String bulUpdateCode);

    int insert(GaiaBatchUpdateLog record);

    int insertSelective(GaiaBatchUpdateLog record);

    GaiaBatchUpdateLog selectByPrimaryKey(String bulUpdateCode);

    int updateByPrimaryKeySelective(GaiaBatchUpdateLog record);

    int updateByPrimaryKey(GaiaBatchUpdateLog record);

    /**
     * 批量操作编码
     *
     * @return
     */
    String selectBulUpdateCode();

    /**
     * 批量导入列表
     *
     * @param bulDateType     数据类型
     * @param bulUpdateType   操作类型
     * @param bulUpdateStatus 操作状态
     * @return
     */
    List<BatchResult> queryBatchList(@Param("bulDateType") String bulDateType,
                                     @Param("bulUpdateType") String bulUpdateType,
                                     @Param("bulUpdateStatus") String bulUpdateStatus);
}