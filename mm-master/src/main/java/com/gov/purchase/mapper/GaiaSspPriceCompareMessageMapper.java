package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSspPriceCompare;import com.gov.purchase.entity.GaiaSspPriceCompareMessage;import org.apache.ibatis.annotations.Param;

public interface GaiaSspPriceCompareMessageMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaSspPriceCompareMessage record);

    int insertSelective(GaiaSspPriceCompareMessage record);

    GaiaSspPriceCompareMessage selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaSspPriceCompareMessage record);

    int updateByPrimaryKey(GaiaSspPriceCompareMessage record);

    void insertSelectList(@Param("priceCompare") GaiaSspPriceCompare priceCompare, @Param("userId") String userId);
}