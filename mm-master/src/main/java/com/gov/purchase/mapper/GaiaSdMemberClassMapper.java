package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdMemberClass;
import com.gov.purchase.entity.GaiaSdMemberClassKey;

public interface GaiaSdMemberClassMapper {
    int deleteByPrimaryKey(GaiaSdMemberClassKey key);

    int insert(GaiaSdMemberClass record);

    int insertSelective(GaiaSdMemberClass record);

    GaiaSdMemberClass selectByPrimaryKey(GaiaSdMemberClassKey key);

    int updateByPrimaryKeySelective(GaiaSdMemberClass record);

    int updateByPrimaryKey(GaiaSdMemberClass record);
}