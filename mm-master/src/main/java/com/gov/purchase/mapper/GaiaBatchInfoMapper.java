package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBatchInfo;
import com.gov.purchase.entity.GaiaBatchInfoKey;

import java.util.List;

public interface GaiaBatchInfoMapper {
    int deleteByPrimaryKey(GaiaBatchInfoKey key);

    int insert(GaiaBatchInfo record);

    int insertSelective(GaiaBatchInfo record);

    GaiaBatchInfo selectByPrimaryKey(GaiaBatchInfoKey key);

    int updateByPrimaryKeySelective(GaiaBatchInfo record);

    int updateByPrimaryKey(GaiaBatchInfo record);

    GaiaBatchInfo selectForMdoc(GaiaBatchInfoKey key);

    List<String> selectBatch(GaiaBatchInfoKey key);

    Integer updateFPByPrimaryKeySelective(GaiaBatchInfo record);
}