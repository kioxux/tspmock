package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdRemotePrescribeD;
import com.gov.purchase.entity.GaiaSdRemotePrescribeDKey;

public interface GaiaSdRemotePrescribeDMapper {
    int deleteByPrimaryKey(GaiaSdRemotePrescribeDKey key);

    int insert(GaiaSdRemotePrescribeD record);

    int insertSelective(GaiaSdRemotePrescribeD record);

    GaiaSdRemotePrescribeD selectByPrimaryKey(GaiaSdRemotePrescribeDKey key);

    int updateByPrimaryKeySelective(GaiaSdRemotePrescribeD record);

    int updateByPrimaryKey(GaiaSdRemotePrescribeD record);
}