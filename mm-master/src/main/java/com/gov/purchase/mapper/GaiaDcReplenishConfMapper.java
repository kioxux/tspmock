package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaDcReplenishConf;
import com.gov.purchase.entity.GaiaDcReplenishConfKey;

public interface GaiaDcReplenishConfMapper {
    int deleteByPrimaryKey(GaiaDcReplenishConfKey key);

    int insert(GaiaDcReplenishConf record);

    int insertSelective(GaiaDcReplenishConf record);

    GaiaDcReplenishConf selectByPrimaryKey(GaiaDcReplenishConfKey key);

    int updateByPrimaryKeySelective(GaiaDcReplenishConf record);

    int updateByPrimaryKey(GaiaDcReplenishConf record);
}