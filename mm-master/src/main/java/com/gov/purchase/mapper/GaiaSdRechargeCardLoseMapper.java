package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdRechargeCardLose;
import com.gov.purchase.entity.GaiaSdRechargeCardLoseKey;

public interface GaiaSdRechargeCardLoseMapper {
    int deleteByPrimaryKey(GaiaSdRechargeCardLoseKey key);

    int insert(GaiaSdRechargeCardLose record);

    int insertSelective(GaiaSdRechargeCardLose record);

    GaiaSdRechargeCardLose selectByPrimaryKey(GaiaSdRechargeCardLoseKey key);

    int updateByPrimaryKeySelective(GaiaSdRechargeCardLose record);

    int updateByPrimaryKey(GaiaSdRechargeCardLose record);
}