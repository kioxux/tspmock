package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSmsTemplate;
import org.apache.ibatis.annotations.Param;

public interface GaiaSmsTemplateMapper {
    int deleteByPrimaryKey(String smsId);

    int insert(GaiaSmsTemplate record);

    int insertSelective(GaiaSmsTemplate record);

    GaiaSmsTemplate selectByPrimaryKey(String smsId);

    int updateByPrimaryKeySelective(GaiaSmsTemplate record);

    int updateByPrimaryKey(GaiaSmsTemplate record);

    /**
     * 跟据审批流程编号 获取唯一短信模板
     */
    GaiaSmsTemplate selectTemplateByFlowNo(@Param("flowNo") String flowNo);

    /**
     * 跟据审批流程编号 跟新审批状态
     */
    void updateByFlowNo(GaiaSmsTemplate smsTemplate);
}
