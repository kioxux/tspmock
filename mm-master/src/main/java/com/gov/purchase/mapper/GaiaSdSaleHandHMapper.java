package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdSaleHandH;
import com.gov.purchase.entity.GaiaSdSaleHandHKey;

public interface GaiaSdSaleHandHMapper {
    int deleteByPrimaryKey(GaiaSdSaleHandHKey key);

    int insert(GaiaSdSaleHandH record);

    int insertSelective(GaiaSdSaleHandH record);

    GaiaSdSaleHandH selectByPrimaryKey(GaiaSdSaleHandHKey key);

    int updateByPrimaryKeySelective(GaiaSdSaleHandH record);

    int updateByPrimaryKey(GaiaSdSaleHandH record);
}