package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSupplierSalesman;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.SupplierSalesmanProDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalesmanMaintainProMapper {

    // 查询该业务员授权的品种（是否按照品种管控 1：是，0：否）
    String queryIsControl(@Param("client") String client, @Param("supSite") String supSite, @Param("supSelfCode") String supSelfCode, @Param("gssCode") String gssCode);

    // 查询该业务员授权委托书效期
    String queryGssWtsxq(@Param("client") String client, @Param("supSite") String supSite, @Param("supSelfCode") String supSelfCode, @Param("gssCode") String gssCode);

    // 查询业务员对应授权的商品列表
    List<SupplierSalesmanProDTO> queryProductList(@Param("client") String client, @Param("supSite") String supSite, @Param("supSelfCode") String supSelfCode, @Param("gssCode") String gssCode);

    // 修改业务员是否按照品种管控的状态（是否按照品种管控 1：是，0：否）
    void updateIsControl(@Param("client") String client, @Param("supSite") String supSite, @Param("supSelfCode") String supSelfCode, @Param("gssCode") String gssCode, @Param("isControl") String isControl);

    // 删除业务员授权的所有商品
    void deleteAllPro(@Param("client") String client, @Param("supSite") String supSite, @Param("supSelfCode") String supSelfCode, @Param("gssCode") String gssCode);

    // 增加业务员授权的商品
    void addPro(@Param("client") String client, @Param("supSite") String supSite, @Param("supSelfCode") String supSelfCode, @Param("gssCode") String gssCode, @Param("createDate") String createDate, @Param("proCode") String proCode);

    // 查询是否存在该商品
    Integer queryCountPro(@Param("client") String client, @Param("supSite") String supSite, @Param("proCode") String proCode);

    // 查询商品明细列表
    List<SupplierSalesmanProDTO> queryProductDetailsList(@Param("client") String client, @Param("supSite") String supSite, @Param("proInfo") String proInfo, @Param("pageSize") Integer pageSize);

    // 查询商品明细
    SupplierSalesmanProDTO queryProductDetails(@Param("client") String client, @Param("supSite") String supSite, @Param("proCode") String proCode);

    // 根据地点查询供应商业务员列表
    List<GaiaSupplierSalesman> querySupplierSalesmanList(@Param("client") String client, @Param("supSite") String supSite);

    // 根据业务员code授权查找是否存在商品
    Integer queryCountProByGssCode(@Param("client") String client, @Param("supSite") String supSite, @Param("supSelfCode")String supSelfCode, @Param("gssCode")String gssCode, @Param("proCode") String proCode);
}
