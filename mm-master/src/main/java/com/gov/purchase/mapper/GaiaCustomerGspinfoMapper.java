package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaCustomerGspinfo;
import com.gov.purchase.entity.GaiaCustomerGspinfoKey;
import com.gov.purchase.module.customer.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaCustomerGspinfoMapper {
    int deleteByPrimaryKey(GaiaCustomerGspinfoKey key);

    int insert(GaiaCustomerGspinfo record);

    int insertSelective(GaiaCustomerGspinfo record);

    GaiaCustomerGspinfo selectByPrimaryKey(GaiaCustomerGspinfoKey key);

    int updateByPrimaryKeySelective(GaiaCustomerGspinfo record);

    int updateByPrimaryKey(GaiaCustomerGspinfo record);

    List<CustomerGspInfoListReponseDto> queryCustomerGspInfoList(CustomerGspInfoListRequestDto dto);

    // 查询首映详情 带地点
    CustomerGspInfoDetailReponseDto selectByPrimaryKeyWithSiteName(GaiaCustomerGspinfoKey key);

    List<GaiaCustomerGspinfo> selectGspInfoByPrimaryKey(GaiaCustomerGspinfoKey gspinfoKey);

    /**
     * 根据流程编号获取首营信息
     * @param cusFlowNo
     * @return
     */
    List<GaiaCustomerGspinfo> selectCustomerGspByFlowNo(@Param("client") String client,@Param("cusFlowNo") String cusFlowNo);

    GaiaCustomerGspinfo getCustomerGspinfo(@Param("client") String client, @Param("cusSite") String cusSite, @Param("cusSelfCode") String cusSelfCode);

    /**
     * 导出客户首营列表
     * @param dto
     * @return
     */
    List<CustomerGspInfoListReponseDtoCSV> exportQueryCustomerGspList(CustomerGspInfoListRequestDto dto);
}