package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdEmpGroup;
import com.gov.purchase.entity.GaiaSdEmpGroupKey;

public interface GaiaSdEmpGroupMapper {
    int deleteByPrimaryKey(GaiaSdEmpGroupKey key);

    int insert(GaiaSdEmpGroup record);

    int insertSelective(GaiaSdEmpGroup record);

    GaiaSdEmpGroup selectByPrimaryKey(GaiaSdEmpGroupKey key);

    int updateByPrimaryKeySelective(GaiaSdEmpGroup record);

    int updateByPrimaryKey(GaiaSdEmpGroup record);
}