package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaApproveRule;
import org.apache.ibatis.annotations.Param;

public interface GaiaApproveRuleMapper {
    int insert(GaiaApproveRule record);

    int insertSelective(GaiaApproveRule record);

    GaiaApproveRule selectByPrimaryKey(@Param("client")String client, @Param("poCompanyCode")String poCompanyCode);
}