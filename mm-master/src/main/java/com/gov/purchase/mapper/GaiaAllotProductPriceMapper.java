package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaAllotProductPrice;
import com.gov.purchase.entity.GaiaAllotProductPriceKey;
import com.gov.purchase.module.delivery.dto.GaiaAllotProductPriceVO;
import com.gov.purchase.module.delivery.dto.StoreVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaAllotProductPriceMapper {
    int deleteByPrimaryKey(GaiaAllotProductPriceKey key);

    int insert(GaiaAllotProductPrice record);

    int insertSelective(GaiaAllotProductPrice record);

    GaiaAllotProductPrice selectByPrimaryKey(GaiaAllotProductPriceKey key);

    int updateByPrimaryKeySelective(GaiaAllotProductPrice record);

    int updateByPrimaryKey(GaiaAllotProductPrice record);

    //extend
    List<GaiaAllotProductPriceVO> selectByGapgCode(@Param("client") String client,
                                                   @Param("gapgCode") String gapgCode,
                                                   @Param("gapgType") String gapgType);

    List<GaiaAllotProductPrice> selectByPrimaryKeyList(List<GaiaAllotProductPriceKey> list);

    List<StoreVO> selectStoreVO(@Param("client") String client);

    List<StoreVO> selectStoreVOBySite(@Param("client") String client,
                                      @Param("alpReceiveSite") String alpReceiveSite);

    int batchUpdateByPrimaryKey(List<GaiaAllotProductPriceVO> list);

    int batchInsert(List<GaiaAllotProductPriceVO> list);

    int batchDelete(List<GaiaAllotProductPrice> list);

    int selectMaxIndex();
}