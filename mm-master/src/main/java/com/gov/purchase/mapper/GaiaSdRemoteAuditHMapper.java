package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdRemoteAuditH;
import com.gov.purchase.entity.GaiaSdRemoteAuditHKey;

public interface GaiaSdRemoteAuditHMapper {
    int deleteByPrimaryKey(GaiaSdRemoteAuditHKey key);

    int insert(GaiaSdRemoteAuditH record);

    int insertSelective(GaiaSdRemoteAuditH record);

    GaiaSdRemoteAuditH selectByPrimaryKey(GaiaSdRemoteAuditHKey key);

    int updateByPrimaryKeySelective(GaiaSdRemoteAuditH record);

    int updateByPrimaryKey(GaiaSdRemoteAuditH record);
}