package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaFieldRequiredData;
import com.gov.purchase.entity.GaiaFieldRequiredDataKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaFieldRequiredDataMapper {
    int deleteByPrimaryKey(GaiaFieldRequiredDataKey key);

    int insert(GaiaFieldRequiredData record);

    int insertSelective(GaiaFieldRequiredData record);

    GaiaFieldRequiredData selectByPrimaryKey(GaiaFieldRequiredDataKey key);

    int updateByPrimaryKeySelective(GaiaFieldRequiredData record);

    int updateByPrimaryKey(GaiaFieldRequiredData record);

    /**
     * 首营字段配置列表
     *
     * @return 字段配置列表
     */
    List<GaiaFieldRequiredData> queryFieldRequiredDataList();
}