package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaCustomerBusiness;
import com.gov.purchase.entity.GaiaCustomerBusinessKey;
import com.gov.purchase.entity.GaiaCustomerChange;
import com.gov.purchase.module.base.dto.MainStore;
import com.gov.purchase.module.customer.dto.GetCusListRequestDTO;
import com.gov.purchase.module.customer.dto.SaveCusList1RequestDTO;
import com.gov.purchase.module.purchase.dto.customerDto;
import com.gov.purchase.module.supplier.dto.GaiaCustomerBusinessDTO;
import com.gov.purchase.module.wholesale.dto.GaiaBillOfArVO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GaiaCustomerBusinessMapper {
    int deleteByPrimaryKey(GaiaCustomerBusinessKey key);

    int insert(GaiaCustomerBusiness record);

    int insertSelective(GaiaCustomerBusiness record);

    GaiaCustomerBusiness selectByPrimaryKey(GaiaCustomerBusinessKey key);

    int updateByPrimaryKeySelective(GaiaCustomerBusiness record);

    int updateByPrimaryKey(GaiaCustomerBusiness record);

    /**
     * 客户集合下拉框
     *
     * @param client
     * @return
     */
    List<MainStore> mainStore(@Param("client") String client);

    /**
     * 客户信息下拉框
     *
     * @param client
     * @param site
     * @return
     */
    List<customerDto> customerInfo(@Param("client") String client, @Param("site") String site);

    List<GaiaCustomerBusiness> selectBusinessInfoByPrimaryKey(GaiaCustomerBusinessKey businessKey);

    GaiaCustomerBusiness getCustomerBusiness(@Param("client") String client, @Param("cusSite") String cusSite, @Param("cusSelfCode") String cusSelfCode);

    /**
     * 根据加盟商、自编码查询数据
     *
     * @param client      加盟商
     * @param cusSelfCode 自编码
     * @return
     */
    List<GaiaCustomerBusiness> selfOnlyCheck(@Param("client") String client, @Param("cusSelfCode") String cusSelfCode, @Param("cusSite") String cusSite);

    /**
     * 自编码集合
     * @param client
     * @param cusSite
     * @return
     */
    List<String> getSelfCodeList(@Param("client") String client, @Param("cusSite") String cusSite, @Param("cusCode") String proCode);


    /**
     * 地点+对应行的统一社会信用代码精确匹配business表
     */
    List<GaiaCustomerBusiness> getCompList(@Param("client") String client, @Param("list") List<SaveCusList1RequestDTO> list);

    /**
     * 自编码列表
     */
    List<String> getSelfCodeList2(@Param("client") String client, @Param("siteList") List<String> siteList);

    /**
     * 客户列表
     */
    List<GaiaCustomerBusinessDTO> getCusList(@Param("dto") GetCusListRequestDTO dto);

    /**
     * 加盟商客户万列表
     *
     * @param client
     * @return
     */
    List<GaiaCustomerBusiness> getCustomerDataListByClient(@Param("client") String client);

    /**
     * 根据主键批量查询
     *
     * @param customerKeyList 客户主键列表
     * @return 客户信息
     */
    List<GaiaCustomerBusiness> selectCusListByKeyList(@Param("cusKeyList") List<GaiaCustomerBusinessKey> customerKeyList);

    /**
     * 批量更新
     *  @param customerBusiness 数据
     * @param customerKeyList  主键列表
     */
    void updateBatch(@Param("business") GaiaCustomerBusiness customerBusiness, @Param("cusKeyList") List<GaiaCustomerBusinessKey> customerKeyList);

    /**
     * 修改记录批量保存
     *
     * @param changeList 修改记录集合
     */
    void saveChangeList(@Param("list") List<GaiaCustomerChange> changeList);

    /**
     * 查询客户信息通过固定到期号
     *
     * @param cusZqts 固定到期号
     */
    List<GaiaCustomerBusiness> getCusListByZQTS(@Param("cusZqts") Integer cusZqts);

    /**
     * 查询客户信息通过固定到期号
     *
     * @param cusZqts 固定到期号
     */
    List<GaiaCustomerBusiness> getCusListByGreaterOrEqZQTS(@Param("cusZqts") Integer cusZqts);

    void updateByCusArAmt(@Param("billOfAr") GaiaBillOfArVO billOfAr,@Param("arAmt") BigDecimal arAmt);
}