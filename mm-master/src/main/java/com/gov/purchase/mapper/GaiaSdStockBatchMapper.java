package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdStockBatch;
import com.gov.purchase.entity.GaiaSdStockBatchKey;
import com.gov.purchase.module.replenishment.dto.dcReplenish.StoProStock;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdStockBatchMapper {
    int deleteByPrimaryKey(GaiaSdStockBatchKey key);

    int insert(GaiaSdStockBatch record);

    int insertSelective(GaiaSdStockBatch record);

    GaiaSdStockBatch selectByPrimaryKey(GaiaSdStockBatchKey key);

    int updateByPrimaryKeySelective(GaiaSdStockBatch record);

    int updateByPrimaryKey(GaiaSdStockBatch record);

    List<StoProStock> getStoreStock(@Param("client") String client, @Param("dcCode") String dcCode, @Param("proSelfCode") String proSelfCode);
}