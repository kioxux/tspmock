package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaContractMLog;
import com.gov.purchase.entity.GaiaContractMLogKey;
import com.gov.purchase.module.purchase.dto.PurchaseContractDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaContractMLogMapper {
    int deleteByPrimaryKey(GaiaContractMLogKey key);

    int insert(GaiaContractMLog record);

    int insertSelective(GaiaContractMLog record);

    GaiaContractMLog selectByPrimaryKey(GaiaContractMLogKey key);

    int updateByPrimaryKeySelective(GaiaContractMLog record);

    int updateByPrimaryKey(GaiaContractMLog record);

    /**
     * 批量插入合同记录
     *
     * @param record
     * @return
     */
    int insertBatch(@Param("list") List<GaiaContractMLog> record);

    /**
     * 合同明细列表
     *
     * @param client
     * @param conId
     * @return
     */
    List<GaiaContractMLog> queryContractDetail(@Param("client") String client, @Param("conCompanyCode") String conCompanyCode,
                                               @Param("conSupplierId") String conSupplierId, @Param("conId") String conId,
                                               @Param("conCompileIndex") Integer conCompileIndex);

    int deleteByConId(@Param("client") String client, @Param("conId") String conId);

    /**
     * 审批明细
     *
     * @param client
     * @param conCompanyCode
     * @param conSupplierId
     * @param conId
     * @param conCompileIndex
     * @return
     */
    List<PurchaseContractDetailVO> queryPurchaseApproveContractDetails(@Param("client") String client, @Param("conCompanyCode") String conCompanyCode,
                                                                       @Param("conSupplierId") String conSupplierId, @Param("conId") String conId,
                                                                       @Param("conCompileIndex") Integer conCompileIndex);
}
