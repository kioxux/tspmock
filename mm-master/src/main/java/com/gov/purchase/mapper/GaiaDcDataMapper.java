package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaDcDataKey;
import com.gov.purchase.entity.GaiaDcReplenishConf;
import com.gov.purchase.module.base.dto.MainStore;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentListResponseDto;
import com.gov.purchase.module.replenishment.dto.dcReplenish.ProReplenishmentParams;
import com.gov.purchase.module.store.dto.DcDataListRequestDto;
import com.gov.purchase.module.store.dto.GaiaDcDataListDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface GaiaDcDataMapper {
    int deleteByPrimaryKey(GaiaDcDataKey key);

    int insert(GaiaDcData record);

    int insertSelective(GaiaDcData record);

    GaiaDcData selectByPrimaryKey(GaiaDcDataKey key);

    int updateByPrimaryKeySelective(GaiaDcData record);

    int updateByPrimaryKey(GaiaDcData record);

    List<GaiaDcDataListDto> queryDcList(DcDataListRequestDto dto);

    GaiaDcData selectByDcCode(@Param("client") String client, @Param("dcCode") String dcCode);

    List<GaiaDcData> selectSupplierListe(SupplierListForPurchaseRequestDto dto);

    List<GetSiteListResponseDto> selectSiteList(GetSiteListRequestDto dto);

    List<GetSiteListResponseDto> selectSiteList1(GetSiteListRequestDto dto);

    List<GetSiteListResponseDto> storeSelect(StoreSelectRequestDto dto);

    List<MainStore> mainStore(@Param("client") String client);

    /**
     * DC补货列表
     *
     * @param map 参数
     * @return
     */
    List<DcReplenishmentListResponseDto> queryReplenishmentList(Map<String, Object> map);

    /**
     * 批量查询
     *
     * @param searchValBasicMap
     * @return
     */
    List<GaiaDcDataListDto> selectDcBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);

    /**
     * DC补货下拉集合
     *
     * @param client
     * @return
     */
    List<MainStore> getReplenishDcDataList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 门店数量
     *
     * @param client
     * @param dcChainHead
     * @return
     */
    Integer queryStoreCountByChainHead(@Param("client") String client, @Param("dcChainHead") String dcChainHead);

    /**
     * 30天门店销售量
     *
     * @param client
     * @param dcChainHead
     * @param proSelfCode
     * @return
     */
    BigDecimal query30DaysSalesCount(@Param("client") String client, @Param("dcChainHead") String dcChainHead, @Param("proSelfCode") String proSelfCode);

    /**
     * 开票查询
     */
    List<InvoicePayingDTO> listDcCodeTax(ListInvoiceRequestDTO dto);

    /**
     * 付款查询
     */
    List<ListPayingResponseDTO> listPayingDc(ListPayingRequestDTO dto);

    /**
     * 纳税主体查询
     */
    List<ListTaxpayerResponseDto> listDcTaxpayer(ListTaxpayerRequestDTO dto);

    /**
     * 配送中心
     *
     * @param client
     * @return
     */
    List<GaiaDcData> getDcList(@Param("client") String client);

    /**
     * 配送中心列表_权限
     *
     * @param client
     * @param userId
     * @return
     */
    List<MainStore> getAuthDcDataList(@Param("client") String client, @Param("userId") String userId);

    List<GaiaDcData> SelectDcAuth(Map<Object, Object> map);

    /**
     * DC补货查询 新
     *
     * @param client
     * @param dcCode
     * @return
     */
    List<DcReplenishmentListResponseDto> getReplenishmentList(@Param("client") String client, @Param("dcCode") String dcCode,
                                                              @Param("gaiaDcReplenishConf") GaiaDcReplenishConf gaiaDcReplenishConf);

    /**
     * DC补货查询 新
     *
     * @param client
     * @param dcCode
     * @return
     */
    List<DcReplenishmentListResponseDto> getReplenishmentList2(@Param("client") String client, @Param("dcCode") String dcCode,
                                                               @Param("gaiaDcReplenishConf") GaiaDcReplenishConf gaiaDcReplenishConf);

    /**
     * 根据条件获取配送中心列表
     *
     * @param dcCond 查询条件
     */
    List<GaiaDcData> findList(GaiaDcData dcCond);

    /**
     * 商品补货数据表
     *
     * @param client
     * @param dcCode
     * @param proClass
     * @param lastSupp
     * @param recSupp
     * @param proPurchaseRate
     * @return
     */
    List<DcReplenishmentListResponseDto> getProReplenishmentList(@Param("client") String client, @Param("dcCode") String dcCode, @Param("stoCode") String stoCode,
                                                                 @Param("proSelfCode") String proSelfCode, @Param("proClass") String proClass,
                                                                 @Param("lastSupp") String lastSupp, @Param("recSupp") String recSupp,
                                                                 @Param("proZdy1") List<String> proZdy1Name, @Param("proZdy2") List<String> proZdy2Name,
                                                                 @Param("proZdy3") List<String> proZdy3Name, @Param("proZdy4") List<String> proZdy4Name,
                                                                 @Param("proZdy5") List<String> proZdy5Name, @Param("proSlaeClass") String proSlaeClass,
                                                                 @Param("proSclass") String proSclass,
                                                                 @Param("proNoPurchase") Integer proNoPurchase, @Param("proPosition") Integer proPosition,
                                                                 @Param("params") List<ProReplenishmentParams> proReplenishmentParamsList,
                                                                 @Param("salesVolumeStartDate") String salesVolumeStartDate, @Param("salesVolumeEndDate") String salesVolumeEndDate,
                                                                 @Param("proPurchaseRate") String proPurchaseRate, @Param("excDea") Integer excDea,
                                                                 @Param("storeCodes") List<String> storeCodes
    );

    /**
     * 采购员下拉框
     */
    List<String> getProPurchaseRateList(@Param("client") String client);

    List<GaiaDcData> selectDcListByClient(@Param("client") String client);

    /**
     * 新增商品
     *
     * @param client
     * @param dcCode
     * @param proSelfCode
     * @return
     */
    DcReplenishmentListResponseDto addReplenishmentPro(@Param("client") String client, @Param("dcCode") String dcCode, @Param("proSelfCode") String proSelfCode);

    /**
     * 判断仓库编码是否存在
     *
     * @param client
     * @param code
     * @return
     */
    int queryDc(@Param("client") String client,
                @Param("code") String code);

    List<GaiaDcData> getEntrustDcList(@Param("client") String client, @Param("dcCode") String dcCode);

    List<GaiaDcData> selectGaiaDcDataByDcWtdc(@Param("client") String client, @Param("dcWtdc") String dcWtdc);

    Integer selectCompadmWmsCnt(@Param("client") String client, @Param("dcCode") String dcCode);
}
