package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdRemoteAuditD;
import com.gov.purchase.entity.GaiaSdRemoteAuditDKey;

public interface GaiaSdRemoteAuditDMapper {
    int deleteByPrimaryKey(GaiaSdRemoteAuditDKey key);

    int insert(GaiaSdRemoteAuditD record);

    int insertSelective(GaiaSdRemoteAuditD record);

    GaiaSdRemoteAuditD selectByPrimaryKey(GaiaSdRemoteAuditDKey key);

    int updateByPrimaryKeySelective(GaiaSdRemoteAuditD record);

    int updateByPrimaryKey(GaiaSdRemoteAuditD record);
}