package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.entity.GaiaStoreDataKey;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.MainStore;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.base.dto.excel.StoreExport;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.replenishment.dto.GetStoreTypeIdItemListAndAreaListDTO;
import com.gov.purchase.module.replenishment.dto.StoreRequestDto;
import com.gov.purchase.module.store.dto.GaiaStoreDataDto;
import com.gov.purchase.module.store.dto.store.StoreInfoDetailReponseDto;
import com.gov.purchase.module.store.dto.store.StoreInfoDetailRequestDto;
import com.gov.purchase.module.store.dto.store.StoreListRequestDto;
import com.gov.purchase.module.store.dto.store.StoreListResponseDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface GaiaStoreDataMapper {
    int deleteByPrimaryKey(GaiaStoreDataKey key);

    int insert(GaiaStoreData record);

    int insertSelective(GaiaStoreData record);

    GaiaStoreData selectByPrimaryKey(GaiaStoreDataKey key);

    int updateByPrimaryKeySelective(GaiaStoreData record);

    int updateByPrimaryKey(GaiaStoreData record);

    int selectCountByPrimaryKey(GaiaStoreDataKey key);

    /**
     * 门店集合
     *
     * @return
     */
    List<GaiaStoreData> storeDataList();

    /**
     * 查询门店列表  加盟商编号  门店编号 门店名称 门店状态
     *
     * @return
     */
    List<StoreListResponseDto> queryStoreList(StoreListRequestDto dto);

    /**
     * 查询门店详情
     *
     * @return
     */
    StoreInfoDetailReponseDto getStoreInfo(StoreInfoDetailRequestDto dto);

    /**
     * 门店集合下拉框
     *
     * @param client
     * @return
     */
    List<MainStore> mainStore(@Param("client") String client);

    /**
     * 批次库存查询
     */
    List<BatchStockListResponseDto> selectBatchStockList(BatchStockListRequestDto dto);

    /**
     * 物流模式门店
     */
    List<DeliveryStoreResponseDto> selectDeliveryStore(DeliveryStoreRequestDto dto);

    List<DeliveryStoreResponseDto> selectDeliveryStoreShort(DeliveryStoreRequestDto dto);

    /**
     * 加盟商 + DC编号获取门店集合
     *
     * @param dto
     * @return
     */
    List<GaiaStoreData> getStoreListByDc(StoreRequestDto dto);

    /**
     * 批量查询
     *
     * @param searchValBasicMap
     * @return
     */
    List<StoreExport> selectStoreBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);

    /**
     * 纳税主体选择
     *
     * @param chainHead
     * @return
     */
    List<Dictionary> getTaxSubject(@Param("client") String client, @Param("chainHead") String chainHead);

    /**
     * 开票查询
     */
    List<InvoicePayingDTO> listStoreByCodeTax(ListInvoiceRequestDTO dto);

    /**
     * 开票查询
     */
    List<InvoicePayingDTO> listStoreByCodeTaxChainHead(ListInvoiceRequestDTO dto);

    /**
     * 付款查询 单体门店
     */
    List<ListPayingResponseDTO> listPayingStore(ListPayingRequestDTO dto);

    /**
     * 付款查询, 连锁下的门店和DC
     */
    List<ListPayingResponseDTO> listPayingStoreAndDc(ListPayingRequestDTO dto);

    /**
     * 根据加盟商获得门店集合
     */
    List<GaiaStoreData> getStoreListByClient(@Param("client") String client);

    /**
     * 纳税主体查询
     */
    List<ListTaxpayerResponseDto> ListStoreTaxpayer(ListTaxpayerRequestDTO dto);

    /**
     * 纳税主体查询
     */
    List<ListTaxpayerResponseDto> listStoreTaxpayerChainHead(ListTaxpayerRequestDTO dto);

    /**
     * 门店集合
     *
     * @return
     */
    List<GaiaStoreData> SelectStoreAuth(Map<Object, Object> map);

    /**
     * 门店集合
     *
     * @return
     */
    List<GaiaStoreDataDto> selectstoreDataList(Map<Object, Object> map);

    Integer updateBatch(@Param("batchList") List<GaiaStoreData> list);

    /**
     * 门店集合
     *
     * @return
     */
    List<GaiaStoreData> storeListByMap(Map<Object, Object> map);

    /**
     * 门店组集合
     *
     * @return
     */
    List<StoreListResponseDto> storeGroupListByMap(Map<Object, Object> map);


    /**
     * 门店组集合
     *
     * @param map
     * @return
     */
    List<StoreListResponseDto> storeGroupListExportByMap(Map<Object, Object> map);

    /**
     * 数据初始化
     *
     * @param client
     * @return
     */
    int initInvoice(@Param("client") String client);

    /**
     * 配送中心 + 单体店
     *
     * @param client
     * @param userId
     * @return
     */
    List<GaiaStoreData> gspInfoSiteList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 门店集合
     *
     * @param client
     * @return
     */
    List<GaiaStoreData> getStoreDataListByClient(@Param("client") String client);

    /**
     * 清空门店店组
     *
     * @param client
     * @return
     */
    int clearStog(@Param("client") String client);

    /**
     * 非连锁门店列表
     *
     * @param client
     */
    List<String> getNotChainStoCodeList(@Param("client") String client);

    /**
     * 门店主数据地点不为空的 地点
     *
     * @param client 加盟商
     * @return
     */
    List<String> getStoreCodeList(@Param("client") String client);

    /**
     * 获取当前DC下的门店
     *
     * @param client
     * @param dcCode
     * @return
     */
    List<GaiaStoreData> getStoreCont(@Param("client") String client, @Param("dcCode") String dcCode);

    /**
     * 门店编码存在于客户编码中
     */
    List<String> getStoExistsCustomer(@Param("client") String client, @Param("list") List<String> stoCodeList);

    /**
     * 门店编码存在于客户编码中
     */
    String getStoExistsCustomer2(@Param("client") String client, @Param("stoCode") String stoCode);

    List<GaiaStoreData> selectByPrimaryKeyList(@Param("client") String client, @Param("params") List<Map<String, String>> params);

    Integer getStoreCount(@Param("client") String client);

    /**
     * 店型下拉框
     */
    List<GetStoreTypeIdItemListAndAreaListDTO.Item> getStoreTypeIdItemList(@Param("client") String client);

    /**
     * 区域下拉框
     */
    List<GetStoreTypeIdItemListAndAreaListDTO.Item> getStoreAreaList(@Param("client") String client);


    /**
     * 根据连锁总部id查询门店
     * @param client
     * @return
     */
    List<GaiaStoreData> queryStoreListByCompadmId(@Param("client") String client, @Param("stoChainHead") String stoChainHead);

    /**
     * 查询门店信息通过固定到期号
     *
     * @param stoZqts 固定到期号
     */
    List<GaiaStoreData> getStoreListByZQTS(@Param("stoZqts") Integer stoZqts);

    /**
     * 查询门店信息通过固定到期号
     *
     * @param stoZqts 固定到期号
     */
    List<GaiaStoreData> getStoreListByGreaterOrEqZQTS(@Param("stoZqts") Integer stoZqts);

    // 更新门店应收余额
    void increaserRemainingSum(@Param("client")String client,@Param("matSiteCode")String matSiteCode, @Param("matAddAmt")BigDecimal matAddAmt);

    // 初始化门店余额
    void initRemainingSum(@Param("client")String client, @Param("stoCode")String stoCode,@Param("sum") BigDecimal sum);

    List<GaiaStoreData> selectStoreDataListByClient(@Param("client")String client);
}
