package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdSaleH;
import com.gov.purchase.entity.GaiaSdSaleHKey;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdSaleHMapper {
    int deleteByPrimaryKey(GaiaSdSaleHKey key);

    int insert(GaiaSdSaleH record);

    int insertSelective(GaiaSdSaleH record);

    GaiaSdSaleH selectByPrimaryKey(GaiaSdSaleHKey key);

    int updateByPrimaryKeySelective(GaiaSdSaleH record);

    int updateByPrimaryKey(GaiaSdSaleH record);

    void lastUpdateTime(@Param("client") String client, @Param("gsshBillNo") String gsshBillNo);
}