package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdIntegralCashSet;
import com.gov.purchase.entity.GaiaSdIntegralCashSetKey;

public interface GaiaSdIntegralCashSetMapper {
    int deleteByPrimaryKey(GaiaSdIntegralCashSetKey key);

    int insert(GaiaSdIntegralCashSet record);

    int insertSelective(GaiaSdIntegralCashSet record);

    GaiaSdIntegralCashSet selectByPrimaryKey(GaiaSdIntegralCashSetKey key);

    int updateByPrimaryKeySelective(GaiaSdIntegralCashSet record);

    int updateByPrimaryKey(GaiaSdIntegralCashSet record);
}