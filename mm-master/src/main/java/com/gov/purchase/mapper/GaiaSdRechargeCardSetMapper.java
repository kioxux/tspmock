package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdRechargeCardSet;

public interface GaiaSdRechargeCardSetMapper {
    int deleteByPrimaryKey(String client);

    int insert(GaiaSdRechargeCardSet record);

    int insertSelective(GaiaSdRechargeCardSet record);

    GaiaSdRechargeCardSet selectByPrimaryKey(String client);

    int updateByPrimaryKeySelective(GaiaSdRechargeCardSet record);

    int updateByPrimaryKey(GaiaSdRechargeCardSet record);
}