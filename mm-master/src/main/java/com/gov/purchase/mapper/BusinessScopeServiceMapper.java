package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaJyfwData;
import com.gov.purchase.entity.GaiaJyfwDataGspinfoKey;
import com.gov.purchase.module.base.dto.businessScope.GetScopeListDTO;
import com.gov.purchase.module.base.dto.businessScope.JyfwDataDO;
import com.gov.purchase.module.goods.dto.JyfwooData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BusinessScopeServiceMapper {


    /**
     * 经营范围列表
     *
     * @param client
     * @param jyfwOrgid
     * @param jyfwOrgname
     * @return
     */
    List<GetScopeListDTO> getScopeList(@Param("client") String client,
                                       @Param("jyfwOrgid") String jyfwOrgid,
                                       @Param("jyfwOrgname") String jyfwOrgname,
                                       @Param("jyfwSite") String jyfwSite,
                                       @Param("jyfwType") Integer jyfwType,
                                       @Param("jyfwClass") String jyfwClass);

    /**
     * 数据表删除
     *
     * @param client
     * @param jyfwOrgid
     * @param jyfwOrgname
     */
    void deleteScope(@Param("client") String client,
                     @Param("jyfwOrgid") String jyfwOrgid,
                     @Param("jyfwOrgname") String jyfwOrgname,
                     @Param("jyfwSite") String jyfwSite,
                     @Param("jyfwType") Integer jyfwType);

    void deleteScopes(@Param("list") List<Map<String, Object>> list);

    /**
     * 批量插入
     *
     * @param list
     */
    void saveBatchJyfwData(@Param("list") List<JyfwDataDO> list);

    /**
     * @param client
     * @param jyfwOrgid
     * @return
     */
    List<String> getsupScopeList(@Param("client") String client, @Param("jyfwOrgid") String jyfwOrgid, @Param("jyfwType") Integer jyfwType, @Param("jyfwSite") String jyfwSite);

    // 经营范围类别中有3
    List<String> getsupScopeListClass3(@Param("client") String client, @Param("jyfwOrgid") String jyfwOrgid, @Param("jyfwType") Integer jyfwType, @Param("jyfwSite") String jyfwSite);

    /**
     * 特殊经营范围配置表 批量保存
     *
     * @param jyfwDOList
     */
    void saveJyfwDataList(@Param("list") List<GaiaJyfwData> jyfwDOList);

    /**
     * 特殊经营范围首营表 批量保存
     *
     * @param jyfwGspInfoDOList
     */
    void saveJyfwDataGspInfoList(@Param("list") List<GaiaJyfwDataGspinfoKey> jyfwGspInfoDOList);

    /**
     * 指定首营活动id 下的特殊经营范围
     *
     * @param client 加盟商
     * @param flowNo 流程编号
     */
    List<GaiaJyfwDataGspinfoKey> getJyfwGspinfoDataByFlowNo(@Param("client") String client, @Param("flowNo") String flowNo);

    /**
     * 经营范围列表 全部
     *
     * @return
     */
    List<JyfwooData> getJyfwooDataList(@Param("jyfwClass") String jyfwClass);


    /**
     * 指定机构的经营范围
     */
    List<String> getJyfwNameByjyfwIdList(@Param("list") String jyfwIdList);

    /**
     * 指定id集合经营范围列表
     */
    List<JyfwooData> getJyfwIdAndNameByIdList(@Param("list") String jyfwIdList);
}
