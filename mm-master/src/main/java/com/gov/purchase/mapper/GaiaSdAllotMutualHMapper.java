package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdAllotMutualH;
import com.gov.purchase.entity.GaiaSdAllotMutualHKey;

public interface GaiaSdAllotMutualHMapper {
    int deleteByPrimaryKey(GaiaSdAllotMutualHKey key);

    int insert(GaiaSdAllotMutualH record);

    int insertSelective(GaiaSdAllotMutualH record);

    GaiaSdAllotMutualH selectByPrimaryKey(GaiaSdAllotMutualHKey key);

    int updateByPrimaryKeySelective(GaiaSdAllotMutualH record);

    int updateByPrimaryKey(GaiaSdAllotMutualH record);
}