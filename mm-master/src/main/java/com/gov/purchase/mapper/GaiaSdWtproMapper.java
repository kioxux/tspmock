package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdWtpro;
import com.gov.purchase.module.goods.dto.SaveGaiaProductBusiness;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 委托配送第三方商品编码对照表(GaiaSdWtpro)表数据库访问层
 *
 * @author makejava
 * @since 2021-08-13 15:57:13
 */
public interface GaiaSdWtproMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaSdWtpro queryById(GaiaSdWtpro param);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaSdWtpro> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaSdWtpro 实例对象
     * @return 对象列表
     */
    List<GaiaSdWtpro> queryAll(GaiaSdWtpro gaiaSdWtpro);

    /**
     * 新增数据
     *
     * @param gaiaSdWtpro 实例对象
     * @return 影响行数
     */
    int insert(GaiaSdWtpro gaiaSdWtpro);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaSdWtpro> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaSdWtpro> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaSdWtpro> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaSdWtpro> entities);

    /**
     * 修改数据
     *
     * @param gaiaSdWtpro 实例对象
     * @return 影响行数
     */
    int update(GaiaSdWtpro gaiaSdWtpro);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    Integer checkHas(SaveGaiaProductBusiness proInfo);

    /**
     * 查询已存在商品
     *
     * @param pros
     * @return
     */
    List<String> querySdWtPro(@Param("client") String client, @Param("pros") List<String> pros);
}

