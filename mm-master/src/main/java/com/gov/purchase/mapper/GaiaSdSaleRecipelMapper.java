package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdSaleRecipel;
import com.gov.purchase.entity.GaiaSdSaleRecipelKey;

public interface GaiaSdSaleRecipelMapper {
    int deleteByPrimaryKey(GaiaSdSaleRecipelKey key);

    int insert(GaiaSdSaleRecipel record);

    int insertSelective(GaiaSdSaleRecipel record);

    GaiaSdSaleRecipel selectByPrimaryKey(GaiaSdSaleRecipelKey key);

    int updateByPrimaryKeySelective(GaiaSdSaleRecipel record);

    int updateByPrimaryKey(GaiaSdSaleRecipel record);
}