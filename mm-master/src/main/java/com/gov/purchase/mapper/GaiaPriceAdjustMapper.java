package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPriceAdjust;
import com.gov.purchase.entity.GaiaPriceAdjustKey;
import com.gov.purchase.module.store.dto.price.AdjustPriceRequestDto;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaPriceAdjustMapper {
    int deleteByPrimaryKey(GaiaPriceAdjustKey key);

    int insert(GaiaPriceAdjust record);

    int insertSelective(GaiaPriceAdjust record);

    GaiaPriceAdjust selectByPrimaryKey(GaiaPriceAdjustKey key);

    int updateByPrimaryKeySelective(GaiaPriceAdjust record);

    int updateByPrimaryKey(GaiaPriceAdjust record);

    int insertBatch(@Param("list") List<GaiaPriceAdjust> record);

    int updateBatch(@Param("list") List<GaiaPriceAdjust> record);

    GaiaPriceAdjust getCardNumber(Map<Object, Object> map);

    List<GaiaPriceAdjustDto> selectByFlowNo(Map<Object, Object> map);

    List<GaiaPriceAdjustDto> selectByMap(Map<Object, Object> map);


    List<GaiaPriceAdjustDto> selectByMapForList(Map<Object, Object> map);

    List<GaiaPriceAdjust> selectList(@Param("CLIENT") String client, @Param("list") List<AdjustPriceRequestDto> list);


    List<GaiaPriceAdjustDto> selectByMapForGoodList(Map<Object, Object> map);

    List<GaiaPriceAdjustDto> selectByMapForGoodList2(Map<Object, Object> map);

    List<GaiaPriceAdjustDto> selectPriceAdjustList(GaiaPriceAdjustKey gaiaPriceAdjustKey);
}