package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaRetailPrice;
import com.gov.purchase.entity.GaiaRetailPriceKey;
import com.gov.purchase.module.store.dto.GetStoreProductPriceListDTO;
import com.gov.purchase.module.store.dto.RetailStore;
import com.gov.purchase.module.store.dto.price.*;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListReponseDto;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListRequest;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListRequestDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface GaiaRetailPriceMapper {
    int deleteByPrimaryKey(GaiaRetailPriceKey key);

    int insert(GaiaRetailPrice record);

    int insertSelective(GaiaRetailPrice record);

    GaiaRetailPrice selectByPrimaryKey(GaiaRetailPriceKey key);

    int updateByPrimaryKeySelective(GaiaRetailPrice record);

    int updateByPrimaryKey(GaiaRetailPrice record);

    ProductBasicListReponseDto queryProductBasicList(ProductBasicListRequestDto dto);

    // 查询最大流水号
    String selectMaxModifNo(@Param("client") String client, @Param("date") String date);

    // 调价单审批列表
    List<PriceApprovalListReponseDto> queryPriceApprovalList(PriceApprovalListRequestDto dto);

    // 销售标签列表
    List<SalesTagListReponseDto> queryTagList(SalesTagListRequestDto dto);

    // 销售标签列表带生效日期
    List<SalesTagListReponseDto> queryTagListWithDate(SalesTagListRequestDto dto);

    //调价查看列表 按价格查询
    List<PriceListReponseDto> queryPriceListOnPrice(PriceListRequestDto dto);

    //调价查看列表 按商品查询
    List<PriceListReponseDto> queryPriceListOnProduct(PriceListRequestDto dto);

    // 调价提交数据重复验证
    int validRetailPriceExist(Map<String, Object> map);

    List<GaiaRetailPrice> selectByModifyNo(@Param("gaiaRetailPrices") List<GaiaRetailPrice> gaiaRetailPrices);

    int updateByPrcModfiyNo(@Param("gaiaRetailPriceList") List<GaiaRetailPrice> gaiaRetailPriceList);

    List<SalesTagListReponseDto> querySalesTagList2(SalesTagListRequest salesTagListRequest);

    /**
     * 调价商品选择
     *
     * @param client
     * @param depId
     * @param params
     * @return
     */
    List<StoreProductDto> getProductListBySto(@Param("client") String client, @Param("stoCode") String depId, @Param("params") String params, @Param("inStock") Integer inStock, @Param("prcClass") String prcClass);

    /**
     * 门店调价申请
     *
     * @param gaiaRetailPriceList
     */
    void insertList(@Param("gaiaRetailPriceList") List<GaiaRetailPrice> gaiaRetailPriceList);

    /**
     * 门店调价查询
     *
     * @param client
     * @param depId
     * @param prcSource
     * @param prcEffectDate
     * @param proKey
     * @param prcApprovalSuatus
     * @return
     */
    List<StoreProductDto> selectStoreRetailPriceList(@Param("client") String client, @Param("depId") String depId, @Param("prcSource") String prcSource,
                                                     @Param("prcEffectDate") String prcEffectDate, @Param("proKey") String proKey,
                                                     @Param("prcApprovalSuatus") String prcApprovalSuatus,
                                                     @Param("prcModfiyNo") String prcModfiyNo);

    String getPrcModfiyNo(@Param("client") String client, @Param("depId") String depId, @Param("praAdjustNo") String praAdjustNo);

    /**
     * 查询调前价格
     *
     * @param dto
     * @return
     */
    BigDecimal queryPrePrice(ProductBasicListRequestDto dto);

    /**
     * 调价门店
     *
     * @param client
     * @param prcModfiyNo
     * @param prcProduct
     * @return
     */
    List<RetailStore> queryStoreList(@Param("client") String client, @Param("prcModfiyNo") String prcModfiyNo, @Param("prcProduct") String prcProduct);

    /**
     * 调价单明细
     */
    List<GetStoreProductPriceListDTO> getStoreProductPriceList(@Param("client") String client, @Param("proSelfCode") String proSelfCode, @Param("prcClass") String prcClass, @Param("prcModfiyNo") String prcModfiyNo);

    List<String> selectRetailSiteList(@Param("client") String client, @Param("retailStoreList") List<RetailStore> retailStoreList);

    List<ProductBasicListReponseDto> queryProductBasicListImport(@Param("client") String client, @Param("prcClass") String prcClass,
                                                                 @Param("list") List<ProductBasicListRequestDto> productBasicListRequestDtoList);
}