package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaUnit;

public interface GaiaUnitMapper {
    int deleteByPrimaryKey(String unitCode);

    int insert(GaiaUnit record);

    int insertSelective(GaiaUnit record);

    GaiaUnit selectByPrimaryKey(String unitCode);

    int updateByPrimaryKeySelective(GaiaUnit record);

    int updateByPrimaryKey(GaiaUnit record);
}