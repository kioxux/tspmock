package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdMedCheckD;
import com.gov.purchase.entity.GaiaSdMedCheckDKey;

public interface GaiaSdMedCheckDMapper {
    int deleteByPrimaryKey(GaiaSdMedCheckDKey key);

    int insert(GaiaSdMedCheckD record);

    int insertSelective(GaiaSdMedCheckD record);

    GaiaSdMedCheckD selectByPrimaryKey(GaiaSdMedCheckDKey key);

    int updateByPrimaryKeySelective(GaiaSdMedCheckD record);

    int updateByPrimaryKey(GaiaSdMedCheckD record);
}