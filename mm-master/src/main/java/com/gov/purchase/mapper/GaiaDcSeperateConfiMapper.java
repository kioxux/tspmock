package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaDcSeperateConfi;
import com.gov.purchase.entity.GaiaDcSeperateConfiKey;

public interface GaiaDcSeperateConfiMapper {
    int deleteByPrimaryKey(GaiaDcSeperateConfiKey key);

    int insert(GaiaDcSeperateConfi record);

    int insertSelective(GaiaDcSeperateConfi record);

    GaiaDcSeperateConfi selectByPrimaryKey(GaiaDcSeperateConfiKey key);

    int updateByPrimaryKeySelective(GaiaDcSeperateConfi record);

    int updateByPrimaryKey(GaiaDcSeperateConfi record);
}