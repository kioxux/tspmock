package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaArea;
import com.gov.purchase.module.base.dto.AreaDto;

import java.util.List;

public interface GaiaAreaMapper {
    int deleteByPrimaryKey(String areaId);

    int insert(GaiaArea record);

    int insertSelective(GaiaArea record);

    GaiaArea selectByPrimaryKey(String areaId);

    int updateByPrimaryKeySelective(GaiaArea record);

    int updateByPrimaryKey(GaiaArea record);

    List<AreaDto> getAreaList();

    List<GaiaArea> selectByName(String areaName);
}