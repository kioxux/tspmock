package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdTempHumi;
import com.gov.purchase.entity.GaiaSdTempHumiKey;

public interface GaiaSdTempHumiMapper {
    int deleteByPrimaryKey(GaiaSdTempHumiKey key);

    int insert(GaiaSdTempHumi record);

    int insertSelective(GaiaSdTempHumi record);

    GaiaSdTempHumi selectByPrimaryKey(GaiaSdTempHumiKey key);

    int updateByPrimaryKeySelective(GaiaSdTempHumi record);

    int updateByPrimaryKey(GaiaSdTempHumi record);
}