package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdExamineD;
import com.gov.purchase.entity.GaiaSdExamineDKey;

public interface GaiaSdExamineDMapper {
    int deleteByPrimaryKey(GaiaSdExamineDKey key);

    int insert(GaiaSdExamineD record);

    int insertSelective(GaiaSdExamineD record);

    GaiaSdExamineD selectByPrimaryKey(GaiaSdExamineDKey key);

    int updateByPrimaryKeySelective(GaiaSdExamineD record);

    int updateByPrimaryKey(GaiaSdExamineD record);
}