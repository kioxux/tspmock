package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaCompadm;
import com.gov.purchase.entity.GaiaCompadmKey;
import com.gov.purchase.module.purchase.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaCompadmMapper {
    int deleteByPrimaryKey(GaiaCompadmKey key);

    int insert(GaiaCompadm record);

    int insertSelective(GaiaCompadm record);

    GaiaCompadm selectByPrimaryKey(GaiaCompadmKey key);

    int updateByPrimaryKeySelective(GaiaCompadm record);

    int updateByPrimaryKey(GaiaCompadm record);

    List<GaiaCompadm> selectCompadmListByClien(@Param("client") String client);

    GaiaCompadm selectByPrimaryId(@Param("client") String client, @Param("compadmId") String compadmId);

    List<GaiaCompadm> selectCompadmListByClienDC(@Param("client") String client, @Param("dcCode") String dcCode);

    List<InvoicePayingDTO> selectCompadmInvoiceListByClien(ListInvoiceRequestDTO dto);

    List<ListTaxpayerResponseDto> selectCompadmTaxpayerListByClient(ListTaxpayerRequestDTO dto);

    List<GaiaCompadm> SelectDcAuth(Map<Object, Object> map);

    List<GaiaCompadm> getCompadmAndCompadmWms(String client, String dcCode);

    /**
     * 查询该加盟商下的连锁总部列表
     * @param Client
     * @return
     */
    List<CompadmVO> selectCompadms(@Param("client") String Client);
}