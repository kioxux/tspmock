package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductChange;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductChangeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GaiaProductChange record);

    int insertSelective(GaiaProductChange record);

    GaiaProductChange selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaProductChange record);

    int updateByPrimaryKey(GaiaProductChange record);

    /**
     * 批量保存
     */
    void saveBatch(@Param("changeList") List<GaiaProductChange> changeList);

    /**
     * 记录列表
     */
    List<GaiaProductChange> checkFlowNoExit(@Param("wfOrder") String wfOrder);

    /**
     * 审批通过更新记录状态
     */
    void updateStatusSuccess(@Param("changeIdList") List<Integer> changeIdList);

    /**
     * 审批驳回更新记录状态
     */
    void updateStatusFaile(@Param("changeIdList") List<Integer> changeIdList);

}