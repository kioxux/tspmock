package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBussinessScope;
import com.gov.purchase.entity.GaiaBussinessScopeKey;

public interface GaiaBussinessScopeMapper {
    int deleteByPrimaryKey(GaiaBussinessScopeKey key);

    int insert(GaiaBussinessScope record);

    int insertSelective(GaiaBussinessScope record);

    GaiaBussinessScope selectByPrimaryKey(GaiaBussinessScopeKey key);

    int updateByPrimaryKeySelective(GaiaBussinessScope record);

    int updateByPrimaryKey(GaiaBussinessScope record);
}