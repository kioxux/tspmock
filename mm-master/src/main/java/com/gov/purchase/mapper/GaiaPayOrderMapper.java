package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPayOrder;
import com.gov.purchase.entity.GaiaPayOrderKey;
import com.gov.purchase.module.purchase.dto.PayOrderApprovalDto;
import com.gov.purchase.module.purchase.dto.PayOrderListRequestDto;
import com.gov.purchase.module.purchase.dto.PayOrderListResponseDto;
import com.gov.purchase.module.purchase.dto.PaymentReqSaveRequestDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaPayOrderMapper {
    int deleteByPrimaryKey(GaiaPayOrderKey key);

    int insert(GaiaPayOrder record);

    int insertSelective(GaiaPayOrder record);

    GaiaPayOrder selectByPrimaryKey(GaiaPayOrderKey key);

    int updateByPrimaryKeySelective(GaiaPayOrder record);

    int updateByPrimaryKey(GaiaPayOrder record);

    /**
     * 付款申请单号,付款申请单行号
     */
    GaiaPayOrder selectPayOrderNo(PaymentReqSaveRequestDto dto);

    /**
     * 付款单查询
     */
    List<PayOrderListResponseDto> selectPayOrderList(PayOrderListRequestDto dto);

    /**
     * 付款单数据
     *
     * @param payOrderId
     * @return
     */
    PayOrderApprovalDto selectListByOrderId(@Param("payOrderId") String payOrderId);

    /**
     * 根据流程编号查询付款单
     * @param payOrderId
     * @return
     */
    GaiaPayOrder selectByOrderId(@Param("client") String client,@Param("payOrderId") String payOrderId);
}