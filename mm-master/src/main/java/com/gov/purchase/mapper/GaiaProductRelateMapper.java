package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductRelate;
import com.gov.purchase.entity.GaiaProductRelateKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductRelateMapper {
    int deleteByPrimaryKey(GaiaProductRelateKey key);

    int insert(GaiaProductRelate record);

    int insertSelective(GaiaProductRelate record);

    GaiaProductRelate selectByPrimaryKey(GaiaProductRelateKey key);

    int updateByPrimaryKeySelective(GaiaProductRelate record);

    int updateByPrimaryKey(GaiaProductRelate record);

    List<GaiaProductRelate> selectByRelate(@Param("client") String client, @Param("proSite") String proSite,
                                           @Param("proRelateCode") String proRelateCode, @Param("supCode") String supCode);
}