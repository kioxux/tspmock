package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPayingHeader;
import com.gov.purchase.entity.GaiaPayingHeaderKey;
import com.gov.purchase.module.goods.dto.QueryProductBasicResponseDto;
import com.gov.purchase.module.purchase.dto.ListInvoicePayingRequestDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaPayingHeaderMapper {
    int deleteByPrimaryKey(GaiaPayingHeaderKey key);

    int insert(GaiaPayingHeader record);

    int insertSelective(GaiaPayingHeader record);

    GaiaPayingHeader selectByPrimaryKey(GaiaPayingHeaderKey key);

    int updateByPrimaryKeySelective(GaiaPayingHeader record);

    int updateByPrimaryKey(GaiaPayingHeader record);

    /**
     * 根据订单号查询
     */
    GaiaPayingHeader selectByFicoId(@Param("ficoId") String ficoId);

    /**
     * 根据订单号更新支付状态
     */
    int updateByFicoId(GaiaPayingHeader record);

    /**
     * 未支付的有效订单
     *
     * @param expireTime
     * @return
     */
    List<GaiaPayingHeader> selectPayingOrder(@Param("expireTime") Integer expireTime);

    /**
     * 已支付未开票
     *
     * @return
     */
    List<GaiaPayingHeader> selectUnInvoice();
}
