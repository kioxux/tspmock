package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaRetailPrice;
import com.gov.purchase.entity.GaiaSdProductPrice;
import com.gov.purchase.entity.GaiaSdProductPriceKey;
import com.gov.purchase.module.base.dto.GaiaSdProductPriceDto;
import com.gov.purchase.module.store.dto.SdProductPrice;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSdProductPriceMapper {
    int deleteByPrimaryKey(GaiaSdProductPriceKey key);

    int insert(GaiaSdProductPrice record);

    int insertSelective(GaiaSdProductPrice record);

    GaiaSdProductPrice selectByPrimaryKey(GaiaSdProductPriceKey key);

    int updateByPrimaryKeySelective(GaiaSdProductPrice record);

    int updateByPrimaryKey(GaiaSdProductPrice record);

    List<GaiaSdProductPriceDto> selectGaiaSdProductPricesApprove(@Param("priceList") List<GaiaRetailPrice> priceList);

    List<Integer> selectGaiaSdProductPricesApproveIndex(@Param("priceList") List<GaiaRetailPrice> priceList);

    void updateList(@Param("gaiaSdProductPriceDtos") List<GaiaSdProductPriceDto> gaiaSdProductPriceDtos);

    void insertList(@Param("priceDtos") List<GaiaSdProductPriceDto> priceDtos);

    void deleteList(@Param("priceDtos") List<GaiaSdProductPriceDto> priceDtos);

    List<SdProductPrice> getProPriceList(@Param("client") String client, @Param("proSelfCode") String proSelfCode);

    List<GaiaSdProductPrice> selectByPrimaryKeyList(@Param("client") String client, @Param("params") List<Map<String, String>> params);

    /**
     * 批量插入
     *
     * @param insertList
     */
    void insertSelectiveList(@Param("insertList") List<GaiaSdProductPrice> insertList);

    /**
     * 批量更新
     *
     * @param updateList
     */
    void updateByPrimaryKeySelectiveList(@Param("updateList") List<GaiaSdProductPrice> updateList);
}