package com.gov.purchase.mapper;


import com.gov.purchase.entity.DistributionCountInfo;
import com.gov.purchase.entity.GaiaNewDistributionPlan;
import com.gov.purchase.entity.GaiaNewDistributionPlanDetail;

import java.util.List;

/**
 * @desc:
 * @author: xiao
 * @createTime: 2021/5/31 11:15
 */
public interface GaiaNewDistributionPlanDetailMapper {

    int addList(List<GaiaNewDistributionPlanDetail> detailList);

    int update(GaiaNewDistributionPlanDetail distributionPlanDetail);

    List<GaiaNewDistributionPlanDetail> findList(GaiaNewDistributionPlanDetail detailCond);

    DistributionCountInfo getPlanCountInfo(GaiaNewDistributionPlan distributionPlan);

    GaiaNewDistributionPlanDetail getById(Long id);
}
