package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaFieldRequiredConfKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaFieldRequiredConfMapper {
    int deleteByPrimaryKey(GaiaFieldRequiredConfKey key);

    int insert(GaiaFieldRequiredConfKey record);

    int insertSelective(GaiaFieldRequiredConfKey record);

    /**
     * 首营必填字段列表
     *
     * @param client 加盟商
     * @return 必填字段列表
     */
    List<GaiaFieldRequiredConfKey> queryFieldRequiredConfList(@Param("client") String client);

    /**
     * 删除必填字段
     *
     * @param client 加盟商
     * @param type   类型（1-商品 2-供应商 3-客户）
     * @return
     */
    int deleteFieldConf(@Param("client") String client, @Param("type") String type);

    /**
     * 批量插入必填字段
     *
     * @param confKeyList 必填字段列表
     * @return
     */
    int insertBatch(@Param("list") List<GaiaFieldRequiredConfKey> confKeyList);
}