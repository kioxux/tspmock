package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdIndrawerD;
import com.gov.purchase.entity.GaiaSdIndrawerDKey;

public interface GaiaSdIndrawerDMapper {
    int deleteByPrimaryKey(GaiaSdIndrawerDKey key);

    int insert(GaiaSdIndrawerD record);

    int insertSelective(GaiaSdIndrawerD record);

    GaiaSdIndrawerD selectByPrimaryKey(GaiaSdIndrawerDKey key);

    int updateByPrimaryKeySelective(GaiaSdIndrawerD record);

    int updateByPrimaryKey(GaiaSdIndrawerD record);
}