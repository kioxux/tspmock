package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdRechargeCardChange;
import com.gov.purchase.entity.GaiaSdRechargeCardChangeKey;

public interface GaiaSdRechargeCardChangeMapper {
    int deleteByPrimaryKey(GaiaSdRechargeCardChangeKey key);

    int insert(GaiaSdRechargeCardChange record);

    int insertSelective(GaiaSdRechargeCardChange record);

    GaiaSdRechargeCardChange selectByPrimaryKey(GaiaSdRechargeCardChangeKey key);

    int updateByPrimaryKeySelective(GaiaSdRechargeCardChange record);

    int updateByPrimaryKey(GaiaSdRechargeCardChange record);
}