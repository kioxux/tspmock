package com.gov.purchase.mapper;


import com.gov.purchase.entity.WmsRkys;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/3 16:30
 **/
public interface GaiaWmsRkysMapper {

    WmsRkys getUnique(WmsRkys cond);

    List<WmsRkys> findList(WmsRkys cond);

    List<WmsRkys> findDistinctList(WmsRkys cond);
}
