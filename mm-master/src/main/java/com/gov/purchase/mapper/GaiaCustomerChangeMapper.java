package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaCustomerChange;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaCustomerChangeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GaiaCustomerChange record);

    int insertSelective(GaiaCustomerChange record);

    GaiaCustomerChange selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaCustomerChange record);

    int updateByPrimaryKey(GaiaCustomerChange record);

    /**
     * 修改记录列表
     *
     * @param client  加盟商
     * @param wfOrder 工作流编码
     * @return
     */
    List<GaiaCustomerChange> getChangeList(@Param("client") String client, @Param("wfOrder") String wfOrder);

    /**
     * 更新状态，审批驳回
     *
     * @param changeIdList 主键列表
     */
    void updateStatusFaile(@Param("changeIdList") List<Integer> changeIdList);

    /**
     * 更新状态，审批成功
     *
     * @param changeIdList 主键列表
     */
    void updateStatusSuccess(@Param("changeIdList") List<Integer> changeIdList);

    /**
     * 批量保存
     *
     * @param changeList 修改记录集合
     */
    void saveBatch(@Param("changeList") List<GaiaCustomerChange> changeList);

}