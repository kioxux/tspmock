package com.gov.purchase.mapper;


import com.gov.purchase.entity.GaiaNewDistributionPlan;
import com.gov.purchase.entity.GaiaProductBusiness;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 11:15
 */
public interface GaiaNewDistributionPlanMapper {

    GaiaNewDistributionPlan getById(Long id);

    int add(GaiaNewDistributionPlan newDistributionPlan);

    int update(GaiaNewDistributionPlan gaiaNewDistributionPlan);

    GaiaNewDistributionPlan getUnique(GaiaNewDistributionPlan cond);

    String getPlanCode(@Param("clientId") String clientId);

    GaiaNewDistributionPlan getPlan(GaiaProductBusiness business);

    int updatePlan(GaiaNewDistributionPlan plan);

    List<GaiaNewDistributionPlan> findList(GaiaNewDistributionPlan cond);

    List<GaiaNewDistributionPlan> getWaitAutoList(GaiaNewDistributionPlan cond);

    List<GaiaNewDistributionPlan> getCompleteAndExpiredList();

}
