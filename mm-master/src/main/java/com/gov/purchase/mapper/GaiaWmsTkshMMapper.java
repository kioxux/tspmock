package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaWmsTkshM;
import com.gov.purchase.entity.GaiaWmsTkshMKey;

public interface GaiaWmsTkshMMapper {
    int deleteByPrimaryKey(GaiaWmsTkshMKey key);

    int insert(GaiaWmsTkshM record);

    int insertSelective(GaiaWmsTkshM record);

    GaiaWmsTkshM selectByPrimaryKey(GaiaWmsTkshMKey key);

    int updateByPrimaryKeySelective(GaiaWmsTkshM record);

    int updateByPrimaryKey(GaiaWmsTkshM record);

    GaiaWmsTkshM selectWmsTkshM(GaiaWmsTkshM gaiaWmsTkshM);
}