package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBank;

public interface GaiaBankMapper {
    int deleteByPrimaryKey(String bankCode);

    int insert(GaiaBank record);

    int insertSelective(GaiaBank record);

    GaiaBank selectByPrimaryKey(String bankCode);

    int updateByPrimaryKeySelective(GaiaBank record);

    int updateByPrimaryKey(GaiaBank record);
}