package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaWholesaleSalesman;
import com.gov.purchase.module.supplier.dto.WholesaleSalesVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaWholesaleSalesmanMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaWholesaleSalesman record);

    int insertSelective(GaiaWholesaleSalesman record);

    GaiaWholesaleSalesman selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaWholesaleSalesman record);

    int updateByPrimaryKey(GaiaWholesaleSalesman record);

    //extend
    List<WholesaleSalesVO> getWholesaleSalesList(@Param("client") String client,
                                                 @Param("userId") String userId,
                                                 @Param("salesManId") String salesManId);

    List<GaiaWholesaleSalesman> getSalesmanList(@Param("client") String client, @Param("supSite") String supSite);
}