package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaSdReplenishH;
import com.gov.purchase.entity.GaiaSdReplenishHKey;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.base.dto.StoreNeedImportProDTO;
import com.gov.purchase.module.base.dto.businessImport.StoreNeedImportDTO;
import com.gov.purchase.module.base.dto.businessImport.TaxCodeValueDTO;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.replenishment.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdReplenishHMapper {
    int deleteByPrimaryKey(GaiaSdReplenishHKey key);

    int insert(GaiaSdReplenishH record);

    int insertSelective(GaiaSdReplenishH record);

    GaiaSdReplenishH selectByPrimaryKey(GaiaSdReplenishHKey key);

    int updateByPrimaryKeySelective(GaiaSdReplenishH record);

    int updateByPrimaryKey(GaiaSdReplenishH record);

    /**
     * 铺货查询
     *
     * @param searchBatchStoc
     * @return
     */
    List<SearchReplenish> searchReplenishList(SearchReplenishReq searchBatchStoc);

    List<StoreReplenishmentListResponseDto> queryReplenishList(@Param("client") String client, @Param("gsrhBrId") String gsrhBrId,
                                                               @Param("gsrhVoucherId") String gsrhVoucherId, @Param("gsrhDate") String gsrhDate,
                                                               @Param("userId") String userId, @Param("gsrhBrIds") List<String> gsrhBrIds);

    /**
     * 有权限的DC列表
     *
     * @param client
     * @param userId
     * @return
     */
    List<GaiaDcData> getAuthDcList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 门店列表
     *
     * @param client
     * @param dcCode
     * @return
     */
    List<GetStoreListByAuthDcDTO> getStoreListByAuthDc(@Param("client") String client, @Param("dcCode") String dcCode, @Param("gssgId") String gssgId, @Param("stoDistrict") String stoDistrict);

    /**
     * 商品选择弹出框
     *
     * @param dto
     */
    List<StoreReplenishDetails> getProList(@Param("dto") GetProListRequestDTO dto);


    /**
     * 门店可铺货商品
     *
     * @param gsrhAddr
     * @param gaiaStoreDataList
     * @param storeReplenishDetailsList
     * @return
     */
    List<StoreReplenishDetails> selectNeedProductList(@Param("client") String client, @Param("gsrhAddr") String gsrhAddr,
                                                      @Param("gaiaStoreDataList") List<String> gaiaStoreDataList,
                                                      @Param("storeReplenishDetailsList") List<StoreReplenishDetails> storeReplenishDetailsList);

    /**
     * 不可铺货商品
     *
     * @param client
     * @param gsrhAddr
     * @param gaiaStoreDataList
     * @param storeReplenishDetailsList
     * @return
     */
    List<StoreReplenishDetails> selectUnNeedProductList(@Param("client") String client, @Param("gsrhAddr") String gsrhAddr,
                                                        @Param("gaiaStoreDataList") List<String> gaiaStoreDataList,
                                                        @Param("storeReplenishDetailsList") List<StoreReplenishDetails> storeReplenishDetailsList);

    List<BatchNoInfo> selectBatchNoInfoByWarehuPro(@Param("client") String client, @Param("gsrhAddr") String gsrhAddr, @Param("proSelfCode") String proSelfCode, @Param("batBatchNo") String batBatchNo);

    /**
     * 获取最大铺货单号
     *
     * @param client
     * @return
     */
    String getMaxVoucherId(@Param("client") String client);

    /**
     * 批量保存
     *
     * @param replenishHList
     */
    void saveBatch(@Param("replenishHList") List<GaiaSdReplenishH> replenishHList);

    /**
     * 匹配配置表
     *
     * @param client
     * @param dcCode
     * @param stoCode
     * @param proSelfCode
     * @return
     */
    int check(@Param("client") String client, @Param("dcCode") String dcCode, @Param("stoCode") String stoCode, @Param("proSelfCode") String proSelfCode);


    /**
     * 检查该DC下是否有这个门店
     *
     * @param client
     * @param dcCode
     * @param stoCodeList
     * @return
     */
    List<GaiaStoreData> checkDCcontainsStore(@Param("client") String client, @Param("dcCode") String dcCode, @Param("stoCodeList") List<String> stoCodeList);

    /**
     * 对应商品的税率
     *
     * @param client
     * @param list
     * @return
     */
    List<TaxCodeValueDTO> getTaxCodeValue(@Param("client") String client, @Param("list") List<StoreNeedImportDTO> list);

    /**
     * 门店和商品的匹配
     *
     * @param client
     * @param stoCode
     * @param proSelfCode
     * @return
     */
    int checkStorecontainsPro(@Param("client") String client, @Param("stoCode") String stoCode, @Param("proSelfCode") String proSelfCode);

    /**
     * 根据导入商品数据查询商品详细数据
     *
     * @param client
     * @param list
     */
    List<StoreNeedImportProDTO> getProCostPriceByImportProList(@Param("client") String client, @Param("list") List<StoreNeedImportDTO> list);

    /**
     * 自动开单 开关
     */
    String wmsConfig(@Param("client") String client, @Param("gsrhAddr") String gsrhAddr, @Param("configName") String configName);

    List<GetStoreListByAuthDcDTO> getElectronicCouponStoreList(String client, String gssgId, String stoDistrict);

    /**
     * 请货订单数据查询
     *
     * @param compadmForStoreDto
     * @return
     */
    List<SdReplenishHExtandVO> queryStoreRequestsRecord(CompadmForStoreDto compadmForStoreDto);

    /**
     * 请货订单明细列表查询
     *
     * @param compadmForStoreDto
     * @return
     */
    List<SdReplenishDExtandDTO> queryStoreRequestsDetailRecord(CompadmForStoreDto compadmForStoreDto);

    /**
     * 清货订单关闭功能
     *
     * @param list
     * @return
     */
    int updateFlag(List<CompadmForStoreDto> list);

    /**
     * 查询委托配送中心
     *
     * @param client
     * @param stoCode
     * @return
     */
    String selectSupplier(@Param("client") String client, @Param("stoCode") String stoCode);

    String getParamByClientAndGcspId(@Param("client") String client, @Param("gcspId") String gcspId);

    List<String> getPoReqIdList(@Param("client") String client, @Param("list") List<String> voucherIds);
}