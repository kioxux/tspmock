package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaWholesaleChannelDetail;
import com.gov.purchase.entity.GaiaWholesaleChannelDetailKey;

public interface GaiaWholesaleChannelDetailMapper {
    int deleteByPrimaryKey(GaiaWholesaleChannelDetailKey key);

    int insert(GaiaWholesaleChannelDetail record);

    int insertSelective(GaiaWholesaleChannelDetail record);

    GaiaWholesaleChannelDetail selectByPrimaryKey(GaiaWholesaleChannelDetailKey key);

    int updateByPrimaryKeySelective(GaiaWholesaleChannelDetail record);

    int updateByPrimaryKey(GaiaWholesaleChannelDetail record);
}