package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdSystemPara;
import com.gov.purchase.entity.GaiaSdSystemParaKey;

public interface GaiaSdSystemParaMapper {
    int deleteByPrimaryKey(GaiaSdSystemParaKey key);

    int insert(GaiaSdSystemPara record);

    int insertSelective(GaiaSdSystemPara record);

    GaiaSdSystemPara selectByPrimaryKey(GaiaSdSystemParaKey key);

    int updateByPrimaryKeySelective(GaiaSdSystemPara record);

    int updateByPrimaryKey(GaiaSdSystemPara record);
}