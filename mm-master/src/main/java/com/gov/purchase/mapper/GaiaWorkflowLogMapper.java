package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaWorkflowLog;
import com.gov.purchase.entity.GaiaWorkflowLogKey;
import com.gov.purchase.module.goods.dto.QueryGspinfoWorkflowLogRequestDto;
import com.gov.purchase.module.goods.dto.QueryProductBasicRequestDto;
import com.gov.purchase.module.goods.dto.QueryProductBasicResponseDto;

import java.util.List;

public interface GaiaWorkflowLogMapper {
    int deleteByPrimaryKey(GaiaWorkflowLogKey key);

    int insert(GaiaWorkflowLog record);

    int insertSelective(GaiaWorkflowLog record);

    GaiaWorkflowLog selectByPrimaryKey(GaiaWorkflowLogKey key);

    int updateByPrimaryKeySelective(GaiaWorkflowLog record);

    int updateByPrimaryKey(GaiaWorkflowLog record);

    List<GaiaWorkflowLog> selectQueryGspinfoWorkflowLogInfo(QueryGspinfoWorkflowLogRequestDto dto);
}