package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdPayDayreportH;
import com.gov.purchase.entity.GaiaSdPayDayreportHKey;

public interface GaiaSdPayDayreportHMapper {
    int deleteByPrimaryKey(GaiaSdPayDayreportHKey key);

    int insert(GaiaSdPayDayreportH record);

    int insertSelective(GaiaSdPayDayreportH record);

    GaiaSdPayDayreportH selectByPrimaryKey(GaiaSdPayDayreportHKey key);

    int updateByPrimaryKeySelective(GaiaSdPayDayreportH record);

    int updateByPrimaryKey(GaiaSdPayDayreportH record);
}