package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductMinDisplay;
import com.gov.purchase.entity.GaiaProductMinDisplayKey;
import com.gov.purchase.module.store.dto.store.StoreMinQtyExportSearchDto;
import com.gov.purchase.module.store.dto.store.StoreMinQtyResultDto;
import com.gov.purchase.module.store.dto.store.StoreMinQtySearchDto;

import java.util.List;

public interface GaiaProductMinDisplayMapper {
    int deleteByPrimaryKey(GaiaProductMinDisplayKey key);

    int insert(GaiaProductMinDisplay record);

    int insertSelective(GaiaProductMinDisplay record);

    GaiaProductMinDisplay selectByPrimaryKey(GaiaProductMinDisplayKey key);

    int updateByPrimaryKeySelective(GaiaProductMinDisplay record);

    int updateByPrimaryKey(GaiaProductMinDisplay record);

    List<StoreMinQtyResultDto> queryPageList(StoreMinQtySearchDto dto);

    List<StoreMinQtyResultDto> queryExportList(StoreMinQtyExportSearchDto dto);
}