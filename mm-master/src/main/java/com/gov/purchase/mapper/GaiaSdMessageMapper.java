package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdMessage;
import com.gov.purchase.entity.GaiaSdMessageKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdMessageMapper {
    int deleteByPrimaryKey(GaiaSdMessageKey key);

    int insert(GaiaSdMessage record);

    int insertSelective(GaiaSdMessage record);

    GaiaSdMessage selectByPrimaryKey(GaiaSdMessageKey key);

    int updateByPrimaryKeySelective(GaiaSdMessage record);

    int updateByPrimaryKey(GaiaSdMessage record);

    String getCurrentVoucherId( GaiaSdMessage sdMessage);

    String selectNextVoucherId(@Param("clientId") String client, @Param("type") String type);

    void insertBatch(List<GaiaSdMessage> list);
}