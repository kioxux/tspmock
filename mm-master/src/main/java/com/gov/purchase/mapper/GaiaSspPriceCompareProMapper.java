package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSspPriceCompare;
import com.gov.purchase.entity.GaiaSspPriceComparePro;
import com.gov.purchase.module.priceCompare.dto.PriceCompareProDto;
import com.gov.purchase.module.priceCompare.vo.PriceCompareProInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSspPriceCompareProMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(GaiaSspPriceComparePro record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(GaiaSspPriceComparePro record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    GaiaSspPriceComparePro selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(GaiaSspPriceComparePro record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(GaiaSspPriceComparePro record);

    int insertList(@Param("priceComparePros") List<GaiaSspPriceComparePro> priceComparePros);

    void updateListByParams(@Param("params") List<PriceCompareProInfoVo> params, @Param("isOrder") int isOrder, @Param("userId") String userId);

    void insertSelectList(@Param("priceCompareProList") List<PriceCompareProDto> priceCompareProList, @Param("priceCompare") GaiaSspPriceCompare priceCompare, @Param("userId") String userId);

    List<Integer> selectProFinish(@Param("priceCompareId") Long priceCompareId);
}