package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdAllotMutualD;
import com.gov.purchase.entity.GaiaSdAllotMutualDKey;

public interface GaiaSdAllotMutualDMapper {
    int deleteByPrimaryKey(GaiaSdAllotMutualDKey key);

    int insert(GaiaSdAllotMutualD record);

    int insertSelective(GaiaSdAllotMutualD record);

    GaiaSdAllotMutualD selectByPrimaryKey(GaiaSdAllotMutualDKey key);

    int updateByPrimaryKeySelective(GaiaSdAllotMutualD record);

    int updateByPrimaryKey(GaiaSdAllotMutualD record);
}