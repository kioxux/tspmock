package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaDeliveryCompany;
import com.gov.purchase.module.base.dto.MainStore;

import java.util.List;

public interface GaiaDeliveryCompanyMapper {
    int deleteByPrimaryKey(String decCode);

    int insert(GaiaDeliveryCompany record);

    int insertSelective(GaiaDeliveryCompany record);

    GaiaDeliveryCompany selectByPrimaryKey(String decCode);

    int updateByPrimaryKeySelective(GaiaDeliveryCompany record);

    int updateByPrimaryKey(GaiaDeliveryCompany record);

    List<MainStore> mainStore();
}