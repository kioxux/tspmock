package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdStock;
import com.gov.purchase.entity.GaiaSdStockKey;
import org.apache.ibatis.annotations.Param;

public interface GaiaSdStockMapper {
    int deleteByPrimaryKey(GaiaSdStockKey key);

    int insert(GaiaSdStock record);

    int insertSelective(GaiaSdStock record);

    GaiaSdStock selectByPrimaryKey(GaiaSdStockKey key);

    int updateByPrimaryKeySelective(GaiaSdStock record);

    int updateByPrimaryKey(GaiaSdStock record);

    void gaiaSdStockBackups(@Param("year") String year, @Param("month") String month);

    void gaiaSdStockBatchBackups(@Param("year") String year, @Param("month") String month);
}