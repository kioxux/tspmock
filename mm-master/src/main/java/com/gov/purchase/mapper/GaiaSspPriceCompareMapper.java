package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSspPriceCompare;import com.gov.purchase.module.priceCompare.dto.PriceComparePageDto;import com.gov.purchase.module.priceCompare.vo.PriceComparePageVo;import com.gov.purchase.module.priceCompare.vo.PriceCompareProInfoVo;import com.gov.purchase.module.priceCompare.vo.PriceCompareSupplierInfoVo;import org.apache.ibatis.annotations.Param;import java.util.List;
import java.util.Map;

public interface GaiaSspPriceCompareMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(GaiaSspPriceCompare record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(GaiaSspPriceCompare record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    GaiaSspPriceCompare selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(GaiaSspPriceCompare record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(GaiaSspPriceCompare record);

    Integer selectCountByToday();

    List<PriceComparePageVo> selectPageByParams(@Param("param") PriceComparePageDto param);

    List<PriceCompareProInfoVo> selectInfo(Long id);

    List<PriceCompareSupplierInfoVo> selectSuppliers(Long id);

    void updateInvalidStatusByParams(Map<String, Object> map);
}