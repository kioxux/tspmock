package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaWmsDiaoboM;
import com.gov.purchase.entity.GaiaWmsDiaoboMKey;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

public interface GaiaWmsDiaoboMMapper {
    int deleteByPrimaryKey(GaiaWmsDiaoboMKey key);

    int insert(GaiaWmsDiaoboM record);

    int insertSelective(GaiaWmsDiaoboM record);

    GaiaWmsDiaoboM selectByPrimaryKey(GaiaWmsDiaoboMKey key);

    int updateByPrimaryKeySelective(GaiaWmsDiaoboM record);

    int updateByPrimaryKey(GaiaWmsDiaoboM record);

    int updateCkj(GaiaWmsDiaoboM gaiaWmsDiaoboM);

    String selectPoId(@Param("client") String client, @Param("proCode") String proCode, @Param("wmPch") String wmPch);

    BigDecimal selectCbj(@Param("client") String client, @Param("wmPch") String wmPch, @Param("wmSpBm") String wmSpBm);
}