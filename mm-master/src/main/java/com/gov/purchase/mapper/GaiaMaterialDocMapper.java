package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaMaterialDoc;
import com.gov.purchase.entity.GaiaMaterialDocKey;
import com.gov.purchase.module.wholesale.dto.GaiaBillOfApVO;
import com.gov.purchase.module.wholesale.dto.GaiaBillOfArVO;
import com.gov.purchase.module.purchase.dto.FuCertificateRequestDto;
import com.gov.purchase.module.purchase.dto.FuCertificateResponseDto;
import com.gov.purchase.module.wholesale.dto.SupplierDealOutData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaMaterialDocMapper {
    int deleteByPrimaryKey(GaiaMaterialDocKey key);

    int insert(GaiaMaterialDoc record);

    int insertSelective(GaiaMaterialDoc record);

    GaiaMaterialDoc selectByPrimaryKey(GaiaMaterialDocKey key);

    int updateByPrimaryKeySelective(GaiaMaterialDoc record);

    int updateByPrimaryKey(GaiaMaterialDoc record);

    /**
     * 后续凭证查询
     */
    List<FuCertificateResponseDto> selectFuCertificate(FuCertificateRequestDto dto);

    /**
     * 生成采购凭证号
     *
     * @return
     */
    String selectNo(@Param("client") String client);

    GaiaMaterialDoc selectMaterialDocByPoId(@Param("client") String client, @Param("proCode") String proCode, @Param("poId") String poId);

    /**
     * 订单号回查 ND 用
     *
     * @param client
     * @param poId
     * @param poLineNo
     * @return
     */
    GaiaMaterialDoc selectGaiaMaterialDocByPoIdLineNo(@Param("client") String client, @Param("poId") String poId, @Param("poLineNo") String poLineNo);

    /**
     * 原单查询
     *
     * @param client
     * @param matPoId
     * @param matPoLineno
     * @return
     */
    GaiaMaterialDoc selectMaterialDocByPoIdForEd(@Param("client") String client, @Param("matPoId") String matPoId, @Param("matPoLineno") String matPoLineno);

    /**
     * 物料凭证
     *
     * @param client
     * @param matDnId
     * @return
     */
    List<GaiaMaterialDoc> getGaiaMaterialDocByMatDnId(@Param("client") String client, @Param("matDnId") String matDnId,
                                                      @Param("matDnLineno") String matDnLineno, @Param("matSiteCode") String matSiteCode);

    GaiaBillOfArVO getReceivablesInfoForXD(@Param("client") String client, @Param("matDnId") String matDnId);

    GaiaBillOfArVO getReceivablesInfoForED(@Param("client") String client, @Param("matDnId") String matDnId);

    String getCusSelfCodeForED(@Param("client") String client, @Param("matDnId") String matDnId);

    String getCusSelfCodeForXD(@Param("client") String client, @Param("matDnId") String matDnId);

    List<SupplierDealOutData> selectSupplierDealPageBySiteCode(@Param("matType")String matType,@Param("client")String client,@Param("dnId") String matDnId,@Param("date")String date);

    void insertBillOfAp(@Param("ofApVO")GaiaBillOfApVO ofApVO);
}