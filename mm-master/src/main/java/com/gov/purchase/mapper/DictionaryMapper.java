package com.gov.purchase.mapper;

import com.gov.purchase.entity.PaymentApplications;
import com.gov.purchase.entity.SalaryProgram;
import com.gov.purchase.module.base.dto.Dictionary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.18
 */
public interface DictionaryMapper {

    /**
     * 字典集合数据查询
     *
     * @param tableName  表名
     * @param field      查询字段
     * @param condition  条件
     * @param sortFields 排序
     * @return
     */
    List<Dictionary> getDictionaryList(@Param("tableName") String tableName, @Param("field") String field,
                                       @Param("condition") String condition, @Param("sortFields") String sortFields);

    /**
     * 薪酬案例查询
     *
     * @param gspFlowNo
     * @return
     */
    List<SalaryProgram> selectSalary(@Param("client") String client, @Param("gspFlowNo") String gspFlowNo);

    /**
     * 薪酬案例审批
     *
     * @param salaryProgram
     * @return
     */
    int salaryApproval(SalaryProgram salaryProgram);

    /**
     * 供应商付款
     *
     * @param clientId
     * @param flowNo
     * @return
     */
    List<PaymentApplications> selectPaymentApplications(@Param("client") String clientId, @Param("flowNo") String flowNo);

    /**
     * 付款审批
     *
     * @param clientId
     * @param flowNo
     * @param approvalStatus
     * @return
     */
    int paymentApplications(@Param("client") String clientId, @Param("flowNo") String flowNo, @Param("approvalStatus") Integer approvalStatus);

    int setSupplierPayAttr(@Param("gspFile") String gspFile, @Param("gspFileName") String gspFileName, @Param("client") String client, @Param("payOrderId") String payOrderId);

    void insertBillOfAp(@Param("application")PaymentApplications paymentApplications);

}

