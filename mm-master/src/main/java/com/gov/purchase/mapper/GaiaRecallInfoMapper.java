package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaRecallInfo;
import com.gov.purchase.entity.GaiaRecallInfoKey;
import com.gov.purchase.module.delivery.dto.GoodsRecallListRequestDto;
import com.gov.purchase.module.delivery.dto.GoodsRecallListResponseDto;
import com.gov.purchase.module.purchase.dto.GetProductListResponseDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaRecallInfoMapper {
    int deleteByPrimaryKey(GaiaRecallInfoKey key);

    int insert(GaiaRecallInfo record);

    int insertSelective(GaiaRecallInfo record);

    GaiaRecallInfo selectByPrimaryKey(GaiaRecallInfoKey key);

    int updateByPrimaryKeySelective(GaiaRecallInfo record);

    int updateByPrimaryKey(GaiaRecallInfo record);

    /**
     * 召回单号生成
     * @return
     */
    String selectNo();

    /**
     * 商品召回查看列表
     * @param dto GoodsRecallListRequestDto
     * @return
     */
    List<GoodsRecallListResponseDto> selectGoodsRecallList(GoodsRecallListRequestDto dto);

    List<GetProductListResponseDto> getProductListForRecall(@Param("client") String client, @Param("chain") String chain, @Param("proCode") String proCode);
}