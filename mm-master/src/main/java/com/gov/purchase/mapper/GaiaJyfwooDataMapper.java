package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaJyfwooData;

public interface GaiaJyfwooDataMapper {
    int deleteByPrimaryKey(String jyfwId);

    int insert(GaiaJyfwooData record);

    int insertSelective(GaiaJyfwooData record);

    GaiaJyfwooData selectByPrimaryKey(String jyfwId);

    int updateByPrimaryKeySelective(GaiaJyfwooData record);

    int updateByPrimaryKey(GaiaJyfwooData record);
}