package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaChajiaZ;
import com.gov.purchase.entity.GaiaChajiaZKey;
import com.gov.purchase.module.purchase.dto.PurchaseReceiptPriceDTO;
import com.gov.purchase.module.purchase.dto.PurchaseReceiptPriceVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaChajiaZMapper {
    int deleteByPrimaryKey(GaiaChajiaZKey key);

    int insert(GaiaChajiaZ record);

    int insertSelective(GaiaChajiaZ record);

    GaiaChajiaZ selectByPrimaryKey(GaiaChajiaZKey key);

    int updateByPrimaryKeySelective(GaiaChajiaZ record);

    int updateByPrimaryKey(GaiaChajiaZ record);

    /**
     * 采购入库差价单号列表(未审核)
     *
     * @param client
     * @return
     */
    List<String> selectCjIdList(@Param("client") String client);

    /**
     * 采购差价单信息
     *
     * @param dto
     * @return
     */
    List<PurchaseReceiptPriceVO> selectPurchaseReceiptPriceOrder(PurchaseReceiptPriceDTO dto);

    /**
     * 采购单号
     *
     * @param client
     * @param cjId
     * @return
     */
    String getCjId(@Param("client") String client, @Param("cjId") String cjId);
}