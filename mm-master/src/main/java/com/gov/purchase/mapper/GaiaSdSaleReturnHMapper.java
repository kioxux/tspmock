package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdSaleReturnH;
import com.gov.purchase.entity.GaiaSdSaleReturnHKey;

public interface GaiaSdSaleReturnHMapper {
    int deleteByPrimaryKey(GaiaSdSaleReturnHKey key);

    int insert(GaiaSdSaleReturnH record);

    int insertSelective(GaiaSdSaleReturnH record);

    GaiaSdSaleReturnH selectByPrimaryKey(GaiaSdSaleReturnHKey key);

    int updateByPrimaryKeySelective(GaiaSdSaleReturnH record);

    int updateByPrimaryKey(GaiaSdSaleReturnH record);
}