package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdMedCheckH;
import com.gov.purchase.entity.GaiaSdMedCheckHKey;

public interface GaiaSdMedCheckHMapper {
    int deleteByPrimaryKey(GaiaSdMedCheckHKey key);

    int insert(GaiaSdMedCheckH record);

    int insertSelective(GaiaSdMedCheckH record);

    GaiaSdMedCheckH selectByPrimaryKey(GaiaSdMedCheckHKey key);

    int updateByPrimaryKeySelective(GaiaSdMedCheckH record);

    int updateByPrimaryKey(GaiaSdMedCheckH record);
}