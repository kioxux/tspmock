package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdPhysicalCountDiff;
import com.gov.purchase.entity.GaiaSdPhysicalCountDiffKey;

public interface GaiaSdPhysicalCountDiffMapper {
    int deleteByPrimaryKey(GaiaSdPhysicalCountDiffKey key);

    int insert(GaiaSdPhysicalCountDiff record);

    int insertSelective(GaiaSdPhysicalCountDiff record);

    GaiaSdPhysicalCountDiff selectByPrimaryKey(GaiaSdPhysicalCountDiffKey key);

    int updateByPrimaryKeySelective(GaiaSdPhysicalCountDiff record);

    int updateByPrimaryKey(GaiaSdPhysicalCountDiff record);
}