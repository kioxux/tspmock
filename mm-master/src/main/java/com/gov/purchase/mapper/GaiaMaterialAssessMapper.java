package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaMaterialAssess;
import com.gov.purchase.entity.GaiaMaterialAssessKey;
import com.gov.purchase.module.delivery.dto.StockNumRequestDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface GaiaMaterialAssessMapper {
    int deleteByPrimaryKey(GaiaMaterialAssessKey key);

    int insert(GaiaMaterialAssess record);

    int insertSelective(GaiaMaterialAssess record);

    GaiaMaterialAssess selectByPrimaryKey(GaiaMaterialAssessKey key);

    int updateByPrimaryKeySelective(GaiaMaterialAssess record);

    int updateByPrimaryKey(GaiaMaterialAssess record);


    List<GaiaMaterialAssess> selectByPrimaryKeyList(@Param("params") List<Map<String, String>> params);
}