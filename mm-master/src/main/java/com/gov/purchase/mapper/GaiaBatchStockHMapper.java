package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBatchStockH;
import com.gov.purchase.entity.GaiaBatchStockHKey;

public interface GaiaBatchStockHMapper {
    int deleteByPrimaryKey(GaiaBatchStockHKey key);

    int insert(GaiaBatchStockH record);

    int insertSelective(GaiaBatchStockH record);

    GaiaBatchStockH selectByPrimaryKey(GaiaBatchStockHKey key);

    int updateByPrimaryKeySelective(GaiaBatchStockH record);

    int updateByPrimaryKey(GaiaBatchStockH record);
}