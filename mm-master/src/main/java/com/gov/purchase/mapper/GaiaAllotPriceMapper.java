package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaAllotPrice;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.module.delivery.dto.GaiaAllotPriceDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaAllotPriceMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaAllotPrice record);

    int insertSelective(GaiaAllotPrice record);

    GaiaAllotPrice selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaAllotPrice record);

    int updateByPrimaryKey(GaiaAllotPrice record);

    /**
     * 调拨价格查询
     *
     * @param alpReceiveSite
     * @param alpProCode
     * @return
     */
    List<GaiaAllotPriceDto> selectAllotPriceList(@Param("client") String client, @Param("alpReceiveSite") String alpReceiveSite, @Param("alpProCode") String alpProCode);

    List<GaiaAllotPrice> selectAllotPriceListForGroup(@Param("client") String client,
                                                      @Param("alpReceiveSiteList") List<String> alpReceiveSiteList,
                                                      @Param("gapgType") Integer gapgType);

    List<GaiaAllotPrice> selectAllotPriceListByPro(@Param("client") String client,
                                                   @Param("proList") List<String> proList,
                                                   @Param("gapgType") Integer gapgType);

    /**
     * 商品列表
     *
     * @param client
     * @param alpReceiveSite
     * @param alpProCode
     * @return
     */
    List<GaiaProductBusiness> getProductList(@Param("client") String client, @Param("alpReceiveSite") String alpReceiveSite,
                                             @Param("alpProCode") String alpProCode, @Param("proSelfCode") String proSelfCode);

    /**
     * 调拨价查询
     *
     * @param client
     * @param alpReceiveSite
     * @return
     */
    GaiaAllotPrice selectAllotPriceBySite(@Param("client") String client, @Param("alpReceiveSite") String alpReceiveSite);

    /**
     * 调拨价格查询
     *
     * @param client
     * @param alpReceiveSite
     * @param alpProCode
     * @return
     */
    GaiaAllotPrice selectAllotPriceByPro(@Param("client") String client, @Param("alpReceiveSite") String alpReceiveSite, @Param("alpProCode") String alpProCode);

    /**
     * 加盟商调拨价格
     *
     * @return
     */
    List<GaiaAllotPrice> selectAllotPriceByClient(@Param("client") String client, @Param("siteList") List<String> siteList, @Param("proList") List<String> proList);

    /**
     * 当前加盟商下的DC列表
     *
     * @param client 加盟商
     * @return
     */
    List<GaiaDcData> getDCListByCurrentClient(@Param("client") String client);

    int batchInsert(List<GaiaAllotPrice> list);

    int batchUpdate(List<GaiaAllotPrice> list);
}