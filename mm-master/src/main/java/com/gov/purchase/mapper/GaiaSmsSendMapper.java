package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSmsSend;

public interface GaiaSmsSendMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GaiaSmsSend record);

    int insertSelective(GaiaSmsSend record);

    GaiaSmsSend selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaSmsSend record);

    int updateByPrimaryKey(GaiaSmsSend record);
}