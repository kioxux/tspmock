package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductGspinfo;
import com.gov.purchase.entity.GaiaProductGspinfoKey;
import com.gov.purchase.module.goods.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaProductGspinfoMapper {
    int deleteByPrimaryKey(GaiaProductGspinfoKey key);

    int insert(GaiaProductGspinfo record);

    int insertSelective(GaiaProductGspinfo record);

    GaiaProductGspinfo selectByPrimaryKey(GaiaProductGspinfoKey key);

    int updateByPrimaryKeySelective(GaiaProductGspinfo record);

    int updateByPrimaryKey(GaiaProductGspinfo record);

    List<QueryGspResponseDto> selectQueryGspinfo(QueryGspRequestDto dto);

    QueryGspinfoResponseDto selectByPrimary(GspinfoRequestDto key);

    /**
     * 根据加盟商、自编码查询数据
     *
     * @param client 加盟商
     * @param proSelfCode 自编码
     * @return
     */
    List<GaiaProductGspinfo> selfOnlyCheck(@Param("client") String client, @Param("proSelfCode") String proSelfCode, @Param("proSite") String proSite);

    /**
     * 商品分类字段值为“中药饮片”时（即PRO_CLASS字段值为3开头）以外的数据只能有一个商品自编码
     *
     * @param client 加盟商
     * @param proSite 地点
     * @param proCode 商品编码
     * @return
     */
    List<GaiaProductGspinfo> nonClass3Check(@Param("client") String client, @Param("proSite") String proSite, @Param("proCode") String proCode);

    /**
     * 根据工作流编号抽首营信息
     * @param client
     * @param proFlowNo
     * @return
     */
    List<GaiaProductGspinfo> selectProductGspByFlowNo(@Param("client") String client, @Param("proFlowNo") String proFlowNo);

    /**
     * 该商品在首营表中是否存在
     * @return
     * @param dto
     */
    String getProductGspinfoExit(GaiaProductGspinfoKeyDTO dto);

    /**
     * 指定工作流创建人
     */
    String getCreateUser(@Param("client") String client, @Param("wfOrder") String wfOrder);

    /**
     * 根据查询条件导出表中所有内容
     * @param dto
     * @return
     */
    List<QueryGspResponseDtoCSV> selectExportSelectQueryGspinfo(QueryGspRequestDto dto);

    /**
     * 经营类别
     *
     * @param id
     * @return
     */
    List<JyfwooData> getJyfwooData(@Param("id") String id);
}