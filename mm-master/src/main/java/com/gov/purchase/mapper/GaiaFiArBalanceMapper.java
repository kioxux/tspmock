package com.gov.purchase.mapper;


import com.gov.purchase.module.wholesale.dto.GaiaBillOfArVO;
import com.gov.purchase.module.wholesale.dto.GaiaFiArBalance;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 应收期末余额(GaiaFiArBalance)表数据库访问层
 *
 * @author makejava
 * @since 2021-09-18 15:29:14
 */
public interface GaiaFiArBalanceMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GaiaFiArBalance queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaFiArBalance> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param gaiaFiArBalance 实例对象
     * @return 对象列表
     */
    List<GaiaFiArBalance> queryAll(GaiaFiArBalance gaiaFiArBalance);

    /**
     * 新增数据
     *
     * @param gaiaFiArBalance 实例对象
     * @return 影响行数
     */
    int insert(GaiaFiArBalance gaiaFiArBalance);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaFiArBalance> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<GaiaFiArBalance> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<GaiaFiArBalance> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<GaiaFiArBalance> entities);

    /**
     * 修改数据
     *
     * @param gaiaFiArBalance 实例对象
     * @return 影响行数
     */
    int update(GaiaFiArBalance gaiaFiArBalance);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    GaiaFiArBalance getByDcCodeAndCusSelfCode(GaiaBillOfArVO inData);

}

