package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSspPriceCompareSupplier;

public interface GaiaSspPriceCompareSupplierMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(GaiaSspPriceCompareSupplier record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(GaiaSspPriceCompareSupplier record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    GaiaSspPriceCompareSupplier selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(GaiaSspPriceCompareSupplier record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(GaiaSspPriceCompareSupplier record);
}