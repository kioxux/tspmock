package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdCleandrawerH;
import com.gov.purchase.entity.GaiaSdCleandrawerHKey;

public interface GaiaSdCleandrawerHMapper {
    int deleteByPrimaryKey(GaiaSdCleandrawerHKey key);

    int insert(GaiaSdCleandrawerH record);

    int insertSelective(GaiaSdCleandrawerH record);

    GaiaSdCleandrawerH selectByPrimaryKey(GaiaSdCleandrawerHKey key);

    int updateByPrimaryKeySelective(GaiaSdCleandrawerH record);

    int updateByPrimaryKey(GaiaSdCleandrawerH record);
}