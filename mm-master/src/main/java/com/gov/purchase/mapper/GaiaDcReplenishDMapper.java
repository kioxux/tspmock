package com.gov.purchase.mapper;

import com.gov.purchase.entity.DcReplenishDetailVO;
import com.gov.purchase.entity.GaiaDcReplenishD;
import com.gov.purchase.entity.GaiaDcReplenishH;
import com.gov.purchase.entity.GaiaProductBusiness;

import java.util.List;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/21 16:36
 */
public interface GaiaDcReplenishDMapper {

    List<DcReplenishDetailVO> getReplenishDetail(GaiaDcReplenishD cond);

    List<GaiaProductBusiness> getReplenishProductList(GaiaDcReplenishH cond);
}
