package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaMaterialAssessH;
import com.gov.purchase.entity.GaiaMaterialAssessHKey;

public interface GaiaMaterialAssessHMapper {
    int deleteByPrimaryKey(GaiaMaterialAssessHKey key);

    int insert(GaiaMaterialAssessH record);

    int insertSelective(GaiaMaterialAssessH record);

    GaiaMaterialAssessH selectByPrimaryKey(GaiaMaterialAssessHKey key);

    int updateByPrimaryKeySelective(GaiaMaterialAssessH record);

    int updateByPrimaryKey(GaiaMaterialAssessH record);
}