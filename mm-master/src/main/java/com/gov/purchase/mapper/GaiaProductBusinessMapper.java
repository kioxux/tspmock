package com.gov.purchase.mapper;

import com.gov.purchase.entity.*;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.ProductDto;
import com.gov.purchase.module.goods.dto.*;
import com.gov.purchase.module.qa.dto.*;
import com.gov.purchase.module.replenishment.dto.ProZdyListDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface GaiaProductBusinessMapper {
    int deleteByPrimaryKey(GaiaProductBusinessKey key);

    int insert(GaiaProductBusiness record);

    int insertSelective(GaiaProductBusiness record);

    GaiaProductBusiness selectByPrimaryKey(GaiaProductBusinessKey key);

    int updateByPrimaryKeySelective(GaiaProductBusiness record);

    int updateByPrimaryKey(GaiaProductBusiness record);

    List<QueryProResponseDto> selectQueryProInfo(QueryProRequestDto dto);

    List<QueryProductListResponseDto> queryProductList(QueryProductListRequestDto dto);

    int updateProduct(List<UpdateProductResquestDto> list);

    List<QueryStockListResponseDto> queryStockList(QueryStockListResquestDto dto);

    GaiaProductBusiness selectGoodsStatus(@Param("client") String client, @Param("poProCode") String poProCode,
                                          @Param("poSiteCode") String poSiteCode);

    /**
     * 根据加盟商、自编码查询数据
     *
     * @param client      加盟商
     * @param proSelfCode 自编码
     * @return
     */
    List<GaiaProductBusiness> selfOnlyCheck(@Param("client") String client, @Param("proSelfCode") String proSelfCode, @Param("proSite") String proSite);

    /**
     * 商品分类字段值为“中药饮片”时（即PRO_CLASS字段值为3开头）以外的数据只能有一个商品自编码
     *
     * @param client  加盟商
     * @param proSite 地点
     * @param proCode 商品编码
     * @return
     */
    List<GaiaProductBusiness> nonClass3Check(@Param("client") String client, @Param("proSite") String proSite, @Param("proCode") String proCode);

    /**
     * 商品业务数据
     *
     * @param client      加盟商
     * @param proSite     地点
     * @param proSelfCode 商品自编码
     * @return
     */
    GaiaProductBusiness getProductBusiness(@Param("client") String client, @Param("proSite") String proSite, @Param("proSelfCode") String proSelfCode);

    /**
     * 商品业务数据
     *
     * @param client 加盟商
     * @return
     */
    List<GaiaProductBusiness> getProductBusiness2(@Param("client") String client, @Param("payKeyList") List<GaiaProductBusinessKey> payKeyList);

    List<GaiaProductBusiness> getAllProductBusiness(@Param("client") String client, @Param("codes") List<String> codes, @Param("sites") List<String> sites);

    /**
     * 商品业务数据
     */
    int checkProductRepeat(@Param("client") String client, @Param("proSelfCode") String proSelfCode, @Param("proSite") String proSite, @Param("proTcmSpecs") String proTcmSpecs
            , @Param("proTcmRegisterNo") String proTcmRegisterNo, @Param("proTcmFactoryCode") String proTcmFactoryCode,
                           @Param("proTcmPlace") String proTcmPlace);

    /**
     * 当前门店非数字自编码集合
     *
     * @param client
     * @param proSite
     * @return
     */
    List<String> getSelfCodeList(@Param("client") String client, @Param("proSite") String proSite, @Param("proCode") String proCode);

    /**
     * 根据商品分类或者自分类生成新编码
     *
     * @param client
     * @param proSite
     * @return
     */
    List<String> getSelfCodeListForClass(@Param("client") String client, @Param("proSite") String proSite, @Param("proCode") String proCode, @Param("beginCode") String beginCode);

    /******************************************************五期调整*****************************************************/

    /**
     * 商品列表
     *
     * @return
     */

    List<GaiaProductBusinessDTO> getProList(@Param("dto") GetProListRequestDTO dto, @Param("stoList") List<String> stoList);

    /**
     * 商品自编码
     */
    List<String> getSelfCodeList2(@Param("client") String client);

    /**
     * 根据门店编码和商品编码、商品名称、通用名称、助记码，精确匹配国际条形码1或2 来查询数据
     */
    List<QueryProductBusinessResponseDto> getStoreProList(Map<Object, Object> map);

    /**
     * 根据门店编码 商品编码查询
     *
     * @param map
     * @return
     */
    List<QueryProductBusinessResponseDto> selectNumList(Map<Object, Object> map);

    /**
     * 根据门店编码和商品编码、商品名称、通用名称、助记码，精确匹配国际条形码1或2 来查询数据
     * 门店组合list
     */
    List<QueryProductBusinessResponseDto> getStorePriceProList(Map<Object, Object> map);

    /**
     * 根据商品编码list 和加盟商来查询商品数据
     */
    List<QueryProductBusinessResponseDto> getStoreGroupProList(Map<Object, Object> map);

    /**
     * 根据[加盟商][地点][自编码]列表获取数据
     */
    List<GaiaProductBusiness> getOriginalBusinessList(@Param("client") String client, @Param("list") List<GaiaProductBusiness> list);

    /**
     * 主数据地点
     */
    GaiaStoreData getStoMdSite(@Param("proSite") String proSite, @Param("client") String client);

    /**
     * 自定义字段
     */
    GaiaProductZdy getProductZdy(@Param("client") String client, @Param("proSite") String proSite);

    /**
     * 商品列表
     *
     * @param client
     * @param dcCode
     * @param proCodes
     * @return
     */
    List<GaiaProductBusiness> selectProList(@Param("client") String client, @Param("dcCode") String dcCode, @Param("proCodes") List<String> proCodes);

    /**
     * 配送中心下的门店 + 门店主数据地点相同的其他门店
     *
     * @param client
     * @param proSite
     * @return
     */
    List<String> getStoreByDcAndAtoMdSite(@Param("client") String client, @Param("proSite") String proSite);

    /**
     * 批量更新
     *
     * @param business
     * @param productKeyList
     */
    void updateBatch(@Param("business") GaiaProductBusiness business, @Param("productKeyList") List<GaiaProductBusinessKey> productKeyList);

    /**
     * 商品分类编码列表
     *
     * @return
     */
    List<GaiaProductClass> getProductClassList();

    /**
     * 商品自分类编码列表
     *
     * @param client
     * @return
     */
    List<GaiaProductSclass> getProductSclassList(@Param("client") String client);

    /**
     * 进销项税率
     *
     * @return
     */
    List<GaiaTaxCode> getTaxCodeList();

    /**
     * 门店编码列表
     *
     * @param client  加盟商
     * @param proSite 地点
     * @return
     */
    List<String> getStoreByMdSite(@Param("client") String client, @Param("proSite") String proSite);

    /**
     * 经营类别下拉列表
     *
     * @param jyfwName
     * @return
     */
    List<JyfwooData> getJyfwooDataList(@Param("jyfwName") String jyfwName);

    /**
     * 跟据主键集合获取商品集合
     *
     * @param productKeyList 复合主健集合
     * @return 商品集合
     */
    List<GaiaProductBusiness> selectProListByKeyList(@Param("client") String client, @Param("proSelfCode") String proSelfCode, @Param("productKeyList") List<GaiaProductBusinessKey> productKeyList);

    /**
     * 商品、供应商、客户修改是否发起工作流开关
     */
    Integer getCountNeedWorkFlowDC(@Param("client") String client);

    /**
     * 查询商品自编码是否用过
     *
     * @param client
     * @return
     */
    Integer getPorSelfCodeCnt(@Param("client") String client, @Param("proSelfCode") String proSelfCode);

    /**
     * 根据加盟商 获取商品定位信息
     */
    int getProductPositionByClient(@Param("client") String client);

    /**
     * 指定加盟商_商品定位配置表默认值
     */
    void productPositionExcludeDefaultValue(@Param("list") List<ProductPositionExcludeDTO> list);

    /**
     * 指定加盟商_商品定位配置表默认值
     */
    List<ProductPositionExcludeDTO> getProductPositionDefaultValue(@Param("client") String client);

    /**
     * 根据加盟商+商品编码更新商品定位为X。
     */
    void updateProductPosition(@Param("client") String client, @Param("proSelfCode") String proSelfCode);

    HashMap<String, String> getSwitchForPH(@Param("client") String client, @Param("code") String code);

    List<GaiaProductBusiness> selectByPrimaryKeyList(@Param("client") String client, @Param("params") List<Map<String, String>> params);

    List<GaiaProductBusiness> selectByPrimaryKeyList1(@Param("client") String client, @Param("proSite") String proSite,
                                                      @Param("proSelfCodeList") List<String> proSelfCodeList,
                                                      @Param("batchEditParamList") List<BatchEditParam> batchEditParamList);

    List<GaiaProductBusiness> selectByClientList(@Param("params") List<Map<String, String>> params);

    GaiaProductBusiness getUnique(GaiaProductBusiness productCond);

    /**
     * 商品库存
     */
    List<ProStockDTO> getProStock(@Param("client") String client, @Param("list") List<String> proSelfCodeList);

    List<GaiaProductBusiness> selectByPrimaryByBuyunxupinlei(@Param("client") String client, @Param("list") List<GaiaProductBusinessKey> list);

    List<Dictionary> selectProSlaeClass(@Param("client") String client);

    int updateProductByItem(@Param("client") String client, @Param("proSelfCode") String proSelfCode,
                            @Param("field") String field, @Param("val") String val, @Param("list") List<GaiaProductBusinessKey> list);

    int updateProductListByItem(@Param("client") String client, @Param("list") List<Map<String, Object>> list);

    int updateProductItemsByKey(@Param("client") String client, @Param("proSelfCode") String proSelfCode,
                                @Param("itemList") List<Map<String, String>> itemList, @Param("keyList") List<GaiaProductBusinessKey> keyList);

    /**
     * 指定商品集合的经营范围列表
     */
    List<GaiaProductBusiness> getProJyfwIdList(@Param("client") String client, @Param("proSite") String proSite, @Param("proSelfCodeList") List<String> proSelfCodeList);

    List<GaiaProductBusiness> selectProListNoDC(@Param("client") String client, @Param("keyList") List<GaiaProductBusinessKey> gaiaProductBusinessKeyList);

    List<ProductDto> queryGoodsPrice(@Param("client") String client, @Param("proSite") String proSite, @Param("proSelfCode") String proSelfCode,
                                     @Param("proSelfCodeList") List<String> proSelfCodeList);


    /**
     * 药品信息
     *
     * @param drugQualityDTO
     * @return
     */
    DrugQualityVO queryProInfo(DrugQualityDTO drugQualityDTO);

    /**
     * 药品质量档案
     *
     * @param drugQualityDTO
     * @return
     */
    List<GoodsQualityDTO> queryDrugQuality(DrugQualityDTO drugQualityDTO);

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatchPro(@Param("list") List<GaiaProductBusiness> list);

    /**
     * 商品主号码段配置表查询
     *
     * @param client
     * @return
     */
    String getProductNumber(@Param("client") String client);


    String getProductNumberDigit(@Param("client") String client);

    int updateProLsj(@Param("client") String client, @Param("proSelfCode") String proSelfCode, @Param("proLsj") BigDecimal proLsj, @Param("proSiteList") List<String> proSiteList);

    /**
     * 连锁（包括仓库和门店）
     *
     * @param client
     * @param dcCode
     * @return
     */
    List<String> getNoWwsSiteList(@Param("client") String client, @Param("dcCode") String dcCode);

    /**
     * 所有商品
     *
     * @param client
     * @return
     */
    List<ProZdyListDto> selectAllPro(@Param("client") String client);
}