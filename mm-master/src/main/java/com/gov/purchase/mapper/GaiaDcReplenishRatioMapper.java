package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaDcReplenishRatio;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaDcReplenishRatioMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GaiaDcReplenishRatio record);

    int insertSelective(GaiaDcReplenishRatio record);

    GaiaDcReplenishRatio selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaDcReplenishRatio record);

    int updateByPrimaryKey(GaiaDcReplenishRatio record);

    int deleteByDc(@Param("client") String client, @Param("gdreSite") String gdreSite);

    int insertBatch(@Param("list") List<GaiaDcReplenishRatio> list);

    List<GaiaDcReplenishRatio> selectByDc(@Param("client") String client, @Param("gdreSite") String gdreSite);
}