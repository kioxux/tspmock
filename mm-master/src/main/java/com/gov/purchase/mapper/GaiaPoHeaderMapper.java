package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPoHeader;
import com.gov.purchase.entity.GaiaPoHeaderKey;
import com.gov.purchase.entity.GaiaWholesaleSalesman;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.wholesale.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaPoHeaderMapper {
    int deleteByPrimaryKey(GaiaPoHeaderKey key);

    int insert(GaiaPoHeader record);

    int insertSelective(GaiaPoHeader record);

    GaiaPoHeader selectByPrimaryKey(GaiaPoHeaderKey key);

    int updateByPrimaryKeySelective(GaiaPoHeader record);

    int updateByPrimaryKey(GaiaPoHeader record);

    String getPoId(GaiaPoHeader code);

    List<PoHeaderListResponseDto> selectPoHeaderList(PoHeaderListRequestDto code);

    List<PoHeaderListResponseDto> getPoHeaderList(PoHeaderListRequestDto code);

    List<PoItemListDto> selectPoItemList(PoItemRequestDto dto);

    List<PoItemResponseDto> selectPoHeader(PoItemRequestDto dto);

    int updatePoHeader(SavePurchaseOrderRequestDto dto);

    /**
     * 审批状态更新
     *
     * @return
     */
    int updateApproveStatus(@Param("client") String client, @Param("poId") String poId, @Param("poFlowNo") String poFlowNo);

    /**
     * 采购订单打印信息
     *
     * @param dto
     * @return
     */
    PurchaseOrderResponseDto getPurchaseOrderInfo(PoItemRequestDto dto);

    /**
     * 流程编号查询采购订单
     *
     * @param poFlowNo
     * @return
     */
    GaiaPoHeader selectByPoFlowNo(@Param("client") String client, @Param("poFlowNo") String poFlowNo);

    /**
     * 更新交货已完成标记
     */
    void closePo();

    List<PoItemResponseDto> temporaryDetails(@Param("client") String client, @Param("userId") String userId, @Param("poId") String poId);

    /**
     * 暂存明细
     */
    List<PoItemListDto> temporaryPoItemDetails(@Param("client") String client, @Param("poId") String poId);

    /**
     * 暂存下拉框
     */
    List<PoHeaderListResponseDto> temporaryList(@Param("client") String client, @Param("userId") String userId);

    List<BatchInfoDto> selectHisPo(@Param("client") String client, @Param("proSelfCode") String proSelfCode);


    /**
     * 后续单据
     *
     * @param client
     * @param poId
     * @return
     */
    Integer selectFollow(@Param("client") String client, @Param("poId") String poId);

    /**
     * 批发仓库列表
     *
     * @return
     */
    List<CompadmWmsVO> queryCompadmWms(@Param("client") String client);

    /**
     * 客户列表
     *
     * @return
     */
    List<PurchaseCompanyDTO> queryCompanyCode(@Param("client") String client);


    /**
     * 连锁采购单列表
     *
     * @param chainPurchaseOrderDto
     * @return
     */
    List<ChainPurchaseOrderVO> queryChainPurchaseOrderList(ChainPurchaseOrderDto chainPurchaseOrderDto);


    /**
     * 关闭连锁采购单
     *
     * @param list
     * @return
     */
    int updateFlag(List<ChainPurchaseOrderDto> list);

    /**
     * 连锁采购单明细
     *
     * @param chainPurchaseOrderDto
     * @return
     */
    List<ChainPurchaseOrderDetailDTO> queryChainPurchaseOrderDetail(ChainPurchaseOrderDto chainPurchaseOrderDto);

    /**
     * 销售员列表
     *
     * @return
     */
    List<GaiaWholesaleSalesman> querySalesManList(@Param("client") String client, @Param("gwsCode") String gwsCode);

    /**
     * 查询审批人
     *
     * @param client
     * @param proSite
     * @return
     */
    List<GaiaWmsShenPiPeiZhiDTO> queryUserRole(@Param("client") String client, @Param("proSite") String proSite);

    /**
     * 采购计划列表
     *
     * @param dto
     * @return
     */
    List<PurchasePlanReviewVo> queryPurchasePlanReview(PurchasePlanReviewDto dto);

    /**
     * 审核采购计划
     *
     * @param dto
     * @return
     */
    int updatePurchasePlan(PurchasePlanReviewDto dto);

    /**
     * 审核采购计划
     *
     * @param client
     * @param poId
     * @return
     */
    int refusePurchasePlan(@Param("client") String client, @Param("poId") String poId);
}