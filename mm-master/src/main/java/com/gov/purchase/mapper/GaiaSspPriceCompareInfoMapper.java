package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSspPriceCompareInfo;import com.gov.purchase.module.priceCompare.vo.PriceCompareProInfoVo;import org.apache.ibatis.annotations.Param;import java.util.List;

public interface GaiaSspPriceCompareInfoMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(GaiaSspPriceCompareInfo record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(GaiaSspPriceCompareInfo record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    GaiaSspPriceCompareInfo selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(GaiaSspPriceCompareInfo record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(GaiaSspPriceCompareInfo record);

    void deleteByPriceCompareProId(@Param("priceCompareProId") List<Long> priceCompareProId);

    void insertList2(@Param("params") List<PriceCompareProInfoVo> params, @Param("userId") String userId);
}