package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaWmsHuowei;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaWmsHuoweiMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaWmsHuowei record);

    int insertSelective(GaiaWmsHuowei record);

    GaiaWmsHuowei selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaWmsHuowei record);

    int updateByPrimaryKey(GaiaWmsHuowei record);

    List<GaiaWmsHuowei> getListBySiteHwh(@Param("client") String client, @Param("proSite") String proSite, @Param("wmHwhList") List<String> wmHwhList);
}