package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaBatchInfo;
import com.gov.purchase.entity.GaiaSoHeader;
import com.gov.purchase.entity.GaiaSoHeaderKey;
import com.gov.purchase.module.wholesale.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSoHeaderMapper {
    int deleteByPrimaryKey(GaiaSoHeaderKey key);

    int insert(GaiaSoHeader record);

    int insertSelective(GaiaSoHeader record);

    GaiaSoHeader selectByPrimaryKey(GaiaSoHeaderKey key);

    int updateByPrimaryKeySelective(GaiaSoHeader record);

    int updateByPrimaryKey(GaiaSoHeader record);

    /**
     * 销售凭证号取得
     *
     * @param client
     * @param soId
     * @return
     */
    String getSoId(@Param("client") String client, @Param("soId") String soId);

    /**
     * 批发订单处理 商品列表获取
     *
     * @param client  加盟商
     * @param proSite 地点
     * @param name    参数
     * @return
     */
    List<RecallInfo> queryGoodsList(@Param("client") String client, @Param("proSite") String proSite, @Param("soCustomerId") String soCustomerId,
                                    @Param("name") String name, @Param("gwsCode") String gwsCode, @Param("owner") String owner);

    /**
     * 批发销售查询列表
     *
     * @param querySalesOrderList
     * @return
     */
    List<SalesOrder> querySalesOrderList(QuerySalesOrderList querySalesOrderList);

    /**
     * 销售订单主表
     *
     * @param client 加盟商
     * @param soId   订单号
     * @return
     */
    SalesOrder querySalesOrder(@Param("client") String client, @Param("soId") String soId);

    /**
     * 调取请货单列表
     *
     * @param client        加盟商
     * @param gsrhBrId      门店
     * @param gsrhDateStart 开始时间
     * @param gsrhDateEnd   结束时间
     * @return
     */
    List<GoodsOrderDTO> getRequestGoodsOrderList(@Param("client") String client,
                                                 @Param("gsrhBrId") String gsrhBrId,
                                                 @Param("gsrhDateStart") String gsrhDateStart,
                                                 @Param("gsrhDateEnd") String gsrhDateEnd,
                                                 @Param("proCodeList") List<String> proCodeList,
                                                 @Param("type") String type
    );

    /**
     * 请货单明细
     *
     * @param item
     * @return
     */
    List<RecallInfo> queryGoodsListByReplenish(@Param("item") GoodsOrderDTO item);

    /**
     * 批发销售查询列表_主表
     *
     * @param vo
     * @return
     */
    List<SalesOrderHeader> querySalesOrderHeaderList(QuerySalesOrderHeaderVO vo);

    /**
     * 商品批号信息
     */
    List<GaiaBatchInfo> getProBatchInfo(@Param("client") String client, @Param("site") String site, @Param("proSelfCode") String proSelfCode);

    List<RecallInfo> selectBatPriceByPro(@Param("client") String client, @Param("proSelfCodeList") List<String> proSelfCodeList);

    Integer selectWmsDiaoboZCnt(@Param("client") String client, @Param("wmDdh") String wmDdh);


    /**
     * 查询审批配置
     *
     * @param client
     * @param proSite
     * @return
     */
    GaiaWmsShenPiPeiZhiDTO queryShenPiPeiZhi(@Param("client") String client, @Param("proSite") String proSite, @Param("type") Integer type);

    /**
     * 批发-销售/退货-审批详情
     *
     * @param wholeSaleApproveDTO
     * @return
     */
    List<WholeSaleApproveVo> queryWholesaleApproveList(WholeSaleApproveDTO wholeSaleApproveDTO);


    /**
     * 更新批发-销售/退货-审批状态
     *
     * @param client
     * @param soId
     * @param soApproveStatus
     * @param siteCode
     * @param soApproveBy
     * @param soApproveDate
     */
    void updateApproveStatus(@Param("client") String client, @Param("soId") String soId, @Param("soApproveStatus") String soApproveStatus, @Param("siteCode") String siteCode
            , @Param("soApproveBy") String soApproveBy, @Param("soApproveDate") String soApproveDate);

    /**
     * 查询审批人角色
     *
     * @param client
     * @param proSite
     * @return
     */
    GaiaWmsShenPiPeiZhiDTO queryUserRole(@Param("client") String client, @Param("proSite") String proSite, @Param("type") Integer type);

    /**
     * 批发订单处理 批量导入商品查询
     *
     * @param client       加盟商
     * @param proSite      地点
     * @param soCustomerId 参数
     * @return
     */
    List<RecallInfo> queryProductsList(@Param("client") String client, @Param("proSite") String proSite, @Param("soCustomerId") String soCustomerId);

    /**
     * 调取单体退货单
     *
     * @param dto
     * @return
     */
    List<ReturnOrderVO> queryReturnOrder(ReturnOrderDTO dto);

    /**
     * 更新单体退货状态
     *
     * @param client
     * @param orderIds
     */
    void updateStoreReturnOrderType(@Param("client") String client, @Param("orderIds") List<String> orderIds);

    /**
     * 更新连锁退货状态
     *
     * @param client
     * @param orderIds
     */
    void updateChainReturnOrderType(@Param("client") String client, @Param("orderIds") List<String> orderIds);

    /**
     * 调取单体退货单明细
     *
     * @param dto
     * @return
     */
    List<ReturnOrderDetailVO> queryStoreReturnOrderDetail(ReturnOrderDTO dto);

    /**
     * 调取单体退货单明细
     *
     * @param dto
     * @return
     */
    Integer closeReturnOrder(ReturnOrderDTO dto);

    /**
     * 调取连锁退货单
     *
     * @param dto
     * @return
     */
    List<ReturnOrderVO> queryChainReturnOrder(ReturnOrderDTO dto);

    /**
     * 调取连锁退货单明细
     *
     * @param dto
     * @return
     */
    List<ReturnOrderDetailVO> queryChainReturnOrderDetail(ReturnOrderDTO dto);

    /**
     * 关闭
     *
     * @param dto
     * @return
     */
    Integer closeWmsReturnOrder(ReturnOrderDTO dto);

    /**
     * 删除销售订单明细
     *
     * @param client
     * @param soId
     * @return
     */
    int deletePoItemByClientAndSoId(@Param("client") String client, @Param("soId") String soId);

    /**
     * 销售暂存列表
     *
     * @param client
     * @return
     */
    List<GaiaSoHeaderVO> temporaryList(@Param("client") String client, @Param("soId") String soId);
}