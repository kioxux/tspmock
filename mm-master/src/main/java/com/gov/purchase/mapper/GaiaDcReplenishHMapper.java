package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaDcReplenishH;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/7/21 15:49
 */
public interface GaiaDcReplenishHMapper {

    GaiaDcReplenishH getUnique(GaiaDcReplenishH cond);

    void update(GaiaDcReplenishH dcReplenishH);
}
