package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaMaterialDocLog;
import com.gov.purchase.entity.GaiaMaterialDocLogKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaMaterialDocLogMapper {
    int deleteByPrimaryKey(GaiaMaterialDocLogKey key);

    int insert(GaiaMaterialDocLog record);

    int insertSelective(GaiaMaterialDocLog record);

    GaiaMaterialDocLog selectByPrimaryKey(GaiaMaterialDocLogKey key);

    int updateByPrimaryKeySelective(GaiaMaterialDocLog record);

    int updateByPrimaryKey(GaiaMaterialDocLog record);

    int insertList(@Param("list") List<GaiaMaterialDocLog> list);
}