package com.gov.purchase.mapper;

import com.gov.purchase.entity.WmsKuCun;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/21 17:05
 **/
public interface GaiaWmsKuCunMapper {

    WmsKuCun getKuCunInfo(WmsKuCun cond);

}
