package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaStoreTax;
import com.gov.purchase.entity.GaiaStoreTaxKey;

public interface GaiaStoreTaxMapper {
    int deleteByPrimaryKey(GaiaStoreTaxKey key);

    int insert(GaiaStoreTax record);

    int insertSelective(GaiaStoreTax record);

    GaiaStoreTax selectByPrimaryKey(GaiaStoreTaxKey key);

    GaiaStoreTax  selectByClientAndCode(GaiaStoreTax record);

    int updateByPrimaryKeySelective(GaiaStoreTax record);

    int updateByPrimaryKey(GaiaStoreTax record);
}