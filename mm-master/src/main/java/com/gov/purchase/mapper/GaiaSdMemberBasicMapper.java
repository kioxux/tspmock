package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdMemberBasic;
import com.gov.purchase.entity.GaiaSdMemberBasicKey;

public interface GaiaSdMemberBasicMapper {
    int deleteByPrimaryKey(GaiaSdMemberBasicKey key);

    int insert(GaiaSdMemberBasic record);

    int insertSelective(GaiaSdMemberBasic record);

    GaiaSdMemberBasic selectByPrimaryKey(GaiaSdMemberBasicKey key);

    int updateByPrimaryKeySelective(GaiaSdMemberBasic record);

    int updateByPrimaryKey(GaiaSdMemberBasic record);
}