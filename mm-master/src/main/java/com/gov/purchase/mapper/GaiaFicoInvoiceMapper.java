package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaFicoInvoice;
import com.gov.purchase.entity.GaiaFicoInvoiceKey;
import com.gov.purchase.entity.GaiaInvoice;
import com.gov.purchase.module.purchase.dto.ListInvoicePayingRequestDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaFicoInvoiceMapper {
    int deleteByPrimaryKey(GaiaFicoInvoiceKey key);

    int insert(GaiaFicoInvoice record);

    int insertSelective(GaiaFicoInvoice record);

    GaiaFicoInvoice selectByPrimaryKey(GaiaFicoInvoiceKey key);

    int updateByPrimaryKeySelective(GaiaFicoInvoice record);

    int updateByPrimaryKey(GaiaFicoInvoice record);

    /**
     * 加盟商下所有发票查询
     */
    List<GaiaFicoInvoice> selectByClient();

    /**
     * 发票付款成功查询
     */
    List<GaiaFicoInvoice> selectSuccessByClient(ListInvoicePayingRequestDTO dto);

    /**
     * 查询发票信息
     *
     * @param index
     * @return
     */
    GaiaFicoInvoice selectByIndex(@Param("index") Integer index);

    /**
     * 短信发票申请
     * @return
     */
    List<GaiaFicoInvoice> selectSmsByClient();
}