package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaJyfwDataGspinfoKey;

public interface GaiaJyfwDataGspinfoMapper {
    int deleteByPrimaryKey(GaiaJyfwDataGspinfoKey key);

    int insert(GaiaJyfwDataGspinfoKey record);

    int insertSelective(GaiaJyfwDataGspinfoKey record);
}