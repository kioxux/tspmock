package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPayingItem;
import com.gov.purchase.entity.GaiaPayingItemKey;
import com.gov.purchase.module.purchase.dto.GetPayingItemDetailDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaPayingItemMapper {
    int deleteByPrimaryKey(GaiaPayingItemKey key);

    int insert(GaiaPayingItem record);

    int insertSelective(GaiaPayingItem record);

    GaiaPayingItem selectByPrimaryKey(GaiaPayingItemKey key);

    int updateByPrimaryKeySelective(GaiaPayingItem record);

    int updateByPrimaryKey(GaiaPayingItem record);

    /**
     * 批量插入
     * @param list
     * @return
     */
    int insertBatch(List<GaiaPayingItem> list);

    List<String> listNotPaying(@Param("client") String client,
                               @Param("codeList") List<String> codeList,
                               @Param("status") String status,
                               @Param("expireTime") int expireTime);

    List<GetPayingItemDetailDTO> selectByClientAndPicoId(@Param("client") String client, @Param("ficoId") String ficoId);

    /**
     * 最后一次付款详情
     * @param client
     * @param ficoCompanyCode
     * @return
     */
    GaiaPayingItem selectPayingItem(@Param("client") String client, @Param("ficoCompanyCode") String ficoCompanyCode);

}
