package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdStockMonth;
import com.gov.purchase.entity.GaiaSdStockMonthKey;

public interface GaiaSdStockMonthMapper {
    int deleteByPrimaryKey(GaiaSdStockMonthKey key);

    int insert(GaiaSdStockMonth record);

    int insertSelective(GaiaSdStockMonth record);

    GaiaSdStockMonth selectByPrimaryKey(GaiaSdStockMonthKey key);

    int updateByPrimaryKeySelective(GaiaSdStockMonth record);

    int updateByPrimaryKey(GaiaSdStockMonth record);
}