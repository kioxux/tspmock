package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdIntegralExchangeSet;
import com.gov.purchase.entity.GaiaSdIntegralExchangeSetKey;

public interface GaiaSdIntegralExchangeSetMapper {
    int deleteByPrimaryKey(GaiaSdIntegralExchangeSetKey key);

    int insert(GaiaSdIntegralExchangeSet record);

    int insertSelective(GaiaSdIntegralExchangeSet record);

    GaiaSdIntegralExchangeSet selectByPrimaryKey(GaiaSdIntegralExchangeSetKey key);

    int updateByPrimaryKeySelective(GaiaSdIntegralExchangeSet record);

    int updateByPrimaryKey(GaiaSdIntegralExchangeSet record);
}