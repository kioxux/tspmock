package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSoItem;
import com.gov.purchase.entity.GaiaSoItemKey;
import com.gov.purchase.module.purchase.dto.SalesOrderRecordDTO;
import com.gov.purchase.module.purchase.dto.SalesOrderRecordVO;
import com.gov.purchase.module.wholesale.dto.*;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GaiaSoItemMapper {
    int deleteByPrimaryKey(GaiaSoItemKey key);

    int insert(GaiaSoItem record);

    int insertSelective(GaiaSoItem record);

    GaiaSoItem selectByPrimaryKey(GaiaSoItemKey key);

    int updateByPrimaryKeySelective(GaiaSoItem record);

    int updateByPrimaryKey(GaiaSoItem record);

    /**
     * 总销量查询
     *
     * @return
     */
    BigDecimal selectTotalSale(@Param("client") String client, @Param("poSiteCode") String poSiteCode);

    /**
     * 销售订单明细
     *
     * @param client 加盟商
     * @param soId   订单号
     * @return
     */
    List<SalesOrderItem> querySalesDetails(@Param("client") String client, @Param("soId") String soId);

    /**
     * 销售退货订单列表
     *
     * @param queryReturnOrder
     * @return
     */
    List<ReturnOrder> querySalesOrderList(QueryReturnOrder queryReturnOrder);

    /**
     * 更新请货单的状态
     *
     * @param client
     * @param stoCode
     * @param voucherIdList
     */
    void updateReplenishStatus(@Param("client") String client, @Param("stoCode") String stoCode, @Param("voucherIdList") List<String> voucherIdList);

    /**
     * 查询已经开单的请货单个数
     *
     * @param client
     * @param stoCode
     * @param voucherIdList
     * @return
     */
    List<String> getReplenishStatus(@Param("client") String client, @Param("stoCode") String stoCode, @Param("voucherIdList") List<String> voucherIdList);

    /**
     * 根据主表主键，查询明细表数据
     */
    List<GaiaSoItem> getItemListByHeaderKey(@Param("client") String client, @Param("soId") String soId);


    /**
     * 查询销售记录明细（或销售退回记录明细）
     *
     * @param salesOrderRecordDTO
     * @return
     */
    List<SalesOrderRecordVO> queryRecord(SalesOrderRecordDTO salesOrderRecordDTO);


    /**
     * 销售明细汇总查询
     *
     * @param client
     * @param soIds
     * @param siteCode
     * @return
     */
    List<SoItemVo> querySoItemTotal(@Param("client") String client, @Param("soIds") List<String> soIds, @Param("siteCode") String siteCode);


    /**
     * 查询商品上次开票价
     *
     * @param client
     * @param proCodes
     * @param soCustomerId
     * @return
     */
    List<SalesOrderItem> queryProWmCkj(@Param("client") String client, @Param("proCodes") List<String> proCodes, @Param("soCustomerId") String soCustomerId);

    void insertSelectiveList(@Param("gaiaSoItemList") List<GaiaSoItem> gaiaSoItemList);
}