package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdPayDayreportD;
import com.gov.purchase.entity.GaiaSdPayDayreportDKey;

public interface GaiaSdPayDayreportDMapper {
    int deleteByPrimaryKey(GaiaSdPayDayreportDKey key);

    int insert(GaiaSdPayDayreportD record);

    int insertSelective(GaiaSdPayDayreportD record);

    GaiaSdPayDayreportD selectByPrimaryKey(GaiaSdPayDayreportDKey key);

    int updateByPrimaryKeySelective(GaiaSdPayDayreportD record);

    int updateByPrimaryKey(GaiaSdPayDayreportD record);
}