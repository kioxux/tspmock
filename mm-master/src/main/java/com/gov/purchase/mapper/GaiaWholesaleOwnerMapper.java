package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaWholesaleOwner;
import com.gov.purchase.entity.GaiaWholesaleOwnerKey;
import com.gov.purchase.module.wholesale.dto.ControlWholesaleOrdersVO;
import com.gov.purchase.module.wholesale.dto.WholesaleConsignorDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaWholesaleOwnerMapper {
    int deleteByPrimaryKey(GaiaWholesaleOwnerKey key);

    int insert(GaiaWholesaleOwner record);

    int insertSelective(GaiaWholesaleOwner record);

    GaiaWholesaleOwner selectByPrimaryKey(GaiaWholesaleOwnerKey key);

    int updateByPrimaryKeySelective(GaiaWholesaleOwner record);

    int updateByPrimaryKey(GaiaWholesaleOwner record);

    /**
     * 批发货主订单查询
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    List<ControlWholesaleOrdersVO> queryControlWholesaleOrders(WholesaleConsignorDTO wholesaleConsignorDTO);

    /**
     * 查询最大货主编码
     *
     * @param client
     * @return
     */
    String queryOwner(@Param("client") String client);

    /**
     * 批量插入货主
     *
     * @param list
     */
    void insertBatchOwner(List<GaiaWholesaleOwner> list);

    /**
     * 更新货主
     *
     * @param record
     * @return
     */
    int updateOwner(GaiaWholesaleOwner record);

    /**
     * 货主列表
     *
     * @param client
     * @param dcCode
     * @param saleManCode
     * @param owner
     * @return
     */
    List<GaiaWholesaleOwner> queryWholesaleConsignor(@Param("client") String client,
                                                     @Param("dcCode") String dcCode,
                                                     @Param("saleManCode") String saleManCode,
                                                     @Param("owner") String owner);
}