package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaClientInvoiceInfo;

public interface GaiaClientInvoiceInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GaiaClientInvoiceInfo record);

    int insertSelective(GaiaClientInvoiceInfo record);

    GaiaClientInvoiceInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaClientInvoiceInfo record);

    int updateByPrimaryKey(GaiaClientInvoiceInfo record);
}