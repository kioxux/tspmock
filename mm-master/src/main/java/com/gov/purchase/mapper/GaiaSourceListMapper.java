package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSourceList;
import com.gov.purchase.entity.GaiaSourceListKey;
import com.gov.purchase.module.replenishment.dto.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSourceListMapper {
    int deleteByPrimaryKey(GaiaSourceListKey key);

    int insert(GaiaSourceList record);

    int insertSelective(GaiaSourceList record);

    GaiaSourceList selectByPrimaryKey(GaiaSourceListKey key);

    int updateByPrimaryKeySelective(GaiaSourceList record);

    int updateByPrimaryKey(GaiaSourceList record);

    List<GaiaSourceList> selectSourceList(GaiaSourceList dto);

    /**
     * 历史供应商列表
     *
     * @param dto
     * @return
     */
    List<SupplierResponseDto> querySupplierHistoryList(SupplierHistoryRequestDto dto);

    /**
     * 货源清单创建、修改列表
     *
     * @param dto
     * @return
     */
    List<SourceCreateListResponseDto> querySourceCreateList(SourceCreateListRequestDto dto);

    /**
     * 货源清单查看列表
     *
     * @param dto
     * @return
     */
    List<SourceListResponseDto> querySourceList(SourceListRequestDto dto);

    /**
     * @param sourceListKey
     * @return
     */
    String getNewSouListCode(GaiaSourceListKey sourceListKey);

    /**
     * 根据加盟商 + 地点 + 商品自编码 + 供应商编号查询
     *
     * @param sourceList
     * @return
     */
    int selectCountBySupplier(GaiaSourceList sourceList);

    /**
     * 货源清单编码
     */
    GaiaSourceList selectNo(@Param("client") String client, @Param("poProCode") String poProCode, @Param("poSiteCode") String poSiteCode);

    GaiaSourceList getMaxSourceList(GaiaSourceList gaiaSourceList);

    /**
     * 货源清单批量更新
     *
     * @param list
     * @return
     */
    int batchUpdateSourceList(List<GaiaSourceList> list);

    /**
     * 货源清单列表导出
     *
     * @param dto
     * @return
     */
    List<SourceListResponseDto> querySourceExportList(SourceListExportDto dto);
}