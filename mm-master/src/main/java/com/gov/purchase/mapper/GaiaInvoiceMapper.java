package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaInvoice;
import com.gov.purchase.module.purchase.dto.ListInvoicePayingRequestDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author staxc
 * @Date 2020/10/28 16:14
 * @desc 发票
 */
public interface GaiaInvoiceMapper {
    /**
     * 加盟商下所有发票查询
     */
    List<GaiaInvoice> selectByClient();

    int updateByPrimaryKeySelective(GaiaInvoice record);

    int insert(GaiaInvoice invoice);

    /**
     * 发票付款成功查询
     */
    List<GaiaInvoice> selectSuccessByClient(ListInvoicePayingRequestDTO dto);

    /**
     * 查询发票信息
     *
     * @param index
     * @return
     */
    GaiaInvoice selectByIndex(@Param("index") Integer index);
}
