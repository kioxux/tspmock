package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSupplierChange;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSupplierChangeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GaiaSupplierChange record);

    int insertSelective(GaiaSupplierChange record);

    GaiaSupplierChange selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaSupplierChange record);

    int updateByPrimaryKey(GaiaSupplierChange record);

    /**
     * 根据流程单号获取修改记录
     *
     * @param wfOrder 流程单号
     * @return 修改记录
     */
    List<GaiaSupplierChange> getChangeList(@Param("client") String client, @Param("wfOrder") String wfOrder);

    /**
     * 修改状态，审批驳回
     *
     * @param changeIdList 主键列表
     */
    void updateStatusFaile(@Param("changeIdList") List<Integer> changeIdList);

    /**
     * 修改状态，审批成功
     *
     * @param changeIdList 主键列表
     */
    void updateStatusSuccess(@Param("changeIdList") List<Integer> changeIdList);

    /**
     * 批量保存
     *
     * @param changeList 供应商修改记录集合
     */
    void saveBatch(@Param("changeList") List<GaiaSupplierChange> changeList);
}