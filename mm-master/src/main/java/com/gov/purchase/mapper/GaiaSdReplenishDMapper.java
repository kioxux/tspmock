package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdReplenishD;
import com.gov.purchase.entity.GaiaSdReplenishDKey;
import com.gov.purchase.module.purchase.dto.DirectPurchaseListDTO;
import com.gov.purchase.module.purchase.dto.DirectPurchaseListVO;
import com.gov.purchase.module.replenishment.dto.NeedProWmsParams;
import com.gov.purchase.module.replenishment.dto.StoreReplenishDetails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdReplenishDMapper {
    int deleteByPrimaryKey(GaiaSdReplenishDKey key);

    int insert(GaiaSdReplenishD record);

    int insertSelective(GaiaSdReplenishD record);

    GaiaSdReplenishD selectByPrimaryKey(GaiaSdReplenishDKey key);

    int updateByPrimaryKeySelective(GaiaSdReplenishD record);

    int updateByPrimaryKey(GaiaSdReplenishD record);

    /**
     * 根据单号取明细
     *
     * @return
     */
    List<GaiaSdReplenishD> selectByVoucherId(@Param("gaiaSdReplenishDKey") GaiaSdReplenishDKey gaiaSdReplenishDKey);

    /**
     * 更新行号
     *
     * @param gaiaSdReplenishDKey
     * @return
     */
    int updateSerial(@Param("gaiaSdReplenishDKey") GaiaSdReplenishDKey gaiaSdReplenishDKey, @Param("gsrdSerial") String gsrdSerial);

    /**
     * 更新订单价
     *
     * @param gaiaSdReplenishD
     * @return
     */
    int updateVoucherAmt(GaiaSdReplenishD gaiaSdReplenishD);

    /**
     * 门店补货验证查询
     *
     * @param gaiaSdReplenishD
     * @return
     */
    List<GaiaSdReplenishD> selectByVoucherIdForStore(GaiaSdReplenishD gaiaSdReplenishD);

    /**
     * 税率
     *
     * @param client
     * @param poId
     * @param proCode
     * @return
     */
    String getInputTax1(@Param("client") String client, @Param("poId") String poId, @Param("proCode") String proCode);

    /**
     * 税率
     *
     * @param client
     * @param poId
     * @param proCode
     * @return
     */
    String getInputTax2(@Param("client") String client, @Param("poId") String poId, @Param("proCode") String proCode);

    /**
     * 批量保存
     *
     * @param replenishDList
     */
    void saveBatch(@Param("replenishDList") List<GaiaSdReplenishD> replenishDList);

    /**
     * 根据主表主键，查询子表明细
     */
    List<GaiaSdReplenishD> getListByHeaderKey(@Param("client") String client, @Param("gsrhBrId") String gsrhBrId, @Param("gsrhVoucherId") String gsrhVoucherId);

    List<GaiaSdReplenishD> findList(GaiaSdReplenishD cond);

    List<DirectPurchaseListVO> selectDirectPurchaseList(@Param("dto") DirectPurchaseListDTO directPurchaseListDTO);

    List<StoreReplenishDetails> selectNeedProListByWms(@Param("client") String client, @Param("params") NeedProWmsParams needProWmsParams);

    List<GaiaSdReplenishD> selectGsrdNeedQtyByPro(@Param("client") String client, @Param("proCodes") List<String> proCodes);
}