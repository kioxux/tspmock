package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdMarketing;
import com.gov.purchase.entity.GaiaSdMarketingKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdMarketingMapper {
    int deleteByPrimaryKey(GaiaSdMarketingKey key);

    int insert(GaiaSdMarketing record);

    int insertSelective(GaiaSdMarketing record);

    GaiaSdMarketing selectByPrimaryKey(GaiaSdMarketingKey key);

    int updateByPrimaryKeySelective(GaiaSdMarketing record);

    int updateByPrimaryKeyWithBLOBs(GaiaSdMarketing record);

    int updateByPrimaryKey(GaiaSdMarketing record);

    /**
     * 根据审批流程编码查找营销活动
     */
    GaiaSdMarketing selectTemplateByFlowNo(@Param("gsmFlowNo") String gsmFlowNo);

    /**
     * 更具审批流程编号更新状态
     */
    void updateByFlowNo(@Param("marketing") GaiaSdMarketing marketing);

    /**
     * 批量保存
     */
    void saveBatch(@Param("list") List<GaiaSdMarketing> list);
}
