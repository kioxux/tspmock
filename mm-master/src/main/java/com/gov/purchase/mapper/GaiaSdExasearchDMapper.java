package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdExasearchD;
import com.gov.purchase.entity.GaiaSdExasearchDKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdExasearchDMapper {
    int deleteByPrimaryKey(GaiaSdExasearchDKey key);

    int insert(GaiaSdExasearchD record);

    int insertSelective(GaiaSdExasearchD record);

    GaiaSdExasearchD selectByPrimaryKey(GaiaSdExasearchDKey key);

    int updateByPrimaryKeySelective(GaiaSdExasearchD record);

    int updateByPrimaryKey(GaiaSdExasearchD record);

    /**
     * 批量保存
     */
    void saveBatch(@Param("list") List<GaiaSdExasearchD> list);
}
