package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdPaymentMethod;
import com.gov.purchase.entity.GaiaSdPaymentMethodKey;

public interface GaiaSdPaymentMethodMapper {
    int deleteByPrimaryKey(GaiaSdPaymentMethodKey key);

    int insert(GaiaSdPaymentMethod record);

    int insertSelective(GaiaSdPaymentMethod record);

    GaiaSdPaymentMethod selectByPrimaryKey(GaiaSdPaymentMethodKey key);

    int updateByPrimaryKeySelective(GaiaSdPaymentMethod record);

    int updateByPrimaryKey(GaiaSdPaymentMethod record);
}