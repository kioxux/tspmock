package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaAllotPriceGroup;
import com.gov.purchase.entity.GaiaAllotPriceGroupKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaAllotPriceGroupMapper {
    int deleteByPrimaryKey(GaiaAllotPriceGroupKey key);

    int insert(GaiaAllotPriceGroup record);

    int insertSelective(GaiaAllotPriceGroup record);

    GaiaAllotPriceGroup selectByPrimaryKey(GaiaAllotPriceGroupKey key);

    int updateByPrimaryKeySelective(GaiaAllotPriceGroup record);

    int updateByPrimaryKey(GaiaAllotPriceGroup record);

    //extend
    List<GaiaAllotPriceGroup> queryAllotPriceGroupList(@Param("client") String client,
                                                       @Param("alpReceiveSite") String alpReceiveSite,
                                                       @Param("gapgType") String gapgType);

    List<GaiaAllotPriceGroup> selectByPrimaryKeyList(List<GaiaAllotPriceGroupKey> list);

    int insertBatchList(List<GaiaAllotPriceGroup> list);

    int updateBatch(List<GaiaAllotPriceGroup> list);

    int batchDelete(List<GaiaAllotPriceGroup> list);

}