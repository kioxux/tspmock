package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdMarketingSearch;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaSdMarketingSearchMapper {
    int deleteByPrimaryKey(String gsmsId);

    int insert(GaiaSdMarketingSearch record);

    int insertSelective(GaiaSdMarketingSearch record);

    GaiaSdMarketingSearch selectByPrimaryKey(String gsmsId);

    int updateByPrimaryKeySelective(GaiaSdMarketingSearch record);

    int updateByPrimaryKeyWithBLOBs(GaiaSdMarketingSearch record);

    int updateByPrimaryKey(GaiaSdMarketingSearch record);

    /**
     * 批量保存
     */
    void saveBatch(@Param("searchList") List<GaiaSdMarketingSearch> searchList);
}
