package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaOwnerCustomer;
import com.gov.purchase.entity.GaiaOwnerCustomerKey;
import com.gov.purchase.module.wholesale.dto.OwnerCustomerVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaOwnerCustomerMapper {
    int deleteByPrimaryKey(GaiaOwnerCustomerKey key);

    int insert(GaiaOwnerCustomer record);

    int insertSelective(GaiaOwnerCustomer record);

    GaiaOwnerCustomer selectByPrimaryKey(GaiaOwnerCustomerKey key);

    int updateByPrimaryKeySelective(GaiaOwnerCustomer record);

    int updateByPrimaryKey(GaiaOwnerCustomer record);

    /**
     * 根据仓库地点查询货主客户
     *
     * @param client 加盟商
     * @param dcCode 仓库
     * @param owner  货主编码
     * @return
     */
    List<OwnerCustomerVO> selectCusByDcCodeAndOwner(@Param("client") String client, @Param("dcCode") String dcCode, @Param("owner") String owner);

    /**
     * 删除货主下的客户
     *
     * @param client
     * @param dcCode
     * @param owner
     * @return
     */
    int deleteOwnerCus(@Param("client") String client, @Param("dcCode") String dcCode, @Param("owner") String owner);

    /**
     * 批量插入货主客户
     *
     * @param list
     */
    void insertBatchOwnerCustomer(List<OwnerCustomerVO> list);

}