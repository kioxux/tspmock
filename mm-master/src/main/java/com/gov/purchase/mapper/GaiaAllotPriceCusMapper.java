package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaAllotPriceCus;
import com.gov.purchase.module.delivery.dto.GaiaAllotPriceCusDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaAllotPriceCusMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GaiaAllotPriceCus record);

    int insertSelective(GaiaAllotPriceCus record);

    GaiaAllotPriceCus selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GaiaAllotPriceCus record);

    int updateByPrimaryKey(GaiaAllotPriceCus record);

    /**
     * 客户调拨价格列表
     *
     * @param client
     * @param alpReceiveSite
     * @param alpCusCode
     * @param alpProCode
     * @return
     */
    List<GaiaAllotPriceCusDto> selectAllotPriceCusList(@Param("client") String client,
                                                       @Param("alpReceiveSite") String alpReceiveSite,
                                                       @Param("alpCusCode") String alpCusCode,
                                                       @Param("alpProCode") String alpProCode);

    GaiaAllotPriceCus selectAllotPriceCusBySite(@Param("client") String client,
                                                @Param("alpReceiveSite") String alpReceiveSite,
                                                @Param("alpCusCode") String alpCusCode);

    GaiaAllotPriceCus selectAllotPriceCusByPro(@Param("client") String client,
                                               @Param("alpReceiveSite") String alpReceiveSite,
                                               @Param("alpCusCode") String alpCusCode,
                                               @Param("alpProCode") String alpProCode);

    int batchInsert(List<GaiaAllotPriceCus> list);

    int batchUpdate(List<GaiaAllotPriceCus> list);

    List<GaiaAllotPriceCusDto> selectAllotPriceCusListForGroup(@Param("client") String client,
                                                               @Param("alpReceiveSite") String alpReceiveSite,
                                                               @Param("cusList") List<String> cusList,
                                                               @Param("gapgType") Integer gapgType);

    List<GaiaAllotPriceCusDto> selectAllotPriceCusListByPro(@Param("client") String client,
                                                            @Param("alpReceiveSite") String alpReceiveSite,
                                                            @Param("proList") List<String> cusList,
                                                            @Param("gapgType") Integer gapgType);
}