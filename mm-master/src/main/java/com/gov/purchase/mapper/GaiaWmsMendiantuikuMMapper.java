package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaWmsMendiantuikuM;
import com.gov.purchase.entity.GaiaWmsMendiantuikuMKey;
import org.apache.ibatis.annotations.Param;

public interface GaiaWmsMendiantuikuMMapper {
    int deleteByPrimaryKey(GaiaWmsMendiantuikuMKey key);

    int insert(GaiaWmsMendiantuikuM record);

    int insertSelective(GaiaWmsMendiantuikuM record);

    GaiaWmsMendiantuikuM selectByPrimaryKey(GaiaWmsMendiantuikuMKey key);

    int updateByPrimaryKeySelective(GaiaWmsMendiantuikuM record);

    int updateByPrimaryKey(GaiaWmsMendiantuikuM record);

    GaiaWmsMendiantuikuM selectWmsMendiantuikuByPoId(@Param("client") String client, @Param("matPoId") String matPoId, @Param("matProCode") String matProCode);
}