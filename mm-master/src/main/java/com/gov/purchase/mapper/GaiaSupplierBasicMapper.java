package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSupplierBasic;
import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.base.dto.SupplierDto;
import com.gov.purchase.module.purchase.dto.SupplierListForPurchaseRequestDto;
import com.gov.purchase.module.purchase.dto.SupplierListReturnDto;
import com.gov.purchase.module.supplier.dto.GetSupBasicListRequestDTO;
import com.gov.purchase.module.supplier.dto.QuerySupListRequestDto;
import com.gov.purchase.module.supplier.dto.QuerySupListResponseDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaSupplierBasicMapper {
	int deleteByPrimaryKey(String supCode);

	int insert(GaiaSupplierBasic record);

	int insertSelective(GaiaSupplierBasic record);

	GaiaSupplierBasic selectByPrimaryKey(String supCode);

	int updateByPrimaryKeySelective(GaiaSupplierBasic record);

	int updateByPrimaryKey(GaiaSupplierBasic record);

	List<QuerySupListResponseDto> selectBasicDrugSupplierInfo(QuerySupListRequestDto dto);

	List<QuerySupListResponseDto> selectDrugSupplierInfo(QuerySupListRequestDto dto);

	List<SupplierListReturnDto> selectSupplierList(SupplierListForPurchaseRequestDto dto);


	/**
	 * 供应商编码生成
	 */
	String getSupCode();

	/**
	 * 批量查询（通用）
	 *
	 * @param searchValBasicMap
	 * @return
	 */
	List<SupplierDto> selectBasicBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);

	/**
	 * 批量查询（通用+业务）
	 *
	 * @param searchValBasicMap
	 * @return
	 */
	List<SupplierDto> selectBusinessBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);

	/**
	 * 供应商回车查询
	 */
	List<GaiaSupplierBusiness> getSupBasicList(@Param("dto") GetSupBasicListRequestDTO dto);
}