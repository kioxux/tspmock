package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaPriceTag;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GaiaPriceTagMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GaiaPriceTag record);

    int insertSelective(GaiaPriceTag record);

    GaiaPriceTag selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GaiaPriceTag record);

    int updateByPrimaryKeyWithBLOBs(GaiaPriceTag record);

    int updateByPrimaryKey(GaiaPriceTag record);

    List<GaiaPriceTag> selectPriceTagList(@Param("client") String client, @Param("gptName") String gptName, @Param("gptType") Integer gptType);
}