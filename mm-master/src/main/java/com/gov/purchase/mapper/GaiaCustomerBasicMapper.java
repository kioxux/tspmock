package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaCustomerBasic;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.module.base.dto.CustomerDto;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.customer.dto.CustomerGspListRequestDto;
import com.gov.purchase.module.customer.dto.CustomerGspListResponseDto;
import com.gov.purchase.module.customer.dto.CustomerInfoRequestDto;
import com.gov.purchase.module.customer.dto.CustomerListReponseDto;
import com.gov.purchase.module.customer.dto.CustomerListRequestDto;
import com.gov.purchase.module.customer.dto.CustomerInfoReponseDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GaiaCustomerBasicMapper {
    int deleteByPrimaryKey(String cusCode);

    int insert(GaiaCustomerBasic record);

    int insertSelective(GaiaCustomerBasic record);

    GaiaCustomerBasic selectByPrimaryKey(String cusCode);

    int updateByPrimaryKeySelective(GaiaCustomerBasic record);

    int updateByPrimaryKey(GaiaCustomerBasic record);

    List<CustomerGspListResponseDto> queryCustomerList(CustomerGspListRequestDto dto);

    /*
    查询客户维护列表
     */
    List<CustomerListReponseDto> queryCustomerMatainList(CustomerListRequestDto dto);

    CustomerInfoReponseDto getCustomerInfo(CustomerInfoRequestDto dto);

    /**
     * 客户编码生成
     */
    String getCusCode();

    /**
     * 批量查询（通用）
     *
     * @param searchValBasicMap
     * @return
     */
    List<CustomerDto> selectBasicBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);

    /**
     * 批量查询（通用+业务）
     *
     * @param searchValBasicMap
     * @return
     */
    List<CustomerDto> selectBusinessBatch(@Param("searchValBasicMap") Map<String, SearchValBasic> searchValBasicMap);


    /**
     * 选择机构下拉列表
     */
    List<GaiaDcData> getSiteList(@Param("client") String client, @Param("userId") String userId);

    /**
     * 回车查询
     */
    List<GaiaCustomerBasic> enterSelect(@Param("cusName") String cusName, @Param("cusCreditCode") String cusCreditCode);

}