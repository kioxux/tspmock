package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdDeviceCheck;
import com.gov.purchase.entity.GaiaSdDeviceCheckKey;

public interface GaiaSdDeviceCheckMapper {
    int deleteByPrimaryKey(GaiaSdDeviceCheckKey key);

    int insert(GaiaSdDeviceCheck record);

    int insertSelective(GaiaSdDeviceCheck record);

    GaiaSdDeviceCheck selectByPrimaryKey(GaiaSdDeviceCheckKey key);

    int updateByPrimaryKeySelective(GaiaSdDeviceCheck record);

    int updateByPrimaryKey(GaiaSdDeviceCheck record);
}