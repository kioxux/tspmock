package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaProductGspinfoLog;
import com.gov.purchase.entity.GaiaProductGspinfoLogKey;

public interface GaiaProductGspinfoLogMapper {
    int deleteByPrimaryKey(GaiaProductGspinfoLogKey key);

    int insert(GaiaProductGspinfoLog record);

    int insertSelective(GaiaProductGspinfoLog record);

    GaiaProductGspinfoLog selectByPrimaryKey(GaiaProductGspinfoLogKey key);

    int updateByPrimaryKeySelective(GaiaProductGspinfoLog record);

    int updateByPrimaryKey(GaiaProductGspinfoLog record);
}