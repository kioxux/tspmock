package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdIntegralAddSet;
import com.gov.purchase.entity.GaiaSdIntegralAddSetKey;

public interface GaiaSdIntegralAddSetMapper {
    int deleteByPrimaryKey(GaiaSdIntegralAddSetKey key);

    int insert(GaiaSdIntegralAddSet record);

    int insertSelective(GaiaSdIntegralAddSet record);

    GaiaSdIntegralAddSet selectByPrimaryKey(GaiaSdIntegralAddSetKey key);

    int updateByPrimaryKeySelective(GaiaSdIntegralAddSet record);

    int updateByPrimaryKey(GaiaSdIntegralAddSet record);
}