package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdSaleD;
import com.gov.purchase.entity.GaiaSdSaleDKey;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentListResponseDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GaiaSdSaleDMapper {
    int deleteByPrimaryKey(GaiaSdSaleDKey key);

    int insert(GaiaSdSaleD record);

    int insertSelective(GaiaSdSaleD record);

    GaiaSdSaleD selectByPrimaryKey(GaiaSdSaleDKey key);

    int updateByPrimaryKeySelective(GaiaSdSaleD record);

    int updateByPrimaryKey(GaiaSdSaleD record);

    /**
     * 门店30天销量查询
     */
    BigDecimal selectSaleNum(@Param("client") String client, @Param("poSiteCode") String poSiteCode, @Param("proCode") String proCode);

    /**
     * 销量
     *
     * @param client
     * @param salesVolumeStartDate
     * @param salesVolumeEndDate
     * @param proSelfCodeList
     * @return
     */
    List<DcReplenishmentListResponseDto> selectSaleProByDays(@Param("client") String client, @Param("salesVolumeStartDate") String salesVolumeStartDate,
                                                             @Param("salesVolumeEndDate") String salesVolumeEndDate, @Param("proSelfCodeList") List<String> proSelfCodeList,
                                                             @Param("dcCode") String dcCode, @Param("storeCodes") List<String> storeCodes);
}