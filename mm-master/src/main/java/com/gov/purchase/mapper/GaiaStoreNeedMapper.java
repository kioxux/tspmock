package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaStoreNeed;
import com.gov.purchase.entity.GaiaStoreNeedKey;
import org.apache.ibatis.annotations.Param;

public interface GaiaStoreNeedMapper {
    int deleteByPrimaryKey(GaiaStoreNeedKey key);

    int insert(GaiaStoreNeed record);

    int insertSelective(GaiaStoreNeed record);

    GaiaStoreNeed selectByPrimaryKey(GaiaStoreNeedKey key);

    int updateByPrimaryKeySelective(GaiaStoreNeed record);

    int updateByPrimaryKey(GaiaStoreNeed record);

    /**
     * 生成最新单号
     * @param client
     * @return
     */
    String getNewStoreNeedCode(@Param("client") String client);
}