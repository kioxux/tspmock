package com.gov.purchase.mapper;

import com.gov.purchase.entity.GaiaSdCleandrawerD;
import com.gov.purchase.entity.GaiaSdCleandrawerDKey;

public interface GaiaSdCleandrawerDMapper {
    int deleteByPrimaryKey(GaiaSdCleandrawerDKey key);

    int insert(GaiaSdCleandrawerD record);

    int insertSelective(GaiaSdCleandrawerD record);

    GaiaSdCleandrawerD selectByPrimaryKey(GaiaSdCleandrawerDKey key);

    int updateByPrimaryKeySelective(GaiaSdCleandrawerD record);

    int updateByPrimaryKey(GaiaSdCleandrawerD record);
}