package com.gov.purchase.module.replenishment.dto;

import lombok.Data;

/**
 * @description: 自定义字段列表
 * @author: yzf
 * @create: 2022-01-19 11:53
 */
@Data
public class ProZdyListDto {
    private String value;

    private String type;
}
