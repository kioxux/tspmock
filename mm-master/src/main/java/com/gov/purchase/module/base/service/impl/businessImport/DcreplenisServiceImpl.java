package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.module.base.dto.businessImport.DcreplenisImp;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.replenishment.dto.GaiaDcReplenishExcludDto;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.12
 */
@Service
public class DcreplenisServiceImpl extends BusinessImport {

    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private FeignService feignService;

    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 参数 验证
        if (field == null || field.isEmpty()) {
            throw new CustomResultException("参数为空");
        }
        if (field.get("site") == null || StringUtils.isBlank(field.get("site").toString())) {
            throw new CustomResultException("配送中心不能为空");
        }
        if (field.get("type") == null || StringUtils.isBlank(field.get("type").toString())) {
            throw new CustomResultException("类型不能为空");
        }
        // 错误消息
        List<String> errorList = new ArrayList<>();
        // 当前用户
        TokenUser user = feignService.getLoginInfo();
        // 配送中心
        String site = field.get("site").toString();
        // 类型
        Integer type = NumberUtils.createInteger(field.get("type").toString());
        List<String> proList = new ArrayList<>();
        // 验证商品数据
        for (Integer key : map.keySet()) {
            // 行数据
            DcreplenisImp dcreplenisImp = (DcreplenisImp) map.get(key);
            // 商品重复
            if (proList.contains(dcreplenisImp.getGdreCode())) {
                errorList.add(MessageFormat.format(ResultEnum.E0119.getMsg(), key + 1));
                continue;
            }
            // 商品编码
            proList.add(dcreplenisImp.getGdreCode());
        }
        if (errorList.size() > 0) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        // 商品列表
        List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectProList(user.getClient(), site, proList);
        if (CollectionUtils.isEmpty(gaiaProductBusinessList)) {
            throw new CustomResultException("请填写正确的商品编码");
        }
        List<GaiaDcReplenishExcludDto> resultList = new ArrayList();
        // 验证商品数据
        for (Integer key : map.keySet()) {
            // 行数据
            DcreplenisImp dcreplenisImp = (DcreplenisImp) map.get(key);
            GaiaProductBusiness entity = gaiaProductBusinessList.stream().filter(item -> (item.getProSelfCode().equals(dcreplenisImp.getGdreCode()))).findFirst().orElse(null);
            if (entity == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品编码"));
                continue;
            }
            GaiaDcReplenishExcludDto gaiaDcReplenishExclud = new GaiaDcReplenishExcludDto();
            // 业务类型
            gaiaDcReplenishExclud.setGdreBtype(type);
            // 商品类型
            gaiaDcReplenishExclud.setGdrePtype(2);
            // 编号
            gaiaDcReplenishExclud.setGdreCode(dcreplenisImp.getGdreCode());
            // 补货量
            gaiaDcReplenishExclud.setGdreReplenishNum(NumberUtils.toInt(dcreplenisImp.getGdreReplenishNum()));
            // 商品描述
            gaiaDcReplenishExclud.setProDepict(entity.getProDepict());
            // 商品规格
            gaiaDcReplenishExclud.setProSpecs(entity.getProSpecs());
            // 生产厂家
            gaiaDcReplenishExclud.setProFactoryName(entity.getProFactoryName());
            resultList.add(gaiaDcReplenishExclud);
        }
        if (errorList.size() > 0) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success(resultList);
    }
}
