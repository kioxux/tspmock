package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author staxc
 * @Date 2020/10/27 10:55
 * @desc
 */
@Data
@ApiModel("发票查询接口传出参数")
public class InvoiceSendResponseDto {
    /**
     * pdf地址
     */
    private String cUrl;
    /**
     * 发票请求流水号
     */
    private String ficoFpqqlsh;

    /**
     * 发票号码
     */
    private String ficoFphm;

    /**
     * 发票代码
     */
    private String ficoFpdm;

    /**
     * 发票开票日期
     */
    private String ficoFprq;

    /**
     * 发票开票信息
     */
    private String cMsg;


}
