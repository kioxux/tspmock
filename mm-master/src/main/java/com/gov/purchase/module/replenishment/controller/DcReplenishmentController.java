package com.gov.purchase.module.replenishment.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentRequestDto;
import com.gov.purchase.module.replenishment.dto.SupplierHistoryRequestDto;
import com.gov.purchase.module.replenishment.dto.SupplierRequestDto;
import com.gov.purchase.module.replenishment.service.DcReplenishmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "DC补货")
@RestController
@RequestMapping("dcReplenishment")
public class DcReplenishmentController {

    @Resource
    private DcReplenishmentService dcReplenishmentService;

    @Log("DC补货列表")
    @ApiOperation("DC补货列表")
    @GetMapping("queryReplenishmentList")
    public Result queryReplenishmentList(@RequestParam("dcCode") String dcCode
                                          , @RequestParam("pageSize") Integer pageSize, @RequestParam("pageNum") Integer pageNum
    ) {
        return ResultUtil.success(dcReplenishmentService.queryReplenishmentList(dcCode, pageNum, pageSize));
    }

    @Log("供应商历史列表")
    @ApiOperation("供应商历史列表")
    @PostMapping("querySupplierHistoryList")
    public Result querySupplierHistoryList(@Valid @RequestBody SupplierHistoryRequestDto dto) {
        return ResultUtil.success(dcReplenishmentService.querySupplierHistoryList(dto));
    }

    @Log("选择供应商列表")
    @ApiOperation("选择供应商列表")
    @PostMapping("querySupplierList")
    public Result querySupplierList(@Valid @RequestBody SupplierRequestDto dto) {
        return ResultUtil.success(dcReplenishmentService.querySupplierList(dto));
    }


    @Log("DC补货下单")
    @ApiOperation("DC补货下单")
    @PostMapping("saveDcReplenishmentInfo")
    public Result saveDcReplenishmentInfo(@Valid @RequestBody List<DcReplenishmentRequestDto> list) {
        return dcReplenishmentService.saveDcReplenishmentInfo(list);
    }

    @Log("商品大类列表")
    @ApiOperation("商品大类列表")
    @PostMapping("getProductClass")
    public Result getProductClass() {
        return ResultUtil.success(dcReplenishmentService.getProductClass());
    }

}
