package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonConstants;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaRetailPrice;
import com.gov.purchase.entity.GaiaSdMessage;
import com.gov.purchase.entity.GaiaSdProductPrice;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.businessImport.PriceGroupProduct;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.store.dto.price.PriceGroupProductVO;
import com.gov.purchase.utils.DateUtils;
import org.apache.commons.collections4.CollectionUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : Yzf
 * description: 价格组商品导入
 * create time: 2021/12/21 11:42
 */
@Service
public class PriceGroupProductImport extends BusinessImport {
    @Resource
    private FeignService feignService;

    @Resource
    private GaiaPriceGroupMapper gaiaPriceGroupMapper;

    @Resource
    private GaiaProductBasicMapper gaiaProductBasicMapper;

    @Resource
    private GaiaRetailPriceMapper gaiaRetailPriceMapper;

    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;

    @Resource
    private RedisClient redisClient;

    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        List<String> errorProPriceList = new ArrayList<>();
        List<String> errorList = new ArrayList<>();
        List<String> error = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        // 价格组id
        String prcGroupId = (String) field.get("prcGroupId");
        if (StringUtils.isBlank(prcGroupId)) {
            return ResultUtil.error(ResultEnum.GAPG_CODE_EMPTY);
        }
        Set<PriceGroupProduct> products = new HashSet<>();
        for (Integer key : map.keySet()) {
            PriceGroupProduct priceGroupProduct = (PriceGroupProduct) map.get(key);
            products.add(priceGroupProduct);
        }
        List<String> proIds = products.stream().map(PriceGroupProduct::getPrcProId).collect(Collectors.toList());
        List<String> proSelfCodeList = gaiaProductBasicMapper.getDistinctProductList(user.getClient(), proIds);

        // 查询过滤存在的商品
        List<PriceGroupProductVO> pros = gaiaPriceGroupMapper.selectProByPriceGroup(user.getClient(), prcGroupId);
        List<String> existPro = pros.stream().map(PriceGroupProductVO::getPrcProId).collect(Collectors.toList());
        List<String> stores = gaiaPriceGroupMapper.selectPriceGroupStore(user.getClient(), prcGroupId);
        List<PriceGroupProductVO> productOld = new ArrayList<>();
        List<PriceGroupProductVO> productNew = new ArrayList<>();
        for (Integer key : map.keySet()) {
            PriceGroupProduct product = (PriceGroupProduct) map.get(key);
            // 筛选出不存在商品
            if (!proSelfCodeList.contains(product.getPrcProId())) {
                errorList.add(MessageFormat.format("第{0}行，商品编码不存在", key + 1));
                continue;
            }
            int countStore = gaiaProductBasicMapper.selectProInStore(user.getClient(), product.getPrcProId(), stores);
            if (countStore == 0) {
                error.add(MessageFormat.format("第{0}行，当前价格组门店不存在该商品", key + 1));
                continue;
            }
            List<String> storeIds = gaiaPriceGroupMapper.selectBatchStore(user.getClient(), product.getPrcProId());
            boolean flag = false;
            for (String item : stores) {
                if (!storeIds.contains(item)) {
                    flag = true;
                    break;
                }
            }
            // 筛选出商品
            if (flag && StringUtils.isBlank(product.getPrcPriceNormal())) {
                errorProPriceList.add(MessageFormat.format("第{0}行，商品调后售价不为空", key + 1));
                continue;
            }
            PriceGroupProductVO productVO = new PriceGroupProductVO();
            productVO.setClient(user.getClient());
            productVO.setPrcGroupId(prcGroupId);
            productVO.setPrcProId(product.getPrcProId());
            productVO.setPrcPriceNormal(StringUtils.isNotBlank(product.getPrcPriceNormal()) ? new BigDecimal(product.getPrcPriceNormal()) : null);
            productVO.setPrcPriceHy(StringUtils.isNotBlank(product.getPrcPriceHy()) ? new BigDecimal(product.getPrcPriceHy()) : null);
            productVO.setPrcPriceHyr(StringUtils.isNotBlank(product.getPrcPriceHyr()) ? new BigDecimal(product.getPrcPriceHyr()) : null);
            productVO.setPrcPriceNormalEnd(StringUtils.isNotBlank(product.getPrcPriceNormal()) ? new BigDecimal(product.getPrcPriceNormal()) : null);
            productVO.setPrcPriceHyEnd(StringUtils.isNotBlank(product.getPrcPriceHy()) ? new BigDecimal(product.getPrcPriceHy()) : null);
            productVO.setPrcPriceHyrEnd(StringUtils.isNotBlank(product.getPrcPriceHyr()) ? new BigDecimal(product.getPrcPriceHyr()) : null);
            if (!existPro.contains(product.getPrcProId())) {
                productVO.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                productVO.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                productVO.setPrcCreateBy(user.getUserId());
                productNew.add(productVO);
            } else {
                for (PriceGroupProductVO pro : pros) {
                    if (pro.getPrcProId().equalsIgnoreCase(product.getPrcProId())) {
                        if (productVO.getPrcPriceNormal() == null) {
                            productVO.setPrcPriceNormal(pro.getPrcPriceNormal());
                            productVO.setPrcPriceNormalEnd(pro.getPrcPriceNormal());
                        }
                        if (productVO.getPrcPriceHy() == null) {
                            productVO.setPrcPriceHy(pro.getPrcPriceHy());
                            productVO.setPrcPriceHyEnd(pro.getPrcPriceHy());
                        }
                        if (productVO.getPrcPriceHyr() == null) {
                            productVO.setPrcPriceHyr(pro.getPrcPriceHyr());
                            productVO.setPrcPriceHyrEnd(pro.getPrcPriceHyr());
                        }
                        break;
                    }
                }
                productVO.setPrcChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                productVO.setPrcChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                productOld.add(productVO);
            }
        }
        if (!CollectionUtils.isEmpty(errorList)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(errorList);
            return result;
        }
        if (!CollectionUtils.isEmpty(error)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(error);
            return result;
        }
        if (!CollectionUtils.isEmpty(errorProPriceList)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(errorProPriceList);
            return result;
        }
        if (CollectionUtils.isNotEmpty(productOld)) {
            gaiaPriceGroupMapper.updateGroupProBatch(productOld);
        }
        if (CollectionUtils.isNotEmpty(productNew)) {
            gaiaPriceGroupMapper.insertPriceProBatch(productNew);
        }
        List<GaiaRetailPrice> retailPrices = new ArrayList<>();
        List<GaiaSdMessage> sdMessages = new ArrayList<>();
        /**
         * 调价单插入
         */
        String yymmdd = LocalDate.now().toString().replace("-", "");
        //  主键: 生成调价单号  YYYYMMDD+4位流水
        // 获取当前最大流水
        String currentMaxNo = gaiaRetailPriceMapper.selectMaxModifNo(user.getClient(), yymmdd);
        String prcModfiyNo = yymmdd + "0001";
        if (currentMaxNo != null) {
            prcModfiyNo = yymmdd + (String.format("%04d", Integer.parseInt(currentMaxNo.substring(currentMaxNo.length() - 4)) + 1));
        }
        String finalPrcModfiyNo = prcModfiyNo;
        List<GaiaSdProductPrice> storeProNew = new ArrayList<>();
        List<PriceGroupProductVO> all = new ArrayList<>();
        all.addAll(productNew);
        all.addAll(productOld);
        List<String> proAllIds = all.stream().map(PriceGroupProductVO::getPrcProId).collect(Collectors.toList());
        List<GaiaProductBusiness> allBus = gaiaProductBusinessMapper.getAllProductBusiness(user.getClient(), proAllIds, stores);
        Map<String, String> maps = new HashMap<>();
        for (String store : stores) {
            for (PriceGroupProduct pro : products) {
                List<GaiaProductBusiness> one = allBus.stream().filter(u -> u.getProSelfCode().equals(pro.getPrcProId()) && u.getProSite().equals(store)).collect(Collectors.toList());
                if (CollectionUtils.isEmpty(one)) {
                    continue;
                }
                GaiaSdProductPrice gaiaSdProductPrice = new GaiaSdProductPrice();
                gaiaSdProductPrice.setClient(user.getClient());
                gaiaSdProductPrice.setGsppBrId(store);
                gaiaSdProductPrice.setGsppProId(pro.getPrcProId());
                gaiaSdProductPrice.setGsppPriceNormal(StringUtils.isNotBlank(pro.getPrcPriceNormal()) ? new BigDecimal(pro.getPrcPriceNormal()) : null);
                gaiaSdProductPrice.setGsppPriceHy(StringUtils.isNotBlank(pro.getPrcPriceHy()) ? new BigDecimal(pro.getPrcPriceHy()) : null);
                gaiaSdProductPrice.setGsppPriceHyr(StringUtils.isNotBlank(pro.getPrcPriceHyr()) ? new BigDecimal(pro.getPrcPriceHyr()) : null);
                storeProNew.add(gaiaSdProductPrice);

                String currentVoucherId = maps.get(store);
                GaiaSdMessage sdMessage = new GaiaSdMessage();
                sdMessage.setClient(user.getClient());
                // 调价门店
                sdMessage.setGsmId(store);
                // 用来查询前缀
                sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear());
                if (StringUtils.isBlank(currentVoucherId)) {
                    currentVoucherId = gaiaSdMessageMapper.getCurrentVoucherId(sdMessage);
                }
                long voucherId = NumberUtils.toLong(currentVoucherId.substring(6));
                while (redisClient.exists(StringUtils.parse(CommonConstants.GAIA_SD_MESSAGE_GSM_VOUCHER_ID, user.getClient(), store, String.valueOf(voucherId)))) {
                    voucherId++;
                }
                redisClient.set(StringUtils.parse(CommonConstants.GAIA_SD_MESSAGE_GSM_VOUCHER_ID, user.getClient(), store, String.valueOf(voucherId)), String.valueOf(voucherId), 1800);
                sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear() + StringUtils.leftPad(String.valueOf(voucherId), 9, "0"));
                maps.put(store, sdMessage.getGsmVoucherId());
                sdMessage.setGsmValue("praAdjustNo=" + sdMessage.getGsmVoucherId() + "&praStore=" + sdMessage.getGsmId());
                sdMessage.setGsmRemark(MessageFormat.format("您有新的商品调价信息,商品编码为[{0}],请及时查看", pro.getPrcProId()));
                sdMessage.setGsmPlatform("FX");
                // 是否查看 默认为N-未查看
                sdMessage.setGsmFlag("N");
                // 调价单号
                sdMessage.setGsmBusinessVoucherId(finalPrcModfiyNo);
                sdMessage.setGsmArriveDate(DateUtils.getCurrentDateStrYYMMDD());
                sdMessage.setGsmArriveTime(DateUtils.getCurrentTimeStrHHMMSS());
                sdMessage.setGsmPage("pricingList");
                sdMessages.add(sdMessage);
                if (pro.getPrcPriceNormal() != null) {
                    GaiaRetailPrice price = new GaiaRetailPrice();
                    price.setClient(user.getClient());
                    price.setPrcStore(store);
                    price.setPrcProduct(pro.getPrcProId());
                    price.setPrcClass("P001");
                    // 调价单号
                    price.setPrcModfiyNo(finalPrcModfiyNo);
                    // 审批状态默认为否
                    price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                    // 修改之后价格 (9,4)
                    price.setPrcAmount(new BigDecimal(pro.getPrcPriceNormal()));
                    // 修改前价格  (9,4)
                    price.setPrcAmountBefore(new BigDecimal(pro.getPrcPriceNormal()));
                    // 创建日期
                    price.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                    // 创建时间
                    price.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    price.setPrcSource("0");
                    price.setPrcCreateUser(user.getUserId());
                    retailPrices.add(price);
                }
                if (pro.getPrcPriceHy() != null) {
                    GaiaRetailPrice price = new GaiaRetailPrice();
                    price.setClient(user.getClient());
                    price.setPrcStore(store);
                    price.setPrcProduct(pro.getPrcProId());
                    price.setPrcClass("P002");
                    // 调价单号
                    price.setPrcModfiyNo(finalPrcModfiyNo);
                    // 审批状态默认为否
                    price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                    // 修改之后价格 (9,4)
                    price.setPrcAmount(new BigDecimal(pro.getPrcPriceHy()));
                    // 修改前价格  (9,4)
                    price.setPrcAmountBefore(new BigDecimal(pro.getPrcPriceHy()));
                    // 创建日期
                    price.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                    // 创建时间
                    price.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    price.setPrcSource("0");
                    price.setPrcCreateUser(user.getUserId());
                    retailPrices.add(price);
                }
                if (pro.getPrcPriceHyr() != null) {
                    GaiaRetailPrice price = new GaiaRetailPrice();
                    price.setClient(user.getClient());
                    price.setPrcStore(store);
                    price.setPrcProduct(pro.getPrcProId());
                    price.setPrcClass("P004");
                    // 调价单号
                    price.setPrcModfiyNo(finalPrcModfiyNo);
                    // 审批状态默认为否
                    price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                    // 修改之后价格 (9,4)
                    price.setPrcAmount(new BigDecimal(pro.getPrcPriceHyr()));
                    // 修改前价格  (9,4)
                    price.setPrcAmountBefore(new BigDecimal(pro.getPrcPriceHyr()));
                    // 创建日期
                    price.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                    // 创建时间
                    price.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    price.setPrcSource("0");
                    price.setPrcCreateUser(user.getUserId());
                    retailPrices.add(price);
                }
            }

        }
        if (CollectionUtils.isNotEmpty(storeProNew)) {
            List<GaiaSdProductPrice> exist = gaiaPriceGroupMapper.selectStoreProBatch(user.getClient(), storeProNew);
            if (CollectionUtils.isNotEmpty(exist)) {
                List<GaiaSdProductPrice> olds = new ArrayList<>();
                List<GaiaSdProductPrice> news = new ArrayList<>();
                storeProNew.forEach(
                        item -> {
                            int count = 0;
                            for (GaiaSdProductPrice one : exist) {
                                if (item.getGsppBrId().equalsIgnoreCase(one.getGsppBrId()) && item.getGsppProId().equalsIgnoreCase(one.getGsppProId())) {
                                    if (item.getGsppPriceNormal() == null) {
                                        item.setGsppPriceNormal(one.getGsppPriceNormal());
                                    }
                                    if (item.getGsppPriceHy() == null) {
                                        item.setGsppPriceHy(one.getGsppPriceHy());
                                    }
                                    if (item.getGsppPriceHyr() == null) {
                                        item.setGsppPriceHyr(one.getGsppPriceHyr());
                                    }
                                    count = count + 1;
                                }
                            }
                            if (count == 0) {
                                news.add(item);
                            } else {
                                if (item.getGsppPriceNormal() != null || item.getGsppPriceHy() != null || item.getGsppPriceHyr() != null) {
                                    olds.add(item);
                                }
                            }
                        }
                );
                if (CollectionUtils.isNotEmpty(news)) {
                    gaiaPriceGroupMapper.insertStoreProBatch(news);
                }
                if (CollectionUtils.isNotEmpty(olds)) {
                    gaiaPriceGroupMapper.updateStoreProBatch(olds);
                }
            } else {
                gaiaPriceGroupMapper.insertStoreProBatch(storeProNew);
            }
        }
        if (CollectionUtils.isNotEmpty(retailPrices)) {
            gaiaRetailPriceMapper.insertList(retailPrices);
        }
        if (CollectionUtils.isNotEmpty(sdMessages)) {
            try {
                gaiaSdMessageMapper.insertBatch(sdMessages);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ResultUtil.success();
    }
}
