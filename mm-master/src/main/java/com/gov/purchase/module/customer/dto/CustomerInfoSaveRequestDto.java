package com.gov.purchase.module.customer.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class CustomerInfoSaveRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client", hidden = true)
    private String client;

    @ApiModelProperty(value = "地点", name = "cusSite")
    @Size(min = 0, max = 10, message = "地点长度超限")
    @NotBlank(message = "地点不能为空")
    private String cusSite;

    @ApiModelProperty(value = "客户编号（企业）", name = "cusSelfCode")
    @Size(min = 0, max = 50, message = "客户编号（企业）长度超限")
    @NotBlank(message = "客户编号（企业）不能为空")
    private String cusSelfCode;

    @ApiModelProperty(value = "客户编号", name = "cusCode")
    @Size(min = 0, max = 10, message = "客户编号长度超限")
    private String cusCode;

    @ApiModelProperty(value = "银行代码", name = "cusBankCode")
    @Size(min = 0, max = 10, message = "银行代码长度超限")
    private String cusBankCode;

    @ApiModelProperty(value = "银行名称", name = "cusBankName")
    @Size(min = 0, max = 50, message = "银行名称长度超限")
    private String cusBankName;

    @ApiModelProperty(value = "账户持有人", name = "cusAccountPerson")
    @Size(min = 0, max = 20, message = "账户持有人长度超限")
    private String cusAccountPerson;

    @ApiModelProperty(value = "银行账号", name = "cusBankAccount")
    @Size(min = 0, max = 20, message = "银行账号长度超限")
    private String cusBankAccount;

    @ApiModelProperty(value = "付款条件（源自：GAIA_PAYMENT_TYPE的 ORD_TYPE）", name = "cusPayTerm")
    @Size(min = 0, max = 10, message = "付款条件长度超限")
    private String cusPayTerm;

    @ApiModelProperty(value = "支付方式(1-银行转账、2-承兑汇票、3-线下票据； 4 - 赊销5 - 电汇)", name = "cusPayMode")
    @Size(min = 0, max = 1, message = "支付方式长度超限")
    private String cusPayMode;

    @ApiModelProperty(value = "铺底授信额度", name = "cusCreditAmt")
    @Size(min = 0, max = 20, message = "铺底授信额度长度超限")
    private String cusCreditAmt;

    @ApiModelProperty(value = "禁止销售(0-否，1-是)", name = "cusNoPurchase")
    @Size(min = 0, max = 1, message = "禁止采购长度超限")
    private String cusNoSale;

    @ApiModelProperty(value = "禁止退货(0-否，1-是)", name = "cusNoCusplier")
    @Size(min = 0, max = 1, message = "禁止退货长度超限")
    private String cusNoReturn;

    @ApiModelProperty(value = "业务联系人", name = "cusBussinessContact")
    @Size(min = 0, max = 20, message = "业务联系人长度超限")
    private String cusBussinessContact;

    @ApiModelProperty(value = "联系人电话", name = "cusContactTel")
    @Size(min = 0, max = 20, message = "联系人电话长度超限")
    private String cusContactTel;

    @ApiModelProperty(value = "是否启用信用管理(0-否，1-是)", name = "cusCreditFlag")
    @Size(min = 0, max = 1, message = "是否启用信用管理长度超限")
    private String cusCreditFlag;

    @ApiModelProperty(value = "信用额度", name = "cusCreditQuota")
    @Size(min = 0, max = 16, message = "信用额度长度超限")
    private String cusCreditQuota;

    @ApiModelProperty(value = "信用检查点(1-销售订单，2-发货过账，3-开票)", name = "cusCreditCheck")
    @Size(min = 0, max = 1, message = "信用检查点长度超限")
    private String cusCreditCheck;


}
