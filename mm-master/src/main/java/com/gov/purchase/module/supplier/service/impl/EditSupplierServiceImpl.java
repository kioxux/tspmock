package com.gov.purchase.module.supplier.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.entity.GaiaSupplierBusinessKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.supplier.dto.QuerySupRequestDto;
import com.gov.purchase.module.supplier.dto.QueryUnDrugUpdateRequestDto;
import com.gov.purchase.module.supplier.dto.SupplierListRequestDto;
import com.gov.purchase.module.supplier.dto.SupplierListResponseDto;
import com.gov.purchase.module.supplier.service.EditSupplierService;
import com.gov.purchase.module.supplier.service.UnDrugSupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class EditSupplierServiceImpl implements EditSupplierService {

    @Resource
    GaiaSupplierBasicMapper gaiaSupplierBasicMapper;

    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;

    @Resource
    FeignService feignService;

    @Resource
    UnDrugSupplierService unDrugSupplierService;

    @Resource
    GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;
    @Resource
    OperationFeignService operationFeignService;
    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;

    /**
     * 供应商列表
     *
     * @param dto QueryProRequestDto
     * @return PageInfo<SupplierListResponseDto>
     */
    public PageInfo<SupplierListResponseDto> selectQuerySupInfo(SupplierListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        // 设置当前登录人的加盟商编号
        dto.setClient(user.getClient());
        //分页查询
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<SupplierListResponseDto> QueryProList = gaiaSupplierBusinessMapper.selectQueryProInfo(dto);
        PageInfo<SupplierListResponseDto> pageInfo = new PageInfo<>(QueryProList);
        return pageInfo;
    }

    /**
     * 供应商详情
     *
     * @param dto QuerySupRequestDto
     * @return QuerySupResponseDto
     */
    public GaiaSupplierBusiness selectQuerySup(QuerySupRequestDto dto) {
        GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
        // 加盟商
        gaiaSupplierBusinessKey.setClient(dto.getClient());
        // 地点
        gaiaSupplierBusinessKey.setSupSite(dto.getSupSite());
        // 自编码
        gaiaSupplierBusinessKey.setSupSelfCode(dto.getSupSelfCode());
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(gaiaSupplierBusinessKey);
        return gaiaSupplierBusiness;
    }

    /*
     * 供应商维护
     *
     * @param dto QueryUnDrugUpdateRequestDto
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateSupEditInfo(QueryUnDrugUpdateRequestDto dto) {
        String client = feignService.getLoginInfo().getClient();

        // 非连锁门店集合
        List<String> stoList = gaiaProductBasicMapper.getStoList(client);
        // 如果地点是DC包括地点下的门店
        List<GaiaDcData> dcList = gaiaProductBasicMapper.getDcListToUpdate(client, null);

        GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
        // 加盟商
        gaiaSupplierBusinessKey.setClient(dto.getClient());
        // 地点
        gaiaSupplierBusinessKey.setSupSite(dto.getSupSite());
        // 供应商自编码
        gaiaSupplierBusinessKey.setSupSelfCode(dto.getSupSelfCode());
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(gaiaSupplierBusinessKey);
        // 供应商不存在
        if (gaiaSupplierBusiness == null) {
            throw new CustomResultException(ResultEnum.E0163);
        }
        BeanUtils.copyProperties(dto, gaiaSupplierBusiness);
        //非药供应商编辑保存
        gaiaSupplierBusinessMapper.updateByPrimaryKey(gaiaSupplierBusiness);
        gayOperationRef(gaiaSupplierBusiness, stoList);

        // 如果地点是DC
        GaiaDcData gaiaDcData = dcList.stream().filter(Objects::nonNull)
                .filter(item -> item.getDcCode().equals(dto.getSupSite()))
                .findFirst().orElse(null);
        if (!ObjectUtils.isEmpty(gaiaDcData)) {
            // # 配送中心连锁总部下的门店 + 商品主数据下的门店
            List<String> stoToUpdateList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(client, dto.getSupSite());
            stoToUpdateList.stream().filter(Objects::nonNull).distinct().forEach(stoCode -> {
                GaiaSupplierBusiness business = new GaiaSupplierBusiness();
                BeanUtils.copyProperties(dto, business);
                business.setClient(client);
                business.setSupSite(stoCode);
                business.setSupSelfCode(dto.getSupSelfCode());
                gaiaSupplierBusinessMapper.updateByPrimaryKey(business);
                gayOperationRef(business, stoList);
            });
        }

    }

    public void gayOperationRef(GaiaSupplierBusiness gaiaSupplierBusiness, List<String> stoList) {
        try {
            if (!CollectionUtils.isEmpty(stoList) && stoList.contains(gaiaSupplierBusiness.getSupSite())) {
                operationFeignService.supplierSync(gaiaSupplierBusiness.getClient(), gaiaSupplierBusiness.getSupSite(), "1", Collections.singletonList(gaiaSupplierBusiness.getSupSelfCode()));
            } else {
                operationFeignService.supplierSync(gaiaSupplierBusiness.getClient(), gaiaSupplierBusiness.getSupSite(), "2", Collections.singletonList(gaiaSupplierBusiness.getSupSelfCode()));
            }
        } catch (Exception e) {
            log.info("[供应商信息新增/修改 gys-operation调用 异常]{}", e.getMessage());
        }
    }
}
