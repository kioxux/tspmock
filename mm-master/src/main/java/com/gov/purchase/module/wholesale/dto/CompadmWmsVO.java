package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

/**
 * @description: 批发公司
 * @author: yzf
 * @create: 2021-11-17 17:06
 */
@Data
public class CompadmWmsVO {

    private String client;

    /**
     * 批发公司ID
     */
    private String compadmId;

    /**
     * 批发公司名称
     */
    private String compadmName;
}
