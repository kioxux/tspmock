package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaPoItem;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 调取连锁采购单明细
 * @author: yzf
 * @create: 2021-11-18 09:48
 */
@Data
public class ChainPurchaseOrderDetailDTO extends GaiaPoItem {

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 中包装量
     */
    private String proMidPackage;

    /**
     * 商品定位
     */
    private String proPosition;

    /**
     * 销售级别
     */
    private String proSlaeClass;

    /**
     * 销售价
     */
    private BigDecimal salePrice;

    /**
     * 销售金额
     */
    private BigDecimal saleTotal;

    private BigDecimal priceNormal;
    /**
     * 零售金额
     */
    private BigDecimal totalNormal;

    /**
     * 实际数量
     */
    private BigDecimal realNeedQty;

    /**
     * 门店库存
     */
    private BigDecimal gssQty;

    /**
     * 备注
     */
    private String poHeadRemark;

    /**
     * 创建时间
     */
    private String poCreatTime;

    /**
     * 创建日期
     */
    private String poCreatDate;

    /**
     * 采购日期
     */
    private String poDate;

    /**
     * 审核状态
     */
    private String poSoFlag;

    /**
     * 税率
     */
    private String taxCodeValue;

    /**
     * 门店Id
     */
    private String poDeliveryTypeStore;

    /**
     * 客户id
     */
    private String poCompanyCode;

    private String dcName;

    private String stoName;

    private String cusStoName;

    /**
     * 客户名
     */
    private String customerName;

    private String poSupplierId;
    /**
     * 销售员
     */
    private String salePerName;

    /**
     * 销售员Id
     */
    private String poSupplierSalesman;

    private BigDecimal batPoPrice;

    /**
     * 零售价
     */
    private BigDecimal proLsj;

    /**
     * 会员价
     */
    private BigDecimal proHyj;

    /**
     * 国批价
     */
    private BigDecimal proGpj;

    /**
     * 国零价
     */
    private BigDecimal proGlj;

    /**
     * 预设售价1
     */
    private BigDecimal proYsj1;

    /**
     * 预设售价2
     */
    private BigDecimal proYsj2;

    /**
     * 预设售价3
     */
    private BigDecimal proYsj3;

    private String proOutputTax;

    private String soId;
}
