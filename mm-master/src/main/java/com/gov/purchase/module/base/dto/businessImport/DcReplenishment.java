package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class DcReplenishment {

    /**
     * 商品
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = true)
    private String proSelfCode;

    /**
     * 供应商
     */
    @ExcelValidate(index = 1, name = "供应商编码", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false)
    private String poSupplierId;

    private String poSupplierName;

    /**
     * 付款条件
     */
    @ExcelValidate(index = 2, name = "付款条件", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false)
    private String supPayTerm;

    /**
     * 采购数量
     */
    @ExcelValidate(index = 3, name = "采购数量", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = true)
    private String poQty;

    /**
     * 计划交货时间
     */
    @ExcelValidate(index = 4, name = "计划交货时间", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd", addRequired = false)
    private String poDeliveryDate;

    /**
     * 备注
     */
    @ExcelValidate(index = 5, name = "备注", type = ExcelValidate.DataType.STRING, maxLength = 200, addRequired = false)
    private String poLineRemark;

    /**
     * 采购单价
     */
    @ExcelValidate(index = 6, name = "采购单价", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.99, addRequired = false)
    private String purchasePrice;

    /**
     * 生产批号
     */
    @ExcelValidate(index = 7, name = "生产批号", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false)
    private String poBatchNo;

    /**
     * 生产日期
     */
    @ExcelValidate(index = 8, name = "生产日期", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd", addRequired = false)
    private String poScrq;

    /**
     * 有效期
     */
    @ExcelValidate(index = 9, name = "有效期", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd", addRequired = false)
    private String poYxq;

    /**
     * 税率
     */
    private String proInputTax;

    /**
     * 供应商显示名称
     */
    private String supplierDisplayName;

}

