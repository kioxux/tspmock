package com.gov.purchase.module.goods.service;

import com.gov.purchase.module.goods.dto.SaveBasicProListRequestDTO;

/**
 * @author Zhangchi
 * @since 2021/11/02/10:58
 */
public interface GoodsInfoService {

    /**
     * 商品首营
     * @param dto
     */
    void firstSaleGoods(SaveBasicProListRequestDTO dto);
}
