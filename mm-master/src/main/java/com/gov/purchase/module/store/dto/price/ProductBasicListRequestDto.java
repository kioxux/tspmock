package com.gov.purchase.module.store.dto.price;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel("商品名称筛选请求参数")
public class ProductBasicListRequestDto {

    @ApiModelProperty(value = "价格类型(P001-零售价、P002-会员价、P003-医保价、P004-会员日价、P005拆零价、P006网上零售价、P007-网上会员价)", name = "prcClass")
    @NotBlank(message = "价格类型不能为空")
    private String prcClass;

    @ApiModelProperty(value = "加盟商", name = "client")
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;

    @ApiModelProperty(value = "门店编码集合", name = "storeList")
    private List<String> storeList;

    @ApiModelProperty(value = "门店编码", name = "storeCode")
    private String storeCode;
}
