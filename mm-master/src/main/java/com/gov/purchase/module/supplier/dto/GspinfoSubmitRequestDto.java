package com.gov.purchase.module.supplier.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@ApiModel(value = "供应商首营提交传入参数")
public class GspinfoSubmitRequestDto {

    @ApiModelProperty(value = "营业执照编号", name = "supCreditCode", required = true)
    // @NotBlank(message = "营业执照编号不能为空")
    private String supCreditCode;

    @ApiModelProperty(value = "供应商名称", name = "supName", required = true)
    @NotBlank(message = "供应商名称不能为空")
    private String supName;

    @ApiModelProperty(value = "供应商自编码", name = "supSelfCode", required = true)
    @NotBlank(message = "供应商自编码不能为空")
    private String supSelfCode;

    @ApiModelProperty(value = "供应商编码", name = "supCode", required = true)
    @NotBlank(message = "供应商编码不能为空")
    private String supCode;

    @ApiModelProperty(value = "地点", name = "supSite", required = true)
    @NotBlank(message = "地点不能为空")
    private String supSite;

    @ApiModelProperty(value = "营业期限", name = "supCreditDate", required = true)
    // @NotBlank(message = "营业期限不能为空")
    private String supCreditDate;

    @ApiModelProperty(value = "法人", name = "supLegalPerson", required = true)
    // @NotBlank(message = "法人不能为空")
    private String supLegalPerson;

    @ApiModelProperty(value = "注册地址", name = "supRegAdd", required = true)
    // @NotBlank(message = "注册地址不能为空")
    private String supRegAdd;

    @ApiModelProperty(value = "许可证编号", name = "supCreditCode")
    private String supLicenceNo;

    @ApiModelProperty(value = "发证单位", name = "supStatus")
    private String supLicenceOrgan;

    @ApiModelProperty(value = "发证日期", name = "sell")
    private String supLicenceDate;

    @ApiModelProperty(value = "有效期至", name = "supLicenceValid")
    private String supLicenceValid;

    @ApiModelProperty(value = "邮政编码", name = "supPostalCode")
    private String supPostalCode;

    @ApiModelProperty(value = "税务登记证", name = "supStatus")
    private String supTaxCard;

    @ApiModelProperty(value = "组织机构代码证", name = "sell")
    private String supOrgCard;

    @ApiModelProperty(value = "开户户名", name = "supAccountName")
    private String supAccountName;

    @ApiModelProperty(value = "开户银行", name = "supAccountBank")
    private String supAccountBank;

    @ApiModelProperty(value = "银行账号", name = "supStatus")
    private String supBankAccount;

    @ApiModelProperty(value = "随货同行单（票）情况", name = "sell")
    private String supDocState;

    @ApiModelProperty(value = "企业印章情况", name = "supSealState")
    private String supSealState;

    @ApiModelProperty(value = "生产或经营范围", name = "supScope")
    private String supScope;

    @ApiModelProperty(value = "经营方式", name = "supOperationMode")
    private String supOperationMode;

    @ApiModelProperty(value = "首营日期", name = "supGspDate")
    private String supGspDate;

    @ApiModelProperty(value = "首营流程编号", name = "supFlowNo")
    private String supFlowNo;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "首营状态", name = "supGspStatus")
    private String supGspStatus;
    /**
     * 供应商分类
     */
    private String supClass;

    /**
     * 质量负责人
     */
    private String supZlfzr;

    /**
     * 业务联系人
     */
    private String supBussinessContact;

    /**
     * 送货前置期
     */
    private String supLeadTime;

    /**
     * 起订金额
     */
    private BigDecimal supMixAmt;

    /**
     * 样章
     */
    private String supSampleSeal;

    /**
     * 样章图片
     */
    private String supSampleSealImage;

    /**
     * 样票
     */
    private String supSampleTicket;

    /**
     * 样票图片
     */
    private String supSampleTicketImage;

    /**
     * 样膜
     */
    private String supSampleMembrane;

    /**
     * 样膜图片
     */
    private String supSampleMembraneImage;

    /**
     * 年度报告有效期
     */
    private String supNdbgyxq;

    /**
     * 首营日期
     */
    private String supCreateDate;

    /**
     * 购销合同
     */
    private String supGxht;

    /**
     * 购销合同有效期
     */
    private String supGxhtyxq;

    /**
     * 财务编码
     */
    private String supCwbm;
    /**
     * 发票类型 增值税专用发票、普通发票或普票
     */
    private String supFplx;
    /**
     * 发照机关
     */
    private String supFzjg;
    /**
     * 企业负责人
     */
    private String supQyfzr;
    /**
     * 注册资本
     */
    private String supZczb;
    /**
     * GSP类型 I类医疗器械 II类医疗器械
     */
    private String supGspgklx;

    /**
     * 第二类医疗器械备案凭证
     */
    private String supDrlylqxbapz;

    /**
     * 第二类医疗器械备案凭证有效期
     */
    private String supDrlylqxbapzDate;

    /**
     * 第二类医疗器械备案凭证图片
     */
    private String supDrlylqxbapzImg;

    /**
     * 采购员（供应商）
     */
    private String supCgy;
}
