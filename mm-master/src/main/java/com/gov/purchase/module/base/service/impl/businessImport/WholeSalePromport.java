package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaWholesaleChannelDetail;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductBasicMapper;
import com.gov.purchase.mapper.GaiaWholesaleChannelMapper;
import com.gov.purchase.module.base.dto.businessImport.WholesalePro;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.utils.DateUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

@Service
public class WholeSalePromport extends BusinessImport {
    @Resource
    private FeignService feignService;

    @Resource
    private GaiaProductBasicMapper gaiaProductBasicMapper;

    @Resource
    private GaiaWholesaleChannelMapper gaiaWholesaleChannelMapper;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        List<String> errorList = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        // 仓库（地点）自编码
        String dcCode = (String) field.get("dcCode");
        if (StringUtils.isBlank(dcCode)) {
            return ResultUtil.error(ResultEnum.WHOLESALE_DC_CODE_EMPTY);
        }
        // 批发渠道自编码
        String chaCode = (String) field.get("chaCode");
        if (StringUtils.isBlank(chaCode)) {
            return ResultUtil.error(ResultEnum.WHOLESALE_CHA_CODE_EMPTY);
        }
        Set<String> products = new HashSet<>();
        for (Integer key : map.keySet()) {
            WholesalePro wholesalePro = (WholesalePro) map.get(key);
            products.add(wholesalePro.getProCode());
        }
        // 查询过滤存在的商品
        List<String> proSelfCodeList = gaiaProductBasicMapper.getDistinctSiteList(user.getClient(), dcCode, new ArrayList<String>(products));
        List<GaiaWholesaleChannelDetail> wholesaleProList = new ArrayList<>();
        for (Integer key : map.keySet()) {
            WholesalePro wholesalePro = (WholesalePro) map.get(key);
            // 筛选出不存在商品
            if (!proSelfCodeList.contains(wholesalePro.getProCode())) {
                errorList.add(MessageFormat.format("第{0}行，商品编码不存在", key + 1));
                continue;
            }
            GaiaWholesaleChannelDetail gaiaWholesaleChannelDetail = new GaiaWholesaleChannelDetail();
            gaiaWholesaleChannelDetail.setClient(user.getClient());
            gaiaWholesaleChannelDetail.setGwcdDcCode(dcCode);
            gaiaWholesaleChannelDetail.setGwcdChaCode(chaCode);
            gaiaWholesaleChannelDetail.setGwcdSelfCode(wholesalePro.getProCode());
            gaiaWholesaleChannelDetail.setGwcdType("0");
            gaiaWholesaleChannelDetail.setGwcdCreDate(DateUtils.getCurrentDateStrYYMMDD());
            gaiaWholesaleChannelDetail.setGwcdCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            gaiaWholesaleChannelDetail.setGwcdCreId(user.getUserId());
            wholesaleProList.add(gaiaWholesaleChannelDetail);
        }
        if (!CollectionUtils.isEmpty(errorList)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(errorList);
            return result;
        }
        // 批量删除批发渠道商品明细
        gaiaWholesaleChannelMapper.deleteBatch(user.getClient(), dcCode, chaCode, "0", proSelfCodeList);
        // 批量插入批发渠道商品明细
        gaiaWholesaleChannelMapper.insertBatchWholeSaleChannelDetail(wholesaleProList);
        return ResultUtil.success();
    }
}
