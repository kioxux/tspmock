package com.gov.purchase.module.qa.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaBusinessScope;
import com.gov.purchase.entity.GaiaBusinessScopeKey;
import com.gov.purchase.entity.GaiaPermitData;
import com.gov.purchase.entity.GaiaPermitDataKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaPermitDataMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.qa.dto.*;
import com.gov.purchase.module.qa.service.PermitService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class PermitServiceImpl implements PermitService {

    @Resource
    private GaiaPermitDataMapper gaiaPermitDataMapper;

    @Resource
    private FeignService feignService;

    @Override
    public Result validPermitDataInfo(List<ValidPermitDataInfoRequestDto> list) {
        // 重复校验集合
        List<String> perCodeList = new ArrayList<>();
        for (ValidPermitDataInfoRequestDto validPermitDataInfoRequestDto : list) {
            // 单个实体验证
            ValidationResult result = ValidateUtil.validateEntity(validPermitDataInfoRequestDto);
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
            if (perCodeList.contains(validPermitDataInfoRequestDto.getPerCodeNew())) {
                throw new CustomResultException(ResultEnum.PERMIT_EXIST);
            }
            if (!"0".equals(validPermitDataInfoRequestDto.getType())) {
                perCodeList.add(validPermitDataInfoRequestDto.getPerCodeNew());
            }
            switch (validPermitDataInfoRequestDto.getType()) {
                case "1": //  新增 验证重复性
                    boolean flg = false;
                    for (ValidPermitDataInfoRequestDto entity : list) {
                        if (StringUtils.isNotBlank(entity.getPerCode()) &&
                                validPermitDataInfoRequestDto.getPerCodeNew().equals(entity.getPerCode()) &&
                                "0".equals(entity.getType())) {
                            flg = true;
                            continue;
                        }
                    }
                    if (!flg) {
                        GaiaPermitDataKey gaiaPermitDataKey = new GaiaPermitDataKey();
                        //加盟商
                        gaiaPermitDataKey.setClient(validPermitDataInfoRequestDto.getClient());
                        //实体类型
                        gaiaPermitDataKey.setPerEntityClass(validPermitDataInfoRequestDto.getPerEntityClass());
                        //实体编码
                        gaiaPermitDataKey.setPerEntityId(validPermitDataInfoRequestDto.getPerEntityId());
                        //证照编码
                        gaiaPermitDataKey.setPerCode(validPermitDataInfoRequestDto.getPerCodeNew());
                        GaiaPermitData permitData = gaiaPermitDataMapper.selectByPrimaryKey(gaiaPermitDataKey);
                        if (permitData != null && (StringUtils.isBlank(permitData.getPerStopFlag()) || "0".equals(permitData.getPerStopFlag()))) {
                            throw new CustomResultException(ResultEnum.PERMIT_EXIST);
                        }
                    }
                    break;
                default:   // 更新 / 删除 时 验证数据是否存在，不存在 则提示 证照不存在
//                    if (permitData == null) {
//                        throw new CustomResultException(ResultEnum.PERMIT_NOT_EXIST);
//                    }
                    break;
            }
        }
        return ResultUtil.success();
    }

    @Override
    @Transactional
    public Result savePermitDataInfo(List<ValidPermitDataInfoRequestDto> list) {
        /**
         * 1.数据验证
         * 2.加盟商 + 实体类型 + 实体编码 中是否有2个一样的证照编码 如果有则提示，证照编码已存在
         * 3.新增验证、更新/删除验证
         */
        validPermitDataInfo(list);

        /**
         * 4.根据TYPE判断是插入/更新/删除证照资质数据信息表（GAIA_PERMIT_DATA）的数据
         */
        for (ValidPermitDataInfoRequestDto validPermitDataInfoRequestDto : list) {
            savePermitDataInfo("0", validPermitDataInfoRequestDto);
        }
        for (ValidPermitDataInfoRequestDto validPermitDataInfoRequestDto : list) {
            savePermitDataInfo("1", validPermitDataInfoRequestDto);
        }
        for (ValidPermitDataInfoRequestDto validPermitDataInfoRequestDto : list) {
            savePermitDataInfo("2", validPermitDataInfoRequestDto);
        }

        return ResultUtil.success();
    }

    /**
     * 保存
     * @param type
     */
    private void savePermitDataInfo(String type, ValidPermitDataInfoRequestDto validPermitDataInfoRequestDto) {
        if (!type.equals(validPermitDataInfoRequestDto.getType())) {
            return;
        }
        TokenUser user = feignService.getLoginInfo();
        GaiaPermitDataKey gaiaPermitDataKey = new GaiaPermitDataKey();
        //加盟商
        gaiaPermitDataKey.setClient(validPermitDataInfoRequestDto.getClient());
        //实体类型
        gaiaPermitDataKey.setPerEntityClass(validPermitDataInfoRequestDto.getPerEntityClass());
        //实体编码
        gaiaPermitDataKey.setPerEntityId(validPermitDataInfoRequestDto.getPerEntityId());
        //证照编码
        gaiaPermitDataKey.setPerCode(validPermitDataInfoRequestDto.getPerCodeNew());
        GaiaPermitData gaiaPermitData = new GaiaPermitData();
        BeanUtils.copyProperties(validPermitDataInfoRequestDto, gaiaPermitData);
        gaiaPermitData.setPerCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaPermitData.setPerCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
        gaiaPermitData.setPerCreateUser(user.getUserId());
        switch (validPermitDataInfoRequestDto.getType()) {
            case "1": //  新增
                //证照编码
                gaiaPermitDataKey.setPerCode(validPermitDataInfoRequestDto.getPerCodeNew());
                gaiaPermitDataMapper.deleteByPrimaryKey(gaiaPermitDataKey);
                gaiaPermitData.setPerStopFlag("0"); // 停用
                gaiaPermitData.setPerCode(validPermitDataInfoRequestDto.getPerCodeNew());
                gaiaPermitDataMapper.insert(gaiaPermitData);
                break;
            case "2":  // 更新
                //证照编码
                if (StringUtils.isNotBlank(validPermitDataInfoRequestDto.getPerCode())) {
                    gaiaPermitDataKey.setPerCode(validPermitDataInfoRequestDto.getPerCode());
                }
                gaiaPermitDataMapper.deleteByPrimaryKey(gaiaPermitDataKey);
                gaiaPermitData.setPerChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                gaiaPermitData.setPerChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaPermitData.setPerChangeUser(user.getUserId());
                gaiaPermitData.setPerStopFlag("0"); // 停用
                gaiaPermitDataMapper.insert(gaiaPermitData);
                break;
            default:  // 删除
                if (StringUtils.isNotBlank(validPermitDataInfoRequestDto.getPerCode())) {
                    GaiaPermitData record = new GaiaPermitData();
                    //加盟商
                    record.setClient(validPermitDataInfoRequestDto.getClient());
                    //实体类型
                    record.setPerEntityClass(validPermitDataInfoRequestDto.getPerEntityClass());
                    //实体编码
                    record.setPerEntityId(validPermitDataInfoRequestDto.getPerEntityId());
                    //证照编码
                    record.setPerCode(validPermitDataInfoRequestDto.getPerCode());
                    //停用标志
                    record.setPerStopFlag("1");
                    //更新日期
                    record.setPerChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    //更新时间
                    record.setPerCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    //更新人
                    record.setPerChangeUser(user.getUserId());
                    gaiaPermitDataMapper.updateByPrimaryKeySelective(record);
                }
                break;
        }
    }

    @Override
    public PageInfo<QueryPermitDataListResponseDto> queryPermitDataList(QueryPermitDataListRequestDto dto) {
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());

        return new PageInfo<>(gaiaPermitDataMapper.queryPermitDataList(dto));
    }

    @Override
    public QueryPermitDataInfoResponseDto getPermitDataInfo(QueryPermitDataInfoRequestDto dto) {
        /**
         * 加盟商 + 实体类型 + 实体编号 查询资质数据信息表（GAIA_PERMIT_DATA）
         */
        List<GaiaPermitDataExtendDto> list = gaiaPermitDataMapper.getPermitDataInfo(dto);
        QueryPermitDataInfoResponseDto responseDto = new QueryPermitDataInfoResponseDto();

        List<PermitDetailDto> permitList = new ArrayList<>();
        PermitDetailDto detailDto = null;
        for (GaiaPermitDataExtendDto extendDto : list) {
            BeanUtils.copyProperties(extendDto, responseDto); //设置主属性
            detailDto = new PermitDetailDto();
            BeanUtils.copyProperties(extendDto, detailDto); // 设置证照属性
            permitList.add(detailDto);   // 添加到证照集合
        }
        responseDto.setPermitList(permitList); // 证照集合 添加到对象
        return responseDto;
    }

}
