package com.gov.purchase.module.priceCompare.dto;

import lombok.Data;

@Data
public class PriceComparePageDto {

    private Integer pageNum = 1;

    private Integer pageSize = 10;

    /**
     * 单号（唯一）
     */
    private String priceCompareNo;

    /**
     * 开始日期
     */
    private String startDate;

    /**
     * 结束日期
     */
    private String endDate;

    /**
     * 单据状态1：  处理中 ，2已完成  3：已失效）
     */
    private Integer status;

    /**
     * 采购员
     */
    private String purchaserName;

    private String client;

}
