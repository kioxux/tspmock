package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 明细清单表(GaiaBillOfAr)实体类
 *
 * @author makejava
 * @since 2021-09-18 15:13:11
 */
@Data
public class GaiaBillOfArVO implements Serializable {
    private static final long serialVersionUID = -38101044084295338L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 回款单号
     */
    private String billCode;
    /**
     * 仓库编码
     */
    private String dcCode;
    /**
     * 应收方式编码
     */
    private String arMethodId;
    /**
     * 发生金额
     */
    private BigDecimal arAmt;
    /**
     * 核销金额
     */
    private BigDecimal hxAmt;
    /**
     * 备注
     */
    private String remark;
    /**
     * 发生日期
     */
    private Date billDate;
    /**
     * 客户自编码
     */
    private String cusSelfCode;
    /**
     * 客户名称
     */
    private String cusName;
    /**
     * 核销状态 0-未核销 1-核销中 2-已核销
     */
    private Integer status;
    /**
     * 删除标记:0-正常 1-删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建者
     */
    private String createUser;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新者
     */
    private String updateUser;


}
