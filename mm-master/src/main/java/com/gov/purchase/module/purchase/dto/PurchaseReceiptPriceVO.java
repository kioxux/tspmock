package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaChajiaZ;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description: 采购入库差价单功能
 * @author: yzf
 * @create: 2021-12-13 10:21
 */
@Data
public class PurchaseReceiptPriceVO extends GaiaChajiaZ {

    /**
     * 地点名称
     */
    private String cjSiteName;

    /**
     * 供应商名称
     */
    private String cjSupName;

    /**
     * 业务员名称
     */
    private String cjSalesmanName;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 总金额
     */
    private BigDecimal cjTotalAmt;

    /**
     * 总数量
     */
    private BigDecimal cjTotalQty;

    /**
     * 总行数
     */
    private String cjTotalLine;


    private List<PurchaseReceiptPriceDetailVO> details;

}
