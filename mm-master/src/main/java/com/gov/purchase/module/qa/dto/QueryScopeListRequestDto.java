package com.gov.purchase.module.qa.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "经营范围列表传入参数")
public class QueryScopeListRequestDto extends Pageable {

    @ApiModelProperty(value = "加盟商编号", name = "client" )
    private String client;

    @ApiModelProperty(value = "实体类型", name = "bscEntityClass" )
    private String bscEntityClass;

    @ApiModelProperty(value = "实体编码", name = "bscEntityId" )
    private String bscEntityId;

    @ApiModelProperty(value = "实体名称", name = "bscEntityName" )
    private String bscEntityName;
}
