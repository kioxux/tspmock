package com.gov.purchase.module.store.dto.store;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors( chain =true)
@ApiModel(value = "门店详情传入参数")
public class StoreInfoDetailRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    private String client;

    @ApiModelProperty(value = "门店编号", name = "stoCode")
    private String stoCode;
}
