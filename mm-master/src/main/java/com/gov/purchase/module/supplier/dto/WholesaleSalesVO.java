package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaWholesaleSalesman;
import lombok.Data;

@Data
public class WholesaleSalesVO extends GaiaWholesaleSalesman {

    private String gwsName;
}
