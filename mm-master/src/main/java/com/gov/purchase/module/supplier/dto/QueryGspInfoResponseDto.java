package com.gov.purchase.module.supplier.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "供应商首营详情返回参数")
public class QueryGspInfoResponseDto {

    private String supCode;

    private String supSelfCode;

    private String supName;

    private String supRegisterNo;

    private String supFactoryName;

    private String supSpecs;

    private String supBarcode;

    private String supUnit;

    private String supStatus;

    private String supSite;

    private String supSiteName;

    private String client;

}
