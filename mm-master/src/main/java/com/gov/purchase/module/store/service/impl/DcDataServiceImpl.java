package com.gov.purchase.module.store.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaCompadm;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaDcDataKey;
import com.gov.purchase.entity.GaiaStoreDataKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.WmsFeign;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaCompadmMapper;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.store.constant.Constant;
import com.gov.purchase.module.store.dto.DcDataListRequestDto;
import com.gov.purchase.module.store.dto.DcDataRequestDto;
import com.gov.purchase.module.store.dto.DcDataStatusRequestDto;
import com.gov.purchase.module.store.dto.GaiaDcDataListDto;
import com.gov.purchase.module.store.service.DcDataService;
import com.gov.purchase.utils.JsonUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DcDataServiceImpl implements DcDataService {

    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    private GaiaCompadmMapper gaiaCompadmMapper;

    @Resource
    private WmsFeign wmsFeign;

    @Resource
    FeignService feignService;

    /**
     * DC 列表查询
     *
     * @param dto
     * @return
     */
    @Override
    public PageInfo<GaiaDcDataListDto> queryDcList(DcDataListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        dto.setClient(user.getClient());
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        return new PageInfo<>(gaiaDcDataMapper.queryDcList(dto));
    }

    /**
     * DC编辑初始化
     *
     * @param client 加盟商
     * @param dcCode DC编码
     * @return
     */
    @Override
    public GaiaDcData getDcInfo(String client, String dcCode) {
        GaiaDcDataKey dcDataKey = new GaiaDcDataKey();
        dcDataKey.setClient(client);
        dcDataKey.setDcCode(dcCode);
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(dcDataKey);

        //新增字段 商品配送范围
        String dcSppsfw = gaiaDcData.getDcSppsfw();
        if (ObjectUtils.isNotEmpty(dcSppsfw)) {
            String[] split = dcSppsfw.split(",");
            gaiaDcData.setDcSppsfwList(Arrays.asList(split));
        }
        return gaiaDcData;
    }

    /**
     * DC验证
     *
     * @param dto
     * @return
     */
    @Override
    public Result validDcInfo(DcDataRequestDto dto) {

        if (StringUtils.isNotBlank(dto.getDcTel())) {
            // DC电话长度不能大于20位
            if (dto.getDcTel().length() > 20) {
                throw new CustomResultException(ResultEnum.E0020);
            }
            //电话号码格式验证
            Pattern p = Pattern.compile(Constant.TEL_REGEX);
            Matcher m = p.matcher(dto.getDcTel());
            boolean isMatch = m.matches();
            if (isMatch) {
                throw new CustomResultException(ResultEnum.E0021);
            }
        }

        // 根据加盟商 + DC编码查询GAIA_DC_DATA 表
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByDcCode(dto.getClient(), dto.getDcCode());

        // 更新时验证
        if (dto.getType().equals(Constant.IS_UPDATE)) {
            if (gaiaDcData == null) { // DC编码不存在
                return ResultUtil.error(ResultEnum.DC_CODE_NOT_EXIST_ERROR);
            }
            List<GaiaCompadm> gaiaCompadmList = gaiaCompadmMapper.selectCompadmListByClienDC(dto.getClient(), dto.getDcCode());
            // 连锁总部ID
//            if (StringUtils.isNotBlank(dto.getDcChainHead())) {
//                List<String> courseIds = gaiaCompadmList.stream().map(GaiaCompadm::getCompadmId).collect(Collectors.toList());
//                if (!courseIds.contains(dto.getDcChainHead())) {
//                    return ResultUtil.error(ResultEnum.E0158);
//                }
//            }
        } else {
            // 插入时验证
            if (gaiaDcData != null) { // DC编码已存在
                return ResultUtil.error(ResultEnum.DC_CODE_EXIST_ERROR);
            }
            List<GaiaCompadm> gaiaCompadmList = gaiaCompadmMapper.selectCompadmListByClienDC(dto.getClient(), null);
//            if (StringUtils.isNotBlank(dto.getDcChainHead())) {
//                // 连锁总部ID
//                List<String> courseIds = gaiaCompadmList.stream().map(GaiaCompadm::getCompadmId).collect(Collectors.toList());
//                if (!courseIds.contains(dto.getDcChainHead())) {
//                    return ResultUtil.error(ResultEnum.E0158);
//                }
//            }
            // 查询 门店主数据信息表 （GAIA_STORE_DATA） 是否存在
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(dto.getClient());
            gaiaStoreDataKey.setStoCode(dto.getDcCode());
            int count = gaiaStoreDataMapper.selectCountByPrimaryKey(gaiaStoreDataKey);
            // 存在则 提示  DC编码已存在
            if (count > 0) {
                return ResultUtil.error(ResultEnum.DC_CODE_EXIST_ERROR);
            }

        }
        return ResultUtil.success();
    }

    /**
     * 保存DC数据
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional
    public Result saveDcInfo(DcDataRequestDto dto) {
        // 根据加盟商 + DC编码查询GAIA_DC_DATA 表
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByDcCode(dto.getClient(), dto.getDcCode());

        // 连锁
//        if (StringUtils.isNotBlank(dto.getDcChainHead())) {
//            if (StringUtils.isBlank(dto.getDcTaxSubject())) {
//                return ResultUtil.error(ResultEnum.E0170.getCode(), MessageFormat.format(ResultEnum.E0170.getMsg(), "纳税主体"));
//            }
//        }

        // 更新时验证
        if (dto.getType().equals(Constant.IS_UPDATE)) {
            if (gaiaDcData == null) { // DC编码不存在
                return ResultUtil.error(ResultEnum.DC_CODE_NOT_EXIST_ERROR);
            }
            BeanUtils.copyProperties(dto, gaiaDcData);
            // 批发公司
//            if (StringUtils.isBlank(gaiaDcData.getDcChainHead())) {
//                gaiaDcData.setDcWholesale("1");
//                gaiaDcData.setDcType("30");
//                gaiaDcData.setDcTaxSubject(null);
//            }
//            // 连锁
//            if (StringUtils.isNotBlank(gaiaDcData.getDcChainHead())) {
//                gaiaDcData.setDcWholesale("0");
//                gaiaDcData.setDcType("20");
//            }

            //处理商品范围
            List<String> dcSppsfwList = dto.getDcSppsfwList();
            if (ObjectUtils.isNotEmpty(dcSppsfwList)) {
                String joinStr = org.apache.commons.lang3.StringUtils.join(dcSppsfwList.toArray(), ",");
                gaiaDcData.setDcSppsfw(joinStr);
            }
            int count = gaiaDcDataMapper.updateByPrimaryKeySelective(gaiaDcData);
            if (count == 0) {
                throw new CustomResultException(ResultEnum.UPDATE_ERRROR);
            }
        } else {
            // 插入时验证
            if (gaiaDcData != null) { // DC编码已存在
                return ResultUtil.error(ResultEnum.DC_CODE_EXIST_ERROR);
            }
            // 查询 门店主数据信息表 （GAIA_STORE_DATA） 是否存在
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(dto.getClient());
            gaiaStoreDataKey.setStoCode(dto.getDcCode());
            int count = gaiaStoreDataMapper.selectCountByPrimaryKey(gaiaStoreDataKey);
            // 存在则 提示  DC编码已存在
            if (count > 0) {
                return ResultUtil.error(ResultEnum.DC_CODE_EXIST_ERROR);
            }
            GaiaDcData dcData = new GaiaDcData();
            BeanUtils.copyProperties(dto, dcData);
            // 批发公司
//            if (StringUtils.isBlank(dcData.getDcChainHead())) {
//                dcData.setDcWholesale("1");
//                dcData.setDcType("30");
//                dcData.setDcTaxSubject(null);
//            }
//            // 连锁
//            if (StringUtils.isNotBlank(dcData.getDcChainHead())) {
//                dcData.setDcWholesale("0");
//                dcData.setDcType("20");
//            }
            //处理商品范围
            List<String> dcSppsfwList = dto.getDcSppsfwList();
            if (ObjectUtils.isNotEmpty(dcSppsfwList)) {
                String joinStr = org.apache.commons.lang3.StringUtils.join(dcSppsfwList.toArray(), ",");
                gaiaDcData.setDcSppsfw(joinStr);
            }
            gaiaDcDataMapper.insert(dcData);
            try {
                Map<String, String> map = new HashMap<>();
                map.put("client", dto.getClient());
                map.put("site", dto.getDcCode());
                log.info("仓库初始化！ 调用参数：{}", JsonUtils.beanToJson(map));
                FeignResult feignResult = wmsFeign.baseData(map);
                log.info("仓库初始化！ 返回结果：{}", JsonUtils.beanToJson(feignResult));
                if (feignResult.getCode() != 200) {
                    log.info("仓库初始化失败");
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return ResultUtil.error(ResultEnum.E0160);
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.info("仓库初始化失败");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ResultUtil.error(ResultEnum.E0160);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 更新dc状态
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional
    public Result updateDcStatus(DcDataStatusRequestDto dto) {
        GaiaDcData dcData = new GaiaDcData();
        dcData.setClient(dto.getClient()); // 加盟商code
        dcData.setDcCode(dto.getDcCode()); // DC编码
        dcData.setDcStatus(dto.getDcStatus());  // 状态
        int count = gaiaDcDataMapper.updateByPrimaryKeySelective(dcData);
        if (count == 0) {
            throw new CustomResultException(ResultEnum.UPDATE_ERRROR);
        }
        return ResultUtil.success();
    }

    /**
     * 委托配送中心
     *
     * @param dcCode
     * @return
     */
    @Override
    public Result getEntrustDcList(String dcCode) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaDcData> list = gaiaDcDataMapper.getEntrustDcList(user.getClient(), dcCode);
        return ResultUtil.success(list);
    }

}
