package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.21
 */
@Data
public class SearchReplenish {
    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;
    /**
     * 加盟商名称
     */
    private String francName;
    /**
     * 铺货人
     */
    private String userNam;
    /**
     * 铺货门店
     */
    @ApiModelProperty(value = "铺货门店", name = "gsrhBrId", required = true)
    @NotBlank(message = "铺货门店不能为空")
    private String gsrhBrId;
    /**
     * 铺货门店名
     */
    private String stoName;
    /**
     * 铺货日期
     */
    @ApiModelProperty(value = "铺货日期", name = "gsrhDate", required = true)
    @NotBlank(message = "铺货日期不能为空")
    private String gsrhDate;
    /**
     * 铺货订单号
     */
    @ApiModelProperty(value = "铺货订单号", name = "gsrhVoucherId", required = true)
    @NotBlank(message = "铺货订单号不能为空")
    private String gsrhVoucherId;
    /**
     * 订单行
     */
    @ApiModelProperty(value = "订单行", name = "gsrdSerial", required = true)
    @NotBlank(message = "订单行不能为空")
    private String gsrdSerial;
    /**
     * 商品编码
     */
    private String gsrdProId;
    /**
     * 商品名
     */
    private String proName;
    /**
     * 铺货数量
     */
    @ApiModelProperty(value = "铺货数量", name = "gsrdNeedQty", required = true)
    @NotNull(message = "铺货数量不能为空")
    private BigDecimal gsrdNeedQty;
    /**
     * 铺货批号
     */
    private String gsrdBatchNo;
    /**
     * 铺货状态
     */
    private String gsrhStatus;
    /**
     * 仓库分配数量
     */
    private BigDecimal wmCkysl;
    /**
     * 仓库发货数量
     */
    private BigDecimal wmGzsl;
    /**
     * 是否删除
     */
    private String isDel;

    /**
     * 开单标记　0未开单，1已开单
     */
    private String gsrhDnFlag;

    /**
     * 门店请货单号
     */
    private String poReqId;
}

