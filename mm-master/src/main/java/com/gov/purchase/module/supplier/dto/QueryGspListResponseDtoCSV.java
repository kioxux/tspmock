package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.utils.csv.annotation.CsvCell;
import com.gov.purchase.utils.csv.annotation.CsvRow;
import lombok.Data;

import java.math.BigDecimal;

@Data
@CsvRow("供应商列表返回参数")
public class QueryGspListResponseDtoCSV {

    /**
     * client
     */
//    @CsvCell(title = "client", index = 1, fieldNo = 1)
    private String client;

    /**
     * 供应商自编码
     */
    @CsvCell(title = "供应商自编码", index = 1, fieldNo = 1)
    private String supSelfCode;

    /**
     * 地点
     */
    @CsvCell(title = "地点", index = 1, fieldNo = 1)
    private String supSite;

    /**
     * sup_code
     */
//    @CsvCell(title = "sup_code", index = 1, fieldNo = 1)
    private String supCode;

    /**
     * 供应商名称
     */
    @CsvCell(title = "供应商名称", index = 1, fieldNo = 1)
    private String supName;

    /**
     * 营业执照编号
     */
    @CsvCell(title = "营业执照编号", index = 1, fieldNo = 1)
    private String supCreditCode;

    /**
     * 营业期限
     */
    @CsvCell(title = "营业期限", index = 1, fieldNo = 1)
    private String supCreditDate;

    /**
     * 法人
     */
    @CsvCell(title = "法人", index = 1, fieldNo = 1)
    private String supLegalPerson;

    /**
     * 注册地址
     */
    @CsvCell(title = "注册地址", index = 1, fieldNo = 1)
    private String supRegAdd;

    /**
     * 许可证编号
     */
    @CsvCell(title = "许可证编号", index = 1, fieldNo = 1)
    private String supLicenceNo;

    /**
     * 发证单位
     */
    @CsvCell(title = "发证单位", index = 1, fieldNo = 1)
    private String supLicenceOrgan;

    /**
     * 发证日期
     */
    @CsvCell(title = "发证日期", index = 1, fieldNo = 1)
    private String supLicenceDate;

    /**
     * 有效期至
     */
    @CsvCell(title = "有效期至", index = 1, fieldNo = 1)
    private String supLicenceValid;

    /**
     * 邮政编码
     */
    @CsvCell(title = "邮政编码", index = 1, fieldNo = 1)
    private String supPostalCode;

    /**
     * 税务登记证
     */
    @CsvCell(title = "税务登记证", index = 1, fieldNo = 1)
    private String supTaxCard;

    /**
     * 组织机构代码证
     */
    @CsvCell(title = "组织机构代码证", index = 1, fieldNo = 1)
    private String supOrgCard;

    /**
     * 开户户名
     */
    @CsvCell(title = "开户户名", index = 1, fieldNo = 1)
    private String supAccountName;

    /**
     * 开户银行
     */
    @CsvCell(title = "开户银行", index = 1, fieldNo = 1)
    private String supAccountBank;

    /**
     * 银行账号
     */
    @CsvCell(title = "银行账号", index = 1, fieldNo = 1)
    private String supBankAccount;

    /**
     * 随货同行单（票）情况
     */
    @CsvCell(title = "随货同行单（票）情况", index = 1, fieldNo = 1)
    private String supDocState;

    /**
     * 企业印章情况
     */
    @CsvCell(title = "企业印章情况", index = 1, fieldNo = 1)
    private String supSealState;

    /**
     * 生产或经营范围
     */
    @CsvCell(title = "生产或经营范围", index = 1, fieldNo = 1)
    private String supScope;

    /**
     * 经营方式
     */
    @CsvCell(title = "经营方式", index = 1, fieldNo = 1)
    private String supOperationMode;

    /**
     * 首营日期
     */
    @CsvCell(title = "首营日期", index = 1, fieldNo = 1)
    private String supGspDate;

    /**
     * 首营流程编号
     */
    @CsvCell(title = "首营流程编号", index = 1, fieldNo = 1)
    private String supFlowNo;

    /**
     * sup_gsp_status
     */
//    @CsvCell(title = "sup_gsp_status", index = 1, fieldNo = 1)
    private String supGspStatus;

    /**
     * 供应商分类
     */
    @CsvCell(title = "供应商分类", index = 1, fieldNo = 1)
    private String supClass;

    /**
     * 质量负责人
     */
    @CsvCell(title = "质量负责人", index = 1, fieldNo = 1)
    private String supZlfzr;

    /**
     * 业务联系人
     */
    @CsvCell(title = "业务联系人", index = 1, fieldNo = 1)
    private String supBussinessContact;

    /**
     * 送货前置期
     */
    @CsvCell(title = "送货前置期", index = 1, fieldNo = 1)
    private String supLeadTime;

    /**
     * 起订金额
     */
    @CsvCell(title = "起订金额", index = 1, fieldNo = 1)
    private BigDecimal supMixAmt;

    /**
     * 匹配状态
     */
//    @CsvCell(title = "匹配状态", index = 1, fieldNo = 1)
    private String supMatchStatus;

    /**
     * 助记码
     */
    @CsvCell(title = "助记码", index = 1, fieldNo = 1)
    private String supPym;

    /**
     * 供应商状态
     */
    @CsvCell(title = "供应商状态", index = 1, fieldNo = 1)
    private String supStatus;

    /**
     * 禁止采购
     */
    @CsvCell(title = "禁止采购", index = 1, fieldNo = 1)
    private String supNoPurchase;

    /**
     * 禁止退厂
     */
    @CsvCell(title = "禁止退厂", index = 1, fieldNo = 1)
    private String supNoSupplier;

    /**
     * 付款条件
     */
    @CsvCell(title = "付款条件", index = 1, fieldNo = 1)
    private String supPayTerm;

    /**
     * 联系人电话
     */
    @CsvCell(title = "联系人电话", index = 1, fieldNo = 1)
    private String supContactTel;

    /**
     * 银行代码
     */
    @CsvCell(title = "银行代码", index = 1, fieldNo = 1)
    private String supBankCode;

    /**
     * 银行名称
     */
    @CsvCell(title = "银行名称", index = 1, fieldNo = 1)
    private String supBankName;

    /**
     * 账户持有人
     */
    @CsvCell(title = "账户持有人", index = 1, fieldNo = 1)
    private String supAccountPerson;

    /**
     * 支付方式
     */
    @CsvCell(title = "支付方式", index = 1, fieldNo = 1)
    private String supPayMode;

    /**
     * 铺底授信额度
     */
    @CsvCell(title = "铺底授信额度", index = 1, fieldNo = 1)
    private String supCreditAmt;

    /**
     * 法人委托书r
     */
    @CsvCell(title = "法人委托书r", index = 1, fieldNo = 1)
    private String supFrwts;

    /**
     * 法人委托书效期r
     */
    @CsvCell(title = "法人委托书效期r", index = 1, fieldNo = 1)
    private String supFrwtsxq;

    /**
     * 仓库地址r
     */
    @CsvCell(title = "仓库地址r", index = 1, fieldNo = 1)
    private String supCkdz;

    /**
     * 网页登陆地址r
     */
//    @CsvCell(title = "网页登陆地址r", index = 1, fieldNo = 1)
    private String supLoginAdd;

    /**
     * 登陆用户名r
     */
//    @CsvCell(title = "登陆用户名r", index = 1, fieldNo = 1)
    private String supLoginUser;

    /**
     * 登陆密码r
     */
//    @CsvCell(title = "登陆密码r", index = 1, fieldNo = 1)
    private String supLoginPassword;

    /**
     * 系统参数r
     */
//    @CsvCell(title = "系统参数r", index = 1, fieldNo = 1)
    private String supSysParm;

    /**
     * 法人委托书图片
     */
//    @CsvCell(title = "法人委托书图片", index = 1, fieldNo = 1)
    private String supPictureFrwts;

    /**
     * gsp证书图片
     */
//    @CsvCell(title = "gsp证书图片", index = 1, fieldNo = 1)
    private String supPictureGsp;

    /**
     * gsp证书
     */
    @CsvCell(title = "gsp证书", index = 1, fieldNo = 1)
    private String supGsp;

    /**
     * gmp证书图片
     */
//    @CsvCell(title = "gmp证书图片", index = 1, fieldNo = 1)
    private String supPictureGmp;

    /**
     * gmp证书
     */
    @CsvCell(title = "gmp证书", index = 1, fieldNo = 1)
    private String supGmp;

    /**
     * gmp有效期
     */
    @CsvCell(title = "gmp有效期", index = 1, fieldNo = 1)
    private String supGmpDate;

    /**
     * 随货同行单图片
     */
//    @CsvCell(title = "随货同行单图片", index = 1, fieldNo = 1)
    private String supPictureShtxd;

    /**
     * 其他图片
     */
//    @CsvCell(title = "其他图片", index = 1, fieldNo = 1)
    private String supPictureQt;

    /**
     * 配送方式
     */
    @CsvCell(title = "配送方式", index = 1, fieldNo = 1)
    private String supDeliveryMode;

    /**
     * 承运单位
     */
    @CsvCell(title = "承运单位", index = 1, fieldNo = 1)
    private String supCarrier;

    /**
     * 运输时间
     */
    @CsvCell(title = "运输时间", index = 1, fieldNo = 1)
    private String supDeliveryTimes;

    /**
     * 质量保证协议
     */
    @CsvCell(title = "质量保证协议", index = 1, fieldNo = 1)
    private String supQuality;

    /**
     * 质保协议有效期
     */
    @CsvCell(title = "质保协议有效期", index = 1, fieldNo = 1)
    private String supQualityDate;

    /**
     * 生产许可证书图片
     */
//    @CsvCell(title = "生产许可证书图片", index = 1, fieldNo = 1)
    private String supPictureScxkz;

    /**
     * 生产许可证书
     */
    @CsvCell(title = "生产许可证书", index = 1, fieldNo = 1)
    private String supScxkz;

    /**
     * 生产许可证有效期
     */
    @CsvCell(title = "生产许可证有效期", index = 1, fieldNo = 1)
    private String supScxkzDate;

    /**
     * 食品许可证书图片
     */
//    @CsvCell(title = "食品许可证书图片", index = 1, fieldNo = 1)
    private String supPictureSpxkz;

    /**
     * 食品许可证书
     */
    @CsvCell(title = "食品许可证书", index = 1, fieldNo = 1)
    private String supSpxkz;

    /**
     * 食品许可证有效期
     */
    @CsvCell(title = "食品许可证有效期", index = 1, fieldNo = 1)
    private String supSpxkzDate;

    /**
     * 医疗器械经营许可证图片
     */
//    @CsvCell(title = "医疗器械经营许可证图片", index = 1, fieldNo = 1)
    private String supPictureYlqxxkz;

    /**
     * 医疗器械经营许可证书
     */
    @CsvCell(title = "医疗器械经营许可证书", index = 1, fieldNo = 1)
    private String supYlqxxkz;

    /**
     * 医疗器械经营许可证在有效期
     */
    @CsvCell(title = "医疗器械经营许可证在有效期", index = 1, fieldNo = 1)
    private String supYlqxxkzDate;

    /**
     * 法人委托书身份证
     */
    @CsvCell(title = "法人委托书身份证", index = 1, fieldNo = 1)
    private String supFrwtsSfz;

    /**
     * 在途关闭天数
     */
    @CsvCell(title = "在途关闭天数", index = 1, fieldNo = 1)
    private String supCloseDate;

    /**
     * gsp有效期
     */
    @CsvCell(title = "gsp有效期", index = 1, fieldNo = 1)
    private String supGspDateValidity;

    /**
     * 样章
     */
    @CsvCell(title = "样章", index = 1, fieldNo = 1)
    private String supSampleSeal;

    /**
     * 样章图片
     */
//    @CsvCell(title = "样章图片", index = 1, fieldNo = 1)
    private String supSampleSealImage;

    /**
     * 样票
     */
    @CsvCell(title = "样票", index = 1, fieldNo = 1)
    private String supSampleTicket;

    /**
     * 样票图片
     */
//    @CsvCell(title = "样票图片", index = 1, fieldNo = 1)
    private String supSampleTicketImage;

    /**
     * 样膜
     */
    @CsvCell(title = "样膜", index = 1, fieldNo = 1)
    private String supSampleMembrane;

    /**
     * 样膜图片
     */
//    @CsvCell(title = "样膜图片", index = 1, fieldNo = 1)
    private String supSampleMembraneImage;

    /**
     * 档案号
     */
    @CsvCell(title = "档案号", index = 1, fieldNo = 1)
    private String supDah;

    /**
     * 备注
     */
    @CsvCell(title = "备注", index = 1, fieldNo = 1)
    private String supBeizhu;

    /**
     * 年度报告有效期
     */
    @CsvCell(title = "年度报告有效期", index = 1, fieldNo = 1)
    private String supNdbgyxq;

    /**
     * 首营日期
     */
    @CsvCell(title = "首营日期", index = 1, fieldNo = 1)
    private String supCreateDate;

}
