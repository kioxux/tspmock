package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * @author tl
 */
@Data
@ApiModel(value = "货源清单请求参数")
public class SourceRequestNewDTO {

    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "商品编码", name = "souProCode", required = true)
    @NotBlank(message = "商品编码不能为空")
    private String souProCode;

    @ApiModelProperty(value = "地点", name = "souSiteCode", required = true)
    @NotBlank(message = "地点不能为空")
    private String souSiteCode;

    @ApiModelProperty(value = "供应商", name = "souSupplierId", required = true)
    @NotBlank(message = "供应商不能为空")
    private String souSupplierId;

    @ApiModelProperty(value = "有效期从", name = "souEffectFrom", required = true)
    @NotBlank(message = "有效期从不能为空")
    private String souEffectFrom;

    @ApiModelProperty(value = "有效期至", name = "souEffectEnd", required = true)
    @NotBlank(message = "有效期至不能为空")
    private String souEffectEnd;

    @ApiModelProperty(value = "是否主供应商", name = "souMainSupplier", required = true)
    @NotBlank(message = "是否主供应商不能为空")
    private String souMainSupplier;

    @ApiModelProperty(value = "是否锁定供应商", name = "souLockSupplier", required = true)
    @NotBlank(message = "是否锁定供应商不能为空")
    private String souLockSupplier;

    @ApiModelProperty(value = "冻结标志", name = "souDeleteFlag", required = true)
    @NotBlank(message = "冻结标志不能为空")
    private String souDeleteFlag;

}
