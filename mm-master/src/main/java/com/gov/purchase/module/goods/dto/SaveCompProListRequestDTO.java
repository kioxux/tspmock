package com.gov.purchase.module.goods.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class SaveCompProListRequestDTO {
    /**
     * 地点/机构
     */
    private String proSite;
    /**
     * 国际条形码
     */
    private String proBarcode;
    /**
     * 商品编码
     */
    private String proCode;
    /**
     * 商品名
     */
    private String proName;
    /**
     * 批准文号
     */
    private String proRegisterNo;
    /**
     * 生产企业代码
     */
    private String proFactoryCode;
    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 操作 1:忽略 2:新增
     */
    private Integer operation;
}
