package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "证照资质明细输出参数")
public class QueryPermitDataInfoResponseDto {

    private String client;

    private String francName;

    private String perEntityClass;

    private String perEntityId;

    private String perEntityName;

    private List<PermitDetailDto> permitList;


}
