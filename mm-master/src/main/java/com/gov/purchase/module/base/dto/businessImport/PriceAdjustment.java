package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class PriceAdjustment {

    /**
     * 门店号
     */
    @ExcelValidate(index = 0, name = "门店号", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String prcStore;
    /**
     * 商品编码
     */
    @ExcelValidate(index = 1, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String prcProduct;
    /**
     * 单价
     */
    @ExcelValidate(index = 2, name = "单价", type = ExcelValidate.DataType.DECIMAL)
    private String proAmount;
//    /**
//     * 生效日期
//     */
//    @ExcelValidate(index = 3, name = "生效日期", type = ExcelValidate.DataType.DATE)
//    private String prcEffectDate;
//    /**
//     * 不允许积分
//     */
//    @ExcelValidate(index = 5, name = "不允许积分", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
//            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES)
//    private String prcNoIntegral;
//    /**
//     * 不允许会员卡打折
//     */
//    @ExcelValidate(index = 6, name = "不允许会员卡打折", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
//            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES)
//    private String prcNoDiscount;
//    /**
//     * 不允许积分兑换
//     */
//    @ExcelValidate(index = 7, name = "不允许积分兑换", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
//            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES)
//    private String prcNoExchange;
//    /**
//     * 限购数量
//     */
//    @ExcelValidate(index = 8, name = "限购数量", type = ExcelValidate.DataType.INTEGER, min = 0, max = 9999999999.0, addRequired = false)
//    private String prcLimitAmount;
}

