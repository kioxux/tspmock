package com.gov.purchase.module.delivery.service.impl;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaMaterialAssess;
import com.gov.purchase.entity.GaiaMaterialAssessKey;
import com.gov.purchase.entity.GaiaRecallInfo;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaMaterialAssessMapper;
import com.gov.purchase.mapper.GaiaRecallInfoMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.delivery.dto.GoodsRecallListRequestDto;
import com.gov.purchase.module.delivery.dto.GoodsRecallListResponseDto;
import com.gov.purchase.module.delivery.dto.SaveGoodsRecallRequestDto;
import com.gov.purchase.module.delivery.dto.StockNumRequestDto;
import com.gov.purchase.module.delivery.service.GoodsRecallService;
import com.gov.purchase.module.purchase.dto.GetProductListResponseDto;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@Service
public class GoodsRecallServiceImpl implements GoodsRecallService {

    @Resource
    GaiaMaterialAssessMapper gaiaMaterialAssessMapper;

    @Resource
    GaiaRecallInfoMapper gaiaRecallInfoMapper;

    @Resource
    FeignService feignService;

    /**
     * 门店库存获取
     *
     * @param dto StockNumRequestDto
     * @return BigDecimal
     */
    public BigDecimal selectStockNum(StockNumRequestDto dto) {
        // 库存
        GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
        // 加盟商
        gaiaMaterialAssessKey.setClient(dto.getClient());
        // 商品编码
        gaiaMaterialAssessKey.setMatProCode(dto.getProCode());
        // 估价地点
        gaiaMaterialAssessKey.setMatAssessSite(dto.getSiteCode());
        GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
        if (gaiaMaterialAssess != null) {
            return gaiaMaterialAssess.getMatTotalQty() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatTotalQty();
        }
        return BigDecimal.ZERO;
    }

    /**
     * 商品召回提交
     *
     * @param dto List<SaveGoodsRecallRequestDto>
     * @return StockNumResponseDto
     */
    public void insertSaveGoodsRecall(List<SaveGoodsRecallRequestDto> dto) {
        TokenUser user = feignService.getLoginInfo();
        //商品召回提交
        int index = 0;
        //取得既有最大召回单号和行号
        String recNumber = gaiaRecallInfoMapper.selectNo();
        for (SaveGoodsRecallRequestDto saveGoodsRecallRequestDto : dto) {
            GaiaRecallInfo gaiaRecallInfo = new GaiaRecallInfo();
            BeanUtils.copyProperties(saveGoodsRecallRequestDto, gaiaRecallInfo);
            gaiaRecallInfo.setRecCreateBy(user.getUserId());
            gaiaRecallInfo.setRecCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            gaiaRecallInfo.setRecCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            //数量
            if (StringUtils.isNotBlank(saveGoodsRecallRequestDto.getRecQty())) {
                gaiaRecallInfo.setRecQty(new BigDecimal(saveGoodsRecallRequestDto.getRecQty()));
            }
            gaiaRecallInfo.setRecNumber(recNumber);
            gaiaRecallInfo.setRecLineno(String.valueOf(index + 1));
            index++;
            GaiaRecallInfo gaiaRecall = gaiaRecallInfoMapper.selectByPrimaryKey(gaiaRecallInfo);

            if (null != gaiaRecall) {
                throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
            }
            gaiaRecallInfoMapper.insertSelective(gaiaRecallInfo);
        }
    }

    /**
     * 商品召回查看列表
     *
     * @param dto GoodsRecallListRequestDto
     * @return PageInfo<GoodsRecallListResponseDto>
     */
    public PageInfo<GoodsRecallListResponseDto> selectGoodsRecallList(GoodsRecallListRequestDto dto) {

        //商品召回查看列表
        List<GoodsRecallListResponseDto> goodsRecallList = gaiaRecallInfoMapper.selectGoodsRecallList(dto);
        PageInfo<GoodsRecallListResponseDto> pageInfo = new PageInfo<>(goodsRecallList);
        return pageInfo;
    }

    /**
     * @param client 加盟商
     * @param chain  连锁总部
     * @return
     */
    @Override
    public Result getProductListForRecall(String client, String chain) {
        List<GetProductListResponseDto> list = gaiaRecallInfoMapper.getProductListForRecall(client, chain, null);
        return ResultUtil.success(list);
    }
}
