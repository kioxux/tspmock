package com.gov.purchase.module.store.dto.price;

import com.gov.purchase.common.excel.annotation.ExcelField;
import com.gov.purchase.constants.CommonEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PriceListByGoodsDto {

    @ExcelField(title = "门店名称", type = 1, sort = 0)
    private String prcStoreName;

    @ExcelField(title = "商品编号", type = 1, sort = 1)
    private String proSelfCode;

    @ExcelField(title = "商品描述", type = 1, sort = 2)
    private String proDepict;

    @ExcelField(title = "规格", type = 1, sort = 3)
    private String proSpecs;

    @ExcelField(title = "会员价", type = 1, sort = 4)
    private BigDecimal memberAmount;

    @ExcelField(title = "零售价", type = 1, sort = 5)
    private BigDecimal retailAmount;

    @ExcelField(title = "医保价", type = 1, sort = 6)
    private BigDecimal ybAmount;

    @ExcelField(title = "拆零价", type = 1, sort = 7)
    private BigDecimal clAmount;

    @ExcelField(title = "会员日价", type = 1, sort = 8)
    private BigDecimal memberDayAmount;

    @ExcelField(title = "网上零售价", type = 1, sort = 9)
    private BigDecimal retailOnlineAmount;

    @ExcelField(title = "网上会员价", type = 1, sort = 10)
    private BigDecimal memberOnlineAmount;

    @ExcelField(title = "单位", type = 1, sort = 11)
    private String unitName;

    @ExcelField(title = "不允许积分", type = 1, sort = 12, dictType = CommonEnum.DictionaryStaticData.NO_YES)
    private String prcNoIntegral;

    @ExcelField(title = "不允许积分兑换", type = 1, sort = 13, dictType = CommonEnum.DictionaryStaticData.NO_YES)
    private String prcNoDiscount;

    @ExcelField(title = "不允许会员卡打折", type = 1, sort = 14, dictType = CommonEnum.DictionaryStaticData.NO_YES)
    private String prcNoExchange;


}
