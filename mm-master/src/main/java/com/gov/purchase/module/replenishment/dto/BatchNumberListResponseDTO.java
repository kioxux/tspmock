package com.gov.purchase.module.replenishment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class BatchNumberListResponseDTO {
    /**
     * 生产批号
     */
    private String batBatchNo;
    /**
     * 有效期至
     */
    private String batExpiryDate;
    /**
     * 库存数量
     */
    private String wmKcsl;
}
