package com.gov.purchase.module.store.dto;

import com.gov.purchase.entity.GaiaRetailPrice;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class GetStoreProductPriceListDTO extends GaiaRetailPrice {

    /**
     * 门店名
     */
    private String stoName;

    /**
     * 商品通用名
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

}
