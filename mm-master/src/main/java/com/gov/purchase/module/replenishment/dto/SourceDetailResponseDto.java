package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class SourceDetailResponseDto {

    /**
     * 加盟商编号
     */
    private String client;

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 地点
     */
    private String souSiteCode;
    /**
     * 地点名称
     */
    private String siteName;

    /**
     * 商品自编码
     */
    private String souProCode;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 商品描述
     */
    private String proDepict;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 供应商明细
     */
    private List<SourceCreateListResponseDto> supplierList;
}
