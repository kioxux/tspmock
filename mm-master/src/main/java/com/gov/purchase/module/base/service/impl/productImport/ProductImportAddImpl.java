package com.gov.purchase.module.base.service.impl.productImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBasic;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductBusinessKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaProductBasicMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.DictionaryParams;
import com.gov.purchase.module.base.dto.excel.Product;
import com.gov.purchase.module.base.service.impl.ImportData;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.30
 */
@Service
public class ProductImportAddImpl extends ProductImport {

    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;

    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;

    /**
     * 业务验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap) {
        return super.errorListProduct((LinkedHashMap<Integer, Product>) dataMap);
    }

    /**
     * 主数据表验证
     *
     * @param product
     * @param errorList
     * @param key
     */
    @Override
    protected void baseCheck(Product product, List<String> errorList, int key) {

        // 药品、器械：商品分类开头为1、2、3、5
        List<String> proClassList = new ArrayList<String>() {{
            add("1");
            add("2");
            add("3");
            add("5");
        }};

        // 细分剂型
        if (super.isNotBlank(product.getProPartform())) {
            if (StringUtils.isBlank(product.getProForm())) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "剂型"));
            } else {
                // 参数
                DictionaryParams dictionaryParams = new DictionaryParams();
                // key
                dictionaryParams.setKey(CommonEnum.DictionaryData.PRO_PARTFORM.getCode());
                // 参数
                Map<String, String> map = new HashMap<>();
                map.put("PRO_FORM", product.getProForm());
                dictionaryParams.setExactParams(map);
                Result result = dictionaryService.getDictionary(dictionaryParams);
                if (result.getData() == null || CollectionUtils.isEmpty((Collection<?>) result.getData())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "细分剂型"));
                } else {
                    Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue((List<Dictionary>) result.getData(), product.getProPartform());
                    if (dictionary == null) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "细分剂型"));
                    }
                }
            }
        }

        // 药品、器械：商品分类开头为1、2、3、5
        // 保质期 保质期单位 不为空
        if (StringUtils.isNotBlank(product.getProClass())) {
            // 遍历是否包含
            for (String proClass : proClassList) {
                if (product.getProClass().startsWith(proClass)) {
                    // 批准文号
                    if (!super.isNotBlank(product.getProRegisterNo())) {
                        errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "批准文号"));
                    }
                    // 批准文号批准日期
                    if (!super.isNotBlank(product.getProRegisterDate())) {
                        errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "批准文号批准日期"));
                    }
                    // 保质期
                    if (!super.isNotBlank(product.getProLife())) {
                        errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "保质期"));
                    }
                }
            }

            // 中包装量 中药饮片必填
            // 中药饮片：商品分类开头为3
            if (product.getProClass().startsWith("3")) {
                if (!super.isNotBlank(product.getProMidPackage())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中包装量"));
                }
            }
        }
    }

    /**
     * 业务数据验证
     *
     * @param product
     * @param errorList
     * @param key
     */
    @Override
    protected void businessCheck(Product product, List<String> errorList, int key) {
        // 加盟商 商品自编码 存在验证
        List<GaiaProductBusiness> businessList = gaiaProductBusinessMapper.selfOnlyCheck(product.getClient(), product.getProSelfCode(), product.getProSite());
        if (CollectionUtils.isNotEmpty(businessList)) {
            errorList.add(MessageFormat.format(ResultEnum.E0118.getMsg(), key + 1));
        }

        // 商品分类开头为3
        // 中药规格 中药批准文号 中药生产企业代码 中药产地 不为空
        if (StringUtils.isNotBlank(product.getProClass())) {
            if (product.getProClass().startsWith("3")) {
                // 中药规格
                if (!super.isNotBlank(product.getProTcmSpecs())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药规格"));
                }
                // 中药批准文号
                if (!super.isNotBlank(product.getProTcmRegisterNo())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药批准文号"));
                }
                // 中药生产企业代码
                if (!super.isNotBlank(product.getProTcmFactoryCode())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药生产企业代码"));
                }
                // 中药产地
                if (!super.isNotBlank(product.getProTcmPlace())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药产地"));
                }
            }
        }
    }

    /**
     * 数据验证完成
     *
     * @return
     */
    @Override
    protected void checkEnd(String path) {
        // 生成导入日志
        createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.WAIT.getCode());
    }

    /**
     * 数据导入完成
     *
     * @param bulUpdateCode
     * @param map
     * @param <T>
     */
    @Override
    @Transactional
    protected <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> map) {
        // 批量导入
        try {
            for (Integer key : map.keySet()) {
                // 行数据
                //ProductBasic表
                Product product = (Product) map.get(key);
                GaiaProductBasic gaiaProductBasic = new GaiaProductBasic();
                BeanUtils.copyProperties(product, gaiaProductBasic);
                //商品状态
//                gaiaProductBasic.setProStatus(product.getProStatusValue());
                // 保质期单位
                gaiaProductBasic.setProLifeUnit(product.getProLifeUnitValue());
                // 启用电子监管码
                gaiaProductBasic.setProElectronicCode(product.getProElectronicCodeValue());
                // 国家医保品种
                gaiaProductBasic.setProMedProdct(product.getProMedProdctValue());
                // 状态
                gaiaProductBasic.setProStatus("0");
                // 清空数据处理
                super.initNull(gaiaProductBasic);
                //商品编码生成
                String proCode = gaiaProductBasicMapper.getProCode();
                gaiaProductBasic.setProCode(proCode);
                gaiaProductBasicMapper.insertSelective(gaiaProductBasic);

                // 业务表数据存在判断
                if (!super.isNotBlank(product.getClient())) {
                    continue;
                }

                //ProductBusiness表
                if (!StringUtils.isBlank(product.getClient())) {
                    GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
                    BeanUtils.copyProperties(product, gaiaProductBusiness);
                    // 商品状态
                    gaiaProductBusiness.setProStatus(product.getProStatusValue());
                    // 禁止销售
                    gaiaProductBusiness.setProNoRetail(product.getProNoRetailValue());
                    // 禁止采购
                    gaiaProductBusiness.setProNoPurchase(product.getProNoPurchaseValue());
                    // 禁止配送
                    gaiaProductBusiness.setProNoDistributed(product.getProNoDistributedValue());
                    // 禁止退厂
                    gaiaProductBusiness.setProNoSupplier(product.getProNoSupplierValue());
                    // 禁止退仓
                    gaiaProductBusiness.setProNoDc(product.getProNoDcValue());
                    // 禁止调剂
                    gaiaProductBusiness.setProNoAdjust(product.getProNoAdjustValue());
                    // 禁止批发
                    gaiaProductBusiness.setProNoSale(product.getProNoSaleValue());
                    // 禁止请货
                    gaiaProductBusiness.setProNoApply(product.getProNoApplyValue());
                    // 是否拆零
                    gaiaProductBusiness.setProIfpart(product.getProIfpartValue());
                    // 是否医保
                    gaiaProductBusiness.setProIfMed(product.getProIfMedValue());
                    // 按中包装量倍数配货
                    gaiaProductBusiness.setProPackageFlag(product.getProPackageFlagValue());
                    // 是否重点养护
                    gaiaProductBusiness.setProKeyCare(product.getProKeyCareValue());

                    // 清空数据处理
                    super.initNull(gaiaProductBusiness);

                    //限购数量
                    if (!ImportData.NULLVALUE.equals(product.getProLimitQty()) && !StringUtils.isBlank(product.getProLimitQty())) {
                        gaiaProductBusiness.setProLimitQty(new BigDecimal(product.getProLimitQty()));
                    }
                    //最小订货量
                    if (!ImportData.NULLVALUE.equals(product.getProMinQty()) && !StringUtils.isBlank(product.getProMinQty())) {
                        gaiaProductBusiness.setProMinQty(new BigDecimal(product.getProMinQty()));
                    }
                    //含麻最大配货量
                    if (!ImportData.NULLVALUE.equals(product.getProMaxQty()) && !StringUtils.isBlank(product.getProMaxQty())) {
                        gaiaProductBusiness.setProMaxQty(new BigDecimal(product.getProMaxQty()));
                    }
                    gaiaProductBusiness.setProCode(proCode);
                    // 固定货位
                    if (StringUtils.isBlank(gaiaProductBusiness.getProFixBin())) {
                        gaiaProductBusiness.setProFixBin(gaiaProductBusiness.getProSelfCode());
                    }
                    gaiaProductBusiness.setProMatchStatus(CommonEnum.MatchStatus.EXACTMATCH.getCode());
                    gaiaProductBusiness.setProCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    gaiaProductBusiness.setProStatus(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
                    if (StringUtils.isBlank(gaiaProductBusiness.getProStorageCondition())) {
                        gaiaProductBusiness.setProStorageCondition("1");
                    }
                    if (StringUtils.isBlank(gaiaProductBusiness.getProCompclass())) {
                        gaiaProductBusiness.setProCompclass("N999999");
                        gaiaProductBusiness.setProCompclassName("N999999未匹配");
                    }
                    gaiaProductBusinessMapper.insertSelective(gaiaProductBusiness);
                }
            }
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

        // 更新导入日志
        editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.COMPLETE.getCode());
        return ResultUtil.success();
    }
}

