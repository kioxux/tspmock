package com.gov.purchase.module.goods.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.ListUtils;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.BaseService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.goods.dto.*;
import com.gov.purchase.module.goods.service.GoodsService;
import com.gov.purchase.module.purchase.dto.SupplierListForReturnsRequestDto;
import com.gov.purchase.module.purchase.dto.SupplierListForReturnsResponseDto;
import com.gov.purchase.utils.*;
import com.gov.purchase.utils.csv.CsvClient;
import com.gov.purchase.utils.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class GoodsServiceImpl implements GoodsService {

    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    GaiaProductGspinfoMapper gaiaProductGspinfoMapper;
    @Resource
    GaiaWorkflowLogMapper gaiaWorkflowLogMapper;
    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    FeignService feignService;
    @Resource
    GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;
    @Resource
    GaiaProductGspinfoLogMapper gaiaProductGspinfoLogMapper;
    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    GaiaTaxCodeMapper gaiaTaxCodeMapper;
    @Resource
    GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Resource
    OperationFeignService operationFeignService;
    @Resource
    BaseService baseService;
    @Resource
    GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Autowired
    private CosUtils cosUtils;

    /**
     * 药品库搜索
     *
     * @param dto QueryProductBasicRequestDto
     * @return PageInfo<QueryProductBasicResponseDto>
     */
    public PageInfo<QueryProductBasicResponseDto> selectGoodsInfo(QueryProductBasicRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //分页查询
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        //加盟商
        dto.setClient(user.getClient());
        List<QueryProductBasicResponseDto> GoodsResponseList;
        if (StringUtils.isBlank(dto.getProSite())) {
            //地点是空的情况下，只查询GAIA_PRODUCT_BASIC
            GoodsResponseList = gaiaProductBasicMapper.selectProductBasicInfo(dto);
        } else {
            GoodsResponseList = gaiaProductBasicMapper.selectGoodsInfo(dto);
        }
        PageInfo<QueryProductBasicResponseDto> pageInfo = new PageInfo<>(GoodsResponseList);
        return pageInfo;
    }

    /**
     * 首营提交
     *
     * @param dto GspinfoSubmitRequestDto
     * @return int
     */
    @Transactional
    public Map<String, Object> insertGspinfoSubmitInfo(GaiaProductGspinfo dto) {
        Map<String, Object> resultMap = new HashMap<>();
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        dto.setClient(user.getClient());
        // 商品自编码重复验证
        // 同一加盟商门店自编码不可以重复
        {
            List<GaiaProductGspinfo> gspinfos = gaiaProductGspinfoMapper.selfOnlyCheck(dto.getClient(), dto.getProSelfCode(), dto.getProSite());
            if (CollectionUtils.isNotEmpty(gspinfos)) {
                throw new CustomResultException(ResultEnum.SELFCODE_ERROR);
            }
            List<GaiaProductBusiness> businesses = gaiaProductBusinessMapper.selfOnlyCheck(dto.getClient(), dto.getProSelfCode(), dto.getProSite());
            if (CollectionUtils.isNotEmpty(businesses)) {
                throw new CustomResultException(ResultEnum.SELFCODE_ERROR);
            }
        }
        // 停用商品判断
        GaiaProductBasic gaiaProductBasic = gaiaProductBasicMapper.selectByPrimaryKey(dto.getProCode());
        if (CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(gaiaProductBasic.getProStatus())) {
            throw new CustomResultException(ResultEnum.DEACTIVATE_ERROR);
        }
        // 商品分类字段值为“中药饮片”时（即PRO_CLASS字段值为3开头）
        if (StringUtils.isNotBlank(gaiaProductBasic.getProClass()) && !gaiaProductBasic.getProClass().startsWith("3")) {
            List<GaiaProductGspinfo> gspinfos = gaiaProductGspinfoMapper.nonClass3Check(dto.getClient(), dto.getProSite(), dto.getProCode());
            if (CollectionUtils.isNotEmpty(gspinfos)) {
                throw new CustomResultException(ResultEnum.E0133, gspinfos.get(0).getProFlowNo());
            }
            List<GaiaProductBusiness> businesses = gaiaProductBusinessMapper.nonClass3Check(dto.getClient(), dto.getProSite(), dto.getProCode());
            if (CollectionUtils.isNotEmpty(businesses)) {
                throw new CustomResultException(ResultEnum.E0109);
            }
        }
        //获取当前系统时间
        dto.setProGspDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        //首营状态
        dto.setProGspStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
        // 初始编码
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        dto.setProFlowNo(System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0"));
        // 非空参数
        log.info("首营提交参数：{}", JsonUtils.beanToJson(dto));
        if (StringUtils.isEmpty(dto.getProSelfCode()) || StringUtils.isEmpty(dto.getProSite())
                || StringUtils.isEmpty(dto.getProCommonname())
                || StringUtils.isEmpty(dto.getProSpecs()) || StringUtils.isEmpty(dto.getProFactoryName()) || StringUtils.isEmpty(dto.getProIfGmp())
                || StringUtils.isEmpty(dto.getProStorageCondition()) || StringUtils.isEmpty(dto.getProSupplyName()) || StringUtils.isEmpty(dto.getProUnit())
                || StringUtils.isEmpty(dto.getProInputTax()) || StringUtils.isEmpty(dto.getProStorageArea())
        ) {
            throw new CustomResultException(ResultEnum.CHECK_PARAMETER);
        }
        // 中包装量
        dto.setProMidPackage(StringUtils.isBlank(dto.getProMidPackage()) ? "1" : dto.getProMidPackage());
//        //检查商品是否存在重复
//        int count = gaiaProductBusinessMapper.checkProductRepeat(dto.getClient(), dto.getProSelfCode(), dto.getProSite(), dto.getProTcmSpecs(), dto.getProTcmRegisterNo(),
//                dto.getProTcmFactoryCode(), dto.getProTcmPlace());
//        if (count > 0) {
//            throw new CustomResultException(ResultEnum.E0022);
//        }
        // 是否连锁
        boolean isChain = false;
        // 非连锁时，取消发起工作流
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(dto.getClient());
        gaiaStoreDataKey.setStoCode(dto.getProSite());
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        // 首营地点为门店
        if (gaiaStoreData != null) {
            // 连锁
            if (CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(gaiaStoreData.getStoAttribute())) {
                isChain = true;
            }
        } else {
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(dto.getClient());
            gaiaDcDataKey.setDcCode(dto.getProSite());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            if (StringUtils.isNotBlank(gaiaDcData.getDcChainHead())) {
                isChain = true;
            }
        }
        // 参考毛利率按照零售、采购价自动计算存入，公式：（参考售价-参考进价）/参考售价*100%（若参考售价为空或零，直接赋值0）
        // 参考进货价
        BigDecimal pro_cgj = dto.getProCgj() == null ? BigDecimal.ZERO : dto.getProCgj();
        // 参考零售价
        BigDecimal pro_lsj = dto.getProLsj() == null ? BigDecimal.ZERO : dto.getProLsj();
        // 参考毛利率
        BigDecimal pro_mll = BigDecimal.ZERO;
        // 若参考售价为空或零，直接赋值0
        if (pro_lsj.compareTo(BigDecimal.ZERO) != 0) {
            // （参考售价-参考进价）/参考售价*100%
            pro_mll = (pro_lsj.subtract(pro_cgj)).divide(pro_lsj, 4, BigDecimal.ROUND_HALF_UP);
        }
        if (org.apache.commons.lang3.StringUtils.isBlank(dto.getProDepict())) {
            String proDepict = dto.getProCommonname();
            if (StringUtils.isNotBlank(dto.getProCommonname()) && !dto.getProCommonname().equals(dto.getProName())) {
                String proNameTemp = StringUtils.isBlank(dto.getProName()) ? "" : "(" + dto.getProName() + ")";
                proDepict = dto.getProCommonname() + proNameTemp;
            }
            dto.setProDepict(proDepict);
        }
        if (org.apache.commons.lang3.StringUtils.isBlank(dto.getProPym())) {
            dto.setProPym(com.gov.purchase.utils.StringUtils.ToFirstChar(dto.getProDepict()));
        }
        List<GaiaProductClass> gaiaProductClassList = baseService.selectGaiaProductClass();
        // 连锁
        if (isChain) {
            // 商品分类
            // 当商品首营时，如果选择商品分类未指定到小类（只指定了大类或者中类），存表时系统自动根据大类，用9补齐5位值。
            // 比如用户商品分类仅选择3-中药，则存表时，默认存39999；又比如用户商品分类选择201-抗肿瘤用药，则存表时，默认存29999。
            if (StringUtils.isNotBlank(dto.getProClass()) && StringUtils.length(dto.getProClass().trim()) < 5) {
                dto.setProClass(StringUtils.rightPad(StringUtils.left(dto.getProClass(), 1), 5, "9"));
                // 商品分类名称
                if (!CollectionUtils.isEmpty(gaiaProductClassList)) {
                    GaiaProductClass gaiaProductClass = gaiaProductClassList.stream().filter(item -> dto.getProClass().equals(item.getProClassCode())).findFirst().orElse(null);
                    if (gaiaProductClass != null) {
                        dto.setProClassName(gaiaProductClass.getProClassName());
                    }
                }
            }
            // 20210726:新增pro_code写入逻辑，如果未引用pro_code，则默认写成99999999
            if (StringUtils.isBlank(dto.getProCode())) {
                dto.setProCode("99999999");
            }
            gaiaProductGspinfoMapper.insert(dto);
            // 提交后自动新建工作流，流程节点根据人员所属门店号，从权限表取出对应质量负责人、企业负责人。审批流程：提交人（已提交）->质量->企业负责人->END
            FeignResult result = feignService.createProductWorkflow(dto);
            if (result.getCode() != 0) {
                throw new CustomResultException(result.getMessage());
            }
        } else { // 非连锁
            // 销项税率
            String pro_output_tax = gaiaProductBasic.getProInputTax();
            // 进项税率
            String pro_input_tax = dto.getProInputTax();
            if (StringUtils.isBlank(pro_output_tax)) {
                try {
                    // 税率
                    GaiaTaxCode gaiaTaxCode = gaiaTaxCodeMapper.selectByPrimaryKey(pro_input_tax);
                    List<GaiaTaxCode> gaiaTaxCodeList = gaiaTaxCodeMapper.selectTaxList();
                    // 销项税率集合
                    List<GaiaTaxCode> boys = gaiaTaxCodeList.stream().filter(s -> s.getTaxCodeValue().equals(gaiaTaxCode.getTaxCodeValue()) && s.getTaxCodeClass().equals("2")).collect(Collectors.toList());
                    pro_output_tax = boys.get(0).getTaxCode();
                } catch (Exception e) {
                    pro_output_tax = pro_input_tax.replace("J", "X");
                }
            }
            // 商品业务表
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            SpringUtil.copyPropertiesIgnoreNull(gaiaProductBasic, gaiaProductBusiness);
            SpringUtil.copyPropertiesIgnoreNull(dto, gaiaProductBusiness);
            gaiaProductBusiness.setProCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            gaiaProductBusiness.setProOutputTax(pro_output_tax);
            gaiaProductBusiness.setProInputTax(pro_input_tax);
            // 助记码
            if (StringUtils.isBlank(gaiaProductBusiness.getProPym())) {
                gaiaProductBusiness.setProPym(StringUtils.ToFirstChar(gaiaProductBasic.getProDepict()));
            }
            // 固定货位
            gaiaProductBusiness.setProFixBin(StringUtils.isBlank(gaiaProductBusiness.getProFixBin()) ? gaiaProductBusiness.getProSelfCode() : gaiaProductBusiness.getProFixBin());
            // 启用电子监管码
            // gaiaProductBusiness.setProElectronicCode(CommonEnum.NoYesStatus.NO.getCode());
            gaiaProductBusiness.setProMll(pro_mll);
            // 国际条形码
            gaiaProductBusiness.setProBarcode(dto.getProBarcode());
            // 匹配状态
            gaiaProductBusiness.setProMatchStatus(CommonEnum.MatchStatus.EXACTMATCH.getCode());
            gaiaProductBusiness.setProStatus(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
            if (StringUtils.isBlank(gaiaProductBusiness.getProStorageCondition())) {
                gaiaProductBusiness.setProStorageCondition("1");
            }
            if (StringUtils.isBlank(gaiaProductBusiness.getProCompclass())) {
                gaiaProductBusiness.setProCompclass("N999999");
                gaiaProductBusiness.setProCompclassName("N999999未匹配");
            }
            // 当商品首营时，如果选择商品分类未指定到小类（只指定了大类或者中类），存表时系统自动根据大类，用9补齐5位值。
            // 比如用户商品分类仅选择3-中药，则存表时，默认存39999；又比如用户商品分类选择201-抗肿瘤用药，则存表时，默认存29999。
            if (StringUtils.isNotBlank(gaiaProductBusiness.getProClass()) && StringUtils.length(gaiaProductBusiness.getProClass().trim()) < 5) {
                gaiaProductBusiness.setProClass(StringUtils.rightPad(StringUtils.left(gaiaProductBusiness.getProClass(), 1), 5, "9"));
                // 商品分类名称
                if (!CollectionUtils.isEmpty(gaiaProductClassList)) {
                    GaiaProductClass gaiaProductClass = gaiaProductClassList.stream().filter(item -> gaiaProductBusiness.getProClass().equals(item.getProClassCode())).findFirst().orElse(null);
                    if (gaiaProductClass != null) {
                        gaiaProductBusiness.setProClassName(gaiaProductClass.getProClassName());
                    }
                }
            }
            // 20210726:新增pro_code写入逻辑，如果未引用pro_code，则默认写成99999999
            if (StringUtils.isBlank(gaiaProductBusiness.getProCode())) {
                gaiaProductBusiness.setProCode("99999999");
            }
            gaiaProductBusinessMapper.insert(gaiaProductBusiness);
            GaiaProductGspinfo info = new GaiaProductGspinfo();
            BeanUtils.copyProperties(gaiaProductBusiness, info);
            info.setProGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
            int index = (int) ((Math.random() * 4 + 1) * 100000);
            info.setProFlowNo("N" + System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(index), 6, "0"));
            info.setProIfGmp(dto.getProIfGmp());
            info.setProSupplyName(dto.getProSupplyName());
            info.setProGspDate(DateUtils.getCurrentDateStrYYMMDD());
            gaiaProductGspinfoMapper.insert(info);

            resultMap.put("client", gaiaProductBusiness.getClient());
            resultMap.put("proSiteList", Collections.singletonList(gaiaProductBusiness.getProSite()));
            resultMap.put("proSelfCodeList", Collections.singletonList(gaiaProductBusiness.getProSelfCode()));

            // 零售价联动
            GaiaSdProductPriceKey gaiaSdProductPriceKey = new GaiaSdProductPriceKey();
            // 加盟商
            gaiaSdProductPriceKey.setClient(dto.getClient());
            // 门店编码
            gaiaSdProductPriceKey.setGsppBrId(dto.getProSite());
            // 商品编码
            gaiaSdProductPriceKey.setGsppProId(gaiaProductBusiness.getProSelfCode());
            GaiaSdProductPrice gaiaSdProductPrice = gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPriceKey);
            if (gaiaSdProductPrice == null) {
                gaiaSdProductPrice = new GaiaSdProductPrice();
                // 加盟商
                gaiaSdProductPrice.setClient(dto.getClient());
                // 门店编码
                gaiaSdProductPrice.setGsppBrId(dto.getProSite());
                // 商品编码
                gaiaSdProductPrice.setGsppProId(gaiaProductBusiness.getProSelfCode());
                // 零售价
                gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
                gaiaSdProductPriceMapper.insertSelective(gaiaSdProductPrice);
            } else {
                // 零售价
                gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
                gaiaSdProductPriceMapper.updateByPrimaryKeySelective(gaiaSdProductPrice);
            }
        }
        // 首营日志
        GaiaProductGspinfoLog gaiaProductGspinfoLogOld = new GaiaProductGspinfoLog();
        BeanUtils.copyProperties(gaiaProductBasic, gaiaProductGspinfoLogOld);
        //加盟商
        gaiaProductGspinfoLogOld.setClient(dto.getClient());
        //商品自编码
        gaiaProductGspinfoLogOld.setProSelfCode(dto.getProSelfCode());
        //地点
        gaiaProductGspinfoLogOld.setProSite(dto.getProSite());
        gaiaProductGspinfoLogOld.setProLine(1);
        gaiaProductGspinfoLogOld.setProMll(pro_mll);
        gaiaProductGspinfoLogMapper.insertSelective(gaiaProductGspinfoLogOld);

        GaiaProductGspinfoLog gaiaProductGspinfoLogNew = new GaiaProductGspinfoLog();
        BeanUtils.copyProperties(dto, gaiaProductGspinfoLogNew);
        gaiaProductGspinfoLogNew.setProLine(2);
        gaiaProductGspinfoLogNew.setProMll(pro_mll);
        gaiaProductGspinfoLogMapper.insertSelective(gaiaProductGspinfoLogNew);

        return resultMap;
    }

    /**
     * 首营列表
     *
     * @param dto QueryGspRequestDto
     * @return PageInfo<QueryGspResponseDto>
     */
    public PageInfo<QueryGspResponseDto> selectQueryGspinfo(QueryGspRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        dto.setClient(user.getClient());
        List<QueryGspResponseDto> QueryGspList = new ArrayList<>();
        //用户权限判断
        List<GaiaStoreData> limit = gaiaAuthconfiDataMapper.getSiteList(user.getClient(), user.getUserId());
        //参数地点为空的场合
        if (StringUtils.isBlank(dto.getProSite())) {
            if (null != limit && limit.size() > 0) {
                dto.setLimitList(limit);
            } else {
                PageInfo<QueryGspResponseDto> pageInfo = new PageInfo<>(QueryGspList);
                return pageInfo;
            }
            //参数地点不为空的场合
        } else {
            List<GaiaStoreData> gaiaStoreData = new ArrayList<>();
            GaiaStoreData gaiaStore = new GaiaStoreData();
            gaiaStore.setStoCode(dto.getProSite());
            gaiaStoreData.add(gaiaStore);
            dto.setLimitList(gaiaStoreData);
        }
        //分页查询
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        QueryGspList = gaiaProductGspinfoMapper.selectQueryGspinfo(dto);
        PageInfo<QueryGspResponseDto> pageInfo = new PageInfo<>(QueryGspList);
        return pageInfo;
    }

    @Override
    public String queryReferenceSite() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaSdReplenishHMapper.getParamByClientAndGcspId(user.getClient(), "GSPINFO_LIST_SITE");
    }

    /**
     * 首营审批日志
     *
     * @param dto QueryGspinfoWorkflowLogRequestDto
     * @return List<GaiaWorkflowLog>
     */
    public List<GaiaWorkflowLog> selectQueryGspinfoWorkflowLogInfo(QueryGspinfoWorkflowLogRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        dto.setClient(user.getClient());
        return gaiaWorkflowLogMapper.selectQueryGspinfoWorkflowLogInfo(dto);
    }

    /**
     * 首营详情
     *
     * @param dto GspinfoRequestDto
     * @return PageInfo<GaiaProductGspinfo>
     */
    public QueryGspinfoResponseDto selectQueryGspValue(GspinfoRequestDto dto) {
        QueryGspinfoResponseDto gaiaProductGspinfo = gaiaProductGspinfoMapper.selectByPrimary(dto);
        if (gaiaProductGspinfo != null) {
            if (StringUtils.isNotBlank(gaiaProductGspinfo.getProJylb())) {
                List<JyfwooData> jyfwooDataList = gaiaProductGspinfoMapper.getJyfwooData(gaiaProductGspinfo.getProJylb());
                if (CollectionUtils.isNotEmpty(jyfwooDataList)) {
                    List<String> name = jyfwooDataList.stream().map(JyfwooData::getJyfwName).collect(Collectors.toList());
                    gaiaProductGspinfo.setProJylbName(String.join(",", name));
                }
            }
        }
        return gaiaProductGspinfo;
    }

    /**
     * 商品自编码
     *
     * @param client  加盟商
     * @param proSite 门店
     * @param code    编码
     * @return
     */
    @Override
    public Result getMaxSelfCodeNum(String client, String proSite, String code) {
        // 自编码集合
        List<String> list = gaiaProductBusinessMapper.getSelfCodeList(client, proSite, code);
        if (CollectionUtils.isEmpty(list)) {
            return ResultUtil.success();
        }
        String maxCode = ListUtils.getMaxCode(list);
        String selfCode = ListUtils.getMaxCode(maxCode);
        return ResultUtil.success(selfCode);
    }

    /**
     * 商品详情
     *
     * @param proCode 商品编号
     * @return
     */
    @Override
    public Result getProBasicByCode(String proCode) {
        // 商品详情
        GaiaProductBasic gaiaProductBasic = gaiaProductBasicMapper.selectByPrimaryKey(proCode);
        return ResultUtil.success(gaiaProductBasic);
    }

    /**
     * 手工新增
     *
     * @param gaiaProductGspinfo
     * @return
     */
    @Override
    @Transactional
    public Map<String, Object> addBusioness(GaiaProductGspinfo gaiaProductGspinfo) {
        Map<String, Object> resultMap = new HashMap<>();
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        gaiaProductGspinfo.setClient(user.getClient());
        // 商品自编码重复验证
        // 同一加盟商门店自编码不可以重复
        {
            List<GaiaProductGspinfo> gspinfos = gaiaProductGspinfoMapper.selfOnlyCheck(gaiaProductGspinfo.getClient(), gaiaProductGspinfo.getProSelfCode(), gaiaProductGspinfo.getProSite());
            if (CollectionUtils.isNotEmpty(gspinfos)) {
                throw new CustomResultException(ResultEnum.SELFCODE_ERROR);
            }
            List<GaiaProductBusiness> businesses = gaiaProductBusinessMapper.selfOnlyCheck(gaiaProductGspinfo.getClient(), gaiaProductGspinfo.getProSelfCode(), gaiaProductGspinfo.getProSite());
            if (CollectionUtils.isNotEmpty(businesses)) {
                throw new CustomResultException(ResultEnum.SELFCODE_ERROR);
            }
        }
        //获取当前系统时间
        gaiaProductGspinfo.setProGspDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        //首营状态
        gaiaProductGspinfo.setProGspStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
        // 初始编码
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        gaiaProductGspinfo.setProFlowNo(System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0"));
        // 非空参数
        if (StringUtils.isEmpty(gaiaProductGspinfo.getProSelfCode()) || StringUtils.isEmpty(gaiaProductGspinfo.getProSite())
                || StringUtils.isEmpty(gaiaProductGspinfo.getProCommonname())
                || StringUtils.isEmpty(gaiaProductGspinfo.getProSpecs())
                || StringUtils.isEmpty(gaiaProductGspinfo.getProFactoryName()) || StringUtils.isEmpty(gaiaProductGspinfo.getProIfGmp())
                || StringUtils.isEmpty(gaiaProductGspinfo.getProStorageCondition()) || StringUtils.isEmpty(gaiaProductGspinfo.getProSupplyName())
                || StringUtils.isEmpty(gaiaProductGspinfo.getProUnit()) || StringUtils.isEmpty(gaiaProductGspinfo.getProInputTax())
                || StringUtils.isEmpty(gaiaProductGspinfo.getProStorageArea())
        ) {
            throw new CustomResultException(ResultEnum.CHECK_PARAMETER);
        }
        // 参考毛利率按照零售、采购价自动计算存入，公式：（参考售价-参考进价）/参考售价*100%（若参考售价为空或零，直接赋值0）
        // 参考进货价
        BigDecimal pro_cgj = gaiaProductGspinfo.getProCgj() == null ? BigDecimal.ZERO : gaiaProductGspinfo.getProCgj();
        // 参考零售价
        BigDecimal pro_lsj = gaiaProductGspinfo.getProLsj() == null ? BigDecimal.ZERO : gaiaProductGspinfo.getProLsj();
        // 参考毛利率
        BigDecimal pro_mll = BigDecimal.ZERO;
        // 若参考售价为空或零，直接赋值0
        if (pro_lsj.compareTo(BigDecimal.ZERO) != 0) {
            // （参考售价-参考进价）/参考售价*100%
            pro_mll = (pro_lsj.subtract(pro_cgj)).divide(pro_lsj, 4, BigDecimal.ROUND_HALF_UP);
        }
        gaiaProductGspinfo.setProMll(pro_mll);
        // 是否连锁
        boolean isChain = false;
        // 非连锁时，取消发起工作流
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(gaiaProductGspinfo.getClient());
        gaiaStoreDataKey.setStoCode(gaiaProductGspinfo.getProSite());
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        // 首营地点为门店
        if (gaiaStoreData != null) {
            // 连锁
            if (CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(gaiaStoreData.getStoAttribute())) {
                isChain = true;
            }
        } else {
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(gaiaProductGspinfo.getClient());
            gaiaDcDataKey.setDcCode(gaiaProductGspinfo.getProSite());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            if (StringUtils.isNotBlank(gaiaDcData.getDcChainHead())) {
                isChain = true;
            }
        }
        if (org.apache.commons.lang3.StringUtils.isBlank(gaiaProductGspinfo.getProDepict())) {
            String proDepict = gaiaProductGspinfo.getProCommonname();
            if (StringUtils.isNotBlank(gaiaProductGspinfo.getProCommonname()) && !gaiaProductGspinfo.getProCommonname().equals(gaiaProductGspinfo.getProName())) {
                String proNameTemp = StringUtils.isBlank(gaiaProductGspinfo.getProName()) ? "" : "(" + gaiaProductGspinfo.getProName() + ")";
                proDepict = gaiaProductGspinfo.getProCommonname() + proNameTemp;
            }
            gaiaProductGspinfo.setProDepict(proDepict);
        }
        if (org.apache.commons.lang3.StringUtils.isBlank(gaiaProductGspinfo.getProPym())) {
            gaiaProductGspinfo.setProPym(com.gov.purchase.utils.StringUtils.ToFirstChar(gaiaProductGspinfo.getProDepict()));
        }
        List<GaiaProductClass> gaiaProductClassList = baseService.selectGaiaProductClass();
        // 连锁
        if (isChain) {
            // 当商品首营时，如果选择商品分类未指定到小类（只指定了大类或者中类），存表时系统自动根据大类，用9补齐5位值。
            // 比如用户商品分类仅选择3-中药，则存表时，默认存39999；又比如用户商品分类选择201-抗肿瘤用药，则存表时，默认存29999。
            if (StringUtils.isNotBlank(gaiaProductGspinfo.getProClass()) && StringUtils.length(gaiaProductGspinfo.getProClass().trim()) < 5) {
                gaiaProductGspinfo.setProClass(StringUtils.rightPad(StringUtils.left(gaiaProductGspinfo.getProClass(), 1), 5, "9"));
                // 商品分类名称
                if (!CollectionUtils.isEmpty(gaiaProductClassList)) {
                    GaiaProductClass gaiaProductClass = gaiaProductClassList.stream().filter(item -> gaiaProductGspinfo.getProClass().equals(item.getProClassCode())).findFirst().orElse(null);
                    if (gaiaProductClass != null) {
                        gaiaProductGspinfo.setProClassName(gaiaProductClass.getProClassName());
                    }
                }
            }
            // 20210726:新增pro_code写入逻辑，如果未引用pro_code，则默认写成99999999
            if (StringUtils.isBlank(gaiaProductGspinfo.getProCode())) {
                gaiaProductGspinfo.setProCode("99999999");
            }
            gaiaProductGspinfoMapper.insert(gaiaProductGspinfo);
            // 提交后自动新建工作流，流程节点根据人员所属门店号，从权限表取出对应质量负责人、企业负责人。审批流程：提交人（已提交）->质量->企业负责人->END
            FeignResult result = feignService.createProductWorkflow(gaiaProductGspinfo);
            if (result.getCode() != 0) {
                throw new CustomResultException(result.getMessage());
            }
        } else { // 非连锁
            // 销项税率
            String pro_output_tax = gaiaProductGspinfo.getProInputTax();
            // 进项税率
            String pro_input_tax = gaiaProductGspinfo.getProInputTax();
            if (StringUtils.isBlank(pro_output_tax)) {
                try {
                    // 税率
                    GaiaTaxCode gaiaTaxCode = gaiaTaxCodeMapper.selectByPrimaryKey(pro_input_tax);
                    List<GaiaTaxCode> gaiaTaxCodeList = gaiaTaxCodeMapper.selectTaxList();
                    // 销项税率集合
                    List<GaiaTaxCode> boys = gaiaTaxCodeList.stream().filter(s -> s.getTaxCodeValue().equals(gaiaTaxCode.getTaxCodeValue()) && s.getTaxCodeClass().equals("2")).collect(Collectors.toList());
                    pro_output_tax = boys.get(0).getTaxCode();
                } catch (Exception e) {
                    pro_output_tax = pro_input_tax.replace("J", "X");
                }
            }
            // 商品业务表
            GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
            SpringUtil.copyPropertiesIgnoreNull(gaiaProductGspinfo, gaiaProductBusiness);
            gaiaProductBusiness.setProCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            gaiaProductBusiness.setProOutputTax(pro_output_tax);
            gaiaProductBusiness.setProInputTax(pro_input_tax);
            // (商品名)+商品通用名称
            if (StringUtils.isBlank(gaiaProductBusiness.getProDepict())) {
                String proDepict = gaiaProductBusiness.getProCommonname();
                if (StringUtils.isNotBlank(gaiaProductBusiness.getProCommonname()) && !gaiaProductBusiness.getProCommonname().equals(gaiaProductBusiness.getProName())) {
                    String proNameTemp = StringUtils.isBlank(gaiaProductBusiness.getProName()) ? "" : "(" + gaiaProductBusiness.getProName() + ")";
                    proDepict = gaiaProductBusiness.getProCommonname() + proNameTemp;
                }
                gaiaProductBusiness.setProDepict(proDepict);
            }
            // 助记码
            if (StringUtils.isBlank(gaiaProductBusiness.getProPym())) {
                gaiaProductBusiness.setProPym(StringUtils.ToFirstChar(gaiaProductBusiness.getProDepict()));
            }
            // 固定货位
            gaiaProductBusiness.setProFixBin(StringUtils.isBlank(gaiaProductBusiness.getProFixBin()) ? gaiaProductBusiness.getProSelfCode() : gaiaProductBusiness.getProFixBin());
            // 启用电子监管码
            // gaiaProductBusiness.setProElectronicCode(CommonEnum.NoYesStatus.NO.getCode());
            gaiaProductBusiness.setProMll(pro_mll);
            // 国际条形码
            gaiaProductBusiness.setProBarcode(gaiaProductGspinfo.getProBarcode());
            // 匹配状态
            gaiaProductBusiness.setProMatchStatus(CommonEnum.MatchStatus.DIDMATCH.getCode());
            gaiaProductBusiness.setProStatus(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
            if (StringUtils.isBlank(gaiaProductBusiness.getProStorageCondition())) {
                gaiaProductBusiness.setProStorageCondition("1");
            }
            if (StringUtils.isBlank(gaiaProductBusiness.getProCompclass())) {
                gaiaProductBusiness.setProCompclass("N999999");
                gaiaProductBusiness.setProCompclassName("N999999未匹配");
            }
            // 当商品首营时，如果选择商品分类未指定到小类（只指定了大类或者中类），存表时系统自动根据大类，用9补齐5位值。
            // 比如用户商品分类仅选择3-中药，则存表时，默认存39999；又比如用户商品分类选择201-抗肿瘤用药，则存表时，默认存29999。
            if (StringUtils.isNotBlank(gaiaProductBusiness.getProClass()) && StringUtils.length(gaiaProductBusiness.getProClass().trim()) < 5) {
                gaiaProductBusiness.setProClass(StringUtils.rightPad(StringUtils.left(gaiaProductBusiness.getProClass(), 1), 5, "9"));
                // 商品分类名称
                if (!CollectionUtils.isEmpty(gaiaProductClassList)) {
                    GaiaProductClass gaiaProductClass = gaiaProductClassList.stream().filter(item -> gaiaProductBusiness.getProClass().equals(item.getProClassCode())).findFirst().orElse(null);
                    if (gaiaProductClass != null) {
                        gaiaProductBusiness.setProClassName(gaiaProductClass.getProClassName());
                    }
                }
            }
            // 20210726:新增pro_code写入逻辑，如果未引用pro_code，则默认写成99999999
            if (StringUtils.isBlank(gaiaProductBusiness.getProCode())) {
                gaiaProductBusiness.setProCode("99999999");
            }
            gaiaProductBusinessMapper.insert(gaiaProductBusiness);
            GaiaProductGspinfo info = new GaiaProductGspinfo();
            BeanUtils.copyProperties(gaiaProductGspinfo, info);
            BeanUtils.copyProperties(gaiaProductBusiness, info);
            info.setProGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
            int index = (int) ((Math.random() * 4 + 1) * 100000);
            info.setProFlowNo("N" + System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(index), 6, "0"));
            if (StringUtils.isBlank(info.getProMidPackage())) {
                info.setProMidPackage("");
            }
            if (StringUtils.isBlank(info.getProIfGmp())) {
                info.setProIfGmp(gaiaProductGspinfo.getProIfGmp());
            }
            if (StringUtils.isBlank(info.getProSupplyName())) {
                info.setProSupplyName(gaiaProductGspinfo.getProSupplyName());
            }
            if (StringUtils.isBlank(info.getProGspDate())) {
                info.setProGspDate(DateUtils.getCurrentDateStrYYMMDD());
            }
            gaiaProductGspinfoMapper.insert(info);

            // 第三方 gys-operation
            resultMap.put("client", gaiaProductBusiness.getClient());
            resultMap.put("proSiteList", Collections.singletonList(gaiaProductBusiness.getProSite()));
            resultMap.put("proSelfCodeList", Collections.singletonList(gaiaProductBusiness.getProSelfCode()));

            // 零售价联动
            GaiaSdProductPriceKey gaiaSdProductPriceKey = new GaiaSdProductPriceKey();
            // 加盟商
            gaiaSdProductPriceKey.setClient(gaiaProductGspinfo.getClient());
            // 门店编码
            gaiaSdProductPriceKey.setGsppBrId(gaiaProductGspinfo.getProSite());
            // 商品编码
            gaiaSdProductPriceKey.setGsppProId(gaiaProductBusiness.getProSelfCode());
            GaiaSdProductPrice gaiaSdProductPrice = gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPriceKey);
            if (gaiaSdProductPrice == null) {
                gaiaSdProductPrice = new GaiaSdProductPrice();
                // 加盟商
                gaiaSdProductPrice.setClient(gaiaProductGspinfo.getClient());
                // 门店编码
                gaiaSdProductPrice.setGsppBrId(gaiaProductGspinfo.getProSite());
                // 商品编码
                gaiaSdProductPrice.setGsppProId(gaiaProductBusiness.getProSelfCode());
                // 零售价
                gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
                gaiaSdProductPriceMapper.insertSelective(gaiaSdProductPrice);
            } else {
                // 零售价
                gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
                gaiaSdProductPriceMapper.updateByPrimaryKeySelective(gaiaSdProductPrice);
            }
        }
        return resultMap;
    }

    /**
     * 供货单位
     *
     * @param site
     * @return
     */
    @Override
    public Result getSupplierList(String site) {
        TokenUser user = feignService.getLoginInfo();
        SupplierListForReturnsRequestDto supplierListForReturnsRequestDto = new SupplierListForReturnsRequestDto();
        supplierListForReturnsRequestDto.setClient(user.getClient());
        supplierListForReturnsRequestDto.setProSite(site);
        List<SupplierListForReturnsResponseDto> supplierList = gaiaSupplierBusinessMapper.selectSupplierList(supplierListForReturnsRequestDto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 商品首营详情
     */
    @Override
    public GaiaProductGspinfo getGspinfoDetails(GetGspinfoDetailsDTO dto) {
        GaiaProductGspinfoKey key = new GaiaProductGspinfo();
        BeanUtils.copyProperties(dto, key);
        GaiaProductGspinfo gaiaProductGspinfo = gaiaProductGspinfoMapper.selectByPrimaryKey(key);
        GaiaProductBusiness productBusiness = gaiaProductBusinessMapper.getProductBusiness(
                gaiaProductGspinfo.getClient(), gaiaProductGspinfo.getProSite(), gaiaProductGspinfo.getProSelfCode());
        if (productBusiness == null) {
            productBusiness = new GaiaProductBusiness();
        }
        gaiaProductGspinfo.setGaiaProductBusiness(productBusiness);
        gaiaProductGspinfo.setIsWjsp(productBusiness.getIsWjsp());
        return gaiaProductGspinfo;


    }

    @Override
    public Result exportSelectQueryGspinfo(QueryGspRequestDto dto) throws Exception {
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        dto.setClient(user.getClient());
        List<QueryGspResponseDtoCSV> queryGspList = new ArrayList<>();
        //用户权限判断
        List<GaiaStoreData> limit = gaiaAuthconfiDataMapper.getSiteList(user.getClient(), user.getUserId());
        CsvFileInfo csvInfo;
        Result result = new Result();
        //参数地点为空的场合
        if (StringUtils.isBlank(dto.getProSite())) {
            if (null != limit && limit.size() > 0) {
                dto.setLimitList(limit);
            } else {
                result.setMessage("提示：查询结果为空");
                return result;
            }
            //参数地点不为空的场合
        } else {
            List<GaiaStoreData> gaiaStoreData = new ArrayList<>();
            GaiaStoreData gaiaStore = new GaiaStoreData();
            gaiaStore.setStoCode(dto.getProSite());
            gaiaStoreData.add(gaiaStore);
            dto.setLimitList(gaiaStoreData);
        }
        // 不分页查询
        dto.setPageNum(0);
        dto.setPageSize(0);
        queryGspList = gaiaProductGspinfoMapper.selectExportSelectQueryGspinfo(dto);
        if (ObjectUtils.isEmpty(queryGspList)) {
            result.setMessage("提示：查询结果为空");
            return result;
        }
        for (QueryGspResponseDtoCSV queryGspResponseDtoCSV : queryGspList) {
            // 转换数据内容
            convertData(queryGspResponseDtoCSV);
        }
        try {
            csvInfo = CsvClient.getCsvByte(queryGspList, "商品首营列表", Collections.singletonList((short) 1));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                bos.flush();
                bos.close();
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void convertData(QueryGspResponseDtoCSV queryGspResponseDtoCSV) {
        // 防止科学计数

        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProSelfCode())) {
            queryGspResponseDtoCSV.setProSelfCode(queryGspResponseDtoCSV.getProSelfCode() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProFlowNo())) {
            queryGspResponseDtoCSV.setProFlowNo(queryGspResponseDtoCSV.getProFlowNo() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProSite())) {
            queryGspResponseDtoCSV.setProSite(queryGspResponseDtoCSV.getProSite() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProBarcode())) {
            queryGspResponseDtoCSV.setProBarcode(queryGspResponseDtoCSV.getProBarcode() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProBasicCode())) {
            queryGspResponseDtoCSV.setProBasicCode(queryGspResponseDtoCSV.getProBasicCode() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProIfMed())) {
            switch (queryGspResponseDtoCSV.getProIfMed()) {
                case "0":
                    queryGspResponseDtoCSV.setProIfMed("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProIfMed("是");
                    break;
            }
        }

        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProBrandClass())) {
            switch (queryGspResponseDtoCSV.getProBrandClass()) {
                case "1":
                    queryGspResponseDtoCSV.setProBrandClass("全国品牌");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProBrandClass("省份品牌");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setProBrandClass("地区品牌");
                    break;
                case "4":
                    queryGspResponseDtoCSV.setProBrandClass("本地品牌");
                    break;
                case "5":
                    queryGspResponseDtoCSV.setProBrandClass("其它");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProControlClass())) {
            switch (queryGspResponseDtoCSV.getProControlClass()) {
                case "1":
                    queryGspResponseDtoCSV.setProControlClass("毒性药品");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProControlClass("麻醉药品");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setProControlClass("一类精神药品");
                    break;
                case "4":
                    queryGspResponseDtoCSV.setProControlClass("二类精神药品");
                    break;
                case "5":
                    queryGspResponseDtoCSV.setProControlClass("易制毒药品（麻黄碱）");
                    break;
                case "6":
                    queryGspResponseDtoCSV.setProControlClass("放射性药品");
                    break;
                case "7":
                    queryGspResponseDtoCSV.setProControlClass("生物制品（含胰岛素）");
                    break;
                case "8":
                    queryGspResponseDtoCSV.setProControlClass("兴奋剂（除胰岛素）");
                    break;
                case "9":
                    queryGspResponseDtoCSV.setProControlClass("第一类器械");
                    break;
                case "10":
                    queryGspResponseDtoCSV.setProControlClass("第二类器械");
                    break;
                case "11":
                    queryGspResponseDtoCSV.setProControlClass("第三类器械");
                    break;
                case "12":
                    queryGspResponseDtoCSV.setProControlClass("其它管制");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProProduceClass())) {
            switch (queryGspResponseDtoCSV.getProProduceClass()) {
                case "1":
                    queryGspResponseDtoCSV.setProProduceClass("辅料");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProProduceClass("化学药品");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setProProduceClass("生物制品");
                    break;
                case "4":
                    queryGspResponseDtoCSV.setProProduceClass("中药");
                    break;
                case "5":
                    queryGspResponseDtoCSV.setProProduceClass("器械");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProElectronicCode())) {
            switch (queryGspResponseDtoCSV.getProElectronicCode()) {
                case "0":
                    queryGspResponseDtoCSV.setProElectronicCode("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProElectronicCode("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProPosition())) {
            switch (queryGspResponseDtoCSV.getProPosition()) {
                case "T":
                    queryGspResponseDtoCSV.setProPosition("淘汰");
                    break;
                case "Q":
                    queryGspResponseDtoCSV.setProPosition("清场");
                    break;
                case "X":
                    queryGspResponseDtoCSV.setProPosition("新品");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProTssx())) {
            switch (queryGspResponseDtoCSV.getProTssx()) {
                case "1":
                    queryGspResponseDtoCSV.setProTssx("防疫");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProYblx())) {
            switch (queryGspResponseDtoCSV.getProYblx()) {
                case "1":
                    queryGspResponseDtoCSV.setProYblx("甲类");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProYblx("乙类");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProOut())) {
            switch (queryGspResponseDtoCSV.getProOut()) {
                case "1":
                    queryGspResponseDtoCSV.setProOut("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProIfWholesale())) {
            switch (queryGspResponseDtoCSV.getProIfWholesale()) {
                case "0":
                    queryGspResponseDtoCSV.setProIfWholesale("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProIfWholesale("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProGspStatus())) {
            switch (queryGspResponseDtoCSV.getProGspStatus()) {
                case "0":
                    queryGspResponseDtoCSV.setProGspStatus("审批中");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProGspStatus("已审批");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProGspStatus("已废弃");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProIfGmp())) {
            switch (queryGspResponseDtoCSV.getProIfGmp()) {
                case "0":
                    queryGspResponseDtoCSV.setProIfGmp("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProIfGmp("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProStorageCondition())) {
            switch (queryGspResponseDtoCSV.getProStorageCondition()) {
                case "1":
                    queryGspResponseDtoCSV.setProStorageCondition("常温");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProStorageCondition("阴凉");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setProStorageCondition("冷藏");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProLifeUnit())) {
            switch (queryGspResponseDtoCSV.getProLifeUnit()) {
                case "1":
                    queryGspResponseDtoCSV.setProLifeUnit("天");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProLifeUnit("月");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setProLifeUnit("年");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProRegisterClass())) {
            switch (queryGspResponseDtoCSV.getProRegisterClass()) {
                // 批准文号分类 1-国产药品，2-进口药品，3-国产器械，4-进口器械，5-中药饮片，6-特殊化妆品，7-消毒用品，8-保健食品，9-QS商品，10-其它
                case "1":
                    queryGspResponseDtoCSV.setProRegisterClass("国产药品");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProRegisterClass("进口药品");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setProRegisterClass("国产器械");
                    break;
                case "4":
                    queryGspResponseDtoCSV.setProRegisterClass("进口器械");
                    break;
                case "5":
                    queryGspResponseDtoCSV.setProRegisterClass("中药饮片");
                    break;
                case "6":
                    queryGspResponseDtoCSV.setProRegisterClass("特殊化妆品");
                    break;
                case "7":
                    queryGspResponseDtoCSV.setProRegisterClass("消毒用品");
                    break;
                case "8":
                    queryGspResponseDtoCSV.setProRegisterClass("保健食品");
                    break;
                case "9":
                    queryGspResponseDtoCSV.setProRegisterClass("QS商品");
                    break;
                case "10":
                    queryGspResponseDtoCSV.setProRegisterClass("其它");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProStorageArea())) {
            switch (queryGspResponseDtoCSV.getProStorageArea()) {
                // 商品仓储分区 1-内服药品，2-外用药品，3-中药饮片区，4-精装中药区，5-针剂，6-二类精神药品，7-含麻药品，8-冷藏商品，9-外用非药，10-医疗器械，11-食品，12-保健食品，13-易燃商品区
                case "1":
                    queryGspResponseDtoCSV.setProStorageArea("内服药品");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProStorageArea("外用药品");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setProStorageArea("中药饮片区");
                    break;
                case "4":
                    queryGspResponseDtoCSV.setProStorageArea("精装中药区");
                    break;
                case "5":
                    queryGspResponseDtoCSV.setProStorageArea("针剂");
                    break;
                case "6":
                    queryGspResponseDtoCSV.setProStorageArea("二类精神药品");
                    break;
                case "7":
                    queryGspResponseDtoCSV.setProStorageArea("含麻药品");
                    break;
                case "8":
                    queryGspResponseDtoCSV.setProStorageArea("冷藏商品");
                    break;
                case "9":
                    queryGspResponseDtoCSV.setProStorageArea("外用非药");
                    break;
                case "10":
                    queryGspResponseDtoCSV.setProStorageArea("医疗器械");
                    break;
                case "11":
                    queryGspResponseDtoCSV.setProStorageArea("食品");
                    break;
                case "12":
                    queryGspResponseDtoCSV.setProStorageArea("保健食品");
                    break;
                case "13":
                    queryGspResponseDtoCSV.setProStorageArea("易燃商品区");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProMedProdct())) {
            switch (queryGspResponseDtoCSV.getProMedProdct()) {
                case "0":
                    queryGspResponseDtoCSV.setProMedProdct("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProMedProdct("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProPresclass())) {
            switch (queryGspResponseDtoCSV.getProPresclass()) {
                case "1":
                    queryGspResponseDtoCSV.setProPresclass("处方药");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProPresclass("甲类otc");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setProPresclass("乙类otc");
                    break;
                case "4":
                    queryGspResponseDtoCSV.setProPresclass("双跨处方");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProStatus())) {
            switch (queryGspResponseDtoCSV.getProStatus()) {
                case "0":
                    queryGspResponseDtoCSV.setProStatus("可用");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProStatus("不可用");
                    break;
            }
        }


        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProNoPurchase())) {
            switch (queryGspResponseDtoCSV.getProNoPurchase()) {
                case "0":
                    queryGspResponseDtoCSV.setProNoPurchase("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProNoPurchase("是");
                    break;
            }
        }

        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProNoRetail())) {
            switch (queryGspResponseDtoCSV.getProNoRetail()) {
                case "0":
                    queryGspResponseDtoCSV.setProNoRetail("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProNoRetail("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProNoDistributed())) {
            switch (queryGspResponseDtoCSV.getProNoDistributed()) {
                case "0":
                    queryGspResponseDtoCSV.setProNoDistributed("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProNoDistributed("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProNoSupplier())) {
            switch (queryGspResponseDtoCSV.getProNoSupplier()) {
                case "0":
                    queryGspResponseDtoCSV.setProNoSupplier("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProNoSupplier("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProNoDc())) {
            switch (queryGspResponseDtoCSV.getProNoDc()) {
                case "0":
                    queryGspResponseDtoCSV.setProNoDc("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProNoDc("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProNoAdjust())) {
            switch (queryGspResponseDtoCSV.getProNoAdjust()) {
                case "0":
                    queryGspResponseDtoCSV.setProNoAdjust("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProNoAdjust("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProNoSale())) {
            switch (queryGspResponseDtoCSV.getProNoSale()) {
                case "0":
                    queryGspResponseDtoCSV.setProNoSale("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProNoSale("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProNoApply())) {
            switch (queryGspResponseDtoCSV.getProNoApply()) {
                case "0":
                    queryGspResponseDtoCSV.setProNoApply("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProNoApply("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProIfpart())) {
            switch (queryGspResponseDtoCSV.getProIfpart()) {
                case "0":
                    queryGspResponseDtoCSV.setProIfpart("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProIfpart("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProKeyCare())) {
            switch (queryGspResponseDtoCSV.getProKeyCare()) {
                case "0":
                    queryGspResponseDtoCSV.setProKeyCare("不养护");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProKeyCare("重点养护");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setProKeyCare("一般养护");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProIfSms())) {
            switch (queryGspResponseDtoCSV.getProIfSms()) {
                case "0":
                    queryGspResponseDtoCSV.setProIfSms("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProIfSms("是");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getProIfpart())) {
            switch (queryGspResponseDtoCSV.getProIfpart()) {
                case "0":
                    queryGspResponseDtoCSV.setProIfpart("否");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setProIfpart("是");
                    break;
            }
        }
    }
}
