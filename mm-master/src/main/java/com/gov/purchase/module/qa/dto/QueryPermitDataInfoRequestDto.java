package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "证照资质明细传入参数")
public class QueryPermitDataInfoRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    @NotBlank(message = "加盟商编号不能为空")
    private String client;

    @ApiModelProperty(value = "实体类型", name = "perEntityClass")
    @NotBlank(message = "实体类型不能为空")
    private String perEntityClass;

    @ApiModelProperty(value = "实体编码", name = "perEntityId")
    @NotBlank(message = "实体编码不能为空")
    private String perEntityId;

}
