package com.gov.purchase.module.base.service.impl.supplierImport;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaSupplierBasic;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.*;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.18
 */
@Service
public class SupplierSearch {

    @Resource
    GaiaSupplierBasicMapper gaiaSupplierBasicMapper;

    @Resource
    GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Resource
    CosUtils cosUtils;

    /**
     * 供应商查询（通用）
     *
     * @param batSearchValBasic
     * @return
     */
    public Result selectSupplierBasicBatch(BatSearchValBasic batSearchValBasic) {
        // 查询条件
        Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
        initSupplierBasicWhere(map);
        PageHelper.startPage(batSearchValBasic.getPageNum(), batSearchValBasic.getPageSize());
        List<SupplierDto> batchList = gaiaSupplierBasicMapper.selectBasicBatch(map);
        PageInfo<SupplierDto> pageInfo = new PageInfo<>(batchList);

        return ResultUtil.success(pageInfo);
    }

    /**
     * 供应商查询（通用+业务）
     *
     * @param batSearchValBasic
     * @return
     */
    public Result selectSupplierBusinessBatch(BatSearchValBasic batSearchValBasic) {
        // 查询条件
        Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
        initSupplierBusinessWhere(map);
        PageHelper.startPage(batSearchValBasic.getPageNum(), batSearchValBasic.getPageSize());
        List<SupplierDto> batchList = gaiaSupplierBasicMapper.selectBusinessBatch(map);
        PageInfo<SupplierDto> pageInfo = new PageInfo<>(batchList);

        return ResultUtil.success(pageInfo);
    }

    /**
     * 加载查询条件
     *
     * @param map
     */
    private void initSupplierBasicWhere(Map<String, SearchValBasic> map) {
        // 查询条件为空
        if (map == null || map.isEmpty()) {
            return;
        }

        // 供应商分类
        SearchValBasic sup_class = map.get("c");
        if (sup_class != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(sup_class.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.SUP_CLASS, sup_class.getSearchVal()));
                list.add(sup_class.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(sup_class.getMoreVal())) {
                for (String item : sup_class.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.SUP_CLASS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                sup_class.setSearchVal(null);
            }
            sup_class.setMoreVal(list);
        }

        // 供应商状态
        SearchValBasic sup_status = map.get("d");
        if (sup_status != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(sup_status.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.SUP_STATUS, sup_status.getSearchVal()));
                list.add(sup_status.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(sup_status.getMoreVal())) {
                for (String item : sup_status.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.SUP_STATUS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                sup_status.setSearchVal(null);
            }
            sup_status.setMoreVal(list);
        }
    }

    /**
     * 加载查询条件
     *
     * @param map
     */
    private void initSupplierBusinessWhere(Map<String, SearchValBasic> map) {
        // 查询条件为空
        if (map == null || map.isEmpty()) {
            return;
        }

        initSupplierBasicWhere(map);

        // 加盟商
        SearchValBasic client = map.get("e");
        if (client != null) {

            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(client.getSearchVal()) || CollectionUtils.isNotEmpty(client.getMoreVal())) {
                list.addAll(gaiaFranchiseeMapper.getFrancName(client));
                if (StringUtils.isNotBlank(client.getSearchVal())) {
                    list.add(client.getSearchVal());
                } else {
                    list.addAll(client.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                client.setSearchVal(null);
            }
            client.setMoreVal(list);
        }

        // 供应商状态
        SearchValBasic sup_status = map.get("h");
        if (sup_status != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(sup_status.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.SUP_STATUS, sup_status.getSearchVal()));
                list.add(sup_status.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(sup_status.getMoreVal())) {
                for (String item : sup_status.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.SUP_STATUS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                sup_status.setSearchVal(null);
            }
            sup_status.setMoreVal(list);
        }
    }

    /**
     * 供应商主数据导出
     *
     * @param batSearchValBasic
     * @return
     */
    public Result exportSupplierBasicBatch(BatSearchValBasic batSearchValBasic) {
        try {
            // 查询条件
            Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
            initSupplierBasicWhere(map);
            List<SupplierDto> batchList = gaiaSupplierBasicMapper.selectBasicBatch(map);
            // 导出数据
            List<List<String>> basicListc = new ArrayList<>();
            initData(batchList, basicListc, null);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(basicHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(basicListc);
                    }},
                    new ArrayList<String>() {{
                        add("供应商主数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 供应商业务数据导出
     *
     * @param batSearchValBasic
     * @return
     */
    public Result exportSupplierBusinessBatch(BatSearchValBasic batSearchValBasic) {
        try {
            // 查询条件
            Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
            initSupplierBusinessWhere(map);
            List<SupplierDto> batchList = gaiaSupplierBasicMapper.selectBusinessBatch(map);
            // 导出数据
            List<List<String>> businessList = new ArrayList<>();
            initData(batchList, null, businessList);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(businessHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(businessList);
                    }},
                    new ArrayList<String>() {{
                        add("供应商业务数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param basicListc
     * @param businessList
     */
    private void initData(List<SupplierDto> batchList, List<List<String>> basicListc, List<List<String>> businessList) {
        for (SupplierDto supplierDto : batchList) {
            if (basicListc != null) {
                List<String> basicData = new ArrayList<>();
                //供应商编码
                basicData.add(supplierDto.getSupCode());
                //助记码
                basicData.add(supplierDto.getSupPym());
                //供应商名称
                basicData.add(supplierDto.getSupName());
                //统一社会信用代码
                basicData.add(supplierDto.getSupCreditCode());
                //营业期限
                basicData.add(supplierDto.getSupCreditDate());
                //供应商分类
                String sup_class = supplierDto.getSupClass();
                initDictionaryStaticData(basicData, sup_class, CommonEnum.DictionaryStaticData.SUP_CLASS);
                //法人
                basicData.add(supplierDto.getSupLegalPerson());
                //注册地址
                basicData.add(supplierDto.getSupRegAdd());
                //供应商状态
                String sup_status = supplierDto.getSupStatus();
                initDictionaryStaticData(basicData, sup_status, CommonEnum.DictionaryStaticData.SUP_STATUS);
                //许可证编号
                basicData.add(supplierDto.getSupLicenceNo());
                //发证日期
                basicData.add(supplierDto.getSupLicenceDate());
                //有效期至
                basicData.add(supplierDto.getSupLicenceValid());
                //生产或经营范围
                basicData.add(supplierDto.getSupScope());
                basicListc.add(basicData);
            }
            if (businessList != null) {
                List<String> businessData = new ArrayList<>();
                //加盟商
                businessData.add(supplierDto.getClient());
                //地点
                businessData.add(supplierDto.getSiteName());
                //供应商自编码
                businessData.add(supplierDto.getSupSelfCode());
                //供应商编码
                businessData.add(supplierDto.getSupCode());
                // 匹配状态
                String sup_match_status = supplierDto.getSupMatchStatus();
                initDictionaryStaticData(businessData, sup_match_status, CommonEnum.DictionaryStaticData.PRO_MATCH_STATUS);
                //助记码
                businessData.add(supplierDto.getSupPym());
                //供应商名称
                businessData.add(supplierDto.getSupName());
                //统一社会信用代码
                businessData.add(supplierDto.getSupCreditCode());
                //营业期限
                businessData.add(supplierDto.getSupCreditDate());
                //供应商分类
                String sup_class = supplierDto.getSupClass();
                initDictionaryStaticData(businessData, sup_class, CommonEnum.DictionaryStaticData.SUP_CLASS);
                //法人
                businessData.add(supplierDto.getSupLegalPerson());
                //注册地址
                businessData.add(supplierDto.getSupRegAdd());
                //许可证编号
                businessData.add(supplierDto.getSupLicenceNo());
                //发证日期
                businessData.add(supplierDto.getSupLicenceDate());
                //有效期至
                businessData.add(supplierDto.getSupLicenceValid());
                //生产或经营范围
                businessData.add(supplierDto.getSupScope());
                //供应商状态
                String sup_status = supplierDto.getSupStatusBussiness();
                initDictionaryStaticData(businessData, sup_status, CommonEnum.DictionaryStaticData.SUP_STATUS);
                //禁止采购
                String sup_no_purchase = supplierDto.getSupNoPurchase();
                initDictionaryStaticData(businessData, sup_no_purchase, CommonEnum.DictionaryStaticData.NO_YES);
                //禁止退厂
                String sup_no_supplier = supplierDto.getSupNoSupplier();
                initDictionaryStaticData(businessData, sup_no_supplier, CommonEnum.DictionaryStaticData.NO_YES);
                //采购付款条件
                businessData.add(supplierDto.getPayTypeDesc());
                //业务联系人
                businessData.add(supplierDto.getSupBussinessContact());
                //联系人电话
                businessData.add(supplierDto.getSupContactTel());
                //送货前置期
                businessData.add(supplierDto.getSupLeadTime());
                //银行代码
                businessData.add(supplierDto.getSupBankCode());
                //银行名称
                businessData.add(supplierDto.getSupBankName());
                //账户持有人
                businessData.add(supplierDto.getSupAccountPerson());
                //银行账号
                businessData.add(supplierDto.getSupBankAccount());
                //支付方式
                String sup_pay_mode = supplierDto.getSupPayMode();
                initDictionaryStaticData(businessData, sup_pay_mode, CommonEnum.DictionaryStaticData.SUP_PAY_MODE);
                //铺底授信额度
                businessData.add(supplierDto.getSupCreditAmt());
                businessList.add(businessData);
            }
        }
    }

    /**
     * 查询数据静态字典数据处理
     * @param list
     * @param value
     */
    private void initDictionaryStaticData(List<String> list, String value, CommonEnum.DictionaryStaticData dictionaryStaticData) {
        if (StringUtils.isBlank(value)) {
            list.add("");
        } else {
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, value);
            if (dictionary != null) {
                list.add(dictionary.getLabel());
            } else {
                list.add("");
            }
        }
    }

    /**
     * 业务数据表头
     */
    private String[] businessHead = {
            "加盟商",
            "地点",
            "供应商自编码",
            "供应商编码",
            "匹配状态",
            "助记码",
            "供应商名称",
            "统一社会信用代码",
            "营业期限",
            "供应商分类",
            "法人",
            "注册地址",
            "许可证编号",
            "发证日期",
            "有效期至",
            "生产或经营范围",
            "供应商状态",
            "禁止采购",
            "禁止退厂",
            "采购付款条件",
            "业务联系人",
            "联系人电话",
            "送货前置期",
            "银行代码",
            "银行名称",
            "账户持有人",
            "银行账号",
            "支付方式",
            "铺底授信额度"
    };

    /**
     * 基础数据头
     */
    private String[] basicHead = {
            "供应商编码",
            "助记码",
            "供应商名称",
            "统一社会信用代码",
            "营业期限",
            "供应商分类",
            "法人",
            "注册地址",
            "供应商状态",
            "许可证编号",
            "发证日期",
            "有效期至",
            "生产或经营范围"
    };

//供应商	2	数据库	1	供应商编码	a
//		加盟商	2	供应商名称	b
//				供应商分类	c
//				供应商状态	d
//				加盟商	e
//				地点	f
//				供应商自编码	g
//				供应商状态	h
}
