package com.gov.purchase.module.base.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.base.dto.BatSearchValBasic;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.20
 */
public interface BatchService {
    /**
     * 批量导入列表
     * @param pageSize
     * @param pageNum
     * @param bulDateType     数据类型
     * @param bulUpdateType   操作类型
     * @param bulUpdateStatus 操作状态
     * @return
     */
    Result queryBatchList(Integer pageSize, Integer pageNum, String bulDateType, String bulUpdateType, String bulUpdateStatus);

    /**
     * excel 上传
     * @param type 1:药品库导入
     * @param path 文件
     * @return
     */
    Result uploadBatch(String type, String path);

    /**
     * 数据导入
     * @param bulUpdateCode
     * @return
     */
    Result importBatch(String bulUpdateCode);

    /**
     * 批量查询
     * @param batSearchValBasic
     * @return
     */
    Result searchBatch(BatSearchValBasic batSearchValBasic);

    /**
     * 导出
     * @param batSearchValBasic
     * @return
     */
    Result exportBatch(BatSearchValBasic batSearchValBasic);
}
