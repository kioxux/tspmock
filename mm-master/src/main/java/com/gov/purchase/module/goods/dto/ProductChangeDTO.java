package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductChange;
import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021/4/19 16:36
 */
@Data
public class ProductChangeDTO extends GaiaProductChange {

    private String changeColumnDes;
}
