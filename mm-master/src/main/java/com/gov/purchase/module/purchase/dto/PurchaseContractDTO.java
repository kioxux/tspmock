package com.gov.purchase.module.purchase.dto;

import lombok.Data;

/**
 * @description: 采购合同
 * @author: yzf
 * @create: 2022-01-28 13:58
 */
@Data
public class PurchaseContractDTO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 采购主体
     */
    private String conCompanyCode;

    /**
     * 供应商
     */
    private String conSupplierId;

    /**
     * 业务员
     */
    private String conSalesmanCode;

    /**
     * 合同编号
     */
    private String conId;

    /**
     * 商品编码
     */
    private String conProCode;

    /**
     * 合同状态
     */
    private String conType;

    /**
     * 创建日期起始
     */
    private String conCreateDateBegin;

    /**
     * 创建日期截止
     */
    private String conCreateDateEnd;
}
