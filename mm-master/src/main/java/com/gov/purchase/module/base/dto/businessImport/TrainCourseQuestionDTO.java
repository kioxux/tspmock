package com.gov.purchase.module.base.dto.businessImport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@EqualsAndHashCode
public class TrainCourseQuestionDTO {

    /**
     * 类别
     */
    private Integer questionType;

    /**
     * 难易级别
     */
    private Integer questionLevel;

    /**
     * 题目名称
     */
    private String questionName;

    /**
     * 题目释意
     */
    private String questionRemark;

    /**
     * 答案列表
     */
    private List<Integer> questionAnswerList;

    /**
     * 排序
     */
    private Integer questionSort;

    /**
     * 选项列表
     */
    private List<TrainCourseQuestionItem> questionItemList;


    @Data
    @EqualsAndHashCode
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TrainCourseQuestionItem {
        /**
         * 值（索引）
         */
        private Integer itemValue;

        /**
         * 选项
         */
        private String itemName;
    }

}

