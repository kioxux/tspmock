package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "付款申请保存返回参数")
public class PaymentReqSaveResponseDto {

    /**
     * 付款申请单号
     */
    private String payOrderId;
}