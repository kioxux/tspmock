package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaSupplierSalesman;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/5/10 9:52
 */
@Data
public class EditSupplierSalesmanListVO {

    @NotBlank(message = "供应商地点不能为空")
    private String supSite;

    @NotBlank(message = "供应商自编码不能为空")
    private String supSelfCode;

    private List<GaiaSupplierSalesman> supplierSalesmanList;


}
