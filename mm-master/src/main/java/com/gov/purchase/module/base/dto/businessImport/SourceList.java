package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

@Data
public class SourceList {

    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = true)
    private String souProCode;

    @ExcelValidate(index = 1, name = "工厂", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = true)
    private String souSiteCode;

    @ExcelValidate(index = 2, name = "有效期起始", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd", addRequired = true)
    private String souEffectFrom;

    @ExcelValidate(index = 3, name = "有效期截至", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd", addRequired = true)
    private String souEffectEnd;

    @ExcelValidate(index = 4, name = "供应商编码", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = true)
    private String souSupplierId;

    @ExcelValidate(index = 5, name = "是否主供应商", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = true, dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES)
    private String souMainSupplier;

    @ExcelValidate(index = 6, name = "是否锁定供应商", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = true, dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES)
    private String souLockSupplier;

    @ExcelValidate(index = 7, name = "冻结标志", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = true, dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES)
    private String souDeleteFlag;
}
