package com.gov.purchase.module.purchase.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.purchase.dto.PurchaseReceiptPriceDTO;
import com.gov.purchase.module.purchase.service.PurchaseReceiptPriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 采购入库差价单功能
 * @author: yzf
 * @create: 2021-12-13 09:21
 */
@Api(tags = "采购入库差价单功能")
@RestController
@RequestMapping("purchaseReceiptPrice")
public class PurchaseReceiptPriceController {

    @Resource
    private PurchaseReceiptPriceService purchaseReceiptPriceService;

    @Log("采购入库未审核差价单列表")
    @ApiOperation("采购入库未审核差价单查询")
    @PostMapping("getPurchaseReceiptPriceList")
    public Result getPurchaseReceiptPriceList() {
        return ResultUtil.success(purchaseReceiptPriceService.selectCjIdList());
    }


    @Log("采购入库差价单保存")
    @ApiOperation("采购入库差价单保存")
    @PostMapping("savePurchaseReceiptPriceList")
    public Result savePurchaseReceiptPriceList(@RequestBody PurchaseReceiptPriceDTO dto) {
        if (dto != null) {
            dto.setCjStatus("0");
        }
        return purchaseReceiptPriceService.savePurchaseReceiptPriceList(dto);
    }


    @Log("采购入库差价单审核")
    @ApiOperation("采购入库差价单审核")
    @PostMapping("auditPurchaseReceiptPriceList")
    public Result auditPurchaseReceiptPriceList(@RequestBody PurchaseReceiptPriceDTO dto) {
        if (dto != null) {
            dto.setCjStatus("1");
        }
        return purchaseReceiptPriceService.savePurchaseReceiptPriceList(dto);
    }


    @Log("采购入库差价单删除")
    @ApiOperation("采购入库差价单删除")
    @PostMapping("deletePurchaseReceiptPriceList")
    public Result deletePurchaseReceiptPriceList(@RequestBody PurchaseReceiptPriceDTO dto) {
        return purchaseReceiptPriceService.deletePurchaseReceiptPriceList(dto);
    }


    @Log("根据采购差价单号查询")
    @ApiOperation("根据采购差价单号查询")
    @PostMapping("getRecordByCjId")
    public Result getRecordByCjId(@RequestBody PurchaseReceiptPriceDTO dto) {
        return ResultUtil.success(purchaseReceiptPriceService.getRecordByCjId(dto));
    }


    @Log("采购入库差价单查询")
    @ApiOperation("采购入库差价单查询")
    @PostMapping("getRecord")
    public Result getRecord(@RequestBody PurchaseReceiptPriceDTO dto) {
        return ResultUtil.success(purchaseReceiptPriceService.getPurchaseReceiptPriceRecord(dto));
    }
}
