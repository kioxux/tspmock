package com.gov.purchase.module.supplier.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.module.supplier.dto.*;


public interface EditSupplierService {

    /**
     * 供应商列表
     *
     * @param dto SupplierListRequestDto
     * @return PageInfo<SupplierListResponseDto>
     */
    PageInfo<SupplierListResponseDto> selectQuerySupInfo(SupplierListRequestDto dto);

    /**
     * 供应商详情
     *
     * @param dto QuerySupRequestDto
     * @return List<QuerySupResponseDto>
     */
    GaiaSupplierBusiness selectQuerySup(QuerySupRequestDto dto);

    /**
     * 供应商维护
     *
     * @param dto QueryUnDrugUpdateRequestDto
     */
    void updateSupEditInfo(QueryUnDrugUpdateRequestDto dto);
}
