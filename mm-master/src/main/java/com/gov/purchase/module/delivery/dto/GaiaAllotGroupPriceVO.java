package com.gov.purchase.module.delivery.dto;

import com.gov.purchase.entity.GaiaAllotGroupPrice;
import lombok.Data;

@Data
public class GaiaAllotGroupPriceVO extends GaiaAllotGroupPrice {
    private String id;
    private String stoName;
}
