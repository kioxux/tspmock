package com.gov.purchase.module.store.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.excel.ExportExcel;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.CommonConstants;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.BaseService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.store.dto.GetStoreProductPriceListDTO;
import com.gov.purchase.module.store.dto.RetailStore;
import com.gov.purchase.module.store.dto.SdProductPrice;
import com.gov.purchase.module.store.dto.price.*;
import com.gov.purchase.module.store.service.PriceService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class PriceServiceImpl implements PriceService {

    @Resource
    private GaiaPriceGroupMapper gaiaPriceGroupMapper;
    @Resource
    private GaiaRetailPriceMapper gaiaRetailPriceMapper;
    @Resource
    private GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private BaseService baseService;
    @Resource
    private FeignService feignService;
    @Resource
    private RedisClient redisClient;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private OperationFeignService operationFeignService;
    @Resource
    private GaiaSdSystemParaMapper gaiaSdSystemParaMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Resource
    GaiaTaxCodeMapper gaiaTaxCodeMapper;
    @Resource
    GaiaMaterialAssessMapper gaiaMaterialAssessMapper;
    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;

    @Resource
    private GaiaProfitLevelMapper gaiaProfitLevelMapper;
    private static final String PRICESERVICEIMPL_PRC_MODFIY_NO = "PRICESERVICEIMPL_PRC_MODFIY_NO";
    private Stream<GaiaStoreData> stream;

    /**
     * 价格组列表
     *
     * @return
     */
    @Override
    public List<PriceGroupListReponseDto> queryPriceGroupList(PriceGroupListRequestDto dto) {

        return gaiaPriceGroupMapper.selectPriceGroupList(dto);
    }


    /**
     * 价格范围选择
     *
     * @param dto
     * @return
     */
    @Override
    public List<PriceStoreListReponseDto> queryPriceStoreList(PriceStoreListRequestDto dto) {
        return gaiaPriceGroupMapper.queryPriceStoreList(dto);
    }

    /**
     * 根据商品名称筛选
     *
     * @param dto
     * @return
     */
    @Override
    public ProductBasicListReponseDto queryProductBasicList(ProductBasicListRequestDto dto) {
        ProductBasicListReponseDto productBasicListReponseDto = gaiaRetailPriceMapper.queryProductBasicList(dto);
        // 单独设置调前价格
        if (ObjectUtils.isNotEmpty(productBasicListReponseDto)) {
            BigDecimal prePrice = gaiaRetailPriceMapper.queryPrePrice(dto);
            productBasicListReponseDto.setPrePrice(prePrice);
        }
        return productBasicListReponseDto;
    }

    /**
     * 调价申请提交
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveRetailPriceInfo(RetailPriceInfoSaveRequestDto dto) {
        /**
         * 1. 验证
         */
        validRetailPriceInfo(dto);
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        /**
         * 调价单插入
         */
        String yymmdd = LocalDate.now().toString().replace("-", "");
        //  主键: 生成调价单号  YYYYMMDD+4位流水
        // 获取当前最大流水
        String currentMaxNo = gaiaRetailPriceMapper.selectMaxModifNo(dto.getClient(), yymmdd);
        String prcModfiyNo = yymmdd + "0001";
        if (currentMaxNo != null) {
            prcModfiyNo = yymmdd + (String.format("%04d", Integer.parseInt(currentMaxNo.substring(currentMaxNo.length() - 4)) + 1));
        }
        for (String storeNo : dto.getStoreList()) {
            for (RetailPriceInfoItem item : dto.getProList()) {
                GaiaRetailPrice price = new GaiaRetailPrice();
                BeanUtils.copyProperties(item, price);
                // 调价单号
                price.setPrcModfiyNo(prcModfiyNo);
                //  主键:  client   加盟商
                price.setClient(dto.getClient());
                //  主键:  prouct    商品自编码
                price.setPrcProduct(item.getPrcProduct());
                //  主键:  prc_class 价格类型
                price.setPrcClass(dto.getPrcClass());
                //  主键 : sotre     门店编码
                price.setPrcStore(storeNo);
                // 审批状态默认为否
                price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
                // 修改之后价格 (9,4)
                price.setPrcAmount(item.getPrcAmount());
                // 修改前价格  (9,4)
                price.setPrcAmountBefore(item.getPrcAmountBefore());
                // 创建日期
                price.setPrcCreateDate(DateUtils.getCurrentDateStr().replaceAll("-", ""));
                // 创建时间
                price.setPrcCreateTime(DateUtils.getCurrentTimeStr().replaceAll(":", ""));
                price.setPrcSource("0");
                // 总部调价
                price.setPrcHeadPrice(item.getPrcHeadPrice());
                price.setPrcCreateUser(user.getUserId());
                // 销售级别
                price.setPrcSlaeClass(item.getPrcSlaeClass());
                // 自定义字段
                price.setPrcZdy1(item.getPrcZdy1());
                price.setPrcZdy2(item.getPrcZdy2());
                price.setPrcZdy3(item.getPrcZdy3());
                price.setPrcZdy4(item.getPrcZdy4());
                price.setPrcZdy5(item.getPrcZdy5());
                gaiaRetailPriceMapper.insert(price);
            }
        }
        return ResultUtil.success();
    }

    @Override
    public PageInfo<PriceApprovalListReponseDto> queryPriceApprovalList(PriceApprovalListRequestDto dto) {
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<PriceApprovalListReponseDto> list = gaiaRetailPriceMapper.queryPriceApprovalList(dto);
        for (PriceApprovalListReponseDto priceApprovalListReponseDto : list) {
            List<RetailStore> retailStoreList = gaiaRetailPriceMapper.queryStoreList(user.getClient(), priceApprovalListReponseDto.getPrcModfiyNo(),
                    priceApprovalListReponseDto.getPrcProduct());
            priceApprovalListReponseDto.setPrcStoreList(retailStoreList);
        }
        return new PageInfo<>(list);
    }

    @Override
    public PageInfo<PriceListReponseDto> queryPriceList(PriceListRequestDto dto) {
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        //当前权限下的门店列表
        List<String> stoList = getCurrentUserAuthStoreList(null).stream().filter(Objects::nonNull)
                .map(PriceStoreListReponseDto::getStoCode).collect(Collectors.toList());
        dto.setStoList(stoList);
        //  1.按价格查询
        if (dto.getType().equals("1")) {
            // 分頁查詢
            PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
            return new PageInfo<>(gaiaRetailPriceMapper.queryPriceListOnPrice(dto));
        } //2.按商品查询
        else if (dto.getType().equals("2")) {
            // 将价格类型、调价单号 置为 空 不用拿来查询
            dto.setPrcClass(null);
            dto.setPrcModfiyNo(null);
            // 分頁查詢
            PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
            return new PageInfo<>(gaiaRetailPriceMapper.queryPriceListOnProduct(dto));
        } else {
            // 查询方式错误
            throw new CustomResultException(ResultEnum.REQUEST_TYPE_ERROR);
        }

    }

    /**
     * 调价申请验证
     *
     * @param dto
     * @return
     */
    @Override
    public Result validRetailPriceInfo(RetailPriceInfoSaveRequestDto dto) {
        Map<String, Object> map = new HashMap<>();
        map.put("client", dto.getClient());  // 加盟商
        map.put("prcClass", dto.getPrcClass()); // 价格类型
        for (String storeNo : dto.getStoreList()) {
            for (RetailPriceInfoItem item : dto.getProList()) {
                ValidationResult result = ValidateUtil.validateEntity(item);
                if (result.isHasErrors()) {
                    throw new CustomResultException(result.getMessage());
                }
                if (StringUtils.isNotBlank(item.getPrcLimitAmount())) {
                    if (!item.getPrcLimitAmount().matches("^[0-9]\\d*|0$")) {
                        throw new CustomResultException(ResultEnum.RETAIL_LIMITNUM_FORMAT);
                    }
                }
//                map.put("prcStore", storeNo);  // 门店编号
//                map.put("prcProduct", item.getPrcProduct()); // 商品编号
//                map.put("prcEffectDate", item.getPrcEffectDate()); //有效期起
//                int count = gaiaRetailPriceMapper.validRetailPriceExist(map);
//                if (count > 0) {
//                    throw new CustomResultException(ResultEnum.RETAIL_PRICE_EXIST);
//                }
            }
        }

        return ResultUtil.success();
    }

    @Override
    public void excelPriceList(PriceListRequestDto dto) throws IOException {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();
        PriceListRequestDto price = new PriceListRequestDto();
        BeanUtils.copyProperties(dto, price);
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        //当前权限下的门店列表
        List<String> stoList = getCurrentUserAuthStoreList(null).stream().filter(Objects::nonNull)
                .map(PriceStoreListReponseDto::getStoCode).collect(Collectors.toList());
        price.setStoList(stoList);
        PageHelper.startPage(0, 0);
        //  1.按价格查询
        if (dto.getType().equals("1")) {
            List<PriceListReponseDto> list = gaiaRetailPriceMapper.queryPriceListOnPrice(price);
            List<PriceListByTypeDto> excelDataList = new ArrayList<>();
            PriceListByTypeDto priceListByTypeDto = null;
            for (PriceListReponseDto priceListReponseDto : list) {
                priceListByTypeDto = new PriceListByTypeDto();
                BeanUtils.copyProperties(priceListReponseDto, priceListByTypeDto);
                excelDataList.add(priceListByTypeDto);
            }
            ExportExcel excel = new ExportExcel("零售价格", PriceListByTypeDto.class, 1);
            excel.setDataList(excelDataList);
            excel.write(response, "零售价格" + DateUtils.getCurrentDateTimeStr("yyyyMMddHHmmss") + ".xlsx");
        } //2.按商品查询
        else if (dto.getType().equals("2")) {
            // 将价格类型、调价单号 置为 空 不用拿来查询
            dto.setPrcClass(null);
            dto.setPrcModfiyNo(null);
            List<PriceListReponseDto> list = gaiaRetailPriceMapper.queryPriceListOnProduct(price);
            List<PriceListByGoodsDto> excelDataList = new ArrayList<>();
            PriceListByGoodsDto priceListByGoodsDto = null;
            for (PriceListReponseDto priceListReponseDto : list) {
                priceListByGoodsDto = new PriceListByGoodsDto();
                BeanUtils.copyProperties(priceListReponseDto, priceListByGoodsDto);
                excelDataList.add(priceListByGoodsDto);
            }
            ExportExcel excel = new ExportExcel("零售价格", PriceListByGoodsDto.class, 1);
            excel.setDataList(excelDataList);
            excel.write(response, "零售价格" + DateUtils.getCurrentDateTimeStr("yyyyMMddHHmmss") + ".xlsx");
        } else {
            // 查询方式错误
            throw new CustomResultException(ResultEnum.REQUEST_TYPE_ERROR);
        }
    }

    /**
     * 调价单审批
     *
     * @param list
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approvePriceList(List<PriceApprovalListReponseDto> list, Map<String, Object> map) {
        if (list == null || list.size() == 0) {
            throw new CustomResultException("请选择调价单");
        }
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        List<Map<String, String>> params = new ArrayList<>();
        List<GaiaRetailPrice> gaiaRetailPrices = new ArrayList<>();
        // 毛利级别
        List<GaiaProfitLevel> gaiaProfitLevelList = gaiaProfitLevelMapper.selectProfitList(user.getClient());
        for (PriceApprovalListReponseDto priceApprovalListReponseDto : list) {
            String modifyNo = priceApprovalListReponseDto.getPrcModfiyNo();
            String prcProduct = priceApprovalListReponseDto.getPrcProduct();
            GaiaRetailPrice gaiaRetailPrice = new GaiaRetailPrice();
            gaiaRetailPrice.setClient(user.getClient());
            gaiaRetailPrice.setPrcModfiyNo(modifyNo);
            gaiaRetailPrice.setPrcProduct(prcProduct);
            gaiaRetailPrices.add(gaiaRetailPrice);
            // 调价门店
            List<RetailStore> retailStoreList = priceApprovalListReponseDto.getPrcStoreList();
            for (RetailStore retailStore : retailStoreList) {
                Map<String, String> param = new HashMap<>();
                param.put("client", user.getClient());
                param.put("stoCode", retailStore.getStoCode());
                param.put("proSelfCode", priceApprovalListReponseDto.getPrcProduct());
                params.add(param);
            }
        }
        List<GaiaRetailPrice> gaiaRetailPriceList = gaiaRetailPriceMapper.selectByModifyNo(gaiaRetailPrices);
        // 税率
        List<GaiaTaxCode> gaiaTaxCodeList = gaiaTaxCodeMapper.selectTaxList();
        // 商品价格
        List<GaiaSdProductPrice> gaiaSdProductPriceList = gaiaSdProductPriceMapper.selectByPrimaryKeyList(user.getClient(), params);
        // 物料评估
        List<GaiaMaterialAssess> gaiaMaterialAssessList = gaiaMaterialAssessMapper.selectByPrimaryKeyList(params);
        // 商品主数据
        List<GaiaProductBusiness> gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKeyList(user.getClient(), params);
        // 门店主数据
        List<GaiaStoreData> gaiaStoreDataList = gaiaStoreDataMapper.selectByPrimaryKeyList(user.getClient(), params);

        List<String> site = new ArrayList<>();
        List<String> proIds = new ArrayList<>();
        List<GaiaSdMessage> gaiaSdMessageList = new ArrayList<>();
        // 更新状态
        List<GaiaRetailPrice> updateGaiaRetailPriceList = new ArrayList<>();
        Map<String, String> mapId = new HashMap<>();
        GaiaSdMessage gaiaSdMessage = new GaiaSdMessage();
        boolean insertFlg = false;
        // 遍历处理
        for (PriceApprovalListReponseDto priceApprovalListReponseDto : list) {
            if (insertFlg) {
                gaiaSdProductPriceList = gaiaSdProductPriceMapper.selectByPrimaryKeyList(user.getClient(), params);
                insertFlg = false;
            }
            String modifyNo = priceApprovalListReponseDto.getPrcModfiyNo();
            String prcProduct = priceApprovalListReponseDto.getPrcProduct();
            List<GaiaRetailPrice> priceList =
                    gaiaRetailPriceList.stream().filter(s ->
                            user.getClient().equals(s.getClient()) && modifyNo.equals(s.getPrcModfiyNo()) && prcProduct.equals(s.getPrcProduct())
                    ).collect(Collectors.toList());
            if (priceList.size() == 0) {
                throw new CustomResultException("调价单" + modifyNo + "不存在");
            }
            for (GaiaRetailPrice price : priceList) {
                if (!CommonEnum.DictionaryStaticData.APPROVE_STATUS.getList().get(0).getValue().equals(price.getPrcApprovalSuatus())) {
                    throw new CustomResultException("调价单:" + modifyNo + "商品:" + price.getPrcProduct() + "已审批");
                }
            }
            // 调价门店
            List<RetailStore> retailStoreList = priceApprovalListReponseDto.getPrcStoreList();
            List<RetailStore> retailList = new ArrayList<>();
            // 调价
            List<GaiaSdProductPrice> insertList = new ArrayList<>();
            List<GaiaSdProductPrice> updateList = new ArrayList<>();
            // 更新商品零售价
            List<String> lsjSiteList = new ArrayList<>();
            for (RetailStore retailStore : retailStoreList) {
                if (retailStore.getSelectd() == 1) {
                    retailList.add(retailStore);
                    List<GaiaRetailPrice> items = priceList.stream().filter(s -> retailStore.getStoCode().equals(s.getPrcStore())).collect(Collectors.toList());

//                    GaiaSdProductPrice gaiaSdProductPrice = gaiaSdProductPriceList.stream().filter(
//                            item -> user.getClient().equals(item.getClient()) &&
//                                    priceApprovalListReponseDto.getPrcProduct().equals(item.getGsppProId()) &&
//                                    retailStore.getStoCode().equals(item.getGsppBrId())
//                    ).findFirst().orElse(null);
//                    if (gaiaSdProductPrice != null) {
//                        gaiaSdProductPrice.setGsppPriceNormal(priceApprovalListReponseDto.getPrcAmount());
//                    }
                    // 新增门店
                    if (CollectionUtils.isEmpty(items)) {
                        // 价格调整
                        initSdPrice(priceApprovalListReponseDto, priceApprovalListReponseDto.getPrcProduct(), retailStore.getStoCode(), gaiaSdMessageList, mapId,
                                insertList, updateList, gaiaSdMessage, lsjSiteList, gaiaSdProductPriceList, gaiaStoreDataList);
                        GaiaRetailPrice gaiaRetailPrice = new GaiaRetailPrice();
                        BeanUtils.copyProperties(priceApprovalListReponseDto, gaiaRetailPrice);
                        // 商品
                        gaiaRetailPrice.setPrcProduct(priceApprovalListReponseDto.getPrcProduct());
                        // 门店
                        gaiaRetailPrice.setPrcStore(retailStore.getStoCode());
                        // 状态
                        gaiaRetailPrice.setPrcApprovalSuatus(CommonEnum.DictionaryStaticData.APPROVE_STATUS.getList().get(1).getValue());
                        gaiaRetailPrice.setPrcApprovalDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                        gaiaRetailPrice.setPrcApprovalTime(DateUtils.getCurrentTimeStr("HHmmss"));
                        gaiaRetailPrice.setPrcApprovalUser(user.getUserId());
                        // 审批人
                        gaiaRetailPrice.setPrcApprovalUser(user.getUserId());
                        // 创建日期
                        gaiaRetailPrice.setPrcCreateDate(DateUtils.getCurrentDateStr().replaceAll("-", ""));
                        // 创建时间
                        gaiaRetailPrice.setPrcCreateTime(DateUtils.getCurrentTimeStr().replaceAll(":", ""));
                        // 创建人
                        gaiaRetailPrice.setPrcCreateUser(user.getUserId());
                        gaiaRetailPriceMapper.insertSelective(gaiaRetailPrice);
                        // 第三方 gys-operation
                        site.add(gaiaRetailPrice.getPrcStore());
                        proIds.add(gaiaRetailPrice.getPrcProduct());
                        // 毛利级别更新
                        baseService.setProProfit(user.getClient(), retailStore.getStoCode(), priceApprovalListReponseDto.getPrcProduct(),
                                gaiaTaxCodeList, gaiaSdProductPriceList, gaiaMaterialAssessList, gaiaProductBusiness, gaiaStoreDataList, gaiaProfitLevelList);
                    } else { // 原门店
                        for (GaiaRetailPrice retailPrice : items) {
                            // 价格调整
                            initSdPrice(priceApprovalListReponseDto, priceApprovalListReponseDto.getPrcProduct(), retailPrice.getPrcStore(), gaiaSdMessageList, mapId,
                                    insertList, updateList, gaiaSdMessage, lsjSiteList, gaiaSdProductPriceList, gaiaStoreDataList);

                            // 更新为已审批
                            GaiaRetailPrice price = new GaiaRetailPrice();
                            price.setClient(user.getClient());
                            price.setPrcModfiyNo(modifyNo);
                            price.setPrcStore(retailPrice.getPrcStore());
                            price.setPrcProduct(priceApprovalListReponseDto.getPrcProduct());
                            price.setPrcAmount(priceApprovalListReponseDto.getPrcAmount());
                            price.setPrcApprovalSuatus(CommonEnum.DictionaryStaticData.APPROVE_STATUS.getList().get(1).getValue());
                            price.setPrcApprovalDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                            price.setPrcApprovalTime(DateUtils.getCurrentTimeStr("HHmmss"));
                            price.setPrcApprovalUser(user.getUserId());
//                            gaiaRetailPriceMapper.updateByPrcModfiyNo(price);
                            updateGaiaRetailPriceList.add(price);
                            // 第三方 gys-operation
                            site.add(price.getPrcStore());
                            proIds.add(price.getPrcProduct());
                            // 毛利级别更新
                            baseService.setProProfit(user.getClient(), price.getPrcStore(), price.getPrcProduct(),
                                    gaiaTaxCodeList, gaiaSdProductPriceList, gaiaMaterialAssessList, gaiaProductBusiness, gaiaStoreDataList, gaiaProfitLevelList);
                        }
                    }
                }
            }
            // 商品零售价更新
            if (CollectionUtils.isNotEmpty(lsjSiteList)) {
                gaiaProductBusinessMapper.updateProLsj(user.getClient(), prcProduct, priceApprovalListReponseDto.getPrcAmount(), lsjSiteList);
            }
            // 调价
            if (CollectionUtils.isNotEmpty(insertList)) {
                gaiaSdProductPriceMapper.insertSelectiveList(insertList);
                insertFlg = true;
            }
            if (CollectionUtils.isNotEmpty(updateList)) {
                gaiaSdProductPriceMapper.updateByPrimaryKeySelectiveList(updateList);
            }
            // “销售级别”、“自定义字段1-5“
            if (CollectionUtils.isNotEmpty(retailList)) {
                List<String> gaiaStoreDataLists = gaiaRetailPriceMapper.selectRetailSiteList(user.getClient(), retailList);
                if (CollectionUtils.isNotEmpty(gaiaStoreDataLists)) {
                    List<Map<String, String>> itemList = new ArrayList<>();
                    List<GaiaProductBusinessKey> keyList = new ArrayList<>();
                    gaiaStoreDataLists.forEach(item -> {
                        GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                        gaiaProductBusinessKey.setClient(user.getClient());
                        gaiaProductBusinessKey.setProSite(item);
                        gaiaProductBusinessKey.setProSelfCode(priceApprovalListReponseDto.getPrcProduct());
                        keyList.add(gaiaProductBusinessKey);
                    });
                    // 销售级别
                    String prcSlaeClass = priceApprovalListReponseDto.getPrcSlaeClass();
                    if (StringUtils.isNotBlank(prcSlaeClass)) {
                        Map<String, String> item = new HashMap<>();
                        item.put("val", prcSlaeClass);
                        item.put("field", "PRO_SLAE_CLASS");
                        itemList.add(item);
                    }
                    // 自定义字段1
                    String prcZdy1 = priceApprovalListReponseDto.getPrcZdy1();
                    if (StringUtils.isNotBlank(prcZdy1)) {
                        Map<String, String> item = new HashMap<>();
                        item.put("val", prcZdy1);
                        item.put("field", "PRO_ZDY1");
                        itemList.add(item);
                    }
                    // 自定义字段2
                    String prcZdy2 = priceApprovalListReponseDto.getPrcZdy2();
                    if (StringUtils.isNotBlank(prcZdy2)) {
                        Map<String, String> item = new HashMap<>();
                        item.put("val", prcZdy2);
                        item.put("field", "PRO_ZDY2");
                        itemList.add(item);
                    }
                    // 自定义字段3
                    String prcZdy3 = priceApprovalListReponseDto.getPrcZdy3();
                    if (StringUtils.isNotBlank(prcZdy3)) {
                        Map<String, String> item = new HashMap<>();
                        item.put("val", prcZdy3);
                        item.put("field", "PRO_ZDY3");
                        itemList.add(item);
                    }
                    // 自定义字段4
                    String prcZdy4 = priceApprovalListReponseDto.getPrcZdy4();
                    if (StringUtils.isNotBlank(prcZdy4)) {
                        Map<String, String> item = new HashMap<>();
                        item.put("val", prcZdy4);
                        item.put("field", "PRO_ZDY4");
                        itemList.add(item);
                    }
                    // 自定义字段5
                    String prcZdy5 = priceApprovalListReponseDto.getPrcZdy5();
                    if (StringUtils.isNotBlank(prcZdy5)) {
                        Map<String, String> item = new HashMap<>();
                        item.put("val", prcZdy5);
                        item.put("field", "PRO_ZDY5");
                        itemList.add(item);
                    }
                    if (CollectionUtils.isNotEmpty(itemList) && CollectionUtils.isNotEmpty(keyList)) {
                        gaiaProductBusinessMapper.updateProductItemsByKey(user.getClient(), prcProduct, itemList, keyList);
                    }
                }
            }
        }
        // 状态更新
        if (CollectionUtils.isNotEmpty(updateGaiaRetailPriceList)) {
            gaiaRetailPriceMapper.updateByPrcModfiyNo(updateGaiaRetailPriceList);
        }
        if (CollectionUtils.isNotEmpty(gaiaSdMessageList)) {
            try {
                gaiaSdMessageMapper.insertBatch(gaiaSdMessageList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        map.put("client", user.getClient());
        map.put("site", site);
        map.put("proIds", proIds);
    }

    private void initSdPrice(PriceApprovalListReponseDto priceApprovalListReponseDto, String prcProduct, String prcStore,
                             List<GaiaSdMessage> gaiaSdMessageList, Map<String, String> mapId, List<GaiaSdProductPrice> insertList,
                             List<GaiaSdProductPrice> updateList, GaiaSdMessage gaiaSdMessage, List<String> lsjSiteList,
                             List<GaiaSdProductPrice> gaiaSdProductPriceList, List<GaiaStoreData> gaiaStoreDataList) {
        if (gaiaSdProductPriceList == null) {
            gaiaSdProductPriceList = new ArrayList<>();
        }
        // 价格表
        GaiaSdProductPrice gaiaSdProductPrice = gaiaSdProductPriceList.stream().filter(t ->
                t.getClient().equals(priceApprovalListReponseDto.getClient()) && t.getGsppBrId().equals(prcStore) && t.getGsppProId().equals(prcProduct)
        ).findFirst().orElse(null);
        GaiaRetailPrice entity = new GaiaRetailPrice();
        BeanUtils.copyProperties(priceApprovalListReponseDto, entity);
        if (gaiaSdProductPrice == null) {
            gaiaSdProductPrice = new GaiaSdProductPrice();
            // 加盟商
            gaiaSdProductPrice.setClient(priceApprovalListReponseDto.getClient());
            // 门店编码
            gaiaSdProductPrice.setGsppBrId(prcStore);
            // 商品编码
            gaiaSdProductPrice.setGsppProId(prcProduct);
            // 零售价
            gaiaSdProductPrice.setGsppPriceNormal(priceApprovalListReponseDto.getPrcAmount());
            // 设置价格
            this.setAmount(gaiaSdProductPrice, entity);
            if (insertList == null) {
                gaiaSdProductPriceMapper.insertSelective(gaiaSdProductPrice);
            } else {
                insertList.add(gaiaSdProductPrice);
            }
            gaiaSdMessageInsert(priceApprovalListReponseDto.getClient(), prcStore, priceApprovalListReponseDto.getPrcModfiyNo(), prcProduct, gaiaSdMessageList, mapId, gaiaSdMessage);
        } else {
            // 设置价格
            this.setAmount(gaiaSdProductPrice, entity);
            if (updateList == null) {
                gaiaSdProductPriceMapper.updateByPrimaryKeySelective(gaiaSdProductPrice);
            } else {
                updateList.add(gaiaSdProductPrice);
            }
            gaiaSdMessageInsert(priceApprovalListReponseDto.getClient(), prcStore, priceApprovalListReponseDto.getPrcModfiyNo(), prcProduct, gaiaSdMessageList, mapId, gaiaSdMessage);
        }
        // 总部调价 商品主数据更新
        if ("1".equals(priceApprovalListReponseDto.getPrcHeadPrice()) && CommonEnum.GaiaRetailPrice_PrcClass.P001.getCode().equals(priceApprovalListReponseDto.getPrcClass())) {
            // 门店零售价更新
            GaiaProductBusiness gaiaProductBusinessKey = new GaiaProductBusiness();
            gaiaProductBusinessKey.setClient(priceApprovalListReponseDto.getClient());
            gaiaProductBusinessKey.setProSite(prcStore);
            gaiaProductBusinessKey.setProSelfCode(prcProduct);
//            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
//            if (gaiaProductBusiness != null) {
            gaiaProductBusinessKey.setProLsj(priceApprovalListReponseDto.getPrcAmount());
            lsjSiteList.add(prcStore);
//            gaiaProductBusinessMapper.updateByPrimaryKeySelective(gaiaProductBusinessKey);
//            }

            // DC零售价更新 配送中心 主数据地点
//            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
//            gaiaStoreDataKey.setClient(priceApprovalListReponseDto.getClient());
//            gaiaStoreDataKey.setStoCode(prcStore);
//            GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
            GaiaStoreData gaiaStoreData = gaiaStoreDataList.stream().filter(t ->
                    t.getClient().equals(priceApprovalListReponseDto.getClient()) && t.getStoCode().equals(prcStore)
            ).findFirst().orElse(null);
            if (gaiaStoreData != null) {
                // 配送中心 主数据地点
                if (StringUtils.isNotBlank(gaiaStoreData.getStoDcCode()) || StringUtils.isNotBlank(gaiaStoreData.getStoMdSite())) {
                    GaiaProductBusiness gaiaProductBusinessKey1 = new GaiaProductBusiness();
                    gaiaProductBusinessKey1.setClient(priceApprovalListReponseDto.getClient());
                    if (StringUtils.isNotBlank(gaiaStoreData.getStoDcCode())) {
                        gaiaProductBusinessKey1.setProSite(gaiaStoreData.getStoDcCode());
                        lsjSiteList.add(gaiaStoreData.getStoDcCode());
                    } else {
                        gaiaProductBusinessKey1.setProSite(gaiaStoreData.getStoMdSite());
                        lsjSiteList.add(gaiaStoreData.getStoMdSite());
                    }
                    gaiaProductBusinessKey1.setProSelfCode(prcProduct);
                    gaiaProductBusinessKey1.setProLsj(priceApprovalListReponseDto.getPrcAmount());
//                    gaiaProductBusinessMapper.updateByPrimaryKeySelective(gaiaProductBusinessKey1);
                }
            }
        }
    }

    /**
     * 插入消息推送表
     */
    private void gaiaSdMessageInsert(String client, String stoCode, String voucherId, String prcProduct, List<GaiaSdMessage> gaiaSdMessageList,
                                     Map<String, String> mapId, GaiaSdMessage gaiaSdMessage) {
        try {
            GaiaSdMessage sdMessage = new GaiaSdMessage();
            sdMessage.setClient(client);
            // 调价门店
            sdMessage.setGsmId(stoCode);
            String currentVoucherId = mapId.get(stoCode);
            if (StringUtils.isBlank(currentVoucherId)) {
                if (gaiaSdMessage == null || StringUtils.isBlank(gaiaSdMessage.getGsmVoucherId())) {
                    // 用来查询前缀
                    sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear());
                    currentVoucherId = gaiaSdMessageMapper.getCurrentVoucherId(sdMessage);
                    gaiaSdMessage.setGsmVoucherId(currentVoucherId);
                } else {
                    currentVoucherId = gaiaSdMessage.getGsmVoucherId();
                }
            }
            long messageId = NumberUtils.toLong(currentVoucherId.substring(6));
            while (redisClient.exists(StringUtils.parse(CommonConstants.GAIA_SD_MESSAGE_GSM_VOUCHER_ID, client, stoCode, String.valueOf(messageId)))) {
                messageId++;
            }
            redisClient.set(StringUtils.parse(CommonConstants.GAIA_SD_MESSAGE_GSM_VOUCHER_ID, client, stoCode, String.valueOf(messageId)), String.valueOf(messageId), 1800);
            sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear() + StringUtils.leftPad(String.valueOf(messageId), 9, "0"));
            mapId.put(stoCode, sdMessage.getGsmVoucherId());
            sdMessage.setGsmValue("praAdjustNo=" + sdMessage.getGsmVoucherId() + "&praStore=" + sdMessage.getGsmId());
            sdMessage.setGsmRemark(MessageFormat.format("您有新的商品调价信息,商品编码为[{0}],请及时查看", prcProduct));
            sdMessage.setGsmPlatform("FX");
            // 是否查看 默认为N-未查看
            sdMessage.setGsmFlag("N");
            // 调价单号
            sdMessage.setGsmBusinessVoucherId(voucherId);
            sdMessage.setGsmArriveDate(DateUtils.getCurrentDateStrYYMMDD());
            sdMessage.setGsmArriveTime(DateUtils.getCurrentTimeStrHHMMSS());
            sdMessage.setGsmPage("pricingList");
//            gaiaSdMessageMapper.insertSelective(sdMessage);
            gaiaSdMessageList.add(sdMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 调价商品选择
     *
     * @param params
     * @param labStore
     * @return
     */
    @Override
    public Result getProductListBySto(String params, Integer inStock, String labStore, String prcClass) {
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        String store = labStore;
        if (StringUtils.isBlank(store)) {
            store = user.getDepId();
            if (StringUtils.isBlank(store)) {
                throw new CustomResultException("请选择门店");
            }
        }
        List<StoreProductDto> list = gaiaRetailPriceMapper.getProductListBySto(user.getClient(), store, params, inStock, prcClass);
        return ResultUtil.success(list);
    }

    /**
     * 门店调价提交
     * f
     *
     * @param list
     * @return
     */
    @Override
    public Result saveStoreRetailPriceInfo(ProductPriceDto dto) {
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        if (CollectionUtils.isEmpty(dto.getList())) {
            throw new CustomResultException("未提交任何数据");
        }
        boolean approveFlg = false;
        // 自动审批
        GaiaSdSystemParaKey gaiaSdSystemParaKey = new GaiaSdSystemParaKey();
        // 加盟商
        gaiaSdSystemParaKey.setClient(user.getClient());
        // 门店编码
        gaiaSdSystemParaKey.setGsspBrId(user.getDepId());
        // 参数名
        gaiaSdSystemParaKey.setGsspId(CommonEnum.GaiaSdSystemParaEnum.ADJUST_PRICE.getCode());
        GaiaSdSystemPara gaiaSdSystemPara = gaiaSdSystemParaMapper.selectByPrimaryKey(gaiaSdSystemParaKey);
        // 生成调价单信息中的「审批状态」直接置为1-已审批
        if (gaiaSdSystemPara != null && CommonEnum.NoYesStatus.YES.getCode().equals(gaiaSdSystemPara.getGsspPara())) {
            approveFlg = true;
        }

        boolean flg = true;
        String prcModfiyNo = "";
        while (flg) {
            // 流水号
            if (StringUtils.isBlank(prcModfiyNo)) {
                String yymmdd = LocalDate.now().toString().replace("-", "");
                String currentMaxNo = gaiaRetailPriceMapper.selectMaxModifNo(user.getClient(), yymmdd);
                prcModfiyNo = yymmdd + "0001";
                if (currentMaxNo != null) {
                    prcModfiyNo = yymmdd + (String.format("%04d", Integer.parseInt(currentMaxNo.substring(currentMaxNo.length() - 4)) + 1));
                }
            } else {
                prcModfiyNo = String.valueOf(NumberUtils.toInt(prcModfiyNo) + 1);
            }
            String prc_modfiy_no = redisClient.get(PRICESERVICEIMPL_PRC_MODFIY_NO + "_" + user.getClient() + "_" + prcModfiyNo);
            log.info("门店调价流水号:redis.getkey:{},value:{}", PRICESERVICEIMPL_PRC_MODFIY_NO + "_" + user.getClient() + "_" + prcModfiyNo, prc_modfiy_no);
            if (StringUtils.isBlank(prc_modfiy_no)) {
                redisClient.set(PRICESERVICEIMPL_PRC_MODFIY_NO + "_" + user.getClient() + "_" + prcModfiyNo, prcModfiyNo, 60);
                log.info("门店调价流水号:redis.setkey:{},value:{}", PRICESERVICEIMPL_PRC_MODFIY_NO + "_" + user.getClient() + "_" + prcModfiyNo, prcModfiyNo);
                flg = false;
            }
        }
        // 毛利级别
        List<GaiaProfitLevel> gaiaProfitLevelList = gaiaProfitLevelMapper.selectProfitList(user.getClient());
        // 税率
        List<GaiaTaxCode> gaiaTaxCodeList = gaiaTaxCodeMapper.selectTaxList();
        List<GaiaRetailPrice> gaiaRetailPriceList = new ArrayList<>();
        List<GaiaSdMessage> gaiaSdMessageList = new ArrayList<>();
        Map<String, String> mapId = new HashMap<>();
        List<Map<String, String>> params = new ArrayList<>();
        for (StoreProductDto storeProductDto : dto.getList()) {
            Map<String, String> param = new HashMap<>();
            param.put("client", user.getClient());
            param.put("stoCode", user.getDepId());
            param.put("proSelfCode", storeProductDto.getProSelfCode());
            params.add(param);
        }
        // 商品价格
        List<GaiaSdProductPrice> gaiaSdProductPriceList = gaiaSdProductPriceMapper.selectByPrimaryKeyList(user.getClient(), params);
        // 门店主数据
        List<GaiaStoreData> gaiaStoreDataList = gaiaStoreDataMapper.selectByPrimaryKeyList(user.getClient(), params);
        for (StoreProductDto storeProductDto : dto.getList()) {
            GaiaRetailPrice price = new GaiaRetailPrice();
            //  主键:  client   加盟商
            price.setClient(user.getClient());
            //  主键 : sotre     门店编码
            price.setPrcStore(user.getDepId());
            //  主键:  prouct    商品自编码
            price.setPrcProduct(storeProductDto.getProSelfCode());
            //  主键:  prc_class 价格类型
            price.setPrcClass(dto.getPrcClass());
            // 调价单号
            price.setPrcModfiyNo(prcModfiyNo);
            // 金额
            price.setPrcAmount(storeProductDto.getNewGsppPriceNormal());
            // 有效期起
            price.setPrcEffectDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            // 修改前价格
            price.setPrcAmountBefore(storeProductDto.getGsppPriceNormal());
            // 单位
            price.setPrcUnit(storeProductDto.getProUnit());
            // 不允许积分
            price.setPrcNoIntegral(CommonEnum.NoYesStatus.NO.getCode());
            // 不允许会员卡打折
            price.setPrcNoDiscount(CommonEnum.NoYesStatus.NO.getCode());
            // 不允许积分兑换
            price.setPrcNoExchange(CommonEnum.NoYesStatus.NO.getCode());
            if (approveFlg) {
                // 审批状态默认为否
                GaiaProductBusiness productBusiness = gaiaProductBusinessMapper.getProductBusiness(user.getClient(), user.getDepId(), storeProductDto.getProSelfCode());
                if (productBusiness != null) {
                    if (productBusiness.getIsWjsp() == null || productBusiness.getIsWjsp() == 0) {
                        price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                        price.setPrcApprovalDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                        price.setPrcApprovalTime(DateUtils.getCurrentTimeStr("HHmmss"));
                        price.setPrcApprovalUser(user.getUserId());
                    } else {
                        // 审批状态默认为否
                        price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
                        approveFlg = false;
                    }
                } else {
                    // 审批状态默认为否
                  /*price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
                  approveFlg = false;*/
                    price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                    price.setPrcApprovalDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    price.setPrcApprovalTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    price.setPrcApprovalUser(user.getUserId());
                    log.info("*********未查到相关商品主数据业务信息！");

                }
            } else {
                // 审批状态默认为否
                price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
            }
            price.setPrcSource("2");
            // 创建日期
            price.setPrcCreateDate(DateUtils.getCurrentDateStr().replaceAll("-", ""));
            // 创建时间
            price.setPrcCreateTime(DateUtils.getCurrentTimeStr().replaceAll(":", ""));
            // 调价原因
            price.setPrcReason(storeProductDto.getPrcReason());
            // 调价门店
            price.setPrcInitStore(user.getDepId());
            // 创建人
            price.setPrcCreateUser(user.getUserId());
            gaiaRetailPriceList.add(price);

            // 自动审批
            if (approveFlg) {
                PriceApprovalListReponseDto priceApprovalListReponseDto = new PriceApprovalListReponseDto();
                priceApprovalListReponseDto.setClient(user.getClient());
                priceApprovalListReponseDto.setPrcAmount(price.getPrcAmount());
                priceApprovalListReponseDto.setPrcHeadPrice(price.getPrcHeadPrice());
                priceApprovalListReponseDto.setPrcClass(CommonEnum.GaiaRetailPrice_PrcClass.P001.getCode());
                List<String> lsjSiteList = new ArrayList<>();
                // 价格设置
                initSdPrice(priceApprovalListReponseDto, storeProductDto.getProSelfCode(), user.getDepId(), gaiaSdMessageList, mapId,
                        null, null, null, lsjSiteList, gaiaSdProductPriceList, gaiaStoreDataList);
                // 商品零售价更新
                if (CollectionUtils.isNotEmpty(lsjSiteList)) {
                    gaiaProductBusinessMapper.updateProLsj(user.getClient(), storeProductDto.getProSelfCode(), priceApprovalListReponseDto.getPrcAmount(), lsjSiteList);
                }
                // 第三方 gys-operation
                operationFeignService.proPriceSync(user.getClient(), user.getDepId(), Collections.singletonList(storeProductDto.getProSelfCode()));
                // 毛利级别更新
                baseService.setProProfit(user.getClient(), user.getDepId(), storeProductDto.getProSelfCode(), gaiaTaxCodeList, null, null, null, null, gaiaProfitLevelList);
            }
        }
        gaiaRetailPriceMapper.insertList(gaiaRetailPriceList);
        if (CollectionUtils.isNotEmpty(gaiaSdMessageList)) {
            try {
                gaiaSdMessageMapper.insertBatch(gaiaSdMessageList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ResultUtil.success();
    }

    /**
     * 门店调价查询
     *
     * @param pageNum
     * @param pageSize
     * @param prcSource
     * @param prcEffectDate
     * @param proKey
     * @param prcApprovalSuatus
     * @return
     */
    @Override
    public Result selectStoreRetailPriceList(Integer pageNum, Integer pageSize, String prcSource, String prcEffectDate, String proKey, String prcApprovalSuatus, String praAdjustNo) {
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        String prcModfiyNo = getPrcModfiyNo(user.getClient(), user.getDepId(), praAdjustNo);
        // 分頁查詢
        PageHelper.startPage(pageNum, pageSize);
        List<StoreProductDto> list = gaiaRetailPriceMapper.selectStoreRetailPriceList(user.getClient(), user.getDepId(), prcSource, prcEffectDate, proKey, prcApprovalSuatus, prcModfiyNo);
        PageInfo pageInfo = new PageInfo<StoreProductDto>(list);
        return ResultUtil.success(pageInfo);
    }

    public String getPrcModfiyNo(String client, String storeId, String praAdjustNo) {
        if (StringUtils.isEmpty(praAdjustNo)) {
            return null;
        }
        return gaiaRetailPriceMapper.getPrcModfiyNo(client, storeId, praAdjustNo);
    }

    @Override
    public Result exportStoreRetailPriceList(String prcSource, String prcEffectDate, String proKey, String prcApprovalSuatus, String praAdjustNo) {
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        if (StringUtils.isEmpty(user.getDepId())) {
            throw new CustomResultException("请选择门店");
        }
        String prcModfiyNo = getPrcModfiyNo(user.getClient(), user.getDepId(), praAdjustNo);
        List<StoreProductDto> list = gaiaRetailPriceMapper.selectStoreRetailPriceList(user.getClient(), user.getDepId(), prcSource, prcEffectDate, proKey, prcApprovalSuatus, prcModfiyNo);
        List<List<Object>> dataList = new ArrayList<>();
        for (StoreProductDto dto : list) {
            // 打印行
            List<Object> item = new ArrayList<>();
            // 申请日期
            item.add(dto.getPrcEffectDate());
            // 商品编码
            item.add(dto.getProSelfCode());
            // 商品名称
            item.add(dto.getProName());
            // 通用名称
            item.add(dto.getProCommonname());
            // 规格
            item.add(dto.getProSpecs());
            // 生产厂家
            item.add(dto.getProFactoryName());
            // 单位
            item.add(dto.getProUnit());
            // 原零售价
            item.add(dto.getPrcAmountBefore());
            // 申请零售价
            item.add(dto.getPrcAmount());
            // 状态
            item.add(
                    "0".equals(dto.getPrcApprovalSuatus()) ? "待审批" :
                            "1".equals(dto.getPrcApprovalSuatus()) ? "已审批" :
                                    "2".equals(dto.getPrcApprovalSuatus()) ? "拒绝" : ""
            );
            dataList.add(item);
        }
        try {
            HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<Object>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("门店调价");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 调价单审批拒绝
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void refusePriceList(List<PriceApprovalListReponseDto> list) {
        if (list == null || list.size() == 0) {
            throw new CustomResultException("请选择调价单");
        }

        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();

        List<GaiaRetailPrice> gaiaRetailPrices = new ArrayList<>();
        for (PriceApprovalListReponseDto priceApprovalListReponseDto : list) {
            String modifyNo = priceApprovalListReponseDto.getPrcModfiyNo();
            String prcProduct = priceApprovalListReponseDto.getPrcProduct();
            GaiaRetailPrice gaiaRetailPrice = new GaiaRetailPrice();
            gaiaRetailPrice.setClient(user.getClient());
            gaiaRetailPrice.setPrcModfiyNo(modifyNo);
            gaiaRetailPrice.setPrcProduct(prcProduct);
            gaiaRetailPrices.add(gaiaRetailPrice);
        }
        List<GaiaRetailPrice> gaiaRetailPriceList = gaiaRetailPriceMapper.selectByModifyNo(gaiaRetailPrices);
        List<GaiaRetailPrice> updateList = new ArrayList<>();
        // 遍历处理
        for (PriceApprovalListReponseDto priceApprovalListReponseDto : list) {
            String modifyNo = priceApprovalListReponseDto.getPrcModfiyNo();
            String prcProduct = priceApprovalListReponseDto.getPrcProduct();
            List<GaiaRetailPrice> priceList =
                    gaiaRetailPriceList.stream().filter(s ->
                            user.getClient().equals(s.getClient()) && modifyNo.equals(s.getPrcModfiyNo()) && prcProduct.equals(s.getPrcProduct())
                    ).collect(Collectors.toList());
            if (priceList.size() == 0) {
                throw new CustomResultException("调价单" + modifyNo + "不存在");
            }
            for (GaiaRetailPrice price : priceList) {
                if (!CommonEnum.DictionaryStaticData.APPROVE_STATUS.getList().get(0).getValue().equals(price.getPrcApprovalSuatus())) {
                    throw new CustomResultException("调价单:" + modifyNo + "商品:" + price.getPrcProduct() + "已审批");
                }
            }
            // 更新为已审批
            GaiaRetailPrice price = new GaiaRetailPrice();
            price.setClient(user.getClient());
            price.setPrcModfiyNo(modifyNo);
            price.setPrcProduct(priceApprovalListReponseDto.getPrcProduct());
            price.setPrcApprovalSuatus(CommonEnum.DictionaryStaticData.APPROVE_STATUS.getList().get(2).getValue());
            price.setPrcApprovalDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            price.setPrcApprovalTime(DateUtils.getCurrentTimeStr("HHmmss"));
            price.setPrcApprovalUser(user.getUserId());
//            gaiaRetailPriceMapper.updateByPrcModfiyNo(price);
            updateList.add(price);
        }
        if (CollectionUtils.isNotEmpty(updateList)) {
            gaiaRetailPriceMapper.updateByPrcModfiyNo(updateList);
        }
    }

    /**
     * 商品门店价格
     *
     * @param proSelfCode
     * @return
     */
    @Override
    public Result getProPriceList(String proSelfCode) {
        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();
        List<SdProductPrice> list = gaiaSdProductPriceMapper.getProPriceList(user.getClient(), proSelfCode);
        return ResultUtil.success(list);
    }

    /**
     * 调价门店
     *
     * @param prcModfiyNo
     * @param proSelfCode
     * @return
     */
    @Override
    public Result queryStoreList(String prcModfiyNo, String proSelfCode) {
        TokenUser user = feignService.getLoginInfo();
        List<RetailStore> list = gaiaRetailPriceMapper.queryStoreList(user.getClient(), prcModfiyNo, proSelfCode);
        return ResultUtil.success(list);
    }

    /**
     * 调价单明细
     */
    @Override
    public List<GetStoreProductPriceListDTO> getStoreProductPriceList(String client, String proSelfCode, String prcClass, String prcModfiyNo) {
        return gaiaRetailPriceMapper.getStoreProductPriceList(client, proSelfCode, prcClass, prcModfiyNo);
    }

    /**
     * 当前用户商品组以及权限下的门店列表
     */
    @Override
    public List<PriceStoreListReponseDto> getCurrentUserAuthStoreList(String prcGroupId) {
        TokenUser user = feignService.getLoginInfo();
        return gaiaPriceGroupMapper.getCurrentUserAuthStoreList(user.getClient(), user.getUserId(), prcGroupId);
    }

    @Override
    public Result queryPriceGroupAndStoreList() {
        List<PriceGroupVO> result = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        PriceGroupListRequestDto dto = new PriceGroupListRequestDto();
        dto.setClient(user.getClient());
        List<GaiaPriceGroup> groups = gaiaPriceGroupMapper.selectStoreInPriceGroup(user.getClient());
        if (CollectionUtils.isNotEmpty(groups)) {
            List<String> storesId = groups.stream().map(GaiaPriceGroup::getPrcStore).collect(Collectors.toList());
            List<GaiaStoreData> stores = gaiaPriceGroupMapper.queryAllPriceStore(user.getClient(), storesId);
            Map<String, List<GaiaPriceGroup>> collect = groups.stream().collect(Collectors.groupingBy(GaiaPriceGroup::getPrcGroupId));
            for (Map.Entry<String, List<GaiaPriceGroup>> entry : collect.entrySet()) {
                String groupId = entry.getKey();
                List<GaiaPriceGroup> storeList = entry.getValue();
                PriceGroupVO vo = new PriceGroupVO();
                List<GaiaStoreData> storeAll = new ArrayList<>();
                for (GaiaPriceGroup item : groups) {
                    if (item.getPrcGroupId().equalsIgnoreCase(groupId)) {
                        vo.setPrcGroupId(groupId);
                        vo.setPrcGroupName(item.getPrcGroupName());
                        vo.setClient(item.getClient());
                        break;
                    }
                }
                if (CollectionUtils.isNotEmpty(storeList)) {
                    List<String> storeIdList = storeList.stream().map(GaiaPriceGroup::getPrcStore).collect(Collectors.toList());
                    storeAll = stores.stream()
                            .filter(store -> storeIdList.contains(store.getStoCode()))
                            .collect(Collectors.toList());
                }
                vo.setStoreList(storeAll);
                result.add(vo);
            }
        }
        return ResultUtil.success(result);
    }

    @Override
    public List<PriceGroupProductVO> queryPriceGroupProList(String groupId) {
        TokenUser user = feignService.getLoginInfo();
        return gaiaPriceGroupMapper.selectProByGroup(user.getClient(), groupId);
    }

    @Override
    public void delPriceGroup(String groupId) {
        TokenUser user = feignService.getLoginInfo();
        gaiaPriceGroupMapper.deleteBatchPriceGroup(user.getClient(), groupId, null);
        gaiaPriceGroupMapper.deleteBatchPricePro(user.getClient(), groupId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result savePriceGroupAndStore(PriceGroupProductDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        if (StringUtils.isBlank(dto.getPrcGroupId())) {
            String maxGroupId = gaiaPriceGroupMapper.selectMaxGroupId(user.getClient());
            if (CollectionUtils.isNotEmpty(dto.getStoreIds())) {
                List<GaiaPriceGroup> groups = new ArrayList<>();
                String finalMaxGroupId = maxGroupId != null ? String.valueOf(NumberUtils.toInt(maxGroupId) + 1) : "1";
                dto.getStoreIds().forEach(
                        item -> {
                            GaiaPriceGroup priceGroup = new GaiaPriceGroup();
                            priceGroup.setClient(user.getClient());
                            priceGroup.setPrcGroupName(dto.getPrcGroupName());
                            priceGroup.setPrcStore(item);
                            priceGroup.setPrcGroupId(finalMaxGroupId);
                            priceGroup.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                            priceGroup.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                            groups.add(priceGroup);
                        }
                );
                gaiaPriceGroupMapper.deleteBatchPriceGroupByStore(user.getClient(), finalMaxGroupId, dto.getStoreIds());
                gaiaPriceGroupMapper.insertBatch(groups);
            }
        } else {
            List<String> exist = gaiaPriceGroupMapper.selectPriceGroupStore(user.getClient(), dto.getPrcGroupId());
            gaiaPriceGroupMapper.deleteBatchPriceGroup(user.getClient(), dto.getPrcGroupId(), dto.getStoreIds());
            gaiaPriceGroupMapper.deleteBatchPriceGroupByStore(user.getClient(), dto.getPrcGroupId(), dto.getStoreIds());
            List<String> newStore = dto.getStoreIds().stream()
                    .filter(nowStore -> !exist.contains(nowStore))
                    .collect(Collectors.toList());
            List<String> oldStore = dto.getStoreIds().stream()
                    .filter(exist::contains)
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(oldStore)) {
                List<GaiaPriceGroup> oldGroups = new ArrayList<>();
                oldStore.forEach(
                        item -> {
                            GaiaPriceGroup priceGroup = new GaiaPriceGroup();
                            priceGroup.setClient(user.getClient());
                            priceGroup.setPrcGroupName(dto.getPrcGroupName());
                            priceGroup.setPrcStore(item);
                            priceGroup.setPrcGroupId(dto.getPrcGroupId());
                            priceGroup.setPrcChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                            priceGroup.setPrcChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                            oldGroups.add(priceGroup);
                        }
                );
                gaiaPriceGroupMapper.updateBatch(oldGroups);
            }
            if (CollectionUtils.isNotEmpty(newStore)) {
                List<GaiaPriceGroup> newGroups = new ArrayList<>();
                newStore.forEach(
                        item -> {
                            GaiaPriceGroup priceGroup = new GaiaPriceGroup();
                            priceGroup.setClient(user.getClient());
                            priceGroup.setPrcGroupName(dto.getPrcGroupName());
                            priceGroup.setPrcStore(item);
                            priceGroup.setPrcGroupId(dto.getPrcGroupId());
                            priceGroup.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                            priceGroup.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                            newGroups.add(priceGroup);
                        }
                );
                gaiaPriceGroupMapper.insertBatch(newGroups);
            }
            List<PriceGroupProductVO> groupProducts = gaiaPriceGroupMapper.selectProByPriceGroup(user.getClient(), dto.getPrcGroupId());
            Map<String, List<PriceGroupProductVO>> collect = groupProducts.stream().collect(Collectors.groupingBy(PriceGroupProductVO::getPrcGroupId));
            List<PriceGroupProductVO> pricePros = collect.get(dto.getPrcGroupId());
            if (CollectionUtils.isNotEmpty(pricePros)) {
                List<GaiaSdProductPrice> sdNewProductPrices = new ArrayList<>();
                List<GaiaRetailPrice> retailPrices = new ArrayList<>();
                List<GaiaSdMessage> sdMessages = new ArrayList<>();
                /**
                 * 调价单插入
                 */
                String yymmdd = LocalDate.now().toString().replace("-", "");
                //  主键: 生成调价单号  YYYYMMDD+4位流水
                // 获取当前最大流水
                String currentMaxNo = gaiaRetailPriceMapper.selectMaxModifNo(dto.getClient(), yymmdd);
                String prcModfiyNo = yymmdd + "0001";
                if (currentMaxNo != null) {
                    prcModfiyNo = yymmdd + (String.format("%04d", Integer.parseInt(currentMaxNo.substring(currentMaxNo.length() - 4)) + 1));
                }
                List<String> proAllIds = pricePros.stream().map(PriceGroupProductVO::getPrcProId).collect(Collectors.toList());
                List<GaiaProductBusiness> allBus = gaiaProductBusinessMapper.getAllProductBusiness(user.getClient(), proAllIds, newStore);
                Map<String, String> maps = new HashMap<>();
                for (String item : newStore) {
                    for (PriceGroupProductVO pro : pricePros) {
                        List<GaiaProductBusiness> one = allBus.stream().filter(u -> u.getProSelfCode().equals(pro.getPrcProId()) && u.getProSite().equals(item)).collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(one)) {
                            continue;
                        }

                        if (pro.getPrcPriceNormal() != null) {

                            GaiaSdProductPrice gaiaSdProductPrice = new GaiaSdProductPrice();
                            gaiaSdProductPrice.setClient(user.getClient());
                            gaiaSdProductPrice.setGsppBrId(item);
                            gaiaSdProductPrice.setGsppProId(pro.getPrcProId());
                            gaiaSdProductPrice.setGsppPriceNormal(pro.getPrcPriceNormal());
                            gaiaSdProductPrice.setGsppPriceHy(pro.getPrcPriceHy());
                            gaiaSdProductPrice.setGsppPriceHyr(pro.getPrcPriceHyr());
                            sdNewProductPrices.add(gaiaSdProductPrice);

                            String currentVoucherId = maps.get(item);
                            GaiaSdMessage sdMessage = new GaiaSdMessage();
                            sdMessage.setClient(user.getClient());
                            // 调价门店
                            sdMessage.setGsmId(item);
                            // 用来查询前缀
                            sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear());
                            if (org.apache.commons.lang3.StringUtils.isBlank(currentVoucherId)) {
                                currentVoucherId = gaiaSdMessageMapper.getCurrentVoucherId(sdMessage);
                            }
                            long voucherId = NumberUtils.toLong(currentVoucherId.substring(6));
                            while (redisClient.exists(StringUtils.parse(CommonConstants.GAIA_SD_MESSAGE_GSM_VOUCHER_ID, user.getClient(), item, String.valueOf(voucherId)))) {
                                voucherId++;
                            }
                            redisClient.set(StringUtils.parse(CommonConstants.GAIA_SD_MESSAGE_GSM_VOUCHER_ID, user.getClient(), item, String.valueOf(voucherId)), String.valueOf(voucherId), 1800);
                            sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear() + StringUtils.leftPad(String.valueOf(voucherId), 9, "0"));
                            maps.put(item, sdMessage.getGsmVoucherId());
                            sdMessage.setGsmValue("praAdjustNo=" + sdMessage.getGsmVoucherId() + "&praStore=" + sdMessage.getGsmId());
                            sdMessage.setGsmRemark(MessageFormat.format("您有新的商品调价信息,商品编码为[{0}],请及时查看", pro.getPrcProId()));
                            sdMessage.setGsmPlatform("FX");
                            // 是否查看 默认为N-未查看
                            sdMessage.setGsmFlag("N");
                            // 调价单号
                            sdMessage.setGsmBusinessVoucherId(prcModfiyNo);
                            sdMessage.setGsmArriveDate(DateUtils.getCurrentDateStrYYMMDD());
                            sdMessage.setGsmArriveTime(DateUtils.getCurrentTimeStrHHMMSS());
                            sdMessage.setGsmPage("pricingList");
                            sdMessages.add(sdMessage);

                            if (pro.getPrcPriceNormal() != null) {
                                GaiaRetailPrice price = new GaiaRetailPrice();
                                price.setClient(user.getClient());
                                price.setPrcStore(item);
                                price.setPrcProduct(pro.getPrcProId());
                                price.setPrcClass("P001");
                                // 调价单号
                                price.setPrcModfiyNo(prcModfiyNo);
                                // 审批状态默认为否
                                price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                                // 修改之后价格 (9,4)
                                price.setPrcAmount(pro.getPrcPriceNormal());
                                // 修改前价格  (9,4)
                                price.setPrcAmountBefore(pro.getPrcPriceNormal());
                                // 创建日期
                                price.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                                // 创建时间
                                price.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                                price.setPrcSource("0");
                                price.setPrcCreateUser(user.getUserId());
                                retailPrices.add(price);
                            }
                            if (pro.getPrcPriceHy() != null) {
                                GaiaRetailPrice price = new GaiaRetailPrice();
                                price.setClient(user.getClient());
                                price.setPrcStore(item);
                                price.setPrcProduct(pro.getPrcProId());
                                price.setPrcClass("P002");
                                // 调价单号
                                price.setPrcModfiyNo(prcModfiyNo);
                                // 审批状态默认为否
                                price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                                // 修改之后价格 (9,4)
                                price.setPrcAmount(pro.getPrcPriceHy());
                                // 修改前价格  (9,4)
                                price.setPrcAmountBefore(pro.getPrcPriceHy());
                                // 创建日期
                                price.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                                // 创建时间
                                price.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                                price.setPrcSource("0");
                                price.setPrcCreateUser(user.getUserId());
                                retailPrices.add(price);
                            }
                            if (pro.getPrcPriceHyr() != null) {
                                GaiaRetailPrice price = new GaiaRetailPrice();
                                price.setClient(user.getClient());
                                price.setPrcStore(item);
                                price.setPrcProduct(pro.getPrcProId());
                                price.setPrcClass("P004");
                                // 调价单号
                                price.setPrcModfiyNo(prcModfiyNo);
                                // 审批状态默认为否
                                price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                                // 修改之后价格 (9,4)
                                price.setPrcAmount(pro.getPrcPriceHyr());
                                // 修改前价格  (9,4)
                                price.setPrcAmountBefore(pro.getPrcPriceHyr());
                                // 创建日期
                                price.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                                // 创建时间
                                price.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                                price.setPrcSource("0");
                                price.setPrcCreateUser(user.getUserId());
                                retailPrices.add(price);
                            }
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(sdNewProductPrices)) {
                    List<GaiaSdProductPrice> exists = gaiaPriceGroupMapper.selectStoreProBatch(user.getClient(), sdNewProductPrices);
                    if (CollectionUtils.isNotEmpty(exists)) {
                        List<GaiaSdProductPrice> olds = new ArrayList<>();
                        List<GaiaSdProductPrice> news = new ArrayList<>();
                        sdNewProductPrices.forEach(
                                item -> {
                                    int count = 0;
                                    for (GaiaSdProductPrice one : exists) {
                                        if (item.getGsppBrId().equalsIgnoreCase(one.getGsppBrId()) && item.getGsppProId().equalsIgnoreCase(one.getGsppProId())) {
                                            if (item.getGsppPriceNormal() == null) {
                                                item.setGsppPriceNormal(one.getGsppPriceNormal());
                                            }
                                            if (item.getGsppPriceHy() == null) {
                                                item.setGsppPriceHy(one.getGsppPriceHy());
                                            }
                                            if (item.getGsppPriceHyr() == null) {
                                                item.setGsppPriceHyr(one.getGsppPriceHyr());
                                            }
                                            count = count + 1;
                                        }
                                    }
                                    if (count == 0) {
                                        news.add(item);
                                    } else {
                                        if (item.getGsppPriceNormal() != null || item.getGsppPriceHy() != null || item.getGsppPriceHyr() != null) {
                                            olds.add(item);
                                        }
                                    }
                                }
                        );
                        if (CollectionUtils.isNotEmpty(news)) {
                            gaiaPriceGroupMapper.insertStoreProBatch(news);
                        }
                        if (CollectionUtils.isNotEmpty(olds)) {
                            gaiaPriceGroupMapper.updateStoreProBatch(olds);
                        }
                    } else {
                        gaiaPriceGroupMapper.insertStoreProBatch(sdNewProductPrices);
                    }
                }
                if (CollectionUtils.isNotEmpty(retailPrices)) {
                    gaiaRetailPriceMapper.insertList(retailPrices);
                }
                if (CollectionUtils.isNotEmpty(sdMessages)) {
                    gaiaSdMessageMapper.insertBatch(sdMessages);
                }
            }
        }
        return ResultUtil.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result savePriceGroupPro(PriceGroupProductDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        List<String> errorProPriceList = new ArrayList<>();
        List<String> error = new ArrayList<>();
        dto.setClient(user.getClient());
        List<PriceGroupProductVO> pros = gaiaPriceGroupMapper.selectProByPriceGroup(user.getClient(), dto.getPrcGroupId());
        List<String> existPro = pros.stream().map(PriceGroupProductVO::getPrcProId).collect(Collectors.toList());
        List<String> stores = gaiaPriceGroupMapper.selectPriceGroupStore(user.getClient(), dto.getPrcGroupId());
        List<RetailStore> retailList = new ArrayList<>();
        stores.forEach(
                item -> {
                    RetailStore store = new RetailStore();
                    store.setStoCode(item);
                    retailList.add(store);
                }
        );
        List<GaiaRetailPrice> retailPrices = new ArrayList<>();
        List<GaiaSdMessage> sdMessages = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(dto.getPros())) {
            dto.getPros().forEach(
                    pro -> {
                        int countStore = gaiaProductBasicMapper.selectProInStore(user.getClient(), pro.getPrcProId(), stores);
                        if (countStore == 0) {
                            error.add(MessageFormat.format("商品{0}，当前价格组门店不存在该商品", pro.getPrcProId()));
                        }
                        List<String> storeIds = gaiaPriceGroupMapper.selectBatchStore(user.getClient(), pro.getPrcProId());
                        boolean flag = false;
                        for (String store : stores) {
                            if (!storeIds.contains(store)) {
                                flag = true;
                                break;
                            }
                        }
                        // 筛选出售价为空商品
                        if (flag && pro.getPrcPriceNormalEnd() == null) {
                            errorProPriceList.add(MessageFormat.format("商品{0}, 调后售价不为空", pro.getPrcProId()));
                        }
                    }
            );
            if (!CollectionUtils.isEmpty(error)) {
                Result result = new Result();
                result.setCode(ResultEnum.E0115.getCode());
                result.setData(error);
                return result;
            }
            if (!CollectionUtils.isEmpty(errorProPriceList)) {
                Result result = new Result();
                result.setCode(ResultEnum.E0115.getCode());
                result.setData(errorProPriceList);
                return result;
            }
            List<PriceGroupProductVO> newPro = dto.getPros().stream()
                    .filter(nowPro -> !existPro.contains(nowPro.getPrcProId()))
                    .collect(Collectors.toList());
            List<PriceGroupProductVO> oldPro = dto.getPros().stream()
                    .filter(nowPro -> existPro.contains(nowPro.getPrcProId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < newPro.size(); i++) {
                PriceGroupProductVO item = newPro.get(i);
                item.setClient(user.getClient());
                item.setPrcGroupId(dto.getPrcGroupId());
                item.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                item.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                item.setPrcCreateBy(user.getUserId());
            }

            oldPro.forEach(
                    item -> {
                        item.setClient(user.getClient());
                        item.setPrcGroupId(dto.getPrcGroupId());
                        item.setPrcChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                        item.setPrcChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                        if (item.getPrcPriceNormalEnd() == null) {
                            item.setPrcPriceNormalEnd(item.getPrcPriceNormal());
                        }
                        if (item.getPrcPriceHyEnd() == null) {
                            item.setPrcPriceHyEnd(item.getPrcPriceHy());
                        }
                        if (item.getPrcPriceHyrEnd() == null) {
                            item.setPrcPriceHyrEnd(item.getPrcPriceHyr());
                        }
                    }
            );
            if (CollectionUtils.isNotEmpty(oldPro)) {
                gaiaPriceGroupMapper.updateGroupProBatch(oldPro);
            }
            if (CollectionUtils.isNotEmpty(newPro)) {
                gaiaPriceGroupMapper.insertPriceProBatch(newPro);
            }
            /**
             * 调价单插入
             */
            String yymmdd = LocalDate.now().toString().replace("-", "");
            //  主键: 生成调价单号  YYYYMMDD+4位流水
            // 获取当前最大流水
            String currentMaxNo = gaiaRetailPriceMapper.selectMaxModifNo(dto.getClient(), yymmdd);
            String prcModfiyNo = yymmdd + "0001";
            if (currentMaxNo != null) {
                prcModfiyNo = yymmdd + (String.format("%04d", Integer.parseInt(currentMaxNo.substring(currentMaxNo.length() - 4)) + 1));
            }
            String finalPrcModfiyNo = prcModfiyNo;
            List<String> proAllIds = dto.getPros().stream().map(PriceGroupProductVO::getPrcProId).collect(Collectors.toList());
            List<GaiaProductBusiness> allBus = gaiaProductBusinessMapper.getAllProductBusiness(user.getClient(), proAllIds, stores);
            List<GaiaSdProductPrice> storeProNew = new ArrayList<>();
            Map<String, String> maps = new HashMap<>();
            for (String store : stores) {
                for (PriceGroupProductVO item : dto.getPros()) {
                    List<GaiaProductBusiness> one = allBus.stream().filter(u -> u.getProSelfCode().equals(item.getPrcProId()) && u.getProSite().equals(store)).collect(Collectors.toList());
                    if (CollectionUtils.isEmpty(one)) {
                        continue;
                    }
                    GaiaSdProductPriceVo gaiaSdProductPrice = new GaiaSdProductPriceVo();
                    gaiaSdProductPrice.setClient(user.getClient());
                    gaiaSdProductPrice.setGsppProId(item.getPrcProId());
                    gaiaSdProductPrice.setGsppPriceNormal(item.getPrcPriceNormalEnd());
                    gaiaSdProductPrice.setGsppPriceHy(item.getPrcPriceHyEnd());
                    gaiaSdProductPrice.setGsppPriceHyr(item.getPrcPriceHyrEnd());
                    gaiaSdProductPrice.setProSlaeClass(item.getProSlaeClass());
                    gaiaSdProductPrice.setProZdy1(item.getProZdy1());
                    gaiaSdProductPrice.setProZdy2(item.getProZdy2());
                    gaiaSdProductPrice.setProZdy3(item.getProZdy3());
                    gaiaSdProductPrice.setProZdy4(item.getProZdy4());
                    gaiaSdProductPrice.setProZdy5(item.getProZdy5());
                    gaiaSdProductPrice.setGsppBrId(store);
                    storeProNew.add(gaiaSdProductPrice);

                    String currentVoucherId = maps.get(store);
                    GaiaSdMessage sdMessage = new GaiaSdMessage();
                    sdMessage.setClient(user.getClient());
                    // 调价门店
                    sdMessage.setGsmId(store);
                    // 用来查询前缀
                    sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear());
                    if (org.apache.commons.lang3.StringUtils.isBlank(currentVoucherId)) {
                        currentVoucherId = gaiaSdMessageMapper.getCurrentVoucherId(sdMessage);
                    }
                    long voucherId = NumberUtils.toLong(currentVoucherId.substring(6));
                    while (redisClient.exists(StringUtils.parse(CommonConstants.GAIA_SD_MESSAGE_GSM_VOUCHER_ID, user.getClient(), store, String.valueOf(voucherId)))) {
                        voucherId++;
                    }
                    redisClient.set(StringUtils.parse(CommonConstants.GAIA_SD_MESSAGE_GSM_VOUCHER_ID, user.getClient(), store, String.valueOf(voucherId)), String.valueOf(voucherId), 1800);
                    sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear() + StringUtils.leftPad(String.valueOf(voucherId), 9, "0"));
                    maps.put(store, sdMessage.getGsmVoucherId());
                    sdMessage.setGsmValue("praAdjustNo=" + sdMessage.getGsmVoucherId() + "&praStore=" + sdMessage.getGsmId());
                    sdMessage.setGsmRemark(MessageFormat.format("您有新的商品调价信息,商品编码为[{0}],请及时查看", item.getPrcProId()));
                    sdMessage.setGsmPlatform("FX");
                    // 是否查看 默认为N-未查看
                    sdMessage.setGsmFlag("N");
                    // 调价单号
                    sdMessage.setGsmBusinessVoucherId(finalPrcModfiyNo);
                    sdMessage.setGsmArriveDate(DateUtils.getCurrentDateStrYYMMDD());
                    sdMessage.setGsmArriveTime(DateUtils.getCurrentTimeStrHHMMSS());
                    sdMessage.setGsmPage("pricingList");
                    sdMessages.add(sdMessage);

                    if (item.getPrcPriceNormalEnd() != null) {
                        GaiaRetailPrice price = new GaiaRetailPrice();
                        price.setClient(user.getClient());
                        price.setPrcStore(store);
                        price.setPrcProduct(item.getPrcProId());
                        price.setPrcClass("P001");
                        // 调价单号
                        price.setPrcModfiyNo(finalPrcModfiyNo);
                        // 审批状态默认为否
                        price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                        // 修改之后价格 (9,4)
                        price.setPrcAmount(item.getPrcPriceNormalEnd());
                        // 创建日期
                        price.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                        // 创建时间
                        price.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                        price.setPrcSource("0");
                        price.setPrcCreateUser(user.getUserId());
                        retailPrices.add(price);
                    }
                    if (item.getPrcPriceHyEnd() != null) {
                        GaiaRetailPrice price = new GaiaRetailPrice();
                        price.setClient(user.getClient());
                        price.setPrcStore(store);
                        price.setPrcProduct(item.getPrcProId());
                        price.setPrcClass("P002");
                        // 调价单号
                        price.setPrcModfiyNo(finalPrcModfiyNo);
                        // 审批状态默认为否
                        price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                        // 修改之后价格 (9,4)
                        price.setPrcAmount(item.getPrcPriceHyEnd());
                        // 创建日期
                        price.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                        // 创建时间
                        price.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                        price.setPrcSource("0");
                        price.setPrcCreateUser(user.getUserId());
                        retailPrices.add(price);
                    }
                    if (item.getPrcPriceHyrEnd() != null) {
                        GaiaRetailPrice price = new GaiaRetailPrice();
                        price.setClient(user.getClient());
                        price.setPrcStore(store);
                        price.setPrcProduct(item.getPrcProId());
                        price.setPrcClass("P004");
                        // 调价单号
                        price.setPrcModfiyNo(finalPrcModfiyNo);
                        // 审批状态默认为否
                        price.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
                        // 修改之后价格 (9,4)
                        price.setPrcAmount(item.getPrcPriceHyrEnd());
                        // 创建日期
                        price.setPrcCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                        // 创建时间
                        price.setPrcCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                        price.setPrcSource("0");
                        price.setPrcCreateUser(user.getUserId());
                        retailPrices.add(price);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(storeProNew)) {
                List<GaiaSdProductPrice> exist = gaiaPriceGroupMapper.selectStoreProBatch(user.getClient(), storeProNew);
                if (CollectionUtils.isNotEmpty(exist)) {
                    List<GaiaSdProductPrice> olds = new ArrayList<>();
                    List<GaiaSdProductPrice> news = new ArrayList<>();
                    storeProNew.forEach(
                            item -> {
                                int count = 0;
                                for (GaiaSdProductPrice one : exist) {
                                    if (item.getGsppBrId().equalsIgnoreCase(one.getGsppBrId()) && item.getGsppProId().equalsIgnoreCase(one.getGsppProId())) {
                                        if (item.getGsppPriceNormal() == null) {
                                            item.setGsppPriceNormal(one.getGsppPriceNormal());
                                        }
                                        if (item.getGsppPriceHy() == null) {
                                            item.setGsppPriceHy(one.getGsppPriceHy());
                                        }
                                        if (item.getGsppPriceHyr() == null) {
                                            item.setGsppPriceHyr(one.getGsppPriceHyr());
                                        }
                                        count = count + 1;
                                    }
                                }
                                if (count == 0) {
                                    news.add(item);
                                } else {
                                    if (item.getGsppPriceNormal() != null || item.getGsppPriceHy() != null || item.getGsppPriceHyr() != null) {
                                        olds.add(item);
                                    }
                                }
                            }
                    );
                    if (CollectionUtils.isNotEmpty(news)) {
                        gaiaPriceGroupMapper.insertStoreProBatch(news);
                    }
                    if (CollectionUtils.isNotEmpty(olds)) {
                        gaiaPriceGroupMapper.updateStoreProBatch(olds);
                    }
                } else {
                    gaiaPriceGroupMapper.insertStoreProBatch(storeProNew);
                }
            }
            if (CollectionUtils.isNotEmpty(retailPrices)) {
                gaiaRetailPriceMapper.insertList(retailPrices);
            }
            if (CollectionUtils.isNotEmpty(sdMessages)) {
                try {
                    gaiaSdMessageMapper.insertBatch(sdMessages);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            for (PriceGroupProductVO productVO : dto.getPros()) {
                // “销售级别”、“自定义字段1-5“
                if (CollectionUtils.isNotEmpty(retailList)) {
                    List<String> gaiaStoreDataLists = gaiaRetailPriceMapper.selectRetailSiteList(user.getClient(), retailList);
                    if (CollectionUtils.isNotEmpty(gaiaStoreDataLists)) {
                        List<Map<String, String>> itemList = new ArrayList<>();
                        List<GaiaProductBusinessKey> keyList = new ArrayList<>();
                        gaiaStoreDataLists.forEach(item -> {
                            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                            gaiaProductBusinessKey.setClient(user.getClient());
                            gaiaProductBusinessKey.setProSite(item);
                            gaiaProductBusinessKey.setProSelfCode(productVO.getPrcProId());
                            keyList.add(gaiaProductBusinessKey);
                        });
                        // 销售级别
                        String prcSlaeClass = productVO.getProSlaeClass();
                        if (StringUtils.isNotBlank(prcSlaeClass)) {
                            Map<String, String> item = new HashMap<>();
                            item.put("val", prcSlaeClass);
                            item.put("field", "PRO_SLAE_CLASS");
                            itemList.add(item);
                        }
                        // 自定义字段1
                        String prcZdy1 = productVO.getProZdy1();
                        if (StringUtils.isNotBlank(prcZdy1)) {
                            Map<String, String> item = new HashMap<>();
                            item.put("val", prcZdy1);
                            item.put("field", "PRO_ZDY1");
                            itemList.add(item);
                        }
                        // 自定义字段2
                        String prcZdy2 = productVO.getProZdy2();
                        if (StringUtils.isNotBlank(prcZdy2)) {
                            Map<String, String> item = new HashMap<>();
                            item.put("val", prcZdy2);
                            item.put("field", "PRO_ZDY2");
                            itemList.add(item);
                        }
                        // 自定义字段3
                        String prcZdy3 = productVO.getProZdy3();
                        if (StringUtils.isNotBlank(prcZdy3)) {
                            Map<String, String> item = new HashMap<>();
                            item.put("val", prcZdy3);
                            item.put("field", "PRO_ZDY3");
                            itemList.add(item);
                        }
                        // 自定义字段4
                        String prcZdy4 = productVO.getProZdy4();
                        if (StringUtils.isNotBlank(prcZdy4)) {
                            Map<String, String> item = new HashMap<>();
                            item.put("val", prcZdy4);
                            item.put("field", "PRO_ZDY4");
                            itemList.add(item);
                        }
                        // 自定义字段5
                        String prcZdy5 = productVO.getProZdy5();
                        if (StringUtils.isNotBlank(prcZdy5)) {
                            Map<String, String> item = new HashMap<>();
                            item.put("val", prcZdy5);
                            item.put("field", "PRO_ZDY5");
                            itemList.add(item);
                        }
                        if (CollectionUtils.isNotEmpty(itemList) && CollectionUtils.isNotEmpty(keyList)) {
                            gaiaProductBusinessMapper.updateProductItemsByKey(user.getClient(), productVO.getPrcProId(), itemList, keyList);
                        }
                    }
                }
            }
        }
        return ResultUtil.success();
    }

    private final String[] headList = {
            "申请日期",
            "商品编码",
            "商品名称",
            "通用名称",
            "规格",
            "生产厂家",
            "单位",
            "原零售价",
            "申请零售价",
            "状态"
    };

    /**
     * 根据价格类型 设置价格
     *
     * @param gaiaSdProductPrice
     * @param gaiaRetailPrice
     */
    private void setAmount(GaiaSdProductPrice gaiaSdProductPrice, GaiaRetailPrice gaiaRetailPrice) {
        if (CommonEnum.DictionaryStaticData.PRC_CLASS.getList().get(0).getValue().equals(gaiaRetailPrice.getPrcClass())) {
            gaiaSdProductPrice.setGsppPriceNormal(gaiaRetailPrice.getPrcAmount());
        } else if (CommonEnum.DictionaryStaticData.PRC_CLASS.getList().get(1).getValue().equals(gaiaRetailPrice.getPrcClass())) {
            gaiaSdProductPrice.setGsppPriceHy(gaiaRetailPrice.getPrcAmount());
        } else if (CommonEnum.DictionaryStaticData.PRC_CLASS.getList().get(2).getValue().equals(gaiaRetailPrice.getPrcClass())) {
            gaiaSdProductPrice.setGsppPriceYb(gaiaRetailPrice.getPrcAmount());
        } else if (CommonEnum.DictionaryStaticData.PRC_CLASS.getList().get(3).getValue().equals(gaiaRetailPrice.getPrcClass())) {
            gaiaSdProductPrice.setGsppPriceHyr(gaiaRetailPrice.getPrcAmount());
        } else if (CommonEnum.DictionaryStaticData.PRC_CLASS.getList().get(4).getValue().equals(gaiaRetailPrice.getPrcClass())) {
            gaiaSdProductPrice.setGsppPriceCl(gaiaRetailPrice.getPrcAmount());
        } else if (CommonEnum.DictionaryStaticData.PRC_CLASS.getList().get(5).getValue().equals(gaiaRetailPrice.getPrcClass())) {
            gaiaSdProductPrice.setGsppPriceOnlineNormal(gaiaRetailPrice.getPrcAmount());
        } else {
            gaiaSdProductPrice.setGsppPriceOnlineHy(gaiaRetailPrice.getPrcAmount());
        }
    }

}
