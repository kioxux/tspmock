package com.gov.purchase.module.store.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.module.store.dto.price.*;
import com.gov.purchase.module.store.service.PriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "零售价格")
@EnableAsync
@RestController
@RequestMapping("retailPrice")
public class RetailPriceController {

    @Autowired
    PriceService priceService;
    @Resource
    private OperationFeignService operationFeignService;


    @Log("价格组列表")
    @ApiOperation("价格组列表")
    @PostMapping("queryPriceGroupList")
    public Result queryPriceGroupList(@Valid @RequestBody PriceGroupListRequestDto dto) {
        return ResultUtil.success(priceService.queryPriceGroupList(dto));
    }

    @Log("价格组获取门店列表")
    @ApiOperation("价格组获取门店列表")
    @PostMapping("queryPriceStoreList")
    public Result queryPriceStoreList(@Valid @RequestBody PriceStoreListRequestDto dto) {
        return ResultUtil.success(priceService.queryPriceStoreList(dto));
    }

    @Log("根据商品名称筛选")
    @ApiOperation("根据商品名称筛选")
    @PostMapping("queryProductBasicList")
    public Result queryProductBasicList(@Valid @RequestBody ProductBasicListRequestDto dto) {
        return ResultUtil.success(priceService.queryProductBasicList(dto));
    }

    @Log("调价申请验证")
    @ApiOperation("调价申请验证")
    @PostMapping("validRetailPriceInfo")
    public Result validRetailPriceInfo(@RequestBody RetailPriceInfoSaveRequestDto dto) {
        return priceService.validRetailPriceInfo(dto);
    }

    @Log("调价申请提交")
    @ApiOperation("调价申请提交")
    @PostMapping("saveRetailPriceInfo")
    public Result saveRetailPriceInfo(@RequestBody RetailPriceInfoSaveRequestDto dto) {
        return priceService.saveRetailPriceInfo(dto);
    }

    @Log("调价单审批列表")
    @ApiOperation("调价单审批列表")
    @PostMapping("queryPriceApprovalList")
    public Result queryPriceApprovalList(@RequestBody PriceApprovalListRequestDto dto) {
        return ResultUtil.success(priceService.queryPriceApprovalList(dto));
    }

    @Log("调价门店列表")
    @ApiOperation("调价门店列表")
    @PostMapping("queryStoreList")
    public Result queryStoreList(@RequestJson("prcModfiyNo") String prcModfiyNo, @RequestJson("proSelfCode") String proSelfCode) {
        return priceService.queryStoreList(prcModfiyNo, proSelfCode);
    }

    @Log("调价单审批")
    @ApiOperation("调价单审批")
    @PostMapping("approvePriceList")
    public Result approvePriceList(@RequestBody List<PriceApprovalListReponseDto> list) {
        Map<String, Object> map = new HashMap<>();
        priceService.approvePriceList(list, map);
        operationFeignService.multipleStoreProPriceSync(map.get("client").toString(), (List<String>) map.get("site"), (List<String>) map.get("proIds"));
        return ResultUtil.success();
    }

    @Log("调价单审批拒绝")
    @ApiOperation("调价单审批拒绝")
    @PostMapping("refusePriceList")
    public Result refusePriceList(@RequestBody List<PriceApprovalListReponseDto> list) {
        priceService.refusePriceList(list);
        return ResultUtil.success();
    }

    @Log("调价单查看列表")
    @ApiOperation("调价单查看列表")
    @PostMapping("queryPriceList")
    public Result queryPriceList(@RequestBody PriceListRequestDto dto) {
        return ResultUtil.success(priceService.queryPriceList(dto));
    }

    @Log("当前用户商品组以及权限下的门店列表")
    @ApiOperation("当前用户商品组以及权限下的门店列表")
    @PostMapping("getCurrentUserAuthStoreList")
    public Result getCurrentUserAuthStoreList(@RequestJson(value = "prcGroupId", name = "价格组ID", required = false) String prcGroupId) {
        return ResultUtil.success(priceService.getCurrentUserAuthStoreList(prcGroupId));
    }

    @Log("调价单明细")
    @ApiOperation("调价单明细")
    @PostMapping("getStoreProductPriceList")
    public Result getStoreProductPriceList(@RequestJson(value = "client", name = "加盟商") String client,
                                           @RequestJson(value = "proSelfCode", name = "商品编码") String proSelfCode,
                                           @RequestJson(value = "prcClass", name = "价格类型") String prcClass,
                                           @RequestJson(value = "prcModfiyNo", name = "调价单号") String prcModfiyNo) {
        return ResultUtil.success(priceService.getStoreProductPriceList(client, proSelfCode, prcClass, prcModfiyNo));
    }

    @Log("调价单查看导出")
    @ApiOperation(value = "调价单查看导出", produces = "application/octet-stream")
    @PostMapping("excelPriceList")
    public void excelPriceList(@RequestBody PriceListRequestDto dto) {
        try {
            priceService.excelPriceList(dto);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Log("门店调价商品选择")
    @ApiOperation(value = "门店调价商品选择")
    @PostMapping("getProductListBySto")
    public Result getProductListBySto(@RequestJson(value = "params", required = false) String params,
                                      @RequestJson(value = "inStock", required = false) Integer inStock,
                                      @RequestJson(value = "prcClass",required = false, name = "价格类型") String prcClass,
                                      @RequestJson(value = "labStore", required = false) String labStore) {
        return priceService.getProductListBySto(params, inStock, labStore, prcClass);
    }

    @Log("门店调价提交")
    @ApiOperation(value = "门店调价提交")
    @PostMapping("saveStoreRetailPriceInfo")
    public Result saveStoreRetailPriceInfo(@RequestBody ProductPriceDto dto) {
        return priceService.saveStoreRetailPriceInfo(dto);
    }

    @Log("门店调价查询")
    @ApiOperation(value = "门店调价查询")
    @PostMapping("selectStoreRetailPriceList")
    public Result selectStoreRetailPriceList(@RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                             @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                             @RequestJson(value = "prcSource", required = false) String prcSource,
                                             @RequestJson(value = "prcEffectDate", required = false) String prcEffectDate,
                                             @RequestJson(value = "proKey", required = false) String proKey,
                                             @RequestJson(value = "prcApprovalSuatus", required = false) String prcApprovalSuatus,
                                             @RequestJson(value = "praAdjustNo", required = false) String praAdjustNo) {
        return priceService.selectStoreRetailPriceList(pageNum, pageSize, prcSource, prcEffectDate, proKey, prcApprovalSuatus, praAdjustNo);
    }

    @Log("门店调价导出")
    @ApiOperation(value = "门店调价导出")
    @PostMapping("exportStoreRetailPriceList")
    public Result exportStoreRetailPriceList(@RequestJson(value = "prcSource", required = false) String prcSource,
                                             @RequestJson(value = "prcEffectDate", required = false) String prcEffectDate,
                                             @RequestJson(value = "proKey", required = false) String proKey,
                                             @RequestJson(value = "prcApprovalSuatus", required = false) String prcApprovalSuatus,
                                             @RequestJson(value = "praAdjustNo", required = false) String praAdjustNo) {
        return priceService.exportStoreRetailPriceList(prcSource, prcEffectDate, proKey, prcApprovalSuatus, praAdjustNo);
    }

    @Log("商品门店价格")
    @ApiOperation(value = "商品门店价格")
    @PostMapping("getProPriceList")
    public Result getProPriceList(@RequestJson("proSelfCode") String proSelfCode) {
        return priceService.getProPriceList(proSelfCode);
    }

    @Log("价格组门店保存")
    @ApiOperation(value = "价格组门店保存")
    @PostMapping("savePriceGroupStore")
    public Result savePriceGroupStore(@RequestBody PriceGroupProductDTO dto) {
        return priceService.savePriceGroupAndStore(dto);
    }

    @Log("价格组及其门店列表")
    @ApiOperation("价格组及其门店列表")
    @PostMapping("queryPriceGroupAndStoreList")
    public Result queryPriceGroupAndStoreList() {
        return priceService.queryPriceGroupAndStoreList();
    }

    @Log("价格组商品明细")
    @ApiOperation("价格组商品明细")
    @PostMapping("queryPriceGroupProList")
    public Result queryPriceGroupProList(@RequestJson(value = "prcGroupId") String prcGroupId) {
        return ResultUtil.success(priceService.queryPriceGroupProList(prcGroupId));
    }

    @Log("删除价格组")
    @ApiOperation("删除价格组")
    @PostMapping("delPriceGroup")
    public Result delPriceGroup(@RequestJson(value = "prcGroupId") String prcGroupId) {
        priceService.delPriceGroup(prcGroupId);
        return ResultUtil.success();
    }

    @Log("价格组商品明细保存")
    @ApiOperation(value = "价格组商品明细保存")
    @PostMapping("savePriceGroupPro")
    public Result savePriceGroupPro(@RequestBody PriceGroupProductDTO dto) {
        return priceService.savePriceGroupPro(dto);
    }
}
