package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.21
 */
@Data
public class SearchReplenishReq extends Pageable {
    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;
    /**
     * 铺货订单号
     */
    private String gsrhVoucherId;
    /**
     * 配送中心
     */
    @ApiModelProperty(value = "配送中心", name = "gsrhAddr", required = true)
    @NotBlank(message = "配送中心不能为空")
    private String gsrhAddr;
    /**
     * 铺货日期
     */
    private String gsrhDate;
    /**
     * 铺货起始日期
     */
    private String gsrhStartDate;
    /**
     * 铺货截止日期
     */
    private String gsrhEndDate;
    /**
     * 铺货门店
     */
    private String gsrhBrId;
    /**
     * 商品编码
     */
    private String gsrdProId;
    /**
     * 铺货人
     */
    private String gsrhEmp;
    /**
     * 铺货状态
     */
    private String gsrhStatus;

}

