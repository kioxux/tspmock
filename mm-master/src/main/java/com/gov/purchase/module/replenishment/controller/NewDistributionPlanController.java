package com.gov.purchase.module.replenishment.controller;

import com.alibaba.fastjson.JSON;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaNewDistributionPlan;
import com.gov.purchase.module.replenishment.service.DistributionPlanJobService;
import com.gov.purchase.module.replenishment.service.DistributionPlanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/***
 * @desc: 新品铺货计划
 * @author: ryan
 * @createTime: 2021/6/16 9:37
 **/
@Slf4j
@RestController
@RequestMapping("/api/distributionPlan")
public class NewDistributionPlanController {
    @Resource
    private DistributionPlanService distributionPlanService;
    @Resource
    private DistributionPlanJobService distributionPlanJobService;

    @PostMapping("confirmCall")
    public Result confirmCall(@RequestBody GaiaNewDistributionPlan distributionPlan) {
        log.info(String.format("<新品铺货><确认调取><请求参数：%s>", JSON.toJSONString(distributionPlan)));
        distributionPlanService.confirmCall(distributionPlan);
        return ResultUtil.success();
    }

    @PostMapping("confirmReplenish")
    public Result confirmReplenish(@RequestBody GaiaNewDistributionPlan distributionPlan) {
        log.info(String.format("<新品铺货><确认补铺><请求参数：%s>", JSON.toJSONString(distributionPlan)));
        GaiaNewDistributionPlan cond = new GaiaNewDistributionPlan();
        cond.setPlanCode(distributionPlan.getPlanCode());
        distributionPlan= distributionPlanService.getUnique(cond);
        distributionPlanService.confirmReplenish(distributionPlan);
        return ResultUtil.success();
    }

    @GetMapping("testJob")
    public Result testJob(@RequestParam("date") String date) {
        distributionPlanJobService.autoExecPlan(date);
        return ResultUtil.success();
    }

}
