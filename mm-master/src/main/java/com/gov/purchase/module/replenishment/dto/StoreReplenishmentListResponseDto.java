package com.gov.purchase.module.replenishment.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class StoreReplenishmentListResponseDto {

    /**
     * 加盟商
     */
    private String client;
    /**
     * 补货门店
     */
    private String gsrhBrId;
    /**
     * 补货单号
     */
    private String gsrhVoucherId;
    /**
     * 行号
     */
    private String gsrdSerial;
    /**
     * 补货日期
     */
    private String gsrhDate;
    /**
     * 门店名
     */
    private String stoName;

    /**
     * 连锁总部
     */
    private String stoChainHead;
    /**
     * 配送中心
     */
    private String gsrhAddr;
    /**
     * 商品编号
     */
    private String gsrdProId;
    /**
     * 商品名
     */
    private String proName;
    /**
     * 商品分类
     */
    private String proClass;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 单位名
     */
    private String unitName;
    /**
     * 中包装量
     */
    private String proMidPackage;

    /**
     * 大仓库存
     */
    private BigDecimal dcStockTotal;

    /**
     * 门店库存
     */
    private BigDecimal storeStockTotal;

    /**
     * 总库存量
     */
    private BigDecimal stockTotal;
    /**
     * 最近一次进货日期
     */
    private String lastPurchaseDate;

    /**
     * 最后进货供应商自编码
     */
    private String lastSupplierId;


    /**
     * 供应商送货前置期
     */
    private Integer supLeadTime;

    /**
     * 最后进货供应商
     */
    private String lastSupplier;

    /**
     * 付款条款
     */
    private String supPayTerm;

    /**
     * 末次进货价
     */
    private BigDecimal lastPurchasePrice;

    /**
     * 30天门店销售量
     */
    private BigDecimal daysSalesCount;

    /**
     * 30天对外批发量
     */
    private BigDecimal daysWholesaleCount;

    /**
     * 在途量
     */
    private BigDecimal trafficCount;

    /**
     * 待出量
     */
    private BigDecimal waitCount;

    /**
     * 补货门店数
     */
    private Integer storeCount;

    /**
     * 预计到货时间
     */
    private String poDeliveryDate;

    /**
     * 补货系数
     */
    private BigDecimal replenishmentRatio;

    /**
     * 建议补货量
     */
    private BigDecimal replenishmentValue;

    /**
     * 税率
     */
    private String proInputTax;
    /**
     * 税率
     */
    private String taxCodeName;

    /**
     * 供应商
     */
    List<SupplierResponseDto> supplierResponseDtoList;

    /**
     * 供应商编码
     */
    private String supplierId;

    /**
     * 供应商名称
     */
    private String supplier;

    /**
     * 采购价格
     */
    private BigDecimal purchasePrice;
}
