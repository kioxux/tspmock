package com.gov.purchase.module.purchase.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.purchase.dto.GaiaContractZDetails;
import com.gov.purchase.module.purchase.dto.PurchaseContractDTO;
import com.gov.purchase.module.purchase.dto.PurchaseContractVO;

import java.util.List;

/**
 * 采购合同
 *
 * @author tl
 */
public interface PurchaseContractService {

    /**
     * 作废合同
     *
     * @param conId
     * @return
     */
    Result invalidPurchaseContract(String conId);

    /**
     * 合同明细
     *
     * @return
     */
    Result queryPurchaseContractDetail(String conId);

    /**
     * 合同列表
     *
     * @return
     */
    Result queryPurchaseContractRecord(String conCompanyCode, String conSupplierId, String conSalesmanCode);

    /**
     * 提交合同
     *
     * @param gaiaContractZDetails
     * @return
     */
    Result submitPurchaseContract(GaiaContractZDetails gaiaContractZDetails);

    Result queryPurchaseContractList(Integer pageSize, Integer pageNum, String conCompanyCode, String conSupplierId, String proSelfCode, String conType, String conCreateDateStart, String conCreateDateEnd);

    /**
     * 拒绝
     *
     * @param list
     */
    void refusePurchaseContract(List<GaiaContractZDetails> list);

    /**
     * 合同审批列表
     *
     * @param pageSize
     * @param pageNum
     * @param conCompanyCode
     * @param conSupplierId
     * @param conSalesmanCode
     * @return
     */
    Result queryPurchaseApproveContractList(Integer pageSize, Integer pageNum, String conCompanyCode, String conSupplierId, String conSalesmanCode);

    /**
     * 同意
     *
     * @param list
     */
    void approvePurchaseContract(List<GaiaContractZDetails> list);

    /**
     * 合同查询导出
     *
     * @param conCompanyCode
     * @param conSupplierId
     * @param proSelfCode
     * @param conType
     * @param conCreateDateStart
     * @param conCreateDateEnd
     * @return
     */
    Result queryPurchaseContractListExport(String conCompanyCode, String conSupplierId, String proSelfCode, String conType, String conCreateDateStart, String conCreateDateEnd);

    /**
     * 审批明细
     *
     * @param conId
     * @param conCompileIndex
     * @return
     */
    Result queryPurchaseApproveContractDetails(String conId, String conCompanyCode, String conSupplierId, Integer conCompileIndex);
}
