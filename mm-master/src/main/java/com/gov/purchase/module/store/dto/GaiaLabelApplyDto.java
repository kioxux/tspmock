package com.gov.purchase.module.store.dto;

import com.gov.purchase.entity.GaiaLabelApply;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.08
 */
@Data
public class GaiaLabelApplyDto extends GaiaLabelApply {

    /**
     * 门店名
     */
    private String stoName;

    /**
     * 门店名
     */
    private String stoShortName;

    /**
     *商品名
     */
    private String proName;
    /**
     * 通用名
     */
    private String proCommonname;
    /**
     * 描述
     */
    private String proDepict;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 厂家
     */
    private String proFactoryName;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 产地
     */
    private String proPlace;

    /**
     * 商品编码逗号拼接
     */
    private String proCodeStr;
}
