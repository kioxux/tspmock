package com.gov.purchase.module.priceCompare.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.priceCompare.dto.PriceCompareDto;
import com.gov.purchase.module.priceCompare.dto.PriceComparePageDto;
import com.gov.purchase.module.priceCompare.vo.PriceCompareProInfoVo;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface PriceCompareService {


    void save(@Valid PriceCompareDto param);

    PageInfo page(PriceComparePageDto param);

    void offerEndTime(Long id, String offerEndTime, Boolean isExpires);

    Map info(Long id);

    void infoSave(Long priceCompareId, List<PriceCompareProInfoVo> params, int isOrder);

    Result infoSure(Long priceCompareId, List<PriceCompareProInfoVo> params);

    void listExportExcel(PriceComparePageDto param, HttpServletResponse response) throws IOException;

    void infoExportExcel(Long priceCompareId, HttpServletResponse response) throws IOException;


    void updateCompareInvalid();
}
