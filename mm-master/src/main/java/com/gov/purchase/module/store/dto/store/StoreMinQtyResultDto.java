package com.gov.purchase.module.store.dto.store;

import com.gov.purchase.entity.GaiaProductMinDisplay;
import lombok.Data;

@Data
public class StoreMinQtyResultDto extends GaiaProductMinDisplay {

    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 单位
     */
    private String proUnit;
}
