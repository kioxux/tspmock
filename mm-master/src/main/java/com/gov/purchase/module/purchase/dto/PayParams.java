package com.gov.purchase.module.purchase.dto;

import lombok.Data;

@Data
public class PayParams {

    /**
     * 流水号
     */
    private String billNo;

    /**
     * 消息Id,原样返回
     */
    private String msgId;

    /**
     * 支付总金额
     */
    private Integer totalAmount;

    /**
     * 报文请求时间
     */
    private String requestTimestamp;

    /**
     * 账单描述
     */
    private String billDesc;
    /**
     * 银联银行
     */
    private String bankName;
    /**
     * 渠道号
     */
    private String chnlNo;


}
