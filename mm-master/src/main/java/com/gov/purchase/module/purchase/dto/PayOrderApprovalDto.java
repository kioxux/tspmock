package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PayOrderApprovalDto {

    private String client;

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 采购主体
     */
    private String payCompanyCode;

    /**
     * 采购主体
     */
    private String payCompanyName;

    /**
     * 收款企业Code
     */
    private String payOrderPayer;

    /**
     * 收款企业名称
     */
    private String supName;

    /**
     * 付款单号
     */
    private String payOrderId;

    /**
     * 付款日期
     */
    private String payOrderDate;

    /**
     * 支付方式
     */
    private String payOrderMode;

    /**
     * 应付账款余额
     */
    private BigDecimal balance;

    /**
     * 付款总金额
     */
    private BigDecimal payOrderAmt;


    /**
     * 情况说明
     */
    private String payOrderRemark;




}
