package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "地点集合返回参数")
public class GetSiteListResponseDto {

    /**
     * 值
     */
    private String value;

    /**
     * 表示名
     */
    private String label;

    /**
     * 地点名
     */
    private String siteName;

    /**
     * 地点类型
     */
    private String poSubjectType;

}