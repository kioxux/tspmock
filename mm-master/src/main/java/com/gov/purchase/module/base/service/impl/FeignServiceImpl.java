package com.gov.purchase.module.base.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gov.purchase.common.diff.DiffEntityConfig;
import com.gov.purchase.common.diff.DiffEntityInfo;
import com.gov.purchase.common.diff.DiffEntityUtils;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ProcessEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.AuthFeign;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.feign.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.customer.dto.SaveCusList1RequestDTO;
import com.gov.purchase.module.goods.dto.ProductChangeDTO;
import com.gov.purchase.module.purchase.dto.PayOrderApprovalDto;
import com.gov.purchase.module.supplier.dto.GspinfoSubmitRequestDto;
import com.gov.purchase.utils.ContextHolderUtils;
import com.gov.purchase.utils.JsonUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FeignServiceImpl implements FeignService {

    @Resource
    private AuthFeign authFeign;
    @Resource
    private DiffEntityConfig diffEntityConfig;
    @Resource
    private DiffEntityUtils diffEntityUtils;

    /**
     * 创建商品首营工作流
     *
     * @param gspInfo
     * @return
     */
    @Override
    public FeignResult createProductWorkflow(GaiaProductGspinfo gspInfo) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_014.getCode());  // 类型

        StringBuilder builder = new StringBuilder();
        builder.append("商品首营");
        builder.append("-");
        builder.append(gspInfo.getProName());  //商品名
        builder.append("-");
        builder.append(gspInfo.getProSpecs()); //规格
        builder.append("-");
        builder.append(gspInfo.getProRegisterNo());  //批准文号

        param.put("wfTitle", builder.toString());   // 标题
        param.put("wfDescription", builder.toString());  // 描述
        param.put("wfOrder", gspInfo.getProFlowNo());   // 业务单号

        List<ProductGspFeignDto> productGspFeignDtoList = new ArrayList<>();

        ProductGspFeignDto productGspFeignDto = new ProductGspFeignDto();
        BeanUtils.copyProperties(gspInfo, productGspFeignDto);
        productGspFeignDto.setSccj(gspInfo.getProFactoryName()); // 生产厂家
        productGspFeignDto.setWfSite(gspInfo.getProSite()); // 门店/部门/配送中心
        // 商品仓储分区
        String proStorageArea = productGspFeignDto.getProStorageArea();
        if (StringUtils.isNotBlank(proStorageArea)) {
            Dictionary proStorageAreaDic = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA, proStorageArea);
            if (proStorageAreaDic != null) {
                productGspFeignDto.setProStorageArea(productGspFeignDto.getProStorageArea() + "-" + proStorageAreaDic.getLabel());
            }
        }
        // 贮储条件
        if (StringUtils.isNotBlank(productGspFeignDto.getProStorageCondition())) {
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PRO_STORAGE_CONDITION, productGspFeignDto.getProStorageCondition());
            if (dictionary != null) {
                productGspFeignDto.setProStorageConditionName(dictionary.getLabel());
            }
        }

        // 添加到集合中
        productGspFeignDtoList.add(productGspFeignDto);
        param.put("wfDetail", productGspFeignDtoList); // 明细
        param.put("wfSite", gspInfo.getProSite()); // 门店/部门/配送中心
        log.info("商品首营流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("商品首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 创建商品首营工作流
     *
     * @param list
     * @return
     */
    @Override
    public FeignResult createProductWorkflow2(List<GaiaProductGspinfo> list, String proFlowNo) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_014.getCode());  // 类型

        StringBuilder builder = new StringBuilder();
        builder.append("商品首营");
        builder.append("-");
        builder.append(proFlowNo);  //商品名
        param.put("wfTitle", builder.toString());   // 标题
        param.put("wfOrder", proFlowNo);   // 业务单号
        StringBuilder descBuilder = new StringBuilder();
        List<String> proNames = list.stream().map(GaiaProductGspinfo::getProName).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());
        descBuilder.append("商品首营");
        descBuilder.append("-");
        descBuilder.append(proNames.stream().filter(Objects::nonNull).collect(Collectors.joining("/")));
        param.put("wfDescription", descBuilder.toString());  // 描述

        List<ProductGspFeignDto> productGspFeignDtoList = new ArrayList<>();
        for (GaiaProductGspinfo gaiaProductGspinfo : list) {
            ProductGspFeignDto productGspFeignDto = new ProductGspFeignDto();
            BeanUtils.copyProperties(gaiaProductGspinfo, productGspFeignDto);
            productGspFeignDto.setSccj(gaiaProductGspinfo.getProFactoryName()); // 生产厂家
            productGspFeignDto.setWfSite(gaiaProductGspinfo.getProSite()); // 门店/部门/配送中心
            // 商品仓储分区
            String proStorageArea = productGspFeignDto.getProStorageArea();
            if (StringUtils.isNotBlank(proStorageArea)) {
                Dictionary proStorageAreaDic = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA, proStorageArea);
                if (proStorageAreaDic != null) {
                    productGspFeignDto.setProStorageArea(productGspFeignDto.getProStorageArea() + "-" + proStorageAreaDic.getLabel());
                }
            }
            // 贮储条件
            if (StringUtils.isNotBlank(productGspFeignDto.getProStorageCondition())) {
                Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PRO_STORAGE_CONDITION, productGspFeignDto.getProStorageCondition());
                if (dictionary != null) {
                    productGspFeignDto.setProStorageConditionName(dictionary.getLabel());
                }
            }
            // 添加到集合中
            productGspFeignDtoList.add(productGspFeignDto);
        }
        param.put("wfDetail", productGspFeignDtoList); // 明细
        param.put("wfSite", list.get(0).getProSite()); // 门店/部门/配送中心
        log.info("商品首营流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("商品首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 创建商品首营工作流
     *
     * @param list
     * @return
     */
    @Override
    public FeignResult createProductWorkflowNew(List<GaiaProductGspinfo> list, String proFlowNo, String client, String userId) {
        Map<String, Object> param = new HashMap<>();
        param.put("client", client);
        param.put("userId", userId);
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_014.getCode());  // 类型

        StringBuilder builder = new StringBuilder();
        builder.append("商品首营");
        builder.append("-");
        builder.append(proFlowNo);  //商品名
        param.put("wfTitle", builder.toString());   // 标题
        param.put("wfOrder", proFlowNo);   // 业务单号
        List<String> proNames = list.stream().map(GaiaProductGspinfo::getProName).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());
        StringBuilder descBuilder = new StringBuilder();
        descBuilder.append("商品首营");
        descBuilder.append("-");
        descBuilder.append(proNames.stream().filter(Objects::nonNull).collect(Collectors.joining("/")));
        param.put("wfDescription", descBuilder.toString());  // 描述

        List<ProductGspFeignDto> productGspFeignDtoList = new ArrayList<>();
        for (GaiaProductGspinfo gaiaProductGspinfo : list) {
            ProductGspFeignDto productGspFeignDto = new ProductGspFeignDto();
            BeanUtils.copyProperties(gaiaProductGspinfo, productGspFeignDto);
            productGspFeignDto.setSccj(gaiaProductGspinfo.getProFactoryName()); // 生产厂家
            productGspFeignDto.setWfSite(gaiaProductGspinfo.getProSite()); // 门店/部门/配送中心
            // 商品仓储分区
            String proStorageArea = productGspFeignDto.getProStorageArea();
            if (StringUtils.isNotBlank(proStorageArea)) {
                Dictionary proStorageAreaDic = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA, proStorageArea);
                if (proStorageAreaDic != null) {
                    productGspFeignDto.setProStorageArea(productGspFeignDto.getProStorageArea() + "-" + proStorageAreaDic.getLabel());
                }
            }
            // 贮储条件
            if (StringUtils.isNotBlank(productGspFeignDto.getProStorageCondition())) {
                Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PRO_STORAGE_CONDITION, productGspFeignDto.getProStorageCondition());
                if (dictionary != null) {
                    productGspFeignDto.setProStorageConditionName(dictionary.getLabel());
                }
            }
            // 添加到集合中
            productGspFeignDtoList.add(productGspFeignDto);
        }
        param.put("wfDetail", productGspFeignDtoList); // 明细
        param.put("wfSite", list.get(0).getProSite()); // 门店/部门/配送中心
        log.info("商品首营流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflowNew(param);
        log.info("商品首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }


    /**
     * 修改商品主数据工作流
     *
     * @param changeList 修改数据集合
     * @param flowNO     工作流编码
     * @param zdyNameMap 自定义属性名Map
     * @return
     */
    @Override
    public FeignResult createEditProductWorkflow(List<GaiaProductChange> changeList, String flowNO, BeanMap zdyNameMap) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        // 修改字段 注释
        Map<String, String> columnDesMap = diffEntityConfig.getProductDiffPropertyList().stream().filter(Objects::nonNull)
                .collect(Collectors.toMap(DiffEntityInfo::getPropertyTo, DiffEntityInfo::getColumnDes));
        // 动态字典参数
        Map<String, String> exactParams = new HashMap<>();
        Map<String, String> params = new HashMap<>();

        List<ProductChangeDTO> recordList = changeList.stream().map(change -> {
            ProductChangeDTO dto = new ProductChangeDTO();
            BeanUtils.copyProperties(change, dto);
            // 字段名称
            String proChangeField = change.getProChangeField();
            // 配置数据
            DiffEntityInfo diffEntityInfo = diffEntityConfig.getProductNeedTransPropertyMap().get(proChangeField);
            // 翻译
            if (!ObjectUtils.isEmpty(diffEntityInfo)) {
                exactParams.put("CLIENT", change.getClient());
                exactParams.put("PRO_SITE", change.getProSite());
                String valueTo = diffEntityUtils.getLabelByValueFromDic(diffEntityInfo, change.getProChangeTo(), null, exactParams, null);
                dto.setProChangeTo(valueTo);
                String valueFrom = diffEntityUtils.getLabelByValueFromDic(diffEntityInfo, change.getProChangeFrom(), null, exactParams, null);
                dto.setProChangeFrom(valueFrom);
            }
            dto.setChangeColumnDes(columnDesMap.get(proChangeField));
            // 自定义属性名设置
            if (!ObjectUtils.isEmpty(zdyNameMap)) {
                String columnDes = (String) (zdyNameMap.get(proChangeField + "Name"));
                if (StringUtils.isNotBlank(columnDes)) {
                    dto.setChangeColumnDes(columnDes + "(自定义字段)");
                }
            }
            return dto;
        }).collect(Collectors.toList());
        List<String> commonNames = recordList.stream().map(ProductChangeDTO::getProCommonname).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());
        // token
        param.put("token", token);
        // 类型
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_041.getCode());
        // 标题
        param.put("wfTitle", "商品主数据修改");
        // 地点
        param.put("wfSite", recordList.get(0).getProSite());
        // 描述
        param.put("wfDescription", "商品主数据修改-" + commonNames.stream().filter(Objects::nonNull).collect(Collectors.joining("/")));
        // 业务单号
        param.put("wfOrder", flowNO);
        // 明细
        param.put("wfDetail", recordList);
        log.info("商品主数据修改审批流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
//        FeignResult result = new FeignResult();
        log.info("商品主数据修改审批流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 供应商主数据修改工作流创建
     *
     * @param changeList 修改前后对比数据集合
     * @param flowNO     工作流编码
     * @return
     */
    @Override
    public FeignResult createEditSupplierWorkflow(List<GaiaSupplierChange> changeList, String flowNO) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        changeList.forEach(change -> {
            // 字段名称
            String proChangeField = change.getSupChangeField();
            // 配置数据
            DiffEntityInfo diffEntityInfo = diffEntityConfig.getSupplierNeedTransPropertyMap().get(proChangeField);
            // 翻译
            if (!ObjectUtils.isEmpty(diffEntityInfo)) {
                String valueTo = diffEntityUtils.getLabelByValueFromDic(diffEntityInfo, change.getSupChangeTo(), null, null, null);
                change.setSupChangeTo(valueTo);
                String valueFrom = diffEntityUtils.getLabelByValueFromDic(diffEntityInfo, change.getSupChangeFrom(), null, null, null);
                change.setSupChangeFrom(valueFrom);
            }
        });
        List<String> supNames = changeList.stream().map(GaiaSupplierChange::getSupName).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());
        // token
        param.put("token", token);
        // 类型
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_048.getCode());
        // 标题
        param.put("wfTitle", "供应商主数据修改");
        // 地点
        param.put("wfSite", changeList.get(0).getSupSite());
        // 描述
        param.put("wfDescription", "供应商主数据修改-" + supNames.stream().filter(Objects::nonNull).collect(Collectors.joining("/")));
        // 业务单号
        param.put("wfOrder", flowNO);
        // 明细
        param.put("wfDetail", changeList);
        log.info("供应商主数据修改审批流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
//        FeignResult result = new FeignResult();
        log.info("供应商主数据修改审批流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 客户主数据修改工作流
     *
     * @param changeList 修改前后对比数据集合
     * @param flowNO     工作流编码
     * @return
     */
    @Override
    public FeignResult createEditCustomerWorkflow(List<GaiaCustomerChange> changeList, String flowNO) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        changeList.forEach(change -> {
            // 字段名称
            String proChangeField = change.getCusChangeField();
            // 配置数据
            DiffEntityInfo diffEntityInfo = diffEntityConfig.getCustomerNeedTransPropertyMap().get(proChangeField);
            // 翻译
            if (!ObjectUtils.isEmpty(diffEntityInfo)) {
                String valueTo = diffEntityUtils.getLabelByValueFromDic(diffEntityInfo, change.getCusChangeTo(), null, null, null);
                change.setCusChangeTo(valueTo);
                String valueFrom = diffEntityUtils.getLabelByValueFromDic(diffEntityInfo, change.getCusChangeFrom(), null, null, null);
                change.setCusChangeFrom(valueFrom);
            }
        });
        List<String> cusNames = changeList.stream().map(GaiaCustomerChange::getCusName).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());
        // token
        param.put("token", token);
        // 类型
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_049.getCode());
        // 标题
        param.put("wfTitle", "客户主数据修改");
        // 地点
        param.put("wfSite", changeList.get(0).getCusSite());
        // 描述
        param.put("wfDescription", "客户主数据修改-" + cusNames.stream().filter(Objects::nonNull).collect(Collectors.joining("/")));
        // 业务单号
        param.put("wfOrder", flowNO);
        // 明细
        param.put("wfDetail", changeList);
        log.info("客户主数据修改审批流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
//        FeignResult result = new FeignResult();
        log.info("客户主数据修改审批流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 供应商首营流程创建
     *
     * @param gspInfo
     * @return
     */
    @Override
    public FeignResult createSupplierWorkflow(GspinfoSubmitRequestDto gspInfo) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_015.getCode());  // 类型

        StringBuilder builder = new StringBuilder();
        builder.append("供应商首营");
        builder.append("-");
        builder.append(gspInfo.getSupName());  //供应商名称
        builder.append("-");
        builder.append(gspInfo.getSupCreditCode()); // -统一社会信用代码


        param.put("wfTitle", builder.toString());   // 标题
        param.put("wfDescription", builder.toString());  // 描述
        param.put("wfOrder", gspInfo.getSupFlowNo());   // 业务单号

        // 明细
        List<SupplierGspFeignDto> supplierGspFeignDtoList = new ArrayList<>();
        SupplierGspFeignDto supplierGspFeignDto = new SupplierGspFeignDto();
        BeanUtils.copyProperties(gspInfo, supplierGspFeignDto);
        supplierGspFeignDto.setWfSite(gspInfo.getSupSite()); // 门店/部门/配送中心
        supplierGspFeignDtoList.add(supplierGspFeignDto);
        param.put("wfDetail", supplierGspFeignDtoList);
        param.put("wfSite", gspInfo.getSupSite()); // 门店/部门/配送中心
        log.info("供应商首营流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("供应商首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 供应商首营流程创建
     *
     * @param list
     * @return
     */
    @Override
    public FeignResult createSupplierWorkflow2(List<GspinfoSubmitRequestDto> list, String proFlowNo) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_015.getCode());  // 类型

        StringBuilder builder = new StringBuilder();
        builder.append("供应商首营");
        builder.append("-");
        builder.append(proFlowNo);

        param.put("wfTitle", builder.toString());   // 标题
        param.put("wfOrder", proFlowNo);   // 业务单号

        List<String> supNames = list.stream().map(GspinfoSubmitRequestDto::getSupName).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());
        StringBuilder descBuilder = new StringBuilder();
        descBuilder.append("供应商首营");
        descBuilder.append("-");
        descBuilder.append(supNames.stream().filter(Objects::nonNull).collect(Collectors.joining("/")));
        param.put("wfDescription", descBuilder.toString());  // 描述

        // 明细
        List<SupplierGspFeignDto> supplierGspFeignDtoList = new ArrayList<>();
        for (GspinfoSubmitRequestDto gspinfoSubmitRequestDto : list) {
            SupplierGspFeignDto supplierGspFeignDto = new SupplierGspFeignDto();
            BeanUtils.copyProperties(gspinfoSubmitRequestDto, supplierGspFeignDto);
            supplierGspFeignDto.setWfSite(gspinfoSubmitRequestDto.getSupSite()); // 门店/部门/配送中心
            supplierGspFeignDtoList.add(supplierGspFeignDto);
        }
        param.put("wfDetail", supplierGspFeignDtoList);
        param.put("wfSite", list.get(0).getSupSite()); // 门店/部门/配送中心
        log.info("供应商首营流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("供应商首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 客户首营流程创建
     *
     * @param gspinfo
     * @return
     */
    @Override
    public FeignResult createCustomerWorkflow(GaiaCustomerGspinfo gspinfo) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_016.getCode());  // 类型

        StringBuilder builder = new StringBuilder();
        builder.append("批发客户首营");
        builder.append("-");
        builder.append(gspinfo.getCusName());  //客户名称
        builder.append("-");
        builder.append(gspinfo.getCusCreditCode()); // 统一社会信用代码

        param.put("wfTitle", builder.toString());   // 标题
        param.put("wfDescription", builder.toString());  // 描述
        param.put("wfOrder", gspinfo.getCusFlowNo());   // 业务单号

        // 明细
        List<CustomerGspFeignDto> customerGspFeignDtoList = new ArrayList<>();
        CustomerGspFeignDto customerGspFeignDto = new CustomerGspFeignDto();
        BeanUtils.copyProperties(gspinfo, customerGspFeignDto);
        customerGspFeignDto.setWfSite(gspinfo.getCusSite()); // 门店/部门/配送中心
        customerGspFeignDtoList.add(customerGspFeignDto);
        param.put("wfDetail", customerGspFeignDtoList);
        param.put("wfSite", gspinfo.getCusSite());
        log.info("客户首营流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("客户首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 批量客户首营流程创建
     */
    @Override
    public FeignResult createCustomerWorkflow2(List<SaveCusList1RequestDTO> customerList, String flowNo) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_016.getCode());  // 类型

        StringBuilder builder = new StringBuilder();
        builder.append("批量批发客户首营");
        builder.append("-");
        List<String> titles = customerList.stream().map(s -> s.getCusName() + " " + (StringUtils.isBlank(s.getCusCreditCode()) ? "" : s.getCusCreditCode()))
                .collect(Collectors.toList());
        builder.append(titles.stream().distinct().collect(Collectors.joining("/")));
//        builder.append("-");
//        builder.append(flowNo);

        param.put("wfTitle", builder.toString());   // 标题
        param.put("wfOrder", flowNo);   // 业务单号

        List<String> cusNames = customerList.stream().map(GaiaCustomerGspinfo::getCusName).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());
        StringBuilder descBuilder = new StringBuilder();
        descBuilder.append("批量批发客户首营");
        descBuilder.append("-");
        descBuilder.append(cusNames.stream().filter(Objects::nonNull).collect(Collectors.joining("/")));
        param.put("wfDescription", descBuilder.toString());  // 描述

        // 明细
        List<CustomerGspFeignDto> customerGspFeignDtoList = new ArrayList<>();
        for (GaiaCustomerGspinfo gspinfo : customerList) {
            CustomerGspFeignDto customerGspFeignDto = new CustomerGspFeignDto();
            BeanUtils.copyProperties(gspinfo, customerGspFeignDto);
            customerGspFeignDto.setWfSite(gspinfo.getCusSite()); // 门店/部门/配送中心
            customerGspFeignDtoList.add(customerGspFeignDto);
        }
        param.put("wfDetail", customerGspFeignDtoList);
        param.put("wfSite", customerList.stream().filter(Objects::nonNull).map(GaiaCustomerGspinfo::getCusSite).collect(Collectors.joining(",")));
        log.info("客户首营流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("客户首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 采购订单流程创建
     *
     * @param gaiaPoHeader
     * @param detailList
     * @return
     */
    @Override
    public FeignResult createPurchaseWorkflow(GaiaPoHeader gaiaPoHeader, List<PurchaseFeignDto> detailList) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_017.getCode());  // 类型
        param.put("wfTitle", "采购订单审批,订单号：" + gaiaPoHeader.getPoId());   // 标题
        param.put("wfDescription", "采购订单审批,订单号：" + gaiaPoHeader.getPoId());  // 描述
        param.put("wfOrder", gaiaPoHeader.getPoFlowNo());   // 业务单号
        // 明细
        param.put("wfDetail", detailList);
        log.info("采购订单审批流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("采购订单审批流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 付款单审批
     *
     * @param payOrderApprovalDto
     * @return
     */
    @Override
    public FeignResult createPayOrderWorkflow(PayOrderApprovalDto payOrderApprovalDto) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        param.put("token", token);  // token
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_018.getCode());  // 类型
        param.put("wfTitle", "付款单审批,订单号：" + payOrderApprovalDto.getPayOrderId());   // 标题
        param.put("wfDescription", "付款单审批,订单号：" + payOrderApprovalDto.getPayOrderId());  // 描述
        param.put("wfOrder", payOrderApprovalDto.getPayOrderId());   // 业务单号
        // 明细
        List<PayOrderFeignDto> payOrderFeignDtoList = new ArrayList<>();
        PayOrderFeignDto payOrderFeignDto = new PayOrderFeignDto();
        payOrderFeignDto.setClient(payOrderApprovalDto.getFrancName());
        payOrderFeignDto.setPayCompanyCode(payOrderApprovalDto.getPayCompanyName());
        payOrderFeignDto.setPayOrderPayer(payOrderApprovalDto.getPayOrderPayer() + "-" + payOrderApprovalDto.getSupName());
        payOrderFeignDto.setPayOrderId(payOrderApprovalDto.getPayOrderId());
        payOrderFeignDto.setPayOrderDate(payOrderApprovalDto.getPayOrderDate());
        // 支付方式
        String mode = "";
        if (StringUtils.isNotBlank(payOrderApprovalDto.getPayOrderMode())) {
            for (CommonEnum.CusPayMode cusPayMode : CommonEnum.CusPayMode.values()) {
                if (payOrderApprovalDto.getPayOrderMode().equals(cusPayMode.getCode())) {
                    mode = cusPayMode.getName();
                    break;
                }
            }
        }
        payOrderFeignDto.setPayOrderMode(mode);
        if (payOrderApprovalDto.getBalance() != null) {
            payOrderFeignDto.setPayOrderPayable(payOrderApprovalDto.getBalance().setScale(2).toString());
        } else {
            payOrderFeignDto.setPayOrderPayable("0.00");
        }

        if (payOrderApprovalDto.getPayOrderAmt() != null) {
            payOrderFeignDto.setPayOrderAmt(payOrderApprovalDto.getPayOrderAmt().setScale(2).toString());
        } else {
            payOrderFeignDto.setPayOrderAmt("0.00");
        }

        payOrderFeignDto.setPayOrderRemark(payOrderApprovalDto.getPayOrderRemark());

        payOrderFeignDtoList.add(payOrderFeignDto);
        param.put("wfDetail", payOrderFeignDtoList);
        log.info("付款单审批流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("付款单审批流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }


    /**
     * 创建商品调价工作流
     *
     * @param
     * @return
     */
    @Override
    public FeignResult createProductAdjustWorkflow(String FlowNo, List<GaiaPriceAdjust> gaiaPriceAdjustList) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }


        param.put("token", token);  // token
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_033.getCode());  // 类型

        StringBuilder builder = new StringBuilder();
        builder.append("商品调价审批");
        builder.append("-");
        builder.append(gaiaPriceAdjustList.get(0).getPraAdjustNo());  //商品调价单号

        param.put("wfTitle", builder.toString());   // 标题
        param.put("wfDescription", builder.toString());  // 描述
        param.put("wfOrder", FlowNo);   // 业务单号


        param.put("wfDetail", gaiaPriceAdjustList); // 明细
        param.put("wfSite", gaiaPriceAdjustList.get(0).getPraStore()); // 门店/部门/配送中心
        log.info("商品调价审批流程！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("商品调价审批流程！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }

    /**
     * 补充首营
     */
    @Override
    public FeignResult createReplenishProductGspWorkflow(GaiaProductGspinfo gspinfo, String flowNo) {
        Map<String, Object> param = new HashMap<>();
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        String title = MessageFormat.format("加盟商[{0}],地点[{1}],商品自编码[{2}]", gspinfo.getClient(), gspinfo.getProSite(), gspinfo.getProSelfCode());
        // token
        param.put("token", token);
        // 类型
        param.put("wfDefineCode", ProcessEnum.GAIA_WF_044.getCode());
        // 标题
        param.put("wfTitle", title);
        // 描述
        param.put("wfDescription", title);
        // 业务单号
        param.put("wfOrder", flowNo);

        GaiaProductGspinfo gspinfoDTO = new GaiaProductGspinfo();
        BeanUtils.copyProperties(gspinfo, gspinfoDTO);
        // 商品仓储分区
        String proStorageArea = gspinfoDTO.getProStorageArea();
        if (StringUtils.isNotBlank(proStorageArea)) {
            Dictionary proStorageAreaDic = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA, proStorageArea);
            if (proStorageAreaDic != null) {
                gspinfoDTO.setProStorageArea(gspinfoDTO.getProStorageArea() + "-" + proStorageAreaDic.getLabel());
            }
        }
        // 明细(只能是数组)
        param.put("wfDetail", Collections.singletonList(gspinfoDTO));
        param.put("wfSite", gspinfoDTO.getProSite());
        log.info("补充首营流程创建！ 提交参数：{}", JsonUtils.beanToJson(param));
        FeignResult result = authFeign.createWorkflow(param);
        log.info("补充首营流程创建！ 返回结果：{}", JsonUtils.beanToJson(result));
        return result;
    }


    /**
     * 获取登录人信息
     */
    @Override
    public TokenUser getLoginInfo() {
        String token = ContextHolderUtils.getRequestHeaderParameter("X-Token");
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("唯一标识不能为空");
        }
        Map<String, Object> param = new HashMap<>();
        param.put("token", token);
        log.info("登录人信息！ 提交参数：{}", JsonUtils.beanToJson(param));
        JSONObject object = authFeign.getLoginInfo(param);
        log.info("登录人信息！ 返回结果：{}", object.toJSONString());
        if (object.isEmpty()) {
            throw new CustomResultException("返回结果异常");
        }
        if (object.containsKey("code")) {
            throw new CustomResultException(object.getString("message"));
        }
        return object.toJavaObject(TokenUser.class);
    }
}
