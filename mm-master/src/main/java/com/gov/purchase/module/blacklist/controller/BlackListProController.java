package com.gov.purchase.module.blacklist.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.blacklist.dto.BatchNoData;
import com.gov.purchase.module.blacklist.service.BlackListProService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Api(tags = "商品黑名单维护")
@RestController
@RequestMapping("blackListPro")
public class BlackListProController {
    @Autowired
    private BlackListProService service;

    @Log("导入黑名单商品列表")
    @ApiOperation("导入黑名单商品列表")
    @PostMapping("importBlackListPro")
    public Result importBlackListPro(@Valid @RequestParam("file") MultipartFile file) {
        service.importBlackListPro(file);
        return ResultUtil.success("导入成功！");
    }

    @Log("查询生产批号列表（支持模糊匹配）")
    @ApiOperation(value = "查询生产批号列表（支持模糊匹配）",response = BatchNoData.class)
    @PostMapping("selectBatchNo")
    public Result selectBatchNo(@Valid @RequestBody BatchNoData inData) {
        return ResultUtil.success(service.selectBatchNo(inData));
    }
}
