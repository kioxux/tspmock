package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel(value = "采购订单查询传入参数")
public class PoHeaderListRequestDto extends Pageable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "poSiteCode")
    private String poSiteCode;

    /**
     * 订单类型
     */
    @ApiModelProperty(value = "订单类型", name = "poType")
    private String poType;

    /**
     * 订单创建人
     */
    @ApiModelProperty(value = "订单创建人", name = "poCreateBy")
    private String poCreateBy;

    /**
     * 采购订单号
     */
    @ApiModelProperty(value = "采购订单号", name = "poId")
    private String poId;
    /**
     * 采购订单号 更多
     */
    @ApiModelProperty(value = "采购订单号更多", name = "poIds")
    private List<String> poIds;

    /**
     * 订单日期开始
     */
    @ApiModelProperty(value = "订单日期开始", name = "poDateStart")
    private String poDateStart;

    /**
     * 订单日期结束
     */
    @ApiModelProperty(value = "订单日期结束", name = "poDateEnd")
    private String poDateEnd;

    /**
     * 商品
     */
    @ApiModelProperty(value = "商品", name = "poProCode")
    private String poProCode;
    /**
     * 商品更多
     */
    @ApiModelProperty(value = "商品更多", name = "poProCodes")
    private List<String> poProCodes;

    /**
     * 供应商
     */
    @ApiModelProperty(value = "供应商", name = "poSupplierId")
    private String poSupplierId;
    /**
     * 供应商更多
     */
    @ApiModelProperty(value = "供应商更多", name = "poSupplierIds")
    private List<String> poSupplierIds;

    /**
     * 订单状态
     */
    @ApiModelProperty(value = "订单状态", name = "poOrderStaus")
    private String poOrderStaus;

    /**
     * 物流模式
     */
    @ApiModelProperty(value = "物流模式", name = "poDeliveryType")
    private String poDeliveryType;

    @ApiModelProperty(value = "审批状态", name = "poApproveStatus")
    private String poApproveStatus;

    @ApiModelProperty(value = "商品分类", name = "proClass")
    private String proClass;

    /**
     * 采购员
     */
    private String proPurchaseRate;
    /**
     * 自定义字段1
     */
    private String proZdy1;
    /**
     * 自定义字段2
     */
    private String proZdy2;
    /**
     * 自定义字段3
     */
    private String proZdy3;
    /**
     * 自定义字段4
     */
    private String proZdy4;
    /**
     * 自定义字段5
     */
    private String proZdy5;
    /**
     * 门店
     */
    private String poDeliveryTypeStore;

    /**
     * 删除
     */
    private String poLineDelete;

    /**
     * 暂存数据
     */
    private Integer draft;

    /**
     * 供应商业务员
     */
    private String poSupplierSalesman;
}