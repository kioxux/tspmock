package com.gov.purchase.module.goods.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class SaveBasicProListRequestDTO {

    /**
     * 商品数据列表
     */
    private List<SaveGaiaProductBusiness> basicsList;

    /**
     * 比对的结果数据
     */
    private List<SaveCompProListRequestDTO> compProList;

    private String status;

    private String client;

    private String userId;

}
