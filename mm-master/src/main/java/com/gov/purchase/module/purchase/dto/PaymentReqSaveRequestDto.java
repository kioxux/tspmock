package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@ApiModel(value = "付款申请保存传入参数")
public class PaymentReqSaveRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 采购主体
     */
    @ApiModelProperty(value = "采购主体", name = "payCompanyCode", required = true)
    @NotBlank(message = "采购主体不能为空")
    private String payCompanyCode;

    /**
     * 付款日期
     */
    @ApiModelProperty(value = "付款日期", name = "payOrderDate", required = true)
    @NotBlank(message = "付款日期不能为空")
    private String payOrderDate;

    /**
     * 付款总金额
     */
    @ApiModelProperty(value = "付款总金额", name = "payOrderAmt")
    private BigDecimal payOrderAmt;

    /**
     * 支付方式
     */
    @ApiModelProperty(value = "支付方式", name = "payOrderMode", required = true)
    @NotBlank(message = "支付方式不能为空")
    private String payOrderMode;

    /**
     * 承兑天数
     */
    @ApiModelProperty(value = "承兑天数", name = "payAcceptDay")
    private String payAcceptDay;

    /**
     * 银行代码
     */
    @ApiModelProperty(value = "银行代码", name = "payBankCode", required = true)
    @NotBlank(message = "银行代码不能为空")
    private String payBankCode;

    /**
     * 银行账号
     */
    @ApiModelProperty(value = "银行账号", name = "payBankAccount", required = true)
    @NotBlank(message = "银行账号不能为空")
    private String payBankAccount;

    /**
     * 情况说明
     */
    @ApiModelProperty(value = "情况说明", name = "payOrderRemark")
    private String payOrderRemark;

    /**
     * 出票方
     */
    @ApiModelProperty(value = "出票方", name = "payOrderPayer", required = true)
    @NotBlank(message = "出票方不能为空")
    private String payOrderPayer;

    /**
     * 会计凭证号
     */
    @ApiModelProperty(value = "会计凭证号", name = "invoiceNum", required = true)
    @NotBlank(message = "会计凭证号不能为空")
    private String invoiceNum;

    /**
     * 行号
     */
    @ApiModelProperty(value = "行号", name = "ivLineNo", required = true)
    @NotBlank(message = "行号不能为空")
    private String ivLineNo;

    /**
     * 本次付款金额
     */
    @ApiModelProperty(value = "本次付款金额 ", name = "paymentAmt")
    private BigDecimal paymentAmt;
}