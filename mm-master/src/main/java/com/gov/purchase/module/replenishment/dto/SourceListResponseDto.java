package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.entity.GaiaSourceList;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SourceListResponseDto extends GaiaSourceList {

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 地点（门店 + 配送中心）
     */
    private String siteName;

    /**
     * 商品名称
     */
    private String proName;
    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 商品描述
     */
    private String proDepict;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 规格
     */
    private String proSpecs;

}
