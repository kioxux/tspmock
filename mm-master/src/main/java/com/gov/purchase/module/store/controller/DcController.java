package com.gov.purchase.module.store.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.store.dto.DcDataListRequestDto;
import com.gov.purchase.module.store.dto.DcDataRequestDto;
import com.gov.purchase.module.store.dto.DcDataStatusRequestDto;
import com.gov.purchase.module.store.service.DcDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "DC维护")
@RestController
@RequestMapping("dc")
public class DcController {

    @Resource
    private DcDataService dataService;
    @Resource
    private FeignService feignService;

    /**
     * 查询DC列表
     *
     * @param dto 查询条件
     * @return
     */
    @Log("DC列表")
    @ApiOperation("DC列表")
    @PostMapping("queryDcList")
    public Result queryDcList(@Valid @RequestBody DcDataListRequestDto dto) {
        return ResultUtil.success(dataService.queryDcList(dto));
    }

    /**
     * DC编辑初始化
     *
     * @param client 加盟商编号
     * @param dcCode DC编号
     * @return
     */
    @Log("DC编辑初始化")
    @ApiOperation("DC编辑初始化")
    @GetMapping("getDcInfo")
    @ApiImplicitParams({
//            @ApiImplicitParam(name = "client", value = "加盟商", paramType = "query", required = true),
            @ApiImplicitParam(name = "dcCode", value = "DC编号", paramType = "query", required = true)
    })
    public Result getDcInfo(@RequestParam("dcCode") String dcCode) {
        TokenUser user = feignService.getLoginInfo();
        return ResultUtil.success(dataService.getDcInfo(user.getClient(), dcCode));
    }


    /**
     * DC校验
     *
     * @param dto 提交字段
     * @return
     */
    @Log("DC校验")
    @ApiOperation("DC校验")
    @PostMapping("validDcInfo")
    public Result validDcInfo(@Valid @RequestBody DcDataRequestDto dto) {
        return dataService.validDcInfo(dto);
    }

    /**
     * DC保存
     *
     * @param dto
     * @return
     */
    @Log("DC保存")
    @ApiOperation("DC保存")
    @PostMapping("saveDcInfo")
    public Result saveDcInfo(@Valid @RequestBody DcDataRequestDto dto) {
        return dataService.saveDcInfo(dto);
    }


    /**
     * DC启用/冻结
     *
     * @param dto
     * @return
     */
    @Log("DC启用/冻结")
    @ApiOperation("DC启用/冻结")
    @PostMapping("updateDcStatus")
    public Result updateDcStatus(@Valid @RequestBody DcDataStatusRequestDto dto) {
        return dataService.updateDcStatus(dto);
    }

    @Log("委托配送中心")
    @ApiOperation("委托配送中心")
    @PostMapping("getEntrustDcList")
    public Result getEntrustDcList(@RequestJson(value = "dcCode", required = false) String dcCode) {
        return dataService.getEntrustDcList(dcCode);
    }

}
