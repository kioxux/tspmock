package com.gov.purchase.module.replenishment.service;

import com.gov.purchase.entity.GaiaSupplierSalesman;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.MaintainProductsListQueryDTO;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.MaintainProductsListSaveDTO;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.SupplierSalesmanProDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface SalesmanMaintainProService {

    // 查询授权商品维护列表
    MaintainProductsListQueryDTO getProductsList(String supSite, String supSelfCode, String gssCode);

    // 保存商品维护列表
    void saveProductsList(MaintainProductsListSaveDTO maintainProductsListSaveDTO);

    // 模糊查询商品明细
    List<SupplierSalesmanProDTO> getProDetails(String supSite, String proInfo,Integer pageSize);

    // 导入商品维护列表
    List<SupplierSalesmanProDTO> importProductsList(String supSite, MultipartFile file);

    // 导出商品维护列表
    void exportProductsList(String supSite, String supSelfCode, String gssCode) throws IOException;

    // 导出模板
    void exportTemplete() throws IOException;

    // 供应商业务员下拉框
    List<GaiaSupplierSalesman> getSupplierSalesManList(String dcCode);

}
