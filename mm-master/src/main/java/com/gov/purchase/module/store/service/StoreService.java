package com.gov.purchase.module.store.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.store.dto.store.*;

import java.util.List;

public interface StoreService {


    /**
     * 门店列表
     *
     * @param
     * @return
     */
    PageInfo<StoreListResponseDto> queryStoreList(StoreListRequestDto dto);

    /**
     * 门店详情
     *
     * @return
     */
    StoreInfoDetailReponseDto getStoreInfo(StoreInfoDetailRequestDto dto);

    /**
     * 更新和新增验证
     *
     * @return
     */
    Result validStoreInfo(StoreInfoInsertOrUpdateValidDto dto);

    /**
     * 新增门店
     *
     * @return
     */
    Result insertStoreInfo(StoreInfoInsertOrUpdateRequestDto dto);

    /**
     * 更新门店
     *
     * @return
     */
    Result updateStoreInfo(StoreInfoInsertOrUpdateRequestDto dto);


    /**
     * 有权限的连锁总部的下拉选
     *
     * @return
     */
    Result queryChainList();


    /**
     * 有权限的门店下拉选
     *
     * @return
     */
    Result queryStoreList(String chainCode);


    /**
     * 有权限的门店列表
     *
     * @return
     */
    Result queryList(String stoChainHead, String stoCode, List<String> stoCodeList, String stoName, Integer pageNo, Integer pageSize);


    /**
     * 有权限的门店列表
     *
     * @return
     */
    Result exportList(String stoChainHead, String stoCode, List<String> stoCodeList, String stoName);


    /**
     * 更新数据
     *
     * @return
     */
    Result updateStoreList(List<GaiaStoreData> storeDataList);

    /**
     * 配送中心
     *
     * @return
     */
    Result getDcList();

    /**
     * 门店详情
     */
    GaiaStoreData getStoDetails(String stoCode);

    /**
     * 门店编辑
     */
    void editStoDetails(EditStoDetailsRequestDTO editStoDetails);

    /**
     * 生成门店二维码
     *
     * @param stoCode
     * @return
     */
    Result getQrCode(String stoCode);
}
