package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "付款单查询返回参数")
public class PayOrderListResponseDto {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 付款单号
     */
    private String payOrderId;

    /**
     * 付款申请单行号
     */
    private String payOrderLineno;

    /**
     * 公司代码
     */
    private String payCompanyCode;

    /**
     * 公司名称
     */
    private String payCompanyName;

    /**
     * 供应商
     */
    private String poSupplierId;

    /**
     * 供应商名
     */
    private String poSupplierName;

    /**
     * 出票方
     */
    private String gaiaPayOrder;

    /**
     * 出票方名称
     */
    private String gaiaPayOrderName;

    /**
     * 单据类型
     */
    private String documentType;

    /**
     * 单据类型名
     */
    private String documentTypeName;

    /**
     * 费用类型
     */
    private String costType;

    /**
     * 费用类型名
     */
    private String costTypeName;

    /**
     * 付款总金额
     */
    private BigDecimal payOrderAmt;

    /**
     * 付款方式
     */
    private String payOrderMode;

    /**
     * 承兑天数
     */
    private String payAcceptDay;

    /**
     * 银行编码
     */
    private String payBankDode;

    /**
     * 账户编码
     */
    private String payBankAccount;

    /**
     * 付款状态
     */
    private String payOrderStatus;

    /**
     * 创建日期
     */
    private String payCreateDate;

    /**
     * 创建时间
     */
    private String payCreateTime;

    /**
     * 创建人
     */
    private String payCreateUser;

    /**
     * 付款日期
     */
    private String payOrderDate;

    /**
     * 情况说明
     */
    private String payOrderRemark;
}