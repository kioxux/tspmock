package com.gov.purchase.module.purchase.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaSoItemMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.SalesOrderRecordDTO;
import com.gov.purchase.module.purchase.dto.SalesOrderRecordVO;
import com.gov.purchase.module.purchase.service.SalesOrderRecordService;
import com.gov.purchase.module.wholesale.dto.SalesOrder;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.EnumUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @description: 销售订单
 * @author: yzf
 * @create: 2021-11-04 14:59
 */
@Service
public class SalesOrderRecordServiceImpl implements SalesOrderRecordService {
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaSoItemMapper gaiaSoItemMapper;
    @Resource
    CosUtils cosUtils;

    /**
     * 查询销售记录明细（或销售退回记录明细）
     *
     * @param salesOrderRecordDTO
     * @return
     */
    @Override
    public PageInfo<SalesOrderRecordVO> querySalesRecord(SalesOrderRecordDTO salesOrderRecordDTO) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        salesOrderRecordDTO.setClient(user.getClient());
        if (StringUtils.isNotBlank(salesOrderRecordDTO.getSaleDate())) {
            salesOrderRecordDTO.setSaleDate(salesOrderRecordDTO.getSaleDate().replace("-", ""));
            salesOrderRecordDTO.setStartDate(salesOrderRecordDTO.getSaleDate().split(",")[0]);
            salesOrderRecordDTO.setEndDate(salesOrderRecordDTO.getSaleDate().split(",")[1]);
        }
        PageHelper.startPage(salesOrderRecordDTO.getPageNum(), salesOrderRecordDTO.getPageSize());
        // 查询销售记录明细（或销售退回记录明细）
        List<SalesOrderRecordVO> lists = gaiaSoItemMapper.queryRecord(salesOrderRecordDTO);
        return new PageInfo<>(lists);
    }

    @Override
    public Result exportSalesOrderRecord(SalesOrderRecordDTO salesOrderRecordDTO) {
        try {
            // 当前用户信息
            TokenUser user = feignService.getLoginInfo();
            salesOrderRecordDTO.setClient(user.getClient());
            if (StringUtils.isNotBlank(salesOrderRecordDTO.getSaleDate())) {
                salesOrderRecordDTO.setSaleDate(salesOrderRecordDTO.getSaleDate().replace("-", ""));
                salesOrderRecordDTO.setStartDate(salesOrderRecordDTO.getSaleDate().split(",")[0]);
                salesOrderRecordDTO.setEndDate(salesOrderRecordDTO.getSaleDate().split(",")[1]);
            }
            List<SalesOrderRecordVO> lists = gaiaSoItemMapper.queryRecord(salesOrderRecordDTO);
            List<List<String>> dataList = new ArrayList<>();
            for (int i = 0; CollectionUtils.isNotEmpty(lists) && i < lists.size(); i++) {
                // 明细数据
                SalesOrderRecordVO salesOrderRecordVO = lists.get(i);
                // 打印行
                List<String> item = new ArrayList<>();
                // 序号
                item.add(String.valueOf(i + 1));
                // 销售日期
                item.add(salesOrderRecordVO.getSoCreateDate());
                // 销售订单
                item.add(salesOrderRecordVO.getSoId());
                // 客户编码
                item.add(salesOrderRecordVO.getSoCustomerId());
                // 客户名称
                item.add(salesOrderRecordVO.getCusName());
                // 商品编码
                item.add(salesOrderRecordVO.getSoProCode());
                // 通用名称
                item.add(salesOrderRecordVO.getProCommonName());
                // 规格
                item.add(salesOrderRecordVO.getProSpecs());
                // 剂型
                item.add(salesOrderRecordVO.getProForm());
                // 批准文号
                item.add(salesOrderRecordVO.getProRegisterNo());
                // 批号
                item.add(salesOrderRecordVO.getSoBatchNo());
                // 有效期
                item.add(salesOrderRecordVO.getBatExpiryDate());
                // 生产厂家
                item.add(salesOrderRecordVO.getProFactoryName());
                // 产地
                item.add(salesOrderRecordVO.getProPlace());
                // 购进单位
                item.add(salesOrderRecordVO.getBatSupplierName());
                // 销售数量
                item.add(salesOrderRecordVO.getSoQty() != null ? salesOrderRecordVO.getSoQty().stripTrailingZeros().toPlainString() : "");
                // 单位
                item.add(salesOrderRecordVO.getProUnit());
                // 销售单价
                item.add(salesOrderRecordVO.getSoPrice() != null ? salesOrderRecordVO.getSoPrice().stripTrailingZeros().toPlainString() : "");
                // 合计金额
                item.add(salesOrderRecordVO.getSoTotal() != null ? salesOrderRecordVO.getSoTotal().stripTrailingZeros().toPlainString() : "");
                // 上市许可持有人
                item.add(salesOrderRecordVO.getProHolder());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("销售记录");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    @Override
    public Result exportSalesOrderBackRecord(SalesOrderRecordDTO salesOrderRecordDTO) {
        try {
            // 当前用户信息
            TokenUser user = feignService.getLoginInfo();
            salesOrderRecordDTO.setClient(user.getClient());
            if (StringUtils.isNotBlank(salesOrderRecordDTO.getSaleDate())) {
                salesOrderRecordDTO.setSaleDate(salesOrderRecordDTO.getSaleDate().replace("-", ""));
                salesOrderRecordDTO.setStartDate(salesOrderRecordDTO.getSaleDate().split(",")[0]);
                salesOrderRecordDTO.setEndDate(salesOrderRecordDTO.getSaleDate().split(",")[1]);
            }
            List<SalesOrderRecordVO> lists = gaiaSoItemMapper.queryRecord(salesOrderRecordDTO);
            List<List<String>> dataList = new ArrayList<>();
            for (int i = 0; CollectionUtils.isNotEmpty(lists) && i < lists.size(); i++) {
                // 明细数据
                SalesOrderRecordVO salesOrderRecordVO = lists.get(i);
                // 打印行
                List<String> item = new ArrayList<>();
                // 序号
                item.add(String.valueOf(i + 1));
                // 退回日期
                item.add(salesOrderRecordVO.getSoCreateDate());
                // 退回订单
                item.add(salesOrderRecordVO.getSoId());
                // 客户编码
                item.add(salesOrderRecordVO.getSoCustomerId());
                // 客户名称
                item.add(salesOrderRecordVO.getCusName());
                // 商品编码
                item.add(salesOrderRecordVO.getSoProCode());
                // 通用名称
                item.add(salesOrderRecordVO.getProCommonName());
                // 规格
                item.add(salesOrderRecordVO.getProSpecs());
                // 剂型
                item.add(salesOrderRecordVO.getProForm());
                // 生产厂家
                item.add(salesOrderRecordVO.getProFactoryName());
                // 产地
                item.add(salesOrderRecordVO.getProPlace());
                // 批号
                item.add(salesOrderRecordVO.getSoBatchNo());
                // 有效期
                item.add(salesOrderRecordVO.getBatExpiryDate());
                // 购进单位
                item.add(salesOrderRecordVO.getBatSupplierName());
                // 退回数量
                item.add(salesOrderRecordVO.getSoQty() != null ? salesOrderRecordVO.getSoQty().stripTrailingZeros().toPlainString() : "");
                // 退回单价
                item.add(salesOrderRecordVO.getSoPrice() != null ? salesOrderRecordVO.getSoPrice().stripTrailingZeros().toPlainString() : "");
                // 退回金额
                item.add(salesOrderRecordVO.getSoTotal() != null ? salesOrderRecordVO.getSoTotal().stripTrailingZeros().toPlainString() : "");
                // 单位
                item.add(salesOrderRecordVO.getProUnit());
                // 原销售单
                item.add(salesOrderRecordVO.getSoReferOrder());
                // 销售日期
                item.add(salesOrderRecordVO.getSoOldCreteDate());
                // 销售单价
                item.add(salesOrderRecordVO.getSoOldPrice() != null ? salesOrderRecordVO.getSoOldPrice().stripTrailingZeros().toPlainString() : "");
                // 上市许可持有人
                item.add(salesOrderRecordVO.getProHolder());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(backHeadList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("销售退回记录");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 业务数据表头
     */
    private String[] headList = {
            "序号",
            "销售日期",
            "销售订单",
            "客户编码",
            "客户名称",
            "商品编码",
            "通用名称",
            "规格",
            "剂型",
            "批准文号",
            "批号",
            "有效期",
            "生产厂家",
            "产地",
            "购进单位",
            "销售数量",
            "单位",
            "销售单价",
            "合计金额",
            "上市许可持有人",
    };

    /**
     * 业务数据表头
     */
    private String[] backHeadList = {
            "序号",
            "退回日期",
            "退回订单",
            "客户编码",
            "客户名称",
            "商品编码",
            "通用名称",
            "规格",
            "剂型",
            "生产厂家",
            "产地",
            "批号",
            "有效期",
            "购进单位",
            "退回数量",
            "退回单价",
            "退回金额",
            "单位",
            "原销售单",
            "销售日期",
            "销售单价",
            "上市许可持有人",
    };
}
