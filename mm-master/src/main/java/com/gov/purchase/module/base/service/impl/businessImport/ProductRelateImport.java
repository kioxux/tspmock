package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductBusinessKey;
import com.gov.purchase.entity.GaiaProductRelate;
import com.gov.purchase.entity.GaiaProductRelateKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaProductRelateMapper;
import com.gov.purchase.module.base.dto.businessImport.ProductRelate;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.24
 */
@Service
public class ProductRelateImport extends BusinessImport {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaProductRelateMapper gaiaProductRelateMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    /**
     * 煎药维护
     *
     * @param map   数据
     * @param field 字段
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 参数 验证
        if (field == null || field.isEmpty()) {
            throw new CustomResultException("参数为空");
        }
        if (field.get("site") == null || StringUtils.isBlank(field.get("site").toString())) {
            throw new CustomResultException("机构不能为空");
        }
        if (field.get("sup") == null || StringUtils.isBlank(field.get("sup").toString())) {
            throw new CustomResultException("供应商不能为空");
        }
        // 机构
        String site = field.get("site").toString();
        // 供应商
        String sup = field.get("sup").toString();
        // 当前用户
        TokenUser user = feignService.getLoginInfo();
        // 错误 消息
        List<String> errorList = new ArrayList<>();
        // 行数据处理 验证
        for (Integer key : map.keySet()) {
            // 验证
            ProductRelate params = (ProductRelate) map.get(key);
            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setClient(user.getClient());
            gaiaProductBusinessKey.setProSite(site);
            gaiaProductBusinessKey.setProSelfCode(params.getProSelfCode());
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
            if (gaiaProductBusiness == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "商品编码"));
                continue;
            }
        }

        // 验证报错
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }

        // 数据处理
        for (Integer key : map.keySet()) {
            // 验证
            ProductRelate params = (ProductRelate) map.get(key);
            // 数据插入
            GaiaProductRelateKey gaiaProductRelateKey = new GaiaProductRelateKey();
            gaiaProductRelateKey.setClient(user.getClient());
            gaiaProductRelateKey.setProSite(site);
            gaiaProductRelateKey.setSupSelfCode(sup);
            gaiaProductRelateKey.setProSelfCode(params.getProSelfCode());
            GaiaProductRelate gaiaProductRelate = gaiaProductRelateMapper.selectByPrimaryKey(gaiaProductRelateKey);
            List<GaiaProductRelate> list = gaiaProductRelateMapper.selectByRelate(user.getClient(), site, params.getProRelateCode(), sup);
            if (gaiaProductRelate == null) {
                // 关联商品已存在
                if (!org.springframework.util.CollectionUtils.isEmpty(list)) {
                    throw new CustomResultException(MessageFormat.format(ResultEnum.E0116.getMsg(), key + 1, "关联商品编码"));
                }
                gaiaProductRelate = new GaiaProductRelate();
                // 加盟商
                gaiaProductRelate.setClient(user.getClient());
                // 地点
                gaiaProductRelate.setProSite(site);
                // 商品编码
                gaiaProductRelate.setProSelfCode(params.getProSelfCode());
                // 供应商编码
                gaiaProductRelate.setSupSelfCode(sup);
                // 关联商品编码
                gaiaProductRelate.setProRelateCode(params.getProRelateCode());
                // 采购价格
                gaiaProductRelate.setProPrice(new BigDecimal(params.getProPrice()));
                // 创建人
                gaiaProductRelate.setProCreateUser(user.getUserId());
                // 创建日期
                gaiaProductRelate.setProCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                // 创建时间
                gaiaProductRelate.setProCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                gaiaProductRelateMapper.insertSelective(gaiaProductRelate);
            } else {
                // 关联商品已存在
                if (!org.springframework.util.CollectionUtils.isEmpty(list) && (list.size() > 1 || !list.get(0).getProSelfCode().equals(params.getProSelfCode()))) {
                    throw new CustomResultException(MessageFormat.format(ResultEnum.E0116.getMsg(), key + 1, "关联商品编码"));
                }
                // 关联商品编码
                gaiaProductRelate.setProRelateCode(params.getProRelateCode());
                // 采购价格
                gaiaProductRelate.setProPrice(new BigDecimal(params.getProPrice()));
                // 变更人
                gaiaProductRelate.setProChangeUser(user.getUserId());
                // 变更日期
                gaiaProductRelate.setProChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                // 变更时间
                gaiaProductRelate.setProChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                gaiaProductRelateMapper.updateByPrimaryKeySelective(gaiaProductRelate);
            }
        }

        return ResultUtil.success();
    }
}
