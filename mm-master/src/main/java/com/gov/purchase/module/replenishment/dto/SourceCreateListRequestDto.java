package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "货源清单创建、修改列表请求参数")
public class SourceCreateListRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "地点", name = "souSiteCode", required = true)
    @NotBlank(message = "地点不能为空")
    private String souSiteCode;

    @ApiModelProperty(value = "商品自编码", name = "souProCode", required = true)
    @NotBlank(message = "商品自编码不能为空")
    private String souProCode;
}
