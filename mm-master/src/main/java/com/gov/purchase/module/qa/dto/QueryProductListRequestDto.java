package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "商品状态更新-获取商品编号列表传入参数")
public class QueryProductListRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client" ,required = true)
    private String client;

    @ApiModelProperty(value = "地点集合", name = "siteList" ,required = true)
    private List<String> siteList;

    @ApiModelProperty(value = "商品编码", name = "proCode" ,required = true)
    private String proCode;

    private String fieldName;
}
