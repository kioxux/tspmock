package com.gov.purchase.module.goods.dto;

import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021-06-29 11:16
 */
@Data
public class ProStockDTO {
    /**
     * 商品编码
     */
    private String wmSpBm;
    /**
     * 库存
     */
    private Integer wmKcsl;
}
