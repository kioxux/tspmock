package com.gov.purchase.module.delivery.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "调拨价格列表返回参数")
public class AllotPriceListResponseDto {

    private String client;

    private String alpSupplySite;

    private String supplySiteName;

    private String alpReceiveSite;

    private String receiveSiteName;

    private String alpOrderType;

    private String alpOrderTypeName;

    private String alpChainHead;

    private String alpProCode;

    private String alpEffectFrom;

    private String alpEffectEnd;

    private String alpDeleteFlag;

    private BigDecimal alpConditionQty;

    private String alpChainHeadName;
}
