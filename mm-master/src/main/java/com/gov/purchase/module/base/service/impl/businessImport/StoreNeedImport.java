package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonConstants;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.StoreNeedImportProDTO;
import com.gov.purchase.module.base.dto.businessImport.StoreNeedImportDTO;
import com.gov.purchase.module.base.dto.businessImport.TaxCodeValueDTO;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.replenishment.dto.BatchNoInfo;
import com.gov.purchase.module.replenishment.service.StoreNeedService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.24
 */
@Slf4j
@Service
public class StoreNeedImport extends BusinessImport {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Resource
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private StoreNeedService storeNeedService;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaJyfwDataMapper gaiaJyfwDataMapper;

    /**
     * 煎药维护
     *
     * @param map   数据
     * @param field 字段
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 参数 验证
        if (field == null || field.isEmpty()) {
            throw new CustomResultException("参数为空");
        }
        if (field.get("dcCode") == null || StringUtils.isBlank(field.get("dcCode").toString())) {
            throw new CustomResultException("配送中心编码不能为空");
        }
        int stoCnt = 0;
        int dataCnt = 0;
        // 配送中心
        String dcCode = field.get("dcCode").toString();
        String dcWtdc = field.get("dcWtdc").toString();
        // 当前用户
        TokenUser user = feignService.getLoginInfo();
        // 查询配置表配送方式
        String para = gaiaSdReplenishHMapper.getParamByClientAndGcspId(user.getClient(), "WHOLESALE_DELIVERY_MODE");
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(user.getClient());
        gaiaDcDataKey.setDcCode(dcCode);
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
        List<String> haskeyList = new ArrayList<>();
        Map<String, List<String>> jyfwMap = new HashMap<>();
        Map<String, List<String>> class3Map = new HashMap<>();
        try {
            // 错误 消息
            List<String> errorList = new ArrayList<>();
            List<StoreNeedImportDTO> list = new ArrayList<>();
            List<GaiaProductBusinessKey> gaiaProductBusinessKeyList = new ArrayList<>();
            List<String> stoCodeList = new ArrayList<>();
            List<Map<String, String>> params = new ArrayList<>();
            for (Integer key : map.keySet()) {
                StoreNeedImportDTO needImportDTO = (StoreNeedImportDTO) map.get(key);
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                gaiaProductBusinessKey.setClient(user.getClient());
                gaiaProductBusinessKey.setProSite(needImportDTO.getStoCode());
                gaiaProductBusinessKey.setProSelfCode(needImportDTO.getProSelfCode());
                gaiaProductBusinessKeyList.add(gaiaProductBusinessKey);
                // 门店编码集合
                stoCodeList.add(needImportDTO.getStoCode());

                Map<String, String> proMap = new HashMap<>();
                proMap.put("stoCode", needImportDTO.getStoCode());
                proMap.put("proSelfCode", needImportDTO.getProSelfCode());
                params.add(proMap);
            }
            // 不允许商品列表
            List<GaiaProductBusiness> byxGaiaProductBusinessList = gaiaProductBusinessMapper.selectByPrimaryByBuyunxupinlei(user.getClient(), gaiaProductBusinessKeyList);
            // 门店列表
            List<GaiaStoreData> gaiaStoreDataList = gaiaSdReplenishHMapper.checkDCcontainsStore(user.getClient(), dcCode,
                    stoCodeList.stream().distinct().collect(Collectors.toList()));
            // 商品列表
            List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectByPrimaryKeyList(user.getClient(), params);
            // 经营范围
            List<GaiaJyfwData> storeJyfwByStoList = gaiaJyfwDataMapper.selectStoreJyfwByStoList(user.getClient(), stoCodeList);
            // 经营范围
            List<GaiaJyfwData> storeJyfwByStoClass3List = gaiaJyfwDataMapper.selectStoreJyfwByStoClass3List(user.getClient(), stoCodeList);
            for (Integer key : map.keySet()) {
                StoreNeedImportDTO needImportDTO = (StoreNeedImportDTO) map.get(key);
                GaiaProductBusiness byxGaiaProductBusiness = byxGaiaProductBusinessList.stream().filter(t ->
                        t.getProSelfCode().equals(needImportDTO.getProSelfCode()) && t.getProSite().equals(needImportDTO.getStoCode())
                ).findFirst().orElse(null);
                if (byxGaiaProductBusiness != null) {
                    errorList.add(MessageFormat.format("第[{0}]行,商品{1}对于门店{2}为不允许铺货的品类，不可铺货至门店。",
                            key, needImportDTO.getProSelfCode(), needImportDTO.getStoCode()));
                    continue;
                }
                if (new BigDecimal(needImportDTO.getGsrdNeedQty()).compareTo(BigDecimal.ZERO) <= 0) {
                    errorList.add(MessageFormat.format("第[{0}]行,计划铺货数量必须大于0", key));
                }
                GaiaStoreData gaiaStoreData = gaiaStoreDataList.stream().filter(t -> t.getStoCode().equals(needImportDTO.getStoCode())).findFirst().orElse(null);
                if (gaiaStoreData == null) {
                    errorList.add(MessageFormat.format("第[{0}]行,配送中心[{1}]门店[{2}]不匹配", key, dcCode, needImportDTO.getStoCode()));
                } else {
                    if (!"0".equals(gaiaStoreData.getStoStatus())) {
                        errorList.add(MessageFormat.format("第[{0}]行,门店已闭店", key));
                    }
                }
                if (StringUtils.isNotBlank(needImportDTO.getWmHwh())) {
                    List<BatchNoInfo> batchNoInfoList = gaiaSdReplenishHMapper.selectBatchNoInfoByWarehuPro(user.getClient(), dcCode, needImportDTO.getProSelfCode(), needImportDTO.getBatBatchNo());
                    if (StringUtils.isBlank(needImportDTO.getBatBatchNo())) {
                        if (CollectionUtils.isEmpty(batchNoInfoList)) {
                            errorList.add(MessageFormat.format("第[{0}]行,门店[{1}]商品[{2}]与货位[{3}]不匹配", key, needImportDTO.getStoCode(), needImportDTO.getProSelfCode(), needImportDTO.getWmHwh()));
                        } else {
                            List<String> hwh = batchNoInfoList.stream().map(BatchNoInfo::getWmHwh).collect(Collectors.toList());
                            if (!hwh.contains(needImportDTO.getWmHwh())) {
                                errorList.add(MessageFormat.format("第[{0}]行,门店[{1}]商品[{2}]与货位[{3}]不匹配", key, needImportDTO.getStoCode(), needImportDTO.getProSelfCode(), needImportDTO.getWmHwh()));
                            }
                        }
                    }
                    if (StringUtils.isNotBlank(needImportDTO.getBatBatchNo())) {
                        if (CollectionUtils.isEmpty(batchNoInfoList)) {
                            errorList.add(MessageFormat.format("第[{0}]行,门店[{1}]商品[{2}]批次[{3}]与货位[{4}]不匹配", key, needImportDTO.getStoCode(), needImportDTO.getProSelfCode(), needImportDTO.getBatBatchNo(), needImportDTO.getWmHwh()));
                        } else {
                            List<String> hwh = batchNoInfoList.stream().map(BatchNoInfo::getWmHwh).collect(Collectors.toList());
                            if (!hwh.contains(needImportDTO.getWmHwh())) {
                                errorList.add(MessageFormat.format("第[{0}]行,门店[{1}]商品[{2}]批次[{3}]与货位[{4}]不匹配", key, needImportDTO.getStoCode(), needImportDTO.getProSelfCode(), needImportDTO.getBatBatchNo(), needImportDTO.getWmHwh()));
                            }
                        }

                    }
                }
                // 当前行商品
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessList.stream().filter(t ->
                        t.getProSelfCode().equals(needImportDTO.getProSelfCode()) && t.getProSite().equals(needImportDTO.getStoCode())).findFirst().orElse(null);
                // 经营范围 验证
                if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcJyfwgk()) && (gaiaDcData.getDcJyfwgk().equals("1") || "2".equals(gaiaDcData.getDcJyfwgk()))) {
                    List<GaiaJyfwData> jyfwByStoList = storeJyfwByStoList.stream().filter(t -> t.getJyfwOrgid().equals(needImportDTO.getStoCode())).collect(Collectors.toList());
                    List<String> jyfwList = jyfwByStoList.stream().map(GaiaJyfwData::getJyfwId).collect(Collectors.toList());
                    boolean ignoreScopeCheck = false;
                    if ("2".equals(gaiaDcData.getDcJyfwgk())) {
                        List<GaiaJyfwData> jyfwByStoClass3List = storeJyfwByStoClass3List.stream().filter(t -> t.getJyfwOrgid().equals(needImportDTO.getStoCode())).collect(Collectors.toList());
                        List<String> class3List = jyfwByStoClass3List.stream().map(GaiaJyfwData::getJyfwId).collect(Collectors.toList());
                        if (CollectionUtils.isNotEmpty(class3List)) {
                            ignoreScopeCheck = true;
                        }
                    }
                    if (!ignoreScopeCheck) {
                        if (CollectionUtils.isEmpty(jyfwList)) {
                            errorList.add(StringUtils.parse("第[{0}]行,门店{1}没有商品{2}的经营范围，不可铺货！", key, needImportDTO.getStoCode(), needImportDTO.getProSelfCode()));
                            continue;
                        }
                        if (gaiaProductBusiness == null || StringUtils.isBlank(gaiaProductBusiness.getProJylb())) {
                            errorList.add(StringUtils.parse("第[{0}]行,门店{1}没有商品{2}的经营范围，不可铺货！", key, needImportDTO.getStoCode(), needImportDTO.getProSelfCode()));
                            continue;
                        }
                        if (Collections.disjoint(jyfwList, Arrays.asList(gaiaProductBusiness.getProJylb().split(",")))) {
                            errorList.add(StringUtils.parse("第[{0}]行,门店{1}没有商品{2}的经营范围，不可铺货！", key, needImportDTO.getStoCode(), needImportDTO.getProSelfCode()));
                            continue;
                        }
                    }
                }
                if (gaiaProductBusiness == null) {
                    errorList.add(MessageFormat.format("第[{0}]行,门店[{1}]不存在商品[{2}]", key, needImportDTO.getStoCode(), needImportDTO.getProSelfCode()));
                }
//                int check = gaiaSdReplenishHMapper.check(user.getClient(), dcCode, needImportDTO.getStoCode(), needImportDTO.getProSelfCode());
//                if (check == 0) {
//                    errorList.add(MessageFormat.format("第[{0}]行,门店[{1}]商品[{2}]不可铺货", key, needImportDTO.getStoCode(), needImportDTO.getProSelfCode()));
//                }
                String haskey = StringUtils.concat(needImportDTO.getStoCode(), "_", needImportDTO.getProSelfCode(), "_", needImportDTO.getBatBatchNo());
                if (haskeyList.contains(haskey)) {
                    errorList.add(MessageFormat.format("第[{0}]行,数据重复", key + 1));
                } else {
                    haskeyList.add(haskey);
                }
                list.add(needImportDTO);
                dataCnt++;
            }
            // 验证报错
            if (CollectionUtils.isNotEmpty(errorList)) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            // 税率查询
            List<TaxCodeValueDTO> taxCodeValueList = gaiaSdReplenishHMapper.getTaxCodeValue(user.getClient(), list);
            // 入库
            List<GaiaSdReplenishH> replenishHList = new ArrayList<>();
            List<GaiaSdReplenishD> replenishDList = new ArrayList<>();

            // 查询成本价
            List<StoreNeedImportProDTO> proCostPriceByImportProList = gaiaSdReplenishHMapper.getProCostPriceByImportProList(user.getClient(), list);
//            proCostPriceByImportProList.stream().filter(Objects::nonNull).forEach(item->{
//                StoreNeedImportDTO needImportDTO = list.stream()
//                        .filter(importDTO -> importDTO.getStoCode().equals(item.getStoCode()) && importDTO.getProSelfCode().equals(item.getProSelfCode()))
//                        .findFirst().orElse(null);
//                if (ObjectUtils.isNotEmpty(needImportDTO)) {
//                    item.setBatBatchNo(needImportDTO.getBatBatchNo());
//                    item.setGsrdNeedQty(needImportDTO.getGsrdNeedQty());
//                }
//            });

            List<StoreNeedImportProDTO> storeNeedImportProDTOList = new ArrayList<>();
            for (StoreNeedImportDTO storeNeedImportDTO : list) {
                StoreNeedImportProDTO storeNeedImportProDTO = new StoreNeedImportProDTO();
                BeanUtils.copyProperties(storeNeedImportDTO, storeNeedImportProDTO);
                StoreNeedImportProDTO priceEntity = proCostPriceByImportProList.stream().filter(item ->
                        storeNeedImportDTO.getStoCode().equals(item.getStoCode()) && storeNeedImportDTO.getProSelfCode().equals(item.getProSelfCode())
                ).findFirst().orElse(null);
                if (priceEntity != null) {
                    storeNeedImportProDTO.setCostPrice(priceEntity.getCostPrice());
                }
                storeNeedImportProDTOList.add(storeNeedImportProDTO);
            }

            // 按门店分组
            Map<String, List<StoreNeedImportProDTO>> collect = storeNeedImportProDTOList.stream()
                    .filter(item -> new BigDecimal(item.getGsrdNeedQty()).compareTo(BigDecimal.ZERO) > 0)
                    .collect(Collectors.groupingBy(StoreNeedImportProDTO::getStoCode));
            stoCnt = collect.keySet().size();
            collect.forEach((stoCode, proList) -> {
                // 单号
                String voucherId = getMaxVoucherId(user.getClient());
                // 铺货总数
                BigDecimal gsrdNeedQtySum = proList.stream().map(item -> new BigDecimal(item.getGsrdNeedQty())).reduce(BigDecimal.ZERO, BigDecimal::add);
                // 总金额
                BigDecimal totalAmt = proList.stream().filter(Objects::nonNull)
                        .filter(item -> ObjectUtils.isNotEmpty(item.getCostPrice()) && ObjectUtils.isNotEmpty(item.getGsrdNeedQty()))
                        .map(item -> new BigDecimal(item.getGsrdNeedQty()).multiply(item.getCostPrice()))
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                // 主表
                GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
                // 加盟商
                replenishH.setClient(user.getClient());
                // 补货门店
                replenishH.setGsrhBrId(stoCode);
                // 补货地点
                replenishH.setGsrhAddr(dcCode);
                // 补货单号
                replenishH.setGsrhVoucherId(voucherId);
                // 补货日期
                replenishH.setGsrhDate(DateUtils.getCurrentDateStrYYMMDD());
                // 创建时间
                replenishH.setGsrhCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                // 补货类型(批发仓库铺货时检查配置表是否配置铺货配送方式)
                if (StringUtils.isNotBlank(dcWtdc) && "1".equalsIgnoreCase(dcWtdc)) {
                    if (StringUtils.isNotBlank(para)) {
                        replenishH.setGsrhType(para);
                    }
                }
                // 补货类型
                GaiaStoreData gaiaStoreData = gaiaStoreDataList.stream().filter(t -> t.getStoCode().equals(stoCode)).findFirst().orElse(null);
                if (StringUtils.isBlank(replenishH.getGsrhType())) {
                    if (gaiaStoreData != null && "3".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("3");
                    } else if (gaiaStoreData != null && "4".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("4");
                    } else if (gaiaStoreData != null && "5".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("5");
                    } else if (gaiaStoreData != null && "6".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("6");
                    } else if (gaiaStoreData != null && "7".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("7");
                    } else if (gaiaStoreData != null && "9".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("9");
                    } else {
                        replenishH.setGsrhType("2");
                    }
                }
                // 补货金额
                replenishH.setGsrhTotalAmt(totalAmt);
                // 补货数量
                replenishH.setGsrhTotalQty(gsrdNeedQtySum);
                // 补货人员
                replenishH.setGsrhEmp(user.getUserId());
                // 补货状态是否审核
                replenishH.setGsrhStatus("1");
                // 是否转采购订单
//                replenishH.setGsrhFlag(CommonEnum.NoYesStatus.YES.getCode());
                // 补货方式 0正常补货，1紧急补货,2铺货,3互调
                replenishH.setGsrhPattern("2");
                replenishH.setGsrhFlag("0");
                replenishHList.add(replenishH);

                // 明细表
                // 行号 (同一单号下顺序排列)
                AtomicInteger index = new AtomicInteger(1);
                proList.forEach(pro -> {
                    GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
                    replenishD.setClient(user.getClient());
                    replenishD.setGsrdBrId(pro.getStoCode());
                    replenishD.setGsrdVoucherId(voucherId);
                    replenishD.setGsrdSerial(String.valueOf(index.getAndIncrement()));
                    replenishD.setGsrdProId(pro.getProSelfCode());
                    replenishD.setGsrdDate(DateUtils.getCurrentDateStrYYMMDD());
                    replenishD.setGsrdBatch("");
                    replenishD.setGsrdBatchNo(pro.getBatBatchNo());
                    replenishD.setGsrdGrossMargin("");
                    replenishD.setGsrdSalesDays1(BigDecimal.ZERO);
                    replenishD.setGsrdSalesDays2(BigDecimal.ZERO);
                    replenishD.setGsrdSalesDays3(BigDecimal.ZERO);
                    replenishD.setGsrdProposeQty(BigDecimal.ZERO);
                    replenishD.setGsrdStockStore("");
                    replenishD.setGsrdStockDepot("");
                    replenishD.setGsrdDisplayMin("");
                    replenishD.setGsrdPackMidsize("");
                    replenishD.setGsrdLastSupid("");
                    replenishD.setGsrdLastPrice(BigDecimal.ZERO);
                    replenishD.setGsrdEditPrice(BigDecimal.ZERO);
                    replenishD.setGsrdNeedQty(pro.getGsrdNeedQty());
                    replenishD.setGsrdVoucherAmt(BigDecimal.ZERO);
                    replenishD.setGsrdAverageCost(BigDecimal.ZERO);
                    // 税率
                    TaxCodeValueDTO taxCodeValueDTO = taxCodeValueList.stream()
                            .filter(Objects::nonNull)
                            .filter(item -> item.getStoCode().equals(pro.getStoCode()) && item.getProSelfCode().equals(pro.getProSelfCode()))
                            .findFirst().orElse(null);
                    if (ObjectUtils.isNotEmpty(taxCodeValueDTO)) {
                        replenishD.setGsrdTaxRate(taxCodeValueDTO.getTaxCodeValue());
                    } else {
                        replenishD.setGsrdTaxRate("");
                    }
                    replenishD.setGsrdHwh(pro.getWmHwh());
                    replenishDList.add(replenishD);
                });
            });
            if (CollectionUtils.isNotEmpty(replenishHList)) {
                gaiaSdReplenishHMapper.saveBatch(replenishHList);
            }
            if (CollectionUtils.isNotEmpty(replenishDList)) {
                gaiaSdReplenishDMapper.saveBatch(replenishDList);
            }

            try {
                storeNeedService.automaticBilling(replenishHList);
            } catch (Exception e) {
                log.info("铺货自动开单报错 {}", e.getMessage());
            }

            // 验证报错
            if (CollectionUtils.isNotEmpty(errorList)) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            return ResultUtil.success("铺货导入成功，共生成" + stoCnt + "家门店" + dataCnt + "条数据，请通知仓库发货。");
        } catch (
                Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

    }

    /**
     * 获取最大铺货单号
     *
     * @param client
     * @return
     */
    private String getMaxVoucherId(String client) {
        String maxVoucherId = "";
        while (true) {
            if (StringUtils.isBlank(maxVoucherId)) {
                maxVoucherId = gaiaSdReplenishHMapper.getMaxVoucherId(client);
                maxVoucherId = String.format("%06d", (Integer.parseInt(maxVoucherId) + 1));
            } else {
                maxVoucherId = String.format("%06d", (Integer.parseInt(maxVoucherId) + 1));
            }
            String exists = redisClient.get(CommonConstants.GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID + client + maxVoucherId);
            log.info("补货单号:redis.getkey:{},value:{}", CommonConstants.GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID + client + maxVoucherId, maxVoucherId);
            if (StringUtils.isBlank(exists)) {
                redisClient.set(CommonConstants.GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID + client + maxVoucherId, maxVoucherId, 1800);
                log.info("补货单号:redis.setkey:{},value:{}", CommonConstants.GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID + client + maxVoucherId, maxVoucherId);
                return "UD" + LocalDate.now().getYear() + maxVoucherId;
            }
        }
    }

}
