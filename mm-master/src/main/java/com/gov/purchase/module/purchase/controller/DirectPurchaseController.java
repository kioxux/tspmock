package com.gov.purchase.module.purchase.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.IDirectPurchaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "无仓库采购")
@RestController
@RequestMapping("directPurchase")
public class DirectPurchaseController {
    @Resource
    private IDirectPurchaseService directPurchaseService;

    @Log("门店下拉框")
    @ApiOperation("门店下拉框")
    @PostMapping("selectStore")
    public Result getStore() {
        //批次库存查询
        List<DeliveryStoreResponseDto> BatchStockList = directPurchaseService.selectStore();
        return ResultUtil.success(BatchStockList);
    }

    @Log("要货查询")
    @ApiOperation("要货查询")
    @PostMapping("list")
    public Result list(@RequestBody DirectPurchaseListDTO directPurchaseListDTO) {
        //批次库存查询
        List<DirectPurchaseListVO> BatchStockList = directPurchaseService.selectList(directPurchaseListDTO);
        return ResultUtil.success(BatchStockList);
    }

    @Log("要货查询")
    @ApiOperation("要货查询")
    @PostMapping("summaryList")
    public Result summaryList(@RequestBody DirectPurchaseListDTO directPurchaseListDTO) {
        //批次库存查询
        List<DirectPurchaseSummaryListVO> BatchStockList = directPurchaseService.selectSummaryList(directPurchaseListDTO);
        return ResultUtil.success(BatchStockList);
    }

    @Log("要货下单")
    @ApiOperation("要货下单")
    @PostMapping("save")
    public Result save(@RequestBody List<DirectPurchaseSaveDTO> voList) {
        //批次库存查询
        directPurchaseService.save(voList);
        return ResultUtil.success();
    }

    @Log("导出")
    @ApiOperation("导出")
    @PostMapping("export")
    public Result export(@RequestBody List<DirectPurchaseSaveDTO> voList) {
        //批次库存查询
        return directPurchaseService.export(voList);
    }
}
