package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class AddBusinessScope {

    /**
     * 经营范围编码
     */
    @ExcelValidate(index = 0, name = "经营范围编码", type = ExcelValidate.DataType.STRING, maxLength = 6)
    private String bscCode;

    /**
     * 经营范围名称
     */
    @ExcelValidate(index = 1, name = "经营范围名称", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String bscCodeName;

}

