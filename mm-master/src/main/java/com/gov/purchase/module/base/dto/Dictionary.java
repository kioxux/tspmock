package com.gov.purchase.module.base.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.18
 */
@Data
public class Dictionary {

    public Dictionary(String code, String label) {
        this.value = code;
        this.label = label;
    }

    /**
     * 值
     */
    private String value;

    /**
     * 表示
     */
    private String label;

    /**
     * 类型
     */
    private String type;

    /**
     * 税率值（进项税率所需）
     */
    private String codeValue;

}

