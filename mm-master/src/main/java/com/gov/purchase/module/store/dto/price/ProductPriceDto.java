package com.gov.purchase.module.store.dto.price;

import lombok.Data;

import java.util.List;

/**
 * @description: 价格申请
 * @author: yzf
 * @create: 2022-03-07 09:54
 */
@Data
public class ProductPriceDto {

    private String prcClass;

    private List<StoreProductDto> list;
}
