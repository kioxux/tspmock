package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.entity.GaiaSdStoresGroup;
import com.gov.purchase.entity.GaiaSdStoresGroupSet;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.10.18
 */
@Data
public class GaiaSdStoresGroupSetDto extends GaiaSdStoresGroupSet {

    private List<GaiaSdStoresGroup> gaiaSdStoresGroupList;
}

