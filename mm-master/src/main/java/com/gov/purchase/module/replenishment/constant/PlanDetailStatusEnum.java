package com.gov.purchase.module.replenishment.constant;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/16 19:43
 **/
public enum PlanDetailStatusEnum {

    NOT_EXEC(0, "未执行"),
    EXECUTED(1, "已执行"),;

    public final Integer status;
    public final String name;

    PlanDetailStatusEnum(Integer status, String name) {
        this.status = status;
        this.name = name;
    }
}
