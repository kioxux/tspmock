package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class GoodsStateUpdate {

    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proCode;

    /**
     * 字段值
     */
    @ExcelValidate(index = 1, name = "字段值", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String fieldNewValue;

    /**
     * 字段名称
     */
    @ExcelValidate(index = 2, name = "字段名称", type = ExcelValidate.DataType.STRING, maxLength = 50,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.FIELD_NAME, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "fieldNameValue")
    private String fieldName;

    /**
     * 字段名称
     */
    private String fieldNameValue;
}

