package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaWholesaleChannelDetail;
import lombok.Data;

import java.util.List;

/**
 * @program: mm
 * @description:
 * @author: yzf
 * @create: 2021-11-01 14:35
 */
@Data
public class BatchProcessWholeSaleChannelDetailDTO {
    /**
     * 渠道编码
     */
    private String chaCode;
    /**
     * 仓库编码
     */
    private String dcCode;
    /**
     * 加盟商
     */
    private String client;


    private String type;

    private List<GaiaWholesaleChannelDetail> channelPro;
}
