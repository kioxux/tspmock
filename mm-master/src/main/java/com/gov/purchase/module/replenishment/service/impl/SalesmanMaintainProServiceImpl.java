package com.gov.purchase.module.replenishment.service.impl;

import com.gov.purchase.common.excel.ExportExcel;
import com.gov.purchase.entity.GaiaSupplierSalesman;
import com.gov.purchase.exception.BusinessException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.SalesmanMaintainProMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.MaintainProductsListQueryDTO;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.MaintainProductsListSaveDTO;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.SupplierSalesmanProDTO;
import com.gov.purchase.module.replenishment.service.SalesmanMaintainProService;
import com.gov.purchase.module.store.dto.store.StoreMinQtyExportDto;
import com.gov.purchase.module.store.dto.store.StoreMinQtyResultDto;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SalesmanMaintainProServiceImpl implements SalesmanMaintainProService {

    @Autowired
    FeignService feignService;

    @Autowired
    SalesmanMaintainProMapper salesmanMaintainProMapper;


    @Override
    public List<GaiaSupplierSalesman> getSupplierSalesManList(String dcCode) {
        TokenUser tokenUser = feignService.getLoginInfo();
        return salesmanMaintainProMapper.querySupplierSalesmanList(tokenUser.getClient(),dcCode);
    }

    @Override
    public MaintainProductsListQueryDTO getProductsList(String supSite, String supSelfCode, String gssCode) {
        TokenUser tokenUser = feignService.getLoginInfo();
        MaintainProductsListQueryDTO maintainProductsListQueryDTO = new MaintainProductsListQueryDTO();
        // 查询是否按照品种管控
        String isControl = salesmanMaintainProMapper.queryIsControl(tokenUser.getClient(), supSite, supSelfCode, gssCode);
        if (StringUtils.isEmpty(isControl)) {
            throw new BusinessException("不存在该业务员");
        }
        maintainProductsListQueryDTO.setIsControl(isControl);
        List<SupplierSalesmanProDTO> supplierSalesmanProDTOList = salesmanMaintainProMapper.queryProductList(tokenUser.getClient(), supSite, supSelfCode, gssCode);
        maintainProductsListQueryDTO.setSupplierSalesmanProDTOS(supplierSalesmanProDTOList);
        return maintainProductsListQueryDTO;
    }


    @Override
    @Transactional
    public void saveProductsList(MaintainProductsListSaveDTO maintainProductsListSaveDTO) {
        TokenUser tokenUser = feignService.getLoginInfo();
        // 修改业务员是否按照品种管控的状态（是否按照品种管控 1：是，0：否）
        salesmanMaintainProMapper.updateIsControl(tokenUser.getClient(), maintainProductsListSaveDTO.getSupSite(), maintainProductsListSaveDTO.getSupSelfCode(), maintainProductsListSaveDTO.getGssCode(), maintainProductsListSaveDTO.getIsControl());
        // 先删除所有商品再添加
        salesmanMaintainProMapper.deleteAllPro(tokenUser.getClient(), maintainProductsListSaveDTO.getSupSite(), maintainProductsListSaveDTO.getSupSelfCode(), maintainProductsListSaveDTO.getGssCode());
        // 添加授权商品
        List<String> proList = maintainProductsListSaveDTO.getProSelfCodeList();
        if (proList.size()<1){
            throw new BusinessException("未选择任何商品");
        }
        String createDate = DateUtils.getCurrentDateStrYYMMDD();
        // 对保存商品进行去重
        HashSet<String> proSet = new HashSet<>();
        for (String proCode : proList) {
            proSet.add(proCode);
        }
        for (String proCode : proSet) {
            // 校验商品是否合法
            Integer count = salesmanMaintainProMapper.queryCountPro(tokenUser.getClient(), maintainProductsListSaveDTO.getSupSite(), proCode);
            if (count == 0) {
                throw new BusinessException("商品编码错误，不存在该商品");
            }
            salesmanMaintainProMapper.addPro(tokenUser.getClient(), maintainProductsListSaveDTO.getSupSite(), maintainProductsListSaveDTO.getSupSelfCode(), maintainProductsListSaveDTO.getGssCode(), createDate, proCode);
        }
    }


    @Override
    public List<SupplierSalesmanProDTO> getProDetails(String supSite, String proInfo, Integer pageSize) {
        TokenUser tokenUser = feignService.getLoginInfo();
        return salesmanMaintainProMapper.queryProductDetailsList(tokenUser.getClient(), supSite, proInfo, pageSize);
    }


    @Override
    public List<SupplierSalesmanProDTO> importProductsList(String supSite, MultipartFile file) {
        TokenUser tokenUser = feignService.getLoginInfo();
        ArrayList<String> proCodeList = new ArrayList<>();
        try {
            InputStream inputStream = file.getInputStream();
            XSSFWorkbook wb = new XSSFWorkbook(inputStream);
            Sheet sheet = wb.getSheetAt(0);
            if (sheet.getLastRowNum()<1){
                throw new BusinessException("导入文件中存在不正确数据");
            }
            DataFormatter dataFormatter = new DataFormatter();
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                String proCode = dataFormatter.formatCellValue(row.getCell(0)).trim();
                proCodeList.add(proCode);
            }
            inputStream.close();
            wb.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            throw new BusinessException("请导入正确文件");
        }
        ArrayList<SupplierSalesmanProDTO> supplierSalesmanProDTOS = new ArrayList<>();
        for (String proCode : proCodeList) {
            SupplierSalesmanProDTO supplierSalesmanProDTO = salesmanMaintainProMapper.queryProductDetails(tokenUser.getClient(), supSite, proCode);
            if (ObjectUtils.isEmpty(supplierSalesmanProDTO)){
                throw new BusinessException("导入文件中存在不正确数据");
            }
            supplierSalesmanProDTOS.add(supplierSalesmanProDTO);
        }
        return supplierSalesmanProDTOS;
    }


    @Override
    public void exportProductsList(String supSite, String supSelfCode, String gssCode) throws IOException {
        TokenUser tokenUser = feignService.getLoginInfo();
        // 查询是否按照品种管控
        String isControl = salesmanMaintainProMapper.queryIsControl(tokenUser.getClient(), supSite, supSelfCode, gssCode);
        if (StringUtils.isEmpty(isControl)) {
            throw new BusinessException("不存在该业务员");
        }
        List<SupplierSalesmanProDTO> supplierSalesmanProDTOList = salesmanMaintainProMapper.queryProductList(tokenUser.getClient(), supSite, supSelfCode, gssCode);
        ExportExcel excel = new ExportExcel(null, SupplierSalesmanProDTO.class, 1);
        excel.setDataList(supplierSalesmanProDTOList);
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();
        excel.write(response, "供应商业务员授权商品列表" + DateUtils.getCurrentDateTimeStr("yyyyMMddHHmmss") + ".xlsx");
    }

    @Override
    public void exportTemplete() throws IOException {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("商品编码");
        ExportExcel excel = new ExportExcel(null, strings);
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();
        excel.write(response, "供应商业务员授权商品列表" + DateUtils.getCurrentDateTimeStr("yyyyMMddHHmmss") + ".xlsx");
    }
}
