package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "发票登记查询返回参数")
public class InvoiceRegisterListResponseDto {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 加盟商名
     */
    private String clientName;

    /**
     * 地点代码
     */
    private String poCompanyCode;

    /**
     * 地点名
     */
    private String siteName;

    /**
     * 供应商编码
     */
    private String poSupplierId;

    /**
     * 供应商名
     */
    private String supName;

    /**
     * 余额
     */
    private BigDecimal balance;
}