package com.gov.purchase.module.store.dto.store;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@ApiModel(value = "门店详情返回参数")
public class StoreInfoDetailReponseDto {

    @ApiModelProperty(value = "门店编号", name = "stoCode")
    private String stoCode;

    @ApiModelProperty(value = "门店名称", name = "stoName")
    private String stoName;

    @ApiModelProperty(value = "助记码", name = "stoPym")
    private String stoPym;

    @ApiModelProperty(value = "门店状态（0-营业、1-闭店）", name = "stoStatus")
    private String stoStatus;

    @ApiModelProperty(value = "门店简称", name = "stoShortName")
    private String stoShortName;

    @ApiModelProperty(value = "门店属性（1-单体、2-连锁、3-加盟、4-门诊）", name = "stoAttribute")
    private String stoAttribute;

    @ApiModelProperty(value = "配送方式（1-自采、2-总部配送、3-委托配送）", name = "stoDeliveryMode")
    private String stoDeliveryMode;

    @ApiModelProperty(value = "经营面积（1- 60-80㎡；80-100㎡；100-120㎡；120㎡以上）", name = "stoArea")
    private String stoArea;

    @ApiModelProperty(value = "关联门店(GAIA_STORE_DATA)", name = "stoRelationStore")
    private String stoRelationStore;

    @ApiModelProperty(value = "省", name = "stoProvince")
    private String stoProvince;

    @ApiModelProperty(value = "市", name = "stoCity")
    private String stoCity;

    @ApiModelProperty(value = "区", name = "stoDistrict")
    private String stoDistrict;

    @ApiModelProperty(value = "详细地址", name = "stoAdd")
    private String stoAdd;

    @ApiModelProperty(value = "所属加盟商编号（GAIA_FRANCHISEE）", name = "client")
    private String client;

    @ApiModelProperty(value = "DTP（0-否；1-是）", name = "stoIfDtp")
    private String stoIfDtp;

    @ApiModelProperty(value = "是否医保店（0-否；1-是）", name = "stoIfMedicalcare")
    private String stoIfMedicalcare;

    @ApiModelProperty(value = "连锁总部 (源自：GAIA_DC_DATA)", name = "stoChainHead")
    private String stoChainHead;

    @ApiModelProperty(value = "委托配送公司(源自：GAIA_DELIVERY_COMPANY)", name = "stoDeliveryCompany")
    private String stoDeliveryCompany;

    @ApiModelProperty(value = "开业日期", name = "stoOpenDate")
    private String stoOpenDate;

    @ApiModelProperty(value = "闭店日期", name = "stoCloseDate")
    private String stoCloseDate;

    @ApiModelProperty(value = "税分类（1-小规模纳税人，2-一般纳税人）", name = "stoTaxClass")
    private String stoTaxClass;

    @ApiModelProperty(value = "纳税主体 (源自：GAIA_DC_DATA)", name = "stoTaxSubject")
    private String stoTaxSubject;

    @ApiModelProperty(value = "税率（0%，3%，其他）", name = "stoTaxRate")
    private String stoTaxRate;

    @ApiModelProperty(value = "配送中心", name = "stoDcCode")
    private String stoDcCode;

    /**
     * 收货人签章图片
     */
    private String stoShrqxtp;

    /**
     * 运输时效（小时）
     */
    private String stoYssx;

    /**
     * 是否启用信用管理
     */
    private String stoCreditFlag;
    /**
     *信用额度
     */

    private BigDecimal stoCreditQuota;

    /**
     * 账期类型（0-账期天数，1-固定账期日）
     */
    private String stoZqlx;

    /**
     * 固定账期天数
     */
    private Integer stoZqts;

    /**
     * 欠款标志,0-禁止补货或铺货，1-不禁止
     */
    private String stoArrearsFlag;

    /**
     * 电商文案 1仓库所有合格品;2仅电商仓库
     */
    private String wmsDsfa;
}
