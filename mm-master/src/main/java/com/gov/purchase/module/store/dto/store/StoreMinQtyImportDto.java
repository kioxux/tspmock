package com.gov.purchase.module.store.dto.store;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

@Data
public class StoreMinQtyImportDto {

    /**
     * 门店编码
     */
    @ExcelValidate(index = 0, name = "门店编码", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String gpmdBrId;

    /**
     * 商品编码
     */
    @ExcelValidate(index = 1, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String gpmdProId;

    /**
     * 最小陈列量
     */
    @ExcelValidate(index = 2, name = "最小陈列量", type = ExcelValidate.DataType.DECIMAL, min= 0, max = 999)
    private String gpmdMinQty;
}
