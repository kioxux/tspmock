package com.gov.purchase.module.base.dto;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.23
 */
@Data
public class DictionaryStatic {

    private String key;

    private List<Dictionary> list;
}

