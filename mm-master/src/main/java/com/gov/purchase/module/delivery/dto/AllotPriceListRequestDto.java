package com.gov.purchase.module.delivery.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class AllotPriceListRequestDto extends Pageable {

    /**
     * 收货地点
     */
    private String alpReceiveSite;

    /**
     * 商品编码
     */
    private String alpProCode;
    /**
     * 商品自编码 更多
     */
    private List<String> alpProCodes;
}