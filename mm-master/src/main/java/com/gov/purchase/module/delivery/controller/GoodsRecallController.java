package com.gov.purchase.module.delivery.controller;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.delivery.dto.GoodsRecallListRequestDto;
import com.gov.purchase.module.delivery.dto.GoodsRecallListResponseDto;
import com.gov.purchase.module.delivery.dto.SaveGoodsRecallRequestDto;
import com.gov.purchase.module.delivery.dto.StockNumRequestDto;
import com.gov.purchase.module.delivery.service.GoodsRecallService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@Api(tags = "库存配送商品召回")
@RestController
@RequestMapping("goodsRecall")
public class GoodsRecallController {

    @Resource
    private GoodsRecallService goodsRecallService;

    /**
     * 门店库存获取
     *
     * @param dto StockNumRequestDto
     * @return BigDecimal
     */
    @Log("门店库存获取")
    @ApiOperation("门店库存获取")
    @PostMapping("queryStockNum")
    public Result resultStockNum(@Valid @RequestBody StockNumRequestDto dto){

        // 门店库存获取
        BigDecimal goodsInfoList = goodsRecallService.selectStockNum(dto);
        return ResultUtil.success(goodsInfoList);
    }

    /**
     * 商品召回提交
     *
     * @param dto List<SaveGoodsRecallRequestDto>
     * @return
     */
    @Log("商品召回提交")
    @ApiOperation("商品召回提交")
    @PostMapping("saveGoodsRecall")
    public Result resultSaveGoodsRecall(@Valid @RequestBody List<SaveGoodsRecallRequestDto> dto){

        // 商品召回提交
        goodsRecallService.insertSaveGoodsRecall(dto);
        return ResultUtil.success();
    }

    /**
     * 商品召回查看列表
     *
     * @param dto List<SaveGoodsRecallRequestDto>
     * @return
     */
    @Log("商品召回查看列表")
    @ApiOperation("商品召回查看列表")
    @PostMapping("queryGoodsRecallList")
    public Result resultGoodsRecallList(@Valid @RequestBody GoodsRecallListRequestDto dto){

        // 商品召回查看列表
        PageInfo<GoodsRecallListResponseDto> goodsRecallList = goodsRecallService.selectGoodsRecallList(dto);
        return ResultUtil.success(goodsRecallList);
    }

    /**
     * 连锁总下所有商品
     *
     * @param client 加盟商
     * @param chain 连锁总
     * @return
     */
    @Log("连锁总下所有商品")
    @ApiOperation("连锁总下所有商品")
    @PostMapping("getProductListForRecall")
    public Result getProductListForRecall(@RequestJson("client") String client, @RequestJson("chain") String chain) {
        return goodsRecallService.getProductListForRecall(client, chain);
    }
}
