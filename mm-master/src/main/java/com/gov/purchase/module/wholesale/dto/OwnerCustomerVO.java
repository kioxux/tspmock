package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaOwnerCustomer;
import lombok.Data;

/**
 * @description: 批发货主客户明细
 * @author: yzf
 * @create: 2021-11-24 10:35
 */
@Data
public class OwnerCustomerVO extends GaiaOwnerCustomer {
    /**
     * 客户名称
     */
    private String cusName;
}
