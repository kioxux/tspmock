package com.gov.purchase.module.qa.service.impl;

import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaBatchStockMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.module.qa.dto.*;
import com.gov.purchase.module.qa.service.QualityService;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class QualityServiceImpl implements QualityService {

    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    @Resource
    private GaiaBatchStockMapper gaiaBatchStockMapper;


    /**
     * 获取字段列表
     *
     */
    @Override
    public List queryFieldList() {
        List list = CommonEnum.ProStatus.toList();
        return list;
    }

    /**
     * 商品状态更新-获取商品编号列表
     */
    @Override
    public List<QueryProductListResponseDto> queryProductList(QueryProductListRequestDto dto) {
        List<QueryProductListResponseDto> queryProductListResponseDto = gaiaProductBusinessMapper.queryProductList(dto);
        return queryProductListResponseDto;
    }

    @Override
    @Transactional
    public int updateProduct(List<UpdateProductResquestDto>  list) {
        for(UpdateProductResquestDto dto : list) {
            ValidationResult result = ValidateUtil.validateEntity(dto);
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
        }
        int i = gaiaProductBusinessMapper.updateProduct(list);
        if(i == list.size()){
            return i;
        }
        throw new CustomResultException(ResultEnum.GOODS_STATUS_UPDATE_ERROR);
    }

    @Override
    public List<QueryStockListResponseDto> queryStockList(QueryStockListResquestDto dto) {
        // 库存状态 - 正常 则状态设置为 1000
        if(StringUtils.isEqual(dto.getStatus(),CommonEnum.SupplierStauts.SUPPLIERNORMAL.getCode())){
            dto.setStatus(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(0).getValue());
        } else {
            //库存状态 - 不正常 则状态设置为 7000
            dto.setStatus(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(6).getValue());
        }

        List<QueryStockListResponseDto> list = gaiaProductBusinessMapper.queryStockList(dto);
        for( QueryStockListResponseDto response :  list){
            // 库存状态 - 正常 则状态设置为 1000
            if(StringUtils.isEqual(dto.getStatus(),CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(0).getValue())){
                response.setStatus( CommonEnum.SupplierStauts.SUPPLIERNORMAL.getCode());
            } else {
                //库存状态 - 不正常 则状态设置为 7000
                response.setStatus( CommonEnum.SupplierStauts.SUPPLIERDISNORMAL.getCode());
            }
        }
        return list;
    }

    @Override
    @Transactional
    public int saveBatchStockInfo(List<SaveBatchStockInfoResquestDto> list) {
        for(SaveBatchStockInfoResquestDto dto : list){
            // 字段验证
            ValidationResult result = ValidateUtil.validateEntity(dto);
            // 如错误抛到前台
            if(result.isHasErrors()){
                throw new CustomResultException(result.getMessage());
            }

            // 原库存状态 - 正常 则状态设置为 1000
            if(StringUtils.isEqual(dto.getStatus(),CommonEnum.SupplierStauts.SUPPLIERNORMAL.getCode())){
                dto.setStatus(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(0).getValue());
            } else {
                //库存状态 - 不正常 则状态设置为 7000
                dto.setStatus(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(6).getValue());
            }

            // 新库存状态 - 正常 则状态设置为 1000
            if(StringUtils.isEqual(dto.getNewStatus(),CommonEnum.SupplierStauts.SUPPLIERNORMAL.getCode())){
                dto.setNewStatus(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(0).getValue());
            } else {
                //库存状态 - 不正常 则状态设置为 7000
                dto.setNewStatus(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(6).getValue());
            }
        }
        // 更新库存表(GAIA_BATCH_STOCK)
        int i = gaiaBatchStockMapper.saveBatchStockInfo(list);
        if(i == list.size()){
            return i;
        }
       throw new CustomResultException(ResultEnum.BATCH_NO_STATUS_UPDATE_ERROR);
    }
}
