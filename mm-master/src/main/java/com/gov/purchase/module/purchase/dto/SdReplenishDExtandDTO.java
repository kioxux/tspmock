package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaSdReplenishD;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 门店要货订单列表
 * @author: yzf
 * @create: 2021-11-15 11:40
 */
@Data
public class SdReplenishDExtandDTO extends GaiaSdReplenishD {

    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 补货方式 0正常补货，1紧急补货,2铺货,3互调
     */
    private String gsrhPattern;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 中包装量
     */
    private String proMidPackage;

    /**
     * 商品定位
     */
    private String proPosition;

    /**
     * 销售级别
     */
    private String proSlaeClass;

    /**
     * 门店库存
     */
    private BigDecimal matAddAmt;

    /**
     * 加点后税金
     */
    private BigDecimal matAddTax;

    /**
     * 总库存
     */
    private BigDecimal matTotalQty;

    /**
     * 移动平均价
     */
    private BigDecimal matMovPrice;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 成本金额
     */
    private BigDecimal costTotal;

    /**
     * 零售价
     */
    private BigDecimal priceNormal;

    /**
     * 零售金额
     */
    private BigDecimal totalNormal;

    /**
     * 实际数量
     */
    private String realNeedQty;

    /**
     * 门店库存
     */
    private BigDecimal gssQty;

    /**
     * 创建时间
     */
    private String gsrhCreTime;

    /**
     * 补货地点
     */
    private String gsrhAddr;

    /**
     * 审核状态
     */
    private String gsrhFlag;

    /**
     * 商品进项税率
     */
    private String proInputTax;

    private String taxCodeValue;

    private String poId;

    private BigDecimal gsrdDnQty;

    private String gsrhType;
}
