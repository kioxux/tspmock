package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaContractM;
import com.gov.purchase.entity.GaiaContractZ;
import lombok.Data;

import java.util.List;

/**
 * @description: 合同功能
 * @author: yzf
 * @create: 2022-02-07 13:19
 */
@Data
public class PurchaseContractVO {

    private GaiaContractZ gaiaContractZ;


    /**
     * 明细
     */
    private List<PurchaseContractDetailVO> details;

}
