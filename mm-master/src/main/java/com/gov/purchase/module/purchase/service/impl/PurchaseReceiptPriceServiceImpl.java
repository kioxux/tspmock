package com.gov.purchase.module.purchase.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaChajiaM;
import com.gov.purchase.entity.GaiaChajiaZ;
import com.gov.purchase.entity.GaiaChajiaZKey;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaChajiaMMapper;
import com.gov.purchase.mapper.GaiaChajiaZMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.PurchaseReceiptPriceDTO;
import com.gov.purchase.module.purchase.dto.PurchaseReceiptPriceDetailVO;
import com.gov.purchase.module.purchase.dto.PurchaseReceiptPriceVO;
import com.gov.purchase.module.purchase.service.PurchaseReceiptPriceService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @description: 采购入库差价单功能
 * @author: yzf
 * @create: 2021-12-13 09:29
 */
@Service
public class PurchaseReceiptPriceServiceImpl implements PurchaseReceiptPriceService {
    @Resource
    private FeignService feignService;

    @Resource
    private GaiaChajiaZMapper gaiaChajiaZMapper;

    @Resource
    private GaiaChajiaMMapper gaiaChajiaMMapper;

    @Resource
    private RedisClient redisClient;

    @Override
    public List<String> selectCjIdList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaChajiaZMapper.selectCjIdList(user.getClient());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result savePurchaseReceiptPriceList(PurchaseReceiptPriceDTO dto) {
        if (dto != null) {
            TokenUser user = feignService.getLoginInfo();
            GaiaChajiaZ chajiaZ = new GaiaChajiaZ();
            List<GaiaChajiaM> chajiaMList = new ArrayList<>();
            chajiaZ.setClient(user.getClient());
            chajiaZ.setCjDate(DateUtils.getCurrentDateStrYYMMDD());
            chajiaZ.setCjHeadRemark(dto.getCjHeadRemark());
            chajiaZ.setCjSite(dto.getCjSite());
            chajiaZ.setCjStatus(dto.getCjStatus());
            chajiaZ.setCjSupplierId(dto.getCjSupplierId());
            chajiaZ.setCjSupplierSalesman(dto.getCjSupplierSalesman());
            chajiaZ.setCjCreateBy(user.getUserId());
            chajiaZ.setCjCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            chajiaZ.setCjCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            chajiaZ.setCjType("CJ");
            if (CollectionUtils.isNotEmpty(dto.getDetails())) {
                if (dto.getDetails().get(0).getCjQty().compareTo(BigDecimal.ZERO) < 0) {
                    chajiaZ.setCjType("CJTZ");
                }
            }
            if (!StringUtils.isNotBlank(dto.getCjId())) {
                // 新增
                String cjId = gaiaChajiaZMapper.getCjId(user.getClient(), "CJ" + String.valueOf(LocalDateTime.now().getYear()));
                Long newCjId = Long.parseLong(cjId.split("CJ")[1]);
                while (redisClient.exists("GAIA_CHAJIA_Z_CJ_ID" + newCjId)) {
                    newCjId++;
                }
                redisClient.set("GAIA_CHAJIA_Z_CJ_ID" + newCjId, newCjId.toString(), 120);
                // 差价单号
                chajiaZ.setCjId("CJ" + newCjId.toString());
                gaiaChajiaZMapper.insertSelective(chajiaZ);
                if (CollectionUtils.isNotEmpty(dto.getDetails())) {
                    for (int i = 0; i < dto.getDetails().size(); i++) {
                        if (dto.getDetails().get(i) != null) {
                            GaiaChajiaM gaiaChajiaM = new GaiaChajiaM();
                            gaiaChajiaM.setClient(user.getClient());
                            gaiaChajiaM.setCjId(chajiaZ.getCjId());
                            gaiaChajiaM.setCjLineNo(String.valueOf(i + 1));
                            gaiaChajiaM.setCjProCode(dto.getDetails().get(i).getCjProCode());
                            gaiaChajiaM.setCjQty(dto.getDetails().get(i).getCjQty());
                            gaiaChajiaM.setCjLineAmt(dto.getDetails().get(i).getCjLineAmt());
                            gaiaChajiaM.setCjPrice(dto.getDetails().get(i).getCjPrice());
                            gaiaChajiaM.setCjRateCode(dto.getDetails().get(i).getCjRateCode());
                            gaiaChajiaM.setCjLineRemark(dto.getDetails().get(i).getCjLineRemark());
                            gaiaChajiaM.setCjType(chajiaZ.getCjType());
                            chajiaMList.add(gaiaChajiaM);
                        }
                    }
                    gaiaChajiaMMapper.insertBatch(chajiaMList);
                }
            } else {
                // 审核或者差价单保存更新
                chajiaZ.setCjId(dto.getCjId());
                if (StringUtils.isNotBlank(chajiaZ.getCjStatus())) {
                    if ("1".equalsIgnoreCase(chajiaZ.getCjStatus())) {
                        chajiaZ.setCjShBy(user.getUserId());
                        chajiaZ.setCjShDate(DateUtils.getCurrentDateStrYYMMDD());
                        chajiaZ.setCjShTime(DateUtils.getCurrentTimeStrHHMMSS());
                    } else {
                        chajiaZ.setCjUpdateBy(user.getUserId());
                        chajiaZ.setCjUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
                        chajiaZ.setCjUpdateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    }
                }
                // 更新差价单主表
                gaiaChajiaZMapper.updateByPrimaryKeySelective(chajiaZ);
                // 先删除明细，再重新保存
                gaiaChajiaMMapper.deleteByCjId(user.getClient(), dto.getCjId());
                if (CollectionUtils.isNotEmpty(dto.getDetails())) {
                    for (int i = 0; i < dto.getDetails().size(); i++) {
                        if (dto.getDetails().get(i) != null) {
                            GaiaChajiaM gaiaChajiaM = new GaiaChajiaM();
                            gaiaChajiaM.setClient(dto.getDetails().get(i).getClient());
                            gaiaChajiaM.setCjId(chajiaZ.getCjId());
                            gaiaChajiaM.setCjLineNo(String.valueOf(i + 1));
                            gaiaChajiaM.setCjProCode(dto.getDetails().get(i).getCjProCode());
                            gaiaChajiaM.setCjQty(dto.getDetails().get(i).getCjQty());
                            gaiaChajiaM.setCjLineAmt(dto.getDetails().get(i).getCjLineAmt());
                            gaiaChajiaM.setCjPrice(dto.getDetails().get(i).getCjPrice());
                            gaiaChajiaM.setCjRateCode(dto.getDetails().get(i).getCjRateCode());
                            gaiaChajiaM.setCjLineRemark(dto.getDetails().get(i).getCjLineRemark());
                            gaiaChajiaM.setCjType(chajiaZ.getCjType());
                            chajiaMList.add(gaiaChajiaM);
                        }
                    }
                    gaiaChajiaMMapper.insertBatch(chajiaMList);
                } else {
                    // 明细为空时，删除对应主表
                    GaiaChajiaZKey gaiaChajiaZKey = new GaiaChajiaZKey();
                    gaiaChajiaZKey.setClient(user.getClient());
                    gaiaChajiaZKey.setCjId(dto.getCjId());
                    gaiaChajiaZMapper.deleteByPrimaryKey(gaiaChajiaZKey);
                }
            }
            return ResultUtil.success(chajiaZ.getCjId());
        }
        return ResultUtil.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result deletePurchaseReceiptPriceList(PurchaseReceiptPriceDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        if (dto != null && StringUtils.isNotBlank(dto.getCjId())) {
            GaiaChajiaZKey gaiaChajiaZKey = new GaiaChajiaZKey();
            gaiaChajiaZKey.setClient(user.getClient());
            gaiaChajiaZKey.setCjId(dto.getCjId());
            gaiaChajiaZMapper.deleteByPrimaryKey(gaiaChajiaZKey);
            gaiaChajiaMMapper.deleteByCjId(user.getClient(), dto.getCjId());
        }
        return ResultUtil.success();
    }

    @Override
    public Result getRecordByCjId(PurchaseReceiptPriceDTO dto) {
        if (dto != null && StringUtils.isNotBlank(dto.getCjId())) {
            TokenUser user = feignService.getLoginInfo();
            dto.setClient(user.getClient());
            List<PurchaseReceiptPriceVO> vo = gaiaChajiaZMapper.selectPurchaseReceiptPriceOrder(dto);
            if (CollectionUtils.isNotEmpty(vo) && vo.get(0) != null) {
                List<PurchaseReceiptPriceDetailVO> detailVOList = gaiaChajiaMMapper.selectPurchaseReceiptPriceOrderDetail(user.getClient(), dto.getCjId(), vo.get(0).getCjSite());
                vo.get(0).setDetails(detailVOList);
                return ResultUtil.success(vo.get(0));
            }
        }
        return ResultUtil.success(new PurchaseReceiptPriceVO());
    }

    @Override
    public Result getPurchaseReceiptPriceRecord(PurchaseReceiptPriceDTO dto) {
        List<PurchaseReceiptPriceVO> vo = new ArrayList<>();
        if (dto != null) {
            TokenUser user = feignService.getLoginInfo();
            dto.setClient(user.getClient());
            if (StringUtils.isBlank(dto.getCjStatus())) {
                // 查询页面只查询已审核和已记账的差价单
                dto.setType("1,2");
            }
            PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
            vo = gaiaChajiaZMapper.selectPurchaseReceiptPriceOrder(dto);
        }
        return ResultUtil.success(new PageInfo<>(vo));
    }

}
