package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class PurchaseReturns {

    /**
     * 供应商
     */
    @ExcelValidate(index = 0, name = "供应商", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String batSupplierCode;

    /**
     * 商品编码
     */
    @ExcelValidate(index = 1, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String batProCode;

    /**
     * 生产批号
     */
    @ExcelValidate(index = 2, name = "生产批号", type = ExcelValidate.DataType.STRING, maxLength = 30, addRequired = false)
    private String batBatchNo;

    /**
     * 退货数量
     */
    @ExcelValidate(index = 3, name = "退货数量", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = false)
    private String returnQuantity;

    /**
     * 库存地点
     */
    @ExcelValidate(index = 4, name = "库存地点", type = ExcelValidate.DataType.STRING, maxLength = 5, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PO_LOCATION_CODE, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "batLocationCodeValue")
    private String batLocationCode;

    private String batLocationCodeValue;
}

