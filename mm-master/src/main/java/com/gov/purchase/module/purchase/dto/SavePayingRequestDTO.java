package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
public class SavePayingRequestDTO {

    @NotNull(message = "付款总计不能为空")
    private Integer ficoPaymentAmountTotal;

    @Valid
    @NotNull(message = "付款明细列表不能为空")
    List<SavePayingItemRequestDTO> list;

    /**
     * 支付方式 支付宝 微信 云闪付
     */
    private String payMethod;

    @Valid
    @NotNull(message = "发票类型 1-增值税普通发票；2-增值税专用发票")
    private String ficoInvoiceType;

    /**
     * 支付方式　　1:二维码，2:app银联支付，3：网关付款
     */
    private Integer ficoType;

    /**
     * 银联银行
     */
    private String bankName;
    /**
     * 渠道号
     */
    private String chnlNo;

}
