package com.gov.purchase.module.store.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaLabelApply;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.store.dto.EditLabelItemVO;
import com.gov.purchase.module.store.dto.GaiaLabelApplyDto;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.08
 */
public interface LabelApplyService {

    /**
     * 新增申请
     *
     * @param gaiaLabelApplyList
     */
    void addLabel(List<GaiaLabelApply> gaiaLabelApplyList);

    /**
     * 申请 列表
     *
     * @param pageSize
     * @param pageNum
     * @param labApplyDate
     * @param labProduct
     * @param labSuatus
     * @param labStore
     * @return
     */
    PageInfo<GaiaLabelApplyDto> getLabelList(Integer pageSize, Integer pageNum, String labApplyDate, String labProduct, String labSuatus, String labStore);

    /**
     * @param labStore
     * @param labApplyNo
     * @return
     */
    Result getLabelItemList(String labStore, String labApplyNo);

    /**
     * 申请列表选择
     */
    Result selectLabelList(Integer pageSize, Integer pageNum, String labStore, String labApplyNo, String labApplyDateStart, String labApplyDateEnd, Integer labPrintTimes);

    /**
     * 申请 明细 导出
     *
     * @param labStore
     * @param labApplyNo
     * @return
     */
    Result labelItemExport(String labStore, String labApplyNo);

    /**
     * 申请 删除
     *
     * @param labStore
     * @param labApplyNo
     */
    void delLabel(String labStore, String labApplyNo);

    /**
     * 有权限的门店列表
     *
     * @return
     */
    List<GaiaStoreData> getLabelStoList();

    /**
     * 申请列表编辑
     */
    void editLabelItem(EditLabelItemVO vo);

    /**
     * 申请单关闭
     * @param labStore
     * @param labApplyNo
     */
    void closeLable(String labStore, String labApplyNo);
}
