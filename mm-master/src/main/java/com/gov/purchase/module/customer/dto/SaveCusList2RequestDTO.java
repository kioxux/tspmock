package com.gov.purchase.module.customer.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@EqualsAndHashCode
public class SaveCusList2RequestDTO {

    @NotBlank(message = "批发公司不能为空")
    private String dcCode;

    @NotEmpty(message = "客户列表不能为空")
    private List<SaveCusList1RequestDTO> cusBaseList;

    /**
     * 保存1中比对返回的数据
     */
    private List<CusComp> cusCompList;

    @Data
    @EqualsAndHashCode
    public static class CusComp {
        /**
         * 客户名称
         */
        private String cusName;
        /**
         * 统一社会信用代码
         */
        private String cusCreditCode;

    }


}
