package com.gov.purchase.module.delivery.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "商品召回提交传入参数")
public class SaveGoodsRecallRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    /**
     * 连锁公司
     */
    @ApiModelProperty(value = "连锁公司", name = "recChainHead")
    private String recChainHead;

    /**
     * 门店编号
     */
    @ApiModelProperty(value = "门店编号", name = "recStore")
    private String recStore;

    /**
     * 商品自编号
     */
    @ApiModelProperty(value = "商品自编号", name = "recProCode")
    private String recProCode;

    /**
     * 批次号
     */
    @ApiModelProperty(value = "批次号", name = "recBatchNo")
    private String recBatchNo;

    /**
     * 供应商编号
     */
    @ApiModelProperty(value = "供应商编号", name = "recSupplier")
    private String recSupplier;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", name = "recQty")
    private String recQty;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", name = "recRemark")
    private String recRemark;
}