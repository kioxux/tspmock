package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.util.List;

/**
 * @description: 调取连锁采购单
 * @author: yzf
 * @create: 2021-11-18 09:48
 */
@Data
public class ChainPurchaseOrderDto {

    /**
     * 总部id
     */
    private String compadmId;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 客户id
     */
    private String code;

    /**
     * 送达方id
     */
    private String stoCode;

    /**
     * 订单号
     */
    private String poId;

    /**
     * 审核状态
     */
    private String poSoFlag;

    /**
     * 订单开始时间
     */
    private String poStartDate;

    /**
     * 订单结束日期
     */
    private String poEndDate;

    private Integer pageSize;

    private Integer pageNum;

    /**
     * 开单标记
     */
    private String poDnFlag;

    /**
     * 更新人
     */
    private String poUpdateBy;

    /**
     * 更新日期
     */
    private String poUpdateDate;
    /**
     * 更新时间
     */
    private String poUpdateTime;

    /**
     * 商品编码
     */
    private String proCode;

    private List<ChainPurchaseOrderDetailDTO> details;

    /**
     * 价格方式
     */
    private Integer priceType;

    /**
     * 销售员
     */
    private String salesManCode;

    private String poSupplierId;
}
