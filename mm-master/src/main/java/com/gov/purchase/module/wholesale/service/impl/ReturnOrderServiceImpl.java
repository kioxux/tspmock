package com.gov.purchase.module.wholesale.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaSoHeader;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.wholesale.dto.*;
import com.gov.purchase.module.wholesale.service.ReturnOrderService;
import com.gov.purchase.utils.CommonUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.10
 */
@Service
public class ReturnOrderServiceImpl implements ReturnOrderService {

    @Resource
    GaiaSoItemMapper gaiaSoItemMapper;
    @Resource
    GaiaSoHeaderMapper gaiaSoHeaderMapper;
    @Resource
    FeignService feignService;
    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;
    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private RedisClient redisClient;

    /**
     * 销售退货订单列表
     *
     * @param queryReturnOrder
     * @return
     */
    @Override
    public Result querySalesOrderList(QueryReturnOrder queryReturnOrder) {
        // 分页
        PageHelper.startPage(queryReturnOrder.getPageNum(), queryReturnOrder.getPageSize());
        List<ReturnOrder> list = gaiaSoItemMapper.querySalesOrderList(queryReturnOrder);
        PageInfo<ReturnOrder> pageInfo = new PageInfo<>(list);
        return ResultUtil.success(pageInfo);
    }

    /**
     * 销售退货
     *
     * @param list
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result returnOrder(List<ReturnOrderReq> list) {
        TokenUser user = feignService.getLoginInfo();
        Map<String, List<ReturnOrderReq>> map = new HashMap<>();
        // 数据验证
        for (ReturnOrderReq dto : list) {
            ValidationResult result = ValidateUtil.validateEntity(dto);
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
            String soId = dto.getSoId();
            List<ReturnOrderReq> reqs = map.get(soId);
            if (reqs == null) {
                reqs = new ArrayList<>();
            }
            reqs.add(dto);
            map.put(soId, reqs);
        }
        // 销售退货
        for (String key : map.keySet()) {
            // 销售退货明细
            List<ReturnOrderReq> reqs = map.get(key);
            GaiaSoHeaderKey gaiaSoHeaderKey = new GaiaSoHeaderKey();
            gaiaSoHeaderKey.setClient(reqs.get(0).getClient());
            gaiaSoHeaderKey.setSoId(key);
            GaiaSoHeader entity = gaiaSoHeaderMapper.selectByPrimaryKey(gaiaSoHeaderKey);
            if (entity == null) {
                throw new CustomResultException(ResultEnum.E0125);
            }
            // 销售退货主表
            GaiaSoHeader gaiaSoHeader = new GaiaSoHeader();
            //加盟商
            gaiaSoHeader.setClient(reqs.get(0).getClient());
            CommonUtils utils = new CommonUtils();
            String soId = utils.getPoId(entity.getClient(), CommonEnum.SoType.SRE.getCode(), gaiaSoHeaderMapper, redisClient);
            //销售订单号
            gaiaSoHeader.setSoId(soId);
            //销售订单类型
            gaiaSoHeader.setSoType(CommonEnum.SoType.SRE.getCode());
            //客户编码
            gaiaSoHeader.setSoCustomerId(entity.getSoCustomerId());
            //公司代码
            gaiaSoHeader.setSoCompanyCode(entity.getSoCompanyCode());
            //凭证日期
            gaiaSoHeader.setSoDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            // 付款条款
            gaiaSoHeader.setSoPaymentId(entity.getSoPaymentId());
            // 抬头备注
            gaiaSoHeader.setSoHeadRemark(entity.getSoHeadRemark());
            //创建人
            gaiaSoHeader.setSoCreateBy(user.getUserId());
            //创建日期
            gaiaSoHeader.setSoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            //创建时间
            gaiaSoHeader.setSoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            GaiaWmsShenPiPeiZhiDTO gaiaWmsShenPiPeiZhiDTO = gaiaSoHeaderMapper.queryShenPiPeiZhi(user.getClient(), entity.getSoCompanyCode(), 3);
            if (gaiaWmsShenPiPeiZhiDTO != null
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSprxm1())
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())
            ) {
                gaiaSoHeader.setSoApproveStatus("1");
            } else {
                //审批状态
                gaiaSoHeader.setSoApproveStatus("2");
            }
            //审批状态
//            gaiaSoHeader.setSoApproveStatus("0");
            // 销售退货 插入
            gaiaSoHeaderMapper.insertSelective(gaiaSoHeader);
            // 遍历插入销售明细
            for (int i = 0; i < reqs.size(); i++) {
                // 销售明细
                ReturnOrderReq returnOrderReq = reqs.get(i);
                GaiaSoItemKey gaiaSoItemKey = new GaiaSoItemKey();
                gaiaSoItemKey.setClient(returnOrderReq.getClient());
                gaiaSoItemKey.setSoId(returnOrderReq.getSoId());
                gaiaSoItemKey.setSoLineNo(returnOrderReq.getSoLineNo());
                GaiaSoItem soitem = gaiaSoItemMapper.selectByPrimaryKey(gaiaSoItemKey);
                if (soitem == null) {
                    throw new CustomResultException(ResultEnum.E0125);
                }
                // 销售订单明细表
                GaiaSoItem gaiaSoItem = new GaiaSoItem();
                //加盟商
                gaiaSoItem.setClient(entity.getClient());
                //销售订单号
                gaiaSoItem.setSoId(soId);
                //订单行号
                gaiaSoItem.setSoLineNo(String.valueOf(i + 1));
                //商品编码
                gaiaSoItem.setSoProCode(soitem.getSoProCode());
                //销售订单数量
                gaiaSoItem.setSoQty(returnOrderReq.getReturnQty());
                //订单单位
                gaiaSoItem.setSoUnit(soitem.getSoUnit());
                //销售订单单价
                gaiaSoItem.setSoPrice(soitem.getSoPrice());
                //订单行金额
                if (returnOrderReq.getReturnQty() != null && soitem.getSoPrice() != null) {
                    gaiaSoItem.setSoLineAmt(returnOrderReq.getReturnQty().multiply(soitem.getSoPrice()).setScale(4, BigDecimal.ROUND_HALF_UP));
                }
                //地点
                gaiaSoItem.setSoSiteCode((soitem.getSoSiteCode()));
                //库存地点
                gaiaSoItem.setSoLocationCode(soitem.getSoLocationCode());
                //批次
                gaiaSoItem.setSoBatch(soitem.getSoBatch());
                //税率
                gaiaSoItem.setSoRate(soitem.getSoRate());
                //计划发货日期
                gaiaSoItem.setSoDeliveryDate(soitem.getSoDeliveryDate());
                //订单行备注
                gaiaSoItem.setSoLineRemark(soitem.getSoLineRemark());
                //删除标记
                gaiaSoItem.setSoLineDelete("0");
                gaiaSoItem.setSoCompleteFlag("0");
                //退货参考原单号
                gaiaSoItem.setSoReferOrder(returnOrderReq.getSoId());
                //退货参考原单行号
                gaiaSoItem.setSoReferOrderLineno(returnOrderReq.getSoLineNo());
                // 交货已完成标记
                // 退货原因
                gaiaSoItem.setSoReReason(returnOrderReq.getSoReReason());
                gaiaSoItem.setSoBatchNo(returnOrderReq.getBatchNo());
                gaiaSoItemMapper.insertSelective(gaiaSoItem);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 按商品退货
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void returnOrderByProduct(ReturnOrderByProductVO vo) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        returnOrderByProductParamCheck(vo, client);
        String soId = new CommonUtils().getPoId(client, CommonEnum.SoType.SRE.getCode(), gaiaSoHeaderMapper, redisClient);
        GaiaSoHeader gaiaSoHeader = new GaiaSoHeader();
        if (StringUtils.isNotBlank(vo.getOrderType())) {
            // 调取退货单特殊处理(1 单体退货， 2 连锁退货)
            // 单体  主表里的SO_CUSTOMER_ID 和 SO_CUSTOMER2 均为门店编号 PRO_SITE（GAIA_WMS_TUIGONG_Z）
            // 连锁  SO_CUSTOMER_ID为门店编号 和 SO_CUSTOMER2为仓库编码 WM_CK_BH（GAIA_WMS_MENDIANTUIKU_Z）
            if ("1".equalsIgnoreCase(vo.getOrderType())) {
                // NRE 内部批发退回
                gaiaSoHeader.setSoType("NRE");
                gaiaSoHeader.setSoCustomerId(vo.getSiteCode());
                gaiaSoHeader.setSoCustomer2(vo.getSiteCode());
            } else {
                // WOR 委托配送销售订单
                gaiaSoHeader.setSoType("WRE");
                gaiaSoHeader.setSoCustomerId(vo.getSiteCode());
                gaiaSoHeader.setSoCustomer2(vo.getCode());
            }
        }
        if (StringUtils.isBlank(gaiaSoHeader.getSoType())) {
            //客户编码
            gaiaSoHeader.setSoCustomerId(vo.getCustomerId());
            //销售订单类型
            gaiaSoHeader.setSoType(CommonEnum.SoType.SRE.getCode());
        }
        //加盟商
        gaiaSoHeader.setClient(client);
        //销售订单号
        gaiaSoHeader.setSoId(soId);
        //公司代码
        gaiaSoHeader.setSoCompanyCode(vo.getDcCode());
        //凭证日期
        gaiaSoHeader.setSoDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        // 付款条款
        gaiaSoHeader.setSoPaymentId("");
        // 抬头备注
        gaiaSoHeader.setSoHeadRemark(vo.getRemark());
        //创建人
        gaiaSoHeader.setSoCreateBy(user.getUserId());
        //创建日期
        gaiaSoHeader.setSoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        //创建时间
        gaiaSoHeader.setSoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
        //审批状态
        gaiaSoHeader.setSoApproveStatus("0");
        // 销售退货 插入
        gaiaSoHeaderMapper.insertSelective(gaiaSoHeader);

        List<ReturnOrderByProductVO.ReturnProduct> proList = vo.getProList();
        if (CollectionUtils.isEmpty(proList)) {
            throw new CustomResultException("商品编码不能为空");
        }
        AtomicInteger index = new AtomicInteger();
        proList.forEach(item -> returnOrderByProductItem(item, client, vo.getDcCode(), soId, index));

        if (StringUtils.isNotBlank(vo.getOrderType())) {
            if (!CollectionUtils.isEmpty(vo.getProList())) {
                List<String> orderIds = vo.getProList().stream().map(ReturnOrderByProductVO.ReturnProduct::getOrderId).collect(Collectors.toList());
                if ("1".equalsIgnoreCase(vo.getOrderType())) {
                    gaiaSoHeaderMapper.updateStoreReturnOrderType(client, orderIds);
                } else {
                    gaiaSoHeaderMapper.updateChainReturnOrderType(client, orderIds);
                }
            }
        }

    }

    @Override
    public List<GaiaBatchInfo> getProBatchInfo(String site, String proSelfCode) {
        String client = feignService.getLoginInfo().getClient();
        return gaiaSoHeaderMapper.getProBatchInfo(client, site, proSelfCode);
    }

    @Override
    public Result queryReturnOrder(ReturnOrderDTO dto) {
        if (StringUtils.isBlank(dto.getDcWtdc())) {
            throw new CustomResultException("仓库编码不为空");
        }
        String client = feignService.getLoginInfo().getClient();
        dto.setClient(client);
        // 分页
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<ReturnOrderVO> storeOrder = gaiaSoHeaderMapper.queryReturnOrder(dto);
        PageInfo<ReturnOrderVO> pageInfo = new PageInfo<>(storeOrder);
        return ResultUtil.success(pageInfo);
    }

    @Override
    public Result queryReturnOrderDetail(ReturnOrderDTO dto) {
        String client = feignService.getLoginInfo().getClient();
        dto.setClient(client);
        List<ReturnOrderDetailVO> result = new ArrayList<>();
        if (!CollectionUtils.isEmpty(dto.getList())) {
            Map<String, List<ReturnOrderVO>> collect = dto.getList().stream().collect(Collectors.groupingBy(ReturnOrderVO::getOrderType));
            // 单体退货
            if (collect.containsKey("1")) {
                dto.setList(collect.get("1"));
                result = gaiaSoHeaderMapper.queryStoreReturnOrderDetail(dto);
            }
            // 连锁退货
            if (collect.containsKey("2")) {
                dto.setList(collect.get("2"));
                result = gaiaSoHeaderMapper.queryChainReturnOrderDetail(dto);
            }
        }
        return ResultUtil.success(result);
    }

    /**
     * 关闭退货单
     *
     * @param dto
     */
    @Override
    public void closeReturnOrder(ReturnOrderDTO dto) {
        String client = feignService.getLoginInfo().getClient();
        dto.setClient(client);
        if (!CollectionUtils.isEmpty(dto.getList())) {
            Map<String, List<ReturnOrderVO>> collect = dto.getList().stream().collect(Collectors.groupingBy(ReturnOrderVO::getOrderType));
            if (collect.containsKey("1")) {
                dto.setList(collect.get("1"));
                gaiaSoHeaderMapper.closeReturnOrder(dto);
            }
            // 连锁退货
            if (collect.containsKey("2")) {
                dto.setList(collect.get("2"));
                gaiaSoHeaderMapper.closeWmsReturnOrder(dto);
            }
        }
    }

    /**
     * 按商品退货明细
     */
    public void returnOrderByProductItem(ReturnOrderByProductVO.ReturnProduct item, String client, String dcCode, String soId, AtomicInteger index) {
        // 销售订单明细表
        GaiaSoItem gaiaSoItem = new GaiaSoItem();
        //加盟商
        gaiaSoItem.setClient(client);
        //销售订单号
        gaiaSoItem.setSoId(soId);
        //订单行号
        gaiaSoItem.setSoLineNo(String.valueOf(index.incrementAndGet()));
        //商品编码
        gaiaSoItem.setSoProCode(item.getSoProCode());
        //销售订单数量
        gaiaSoItem.setSoQty(item.getSoQty());
        //订单单位
        gaiaSoItem.setSoUnit(item.getSoUnit());
        //销售订单单价
        gaiaSoItem.setSoPrice(item.getSoPrice());
        //订单行金额
        if (item.getSoQty() != null && item.getSoPrice() != null) {
            gaiaSoItem.setSoLineAmt(item.getSoQty().multiply(item.getSoPrice()).setScale(4, BigDecimal.ROUND_HALF_UP));
        }
        //地点
        gaiaSoItem.setSoSiteCode(dcCode);
        //库存地点
        gaiaSoItem.setSoLocationCode(item.getSoLocationCode());
        //批次
        gaiaSoItem.setSoBatch(item.getBatBatchNo());
        //税率
        gaiaSoItem.setSoRate(item.getProOutputTax());
        //计划发货日期
        gaiaSoItem.setSoDeliveryDate(DateUtils.getCurrentDateStrYYMMDD());
        //订单行备注
        gaiaSoItem.setSoLineRemark(item.getSoLineRemark());
        // 退货原因
        gaiaSoItem.setSoReReason(item.getReason());
        //删除标记
        gaiaSoItem.setSoLineDelete("0");
        gaiaSoItem.setSoCompleteFlag("0");
        gaiaSoItem.setSoReferOrder(item.getOrderId());
        gaiaSoItem.setSoReferOrderLineno(item.getLineNo());
        gaiaSoItemMapper.insertSelective(gaiaSoItem);
    }

    private void returnOrderByProductParamCheck(ReturnOrderByProductVO params, String client) {
        // 客户数据
        GaiaCustomerBusiness gaiaCustomerBusiness =
                gaiaCustomerBusinessMapper.getCustomerBusiness(client, params.getDcCode(), params.getCustomerId());
        // 客户的状态，
        if (!CommonEnum.CustomerStauts.CUSTOMERNORMAL.getCode().equals(gaiaCustomerBusiness.getCusStatus())) {
            throw new CustomResultException(ResultEnum.E0110);
        }
        // 禁采、
        if (StringUtils.isNotBlank(gaiaCustomerBusiness.getCusNoSale()) &&
                !CommonEnum.DictionaryStaticData.NO_YES.getList().get(0).getValue().equals(gaiaCustomerBusiness.getCusNoSale())) {
            throw new CustomResultException(ResultEnum.E0113);
        }
        // 禁退，
        if (StringUtils.isNotBlank(gaiaCustomerBusiness.getCusNoReturn()) &&
                !CommonEnum.DictionaryStaticData.NO_YES.getList().get(0).getValue().equals(gaiaCustomerBusiness.getCusNoReturn())) {
            throw new CustomResultException(ResultEnum.E0113);
        }
        // 商品的状态
        for (ReturnOrderByProductVO.ReturnProduct product : params.getProList()) {
            // 商品数据
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(client, params.getDcCode(), product.getSoProCode());
            if (gaiaProductBusiness == null) {
                throw new CustomResultException("商品" + gaiaProductBusiness.getProSelfCode() + "不存在");
            }
            // 非正常
            if (CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(gaiaProductBusiness.getProStatus())) {
                throw new CustomResultException(MessageFormat.format(ResultEnum.E0111.getMsg(), product.getSoProCode()));
            }
            // 禁止配送
            if (CommonEnum.NoYesStatus.YES.getCode().equals(gaiaProductBusiness.getProNoDistributed())) {
                throw new CustomResultException(MessageFormat.format("商品[{0}]已禁止配送，请删除该商品", product.getSoProCode()));
            }
        }
    }

}

