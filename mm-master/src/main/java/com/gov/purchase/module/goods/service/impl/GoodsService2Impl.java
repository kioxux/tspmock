package com.gov.purchase.module.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.gov.purchase.common.diff.DiffEntityConfig;
import com.gov.purchase.common.diff.DiffEntityReuslt;
import com.gov.purchase.common.diff.DiffEntityUtils;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.ListUtils;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.service.BaseService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.goods.dto.*;
import com.gov.purchase.module.goods.service.GoodsService2;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class GoodsService2Impl implements GoodsService2 {

    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    GaiaProductGspinfoMapper gaiaProductGspinfoMapper;
    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    FeignService feignService;
    @Resource
    GaiaProductGspinfoLogMapper gaiaProductGspinfoLogMapper;
    @Resource
    GaiaTaxCodeMapper gaiaTaxCodeMapper;
    @Resource
    GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Resource
    GaiaProductClassMapper gaiaProductClassMapper;
    @Resource
    CosUtils cosUtils;
    @Resource
    GaiaProductChangeMapper gaiaProductChangeMapper;
    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    OperationFeignService operationFeignService;
    @Resource
    DiffEntityConfig diffEntityConfig;
    @Resource
    DiffEntityUtils diffEntityUtils;
    @Resource
    GaiaGlobalGspConfigureMapper gaiaGlobalGspConfigureMapper;
    @Resource
    GaiaNewDistributionPlanMapper gaiaNewDistributionPlanMapper;
    @Resource
    GaiaNewDistributionPlanDetailMapper gaiaNewDistributionPlanDetailMapper;
    @Resource
    BaseService baseService;
    @Resource
    GaiaSdWtproMapper gaiaSdWtproMapper;
    @Resource
    GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Resource
    GaiaBatchInfoMapper gaiaBatchInfoMapper;

    /**
     * 商品首营_回车查询
     */
    @Override
    public List<GetProBasicListDTO> getProBasicList(GetProBasicListRequestDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        // 地点列表
//        List<String> siteList = gaiaProductBasicMapper.getSiteList(client, userId);
        // 只用国际条形码进行查询
        if (!StringUtils.isBlank(dto.getProBarcode()) && StringUtils.isBlank(dto.getProRegisterNo()) && StringUtils.isBlank(dto.getProName())) {
            return getProBasicList(dto, client, userId);
        }
        // 只用批准文号进行查询
        if (StringUtils.isBlank(dto.getProBarcode()) && !StringUtils.isBlank(dto.getProRegisterNo()) && StringUtils.isBlank(dto.getProName())) {
            // 取字符串中的 字母或者数字
            String proRegisterNo = dto.getProRegisterNo().replaceAll("[^a-zA-Z0-9]", "");
            dto.setProRegisterNo(proRegisterNo);
            return getProBasicList(dto, client, userId);
        }
        // 只用商品名称进行查询
        if (StringUtils.isBlank(dto.getProBarcode()) && StringUtils.isBlank(dto.getProRegisterNo()) && !StringUtils.isBlank(dto.getProName())) {
            return getProBasicList(dto, client, userId);
        }
        throw new CustomResultException("请输入[国际条形码], [批准文号], [商品名称]其中一个进行提示查询");
    }

    /**
     * 回车查询
     */
    private List<GetProBasicListDTO> getProBasicList(GetProBasicListRequestDTO dto, String client, String userId) {
        // 1.先匹配basic 表中的数据
        List<GetProBasicListDTO> proBasicListByBusinessList = gaiaProductBasicMapper.getProBasicList(dto, client);
        // 2.若basic表中未匹配到数据，到busiess 表中进行匹配数据
        if (CollectionUtils.isEmpty(proBasicListByBusinessList)) {
            proBasicListByBusinessList = gaiaProductBasicMapper.getProBasicListByBusiness(dto, client);
        } else {
            // 只查出一行的情况
            if (proBasicListByBusinessList.size() == 1) {
                String proCode = gaiaProductBasicMapper.getProHasBeenUsedCode(proBasicListByBusinessList.get(0).getProCode(), client);
                proBasicListByBusinessList.get(0).setOtherProSelfCode(proCode);
            }
        }
        // 3.如果 [匹配结果]或[地点]为空，直接返回
        if (CollectionUtils.isEmpty(proBasicListByBusinessList)) {
            return proBasicListByBusinessList;
        }

        // ‘当前行商品自编码’有值，则直接返回当前行商品自编码
        if (!StringUtil.isEmpty(dto.getCurrentRowProSeflCode())) {
            proBasicListByBusinessList.forEach(item -> item.setProSelfCode(dto.getCurrentRowProSeflCode()));
            return proBasicListByBusinessList;
        }
        // ‘最后一行商品自编码’ 有值，返回 ListUtils.getMaxCode(dto.getLastRowProSeflCode())
        if (!StringUtils.isBlank(dto.getLastRowProSeflCode())) {
            String selfCode = ListUtils.getMaxCode(dto.getLastRowProSeflCode());
            proBasicListByBusinessList.forEach(item -> item.setProSelfCode(selfCode));
            return proBasicListByBusinessList;
        }
        // 自编码集合 基础数据为空，直接返回
        List<String> seflCodelist = gaiaProductBusinessMapper.getSelfCodeList2(client);
        if (CollectionUtils.isEmpty(seflCodelist)) {
            return proBasicListByBusinessList;
        }
        // 设置自编码
        String maxCode = ListUtils.getMaxCode(seflCodelist);
        proBasicListByBusinessList.forEach(item -> {
            String selfCode = ListUtils.getMaxCode(maxCode);
            item.setProSelfCode(selfCode);
        });
        return proBasicListByBusinessList;
    }

    /**
     * 商品首营_商品保存1
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<GaiaProductBusiness> saveCompProList(List<SaveGaiaProductBusiness> list) {

        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        checkParams(list, client);
        // 1.获取有国际条形码的列表
        List<String> barcodeList = list.stream().filter(Objects::nonNull)
                .filter(item -> !StringUtils.isBlank(item.getProBarcode()))
                .map(GaiaProductBusiness::getProBarcode)
                .distinct()
                .collect(Collectors.toList());

        // 2.当前权限下的 地点列表
        List<String> siteList = gaiaProductBasicMapper.getSiteList(client, userId);
        if (CollectionUtils.isEmpty(siteList)) {
            throw new CustomResultException("所有门店/配送中心都不具有权限");
        }

        if (CollectionUtils.isEmpty(barcodeList) || CollectionUtils.isEmpty(siteList)) {
            return null;
        }

        // 3.根据地点+对应行的国际条形码 精确匹配 business表
        List<GaiaProductBusiness> compBusinessList = gaiaProductBasicMapper.getCompBusinessList(client, barcodeList, siteList);
        if (!CollectionUtils.isEmpty(compBusinessList)) {
            return compBusinessList;
        }
        return null;
    }

    /**
     * 商品首营_商品保存2
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> saveBasicProList(SaveBasicProListRequestDTO dto) {
        String client = StringUtils.isEmpty(dto.getClient()) ? feignService.getLoginInfo().getClient() : dto.getClient();
        String userId = StringUtils.isEmpty(dto.getUserId()) ? feignService.getLoginInfo().getUserId() : dto.getUserId();
        // 商品列表基础数据
        List<SaveGaiaProductBusiness> basicsList = dto.getBasicsList();
        checkParams(basicsList, client);
        // 比对的结果数据
        List<SaveCompProListRequestDTO> compProList = dto.getCompProList();
        // 比对结果数据 筛选[操作:忽略] 分组[国际条形码+地点]
        Map<String, List<SaveCompProListRequestDTO>> compProMap = compProList.stream().filter(Objects::nonNull)
                .filter(item -> item.getOperation() != null && !item.getOperation().equals(2))
                .collect(Collectors.groupingBy(item -> item.getProBarcode() + item.getProSite()));

        // 2.当前权限下的 地点列表
        List<String> siteList = gaiaProductBasicMapper.getSiteList(client, userId);
        if (CollectionUtils.isEmpty(siteList)) {
            throw new CustomResultException("所有门店/配送中心都不具有权限");
        }

        // 3.1 非连锁门店集合
        List<String> stoList = gaiaProductBasicMapper.getStoList(client);
        // 3.2 配送中心集合
        List<String> dcList = gaiaProductBasicMapper.getDcList(client);
        List<ProductPositionExcludeDTO> proPosiitonList = gaiaProductBusinessMapper.getProductPositionDefaultValue(client);
        List<GaiaProductClass> gaiaProductClassList = baseService.selectGaiaProductClass();
        List<GaiaProductGspinfo> productGspinfoList = new ArrayList<>();
        // 审批流程编号 (只有dc需要审批)
        HashMap<String, String> flowNoMap = new HashMap<>();
        dcList.forEach(dc -> {
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            String flowNO = System.currentTimeMillis() + com.gov.purchase.utils.StringUtils.leftPad(String.valueOf(i), 6, "0");
            flowNoMap.put(dc, flowNO);
        });
        HashMap<String, String> emptyMap = new HashMap<>();
        stoList.forEach(sto -> {
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            String flowNO = "N" + System.currentTimeMillis() + com.gov.purchase.utils.StringUtils.leftPad(String.valueOf(i), 6, "0");
            emptyMap.put(sto, flowNO);
        });
        List<String> multipleStoreProductSyncStoreList = new ArrayList<>();
        List<String> multipleStoreProductSyncProIdList = new ArrayList<>();
        HashMap<String, String> map = gaiaProductBusinessMapper.getSwitchForPH(client, "WF_APPROVE_USER");
        String para1 = ObjectUtils.isEmpty(map) ? null : map.get("param1").toString();
        String para2 = ObjectUtils.isEmpty(map) ? null : map.get("param2").toString();
        for (SaveGaiaProductBusiness proInfo : basicsList) {
            // 国际条形码
            String proBarcode = proInfo.getProBarcode();
            //铺货标识  只有连锁店才铺货，单体店 不铺货
            boolean flag = false;
            String createBy = userId;
            for (String site : siteList) {
                // 如果匹配到值 判断[新增/忽略]
                if (!CollectionUtils.isEmpty(compProMap)) {
                    List<SaveCompProListRequestDTO> saveCompProListRequestDTOS = compProMap.get(proBarcode + site);
                    // 该条数据[忽略]
                    if (!CollectionUtils.isEmpty(saveCompProListRequestDTOS)) {
                        continue;
                    }
                }

                proInfo.setClient(client);
                proInfo.setProSite(site);
                if (StringUtils.isNotBlank(para1) && StringUtils.isNotBlank(para2)) {
                    if (para1.equals(site)) {
                        createBy = para2;
                    } else {
                        createBy = userId;
                    }
                } else {
                    createBy = userId;
                }
                /**
                 * a.若地点为非连锁门店		地址点集合.CODE IN STORE_DATA 条件 client，门店属性 <> 2
                 *   流程状态直接设置为已完成（即自动审批通过）
                 *   后续插入business表的逻辑按照现有逻辑执行
                 */
                GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                gaiaStoreDataKey.setClient(client);
                gaiaStoreDataKey.setStoCode(site);
                GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                if (!CollectionUtils.isEmpty(stoList) && stoList.contains(site)
                        && org.apache.commons.lang3.StringUtils.isBlank(gaiaStoreData.getStoMdSite())) {
                    // 是否订购
                    ProductPositionExcludeDTO productPositionExcludeDTO = null;
                    if (StringUtils.isNotBlank(proInfo.getProCompclass())) {
                        productPositionExcludeDTO = proPosiitonList.stream().filter(Objects::nonNull).filter(item -> {
                            if (StringUtils.isNotBlank(item.getGppeProCompclassStart()) && proInfo.getProCompclass().startsWith(item.getGppeProCompclassStart())) {
                                return true;
                            }
                            if (StringUtils.isNotBlank(item.getGppeProClassStart()) && proInfo.getProClass().startsWith(item.getGppeProClassStart())) {
                                return true;
                            }
                            return false;
                        }).findFirst().orElse(null);
                    }
                    if (null != productPositionExcludeDTO && StringUtils.isNotBlank(productPositionExcludeDTO.getGppeProPosition())) {
                        proInfo.setProPosition(productPositionExcludeDTO.getGppeProPosition());
                    } else {
                        if (proInfo.getProOrderFlag() != null && proInfo.getProOrderFlag().intValue() == 1) {
                            proInfo.setProPosition("D");
                        } else {
                            proInfo.setProPosition("X");
                        }
                    }

                    saveBusiness(proInfo, multipleStoreProductSyncStoreList, multipleStoreProductSyncProIdList, gaiaProductClassList);
                    saveProInfo(proInfo, emptyMap, productGspinfoList, false, gaiaProductClassList, createBy);
                }
                /**
                 * b.若地点为配送中心（即为连锁总部）				地址点集合.CODE IN DC_DATA 条件 client，DC_TYPE = 30
                 *   则提交的流程需要正常审批
                 *   页面list.get(i).PRO_CODE != NULL				PRO_MATCH_STATUS的值设置成2
                 */
                if (!CollectionUtils.isEmpty(dcList) && dcList.contains(site)) {
                    saveProInfo(proInfo, flowNoMap, productGspinfoList, true, gaiaProductClassList, createBy);
                    flag = true;
                }
            }
            if ("1".equals(dto.getStatus())) {
                //保存铺货计划
                savePlanAndDetail(client, userId, proInfo);
            }
            //国药冷链商品新增第三方匹配表
            HashMap<String, String> param = gaiaProductBusinessMapper.getSwitchForPH(client, "COLD_CHAIN");
            String coldFlag = ObjectUtils.isEmpty(param) ? "0" : param.get("param1").toString();
            if (flag && "1".equals(coldFlag) && StringUtil.isNotEmpty(proInfo.getGoodsownid())) {
                //插入第三方编码表
                Integer i = gaiaSdWtproMapper.checkHas(proInfo);
                if (i > 0) {
                    throw new CustomResultException("该第三方编码已存在，请在工作流中审核！请勿重复添加！");
                }
                insertSdWtPro(client, proInfo);
            }
        }
        // 提交后自动新建工作流，流程节点根据人员所属门店号，从权限表取出对应质量负责人、企业负责人。审批流程：提交人（已提交）->质量->企业负责人->END

        if (!CollectionUtils.isEmpty(productGspinfoList)) {
            // 按照地点分组，每个地点发送一条审批
            Map<String, List<GaiaProductGspinfo>> collect = productGspinfoList.stream().collect(Collectors.groupingBy(GaiaProductGspinfo::getProSite));
            collect.forEach((proSite, infoList) -> {
                if (StringUtils.isEmpty(dto.getClient())) {
                    FeignResult result = feignService.createProductWorkflow2(infoList, flowNoMap.get(proSite));
                    if (result.getCode() != 0) {
                        throw new CustomResultException(result.getMessage());
                    }
                } else {
                    FeignResult result = feignService.createProductWorkflowNew(infoList, flowNoMap.get(proSite), dto.getClient(), dto.getUserId());
                    if (result.getCode() != 0) {
                        throw new CustomResultException(result.getMessage());
                    }
                }
            });
        }

        Map<String, Object> result = new HashMap<>();
        result.put("client", client);
        result.put("proSiteList", multipleStoreProductSyncStoreList.stream().distinct().collect(Collectors.toList()));
        result.put("proSelfCodeList", multipleStoreProductSyncProIdList.stream().distinct().collect(Collectors.toList()));
        return result;
    }

    private void insertSdWtPro(String client, SaveGaiaProductBusiness proInfo) {
        GaiaSdWtpro wtpro = new GaiaSdWtpro();
        wtpro.setClient(client);
        wtpro.setProSelfCode(proInfo.getProSelfCode());
        wtpro.setProDsfCode(proInfo.getGoodsownid());
        wtpro.setProDsfName(proInfo.getGoodsname());
        wtpro.setProNm(proInfo.getGoodsno());
        wtpro.setPrice(proInfo.getResaleprice());
        wtpro.setStatus(0);
        gaiaSdWtproMapper.insert(wtpro);
    }

    private void savePlanAndDetail(String client, String userId, SaveGaiaProductBusiness proInfo) {
        log.info("<新品铺货计划><参数：{}>", JSON.toJSONString(proInfo));
        // 剔除成分分类开 头为A20、A21、A22、商品分类开头为8、301、 302的商品
        //Boolean insertFlag = checkClassCode(proInfo);
        if (ObjectUtils.isEmpty(proInfo.getDistributionDTO())) {
            throw new CustomResultException("请先创建铺货计划！");
        }
        DistributionDTO distributionDTO = proInfo.getDistributionDTO();
        log.info("<新品铺货计划><计划详情：{}>", JSON.toJSONString(distributionDTO));
        Date now = new Date();
        //1-门店属性
        if (!ObjectUtils.isEmpty(distributionDTO.getPlanType()) && distributionDTO.getPlanType() == 1) {
            if (ObjectUtils.isEmpty(distributionDTO.getStoAttributesList())) {
                throw new CustomResultException("门店属性列表为空");
            }
            List<DistributionDetailDTO> stoAttributesList = distributionDTO.getStoAttributesList();
            List<String> codeList = stoAttributesList.stream().map(DistributionDetailDTO::getAttribute).collect(Collectors.toList());
            List<DistributionDetailDTO> stoCodeList = gaiaProductBasicMapper.listSto(client, codeList, null, null, null, "DX0001", distributionDTO.getIsIncludeClient());
            if (CollectionUtils.isEmpty(stoCodeList)) {
                throw new CustomResultException("创建铺货计划失败，未查询到所选择门店属性的门店！");
            }
            Map<String, List<DistributionDetailDTO>> collect = stoCodeList.stream().collect(Collectors.groupingBy(DistributionDetailDTO::getAttribute));
            //插入铺货计划主表
            GaiaNewDistributionPlan plan = new GaiaNewDistributionPlan();
            plan.setClient(client);
            String planCode = gaiaNewDistributionPlanMapper.getPlanCode(client);
            plan.setPlanCode(planCode);
            plan.setProSelfCode(proInfo.getProSelfCode());
            plan.setProSelfName(proInfo.getProName());
            plan.setPlanType(distributionDTO.getPlanType());
            plan.setPlanDays(distributionDTO.getPlanDays());
            Date date = DateUtils.addDate(now, distributionDTO.getPlanDays());
            plan.setExpireDate(date);
            plan.setDistributionType(distributionDTO.getDistributionType());
            plan.setPlanStoreNum(stoCodeList.size());
            plan.setStatus(0);
            plan.setCreateTime(now);
            plan.setCreateUser(userId);
            plan.setUpdateTime(now);
            plan.setUpdateUser(userId);

            //插入铺货计划子表
            List<GaiaNewDistributionPlanDetail> detailList = new ArrayList<>();
            BigDecimal planQty = BigDecimal.ZERO;
            for (DistributionDetailDTO distributionDetailDTO : stoAttributesList) {
                List<DistributionDetailDTO> distributionDetailDTOS = collect.get(distributionDetailDTO.getAttribute());
                if (!CollectionUtils.isEmpty(distributionDetailDTOS)) {
                    for (DistributionDetailDTO detailDTO : distributionDetailDTOS) {
                        if (distributionDetailDTO.getAttribute().equals(detailDTO.getAttribute())) {
                            GaiaNewDistributionPlanDetail detail = new GaiaNewDistributionPlanDetail();
                            detail.setClient(client);
                            detail.setPlanCode(planCode);
                            detail.setProSelfCode(plan.getProSelfCode());
                            detail.setStoreId(detailDTO.getStoCode());
                            detail.setStoreName(detailDTO.getStoName());
                            detail.setPlanQuantity(distributionDetailDTO.getQty());
                            detail.setDeleteFlag(0);
                            detail.setCreateTime(now);
                            detail.setCreateUser(userId);
                            detail.setUpdateTime(now);
                            detail.setUpdateUser(userId);
                            detailList.add(detail);
                            planQty = planQty.add(detail.getPlanQuantity());
                        }
                    }
                }
            }
            plan.setPlanQuantity(planQty);
            gaiaNewDistributionPlanMapper.add(plan);
            if (!CollectionUtils.isEmpty(detailList)) {
                gaiaNewDistributionPlanDetailMapper.addList(detailList);
            }
        }
        //2-门店类型
        if (!ObjectUtils.isEmpty(distributionDTO.getPlanType()) && distributionDTO.getPlanType() == 2) {
            if (ObjectUtils.isEmpty(distributionDTO.getStoTypeList())) {
                throw new CustomResultException("门店类型列表为空");
            }
            List<DistributionDetailDTO> stoTypeList = distributionDTO.getStoTypeList();
            List<String> codeList = stoTypeList.stream().map(DistributionDetailDTO::getGssgId).collect(Collectors.toList());
            List<DistributionDetailDTO> dtoList = gaiaProductBasicMapper.listSto(client, null, codeList, null, null, "DX0001", distributionDTO.getIsIncludeClient());
            if (CollectionUtils.isEmpty(dtoList)) {
                throw new CustomResultException("创建铺货计划失败，未查询到所选择门店类型的门店！");
            }
            Map<String, List<DistributionDetailDTO>> collect = dtoList.stream().collect(Collectors.groupingBy(DistributionDetailDTO::getGssgId));
            //插入铺货计划主表
            GaiaNewDistributionPlan plan = new GaiaNewDistributionPlan();
            plan.setClient(client);
            String planCode = gaiaNewDistributionPlanMapper.getPlanCode(client);
            plan.setPlanCode(planCode);
            plan.setProSelfCode(proInfo.getProSelfCode());
            plan.setProSelfName(proInfo.getProName());
            plan.setPlanType(distributionDTO.getPlanType());
            plan.setPlanDays(distributionDTO.getPlanDays());
            Date date = DateUtils.addDate(now, distributionDTO.getPlanDays());
            plan.setExpireDate(date);
            plan.setDistributionType(distributionDTO.getDistributionType());
            plan.setPlanStoreNum(dtoList.size());
            plan.setStatus(0);
            plan.setCreateTime(now);
            plan.setCreateUser(userId);
            plan.setUpdateTime(now);
            plan.setUpdateUser(userId);
            //铺货店型保存是否勾选加盟商
            plan.setIsIncludeClient(Integer.valueOf(distributionDTO.getIsIncludeClient()));
            //插入铺货计划子表
            List<GaiaNewDistributionPlanDetail> detailList = new ArrayList<>();
            BigDecimal planQty = BigDecimal.ZERO;
            for (DistributionDetailDTO distributionDetailDTO : stoTypeList) {
                List<DistributionDetailDTO> distributionDetailDTOS = collect.get(distributionDetailDTO.getGssgId());
                if (!CollectionUtils.isEmpty(distributionDetailDTOS)) {
                    for (DistributionDetailDTO detailDTO : distributionDetailDTOS) {
                        if (distributionDetailDTO.getGssgId().equals(detailDTO.getGssgId())) {
                            GaiaNewDistributionPlanDetail detail = new GaiaNewDistributionPlanDetail();
                            detail.setClient(client);
                            detail.setPlanCode(planCode);
                            detail.setProSelfCode(plan.getProSelfCode());
                            detail.setStoreId(detailDTO.getStoCode());
                            detail.setStoreName(detailDTO.getStoName());
                            detail.setPlanQuantity(distributionDetailDTO.getQty());
                            detail.setDeleteFlag(0);
                            detail.setCreateTime(now);
                            detail.setCreateUser(userId);
                            detail.setUpdateTime(now);
                            detail.setUpdateUser(userId);
                            detailList.add(detail);
                            planQty = planQty.add(detail.getPlanQuantity());
                        }
                    }
                }
            }
            plan.setPlanQuantity(planQty);
            gaiaNewDistributionPlanMapper.add(plan);
            if (!CollectionUtils.isEmpty(detailList)) {
                gaiaNewDistributionPlanDetailMapper.addList(detailList);
            }
        }
        //3-门店
        if (!ObjectUtils.isEmpty(distributionDTO.getPlanType()) && distributionDTO.getPlanType() == 3) {
            if (ObjectUtils.isEmpty(distributionDTO.getStoCodeList())) {
                throw new CustomResultException("门店列表为空");
            }
            List<DistributionDetailDTO> stoCodeList = distributionDTO.getStoCodeList();
            if (CollectionUtils.isEmpty(stoCodeList)) {
                throw new CustomResultException("创建铺货计划失败，未查询到所选择门店！");
            }

            //插入铺货计划主表
            GaiaNewDistributionPlan plan = new GaiaNewDistributionPlan();
            plan.setClient(client);
            String planCode = gaiaNewDistributionPlanMapper.getPlanCode(client);
            plan.setPlanCode(planCode);
            plan.setProSelfCode(proInfo.getProSelfCode());
            plan.setProSelfName(proInfo.getProName());
            plan.setPlanType(distributionDTO.getPlanType());
            plan.setPlanDays(distributionDTO.getPlanDays());
            Date date = DateUtils.addDate(now, distributionDTO.getPlanDays());
            plan.setExpireDate(date);
            plan.setDistributionType(distributionDTO.getDistributionType());
            plan.setPlanStoreNum(stoCodeList.size());
            plan.setStatus(0);
            plan.setCreateTime(now);
            plan.setCreateUser(userId);
            plan.setUpdateTime(now);
            plan.setUpdateUser(userId);

            //插入铺货计划子表
            List<GaiaNewDistributionPlanDetail> detailList = new ArrayList<>();
            BigDecimal planQty = BigDecimal.ZERO;
            for (DistributionDetailDTO distributionDetailDTO : stoCodeList) {
                GaiaNewDistributionPlanDetail detail = new GaiaNewDistributionPlanDetail();
                detail.setClient(client);
                detail.setPlanCode(planCode);
                detail.setProSelfCode(plan.getProSelfCode());
                detail.setStoreId(distributionDetailDTO.getStoCode());
                detail.setStoreName(distributionDetailDTO.getStoName());
                detail.setPlanQuantity(distributionDetailDTO.getQty());
                detail.setDeleteFlag(0);
                detail.setCreateTime(now);
                detail.setCreateUser(userId);
                detail.setUpdateTime(now);
                detail.setUpdateUser(userId);
                detailList.add(detail);
                planQty = planQty.add(detail.getPlanQuantity());
            }
            plan.setPlanQuantity(planQty);
            gaiaNewDistributionPlanMapper.add(plan);
            if (!CollectionUtils.isEmpty(detailList)) {
                gaiaNewDistributionPlanDetailMapper.addList(detailList);
            }
        }
        //4-店效级别
        if (!ObjectUtils.isEmpty(distributionDTO.getPlanType()) && distributionDTO.getPlanType() == 4) {
            if (ObjectUtils.isEmpty(distributionDTO.getStoEffectList())) {
                throw new CustomResultException("门店店效列表为空");
            }
            List<DistributionDetailDTO> stoEffectList = distributionDTO.getStoEffectList();
            List<String> effectList = stoEffectList.stream().map(DistributionDetailDTO::getGssgId).collect(Collectors.toList());
            List<DistributionDetailDTO> dtoList = gaiaProductBasicMapper.listSto(client, null, null, null, effectList, "DX0002", distributionDTO.getIsIncludeClient());
            if (CollectionUtils.isEmpty(dtoList)) {
                throw new CustomResultException("创建铺货计划失败，未查询到所选择店效类型的门店！");
            }
            Map<String, List<DistributionDetailDTO>> collect = dtoList.stream().collect(Collectors.groupingBy(DistributionDetailDTO::getGssgId));
            //插入铺货计划主表
            GaiaNewDistributionPlan plan = new GaiaNewDistributionPlan();
            plan.setClient(client);
            String planCode = gaiaNewDistributionPlanMapper.getPlanCode(client);
            plan.setPlanCode(planCode);
            plan.setProSelfCode(proInfo.getProSelfCode());
            plan.setProSelfName(proInfo.getProName());
            plan.setPlanType(distributionDTO.getPlanType());
            plan.setPlanDays(distributionDTO.getPlanDays());
            Date date = DateUtils.addDate(now, distributionDTO.getPlanDays());
            plan.setExpireDate(date);
            plan.setDistributionType(distributionDTO.getDistributionType());
            plan.setPlanStoreNum(dtoList.size());
            plan.setStatus(0);
            plan.setCreateTime(now);
            plan.setCreateUser(userId);
            plan.setUpdateTime(now);
            plan.setUpdateUser(userId);
            //店效级别保存是否勾选加盟商
            plan.setIsIncludeClient(Integer.valueOf(distributionDTO.getIsIncludeClient()));
            //插入铺货计划子表
            List<GaiaNewDistributionPlanDetail> detailList = new ArrayList<>();
            BigDecimal planQty = BigDecimal.ZERO;
            for (DistributionDetailDTO distributionDetailDTO : stoEffectList) {
                List<DistributionDetailDTO> distributionDetailDTOS = collect.get(distributionDetailDTO.getGssgId());
                if (!CollectionUtils.isEmpty(distributionDetailDTOS)) {
                    for (DistributionDetailDTO detailDTO : distributionDetailDTOS) {
                        if (distributionDetailDTO.getGssgId().equals(detailDTO.getGssgId())) {
                            GaiaNewDistributionPlanDetail detail = new GaiaNewDistributionPlanDetail();
                            detail.setClient(client);
                            detail.setPlanCode(planCode);
                            detail.setProSelfCode(plan.getProSelfCode());
                            detail.setStoreId(detailDTO.getStoCode());
                            detail.setStoreName(detailDTO.getStoName());
                            detail.setPlanQuantity(distributionDetailDTO.getQty());
                            detail.setDeleteFlag(0);
                            detail.setCreateTime(now);
                            detail.setCreateUser(userId);
                            detail.setUpdateTime(now);
                            detail.setUpdateUser(userId);
                            detailList.add(detail);
                            planQty = planQty.add(detail.getPlanQuantity());
                        }
                    }
                }
            }
            plan.setPlanQuantity(planQty);
            gaiaNewDistributionPlanMapper.add(plan);
            if (!CollectionUtils.isEmpty(detailList)) {
                gaiaNewDistributionPlanDetailMapper.addList(detailList);
            }
        }
    }

    private Boolean checkClassCode(SaveGaiaProductBusiness proInfo) {
        Boolean flag = true;
        if (proInfo.getProClass().startsWith("8")) {
            flag = false;
        }
        if (proInfo.getProClass().startsWith("301")) {
            flag = false;
        }
        if (proInfo.getProClass().startsWith("302")) {
            flag = false;
        }
        if (proInfo.getProCompclass().startsWith("A20")) {
            flag = false;
        }
        if (proInfo.getProCompclass().startsWith("A21")) {
            flag = false;
        }
        if (proInfo.getProCompclass().startsWith("A22")) {
            flag = false;
        }
        return flag;
    }


    private void saveProInfo(SaveGaiaProductBusiness gaiaProduct, HashMap<String, String> proFlowNoMap, List<GaiaProductGspinfo> list, boolean flowFlag,
                             List<GaiaProductClass> gaiaProductClassList, String userId) {
        // 参考毛利率按照零售、采购价自动计算存入，公式：（参考售价-参考进价）/参考售价*100%（若参考售价为空或零，直接赋值0）
        // 参考进货价
        BigDecimal pro_cgj = gaiaProduct.getProCgj() == null ? BigDecimal.ZERO : gaiaProduct.getProCgj();
        // 参考零售价
        BigDecimal pro_lsj = gaiaProduct.getProLsj() == null ? BigDecimal.ZERO : gaiaProduct.getProLsj();
        // 参考毛利率
        BigDecimal pro_mll = BigDecimal.ZERO;
        // 若参考售价为空或零，直接赋值0
        if (pro_lsj.compareTo(BigDecimal.ZERO) != 0) {
            // （参考售价-参考进价）/参考售价*100%
            pro_mll = (pro_lsj.subtract(pro_cgj)).divide(pro_lsj, 4, BigDecimal.ROUND_HALF_UP);
        }

        GaiaProductGspinfo productGspinfo = new GaiaProductGspinfo();
        BeanUtils.copyProperties(gaiaProduct, productGspinfo);
        productGspinfo.setProCreateBy(userId);
        if (StringUtils.isBlank(productGspinfo.getProBigPackage())) {
            productGspinfo.setProBigPackage("1");
        }
        //首营状态
        if (flowFlag) {
            productGspinfo.setProGspStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
        } else {
            productGspinfo.setProGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
        }
        // 工作流编码
        productGspinfo.setProFlowNo(proFlowNoMap.get(productGspinfo.getProSite()));
        // 中包装量
        productGspinfo.setProMidPackage(com.gov.purchase.utils.StringUtils.isBlank(productGspinfo.getProMidPackage()) ? "1" : productGspinfo.getProMidPackage());
        // 如果是主数据表和首表同时设置，那么不用做校验，因为在这种情况下主数据表中一定存在数据
        if (flowFlag) {
            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setClient(productGspinfo.getClient());
            gaiaProductBusinessKey.setProSelfCode(productGspinfo.getProSelfCode());
            gaiaProductBusinessKey.setProSite(productGspinfo.getProSite());
            GaiaProductBusiness entity = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
            if (entity != null) {
                throw new CustomResultException("商品编码" + entity.getProSelfCode() + "已存在");
            }
        }
        List<GaiaProductGspinfo> gaiaProductGspinfoList = gaiaProductGspinfoMapper.selfOnlyCheck(productGspinfo.getClient(), productGspinfo.getProSelfCode(), productGspinfo.getProSite());
        if (!CollectionUtils.isEmpty(gaiaProductGspinfoList)) {
            throw new CustomResultException("商品编码" + productGspinfo.getProSelfCode() + "已存在");
        }
        // 商品描述
        if (org.apache.commons.lang3.StringUtils.isBlank(productGspinfo.getProDepict())) {
            String proDepict = productGspinfo.getProCommonname();
            if (StringUtils.isNotBlank(productGspinfo.getProCommonname()) && !productGspinfo.getProCommonname().equals(productGspinfo.getProName())) {
                String proNameTemp = StringUtils.isBlank(productGspinfo.getProName()) ? "" : "(" + productGspinfo.getProName() + ")";
                proDepict = productGspinfo.getProCommonname() + proNameTemp;
            }
            productGspinfo.setProDepict(proDepict);
        }
        // 商品描述
        if (org.apache.commons.lang3.StringUtils.isBlank(productGspinfo.getProDepict())) {
            String proDepict = productGspinfo.getProCommonname();
            if (StringUtils.isNotBlank(productGspinfo.getProCommonname()) && !productGspinfo.getProCommonname().equals(productGspinfo.getProName())) {
                String proNameTemp = StringUtils.isBlank(productGspinfo.getProName()) ? "" : "(" + productGspinfo.getProName() + ")";
                proDepict = productGspinfo.getProCommonname() + proNameTemp;
            }
            productGspinfo.setProDepict(proDepict);
        }
        // 拼音码
        if (org.apache.commons.lang3.StringUtils.isBlank(productGspinfo.getProPym())) {
            productGspinfo.setProPym(com.gov.purchase.utils.StringUtils.ToFirstChar(productGspinfo.getProDepict()));
        }
        // 首营日期
        if (StringUtils.isBlank(gaiaProduct.getProGspDate())) {
            productGspinfo.setProGspDate(DateUtils.getCurrentDateStrYYMMDD());
        }
        // 当商品首营时，如果选择商品分类未指定到小类（只指定了大类或者中类），存表时系统自动根据大类，用9补齐5位值。
        // 比如用户商品分类仅选择3-中药，则存表时，默认存39999；又比如用户商品分类选择201-抗肿瘤用药，则存表时，默认存29999。
        if (StringUtils.isNotBlank(productGspinfo.getProClass()) && StringUtils.length(productGspinfo.getProClass().trim()) < 5) {
            productGspinfo.setProClass(StringUtils.rightPad(StringUtils.left(productGspinfo.getProClass(), 1), 5, "9"));
            if (!CollectionUtils.isEmpty(gaiaProductClassList)) {
                GaiaProductClass gaiaProductClass = gaiaProductClassList.stream().filter(item -> productGspinfo.getProClass().equals(item.getProClassCode())).findFirst().orElse(null);
                if (gaiaProductClass != null) {
                    productGspinfo.setProClassName(gaiaProductClass.getProClassName());
                }
            }
        }
        // 20210726:新增pro_code写入逻辑，如果未引用pro_code，则默认写成99999999
        if (com.gov.purchase.utils.StringUtils.isBlank(productGspinfo.getProCode())) {
            productGspinfo.setProCode("99999999");
        }
        productGspinfo.setProFixBin(StringUtils.isBlank(productGspinfo.getProFixBin()) ? productGspinfo.getProSelfCode() : productGspinfo.getProFixBin());
        gaiaProductGspinfoMapper.insert(productGspinfo);
        if (flowFlag) {
            list.add(productGspinfo);
            // 首营日志
            GaiaProductGspinfoLog gaiaProductGspinfoLogOld = new GaiaProductGspinfoLog();
            BeanUtils.copyProperties(productGspinfo, gaiaProductGspinfoLogOld);
            //加盟商
            gaiaProductGspinfoLogOld.setClient(productGspinfo.getClient());
            //商品自编码
            gaiaProductGspinfoLogOld.setProSelfCode(productGspinfo.getProSelfCode());
            //地点
            gaiaProductGspinfoLogOld.setProSite(productGspinfo.getProSite());
            gaiaProductGspinfoLogOld.setProLine(1);
            gaiaProductGspinfoLogOld.setProMll(pro_mll);
            gaiaProductGspinfoLogMapper.insertSelective(gaiaProductGspinfoLogOld);

            GaiaProductGspinfoLog gaiaProductGspinfoLogNew = new GaiaProductGspinfoLog();
            BeanUtils.copyProperties(productGspinfo, gaiaProductGspinfoLogNew);
            gaiaProductGspinfoLogNew.setProLine(2);
            gaiaProductGspinfoLogNew.setProMll(pro_mll);
            gaiaProductGspinfoLogMapper.insertSelective(gaiaProductGspinfoLogNew);
        }

    }

    private void saveBusiness(GaiaProductBusiness business, List<String> multipleStoreProductSyncStoreList, List<String> multipleStoreProductSyncProIdList,
                              List<GaiaProductClass> gaiaProductClassList) {
        GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
        BeanUtils.copyProperties(business, gaiaProductBusiness);
        // 参考毛利率按照零售、采购价自动计算存入，公式：（参考售价-参考进价）/参考售价*100%（若参考售价为空或零，直接赋值0）
        // 参考进货价
        BigDecimal pro_cgj = gaiaProductBusiness.getProCgj() == null ? BigDecimal.ZERO : gaiaProductBusiness.getProCgj();
        // 参考零售价
        BigDecimal pro_lsj = gaiaProductBusiness.getProLsj() == null ? BigDecimal.ZERO : gaiaProductBusiness.getProLsj();
        // 参考毛利率
        BigDecimal pro_mll = BigDecimal.ZERO;
        // 若参考售价为空或零，直接赋值0
        if (pro_lsj.compareTo(BigDecimal.ZERO) != 0) {
            // （参考售价-参考进价）/参考售价*100%
            pro_mll = (pro_lsj.subtract(pro_cgj)).divide(pro_lsj, 4, BigDecimal.ROUND_HALF_UP);
        }

        // 销项税率
        String pro_output_tax = gaiaProductBusiness.getProOutputTax();
        // 进项税率
        String pro_input_tax = gaiaProductBusiness.getProInputTax();
        if (com.gov.purchase.utils.StringUtils.isBlank(pro_output_tax)) {
            try {
                // 税率
                GaiaTaxCode gaiaTaxCode = gaiaTaxCodeMapper.selectByPrimaryKey(pro_input_tax);
                List<GaiaTaxCode> gaiaTaxCodeList = gaiaTaxCodeMapper.selectTaxList();
                // 销项税率集合
                List<GaiaTaxCode> boys = gaiaTaxCodeList.stream().filter(s -> s.getTaxCodeValue().equals(gaiaTaxCode.getTaxCodeValue()) && s.getTaxCodeClass().equals("2")).collect(Collectors.toList());
                pro_output_tax = boys.get(0).getTaxCode();
            } catch (Exception e) {
                pro_output_tax = pro_input_tax.replace("J", "X");
            }
        }

        // 商品业务表
        gaiaProductBusiness.setProCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaProductBusiness.setProOutputTax(pro_output_tax);
        gaiaProductBusiness.setProInputTax(pro_input_tax);
        // 商品描述
        if (org.apache.commons.lang3.StringUtils.isBlank(gaiaProductBusiness.getProDepict())) {
            String proDepict = gaiaProductBusiness.getProCommonname();
            if (StringUtils.isNotBlank(gaiaProductBusiness.getProCommonname()) && !gaiaProductBusiness.getProCommonname().equals(gaiaProductBusiness.getProName())) {
                String proNameTemp = StringUtils.isBlank(gaiaProductBusiness.getProName()) ? "" : "(" + gaiaProductBusiness.getProName() + ")";
                proDepict = gaiaProductBusiness.getProCommonname() + proNameTemp;
            }
            gaiaProductBusiness.setProDepict(proDepict);
        }
        // 助记码
        if (com.gov.purchase.utils.StringUtils.isBlank(gaiaProductBusiness.getProPym())) {
            gaiaProductBusiness.setProPym(com.gov.purchase.utils.StringUtils.ToFirstChar(gaiaProductBusiness.getProDepict()));
        }
        // 中包装量
        gaiaProductBusiness.setProMidPackage(com.gov.purchase.utils.StringUtils.isBlank(gaiaProductBusiness.getProMidPackage()) ? "1" : gaiaProductBusiness.getProMidPackage());
        // 固定货位
        gaiaProductBusiness.setProFixBin(StringUtils.isBlank(gaiaProductBusiness.getProFixBin()) ? gaiaProductBusiness.getProSelfCode() : gaiaProductBusiness.getProFixBin());
        // 启用电子监管码
        gaiaProductBusiness.setProElectronicCode(StringUtils.isBlank(gaiaProductBusiness.getProElectronicCode()) ? CommonEnum.NoYesStatus.NO.getCode() : gaiaProductBusiness.getProElectronicCode());
        gaiaProductBusiness.setProMll(pro_mll);
//        // 国际条形码
//        gaiaProductBusiness.setProBarcode(dto.getProBarcode());
        // 匹配状态
        gaiaProductBusiness.setProMatchStatus(CommonEnum.MatchStatus.EXACTMATCH.getCode());
        gaiaProductBusiness.setProStatus(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
        if (com.gov.purchase.utils.StringUtils.isBlank(gaiaProductBusiness.getProStorageCondition())) {
            gaiaProductBusiness.setProStorageCondition("1");
        }
        if (com.gov.purchase.utils.StringUtils.isBlank(gaiaProductBusiness.getProCompclass())) {
            gaiaProductBusiness.setProCompclass("N999999");
            gaiaProductBusiness.setProCompclassName("N999999未匹配");
        }

        gaiaProductBusiness.setProStatus(StringUtils.isBlank(gaiaProductBusiness.getProStatus()) ? CommonEnum.GoodsStauts.GOODSNORMAL.getCode() : gaiaProductBusiness.getProStatus());
//        // 默认值
//        //商品状态正常
//        gaiaProductBusiness.setProStatus(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
//        // 禁止销售
//        gaiaProductBusiness.setProNoRetail(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止采购
//        gaiaProductBusiness.setProNoPurchase(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止配送
//        gaiaProductBusiness.setProNoDistributed(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止退厂
//        gaiaProductBusiness.setProNoSupplier(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止退仓
//        gaiaProductBusiness.setProNoDc(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止调剂
//        gaiaProductBusiness.setProNoAdjust(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止批发
//        gaiaProductBusiness.setProNoSale(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止请货
//        gaiaProductBusiness.setProNoApply(CommonEnum.NoYesStatus.NO.getCode());
//        // 是否医保
//        gaiaProductBusiness.setProIfMed(CommonEnum.NoYesStatus.NO.getCode());
//        // 按中包装量倍数配货
//        gaiaProductBusiness.setProPackageFlag(CommonEnum.NoYesStatus.NO.getCode());
//        // 是否重点养护
//        gaiaProductBusiness.setProKeyCare(CommonEnum.NoYesStatus.NO.getCode());

        GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
        gaiaProductBusinessKey.setClient(gaiaProductBusiness.getClient());
        gaiaProductBusinessKey.setProSelfCode(gaiaProductBusiness.getProSelfCode());
        gaiaProductBusinessKey.setProSite(gaiaProductBusiness.getProSite());
        GaiaProductBusiness entity = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
        if (entity != null) {
            throw new CustomResultException("商品编码" + entity.getProSelfCode() + "已存在");
        }
        List<GaiaProductGspinfo> gaiaProductGspinfoList = gaiaProductGspinfoMapper.selfOnlyCheck(gaiaProductBusiness.getClient(), gaiaProductBusiness.getProSelfCode(), gaiaProductBusiness.getProSite());
        if (!CollectionUtils.isEmpty(gaiaProductGspinfoList)) {
            throw new CustomResultException("商品编码" + entity.getProSelfCode() + "已存在");
        }
        // 当商品首营时，如果选择商品分类未指定到小类（只指定了大类或者中类），存表时系统自动根据大类，用9补齐5位值。
        // 比如用户商品分类仅选择3-中药，则存表时，默认存39999；又比如用户商品分类选择201-抗肿瘤用药，则存表时，默认存29999。
        if (StringUtils.isNotBlank(gaiaProductBusiness.getProClass()) && StringUtils.length(gaiaProductBusiness.getProClass().trim()) < 5) {
            gaiaProductBusiness.setProClass(StringUtils.rightPad(StringUtils.left(gaiaProductBusiness.getProClass(), 1), 5, "9"));
            // 商品分名称
            if (!CollectionUtils.isEmpty(gaiaProductClassList)) {
                GaiaProductClass gaiaProductClass = gaiaProductClassList.stream().filter(item -> gaiaProductBusiness.getProClass().equals(item.getProClassCode())).findFirst().orElse(null);
                if (gaiaProductClass != null) {
                    gaiaProductBusiness.setProClassName(gaiaProductClass.getProClassName());
                }
            }
        }
        // 20210726:新增pro_code写入逻辑，如果未引用pro_code，则默认写成99999999
        if (com.gov.purchase.utils.StringUtils.isBlank(gaiaProductBusiness.getProCode())) {
            gaiaProductBusiness.setProCode("99999999");
        }
        if (StringUtils.isBlank(gaiaProductBusiness.getProBigPackage())) {
            gaiaProductBusiness.setProBigPackage("1");
        }
        gaiaProductBusinessMapper.insert(gaiaProductBusiness);
        //检查参数表，若参数存在且为开启状态（值为1）时
        String para = gaiaSdReplenishHMapper.getParamByClientAndGcspId(gaiaProductBusiness.getClient(), "WTPS_SP_AUTO");
        if (StringUtils.isNotBlank(para) && "1".equalsIgnoreCase(para)) {
            GaiaSdWtpro sdWtpro = new GaiaSdWtpro();
            sdWtpro.setClient(gaiaProductBusiness.getClient());
            sdWtpro.setProSelfCode(gaiaProductBusiness.getProSelfCode());
            GaiaSdWtpro result = gaiaSdWtproMapper.queryById(sdWtpro);
            if (result == null) {
                sdWtpro.setProDsfCode(gaiaProductBusiness.getProSelfCode());
                sdWtpro.setProNm(gaiaProductBusiness.getProSelfCode());
                sdWtpro.setProDsfName(gaiaProductBusiness.getProDepict());
                sdWtpro.setStatus(1);
                gaiaSdWtproMapper.insert(sdWtpro);
            }
        }
        // 第三方 gys-operation
        multipleStoreProductSyncStoreList.add(gaiaProductBusiness.getProSite());
        multipleStoreProductSyncProIdList.add(gaiaProductBusiness.getProSelfCode());

        // 零售价联动
        GaiaSdProductPriceKey gaiaSdProductPriceKey = new GaiaSdProductPriceKey();
        // 加盟商
        gaiaSdProductPriceKey.setClient(gaiaProductBusiness.getClient());
        // 门店编码
        gaiaSdProductPriceKey.setGsppBrId(gaiaProductBusiness.getProSite());
        // 商品编码
        gaiaSdProductPriceKey.setGsppProId(gaiaProductBusiness.getProSelfCode());
        GaiaSdProductPrice gaiaSdProductPrice = gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPriceKey);
        if (gaiaSdProductPrice == null) {
            gaiaSdProductPrice = new GaiaSdProductPrice();
            // 加盟商
            gaiaSdProductPrice.setClient(gaiaProductBusiness.getClient());
            // 门店编码
            gaiaSdProductPrice.setGsppBrId(gaiaProductBusiness.getProSite());
            // 商品编码
            gaiaSdProductPrice.setGsppProId(gaiaProductBusiness.getProSelfCode());
            // 零售价
            gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
            // 会员价
            gaiaSdProductPrice.setGsppPriceHy(gaiaProductBusiness.getProHyj());
            gaiaSdProductPriceMapper.insertSelective(gaiaSdProductPrice);
        } else {
            // 零售价
            gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
            // 会员价
            gaiaSdProductPrice.setGsppPriceHy(gaiaProductBusiness.getProHyj());
            gaiaSdProductPriceMapper.updateByPrimaryKeySelective(gaiaSdProductPrice);
        }
        // 首营日志
        GaiaProductGspinfoLog gaiaProductGspinfoLogOld = new GaiaProductGspinfoLog();
        BeanUtils.copyProperties(gaiaProductBusiness, gaiaProductGspinfoLogOld);
        //加盟商
        gaiaProductGspinfoLogOld.setClient(gaiaProductBusiness.getClient());
        //商品自编码
        gaiaProductGspinfoLogOld.setProSelfCode(gaiaProductBusiness.getProSelfCode());
        //地点
        gaiaProductGspinfoLogOld.setProSite(gaiaProductBusiness.getProSite());
        gaiaProductGspinfoLogOld.setProLine(1);
        gaiaProductGspinfoLogOld.setProMll(pro_mll);
        gaiaProductGspinfoLogMapper.insertSelective(gaiaProductGspinfoLogOld);

        GaiaProductGspinfoLog gaiaProductGspinfoLogNew = new GaiaProductGspinfoLog();
        BeanUtils.copyProperties(gaiaProductBusiness, gaiaProductGspinfoLogNew);
        gaiaProductGspinfoLogNew.setProLine(2);
        gaiaProductGspinfoLogNew.setProMll(pro_mll);
        gaiaProductGspinfoLogMapper.insertSelective(gaiaProductGspinfoLogNew);
    }

    /**
     * 参数校验
     */
    private void checkParams(List<SaveGaiaProductBusiness> list, String client) {
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("商品列表不能为空");
        }
        list.forEach(item -> {
            if (StringUtils.isBlank(item.getProSelfCode())) {
                throw new CustomResultException("商品自编码不能为空");
            }

            if (StringUtils.isBlank(item.getProCommonname())) {
                throw new CustomResultException("商品通用名不能为空");
            }
            if (StringUtils.isBlank(item.getProClass())) {
                throw new CustomResultException("商品分类不能为空");
            }
            if (StringUtils.isBlank(item.getProUnit())) {
                throw new CustomResultException("单位不能为空");
            }
            if (StringUtils.isBlank(item.getProSpecs())) {
                throw new CustomResultException("规格不能为空");
            }
//            if (StringUtils.isBlank(item.getProForm())) {
//                throw new CustomResultException("计量单位不能为空");
//            }
            if (StringUtils.isBlank(item.getProFactoryName())) {
                throw new CustomResultException("生产企业不能为空");
            }
            if (StringUtils.isBlank(item.getProInputTax())) {
                throw new CustomResultException("进项税率不能为空");
            }
//            if (StringUtils.isBlank(item.getProOutputTax())) {
//                throw new CustomResultException("销项税率不能为空");
//            }
            if (StringUtils.isBlank(item.getProStorageCondition())) {
                throw new CustomResultException("贮存条件不能为空 1-常温，2-阴凉，3-冷藏");
            }
//            if (StringUtils.isBlank(item.getProStorageArea())) {
//                throw new CustomResultException("商品仓储分区不能为空");
//            }
//            if (StringUtils.isBlank(item.getProElectronicCode())) {
//                throw new CustomResultException("是否启用电子监管码不能为空");
//            }
//            if (StringUtils.isBlank(item.getProFixBin())) {
//                throw new CustomResultException("固定货位不能为空");
//            }
        });
    }

    /**
     * 商品列表
     */
    @Override
    public PageInfo<GaiaProductBusinessDTO> getProList(GetProListRequestDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        dto.setClient(user.getClient());
        if (StringUtils.isBlank(dto.getCompadmId()) && StringUtils.isBlank(dto.getProSite()) && CollectionUtils.isEmpty(dto.getProSites())) {
            throw new CustomResultException("连锁总部和门店必须选择一个");
        }
        dto.setUserId(user.getUserId());
        List<String> stoList = new ArrayList<>();
        dto.getParamList().forEach(item -> item.setColumn("b." + item.getColumn()));
        if (!StringUtils.isBlank(dto.getProSite())) {
            stoList.add(dto.getProSite());
        } else if (!CollectionUtils.isEmpty(dto.getProSites())) {
            stoList.addAll(dto.getProSites());
        } else {
            stoList = gaiaProductBasicMapper.getStoListByComp(client, dto.getCompadmId());
        }
        if (!CollectionUtils.isEmpty(stoList)) {
            if (dto.getProClassCode() != null && dto.getProClassCode().length > 0) {
                List<String> resultList = new ArrayList<>();
                for (String[] proClass : dto.getProClassCode()) {
                    if (proClass != null && proClass.length > 0) {
                        resultList.addAll(Arrays.asList(proClass));
                    }
                }
                if (!CollectionUtils.isEmpty(resultList)) {
                    dto.setProClassList(resultList.stream().distinct().collect(Collectors.toList()));
                }
            }
            PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
            List<GaiaProductBusinessDTO> list = gaiaProductBusinessMapper.getProList(dto, stoList);
            // 拼接 唯一主键（前端使用）
            list.forEach(item -> item.setId(item.getClient() + item.getProSite() + item.getProSelfCode()));
            // 分页信息
            PageInfo<GaiaProductBusinessDTO> pageInfo = new PageInfo<>(list);
            return pageInfo;
        }
        return new PageInfo<GaiaProductBusinessDTO>();
    }

    /**
     * 商品数据导出
     */
    @Override
    public Result exportProList(GetProListRequestDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        dto.setClient(user.getClient());
        if (StringUtils.isBlank(dto.getCompadmId()) && StringUtils.isBlank(dto.getProSite()) && CollectionUtils.isEmpty(dto.getProSites())) {
            throw new CustomResultException("连锁总部和门店必须选择一个");
        }
        List<String> stoList = new ArrayList<>();
        if (!StringUtils.isBlank(dto.getProSite())) {
            stoList.add(dto.getProSite());
        } else if (!CollectionUtils.isEmpty(dto.getProSites())) {
            stoList.addAll(dto.getProSites());
        } else {
            stoList = gaiaProductBasicMapper.getStoListByComp(client, dto.getCompadmId());
        }
        List<GaiaProductBusinessDTO> list = new ArrayList<>();
        if (!CollectionUtils.isEmpty(stoList)) {
            dto.setUserId(user.getUserId());
            dto.getParamList().forEach(item -> item.setColumn("b." + item.getColumn()));

            if (dto.getProClassCode() != null && dto.getProClassCode().length > 0) {
                List<String> resultList = new ArrayList<>();
                for (String[] proClass : dto.getProClassCode()) {
                    if (proClass != null && proClass.length > 0) {
                        resultList.addAll(Arrays.asList(proClass));
                    }
                }
                if (!CollectionUtils.isEmpty(resultList)) {
                    dto.setProClassList(resultList.stream().distinct().collect(Collectors.toList()));
                }
            }

            list = gaiaProductBusinessMapper.getProList(dto, stoList);
        }
        // 商品类别
        Map<String, String> proClassMap = gaiaProductBusinessMapper.getProductClassList().stream().filter(Objects::nonNull)
                .collect(Collectors.toMap(GaiaProductClass::getProClassCode, GaiaProductClass::getProClassName));
        // 导出数据表头
        String[] headList = {
                "机构",
                "商品编码",
                "通用名称",
                "是否发送购药提醒",
                "服药剂量单位",
                "经营管理类别",
                "档案号",
                "备注",
                "最小剂量（mg/ml）",
                "总剂量（mg/ml）",
                "规格",
                "国际条形码",
                "厂家",
                "产地",
                "单位",
                "剂型",
                "细分剂型",
                "批准文号",
                "采购价",
                "销售价",
                "参考毛利",
                "大分类",
                "中分类",
                "小分类",
                "商品分类",
                "商品名称",
                "商品状态",
                "中包装",
                "处方分类",
                "储存条件",
                "商品定位",
                "销售级别",
                "进项税率",
                "商品编码",
                "商品描述",
                "助记码",
                "国际条形码2",
                "批准文号分类",
                "批准文号批准日期",
                "批准文号失效日期",
                "成分分类",
//                "生产企业",
                "商标",
                "品牌标识名",
                "品牌区分",
                "保质期",
                "保质期单位",
                "上市许可持有人",
                "销项税率",
                "药品本位码",
                "税务分类编码",
                "管制特殊分类",
                "生产类别",
                "商品仓储分区",
                "长",
                "宽",
                "高",
                "大包装量",
                "启用电子监管码",
                "生产经营许可证号",
                "最大销售量",
                "说明书代码",
                "说明书内容",
                "国家医保品种",
                "国家医保品种编码",
                "生产国家",
                "可服用天数",
                "用法用量",
                "禁忌说明",
                "禁止销售",
                "禁止采购",
                "禁止配送",
                "禁止退厂",
                "禁止退仓",
                "禁止调剂",
                "禁止批发",
                "禁止请货",
                "是否拆零",
                "拆零单位",
                "拆零比例",
                "采购单位",
                "采购员",
                "销售单位",
                "销售比例",
                "最小订货量",
                "是否医保",
                "限购数量",
                "每单最大配货量",
                "每月最大配货量",
                "按中包装量倍数配货",
                "是否重点养护",
                "固定货位",
                "国家医保目录编号",
                "国家医保目录名称",
                "国家医保医保目录剂型",
                "商品自分类",
                "自定义字段1",
                "自定义字段2",
                "自定义字段3",
                "自定义字段4",
                "自定义字段5",
                "自定义字段6",
                "自定义字段7",
                "自定义字段8",
                "自定义字段9",
                "自定义字段10",
                "医保刷卡数量",
                "医保编码",
                "不打折商品",
                "批文注册证号",
                "质量标准",
                "特殊属性",
                "医保类型",
                "末次进价",
                "末次供应商",
                "是否批发",
//                "经营类别",
                "医疗器械注册人",
                "医疗器械备案人",
                "备案人/注册人住所",
                "医疗器械受托生产企业",
                "医疗器械受托生产企业许可证号",
                "医疗器械备案凭证号",
                "会员价",
                "国批价",
                "国零价",
                "预设售价1",
                "预设售价2",
                "预设售价3",
                "改价最低价格",
                "是否上传药师帮",
                "销售计算排除",
        };
        // 自定义字段名
        GaiaProductZdy productZdy = getProductZdy(client, null);
        if (!ObjectUtils.isEmpty(productZdy)) {
            for (int i = 0; i < headList.length; i++) {
                if (headList[i].equals("自定义字段1") && !StringUtils.isBlank(productZdy.getProZdy1Name())) {
                    headList[i] = productZdy.getProZdy1Name();
                } else if (headList[i].equals("自定义字段2") && !StringUtils.isBlank(productZdy.getProZdy2Name())) {
                    headList[i] = productZdy.getProZdy2Name();
                } else if (headList[i].equals("自定义字段3") && !StringUtils.isBlank(productZdy.getProZdy3Name())) {
                    headList[i] = productZdy.getProZdy3Name();
                } else if (headList[i].equals("自定义字段4") && !StringUtils.isBlank(productZdy.getProZdy4Name())) {
                    headList[i] = productZdy.getProZdy4Name();
                } else if (headList[i].equals("自定义字段5") && !StringUtils.isBlank(productZdy.getProZdy5Name())) {
                    headList[i] = productZdy.getProZdy5Name();
                } else if (headList[i].equals("自定义字段6") && !StringUtils.isBlank(productZdy.getProZdy6Name())) {
                    headList[i] = productZdy.getProZdy6Name();
                } else if (headList[i].equals("自定义字段7") && !StringUtils.isBlank(productZdy.getProZdy7Name())) {
                    headList[i] = productZdy.getProZdy7Name();
                } else if (headList[i].equals("自定义字段8") && !StringUtils.isBlank(productZdy.getProZdy8Name())) {
                    headList[i] = productZdy.getProZdy8Name();
                } else if (headList[i].equals("自定义字段9") && !StringUtils.isBlank(productZdy.getProZdy9Name())) {
                    headList[i] = productZdy.getProZdy9Name();
                } else if (headList[i].equals("自定义字段10") && !StringUtils.isBlank(productZdy.getProZdy10Name())) {
                    headList[i] = productZdy.getProZdy10Name();
                }

            }
        }
        // 商品自分类翻译
        Map<String, String> sclassMap = gaiaProductBusinessMapper.getProductSclassList(client).stream().filter(Objects::nonNull)
                .collect(Collectors.toMap(GaiaProductSclass::getProSclassCode, GaiaProductSclass::getProSclassName, (v1, v2) -> v1));

        // 进/销项税率
        Map<String, String> taxCodeMap = gaiaProductBusinessMapper.getTaxCodeList().stream().filter(Objects::nonNull)
                .collect(Collectors.toMap(GaiaTaxCode::getTaxCode, GaiaTaxCode::getTaxCodeName));

        // 经营类别
        List<JyfwooData> jyfwooDataList = getJyfwooDataList(null);
        try {
            List<List<String>> dataList = new ArrayList<>();
            for (GaiaProductBusinessDTO redto : list) {
                // 打印行
                List<String> item = new ArrayList<>();
                //  机构",
                item.add(redto.getProSite());
                // 商品编码",
                item.add(redto.getProSelfCode());
                // 通用名称",
                item.add(redto.getProCommonname());
                // 是否发送购药提醒",
                initDictionaryStaticData(item, redto.getProIfSms(), CommonEnum.DictionaryStaticData.NO_YES);
                // 服药剂量单位",
                item.add(redto.getProDoseUnit());
                // 经营管理类别",
                item.add(redto.getProJygllb());
                // 档案号",
                item.add(redto.getProDah());
                // 备注",
                item.add(redto.getProBeizhu());
                // 最小剂量（mg/ml）",
                item.add(redto.getProMindose());
                // 总剂量（mg/ml）",
                item.add(redto.getProTotaldose());
                // 规格",
                item.add(redto.getProSpecs());
                // 国际条形码",
                item.add(redto.getProBarcode());
                // 厂家",
                item.add(redto.getProFactoryName());
                // 产地",
                item.add(redto.getProPlace());
                // 单位",
                item.add(redto.getProUnit());
                // 剂型",
                item.add(redto.getProForm());
                // 细分剂型",
                item.add(redto.getProPartform());
                // 批准文号",
                item.add(redto.getProRegisterNo());
                // 采购价",
                item.add(redto.getProCgj() == null ? "" : redto.getProCgj().toString());
                // 销售价",
                item.add(redto.getProLsj() == null ? "" : redto.getProLsj().toString());
                // 参考毛利",
                item.add(redto.getProMll() == null ? "" : redto.getProMll().toString());
                // "大分类
                item.add(redto.getProBigClassName());
                // "中分类
                item.add(redto.getProMidClassName());
                // "小分类
                item.add(redto.getProMinClassName());
                // 商品分类
                item.add(redto.getProClassName());
                // 商品名称",
                item.add(redto.getProName());
                // 商品状态",
                initDictionaryStaticData(item, redto.getProStatus(), CommonEnum.DictionaryStaticData.PRO_STATUS);
                // 中包装",
                item.add(redto.getProMidPackage());
                // 处方分类", PRO_PRESCLASS
                initDictionaryStaticData(item, redto.getProPresclass(), CommonEnum.DictionaryStaticData.PRO_PRESCLASS);
                // 储存条件",
                initDictionaryStaticData(item, redto.getProStorageCondition(), CommonEnum.DictionaryStaticData.PRO_STORAGE_CONDITION);
                // 商品定位",
                initDictionaryStaticData(item, redto.getProPosition(), CommonEnum.DictionaryStaticData.PRO_POSITION);
                // 销售级别",
                item.add(redto.getProSlaeClass());
                // 进项税率",
                item.add(redto.getProInputTax() == null ? null : taxCodeMap.get(redto.getProInputTax()));
                // 商品编码",
                item.add(redto.getProSelfCode());
                // 商品描述",
                item.add(redto.getProDepict());
                // 助记码",
                item.add(redto.getProPym());
                // 国际条形码2",
                item.add(redto.getProBarcode2());
                // 批准文号分类",
                initDictionaryStaticData(item, redto.getProRegisterClass(), CommonEnum.DictionaryStaticData.PRO_REGISTER_CLASS);
                // 批准文号批准日期",
                item.add(formatDate(redto.getProRegisterDate()));
                // 批准文号失效日期",
                item.add(formatDate(redto.getProRegisterExdate()));
                // 成分分类",
                item.add(redto.getProCompclassName());
//                // 生产企业",
//                item.add(redto.getProFactoryName());
                // 商标",
                item.add(redto.getProMark());
                // 品牌标识名",
                item.add(redto.getProBrand());
                // 品牌区分",
                initDictionaryStaticData(item, redto.getProBrandClass(), CommonEnum.DictionaryStaticData.PRO_BRAND_CLASS);
                // 保质期",
                item.add(redto.getProLife());
                // 保质期单位",
                initDictionaryStaticData(item, redto.getProLifeUnit(), CommonEnum.DictionaryStaticData.PRO_LIFE_UNIT);
                // 上市许可持有人",
                item.add(redto.getProHolder());
                // 销项税率",
                item.add(redto.getProOutputTax() == null ? null : taxCodeMap.get(redto.getProOutputTax()));
                // 药品本位码",
                item.add(redto.getProBasicCode());
                // 税务分类编码",
                item.add(redto.getProTaxClass());
                // 管制特殊分类",
                initDictionaryStaticData(item, redto.getProControlClass(), CommonEnum.DictionaryStaticData.PRO_CONTROL_CLASS);
                // 生产类别",
                initDictionaryStaticData(item, redto.getProProduceClass(), CommonEnum.DictionaryStaticData.PRO_PRODUCE_CLASS);
                // 商品仓储分区",
                initDictionaryStaticData(item, redto.getProStorageArea(), CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA);
                // 长(MM)
                item.add(redto.getProLong());
                // 宽(MM)
                item.add(redto.getProWide());
                // 高(MM)
                item.add(redto.getProHigh());
                // 大包装量",
                item.add(redto.getProBigPackage());
                // 启用电子监管码",
                initDictionaryStaticData(item, redto.getProElectronicCode(), CommonEnum.DictionaryStaticData.NO_YES);
                // 生产经营许可证号",
                item.add(redto.getProQsCode());
                // 最大销售量",
                item.add(redto.getProMaxSales());
                // 说明书代码",
                item.add(redto.getProInstructionCode());
                // 说明书内容",
                item.add(redto.getProInstruction());
                // 国家医保品种",
                item.add(redto.getProMedProdct());
                // 国家医保品种编码",
                item.add(redto.getProMedProdctcode());
                // 生产国家",
                item.add(redto.getProCountry());
                // 可服用天数",
                item.add(redto.getProTakeDays());
                // 用法用量",
                item.add(redto.getProUsage());
                // 禁忌说明",
                item.add(redto.getProContraindication());
                // 禁止销售",
                initDictionaryStaticData(item, redto.getProNoRetail(), CommonEnum.DictionaryStaticData.NO_YES);
                // 禁止采购",
                initDictionaryStaticData(item, redto.getProNoPurchase(), CommonEnum.DictionaryStaticData.NO_YES);
                // 禁止配送",
                initDictionaryStaticData(item, redto.getProNoDistributed(), CommonEnum.DictionaryStaticData.NO_YES);
                // 禁止退厂",
                initDictionaryStaticData(item, redto.getProNoSupplier(), CommonEnum.DictionaryStaticData.NO_YES);
                // 禁止退仓",
                initDictionaryStaticData(item, redto.getProNoDc(), CommonEnum.DictionaryStaticData.NO_YES);
                // 禁止调剂",
                initDictionaryStaticData(item, redto.getProNoAdjust(), CommonEnum.DictionaryStaticData.NO_YES);
                // 禁止批发",
                initDictionaryStaticData(item, redto.getProNoSale(), CommonEnum.DictionaryStaticData.NO_YES);
                // 禁止请货",
                initDictionaryStaticData(item, redto.getProNoApply(), CommonEnum.DictionaryStaticData.NO_YES);
                // 是否拆零
                initDictionaryStaticData(item, redto.getProIfpart(), CommonEnum.DictionaryStaticData.NO_YES);
                // 拆零单位",
                item.add(redto.getProPartUint());
                // 拆零比例",
                item.add(redto.getProPartRate());
                // 采购单位",
                item.add(redto.getProPurchaseUnit());
                // 采购员",
                item.add(redto.getProPurchaseRate());
                // 销售单位",
                item.add(redto.getProSaleUnit());
                // 销售比例",
                item.add(redto.getProSaleRate());
                // 最小订货量",
                item.add(redto.getProMinQty() == null ? null : redto.getProMinQty().toString());
                // 是否医保",
                initDictionaryStaticData(item, redto.getProIfMed(), CommonEnum.DictionaryStaticData.NO_YES);
                // 限购数量",
                item.add(redto.getProLimitQty() == null ? null : redto.getProLimitQty().toString());
                // 每单最大配货量",
                item.add(redto.getProMaxQty() == null ? null : redto.getProMaxQty().toString());
                // 每月最大配货量",
                item.add(redto.getProMaxQtyMouth() == null ? null : redto.getProMaxQtyMouth().toString());
                // 按中包装量倍数配货",
                initDictionaryStaticData(item, redto.getProPackageFlag(), CommonEnum.DictionaryStaticData.NO_YES);
                // 是否重点养护",
                initDictionaryStaticData(item, redto.getProKeyCare(), CommonEnum.DictionaryStaticData.PRO_KEY_CARE);
                // 固定货位",
                item.add(redto.getProFixBin());
                // 国家医保目录编号",
                item.add(redto.getProMedListnum());
                // 国家医保目录名称",
                item.add(redto.getProMedListname());
                // 国家医保医保目录剂型",
                item.add(redto.getProMedListform());
                // 商品自分类",
                item.add(redto.getProSclass() == null ? null : sclassMap.get(redto.getProSclass()));
                // 自定义字段1",
                item.add(redto.getProZdy1());
                // 自定义字段2",
                item.add(redto.getProZdy2());
                // 自定义字段3",
                item.add(redto.getProZdy3());
                // 自定义字段4",
                item.add(redto.getProZdy4());
                // 自定义字段5",
                item.add(redto.getProZdy5());
                // 自定义字段6
                item.add(redto.getProZdy6());
                // 自定义字段7
                item.add(redto.getProZdy7());
                // 自定义字段8
                item.add(redto.getProZdy8());
                // 自定义字段9
                item.add(redto.getProZdy9());
                // 自定义字段10
                item.add(redto.getProZdy10());
                // 医保刷卡数量
                item.add(redto.getProMedQty());
                // 医保编码",
                item.add(redto.getProMedId());
                // 不打折商品",
                item.add(redto.getProBdz());
                // 批文注册证号",
                item.add(redto.getProPwzcz());
                // 质量标准",
                item.add(redto.getProZlbz());
                // 特殊属性",
                initDictionaryStaticData(item, redto.getProTssx(), CommonEnum.DictionaryStaticData.PRO_TSSX);
                // 医保类型",
                initDictionaryStaticData(item, redto.getProTssx(), CommonEnum.DictionaryStaticData.PRO_TSSX);
                // 末次进价",
                item.add(redto.getLastPoPrice() == null ? "" : redto.getLastPoPrice().toString());
                // 末次供应商",
                if (StringUtils.isNotBlank(redto.getLastSupCode()) && StringUtils.isNotBlank(redto.getLastSupName())) {
                    item.add(com.gov.purchase.utils.StringUtils.parse("{}-{}", redto.getLastSupCode(), redto.getLastSupName()));
                } else {
                    item.add("");
                }
                // 是否批发",
                initDictionaryStaticData(item, redto.getProIfWholesale(), CommonEnum.DictionaryStaticData.NO_YES);
//                // 经营类别",
//                item.add(redto.getProJylb());
                // 医疗器械注册人",
                item.add(redto.getProYlqxZcr());
                // 医疗器械备案人",
                item.add(redto.getProYlqxBar());
                // 备案人/注册人住所
                item.add(redto.getProBarzcrzs());
                // 医疗器械受托生产企业",
                item.add(redto.getProYlqxScqy());
                // 医疗器械受托生产企业许可证号",
                item.add(redto.getProYlqxScqyxkz());
                // 医疗器械备案凭证号",
                item.add(redto.getProYlqxBapzh());
                // 会员价",
                item.add(redto.getProHyj() == null ? "" : redto.getProHyj().toString());
                // 国批价",
                item.add(redto.getProGpj() == null ? "" : redto.getProGpj().toString());
                // 国零价",
                item.add(redto.getProGlj() == null ? "" : redto.getProGlj().toString());
                // 预设售价1",
                item.add(redto.getProYsj1() == null ? "" : redto.getProYsj1().toString());
                // 预设售价2",
                item.add(redto.getProYsj2() == null ? "" : redto.getProYsj2().toString());
                // 预设售价3",
                item.add(redto.getProYsj3() == null ? "" : redto.getProYsj3().toString());
                // 改价最低价格
                item.add(redto.getProLowerPrice() == null ? "" : redto.getProLowerPrice().toString());
                // 是否上传药师帮
                item.add(redto.getProYsbCode());
                // 销售计算排除
                item.add(redto.getProQueryOut());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("商品主数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 自定义字段名
     */
    @Override
    public GaiaProductZdy getProductZdy(String client, String proSite) {
        GaiaProductZdy gaiaProductZdy = gaiaProductBusinessMapper.getProductZdy(client, proSite);
        if (gaiaProductZdy == null) {
            gaiaProductZdy = new GaiaProductZdy();
        }
        gaiaProductZdy.setProZdy1Name(StringUtils.isBlank(gaiaProductZdy.getProZdy1Name()) ? "自定义字段1" : gaiaProductZdy.getProZdy1Name());
        gaiaProductZdy.setProZdy2Name(StringUtils.isBlank(gaiaProductZdy.getProZdy2Name()) ? "自定义字段2" : gaiaProductZdy.getProZdy2Name());
        gaiaProductZdy.setProZdy3Name(StringUtils.isBlank(gaiaProductZdy.getProZdy3Name()) ? "自定义字段3" : gaiaProductZdy.getProZdy3Name());
        gaiaProductZdy.setProZdy4Name(StringUtils.isBlank(gaiaProductZdy.getProZdy4Name()) ? "自定义字段4" : gaiaProductZdy.getProZdy4Name());
        gaiaProductZdy.setProZdy5Name(StringUtils.isBlank(gaiaProductZdy.getProZdy5Name()) ? "自定义字段5" : gaiaProductZdy.getProZdy5Name());
        gaiaProductZdy.setProZdy6Name(StringUtils.isBlank(gaiaProductZdy.getProZdy6Name()) ? "自定义字段6" : gaiaProductZdy.getProZdy6Name());
        gaiaProductZdy.setProZdy7Name(StringUtils.isBlank(gaiaProductZdy.getProZdy7Name()) ? "自定义字段7" : gaiaProductZdy.getProZdy7Name());
        gaiaProductZdy.setProZdy8Name(StringUtils.isBlank(gaiaProductZdy.getProZdy8Name()) ? "自定义字段8" : gaiaProductZdy.getProZdy8Name());
        gaiaProductZdy.setProZdy9Name(StringUtils.isBlank(gaiaProductZdy.getProZdy9Name()) ? "自定义字段9" : gaiaProductZdy.getProZdy9Name());
        gaiaProductZdy.setProZdy10Name(StringUtils.isBlank(gaiaProductZdy.getProZdy10Name()) ? "自定义字段10" : gaiaProductZdy.getProZdy10Name());
        return gaiaProductZdy;
    }

    /**
     * 该商品在首营表中是否存在
     */
    @Override
    public String getProductGspinfoExit(GaiaProductGspinfoKeyDTO dto) {
        return gaiaProductGspinfoMapper.getProductGspinfoExit(dto);
    }

    /**
     * 补充首营
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void replenishProductGsp(GaiaProductGspinfoKeyDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        GaiaProductBusiness productBusiness = gaiaProductBusinessMapper.getProductBusiness(dto.getClient(), dto.getProSite(), dto.getProSelfCode());
        if (ObjectUtils.isEmpty(productBusiness)) {
            throw new CustomResultException("商品不存在");
        }
        GaiaProductGspinfo gspinfo = new GaiaProductGspinfo();
        BeanUtils.copyProperties(productBusiness, gspinfo);
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNO = System.currentTimeMillis() + com.gov.purchase.utils.StringUtils.leftPad(String.valueOf(i), 6, "0");
        gspinfo.setProFlowNo(flowNO);
        gspinfo.setProGspDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        //首营状态
        gspinfo.setProGspStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
        gspinfo.setProSupplyName("");
        gspinfo.setProIfGmp(com.gov.purchase.utils.StringUtils.isBlank(gspinfo.getProIfGmp()) ? "0" : gspinfo.getProIfGmp());
        gspinfo.setProMidPackage(com.gov.purchase.utils.StringUtils.isBlank(gspinfo.getProMidPackage()) ? "1" : gspinfo.getProMidPackage());
        if (org.apache.commons.lang3.StringUtils.isBlank(gspinfo.getProDepict())) {
            String proDepict = gspinfo.getProCommonname();
            if (StringUtils.isNotBlank(gspinfo.getProCommonname()) && !gspinfo.getProCommonname().equals(gspinfo.getProName())) {
                String proNameTemp = StringUtils.isBlank(gspinfo.getProName()) ? "" : "(" + gspinfo.getProName() + ")";
                proDepict = gspinfo.getProCommonname() + proNameTemp;
            }
            gspinfo.setProDepict(proDepict);
        }
        if (org.apache.commons.lang3.StringUtils.isBlank(gspinfo.getProPym())) {
            gspinfo.setProPym(com.gov.purchase.utils.StringUtils.ToFirstChar(gspinfo.getProDepict()));
        }
        // 参考零售价
        gspinfo.setProLsj(dto.getProLsj());
        HashMap<String, String> map = gaiaProductBusinessMapper.getSwitchForPH(gspinfo.getClient(), "WF_APPROVE_USER");
        String para1 = ObjectUtils.isEmpty(map) ? null : map.get("param1").toString();
        String para2 = ObjectUtils.isEmpty(map) ? null : map.get("param2").toString();
        if (StringUtils.isNotBlank(para1) && StringUtils.isNotBlank(para2)) {
            if (para1.equals(gspinfo.getProSite())) {
                gspinfo.setProCreateBy(para2);
            } else {
                gspinfo.setProCreateBy(user.getUserId());
            }
        } else {
            gspinfo.setProCreateBy(user.getUserId());
        }
        // 插入数据
        gaiaProductGspinfoMapper.insert(gspinfo);
        // 提交申批
        FeignResult result = feignService.createReplenishProductGspWorkflow(gspinfo, flowNO);
        if (result.getCode() != 0) {
            throw new CustomResultException(result.getMessage());
        }

        // 首营日志
        GaiaProductGspinfoLog gaiaProductGspinfoLogOld = new GaiaProductGspinfoLog();
        BeanUtils.copyProperties(gspinfo, gaiaProductGspinfoLogOld);
        //加盟商
        gaiaProductGspinfoLogOld.setClient(gspinfo.getClient());
        //商品自编码
        gaiaProductGspinfoLogOld.setProSelfCode(gspinfo.getProSelfCode());
        //地点
        gaiaProductGspinfoLogOld.setProSite(gspinfo.getProSite());
        gaiaProductGspinfoLogOld.setProLine(1);
        gaiaProductGspinfoLogMapper.insertSelective(gaiaProductGspinfoLogOld);

        GaiaProductGspinfoLog gaiaProductGspinfoLogNew = new GaiaProductGspinfoLog();
        BeanUtils.copyProperties(gspinfo, gaiaProductGspinfoLogNew);
        gaiaProductGspinfoLogNew.setProLine(2);
        gaiaProductGspinfoLogMapper.insertSelective(gaiaProductGspinfoLogNew);
    }

    /**
     * 商品首营_获取商品自编码最大值
     */
    @Override
    public String getMaxProSelfCode(GetMaxProSelfCodeDTO dto) {
        String client = feignService.getLoginInfo().getClient();
        String proBeginField = gaiaProductBusinessMapper.getProductNumber(client);
        if (StringUtils.isNotBlank(proBeginField)) {
            String number = gaiaProductBusinessMapper.getProductNumberDigit(client);
            if (!StringUtils.isBlank(dto.getLastRowProSeflCode())) {
                String code = ListUtils.getMaxCode(dto.getLastRowProSeflCode());
                if (code != null) {
                    if (dto.getBeginCode().length() < Integer.parseInt(number)) {
                        return dto.getBeginCode() + code.substring(dto.getBeginCode().length());
                    } else {
                        return dto.getBeginCode().substring(0, Integer.parseInt(number)) + code.substring(Integer.parseInt(number));
                    }
                } else {
                    return code;
                }
            }
            // 自编码集合
            List<String> list = gaiaProductBusinessMapper.getSelfCodeListForClass(client, null, null, dto.getBeginCode());
            if (list == null || CollectionUtils.isEmpty(list)) {
                return "";
            }
            String maxCode = ListUtils.getMaxCode(list);
            String selfCode = ListUtils.getMaxCode(maxCode);
            return selfCode;
        }
        if (!StringUtils.isBlank(dto.getCurrentRowProSeflCode())) {
            // 自编码集合
            Integer cnt = gaiaProductBusinessMapper.getPorSelfCodeCnt(client, dto.getCurrentRowProSeflCode());
            if (cnt.intValue() > 0) {
                if (!StringUtils.isBlank(dto.getLastRowProSeflCode())) {
                    String selfCode = ListUtils.getMaxCode(dto.getLastRowProSeflCode());
                    return selfCode;
                } else {
                    String selfCode = ListUtils.getMaxCode(dto.getCurrentRowProSeflCode());
                    return selfCode;
                }
            } else {
                return dto.getCurrentRowProSeflCode();
            }
        } else if (!StringUtils.isBlank(dto.getLastRowProSeflCode())) {
            return ListUtils.getMaxCode(dto.getLastRowProSeflCode());
        } else {
            // 自编码集合
            List<String> list = gaiaProductBusinessMapper.getSelfCodeList(client, null, null);
            if (CollectionUtils.isEmpty(list)) {
                return "";
            }
            String maxCode = ListUtils.getMaxCode(list);
            String selfCode = ListUtils.getMaxCode(maxCode);
            return selfCode;
        }
//        List<String> seflCodelist = gaiaProductBusinessMapper.getSelfCodeList2(client);
//        if (CollectionUtils.isEmpty(seflCodelist)) {
//            return null;
//        }
//        String maxCode = ListUtils.getMaxCode(seflCodelist);
//        return ListUtils.getMaxCode(maxCode);
    }

    @Override
    public String getProductNumber() {
        String client = feignService.getLoginInfo().getClient();
        return gaiaProductBusinessMapper.getProductNumber(client);
    }

    /**
     * 经营类别下拉框
     *
     * @param jyfwName
     * @return
     */
    @Override
    public List<JyfwooData> getJyfwooDataList(String jyfwName) {
        return gaiaProductBusinessMapper.getJyfwooDataList(jyfwName);
    }

    /**
     * 商品、供应商、客户修改是否发起工作流开关
     */
    @Override
    public Integer getCountNeedWorkFlowDC() {
        String client = feignService.getLoginInfo().getClient();
        return gaiaProductBusinessMapper.getCountNeedWorkFlowDC(client);
    }

    /**
     * 全局GSP开关
     */
    @Override
    public List<GaiaGlobalGspConfigure> getGlobalGspConfigureList(String gggcSite, String gggcCode) {
        String client = feignService.getLoginInfo().getClient();
        List<GaiaGlobalGspConfigure> currentClientConfigureList = gaiaGlobalGspConfigureMapper.getCurrentClientConfigureList(client, gggcSite, gggcCode);

        // 如果零售价没有配置，默认返回“1”
        if (CommonEnum.GaiaGlobalGspConfigure.GSP_PRO_LSJ.getCode().equals(gggcCode) && CollectionUtils.isEmpty(currentClientConfigureList)) {
            GaiaGlobalGspConfigure configure = new GaiaGlobalGspConfigure();
            configure.setClient(client);
            configure.setGggcCode(CommonEnum.GaiaGlobalGspConfigure.GSP_PRO_LSJ.getCode());
            configure.setGggcValue(CommonEnum.NoYesStatus.YES.getCode());
            currentClientConfigureList.add(configure);
        }
        return currentClientConfigureList;
    }

    /**
     * 指定加盟商_商品排除配置表默认值
     */
    @Override
    public void productPositionExcludeDefaultValue(String client) {
        if (StringUtils.isBlank(client)) {
            throw new CustomResultException("加盟商不能为空");
        }
        int count = gaiaProductBusinessMapper.getProductPositionByClient(client);
        if (count > 0) {
            return;
        }
        List<ProductPositionExcludeDTO> list = Arrays.asList(
                new ProductPositionExcludeDTO(client, "A20", "301", "Z"),
                new ProductPositionExcludeDTO(client, "A21", "302", "Z"),
                new ProductPositionExcludeDTO(client, "A22", "8", "Z"),
                new ProductPositionExcludeDTO(client, "A23", "", "Z"),
                new ProductPositionExcludeDTO(client, "A24", "", "Z")
        );
        gaiaProductBusinessMapper.productPositionExcludeDefaultValue(list);
    }

    @Override
    public boolean getSwitchForPH(String client) {
        HashMap<String, String> map = gaiaProductBusinessMapper.getSwitchForPH(client, "DISTRIBUTION_PLAN");
        String flag = ObjectUtils.isEmpty(map) ? "0" : map.get("param1").toString();
        return "1".equals(flag);
    }

    /**
     * 首营必输验证开关
     *
     * @return
     */
    @Override
    public List<GaiaGlobalGspConfigure> getGspRequire(String gggcCode) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaGlobalGspConfigure> list = gaiaGlobalGspConfigureMapper.getGspRequire(user.getClient(), gggcCode);
        return list;
    }

    @Override
    public GetDefaultSiteResponseDTO getDefaultSite2() {

        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        GetDefaultSiteResponseDTO dto = new GetDefaultSiteResponseDTO();
        // 有权限的连锁
        List<GetDefaultSiteResponseDTO.GaiaCompadmDTO> compadmList = gaiaProductBasicMapper.getCompadmList2(client, userId);
        dto.setCompadmList(compadmList);

        // 有权限的门店
        List<GaiaStoreData> authStoList = gaiaProductBasicMapper.getAuthStoList(client, userId);
        dto.setStoreDataList(authStoList);

        // 连锁下的门店
        if (!CollectionUtils.isEmpty(compadmList)) {
            List<String> compIdList = compadmList.stream().map(GetDefaultSiteResponseDTO.GaiaCompadmDTO::getCompadmId).collect(Collectors.toList());
            List<GaiaStoreData> compStoList = gaiaProductBasicMapper.getCompStoList(client, compIdList);
            // 当前连锁下的门店
            Map<String, List<GaiaStoreData>> stoChainHeadMap = compStoList.stream().collect(Collectors.groupingBy(GaiaStoreData::getStoChainHead));
            compadmList.forEach(item -> {
                List<GaiaStoreData> gaiaStoreData = stoChainHeadMap.get(item.getCompadmId());
                item.setStoreList(gaiaStoreData);
            });
        }

        /**
         * 供应商首营机构
         */
        List<GaiaStoreData> list = gaiaStoreDataMapper.gspInfoSiteList(client, userId);
        dto.setGspInfoSiteList(list);
        return dto;
    }

    /**
     * 查询数据静态字典数据处理
     */
    private void initDictionaryStaticData(List<String> list, String value, CommonEnum.DictionaryStaticData dictionaryStaticData) {
        if (com.gov.purchase.utils.StringUtils.isBlank(value)) {
            list.add("");
        } else {
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, value);
            if (dictionary != null) {
                list.add(dictionary.getLabel());
            } else {
                list.add("");
            }
        }
    }

    private String formatDate(String dateTemp) {
        if (StringUtils.isBlank(dateTemp)) {
            return "";
        }
        LinkedList<String> list = Stream.of(dateTemp.split("")).collect(Collectors.toCollection(LinkedList::new));
        List<String> listTemp = new ArrayList<>();
        for (int i = 0; i <= 9; i++) {
            if (i == 4 || i == 7) {
                listTemp.add("-");
            } else {
                listTemp.add(list.pollFirst());
            }
        }
        return String.join("", listTemp);
    }

    /**
     * 商品分类下拉菜单
     */
    @Override
    public List<GoodsClassResponseDTO> getClassList() {
        List<GoodsClassResponseDTO> classList = gaiaProductClassMapper.getClassList();
        // 第一级
        List<GoodsClassResponseDTO> bigClassList = classList.stream()
                .filter(distinctByKey(GoodsClassResponseDTO::getProBigClassCode))
                .map(item -> {
                    GoodsClassResponseDTO temp = new GoodsClassResponseDTO();
                    temp.setProClassCode(item.getProBigClassCode());
                    temp.setProClassName(item.getProBigClassName());
                    return temp;
                })
                .collect(Collectors.toList());

        // 第二级
        bigClassList.stream().forEach(item -> {
            List<GoodsClassResponseDTO> midClassList = classList.stream()
                    .filter(itemClass -> itemClass.getProBigClassCode().equals(item.getProClassCode()))
                    .filter(distinctByKey(GoodsClassResponseDTO::getProMidClassCode))
                    .map(itemClass -> {
                        GoodsClassResponseDTO temp = new GoodsClassResponseDTO();
                        temp.setProClassCode(itemClass.getProMidClassCode());
                        temp.setProClassName(itemClass.getProMidClassName());
                        return temp;
                    })
                    .collect(Collectors.toList());
            item.setList(midClassList);

            // 第三级
            midClassList.stream().forEach(item1 -> {
                List<GoodsClassResponseDTO> collect = classList.stream()
                        .filter(item2 -> item2.getProMidClassCode().equals(item1.getProClassCode()))
                        .map(item2 -> {
                            GoodsClassResponseDTO temp = new GoodsClassResponseDTO();
                            temp.setProClassCode(item2.getProClassCode());
                            temp.setProClassName(item2.getProClassName());
                            return temp;
                        })
                        .collect(Collectors.toList());
                item1.setList(collect);
            });

        });
        return bigClassList;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    /**
     * 默认选中机构
     */
    @Override
    public GetDefaultSiteResponseDTO getDefaultSite() {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        GetDefaultSiteResponseDTO dto = new GetDefaultSiteResponseDTO();
        // 有权限的连锁
        List<GetDefaultSiteResponseDTO.GaiaCompadmDTO> compadmList = gaiaProductBasicMapper.getCompadmList(client, userId);
        dto.setCompadmList(compadmList);

        // 有权限的门店
        List<GaiaStoreData> authStoList = gaiaProductBasicMapper.getAuthStoList(client, userId);
        dto.setStoreDataList(authStoList);

        // 连锁下的门店
        if (!CollectionUtils.isEmpty(compadmList)) {
            List<String> compIdList = compadmList.stream().map(GetDefaultSiteResponseDTO.GaiaCompadmDTO::getCompadmId).collect(Collectors.toList());
            List<GaiaStoreData> compStoList = gaiaProductBasicMapper.getCompStoList(client, compIdList);
            // 当前连锁下的门店
            Map<String, List<GaiaStoreData>> stoChainHeadMap = compStoList.stream().collect(Collectors.groupingBy(GaiaStoreData::getStoChainHead));
            compadmList.forEach(item -> {
                List<GaiaStoreData> gaiaStoreData = stoChainHeadMap.get(item.getCompadmId());
                item.setStoreList(gaiaStoreData);
            });
        }

        /**
         * 供应商首营机构
         */
        List<GaiaStoreData> list = gaiaStoreDataMapper.gspInfoSiteList(client, userId);
        dto.setGspInfoSiteList(list);
        return dto;
    }

    /**
     * 供货单位
     */
    @Override
    public List<String> getSupplierList() {
        TokenUser user = feignService.getLoginInfo();
        List<String> supplierList = gaiaProductBasicMapper.getSupplierList(user.getClient());
        return supplierList;
    }

    /**
     * 商品查询及维护_商品列表保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> proEditList(List<GaiaProductBusiness> business, String remarks, Integer editNoWws) {
        String client = feignService.getLoginInfo().getClient();
        String userId = feignService.getLoginInfo().getUserId();
        this.proEditListCheck(business);

        List<GaiaDcData> dcListToUpdate = gaiaProductBasicMapper.getDcListToUpdate(client, userId);

        // 第三方 gys-operation
        List<String> multipleStoreProductSyncStoreList = new ArrayList<>();
        List<String> multipleStoreProductSyncProIdList = new ArrayList<>();
        List<GaiaBatchInfo> gaiaBatchInfoList = new ArrayList<>();
        // 工作流
        List<GaiaProductChange> changeList = new ArrayList<>();
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNO = System.currentTimeMillis() + com.gov.purchase.utils.StringUtils.leftPad(String.valueOf(i), 6, "0");

        for (GaiaProductBusiness productBusiness : business) {
            if (StringUtils.isBlank(productBusiness.getProClass()) || StringUtils.length(productBusiness.getProClass().trim()) < 5) {
                productBusiness.setProClass(StringUtils.isBlank(productBusiness.getProClass()) ? "" : productBusiness.getProClass());
                productBusiness.setProClass(StringUtils.rightPad(StringUtils.left(productBusiness.getProClass(), 1), 5, "9"));
            }
            // 设置商品描述和商品助记码
            setProDepictAndPym(productBusiness);
            // 更新list
            List<GaiaProductBusinessKey> productKeyList = new ArrayList<>();
            // 页面修改的这一条
            GaiaProductBusinessKey productKey = new GaiaProductBusinessKey();
            productKey.setClient(productBusiness.getClient());
            productKey.setProSite(productBusiness.getProSite());
            productKey.setProSelfCode(productBusiness.getProSelfCode());
            productKeyList.add(productKey);

            // 如果地点是DC
            GaiaDcData gaiaDcData = dcListToUpdate.stream().filter(Objects::nonNull).filter(item -> item.getDcCode().equals(productBusiness.getProSite())).findFirst().orElse(null);
            if (!ObjectUtils.isEmpty(gaiaDcData)) {
                // # 配送中心连锁总部下的门店 + "门店主数据"地点下的门店
                List<String> stoList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(client, productBusiness.getProSite());
                // 当基于批发修改商品主数据时，保存时，弹框提示“是否同步修改对应连锁的商品资料”，是则同步修改连锁（包括仓库和门店）的数据，否则不修改。
                if (editNoWws != null && editNoWws.intValue() == 1) {
                    List<String> noWwsSiteList = gaiaProductBusinessMapper.getNoWwsSiteList(client, gaiaDcData.getDcCode());
                    if (!CollectionUtils.isEmpty(noWwsSiteList)) {
                        stoList.addAll(noWwsSiteList);
                        stoList = stoList.stream().distinct().collect(Collectors.toList());
                    }
                }
                stoList.stream().filter(Objects::nonNull).distinct().forEach(stoCode -> {
                    GaiaProductBusinessKey proKey = new GaiaProductBusinessKey();
                    proKey.setClient(productBusiness.getClient());
                    proKey.setProSite(stoCode);
                    proKey.setProSelfCode(productBusiness.getProSelfCode());
                    productKeyList.add(proKey);
                    // 第三方 gys-operation
                    if (!CommonEnum.NoYesStatus.YES.getCode().equals(gaiaDcData.getDcXgsfwf())) {
                        multipleStoreProductSyncStoreList.add(stoCode);
                        multipleStoreProductSyncProIdList.add(productBusiness.getProSelfCode());
                    }
                });

                // A.工作流
                if (CommonEnum.NoYesStatus.YES.getCode().equals(gaiaDcData.getDcXgsfwf())) {
                    // 查询更新前的数据
                    List<GaiaProductBusiness> proOriginalProList = gaiaProductBusinessMapper.selectProListByKeyList(client, productBusiness.getProSelfCode(), productKeyList);
                    // 比对以及发起工作流
                    changeList.addAll(productDiff(proOriginalProList, productBusiness, userId, flowNO, remarks));
                    continue;
                }
            }
            // 修改记录
            List<GaiaProductBusiness> proOriginalProList = gaiaProductBusinessMapper.selectProListByKeyList(client, productBusiness.getProSelfCode(), productKeyList);
            if (!CollectionUtils.isEmpty(proOriginalProList)) {
                for (GaiaProductBusiness gaiaProductBusiness : proOriginalProList) {
                    if (!com.gov.purchase.utils.StringUtils.equals(gaiaProductBusiness.getProFixBin(), productBusiness.getProFixBin())) {
                        baseService.updateProFixBin(gaiaProductBusiness.getClient(), gaiaProductBusiness.getProSite(), userId,
                                gaiaProductBusiness.getProSelfCode(), gaiaProductBusiness.getProFixBin(), productBusiness.getProFixBin());
                    }
                }
            }
            List<GaiaProductChange> productDiffList = productDiff(proOriginalProList, productBusiness, userId, null, remarks);
            changeList.addAll(productDiffList);
            // 直接更新
            productBusiness.setProUpdateUser(userId);
            productBusiness.setClient(client);
            gaiaProductBusinessMapper.updateBatch(productBusiness, productKeyList);
            // 生产厂家PRO_FACTORY_NAME，产地 PRO_PLACE时，检查PRO_STORAGE_AREA如果不是3，
            // 则根据加盟商+商品 到GAIA_BATCH_INFO中把对应的BAT_FACTORY_NAME 或者 BAT_PRO_PLACE改掉。
            if (!"3".equals(productBusiness.getProStorageArea())) {
                GaiaProductChange gaiaProductChange = productDiffList.stream().filter(t ->
                        t.getProChangeField().equals("proFactoryName") || t.getProChangeField().equals("proPlace")).findFirst().orElse(null);
                if (gaiaProductChange != null && StringUtils.isNotBlank(productBusiness.getProFactoryName()) || StringUtils.isNotBlank(productBusiness.getProPlace())) {
                    GaiaBatchInfo gaiaBatchInfo = gaiaBatchInfoList.stream().filter(t ->
                            t.getBatProCode().equals(productBusiness.getProSelfCode())
                                    && t.getClient().equals(client)
                    ).findFirst().orElse(null);
                    if (gaiaBatchInfo == null) {
                        gaiaBatchInfo = new GaiaBatchInfo();
                        gaiaBatchInfo.setClient(client);
                        gaiaBatchInfo.setBatProCode(productBusiness.getProSelfCode());
                        gaiaBatchInfo.setBatChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                        gaiaBatchInfo.setBatChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                        gaiaBatchInfo.setBatChangeUser(userId);
                        if (StringUtils.isNotBlank(productBusiness.getProFactoryName())) {
                            gaiaBatchInfo.setBatFactoryCode(productBusiness.getProFactoryCode());
                            gaiaBatchInfo.setBatFactoryName(productBusiness.getProFactoryName());
                        }
                        if (StringUtils.isNotBlank(productBusiness.getProPlace())) {
                            gaiaBatchInfo.setBatProPlace(productBusiness.getProPlace());
                        }
                        gaiaBatchInfoList.add(gaiaBatchInfo);
                        gaiaBatchInfoMapper.updateFPByPrimaryKeySelective(gaiaBatchInfo);
                    }
                }
            }
            // 第三方 gys-operation
            multipleStoreProductSyncStoreList.add(productBusiness.getProSite());
            multipleStoreProductSyncProIdList.add(productBusiness.getProSelfCode());
        }

        // 发起工作流
        if (!CollectionUtils.isEmpty(changeList)) {
            List<GaiaProductChange> excludeChangeList = new ArrayList<>();
            for (GaiaProductChange change : changeList) {
                if (!"proNoApply".equals(change.getProChangeField()) &&
                        !"proNoRetail".equals(change.getProChangeField()) &&
                        !"proNoDc".equals(change.getProChangeField()) &&
                        !"proNoAdjust".equals(change.getProChangeField())) {
                    excludeChangeList.add(change);
                }
            }
            // 修改记录保存DB
            gaiaProductChangeMapper.saveBatch(excludeChangeList);
            // 需要发起工作流的DC
            List<String> dcCodeList = dcListToUpdate.stream().filter(Objects::nonNull)
                    .filter(item -> CommonEnum.NoYesStatus.YES.getCode().equals(item.getDcXgsfwf()))
                    .map(GaiaDcData::getDcCode).collect(Collectors.toList());
            List<GaiaProductChange> changeListToCreateFlow = excludeChangeList.stream().filter(Objects::nonNull)
                    .filter(item -> dcCodeList.contains(item.getProSite())).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(changeListToCreateFlow)) {
                // 自定义页签字段名
                GaiaProductZdy productZdy = getProductZdy(client, null);
                BeanMap zdyNameMap = ObjectUtils.isEmpty(productZdy) ? null : BeanMap.create(productZdy);
                FeignResult result = feignService.createEditProductWorkflow(changeListToCreateFlow, flowNO, zdyNameMap);
                if (result.getCode() != 0) {
                    throw new CustomResultException(result.getMessage());
                }
            }
        }

        Map<String, Object> result = new HashMap<>();
        if (!CollectionUtils.isEmpty(multipleStoreProductSyncStoreList) && !CollectionUtils.isEmpty(multipleStoreProductSyncProIdList)) {
            result.put("client", client);
            result.put("proSiteList", multipleStoreProductSyncStoreList.stream().distinct().collect(Collectors.toList()));
            result.put("proSelfCodeList", multipleStoreProductSyncProIdList.stream().distinct().collect(Collectors.toList()));
        }
        return result;
    }

    /**
     * 设置商品描述和商品助记码
     */
    private void setProDepictAndPym(GaiaProductBusiness productBusiness) {
        // 商品描述
        if (StringUtils.isBlank(productBusiness.getProDepict())) {
            String proDepict = productBusiness.getProCommonname();
            if (StringUtils.isNotBlank(productBusiness.getProCommonname()) && !productBusiness.getProCommonname().equals(productBusiness.getProName())) {
                String proNameTemp = StringUtils.isBlank(productBusiness.getProName()) ? "" : "(" + productBusiness.getProName() + ")";
                proDepict = productBusiness.getProCommonname() + proNameTemp;
            }
            productBusiness.setProDepict(proDepict);
        }
        // 助记码
        if (StringUtils.isBlank(productBusiness.getProPym())) {
            productBusiness.setProPym(com.gov.purchase.utils.StringUtils.ToFirstChar(productBusiness.getProDepict()));
        }
    }

    private List<GaiaProductChange> productDiff(List<GaiaProductBusiness> productFromList, GaiaProductBusiness productTo, String userId, String flowNO, String remarks) {
        return productFromList.stream().flatMap(productFrom -> {
            List<DiffEntityReuslt> diffResultList = diffEntityUtils.diff(productFrom, productTo, diffEntityConfig.getProductDiffPropertyList());
            return diffResultList.stream().map(item -> {
                GaiaProductChange productChange = new GaiaProductChange();
                productChange.setClient(productFrom.getClient());
                productChange.setProSite(productFrom.getProSite());
                productChange.setProSelfCode(productFrom.getProSelfCode());
                productChange.setProCommonname(productFrom.getProCommonname());
                productChange.setProSpecs(productFrom.getProSpecs());
                productChange.setProFactoryName(productFrom.getProFactoryName());
                productChange.setProRegisterNo(productFrom.getProRegisterNo());
                productChange.setProChangeField(item.getPropertyFrom());
                productChange.setProChangeFrom(item.getValueFrom());
                productChange.setProChangeTo(item.getValueTo());
                productChange.setProChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                productChange.setProChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                productChange.setProChangeUser(userId);
                productChange.setProFlowNo(flowNO);
                // todo 这里设置默认值
                productChange.setProSfsp("0");
                productChange.setProSfty("0");
                productChange.setProRemarks(remarks);
                return productChange;
            });
        }).collect(Collectors.toList());
    }

    /**
     * 校验
     */
    private void proEditListCheck(List<GaiaProductBusiness> business) {
        // 非空校验
        if (CollectionUtils.isEmpty(business)) {
            throw new CustomResultException("商品列表不能为空");
        }
        business.forEach(item -> {
            if (StringUtils.isBlank(item.getProSite())) {
                throw new CustomResultException("地点不能为空");
            }
            if (StringUtils.isBlank(item.getProSelfCode())) {
                throw new CustomResultException("商品自编码不能为空");
            }
            if (StringUtils.isBlank(item.getProCommonname())) {
                throw new CustomResultException("通用名称不能为空");
            }
            if (StringUtils.isBlank(item.getProSpecs())) {
                throw new CustomResultException("规格不能为空");
            }
            if (StringUtils.isBlank(item.getProUnit())) {
                throw new CustomResultException("计量单位不能为空");
            }
            if (StringUtils.isBlank(item.getProFactoryName())) {
                throw new CustomResultException("生产企业不能为空");
            }
            if (StringUtils.isBlank(item.getProInputTax())) {
                throw new CustomResultException("进项税率不能为空");
            }
            if (StringUtils.isBlank(item.getProStorageCondition())) {
                throw new CustomResultException("贮存条件不能为空");
            }
            if (StringUtils.isBlank(item.getProMidPackage())) {
                throw new CustomResultException("中包装不能为空");
            }
        });
    }


    /**
     * "商品状态" 为 "停用"时，检查库存（仓库和门店库存）
     */
    private void proStockCheck(List<GaiaProductBusiness> businessList, String client) {

        List<String> proSelfCodeList = businessList.stream().filter(Objects::nonNull).map(GaiaProductBusiness::getProSelfCode).collect(Collectors.toList());
        List<ProStockDTO> proStockList = gaiaProductBusinessMapper.getProStock(client, proSelfCodeList);
        Map<String, Integer> proStockMap = proStockList.stream().filter(Objects::nonNull).collect(Collectors.toMap(ProStockDTO::getWmSpBm, ProStockDTO::getWmKcsl));

        List<String> proList = new ArrayList<>();
        for (GaiaProductBusiness business : businessList) {
            if (CommonEnum.NoYesStatus.YES.getCode().equals(business.getProStatus())) {
                Integer wmKcsl = proStockMap.get(business.getProSelfCode());
                if (!ObjectUtils.isEmpty(wmKcsl) && wmKcsl > 0) {
                    proList.add(business.getProSelfCode());
                }
            }
        }
        if (!CollectionUtils.isEmpty(proList)) {
            throw new CustomResultException(MessageFormat.format("该商品[{0}]在公司下还有库存，不可停用，建议先做禁采!", String.join(",", proList)));
        }

    }


    /**
     * 商品详情
     */
    @Override
    public GetProDetailsResponseDTO getProDetails(String proSite, String proSelfCode) {
        if (StringUtils.isBlank(proSite)) {
            throw new CustomResultException("地点/门店不能为空");
        }
        if (StringUtils.isBlank(proSelfCode)) {
            throw new CustomResultException("自编码不能为空");
        }
        TokenUser user = feignService.getLoginInfo();
        GaiaProductBusinessKey primaryKey = new GaiaProductBusinessKey();
        primaryKey.setClient(user.getClient());
        primaryKey.setProSite(proSite);
        primaryKey.setProSelfCode(proSelfCode);
        GaiaProductBusiness business = gaiaProductBusinessMapper.selectByPrimaryKey(primaryKey);
        GetProDetailsResponseDTO dto = new GetProDetailsResponseDTO();
        BeanUtils.copyProperties(business, dto);
        // 内包装图片
        dto.setProPictureNbzUrl(cosUtils.urlAuth(business.getProPictureNbz()));
        // 批件图片
        dto.setProPicturePjUrl(cosUtils.urlAuth(business.getProPicturePj()));
        // 其他图片
        dto.setProPictureQtUrl(cosUtils.urlAuth(business.getProPictureQt()));
        // 说明书图片
        dto.setProPictureSmsUrl(cosUtils.urlAuth(business.getProPictureSms()));
        // 外包装图片
        dto.setProPictureWbzUrl(cosUtils.urlAuth(business.getProPictureWbz()));
        // 生产企业证照图片
        dto.setProPictureZzUrl(cosUtils.urlAuth(business.getProPictureZz()));
        return dto;
    }

    /**
     * 商品详情编辑
     */
    @Override
    public Map<String, Object> proEdit(ProEditRequestDTO dto, String remarks, Integer editNoWws) {
        TokenUser user = feignService.getLoginInfo();
        if (StringUtils.isBlank(dto.getProClass()) || StringUtils.length(dto.getProClass().trim()) < 5) {
            dto.setProClass(StringUtils.isBlank(dto.getProClass()) ? "" : dto.getProClass());
            dto.setProClass(StringUtils.rightPad(StringUtils.left(dto.getProClass(), 1), 5, "9"));
        }
        GaiaProductBusinessKey primaryKey = new GaiaProductBusinessKey();
        primaryKey.setClient(user.getClient());
        primaryKey.setProSite(dto.getProSite());
        primaryKey.setProSelfCode(dto.getProSelfCode());
        GaiaProductBusiness businessExit = gaiaProductBusinessMapper.selectByPrimaryKey(primaryKey);
        if (ObjectUtils.isEmpty(businessExit)) {
            throw new CustomResultException("[商品自编码]或[地点]错误,不存在该数据");
        }
        // 保存
        GaiaProductBusiness business = new GaiaProductBusiness();
        BeanUtils.copyProperties(dto, business);
        business.setClient(user.getClient());
        this.proStockCheck(Collections.singletonList(business), user.getClient());
        Map<String, Object> map = proEditList(Collections.singletonList(business), remarks, editNoWws);
        return map;
    }

}
