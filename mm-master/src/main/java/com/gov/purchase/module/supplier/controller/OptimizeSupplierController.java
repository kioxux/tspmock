package com.gov.purchase.module.supplier.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaSupplierBusinessKey;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.supplier.dto.*;
import com.gov.purchase.module.supplier.service.OptimizeSupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "供应商优化")
@RestController
@RequestMapping("supplier")
public class OptimizeSupplierController {

    @Resource
    private OptimizeSupplierService optimizeSupplierService;

    @Log("供应商basic表_列表数据")
    @ApiOperation("供应商basic表_列表数据")
    @PostMapping("getSupBasicList")
    public Result getSupBasicList(@Valid @RequestBody GetSupBasicListRequestDTO dto) {
        return ResultUtil.success(optimizeSupplierService.getSupBasicList(dto));
    }

    @Log("供应商保存1_先与已有数据进行比对_返回比对结果集")
    @ApiOperation("供应商保存1_先与已有数据进行比对_返回比对结果集")
    @PostMapping("saveSupList1")
    public Result saveSupList1(@Valid @RequestBody List<GaiaSupplierBusinessInfo> list) {
        return ResultUtil.success(optimizeSupplierService.saveSupList1(list));
    }

    @Log("供应商保存2")
    @ApiOperation("供应商保存2")
    @PostMapping("saveSupList2")
    public Result saveSupList2(@Valid @RequestBody SaveSupList2RequestDTO dto) {
        optimizeSupplierService.saveSupList2(dto);
        return ResultUtil.success();
    }

    @Log("供应商查询列表")
    @ApiOperation("供应商查询列表")
    @PostMapping("getSupList")
    public Result getSupList(@Valid @RequestBody GetSupListRequestDTO dto) {
        return ResultUtil.success(optimizeSupplierService.getSupList(dto));
    }

    @Log("供应商列表导出")
    @ApiOperation("供应商列表导出")
    @PostMapping("exportSupList")
    public Result exportSupList(@Valid @RequestBody GetSupListRequestDTO dto) {
        return optimizeSupplierService.exportSupList(dto);
    }

    @Log("批量更新")
    @ApiOperation("批量更新")
    @PostMapping("editSupList")
    public Result editSupList(@RequestBody @Valid EditSupListVO vo) {
        optimizeSupplierService.editSupList(vo.getBusinessList(), vo.getRemarks());
        return ResultUtil.success();
    }

    @Log("详情")
    @ApiOperation("详情")
    @PostMapping("getSupDetails")
    public Result getSupDetails(@Valid @RequestBody GaiaSupplierBusinessKey key) {
        return ResultUtil.success(optimizeSupplierService.getSupDetails(key));
    }

    @Log("更新")
    @ApiOperation("更新")
    @PostMapping("editSup")
    public Result editSup(@RequestBody @Valid EditSupVO vo) {
        optimizeSupplierService.editSup(vo.getBusiness(), vo.getRemarks());
        return ResultUtil.success();
    }

    @Log("供应商质量负责人、业务联系人")
    @ApiOperation("供应商质量负责人、业务联系人")
    @PostMapping("getUserList")
    public Result getUserList() {
        return ResultUtil.success(optimizeSupplierService.getUserList());
    }

    @Log("供应商业务员编辑")
    @ApiOperation("供应商业务员编辑")
    @PostMapping("editSupplierSalesmanList")
    public Result editSupplierSalesmanList(@Valid @RequestBody EditSupplierSalesmanListVO vo) {
        optimizeSupplierService.editSupplierSalesmanList(vo);
        return ResultUtil.success();
    }

    @Log("供应商业务员列表")
    @ApiOperation("供应商业务员列表")
    @PostMapping("getSupplierSalesmanList")
    public Result getSupplierSalesmanList(@RequestJson("supSite") String supSite,
                                          @RequestJson("supSelfCode") String supSelfCode) {
        return ResultUtil.success(optimizeSupplierService.getSupplierSalesmanList(supSite, supSelfCode));
    }

    @Log("供应商经营范围编辑保存")
    @ApiOperation("供应商经营范围编辑保存")
    @PostMapping("editSupScope")
    public Result editSupScope(@RequestBody @Valid EditScopeVO vo) {
        optimizeSupplierService.editSupScope(vo);
        return ResultUtil.success();
    }

}
