package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaWholesaleChannelDetail;
import lombok.Data;

/**
 * @description:
 * @author: yzf
 * @create: 2021-11-03 13:56
 */
@Data
public class WholeSaleChannelDetailVo extends GaiaWholesaleChannelDetail {
    private String cusName;
    private String cusSelfCode;
    private String proName;
    private String proSelfCode;
    private String proSpecs;
    private String proFactoryName;
    private String proPlace;
    private String proUnit;
    private String proLsj;
    private String proGpj;
    private String proGlj;
    private String proYsj1;
    private String proYsj2;
    private String proYsj3;
}
