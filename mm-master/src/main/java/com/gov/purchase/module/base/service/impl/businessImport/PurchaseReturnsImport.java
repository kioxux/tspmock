package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.module.base.dto.businessImport.PurchaseReturns;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.purchase.dto.BatchStockListRequestDto;
import com.gov.purchase.module.purchase.dto.BatchStockListResponseDto;
import com.gov.purchase.module.purchase.service.ChangePurchaseService;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class PurchaseReturnsImport extends BusinessImport {

    @Resource
    private ChangePurchaseService changePurchaseService;

    /**
     * 业务验证(采购退货单处理)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {

        //错误LIST
        List<String> errorList = new ArrayList<>();

        // 批量导入
        try {
            BatchStockListRequestDto batchStockListRequestDto = new BatchStockListRequestDto();
            for (String key : field.keySet()) {
                if (key.equals("client")) {
                    //参数空值判断
                    if (StringUtils.isBlank((String) field.get(key))) {
                        errorList.add(MessageFormat.format(ResultEnum.CHECK_PARAMETER.getMsg(), ""));
                    }
                    batchStockListRequestDto.setClient((String) field.get(key));
                }
                if (key.equals("batSiteCode")) {
                    //参数空值判断
                    if (StringUtils.isBlank((String) field.get(key))) {
                        errorList.add(MessageFormat.format(ResultEnum.CHECK_PARAMETER.getMsg(), ""));
                    }
                    batchStockListRequestDto.setBatSiteCode((String) field.get(key));
                }
            }
            List<BatchStockListResponseDto> batchStockList = new ArrayList<>();
            // 证照编码
            Map<String, List<String>> proCodeMap = new HashMap<>();

            for (Integer key : map.keySet()) {
                // 行数据
                PurchaseReturns purchaseReturns = (PurchaseReturns) map.get(key);
                batchStockListRequestDto.setBatLocationCode(purchaseReturns.getBatLocationCodeValue());
                batchStockListRequestDto.setBatProCode(purchaseReturns.getBatProCode());
                batchStockListRequestDto.setBatSupplierCode(purchaseReturns.getBatSupplierCode());
                batchStockListRequestDto.setBatBatch(purchaseReturns.getBatBatchNo());
                List<BatchStockListResponseDto> batchStock = changePurchaseService.selectBatchStockList(batchStockListRequestDto);
                // 数据不存在
                if (CollectionUtils.isEmpty(batchStock)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0027.getMsg(), key + 1));
                }
                // 商品编码
                List<String> proCodeList = proCodeMap.get(purchaseReturns.getBatProCode());
                if (proCodeList == null) {
                    proCodeList = new ArrayList<>();
                }

                proCodeList.add(String.valueOf((key + 1)));
                proCodeMap.put(purchaseReturns.getBatProCode(), proCodeList);

                for (BatchStockListResponseDto batchStockListResponseDto : batchStock) {
                    if (StringUtils.isNotBlank(purchaseReturns.getReturnQuantity())) {
                        batchStockListResponseDto.setReturnQuantity(new BigDecimal(purchaseReturns.getReturnQuantity()));
                    }
                    batchStockList.add(batchStockListResponseDto);
                }
            }

            //商品编码重复验证
            for (String proCode : proCodeMap.keySet()) {
                List<String> lineList = proCodeMap.get(proCode);
                if (lineList.size() > 1) {
                    errorList.add(MessageFormat.format(ResultEnum.E0026.getMsg(), String.join(",", lineList)));
                }
            }

            // 验证报错
            if (CollectionUtils.isNotEmpty(errorList)) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }

            return ResultUtil.success(batchStockList);

        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }
    }
}
