package com.gov.purchase.module.base.dto.excel;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.30
 */
@Data
public class Product {

    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false)
    private String proCode;
    /**
     * 通用名称
     */
    @ExcelValidate(index = 1, name = "通用名称", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String proCommonname;
    /**
     * 商品描述
     */
    @ExcelValidate(index = 2, name = "商品描述", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String proDepict;
    /**
     * 助记码
     */
    @ExcelValidate(index = 3, name = "助记码", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String proPym;
    /**
     * 商品名
     */
    @ExcelValidate(index = 4, name = "商品名", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String proName;
    /**
     * 规格
     */
    @ExcelValidate(index = 5, name = "规格", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String proSpecs;
    /**
     * 计量单位
     */
    @ExcelValidate(index = 6, name = "计量单位", type = ExcelValidate.DataType.STRING, editRequired = false)
    private String proUnit;
    /**
     * 剂型
     */
    @ExcelValidate(index = 7, name = "剂型", type = ExcelValidate.DataType.STRING, relyRequired = {"proPartform"}, editRequired = false)
    private String proForm;
    /**
     * 细分剂型
     */
    @ExcelValidate(index = 8, name = "细分剂型", type = ExcelValidate.DataType.STRING, addRequired = false, editRequired = false)
    private String proPartform;
    /**
     * 最小剂量（以mg/ml计算）
     */
    @ExcelValidate(index = 9, name = "最小剂量（以mg/ml计算）", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false)
    private String proMindose;
    /**
     * 总剂量（以mg/ml计算）
     */
    @ExcelValidate(index = 10, name = "总剂量（以mg/ml计算）", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false)
    private String proTotaldose;
    /**
     * 国际条形码1
     */
    @ExcelValidate(index = 11, name = "国际条形码1", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proBarcode;
    /**
     * 国际条形码2
     */
    @ExcelValidate(index = 12, name = "国际条形码2", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proBarcode2;
    /**
     * 批准文号分类
     */
    @ExcelValidate(index = 13, name = "批准文号分类", type = ExcelValidate.DataType.STRING,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_REGISTER_CLASS, editRequired = false)
    private String proRegisterClass;
    /**
     * 批准文号
     */
    @ExcelValidate(index = 14, name = "批准文号", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false)
    private String proRegisterNo;
    /**
     * 批准文号批准日期
     */
    @ExcelValidate(index = 15, name = "批准文号批准日期", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", addRequired = false, editRequired = false)
    private String proRegisterDate;
    /**
     * 批准文号失效日期
     */
    @ExcelValidate(index = 16, name = "批准文号失效日期", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", addRequired = false, editRequired = false)
    private String proRegisterExdate;
    /**
     * 商品分类
     */
    @ExcelValidate(index = 17, name = "商品分类", type = ExcelValidate.DataType.STRING, editRequired = false)
    private String proClass;
    /**
     * 商品分类描述
     */
    private String proClassName;
    /**
     * 成分分类
     */
    @ExcelValidate(index = 18, name = "成分分类", type = ExcelValidate.DataType.STRING, editRequired = false)
    private String proCompclass;
    /**
     * 成分分类描述
     */
    private String proCompclassName;
    /**
     * 处方类别
     */
    @ExcelValidate(index = 19, name = "处方类别", type = ExcelValidate.DataType.STRING,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_PRESCLASS, editRequired = false)
    private String proPresclass;
    /**
     * 生产企业代码
     */
    @ExcelValidate(index = 20, name = "生产企业代码", type = ExcelValidate.DataType.STRING, maxLength = 10, editRequired = false)
    private String proFactoryCode;
    /**
     * 生产企业
     */
    private String proFactoryName;
    /**
     * 商标
     */
    @ExcelValidate(index = 21, name = "商标", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false)
    private String proMark;
    /**
     * 品牌标识名
     */
    @ExcelValidate(index = 22, name = "品牌标识名", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false)
    private String proBrand;
    /**
     * 品牌区分
     */
    @ExcelValidate(index = 23, name = "品牌区分", type = ExcelValidate.DataType.STRING, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_BRAND_CLASS, editRequired = false)
    private String proBrandClass;
    /**
     * 保质期
     */
    @ExcelValidate(index = 24, name = "保质期", type = ExcelValidate.DataType.INTEGER, addRequired = false, min = 1, editRequired = false)
    private String proLife;
    /**
     * 保质期单位
     */
    @ExcelValidate(index = 25, name = "保质期单位", type = ExcelValidate.DataType.STRING, addRequired = false, maxLength = 1,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_LIFE_UNIT, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proLifeUnitValue", editRequired = false)
    private String proLifeUnit;
    /**
     * 保质期单位
     */
    private String proLifeUnitValue;
    /**
     * 上市许可持有人
     */
    @ExcelValidate(index = 26, name = "上市许可持有人", type = ExcelValidate.DataType.STRING, addRequired = false, maxLength = 20, editRequired = false)
    private String proHolder;
    /**
     * 进项税率
     */
    @ExcelValidate(index = 27, name = "进项税率", type = ExcelValidate.DataType.STRING, editRequired = false)
    private String proInputTax;
    /**
     * 销项税率
     */
    @ExcelValidate(index = 28, name = "销项税率", type = ExcelValidate.DataType.STRING, editRequired = false)
    private String proOutputTax;
    /**
     * 药品本位码
     */
    @ExcelValidate(index = 29, name = "药品本位码", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String proBasicCode;
    /**
     * 税务分类编码
     */
    @ExcelValidate(index = 30, name = "税务分类编码", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proTaxClass;
    /**
     * 管制特殊分类
     */
    @ExcelValidate(index = 31, name = "管制特殊分类", type = ExcelValidate.DataType.STRING, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_CONTROL_CLASS, editRequired = false)
    private String proControlClass;
    /**
     * 生产类别
     */
    @ExcelValidate(index = 32, name = "生产类别", type = ExcelValidate.DataType.STRING, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_PRODUCE_CLASS, editRequired = false)
    private String proProduceClass;
    /**
     * 贮存条件
     */
    @ExcelValidate(index = 33, name = "贮存条件", type = ExcelValidate.DataType.STRING,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_STORAGE_CONDITION, editRequired = false)
    private String proStorageCondition;
    /**
     * 商品仓储分区
     */
    @ExcelValidate(index = 34, name = "商品仓储分区", type = ExcelValidate.DataType.STRING,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA, editRequired = false)
    private String proStorageArea;
    /**
     * 长（以MM计算）
     */
    @ExcelValidate(index = 35, name = "长（以MM计算）", type = ExcelValidate.DataType.INTEGER, min = 1, max = 9999999999.0, addRequired = false, editRequired = false)
    private String proLong;
    /**
     * 宽（以MM计算）
     */
    @ExcelValidate(index = 36, name = "宽（以MM计算）", type = ExcelValidate.DataType.INTEGER, min = 1, max = 9999999999.0, addRequired = false, editRequired = false)
    private String proWide;
    /**
     * 高（以MM计算）
     */
    @ExcelValidate(index = 37, name = "高（以MM计算）", type = ExcelValidate.DataType.INTEGER, min = 1, max = 9999999999.0, addRequired = false, editRequired = false)
    private String proHigh;
    /**
     * 中包装量
     */
    @ExcelValidate(index = 38, name = "中包装量", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proMidPackage;
    /**
     * 大包装量
     */
    @ExcelValidate(index = 39, name = "大包装量", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proBigPackage;
    /**
     * 启用电子监管码
     */
    @ExcelValidate(index = 40, name = "启用电子监管码", type = ExcelValidate.DataType.STRING, maxLength = 1,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proElectronicCodeValue", editRequired = false)
    private String proElectronicCode;
    /**
     * 启用电子监管码
     */
    private String proElectronicCodeValue;
    /**
     * 生产经营许可证号
     */
    @ExcelValidate(index = 41, name = "生产经营许可证号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proQsCode;
    /**
     * 最大销售量
     */
    @ExcelValidate(index = 42, name = "最大销售量", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proMaxSales;
    /**
     * 说明书代码
     */
    @ExcelValidate(index = 43, name = "说明书代码", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String proInstructionCode;
    /**
     * 说明书内容
     */
    @ExcelValidate(index = 44, name = "说明书内容", type = ExcelValidate.DataType.STRING, maxLength = 500, addRequired = false, editRequired = false)
    private String proInstruction;
    /**
     * 国家医保品种
     */
    @ExcelValidate(index = 45, name = "国家医保品种", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proMedProdctValue", editRequired = false)
    private String proMedProdct;
    /**
     * 国家医保品种
     */
    private String proMedProdctValue;
    /**
     * 国家医保品种编码
     */
    @ExcelValidate(index = 46, name = "国家医保品种编码", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proMedProdctcode;
    /**
     * 生产国家
     */
    @ExcelValidate(index = 47, name = "生产国家", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String proCountry;
    /**
     * 产地
     */
    @ExcelValidate(index = 48, name = "产地", type = ExcelValidate.DataType.STRING, maxLength = 20, editRequired = false)
    private String proPlace;
    /**
     * 可服用天数
     */
    @ExcelValidate(index = 49, name = "可服用天数", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String proTakeDays;
    /**
     * 用法用量
     */
    @ExcelValidate(index = 50, name = "用法用量", type = ExcelValidate.DataType.STRING, maxLength = 255, addRequired = false, editRequired = false)
    private String proUsage;
    /**
     * 禁忌说明
     */
    @ExcelValidate(index = 51, name = "禁忌说明", type = ExcelValidate.DataType.STRING, maxLength = 255, addRequired = false, editRequired = false)
    private String proContraindication;
    /**
     * 加盟商
     */
    @ExcelValidate(index = 52, name = "加盟商", type = ExcelValidate.DataType.STRING, maxLength = 8, addRequired = false, editRequired = false,
            relyRequired = {"proSite", "proSelfCode", "proStatus", "proPosition", "proNoRetail", "proNoPurchase",
                    "proNoDistributed", "proNoSupplier", "proNoDc", "proNoAdjust", "proNoSale", "proNoApply",
                    "proIfpart", "proPartUint", "proPartRate", "proPurchaseUnit", "proPurchaseRate", "proSaleUnit", "proSaleRate",
                    "proMinQty", "proIfMed", "proSlaeClass", "proLimitQty", "proTcmSpecs", "proTcmRegisterNo", "proTcmFactoryCode", "proTcmPlace",
                    "proMaxQty", "proPackageFlag", "proKeyCare", "proFixBin"})
    private String client;
    /**
     * 地点
     */
    @ExcelValidate(index = 53, name = "地点", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false,
            relyRequired = {"client"})
    private String proSite;
    /**
     * 商品自编码
     */
    @ExcelValidate(index = 54, name = "商品自编码", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false,
            relyRequired = {"client"})
    private String proSelfCode;
    /**
     * 商品状态
     */
    @ExcelValidate(index = 55, name = "商品状态", type = ExcelValidate.DataType.STRING, maxLength = 2, addRequired = false, editRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_STATUS, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            relyRequired = {"client"}, dictionaryStaticField = "proStatusValue")
    private String proStatus;
    /**
     * 商品状态
     */
    private String proStatusValue;
    /**
     * 商品定位
     */
    @ExcelValidate(index = 56, name = "商品定位", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false)
    private String proPosition;
    /**
     * 禁止销售
     */
    @ExcelValidate(index = 57, name = "禁止销售", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proNoRetailValue", editRequired = false)
    private String proNoRetail;
    /**
     * 禁止销售
     */
    private String proNoRetailValue;
    /**
     * 禁止采购
     */
    @ExcelValidate(index = 58, name = "禁止采购", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proNoPurchaseValue", editRequired = false)
    private String proNoPurchase;
    /**
     * 禁止采购
     */
    private String proNoPurchaseValue;
    /**
     * 禁止配送
     */
    @ExcelValidate(index = 59, name = "禁止配送", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proNoDistributedValue", editRequired = false)
    private String proNoDistributed;
    /**
     * 禁止配送
     */
    private String proNoDistributedValue;
    /**
     * 禁止退厂
     */
    @ExcelValidate(index = 60, name = "禁止退厂", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proNoSupplierValue", editRequired = false)
    private String proNoSupplier;
    /**
     * 禁止退厂
     */
    private String proNoSupplierValue;
    /**
     * 禁止退仓
     */
    @ExcelValidate(index = 61, name = "禁止退仓", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proNoDcValue", editRequired = false)
    private String proNoDc;
    /**
     * 禁止退仓
     */
    private String proNoDcValue;
    /**
     * 禁止调剂
     */
    @ExcelValidate(index = 62, name = "禁止调剂", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proNoAdjustValue", editRequired = false)
    private String proNoAdjust;
    /**
     * 禁止调剂
     */
    private String proNoAdjustValue;
    /**
     * 禁止批发
     */
    @ExcelValidate(index = 63, name = "禁止批发", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proNoSaleValue", editRequired = false)
    private String proNoSale;
    /**
     * 禁止批发
     */
    private String proNoSaleValue;
    /**
     * 禁止请货
     */
    @ExcelValidate(index = 64, name = "禁止请货", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proNoApplyValue", editRequired = false)
    private String proNoApply;
    /**
     * 禁止请货
     */
    private String proNoApplyValue;
    /**
     * 是否拆零
     */
    @ExcelValidate(index = 65, name = "是否拆零", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proIfpartValue", editRequired = false)
    private String proIfpart;
    /**
     * 是否拆零
     */
    private String proIfpartValue;
    /**
     * 拆零单位
     */
    @ExcelValidate(index = 66, name = "拆零单位", type = ExcelValidate.DataType.STRING, addRequired = false, editRequired = false)
    private String proPartUint;
    /**
     * 拆零比例
     */
    @ExcelValidate(index = 67, name = "拆零比例", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String proPartRate;
    /**
     * 采购单位
     */
    @ExcelValidate(index = 68, name = "采购单位", type = ExcelValidate.DataType.STRING, addRequired = false, editRequired = false)
    private String proPurchaseUnit;
    /**
     * 采购比例
     */
    @ExcelValidate(index = 69, name = "采购比例", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String proPurchaseRate;
    /**
     * 销售单位
     */
    @ExcelValidate(index = 70, name = "销售单位", type = ExcelValidate.DataType.STRING, addRequired = false, editRequired = false)
    private String proSaleUnit;
    /**
     * 销售比例
     */
    @ExcelValidate(index = 71, name = "销售比例", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String proSaleRate;
    /**
     * 最小订货量
     */
    @ExcelValidate(index = 72, name = "最小订货量", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = false, editRequired = false)
    private String proMinQty;
    /**
     * 是否医保
     */
    @ExcelValidate(index = 73, name = "是否医保", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "proIfMedValue", editRequired = false)
    private String proIfMed;
    /**
     * 是否医保
     */
    private String proIfMedValue;
    /**
     * 销售级别
     */
    @ExcelValidate(index = 74, name = "销售级别", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String proSlaeClass;
    /**
     * 限购数量
     */
    @ExcelValidate(index = 75, name = "限购数量", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = false, editRequired = false)
    private String proLimitQty;
    /**
     * 中药规格
     */
    @ExcelValidate(index = 76, name = "中药规格", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false)
    private String proTcmSpecs;
    /**
     * 中药批准文号
     */
    @ExcelValidate(index = 77, name = "中药批准文号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proTcmRegisterNo;
    /**
     * 中药生产企业代码
     */
    @ExcelValidate(index = 78, name = "中药生产企业代码", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String proTcmFactoryCode;
    /**
     * 中药产地
     */
    @ExcelValidate(index = 79, name = "中药产地", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proTcmPlace;
    /**
     * 含麻最大配货量
     */
    @ExcelValidate(index = 80, name = "含麻最大配货量", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = false, editRequired = false)
    private String proMaxQty;
    /**
     * 按中包装量倍数配货
     */
    @ExcelValidate(index = 81, name = "按中包装量倍数配货", type = ExcelValidate.DataType.STRING, maxLength = 1,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES,
            dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL, dictionaryStaticField = "proPackageFlagValue",
            addRequired = false, editRequired = false)
    private String proPackageFlag;
    /**
     * 按中包装量倍数配货
     */
    private String proPackageFlagValue;
    /**
     * 是否重点养护
     */
    @ExcelValidate(index = 82, name = "是否重点养护", type = ExcelValidate.DataType.STRING, maxLength = 1,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.PRO_KEY_CARE,
            dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL, dictionaryStaticField = "proKeyCareValue",
            addRequired = false, editRequired = false)
    private String proKeyCare;
    /**
     * 是否重点养护
     */
    private String proKeyCareValue;

    /**
     * 固定货位
     */
    @ExcelValidate(index = 83, name = "固定货位", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String proFixBin;
}
