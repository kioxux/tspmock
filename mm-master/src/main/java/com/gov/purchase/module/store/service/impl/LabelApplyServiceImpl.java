package com.gov.purchase.module.store.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaLabelApply;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaLabelApplyMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.store.dto.EditLabelItemVO;
import com.gov.purchase.module.store.dto.GaiaLabelApplyDto;
import com.gov.purchase.module.store.service.LabelApplyService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.08
 */

@Service
public class LabelApplyServiceImpl implements LabelApplyService {

    @Resource
    private GaiaLabelApplyMapper gaiaLabelApplyMapper;
    @Resource
    private FeignService feignService;
    @Resource
    private RedisClient redisClient;
    @Resource
    private CosUtils cosUtils;
    /**
     * 申请编号缓存key
     */
    private final static String LABELAPPLYSERVICEIMPL = "LabelApplyServiceImpl_";

    /**
     * 价签申请
     *
     * @param gaiaLabelApplyList
     */
    @Override
    public void addLabel(List<GaiaLabelApply> gaiaLabelApplyList) {
        if (CollectionUtils.isEmpty(gaiaLabelApplyList)) {
            return;
        }
        TokenUser user = feignService.getLoginInfo();
        gaiaLabelApplyList.forEach(item -> {
            if (StringUtils.isBlank(item.getLabStore())) {
                throw new CustomResultException("请选择门店");
            }
        });
        Long labApplyNo = NumberUtils.toLong(gaiaLabelApplyMapper.selectLabApplyNo(user.getClient()));
        String num;
        do {
            String key = LABELAPPLYSERVICEIMPL + labApplyNo.toString();
            // 缓存取值
            num = redisClient.get(key);
            if (StringUtils.isBlank(num)) {
                // 设置缓存
                redisClient.set(key, key, 60);
            } else {
                labApplyNo++;
            }
        } while (StringUtils.isNotBlank(num));
        List<GaiaLabelApply> list = new ArrayList();
        for (GaiaLabelApply item : gaiaLabelApplyList) {
            GaiaLabelApply gaiaLabelApply = new GaiaLabelApply();
            // 加盟商
            gaiaLabelApply.setClient(user.getClient());
            // 门店编码
            gaiaLabelApply.setLabStore(item.getLabStore());
            // 价签申请单号
            gaiaLabelApply.setLabApplyNo(labApplyNo.toString());
            // 申请日期
            gaiaLabelApply.setLabApplyDate(DateUtils.getCurrentDateStrYYMMDD());
            // 商品编码
            gaiaLabelApply.setLabProduct(item.getLabProduct());
            // 申请份数
            gaiaLabelApply.setLabApplyQty(item.getLabApplyQty());
            // 状态
            gaiaLabelApply.setLabSuatus("0");
            // 创建日期
            gaiaLabelApply.setLabCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            // 创建时间
            gaiaLabelApply.setLabCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            // 打印次数
            gaiaLabelApply.setLabPrintTimes(0);
            // 最后打印日期
            // 最后打印时间
            list.add(gaiaLabelApply);
        }
        gaiaLabelApplyMapper.insertList(list);
    }

    /**
     * 申请列表编辑
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editLabelItem(EditLabelItemVO vo) {
        String client = feignService.getLoginInfo().getClient();
        String labStore = vo.getLabStore();
        String labApplyNo = vo.getLabApplyNo();

        List<EditLabelItemVO.LabelItemStore> labelItemStoreList = vo.getLabelItemStoreList();
        // 传入list为空，全删
        if (CollectionUtils.isEmpty(labelItemStoreList)) {
            gaiaLabelApplyMapper.delLabel(client, labStore, labApplyNo);
            return;
        } else {
            labelItemStoreList.stream().filter(Objects::nonNull).forEach(item -> {
                if (StringUtils.isBlank(item.getLabProduct())) {
                    throw new CustomResultException("商品编码不能为空");
                }
                if (StringUtils.isBlank(item.getLabApplyQty())) {
                    throw new CustomResultException("申请份数不能为空");
                }
            });
        }

        List<String> proExitList = gaiaLabelApplyMapper.getLabelItemNoStoInfoList(client, labApplyNo, labStore);
        // 1.要新增的数据
        List<GaiaLabelApply> labelApplyToInsertList = labelItemStoreList.stream().filter(Objects::nonNull)
                .filter(item -> !proExitList.contains(item.getLabProduct()))
                .map(item -> {
                    GaiaLabelApply gaiaLabelApply = new GaiaLabelApply();
                    // 加盟商
                    gaiaLabelApply.setClient(client);
                    // 门店编码
                    gaiaLabelApply.setLabStore(labStore);
                    // 价签申请单号
                    gaiaLabelApply.setLabApplyNo(labApplyNo);
                    // 申请日期
                    gaiaLabelApply.setLabApplyDate(DateUtils.getCurrentDateStrYYMMDD());
                    // 商品编码
                    gaiaLabelApply.setLabProduct(item.getLabProduct());
                    // 申请份数
                    gaiaLabelApply.setLabApplyQty(item.getLabApplyQty());
                    // 状态
                    gaiaLabelApply.setLabSuatus("0");
                    // 创建日期
                    gaiaLabelApply.setLabCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                    // 创建时间
                    gaiaLabelApply.setLabCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    // 打印次数
                    gaiaLabelApply.setLabPrintTimes(0);

                    return gaiaLabelApply;
                }).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(labelApplyToInsertList)) {
            gaiaLabelApplyMapper.insertList(labelApplyToInsertList);
        }

        // 2.要删除的数据
        List<String> proList = labelItemStoreList.stream().filter(Objects::nonNull)
                .map(EditLabelItemVO.LabelItemStore::getLabProduct).collect(Collectors.toList());

        proExitList.stream().filter(Objects::nonNull)
                .filter(pro -> !proList.contains(pro))
                .forEach(pro -> {
                    gaiaLabelApplyMapper.delLabelItem(client, labStore, labApplyNo, pro);
                });

        // 3.要编辑的数据 (申请份数字段)
        labelItemStoreList.stream().filter(Objects::nonNull)
                .filter(item -> proExitList.contains(item.getLabProduct()))
                .forEach(item -> {
                    gaiaLabelApplyMapper.editLabelItem(client, labStore, labApplyNo, item.getLabProduct(), item.getLabApplyQty());
                });

    }

    /**
     * 申请单关闭
     *
     * @param labStore
     * @param labApplyNo
     */
    @Override
    public void closeLable(String labStore, String labApplyNo) {
        // 当前登录人
        TokenUser user = feignService.getLoginInfo();
        gaiaLabelApplyMapper.closeLable(user.getClient(), labStore, labApplyNo);
    }


    /**
     * 申请 列表
     *
     * @param pageSize
     * @param pageNum
     * @param labApplyDate
     * @param labProduct
     * @param labSuatus
     * @param labStore
     * @return
     */
    @Override
    public PageInfo<GaiaLabelApplyDto> getLabelList(Integer pageSize, Integer pageNum, String labApplyDate, String labProduct, String labSuatus, String labStore) {
        // 当前登录人
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(pageNum, pageSize);
        // 申请查询
        List<GaiaLabelApplyDto> list = gaiaLabelApplyMapper.getLabelList(user.getClient(), labApplyDate, labProduct, labSuatus, labStore, null, null, null, null, user.getUserId());
        PageInfo<GaiaLabelApplyDto> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * 申请 明细
     *
     * @param labStore
     * @param labApplyNo
     * @return
     */
    @Override
    public Result getLabelItemList(String labStore, String labApplyNo) {
        // 当前登录人
        TokenUser user = feignService.getLoginInfo();
        List<GaiaLabelApplyDto> list = gaiaLabelApplyMapper.getLabelItemList(user.getClient(), labStore, labApplyNo);
        return ResultUtil.success(list);
    }

    /**
     * 申请列表选择
     *
     * @param labStore
     * @param labApplyNo
     * @param labApplyDateStart
     * @param labApplyDateEnd
     * @return
     */
    @Override
    public Result selectLabelList(Integer pageSize, Integer pageNum, String labStore, String labApplyNo, String labApplyDateStart, String labApplyDateEnd, Integer labPrintTimes) {
        // 当前登录人
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(pageNum, pageSize);
        // 申请查询
        List<GaiaLabelApplyDto> list = gaiaLabelApplyMapper.getLabelList(user.getClient(), null, null, null,
                labStore, labApplyNo, labApplyDateStart, labApplyDateEnd, labPrintTimes, null);
        PageInfo<GaiaLabelApplyDto> pageInfo = new PageInfo<>(list);

        if (CollectionUtils.isNotEmpty(pageInfo.getList())) {
            for (GaiaLabelApplyDto item : pageInfo.getList()) {
                List<String> itemList = gaiaLabelApplyMapper.getLabelItems(user.getClient(), item.getLabStore(), item.getLabApplyNo());
                if (CollectionUtils.isNotEmpty(itemList)) {
                    item.setProCodeStr(String.join(",", itemList));
                }
            }
        }
        return ResultUtil.success(pageInfo);
    }

    /**
     * 明细 导出
     *
     * @param labStore
     * @param labApplyNo
     * @return
     */
    @Override
    public Result labelItemExport(String labStore, String labApplyNo) {
        try {
            // 当前登录人
            TokenUser user = feignService.getLoginInfo();
            List<GaiaLabelApplyDto> list = gaiaLabelApplyMapper.getLabelItemList(user.getClient(), labStore, labApplyNo);

            List<List<String>> dataList = new ArrayList<>();
            // 配送平均天数  默认为 3天
            for (GaiaLabelApplyDto dto : list) {
                // 打印行
                List<String> item = new ArrayList<>();
                // 商品编码
                item.add(dto.getLabProduct());
                // 商品名称
                item.add(dto.getProName());
                // 通用名称
                item.add(dto.getProCommonname());
                // 规格
                item.add(dto.getProSpecs());
                // 生产厂家
                item.add(dto.getProFactoryName());
                // 单位
                item.add(dto.getProUnit());
                // 产地
                item.add(dto.getProPlace());
                // 申请份数
                item.add(dto.getLabApplyQty());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("价签申请");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (
                IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }


    }

    /**
     * 申请 删除
     *
     * @param labStore
     * @param labApplyNo
     */
    @Override
    public void delLabel(String labStore, String labApplyNo) {
        // 当前登录人
        TokenUser user = feignService.getLoginInfo();
        gaiaLabelApplyMapper.delLabel(user.getClient(), labStore, labApplyNo);
    }

    /**
     * 有权限的门店列表
     *
     * @return
     */
    @Override
    public List<GaiaStoreData> getLabelStoList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaLabelApplyMapper.getLabelStoList(user.getClient(), user.getUserId());
    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "商品编码",
            "商品名称",
            "通用名称",
            "规格",
            "生产厂家",
            "单位",
            "产地",
            "申请份数",
    };
}
