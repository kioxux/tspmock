package com.gov.purchase.module.base.controller;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.26
 */

import com.gov.purchase.common.entity.Tencent;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.base.service.BaseService;
import com.gov.purchase.utils.JsonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Base64;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.18
 */
@Api(tags = "共通接口")
@RestController
@RequestMapping("base")
public class BaseController {

    @Resource
    BaseService baseService;

    @Resource
    Tencent tencent;

    /**
     * 加盟商列表
     *
     * @return
     */
    @Log("加盟商列表")
    @ApiOperation("加盟商列表")
    @GetMapping("getFranchiseeList")
    public Result getFranchiseeList() {
        return ResultUtil.success(baseService.getFranchiseeList());
    }

    @Log("所有加盟商列表")
    @ApiOperation("所有加盟商列表")
    @GetMapping("getAllFranchiseeList")
    public Result getAllFranchiseeList() {
        return ResultUtil.success(baseService.getAllFranchiseeList());
    }

    /**
     * 实体集合
     *
     * @param client 加盟商
     * @param type   类型 1:门店 2:供应商 3:客户
     * @return
     */
    @Log("实体集合")
    @ApiOperation("实体集合")
    @PostMapping("mainStore")
    public Result mainStore(@RequestJson("client") String client, @RequestJson("type") String type) {
        return baseService.mainStore(client, type);
    }

    /**
     * 配送中心列表
     *
     * @param client 加盟商
     * @return
     */
    @Log("配送中心列表")
    @ApiOperation("配送中心列表")
    @GetMapping("getDcDataList")
    public Result getDcDataList(@RequestParam("client") String client) {
        return baseService.getDcDataList(client);
    }

    /**
     * 配送中心列表
     *
     * @param client 加盟商
     * @return
     */
    @Log("配送中心列表_权限")
    @ApiOperation("配送中心列表_权限")
    @GetMapping("getAuthDcDataList")
    public Result getAuthDcDataList(@RequestParam("client") String client) {
        return baseService.getAuthDcDataList(client);
    }

    /**
     * 委托配送公司列表
     *
     * @return
     */
    @Log("委托配送公司列表")
    @ApiOperation("委托配送公司列表")
    @GetMapping("getDeliveryCompanyList")
    public Result getDeliveryCompanyList() {
        return baseService.getDeliveryCompanyList();
    }

    /**
     * DC补货集合
     *
     * @return
     */
    @Log("DC补货集合")
    @ApiOperation("DC补货集合")
    @GetMapping("getReplenishDcDataList")
    public Result getReplenishDcDataList() {
        return baseService.getReplenishDcDataList();
    }

    /**
     * 连锁公司总部列表
     *
     * @param client 加盟商
     * @return
     */
    @Log("连锁公司总部列表")
    @ApiOperation("连锁公司总部列表")
    @GetMapping("getCompadmList")
    public Result getCompadmList(@RequestParam("client") String client) {
        return baseService.getCompadmList(client);
    }

    /**
     * 连锁公司总部列表DC用 需要排除已使用的连锁公司
     *
     * @param client 加盟商
     * @return
     */
    @Log("连锁公司总部列表")
    @ApiOperation("连锁公司总部列表")
    @GetMapping("getCompadmListDC")
    public Result getCompadmListDC(@RequestParam("client") String client, @RequestParam(value = "dcCode", required = false) String dcCode) {
        return baseService.getCompadmListDC(client, dcCode);
    }

    /**
     * 连锁公司总部和批发公司
     *
     * @param client 加盟商
     * @return
     */
    @Log("连锁公司总部列表")
    @ApiOperation("连锁公司总部列表")
    @GetMapping("getCompadmANDCompadmWms")
    public Result getCompadmANDCompadmWms(@RequestParam("client") String client, @RequestParam(value = "dcCode", required = false) String dcCode) {
        return baseService.getCompadmAndCompadmWms(client, dcCode);
    }

    @Log("获取当前加盟商用户")
    @ApiOperation("获取当前加盟商用户")
    @PostMapping("getUserListByClient")
    public Result getUserListByClient() {
        return baseService.getUserListByClient();
    }

    @Log("获取cos配置")
    @ApiOperation("获取cos配置")
    @PostMapping("getCosConfig")
    public Result getCosConfig() {
        byte[] bytes = JsonUtils.beanToJson(tencent).getBytes();
        //Base64 加密
        String encoded = Base64.getEncoder().encodeToString(bytes);
        return ResultUtil.success(encoded);
    }
}

