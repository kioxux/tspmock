package com.gov.purchase.module.customer.dto;

import com.gov.purchase.entity.GaiaCustomerBusiness;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021/4/26 10:45
 */
@Data
public class EditCusVO {
    @NotNull(message = "客户数据不能为空")
    private GaiaCustomerBusiness business;
//    @NotBlank(message = "备注")
    private String remarks;
}
