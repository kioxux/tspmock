package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaBatchInfo;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.09.13
 */
@Data
public class BatchInfoDto extends GaiaBatchInfo {

    /**
     * 含税金额
     */
    private BigDecimal totalAmt;

    /**
     * 供应商名
     */
    private String supName;

    /**
     * 入库单号
     */
    private String batSourceNo;
}

