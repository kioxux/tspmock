package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class SavePayingItemRequestDTO {

    @NotBlank(message = "门店编码不能为空")
    private String ficoCompanyCode;

    private String ficoCompanyName;

    @NotBlank(message = "付款主体编码不能为空")
    private String ficoPayingstoreCode;

    @NotBlank(message = "付款主体名称不能为空")
    private String ficoPayingstoreName;

    @NotBlank(message = "付款方式不能为空")
    private String ficoPaymentMethod;

    @NotNull(message = "付款金额不能为空")
    private Integer ficoPaymentAmount;

    //    @NotBlank(message = "上期截止日期不能为空")
    private String ficoLastDeadline;

    @NotBlank(message = "本期开始日期不能为空")
    private String ficoStartingTime;

    @NotBlank(message = "本期截止日期不能为空")
    private String ficoEndingTime;
}
