package com.gov.purchase.module.delivery.dto;

import lombok.Data;

@Data
public class StoreVO {
    /**
     * 值
     */
    private String value;

    /**
     * 表示
     */
    private String label;
    /**
     * 助记码
     */
    private String stoPym;
    private String id;
}
