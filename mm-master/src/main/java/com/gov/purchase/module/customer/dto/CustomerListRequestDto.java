package com.gov.purchase.module.customer.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "客户维护列表页面请求体")
public class CustomerListRequestDto extends Pageable {

    @ApiModelProperty(value = "加盟商编号", name = "client", hidden = true)
    private String client;

    @ApiModelProperty(value = "客户自编号", name = "cusSelfCode")
    private String cusSelfCode;

    @ApiModelProperty(value = "客户名称", name = "cusName")
    private  String cusName;

    @ApiModelProperty(value = "统一社会信用代码", name = "cusCreditCode")
    private String cusCreditCode;

    @ApiModelProperty(value = "状态 “正常”、“停用“", name = "cusStatus")
    private  String cusStatus;

    @ApiModelProperty(value = "地点", name = "proSite")
    private String cusSite;

}
