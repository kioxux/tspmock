package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductBasic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@EqualsAndHashCode
public class SaveProListRequestDTO {
    @NotEmpty(message = "商品列表不能为空")
    private List<GaiaProductBasic> basicsList;


    @NotEmpty(message = "商品列表不能为空")
    private List<GaiaProductBasic> compBasicsList;



}
