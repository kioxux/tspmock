package com.gov.purchase.module.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class WechatArticles {

    private String title;

    private String description;

    private String url;

    private String picurl;
}
