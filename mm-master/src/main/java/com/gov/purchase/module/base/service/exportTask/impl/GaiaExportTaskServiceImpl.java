package com.gov.purchase.module.base.service.exportTask.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaExportTask;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaExportTaskMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.exportTask.GaiaExportTaskService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.12.23
 */
@Service
public class GaiaExportTaskServiceImpl implements GaiaExportTaskService {

    @Resource
    private GaiaExportTaskMapper gaiaExportTaskMapper;
    @Resource
    private FeignService feignService;

    /**
     * 导出任务列表
     *
     * @param pageSize
     * @param pageNum
     * @param getStatus
     * @param getType
     * @param getDateStart
     * @param getDateEnd
     * @return
     */
    @Override
    public Result getExportTaskList(Integer pageSize, Integer pageNum, Integer getStatus, Integer getType, String getDateStart, String getDateEnd) {
        TokenUser user = feignService.getLoginInfo();
        // 分頁查詢
        PageHelper.startPage(pageNum, pageSize);
        List<GaiaExportTask> list = gaiaExportTaskMapper.getExportTaskList(user.getClient(), user.getUserId(), getStatus, getType, getDateStart, getDateEnd);
        return ResultUtil.success(new PageInfo<GaiaExportTask>(list));
    }

    /**
     * 生成导出记录
     *
     * @param getStatus
     * @param getType
     * @return
     */
    @Override
    public GaiaExportTask initExportTask(Integer getStatus, Integer getType) {
        TokenUser user = feignService.getLoginInfo();
        GaiaExportTask gaiaExportTask = new GaiaExportTask();
        gaiaExportTask.setClient(user.getClient());
        gaiaExportTask.setGetUser(user.getUserId());
        gaiaExportTask.setGetStatus(getStatus);
        gaiaExportTask.setGetType(getType);
        gaiaExportTask.setGetDate(new Date());
        gaiaExportTaskMapper.insertSelective(gaiaExportTask);
        return gaiaExportTask;
    }

    /**
     * 删除导出记录
     *
     * @param ids
     */
    @Override
    public void delExportTaskList(List<Integer> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }
        gaiaExportTaskMapper.delExportTaskList(ids);
    }
}
