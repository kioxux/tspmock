package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaCustomerBusiness;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GaiaCustomerBusinessDTO extends GaiaCustomerBusiness {
    /**
     * 唯一值（前端使用）
     */
    private String id;
}
