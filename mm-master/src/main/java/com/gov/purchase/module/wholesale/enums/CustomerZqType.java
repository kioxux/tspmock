package com.gov.purchase.module.wholesale.enums;

/**
 * @desc: 客户账期类型枚举
 * @author: Ryan
 * @createTime: 2021/12/20 23:32
 */
public enum CustomerZqType {

    ZQ_DAYS("0", "账期天数"),
    ZQ_FIXED("1", "固定账期");

    public final String type;
    public final String name;

    CustomerZqType(String type, String name) {
        this.type = type;
        this.name = name;
    }
}
