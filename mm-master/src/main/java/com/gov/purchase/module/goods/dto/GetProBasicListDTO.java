package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductBusiness;
import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021-06-11 14:48
 */
@Data
public class GetProBasicListDTO extends GaiaProductBusiness {
    /**
     * 是否经营
     */
    private Integer operating;

    /**
     * 已经匹配
     */
    private String otherProSelfCode;
}
