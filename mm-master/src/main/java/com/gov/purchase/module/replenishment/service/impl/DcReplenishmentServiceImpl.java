package com.gov.purchase.module.replenishment.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.BusinessException;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.DictionaryParams;
import com.gov.purchase.module.base.dto.feign.PurchaseFeignDto;
import com.gov.purchase.module.base.service.DictionaryService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.replenishment.dto.*;
import com.gov.purchase.module.replenishment.service.DcReplenishmentService;
import com.gov.purchase.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DcReplenishmentServiceImpl implements DcReplenishmentService {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaSourceListMapper gaiaSourceListMapper;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    GaiaPoHeaderMapper gaiaPoHeaderMapper;
    @Resource
    GaiaApproveRuleMapper gaiaApproveRuleMapper;
    @Resource
    GaiaPoItemMapper gaiaPoItemMapper;
    @Resource
    GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Resource
    GaiaSoItemMapper gaiaSoItemMapper;
    @Resource
    GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Resource
    GaiaMaterialAssessMapper gaiaMaterialAssessMapper;
    @Resource
    CosUtils cosUtils;
    @Resource
    DictionaryService dictionaryService;
    @Resource
    private GaiaBhCsMapper gaiaBhCsMapper;
    @Resource
    private GaiaProductClassMapper gaiaProductClassMapper;
    @Resource
    private BusinessScopeServiceMapper businessScopeServiceMapper;
    @Resource
    private GaiaDcSeperateConfiMapper gaiaDcSeperateConfiMapper;
    @Resource
    private RedisClient redisClient;
    @Autowired
    SalesmanMaintainProMapper salesmanMaintainProMapper;

    /**
     * DC补货列表
     *
     * @return
     */
    @Override
    public PageInfo queryReplenishmentList(String dcCode, Integer pageNum, Integer pageSize) {
//    public List<DcReplenishmentListResponseDto> queryReplenishmentList(String dcCode) {
        TokenUser user = feignService.getLoginInfo();
        Map<String, Object> map = new HashMap<>();
        map.put("dcCode", dcCode);  // 获取当前人的 dc编号
        map.put("client", user.getClient());
        // 根据当前人的配送中心 查询DC补货列表
        pageNum = 0;
        pageSize = 0;
        PageHelper.startPage(pageNum, pageSize);
        GaiaBhCsKey gaiaBhCsKey = new GaiaBhCsKey();
        gaiaBhCsKey.setBghSite(dcCode);
        gaiaBhCsKey.setClient(user.getClient());
        GaiaBhCs gaiaBhCs = gaiaBhCsMapper.selectByPrimaryKey(gaiaBhCsKey);
        if (gaiaBhCs != null && gaiaBhCs.getBghSqxsts() != null) {
            map.put("bghSqxsts", gaiaBhCs.getBghSqxsts());
        }
        List<DcReplenishmentListResponseDto> dcReplenishmentListResponseDtoList = gaiaDcDataMapper.queryReplenishmentList(map);
        PageInfo pageInfo = new PageInfo<>(dcReplenishmentListResponseDtoList);
        List<DcReplenishmentListResponseDto> result = pageInfo.getList();
        if (CollectionUtils.isNotEmpty(result)) {

            // 配送平均天数  默认为 3天
            Integer dcDeliveryDays = 3;
            for (int i = result.size() - 1; i >= 0; i--) {
                DcReplenishmentListResponseDto dto = result.get(i);
                dto.setProName(dto.getProDepict());
//            // 库存
//            GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
//            // 加盟商
//            gaiaMaterialAssessKey.setClient(dto.getClient());
//            // 商品编码
//            gaiaMaterialAssessKey.setMatProCode(dto.getProSelfCode());
//            // 估价地点
//            gaiaMaterialAssessKey.setMatAssessSite(dto.getDcChainHead());
//            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
//            if (gaiaMaterialAssess == null) {
//                dto.setStoreStockTotal(BigDecimal.ZERO);
//            } else {
//                dto.setStoreStockTotal(gaiaMaterialAssess.getMatTotalQty());
//            }
//            // 总库存
//            dto.setStockTotal(dto.getDcStockTotal().add(dto.getStoreStockTotal()));
                // 门店数量
//            Integer storeCount = gaiaDcDataMapper.queryStoreCountByChainHead(dto.getClient(), dto.getDcChainHead());
//            if (storeCount == null) {
//                dto.setStoreCount(0);
//            } else {
//                dto.setStoreCount(storeCount);
//            }
                // 30天门店销量
//            BigDecimal salesCount = gaiaDcDataMapper.query30DaysSalesCount(dto.getClient(), dto.getDcChainHead(), dto.getProSelfCode());
//            if (salesCount == null) {
//                dto.setDaysSalesCount(BigDecimal.ZERO);
//            } else {
//                dto.setDaysSalesCount(salesCount);
//            }
                // 补货系数
                BigDecimal ratio = new BigDecimal(1);
                //  预计到货日期  = 当前日期 + 供应商的前置期queryStoreCountByChainHead
                if (dto.getSupLeadTime() != null) {
                    dto.setPoDeliveryDate(DateUtils.formatLocalDate(LocalDate.now().plusDays(dto.getSupLeadTime()), "yyyyMMdd"));
                    // Q <= 7
                    if (dto.getSupLeadTime() <= 7) {
                        ratio = getReplenishmentRatio(dto, new BigDecimal(1), new BigDecimal("0.8"), new BigDecimal("0.6"), new BigDecimal("0.5"), new BigDecimal("0.5"));
                        // 7 < Q <= 15
                    } else if (dto.getSupLeadTime() > 7 && dto.getSupLeadTime() <= 15) {
                        ratio = getReplenishmentRatio(dto, new BigDecimal("1.5"), new BigDecimal(1), new BigDecimal(1), new BigDecimal(1), new BigDecimal("0.8"));
                    } else {
                        // Q > 15
                        ratio = getReplenishmentRatio(dto, new BigDecimal(2), new BigDecimal(2), new BigDecimal("1.5"), new BigDecimal("1.2"), new BigDecimal(1));
                    }
                } else {
                    ratio = getReplenishmentRatio(dto, new BigDecimal(1), new BigDecimal("0.8"), new BigDecimal("0.6"), new BigDecimal("0.5"), new BigDecimal("0.5"));
                }
                dto.setReplenishmentRatio(ratio);
                /**
                 * （中包装从商品主数据中取值，若为空则默认是1）
                 */
                if (StringUtils.isBlank(dto.getProMidPackage())) {
                    dto.setProMidPackage("1");
                }

                /**
                 * 2021-1-14 变更：中药/非中药、 30/90天销量  剔除数据
                 */
                if (gaiaBhCs != null) {
                    if (dto.getProClass().startsWith("301") || dto.getProClass().startsWith("302")) {  // 中药
                        // 30天有销量
                        if (gaiaBhCs.getBghZy30() != null) {
                            if (dto.getDaysSalesCount() == null) {
                                result.remove(i);
                                continue;
                            } else if (gaiaBhCs.getBghZy30().compareTo(dto.getDaysSalesCount()) > 0) {
                                result.remove(i);
                                continue;
                            }
                        }
                        // 90天销量
                        if (gaiaBhCs.getBghZy90() != null) {
                            if (dto.getDaysSalesCountNinety() == null || dto.getDaysSalesCountNinety().compareTo(BigDecimal.ZERO) == 0) {
                                result.remove(i);
                                continue;
                            } else if (gaiaBhCs.getBghZy90().compareTo(dto.getDaysSalesCountNinety()) > 0) {
                                result.remove(i);
                                continue;
                            }
                        }

                    } else {  // 非中药
                        // 30天有销量
                        if (gaiaBhCs.getBghFzy30() != null) {
                            if (dto.getDaysSalesCount() == null) {
                                result.remove(i);
                                continue;
                            } else if (gaiaBhCs.getBghFzy30().compareTo(dto.getDaysSalesCount()) > 0) {
                                result.remove(i);
                                continue;
                            }
                        }
                        // 90天销量
                        if (gaiaBhCs.getBghFzy90() != null) {
                            if (dto.getDaysSalesCountNinety() == null || dto.getDaysSalesCountNinety().compareTo(BigDecimal.ZERO) == 0) {
                                result.remove(i);
                                continue;
                            } else if (gaiaBhCs.getBghFzy90().compareTo(dto.getDaysSalesCountNinety()) > 0) {
                                result.remove(i);
                                continue;
                            }
                        }

                    }
                }

                // 30 为空 90 有销量 = 门店数据
                if ((dto.getDaysSalesCount() == null || dto.getDaysSalesCount().compareTo(BigDecimal.ZERO) == 0) &&
                        (dto.getDaysSalesCountNinety() != null && dto.getDaysSalesCountNinety().compareTo(BigDecimal.ZERO) != 0)) {

                    /**
                     * 2021-1-13 变更： 与大仓库存做减法（补货门店数 N - 大仓库存）  > 0  则补货量 = 差值;  <= 剔除数据;
                     */
                    if (dto.getDcStockTotal() != null) {
                        BigDecimal diff = new BigDecimal(dto.getStoreCount()).subtract(dto.getDcStockTotal());
                        if (diff.compareTo(BigDecimal.ZERO) > 0) {
                            dto.setReplenishmentValue(diff);
                            dto.setProposalReplenishment(diff);
                        } else {
                            result.remove(i);
                            continue;
                        }
                    } else {
                        dto.setReplenishmentValue(new BigDecimal(dto.getStoreCount()));
                        dto.setProposalReplenishment(new BigDecimal(dto.getStoreCount()));
                    }

                } else if ((dto.getDaysSalesCount() == null || dto.getDaysSalesCount().compareTo(BigDecimal.ZERO) == 0) &&
                        (dto.getDaysSalesCountNinety() == null || dto.getDaysSalesCountNinety().compareTo(BigDecimal.ZERO) == 0)) {
                    // 90 为空 = 0
                    dto.setReplenishmentValue(BigDecimal.ZERO);
                    dto.setProposalReplenishment(BigDecimal.ZERO);
                } else {
                    if (dto.getDcDeliveryDays() != null) {
                        dcDeliveryDays = dto.getDcDeliveryDays();
                    }
                    // 最终补货数量 = X ( S + M / 30 ) - T
                    BigDecimal replenishmentValue = new BigDecimal(0);
                    replenishmentValue = new BigDecimal(dcDeliveryDays).divide(new BigDecimal(30), 1, RoundingMode.HALF_UP);
                    if (dto.getReplenishmentRatio() != null) {
                        replenishmentValue = replenishmentValue.add(dto.getReplenishmentRatio());
                    }
                    replenishmentValue = replenishmentValue.multiply(dto.getDaysSalesCount());
                    // T = 在途量 + 大仓库存
                    BigDecimal t = dto.getTrafficCount().add(dto.getDcStockTotal());
                    replenishmentValue = replenishmentValue.subtract(t);

                    /**
                     * 1. 非中药（商品分类不为3开头）
                     * 二舍八入原则：
                     * 即补货量/中包量 余数<2 向下取整； >=8向上取整；介于中间的就不变。
                     * 例：A商品中包装量10，补货量算出为21，21/10=2.1，余数是1，小于2，则向下取整，最终建议补货量为20。又若补货量算出为28，28/10=2.8，余数是8，向上取整，即补30个。
                     * 2. 中药（商品分类为3开头）
                     * 四舍五入原则：
                     * 即补货量/中包量 余数<5向下取整； >=5向上取整
                     */
                    BigDecimal[] remainArr = replenishmentValue.divideAndRemainder(new BigDecimal(dto.getProMidPackage()));
                    //    BigDecimal[] replenishmentRemainArr = replenishmentValue.divideAndRemainder(new BigDecimal(10));
                    BigDecimal newValue = remainArr[0].multiply(new BigDecimal(dto.getProMidPackage()));
//                    log.info("商品：{}", JSONObject.toJSONString(dto));
                    if (StringUtils.isNotBlank(dto.getProClass()) && (dto.getProClass().startsWith("301") || dto.getProClass().startsWith("302"))) {  // 中药
                        if (remainArr[1].compareTo(new BigDecimal("5")) < 0) {
//                            dto.setReplenishmentValue(newValue);
                            dto.setProposalReplenishment(newValue);
                        } else {
                            newValue = newValue.add(new BigDecimal(dto.getProMidPackage()));
//                            dto.setReplenishmentValue(newValue);
                            dto.setProposalReplenishment(newValue);
                        }
                    } else {  // 非中药
                        if (remainArr[1].compareTo(new BigDecimal("2")) < 0) {
//                            dto.setReplenishmentValue(newValue);
                            dto.setProposalReplenishment(newValue);
                        } else if (remainArr[1].compareTo(new BigDecimal("8")) >= 0) {
                            newValue = newValue.add(new BigDecimal(dto.getProMidPackage()));
//                            dto.setReplenishmentValue(newValue);
                            dto.setProposalReplenishment(newValue);
                        } else {
//                            dto.setReplenishmentValue(replenishmentValue);
                            dto.setProposalReplenishment(newValue);
                        }
                    }

                    // 获取建议补货量
                    // 中药
                    if (StringUtils.isNotBlank(dto.getProClass()) && (dto.getProClass().startsWith("301") || dto.getProClass().startsWith("302"))) {
                        dto.setReplenishmentValue(
                                dto.getProposalReplenishment().divide(new BigDecimal(dto.getProMidPackage()), 0, RoundingMode.CEILING).multiply(new BigDecimal(dto.getProMidPackage()))
                        );
                    } else {
                        // 建议补货 / 中包装量 四舍五入 * 中包装量
                        dto.setReplenishmentValue(
                                dto.getProposalReplenishment().divide(new BigDecimal(dto.getProMidPackage()), 0, RoundingMode.HALF_UP).multiply(new BigDecimal(dto.getProMidPackage()))
                        );
                    }

                }

                if (dto.getReplenishmentValue() == null || dto.getReplenishmentValue().compareTo(BigDecimal.ZERO) <= 0) {
                    result.remove(i);
                    continue;
                }

                // 补货量 < 补货门店数  / 最小补货系数 则剔除数据
                if (gaiaBhCs != null) {
                    if (dto.getProposalReplenishment()
                            .compareTo(
                                    new BigDecimal(dto.getStoreCount())
                                            .divide(gaiaBhCs.getBghZxbhxs(), 0, RoundingMode.HALF_UP)
                            ) < 0) {
                        result.remove(i);
                        continue;
                    }
                }

                // 建议补货量 < 补货门店数  / 最小补货系数 则剔除数据
                if (gaiaBhCs != null) {
                    if (dto.getReplenishmentValue()
                            .compareTo(
                                    new BigDecimal(dto.getStoreCount())
                                            .divide(gaiaBhCs.getBghZxbhxs(), 0, RoundingMode.HALF_UP)
                            ) < 0) {
                        result.remove(i);
                        continue;
                    }
                }

                // 小于舍弃销售天数据的销量，不补货
                if (gaiaBhCs != null && gaiaBhCs.getBghSqxsts() != null && dto.getDaysSalesCountNinetyBghSqxsts() != null) {
                    if (dto.getDaysSalesCountNinetyBghSqxsts().compareTo(dto.getReplenishmentValue()) > 0) {
                        result.remove(i);
                        continue;
                    }
                }

                // 供应商下拉框
                if (pageNum != null && pageNum.intValue() != 0 && pageSize != null && pageSize.intValue() != 0) {
                    SupplierRequestDto supplierRequestDto = new SupplierRequestDto();
                    supplierRequestDto.setClient(dto.getClient());
                    supplierRequestDto.setSiteCode(dto.getDcCode());
                    supplierRequestDto.setProSelfCode(dto.getProSelfCode());
//                    List<SupplierResponseDto> supplierResponseDtoList = querySupplierList(supplierRequestDto);
//                    dto.setSupplierResponseDtoList(supplierResponseDtoList);
                }
            }
        }
        return pageInfo;
    }

    /**
     * 历史供应商列表
     *
     * @param dto
     * @return
     */
    @Override
    public List<SupplierResponseDto> querySupplierHistoryList(SupplierHistoryRequestDto dto) {
        return gaiaSourceListMapper.querySupplierHistoryList(dto);
    }

    /**
     * 选择供应商列表
     *
     * @param dto
     * @return
     */
    @Override
    public List<SupplierResponseDto> querySupplierList(SupplierRequestDto dto) {
        return gaiaSupplierBusinessMapper.querySupplierList(dto);
    }

    /**
     * 保存DC补货信息
     *
     * @param list
     * @return
     */
    @Override
    @Transactional
    public Result saveDcReplenishmentInfo(List<DcReplenishmentRequestDto> list, String... params) {
        TokenUser user = feignService.getLoginInfo();
        /**
         * 1.数据验证
         */
        if (list.size() == 0) {
            throw new CustomResultException(ResultEnum.DC_REPLENISHMENT_EMPTY);
        }
        for (DcReplenishmentRequestDto dto : list) {
            ValidationResult result = ValidateUtil.validateEntity(dto);
            // 对于选择了业务员并且该业务员的 isControl = 1 的数据进行校验
            if (StringUtils.isNotEmpty(dto.getPoSupplierSalesman())) {
                String isControl = salesmanMaintainProMapper.queryIsControl(user.getClient(), dto.getDcCode(), dto.getPoSupplierId(), dto.getPoSupplierSalesman());
                if (StringUtils.isEmpty(isControl)) {
                    throw new BusinessException("不存在该业务员");
                }
                if (StringUtils.equals(isControl, "1")) {
                    // 校验是否已经授权该商品
                    Integer count = salesmanMaintainProMapper.queryCountProByGssCode(user.getClient(), dto.getDcCode(), dto.getPoSupplierId(), dto.getPoSupplierSalesman(), dto.getPoProCode());
                    if (count < 1) {
                        throw new BusinessException("存在未授权的商品");
                    }
                }
            }
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
        }

        List<String> supCodes = new ArrayList<>();
        List<String> proCodes = new ArrayList<>();
        String client = "";
        String dcCode = "";
        for (DcReplenishmentRequestDto row : list) {
            client = row.getClient();
            dcCode = row.getDcCode();
            if (StringUtils.isNotBlank(row.getPoSupplierId())) {
                supCodes.add(row.getPoSupplierId());
            }
            proCodes.add(row.getPoProCode());
        }
        List<String> warning = new ArrayList<>();
        List<String> error = new ArrayList<>();
        // 供应商效期验证
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(client);
        gaiaDcDataKey.setDcCode(dcCode);
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
        Map<String, GaiaSupplierBusiness> supplierBusinessMap = new HashMap<>();
        Map<String, List<String>> supScopeMap = new HashMap<>();
        Map<String, List<String>> class3Map = new HashMap<>();
        for (DcReplenishmentRequestDto row : list) {
            // 是否管控证照效期：1-是
            if (StringUtils.isNotBlank(row.getPoSupplierSalesman()) && gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1")) {
                String wtsxq = salesmanMaintainProMapper.queryGssWtsxq(client, row.getDcCode(), row.getPoSupplierId(), row.getPoSupplierSalesman());
                if (StringUtils.isNotBlank(wtsxq) && wtsxq.trim().length() == 8) {
                    // 业务员的委托书有效期
                    String day = DateUtils.getCurrentDateStr("yyyyMMdd");
                    // 异常
                    if (day.compareTo(wtsxq) > 0) {
                        error.add("业务员授权委托书效期已过期");
                    }
                }
            }
            // 效期验证
            supplierCheck(gaiaDcData, client, row.getDcCode(), row.getPoSupplierId(), supplierBusinessMap, warning, error);
            // 经营范围验证
            if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcJyfwgk()) && (gaiaDcData.getDcJyfwgk().equals("1") || "2".equals(gaiaDcData.getDcJyfwgk()))) {
                // 供应商经营范围
                List<String> supScopeList = supScopeMap.get(row.getPoSupplierId());
                if (supScopeList == null) {
                    supScopeList = businessScopeServiceMapper.getsupScopeList(client, row.getPoSupplierId(), 1, list.get(0).getDcCode());
                    if (supScopeList == null) {
                        supScopeList = new ArrayList<>();
                    }
                    supScopeMap.put(row.getPoSupplierId(), supScopeList);
                }
                if ("2".equals(gaiaDcData.getDcJyfwgk())) {
                    List<String> class3List = class3Map.get(row.getPoSupplierId());
                    if (class3List == null) {
                        class3List = businessScopeServiceMapper.getsupScopeListClass3(client, row.getPoSupplierId(), 1, list.get(0).getDcCode());
                        if (class3List == null) {
                            class3List = new ArrayList<>();
                        }
                        class3Map.put(row.getPoSupplierId(), class3List);
                    }
                    if (CollectionUtils.isNotEmpty(class3List)) {
                        continue;
                    }
                }
                if (CollectionUtils.isEmpty(supScopeList)) {
                    throw new CustomResultException("商品" + row.getPoProCode() + "超出供应商经营范围");
                }
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                gaiaProductBusinessKey.setClient(client);
                gaiaProductBusinessKey.setProSite(list.get(0).getDcCode());
                gaiaProductBusinessKey.setProSelfCode(row.getPoProCode());
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                if (StringUtils.isBlank(gaiaProductBusiness.getProJylb())) {
                    throw new CustomResultException("商品" + row.getPoProCode() + "超出供应商经营范围");
                }
                if (Collections.disjoint(supScopeList, Arrays.asList(gaiaProductBusiness.getProJylb().split(",")))) {
                    throw new CustomResultException("商品" + row.getPoProCode() + "超出供应商经营范围");
                }
            }
        }

        //商品状态检查
        List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectProList(client, dcCode, proCodes);
        if (CollectionUtils.isNotEmpty(gaiaProductBusinessList)) {
            for (GaiaProductBusiness gaiaProductBusiness : gaiaProductBusinessList) {
                String day = DateUtils.getCurrentDateStr("yyyyMMdd");
                // 检查 商品主数据 注册证效期（批准文号有效期）：小于60天-提示，不报错；过期-报错。
                if (gaiaDcData != null) {
                    // 是否管控证照效期：1-是
                    if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1") &&
                            StringUtils.isNotBlank(gaiaProductBusiness.getProRegisterExdate()) && gaiaProductBusiness.getProRegisterExdate().trim().length() == 8) {
                        // 警告
                        Long datediff = DateUtils.datediff(day, gaiaProductBusiness.getProRegisterExdate());
                        if (datediff.intValue() >= 0 && datediff.intValue() <= 60) {
                            warning.add("商品" + gaiaProductBusiness.getProSelfCode() + "批准文号即将过期");
                        }
                        if (datediff.intValue() < 0) {
                            warning.add("商品" + gaiaProductBusiness.getProSelfCode() + "批准文号已过期");
                        }
                    }
                }

                // 参数表：GAIA_CL_SYSTEM_PARA增加参数 PO_PZWH_CHECK采购订单是否强控批准文号有效期；如果GSSP_PARA维护1：开
                String para1 = gaiaSdReplenishHMapper.getParamByClientAndGcspId(client, "PO_PZWH_CHECK");
                // 3.1 默认检查时给出过期提醒，但是可以正常保存。
                // 3.2 参数表：GAIA_CL_SYSTEM_PARA增加参数 PO_PZWH_CHECK采购订单是否强控批准文号有效期；如果GSSP_PARA维护1：开，则采购订单处直接报错控制不让保存。
                // 3.3 检查商品过期时，再增加检查一个字段 GAIA_PRODUCT_BUSINESS - PRO_SCJYXKZYXQ，过期强管控。
                // 批准文号有效期
                if (StringUtils.isNotBlank(gaiaProductBusiness.getProRegisterExdate()) && gaiaProductBusiness.getProRegisterExdate().trim().length() == 8) {
                    if (gaiaProductBusiness.getProRegisterExdate().compareTo(day) < 0) {
                        if ("1".equals(para1)) {
                            throw new CustomResultException("商品" + gaiaProductBusiness.getProSelfCode() + "批准文号已过期");
                        } else {
                            warning.add("商品" + gaiaProductBusiness.getProSelfCode() + "批准文号已过期");
                        }
                    }
                }
                // 生产经营许可证有效期
                if (StringUtils.isNotBlank(gaiaProductBusiness.getProScjyxkzyxq()) && gaiaProductBusiness.getProScjyxkzyxq().trim().length() == 8) {
                    if (gaiaProductBusiness.getProScjyxkzyxq().compareTo(day) < 0) {
                        if ("1".equals(para1)) {
                            throw new CustomResultException("商品" + gaiaProductBusiness.getProSelfCode() + "生产经营许可证已过期");
                        } else {
                            warning.add("商品" + gaiaProductBusiness.getProSelfCode() + "生产经营许可证已过期");
                        }
                    }
                }

                // 商品状态
//                if (CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(status.getProStatus())) {
//                    throw new CustomResultException(ResultEnum.DEACTIVATE_ERROR);
//                }
                //商品禁采检查
                if (CommonEnum.NoYesStatus.YES.getCode().equals(gaiaProductBusiness.getProNoPurchase())) {
                    throw new CustomResultException(MessageFormat.format(ResultEnum.PURCHASE_NOT_VALID.getMsg(), gaiaProductBusiness.getProSelfCode()));
                }
                //商品禁退检查
//                if (CommonEnum.NoYesStatus.YES.getCode().equals(gaiaProductBusiness.getProNoSupplier())) {
//                    throw new CustomResultException(ResultEnum.CUSPLIER_NOT_VALID);
//                }
            }
        }
        if (CollectionUtils.isNotEmpty(error)) {
            Result result = ResultUtil.error(ResultEnum.E0015);
            result.setWarning(error);
            return result;
        }

        /**
         * 将数据按照按照供应商分组
         */
        Map<String, List<DcReplenishmentRequestDto>> mapList = null;
        GaiaDcSeperateConfiKey gaiaDcSeperateConfiKey = new GaiaDcSeperateConfiKey();
        gaiaDcSeperateConfiKey.setClient(client);
        gaiaDcSeperateConfiKey.setGdscSite(dcCode);
        GaiaDcSeperateConfi gaiaDcSeperateConfi = gaiaDcSeperateConfiMapper.selectByPrimaryKey(gaiaDcSeperateConfiKey);
        if (gaiaDcSeperateConfi != null) {
            if ("PRO_ZDY1".equals(gaiaDcSeperateConfi.getGdscField())) {
                mapList = list.stream().collect(Collectors.groupingBy(
                        o -> o.getPoSupplierId() + "_*_" + (StringUtils.isBlank(o.getProZdy1()) ? "" : o.getProZdy1()) + "_*_" + (StringUtils.isBlank(o.getPoSupplierSalesman()) ? "" : o.getPoSupplierSalesman()) + "_*_" + (StringUtils.isBlank(o.getPoOwner()) ? "" : o.getPoOwner())
                ));
            } else if ("PRO_ZDY2".equals(gaiaDcSeperateConfi.getGdscField())) {
                mapList = list.stream().collect(Collectors.groupingBy(
                        o -> o.getPoSupplierId() + "_*_" + (StringUtils.isBlank(o.getProZdy2()) ? "" : o.getProZdy2()) + "_*_" + (StringUtils.isBlank(o.getPoSupplierSalesman()) ? "" : o.getPoSupplierSalesman()) + "_*_" + (StringUtils.isBlank(o.getPoOwner()) ? "" : o.getPoOwner())
                ));
            } else if ("PRO_ZDY3".equals(gaiaDcSeperateConfi.getGdscField())) {
                mapList = list.stream().collect(Collectors.groupingBy(
                        o -> o.getPoSupplierId() + "_*_" + (StringUtils.isBlank(o.getProZdy3()) ? "" : o.getProZdy3()) + "_*_" + (StringUtils.isBlank(o.getPoSupplierSalesman()) ? "" : o.getPoSupplierSalesman()) + "_*_" + (StringUtils.isBlank(o.getPoOwner()) ? "" : o.getPoOwner())
                ));
            } else if ("PRO_ZDY4".equals(gaiaDcSeperateConfi.getGdscField())) {
                mapList = list.stream().collect(Collectors.groupingBy(
                        o -> o.getPoSupplierId() + "_*_" + (StringUtils.isBlank(o.getProZdy4()) ? "" : o.getProZdy4()) + "_*_" + (StringUtils.isBlank(o.getPoSupplierSalesman()) ? "" : o.getPoSupplierSalesman()) + "_*_" + (StringUtils.isBlank(o.getPoOwner()) ? "" : o.getPoOwner())
                ));
            } else if ("PRO_ZDY5".equals(gaiaDcSeperateConfi.getGdscField())) {
                mapList = list.stream().collect(Collectors.groupingBy(
                        o -> o.getPoSupplierId() + "_*_" + (StringUtils.isBlank(o.getProZdy5()) ? "" : o.getProZdy5()) + "_*_" + (StringUtils.isBlank(o.getPoSupplierSalesman()) ? "" : o.getPoSupplierSalesman()) + "_*_" + (StringUtils.isBlank(o.getPoOwner()) ? "" : o.getPoOwner())
                ));
            }
        }
        if (mapList == null) {
            mapList = list.stream().collect(Collectors.groupingBy(o -> o.getPoSupplierId() + "_*_" + (StringUtils.isBlank(o.getPoSupplierSalesman()) ? "" : o.getPoSupplierSalesman()) + "_*_" + (StringUtils.isBlank(o.getPoOwner()) ? "" : o.getPoOwner())));
        }
        CommonUtils utils = new CommonUtils();
        List<GaiaPoHeader> resultList = new ArrayList<>();
        for (Map.Entry<String, List<DcReplenishmentRequestDto>> entry : mapList.entrySet()) {
            List<DcReplenishmentRequestDto> itemList = entry.getValue();
            // 商品分组
            // 冷藏商品
            List<DcReplenishmentRequestDto> coldrequestDtos = itemList.stream().filter(x -> String.valueOf(3).equals(x.getProStorageCondition())).collect(Collectors.toList());
            List<DcReplenishmentRequestDto> normalrequestDtos = itemList.stream().filter(x -> !String.valueOf(3).equals(x.getProStorageCondition())).collect(Collectors.toList());
            //
            GaiaPoHeader poHeader = null;
            if (!coldrequestDtos.isEmpty()) {
                poHeader = generateOrders(entry, coldrequestDtos, utils, params, user);
            }
            // 普通商品
            if (!normalrequestDtos.isEmpty()) {
                poHeader = generateOrders(entry, normalrequestDtos, utils, params, user);
            }
            resultList.add(poHeader);
        }
        Result result = ResultUtil.success();
        result.setWarning(warning);
        result.setData(resultList);
        return result;
    }

    private GaiaPoHeader generateOrders(Map.Entry<String, List<DcReplenishmentRequestDto>> entry, List<DcReplenishmentRequestDto> itemList, CommonUtils utils, String[] params, TokenUser user) {
        String client = itemList.get(0).getClient();
        String supplierId = entry.getKey().split("_*_")[0];
        String dcCode = itemList.get(0).getDcCode();
        List<String> more = Arrays.asList(entry.getKey().split("_*_"));
        String poOwner = more.size() > 0 ? more.get(more.size() - 1) : "";
        //GAIA_PO_HEADER主键重复检查
        GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
        String poId = utils.getPoId(itemList.get(0).getClient(), CommonEnum.PurchaseClass.PURCHASECD.getFieldName(),
                DateUtils.getCurrentDateStr("yyyy"), gaiaPoHeaderMapper, redisClient);
        gaiaPoHeaderKey.setClient(itemList.get(0).getClient());
        gaiaPoHeaderKey.setPoId(poId);

        GaiaPoHeader gaiaPoHeaderInfo = gaiaPoHeaderMapper.selectByPrimaryKey(gaiaPoHeaderKey);
        if (null != gaiaPoHeaderInfo) {
            throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
        }

        //GAIA_PO_HEADER信息插入
        GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
        BeanUtils.copyProperties(gaiaPoHeaderKey, gaiaPoHeader);
        gaiaPoHeader.setPoType(CommonEnum.PurchaseClass.PURCHASECD.getFieldName());  // 订单类型 - Z001
        gaiaPoHeader.setPoSupplierId(supplierId); // 供应商自编码
        gaiaPoHeader.setPoCompanyCode(itemList.get(0).getDcCode()); // 采购主体 - DC编码
        if (params == null || params.length == 0) {
            gaiaPoHeader.setPoSubjectType("1");  // 主体类型  1。连锁
        } else {
            gaiaPoHeader.setPoSubjectType(params[0]);  // 主体类型
        }
        gaiaPoHeader.setPoDate(DateUtils.getCurrentDateStr("yyyyMMdd"));  // 凭证日期  当前日期
        gaiaPoHeader.setPoPaymentId(itemList.get(0).getSupPayTerm()); //付款条款
        gaiaPoHeader.setPoCreateBy(user.getUserId());
        gaiaPoHeader.setPoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaPoHeader.setPoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
        gaiaPoHeader.setPoApproveStatus("0"); // 审批状态 - 不需要审批
        GaiaSupplierBusinessKey supplierBusinessKey = new GaiaSupplierBusinessKey();
        supplierBusinessKey.setClient(client);
        supplierBusinessKey.setSupSite(dcCode);
        supplierBusinessKey.setSupSelfCode(supplierId);
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(supplierBusinessKey);
        if (!ObjectUtils.isEmpty(gaiaSupplierBusiness)) {
            gaiaPoHeader.setPoLeadTime(gaiaSupplierBusiness.getSupLeadTime());
            gaiaPoHeader.setPoDeliveryMode(gaiaSupplierBusiness.getSupDeliveryMode());
            gaiaPoHeader.setPoCarrier(gaiaSupplierBusiness.getSupCarrier());
            gaiaPoHeader.setPoDeliveryTimes(gaiaSupplierBusiness.getSupDeliveryTimes());
        }
        gaiaPoHeader.setPoSupplierSalesman(itemList.get(0).getPoSupplierSalesman());
        gaiaPoHeader.setPoOwner(poOwner);
        gaiaPoHeader.setPoSalesmanName(itemList.get(0).getPoSalesmanName());
        gaiaPoHeader.setPoOwnerName(itemList.get(0).getPoOwnerName());
        gaiaPoHeaderMapper.insert(gaiaPoHeader);

        //查询是否需要审批
        GaiaApproveRule gaiaApproveRule = gaiaApproveRuleMapper.selectByPrimaryKey(client, dcCode);
        int num = 1;
        // 采购审批集合
        List<PurchaseFeignDto> purchaseFeignDtoList = new ArrayList<>();
        for (DcReplenishmentRequestDto item : itemList) {
            Integer approvalType = 0;  // 0-不需要审批， 1-单挑调增金额触发审批， 2-单价调增比例触发审批， 3-存销比触发审批
            //GAIA_PO_ITEM信息插入
            GaiaPoItem gaiaPoItem = new GaiaPoItem();
            BeanUtils.copyProperties(item, gaiaPoItem);
            gaiaPoItem.setClient(item.getClient());
            gaiaPoItem.setPoId(poId);
            gaiaPoItem.setPoSiteCode(item.getDcCode());
            // 行总价
            gaiaPoItem.setPoLineAmt(item.getPoQty().multiply(item.getPoPrice()));
            // 库位 1000 合格库
            gaiaPoItem.setPoLocationCode(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(0).getValue());
            gaiaPoItem.setPoCompleteInvoice("0");
            gaiaPoItem.setPoCompletePay("0");
            gaiaPoItem.setPoLineDelete("0");
            gaiaPoItem.setPoLineNo(String.valueOf(num));
            // 前序订单单号
            gaiaPoItem.setPoPrePoid(item.getPoPrePoid());
            // 前序订单行号
            gaiaPoItem.setPoPrePoitem(item.getPoPrePoitem());
            gaiaPoItem.setPoCompleteFlag("0");
            // 生产批号 PO_BATCH_NO
            gaiaPoItem.setPoBatchNo(item.getPoBatchNo());
            // 生产日期 PO_SCRQ
            gaiaPoItem.setPoScrq(item.getPoScrq());
            // 有效期 PO_YXQ
            gaiaPoItem.setPoYxq(item.getPoYxq());
            gaiaPoItemMapper.insert(gaiaPoItem);
            // 将补货主表中的是否转采购订单字段值更新成：1-是
            if (StringUtils.isNotBlank(item.getPoPrePoid())) {
                GaiaSdReplenishH gaiaSdReplenishH = new GaiaSdReplenishH();
                //加盟商
                gaiaSdReplenishH.setClient(item.getClient());
                //补货门店
                gaiaSdReplenishH.setGsrhBrId(item.getDcCode());
                //补货单号
                gaiaSdReplenishH.setGsrhVoucherId(item.getPoPrePoid());
                //补货日期
                gaiaSdReplenishH.setGsrhDate(item.getGsrhDate());
                gaiaSdReplenishH.setGsrhCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                gaiaSdReplenishH.setGsrhFlag(CommonEnum.NoYesStatus.YES.getCode());
                gaiaSdReplenishHMapper.updateByPrimaryKeySelective(gaiaSdReplenishH);
            }
            //行数自增
            num++;

            //检查货源清单表
            GaiaSourceList gaiaSource = new GaiaSourceList();
            gaiaSource.setClient(item.getClient());
            gaiaSource.setSouProCode(item.getPoProCode());
            gaiaSource.setSouSiteCode(dcCode);
            gaiaSource.setSouSupplierId(supplierId);
            List<GaiaSourceList> gaiaSourceList = gaiaSourceListMapper.selectSourceList(gaiaSource);
            //若无值则插入数据
            if (CollectionUtils.isEmpty(gaiaSourceList)) {
                //GAIA_SOURCE_LIST插入数据
                GaiaSourceList insertSourceList = new GaiaSourceList();
                insertSourceList.setClient(item.getClient());
                //货源清单编码
                GaiaSourceList souListCodeInfo = gaiaSourceListMapper.selectNo(item.getClient(), item.getPoProCode(), item.getDcCode());

                insertSourceList.setSouListCode(souListCodeInfo.getSouListCode());
                insertSourceList.setSouProCode(item.getPoProCode());
                insertSourceList.setSouSiteCode(item.getDcCode());
                insertSourceList.setSouSupplierId(supplierId);
                insertSourceList.setSouEffectFrom(DateUtils.getCurrentDateStr("yyyyMMdd"));
                insertSourceList.setSouEffectEnd("20991231");
                insertSourceList.setSouMainSupplier("0");
                insertSourceList.setSouLockSupplier("0");
                insertSourceList.setSouDeleteFlag("0");
                insertSourceList.setSouCreateType("0");
                insertSourceList.setSouCreateBy(user.getUserId());
                insertSourceList.setSouCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                insertSourceList.setSouCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaSourceListMapper.insert(insertSourceList);
            }

            if (null != gaiaApproveRule) {

                //采购价格平均值获取
                BigDecimal unitPriceList = gaiaPoItemMapper.selectUnitPrice(item.getClient(), item.getDcCode(), item.getPoProCode());
                //采购次数获取
                List<GaiaPoItem> saleTime = gaiaPoItemMapper.selectSaleTime(item.getClient(), item.getDcCode(), item.getPoProCode());
                BigDecimal timeNum = new BigDecimal(saleTime.size());
                if (null == unitPriceList || unitPriceList.equals(BigDecimal.ZERO)) {
                    approvalType = 1;
//                        gaiaPoHeaderMapper.updateApproveStatus(item.getClient(), poId);
//                        break;
                }
                //本次采购价格
                BigDecimal thisPrice = item.getPoPrice();
                //单价增调值
                if (unitPriceList != null && (thisPrice.subtract(unitPriceList).compareTo(gaiaApproveRule.getAprPriceAmt()) == 0
                        || thisPrice.subtract(unitPriceList).compareTo(gaiaApproveRule.getAprPriceAmt()) == 1)) {
//                        gaiaPoHeaderMapper.updateApproveStatus(item.getClient(), poId);
//                        break;
                    if (approvalType == 0) {
                        approvalType = 1;
                    }
                }
                //单价调增比例
                if (unitPriceList != null && (thisPrice.subtract(unitPriceList).divide(timeNum, 4, RoundingMode.HALF_UP).compareTo(gaiaApproveRule.getAprPriceRate()) == 0
                        || thisPrice.subtract(unitPriceList).divide(timeNum, 4, RoundingMode.HALF_UP).compareTo(gaiaApproveRule.getAprPriceRate()) == 1)) {
                    if (approvalType == 0) {
                        approvalType = 3;
                    }
                }
                //总库存量
                BigDecimal stock = BigDecimal.ZERO;
                //30天销售总量
                BigDecimal saleVolume = BigDecimal.ZERO;

                if (params == null || params.length == 0) {
                    saleVolume = gaiaSoItemMapper.selectTotalSale(item.getClient(), item.getDcCode());
                } else {
                    saleVolume = gaiaSdSaleDMapper.selectSaleNum(item.getClient(), item.getDcCode(), item.getPoProCode());
                }
                // 库存
                GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
                // 加盟商
                gaiaMaterialAssessKey.setClient(item.getClient());
                // 商品编码
                gaiaMaterialAssessKey.setMatProCode(item.getPoProCode());
                // 估价地点
                gaiaMaterialAssessKey.setMatAssessSite(item.getDcCode());
                GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
                if (gaiaMaterialAssess != null) {
                    stock = gaiaMaterialAssess.getMatTotalQty() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatTotalQty();
                }
                //最新存销比
                BigDecimal rate = new BigDecimal(0);
                //30天销售总量为0
                if (saleVolume.equals(BigDecimal.ZERO)) {
//                        gaiaPoHeaderMapper.updateApproveStatus(item.getClient(), poId);
//                        break;
                    if (approvalType == 0) {
                        approvalType = 3;
                    }
                } else {
                    //最新存销比
                    rate = item.getPoQty().add(stock).divide(saleVolume, 4, RoundingMode.HALF_UP);
                    if (rate.compareTo(gaiaApproveRule.getAprStockUseRate()) == 0
                            || rate.compareTo(gaiaApproveRule.getAprStockUseRate()) == 1) {
//                        gaiaPoHeaderMapper.updateApproveStatus(item.getClient(), poId);
//                        break;
                        if (approvalType == 0) {
                            approvalType = 3;
                        }
                    }
                }

                if (approvalType > 0) {
                    //商品状态检查
                    GaiaProductBusiness status = gaiaProductBusinessMapper.selectGoodsStatus(client, item.getPoProCode(), dcCode);
                    GaiaProductBasic basic = gaiaProductBasicMapper.selectByPrimaryKey(status.getProCode());
                    PurchaseFeignDto purchaseFeignDto = new PurchaseFeignDto();
                    purchaseFeignDto.setProCode(item.getPoProCode()); // 商品编码
                    purchaseFeignDto.setProName(basic.getProName());  // 商品名
                    purchaseFeignDto.setProSpecs(basic.getProSpecs()); // 规格
                    purchaseFeignDto.setWfSite(item.getDcCode()); // 门店/部门/配送中心
                    if (unitPriceList != null) {
                        purchaseFeignDto.setPriceAverage(unitPriceList.setScale(2).toString());  // 5次均价
                    } else {
                        purchaseFeignDto.setPriceAverage("0.00");  // 5次均价
                    }
                    purchaseFeignDto.setPriceReal(item.getPoPrice().setScale(2).toString()); // 实际采购价
                    // 存销比
                    if (rate != null) {
                        purchaseFeignDto.setStockUseRate(rate.setScale(2).toString());
                    } else {
                        purchaseFeignDto.setStockUseRate("0.00");
                    }
                    if (approvalType == 1) {  // 1-单挑调增金额触发审批，
                        if (unitPriceList != null) {
                            purchaseFeignDto.setWarnValue(item.getPoPrice().subtract(unitPriceList).setScale(2).toString());
                        } else {
                            purchaseFeignDto.setWarnValue("0.00");
                        }
                    } else if (approvalType == 2) {  // 2-单价调增比例触发审批，
                        if (unitPriceList != null) {
                            purchaseFeignDto.setWarnValue(item.getPoPrice().subtract(unitPriceList).divide(unitPriceList, 4, BigDecimal.ROUND_HALF_UP).toString());
                        } else {
                            purchaseFeignDto.setWarnValue("0.00");
                        }
                    } else {  // 3-存销比触发审批
                        if (rate != null) {
                            purchaseFeignDto.setWarnValue(rate.subtract(gaiaApproveRule.getAprStockUseRate()).setScale(2).toString());
                        } else {
                            purchaseFeignDto.setWarnValue("0.00");
                        }
                    }
                    purchaseFeignDtoList.add(purchaseFeignDto);
                }
            }
            //明细循环结束
        }

        // 需要审批 并提交审批流
        if (purchaseFeignDtoList.size() > 0) {
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            gaiaPoHeader.setPoFlowNo(System.currentTimeMillis() + org.apache.commons.lang3.StringUtils.leftPad(String.valueOf(i), 6, "0"));
            gaiaPoHeaderMapper.updateApproveStatus(client, poId, gaiaPoHeader.getPoFlowNo());
            FeignResult result = feignService.createPurchaseWorkflow(gaiaPoHeader, purchaseFeignDtoList);
            if (result.getCode() != 0) {
                throw new CustomResultException(result.getMessage());
            }
        }
        return gaiaPoHeader;
    }


    /**
     * 供应商验证
     *
     * @param client
     * @param site
     * @param supId
     */
    private void supplierCheck(GaiaDcData gaiaDcData, String client, String site, String supId, Map<String, GaiaSupplierBusiness> supplierBusinessMap,
                               List<String> warning, List<String> error) {
        if (gaiaDcData == null) {
            return;
        }
        // 是否管控证照效期：1-是
        if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1")) {
            GaiaSupplierBusiness gaiaSupplierBusiness = supplierBusinessMap.get(supId);
            if (gaiaSupplierBusiness == null) {
                GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
                gaiaSupplierBusinessKey.setClient(client);
                gaiaSupplierBusinessKey.setSupSite(site);
                gaiaSupplierBusinessKey.setSupSelfCode(supId);
                gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(gaiaSupplierBusinessKey);
                if (gaiaSupplierBusiness != null) {
                    supplierBusinessMap.put(supId, gaiaSupplierBusiness);
                }
                if (gaiaSupplierBusiness != null) {
                    // 营业执照期限、
                    dateCheck(supId, gaiaSupplierBusiness.getSupCreditDate(), "营业执照", warning, error);
                    // 发证有效期、
                    dateCheck(supId, gaiaSupplierBusiness.getSupLicenceValid(), "发证有效期", warning, error);
                    // 法人委托书有效期、
                    dateCheck(supId, gaiaSupplierBusiness.getSupFrwtsxq(), "法人委托书", warning, error);
                    // GSP有效期、
                    dateCheck(supId, gaiaSupplierBusiness.getSupGspDate(), "GSP证书", warning, error);
                    // GMP有效期、
                    dateCheck(supId, gaiaSupplierBusiness.getSupGmpDate(), "GMP证书", warning, error);
                    // 质保协议有效期、
                    dateCheck(supId, gaiaSupplierBusiness.getSupQualityDate(), "质保协议", warning, error);
                    // 生产许可证有效期、
                    dateCheck(supId, gaiaSupplierBusiness.getSupScxkzDate(), "生产许可证", warning, error);
                    // 食品许可证有效期、
                    dateCheck(supId, gaiaSupplierBusiness.getSupSpxkzDate(), "食品许可证", warning, error);
                    // 医疗器械经营许可证有效期
                    dateCheck(supId, gaiaSupplierBusiness.getSupYlqxxkzDate(), "医疗器械经营许可证", warning, error);
                }
            }
        }
    }

    /**
     * 效期验证
     *
     * @param data
     * @param msg
     */
    private void dateCheck(String supId, String data, String msg, List<String> warning, List<String> error) {
        String day = DateUtils.getCurrentDateStr("yyyyMMdd");
        if (StringUtils.isNotBlank(data) && data.trim().length() == 8) {
            if (day.compareTo(data) > 0) {
                error.add("供应商" + supId + msg + "已过期");
            }
            // 警告
            Long datediff = DateUtils.datediff(day, data);
            if (datediff.intValue() < 60) {
                warning.add("供应商" + supId + msg + "即将过期");
            }
        }
    }

    /**
     * 得到补货系数
     *
     * @param dto
     * @param args
     * @return
     */
    public BigDecimal getReplenishmentRatio(DcReplenishmentListResponseDto dto, BigDecimal... args) {
        // X<N5
        if (dto.getDaysSalesCount().compareTo(new BigDecimal(5 * dto.getStoreCount())) < 0) {
            return args[0];
            // 5N <= X < 10N
        } else if (dto.getDaysSalesCount().compareTo(new BigDecimal(5 * dto.getStoreCount())) > 0
                && dto.getDaysSalesCount().compareTo(new BigDecimal(10 * dto.getStoreCount())) <= 0
        ) {
            return args[1];
            // 10N<= X < 15N
        } else if (dto.getDaysSalesCount().compareTo(new BigDecimal(10 * dto.getStoreCount())) > 0
                && dto.getDaysSalesCount().compareTo(new BigDecimal(15 * dto.getStoreCount())) <= 0) {
            return args[2];
            // 15N <= X < 30N
        } else if (dto.getDaysSalesCount().compareTo(new BigDecimal(15 * dto.getStoreCount())) > 0
                && dto.getDaysSalesCount().compareTo(new BigDecimal(30 * dto.getStoreCount())) <= 0) {
            return args[3];
            // X>=30N
        } else {
            return args[4];
        }
    }

    /**
     * DC补货列表导出
     *
     * @param dcCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result exportReplenishmentList(String dcCode, Integer pageNum, Integer pageSize) {
        try {
            PageInfo<DcReplenishmentListResponseDto> pageInfo = queryReplenishmentList(dcCode, 0, 0);
            List<DcReplenishmentListResponseDto> batchList = pageInfo.getList();
            // 导出数据
            List<List<String>> dcList = new ArrayList<>();
            initData(batchList, dcList);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(dcHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dcList);
                    }},
                    new ArrayList<String>() {{
                        add("DC补货列表数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Objects.requireNonNull(workbook).write(bos);
            return cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 商品大类列表
     */
    @Override
    public List<GaiaProductClassDTO> getProductClass() {
        return gaiaProductClassMapper.getProductClass();
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param dcList
     */
    private void initData(List<DcReplenishmentListResponseDto> batchList, List<List<String>> dcList) {
        //进项税 字典
        List<Dictionary> dictionaryList = null;
        DictionaryParams dictionaryParams = new DictionaryParams();
        dictionaryParams.setKey("TAX_CODE1");
        Result result = dictionaryService.getDictionary(dictionaryParams);
        if (result.getCode().equals(ResultEnum.SUCCESS.getCode())) {
            dictionaryList = (List<Dictionary>) result.getData();
        }


        for (DcReplenishmentListResponseDto dcDTO : batchList) {
            List<String> basicData = new ArrayList<>();
            //	商品编码
            basicData.add(dcDTO.getProSelfCode());
            //	商品名称
            basicData.add(dcDTO.getProName());
            // 种类
            basicData.add(dcDTO.getProClassName());
            // 商品大分类
            basicData.add(dcDTO.getProBigClassName());
            //	规格
            basicData.add(dcDTO.getProSpecs());
            //	生产厂家
            basicData.add(dcDTO.getProFactoryName());
            //	建议补货量
            basicData.add(ObjectUtils.isEmpty(dcDTO.getReplenishmentValue()) ? "" : dcDTO.getReplenishmentValue().stripTrailingZeros().toPlainString());
            // 补货量
            basicData.add(ObjectUtils.isEmpty(dcDTO.getProposalReplenishment()) ? "" : dcDTO.getProposalReplenishment().stripTrailingZeros().toPlainString());
            //	供应商下拉列表
            if (StringUtils.isBlank(dcDTO.getLastSupplierId()) && StringUtils.isBlank(dcDTO.getLastSupplier())) {
                basicData.add("");
            } else if (StringUtils.isNotBlank(dcDTO.getLastSupplierId()) && StringUtils.isNotBlank(dcDTO.getLastSupplier())) {
                basicData.add(dcDTO.getLastSupplierId() + "-" + dcDTO.getLastSupplier());
            } else if (StringUtils.isNotBlank(dcDTO.getLastSupplierId())) {
                basicData.add(dcDTO.getLastSupplierId());
            } else if (StringUtils.isNotBlank(dcDTO.getLastSupplier())) {
                basicData.add(dcDTO.getLastSupplier());
            } else {
                basicData.add("");
            }
            //	采购价格
            basicData.add(ObjectUtils.isEmpty(dcDTO.getLastPurchasePrice()) ? "" : dcDTO.getLastPurchasePrice().stripTrailingZeros().toPlainString());
            //	进项税率
            if (CollectionUtils.isNotEmpty(dictionaryList)) {
                Dictionary dictionary = dictionaryList.stream().filter(item -> item.getValue().equals(dcDTO.getProInputTax())).findFirst().orElse(null);
                if (ObjectUtils.isNotEmpty(dictionary)) {
                    basicData.add(dictionary.getLabel());
                } else {
                    basicData.add("");
                }
            }
            //	地点
            basicData.add(dcDTO.getDcCode() + "-" + dcDTO.getDcName());
            //	单位
            basicData.add(dcDTO.getProUnit());
            //	中包装量
            basicData.add(dcDTO.getProMidPackage());
            //	大仓库存
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDcStockTotal()) ?
                    dcDTO.getDcStockTotal().stripTrailingZeros().toPlainString() : "");
            //	门店库存
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getStoreStockTotal()) ?
                    dcDTO.getStoreStockTotal().stripTrailingZeros().toPlainString() : "");
            //	总库存量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getStockTotal()) ?
                    dcDTO.getStockTotal().stripTrailingZeros().toPlainString() : "");
            //	最近一次进货日期
            basicData.add(formatDate(dcDTO.getLastPurchaseDate()));
            //	最后进货供应商自编码
            basicData.add(dcDTO.getLastSupplierId());
            //	末次进货价
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getLastPurchasePrice()) ?
                    dcDTO.getLastPurchasePrice().stripTrailingZeros().toPlainString() : "");
            //	7天门店销售量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDaysSalesCountSeven()) ?
                    dcDTO.getDaysSalesCountSeven().stripTrailingZeros().toPlainString() : "");
            //	30天门店销售量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDaysSalesCount()) ?
                    dcDTO.getDaysSalesCount().stripTrailingZeros().toPlainString() : "");
            //	90天门店销售量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDaysSalesCountNinety()) ?
                    dcDTO.getDaysSalesCountNinety().stripTrailingZeros().toPlainString() : "");
            //	30天对外批发量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDaysWholesaleCount()) ?
                    dcDTO.getDaysWholesaleCount().stripTrailingZeros().toPlainString() : "");
            //	在途量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getTrafficCount()) ?
                    dcDTO.getTrafficCount().stripTrailingZeros().toPlainString() : "");
            //	待出量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getWaitCount()) ?
                    dcDTO.getWaitCount().stripTrailingZeros().toPlainString() : "");
            //	补货门店数
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getStoreCount()) ?
                    dcDTO.getStoreCount().toString() : "");
            // 补货点
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getReplenishmentRatio()) ?
                    dcDTO.getReplenishmentRatio().stripTrailingZeros().toString() : "");
//            //	预计到货时间
//            basicData.add(formatDate(dcDTO.getPoDeliveryDate()));

            dcList.add(basicData);
        }
    }

    /**
     * 日期格式化
     */
    private String formatDate(String date) {
        if (StringUtils.isEmpty(date)) {
            return null;
        }
        if (date.length() != 8) {
            return date;
        }
        StringBuilder sb = new StringBuilder(date);
        sb.insert(4, "/");
        sb.insert(7, "/");
        return sb.toString();
    }

    /**
     * 业务数据表头
     */
    private final String[] dcHead = {
            "商品编码",
            "商品名称",
            "种类",
            "商品大分类",
            "规格",
            "生产厂家",
            "建议补货量",
            "补货量",
            "供应商",
            "采购价格",
            "进项税率",
            "地点",
            "单位",
            "中包装量",
            "大仓库存",
            "门店库存",
            "总库存量",
            "最近一次进货日期",
            "最后进货供应商自编码",
            "末次进货价",
            "7天门店销售量",
            "30天门店销售量",
            "90天门店销售量",
            "30天对外批发量",
            "在途量",
            "待出量",
            "补货门店数",
            "补货点",
//            "预计到货时间"
    };


}
