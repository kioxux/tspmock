package com.gov.purchase.module.goods.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ProductChangeRecordDTO {

    /**
     * 修改前的值
     */
    private String beforeValue;
    /**
     * 修改后的值
     */
    private String afterValue;

}
