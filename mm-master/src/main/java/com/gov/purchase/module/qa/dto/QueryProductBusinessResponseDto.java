package com.gov.purchase.module.qa.dto;

import com.gov.purchase.entity.GaiaProductBusiness;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "单体")
public class QueryProductBusinessResponseDto extends GaiaProductBusiness {


    /**
     * 零售价
     */
    private BigDecimal gsppPriceNormal;

    /**
     * 会员价
     */
    private BigDecimal gsppPriceHy;

    /**
     * 医保价
     */
    private BigDecimal gsppPriceYb;

    /**
     * 拆零价
     */
    private BigDecimal gsppPriceCl;

    /**
     * 网上零售价
     */
    private BigDecimal gsppPriceOnlineNormal;

    /**
     * 网上会员价
     */
    private BigDecimal gsppPriceOnlineHy;

    /**
     * 会员日价
     */
    private BigDecimal gsppPriceHyr;

    /**
     * 库存数
     */
    private BigDecimal gssQty;

    /**
     * 成本
     */
    private BigDecimal matMovPrice;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 1 7天
     * 2 30天
     */
    private Integer type;

    private Integer proWeek;

    private Integer proMonth;

    /**
     * 店组编码
     */
    private String stogCode;

    /**
     * 店组名称
     */
    private String stogName;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 门店名称
     */
    private String stoName;

    private BigDecimal price;


    //门店商品list
    private List<QueryProductBusinessResponseDto> children;


}
