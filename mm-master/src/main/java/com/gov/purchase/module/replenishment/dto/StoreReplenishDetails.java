package com.gov.purchase.module.replenishment.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.gov.purchase.common.excel.annotation.ExcelField;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.01.12
 */
@Data
public class StoreReplenishDetails {

    /**
     * 商品编码
     */
    @ExcelProperty(value = "商品编码",index = 2)
    private String proSelfCode;

    /**
     * 商品名
     */
    @ExcelIgnore
    private String proName;

    /**
     * 门店编码
     */
    @ExcelProperty(value = "门店编码",index = 0)
    private String stoCode;

    /**
     * 门店名
     */
    @ExcelProperty(value = "门店名",index = 1)
    private String stoName;

    /**
     * 规格
     */
    @ExcelProperty(value = "规格" ,index = 4)
    private String proSpecs;

    /**
     * 生产厂家
     */
    @ExcelProperty(value = "生产厂家" ,index = 5)
    private String proFactoryName;

    /**
     * 单位
     */
    @ExcelProperty(value = "单位" ,index = 6)
    private String proUnit;

    /**
     * 加点金额
     */
    @ExcelIgnore
    private BigDecimal proAddAmt;

    /**
     * 加点比例
     */
    @ExcelIgnore
    private BigDecimal proAddRate;

    /**
     * 配送价
     */
    @ExcelIgnore
    private BigDecimal deliveryPrice;

    /**
     * 零售价
     */
    @ExcelProperty(value = "零售价" ,index = 10)
    private BigDecimal proLsj;

    /**
     * 成本价
     */
    @ExcelProperty(value = "成本价" ,index = 11)
    private BigDecimal costPrice;

    /**
     * 是否医保
     */
    @ExcelIgnore
    private String proIfMed;

    /**
     * 定位
     */
    @ExcelProperty(value = "商品定位" ,index = 8)
    private String proPosition;

    /**
     * 销售级别
     */
    @ExcelProperty(value = "销售级别" ,index = 7)
    private String proSlaeClass;

    /**
     * 门店库存
     */
    @ExcelProperty(value = "门店库存" ,index = 12)
    private BigDecimal gssQty;

    /**
     * 仓库库存
     */
    @ExcelProperty(value = "仓库库存" ,index = 13)
    private BigDecimal dcQty;

    /**
     * 铺货数量
     */
    @ExcelProperty(value = "铺货数量" ,index =18)
    private Integer gsrdNeedQty;

    /**
     * 货位号
     */
    @ExcelIgnore
    private String gsrdHwh;

    /**
     * 批号信息
     */
    @ExcelIgnore
    private List<BatchNoInfo> batchNoInfoList;

    /**
     * 铺货地点（dc编码）
     */
    @ExcelIgnore
    private String dcCode;

    /**
     * 委托配送中心
     */
    @ExcelProperty(value = "配送点" ,index = 9)
    private String dcWtdc;

    /**
     * 税率
     */
    @ExcelIgnore
    private String gsrdTaxRate;

    /**
     * 已铺未出数量
     */
    @ExcelProperty(value = "已铺未出数量" ,index = 17)
    private BigDecimal gsrdNeedNotDnQty;

    /**
     * 备注
     */
    @ExcelIgnore
    private String gsrdBz;

    /**
     * 商品描述
     */
    @ExcelProperty(value = "商品名称",index = 3)
    private String proDepict;
    /**
     * 经营类别
     */
    @ExcelIgnore
    private String proJylb;

    /**
     * 加点金额
     */
    @ExcelIgnore
    private BigDecimal alpAddAmt;
    /**
     * 加点比例
     */
    @ExcelIgnore
    private BigDecimal alpAddRate;
    /**
     * 目录价
     */
    @ExcelIgnore
    private BigDecimal alpCataloguePrice;
    /**
     * 7天销量
     */
    @ExcelProperty(value = "7天销量" ,index = 14)
    private BigDecimal gssdQty7;
    /**
     * 30天销量
     */
    @ExcelProperty(value = "30天销量" ,index = 15)
    private BigDecimal gssdQty30;
    /**
     * 90天销量
     */
    @ExcelProperty(value = "90天销量" ,index = 16)
    private BigDecimal gssdQty90;

    /**
     * 中包装
     */
    @ExcelIgnore
    private String proMidPackage;
}
