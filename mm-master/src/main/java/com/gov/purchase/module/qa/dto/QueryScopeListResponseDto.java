package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "经营范围列表输出结果")
public class QueryScopeListResponseDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    private String client;

    @ApiModelProperty(value = "加盟商名称", name = "francName")
    private String francName;

    @ApiModelProperty(value = "实体分类", name = "bscEntityClass")
    private String bscEntityClass;

    @ApiModelProperty(value = "实体编号", name = "bscEntityId")
    private String bscEntityId;

    @ApiModelProperty(value = "实体名称", name = "bscEntityName")
    private String bscEntityName;
}
