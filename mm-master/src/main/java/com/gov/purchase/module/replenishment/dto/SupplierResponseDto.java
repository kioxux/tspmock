package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@ApiModel(value = "供应商返回参数")
public class SupplierResponseDto {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商编号
     */
    private String supCode;

    /**
     * 供应商自编码
     */
    private String supSelfCode;
    /**
     * 供应商名称
     */
    private String supName;
    /**
     * 供应商分类
     */
    private String supClass;

    /**
     * 付款条件
     */
    private String supPayTerm;

    /**
     * 统一社会信用代码
     */
    private String supCreditCode;
    /**
     * 业务员
     */
    private String supBussinessContact;

    /**
     * 联系电话
     */
    private String supContactTel;

    /**
     * 含税进价
     */
    private BigDecimal poPrice;

    /**
     * 税率
     */
    private String poRate;

    /**
     * 末次价
     */
    private BigDecimal lastPrice;

    /**
     * 助记码
     */
    private String supPym;

    /**
     * 采购员（供应商）
     */
    private String supCgy;

}
