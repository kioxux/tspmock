package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "质量管控-批次状态列表传入参数")
public class QueryStockListResquestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client" ,required = true)
    private String client;

    @ApiModelProperty(value = "地点集合", name = "siteList" ,required = true)
    private List<String> siteList;

    @ApiModelProperty(value = "商品编码", name = "proCode" ,required = true)
    private String proCode;

    @ApiModelProperty(value = "库存状态(0-正常,1-异常)", name = "status" ,required = true)
    private String status;

    @ApiModelProperty(value = "批次号", name = "batBatch")
    private String batBatch;
}
