package com.gov.purchase.module.base.service.impl.supplierImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaSupplierBasicMapper;
import com.gov.purchase.mapper.GaiaSupplierBusinessMapper;
import com.gov.purchase.module.base.dto.excel.Supplier;
import com.gov.purchase.utils.SpringUtil;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.30
 */
@Service
public class SupplierImportEditImpl extends SupplierImport {

    @Resource
    GaiaSupplierBasicMapper gaiaSupplierBasicMapper;

    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;


    /**
     * 业务验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap) {
        return super.errorListSupplier((LinkedHashMap<Integer, Supplier>) dataMap);
    }

    /**
     * 主数据表验证
     *
     * @param supplier
     * @param errorList
     * @param key
     */
    protected void baseCheck(Supplier supplier, List<String> errorList, int key) {
        GaiaSupplierBasic gaiaSupplierBasic = gaiaSupplierBasicMapper.selectByPrimaryKey(supplier.getSupCode());
        if (gaiaSupplierBasic == null) {
            errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
        }
    }

    /**
     * 业务数据验证
     *
     * @param supplier
     * @param errorList
     * @param key
     */
    protected void businessCheck(Supplier supplier, List<String> errorList, int key) {
        GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
        //加盟商
        gaiaSupplierBusinessKey.setClient(supplier.getClient());
        //供应商编码
        //地点
        gaiaSupplierBusinessKey.setSupSite(supplier.getSupSite());
        //供应商自编码
        gaiaSupplierBusinessKey.setSupSelfCode(supplier.getSupSelfCode());
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(gaiaSupplierBusinessKey);
        if (gaiaSupplierBusiness == null) {
            errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
        }
    }

    /**
     * 数据验证完成
     *
     * @return
     */
    @Override
    protected void checkEnd(String path) {
        // 生成导入日志
        createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.WAIT.getCode());
    }

    /**
     * 数据导入完成
     *
     * @param bulUpdateCode
     * @param map
     * @param <T>
     */
    @Override
    @Transactional
    protected <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> map) {
        List<String> errorList = new ArrayList<>();
        // 批量导入
        try {
            for (Integer key : map.keySet()) {
                // 行数据
                //SupplierBasic表
                Supplier supplier = (Supplier) map.get(key);
                // 数据库对象
                GaiaSupplierBasic gaiaSupplierBasic = gaiaSupplierBasicMapper.selectByPrimaryKey(supplier.getSupCode());
                if (gaiaSupplierBasic == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
                    break;
                }
                SpringUtil.copyPropertiesIgnoreNull(supplier, gaiaSupplierBasic);
                // 供应商状态
                if (super.isClear(supplier.getSupStatus())) {
                    // 清空
                    gaiaSupplierBasic.setSupStatus(null);
                } else if (super.isNotBlank(supplier.getSupStatusValue())) {
                    gaiaSupplierBasic.setSupStatus(supplier.getSupStatusValue());
                }
                // 清空数据处理
                super.initNull(gaiaSupplierBasic);
                gaiaSupplierBasicMapper.updateByPrimaryKey(gaiaSupplierBasic);

                // 业务表数据存在判断
                if (StringUtils.isBlank(supplier.getClient())) {
                    continue;
                }
                // 数据库对象
                GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
                gaiaSupplierBusinessKey.setClient(supplier.getClient());
                gaiaSupplierBusinessKey.setSupSite(supplier.getSupSite());
                gaiaSupplierBusinessKey.setSupSelfCode(supplier.getSupSelfCode());
                GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(gaiaSupplierBusinessKey);
                if (gaiaSupplierBusiness == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
                    break;
                }
                SpringUtil.copyPropertiesIgnoreNull(supplier, gaiaSupplierBusiness);
                // 供应商状态
                if (super.isClear(supplier.getSupStatus())) {
                    // 清空
                    gaiaSupplierBusiness.setSupStatus(null);
                } else if (super.isNotBlank(supplier.getSupStatusValue())) {
                    gaiaSupplierBusiness.setSupStatus(supplier.getSupStatusValue());
                }
                // 禁止采购
                if (super.isClear(supplier.getSupNoPurchase())) {
                    // 清空
                    gaiaSupplierBusiness.setSupNoPurchase(null);
                } else if (super.isNotBlank(supplier.getSupNoPurchaseValue())) {
                    gaiaSupplierBusiness.setSupNoPurchase(supplier.getSupNoPurchaseValue());
                }
                // 禁止退厂
                if (super.isClear(supplier.getSupNoSupplier())) {
                    // 清空
                    gaiaSupplierBusiness.setSupNoSupplier(null);
                } else if (super.isNotBlank(supplier.getSupNoSupplierValue())) {
                    gaiaSupplierBusiness.setSupNoSupplier(supplier.getSupNoSupplierValue());
                }
                // 清空数据处理
                super.initNull(gaiaSupplierBusiness);
                gaiaSupplierBusinessMapper.updateByPrimaryKey(gaiaSupplierBusiness);
            }
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

        // 没有错误
        if (CollectionUtils.isEmpty(errorList)) {
            // 更新导入日志
            editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.COMPLETE.getCode());
            return ResultUtil.success();
        }
        Result result = ResultUtil.error(ResultEnum.E0115);
        result.setData(errorList);
        return result;
    }
}

