package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 药品采购记录
 * @author: yzf
 * @create: 2021-11-04 11:17
 */
@Data
public class DrugPurchaseVo {

    /**
     * 仓库编码
     */
    private String dcCode;

    /**
     * 商品编码
     */
    private String poProCode;

    /**
     * 采购日期
     */
    private String poCreateDate;

    /**
     * 采购订单
     */
    private String poId;

    private String poLineNo;
    private String poSiteCode;
    /**
     * 供应商
     */
    private String poSupplierId;

    /**
     * 供应商名
     */
    private String supName;
    /**
     * 通用名
     */
    private String proCommonName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 剂型
     */
    private String proForm;
    /**
     * 批准文号
     */
    private String proRegisterNo;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 采购单价
     */
    private BigDecimal poPrice;
    /**
     * 采购数量
     */
    private BigDecimal poQty;
    /**
     * 价税合计
     */
    private BigDecimal poTotal;
    /**
     * 到货数量
     */
    private BigDecimal poCount;

    /**
     * 上市许可持有人
     */
    private String proHolder;
}
