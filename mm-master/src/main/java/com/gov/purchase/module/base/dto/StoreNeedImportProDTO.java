package com.gov.purchase.module.base.dto;

import com.gov.purchase.module.base.dto.businessImport.StoreNeedImportDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author zhoushuai
 * @date 2021/4/12 16:19
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StoreNeedImportProDTO extends StoreNeedImportDTO {

    /**
     * 成本价
     */
    private BigDecimal costPrice;
}
