package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "发票明细查询返回参数")
public class InvoiceDetailsResponseDto {

    /**
     * 到期付款日期
     */
    private String paymentDate;

    /**
     * 银行账号
     */
    private String supBankAccount;

    /**
     * 发票明细List
     */
    private List<InvoiceDetailsDto> invoiceDetailsDto;
}