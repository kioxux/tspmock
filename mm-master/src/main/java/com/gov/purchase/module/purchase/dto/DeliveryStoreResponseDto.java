package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "物流模式门店返回参数")
public class DeliveryStoreResponseDto {

    /**
     * 值
     */
    private String value;

    /**
     * 表示名
     */
    private String label;

    /**
     * 供应商
     */
    private List<SupplierListForReturnsResponseDto> supList;

}