package com.gov.purchase.module.blacklist.service.impl;

import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.BlackListProMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.blacklist.dto.BatchNoData;
import com.gov.purchase.module.blacklist.dto.BlackListRequestDto;
import com.gov.purchase.module.blacklist.dto.BlackListResponseDto;
import com.gov.purchase.module.blacklist.dto.ImportInData;
import com.gov.purchase.module.blacklist.service.BlackListProService;
import com.gov.purchase.module.blacklist.service.BlackListService;
import com.gov.purchase.utils.DateUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class BlackListProServiceImpl implements BlackListProService {
    @Resource
    private FeignService feignService;
    @Resource
    private BlackListProMapper proMapper;
    @Resource
    private BlackListService blackListService;

    @Override
    public void importBlackListPro(MultipartFile file) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        List<ImportInData> inData = new ArrayList<>();
        try {
            InputStream in = file.getInputStream();
            //导入已存在的Excel文件，获得只读的工作薄对象
            XSSFWorkbook wk = new XSSFWorkbook(in);
            //获取第一张Sheet表
            Sheet sheet = wk.getSheetAt(0);
            //取数据
            inData = getRowAndCell(sheet,client);
            in.close();
            wk.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        for (ImportInData data : inData) {
            if (ObjectUtils.isEmpty(data.getBatchNo())){
                data.setBatchNo("");
            }
            data.setClient(client);
            data.setUserId(user.getUserId());
            data.setCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            data.setCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        }
        //获取加盟商下加入黑名单商品
        BlackListRequestDto dto = new BlackListRequestDto();
        dto.setClient(client);
        List<BlackListResponseDto> list = blackListService.listBlack(dto);
        //区分是否有生产批号
        List<BlackListResponseDto> noBillNoBlackList = new ArrayList<>();
        List<BlackListResponseDto> billNoBlackList = new ArrayList<>();
        for (BlackListResponseDto d : list){
            if (ObjectUtils.isNotEmpty(d.getProBatchNo())){
                billNoBlackList.add(d);
            }else{
                noBillNoBlackList.add(d);
            }
        }
        //区分导入数据是否有生产批号
        List<ImportInData> noBillNoList = new ArrayList<>();
        List<ImportInData> billNoList = new ArrayList<>();
        for (ImportInData d : inData) {
            if (ObjectUtils.isNotEmpty(d.getBatchNo())){
                billNoList.add(d);
            }else{
                noBillNoList.add(d);
            }
        }
        //分别对比受否有重复数据
        for (ImportInData noBillData : noBillNoList) {
            String brId = noBillData.getStoCode();
            String proCode = noBillData.getProCode();
            for (BlackListResponseDto noBillNoBlack : noBillNoBlackList) {
                if(brId.equals(noBillNoBlack.getProSite()) && proCode.equals(noBillNoBlack.getProSelfCode())){
                    throw new CustomResultException("门店编码：" + brId + ",商品编码：" +proCode+ "数据重复，请核对" );
                }
            }
        }
        if (noBillNoList.size() > 0){
            proMapper.midifiedProOutList(noBillNoList);
        }
        for (ImportInData billData : billNoList) {
            String brId = billData.getStoCode();
            String proCode = billData.getProCode();
            String billNo = billData.getBatchNo();
            for (BlackListResponseDto billNoBlack : billNoBlackList) {
                if(brId.equals(billNoBlack.getProSite())
                        && proCode.equals(billNoBlack.getProSelfCode())
                        && billNo.equals(billNoBlack.getProBatchNo())){
                    throw new CustomResultException("门店编码：" + brId + ",商品编码：" +proCode+ ",生产批号："+ billNo +"数据重复，请核对" );
                }
            }
        }
        proMapper.insertBatchBlackList(inData);
    }

    @Override
    public List<BatchNoData> selectBatchNo(BatchNoData inData) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        String deptId = user.getDepId();
        inData.setClient(client);
        inData.setStoCode(deptId);
        return proMapper.selectBatchNo(inData);
    }

    public List<ImportInData> getRowAndCell(Sheet sheet,String client) {
        List<ImportInData> importInDataList = new ArrayList<>();
        String msg = null;
        // 获得数据的总行数从0开始
        int totalRows = sheet.getLastRowNum();
        System.out.println("总行数==" + totalRows);
        DataFormatter dataFormatter = new DataFormatter();
        if (totalRows > 0) {
            // 循环输出表格中的内容,首先循环取出行,再根据行循环取出列
            for (int i = 1; i <= totalRows; i++) {
                Row row = sheet.getRow(i); //取出一行数据放入row
                ImportInData newExpert = new ImportInData();
                newExpert.setClient(client);
                newExpert.setStoCode(dataFormatter.formatCellValue(row.getCell(0)).trim());
                newExpert.setProCode(dataFormatter.formatCellValue(row.getCell(1)).trim());
                newExpert.setBatchNo(dataFormatter.formatCellValue(row.getCell(2)).trim());
                importInDataList.add(newExpert);
            } //行end
        }else{
            throw new ClassCastException("EXCEL导入信息为空");
        }
        return importInDataList;
    }
}
