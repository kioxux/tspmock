package com.gov.purchase.module.purchase.controller;


import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaFicoInvoice;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.InvoicePayingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "财务付款与开票")
@RestController
@RequestMapping("invoicePaying")
public class InvoicePayingController {

    @Resource
    private InvoicePayingService payingInvoiceService;

    @Log("开票查询")
    @ApiOperation("开票查询")
    @PostMapping("listInvoice")
    public Result listInvoice(@Valid @RequestBody ListInvoiceRequestDTO listInvoiceRequestDTO) {
        return ResultUtil.success(payingInvoiceService.listInvoice(listInvoiceRequestDTO));
    }

    @Log("开票保存")
    @ApiOperation("开票保存")
    @PostMapping("saveListInvoice")
    public Result saveListInvoice(@Valid @RequestBody List<InvoiceDTO> listDto) {
        return payingInvoiceService.saveListInvoice(listDto);
    }

    @Log("付款查询")
    @ApiOperation("付款查询")
    @PostMapping("listPaying")
    public Result listPaying(@Valid @RequestBody ListPayingRequestDTO dto) {
        return ResultUtil.success(payingInvoiceService.listPaying(dto));
    }

    @Log("付款保存")
    @ApiOperation("付款查询")
    @PostMapping("savePaying")
    public Result savePaying(@Valid @RequestBody SavePayingRequestDTO dto) {
        return ResultUtil.success(payingInvoiceService.savePaying(dto));
    }

    /**
     * 支付回调
     */
    @Log("扫码支付回调")
    @ApiOperation("扫码支付回调")
    @PostMapping("payResult")
    public void payResult(HttpServletRequest httpServletRequest) {
        payingInvoiceService.payResult(httpServletRequest);
    }

    @Log("付款状态查询")
    @ApiOperation("付款状态查询")
    @GetMapping("getPayStatus")
    public Result getPayStatus(@RequestParam("ficoId") String ficoId, @RequestParam("client") String client) {
        return ResultUtil.success(payingInvoiceService.getPayStatus(ficoId, client));
    }

    @Log("发票付款成功查询")
    @ApiOperation("发票付款成功查询")
    @PostMapping("listInvoicePaying")
    public Result listInvoicePayingSuccess(@Valid @RequestBody ListInvoicePayingRequestDTO dto) {
        return ResultUtil.success(payingInvoiceService.listInvoicePayingSuccess(dto));
    }

    @Log("开票")
    @ApiOperation("开票")
    @PostMapping("invoice")
    public Result invoice(@RequestBody GaiaFicoInvoice gaiaFicoInvoice) {
        payingInvoiceService.invoice(gaiaFicoInvoice);
        return ResultUtil.success();
    }

    @Log("发票下载")
    @ApiOperation("发票下载")
    @GetMapping("downloadInvoice")
    public Result downloadInvoice(@RequestParam("ficoPdfUrl") String ficoPdfUrl) {
        return ResultUtil.success(payingInvoiceService.downloadInvoice(ficoPdfUrl));
    }

    @Log("APP付款")
    @ApiOperation("APP付款")
    @PostMapping("appPay")
    public Result appPay(@Valid @RequestBody SavePayingRequestDTO dto) throws Exception {
        return payingInvoiceService.appPay(dto);
    }

    /**
     * APP支付回调
     */
    @Log("app支付回调")
    @ApiOperation("app支付回调")
    @PostMapping("appPayResult")
    public void appPayResult(HttpServletRequest httpServletRequest) {
        payingInvoiceService.appPayResult(httpServletRequest);
    }

    @Log("支付单价")
    @ApiOperation("支付单价")
    @GetMapping("payPrice")
    public Result payPrice() {
        return payingInvoiceService.payPrice();
    }

    @Log("支付回调事务")
    @ApiOperation("支付回调事务")
    @GetMapping("payCallback")
    public void payCallback() throws Exception {
        payingInvoiceService.payCallback();
    }

    @Log("生成发票")
    @ApiOperation("生成发票")
    @GetMapping("initInvoice")
    public void initInvoice() throws Exception {
        payingInvoiceService.initInvoice();
    }

    @Log("发票下载")
    @ApiOperation("发票下载")
    @GetMapping("/{key}")
    public void download(HttpServletResponse response, @PathVariable String key) throws Exception {
        payingInvoiceService.download(response, key);
    }

    @Log("购买短信")
    @ApiOperation("购买短信")
    @PostMapping("buySms")
    public Result buySms(@RequestJson(value = "gsrrRechargeCount", name = "购买条数") Integer gsrrRechargeCount,
                         @RequestJson(value = "gsrrRechargeAmt", name = "购买金额") Integer gsrrRechargeAmt) {
        return payingInvoiceService.buySms(gsrrRechargeCount, gsrrRechargeAmt);
    }
}

