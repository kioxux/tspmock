package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaPoHeader;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description: 采购计划
 * @author: yzf
 * @create: 2021-12-02 13:12
 */

@Data
public class PurchasePlanReviewVo extends GaiaPoHeader {

    /**
     * 单据行数
     */
    private String lineNum;

    /**
     * 单据总数量
     */
    private BigDecimal qtySum;

    /**
     * 单据总金额
     */
    private BigDecimal amtSum;

    /**
     * 采购主体名称
     */
    private String poCompanyName;

    /**
     * 创建人名称
     */
    private String createByName;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 审批人名称
     */
    private String poApproveName;

    /**
     * 明细
     */
    private List<PurchasePlanReviewDetailVo> detailList;
}
