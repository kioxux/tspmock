package com.gov.purchase.module.priceCompare.vo;

import com.gov.purchase.entity.GaiaSspPriceCompareInfo;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PriceCompareSupplierInfoVo {


    private Long priceCompareProId;
    private Long priceCompareSupplierId;

    /**
     * 供应商CODE
     */
    private String supplierCode;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 价格ID
     */
    private Long priceCompareInfoId;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 最终价格
     */
    private BigDecimal finalPrice;


    /**
     * CLient
     */
    private  String client;

    /**
     * CLient
     */
    private  String remarks;

    /**
     * 总价格
     */
    private BigDecimal totalPrice;

    /**
     * 付款条款
     */
    private String supPayTerm;

}
