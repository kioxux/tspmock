package com.gov.purchase.module.base.service.impl.supplierImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaSupplierBasic;
import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.entity.GaiaSupplierBusinessKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaSupplierBasicMapper;
import com.gov.purchase.mapper.GaiaSupplierBusinessMapper;
import com.gov.purchase.module.base.dto.excel.Supplier;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.30
 */
@Service
public class SupplierImportAddImpl extends SupplierImport {

    @Resource
    GaiaSupplierBasicMapper gaiaSupplierBasicMapper;

    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;

    /**
     * 业务验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap) {
        return super.errorListSupplier((LinkedHashMap<Integer, Supplier>) dataMap);
    }

    /**
     * 主数据表验证
     *
     * @param supplier
     * @param errorList
     * @param key
     */
    protected void baseCheck(Supplier supplier, List<String> errorList, int key) {
    }

    /**
     * 业务数据验证
     *
     * @param supplier
     * @param errorList
     * @param key
     */
    protected void businessCheck(Supplier supplier, List<String> errorList, int key) {
        // 加盟商 供应商自编码 存在验证
        List<GaiaSupplierBusiness> businessList = gaiaSupplierBusinessMapper.selfOnlyCheck(supplier.getClient(), supplier.getSupSelfCode(), supplier.getSupSite());
        if (CollectionUtils.isNotEmpty(businessList)) {
            errorList.add(MessageFormat.format(ResultEnum.E0132.getMsg(), key + 1));
        }
    }

    /**
     * 数据验证完成
     *
     * @return
     */
    @Override
    protected void checkEnd(String path) {
        // 生成导入日志
        createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.WAIT.getCode());
    }

    /**
     * 数据导入完成
     *
     * @param bulUpdateCode
     * @param map
     * @param <T>
     */
    @Override
    @Transactional
    protected <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> map) {
        // 批量导入
        try {
            for (Integer key : map.keySet()) {
                // 行数据
                //SupplierBasic表
                Supplier supplier = (Supplier) map.get(key);
                GaiaSupplierBasic gaiaSupplierBasic = new GaiaSupplierBasic();
                BeanUtils.copyProperties(supplier, gaiaSupplierBasic);
                //供应商状态
                gaiaSupplierBasic.setSupStatus(supplier.getSupStatusValue());
                // 清空数据处理
                super.initNull(gaiaSupplierBasic);
                //供应商编码生成
                String supCode = gaiaSupplierBasicMapper.getSupCode();
                gaiaSupplierBasic.setSupCode(supCode);
                gaiaSupplierBasicMapper.insertSelective(gaiaSupplierBasic);

                // 业务表数据存在判断
                if (!super.isNotBlank(supplier.getClient())) {
                    continue;
                }
                //SupplierBusiness表
                GaiaSupplierBusiness gaiaSupplierBusiness = new GaiaSupplierBusiness();
                BeanUtils.copyProperties(supplier, gaiaSupplierBusiness);
                // 供应商状态
                gaiaSupplierBusiness.setSupStatus(supplier.getSupplierStatusValue());
                // 禁止采购
                gaiaSupplierBusiness.setSupNoPurchase(supplier.getSupNoPurchaseValue());
                // 禁止退厂
                gaiaSupplierBusiness.setSupNoSupplier(supplier.getSupNoSupplierValue());
                // 清空数据处理
                super.initNull(gaiaSupplierBusiness);
                //供应商编码生成
                gaiaSupplierBusiness.setSupCode(supCode);
                // 匹配状态
                gaiaSupplierBusiness.setSupMatchStatus(CommonEnum.MatchStatus.EXACTMATCH.getCode());
                gaiaSupplierBusinessMapper.insertSelective(gaiaSupplierBusiness);
            }
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

        // 更新导入日志
        editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.COMPLETE.getCode());

        return ResultUtil.success();
    }
}

