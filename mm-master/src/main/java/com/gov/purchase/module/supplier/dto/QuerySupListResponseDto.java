package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaSupplierBasic;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "厂商库搜索返回参数")
public class QuerySupListResponseDto extends GaiaSupplierBasic {

    private String sell;

    private String client;
}
