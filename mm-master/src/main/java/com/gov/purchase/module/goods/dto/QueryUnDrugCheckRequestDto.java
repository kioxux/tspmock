package com.gov.purchase.module.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@ApiModel(value = "非药新建校验传入参数")
public class QueryUnDrugCheckRequestDto {

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 验证类型
     */
    @ApiModelProperty(value = "验证类型", name = "type", required = true)
    @NotBlank(message = "验证类型不能为空")
    private String type;

    /**
     * 通用名称
     */
    @ApiModelProperty(value = "通用名称", name = "proCommonname", required = true)
    @NotBlank(message = "通用名称不能为空")
    private String proCommonname;

    /**
     * 商品描述
     */
    @ApiModelProperty(value = "商品描述", name = "proDepict", required = true)
    private String proDepict;

    /**
     * 助记码
     */
    @ApiModelProperty(value = "助记码", name = "proPym", required = true)
    @NotBlank(message = "助记码不能为空")
    private String proPym;

    /**
     * 商品名
     */
    @ApiModelProperty(value = "商品名", name = "proName", required = true)
    private String proName;

    /**
     * 规格
     */
    @ApiModelProperty(value = "规格", name = "proSpecs", required = true)
    @NotBlank(message = "规格不能为空")
    private String proSpecs;

    /**
     * 计量单位
     */
    @ApiModelProperty(value = "计量单位", name = "proUnit", required = true)
    @NotBlank(message = "计量单位不能为空")
    private String proUnit;

    /**
     * 剂型
     */
    // @ApiModelProperty(value = "剂型", name = "proForm", required = true)
    private String proForm;

    /**
     * 细分剂型
     */
    private String proPartform;

    /**
     * 最小剂量（以mg/ml计算）
     */
    private String proMindose;

    /**
     * 总剂量（以mg/ml计算）
     */
    private String proTotaldose;

    /**
     * 国际条形码1
     */
    private String proBarcode;

    /**
     * 国际条形码2
     */
    private String proBarcode2;

    /**
     * 批准文号分类
     */
    @ApiModelProperty(value = "批准文号分类", name = "proRegisterClass", required = true)
    private String proRegisterClass;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 批准文号批准日期
     */
    private String proRegisterDate;

    /**
     * 批准文号失效日期
     */
    private String proRegisterExdate;

    /**
     * 商品分类
     */
    @ApiModelProperty(value = "商品分类", name = "proClass", required = true)
    @NotBlank(message = "商品分类不能为空")
    private String proClass;

    /**
     * 商品分类描述
     */
    @ApiModelProperty(value = "商品分类描述", name = "proClassName", required = true)
    private String proClassName;

    /**
     * 成分分类
     */
    @ApiModelProperty(value = "成分分类", name = "proCompclass", required = true)
    private String proCompclass;

    /**
     * 成分分类描述
     */
    @ApiModelProperty(value = "成分分类描述", name = "proCompclassName", required = true)
    private String proCompclassName;

    /**
     * 处方类别
     */
    @ApiModelProperty(value = "处方类别", name = "proPresclass", required = true)
    private String proPresclass;

    /**
     * 生产企业代码
     */
    private String proFactoryCode;

    /**
     * 生产企业
     */
    @ApiModelProperty(value = "生产企业", name = "proFactoryName", required = true)
    @NotBlank(message = "生产企业不能为空")
    private String proFactoryName;

    /**
     * 商标
     */
    private String proMark;

    /**
     * 品牌标识名
     */
    private String proBrand;

    /**
     * 品牌区分
     */
    private String proBrandClass;

    /**
     * 保质期
     */
    // @ApiModelProperty(value = "保质期", name = "proLife", required = true)
    private String proLife;

    /**
     * 保质期单位
     */
    // @ApiModelProperty(value = "保质期单位", name = "proLifeUnit", required = true)
    private String proLifeUnit;

    /**
     * 上市许可持有人
     */
    private String proHolder;

    /**
     * 进项税率
     */
    @ApiModelProperty(value = "进项税率", name = "proInputTax", required = true)
    @NotBlank(message = "进项税率不能为空")
    private String proInputTax;

    /**
     * 销项税率
     */
    @ApiModelProperty(value = "销项税率", name = "proOutputTax", required = true)
    @NotBlank(message = "销项税率不能为空")
    private String proOutputTax;

    /**
     * 药品本位码
     */
    private String proBasicCode;

    /**
     * 税务分类编码
     */
    private String proTaxClass;

    /**
     * 管制特殊分类
     */
    private String proControlClass;

    /**
     * 生产类别
     */
    private String proProduceClass;

    /**
     * 贮存条件
     */
    @ApiModelProperty(value = "贮存条件", name = "proStorageCondition", required = true)
    @NotBlank(message = "贮存条件不能为空")
    private String proStorageCondition;

    /**
     * 商品仓储分区
     */
    @ApiModelProperty(value = "商品仓储分区", name = "proStorageArea", required = true)
    @NotBlank(message = "商品仓储分区不能为空")
    private String proStorageArea;

    /**
     * 长（以MM计算）
     */
    private String proLong;

    /**
     * 宽（以MM计算）
     */
    private String proWide;

    /**
     * 高（以MM计算）
     */
    private String proHigh;

    /**
     * 中包装量
     */
    @ApiModelProperty(value = "中包装量", name = "proMidPackage", required = true)
    private String proMidPackage;

    /**
     * 大包装量
     */
    private String proBigPackage;

    /**
     * 启用电子监管码
     */
    @ApiModelProperty(value = "启用电子监管码", name = "proElectronicCode", required = true)
    // @NotBlank(message = "启用电子监管码不能为空")
    private String proElectronicCode;

    /**
     * 生产经营许可证号
     */
    private String proQsCode;

    /**
     * 最大销售量
     */
    private String proMaxSales;

    /**
     * 说明书内容
     */
    private String proInstruction;

    /**
     * 说明书代码
     */
    private String proInstructionCode;

    /**
     * 国家医保品种
     */
    private String proMedProdct;

    /**
     * 国家医保品种编码
     */
    private String proMedProdctcode;

    /**
     * 生产国家
     */
    private String proCountry;

    /**
     * 产地
     */
    @ApiModelProperty(value = "产地", name = "proPlace", required = true)
    private String proPlace;

    /**
     * 可服用天数
     */
    private String proTakeDays;

    /**
     * 用法用量
     */
    private String proUsage;

    /**
     * 禁忌说明
     */
    private String proContraindication;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "proSite", required = true)
    @NotBlank(message = "地点不能为空")
    private String proSite;

    /**
     * 商品自编码
     */
    @ApiModelProperty(value = "商品自编码", name = "proSelfCode", required = true)
    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;

    /**
     * 商品定位
     */
    private String proPosition;

    /**
     * 商品状态
     */
    private String proStatus;

    /**
     * 禁止销售
     */
    private String proNoRetail;

    /**
     * 禁止采购
     */
    private String proNoPurchase;

    /**
     * 禁止配送
     */
    private String proNoDistributed;

    /**
     * 禁止退厂
     */
    private String proNoSupplier;

    /**
     * 禁止退仓
     */
    private String proNoDc;

    /**
     * 禁止调剂
     */
    private String proNoAdjust;

    /**
     * 禁止批发
     */
    private String proNoSale;

    /**
     * 禁止请货
     */
    private String proNoApply;

    /**
     * 是否拆零
     */
    private String proIfpart;

    /**
     * 拆零单位
     */
    private String proPartUint;

    /**
     * 拆零比例
     */
    private String proPartRate;

    /**
     * 采购单位
     */
    private String proPurchaseUnit;

    /**
     * 采购比例
     */
    private String proPurchaseRate;

    /**
     * 采购单位
     */
    private String proSaleUnit;

    /**
     * 采购比例
     */
    private String proSaleRate;

    /**
     * 最小订货量
     */
    private BigDecimal proMinQty;

    /**
     * 是否医保
     */
    private String proIfMed;

    /**
     * 销售级别
     */
    private String proSlaeClass;

    /**
     * 限购数量
     */
    private BigDecimal proLimitQty;

    /**
     * 中药规格
     */
    private String proTcmSpecs;

    /**
     * 中药批准文号
     */
    private String proTcmRegisterNo;

    /**
     * 中药生产企业代码
     */
    private String proTcmFactoryCode;

    /**
     * 中药产地
     */
    private String proTcmPlace;

    /**
     * 含麻最大配货量
     */
    private BigDecimal proMaxQty;

    /**
     * 按中包装量倍数配货
     */
    private String proPackageFlag;

    /**
     * 是否重点养护
     */
    private String proKeyCare;

    /**
     * 固定货位
     */
    private String proFixBin;

    /**
     * 参考进货价
     */
    private BigDecimal proCgj;

    /**
     * 参考零售价
     */
    private BigDecimal proLsj;

    /**
     * 医保刷卡数量
     */
    private String proMedQty;

    /**
     * 医保编码
     */
    private String proMedId;

    /**
     * 不打折商品
     */
    private String proBdz;

    /**
     * 服药剂量单位
     */
    private String proDoseUnit;
    /**
     * 经营管理类别
     */
    private String proJygllb;
    /**
     * 档案号
     */
    private String proDah;
    /**
     * 是否发送购药提醒
     */
    private String proIfSms;
    /**
     * 备注
     */
    private String proBeizhu;
}
