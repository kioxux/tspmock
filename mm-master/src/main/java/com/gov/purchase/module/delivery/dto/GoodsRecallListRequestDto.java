package com.gov.purchase.module.delivery.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "商品召回查看列表传入参数")
public class GoodsRecallListRequestDto extends Pageable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    /**
     * 连锁公司
     */
    @ApiModelProperty(value = "连锁公司", name = "recChainHead")
    private String recChainHead;

    /**
     * 门店编号
     */
    @ApiModelProperty(value = "门店编号", name = "recStore")
    private String recStore;
    /**
     * 门店编号 更多
     */
    @ApiModelProperty(value = "门店编号更多", name = "recStores")
    private List<String> recStores;

    /**
     * 商品自编号
     */
    @ApiModelProperty(value = "商品自编号", name = "recProCode")
    private String recProCode;
    /**
     * 商品自编号 更多
     */
    @ApiModelProperty(value = "商品自编号 更多", name = "recProCodes")
    private List<String> recProCodes;

    /**
     * 批次号
     */
    @ApiModelProperty(value = "批次号", name = "recBatchNo")
    private String recBatchNo;

    /**
     * 批次号 更多
     */
    @ApiModelProperty(value = "批次号更多", name = "recBatchNos")
    private List<String> recBatchNos;

    /**
     * 供应商编号
     */
    @ApiModelProperty(value = "供应商编号", name = "recSupplier")
    private String recSupplier;

    /**
     * 供应商编号 更多
     */
    @ApiModelProperty(value = "供应商编号 更多", name = "recSuppliers")
    private List<String> recSuppliers;
}