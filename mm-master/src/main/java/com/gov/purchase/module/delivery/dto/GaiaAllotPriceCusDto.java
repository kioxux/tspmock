package com.gov.purchase.module.delivery.dto;

import com.gov.purchase.entity.GaiaAllotPriceCus;
import com.gov.purchase.module.purchase.dto.GetProductListResponseDto;
import com.gov.purchase.module.purchase.dto.customerDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class GaiaAllotPriceCusDto extends GaiaAllotPriceCus {

    /**
     * 商品gqge
     */
    private List<GetProductListResponseDto> goodsList;
    /**
     * 客户列表
     */
    private List<customerDto> customerList;
    //客户名称
    private String alpCusName;
    //商品描述
    private String proDepict;
    //规格
    private String proSpecs;
    //单位
    private String proUnit;
    //厂家
    private String proFactName;
    //地点名称
    private String siteName;
    //零售价
    private BigDecimal priceNormal;
    //仓库成本价
    private BigDecimal costPrice;
}
