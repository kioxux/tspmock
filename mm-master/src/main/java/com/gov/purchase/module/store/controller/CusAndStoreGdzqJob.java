package com.gov.purchase.module.store.controller;

import com.gov.purchase.module.store.service.GdzqJobService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 固定到期号自动变更客户和门店欠款标志
 */
@Slf4j
@Component
public class CusAndStoreGdzqJob {

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Resource
    private GdzqJobService gdzqJobService;

    /**
     * 比对已完成、已过期的铺货计划是否存在差异
     */
    @XxlJob("autoExecModArrearsFlag")
    public void autoExecModArrearsFlag() {
        String jobParam = XxlJobHelper.getJobParam();
        log.info(String.format("<固定到期号><自动变更欠款标志任务调用成功，param=%s>", jobParam));
        threadPoolTaskExecutor.execute(() -> gdzqJobService.autoExecModArrearsFlag());
        XxlJobHelper.handleSuccess();
    }
}
