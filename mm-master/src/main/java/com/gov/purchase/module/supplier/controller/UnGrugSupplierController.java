package com.gov.purchase.module.supplier.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.supplier.dto.QueryUnDrugSupCheckRequestDto;
import com.gov.purchase.module.supplier.service.UnDrugSupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "非药品供应商新建")
@RestController
@RequestMapping("supplier")
public class UnGrugSupplierController {

    @Resource
    private UnDrugSupplierService unGrugService;

    /*
     * 非药供应商新建校验
     *
     * @param dto QueryUnDrugSupCheckRequestDto
     * @return Result
     */
    @Log("非药供应商校验")
    @ApiOperation("非药供应商校验")
    @PostMapping("unDrugSupCheck")
    public Result resultUnDrugSupCheckInfo(@Valid @RequestBody QueryUnDrugSupCheckRequestDto queryUnDrugSupCheckRequestDto) {
        //非药供应商新建校验
        unGrugService.resultUnDrugSupCheckInfo(queryUnDrugSupCheckRequestDto);
        return ResultUtil.success();
    }

    /*
     * 非药供应商新建
     *
     * @param dto QueryUnDrugSupCheckRequestDto
     * @return
     */
    @Log("非药供应商新建")
    @ApiOperation("非药供应商新建")
    @PostMapping("unDrugSupAdd")
    public Result resultUnDrugSupAddInfo(@Valid @RequestBody QueryUnDrugSupCheckRequestDto queryUnDrugAddRequestDto) {
        //非药供应商新建
        unGrugService.insertUnDrugSupAddInfo(queryUnDrugAddRequestDto);
        return ResultUtil.success();
    }
}
