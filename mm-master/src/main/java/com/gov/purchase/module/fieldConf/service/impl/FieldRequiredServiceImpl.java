package com.gov.purchase.module.fieldConf.service.impl;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaFieldRequiredConfKey;
import com.gov.purchase.entity.GaiaFieldRequiredData;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaFieldRequiredConfMapper;
import com.gov.purchase.mapper.GaiaFieldRequiredDataMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.fieldConf.dto.FieldRequiredConfDTO;
import com.gov.purchase.module.fieldConf.service.FieldRequiredService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description: 首营字段配置以及必填字段
 * @author: yzf
 * @create: 2021-12-08 10:01
 */
@Service
public class FieldRequiredServiceImpl implements FieldRequiredService {

    @Resource
    private GaiaFieldRequiredDataMapper gaiaFieldRequiredDataMapper;

    @Resource
    private GaiaFieldRequiredConfMapper gaiaFieldRequiredConfMapper;

    @Resource
    private FeignService feignService;

    @Override
    public Result queryFieldRequiredDataList() {
        List<GaiaFieldRequiredData> fieldRequiredDataList = gaiaFieldRequiredDataMapper.queryFieldRequiredDataList();
        Map<String, List<GaiaFieldRequiredData>> fieldList = fieldRequiredDataList.stream().collect(Collectors.groupingBy(GaiaFieldRequiredData::getGfrdType));
        return ResultUtil.success(fieldList);
    }

    @Override
    public Result queryFieldRequiredConfList() {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaFieldRequiredConfKey> fieldRequiredData = gaiaFieldRequiredConfMapper.queryFieldRequiredConfList(user.getClient());
        Map<String, List<GaiaFieldRequiredConfKey>> fieldList = fieldRequiredData.stream().collect(Collectors.groupingBy(GaiaFieldRequiredConfKey::getGfrcType));
        return ResultUtil.success(fieldList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateFieldRequiredConf(FieldRequiredConfDTO fieldConfDTO) {
        TokenUser user = feignService.getLoginInfo();
        gaiaFieldRequiredConfMapper.deleteFieldConf(user.getClient(), fieldConfDTO.getType());
        List<GaiaFieldRequiredConfKey> confKeyList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(fieldConfDTO.getField())) {
            fieldConfDTO.getField().forEach(
                    item -> {
                        GaiaFieldRequiredConfKey confKey = new GaiaFieldRequiredConfKey();
                        confKey.setClient(user.getClient());
                        confKey.setGfrcType(fieldConfDTO.getType());
                        confKey.setGfrcField(item);
                        confKeyList.add(confKey);
                    }
            );
            gaiaFieldRequiredConfMapper.insertBatch(confKeyList);
        }
    }
}
