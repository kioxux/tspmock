package com.gov.purchase.module.wholesale.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 供应商应付明细清单表
 * </p>
 *
 * @author QiCheng.Wang
 * @since 2021-08-25
 */
@Data
@Accessors(chain = true)
public class GaiaBillOfApVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "加盟商")
    private String client;

    @ApiModelProperty(value = "DC编码")
    private String dcCode;

    @ApiModelProperty(value = "应付方式编码")
    private String paymentId;

    @ApiModelProperty(value = "发生金额")
    private BigDecimal amountOfPayment;


    @ApiModelProperty(value = "去税金额")
    private BigDecimal amountExcludingTax;

    @ApiModelProperty(value = "税额")
    private BigDecimal amountOfTax;

    @ApiModelProperty(value = "备注")
    private String remarks;

    @ApiModelProperty(value = "发生日期")
    private String paymentDate;

    @ApiModelProperty(value = "供应商自编码")
    private String supSelfCode;

    @ApiModelProperty(value = "供应商编码")
    private String supCode;

    @ApiModelProperty(value = "供应商名称")
    private String supName;

    @ApiModelProperty(value = "业务员")
    private String salesmanName;

    @ApiModelProperty(value = "业务员ID")
    private String salesmanId;

}
