package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "商品列表返回参数")
public class GetProductListResponseDto {

    /**
     * 国际条形码
     */
    private String proBarcode;

    /**
     * 国际条形码2
     */
    private String proBarcode2;

    /**
     * 值
     */
    private String value;

    /**
     * 表示名
     */
    private String label;

    /**
     * 采购单位
     */
    private String proPurchaseUnit;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 进项税率
     */
    private String proInputTax;

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 商品描述
     */
    private String proDepict;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    //存储条件
    private String proStorageCondition;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 库存
     */
    private BigDecimal storeNum;

    /**
     * 定位
     */
    private String proPosition;

    /**
     * 商品名
     */
    private String proNameExt;

    /**
     * 进货价
     */
    private BigDecimal poPrice;

    /**
     * 通用名
     */
    private String proCommonname;

    /**
     * 助记码
     */
    private String proPym;

    /**
     * PRO_FORM
     */
    private String proForm;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 禁止采购
     */
    private String proNoPurchase;

    private String proSite;

    private String proOutputTax;
    /**
     * 批准文号
     */
    private String proRegisterNo;

    //末次进价
    private BigDecimal lastProPrice;

    //零售价
    private BigDecimal gsppPriceNormal;

    //末次供应商
    private String lastSupplierCode;
    //7天销售量
    private Integer sales7;
    //30天销售量
    private Integer sales30;
    //中包装量
    private Integer proMidPackage;
    //末次日期
    private String lastDate;
    //门店库存
    private Integer gssQty;
    //补货供应商
    private String supplierCode;
    private String supplierId;
    //采购单价
    private BigDecimal proPrice;
    //成本价
    private BigDecimal costPrice;
    //是否医保 0 1
    private String proIfMed;
    //销售级别
    private String proSalesClass;
    // 零售价
    private BigDecimal proLsj;
    // 国批价
    private BigDecimal proGpj;
    // 预设售价1
    private BigDecimal proYsj1;
    // 预设售价2
    private BigDecimal proYsj2;
    // 预设售价3
    private BigDecimal proYsj3;

    /**
     * 大包装
     */
    private String proBigPackage;

    /**
     * 自字义1
     */
    private String proZdy1;
    /**
     * 自字义2
     */
    private String proZdy2;
    /**
     * 自字义3
     */
    private String proZdy3;
    /**
     * 自字义4
     */
    private String proZdy4;
    /**
     * 自字义5
     */
    private String proZdy5;

    /**
     * 仓库库存
     */
    private BigDecimal dcStockTotal;

    /**
     * 在途量
     */
    private BigDecimal trafficCount;

    /**
     * 仓库库存
     */
    private BigDecimal wmKcsl;

    /**
     * 毛利率
     */
    private BigDecimal proMll;

    /**
     * 上次采购价
     */
    private BigDecimal lastPoPrice;

    /**
     * 仓库对应门店月销之和
     */
    private Integer salesThirty;
}