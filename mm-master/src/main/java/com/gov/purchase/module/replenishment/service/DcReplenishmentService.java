package com.gov.purchase.module.replenishment.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.replenishment.dto.*;

import java.util.List;

/**
 * DC补货
 */
public interface DcReplenishmentService {

    /**
     * DC补货列表
     *
     * @return
     */
    PageInfo queryReplenishmentList(String dcCode, Integer pageNum, Integer pageSize);
//    List<DcReplenishmentListResponseDto> queryReplenishmentList(String dcCode);

    /**
     * 历史供应商列表
     *
     * @param dto
     * @return
     */
    List<SupplierResponseDto> querySupplierHistoryList(SupplierHistoryRequestDto dto);

    /**
     * 选择供应商列表
     *
     * @param dto
     * @return
     */
    List<SupplierResponseDto> querySupplierList(SupplierRequestDto dto);

    /**
     * 保存DC补货信息
     *
     * @param list
     * @return
     */
    Result saveDcReplenishmentInfo(List<DcReplenishmentRequestDto> list, String... params);

    /**
     * DC补货列表导出
     *
     * @param dcCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result exportReplenishmentList(String dcCode, Integer pageNum, Integer pageSize);

    /**
     * 商品大类列表
     */
    List<GaiaProductClassDTO> getProductClass();
}
