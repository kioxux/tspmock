package com.gov.purchase.module.base.dto.businessImport;

import lombok.Data;

@Data
public class StoreDistributionRequestDto {

    private String client;

    private String stoChainHead;

    private String proSelfCode;

    private String siteCode;
}
