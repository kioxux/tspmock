package com.gov.purchase.module.base.dto.feign;

import lombok.Data;

@Data
public class CustomerGspFeignDto {

    private String cusCode;
    private String cusName;
    private String cusCreditCode;
    private String cusCreditDate;
    private String cusLegalPerson;
    private String cusRegAdd;
    private String cusLicenceNo;
    private String cusLicenceOrgan;
    private String cusLicenceDate;
    private String cusLicenceValid;
    private String cusPostalCode;
    private String cusTaxCard;
    private String cusOrgCard;
    private String cusAccountName;
    private String cusAccountBank;
    private String cusBankAccount;
    private String cusDocState;
    private String cusSealState;
    private String cusScope;
    private String cusOperationMode;
    private String wfSite;
    /**
     * 客户自编码
     */
    private String cusSelfCode;
}
