package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel(value = "发票登记查询传入参数")
public class InvoiceRegisterListRequestDto extends Pageable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 供应商
     */
    @ApiModelProperty(value = "供应商", name = "poSupplierId")
    private String poSupplierId;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "poCompanyCode")
    private String poCompanyCode;

    /**
     * 采购订单号
     */
    @ApiModelProperty(value = "采购订单号", name = "matPo_id")
    private String matPo_id;

    /**
     * 采购订单号更多
     */
    @ApiModelProperty(value = "采购订单号更多", name = "matPo_ids")
    private List<String> matPo_ids;

    /**
     * 发票号
     */
    @ApiModelProperty(value = "发票号", name = "invoiceNum")
    private String invoiceNum;

    /**
     * 发票号更多
     */
    @ApiModelProperty(value = "发票号更多", name = "invoiceNums")
    private List<String> invoiceNums;

    /**
     * 入库日期开始
     */
    @ApiModelProperty(value = "入库日期开始", name = "matPostDateStart")
    private String matPostDateStart;

    /**
     * 入库日期结束
     */
    @ApiModelProperty(value = "入库日期结束", name = "matPostDateEnd")
    private String matPostDateEnd;
}