package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.util.List;

/**
 * @description: 批发订单按货主管控功能
 * @author: yzf
 * @create: 2021-11-23 14:11
 */
@Data
public class WholesaleConsignorDTO {

    private String client;

    /**
     * 批发仓库
     */
    private String dcCode;

    /**
     * 客户编码
     */
    private String customerCode;

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 货主名称
     */
    private String ownerName;

    /**
     * 状态
     */
    private String status;

    private Integer pageSize;

    private Integer pageNum;

    /**
     * 商品规则
     */
    private String proRule;

    /**
     * 客户规则
     */
    private String cusRule;

    /**
     * 电话
     */
    private String ownerTel;

    /**
     * 销售员
     */
    private String ownerSaleman;

    /**
     * 货主编码
     */
    private String owner;

    /**
     * 客户列表
     */
    List<OwnerCustomerVO> customerList;

    /**
     * 商品列表
     */
    List<OwnerProductVO> productList;

}
