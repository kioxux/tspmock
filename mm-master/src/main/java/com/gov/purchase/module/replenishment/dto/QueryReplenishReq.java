package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.common.entity.Pageable;
import com.gov.purchase.common.request.RequestJson;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.06.02
 */
@Data
public class QueryReplenishReq extends Pageable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;
    /**
     * 门店
     */
    @ApiModelProperty(value = "门店", name = "gsrhBrId")
    private String gsrhBrId;

    /**
     * 门店
     */
    @ApiModelProperty(value = "门店", name = "gsrhBrIds")
    private List<String> gsrhBrIds;
    /**
     * 补货单号
     */
    private String gsrhVoucherId;
    /**
     * 补货日期
     */
    private String gsrhDate;
}
