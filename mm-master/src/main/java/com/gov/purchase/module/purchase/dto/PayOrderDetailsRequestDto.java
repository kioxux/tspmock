package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "付款单明细传入参数")
public class PayOrderDetailsRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "payCompanyCode", required = true)
    @NotBlank(message = "地点不能为空")
    private String payCompanyCode;

    /**
     * 付款单号
     */
    @ApiModelProperty(value = "付款单号", name = "payOrderId", required = true)
    @NotBlank(message = "付款单号不能为空")
    private String payOrderId;

    /**
     * 付款申请单行号
     */
    @ApiModelProperty(value = "付款申请单行号", name = "payOrderLineno", required = true)
    @NotBlank(message = "付款申请单行号不能为空")
    private String payOrderLineno;
}