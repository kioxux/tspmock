package com.gov.purchase.module.blacklist.service;


import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaCustomerBasic;
import com.gov.purchase.module.blacklist.dto.BlackListRequestDto;
import com.gov.purchase.module.blacklist.dto.BlackListResponseDto;
import com.gov.purchase.module.customer.dto.*;

import java.util.List;

public interface BlackListService {

    Result saveBlackList(BlackListRequestDto dto);

    Result deleteBatchBlackList(List<BlackListRequestDto> dtoList);

    List<BlackListResponseDto> listBlack(BlackListRequestDto dto);

    Result exportBlackList(BlackListRequestDto dto);
}
