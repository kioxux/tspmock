package com.gov.purchase.module.store.dto.store;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
@ApiModel(value = "门店信息更新或者新增验证传入参数")
public class StoreInfoInsertOrUpdateValidDto extends StoreInfoInsertOrUpdateRequestDto {

    @ApiModelProperty(value = "标识（0-新建，1-更新）", name = "type")
    @NotBlank(message ="缺少请求类型")
    private String type;

}
