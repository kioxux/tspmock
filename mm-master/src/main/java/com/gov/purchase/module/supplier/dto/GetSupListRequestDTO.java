package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class GetSupListRequestDTO extends Pageable {
    /**
     * 连锁总部id
     */
    private String compadmId;
    /**
     * 地点
     */
    private String proSite;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 统一社会信用代码
     */
    private String supCreditCode;

    /**
     * 供应商自编码
     */
    private String supSelfCode;
    /**
     * 供应商自编码列表
     */
    private List<String> supSelfCodes;

    /**
     * 加盟商（sql查询处使用）
     */
    private String client;

    /**
     * 排序字段列表 (格式：supSelfCode desc)
     */
    private String orderItem;

    /**
     * 用户ID
     */
    private String userId;

    private List<ParamItem> paramList;

    @Data
    public static class ParamItem {

        /**
         * 1：相等，2：模糊，3：in
         */
        private Integer type;

        private String field;

        private Object data;
    }
}
