package com.gov.purchase.module.replenishment.service;

import com.gov.purchase.constants.GenerateMessageEnum;
import com.gov.purchase.entity.GaiaNewDistributionPlan;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.WmsRkys;

import java.util.List;
import java.util.Map;
import java.util.Set;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/16 9:42
 **/
public interface DistributionPlanService {

    List<GaiaNewDistributionPlan> findList(GaiaNewDistributionPlan cond);

    List<GaiaNewDistributionPlan> getWaitAutoList(GaiaNewDistributionPlan cond);

    void update(GaiaNewDistributionPlan distributionPlan);

    void confirmCall(GaiaNewDistributionPlan distributionPlan);

    void confirmReplenish(GaiaNewDistributionPlan distributionPlan);

    GaiaNewDistributionPlan getUnique(GaiaNewDistributionPlan gaiaNewDistributionPlan);

    void checkNewProductByWeek();

    void checkDistributionDiff();

    void generateDistributionMessage(String client, Set<String> productList, GenerateMessageEnum messageEnum);

    void execSingleDistributionPlan(WmsRkys wmsRkys, Map<String, Set<String>> noPlanMessageMap, Map<String, Set<String>> notStockMessageMap,String proPosition);

    GaiaProductBusiness getProductInfoByRkys(WmsRkys wmsRkys);
}
