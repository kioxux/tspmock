package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaSdReplenishD;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description: 门店要货订单列表
 * @author: yzf
 * @create: 2021-11-15 11:40
 */
@Data
public class SdReplenishDExtandVO{
    /**
     * 补货日期
     */
    private String gsrhDate;
    /**
     * 加盟商
     */
    private String client;

    /**
     * 补货单号
     */
    private String gsrhVoucherId;
    /**
     * 门店名称
     */
    private String stoName;
    /**
     * 补货方式 0正常补货，1紧急补货,2铺货,3互调
     */
    private String gsrhPattern;
    /**
     * 是否转采购订单
     */
    private String gsrhFlag;
    /**
     * 开单人
     */
    private String gsrhDnBy;

    /**
     * 开单日期
     */
    private String gsrhDnDate;
    /**
     * 备注
     */
    private String gsrdBz;

    /**
     * 审核人
     */
    private String gsrhDnByName;

    /**
     * 补货地点
     */
    private String gsrhAddr;


    private String poId;

    private String gsrhType;

    /**
     * 请货明细列表
     */
   List<SdReplenishDExtandDTO> sdReplenishDExtandDTOList;
}
