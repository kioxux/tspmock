package com.gov.purchase.module.qa.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.qa.dto.QueryPermitDataInfoRequestDto;
import com.gov.purchase.module.qa.dto.QueryPermitDataListRequestDto;
import com.gov.purchase.module.qa.dto.ValidPermitDataInfoRequestDto;
import com.gov.purchase.module.qa.service.PermitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "证照资质")
@RestController
@RequestMapping("permit")
public class PermitController {

    @Resource
    private PermitService permitService;

    @Log("证照资质验证")
    @ApiOperation("证照资质验证")
    @PostMapping("validPermitDataInfo")
    public Result validPermitDataInfo(@Valid @RequestBody List<ValidPermitDataInfoRequestDto> list) {
        return permitService.validPermitDataInfo(list);
    }

    @Log("证照资质提交")
    @ApiOperation("证照资质提交")
    @PostMapping("savePermitDataInfo")
    public Result savePermitDataInfo(@Valid @RequestBody List<ValidPermitDataInfoRequestDto> list) {
        return permitService.savePermitDataInfo(list);
    }

    @Log("证照资质列表")
    @ApiOperation("证照资质列表")
    @PostMapping("queryPermitDataList")
    public Result queryPermitDataList(@RequestBody QueryPermitDataListRequestDto dto) {
        return ResultUtil.success(permitService.queryPermitDataList(dto));
    }

    @Log("证照资质详情")
    @ApiOperation("证照资质详情")
    @PostMapping("getPermitDataInfo")
    public Result getPermitDataInfo(@RequestBody QueryPermitDataInfoRequestDto dto) {
        return ResultUtil.success(permitService.getPermitDataInfo(dto));
    }


}
