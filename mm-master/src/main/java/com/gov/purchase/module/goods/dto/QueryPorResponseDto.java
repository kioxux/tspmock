package com.gov.purchase.module.goods.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "非药新建保存返回参数")
public class QueryPorResponseDto {

    private String proCode;

    private String proCommonname;

    private String proDepict;

    private String proPym;

    private String proName;

    private String proSpecs;

    private String proUnit;

    private String proForm;

    private String proPartform;

    private String proMindose;

    private String proTotaldose;

    private String proBarcode;

    private String proBarcode2;

    private String proRegisterClass;

    private String proRegisterNo;

    private String proRegisterDate;

    private String proRegisterExdate;

    private String proClass;

    private String proClassName;

    private String proCompclass;

    private String proCompclassName;

    private String proPresclass;

    private String proFactoryCode;

    private String proFactoryName;

    private String proMark;

    private String proBrand;

    private String proBrandClass;

    private String proLife;

    private String proLifeUnit;

    private String proHolder;

    private String proInputTax;

    private String proOutputTax;

    private String proBasicCode;

    private String proTaxClass;

    private String proControlClass;

    private String proProduceClass;

    private String proStorageCondition;

    private String proStorageArea;

    private String proLong;

    private String proWide;

    private String proHigh;

    private String proMidPackage;

    private String proBigPackage;

    private String proElectronicCode;

    private String proQsCode;

    private String proMaxSales;

    private String proInstructionCode;

    private String proInstruction;

    private String proMedProdct;

    private String proMedProdctcode;

    private String proCountry;

    private String proPlace;

    private String proTakeDays;

    private String proUsage;

    private String proContraindication;

    private String client;

    private String proSite;

    private String proSiteName;

    private String proSelfCode;

    private String proStatus;

    private String proPosition;

    private String proNoRetail;

    private String proNoPurchase;

    private String proNoDistributed;

    private String proNoSupplier;

    private String proNoDc;

    private String proNoAdjust;

    private String proNoSale;

    private String proNoApply;

    private String proIfpart;

    private String proPartUint;

    private String proPartRate;

    private String proPurchaseUnit;

    private String proPurchaseRate;

    private String proSaleUnit;

    private String proSaleRate;

    private BigDecimal proMinQty;

    private String proIfMed;

    private String proSlaeClass;

    private BigDecimal proLimitQty;

    private String proTcmSpecs;

    private String proTcmRegisterNo;

    private String proTcmFactoryCode;

    private String proTcmPlace;

    private String proMaxQty;

    private String proPackageFlag;

    private String proKeyCare;
}
