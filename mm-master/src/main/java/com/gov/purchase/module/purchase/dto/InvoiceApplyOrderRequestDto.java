package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @Author staxc
 * @Date 2020/10/23 14:04
 * @desc 发票申请
 */
@Data
@ApiModel(value = "发票申请传入参数")
public class InvoiceApplyOrderRequestDto {

    /**
     * 购方企业名称
     */
    private String buyername;

    /**
     * 购方企业税号 企业要填，个人可为空
     */
    private String taxnum;
    /**
     * 购方企业地址 企业要填，个人可为空
     */
    private String address;
    /**
     * 购方企业银行开户行及账号 企业要填，个人可为空
     */
    private String account;
    /**
     * 购方企业电话 企业要填，个人可为空
     */
    private String telephone;

    /**
     * 订单号
     */
    private String orderno;

    /**
     * 单据时间
     */
    private String invoicedate;

    /**
     * 销方企业税号
     */
    private String saletaxnum;


    /**
     * 销方企业银行开户行及账号
     */
    private String saleaccount;

    /**
     * 销方企业电话
     */
    private String salephone;

    /**
     * 销方企业地址
     */
    private String saleaddress;

    /**
     * 发票类型，1:正票;2：红票
     */
    private String kptype;

    /**
     * 开票员
     */
    private String clerk;


    /**
     * 推送方式，-1:不推送;0:邮箱;1:手机(默认);2:邮箱&手机
     */
    private String tsfs;

    /**
     * 推送手机(开票成功会短信提醒购方)
     */
    private String phone;


    /**
     * 清单标志，0:非清单
     * 默认为0
     */
    private String qdbz;
    /**
     * 代开标志，0:非代开;
     */
    private String dkbz;

    /**
     * 发票种类，p:电子增值税普通发票
     * 默认为普通电票p。
     */
    private String invoiceLine;
    /**
     * 成品油标志：0非成品油，1成品油
     * 默认为0非成品油
     */
    private String cpybz;

    /**
     * 发票明细
     */
    private List<InvoiceApplyDetailRequestDto> detail;


}
