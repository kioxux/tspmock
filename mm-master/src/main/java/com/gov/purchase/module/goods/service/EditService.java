package com.gov.purchase.module.goods.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.goods.dto.*;

import java.util.List;
import java.util.Map;


public interface EditService {

    /**
     * 商品列表
     *
     * @param dto QueryProRequestDto
     * @return PageInfo<QueryProResponseDto>
     */
    PageInfo<QueryProResponseDto> selectQueryProInfo(QueryProRequestDto dto);

    /**
     * 商品维护
     *
     * @param dto QueryUnDrugCheckRequestDto
     */
    Map<String, Object> updateProEditInfo(QueryUnDrugCheckRequestDto dto);

    /**
     * 商品详情
     *
     * @param client
     * @param proSite
     * @param proSelfCode
     * @return
     */
    Result queryBusinessPor(String client, String proSite, String proSelfCode);
}
