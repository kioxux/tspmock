package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaSupplierBusiness;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetSupDetailsDTO extends GaiaSupplierBusiness {

    /**
     * 法人委托书图片
     */
    private String supPictureFrwtsUrl;
    /**
     *
     */
    private String supPictureGspUrl;
    /**
     *
     */
    private String supGspUrl;
    /**
     *
     */
    private String supPictureGmpUrl;
    /**
     *
     */
    private String supGmpUrl;
    /**
     *
     */
    private String supPictureShtxdUrl;
    /**
     *
     */
    private String supPictureQtUrl;



    /**
     * 生产许可证书图片
     */
    private String supPictureScxkzUrl;
    /**
     * 生产许可证书
     */
    private String supScxkzUrl;

    /**
     * 食品许可证书图片
     */
    private String supPictureSpxkzUrl;
    /**
     * 食品许可证书
     */
    private String supSpxkzUrl;

    /**
     * 医疗器械经营许可证图片
     */
    private String supPictureYlqxxkzUrl;
    /**
     * 医疗器械经营许可证书
     */
    private String supYlqxxkzUrl;


    /**
     * 法人委托书身份证
     */
    private String supFrwtsSfzUrl;

    /**
     * 样章图片
     */
    private String supSampleSealImageUrl;
    /**
     * 样票图片
     */
    private String supSampleTicketImageUrl;
    /**
     * 样膜图片
     */
    private String supSampleMembraneImageUrl;

}
