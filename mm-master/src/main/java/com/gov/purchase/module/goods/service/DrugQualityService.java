package com.gov.purchase.module.goods.service;

import com.gov.purchase.module.goods.dto.DrugQualityDTO;
import com.gov.purchase.module.goods.dto.DrugQualityVO;

/**
 * 查询药品质量档案
 */
public interface DrugQualityService {


    /**
     * 查询药品质量档案
     *
     * @param drugQualityDTO
     * @return
     */
    DrugQualityVO queryDrugQuality(DrugQualityDTO drugQualityDTO);
}
