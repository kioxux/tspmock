package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "批次库存查询传入参数")
public class BatchStockListRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "batSiteCode", required = true)
    @NotBlank(message = "地点不能为空")
    private String batSiteCode;

    /**
     * 退货库位
     */
    @ApiModelProperty(value = "退货库位", name = "batLocationCode")
    private String batLocationCode;

    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码", name = "batProCode")
    private String batProCode;

    /**
     * 采购供应商编码
     */
    @ApiModelProperty(value = "采购供应商编码", name = "batSupplierCode")
    private String batSupplierCode;

    /**
     * 生产批号
     */
    @ApiModelProperty(value = "生产批号", name = "batBatch")
    private String batBatch;
}