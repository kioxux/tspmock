package com.gov.purchase.module.purchase.service;

import com.gov.purchase.module.purchase.dto.*;

import java.util.List;

public interface ChangePurchaseService {

    /**
     * 批次库存查询
     *
     * @param dto BatchStockListRequestDto
     * @return List<BatchStockListResponseDto>
     */
    List<BatchStockListResponseDto> selectBatchStockList(BatchStockListRequestDto dto);

    /**
     * 物流模式门店
     *
     * @param dto DeliveryStoreRequestDto
     * @return List<DeliveryStoreResponseDto>
     */
    List<DeliveryStoreResponseDto> selectDeliveryStore(DeliveryStoreRequestDto dto);

    /**
     * 供应商列表
     *
     * @param dto SupplierListForReturnsRequestDto
     * @return List<SupplierListForReturnsResponseDto>
     */
    List<SupplierListForReturnsResponseDto> selectSupplierListForReturns(SupplierListForReturnsRequestDto dto);

    /**
     * 采购退货
     *
     * @param dto PurchaseReturnsRequestDto
     * @return int
     */
    void insertPurchaseReturns(PurchaseReturnsRequestDto dto);
}
