package com.gov.purchase.module.purchase.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.purchase.dto.SalesOrderRecordDTO;
import com.gov.purchase.module.purchase.dto.SalesOrderRecordVO;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * @author tl
 */
public interface SalesOrderRecordService {

    /**
     * 查询销售记录明细（或退回记录明细）
     *
     * @param salesOrderRecordDTO
     * @return
     */
    PageInfo<SalesOrderRecordVO> querySalesRecord(SalesOrderRecordDTO salesOrderRecordDTO);

    /**
     * 销售记录导出
     *
     * @param salesOrderRecordDTO
     * @return
     */
    Result exportSalesOrderRecord(SalesOrderRecordDTO salesOrderRecordDTO);

    /**
     * 销售退回记录导出
     *
     * @param salesOrderRecordDTO
     * @return
     */
    Result exportSalesOrderBackRecord(SalesOrderRecordDTO salesOrderRecordDTO);
}
