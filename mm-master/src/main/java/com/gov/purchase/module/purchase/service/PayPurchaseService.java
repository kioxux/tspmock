package com.gov.purchase.module.purchase.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.purchase.dto.*;

import java.util.List;

public interface PayPurchaseService {

    /**
     * 发票登记查询
     *
     * @param dto InvoiceRegisterListRequestDto
     * @return PageInfo<InvoiceRegisterListResponseDto>
     */
    PageInfo<InvoiceRegisterListResponseDto> selectInvoiceRegisterList(InvoiceRegisterListRequestDto dto);

    /**
     * 发票列表查询
     *
     * @param dto InvoiceListRequestDto
     * @return PageInfo<InvoiceListResponseDto>
     */
    PageInfo<InvoiceListResponseDto> selectInvoiceList(InvoiceListRequestDto dto);

    /**
     * 发票明细查询
     *
     * @param dto InvoiceDetailsRequestDto
     * @return InvoiceDetailsResponseDto
     */
    InvoiceDetailsResponseDto selectInvoiceDetails(InvoiceDetailsRequestDto dto);

    /**
     * 付款申请保存
     *
     * @param dto PaymentReqSaveRequestDto
     * @return String
     */
    String insertPaymentReqSave(List<PaymentReqSaveRequestDto> dto);

    /**
     * 付款单查询
     *
     * @param dto PayOrderListRequestDto
     * @return PageInfo<PayOrderListResponseDto>
     */
    PageInfo<PayOrderListResponseDto> selectPayOrderList(PayOrderListRequestDto dto);

    /**
     * 付款单明细
     *
     * @param dto PayOrderDetailsRequestDto
     * @return List<PayOrderDetailsResponseDto>
     */
    List<PayOrderDetailsResponseDto> selectPayOrderDetails(PayOrderDetailsRequestDto dto);

    /**
     * 付款单发送审批
     * @param payOrderId 付款单号
     * @return
     */
    Result sendApproval(String payOrderId);
}
