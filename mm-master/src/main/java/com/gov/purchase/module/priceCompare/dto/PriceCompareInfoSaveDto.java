package com.gov.purchase.module.priceCompare.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class PriceCompareInfoSaveDto {

    /**
     * 供应商CODE
     */
    @NotNull(message = "缺少参数： priceCompareSupplierId")
    private Long priceCompareSupplierId;

    /**
     * 供应商CODE
     */
    private String supplierCode;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 价格
     */
    private BigDecimal price;


    /**
     * 备注
     */
    private String remarks;


}
