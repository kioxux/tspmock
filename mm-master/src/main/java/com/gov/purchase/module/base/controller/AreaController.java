package com.gov.purchase.module.base.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.base.service.AreaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "省市区列表")
@RestController
@RequestMapping("area")
public class AreaController {

    @Resource
    private AreaService areaService;

    @Log("获取省市区信息")
    @ApiOperation("获取省市区信息")
    @GetMapping("getAreaList")
    public Result getAreaList() {
        return areaService.getAreaList();
    }
}
