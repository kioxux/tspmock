package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "批次库存查询返回参数")
public class BatchStockListResponseDto {

    /**
     * 选中
     */
    private String selected;

    /**
     * 地点
     */
    private String batSiteCode;

    /**
     * 地点名
     */
    private String batSiteName;

    /**
     * 商品编码
     */
    private String batProCode;

    /**
     * 品名
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String batFactoryCode;

    /**
     * 生产厂家名
     */
    private String batFactoryName;

    /**
     * 产地
     */
    private String batProPlace;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 单位名
     */
    private String proUnitName;

    /**
     * 批次
     */
    private String batBatch;

    /**
     * 生产批号
     */
    private String batBatchNo;

    /**
     * 效期
     */
    private String batExpiryDate;

    /**
     * 库存地点
     */
    private String batLocationCode;

    /**
     * 库存数量
     */
    private BigDecimal batNormalQty;

    /**
     * 库存金额
     */
    private BigDecimal batAmount;

    /**
     * 可退货数量
     */
    private BigDecimal retractableNum;

    /**
     * 采购单号
     */
    private String batPoId;

    /**
     * 采购单行号
     */
    private String batPoLineno;

    /**
     * 供应商
     */
    private String batSupplierCode;

    /**
     * 供应商名
     */
    private String batSupplierName;

    /**
     * 采购价格
     */
    private BigDecimal batPoPrice;

    /**
     * 税率
     */
    private String poRate;

    /**
     * 税率名称
     */
    private String taxCodeName;

    /**
     * 本次退货数量
     */
    private BigDecimal returnQuantity;
}