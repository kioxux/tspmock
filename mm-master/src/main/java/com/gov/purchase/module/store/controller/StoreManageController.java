package com.gov.purchase.module.store.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.store.dto.store.EditStoDetailsRequestDTO;
import com.gov.purchase.module.store.service.StoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.22
 */
@Api(tags = "门店管理")
@RestController
@RequestMapping("storeManage")
public class StoreManageController {

    @Autowired
    private StoreService storeService;

    @Log("门店列表")
    @ApiOperation("门店列表")
    @PostMapping("queryList")
    public Result queryList(@RequestJson(value = "stoChainHead", name = "连锁总部", required = false) String stoChainHead,
                            @RequestJson(value = "stoCode", name = "门店", required = false) String stoCode,
                            @RequestJson(value = "stoCodeList", name = "门店", required = false) List<String> stoCodeList,
                            @RequestJson(value = "stoName", name = "门店名", required = false) String stoName,
                            @RequestJson(value = "pageNum", name = "分页", required = true) Integer pageNo,
                            @RequestJson(value = "pageSize", name = "分页", required = true) Integer pageSize
    ) {

        return storeService.queryList(stoChainHead, stoCode, stoCodeList, stoName, pageNo, pageSize);
    }

    @Log("有Dc权限的连锁总部下拉选")
    @ApiOperation("有Dc权限的连锁总部下拉选")
    @PostMapping("queryChainList")
    public Result queryChainList() {

        return storeService.queryChainList();
    }

    @Log("有权限的门店下拉选")
    @ApiOperation("有Dc权限的连锁总部下拉选")
    @GetMapping("queryStoreList")
    public Result queryStoreList(String chainCode) {

        return storeService.queryStoreList(chainCode);
    }

    @Log("门店列表导出")
    @ApiOperation("门店列表导出")
    @PostMapping("storeExport")
    public Result storeExport(@RequestJson(value = "stoChainHead", name = "连锁总部", required = false) String stoChainHead,
                              @RequestJson(value = "stoCode", name = "门店", required = false) String stoCode,
                              @RequestJson(value = "stoCodeList", name = "门店", required = false) List<String> stoCodeList,
                              @RequestJson(value = "stoName", name = "门店名", required = false) String stoName) {

        return storeService.exportList(stoChainHead, stoCode, stoCodeList, stoName);
    }

    @Log("门店列表的修改")
    @ApiOperation("门店列表的修改")
    @PostMapping("updateStore")
    public Result updateStore(@RequestBody List<GaiaStoreData> storeDataList) {
        return storeService.updateStoreList(storeDataList);
    }

    @Log("门店配送中心集合")
    @ApiOperation("门店配送中心集合")
    @PostMapping("getDcList")
    public Result getDcList() {
        return storeService.getDcList();
    }

    @Log("门店详情")
    @ApiOperation("门店详情")
    @PostMapping("getStoDetails")
    public Result getStoDetails(@RequestJson(value = "stoCode", name = "门店编码") String stoCode) {
        return ResultUtil.success(storeService.getStoDetails(stoCode));
    }

    @Log("门店编辑")
    @ApiOperation("门店编辑")
    @PostMapping("editStoDetails")
    public Result editStoDetails(@RequestBody EditStoDetailsRequestDTO editStoDetails) {
        storeService.editStoDetails(editStoDetails);
        return ResultUtil.success();
    }

}
