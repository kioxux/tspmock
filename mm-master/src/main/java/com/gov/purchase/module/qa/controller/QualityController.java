package com.gov.purchase.module.qa.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.qa.dto.QueryProductListRequestDto;
import com.gov.purchase.module.qa.dto.QueryStockListResquestDto;
import com.gov.purchase.module.qa.dto.SaveBatchStockInfoResquestDto;
import com.gov.purchase.module.qa.dto.UpdateProductResquestDto;
import com.gov.purchase.module.qa.service.QualityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "质量")
@RestController
@RequestMapping("quality")
public class QualityController {

    @Autowired
    private QualityService qualityService;

    @Log("获取字段列表")
    @ApiOperation("获取字段列表")
    @GetMapping("queryFieldList")
    public Result queryFieldList(){
        return ResultUtil.success(qualityService.queryFieldList());
    }

    @Log("商品状态更新-获取商品编号列表")
    @ApiOperation("商品状态更新-获取商品编号列表")
    @PostMapping("queryProductList")
    public Result queryProductList(@Valid @RequestBody QueryProductListRequestDto dto){
        return ResultUtil.success(qualityService.queryProductList(dto));
    }

    @Log("商品状态更新提交")
    @ApiOperation("商品状态更新提交")
    @PostMapping("updateProduct")
    public Result updateProduct(@RequestBody List<UpdateProductResquestDto> list){
        return ResultUtil.success(qualityService.updateProduct(list));
    }

    @Log("质量管控-批次状态列表")
    @ApiOperation("质量管控-批次状态列表")
    @PostMapping("queryStockList")
    public Result queryStockList(@Valid @RequestBody QueryStockListResquestDto dto){
        return ResultUtil.success(qualityService.queryStockList(dto));
    }

    @Log("质量管控-批次状态提交")
    @ApiOperation("质量管控-批次状态提交")
    @PostMapping("saveBatchStockInfo")
    public Result saveBatchStockInfo(@Valid @RequestBody List<SaveBatchStockInfoResquestDto> list){
        return ResultUtil.success(qualityService.saveBatchStockInfo(list));
    }
}
