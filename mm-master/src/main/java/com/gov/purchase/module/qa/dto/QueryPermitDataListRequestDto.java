package com.gov.purchase.module.qa.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "证照资质列表传入参数")
public class QueryPermitDataListRequestDto extends Pageable {

    @ApiModelProperty(value = "加盟商编号", name = "client" )
    private String client;

    @ApiModelProperty(value = "实体类型", name = "perEntityClass" )
    private String perEntityClass;

    @ApiModelProperty(value = "实体编码", name = "perEntityId" )
    private String perEntityId;

    @ApiModelProperty(value = "实体名称", name = "perEntityName" )
    private String perEntityName;
}
