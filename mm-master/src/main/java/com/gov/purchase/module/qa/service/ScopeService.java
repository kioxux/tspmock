package com.gov.purchase.module.qa.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.qa.dto.*;

import java.util.List;

public interface ScopeService {

    /**
     * 经营范围列表
     *
     * @param dto
     * @return
     */
    PageInfo<QueryScopeListResponseDto> queryBusinessScopeList(QueryScopeListRequestDto dto);


    /**
     * 经营范围验证
     *
     * @param list
     * @return
     */
    Result validBusinessScopeInfo(List<ValidBusinessScopeInfoRequestDto> list);

    /**
     * 经营范围提交
     *
     * @param list
     * @return
     */
    Result saveBusinessScopeInfo(List<ValidBusinessScopeInfoRequestDto> list);

    /**
     * 经营范围详情
     * @param dto
     * @return
     */
    QueryBusinessScopeInfoResponseDto getBusinessScopeInfo(QueryBusinessScopeInfoRequestDto dto);
}
