package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaContractZ;
import lombok.Data;

import java.util.List;

@Data
public class GaiaContractZDetails extends GaiaContractZ {

    /**
     * 采购主体
     */
    private String conCompanyName;

    /**
     * 供应商名
     */
    private String supName;

    /**
     * 业务员名
     */
    private String conSalesmanName;

    /**
     * 创建人
     */
    private String conCreateByName;

    /**
     * 明细
     */
    private List<PurchaseContractDetailVO> details;

    /////////////////////日志表数据/////////////////////////
    private String logClient;
    private String logConCompanyCode;
    private String logConSupplierId;
    private String logConSalesmanCode;
    private String logConDate;
    private String logConId;
    private Integer logConCompileIndex;
    private String logConType;
    private String logConExpiryDateFrom;
    private String logConExpiryDateTo;
    private String logConPaymentTerms;
    private String logConRemark;
    private String logConCreateDate;
    private String logConCreateBy;
    private String logConCreateTime;
    private String logConApproveDate;
    private String logConApproveBy;
    private String logConApproveTime;
    private String logConUpdateDate;
    private String logConUpdateBy;
    private String logConUpdateTime;
}
