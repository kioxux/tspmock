package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author staxc
 * @Date 2020/10/28 10:11
 * @desc
 */
@Data
@ApiModel("纳税主体查询接口传入参数")
public class ListTaxpayerRequestDTO {

    @NotBlank(message = "组织类型不能为空，0:单体,1:连锁,2:批发")
    private String ficoCompanyCode;

    /**
     * 门店编码
     */
    @NotBlank(message = "门店编码不能为空")
    private String ficoStoreCode;

    /**
     * 单体门店类型
     */
    private String[] typeList;

    /**
     * 配送中心类型
     */
    private String type;

    private String client;

    private String chainHead;

}
