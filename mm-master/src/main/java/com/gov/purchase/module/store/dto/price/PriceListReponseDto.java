package com.gov.purchase.module.store.dto.price;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@ApiModel(value = "调价单审批列表返回结果")
public class PriceListReponseDto {

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "门店编码", name = "prcStore")
    private String prcStore;

    @ApiModelProperty(value = "门店名称", name = "prcStoreName")
    private String prcStoreName;

    @ApiModelProperty(value = "商品编码", name = "prcProduct")
    private String prcProduct;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    private String proSelfCode;

    @ApiModelProperty(value = "商品名称", name = "proName")
    private String proName;

    @ApiModelProperty(value = "商品描述", name = "proDepict")
    private String proDepict;

    @ApiModelProperty(value = "商品规格", name = "proSpecs")
    private String proSpecs;

    @ApiModelProperty(value = "价格分类", name = "prcClass")
    private String prcClass;

    @ApiModelProperty(value = "调价单号", name = "prcModfiyNo")
    private String prcModfiyNo;

    @ApiModelProperty(value = "价格", name = "prcAmount")
    private BigDecimal prcAmount;

    @ApiModelProperty(value = "有效期起", name = "prcEffectDate")
    private String prcEffectDate;

    @ApiModelProperty(value = "单位", name = "prcUnit")
    private String prcUnit;

    @ApiModelProperty(value = "单位名称", name = "unitName")
    private String unitName;

    @ApiModelProperty(value = "不允许积分", name = "prcNoIntegral")
    private String prcNoIntegral;

    @ApiModelProperty(value = "不允许积分兑换", name = "prcNoDiscount")
    private String prcNoDiscount;

    @ApiModelProperty(value = "不允许会员卡打折", name = "prcNoExchange")
    private String prcNoExchange;

    @ApiModelProperty(value = "限购数量", name = "prcLimitAmount")
    private String prcLimitAmount;

    @ApiModelProperty(value = "会员价", name = "memberAmount")
    private BigDecimal memberAmount;

    @ApiModelProperty(value = "零售价", name = "retailAmount")
    private BigDecimal retailAmount;

    @ApiModelProperty(value = "医保价", name = "ybAmount")
    private BigDecimal ybAmount;

    @ApiModelProperty(value = "拆零价", name = "clAmount")
    private BigDecimal clAmount;

    @ApiModelProperty(value = "会员日价", name = "memberDayAmount")
    private BigDecimal memberDayAmount;

    @ApiModelProperty(value = "网上零售价", name = "retailOnlineAmount")
    private BigDecimal retailOnlineAmount;

    @ApiModelProperty(value = "网上会员价", name = "memberOnlineAmount")
    private BigDecimal memberOnlineAmount;

    @ApiModelProperty(value = "审批状态", name = "memberOnlineAmount")
    private String prcApprovalSuatus;

    @ApiModelProperty(value = "调价来源", name = "prcSource")
    private String prcSource;

    /**
     * 调价原因
     */
    @ApiModelProperty(value = "调价原因", name = "prcReason")
    private String prcReason;

    /**
     * 发起门店
     */
    @ApiModelProperty(value = "发起门店", name = "prcInitStore")
    private String prcInitStore;

    /**
     * 发起门店名
     */
    @ApiModelProperty(value = "发起门店名", name = "prcInitStoreName")
    private String prcInitStoreName;

    /**
     * 审批人
     */
    private String prcApprovalUserName;

    /**
     * 创建人
     */
    private String prcCreateUserName;

    /**
     * 总部调价
     */
    private String prcHeadPrice;

    /**
     * 供货单位名称
     */
    private String proFactoryName;
}
