package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaOwnerProduct;
import com.gov.purchase.entity.GaiaWholesaleChannelDetail;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaOwnerProductMapper;
import com.gov.purchase.mapper.GaiaProductBasicMapper;
import com.gov.purchase.mapper.GaiaWholesaleChannelMapper;
import com.gov.purchase.module.base.dto.businessImport.WholesaleConsignorPro;
import com.gov.purchase.module.base.dto.businessImport.WholesalePro;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.wholesale.dto.OwnerProductVO;
import com.gov.purchase.utils.DateUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * @author : Yzf
 * description: 批发货主商品明细导入
 * create time: 2021/11/24 11:42
 */
@Service
public class WholeSaleConsignorProImport extends BusinessImport {
    @Resource
    private FeignService feignService;

    @Resource
    private GaiaProductBasicMapper gaiaProductBasicMapper;

    @Resource
    private GaiaOwnerProductMapper gaiaOwnerProductMapper;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        List<String> errorList = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        // 仓库（地点）自编码
        String dcCode = (String) field.get("dcCode");
        if (StringUtils.isBlank(dcCode)) {
            return ResultUtil.error(ResultEnum.OWNER_SITE_EMPTY);
        }
        // 货主编码
        String owner = (String) field.get("owner");
        if (StringUtils.isBlank(owner)) {
            return ResultUtil.error(ResultEnum.OWNER_EMPTY);
        }
        Set<String> products = new HashSet<>();
        for (Integer key : map.keySet()) {
            WholesaleConsignorPro wholesaleConsignorPro = (WholesaleConsignorPro) map.get(key);
            products.add(wholesaleConsignorPro.getProCode());
        }
        // 查询过滤存在的商品
        List<String> proSelfCodeList = gaiaProductBasicMapper.getDistinctSiteList(user.getClient(), dcCode, new ArrayList<String>(products));
        List<OwnerProductVO> productVOS = new ArrayList<>();
        for (Integer key : map.keySet()) {
            WholesaleConsignorPro wholesaleConsignorPro = (WholesaleConsignorPro) map.get(key);
            // 筛选出不存在商品
            if (!proSelfCodeList.contains(wholesaleConsignorPro.getProCode())) {
                errorList.add(MessageFormat.format("第{0}行，商品编码不存在", key + 1));
                continue;
            }
            OwnerProductVO ownerProductVO = new OwnerProductVO();
            ownerProductVO.setClient(user.getClient());
            ownerProductVO.setGopSite(dcCode);
            ownerProductVO.setGopOwner(owner);
            ownerProductVO.setGopProCode(wholesaleConsignorPro.getProCode());
            ownerProductVO.setGopCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            ownerProductVO.setGopCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            ownerProductVO.setGopCreateBy(user.getUserId());
            productVOS.add(ownerProductVO);
        }
        if (!CollectionUtils.isEmpty(errorList)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(errorList);
            return result;
        }
        // 批量删除批发货主商品明细
        gaiaOwnerProductMapper.deleteBatch(user.getClient(), dcCode, owner
                , proSelfCodeList);
        // 批量插入批发货主商品明细
        gaiaOwnerProductMapper.insertBatchOwnerProduct(productVOS);
        return ResultUtil.success();
    }
}
