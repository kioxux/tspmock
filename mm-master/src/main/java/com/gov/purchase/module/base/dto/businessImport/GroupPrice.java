package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

@Data
public class GroupPrice {
    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "门店编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String stoCode;

}
