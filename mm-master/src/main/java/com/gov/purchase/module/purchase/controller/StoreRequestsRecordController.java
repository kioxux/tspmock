package com.gov.purchase.module.purchase.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.purchase.dto.CompadmForStoreDto;
import com.gov.purchase.module.purchase.service.StoreRequestsRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 连锁总部请货数据
 * @author: yzf
 * @create: 2021-11-04 11:14
 */
@Api(tags = "连锁总部请货数据查询")
@RestController
@RequestMapping("storeRequestsRecord")
public class StoreRequestsRecordController {

    @Resource
    private StoreRequestsRecordService storeRequestsRecordService;

    /**
     * 连锁总部列表查询
     *
     * @return
     */
    @Log("连锁总部列表查询")
    @ApiOperation("连锁总部列表查询")
    @PostMapping("queryCompadms")
    public Result queryCompadms() {
        return ResultUtil.success(storeRequestsRecordService.queryCompadms());
    }

    /**
     * 门店列表查询
     *
     * @return
     */
    @Log("门店列表查询")
    @ApiOperation("门店列表查询")
    @PostMapping("queryStores")
    public Result queryStores(@RequestJson("stoChainHead") String stoChainHead) {
        return ResultUtil.success(storeRequestsRecordService.queryStores(stoChainHead));
    }


    /**
     * 请货订单数据查询
     *
     * @return
     */
    @Log("请货订单数据查询")
    @ApiOperation("请货订单数据查询")
    @PostMapping("queryStoreRequestsRecord")
    public Result queryStoreRequestsRecord(@RequestBody CompadmForStoreDto compadmForStoreDto) {
        return ResultUtil.success(storeRequestsRecordService.queryStoreRequestsRecord(compadmForStoreDto));
    }

    /**
     * 请货订单审核功能
     *
     * @return
     */
    @Log("请货订单审核功能")
    @ApiOperation("请货订单审核功能")
    @PostMapping("examineStoreRequests")
    public Result examineStoreRequests(@RequestBody List<CompadmForStoreDto> list) {
        return storeRequestsRecordService.examineStoreRequests(list);
    }

    /**
     * 请货订单明细数据查询
     *
     * @return
     */
    @Log("请货订单-明细数据查询")
    @ApiOperation("请货订单-明细数据查询")
    @PostMapping("queryStoreRequestsDetailRecord")
    public Result queryStoreRequestsDetailRecord(@RequestBody CompadmForStoreDto compadmForStoreDto) {
        return ResultUtil.success(storeRequestsRecordService.queryStoreRequestsDetailRecord(compadmForStoreDto));
    }

    /**
     * 请货明细数据查询
     *
     * @return
     */
    @Log("请货明细数据查询")
    @ApiOperation("请货明细数据查询")
    @PostMapping("queryDetailRecord")
    public Result queryDetailRecord(@RequestBody CompadmForStoreDto compadmForStoreDto) {
        return ResultUtil.success(storeRequestsRecordService.queryDetailRecord(compadmForStoreDto));
    }


    /**
     * 请货订单关闭
     *
     * @return
     */
    @Log("请货订单关闭")
    @ApiOperation("请货订单关闭")
    @PostMapping("updateFlag")
    public Result updateFlag(@RequestBody List<CompadmForStoreDto> compadmForStoreDto) {
        storeRequestsRecordService.updateFlag(compadmForStoreDto);
        return ResultUtil.success();
    }


}
