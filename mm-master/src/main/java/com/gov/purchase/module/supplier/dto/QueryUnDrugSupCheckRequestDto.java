package com.gov.purchase.module.supplier.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "非药供应商校验传入参数")
public class QueryUnDrugSupCheckRequestDto {

    /**
     * 供应商名称
     */
    @ApiModelProperty(value = "供应商名称", name = "supName", required = true)
    @NotBlank(message = "供应商名称不能为空")
    private String supName;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 供应商编码
     */
    @ApiModelProperty(value = "供应商编码", name = "supCode")
    private String supCode;

    /**
     * 供应商状态
     */
    @ApiModelProperty(value = "供应商状态", name = "supStatus")
    private String supStatus;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型", name = "type", required = true)
    @NotBlank(message = "类型不能为空")
    private String type;

    /**
     * 助记码
     */
    @ApiModelProperty(value = "助记码", name = "supPym", required = true)
    // @NotBlank(message = "助记码不能为空")
    private String supPym;

    /**
     * 统一社会信用代码
     */
    @ApiModelProperty(value = "统一社会信用代码", name = "supCreditCode", required = true)
    // @NotBlank(message = "统一社会信用代码不能为空")
    private String supCreditCode;

    /**
     * 营业期限
     */
    @ApiModelProperty(value = "营业期限", name = "supCreditDate", required = true)
    // @NotBlank(message = "营业期限不能为空")
    private String supCreditDate;

    /**
     * 供应商分类
     */
    @ApiModelProperty(value = "供应商分类", name = "supClass", required = true)
    // @NotBlank(message = "供应商分类不能为空")
    private String supClass;

    /**
     * 法人
     */
    @ApiModelProperty(value = "法人", name = "supLegalPerson", required = true)
    // @NotBlank(message = "法人不能为空")
    private String supLegalPerson;

    /**
     * 注册地址
     */
    @ApiModelProperty(value = "注册地址", name = "supRegAdd", required = true)
    // @NotBlank(message = "注册地址不能为空")
    private String supRegAdd;

    /**
     * 许可证编号
     */
    private String supLicenceNo;

    /**
     * 发证日期
     */
    private String supLicenceDate;

    /**
     * 有效期至
     */
    private String supLicenceValid;

    /**
     * 生产或经营范围
     */
    private String supScope;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "supSite", required = true)
    @NotBlank(message = "地点不能为空")
    private String supSite;

    /**
     * 供应商自编码
     */
    @ApiModelProperty(value = "供应商自编码", name = "supSelfCode", required = true)
    @NotBlank(message = "供应商自编码不能为空")
    private String supSelfCode;

    /**
     * 禁止采购
     */
    private String supNoPurchase;

    /**
     * 禁止退厂
     */
    private String supNoSupplier;

    /**
     * 采购付款条件
     */
    private String supPayTerm;

    /**
     * 业务联系人
     */
    private String supBussinessContact;

    /**
     * 联系人电话
     */
    private String supContactTel;

    /**
     * 送货前置期
     */
    private String supLeadTime;

    /**
     * 银行代码
     */
    private String supBankCode;

    /**
     * 银行名称
     */
    private String supBankName;

    /**
     * 账户持有人
     */
    private String supAccountPerson;

    /**
     * 银行账号
     */
    private String supBankAccount;

    /**
     * 支付方式
     */
    private String supPayMode;

    /**
     * 铺底授信额度
     */
    private String supCreditAmt;

}
