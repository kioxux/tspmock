package com.gov.purchase.module.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "客户详情返回结果")
public class CustomerInfoReponseDto {

    @ApiModelProperty(value = "客户编号", name = "cusCode")
    private String cusCode;

    @ApiModelProperty(value = "地点", name = "cusSite")
    private String cusSite;

    @ApiModelProperty(value = "客户编号（企业", name = "cusSelfCode")
    private String cusSelfCode;

    @ApiModelProperty(value = "客户名称", name = "cusName")
    private String cusName;

    @ApiModelProperty(value = "营业执照编号", name = "cusCreditCode")
    private String cusCreditCode;

    @ApiModelProperty(value = "法人", name = "cusLegalPerson")
    private String cusLegalPerson;

    @ApiModelProperty(value = "助记码", name = "cusPym")
    private String cusPym;

    @ApiModelProperty(value = "注册地址", name = "cusRegAdd")
    private String cusRegAdd;

    @ApiModelProperty(value = "许可证编号", name = "cusLicenceNo")
    private String cusLicenceNo;

    @ApiModelProperty(value = "发证日期", name = "cusLicenceDate")
    private String cusLicenceDate;

    @ApiModelProperty(value = "有效期至", name = "cusLicenceValid")
    private String cusLicenceValid;

    @ApiModelProperty(value = "生产或经营范围", name = "cusScope")
    private String cusScope;

    @ApiModelProperty(value = "银行代码", name = "cusClass")
    private String cusClass;

    @ApiModelProperty(value = "客户编号", name = "cusBankCode")
    private String cusBankCode;

    @ApiModelProperty(value = "银行代码", name = "cusBankName")
    private String cusBankName;

    @ApiModelProperty(value = "账户持有人", name = "cusAccountPerson")
    private String cusAccountPerson;

    @ApiModelProperty(value = "银行账号", name = "cusBankAccount")
    private String cusBankAccount;

    @ApiModelProperty(value = "付款条件（源自：GAIA_PAYMENT_TYPE的 ORD_TYPE）", name = "cusPayTerm")
    private String cusPayTerm;

    @ApiModelProperty(value = "支付方式(1-银行转账、2-承兑汇票、3-线下票据；；4 - 赊销5 - 电汇", name = "cusPayMode")
    private String cusPayMode;

    @ApiModelProperty(value = "铺底授信额度", name = "cusCreditAmt")
    private String cusCreditAmt;

    @ApiModelProperty(value = "禁止销售(0-否，1-是)", name = "cusNoSale")
    private String cusNoSale;

    @ApiModelProperty(value = "禁止退货(0-否，1-是)", name = "cusNoReturn")
    private String cusNoReturn;

    @ApiModelProperty(value = "业务联系人", name = "cusBussinessContact")
    private String cusBussinessContact;

    @ApiModelProperty(value = "联系人电话", name = "cusContactTel")
    private String cusContactTel;


    @ApiModelProperty(value = "是否启用信用管理(0-否，1-是)", name = "cusCreditFlag")
    private String cusCreditFlag;

    @ApiModelProperty(value = "信用额度", name = "cusCreditQuota")
    private BigDecimal cusCreditQuota;

    @ApiModelProperty(value = "信用检查点(1-销售订单，2-发货过账，3-开票", name = "cusCreditCheck")
    private String cusCreditCheck;

    @ApiModelProperty(value = "匹配状态:0-未匹配，1-部分匹配，2-完全匹配", name = "cusMatchStatus")
    private String cusMatchStatus;
}
