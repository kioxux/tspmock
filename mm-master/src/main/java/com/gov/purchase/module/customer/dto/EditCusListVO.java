package com.gov.purchase.module.customer.dto;

import com.gov.purchase.entity.GaiaCustomerBusiness;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/26 10:34
 */
@Data
public class EditCusListVO {
    @NotEmpty(message = "客户列表不能为空")
    private List<GaiaCustomerBusiness> businessList;
//    @NotBlank(message = "备注不能为空")
    private String remarks;
}
