package com.gov.purchase.module.priceCompare.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.entity.PriceCompareBase;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.utils.ExcelUtils;
import com.gov.purchase.entity.GaiaSspPriceCompare;
import com.gov.purchase.entity.GaiaSspPriceCompareMessage;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaSspPriceCompareInfoMapper;
import com.gov.purchase.mapper.GaiaSspPriceCompareMapper;
import com.gov.purchase.mapper.GaiaSspPriceCompareMessageMapper;
import com.gov.purchase.mapper.GaiaSspPriceCompareProMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.priceCompare.dto.PriceCompareDto;
import com.gov.purchase.module.priceCompare.dto.PriceComparePageDto;
import com.gov.purchase.module.priceCompare.service.PriceCompareService;
import com.gov.purchase.module.priceCompare.vo.PriceComparePageVo;
import com.gov.purchase.module.priceCompare.vo.PriceCompareProInfoVo;
import com.gov.purchase.module.priceCompare.vo.PriceCompareSupplierInfoVo;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentRequestDto;
import com.gov.purchase.module.replenishment.service.DcReplenishmentService;
import com.gov.purchase.utils.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PriceCompareServiceImpl implements PriceCompareService {

    @Resource
    private RedisClient redisClient;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaSspPriceCompareMapper priceCompareMapper;
    @Resource
    private GaiaSspPriceCompareProMapper priceCompareProMapper;
    @Resource
    private GaiaSspPriceCompareInfoMapper priceCompareInfoMapper;
    @Resource
    private GaiaSspPriceCompareMessageMapper priceCompareMessageMapper;
    @Resource
    private DcReplenishmentService dcReplenishmentService;

    private String PRICE_COMPARE_NO_KEY = "PRICE_COMPARE_NO_KEY_";


    /**
     * 新增
     *
     * @param param
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(@Valid PriceCompareDto param) {
        // 当前登陆人
        TokenUser currentUser = feignService.getLoginInfo();
        // 最新编号
        String priceCompareNo = this.getOnlyPriceCompareNo();
        // 比价主表
        GaiaSspPriceCompare priceCompare = new GaiaSspPriceCompare();
        BeanUtils.copyProperties(param, priceCompare);
        priceCompare.setPriceCompareNo(priceCompareNo);
        priceCompare.setStatus(1);
        priceCompare.setCreateBy(currentUser.getUserId());
        priceCompare.setCreateTime(LocalDateTime.now());
        priceCompare.setClient(currentUser.getClient());
        priceCompareMapper.insertSelective(priceCompare);
        // 比价商品表
        priceCompareProMapper.insertSelectList(param.getPriceCompareProList(), priceCompare, currentUser.getUserId());
        // 比价消息
        priceCompareMessageMapper.insertSelectList(priceCompare, currentUser.getUserId());
    }


    /**
     * 分页
     *
     * @param param
     * @return
     */
    @Override
    public PageInfo page(PriceComparePageDto param) {
        PageHelper.startPage(param.getPageNum(), param.getPageSize());
        // 当前登陆人
        TokenUser currentUser = feignService.getLoginInfo();
        param.setClient(currentUser.getClient());
        List<PriceComparePageVo> list = priceCompareMapper.selectPageByParams(param);
        PageInfo<PriceComparePageVo> pageInfo = new PageInfo(list);
        return pageInfo;
    }


    /**
     * 比价 - 截至时间
     *
     * @param id
     * @param offerEndTime 截至日期
     * @param isExpires    是否立即失效 （true: 是 ,false: 否）
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void offerEndTime(Long id, String offerEndTime, Boolean isExpires) {
        TokenUser currentUser = feignService.getLoginInfo();
        LocalDateTime newOfferEndTime = isExpires == true ? LocalDateTime.now() : LocalDateTime.parse(offerEndTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        // 比价主表
        GaiaSspPriceCompare priceCompare = new GaiaSspPriceCompare();
        priceCompare.setId(id);
        priceCompare.setUpdateBy(currentUser.getUserId());
        priceCompare.setUpdateTime(LocalDateTime.now());
        priceCompare.setOfferEndTime(newOfferEndTime);
        priceCompareMapper.updateByPrimaryKeySelective(priceCompare);
        // 比价消息表
        GaiaSspPriceCompareMessage priceCompareMessage = new GaiaSspPriceCompareMessage();
        priceCompareMessage.setPriceCompareId(id);
        priceCompareMessage.setUpdateBy(currentUser.getUserId());
        priceCompareMessage.setUpdateTime(LocalDateTime.now());
        priceCompareMessage.setOfferEndTime(newOfferEndTime);
        priceCompareMessageMapper.updateByPrimaryKeySelective(priceCompareMessage);
    }

    /**
     * 详情
     *
     * @param id
     * @return
     */
    @Override
    public Map info(Long id) {
        List<PriceCompareSupplierInfoVo> titles = priceCompareMapper.selectSuppliers(id);
        List<PriceCompareProInfoVo> list = priceCompareMapper.selectInfo(id);
        Map map = new HashMap();
        map.put("titles", titles);
        map.put("list", list);
        return map;
    }

    /**
     * 比价 - 保存
     *
     * @param priceCompareId
     * @param params
     * @param isOrder        是否已下单（0；未下单 ，1已下单）
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void infoSave(Long priceCompareId, List<PriceCompareProInfoVo> params, int isOrder) {
        TokenUser currentUser = feignService.getLoginInfo();
        List<PriceCompareProInfoVo> infos = new ArrayList<>();
        priceCompareProMapper.updateListByParams(params, isOrder, currentUser.getUserId());
        List<Long> priceCompareProId = params.stream().map(PriceCompareBase::getId).collect(Collectors.toList());
        // 删除价格
        priceCompareInfoMapper.deleteByPriceCompareProId(priceCompareProId);

        priceCompareInfoMapper.insertList2(params, currentUser.getUserId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result infoSure(Long priceCompareId, List<PriceCompareProInfoVo> params) {
        GaiaSspPriceCompare one = priceCompareMapper.selectByPrimaryKey(priceCompareId);
        // 价格写入数据库
        this.infoSave(priceCompareId, params, 1);
        //下单
        List<DcReplenishmentRequestDto> dtos = new ArrayList<>();
        DcReplenishmentRequestDto dto;
        for (PriceCompareProInfoVo item : params) {
            dto = new DcReplenishmentRequestDto();
            dto.setClient(one.getClient());
            dto.setDcCode(one.getDcCode());
            dto.setPoProCode(item.getProNo());
            dto.setPoUnit(item.getUnit());
            dto.setPoPrice(item.getOrderPrice());
            dto.setPoSupplierId(item.getOrderSupplierCode());
            dto.setPoQty(new BigDecimal(item.getFinalAmount()));
            dto.setPoRate(item.getProInputTax());
            dto.setSupPayTerm(item.getSupPayTerm());
            dto.setPoOwner(item.getPoOwner());
            dto.setPoOwnerName(item.getPoOwnerName());
            dto.setProZdy1(item.getProZdy1());
            dto.setProZdy2(item.getProZdy2());
            dto.setProZdy3(item.getProZdy3());
            dto.setProZdy4(item.getProZdy4());
            dto.setProZdy5(item.getProZdy5());
            dtos.add(dto);
        }
        // 全部下单 修改主表状态
        List<Integer> isOrderList = priceCompareProMapper.selectProFinish(priceCompareId);
        boolean allMatch = isOrderList.stream().allMatch(item -> item == 1);
        if (allMatch) {
            GaiaSspPriceCompare priceCompare = new GaiaSspPriceCompare();
            priceCompare.setId(priceCompareId);
            priceCompare.setStatus(2);
            priceCompare.setFinishTime(LocalDateTime.now());
            priceCompareMapper.updateByPrimaryKeySelective(priceCompare);
        }
        // 下单
        return dcReplenishmentService.saveDcReplenishmentInfo(dtos);
    }

    @Override
    public void listExportExcel(PriceComparePageDto param, HttpServletResponse response) throws IOException {
        TokenUser currentUser = feignService.getLoginInfo();
        param.setClient(currentUser.getClient());
        List<PriceComparePageVo> list = priceCompareMapper.selectPageByParams(param);
        this.listExport(list, response);
    }

    @Override
    public void infoExportExcel(Long priceCompareId, HttpServletResponse response) throws IOException {
        List<PriceCompareSupplierInfoVo> titles = priceCompareMapper.selectSuppliers(priceCompareId);
        List<PriceCompareProInfoVo> list = priceCompareMapper.selectInfo(priceCompareId);
        this.infoExport(titles, list, response);
    }

    /**
     * 更新比价失效状态&完成时间
     */
    @Override
    public void updateCompareInvalid() {
        Map<String, Object> map = new HashMap<>();
        // 昨日
        LocalDate invalidDate = LocalDate.now().plusDays(-1);
        map.put("expiringDate", invalidDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        // 处理中
        map.put("status", 1);
        priceCompareMapper.updateInvalidStatusByParams(map);
    }


    private String getPriceCompareNo(String today, Integer count) {
        StringBuilder s = new StringBuilder();
        if (count.toString().length() <= 5) {
            for (int i = 0; i < 5 - (count.toString().length()); i++) {
                s.append("0");
            }
            s.append(count);
        } else {
            s.append(count);
        }
        String No = "BJ" + today + s;
        return No;
    }


    /**
     * redis 缓存保证  编号 唯一
     *
     * @return
     */
    private String getOnlyPriceCompareNo() {
        String priceCompareNo = null;
        Integer count = priceCompareMapper.selectCountByToday();
        String today = new SimpleDateFormat("yyyyMMdd").format(new Date());
        do {
            priceCompareNo = this.getPriceCompareNo(today, count);
            count++;
        } while (redisClient.hexists(PRICE_COMPARE_NO_KEY + today, priceCompareNo));
        redisClient.hset(PRICE_COMPARE_NO_KEY + today, priceCompareNo, priceCompareNo);
        redisClient.expire(PRICE_COMPARE_NO_KEY + today, 3600 * 24);
        return priceCompareNo;
    }

    private void listExport(List<PriceComparePageVo> list, HttpServletResponse response) throws IOException {
        List<String> titles = Arrays.asList(new String[]{"序号", "采购比价单表", "比价开始时间", "比价截至时间", "比价完成日期", "参与比价供应商数", "已反馈比价供应商", "单据状态"});
        ExcelUtils utils = new ExcelUtils("比价结果");
        HSSFSheet sheet = utils.getSheet();

        // 表格标题
        utils.setHead(7);
        // 设置列宽
        sheet.setColumnWidth(0, 2500);
        sheet.setColumnWidth(1, 5000);
        sheet.setColumnWidth(2, 10000);
        sheet.setColumnWidth(3, 10000);
        sheet.setColumnWidth(4, 10000);
        sheet.setColumnWidth(5, 5000);
        sheet.setColumnWidth(6, 5000);
        sheet.setColumnWidth(7, 5000);


        // 样式
        HSSFCellStyle titleStyle = utils.getTitleStyle();
        HSSFCellStyle titleStyleB = utils.getTitleStyleB();
        HSSFCellStyle titleStyleC = utils.getTitleStyleC();
        HSSFCellStyle contentStyle = utils.getContentStyle();
        HSSFCellStyle contentRightStyle = utils.getContentRightStyle();
        HSSFCellStyle contentLeftStyle = utils.getContentLeftStyle();

        // 第二行
        HSSFRow titleRow1 = sheet.createRow(1);
        for (int i = 0; i < titles.size(); i++) {
            HSSFCell titlesACell = titleRow1.createCell(i);
            titlesACell.setCellValue(titles.get(i));
            titlesACell.setCellStyle(titleStyle);
        }

        for (int i = 0; i < list.size(); i++) {
            PriceComparePageVo item = list.get(i);
            HSSFRow contentRow = sheet.createRow(2 + i);

            HSSFCell cell0 = contentRow.createCell(0);
            cell0.setCellValue(i + 1);
            cell0.setCellStyle(contentStyle);

            HSSFCell cell1 = contentRow.createCell(1);
            cell1.setCellValue(item.getPriceCompareNo());
            cell1.setCellStyle(contentStyle);

            HSSFCell cell2 = contentRow.createCell(2);
            cell2.setCellValue(item.getCreateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            cell2.setCellStyle(contentStyle);

            HSSFCell cell3 = contentRow.createCell(3);
            cell3.setCellValue(item.getOfferEndTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            cell3.setCellStyle(contentStyle);

            HSSFCell cell4 = contentRow.createCell(4);
            cell4.setCellValue(item.getFinishTime() == null ? null : item.getFinishTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            cell4.setCellStyle(contentStyle);

            HSSFCell cell5 = contentRow.createCell(5);
            cell5.setCellValue(item.getSupplierTotal());
            cell5.setCellStyle(contentStyle);

            HSSFCell cell6 = contentRow.createCell(6);
            cell6.setCellValue(item.getFeedbackSupplierNum());
            cell6.setCellStyle(contentStyle);

            HSSFCell cell7 = contentRow.createCell(7);
            cell7.setCellValue(item.getStatus() == 1 ? "处理中" : item.getStatus() == 2 ? "已完成" : "已失效");
            cell7.setCellStyle(contentStyle);
        }
        // 流导出
        utils.exportOut(response);


    }


    private void infoExport(List<PriceCompareSupplierInfoVo> titlesB, List<PriceCompareProInfoVo> list, HttpServletResponse response) throws IOException {
        ExcelUtils utils = new ExcelUtils("比价");
        HSSFSheet sheet = utils.getSheet();
        HSSFCellStyle titleStyle = utils.getTitleStyle();
        HSSFCellStyle titleStyleB = utils.getTitleStyleB();
        HSSFCellStyle titleStyleC = utils.getTitleStyleC();
        HSSFCellStyle contentStyle = utils.getContentStyle();
        HSSFCellStyle contentRightStyle = utils.getContentRightStyle();
        HSSFCellStyle contentLeftStyle = utils.getContentLeftStyle();
        // 设置列宽
        sheet.setColumnWidth(0, 2500);
        sheet.setColumnWidth(1, 2500);
        sheet.setColumnWidth(2, 4500);
        sheet.setColumnWidth(3, 5000);
        sheet.setColumnWidth(4, 4500);
        sheet.setColumnWidth(5, 7000);
        sheet.setColumnWidth(6, 4500);
        sheet.setColumnWidth(7, 7000);
        sheet.setColumnWidth(8, 2000);
        sheet.setColumnWidth(9, 2000);
        sheet.setColumnWidth(10, 2000);
        sheet.setColumnWidth(11, 3000);
        sheet.setColumnWidth(12, 3000);
        sheet.setColumnWidth(13, 5000);
        sheet.setColumnWidth(14, 5000);
        // 表格标题
        utils.setHead(14);
        //在sheet里创建第二行
        HSSFRow titleRow1 = sheet.createRow(1);
        HSSFRow titleRow2 = sheet.createRow(2);
        titleRow1.setHeight((short) 400);
        titleRow2.setHeight((short) 400);
        List<String> titlesA = this.titleList();
        for (int i = 0; i < titlesA.size(); i++) {
            HSSFCell titlesACell = titleRow1.createCell(i);
            HSSFCell titlesACell2 = titleRow2.createCell(i);
            titlesACell.setCellValue(titlesA.get(i));
            titlesACell.setCellStyle(titleStyle);
            titlesACell2.setCellStyle(titleStyle);
            // 合并单元格
            sheet.addMergedRegion(new CellRangeAddress(1, 2, i, i));
        }
        for (int i = 0; i < titlesB.size(); i++) {
            HSSFCell titlesBCell = titleRow1.createCell(titlesA.size() + i);
            HSSFCell titlesBCell2 = titleRow2.createCell(titlesA.size() + i);
            titlesBCell.setCellValue(titlesB.get(i).getSupplierName());
            BigDecimal totalPrice = titlesB.get(i).getTotalPrice();
            titlesBCell2.setCellValue(totalPrice == null ? null : String.valueOf(totalPrice));
            sheet.setColumnWidth(titlesA.size() + i, 8000);
            titlesBCell.setCellStyle(titleStyleB);
            titlesBCell2.setCellStyle(titleStyleC);
        }
        for (int i = 0; i < list.size(); i++) {
            PriceCompareProInfoVo one = list.get(i);
            HSSFRow contentRow = sheet.createRow(3 + i);
            // 确认情况
            HSSFCell cell = contentRow.createCell(0);
            HSSFCell cell1 = contentRow.createCell(1);
            HSSFCell cell2 = contentRow.createCell(2);
            HSSFCell cell3 = contentRow.createCell(3);
            HSSFCell cell4 = contentRow.createCell(4);
            HSSFCell cell5 = contentRow.createCell(5);
            HSSFCell cell6 = contentRow.createCell(6);
            HSSFCell cell7 = contentRow.createCell(7);
            HSSFCell cell8 = contentRow.createCell(8);
            HSSFCell cell9 = contentRow.createCell(9);
            HSSFCell cell10 = contentRow.createCell(10);
            HSSFCell cell11 = contentRow.createCell(11);
            HSSFCell cell12 = contentRow.createCell(12);
            HSSFCell cell13 = contentRow.createCell(13);
            HSSFCell cell14 = contentRow.createCell(14);
            cell.setCellStyle(contentStyle);
            cell1.setCellStyle(contentStyle);
            cell2.setCellStyle(contentStyle);
            cell3.setCellStyle(contentStyle);
            cell4.setCellStyle(contentStyle);
            cell5.setCellStyle(contentLeftStyle);
            cell6.setCellStyle(contentStyle);
            cell7.setCellStyle(contentLeftStyle);
            cell8.setCellStyle(contentStyle);
            cell9.setCellStyle(contentStyle);
            cell10.setCellStyle(contentStyle);
            cell11.setCellStyle(contentStyle);
            cell12.setCellStyle(contentStyle);
            cell13.setCellStyle(contentStyle);
            cell14.setCellStyle(contentRightStyle);
            //是否确认
            cell.setCellValue(one.getIsOrder() == 1 ? "已确认" : "未确认");
            // 序号
            cell1.setCellValue(i + 1);
            // 商品描述
            cell2.setCellValue(one.getCreateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            // 采购比价单号
            cell3.setCellValue(one.getPriceCompareNo());
            // 客户商品编码
            cell4.setCellValue(one.getProNo());
            // 商品描述
            cell5.setCellValue(one.getProName());
            // 规格
            cell6.setCellValue(one.getSpecs());
            // 生产厂家
            cell7.setCellValue(one.getManufacturer());
            // 单位
            cell8.setCellValue(one.getUnit());
            // 中包装
            cell9.setCellValue(one.getMediumPack());
            // 大包装
            cell10.setCellValue(one.getBigPack());
            // 建议采购量
            if (one.getAdviseAmount() != null) {
                cell11.setCellValue(one.getAdviseAmount().doubleValue());
            } else {
                cell11.setCellValue("");
            }

            // 最终采购量
            if (one.getFinalAmount() != null) {
                cell12.setCellValue(one.getFinalAmount().doubleValue());
            } else {
                cell12.setCellValue("");
            }

            // 备注
            cell13.setCellValue(one.getRemarks());
            // 最低报价
            if (one.getMinPrice() != null) {
                cell14.setCellValue(one.getMinPrice().doubleValue());
            } else {
                cell14.setCellValue("");
            }
            List<String> values = new ArrayList<>();
            for (int j = 0; j < titlesB.size(); j++) {
                PriceCompareSupplierInfoVo titleB = titlesB.get(j);
                List<PriceCompareSupplierInfoVo> collect = one.getSupplierList().stream().filter(item -> item.getPriceCompareSupplierId().longValue() == titleB.getPriceCompareSupplierId().longValue()).collect(Collectors.toList());
                BigDecimal finalPrice = collect.get(0).getFinalPrice();
                String remarks = collect.get(0).getRemarks();
                if (StringUtils.isNotEmpty(remarks)) {
                    values.add(finalPrice == null ? null : finalPrice + "\n" + remarks);
                } else {
                    values.add(finalPrice == null ? null : String.valueOf(finalPrice));
                }
            }
            System.out.println(String.valueOf(values));
            for (int j = 0; j < values.size(); j++) {
                HSSFCell cellX = contentRow.createCell(titlesA.size() + j);
                cellX.setCellValue(values.get(j));
                cellX.setCellStyle(contentRightStyle);
            }
        }
        utils.exportOut(response);
    }


    private List<String> titleList() {
        return Arrays.asList(new String[]{"确认情况", "序号", "比价日期", "采购比价单号", "客户商品编码", "商品描述", "规格", "生产厂家", "单位", "中包装", "大包装", "建议采购量", "最终采购量", "备注", "最低报价"});
    }


}
