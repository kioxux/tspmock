package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.feign.dto.TokenUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description: 销售订单
 *
 * @author: libb
 * @date: 2020.04.02
 */
@Data
public class GaiaSoHeader {

    @ApiModelProperty(value = "订单类型", name = "soType")
    @NotBlank(message = "订单类型不能为空")
    private String soType;
    @ApiModelProperty(value = "客户信息", name = "soCustomerId")
    @NotBlank(message = "客户信息不能为空")
    private String soCustomerId;
    @ApiModelProperty(value = "销售主体", name = "soCompanyCode")
    @NotBlank(message = "销售主体不能为空")
    private String soCompanyCode;
    @ApiModelProperty(value = "凭证日期", name = "soDate")
    @NotBlank(message = "凭证日期不能为空")
    private String soDate;
    @ApiModelProperty(value = "收款条款", name = "soPaymentId")
    private String soPaymentId;
    @ApiModelProperty(value = "抬头备注", name = "soHeadRemark")
    private String soHeadRemark;
    @ApiModelProperty(value = "销售订单明细", name = "soItemList")
    private List<SoItem> soItemList;
    @ApiModelProperty(value = "客户2", name = "soCustomer2")
    private String soCustomer2;
    /**
     * 请货单列表
     */
    private List<String> voucherIdList;

    /**
     * 用户信息：自动执行时无登录用户
     */
    private TokenUser tokenUser;

    /**
     * 销售员编号
     */
    private String gwsCode;

    /**
     * 货主
     */
    private String soOwner;

    private String soId;

    /**
     * 暂存 - false， 保存 - true
     */
    private boolean type;
}

