package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PayingHeaderDTO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 付款订单号
     */
    private String ficoId;

    /**
     * 本次付款总计
     */
    private BigDecimal ficoPaymentAmountTotal;

    /**
     * 下单时间
     */
    private String ficoOrderDate;

    /**
     * 操作者
     */
    private String ficoOperator;

    /**
     * 发票状态
     */
    private String ficoInvoiceStatus;

    /**
     * 付款状态
     */
    private String ficoPaymentStatus;

    /**
     * 流水号
     */
    private String ficoSerialNumber;

    /**
     * 二维码编号
     */
    private String ficoQrcode;

    /**
     * 付款时间
     */
    private String ficoPayingDate;
}
