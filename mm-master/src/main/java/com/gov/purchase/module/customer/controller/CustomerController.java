package com.gov.purchase.module.customer.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.customer.dto.CustomerGspInfoDetailRequestDto;
import com.gov.purchase.module.customer.dto.CustomerInfoRequestDto;
import com.gov.purchase.module.customer.dto.CustomerInfoSaveRequestDto;
import com.gov.purchase.module.customer.dto.CustomerListRequestDto;
import com.gov.purchase.module.customer.service.CustomerService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "客户")
@RestController
@RequestMapping("customer")
public class CustomerController {


    @Autowired
    CustomerService customerService;

    @Log("客户维护列表页面")
    @ApiOperation("客户维护列表页面")
    @PostMapping("queryCustomerList")
    public Result queryCustomerList(@Valid @RequestBody CustomerListRequestDto dto) {
        return ResultUtil.success(customerService.queryCustomerList(dto));
    }

    @Log("客户信息详情")
    @ApiOperation("客户信息详情")
    @GetMapping("getCustomerInfo")
    public Result getCustomerInfo(CustomerInfoRequestDto dto) {
        return ResultUtil.success(customerService.getCustomerInfo(dto));
    }

    @Log("客户维护验证")
    @ApiOperation("客户维护验证")
    @PostMapping("validCustomerInfo")
    public Result validCustomerInfo(@Valid @RequestBody CustomerInfoSaveRequestDto dto) {
        return ResultUtil.success(customerService.validCustomerInfo(dto));
    }

    @Log("客户维护提交")
    @ApiOperation("客户维护提交")
    @PostMapping("saveCustomerInfo")
    public Result saveCustomerInfo(@Valid @RequestBody CustomerInfoSaveRequestDto dto) {
        return ResultUtil.success(customerService.saveCustomerInfo(dto));
    }
}
