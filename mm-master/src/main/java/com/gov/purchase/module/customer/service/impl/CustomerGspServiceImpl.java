package com.gov.purchase.module.customer.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.ListUtils;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaCustomerBasicMapper;
import com.gov.purchase.mapper.GaiaCustomerBusinessMapper;
import com.gov.purchase.mapper.GaiaCustomerGspinfoLogMapper;
import com.gov.purchase.mapper.GaiaCustomerGspinfoMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.customer.dto.*;
import com.gov.purchase.module.customer.service.CustomerGspService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.csv.CsvClient;
import com.gov.purchase.utils.csv.dto.CsvFileInfo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * 客户首营服务
 */
@Service
public class CustomerGspServiceImpl implements CustomerGspService {

    @Resource
    private FeignService feignService;

    @Resource
    private GaiaCustomerBasicMapper gaiaCustomerBasicMapper;

    @Resource
    private GaiaCustomerGspinfoMapper gaiaCustomerGspinfoMapper;

    @Resource
    private GaiaCustomerGspinfoLogMapper gaiaCustomerGspinfoLogMapper;

    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;

    @Autowired
    private CosUtils cosUtils;

    /**
     * 客户库搜索列表
     *
     * @param dto 查询条件
     * @return
     */
    @Override
    public PageInfo<CustomerGspListResponseDto> queryCustomerList(CustomerGspListRequestDto dto) {
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        return new PageInfo<>(gaiaCustomerBasicMapper.queryCustomerList(dto));
    }

    /**
     * 客户首营初始化
     *
     * @param cusCode 客户编号
     * @return
     */
    @Override
    public GaiaCustomerBasic getCustomerBasicInfo(String cusCode) {
        return gaiaCustomerBasicMapper.selectByPrimaryKey(cusCode);
    }

    /**
     * 客户首营提交
     *
     * @param dto 提交参数
     * @return
     */
    @Override
    @Transactional
    public Result saveCustomerGspInfo(CustomerGspRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        /**
         * 验证客户自编码重复性
         */
        GaiaCustomerGspinfoKey gspinfoKey = new GaiaCustomerGspinfoKey();
        gspinfoKey.setClient(user.getClient());
        gspinfoKey.setCusSelfCode(dto.getCusSelfCode()); // 客户自编码
        gspinfoKey.setCusSite(dto.getCusSite());
        List<GaiaCustomerGspinfo> gaiaCustomerGspinfos = gaiaCustomerGspinfoMapper.selectGspInfoByPrimaryKey(gspinfoKey);
        if (CollectionUtils.isNotEmpty(gaiaCustomerGspinfos)) {
            // 客户编码（企业）已存在
            throw new CustomResultException(ResultEnum.E0202);
        }

        /**
         * 查询客户主数据业务表 查询是否存在 存在则提示 客户编码（企业）已存在
         */
        GaiaCustomerBusinessKey businessKey = new GaiaCustomerBusinessKey();
        businessKey.setClient(user.getClient());
        businessKey.setCusSite(dto.getCusSite());
        businessKey.setCusSelfCode(dto.getCusSelfCode());
        List<GaiaCustomerBusiness> gaiaCustomerBusinesses = gaiaCustomerBusinessMapper.selectBusinessInfoByPrimaryKey(businessKey);
        if (CollectionUtils.isNotEmpty(gaiaCustomerBusinesses)) {
            // 客户编码（企业）已存在
            throw new CustomResultException(ResultEnum.E0202);
        }

        /**
         * 客户基础数据 如果停用 则提示 停用客户不可以提交
         */
        GaiaCustomerBasic basic = gaiaCustomerBasicMapper.selectByPrimaryKey(dto.getCusCode());
        if (basic == null || CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(basic.getCusStatus())) {
            throw new CustomResultException(ResultEnum.CUS_DEACTIVATE_ERROR);
        }

        GaiaCustomerGspinfo gspinfo = new GaiaCustomerGspinfo();
        // 设置当前登录人的加盟商编号
        gspinfo.setClient(user.getClient());
        gspinfo.setCusCode(dto.getCusCode());
        gspinfo.setCusSelfCode(dto.getCusSelfCode()); // 客户编码（企业）
        gspinfo.setCusSite(dto.getCusSite()); // 地点
        gspinfo.setCusName(dto.getCusName()); // 客户名称
        gspinfo.setCusCreditCode(dto.getCusCreditCode()); // 营业执照编号
        gspinfo.setCusCreditDate(dto.getCusCreditDate()); //营业期限
        gspinfo.setCusLegalPerson(dto.getCusLegalPerson()); // 法人
        gspinfo.setCusRegAdd(dto.getCusRegAdd()); // 注册地址
        gspinfo.setCusLicenceNo(dto.getCusLicenceNo()); //许可证编号
        gspinfo.setCusLicenceOrgan(dto.getCusLicenceOrgan()); //发证单位
        gspinfo.setCusLicenceDate(dto.getCusLicenceDate()); //发证日期
        gspinfo.setCusLicenceValid(dto.getCusLicenceValid());// 有效期至
        gspinfo.setCusPostalCode(dto.getCusPostalCode()); // 邮政编码
        gspinfo.setCusTaxCard(dto.getCusTaxCard()); //税务登记证
        gspinfo.setCusOrgCard(dto.getCusOrgCard()); //组织机构代码证
        gspinfo.setCusAccountName(dto.getCusAccountName()); //开户户名
        gspinfo.setCusAccountBank(dto.getCusAccountBank()); //开户银行
        gspinfo.setCusBankAccount(dto.getCusBankAccount());// 银行账号
        gspinfo.setCusDocState(dto.getCusDocState()); //随货同行单（票）情况
        gspinfo.setCusSealState(dto.getCusSealState()); // 企业印章情况
        gspinfo.setCusScope(dto.getCusScope()); // 生产或经营范围
        gspinfo.setCusOperationMode(dto.getCusOperationMode());// 经营方式
        gspinfo.setCusGspStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode()); //审批状态 0-审批中
        gspinfo.setCusGspDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        // 流程编号
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        gspinfo.setCusFlowNo(System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0"));
        gaiaCustomerGspinfoMapper.insert(gspinfo);
        // 首营日志
        GaiaCustomerGspinfoLog gaiaCustomerGspinfoLogOld = new GaiaCustomerGspinfoLog();
        BeanUtils.copyProperties(basic, gaiaCustomerGspinfoLogOld);
        // 加盟商
        gaiaCustomerGspinfoLogOld.setClient(user.getClient());
        // 自编码
        gaiaCustomerGspinfoLogOld.setCusSelfCode(dto.getCusSelfCode());
        // 地点
        gaiaCustomerGspinfoLogOld.setCusSite(dto.getCusSite());
        gaiaCustomerGspinfoLogOld.setCusLine(1);
        gaiaCustomerGspinfoLogMapper.insertSelective(gaiaCustomerGspinfoLogOld);

        GaiaCustomerGspinfoLog gaiaCustomerGspinfoLogNew = new GaiaCustomerGspinfoLog();
        BeanUtils.copyProperties(gspinfo, gaiaCustomerGspinfoLogNew);
        gaiaCustomerGspinfoLogOld.setCusLine(2);
        gaiaCustomerGspinfoLogMapper.insertSelective(gaiaCustomerGspinfoLogNew);
        // 提交后自动新建工作流，流程节点根据人员所属门店号，从权限表取出对应质量负责人、企业负责人。审批流程：提交人（已提交）->质量->企业负责人->END
        FeignResult result = feignService.createCustomerWorkflow(gspinfo);
        if (result.getCode() != 0) {
            throw new CustomResultException(result.getMessage());
        }
        return ResultUtil.success();
    }


    /**
     * 客户首营列表
     *
     * @param dto 提交参数
     * @return
     */
    @Override
    public PageInfo<CustomerGspInfoListReponseDto> queryCustomerGspList(CustomerGspInfoListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        // 设置当前登录人的加盟商编号
        dto.setClient(user.getClient());
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        return new PageInfo<>(gaiaCustomerGspinfoMapper.queryCustomerGspInfoList(dto));
    }

    /**
     * 客户首营详情
     *
     * @param dto
     * @return
     */

    @Override
    public CustomerGspInfoDetailReponseDto queryCustomerGspDetail(CustomerGspInfoDetailRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        // 设置当前登录人的加盟商编号
        dto.setClient(user.getClient());

        //请求参数转化
        GaiaCustomerGspinfoKey key = new GaiaCustomerGspinfoKey();
        BeanUtils.copyProperties(dto, key);
        //返回结果转化
        CustomerGspInfoDetailReponseDto result = gaiaCustomerGspinfoMapper.selectByPrimaryKeyWithSiteName(key);
        return result;
    }

    /**
     * 查询客户最大自编码
     *
     * @param client
     * @param cusSite
     * @param code
     * @return
     */
    @Override
    public Result getMaxSelfCodeNum(String client, String cusSite, String code) {
        if (StringUtils.isNotBlank(code)) {
            return ResultUtil.success(code);
        }
        // 自编码集合
        List<String> list = gaiaCustomerBusinessMapper.getSelfCodeList(client, cusSite, code);
        if (CollectionUtils.isEmpty(list)) {
            return ResultUtil.success();
        }
        String maxCode = ListUtils.getMaxCode(list);
        String selfCode = ListUtils.getMaxCode(maxCode);
        return ResultUtil.success(selfCode);
    }

    /**
     * 手工新增
     *
     * @param customerGspRequestDto
     * @return
     */
    @Override
    public Result addBusioness(CustomerGspRequestDto customerGspRequestDto) {
        //加盟商
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        customerGspRequestDto.setClient(client);

        /**
         * 验证客户自编码重复性
         */
        GaiaCustomerGspinfoKey gspinfoKey = new GaiaCustomerGspinfoKey();
        gspinfoKey.setClient(customerGspRequestDto.getClient());
        gspinfoKey.setCusSelfCode(customerGspRequestDto.getCusSelfCode()); // 客户自编码
        gspinfoKey.setCusSite(customerGspRequestDto.getCusSite());
        List<GaiaCustomerGspinfo> gaiaCustomerGspinfos = gaiaCustomerGspinfoMapper.selectGspInfoByPrimaryKey(gspinfoKey);
        if (CollectionUtils.isNotEmpty(gaiaCustomerGspinfos)) {
            // 客户编码（企业）已存在
            throw new CustomResultException(ResultEnum.E0202);
        }

        /**
         * 查询客户主数据业务表 查询是否存在 存在则提示 客户编码（企业）已存在
         */
        GaiaCustomerBusinessKey businessKey = new GaiaCustomerBusinessKey();
        businessKey.setClient(customerGspRequestDto.getClient());
        businessKey.setCusSite(customerGspRequestDto.getCusSite());
        businessKey.setCusSelfCode(customerGspRequestDto.getCusSelfCode());
        List<GaiaCustomerBusiness> gaiaCustomerBusinesses = gaiaCustomerBusinessMapper.selectBusinessInfoByPrimaryKey(businessKey);
        if (CollectionUtils.isNotEmpty(gaiaCustomerBusinesses)) {
            // 客户编码（企业）已存在
            throw new CustomResultException(ResultEnum.E0202);
        }
        GaiaCustomerGspinfo gspinfo = new GaiaCustomerGspinfo();
        BeanUtils.copyProperties(customerGspRequestDto, gspinfo);
        //首营日期
        gspinfo.setCusGspDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        //首营状态
        gspinfo.setCusGspStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode().toString());
        // 首营流程编号
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        gspinfo.setCusFlowNo(System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0"));
        gaiaCustomerGspinfoMapper.insert(gspinfo);
        // 提交后自动新建工作流，流程节点根据人员所属门店号，从权限表取出对应质量负责人、企业负责人。审批流程：提交人（已提交）->质量->企业负责人->END
        FeignResult result = feignService.createCustomerWorkflow(gspinfo);
        if (result.getCode() != 0) {
            throw new CustomResultException(result.getMessage());
        }
        return ResultUtil.success();
    }

    @Override
    public Result exportQueryCustomerGspList(CustomerGspInfoListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        // 设置当前登录人的加盟商编号
        dto.setClient(user.getClient());
        // 不分頁查詢
        dto.setPageNum(0);
        dto.setPageSize(0);
        CsvFileInfo csvInfo;
        Result result = new Result();
        List<CustomerGspInfoListReponseDtoCSV> queryGspList = gaiaCustomerGspinfoMapper.exportQueryCustomerGspList(dto);
        if (ObjectUtils.isEmpty(queryGspList)) {
            result.setMessage("提示：查询结果为空");
            return result;
        }
        for (CustomerGspInfoListReponseDtoCSV queryGspResponseDtoCSV : queryGspList) {
            // 转换数据内容
            convertData(queryGspResponseDtoCSV);
        }
        try {
            csvInfo = CsvClient.getCsvByte(queryGspList, "客户首营列表", Collections.singletonList((short) 1));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                bos.flush();
                bos.close();
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void convertData(CustomerGspInfoListReponseDtoCSV queryGspResponseDtoCSV) {
        // 防止科学计数
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getCusFlowNo())) {
            queryGspResponseDtoCSV.setCusFlowNo(queryGspResponseDtoCSV.getCusFlowNo() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getCusLicenceNo())) {
            queryGspResponseDtoCSV.setCusLicenceNo(queryGspResponseDtoCSV.getCusLicenceNo() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getCusSelfCode())) {
            queryGspResponseDtoCSV.setCusSelfCode(queryGspResponseDtoCSV.getCusSelfCode() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getCusCreditCode())) {
            queryGspResponseDtoCSV.setCusCreditCode(queryGspResponseDtoCSV.getCusCreditCode() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getCusBankAccount())) {
            queryGspResponseDtoCSV.setCusBankAccount(queryGspResponseDtoCSV.getCusBankAccount() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getCusContactTel())) {
            queryGspResponseDtoCSV.setCusContactTel(queryGspResponseDtoCSV.getCusContactTel() + "\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getCusClass())) {
            switch (queryGspResponseDtoCSV.getCusClass()) {
                case "1":
                    queryGspResponseDtoCSV.setCusClass("外部客户");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setCusClass("配送中心");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setCusClass("门店");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getCusStatus())) {
            switch (queryGspResponseDtoCSV.getCusStatus()) {
                case "0":
                    queryGspResponseDtoCSV.setCusStatus("正常");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setCusStatus("停用");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getCusPayMode())) {
            switch (queryGspResponseDtoCSV.getCusPayMode()) {
                case "1":
                    queryGspResponseDtoCSV.setCusPayMode("银行转账");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setCusPayMode("承兑汇票");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setCusPayMode("线下票据");
                    break;
                case "4":
                    queryGspResponseDtoCSV.setCusPayMode("赊账");
                    break;
                case "5":
                    queryGspResponseDtoCSV.setCusPayMode("电汇");
                    break;
            }
        }
    }
}
