package com.gov.purchase.module.base.controller.exportTask;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.base.service.exportTask.GaiaExportTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.12.23
 */
@Slf4j
@Api(tags = "导出报表任务")
@RestController
@RequestMapping("exportTask")
public class ExportTaskController {

    @Resource
    private GaiaExportTaskService gaiaExportTaskService;

    @Log("导出列表")
    @ApiOperation("导出列表")
    @PostMapping("getExportTaskList")
    public Result getExportTaskList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize, @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                    @RequestJson(value = "getStatus", required = false) Integer getStatus,
                                    @RequestJson(value = "getType", required = false) Integer getType,
                                    @RequestJson(value = "getDateStart", required = false) String getDateStart,
                                    @RequestJson(value = "getDateEnd", required = false) String getDateEnd
    ) {
        return gaiaExportTaskService.getExportTaskList(pageSize, pageNum, getStatus, getType, getDateStart, getDateEnd);
    }

    @Log("删除导出记录")
    @ApiOperation("删除导出记录")
    @PostMapping("delExportTaskList")
    public Result delExportTaskList(@RequestBody List<Integer> ids) {
        gaiaExportTaskService.delExportTaskList(ids);
        return ResultUtil.success();
    }

}
