package com.gov.purchase.module.goods.controller;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.module.goods.dto.*;
import com.gov.purchase.module.goods.service.EditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Api(tags = "商品维护")
@RestController
@RequestMapping("edit")
public class EditController {

    @Resource
    private EditService editService;

    @Resource
    private OperationFeignService operationFeignService;

    /**
     * 商品列表
     *
     * @param dto QueryProRequestDto
     * @return pageInfo PageInfo<QueryProResponseDto>
     */
    @Log("商品列表")
    @ApiOperation("商品列表")
    @PostMapping("queryProList")
    public Result resultQueryProInfo(@Valid @RequestBody QueryProRequestDto dto) {
        // 藥品信息查詢
        PageInfo<QueryProResponseDto> queryProList = editService.selectQueryProInfo(dto);
        return ResultUtil.success(queryProList);
    }

    /**
     * 商品详情
     *
     * @param client      加盟商
     * @param proSite     地点
     * @param proSelfCode 自编码
     * @return
     */
    @Log("商品详情")
    @ApiOperation("商品详情")
    @PostMapping("queryBusinessPor")
    public Result queryBusinessPor(@RequestJson("client") String client, @RequestJson("proSite") String proSite, @RequestJson("proSelfCode") String proSelfCode) {
        return editService.queryBusinessPor(client, proSite, proSelfCode);
    }

    /**
     * 商品维护
     *
     * @param dto QueryUnDrugCheckRequestDto
     * @return
     */
    @Log("商品维护")
    @ApiOperation("商品维护")
    @PostMapping("proEdit")
    public Result resultProEditInfo(@Valid @RequestBody QueryUnDrugCheckRequestDto dto) {
        // 商品维护
        Map<String, Object> map = editService.updateProEditInfo(dto);

        try {
            // 第三方 gys-operation
            if (map != null && !map.isEmpty()) {
                List<String> proSiteList = (List<String>) map.get("proSiteList");
                List<String> proSelfCodeList = (List<String>) map.get("proSelfCodeList");
                if (CollectionUtils.isNotEmpty(proSiteList) && CollectionUtils.isNotEmpty(proSelfCodeList)) {
                    operationFeignService.multipleStoreProductSync(map.get("client").toString(), proSiteList, proSelfCodeList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResultUtil.success();
    }
}
