package com.gov.purchase.module.purchase.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.ChangePurchaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "采购退货处理")
@RestController
@RequestMapping("purchase")
public class ChangePurchaseController {

    @Resource
    private ChangePurchaseService changePurchaseService;

    /**
     * 批次库存查询
     *
     * @param dto BatchStockListRequestDto
     * @return List<BatchStockListResponseDto>
     */
    @Log("批次库存查询")
    @ApiOperation("批次库存查询")
    @PostMapping("queryBatchStockList")
    public Result resultBatchStockListForPurchase(@Valid @RequestBody BatchStockListRequestDto dto) {

        //批次库存查询
        List<BatchStockListResponseDto> BatchStockList = changePurchaseService.selectBatchStockList(dto);
        return ResultUtil.success(BatchStockList);
    }

    /**
     * 物流模式门店
     *
     * @param dto DeliveryStoreRequestDto
     * @return List<DeliveryStoreResponseDto>
     */
    @Log("物流模式门店")
    @ApiOperation("物流模式门店")
    @PostMapping("getDeliveryStore")
    public Result resultDeliveryStore(@Valid @RequestBody DeliveryStoreRequestDto dto) {

        //批次库存查询
        List<DeliveryStoreResponseDto> BatchStockList = changePurchaseService.selectDeliveryStore(dto);
        return ResultUtil.success(BatchStockList);
    }

    /**
     * 供应商列表
     *
     * @param dto SupplierListForReturnsResponseDto
     * @return List<SupplierListForReturnsRequestDto>
     */
    @Log("供应商列表")
    @ApiOperation("供应商列表")
    @PostMapping("getSupplierListForReturns")
    public Result resultSupplierListForReturns(@Valid @RequestBody SupplierListForReturnsRequestDto dto) {
        //批次库存查询
        List<SupplierListForReturnsResponseDto> supplierListForReturns = changePurchaseService.selectSupplierListForReturns(dto);
        return ResultUtil.success(supplierListForReturns);
    }

    /*
     * 采购退货
     *
     * @param dto PurchaseReturnsRequestDto
     * @return
     */
    @Log("采购退货")
    @ApiOperation("采购退货")
    @PostMapping("purchaseReturns")
    public Result resultPurchaseReturns(@Valid @RequestBody PurchaseReturnsRequestDto dto) {

        // 采购退货
        changePurchaseService.insertPurchaseReturns(dto);

        return ResultUtil.success();
    }
}
