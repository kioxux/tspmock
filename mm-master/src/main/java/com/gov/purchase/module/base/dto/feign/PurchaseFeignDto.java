package com.gov.purchase.module.base.dto.feign;

import lombok.Data;

@Data
public class PurchaseFeignDto {

    private String proCode;
    private String proName;
    private String proSpecs;
    private String priceAverage;
    private String priceReal;
    private String warnValue;
    private String stockUseRate;
    private String wfSite;
}
