package com.gov.purchase.module.supplier.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class SaveSupList1ResponseDTO {

    /**
     * 供应商自编码
     */
    private String supSelfCode;

    /**
     * 供应商名称
     */
    private String supName;

}
