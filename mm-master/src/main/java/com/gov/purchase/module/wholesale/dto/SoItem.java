package com.gov.purchase.module.wholesale.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.02
 */
@Data
public class SoItem {
    @ApiModelProperty(value = "商品编码", name = "soProCode")
    @NotBlank(message = "商品编码不能为空")
    private String soProCode;
    @ApiModelProperty(value = "销售订单数量", name = "soQty")
    @NotNull(message = "销售订单数量不能为空")
    private BigDecimal soQty;
    @ApiModelProperty(value = "订单单位", name = "soUnit")
    @NotBlank(message = "订单单位不能为空")
    private String soUnit;
    @ApiModelProperty(value = "销售订单单价", name = "soPrice")
    @NotNull(message = "单价不能为空")
    private BigDecimal soPrice;
    @ApiModelProperty(value = "订单行金额", name = "soLineAmt")
    @NotNull(message = "行总价不能为空")
    private BigDecimal soLineAmt;
    @ApiModelProperty(value = "地点", name = "soSiteCode")
    @NotBlank(message = "地点不能为空")
    private String soSiteCode;
    @ApiModelProperty(value = "库存地点", name = "soLocationCode")
    private String soLocationCode;
    @ApiModelProperty(value = "批次", name = "soBatch")
//    @NotNull(message = "批次不能为空")
    private String soBatch;
    @ApiModelProperty(value = "税率", name = "soRate")
    private String soRate;
    @ApiModelProperty(value = "计划发货日期", name = "soDeliveryDate")
    private String soDeliveryDate;
    @ApiModelProperty(value = "订单行备注", name = "soLineRemark")
    private String soLineRemark;
    @ApiModelProperty(value = "生产批号", name = "soBatchNo")
    private String soBatchNo;
    @ApiModelProperty(value = "货位号", name = "gsrdHwh")
    private String gsrdHwh;
    @ApiModelProperty(value = "有效期至", name = "batExpiryDate")
    private String batExpiryDate;
    /**
     * 行号
     */
    private String gsrdSerial;

    /**
     * 补货单号
     */
    private String gsrdVoucherId;
}

