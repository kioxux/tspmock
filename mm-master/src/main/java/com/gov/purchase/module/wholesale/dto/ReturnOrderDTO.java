package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description: 退货单传参
 * @author: yzf
 * @create: 2021-12-28 10:17
 */
@Data
public class ReturnOrderDTO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点编码
     */
    private String siteCode;

    /**
     * 仓库委托配送中心
     */
    private String dcWtdc;

    /**
     * 退货日期开始
     */
    private String orderDateStart;

    /**
     * 退货日期结束
     */
    private String orderDateEnd;

    /**
     * 退货单列表
     */
    private List<ReturnOrderVO> list;

    private Integer pageSize;

    private Integer pageNum;
}
