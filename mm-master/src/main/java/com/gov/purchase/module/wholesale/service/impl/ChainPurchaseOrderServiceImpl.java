package com.gov.purchase.module.wholesale.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.entity.GaiaSoHeader;
import com.gov.purchase.entity.*;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaPoHeaderMapper;
import com.gov.purchase.mapper.GaiaSoHeaderMapper;
import com.gov.purchase.mapper.GaiaSoItemMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.wholesale.dto.*;
import com.gov.purchase.module.wholesale.service.ChainPurchaseOrderService;
import com.gov.purchase.utils.CommonUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @description: 连锁采购单
 * @author: yzf
 * @create: 2021-11-18 09:09
 */
@Service
public class ChainPurchaseOrderServiceImpl implements ChainPurchaseOrderService {
    private static final String SO_ID = "SOID";

    @Resource
    private FeignService feignService;

    @Resource
    private GaiaPoHeaderMapper gaiaPoHeaderMapper;

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    private GaiaSoHeaderMapper gaiaSoHeaderMapper;

    @Resource
    private GaiaSoItemMapper gaiaSoItemMapper;

    @Resource
    RedisClient redisClient;

    @Override
    public List<CompadmWmsVO> queryCompadmWms() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaPoHeaderMapper.queryCompadmWms(user.getClient());
    }

    @Override
    public List<PurchaseCompanyVO> queryCompanyCode() {
        TokenUser user = feignService.getLoginInfo();
        List<PurchaseCompanyVO> purchaseCompanyVOS = new ArrayList<>();
        List<PurchaseCompanyDTO> purchaseCompanyDTOS = gaiaPoHeaderMapper.queryCompanyCode(user.getClient());
        if (CollectionUtils.isNotEmpty(purchaseCompanyDTOS)) {
            purchaseCompanyDTOS.forEach(
                    item -> {
                        PurchaseCompanyVO purchaseCompanyVO = new PurchaseCompanyVO();
                        purchaseCompanyVO.setCode(item.getPoCompanyCode());
                        purchaseCompanyVO.setName(StringUtils.isNotBlank(item.getDcName()) ? item.getDcName() : item.getStoName());
                        purchaseCompanyVOS.add(purchaseCompanyVO);
                    }
            );
        }
        return purchaseCompanyVOS;
    }

    @Override
    public List<GaiaStoreData> queryStoreList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaStoreDataMapper.getStoreDataListByClient(user.getClient());
    }

    @Override
    public Result queryChainPurchaseOrderList(ChainPurchaseOrderDto chainPurchaseOrderDto) {
        TokenUser user = feignService.getLoginInfo();
        chainPurchaseOrderDto.setClient(user.getClient());
        PageHelper.startPage(chainPurchaseOrderDto.getPageNum(), chainPurchaseOrderDto.getPageSize());
        List<ChainPurchaseOrderVO> chainPurchaseOrderVOS = gaiaPoHeaderMapper.queryChainPurchaseOrderList(chainPurchaseOrderDto);
        if (CollectionUtils.isNotEmpty(chainPurchaseOrderVOS)) {
            chainPurchaseOrderVOS.forEach(
                    item -> {
                        item.setCusName(StringUtils.isNotBlank(item.getDcName()) ? item.getDcName() : item.getStoName());
                    }
            );
        }
        PageInfo<ChainPurchaseOrderVO> pageInfo = new PageInfo<>(chainPurchaseOrderVOS);
        return ResultUtil.success(pageInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updateFlag(List<ChainPurchaseOrderDto> chainPurchaseOrderDto) {
        TokenUser user = feignService.getLoginInfo();
        if (CollectionUtils.isNotEmpty(chainPurchaseOrderDto)) {
            chainPurchaseOrderDto.forEach(
                    item -> {
                        item.setClient(user.getClient());
                        item.setPoSoFlag("2");
                        item.setPoDnFlag("1");
                        item.setPoUpdateBy(user.getClient());
                        item.setPoUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
                        item.setPoUpdateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    }
            );
        }
        gaiaPoHeaderMapper.updateFlag(chainPurchaseOrderDto);
        return ResultUtil.success();
    }

    @Override
    public Result queryChainPurchaseOrderDetailList(ChainPurchaseOrderDto chainPurchaseOrderDto) {
        TokenUser user = feignService.getLoginInfo();
        chainPurchaseOrderDto.setClient(user.getClient());
        ChainPurchaseOrderDetailVO chainPurchaseOrderDetailVO = new ChainPurchaseOrderDetailVO();
        List<ChainPurchaseOrderVO> chainPurchaseOrderDtos = gaiaPoHeaderMapper.queryChainPurchaseOrderList(chainPurchaseOrderDto);
        if (CollectionUtils.isNotEmpty(chainPurchaseOrderDtos)) {
            chainPurchaseOrderDetailVO.setPoId(chainPurchaseOrderDtos.get(0).getPoId());
            chainPurchaseOrderDetailVO.setPoDate(chainPurchaseOrderDtos.get(0).getPoDate());
            chainPurchaseOrderDetailVO.setPoDate(chainPurchaseOrderDtos.get(0).getPoDate());
            String name = StringUtils.isNotBlank(chainPurchaseOrderDtos.get(0).getDcName()) ? chainPurchaseOrderDtos.get(0).getDcName() : chainPurchaseOrderDtos.get(0).getStoName();
            chainPurchaseOrderDetailVO.setCustomerName(chainPurchaseOrderDtos.get(0).getPoCompanyCode() + "-" + name);
            chainPurchaseOrderDetailVO.setDeliveryPerCode(chainPurchaseOrderDtos.get(0).getDeliveryPerCode());
            chainPurchaseOrderDetailVO.setDeliveryPerName(chainPurchaseOrderDtos.get(0).getDeliveryPerName());
            chainPurchaseOrderDetailVO.setSaleDate(chainPurchaseOrderDtos.get(0).getPoDnDate());
            chainPurchaseOrderDetailVO.setPoSoFlag(chainPurchaseOrderDtos.get(0).getPoSoFlag());
            chainPurchaseOrderDetailVO.setPoHeadRemark(chainPurchaseOrderDtos.get(0).getPoHeadRemark());
            chainPurchaseOrderDetailVO.setPoCompanyCode(chainPurchaseOrderDtos.get(0).getPoCompanyCode());
            chainPurchaseOrderDetailVO.setPoSupplierId(chainPurchaseOrderDtos.get(0).getPoSupplierId());
            chainPurchaseOrderDetailVO.setPriceType(chainPurchaseOrderDto.getPriceType());
            chainPurchaseOrderDetailVO.setSalesManCode(chainPurchaseOrderDto.getSalesManCode());
            if (StringUtils.isNotBlank(chainPurchaseOrderDto.getSalesManCode())) {
                List<GaiaWholesaleSalesman> wholesaleSalesmanList = gaiaPoHeaderMapper.querySalesManList(user.getClient(), chainPurchaseOrderDto.getSalesManCode());
                chainPurchaseOrderDetailVO.setSalePerName(CollectionUtils.isNotEmpty(wholesaleSalesmanList) ? wholesaleSalesmanList.get(0).getGssName() : "");
            }
        }
        List<ChainPurchaseOrderDetailDTO> chainPurchaseOrderDetailDTOS = gaiaPoHeaderMapper.queryChainPurchaseOrderDetail(chainPurchaseOrderDto);
        if (CollectionUtils.isNotEmpty(chainPurchaseOrderDetailDTOS)) {
            chainPurchaseOrderDetailDTOS.forEach(
                    item -> {
                        // 实际数量
                        item.setRealNeedQty(item.getPoQty());
                        if ("0".equalsIgnoreCase(chainPurchaseOrderDetailVO.getPoSoFlag()) && chainPurchaseOrderDto.getPriceType() != null) {
                            if (chainPurchaseOrderDto.getPriceType().intValue() == 1) {
                                item.setSalePrice(item.getProLsj());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 2) {
                                item.setSalePrice(item.getProHyj());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 3) {
                                item.setSalePrice(item.getProGpj());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 4) {
                                item.setSalePrice(item.getProGlj());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 5) {
                                item.setSalePrice(item.getProYsj1());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 6) {
                                item.setSalePrice(item.getProYsj2());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 7) {
                                item.setSalePrice(item.getProYsj3());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 8) {
                                item.setSalePrice(item.getBatPoPrice());
                            }
                        }
                        if (item.getSalePrice() != null && item.getRealNeedQty() != null) {
                            // 销售金额
                            item.setSaleTotal(item.getSalePrice().multiply(item.getRealNeedQty()));
                        }
                        // 零售金额
                        if (item.getPriceNormal() != null && item.getRealNeedQty() != null) {
                            item.setTotalNormal(item.getPriceNormal().multiply(item.getRealNeedQty()));
                        }
                    }
            );
            chainPurchaseOrderDetailVO.setDetails(chainPurchaseOrderDetailDTOS);
        }
        return ResultUtil.success(chainPurchaseOrderDetailVO);
    }

    @Override
    public Result queryChainPurchaseDetails(ChainPurchaseOrderDto chainPurchaseOrderDto) {
        TokenUser user = feignService.getLoginInfo();
        chainPurchaseOrderDto.setClient(user.getClient());
        PageHelper.startPage(chainPurchaseOrderDto.getPageNum(), chainPurchaseOrderDto.getPageSize());
        List<ChainPurchaseOrderDetailDTO> chainPurchaseOrderDetailDTOS = gaiaPoHeaderMapper.queryChainPurchaseOrderDetail(chainPurchaseOrderDto);
        if (CollectionUtils.isNotEmpty(chainPurchaseOrderDetailDTOS)) {
            chainPurchaseOrderDetailDTOS.forEach(
                    item -> {
                        item.setCustomerName(StringUtils.isNotBlank(item.getDcName()) ? item.getDcName() : item.getCusStoName());
                        // 实际数量
                        item.setRealNeedQty(item.getPoQty());
                        if (chainPurchaseOrderDto.getPriceType() != null) {
                            if (chainPurchaseOrderDto.getPriceType().intValue() == 1) {
                                item.setSalePrice(item.getProLsj());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 2) {
                                item.setSalePrice(item.getProHyj());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 3) {
                                item.setSalePrice(item.getProGpj());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 4) {
                                item.setSalePrice(item.getProGlj());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 5) {
                                item.setSalePrice(item.getProYsj1());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 6) {
                                item.setSalePrice(item.getProYsj2());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 7) {
                                item.setSalePrice(item.getProYsj3());
                            } else if (chainPurchaseOrderDto.getPriceType().intValue() == 8) {
                                item.setSalePrice(item.getBatPoPrice());
                            }
                        }
                        if (item.getSalePrice() != null && item.getRealNeedQty() != null) {
                            // 销售金额
                            item.setSaleTotal(item.getSalePrice().multiply(item.getRealNeedQty()));
                        }
                        // 零售金额
                        if (item.getPriceNormal() != null && item.getRealNeedQty() != null) {
                            item.setTotalNormal(item.getPriceNormal().multiply(item.getRealNeedQty()));
                        }
                    }
            );
        }
        PageInfo<ChainPurchaseOrderDetailDTO> pageInfo = new PageInfo<>(chainPurchaseOrderDetailDTOS);
        return ResultUtil.success(pageInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result auditChainPurchaseOrder(List<ChainPurchaseOrderDto> chainPurchaseOrderDto) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        List<GaiaPoHeader> gaiaPoHeaders = new ArrayList<>();
        List<ChainPurchaseOrderDetailDTO> list = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(chainPurchaseOrderDto)) {
            chainPurchaseOrderDto.forEach(
                    item -> {
                        if (StringUtils.isNotBlank(item.getPoId())) {
                            GaiaPoHeaderKey key = new GaiaPoHeaderKey();
                            key.setClient(user.getClient());
                            key.setPoId(item.getPoId());
                            GaiaPoHeader existPo = gaiaPoHeaderMapper.selectByPrimaryKey(key);
                            if (existPo == null) {
                                return;
                            }
                            if (StringUtils.isNotBlank(existPo.getPoSoFlag()) && !"0".equalsIgnoreCase(existPo.getPoSoFlag())) {
                                return;
                            }
                            GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
                            gaiaPoHeader.setClient(user.getClient());
                            gaiaPoHeader.setPoId(item.getPoId());
                            gaiaPoHeader.setPoDeliveryTypeStore(item.getStoCode());
                            gaiaPoHeader.setPoSupplierId(item.getPoSupplierId());
                            gaiaPoHeader.setPoCompanyCode(item.getCode());
                            gaiaPoHeaders.add(gaiaPoHeader);
                            if (CollectionUtils.isEmpty(item.getDetails())) {
                                item.setClient(user.getClient());
                                List<ChainPurchaseOrderDetailDTO> details = gaiaPoHeaderMapper.queryChainPurchaseOrderDetail(item);
                                for (ChainPurchaseOrderDetailDTO dto : details) {
                                    dto.setRealNeedQty(dto.getPoQty());
                                }
                                list.addAll(details);
                            }
                        }
                        if (CollectionUtils.isNotEmpty(item.getDetails())) {
                            item.setDetails(item.getDetails().stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getClient() + ";" + o.getPoId() + ";" + o.getPoLineNo()))), ArrayList::new)));
                            item.getDetails().forEach(
                                    detail -> {
                                        GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
                                        gaiaPoHeader.setClient(user.getClient());
                                        gaiaPoHeader.setPoId(detail.getPoId());
                                        gaiaPoHeader.setPoDeliveryTypeStore(detail.getPoDeliveryTypeStore());
                                        gaiaPoHeader.setPoSupplierId(detail.getPoSupplierId());
                                        gaiaPoHeader.setPoCompanyCode(detail.getPoCompanyCode());
                                        gaiaPoHeaders.add(gaiaPoHeader);
                                        ChainPurchaseOrderDto purchaseOrderDto = new ChainPurchaseOrderDto();
                                        purchaseOrderDto.setClient(user.getClient());
                                        purchaseOrderDto.setPoId(detail.getPoId());
                                        List<ChainPurchaseOrderDetailDTO> details = gaiaPoHeaderMapper.queryChainPurchaseOrderDetail(purchaseOrderDto);
                                        for (ChainPurchaseOrderDetailDTO dto : details) {
                                            if (dto.getPoProCode().equals(detail.getPoProCode()) && dto.getPoLineNo().equals(detail.getPoLineNo())) {
                                                dto.setRealNeedQty(detail.getRealNeedQty());
                                            }
                                        }
                                        list.addAll(details);
                                    }
                            );
                        }
                    }
            );
            List<GaiaPoHeader> distinctClass = gaiaPoHeaders.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getClient() + ";" + o.getPoId()))), ArrayList::new));
            List<ChainPurchaseOrderDto> dtoList = new ArrayList<>();
            List<ChainPurchaseOrderDetailDTO> realNeedList = list.stream().filter(chainPurchaseOrderDetailDTO -> ObjectUtils.isNotEmpty(chainPurchaseOrderDetailDTO.getRealNeedQty())).collect(Collectors.toList());
            List<ChainPurchaseOrderDetailDTO> distinctDetailClass =
                    realNeedList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getClient() + ";" + o.getPoId() + ";" + o.getPoLineNo()))), ArrayList::new));
            if (CollectionUtils.isNotEmpty(distinctClass)) {
                distinctClass.forEach(
                        item -> {
                            ChainPurchaseOrderDto dto = new ChainPurchaseOrderDto();
                            dto.setClient(user.getClient());
                            dto.setPoSoFlag("1");
                            dto.setPoId(item.getPoId());
                            dto.setPoUpdateBy(user.getClient());
                            dto.setPoUpdateDate(DateUtils.getCurrentDateStrYYMMDD());
                            dto.setPoUpdateTime(DateUtils.getCurrentTimeStrHHMMSS());
                            dtoList.add(dto);

                            GaiaSoHeader gaiaSoHeader = new GaiaSoHeader();
                            gaiaSoHeader.setClient(item.getClient());
                            gaiaSoHeader.setGwsCode(chainPurchaseOrderDto.get(0).getSalesManCode());
                            // 订单类型
                            gaiaSoHeader.setSoType("WOR");
                            String newPoId = new CommonUtils().getPoId(item.getClient(), "WOR", gaiaSoHeaderMapper, redisClient);
                            Long poId = Long.parseLong(newPoId.substring(newPoId.length() - 10));
                            while (redisClient.exists(SO_ID + poId)) {
                                poId++;
                            }
                            redisClient.set(SO_ID + poId, poId.toString(), 1800);
                            gaiaSoHeader.setSoId(CommonEnum.SoType.WOR.getName() + poId);
                            gaiaSoHeader.setSoCustomerId(item.getPoDeliveryTypeStore());
                            gaiaSoHeader.setSoCompanyCode(item.getPoSupplierId());
                            gaiaSoHeader.setSoApproveStatus("2");
                            // 凭证日期
                            gaiaSoHeader.setSoDate(DateUtils.getCurrentDateStrYYMMDD());
                            // 创建人
                            gaiaSoHeader.setSoCreateBy(user.getUserId());
                            // 创建日期
                            gaiaSoHeader.setSoCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                            // 创建时间
                            gaiaSoHeader.setSoCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                            // 客户编码
                            gaiaSoHeader.setSoCustomerId(item.getPoDeliveryTypeStore());
                            gaiaSoHeader.setSoCustomer2(item.getPoCompanyCode());
                            // 前序单据
                            gaiaSoHeader.setSoReferOrder(item.getPoId());
                            gaiaSoHeaderMapper.insertSelective(gaiaSoHeader);
                            if (CollectionUtils.isNotEmpty(distinctDetailClass)) {
                                distinctDetailClass.forEach(
                                        itemDetail -> {
                                            if (item.getClient().equalsIgnoreCase(itemDetail.getClient()) &&
                                                    item.getPoId().equalsIgnoreCase(itemDetail.getPoId())) {
                                                GaiaSoItem gaiaSoItem = new GaiaSoItem();
                                                gaiaSoItem.setClient(itemDetail.getClient());
                                                // 单号
                                                gaiaSoItem.setSoId(gaiaSoHeader.getSoId());
                                                // 行号
                                                gaiaSoItem.setSoLineNo(itemDetail.getPoLineNo());
                                                // 商品编码
                                                gaiaSoItem.setSoProCode(itemDetail.getPoProCode());
                                                // 数量
                                                gaiaSoItem.setSoQty(itemDetail.getRealNeedQty());
                                                // 单位
                                                gaiaSoItem.setSoUnit(itemDetail.getProUnit());
                                                // 价格
                                                if (chainPurchaseOrderDto.get(0).getPriceType() != null) {
                                                    if (chainPurchaseOrderDto.get(0).getPriceType().intValue() == 1) {
                                                        gaiaSoItem.setSoPrice(itemDetail.getProLsj());
                                                    } else if (chainPurchaseOrderDto.get(0).getPriceType().intValue() == 2) {
                                                        gaiaSoItem.setSoPrice(itemDetail.getProHyj());
                                                    } else if (chainPurchaseOrderDto.get(0).getPriceType().intValue() == 3) {
                                                        gaiaSoItem.setSoPrice(itemDetail.getProGpj());
                                                    } else if (chainPurchaseOrderDto.get(0).getPriceType().intValue() == 4) {
                                                        gaiaSoItem.setSoPrice(itemDetail.getProGlj());
                                                    } else if (chainPurchaseOrderDto.get(0).getPriceType().intValue() == 5) {
                                                        gaiaSoItem.setSoPrice(itemDetail.getProYsj1());
                                                    } else if (chainPurchaseOrderDto.get(0).getPriceType().intValue() == 6) {
                                                        gaiaSoItem.setSoPrice(itemDetail.getProYsj2());
                                                    } else if (chainPurchaseOrderDto.get(0).getPriceType().intValue() == 7) {
                                                        gaiaSoItem.setSoPrice(itemDetail.getProYsj3());
                                                    } else if (chainPurchaseOrderDto.get(0).getPriceType().intValue() == 8) {
                                                        gaiaSoItem.setSoPrice(itemDetail.getBatPoPrice());
                                                    }
                                                } else {
                                                    gaiaSoItem.setSoPrice(itemDetail.getSalePrice());
                                                }
                                                if (gaiaSoItem.getSoPrice() != null && gaiaSoItem.getSoQty() != null) {
                                                    // 金额 -> 订单行金额
                                                    gaiaSoItem.setSoLineAmt(gaiaSoItem.getSoPrice().multiply(gaiaSoItem.getSoQty()));
                                                }
                                                // 地点
                                                gaiaSoItem.setSoSiteCode(item.getPoSupplierId());
                                                // 库存地点
                                                gaiaSoItem.setSoLocationCode("1000");
                                                // 计划发货日期
                                                gaiaSoItem.setSoDeliveryDate(DateUtils.getCurrentDateStrYYMMDD());
                                                gaiaSoItem.setSoRate(itemDetail.getProOutputTax());
                                                // 原单号
                                                gaiaSoItem.setSoReferOrder(itemDetail.getPoId());
                                                gaiaSoItem.setSoLineDelete("0");
                                                // 原单行号
                                                gaiaSoItem.setSoReferOrderLineno(itemDetail.getPoLineNo());
                                                // 批号
                                                gaiaSoItem.setSoBatchNo(itemDetail.getPoBatchNo());
                                                gaiaSoItemMapper.insert(gaiaSoItem);
                                            }
                                        }
                                );
                            }
                        }
                );
                gaiaPoHeaderMapper.updateFlag(dtoList);
            }
        }
        return ResultUtil.success();
    }

    @Override
    public List<GaiaWholesaleSalesman> querySalesManList() {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        return gaiaPoHeaderMapper.querySalesManList(user.getClient(), "");
    }
}
