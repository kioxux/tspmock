package com.gov.purchase.module.store.dto.price;

import com.gov.purchase.entity.GaiaPriceAdjust;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.16
 */
@Data
public class GaiaPriceAdjustDto extends GaiaPriceAdjust {

    /**
     * 会员日价
     */
    private BigDecimal gsppPriceHyr;

    /**
     * 店名
     */
    private String stoName;

    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 生产单位
     */
    private String proFactoryName;

    /**
     * 店组
     */
    private String stogCode;

    /**
     * 品牌
     */
    private String proBrand;

    /**
     * 最新进价
     */
    private BigDecimal lastPurchasePrice;
}
