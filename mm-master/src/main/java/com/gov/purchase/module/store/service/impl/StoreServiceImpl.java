package com.gov.purchase.module.store.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.QRCodeUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.store.dto.GaiaStoreDataDto;
import com.gov.purchase.module.store.dto.store.*;
import com.gov.purchase.module.store.service.StoreService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

;

@Service
@Slf4j
public class StoreServiceImpl implements StoreService {
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaStoreTaxMapper gaiaStoreTaxMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaCompadmMapper compadmMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private GaiaOfficialAccountsMapper gaiaOfficialAccountsMapper;
    @Resource
    private QRCodeUtil qrCodeUtil;

    /**
     * 门店列表
     *
     * @param
     * @return
     */
    @Override
    public PageInfo<StoreListResponseDto> queryStoreList(StoreListRequestDto dto) {
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        return new PageInfo<>(gaiaStoreDataMapper.queryStoreList(dto));
    }


    /**
     * 门店详情
     *
     * @return
     */
    @Override
    public StoreInfoDetailReponseDto getStoreInfo(StoreInfoDetailRequestDto dto) {
        return gaiaStoreDataMapper.getStoreInfo(dto);
    }

    @Override
    public Result validStoreInfo(StoreInfoInsertOrUpdateValidDto dto) {

        // 非空和长度验证见dto注解

        // 判断重复性
        GaiaStoreDataKey key = new GaiaStoreDataKey();
        key.setClient(dto.getClient());
        key.setStoCode(dto.getStoCode());
        int currentCount = gaiaStoreDataMapper.selectCountByPrimaryKey(key);
        // 新建
        if (dto.getType().equals("0")) {
            if (currentCount > 0) {
                // 新建时候 已经存在
                throw new CustomResultException(ResultEnum.STORE_ALREADY_EXIST_ERROR);
            }
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            key.setStoCode(dto.getStoCode());
            key.setClient(dto.getClient());
            if (gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey) != null) {
                // 门店编号已存在
                throw new CustomResultException(ResultEnum.STORE_ALREADY_EXIST_ERROR);
            }

        }
        // 更新
        else if (dto.getType().equals("1")) {
            if (currentCount == 0) {
                // 更新时候 记录不存在
                throw new CustomResultException(ResultEnum.STORE_NOT_EXIST_ERROR);
            }
            if (currentCount > 1) {
                // 更新时候 记录太多
                throw new CustomResultException(ResultEnum.STORE_TOO_MANY_EXIST_ERROR);
            }
        } else {
            // 标识有误
            throw new CustomResultException(ResultEnum.REQUEST_TYPE_ERROR);
        }
        return ResultUtil.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result insertStoreInfo(StoreInfoInsertOrUpdateRequestDto dto) {

        StoreInfoInsertOrUpdateValidDto validDto = new StoreInfoInsertOrUpdateValidDto();
        BeanUtils.copyProperties(dto, validDto);
        validDto.setType("0");
        // 验证
        validStoreInfo(validDto);
        // 新增数据至 Gaia_Store_Data
        GaiaStoreData gaiaStoreData = new GaiaStoreData();
        BeanUtils.copyProperties(validDto, gaiaStoreData);
        // 如果纳税主体为空 则设置当前门店为纳税主体
//        if(StringUtils.isBlank(gaiaStoreData.getStoTaxSubject())){
//            gaiaStoreData.setStoTaxSubject(gaiaStoreData.getStoCode());
//        }
        gaiaStoreDataMapper.insertSelective(gaiaStoreData);
        // 新增记录 Gaia_Store_Tax
        insertTax(validDto, 0);

        return ResultUtil.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updateStoreInfo(StoreInfoInsertOrUpdateRequestDto dto) {
        checkStoDeliveryMode(dto, dto.getClient());

        StoreInfoInsertOrUpdateValidDto validDto = new StoreInfoInsertOrUpdateValidDto();
        BeanUtils.copyProperties(dto, validDto);
        validDto.setType("1");
        // 验证
        validStoreInfo(validDto);
        // 更新数据至 Gaia_Store_Data
        GaiaStoreData gaiaStoreData = new GaiaStoreData();
        BeanUtils.copyProperties(validDto, gaiaStoreData);
        GaiaStoreDataKey existKey = new GaiaStoreDataKey();
        existKey.setClient(dto.getClient());
        existKey.setStoCode(dto.getStoCode());
        GaiaStoreData existData = gaiaStoreDataMapper.selectByPrimaryKey(existKey);

        if (existData == null) {
            throw new CustomResultException(ResultEnum.STORE_NOT_EXIST_ERROR);
        }
        //如果账期类型为0-账期天数，忽略欠款标志更新
        if ("0".equals(gaiaStoreData.getStoZqlx())) {
            gaiaStoreData.setStoArrearsFlag(null);
        }
        // 更新表
        gaiaStoreDataMapper.updateByPrimaryKeySelective(gaiaStoreData);

        // 判断是否要更新日志
        if (hasChanged(dto.getStoTaxClass(), existData.getStoTaxClass()) || hasChanged(dto.getStoTaxRate(), existData.getStoTaxRate()) ||
                hasChanged(dto.getStoTaxSubject(), existData.getStoTaxSubject())) {
            GaiaStoreTax key = new GaiaStoreTax();
            key.setClient(validDto.getClient());
            key.setStoCode(validDto.getStoCode());
            key.setStoTaxEnddate("99991231");
            GaiaStoreTax tax = gaiaStoreTaxMapper.selectByClientAndCode(key);
            if (tax == null) {
                //插入一条日志信息（GAIA_STORE_TAX）开始时间 = 当前时间  结束时间 = null
                insertTax(validDto, 0);
            } else {
                tax.setStoTaxEnddate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                //将结束日期（STO_TAX_ENDDATE）更新为当前时间
                gaiaStoreTaxMapper.updateByPrimaryKey(tax);

                //插入一条日志信息（GAIA_STORE_TAX）开始时间 = 当前时间 + 1天 结束时间 = null
                insertTax(validDto, 1);
            }
        }

        return ResultUtil.success();
    }

    //新增一条 GaiaStoreTax 记录
    private int insertTax(StoreInfoInsertOrUpdateValidDto dto, int day) {
        GaiaStoreTax tax = new GaiaStoreTax();
        String startTime = DateUtils.formatLocalDate(LocalDate.now().plusDays(day), "yyyyMMdd");
        tax.setClient(dto.getClient());
        tax.setStoCode(dto.getStoCode());
        // uuid
        tax.setStoGuid(UUID.randomUUID().toString().replaceAll("-", ""));

        tax.setStoTaxClass(dto.getStoTaxClass());
        tax.setStoTaxSubject(dto.getStoTaxSubject());
        tax.setStoTaxRate(dto.getStoTaxRate());
        tax.setStoTaxStartdate(startTime);
        tax.setStoTaxEnddate("99991231");

        gaiaStoreTaxMapper.insertSelective(tax);
        return 0;
    }

    private boolean hasChanged(String a, String b) {

        if (a == null && b == null) {
            return false;
        }
        if (a == null && b != null) {
            return true;
        }
        if (b == null && a != null) {
            return true;
        }
        return !a.equals(b);
    }


    @Override
    public Result queryChainList() {
        TokenUser user = feignService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        map.put("CLIENT", user.getClient());
        map.put("USERID", user.getUserId());
        return ResultUtil.success(compadmMapper.SelectDcAuth(map));
    }


    @Override
    public Result queryStoreList(String chainCode) {
        TokenUser user = feignService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(chainCode)) {
            //连锁总部的code
            map.put("CHAINCODE", chainCode);
        }
        map.put("CLIENT", user.getClient());
        map.put("USERID", user.getUserId());
        return ResultUtil.success(gaiaStoreDataMapper.SelectStoreAuth(map));
    }

    @Override
    public Result queryList(String stoChainHead, String stoCode, List<String> stoCodeList, String stoName, Integer pageNo, Integer pageSize) {
        TokenUser user = feignService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(stoChainHead)) {
            //连锁总部的code
            map.put("CHAINCODE", stoChainHead);
        }
        if (StringUtils.isNotBlank(stoCode)) {
            //门店编码
            map.put("STOCODE", stoCode);
        }
        if (StringUtils.isNotBlank(stoName)) {
            //门店名称
            map.put("STOCNAME", stoName);
        }
        if (null != stoCodeList && !stoCodeList.isEmpty()) {
            //门店名称
            map.put("STOCODELIST", stoCodeList);
        }
        map.put("CLIENT", user.getClient());
        map.put("USERID", user.getUserId());
        // 分頁查詢
        PageHelper.startPage(pageNo, pageSize);
        List<GaiaStoreDataDto> list = gaiaStoreDataMapper.selectstoreDataList(map);
        return ResultUtil.success(new PageInfo<>(list));
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updateStoreList(List<GaiaStoreData> storeDataList) {
        if (null == storeDataList || storeDataList.isEmpty()) {
            throw new CustomResultException("至少传一条数据");
        }
        TokenUser user = feignService.getLoginInfo();
        // 配送方式校验
        checkStoDeliveryMode(storeDataList, user.getClient());
        List<GaiaStoreData> newList = new ArrayList<>();
        storeDataList.forEach(item -> {
            GaiaStoreData storeData = new GaiaStoreData();
            storeData.setClient(user.getClient());
            storeData.setStoCode(item.getStoCode());
            storeData.setStoShortName(item.getStoShortName());
            storeData.setStoDcCode(item.getStoDcCode());
            storeData.setStoMdSite(item.getStoMdSite());
            if ("2".equals(item.getStoAttribute())) {
                if (StringUtils.isBlank(item.getStoChainHead())) {
                    throw new CustomResultException("门店编码" + item.getStoCode() + "连锁总部的编码不能为空");
                }
                storeData.setStoChainHead(item.getStoChainHead());
            } else {
                storeData.setStoChainHead("");
                storeData.setStoDcCode("");
            }
            storeData.setStoAttribute(item.getStoAttribute());
            storeData.setStoStatus(item.getStoStatus());
            storeData.setStoArea(item.getStoArea());
            storeData.setStoOpenDate(item.getStoOpenDate());
            storeData.setStoAdd(item.getStoAdd());
            storeData.setStoIfMedicalcare(item.getStoIfMedicalcare());
            storeData.setStoDcCode(item.getStoDcCode());
            storeData.setStoModiDate(DateUtils.getCurrentDateStrYYMMDD());
            storeData.setStoModiId(user.getUserId());
            storeData.setStoDeliveryMode(item.getStoDeliveryMode());
            newList.add(storeData);

        });
        return ResultUtil.success(gaiaStoreDataMapper.updateBatch(newList));
    }

    private void checkStoDeliveryMode(List<GaiaStoreData> storeDataList, String client) {
        if (CollectionUtils.isEmpty(storeDataList)) {
            return;
        }
        List<String> stoCodeList = storeDataList.stream().filter(Objects::nonNull)
                .filter(item -> !"4".equals(item.getStoDeliveryMode()) && !"5".equals(item.getStoDeliveryMode()))
                .map(GaiaStoreData::getStoCode).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(stoCodeList)) {
            return;
        }
        List<String> stoExistsCustomerCodeList = gaiaStoreDataMapper.getStoExistsCustomer(client, stoCodeList);
        if (!CollectionUtils.isEmpty(stoExistsCustomerCodeList)) {
            throw new CustomResultException(MessageFormat.format("门店[{0}]配送方式只能选择[内部批发]", String.join(",", stoExistsCustomerCodeList)));
        }
    }

    /**
     * 配送中心
     *
     * @return
     */
    @Override
    public Result getDcList() {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaDcData> list = gaiaDcDataMapper.getDcList(user.getClient());
        return ResultUtil.success(list);
    }

    /**
     * 门店编码详情
     */
    @Override
    public GaiaStoreData getStoDetails(String stoCode) {
        String client = feignService.getLoginInfo().getClient();
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(client);
        gaiaStoreDataKey.setStoCode(stoCode);
        GaiaStoreData storeData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        return storeData;
    }

    /**
     * 门店编辑
     */
    @Override
    public void editStoDetails(EditStoDetailsRequestDTO editStoDetails) {
        String client = feignService.getLoginInfo().getClient();
        checkStoDeliveryMode(editStoDetails, client);
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(client);
        gaiaStoreDataKey.setStoCode(editStoDetails.getStoCode());
        GaiaStoreData storeDataExit = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        if (ObjectUtils.isEmpty(storeDataExit)) {
            throw new CustomResultException("该数据不存在");
        }
        GaiaStoreData storeData = new GaiaStoreData();
        BeanUtils.copyProperties(editStoDetails, storeData);
        gaiaStoreDataMapper.updateByPrimaryKey(storeData);
    }

    private void checkStoDeliveryMode(EditStoDetailsRequestDTO editStoDetails, String client) {
        if (ObjectUtils.isEmpty(editStoDetails) || "4".equals(editStoDetails.getStoDeliveryMode()) || "5".equals(editStoDetails.getStoDeliveryMode())) {
            return;
        }
        String stoCode = gaiaStoreDataMapper.getStoExistsCustomer2(client, editStoDetails.getStoCode());
        if (StringUtils.isBlank(stoCode)) {
            return;
        }
        throw new CustomResultException(MessageFormat.format("门店[{0}]配送方式只能选择[内部批发]", stoCode));
    }

    private void checkStoDeliveryMode(StoreInfoInsertOrUpdateRequestDto dto, String client) {
        if (ObjectUtils.isEmpty(dto) || "4".equals(dto.getStoDeliveryMode()) || "5".equals(dto.getStoDeliveryMode())) {
            return;
        }
        String stoCode = gaiaStoreDataMapper.getStoExistsCustomer2(client, dto.getStoCode());
        if (StringUtils.isBlank(stoCode)) {
            return;
        }
        throw new CustomResultException(MessageFormat.format("门店[{0}]配送方式只能选择[内部批发]", stoCode));
    }

    /**
     * 生成门店二维码
     *
     * @param stoCode
     * @return
     */
    @Override
    public Result getQrCode(String stoCode) {
        String client = feignService.getLoginInfo().getClient();
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(client);
        gaiaStoreDataKey.setStoCode(stoCode);
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        if (gaiaStoreData == null) {
            throw new CustomResultException("该数据不存在");
        }
        String code = "";
        int type = 0;
        // 连锁
        if (CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(gaiaStoreData.getStoAttribute())
                && StringUtils.isNotBlank(gaiaStoreData.getStoChainHead())) {
            code = gaiaStoreData.getStoChainHead();
            type = 2;
        } else { // 单体
            code = stoCode;
            type = 3;
        }
        List<GaiaOfficialAccounts> list = gaiaOfficialAccountsMapper.selectConfig(client, type, code);
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("当前公众号未配置APPID和APPSECRET");
        }
        String appid = "", appsecret = "";
        for (GaiaOfficialAccounts gaiaOfficialAccounts : list) {
            if ("WECHAT_OFFICIAL_ACCOUNTS_APPID".equals(gaiaOfficialAccounts.getGoaObjectParam()) &&
                    StringUtils.isNotBlank(gaiaOfficialAccounts.getGoaObjectValue())) {
                appid = gaiaOfficialAccounts.getGoaObjectValue();
            }
            if ("WECHAT_OFFICIAL_ACCOUNTS_APPSECRET".equals(gaiaOfficialAccounts.getGoaObjectParam()) &&
                    StringUtils.isNotBlank(gaiaOfficialAccounts.getGoaObjectValue())) {
                appsecret = gaiaOfficialAccounts.getGoaObjectValue();
            }
        }
        if (StringUtils.isBlank(appid) || StringUtils.isBlank(appsecret)) {
            throw new CustomResultException("当前公众号未配置APPID和APPSECRET");
        }
        String token = qrCodeUtil.getAccessToken(appid, appsecret);
        if (StringUtils.isBlank(token)) {
            throw new CustomResultException("请检查公众号APPID和APPSECRET是否正确");
        }
        String qrCode = qrCodeUtil.createQrcode(token, client + "," + code + "," + stoCode);
        if (StringUtils.isBlank(qrCode)) {
            throw new CustomResultException("公众号二维码生成失败");
        }
        return ResultUtil.success(qrCode);
    }


    /**
     * 导出
     *
     * @param
     * @return
     */
    @Override
    public Result exportList(String stoChainHead, String stoCode, List<String> stoCodeList, String stoName) {
        try {
            TokenUser user = feignService.getLoginInfo();
            Map<Object, Object> map = new HashMap<>();
            if (StringUtils.isNotBlank(stoChainHead)) {
                //连锁总部的code
                map.put("CHAINCODE", stoChainHead);
            }
            if (StringUtils.isNotBlank(stoCode)) {
                //门店编码
                map.put("STOCODE", stoCode);
            }
            if (StringUtils.isNotBlank(stoName)) {
                //门店名称
                map.put("STOCNAME", stoName);
            }
            if (null != stoCodeList && !stoCodeList.isEmpty()) {
                //门店名称
                map.put("STOCODELIST", stoCodeList);
            }
            map.put("CLIENT", user.getClient());
            map.put("USERID", user.getUserId());
            // 分頁查詢
            List<GaiaStoreDataDto> list = gaiaStoreDataMapper.selectstoreDataList(map);

            List<List<String>> dataList = new ArrayList<>();
            // 配送平均天数  默认为 3天
            for (GaiaStoreDataDto dto : list) {
                // 打印行
                List<String> item = new ArrayList<>();
                // 门店编码
                item.add(dto.getStoCode());
                // 门店名称
                item.add(dto.getStoName());
                // 门店简称
                item.add(dto.getStoShortName());
                // 开业日期
                item.add(dto.getStoOpenDate());
                // 经营面积
                item.add(dto.getStoArea());
                // 医保店
                item.add(StringUtils.isNotBlank(dto.getStoIfMedicalcare()) ? "1".equals(dto.getStoIfMedicalcare()) ? "是" : "否" : "");
                // 详细地址
                item.add(dto.getStoAdd());
                // 门店属性
                item.add(StringUtils.isBlank(dto.getStoAttribute()) ? "" : "2".equals(dto.getStoAttribute()) ? "连锁" : "1".equals(dto.getStoAttribute()) ? "单体" : "3".equals(dto.getStoAttribute()) ? "加盟" : "门诊");
                // 配送方式
                item.add(StringUtils.isBlank(dto.getStoDeliveryMode()) ? "" : "1".equals(dto.getStoDeliveryMode()) ? "自采" : "2".equals(dto.getStoDeliveryMode()) ? "总部配送" : "委托配送");
                // 门店状态
                item.add(StringUtils.isNotBlank(dto.getStoStatus()) ? "0".equals(dto.getStoStatus()) ? "营业" : "闭店" : "");
                // 连锁总部
                item.add(dto.getStoChainHeadName());
                // 配送中心
                item.add(dto.getStoDcName());
                // 统一信用码
                item.add(dto.getStoNo());
                // 法人
                item.add(dto.getStoLegalPerson());
                // 质量负责人
                item.add(dto.getStoQuaName());
                // 店长
                item.add(dto.getStoLeaderName());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("门店清单导出");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }

    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "门店编码",
            "门店名称",
            "门店简称",
            "开业日期",
            "经营面积",
            "医保店",
            "详细地址",
            "门店属性",
            "配送方式",
            "门店状态",
            "连锁总部",
            "配送中心",
            "统一信用码",
            "法人",
            "质量负责人",
            "店长"
    };


}
