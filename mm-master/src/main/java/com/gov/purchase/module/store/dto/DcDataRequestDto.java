package com.gov.purchase.module.store.dto;

import com.gov.purchase.module.store.constant.Constant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@ApiModel(value = "DC保存传入参数")
public class DcDataRequestDto {

    @ApiModelProperty(value = "接口类型(0-新增,1-更新)", name = "type")
    @NotBlank(message = "接口类型(0-新增,1-更新)")
    private String type;

    @ApiModelProperty(value = "加盟商", name = "client")
    @NotBlank(message = "加盟商不能为空")
    @Size(max = 8, message = "加盟商长度不能大于8位")
    private String client;

    @ApiModelProperty(value = "DC编码", name = "dcCode")
    @NotBlank(message = "DC编码不能为空")
    @Size(max = 4, message = "DC编号长度不能大于4位")
    private String dcCode;

    @ApiModelProperty(value = "DC名称", name = "dcName")
    @NotBlank(message = "DC名称不能为空")
    @Size(max = 50, message = "DC名称长度不能大于20位")
    private String dcName;

    @ApiModelProperty(value = "DC状态", name = "dcStatus")
    @NotBlank(message = "DC状态不能为空")
    private String dcStatus;

    @ApiModelProperty(value = "助记码", name = "dcPym")
    @Size(max = 50, message = "助记码长度不能大于50位")
    private String dcPym;

    @ApiModelProperty(value = "DC简称", name = "dcShortName")
    @Size(max = 10, message = "DC简称长度不能大于10位")
    private String dcShortName;

    @ApiModelProperty(value = "DC地址", name = "dcPym")
    @Size(max = 50, message = "DC地址长度不能大于50位")
    private String dcAdd;

    @ApiModelProperty(value = "DC电话", name = "dcTel")
    private String dcTel;

    @ApiModelProperty(value = "虚拟仓标记", name = "dcInvent")
    private String dcInvent;

//    @ApiModelProperty(value = "是否有批发资质", name = "dcWholesale")
//    private String dcWholesale;

    @ApiModelProperty(value = "连锁总部", name = "dcChainHead")
    private String dcChainHead;

    @ApiModelProperty(value = "配送平均天数", name = "dcDeliveryDays")
    private String dcDeliveryDays;

    @ApiModelProperty(value = "纳税主体", name = "dcTaxSubject")
    private String dcTaxSubject;

    @ApiModelProperty(value = "统一社会信用代码", name = "dcNo")
    private String dcNo;

    @ApiModelProperty(value = "法人/负责人", name = "dcLegalPerson")
    private String dcLegalPerson;

    @ApiModelProperty(value = "质量负责人", name = "dcQua")
    private String dcQua;

    @ApiModelProperty(value = "商品配送范围列表", name = "dcSppsfwList")
    private List<String> dcSppsfwList;

    /**
     * 委托配送
     */
    @ApiModelProperty(value = "委托配送", name = "dcWtdc")
    private String dcWtdc;
}
