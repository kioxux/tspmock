package com.gov.purchase.module.store.service.impl;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.entity.GaiaPriceAdjust;
import com.gov.purchase.entity.GaiaSdMessage;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.GaiaSdProductPriceDto;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.qa.dto.QueryProductBusinessResponseDto;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;
import com.gov.purchase.module.store.dto.store.StoreInfoDetailReponseDto;
import com.gov.purchase.module.store.dto.store.StoreInfoDetailRequestDto;
import com.gov.purchase.module.store.service.StoreProductService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StoreProductServiceImpl implements StoreProductService {
    @Resource
    GaiaProductBusinessMapper productBusinessMapper;

    @Resource
    FeignService feignService;

    @Resource
    GaiaPriceAdjustMapper priceAdjustMapper;

    @Resource
    GaiaStoreDataMapper storeDataMapper;

    @Resource
    GaiaSdProductPriceMapper productPriceMapper;

    @Resource
    GaiaSdMessageMapper gaiaSdMessageMapper;


    @Override
    public List<QueryProductBusinessResponseDto> queryList(String stoCode, String param) {
        TokenUser user = feignService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();

        if (StringUtils.isNotBlank(stoCode)) {
            //门店编码
            map.put("STOCODE", stoCode);
        }
        if (StringUtils.isNotBlank(param)) {
            //参数
            map.put("PARAM", param);
        }

        map.put("CLIENT", user.getClient());

        List<QueryProductBusinessResponseDto> productBusinessResponseDtos = productBusinessMapper.getStoreProList(map);

        return productBusinessResponseDtos;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveList(List<GaiaPriceAdjustDto> gaiaPriceAdjustList) {
        if (null == gaiaPriceAdjustList || gaiaPriceAdjustList.isEmpty()) {
            throw new CustomResultException("至少一条参数");
        }
        TokenUser user = feignService.getLoginInfo();
        //根据商品编码来去重
        List<GaiaPriceAdjustDto> newList = gaiaPriceAdjustList.stream().collect(
                Collectors.collectingAndThen(Collectors.toCollection(
                        () -> new TreeSet<>(Comparator.comparing(o -> o.getPraProduct()))), ArrayList::new));
        if (newList.size() != gaiaPriceAdjustList.size()) {
            throw new CustomResultException("有重复的商品被编辑了");
        }
        //判断门店是单体还是连锁
        StoreInfoDetailRequestDto dto = new StoreInfoDetailRequestDto();
        dto.setClient(user.getClient());
        dto.setStoCode(gaiaPriceAdjustList.get(0).getPraStore());
        StoreInfoDetailReponseDto storeInfoDetailReponseDto = storeDataMapper.getStoreInfo(dto);
        Map<Object, Object> map = new HashMap<>();
        map.put("client", user.getClient());
        GaiaPriceAdjust priceAdjust = priceAdjustMapper.getCardNumber(map);
        String tjNo = priceAdjust.getPraAdjustNo();

        List<GaiaPriceAdjust> addList = new ArrayList<>();
        //记录调价明细表
        gaiaPriceAdjustList.forEach(item -> {
            item.setClient(user.getClient());
            item.setPraAdjustNo(tjNo);
            item.setPraCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            item.setPraCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            if ("2".equals(storeInfoDetailReponseDto.getStoAttribute())) {
                item.setPraApprovalStatus("1");
            }
            item.setPraSendStatus("1");
            GaiaPriceAdjust adjust = new GaiaPriceAdjust();
            BeanUtils.copyProperties(item, adjust);
            addList.add(adjust);
        });

        priceAdjustMapper.insertBatch(addList);

        //单体和连锁的逻辑
        if (CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(storeInfoDetailReponseDto.getStoAttribute())) {
            //连锁
            //审批工作流
            // 初始编码
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            String FlowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
            FeignResult flowResult = feignService.createProductAdjustWorkflow(FlowNo, addList);
            if (flowResult.getCode() != 0) {
                throw new CustomResultException(flowResult.getMessage());
            }

            List<GaiaPriceAdjust> updateList = new ArrayList<>();

//            更新审批流程单号
            gaiaPriceAdjustList.forEach(item -> {
                GaiaPriceAdjust adjust = new GaiaPriceAdjust();
                adjust.setClient(item.getClient());
                adjust.setPraAdjustNo(item.getPraAdjustNo());
                adjust.setPraProduct(item.getPraProduct());
                adjust.setPraStore(item.getPraStore());
                adjust.setPraFlowNo(FlowNo);
                updateList.add(adjust);
            });
            priceAdjustMapper.updateBatch(updateList);
        } else {
            List<GaiaPriceAdjust> updateList = new ArrayList<>();
            List<GaiaSdProductPriceDto> productPrices = new ArrayList<>();
            //单体
            Integer today = Integer.valueOf(DateUtils.getCurrentDateStrYYMMDD());
            gaiaPriceAdjustList.forEach(item -> {
                //有效期起小于等于当前日期
                Integer mondy = Integer.valueOf(item.getPraStartDate());
                if (mondy <= today) {
                    GaiaSdProductPriceDto productPrice = new GaiaSdProductPriceDto();
                    productPrice.setClient(user.getClient());
                    productPrice.setGsppBrId(item.getPraStore());
                    productPrice.setGsppProId(item.getPraProduct());


                    //零售价
                    if (null != item.getPraPriceNormal()) {
                        //说明调过零售价了
                        productPrice.setGsppPriceNormal(item.getPraPriceNormal());
                    } else {

                        //调后没有价格 则 等于调前价格
                        if (null != item.getPraPriceNormalBefore()) {
                            //说明没调价格
                            productPrice.setGsppPriceNormal(item.getPraPriceNormalBefore());
                        } else {
                            //说明没有零售价 但是调最贵的价格

                            //选取其中最贵的一个价格
                            BigDecimal praPriceHy = null != item.getPraPriceHy() ? item.getPraPriceHy() : BigDecimal.ZERO;
                            BigDecimal gsppPriceYb = null != item.getPraPriceYb() ? item.getPraPriceYb() : BigDecimal.ZERO;
                            BigDecimal gsppPriceCl = null != item.getPraPriceCl() ? item.getPraPriceCl() : BigDecimal.ZERO;
                            BigDecimal praPriceOln = null != item.getPraPriceOln() ? item.getPraPriceOln() : BigDecimal.ZERO;
                            BigDecimal praPriceOlhy = null != item.getPraPriceOlhy() ? item.getPraPriceOlhy() : BigDecimal.ZERO;
                            BigDecimal max = BigDecimal.ZERO;
                            if (praPriceHy.compareTo(gsppPriceYb) > -1) {
                                max = praPriceHy;
                            } else {
                                max = gsppPriceYb;
                            }
                            if (gsppPriceCl.compareTo(max) > -1) {
                                max = gsppPriceCl;
                            }

                            if (praPriceOln.compareTo(max) > -1) {
                                max = praPriceOln;
                            }

                            if (praPriceOlhy.compareTo(max) > -1) {
                                max = praPriceOlhy;
                            }
                            productPrice.setGsppPriceNormal(max);
                        }

                    }

                    //调后没有价格 则 等于调前价格
                    productPrice.setGsppPriceHy(null != item.getPraPriceHy() ? item.getPraPriceHy() : item.getPraPriceYhBefore());
                    productPrice.setGsppPriceYb(null != item.getPraPriceYb() ? item.getPraPriceYb() : item.getPraPriceYbBefore());
                    productPrice.setGsppPriceCl(null != item.getPraPriceCl() ? item.getPraPriceCl() : item.getPraPriceClBefore());
                    productPrice.setGsppPriceOnlineNormal(null != item.getPraPriceOln() ? item.getPraPriceOln() : item.getPraPriceOlnBefore());
                    productPrice.setGsppPriceOnlineHy(null != item.getPraPriceOlhy() ? item.getPraPriceOlhy() : item.getPraPriceOlhyBefore());
                    productPrice.setGsppPriceHyr(item.getGsppPriceHyr());

                    productPrices.add(productPrice);

                    GaiaPriceAdjust adjust = new GaiaPriceAdjust();
                    adjust.setClient(item.getClient());
                    adjust.setPraAdjustNo(item.getPraAdjustNo());
                    adjust.setPraProduct(item.getPraProduct());
                    adjust.setPraStore(item.getPraStore());
                    adjust.setPraSendStatus("2");
                    updateList.add(adjust);
                }
            });

            if (!productPrices.isEmpty()) {

                //全删全插
                productPriceMapper.deleteList(productPrices);


                productPriceMapper.insertList(productPrices);


                // 插入消息推送表
                GaiaSdMessage sdMessage = new GaiaSdMessage();
                sdMessage.setClient(user.getClient());
                sdMessage.setGsmId(gaiaPriceAdjustList.get(0).getPraStore());  // 调价门店
                sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear());  // 用来查询前缀
                String currentVoucherId = gaiaSdMessageMapper.getCurrentVoucherId(sdMessage);
                sdMessage.setGsmVoucherId(currentVoucherId);
                sdMessage.setGsmValue("praAdjustNo=" + currentVoucherId + "&praStore=" + sdMessage.getGsmId());
                String proSelfCodeJoin = gaiaPriceAdjustList.stream().filter(Objects::nonNull).map(GaiaPriceAdjustDto::getPraProduct).collect(Collectors.joining(","));
                sdMessage.setGsmRemark(MessageFormat.format("您有新的商品调价信息,商品编码为[{0}],请及时查看", proSelfCodeJoin));
                sdMessage.setGsmPlatform("FX");
                sdMessage.setGsmFlag("N"); // 是否查看    默认为N-未查看
                sdMessage.setGsmBusinessVoucherId(tjNo);  // 调价单号
                sdMessage.setGsmArriveDate(DateUtils.getCurrentDateStrYYMMDD());
                sdMessage.setGsmArriveTime(DateUtils.getCurrentTimeStrHHMMSS());
                // TODO 缺少消息类型 消息内容 跳转页面
                sdMessage.setGsmPage("pricingList");
                gaiaSdMessageMapper.insertSelective(sdMessage);

                //更新商品調價專題状态
                priceAdjustMapper.updateBatch(updateList);
            }

        }
        return ResultUtil.success();
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updateList() {
        Map<Object, Object> map = new HashMap<>();
        map.put("approvalStatus", 1);
        map.put("sendStatus", 1);
        map.put("today", 1);
        List<GaiaPriceAdjustDto> gaiaPriceAdjustList = priceAdjustMapper.selectByMap(map);
        //根据商品编码 门店 和 加盟商来来去重
        List<GaiaPriceAdjustDto> newList = gaiaPriceAdjustList.stream().collect(
                Collectors.collectingAndThen(Collectors.toCollection(
                        () -> new TreeSet<>(Comparator.comparing(o -> o.getPraProduct() + ";" + o.getClient() + ";" + o.getPraStore()))), ArrayList::new));

        List<GaiaPriceAdjust> updateList = new ArrayList<>();

        List<GaiaSdProductPriceDto> productPrices = new ArrayList<>();
        if (!newList.isEmpty()) {

            gaiaPriceAdjustList.forEach(item -> {
                GaiaPriceAdjust adjust = new GaiaPriceAdjust();
                adjust.setClient(item.getClient());
                adjust.setPraAdjustNo(item.getPraAdjustNo());
                adjust.setPraProduct(item.getPraProduct());
                adjust.setPraStore(item.getPraStore());
                adjust.setPraSendStatus("2");
                updateList.add(adjust);
            });

            newList.forEach(item -> {
                GaiaSdProductPriceDto productPrice = new GaiaSdProductPriceDto();
                productPrice.setPraAdjustNo(item.getPraAdjustNo()); // 暂存调价单号
                productPrice.setGsppBrId(item.getPraStore());
                productPrice.setClient(item.getClient());
                productPrice.setGsppProId(item.getPraProduct());
                productPrice.setGsppPriceHyr(item.getGsppPriceHyr());
                //零售价
                if (null != item.getPraPriceNormal()) {
                    //说明调过零售价了
                    productPrice.setGsppPriceNormal(item.getPraPriceNormal());
                } else {

                    //调后没有价格 则 等于调前价格
                    if (null != item.getPraPriceNormalBefore()) {
                        //说明没调价格
                        productPrice.setGsppPriceNormal(item.getPraPriceNormalBefore());
                    } else {
                        //说明没有零售价 但是调最贵的价格

                        //选取其中最贵的一个价格
                        BigDecimal praPriceHy = null != item.getPraPriceHy() ? item.getPraPriceHy() : BigDecimal.ZERO;
                        BigDecimal gsppPriceYb = null != item.getPraPriceYb() ? item.getPraPriceYb() : BigDecimal.ZERO;
                        BigDecimal gsppPriceCl = null != item.getPraPriceCl() ? item.getPraPriceCl() : BigDecimal.ZERO;
                        BigDecimal praPriceOln = null != item.getPraPriceOln() ? item.getPraPriceOln() : BigDecimal.ZERO;
                        BigDecimal praPriceOlhy = null != item.getPraPriceOlhy() ? item.getPraPriceOlhy() : BigDecimal.ZERO;
                        BigDecimal max = BigDecimal.ZERO;
                        if (praPriceHy.compareTo(gsppPriceYb) > -1) {
                            max = praPriceHy;
                        } else {
                            max = gsppPriceYb;
                        }
                        if (gsppPriceCl.compareTo(max) > -1) {
                            max = gsppPriceCl;
                        }

                        if (praPriceOln.compareTo(max) > -1) {
                            max = praPriceOln;
                        }

                        if (praPriceOlhy.compareTo(max) > -1) {
                            max = praPriceOlhy;
                        }
                        productPrice.setGsppPriceNormal(max);
                    }

                }

                //调后没有价格 则 等于调前价格
                productPrice.setGsppPriceHy(null != item.getPraPriceHy() ? item.getPraPriceHy() : item.getPraPriceYhBefore());
                productPrice.setGsppPriceYb(null != item.getPraPriceYb() ? item.getPraPriceYb() : item.getPraPriceYbBefore());
                productPrice.setGsppPriceCl(null != item.getPraPriceCl() ? item.getPraPriceCl() : item.getPraPriceClBefore());
                productPrice.setGsppPriceOnlineNormal(null != item.getPraPriceOln() ? item.getPraPriceOln() : item.getPraPriceOlnBefore());
                productPrice.setGsppPriceOnlineHy(null != item.getPraPriceOlhy() ? item.getPraPriceOlhy() : item.getPraPriceOlhyBefore());
                productPrices.add(productPrice);
            });

            if (!productPrices.isEmpty()) {
                //全删全插
                productPriceMapper.deleteList(productPrices);

                productPriceMapper.insertList(productPrices);
                // 过滤按照门店过滤
                Map<String, List<GaiaSdProductPriceDto>> mapList = productPrices.stream()
                        .collect(Collectors.groupingBy(GaiaSdProductPriceDto::getGsppBrId));
                for (Map.Entry<String, List<GaiaSdProductPriceDto>> entry : mapList.entrySet()) {
                    // 过滤按照调价单过滤
                    Map<String, List<GaiaSdProductPriceDto>> resultList = entry.getValue().stream()
                            .collect(Collectors.groupingBy(GaiaSdProductPriceDto::getPraAdjustNo));

                    for (Map.Entry<String, List<GaiaSdProductPriceDto>> resultEntry : resultList.entrySet()) {
                        // 插入消息推送表
                        GaiaSdMessage sdMessage = new GaiaSdMessage();
                        sdMessage.setClient(resultEntry.getValue().get(0).getClient());
                        sdMessage.setGsmId(entry.getKey());  // 调价门店
                        sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear());  // 用来查询前缀
                        String currentVoucherId = gaiaSdMessageMapper.getCurrentVoucherId(sdMessage);
                        sdMessage.setGsmVoucherId(currentVoucherId);
                        sdMessage.setGsmValue("praAdjustNo=" + currentVoucherId + "&praStore=" + sdMessage.getGsmId());
                        String proSelfCodeJoin = resultEntry.getValue().stream().filter(Objects::nonNull).map(GaiaSdProductPriceDto::getGsppProId).collect(Collectors.joining(","));
                        sdMessage.setGsmRemark(MessageFormat.format("您有新的商品调价信息,商品编码为[{0}],请及时查看", proSelfCodeJoin));
                        sdMessage.setGsmPlatform("FX");
                        sdMessage.setGsmFlag("N"); // 是否查看    默认为N-未查看
                        sdMessage.setGsmBusinessVoucherId(resultEntry.getKey());  // 调价单号
                        sdMessage.setGsmArriveDate(DateUtils.getCurrentDateStrYYMMDD());
                        sdMessage.setGsmArriveTime(DateUtils.getCurrentTimeStrHHMMSS());
                        sdMessage.setGsmPage("pricingList");
                        gaiaSdMessageMapper.insertSelective(sdMessage);
                    }
                }
                //更新商品調價專題状态
                priceAdjustMapper.updateBatch(updateList);
            }
        }
        return ResultUtil.success();
    }

}
