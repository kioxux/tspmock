package com.gov.purchase.module.delivery.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.delivery.dto.GoodsRecallListRequestDto;
import com.gov.purchase.module.delivery.dto.GoodsRecallListResponseDto;
import com.gov.purchase.module.delivery.dto.SaveGoodsRecallRequestDto;
import com.gov.purchase.module.delivery.dto.StockNumRequestDto;

import java.math.BigDecimal;
import java.util.List;

public interface GoodsRecallService {

    /**
     * 门店库存获取
     *
     * @param dto StockNumRequestDto
     * @return BigDecimal
     */
    BigDecimal selectStockNum(StockNumRequestDto dto);

    /**
     * 商品召回提交
     *
     * @param dto List<SaveGoodsRecallRequestDto>
     * @return
     */
    void insertSaveGoodsRecall(List<SaveGoodsRecallRequestDto> dto);

    /**
     * 商品召回查看列表
     *
     * @param dto GoodsRecallListRequestDto
     * @return PageInfo<GoodsRecallListResponseDto>
     */
    PageInfo<GoodsRecallListResponseDto> selectGoodsRecallList(GoodsRecallListRequestDto dto);

    /**
     * 连锁总部下所有门店、DC的商品
     * @param client 加盟商
     * @param chain 连锁总部
     * @return
     */
    Result getProductListForRecall(String client, String chain);
}


