package com.gov.purchase.module.goods.service.impl;

import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.goods.dto.DrugQualityDTO;
import com.gov.purchase.module.goods.dto.DrugQualityVO;
import com.gov.purchase.module.goods.service.DrugQualityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @description: 药品质量档案表
 * @author: yzf
 * @create: 2021-11-08 17:05
 */
@Service
public class DrugQualityServiceImpl implements DrugQualityService {

    @Resource
    private FeignService feignService;

    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;

    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    /**
     * 查询药品质量档案
     *
     * @param drugQualityDTO
     * @return
     */
    @Override
    public DrugQualityVO queryDrugQuality(DrugQualityDTO drugQualityDTO) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        drugQualityDTO.setClient(user.getClient());
        // 判断机构编码是仓库还是门店
        int dcCount = gaiaDcDataMapper.queryDc(user.getClient(), drugQualityDTO.getCode());
        drugQualityDTO.setCount(dcCount);
        // 查询进货列表
        DrugQualityVO drugQualityVO = gaiaProductBusinessMapper.queryProInfo(drugQualityDTO);
        drugQualityVO.setList(gaiaProductBusinessMapper.queryDrugQuality(drugQualityDTO));
        drugQualityVO.setUserName(user.getLoginName());
        return drugQualityVO;
    }


}
