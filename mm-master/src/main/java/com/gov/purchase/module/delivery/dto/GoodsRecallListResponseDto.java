package com.gov.purchase.module.delivery.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "商品召回查看列表返回参数")
public class GoodsRecallListResponseDto {

    private String client;

    private String recChainHead;

    private String recNumber;

    private String recLineno;

    private String recStore;

    private String storeName;

    private String recProCode;

    private String proName;

    private String proSpecs;

    private String proFactoryName;

    private String proPlace;

    private String proUnit;

    private String recBatchNo;

    private String recSupplier;

    private String supName;

    private BigDecimal recQty;

    private String recRemark;

    private String batExpiryDate;

    private BigDecimal grddStartQty;

    private BigDecimal grddReviseQty;
}
