package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.businessImport.StoreReplenishment;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.replenishment.dto.StoreReplenishmentListResponseDto;
import com.gov.purchase.module.replenishment.dto.SupplierRequestDto;
import com.gov.purchase.module.replenishment.dto.SupplierResponseDto;
import com.gov.purchase.module.replenishment.service.impl.DcReplenishmentServiceImpl;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class StoreReplenishmentImport extends BusinessImport {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;
    @Resource
    private DcReplenishmentServiceImpl dcReplenishmentService;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        TokenUser user = feignService.getLoginInfo();
        // 加盟商
        String client = user.getClient();
        List<String> errorList = new ArrayList<>();
        // 批量导入
        try {
            // 返回值
            List<StoreReplenishmentListResponseDto> list = new ArrayList<>();
            // 验证excel  数据是否正确
            for (Integer key : map.keySet()) {
                // 行数据
                StoreReplenishment storeReplenishment = (StoreReplenishment) map.get(key);
                // 地点
                String stoCode = storeReplenishment.getStoCode();
                // 商品
                String proCode = storeReplenishment.getProCode();
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                gaiaProductBusinessKey.setClient(client);
                gaiaProductBusinessKey.setProSite(stoCode);
                gaiaProductBusinessKey.setProSelfCode(proCode);
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                if (gaiaProductBusiness == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "门店编码和商品编码"));
                    continue;
                }
                // 补货单号
                String gsrhVoucherId = storeReplenishment.getGsrhVoucherId();
                // 行号
                String gsrdSerial = storeReplenishment.getGsrdSerial();
                // 校验补货单号+补货行号+地点至补货表中取得的商品编码与导入的商品编码是否一致
                GaiaSdReplenishD gaiaSdReplenishD = new GaiaSdReplenishD();
                //加盟商
                gaiaSdReplenishD.setClient(client);
                //补货门店
                gaiaSdReplenishD.setGsrdBrId(stoCode);
                //补货单号
                gaiaSdReplenishD.setGsrdVoucherId(gsrhVoucherId);
                //行号
                gaiaSdReplenishD.setGsrdSerial(gsrdSerial);
                // 商品编码
                gaiaSdReplenishD.setGsrdProId(proCode);
                List<GaiaSdReplenishD> gaiaSdReplenishDList = gaiaSdReplenishDMapper.selectByVoucherIdForStore(gaiaSdReplenishD);
                // 不一致则提示“导入数据中补货单号+行号对应的商品与门店申请的不一致，请检查后重新导入”
                if (CollectionUtils.isEmpty(gaiaSdReplenishDList)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "导入数据中补货单号+行号对应的商品与门店申请的不一致，请检查后重新导入"));
                    continue;
                }
                // 供应商
                String supplierId = storeReplenishment.getSupplierId();
                // 当前地点下的所有供应商
                SupplierRequestDto supplierRequestDto = new SupplierRequestDto();
                supplierRequestDto.setClient(client);
                supplierRequestDto.setSiteCode(stoCode);
                List<SupplierResponseDto> supplierResponseDtoList = dcReplenishmentService.querySupplierList(supplierRequestDto);
                if (CollectionUtils.isEmpty(supplierResponseDtoList)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "供应商"));
                    continue;
                }
                boolean flg = false;
                // 判断导入的供应商是否正确
                for (SupplierResponseDto supplierResponseDto : supplierResponseDtoList) {
                    if (supplierId.equals(supplierResponseDto.getSupSelfCode())) {
                        flg = true;
                        break;
                    }
                }
                // 供应商未在当前门店业务表
                if (!flg) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "供应商"));
                    continue;
                }
                StoreReplenishmentListResponseDto entity = new StoreReplenishmentListResponseDto();
                // 加盟商
                entity.setClient(client);
                //门店编码
                entity.setGsrhBrId(stoCode);
                //门店名称
                GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                gaiaStoreDataKey.setClient(client);
                gaiaStoreDataKey.setStoCode(stoCode);
                GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                if (gaiaStoreData == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "门店编码"));
                    continue;
                }
                entity.setStoName(gaiaStoreData.getStoName());
                //商品编码
                entity.setGsrdProId(proCode);
                //商品名称
                entity.setProName(storeReplenishment.getProName());
                //规格
                entity.setProSpecs(storeReplenishment.getProSpecs());
                //生产厂家
                entity.setProFactoryName(storeReplenishment.getProFactoryName());
                // 单位
                entity.setProUnit(gaiaProductBusiness.getProUnit());
                //单位
                entity.setUnitName(storeReplenishment.getUnitName());
                //中包装量
                entity.setProMidPackage(StringUtils.isBlank(storeReplenishment.getProMidPackage()) ? "1" : storeReplenishment.getProMidPackage());
                //补货单号
                entity.setGsrhVoucherId(gsrhVoucherId);
                //补货行号
                entity.setGsrdSerial(gsrdSerial);
                //门店库存
                entity.setStoreStockTotal(StringUtils.isBlank(storeReplenishment.getStoreStockTotal()) ? null : new BigDecimal(storeReplenishment.getStoreStockTotal()));
                //最后一次进货日期
                entity.setLastPurchaseDate(storeReplenishment.getLastPurchaseDate());
                //最后进货供应商编码
                entity.setLastSupplierId(storeReplenishment.getLastSupplierId());
                //最后进货供应商
                entity.setLastSupplier(storeReplenishment.getLastSupplier());
                //末次进货价
                entity.setLastPurchasePrice(StringUtils.isBlank(storeReplenishment.getLastPurchasePrice()) ? null : new BigDecimal(storeReplenishment.getLastPurchasePrice()));
                //30天门店销售量
                entity.setDaysSalesCount(StringUtils.isBlank(storeReplenishment.getDaysSalesCount()) ? null : new BigDecimal(storeReplenishment.getDaysSalesCount()));
                //在途量
                entity.setTrafficCount(StringUtils.isBlank(storeReplenishment.getTrafficCount()) ? null : new BigDecimal(storeReplenishment.getTrafficCount()));
                //待出量
                entity.setWaitCount(StringUtils.isBlank(storeReplenishment.getWaitCount()) ? null : new BigDecimal(storeReplenishment.getWaitCount()));
                //建议补货数量
                entity.setReplenishmentValue(StringUtils.isBlank(storeReplenishment.getReplenishmentValue()) ? null : new BigDecimal(storeReplenishment.getReplenishmentValue()));
                //供应商编码
                entity.setSupplierId(storeReplenishment.getSupplierId());
                //供应商名称
                //采购价格
                entity.setPurchasePrice(StringUtils.isBlank(storeReplenishment.getPurchasePrice()) ? null : new BigDecimal(storeReplenishment.getPurchasePrice()));
                //税率
                entity.setProInputTax(gaiaProductBusiness.getProInputTax());
                //预计到货日期
                entity.setPoDeliveryDate(storeReplenishment.getPoDeliveryDate());

                // 供应商下拉框
                SupplierRequestDto supplierRequestDto1 = new SupplierRequestDto();
                supplierRequestDto1.setClient(client);
                supplierRequestDto1.setSiteCode(stoCode);
                supplierRequestDto1.setProSelfCode(proCode);
                List<SupplierResponseDto> supplierResponseDtoList1 = gaiaSupplierBusinessMapper.querySupplierList(supplierRequestDto1);
                // 供应商下拉框
                entity.setSupplierResponseDtoList(supplierResponseDtoList1);
                list.add(entity);
            }
            // 验证报错
            if (CollectionUtils.isNotEmpty(errorList)) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            return ResultUtil.success(list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException(ResultEnum.E0126);
        }
    }
}
