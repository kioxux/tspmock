package com.gov.purchase.module.base.dto.businessImport;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class TaxCodeValueDTO {

    /**
     * 门店编码
     */
    private String stoCode;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 税率值
     */
    private String taxCodeValue;

}
