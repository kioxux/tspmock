package com.gov.purchase.module.replenishment.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.purchase.dto.SearchReplenish;
import com.gov.purchase.module.purchase.dto.SearchReplenishReq;
import com.gov.purchase.module.replenishment.dto.*;
import com.gov.purchase.module.replenishment.service.StoreNeedService;
import com.gov.purchase.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "门店铺货")
@RestController
@RequestMapping("storeNeed")
public class StoreNeedController {

    @Resource
    private StoreNeedService storeNeedService;

    @Log("门店获取")
    @ApiOperation("门店获取")
    @PostMapping("queryStoreList")
    public Result queryStoreList(@Valid @RequestBody StoreRequestDto dto) {
        return ResultUtil.success(storeNeedService.queryStoreList(dto));
    }


    @Log("门店铺货列表")
    @ApiOperation("门店铺货列表")
    @PostMapping("queryBatchStockList")
    public Result queryBatchStockList(@Valid @RequestBody BatchStockRequestDto dto) {
        return ResultUtil.success(storeNeedService.queryBatchStockList(dto));
    }

    @Log("门店铺货提交")
    @ApiOperation("门店铺货提交")
    @PostMapping("saveStoreNeedList")
    public Result saveStoreNeedList(@Valid @RequestBody List<StoreNeedRequestDto> list) {
        return storeNeedService.saveStoreNeedList(list);
    }

    @Log("铺货查询")
    @ApiOperation("铺货查询")
    @PostMapping("searchReplenishList")
    public Result searchReplenishList(@Valid @RequestBody SearchReplenishReq searchReplenishReq) {
        return storeNeedService.searchReplenishList(searchReplenishReq);
    }

    @Log("铺货保存")
    @ApiOperation("铺货保存")
    @PostMapping("saveReplenish")
    public Result saveReplenish(@RequestBody List<SearchReplenish> list) {
        return storeNeedService.saveReplenish(list);
    }

    @Log("有权限的DC列表")
    @ApiOperation("有权限的DC列表")
    @PostMapping("getAuthDcList")
    public Result getAuthDcList() {
        return ResultUtil.success(storeNeedService.getAuthDcList(null));
    }

    @Log("门店列表")
    @ApiOperation("门店列表")
    @PostMapping("getStoreListByAuthDc")
    public Result getStoreListByAuthDc(@RequestJson(value = "dcCode", name = "有权限的DC编码", required = false) String dcCode,
                                       @RequestJson(value = "gssgId", name = "店型编码") String gssgId,
                                       @RequestJson(value = "stoDistrict", name = "区域编码", required = false) String stoDistrict) {
        return ResultUtil.success(storeNeedService.getStoreListByAuthDc(dcCode, null, gssgId, stoDistrict));
    }

    @Log("商品选择弹出框")
    @ApiOperation("商品选择弹出框")
    @PostMapping("getProList")
    public Result getProList(@RequestBody @Valid GetProListRequestDTO dto) {
        return ResultUtil.success(storeNeedService.getProList(dto, null));
    }

    @Log("门店铺货商品提交")
    @ApiOperation("门店铺货商品提交")
    @PostMapping("saveNeedProList")
    public Result saveNeedProList(@RequestBody @Valid List<SaveNeedProListRequestDTO> list) {
        AutoNeedProDTO autoNeedProDTO = storeNeedService.saveNeedProList(list, null);
        return autoNeedProDTO.getResult();
    }

    @Log("商品选择")
    @ApiOperation("商品选择")
    @PostMapping("selectProductList")
    public Result selectProductList(@RequestBody SelectProductListParams selectProductListParams) {
        return storeNeedService.selectProductList(selectProductListParams, null);
    }

    @ApiOperation(value = "自动选择商品铺货")
    @PostMapping("autoSelectProduct")
    public Result autoSelectProduct(@RequestBody AutoNeedForm autoNeedForm) {
        if (StringUtils.isEmpty(autoNeedForm.getProSelfCodes())) {
            return ResultUtil.error("-1", "参数不能为空");
        }
        return storeNeedService.autoSelectProductList(autoNeedForm.getProSelfCodes(), null);
    }

    @Log("店型下拉框&区域下拉框")
    @ApiOperation("店型下拉框&区域下拉框")
    @PostMapping("getStoreTypeIdItemListAndAreaList")
    public Result getStoreTypeIdItemListAndAreaList() {
        return ResultUtil.success(storeNeedService.getStoreTypeIdItemListAndAreaList());
    }

    @Log("库存对比铺货")
    @ApiOperation("库存对比铺货")
    @PostMapping("selectNeedProListByWms")
    public Result selectNeedProListByWms(@RequestBody NeedProWmsParams needProWmsParams) {
        return storeNeedService.selectNeedProListByWms(needProWmsParams);
    }

    /**
     * 库存对比铺货导出
     * @param needProWmsParams
     * @return
     */
    @Log("库存对比铺货导出")
    @PostMapping("selectNeedProListByWms/export")
    public void needProListByWmsExport(HttpServletResponse response, @RequestBody NeedProWmsParams needProWmsParams){
       this.storeNeedService.needProListByWmsExport(response,needProWmsParams);
    }

    @Log("管量区域")
    @ApiOperation("管量区域")
    @PostMapping("selectStoresGroup")
    public Result selectStoresGroup() {
        return storeNeedService.selectStoresGroup();
    }

    @Log("门店规模")
    @ApiOperation("门店规模")
    @PostMapping("selectStoresScale")
    public Result selectStoresScale() {
        return storeNeedService.selectStoresScale();
    }

    @Log("电子券门店列表")
    @ApiOperation("电子券门店列表")
    @PostMapping("electronicCouponStoreList")
    public Result getStoreList(
            @RequestJson(value = "gssgId", name = "店型编码") String gssgId,
            @RequestJson(value = "stoDistrict", name = "区域编码") String stoDistrict) {
        List<GetStoreListByAuthDcDTO> storeList = storeNeedService.getElectronicCouponStoreList(null, gssgId, stoDistrict);
        return ResultUtil.success(storeList);
    }

    @Log("批次货位")
    @ApiOperation("批次货位")
    @PostMapping("selectBatchAndHw")
    public Result selectBatchAndHw(@RequestBody StoreNeedHwAndBatchDTO storeNeedHwAndBatchDTO) {
        return storeNeedService.selectBatchAndHw(storeNeedHwAndBatchDTO);
    }

}
