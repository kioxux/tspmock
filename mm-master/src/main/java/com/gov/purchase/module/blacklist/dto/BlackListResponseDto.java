package com.gov.purchase.module.blacklist.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "批号黑名单信息详情返回参数")
public class BlackListResponseDto {

    @ApiModelProperty(value = "加盟商编号", name = "client", hidden = true)
    private String client;

    @ApiModelProperty(value = "地点", name = "proSite")
    String proSite;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    String proSelfCode;

    @ApiModelProperty(value = "生产批号", name = "proBatchNo")
    String proBatchNo;

    @ApiModelProperty(value = "创建人", name = "proCreateUser")
    String proCreateUser;

    @ApiModelProperty(value = "创建日期", name = "proCreateDate")
    String proCreateDate;

    @ApiModelProperty(value = "创建时间", name = "proCreateTime")
    String proCreateTime;

    @ApiModelProperty(value = "变更人", name = "proChangeUser")
    String proChangeUser;

    @ApiModelProperty(value = "变更日期", name = "proChangeDate")
    String proChangeDate;

    @ApiModelProperty(value = "变更时间", name = "proChangeTime")
    String proChangeTime;

    @ApiModelProperty(value = "地址", name = "addr")
    String addr;

    @ApiModelProperty(value = "商品名称", name = "proName")
    String proName;

    @ApiModelProperty(value = "规格", name = "proSpecs")
    String proSpecs;

    @ApiModelProperty(value = "厂商", name = "proFactoryName")
    String proFactoryName;

    @ApiModelProperty(value = "单位", name = "proUnit")
    String proUnit;

    @ApiModelProperty(value = "库存量", name = "qty")
    BigDecimal qty;


}
