package com.gov.purchase.module.supplier.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.diff.DiffEntityConfig;
import com.gov.purchase.common.diff.DiffEntityReuslt;
import com.gov.purchase.common.diff.DiffEntityUtils;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.ListUtils;
import com.gov.purchase.common.utils.UUIDUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.base.dto.businessScope.GetScopeListDTO;
import com.gov.purchase.module.base.dto.businessScope.JyfwDataDO;
import com.gov.purchase.module.base.service.BusinessScopeService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.supplier.dto.*;
import com.gov.purchase.module.supplier.service.OptimizeSupplierService;
import com.gov.purchase.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OptimizeSupplierServiceImpl implements OptimizeSupplierService {

    @Resource
    CosUtils cosUtils;
    @Resource
    FeignService feignService;
    @Resource
    GaiaSupplierBasicMapper gaiaSupplierBasicMapper;
    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    GaiaSupplierGspinfoLogMapper gaiaSupplierGspinfoLogMapper;
    @Resource
    GaiaSupplierGspinfoMapper gaiaSupplierGspinfoMapper;
    @Resource
    GaiaUserDataMapper gaiaUserDataMapper;
    @Resource
    OperationFeignService operationFeignService;
    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    BusinessScopeService businessScopeService;
    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    GaiaSupplierChangeMapper gaiaSupplierChangeMapper;
    @Resource
    DiffEntityConfig diffEntityConfig;
    @Resource
    DiffEntityUtils diffEntityUtils;
    @Resource
    GaiaSupplierSalesmanMapper gaiaSupplierSalesmanMapper;
    @Resource
    BusinessScopeServiceMapper businessScopeServiceMapper;

    /**
     * 供应商basic表_列表数据
     */
    @Override
    public List<GaiaSupplierBusiness> getSupBasicList(GetSupBasicListRequestDTO dto) {
        String client = feignService.getLoginInfo().getClient();

        List<GaiaSupplierBusiness> supBasicList = gaiaSupplierBasicMapper.getSupBasicList(dto);
        if (StringUtils.isNotEmpty(dto.getCurrentRowSupSeflCode())) {
            supBasicList.forEach(item -> item.setSupSelfCode(dto.getCurrentRowSupSeflCode()));
            return supBasicList;
        }

        if (StringUtils.isNotEmpty(dto.getLastRowSupSeflCode())) {
            String selfCode = ListUtils.getMaxCode(dto.getLastRowSupSeflCode());
            supBasicList.forEach(item -> item.setSupSelfCode(selfCode));
            return supBasicList;
        }

        // 自编码列表
        List<String> list = gaiaSupplierBusinessMapper.getSelfCodeList(client, null, null);
        supBasicList.forEach(item -> {
            if (!CollectionUtils.isEmpty(list)) {
                String maxCode = ListUtils.getMaxCode(list);
                String selfCode = ListUtils.getMaxCode(maxCode);
                item.setSupSelfCode(selfCode);
            }
        });
        return supBasicList;
    }

    /**
     * 供应商保存1_先与已有数据进行比对_返回比对结果集
     */
    @Override
    public List<SaveSupList1ResponseDTO> saveSupList1(List<GaiaSupplierBusinessInfo> list) {
        checkParam(list);
        TokenUser user = feignService.getLoginInfo();

        // 据地点+对应行的统一社会信用代码 精确匹配
        List<SaveSupList1ResponseDTO> compList = gaiaSupplierBusinessMapper.getCompList(user.getClient(), list);
        return compList;
    }

    /**
     * 保存2
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveSupList2(SaveSupList2RequestDTO dto) {
        if (StringUtils.isBlank(dto.getSupSite())) {
            throw new CustomResultException("请选择首营机构");
        }

        List<GaiaSupplierBusinessInfo> basicsList = dto.getBasicsList();
        checkParam(basicsList);

        String client = feignService.getLoginInfo().getClient();
        String userId = feignService.getLoginInfo().getUserId();
        // 非连锁门店集合
        List<String> stoList = gaiaProductBasicMapper.getStoList(client);
        // 配送中心集合
        List<String> dcList = gaiaProductBasicMapper.getDcList(client);
        // 首营流程编号
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String supFlowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
        List<GaiaSupplierGspinfo> gspinfoSubmitRequestDtoList = new ArrayList<>();
        for (GaiaSupplierBusinessInfo supInfo : basicsList) {

            supInfo.setClient(client);
            supInfo.setSupSite(dto.getSupSite());
            /**
             * 且若地点为非连锁门店则流程状态直接设置为已完成（即自动审批通过），后续插入business表的逻辑按照现有逻辑执行
             */
            if (!CollectionUtils.isEmpty(stoList) && stoList.contains(dto.getSupSite())) {
                saveBusiness(supInfo, client);
                // 首营表数据补充
                saveSupInfo(supInfo, client, "N" + supFlowNo, gspinfoSubmitRequestDtoList, false);
                // 特殊经营范围
                businessScopeService.saveJyfwData(client, userId, supInfo.getSupSelfCode(), supInfo.getSupName(), supInfo.getSupSite(),
                        CommonEnum.JyfwType.SUPPLIER.getCode(), supInfo.getJyfwList());
                // 业务员
                this.saveSupplierSalesmanList(supInfo.getSupplierSalesmanList(), client, supInfo.getSupSite(), supInfo.getSupSelfCode(), null);
            }
            /**
             * 若地点为配送中心（即为连锁总部）则提交的流程需要正常审批，后续逻辑按照现有不变。
             */
            if (!CollectionUtils.isEmpty(dcList) && dcList.contains(dto.getSupSite())) {
                saveSupInfo(supInfo, client, supFlowNo, gspinfoSubmitRequestDtoList, true);
                // 特殊经营范围
                businessScopeService.saveJyfwGspinfoData(client, supInfo.getSupSelfCode(), supFlowNo, supInfo.getJyfwList());
                // 业务员
                this.saveSupplierSalesmanList(supInfo.getSupplierSalesmanList(), client, supInfo.getSupSite(), supInfo.getSupSelfCode(), supFlowNo);
            }
        }

        // 提交后自动新建工作流，流程节点根据人员所属门店号，从权限表取出对应质量负责人、企业负责人。审批流程：提交人（已提交）->质量->企业负责人->END
        if (!CollectionUtils.isEmpty(gspinfoSubmitRequestDtoList)) {
            List<GspinfoSubmitRequestDto> collect = gspinfoSubmitRequestDtoList.stream().map(item -> {
                GspinfoSubmitRequestDto gspinfoSubmitRequestDto = new GspinfoSubmitRequestDto();
                BeanUtils.copyProperties(item, gspinfoSubmitRequestDto);
                return gspinfoSubmitRequestDto;
            }).collect(Collectors.toList());
            FeignResult result = feignService.createSupplierWorkflow2(collect, supFlowNo);
            if (result.getCode() != 0) {
                throw new CustomResultException(result.getMessage());
            }
        }
    }

    private void saveSupInfo(GaiaSupplierBusinessInfo dto, String client, String supFlowNo, List<GaiaSupplierGspinfo> list, boolean flowFlag) {
        GaiaSupplierGspinfo info = new GaiaSupplierGspinfo();
        BeanUtils.copyProperties(dto, info);
        //首营状态
        if (flowFlag) {
            info.setSupGspStatus(StringUtils.isNotEmpty(info.getSupGspStatus()) ? info.getSupGspStatus() : CommonEnum.GspinfoStauts.APPROVAL.getCode());
        } else {
            info.setSupGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
        }

        info.setSupFlowNo(supFlowNo);
        //供应商状态正常
        info.setSupStatus(StringUtils.isNotBlank(info.getSupStatus()) ? info.getSupStatus() : CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
        // 禁止采购
        info.setSupNoPurchase(StringUtils.isNotBlank(info.getSupNoPurchase()) ? info.getSupNoPurchase() : CommonEnum.NoYesStatus.NO.getCode());
        // 禁止退厂
        info.setSupNoSupplier(StringUtils.isNotBlank(info.getSupNoSupplier()) ? info.getSupNoSupplier() : CommonEnum.NoYesStatus.NO.getCode());
        // 匹配状态
        info.setSupMatchStatus(StringUtils.isNotBlank(info.getSupMatchStatus()) ? info.getSupMatchStatus() : CommonEnum.MatchStatus.EXACTMATCH.getCode());
        // 助计码
        info.setSupPym(StringUtils.isBlank(info.getSupPym()) ? StringUtils.ToFirstChar(info.getSupName()) : null);

        if (flowFlag) {
            GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
            gaiaSupplierBusinessKey.setClient(info.getClient());
            gaiaSupplierBusinessKey.setSupSite(info.getSupSite());
            gaiaSupplierBusinessKey.setSupSelfCode(info.getSupSelfCode());
            GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(gaiaSupplierBusinessKey);
            if (gaiaSupplierBusiness != null) {
                throw new CustomResultException("供应商编码" + info.getSupSelfCode() + "已存在");
            }
        }
        List<GaiaSupplierGspinfo> gaiaSupplierGspinfoList = gaiaSupplierGspinfoMapper.selfOnlyCheck(info.getClient(), info.getSupSelfCode(), info.getSupSite());
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(gaiaSupplierGspinfoList)) {
            throw new CustomResultException("供应商编码" + info.getSupSelfCode() + "已存在");
        }
        // 首营日期
        if (StringUtils.isBlank(dto.getSupGspDate())) {
            info.setSupGspDate(DateUtils.getCurrentDateStrYYMMDD());
        }
        gaiaSupplierGspinfoMapper.insert(info);
        if (flowFlag) {
            list.add(info);
            // 首营日志
            // 供应商停用判断
            GaiaSupplierBasic gaiaSupplierBasic = gaiaSupplierBasicMapper.selectByPrimaryKey(dto.getSupCode());
            GaiaSupplierGspinfoLog gaiaSupplierGspinfoLogOld = new GaiaSupplierGspinfoLog();
            if (!ObjectUtils.isEmpty(gaiaSupplierBasic)) {
                BeanUtils.copyProperties(gaiaSupplierBasic, gaiaSupplierGspinfoLogOld);
            }
            // 加盟商
            gaiaSupplierGspinfoLogOld.setClient(client);
            // 自编码
            gaiaSupplierGspinfoLogOld.setSupSelfCode(dto.getSupSelfCode());
            // 地点
            gaiaSupplierGspinfoLogOld.setSupSite(dto.getSupSite());
            gaiaSupplierGspinfoLogOld.setSupLine(1);
            gaiaSupplierGspinfoLogMapper.insertSelective(gaiaSupplierGspinfoLogOld);
            GaiaSupplierGspinfoLog gaiaSupplierGspinfoLogNew = new GaiaSupplierGspinfoLog();
            BeanUtils.copyProperties(dto, gaiaSupplierGspinfoLogNew);
            gaiaSupplierGspinfoLogOld.setSupLine(2);
            gaiaSupplierGspinfoLogMapper.insertSelective(gaiaSupplierGspinfoLogOld);
        }
    }

    private void saveBusiness(GaiaSupplierBusinessInfo dto, String client) {
        // 供应商停用判断
        GaiaSupplierBasic gaiaSupplierBasic = gaiaSupplierBasicMapper.selectByPrimaryKey(dto.getSupCode());
        // 非连锁
        GaiaSupplierBusiness business = new GaiaSupplierBusiness();
        // 主数据复制到业务表
        if (!ObjectUtils.isEmpty(gaiaSupplierBasic)) {
            SpringUtil.copyPropertiesIgnoreNull(gaiaSupplierBasic, business);
        }
        SpringUtil.copyPropertiesIgnoreNull(dto, business);

        //供应商状态正常
        business.setSupStatus(StringUtils.isNotBlank(business.getSupStatus()) ? business.getSupStatus() : CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
        // 禁止采购
        business.setSupNoPurchase(StringUtils.isNotBlank(business.getSupNoPurchase()) ? business.getSupNoPurchase() : CommonEnum.NoYesStatus.NO.getCode());
        // 禁止退厂
        business.setSupNoSupplier(StringUtils.isNotBlank(business.getSupNoSupplier()) ? business.getSupNoSupplier() : CommonEnum.NoYesStatus.NO.getCode());
        // 匹配状态
        business.setSupMatchStatus(StringUtils.isNotBlank(business.getSupMatchStatus()) ? business.getSupMatchStatus() : CommonEnum.MatchStatus.EXACTMATCH.getCode());
        // 助计码
        business.setSupPym(StringUtils.isBlank(business.getSupPym()) ? StringUtils.ToFirstChar(business.getSupName()) : business.getSupPym());

        GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
        gaiaSupplierBusinessKey.setClient(business.getClient());
        gaiaSupplierBusinessKey.setSupSite(business.getSupSite());
        gaiaSupplierBusinessKey.setSupSelfCode(business.getSupSelfCode());
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(gaiaSupplierBusinessKey);
        if (gaiaSupplierBusiness != null) {
            throw new CustomResultException("供应商编码" + business.getSupSelfCode() + "已存在");
        }
        List<GaiaSupplierGspinfo> gaiaSupplierGspinfoList = gaiaSupplierGspinfoMapper.selfOnlyCheck(business.getClient(), business.getSupSelfCode(), business.getSupSite());
        if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(gaiaSupplierGspinfoList)) {
            throw new CustomResultException("供应商编码" + business.getSupSelfCode() + "已存在");
        }
        // gsp有效期
        business.setSupGspDate(dto.getSupGspDateValidity());
        // 创建日期
        if (StringUtils.isBlank(business.getSupCreateDate())) {
            business.setSupCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        }
        gaiaSupplierBusinessMapper.insertSelective(business);

        // 首营日志
        GaiaSupplierGspinfoLog gaiaSupplierGspinfoLogOld = new GaiaSupplierGspinfoLog();
        if (!ObjectUtils.isEmpty(gaiaSupplierBasic)) {
            BeanUtils.copyProperties(gaiaSupplierBasic, gaiaSupplierGspinfoLogOld);
        }
        // 加盟商
        gaiaSupplierGspinfoLogOld.setClient(client);
        // 自编码
        gaiaSupplierGspinfoLogOld.setSupSelfCode(dto.getSupSelfCode());
        // 地点
        gaiaSupplierGspinfoLogOld.setSupSite(dto.getSupSite());
        gaiaSupplierGspinfoLogOld.setSupLine(1);
        gaiaSupplierGspinfoLogMapper.insertSelective(gaiaSupplierGspinfoLogOld);
        GaiaSupplierGspinfoLog gaiaSupplierGspinfoLogNew = new GaiaSupplierGspinfoLog();
        BeanUtils.copyProperties(dto, gaiaSupplierGspinfoLogNew);
        gaiaSupplierGspinfoLogOld.setSupLine(2);
        gaiaSupplierGspinfoLogMapper.insertSelective(gaiaSupplierGspinfoLogOld);

        // 第三方 gys-operation
        try {
            List<String> stoCodeList = gaiaStoreDataMapper.getNotChainStoCodeList(client);
            if (!CollectionUtils.isEmpty(stoCodeList) && stoCodeList.contains(business.getSupSite())) {
                operationFeignService.supplierSync(business.getClient(), business.getSupSite(), "1", Collections.singletonList(business.getSupSelfCode()));
            } else {
                operationFeignService.supplierSync(business.getClient(), business.getSupSite(), "2", Collections.singletonList(business.getSupSelfCode()));
            }
        } catch (Exception e) {
            log.info("[供应商信息新增/修改 gys-operation调用 异常]{}", e.getMessage());
        }


    }

    /**
     * 参数非空校验
     */
    private void checkParam(List<GaiaSupplierBusinessInfo> list) {
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("供应商列表不能为空");
        }
        list.forEach(item -> {
            if (StringUtils.isEmpty(item.getSupSelfCode())) {
                throw new CustomResultException("供应商自编码不能为空");
            }
//            if (StringUtils.isEmpty(item.getSupPym())) {
//                throw new CustomResultException("助记码不能为空");
//            }
            if (StringUtils.isEmpty(item.getSupName())) {
                throw new CustomResultException("供应商名称不能为空");
            }
            if (StringUtils.isEmpty(item.getSupClass())) {
                throw new CustomResultException("供应商分类不能为空");
            }
//            if (StringUtils.isEmpty(item.getSupStatus())) {
//                throw new CustomResultException("供应商状态不能为空");
//            }
        });
    }


    /**
     * 供应商查询列表
     */
    @Override
    public PageInfo<GaiaSupplierBusinessDTO> getSupList(GetSupListRequestDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        dto.setClient(client);

//        // 地点
//        List<String> stoList = new ArrayList<>();
//        if (!org.springframework.util.StringUtils.isEmpty(dto.getProSite())) {
//            stoList.add(dto.getProSite());
//        } else {
//            stoList = gaiaProductBasicMapper.getStoListByComp(client, dto.getCompadmId());
//            if (CollectionUtils.isEmpty(stoList)) {
//                return new PageInfo<GaiaSupplierBusinessDTO>();
//            }
//        }
        dto.setUserId(user.getUserId());
        // 分页
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<GaiaSupplierBusinessDTO> list = gaiaSupplierBusinessMapper.getSupList(dto, null);
        // 拼接 唯一主键（前端使用）
        list.forEach(item -> item.setId(item.getClient() + item.getSupSite() + item.getSupSelfCode()));
        // 分页信息
        PageInfo<GaiaSupplierBusinessDTO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public Result exportSupList(GetSupListRequestDTO dto) {
        dto.setPageNum(0);
        dto.setPageSize(0);
        PageInfo<GaiaSupplierBusinessDTO> supList = getSupList(dto);
        List<GaiaSupplierBusinessDTO> list = supList.getList();
        try {
            List<List<String>> dataList = new ArrayList<>();
            for (GaiaSupplierBusinessDTO redto : list) {
                // 打印行
                List<String> item = new ArrayList<>();
                dataList.add(item);
                // 加盟商
                item.add(redto.getClient() + "-" + redto.getFrancName());
                // 地点
                item.add(redto.getSupSite() + "-" + redto.getSiteName());
                // 供应商自编码
                item.add(redto.getSupSelfCode());
                // 供应商名称
                item.add(redto.getSupName());
                // 助记码
                item.add(redto.getSupPym());
                // 统一社会信用代码
                item.add(redto.getSupCreditCode());
                // 营业期限
                item.add(redto.getSupCreditDate());
                // 供应商分类
                Dictionary supClass = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.SUP_CLASS, redto.getSupClass());
                if (supClass != null) {
                    item.add(supClass.getLabel());
                } else {
                    item.add("");
                }
                // 法人
                item.add(redto.getSupLegalPerson());
                // 注册地址
                item.add(redto.getSupRegAdd());
                // 供应商状态
                item.add(CommonEnum.SupplierStauts.SUPPLIERNORMAL.getCode().equals(redto.getSupStatus()) ?
                        CommonEnum.SupplierStauts.SUPPLIERNORMAL.getName() : CommonEnum.SupplierStauts.SUPPLIERDISNORMAL.getName());
                // 证书号
                item.add(redto.getSupLicenceNo());
                // 发证日期
                item.add(redto.getSupLicenceDate());
                // 有效期至
                item.add(redto.getSupLicenceValid());
                // 生产经营范围
                item.add(redto.getSupScope());
                // 禁止采购
                if (StringUtils.isBlank(redto.getSupNoPurchase())) {
                    item.add("");
                } else {
                    item.add(CommonEnum.NoYesStatus.NO.getCode().equals(redto.getSupNoPurchase()) ?
                            CommonEnum.NoYesStatus.NO.getName() : CommonEnum.NoYesStatus.YES.getName());
                }
                // 禁止退厂
                if (StringUtils.isBlank(redto.getSupNoSupplier())) {
                    item.add("");
                } else {
                    item.add(CommonEnum.NoYesStatus.NO.getCode().equals(redto.getSupNoSupplier()) ?
                            CommonEnum.NoYesStatus.NO.getName() : CommonEnum.NoYesStatus.YES.getName());
                }
                // 付款条件
                item.add(redto.getSupPayTerm());
                // 业务联系人
                item.add(redto.getSupBussinessContact());
                // 联系人电话
                item.add(redto.getSupContactTel());
                // 送货前置期
                item.add(redto.getSupLeadTime());
                //银行代码
                item.add(redto.getSupBankCode());
                //银行名称
                item.add(redto.getSupBankName());
                //账户持有人
                item.add(redto.getSupAccountPerson());
                // 银行账号
                item.add(redto.getSupBankAccount());
                // 支付方式
                item.add(EnumUtils.EnumToMap(CommonEnum.CusPayMode.class).get(redto.getSupPayMode()));
                // 铺底授信额度
                item.add(redto.getSupCreditAmt());
                // 起订金额
                item.add(redto.getSupMixAmt() == null ? "" : redto.getSupMixAmt().stripTrailingZeros().toPlainString());
                // 法人委托书
                item.add(redto.getSupFrwts());
                // 法人委托书效期
                item.add(redto.getSupFrwtsxq());
                // 质量负责人
                item.add(redto.getSupZlfzr());
                // 仓库地址
                item.add(redto.getSupCkdz());
                // 网页登陆地址
                item.add(redto.getSupLoginAdd());
                // 登陆用户名
                item.add(redto.getSupLoginUser());
                // 系统参数
                item.add(redto.getSupSysParm());
                // 配送方式
                item.add(redto.getSupDeliveryMode());
                // 承运单位
                item.add(redto.getSupCarrier());
                // 随货同行单图片
                item.add(redto.getSupPictureShtxd());
                // 质量保证协议
                item.add(redto.getSupQuality());
                // 质保协议有效期
                item.add(redto.getSupQualityDate());
                // GSP证书
                item.add(redto.getSupGsp());
                // GSP有效期
                item.add(redto.getSupGspDate());
                // GMP证书
                item.add(redto.getSupGmp());
                // GMP有效期
                item.add(redto.getSupGmpDate());
                // 法人委托书图片
                item.add(redto.getSupPictureFrwts());
                // GSP证书图片
                item.add(redto.getSupPictureGsp());
                // GMP证书图片
                item.add(redto.getSupPictureGmp());
                // 其他图片
                item.add(redto.getSupPictureQt());
                // 运输时间
                item.add(redto.getSupDeliveryTimes());
                // 生产许可证书图片
                item.add(redto.getSupPictureScxkz());
                // 生产许可证书
                item.add(redto.getSupScxkz());
                // 生产许可证有效期
                item.add(redto.getSupScxkzDate());
                // 食品许可证书图片
                item.add(redto.getSupPictureSpxkz());
                // 食品许可证书
                item.add(redto.getSupSpxkz());
                // 食品许可证有效期
                item.add(redto.getSupSpxkzDate());
                // 医疗器械经营许可证图片
                item.add(redto.getSupPictureYlqxxkz());
                // 医疗器械经营许可证书
                item.add(redto.getSupYlqxxkz());
                // 医疗器械经营许可证在有效期
                item.add(redto.getSupYlqxxkzDate());
                // 法人委托书身份证
                item.add(redto.getSupFrwtsSfz());
                // 在途关闭天数
                item.add(redto.getSupCloseDate());
                // 创建日期
                item.add(redto.getSupCreateDate());
                // 样章
                item.add(redto.getSupSampleSeal());
                // 样章图片
                item.add(redto.getSupSampleSealImage());
                // 样票
                item.add(redto.getSupSampleTicket());
                // 样票图片
                item.add(redto.getSupSampleTicketImage());
                // 样膜
                item.add(redto.getSupSampleMembrane());
                // 样膜图片
                item.add(redto.getSupSampleMembraneImage());
                // 档案号
                item.add(redto.getSupDah());
                // 备注
                item.add(redto.getSupBeizhu());
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("供应商数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "加盟商",
            "地点",
            "供应商自编码",
            "供应商名称",
            "助记码",
            "统一社会信用代码",
            "营业期限",
            "供应商分类",
            "法人",
            "注册地址",
            "供应商状态",
            "证书号",
            "发证日期",
            "有效期至",
            "生产经营范围",
            "禁止采购",
            "禁止退厂",
            "付款条件",
            "业务联系人",
            "联系人电话",
            "送货前置期",
            "银行代码",
            "银行名称",
            "账户持有人",
            "银行账号",
            "支付方式",
            "铺底授信额度",
            "起订金额",
            "法人委托书",
            "法人委托书效期",
            "质量负责人",
            "仓库地址",
            "网页登陆地址",
            "登陆用户名",
            "系统参数",
            "配送方式",
            "承运单位",
            "随货同行单图片",
            "质量保证协议",
            "质保协议有效期",
            "GSP证书",
            "GSP有效期",
            "GMP证书",
            "GMP有效期",
            "法人委托书图片",
            "GSP证书图片",
            "GMP证书图片",
            "其他图片",
            "运输时间",
            "生产许可证书图片",
            "生产许可证书",
            "生产许可证有效期",
            "食品许可证书图片",
            "食品许可证书",
            "食品许可证有效期",
            "医疗器械经营许可证图片",
            "医疗器械经营许可证书",
            "医疗器械经营许可证在有效期",
            "法人委托书身份证",
            "在途关闭天数",
            "创建日期",
            "样章",
            "样章图片",
            "样票",
            "样票图片",
            "样膜",
            "样膜图片",
    };

    /**
     * 批量更新
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editSupList(List<GaiaSupplierBusiness> list, String remarks) {
        list.forEach(item -> {
            if (StringUtils.isEmpty(item.getClient())) {
                throw new CustomResultException("加盟商不能为空");
            }
            if (StringUtils.isEmpty(item.getSupSite())) {
                throw new CustomResultException("地点不能为空");
            }
            if (StringUtils.isEmpty(item.getSupSelfCode())) {
                throw new CustomResultException("自编码不能为空");
            }
        });
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        List<GaiaDcData> dcListToUpdate = gaiaProductBasicMapper.getDcListToUpdate(client, userId);

        // 工作流
        List<GaiaSupplierChange> changeList = new ArrayList<>();
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNO = System.currentTimeMillis() + com.gov.purchase.utils.StringUtils.leftPad(String.valueOf(i), 6, "0");

        // 第三方 gys-operation
        List<GaiaSupplierBusinessKey> gysOperationList = new ArrayList<>();

        for (GaiaSupplierBusiness supplierBusiness : list) {
            List<GaiaSupplierBusinessKey> supplierKeyList = new ArrayList<>();
            // 修改本身的这一条做更新
            GaiaSupplierBusinessKey supplierKey = new GaiaSupplierBusinessKey();
            supplierKey.setClient(supplierBusiness.getClient());
            supplierKey.setSupSite(supplierBusiness.getSupSite());
            supplierKey.setSupSelfCode(supplierBusiness.getSupSelfCode());
            supplierKeyList.add(supplierKey);

            // 如果地点是DC
            GaiaDcData gaiaDcData = dcListToUpdate.stream().filter(Objects::nonNull)
                    .filter(item -> item.getDcCode().equals(supplierBusiness.getSupSite()))
                    .findFirst().orElse(null);
            if (!ObjectUtils.isEmpty(gaiaDcData)) {
                // # 配送中心连锁总部下的门店 + 商品主数据下的门店
                List<String> stoList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(supplierBusiness.getClient(), supplierBusiness.getSupSite());
                stoList.stream().filter(Objects::nonNull).distinct().forEach(stoCode -> {
                    GaiaSupplierBusinessKey supKey = new GaiaSupplierBusinessKey();
                    supKey.setClient(supplierBusiness.getClient());
                    supKey.setSupSite(stoCode);
                    supKey.setSupSelfCode(supplierBusiness.getSupSelfCode());
                    supplierKeyList.add(supKey);
                    // 第三方 gys-operation
                });
                // A.工作流
                if (CommonEnum.NoYesStatus.YES.getCode().equals(gaiaDcData.getDcXgsfwf())) {
                    // 查询更新前的数据
                    List<GaiaSupplierBusiness> supOriginalProList = gaiaSupplierBusinessMapper.selectSupListByKeyList(supplierKeyList);
                    // 比对以及发起工作流
                    changeList.addAll(supplierDiff(supOriginalProList, supplierBusiness, userId, flowNO, remarks));
                    continue;
                }
            }
            List<GaiaSupplierBusiness> supOriginalProList = gaiaSupplierBusinessMapper.selectSupListByKeyList(supplierKeyList);
            changeList.addAll(supplierDiff(supOriginalProList, supplierBusiness, userId, null, remarks));
            // 直接更新
            gaiaSupplierBusinessMapper.updateBatch(supplierBusiness, supplierKeyList);
            // 第三方 gys-operation
            gysOperationList.addAll(supplierKeyList);
        }
        // 发起工作流
        if (!CollectionUtils.isEmpty(changeList)) {
            // 保存DB
            gaiaSupplierChangeMapper.saveBatch(changeList);
            // 地点为DC的发起工作流
            List<String> dcCodeList = dcListToUpdate.stream().filter(Objects::nonNull).map(GaiaDcData::getDcCode).collect(Collectors.toList());
            List<GaiaSupplierChange> changeListToCreateFlow = changeList.stream().filter(Objects::nonNull)
                    .filter(item -> dcCodeList.contains(item.getSupSite()) && StringUtils.isNotBlank(item.getSupFlowNo())).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(changeListToCreateFlow)) {
                FeignResult result = feignService.createEditSupplierWorkflow(changeListToCreateFlow, flowNO);
                if (result.getCode() != 0) {
                    throw new CustomResultException(result.getMessage());
                }
            }
        }

        // 第三方 gys-operation
        try {
            // 非连锁门店集合
            List<String> stoList = gaiaProductBasicMapper.getStoList(client);
            Map<String, List<GaiaSupplierBusinessKey>> collect = gysOperationList.stream().collect(Collectors.groupingBy(GaiaSupplierBusinessKey::getSupSite));
            collect.forEach((supSite, businessKeyList) -> {
                if (!CollectionUtils.isEmpty(stoList) && stoList.contains(supSite)) {
                    operationFeignService.supplierSync(client, supSite, "1", businessKeyList.stream().map(GaiaSupplierBusinessKey::getSupSelfCode).collect(Collectors.toList()));
                } else {
                    operationFeignService.supplierSync(client, supSite, "2", businessKeyList.stream().map(GaiaSupplierBusinessKey::getSupSelfCode).collect(Collectors.toList()));
                }
            });
        } catch (Exception e) {
            log.info("[供应商信息新增/修改 gys-operation调用 异常]{}", e.getMessage());
        }

    }

    private List<GaiaSupplierChange> supplierDiff(List<GaiaSupplierBusiness> supplierFromList, GaiaSupplierBusiness supplierTo, String userId, String flowNO, String remarks) {
        return supplierFromList.stream().flatMap(supplierFrom -> {
            List<DiffEntityReuslt> diffResultList = diffEntityUtils.diff(supplierFrom, supplierTo, diffEntityConfig.getSupplierDiffPropertyList());
            return diffResultList.stream().map(item -> {
                GaiaSupplierChange supplierChange = new GaiaSupplierChange();
                supplierChange.setClient(supplierFrom.getClient());
                supplierChange.setSupSite(supplierFrom.getSupSite());
                supplierChange.setSupSelfCode(supplierFrom.getSupSelfCode());
                supplierChange.setSupName(supplierFrom.getSupName());
                supplierChange.setSupCreditCode(supplierFrom.getSupCreditCode());
                supplierChange.setSupChangeField(item.getPropertyFrom());
                supplierChange.setSupChangeColumn(item.getColumn());
                supplierChange.setSupChangeColumnDes(item.getColumnDes());
                supplierChange.setSupChangeFrom(item.getValueFrom());
                supplierChange.setSupChangeTo(item.getValueTo());
                supplierChange.setSupChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                supplierChange.setSupChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                supplierChange.setSupChangeUser(userId);
                supplierChange.setSupFlowNo(flowNO);
                // todo 这里设置默认值
                supplierChange.setSupSfsp("0");
                supplierChange.setSupSfty("0");
                supplierChange.setSupRemarks(remarks);
                return supplierChange;
            });
        }).collect(Collectors.toList());

    }

    /**
     * 详情
     */
    @Override
    public GetSupDetailsDTO getSupDetails(GaiaSupplierBusinessKey key) {
        if (StringUtils.isEmpty(key.getClient())) {
            throw new CustomResultException("加盟商不能为空");
        }
        if (StringUtils.isEmpty(key.getSupSite())) {
            throw new CustomResultException("地点不能为空");
        }
        if (StringUtils.isEmpty(key.getSupSelfCode())) {
            throw new CustomResultException("自编码不能为空");
        }
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(key);
        if (!ObjectUtils.isEmpty(gaiaSupplierBusiness)) {
            GetSupDetailsDTO dto = new GetSupDetailsDTO();
            BeanUtils.copyProperties(gaiaSupplierBusiness, dto);
            dto.setSupGmpUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupGmp()));
            dto.setSupGspUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupGsp()));
            dto.setSupPictureFrwtsUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupPictureFrwts()));
            dto.setSupPictureGmpUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupPictureGmp()));
            dto.setSupPictureGspUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupPictureGsp()));
            dto.setSupPictureQtUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupPictureQt()));
            dto.setSupPictureShtxdUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupPictureShtxd()));
            dto.setSupPictureScxkzUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupPictureScxkz()));
            dto.setSupScxkzUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupScxkz()));
            dto.setSupPictureSpxkzUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupPictureSpxkz()));
            dto.setSupSpxkzUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupSpxkz()));
            dto.setSupPictureYlqxxkzUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupPictureYlqxxkz()));
            dto.setSupYlqxxkzUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupYlqxxkz()));
            dto.setSupFrwtsSfzUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupFrwtsSfz()));
            dto.setSupSampleSealImageUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupSampleSealImage()));
            dto.setSupSampleTicketImageUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupSampleTicketImage()));
            dto.setSupSampleMembraneImageUrl(cosUtils.urlAuth(gaiaSupplierBusiness.getSupSampleMembraneImage()));
            return dto;
        }
        return null;
    }

    /**
     * 更新
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editSup(GaiaSupplierBusiness business, String remarks) {
        String client = feignService.getLoginInfo().getClient();
        business.setClient(client);
        if (StringUtils.isEmpty(business.getSupSite())) {
            throw new CustomResultException("地点不能为空");
        }
        if (StringUtils.isEmpty(business.getSupSelfCode())) {
            throw new CustomResultException("自编码不能为空");
        }
        if (StringUtils.isEmpty(business.getSupPym())) {
            throw new CustomResultException("助记码");
        }
        if (StringUtils.isEmpty(business.getSupName())) {
            throw new CustomResultException("供应商名称");
        }
        if (StringUtils.isEmpty(business.getSupClass())) {
            throw new CustomResultException("供应商分类");
        }
        if (StringUtils.isEmpty(business.getSupStatus())) {
            throw new CustomResultException("供应商状态");
        }
        editSupList(Collections.singletonList(business), remarks);

//        try {
//            // 非连锁门店集合
//            List<String> stoList = gaiaProductBasicMapper.getStoList(business.getClient());
//            if (!CollectionUtils.isEmpty(stoList) && stoList.contains(business.getSupSite())) {
//                operationFeignService.supplierSync(business.getClient(), business.getSupSite(), "1", Collections.singletonList(business.getSupSelfCode()));
//            } else {
//                operationFeignService.supplierSync(business.getClient(), business.getSupSite(), "2", Collections.singletonList(business.getSupSelfCode()));
//            }
//        } catch (Exception e) {
//            log.info("[供应商信息新增/修改 gys-operation调用 异常]{}", e.getMessage());
//        }
    }

    @Override
    public List<GaiaUserData> getUserList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaUserDataMapper.selectByClient(user.getClient());
    }

    /**
     * 供应商业务员新增
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSupplierSalesmanList(List<GaiaSupplierSalesman> list, String client, String supSite, String supSelfCode, String gssFlowNo) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        checkSalesmanRepeatCodeOrName(list);
        List<GaiaSupplierSalesman> entityList = gaiaSupplierSalesmanMapper.selectSupplierSalesman(client);
        list.forEach(item -> {
            if (StringUtils.isBlank(item.getGssCode())) {
                GaiaSupplierSalesman entity = entityList.stream().filter(i -> item.getGssPhone().equals(i.getGssPhone())).findFirst().orElse(null);
                if (entity == null) {
                    item.setGssCode(StringUtils.concat(String.valueOf(System.currentTimeMillis()), RandomStringUtils.randomNumeric(6)));
                } else {
                    item.setGssCode(entity.getGssCode());
                    if (entity.getGssName().equals(item.getGssName())) {
                        throw new CustomResultException("手机号" + item.getGssPhone() + "已经存在对应业务员：" + entity.getGssName() + "，请确认。");
                    }
                }
            }
            item.setClient(client);
            item.setSupSite(supSite);
            item.setSupSelfCode(supSelfCode);
            item.setGssFlowNo(gssFlowNo);
        });
        gaiaSupplierSalesmanMapper.saveSupplierSalesmanList(list);
    }


    private void checkSalesmanRepeatCodeOrName(List<GaiaSupplierSalesman> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        list.stream().filter(Objects::nonNull).forEach(item -> {
            if (StringUtils.isBlank(item.getGssName())) {
                throw new CustomResultException("业务员不能为空");
            }
            if (StringUtils.isBlank(item.getGssPhone())) {
                throw new CustomResultException("电话不能为空");
            }
        });
        // 校验 电话 不可重复
        list.stream().collect(Collectors.groupingBy(GaiaSupplierSalesman::getGssPhone, Collectors.counting())).forEach((key, count) -> {
            if (count > 1) {
                throw new CustomResultException(MessageFormat.format("业务员电话[{0}]重复", key));
            }
        });
        list.stream().collect(Collectors.groupingBy(GaiaSupplierSalesman::getGssName, Collectors.counting())).forEach((key, count) -> {
            if (count > 1) {
                throw new CustomResultException(MessageFormat.format("业务员名称[{0}]重复", key));
            }
        });
    }

    /**
     * 供应商业务员编辑
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void editSupplierSalesmanList(EditSupplierSalesmanListVO vo) {
        String client = feignService.getLoginInfo().getClient();
        List<GaiaSupplierSalesman> supplierSalesmanList = vo.getSupplierSalesmanList();
        List<String> existsIdList = supplierSalesmanList.stream().filter(Objects::nonNull).filter(item -> StringUtils.isNotBlank(item.getGssCode()))
                .map(GaiaSupplierSalesman::getGssCode).collect(Collectors.toList());
        // 校验
        checkSalesmanRepeatCodeOrName(supplierSalesmanList);
        // 查询该供应商下已经被使用的业务员
        List<GaiaSupplierSalesman> hasUsedSalesmanList = gaiaSupplierSalesmanMapper.getHasUsedSalesmanList(client, vo.getSupSite(), vo.getSupSelfCode());
        if (!CollectionUtils.isEmpty(hasUsedSalesmanList)) {
            hasUsedSalesmanList.stream().filter(Objects::nonNull).forEach(salesman -> {
                if (!existsIdList.contains(salesman.getGssCode())) {
                    throw new CustomResultException(MessageFormat.format("业务员[{0}]已经被使用,不可删除", salesman.getGssName()));
                }
            });
        }
        // 删除
        gaiaSupplierSalesmanMapper.deleteBySup(client, vo.getSupSite(), vo.getSupSelfCode());
        if (CollectionUtils.isEmpty(supplierSalesmanList)) {
            return;
        }
        List<GaiaSupplierSalesman> entityList = gaiaSupplierSalesmanMapper.selectSupplierSalesman(client);
        List<GaiaSupplierSalesman> gaiaSupplierSalesmanList = new ArrayList<>();
        supplierSalesmanList.forEach(item -> {
            GaiaSupplierSalesman entity = entityList.stream().filter(i -> item.getGssPhone().equals(i.getGssPhone())).findFirst().orElse(null);
            if (StringUtils.isBlank(item.getGssCode())) {
                if (entity == null) {
                    item.setGssCode(StringUtils.concat(String.valueOf(System.currentTimeMillis()), RandomStringUtils.randomNumeric(6)));
                } else {
                    item.setGssCode(entity.getGssCode());
                    if (!entity.getGssName().equals(item.getGssName())) {
                        throw new CustomResultException("手机号" + item.getGssPhone() + "已经存在对应业务员：" + entity.getGssName() + "，请确认。");
                    }
                }
            } else {
                if (entity != null) {
                    if (!entity.getGssName().equals(item.getGssName())) {
                        throw new CustomResultException("手机号" + item.getGssPhone() + "已经存在对应业务员：" + entity.getGssName() + "，请确认。");
                    }
                }
            }
            item.setClient(client);
            item.setSupSite(vo.getSupSite());
            item.setSupSelfCode(vo.getSupSelfCode());
            gaiaSupplierSalesmanList.add(item);
        });
        gaiaSupplierSalesmanMapper.saveSupplierSalesmanList(supplierSalesmanList);
    }

    /**
     * 供应商业务员列表
     */
    @Override
    public List<GaiaSupplierSalesmanDTO> getSupplierSalesmanList(String supSite, String supSelfCode) {
        String client = feignService.getLoginInfo().getClient();
        String str = "";
        if (StringUtils.isNotBlank(supSelfCode)){
            String[] strs = supSelfCode.split("-");
            str = strs[0];
        }
        return gaiaSupplierSalesmanMapper.getSupplierSalesmanList(client, supSite,str);
    }

    /**
     * 供应商经营范围编辑
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editSupScope(EditScopeVO vo) {
        TokenUser tokenUser = feignService.getLoginInfo();
        String client = tokenUser.getClient();
        String userId = tokenUser.getUserId();

        // 修改后的值
        List<String> jyfwIdAfterList = vo.getScopeList().stream().filter(Objects::nonNull).filter(scope -> scope.getChecked() == 1).map(EditScopeVO.Scope::getJyfwId).sorted().collect(Collectors.toList());
        String jyfwIdAfterStr = String.join(",", jyfwIdAfterList);

        // 发起工作流编码
        String flowNO = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf((int) ((Math.random() * 4 + 1) * 100000)), 6, "0");

        // 地点集合
        List<GaiaSupplierBusinessKey> supplierKeyList = new ArrayList<>();
        GaiaSupplierBusinessKey supplierBusinessKey = new GaiaSupplierBusinessKey();
        supplierBusinessKey.setClient(client);
        supplierBusinessKey.setSupSite(vo.getJyfwSite());
        supplierBusinessKey.setSupSelfCode(vo.getJyfwOrgid());
        supplierKeyList.add(supplierBusinessKey);

        // 如果地点是DC
        GaiaDcData gaiaDcData = gaiaProductBasicMapper.getDcListToUpdate(client, userId).stream().filter(Objects::nonNull)
                .filter(item -> item.getDcCode().equals(vo.getJyfwSite()))
                .findFirst().orElse(null);
        if (!ObjectUtils.isEmpty(gaiaDcData)) {
            // # 配送中心连锁总部下的门店 + 商品主数据下的门店
            List<String> stoList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(client, vo.getJyfwSite());
            stoList.stream().filter(Objects::nonNull).distinct().forEach(stoCode -> {
                GaiaSupplierBusinessKey supKey = new GaiaSupplierBusinessKey();
                supKey.setClient(client);
                supKey.setSupSite(stoCode);
                supKey.setSupSelfCode(vo.getJyfwOrgid());
                supplierKeyList.add(supKey);
            });
        }
        // 修改记录
        List<GaiaSupplierChange> changeList = new ArrayList<>();
        // 供应商基础数据
        List<GaiaSupplierBusiness> supList = gaiaSupplierBusinessMapper.selectSupListByKeyList(supplierKeyList);
        // 修改前的值
        List<GetScopeListDTO> scopeList = businessScopeService.getScopeList(vo.getJyfwOrgid(), vo.getJyfwOrgname(), vo.getJyfwSite(),
                CommonEnum.JyfwType.SUPPLIER.getCode(), CommonEnum.JyfwClass.MANUFACTURING_ENTERPRISE.getCode())
                .stream().filter(item -> item.getChecked() == 1).collect(Collectors.toList());

        supList.forEach(sup -> {
            String jyfwIdBeforeStr = scopeList.stream().filter(Objects::nonNull)
                    .map(GetScopeListDTO::getJyfwId)
                    .sorted()
                    .collect(Collectors.joining(","));
            changeList.add(this.saveEditSupScopeRecord(client, userId, sup.getSupSite(), sup.getSupSelfCode(), sup.getSupName(), sup.getSupCreditCode(),
                    jyfwIdBeforeStr, jyfwIdAfterStr, flowNO));
        });

        if (!ObjectUtils.isEmpty(gaiaDcData) && CommonEnum.NoYesStatus.YES.getCode().equals(gaiaDcData.getDcXgsfwf())) {
            // 保存修改记录
            gaiaSupplierChangeMapper.saveBatch(changeList);
            // 发起工作流
            List<GaiaSupplierChange> collect = changeList.stream().filter(item -> item.getSupSite().equals(gaiaDcData.getDcCode())).collect(Collectors.toList());
            // 翻译
            collect.forEach(change -> {
                String supChangeFromTranslation = String.join(",", businessScopeServiceMapper.getJyfwNameByjyfwIdList(change.getSupChangeFrom()));
                String supChangeToTranslation = String.join(",", businessScopeServiceMapper.getJyfwNameByjyfwIdList(change.getSupChangeTo()));
                change.setSupChangeFrom(supChangeFromTranslation);
                change.setSupChangeTo(supChangeToTranslation);
            });
            FeignResult result = feignService.createEditSupplierWorkflow(collect, flowNO);
            if (result.getCode() != 0) {
                throw new CustomResultException(result.getMessage());
            }
        } else {
            // 如果地点为非DC 或者 该DC不发起工作流 ----> 直接更新
            List<Map<String, Object>> list = new ArrayList<>();
            List<JyfwDataDO> jyfwDataDOList = new ArrayList<>();
            changeList.forEach(change -> {
                EditScopeVO editScopeVO = new EditScopeVO();
                BeanUtils.copyProperties(vo, editScopeVO);
                editScopeVO.setJyfwSite(change.getSupSite());
                editScopeVO.setJyfwType(CommonEnum.JyfwType.SUPPLIER.getCode());
                if (CommonEnum.JyfwType.SUPPLIER.getCode().equals(editScopeVO.getJyfwType()) && StringUtils.isBlank(editScopeVO.getJyfwSite())) {
                    throw new CustomResultException("供应商经营范围编辑时,地点不能为空");
                }
                if (CommonEnum.JyfwType.CUSTOMER.getCode().equals(editScopeVO.getJyfwType()) && StringUtils.isBlank(editScopeVO.getJyfwSite())) {
                    throw new CustomResultException("客户经营范围编辑时,地点不能为空");
                }
                Map<String, Object> map = new HashMap<>();
                map.put("client", client);
                map.put("jyfwOrgid", editScopeVO.getJyfwOrgid());
                map.put("jyfwOrgname", editScopeVO.getJyfwOrgname());
                map.put("jyfwSite", editScopeVO.getJyfwSite());
                map.put("jyfwType", editScopeVO.getJyfwType());
                list.add(map);

                // 保存
                List<JyfwDataDO> itemJyfwDataDOList = editScopeVO.getScopeList().stream().filter(Objects::nonNull).filter(scope -> scope.getChecked().equals(1)).map(scope -> {
                    JyfwDataDO jyfwDataDO = new JyfwDataDO();
                    jyfwDataDO.setId(UUIDUtil.getUUID());
                    jyfwDataDO.setClient(client);
                    jyfwDataDO.setJyfwOrgid(editScopeVO.getJyfwOrgid());
                    jyfwDataDO.setJyfwOrgname(editScopeVO.getJyfwOrgname());
                    jyfwDataDO.setJyfwType(editScopeVO.getJyfwType());
                    jyfwDataDO.setJyfwSite(editScopeVO.getJyfwSite());
                    jyfwDataDO.setJyfwId(scope.getJyfwId());
                    jyfwDataDO.setJyfwName(scope.getJyfwName());
                    jyfwDataDO.setJyfwModiDate(DateUtils.getCurrentDateStrYYMMDD());
                    jyfwDataDO.setJyfwModiTime(DateUtils.getCurrentTimeStrHHMMSS());
                    jyfwDataDO.setJyfwModiId(userId);
                    return jyfwDataDO;
                }).collect(Collectors.toList());
                jyfwDataDOList.addAll(itemJyfwDataDOList);
            });
            if (!CollectionUtils.isEmpty(list)) {
                businessScopeServiceMapper.deleteScopes(list);
            }
            if (!CollectionUtils.isEmpty(jyfwDataDOList)) {
                businessScopeServiceMapper.saveBatchJyfwData(jyfwDataDOList);
            }
        }
    }

    /**
     * 供应商经营范围编辑_保存记录
     */
    private GaiaSupplierChange saveEditSupScopeRecord(String client, String userId, String site, String supSelfCode, String supName, String supCreditCode,
                                                      String supChangeFrom, String supChangeTo, String flowNO) {
        GaiaSupplierChange supplierChange = new GaiaSupplierChange();
        supplierChange.setClient(client);
        supplierChange.setSupSite(site);
        supplierChange.setSupSelfCode(supSelfCode);
        supplierChange.setSupName(supName);
        supplierChange.setSupCreditCode(supCreditCode);
        supplierChange.setSupChangeField(CommonEnum.JyfwEditFlowField.SUPPLIER.getCode());
        supplierChange.setSupChangeColumn(CommonEnum.JyfwEditFlowField.SUPPLIER.getCode());
        supplierChange.setSupChangeColumnDes("供应商经营范围");
        supplierChange.setSupChangeFrom(supChangeFrom);
        supplierChange.setSupChangeTo(supChangeTo);
        supplierChange.setSupChangeDate(DateUtils.getCurrentDateStrYYMMDD());
        supplierChange.setSupChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
        supplierChange.setSupChangeUser(userId);
        supplierChange.setSupFlowNo(flowNO);
        supplierChange.setSupSfsp("0");
        supplierChange.setSupSfty("0");
        supplierChange.setSupRemarks("供应商经营范围");
        return supplierChange;
    }


}
