package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "供应商列表返回参数")
public class SupplierListForPurchaseResponseDto {

    /**
     * 值
     */
    private String value;

    /**
     * 表示名
     */
    private String label;

    /**
     * 采购付款条件（供应商时存在）
     */
    private String supPayTerm;

    /**
     * 送货前置期（供应商时存在）
     */
    private String supLeadTime;

    /**
     * 拼音码
     */
    private String supPym;

    /**
     * 禁止采购
     */
    private String supNoPurchase;
}