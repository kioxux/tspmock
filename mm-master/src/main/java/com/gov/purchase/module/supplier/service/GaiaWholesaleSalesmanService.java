package com.gov.purchase.module.supplier.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaUserData;
import com.gov.purchase.entity.GaiaWholesaleSalesman;
import com.gov.purchase.module.supplier.dto.GaiaSupplierSalesmanDTO;
import com.gov.purchase.module.supplier.dto.WholesaleSalesVO;

import java.util.List;

public interface GaiaWholesaleSalesmanService {
    List<GaiaUserData> getSalesList();

    List<GaiaSupplierSalesmanDTO> getSalesManList(String supSite);

    PageInfo<WholesaleSalesVO> getWholesaleSalesList(Integer pageSize, Integer pageNum, String userId, String salesManId);

    Result deleteWholesaleSales(Long id);

    Result saveWholesaleSales(GaiaWholesaleSalesman vo);
}
