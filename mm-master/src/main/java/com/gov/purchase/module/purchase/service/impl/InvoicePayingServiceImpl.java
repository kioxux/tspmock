package com.gov.purchase.module.purchase.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.entity.*;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.Feistel;
import com.gov.purchase.common.utils.PayUtils;
import com.gov.purchase.common.utils.SmsUtils;
import com.gov.purchase.common.utils.UnionPayUtils;
import com.gov.purchase.constants.CommonConstants;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.InvoicePayingService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.InvoiceUtils;
import com.gov.purchase.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class InvoicePayingServiceImpl implements InvoicePayingService {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaInvoicePayingBasicMapper gaiaInvoicePayingBasicMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaPayingItemMapper gaiaPayingItemMapper;
    @Resource
    private GaiaPayingHeaderMapper gaiaPayingHeaderMapper;
    @Resource
    private GaiaSmsRechargeRecordMapper gaiaSmsRechargeRecordMapper;
    @Resource
    private GaiaUserDataMapper gaiaUserDataMapper;
    @Resource
    private PayUtils payUtils;
    @Resource
    private CsaobPay csaobPay;
    @Resource
    private BsaobPay bsaobPay;
    @Resource
    private GaiaCompadmMapper gaiaCompadmMapper;
    @Resource
    private InvoiceConfig invoiceConfig;
    @Resource
    CosUtils cosUtils;
    @Resource
    private Tencent tencent;
    @Resource
    private GaiaFicoInvoiceMapper gaiaFicoInvoiceMapper;
    @Resource
    private AppPay appPay;
    @Resource
    private SmsUtils smsUtils;
    @Resource
    private GaiaSmsTemplateMapper gaiaSmsTemplateMapper;
    @Resource
    private GaiaFiUserPaymentMaintenanceMapper gaiaFiUserPaymentMaintenanceMapper;

    /**
     * 开票查询
     */
    @Override
    public List<InvoicePayingDTO> listInvoice(ListInvoiceRequestDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        gaiaStoreDataMapper.initInvoice(user.getClient());
        List<InvoicePayingDTO> result = new ArrayList<>();
        if (StringUtils.isBlank(dto.getFicoCompanyCode()) || CommonEnum.FicoCompanyCode.STORE.getCode().equals(dto.getFicoCompanyCode())) {
            // 单体门店
            dto.setTypeList(new String[]{"1", "3", "4"});
            List<InvoicePayingDTO> list = gaiaStoreDataMapper.listStoreByCodeTax(dto);
            for (InvoicePayingDTO invoice : list) {
                invoice.setFicoCompanyCode(CommonEnum.FicoCompanyCode.STORE.getCode());
            }
            result.addAll(list);
        }
        if (StringUtils.isBlank(dto.getFicoCompanyCode()) || CommonEnum.FicoCompanyCode.DC.getCode().equals(dto.getFicoCompanyCode())) {
            // 批发
            List<InvoicePayingDTO> list = gaiaDcDataMapper.listDcCodeTax(dto);
            for (InvoicePayingDTO invoice : list) {
                invoice.setFicoCompanyCode(CommonEnum.FicoCompanyCode.DC.getCode());
            }
            result.addAll(list);
        }
        if (StringUtils.isBlank(dto.getFicoCompanyCode()) || CommonEnum.FicoCompanyCode.CHAIN.getCode().equals(dto.getFicoCompanyCode())) {
            List<InvoicePayingDTO> invoicePayingDTOList = gaiaCompadmMapper.selectCompadmInvoiceListByClien(dto);
            if (CollectionUtils.isNotEmpty(invoicePayingDTOList)) {
                List<InvoicePayingDTO> list = new ArrayList<>();
                for (InvoicePayingDTO invoicePayingDTO : invoicePayingDTOList) {
                    List<InvoicePayingTaxDTO> taxDetails = new ArrayList<>();
                    invoicePayingDTO.setFicoCompanyCode(CommonEnum.FicoCompanyCode.CHAIN.getCode());
                    list.add(invoicePayingDTO);
                    taxDetails.add(invoicePayingDTO.getTaxDetails().get(0));
                    // 查询条件
                    dto.setChainHead(invoicePayingDTO.getFicoStoreCode());
                    // 连锁门店
                    dto.setTypeList(new String[]{"2"});
                    List<InvoicePayingDTO> invoicePayingDTOList1 = gaiaStoreDataMapper.listStoreByCodeTaxChainHead(dto);
                    for (InvoicePayingDTO invoice : invoicePayingDTOList1) {
                        invoice.setFicoCompanyCode(CommonEnum.FicoCompanyCode.CHAIN.getCode());
                        InvoicePayingTaxDTO taxDTO = new InvoicePayingTaxDTO();
                        taxDTO.setFicoTaxSubjectName(invoice.getFicoStoreName());
                        taxDTO.setFicoTaxSubjectCode(invoice.getFicoStoreCode());
                        taxDetails.add(taxDTO);
                        invoice.setTaxDetails(taxDetails);
                        list.add(invoice);
                    }
                }
                result.addAll(list);
            }
        }
        return result;
    }

    /**
     * 开票保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveListInvoice(List<InvoiceDTO> list) {
        //对插入数据进行校验
        for (InvoiceDTO dto : list) {
            if ("2".equals(dto.getFicoInvoiceType())) {
                if (StringUtils.isBlank(dto.getFicoBankName())
                        || StringUtils.isBlank(dto.getFicoBankNumber())
                        || StringUtils.isBlank(dto.getFicoCompanyPhoneNumber())
                        || StringUtils.isBlank(dto.getFicoCompanyAddress())
                        || StringUtils.isBlank(dto.getFicoPayingbankName())
                        || StringUtils.isBlank(dto.getFicoPayingbankNumber())) {
                    return ResultUtil.error(ResultEnum.CHECK_PARAMETER.getCode(), "请先至基本信息页面维护发票信息！");
                }
            }
            //当前组织的纳税主体不是自己，当前组织不能作为其他组织的纳税主体
            if (!dto.getFicoCompanyCode().equals(dto.getFicoTaxSubjectCode())) {
                List<InvoiceDTO> list1 = list.stream().filter(t -> dto.getFicoCompanyCode().equals(t.getFicoTaxSubjectCode()))
                        .collect(Collectors.toList());
                if (list1.size() > 0) {
                    return ResultUtil.error(ResultEnum.CHECK_PARAMETER.getCode(), "当前组织不能作为其他组织的纳税主体！");
                }
            }
        }
        gaiaInvoicePayingBasicMapper.insertBatch(list);
        return ResultUtil.success();
    }

    /**
     * 付款查询
     */
    @Override
    public List<ListPayingResponseDTO> listPaying(ListPayingRequestDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        List<ListPayingResponseDTO> list;
        if (CommonEnum.FicoCompanyCode.STORE.getCode().equals(dto.getFicoCompanyCode())) {
            // 单体门店
            dto.setStoTypeList(new String[]{"1", "3", "4"});
            list = gaiaStoreDataMapper.listPayingStore(dto);
            for (ListPayingResponseDTO item : list) {
                // 组织
                item.setFicoCompanyCode(CommonEnum.FicoCompanyCode.STORE.getCode());
                // 付款方式
                if (StringUtils.isBlank(item.getFicoPaymentMethod())) {
                    item.setFicoPaymentMethod(dto.getFicoPaymentMethod());
                }
                // 金额
                item.setFicoPaymentAmount(new BigDecimal("0.01"));
            }
        } else if (CommonEnum.FicoCompanyCode.DC.getCode().equals(dto.getFicoCompanyCode())) {
            // 批发
            dto.setDcType("30");
            list = gaiaDcDataMapper.listPayingDc(dto);
            for (ListPayingResponseDTO item : list) {
                item.setFicoCompanyCode(CommonEnum.FicoCompanyCode.DC.getCode());
                if (StringUtils.isBlank(item.getFicoPaymentMethod())) {
                    item.setFicoPaymentMethod(dto.getFicoPaymentMethod());
                }
                item.setFicoPaymentAmount(new BigDecimal("0.01"));
            }
        } else {
            // 连锁门店
            dto.setStoTypeList(new String[]{"2"});
            dto.setDcType("20");
            list = gaiaStoreDataMapper.listPayingStoreAndDc(dto);
            for (ListPayingResponseDTO item : list) {
                item.setFicoCompanyCode(CommonEnum.FicoCompanyCode.CHAIN.getCode());
                if (StringUtils.isBlank(item.getFicoPaymentMethod())) {
                    item.setFicoPaymentMethod(dto.getFicoPaymentMethod());
                }
                item.setFicoPaymentAmount(new BigDecimal("0.01"));
            }
        }
        return list;
    }

    /**
     * 付款保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SavePayingResponseDTO savePaying(SavePayingRequestDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        // 校验 开始日期大于上期截至日期，且开始日期大于等于今天，开始日期小于本期截至时间
        String today = DateUtils.getCurrentDateStrYYMMDD();
        List<String> codeList = new ArrayList<>();
        Integer total = 0;
//        // 首月单价
//        Integer firstMonthPrice = appPay.getFirstMonthPrice();
//        // 月单价
//        Integer monthPrice = appPay.getMonthPrice();
        // 行数据日期验证
        for (SavePayingItemRequestDTO item : dto.getList()) {
            // 上期截止日
            String ficoLastDeadline = item.getFicoLastDeadline();
            if (StringUtils.isBlank(ficoLastDeadline)) {
                ficoLastDeadline = DateUtils.getYesterdayDate();
            }
            // 本期开始日
            String startingTime = item.getFicoStartingTime();
            // 本期截止日
            String endingTime = item.getFicoEndingTime();
            // 开始日期小于截止日期
            if (endingTime.compareTo(startingTime) <= 0) {
                throw new CustomResultException(ResultEnum.E0165, item.getFicoPayingstoreName());
            }
            // 开始日期大于上期截止日
            if (startingTime.compareTo(ficoLastDeadline) <= 0) {
                throw new CustomResultException(ResultEnum.E0166, item.getFicoPayingstoreName());
            }
            // 开始日期大于等于今天
            if (startingTime.compareTo(today) < 0) {
                throw new CustomResultException(ResultEnum.E0167, item.getFicoPayingstoreName());
            }
            // 开始日期验证
            GaiaPayingItem gaiaPayingItem = gaiaPayingItemMapper.selectPayingItem(user.getClient(), item.getFicoCompanyCode());
            // 未充值过
            if (gaiaPayingItem == null) {
                // 开始日期不等于今天
                if (!startingTime.equals(DateUtils.getCurrentDateStrYYMMDD())) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "开始日期不正确");
                }
            } else {
                if (!startingTime.equals(DateUtils.getNextDay(gaiaPayingItem.getFicoEndingTime()))
                        && !startingTime.equals(DateUtils.getCurrentDateStrYYMMDD())) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "开始日期不正确");
                }
            }
            // 付款方式 0月，1季，2半年，3年
            String ficoPaymentMethod = item.getFicoPaymentMethod();
            // 结束日期验证
            if (CommonEnum.PaymentMethod.MONTH.getCode().equals(ficoPaymentMethod)) {
                if (!endingTime.equals(DateUtils.getDayByMonth(startingTime, 1))) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "结束日期不正确");
                }
            }
            if (CommonEnum.PaymentMethod.SEASON.getCode().equals(ficoPaymentMethod)) {
                if (!endingTime.equals(DateUtils.getDayByMonth(startingTime, 3))) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "结束日期不正确");
                }
            }
            if (CommonEnum.PaymentMethod.HALF_YEAR.getCode().equals(ficoPaymentMethod)) {
                if (!endingTime.equals(DateUtils.getDayByMonth(startingTime, 6))) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "结束日期不正确");
                }
            }
            if (CommonEnum.PaymentMethod.YEAR.getCode().equals(ficoPaymentMethod)) {
                if (!endingTime.equals(DateUtils.getDayByMonth(startingTime, 12))) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "结束日期不正确");
                }
            }
            // 验证金额是否正确
            Integer ficoPaymentAmount = item.getFicoPaymentAmount();
            // 月单价
            Integer monthPrice = appPay.getMonthPrice();
            GaiaFiUserPaymentMaintenanceKey gaiaFiUserPaymentMaintenanceKey = new GaiaFiUserPaymentMaintenanceKey();
            gaiaFiUserPaymentMaintenanceKey.setClient(user.getClient());
            gaiaFiUserPaymentMaintenanceKey.setFicoPayingstoreCode(item.getFicoCompanyCode());
            GaiaFiUserPaymentMaintenance gaiaFiUserPaymentMaintenance = gaiaFiUserPaymentMaintenanceMapper.selectByPrimaryKey(gaiaFiUserPaymentMaintenanceKey);
            if (gaiaFiUserPaymentMaintenance != null && gaiaFiUserPaymentMaintenance.getFicoCurrentChargingStandard() != null && gaiaFiUserPaymentMaintenance.getFicoCurrentChargingStandard().compareTo(BigDecimal.ZERO) > 0) {
                monthPrice = gaiaFiUserPaymentMaintenance.getFicoCurrentChargingStandard().multiply(NumberUtils.createBigDecimal("100")).intValue();
            }
            // 充值
            if (CommonEnum.PaymentMethod.MONTH.getCode().equals(ficoPaymentMethod)) {
                if (ficoPaymentAmount.intValue() != monthPrice) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "付款金额不正确");
                }
            }
            if (CommonEnum.PaymentMethod.SEASON.getCode().equals(ficoPaymentMethod)) {
                if (ficoPaymentAmount.intValue() != 3 * monthPrice) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "付款金额不正确");
                }
            }
            if (CommonEnum.PaymentMethod.HALF_YEAR.getCode().equals(ficoPaymentMethod)) {
                if (ficoPaymentAmount.intValue() != 6 * monthPrice) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "付款金额不正确");
                }
            }
            if (CommonEnum.PaymentMethod.YEAR.getCode().equals(ficoPaymentMethod)) {
                if (ficoPaymentAmount.intValue() != 12 * monthPrice) {
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "付款金额不正确");
                }
            }
            codeList.add(item.getFicoCompanyCode());
            total += item.getFicoPaymentAmount().intValue();
        }
        // 校验 二维码有效期（5分钟）之内 相同付款主体不可再次付款
        List<String> listExit = gaiaPayingItemMapper.listNotPaying(user.getClient(), codeList, CommonEnum.PaymentStatus.UNPAID.getCode(), csaobPay.getC2b_expireTime());
        if (!listExit.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0168, StringUtils.join(listExit.toString(), ","));
        }

        // 生成付款二维码
        String billNo = "10VJ" + PayUtils.getBillNo(csaobPay.getC2b_billNoPre());
        PayParams payParams = new PayParams();
        payParams.setMsgId("0001");
        payParams.setTotalAmount(total);
        payParams.setBillNo(billNo);

        String res = null;
        String billQRCode = "";
        String qrCodeId = "";
        String ficoId = "";
        String b2bpayUrl="";
        //兼容老接口
        if(dto.getFicoType()==null||dto.getFicoType()==0){
            //默认扫码支付
            dto.setFicoType(CommonEnum.PayType.SCAN.getCode());
        }
        //扫码支付
        if(CommonEnum.PayType.SCAN.getCode().equals(dto.getFicoType())){
            try {
                res = PayUtils.payV2(payParams, csaobPay);
                Map map = JSONObject.parseObject(res);
                billQRCode = (String) map.get("billQRCode");
                qrCodeId = (String) map.get("qrCodeId");
                ficoId = billNo.substring(8);
                if (StringUtils.isBlank(billQRCode)) {
                    throw new CustomResultException(ResultEnum.E0164);
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new CustomResultException(ResultEnum.E0164);
            }
        //银联对公支付
        }else if(CommonEnum.PayType.GATEWAY.getCode().equals(dto.getFicoType())){
            try{
                payParams.setBankName(dto.getBankName());
                payParams.setChnlNo(dto.getChnlNo());
                b2bpayUrl=PayUtils.payB2B(payParams, bsaobPay);
                ficoId = billNo.substring(8);
            }catch (Exception e){
                e.printStackTrace();
                throw new CustomResultException(ResultEnum.E0172);
            }
        }

        // 主表数据
        GaiaPayingHeader header = new GaiaPayingHeader();
        header.setClient(user.getClient());
        header.setFicoId(ficoId);
        header.setFicoPaymentAmountTotal(BigDecimal.valueOf(total).divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_UP));
        header.setFicoOrderDate(DateUtils.getCurrentDateTimeStrYMDHMS());
        header.setFicoOperator(user.getUserId());
        header.setFicoPaymentStatus(CommonEnum.PaymentStatus.UNPAID.getCode());
        header.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.UN_INVOICE.getCode());
        // 流水号
        header.setFicoSerialNumber(billNo);
        header.setFicoQrcode(qrCodeId);
        header.setFicoType(dto.getFicoType());
        // 发票类型
        header.setFicoInvoiceType(dto.getFicoInvoiceType());
        // 订单类型
        header.setFicoOrderType(CommonEnum.ficoOrderType.STORESERVICE.getCode());
        gaiaPayingHeaderMapper.insertSelective(header);

        // 明细表数据
        List<GaiaPayingItem> list = new ArrayList<>();
        for (SavePayingItemRequestDTO itemDto : dto.getList()) {
            GaiaPayingItem item = new GaiaPayingItem();
            BeanUtils.copyProperties(itemDto, item);
            item.setClient(header.getClient());
            item.setFicoId(header.getFicoId());
            item.setFicoCompanyCode(itemDto.getFicoCompanyCode());

            item.setFicoPaymentAmount(BigDecimal.valueOf(itemDto.getFicoPaymentAmount()).divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_UP));
            // 付款主体验证
            GaiaInvoicePayingBasicKey gaiaInvoicePayingBasicKey = new GaiaInvoicePayingBasicKey();
            gaiaInvoicePayingBasicKey.setClient(header.getClient());
            gaiaInvoicePayingBasicKey.setFicoCompanyCode(itemDto.getFicoCompanyCode());
            GaiaInvoicePayingBasic gaiaInvoicePayingBasic = gaiaInvoicePayingBasicMapper.selectByPrimaryKey(gaiaInvoicePayingBasicKey);
            if (gaiaInvoicePayingBasic == null) {
                throw new CustomResultException("付款主体" + itemDto.getFicoCompanyCode() + "未维护发票信息");
            } else {
                // 纳税主体验证
                GaiaInvoicePayingBasicKey gaiaInvoicePayingBasicKey1 = new GaiaInvoicePayingBasicKey();
                gaiaInvoicePayingBasicKey1.setClient(header.getClient());
                gaiaInvoicePayingBasicKey1.setFicoCompanyCode(gaiaInvoicePayingBasic.getFicoTaxSubjectCode());
                GaiaInvoicePayingBasic gaiaInvoicePayingBasic1 = gaiaInvoicePayingBasicMapper.selectByPrimaryKey(gaiaInvoicePayingBasicKey1);
                if (gaiaInvoicePayingBasic1 == null) {
                    throw new CustomResultException("付款主体" + itemDto.getFicoCompanyCode() + "未维护发票信息");
                } else {
                    if (StringUtils.isBlank(gaiaInvoicePayingBasic1.getFicoSocialCreditCode())) {
                        throw new CustomResultException("付款主体" + itemDto.getFicoCompanyCode() + "的纳税主未维护发票信息");
                    }
                    // 增值税专用发票
                    if (CommonEnum.InvioceType.SPECIAL.getCode().equals(dto.getFicoInvoiceType())) {
                        //
                        if (StringUtils.isBlank(gaiaInvoicePayingBasic1.getFicoBankName()) || StringUtils.isBlank(gaiaInvoicePayingBasic1.getFicoBankNumber())) {
                            throw new CustomResultException("付款主体" + itemDto.getFicoCompanyCode() + "的纳税主体未维开户行名称或账号");
                        }
                    }
                }
            }
            item.setFicoPayingstoreName(gaiaInvoicePayingBasic.getFicoTaxSubjectName());
            item.setFicoPayingstoreCode(gaiaInvoicePayingBasic.getFicoTaxSubjectCode());
            list.add(item);
        }
        gaiaPayingItemMapper.insertBatch(list);

        SavePayingResponseDTO resDto = new SavePayingResponseDTO();
        resDto.setBillQRCode(billQRCode);
        resDto.setFicoId(ficoId);
        resDto.setClient(user.getClient());
        resDto.setOrderUrl(b2bpayUrl);
        return resDto;
    }

    /**
     * 支付回调
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void payResult(HttpServletRequest httpServletRequest) {
        Map<String, String[]> properties = httpServletRequest.getParameterMap();
        log.info("支付回调结果：{}", JsonUtils.beanToJson(properties));

        String[] billNo = properties.get("billNo");
        if (ObjectUtils.isEmpty(billNo)) {
            return;
        }
        // 单号
        String ficoId = billNo[0].substring(8);
        // 付款状态
        String status = properties.get("billStatus")[0];
        GaiaPayingHeader header = new GaiaPayingHeader();
        header.setFicoId(ficoId);
        // 支付成功
        if (CommonEnum.PayResult.PAID.getCode().equals(status)) {
            header.setFicoPaymentStatus(CommonEnum.PaymentStatus.SUCCESS.getCode());
            header.setFicoPayingDate(DateUtils.getCurrentDateTimeStrYMDHMS());
            header.setFicoPayResult(JsonUtils.beanToJson(properties));
            gaiaPayingHeaderMapper.updateByFicoId(header);

            // 短信修改
            GaiaPayingHeader payingHeader = gaiaPayingHeaderMapper.selectByFicoId(ficoId);
            if (payingHeader != null) {
                GaiaSmsRechargeRecord gaiaSmsRechargeRecord = gaiaSmsRechargeRecordMapper.selectByFicoId(ficoId);
                if (gaiaSmsRechargeRecord != null) {
                    gaiaSmsRechargeRecord.setGsrrPayStatus(NumberUtils.toInt(CommonEnum.PaymentStatus.SUCCESS.getCode(), 1));
                    gaiaSmsRechargeRecordMapper.updateByPrimaryKeySelective(gaiaSmsRechargeRecord);
                }
            }
            return;
        }
        // 支付失败
        header.setFicoPaymentStatus(CommonEnum.PaymentStatus.FAILE.getCode());
        header.setFicoPayResult(JsonUtils.beanToJson(properties));
        gaiaPayingHeaderMapper.updateByFicoId(header);
    }

    /**
     * 生成发票数据
     *
     * @param item
     * @param orderNo
     * @param ficoInvoiceLineNum
     * @throws Exception
     */
    private void insertItem(GetPayingItemDetailDTO item, String orderNo, Integer ficoInvoiceLineNum, Integer ficoType) throws Exception {
        String reqJson = getReqOrderJson(item, "信息技术服务");
        log.info("发票生成请求参数reqJson={}", reqJson);
        String fpqqlsh = InvoiceUtils.reqSend(invoiceConfig.getInvoiceReqUrl(), reqJson);
        //获取发票请求流水号 新增发票表数据
        GaiaFicoInvoice gaiaFicoInvoice = new GaiaFicoInvoice();
        BeanUtils.copyProperties(item, gaiaFicoInvoice);
        gaiaFicoInvoice.setFicoTaxSubjectName(item.getFicoPayingstoreName());
        gaiaFicoInvoice.setFicoTaxSubjectCode(item.getFicoPayingstoreCode());
        gaiaFicoInvoice.setFicoInvoiceAmount(item.getFicoPaymentAmount());
        //发票行号
        log.info("发票生成请求返回值ficoInvoiceLineNum={}", ficoInvoiceLineNum.toString());
        gaiaFicoInvoice.setFicoInvoceLineNum(ficoInvoiceLineNum.toString());
        //未开票 fpqqlsh为failure 保存发票数据时，默认发票订单号、发票流水号都为空
//        if ("failure".equals(fpqqlsh)) {
        //直接未开票
        gaiaFicoInvoice.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.UN_INVOICE.getCode());
//        }
        //开票中、开票成功、开票失败  都会返回 fpqqlsh
        if (!"failure".equals(fpqqlsh) && StringUtils.isNotBlank(fpqqlsh)) {
            gaiaFicoInvoice.setFicoFpqqlsh(fpqqlsh);
            gaiaFicoInvoice.setFicoOrderNo(orderNo);
            gaiaFicoInvoice.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.INVOICING.getCode());
        }
        // 开票区分
        gaiaFicoInvoice.setFicoType(ficoType);
        if (CommonEnum.InvioceType.SPECIAL.getCode().equals(ficoType.toString())) {
            gaiaFicoInvoice.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.UN_INVOICE.getCode());
        }
        // 门店服务费
        gaiaFicoInvoice.setFicoClass(1);
        gaiaFicoInvoice.setFicoOperator(item.getFicoOperator());
        gaiaFicoInvoiceMapper.insertSelective(gaiaFicoInvoice);
    }

    /**
     * 诺诺网开票请求入参 order
     */
    private String getReqOrderJson(GetPayingItemDetailDTO item, String goodsname) {
        String phone = invoiceConfig.getPhone();
        // 发票请求
        InvoiceApplyRequestDto reqDto = new InvoiceApplyRequestDto();
        reqDto.setIdentity(invoiceConfig.getIdentity());
        InvoiceApplyOrderRequestDto order = new InvoiceApplyOrderRequestDto();
        order.setBuyername(item.getFicoTaxSubjectName());
        //购方企业 税号、地址、开户行及账号、电话
        order.setTaxnum(item.getFicoSocialCreditCode());
        order.setAddress(item.getFicoCompanyAddress());
        order.setAccount((StringUtils.isBlank(item.getFicoBankName()) ? "" : item.getFicoBankName())
                + (StringUtils.isBlank(item.getFicoBankNumber()) ? "" : item.getFicoBankName()));
        order.setTelephone(item.getFicoCompanyPhoneNumber());
        order.setOrderno(item.getFicoOrderNo());
        order.setInvoicedate(DateUtils.getCurrentDateTimeStr());
        //销方企业税号、地址、开户行及账号、电话
        order.setSaletaxnum(invoiceConfig.getSaletaxnum());
        order.setSaleaddress(invoiceConfig.getSaleaddress());
        order.setSaleaccount(invoiceConfig.getSaleaccount());
        order.setSalephone(invoiceConfig.getSalephone());
        order.setKptype("1");
        //开票员 需要提供
        order.setClerk(invoiceConfig.getClerk());
        //推送手机(开票成功会短信提醒购方) 需要提供
        order.setPhone(phone);
        order.setQdbz("0");
        order.setDkbz("0");
        //发票类型 增值税普通发票 P 目前不支持增值税专用发票 TODO
        order.setInvoiceLine("p");
        order.setCpybz("0");
        order.setTsfs("-1");
        //发票明细
        List<InvoiceApplyDetailRequestDto> list = new ArrayList<>();
        InvoiceApplyDetailRequestDto dto = new InvoiceApplyDetailRequestDto();
        //商品名称 需要提
        dto.setGoodsname(goodsname);
        dto.setNum("1");
        dto.setPrice("1");
        dto.setPrice(item.getFicoPaymentAmount().toString());
        dto.setHsbz("1");
        dto.setTaxrate("0.06");
        dto.setSpec("");
        //单位：升或者吨
        dto.setUnit("");
        //税收分类编码   需要提供
        dto.setSpbm("3040203");
        dto.setFphxz("0");
        dto.setYhzcbs("0");
        list.add(dto);
        order.setDetail(list);
        reqDto.setOrder(order);
        //入参 json
        return JsonUtils.beanToJson(reqDto);
    }

    /**
     * 付款状态查询
     */
    @Override
    public GetPayStatusResponseDTO getPayStatus(String ficoId, String client) {
        GaiaPayingHeaderKey key = new GaiaPayingHeaderKey();
        key.setClient(client);
        key.setFicoId(ficoId);
        GaiaPayingHeader header = gaiaPayingHeaderMapper.selectByPrimaryKey(key);
        if (null == header) {
            return new GetPayStatusResponseDTO(CommonEnum.PaymentStatus.UNPAID.getCode());
        }
        if (CommonEnum.PaymentStatus.SUCCESS.getCode().equals(header.getFicoPaymentStatus())) {
            return new GetPayStatusResponseDTO(header.getFicoPaymentStatus());
        }
        // 构造查询参数
        QueryPayStatus query = new QueryPayStatus();
        query.setMsgId("001");
        query.setBillNo(csaobPay.getC2b_billNoPre() + ficoId);
        query.setBillDate(DateUtils.getCurrentDateStr());
        query.setFicoType(header.getFicoType());

        String responseStr = null;
        try {
            // 发起查询
            responseStr = PayUtils.queryPayStatus(query, csaobPay,bsaobPay);
        } catch (Exception e) {
            return new GetPayStatusResponseDTO(CommonEnum.PaymentStatus.UNPAID.getCode());
        }
        // 支付状态
        String status = ((Map<String, String>) JSON.parse(responseStr)).get("billStatus");
        // 如果支付成功 保存 并返回
        if (CommonEnum.PayResult.PAID.getCode().equals(status)) {
            GaiaPayingHeader headerUpdate = new GaiaPayingHeader();
            // 付款订单号
            headerUpdate.setFicoId(ficoId);
            // 支付状态
            headerUpdate.setFicoPaymentStatus(CommonEnum.PaymentStatus.SUCCESS.getCode());
            // 支付日期
            headerUpdate.setFicoPayingDate(DateUtils.getCurrentDateTimeStrYMDHMS());
            // 支付结果
            headerUpdate.setFicoPayResult(responseStr);
            gaiaPayingHeaderMapper.updateByFicoId(headerUpdate);
            return new GetPayStatusResponseDTO(CommonEnum.PaymentStatus.SUCCESS.getCode());
        }
        return new GetPayStatusResponseDTO(CommonEnum.PaymentStatus.UNPAID.getCode());
    }

    /**
     * 发票付款成功查询
     */
    @Override
    public PageInfo<GaiaFicoInvoice> listInvoicePayingSuccess(ListInvoicePayingRequestDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        dto.setClient(user.getClient());
        dto.setFicoOperator(user.getUserId());
        //分页查询
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        //返回数据
        return new PageInfo<>(gaiaFicoInvoiceMapper.selectSuccessByClient(dto));
    }

    @Override
    public String downloadInvoice(String url) {
        return cosUtils.urlAuth(url);
    }

    /**
     * APP支付申请
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result appPay(SavePayingRequestDTO dto) throws Exception {
        TokenUser user = feignService.getLoginInfo();
        // 校验 开始日期大于上期截至日期，且开始日期大于等于今天，开始日期小于本期截至时间
        String today = DateUtils.getCurrentDateStrYYMMDD();
        List<String> codeList = new ArrayList<>();
        Integer total = 0;
//        // 首月单价
//        Integer firstMonthPrice = appPay.getFirstMonthPrice();
//        // 月单价
//        Integer monthPrice = appPay.getMonthPrice();
        log.info("付款参数：{}", JSONObject.toJSONString(dto));
        // 行数据日期验证
        for (SavePayingItemRequestDTO item : dto.getList()) {
            // 月单价
            Integer monthPrice = appPay.getMonthPrice();
            GaiaFiUserPaymentMaintenanceKey gaiaFiUserPaymentMaintenanceKey = new GaiaFiUserPaymentMaintenanceKey();
            gaiaFiUserPaymentMaintenanceKey.setClient(user.getClient());
            gaiaFiUserPaymentMaintenanceKey.setFicoPayingstoreCode(item.getFicoCompanyCode());
            GaiaFiUserPaymentMaintenance gaiaFiUserPaymentMaintenance = gaiaFiUserPaymentMaintenanceMapper.selectByPrimaryKey(gaiaFiUserPaymentMaintenanceKey);
            if (gaiaFiUserPaymentMaintenance != null && gaiaFiUserPaymentMaintenance.getFicoCurrentChargingStandard() != null && gaiaFiUserPaymentMaintenance.getFicoCurrentChargingStandard().compareTo(BigDecimal.ZERO) > 0) {
                monthPrice = gaiaFiUserPaymentMaintenance.getFicoCurrentChargingStandard().multiply(NumberUtils.createBigDecimal("100")).intValue();
            }
            // 上期截止日
            String ficoLastDeadline = item.getFicoLastDeadline();
            if (StringUtils.isBlank(ficoLastDeadline)) {
                ficoLastDeadline = DateUtils.getYesterdayDate();
            }
            // 本期开始日
            String startingTime = item.getFicoStartingTime();
            // 本期截止日
            String endingTime = item.getFicoEndingTime();
            // 开始日期小于截止日期
            if (endingTime.compareTo(startingTime) <= 0) {
                log.info("付款主体:{},本期开始日期要小于本期截止日期,{},{}", item.getFicoCompanyCode(), startingTime, endingTime);
                throw new CustomResultException(ResultEnum.E0165, item.getFicoCompanyCode());
            }
            // 开始日期大于上期截止日
            if (startingTime.compareTo(ficoLastDeadline) <= 0) {
                log.info("付款主体:{},本期开始时间要大于上期截至时间,{},{}", item.getFicoCompanyCode(), startingTime, ficoLastDeadline);
                throw new CustomResultException(ResultEnum.E0166, item.getFicoCompanyCode());
            }
            // 开始日期大于等于今天
            if (startingTime.compareTo(today) < 0) {
                log.info("付款主体:{},本期开始时间要大于等于今天时间,{},{}", item.getFicoCompanyCode(), startingTime, today);
                throw new CustomResultException(ResultEnum.E0167, item.getFicoCompanyCode());
            }
            // 开始日期验证
            GaiaPayingItem gaiaPayingItem = gaiaPayingItemMapper.selectPayingItem(user.getClient(), item.getFicoCompanyCode());
            // 未充值过
            if (gaiaPayingItem == null) {
                // 开始日期不等于今天
                if (!startingTime.equals(DateUtils.getCurrentDateStrYYMMDD())) {
                    log.info("付款主体:{},开始日期不等于今天,{}", item.getFicoCompanyCode(), startingTime);
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "开始日期不正确");
                }
            } else {
                if (!startingTime.equals(DateUtils.getNextDay(gaiaPayingItem.getFicoEndingTime()))
                        && !startingTime.equals(DateUtils.getCurrentDateStrYYMMDD())) {
                    log.info("付款主体:{},开始日期不正确,{},{}", item.getFicoCompanyCode(), startingTime, DateUtils.getNextDay(gaiaPayingItem.getFicoEndingTime()));
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "开始日期不正确");
                }
            }
            // 付款方式 0月，1季，2半年，3年
            String ficoPaymentMethod = item.getFicoPaymentMethod();
            // 结束日期验证
            if (CommonEnum.PaymentMethod.MONTH.getCode().equals(ficoPaymentMethod)) {
                if (!endingTime.equals(DateUtils.getDayByMonth(startingTime, 1))) {
                    log.info("付款主体:{},结束日期不正确,{},{}", item.getFicoCompanyCode(), endingTime, DateUtils.getDayByMonth(startingTime, 1));
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "结束日期不正确");
                }
            }
            if (CommonEnum.PaymentMethod.SEASON.getCode().equals(ficoPaymentMethod)) {
                if (!endingTime.equals(DateUtils.getDayByMonth(startingTime, 3))) {
                    log.info("付款主体:{},结束日期不正确,{},{}", item.getFicoCompanyCode(), endingTime, DateUtils.getDayByMonth(startingTime, 3));
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "结束日期不正确");
                }
            }
            if (CommonEnum.PaymentMethod.HALF_YEAR.getCode().equals(ficoPaymentMethod)) {
                if (!endingTime.equals(DateUtils.getDayByMonth(startingTime, 6))) {
                    log.info("付款主体:{},结束日期不正确,{},{}", item.getFicoCompanyCode(), endingTime, DateUtils.getDayByMonth(startingTime, 6));
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "结束日期不正确");
                }
            }
            if (CommonEnum.PaymentMethod.YEAR.getCode().equals(ficoPaymentMethod)) {
                if (!endingTime.equals(DateUtils.getDayByMonth(startingTime, 12))) {
                    log.info("付款主体:{},结束日期不正确,{},{}", item.getFicoCompanyCode(), endingTime, DateUtils.getDayByMonth(startingTime, 12));
                    throw new CustomResultException("付款主体" + item.getFicoCompanyCode() + "结束日期不正确");
                }
            }
            // 验证金额是否正确
            Integer ficoPaymentAmount = item.getFicoPaymentAmount();
            // 充值金额验证
            if (CommonEnum.PaymentMethod.MONTH.getCode().equals(ficoPaymentMethod)) {
                if (ficoPaymentAmount.intValue() != monthPrice) {
                    log.info("付款主体:{},付款金额不正确,{},{}", item.getFicoCompanyCode(), monthPrice, ficoPaymentAmount.intValue());
                    throw new CustomResultException("付款主体" + item.getFicoCompanyName() + "付款金额不正确");
                }
            }
            if (CommonEnum.PaymentMethod.SEASON.getCode().equals(ficoPaymentMethod)) {
                if (ficoPaymentAmount.intValue() != 3 * monthPrice) {
                    log.info("付款主体:{},付款金额不正确,{},{}", item.getFicoCompanyCode(), 3 * monthPrice, ficoPaymentAmount.intValue());
                    throw new CustomResultException("付款主体" + item.getFicoCompanyName() + "付款金额不正确");
                }
            }
            if (CommonEnum.PaymentMethod.HALF_YEAR.getCode().equals(ficoPaymentMethod)) {
                if (ficoPaymentAmount.intValue() != 6 * monthPrice) {
                    log.info("付款主体:{},付款金额不正确,{},{}", item.getFicoCompanyCode(), 6 * monthPrice, ficoPaymentAmount.intValue());
                    throw new CustomResultException("付款主体" + item.getFicoCompanyName() + "付款金额不正确");
                }
            }
            if (CommonEnum.PaymentMethod.YEAR.getCode().equals(ficoPaymentMethod)) {
                if (ficoPaymentAmount.intValue() != 12 * monthPrice) {
                    log.info("付款主体:{},付款金额不正确,{},{}", item.getFicoCompanyCode(), 12 * monthPrice, ficoPaymentAmount.intValue());
                    throw new CustomResultException("付款主体" + item.getFicoCompanyName() + "付款金额不正确");
                }
            }
            codeList.add(item.getFicoCompanyCode());
            total += item.getFicoPaymentAmount().intValue();
        }
        // 校验 二维码有效期（5分钟）之内 相同付款主体不可再次付款
        List<String> listExit = gaiaPayingItemMapper.listNotPaying(user.getClient(), codeList, CommonEnum.PaymentStatus.UNPAID.getCode(), csaobPay.getC2b_expireTime());
        if (!listExit.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0168, StringUtils.join(listExit.toString(), ","));
        }
        // 单号
        String billNo = "10VJ" + payUtils.getBillNo(appPay.getBillNoPre());
        String ficoId = billNo.substring(8);
        // 主表数据
        GaiaPayingHeader header = new GaiaPayingHeader();
        header.setClient(user.getClient());
        header.setFicoId(ficoId);
        header.setFicoPaymentAmountTotal(BigDecimal.valueOf(total).divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_UP));
        header.setFicoOrderDate(DateUtils.getCurrentDateTimeStrYMDHMS());
        header.setFicoOperator(user.getUserId());
        header.setFicoPaymentStatus(CommonEnum.PaymentStatus.UNPAID.getCode());
        header.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.UN_INVOICE.getCode());
        // 流水号
        header.setFicoSerialNumber(billNo);
        header.setFicoType(CommonEnum.PayType.APP.getCode());
        // 发票类型
        header.setFicoInvoiceType(dto.getFicoInvoiceType());
        // 订单类型
        header.setFicoOrderType(CommonEnum.ficoOrderType.STORESERVICE.getCode());
        gaiaPayingHeaderMapper.insertSelective(header);

        // 明细表数据
        List<GaiaPayingItem> list = new ArrayList<>();
        for (SavePayingItemRequestDTO itemDto : dto.getList()) {
            GaiaPayingItem item = new GaiaPayingItem();
            BeanUtils.copyProperties(itemDto, item);
            item.setClient(header.getClient());
            item.setFicoId(header.getFicoId());
            item.setFicoCompanyCode(itemDto.getFicoCompanyCode());
            item.setFicoPaymentAmount(BigDecimal.valueOf(itemDto.getFicoPaymentAmount()).divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_UP));
            // 付款主体验证
            GaiaInvoicePayingBasicKey gaiaInvoicePayingBasicKey = new GaiaInvoicePayingBasicKey();
            gaiaInvoicePayingBasicKey.setClient(header.getClient());
            gaiaInvoicePayingBasicKey.setFicoCompanyCode(itemDto.getFicoCompanyCode());
            GaiaInvoicePayingBasic gaiaInvoicePayingBasic = gaiaInvoicePayingBasicMapper.selectByPrimaryKey(gaiaInvoicePayingBasicKey);
            if (gaiaInvoicePayingBasic == null) {
                throw new CustomResultException("付款主体" + itemDto.getFicoCompanyCode() + "未维护发票信息");
            } else {
                // 纳税主体验证
                GaiaInvoicePayingBasicKey gaiaInvoicePayingBasicKey1 = new GaiaInvoicePayingBasicKey();
                gaiaInvoicePayingBasicKey1.setClient(header.getClient());
                gaiaInvoicePayingBasicKey1.setFicoCompanyCode(gaiaInvoicePayingBasic.getFicoTaxSubjectCode());
                GaiaInvoicePayingBasic gaiaInvoicePayingBasic1 = gaiaInvoicePayingBasicMapper.selectByPrimaryKey(gaiaInvoicePayingBasicKey1);
                if (gaiaInvoicePayingBasic1 == null) {
                    throw new CustomResultException("付款主体" + itemDto.getFicoCompanyCode() + "未维护发票信息");
                } else {
                    if (StringUtils.isBlank(gaiaInvoicePayingBasic1.getFicoSocialCreditCode())) {
                        throw new CustomResultException("付款主体" + itemDto.getFicoCompanyCode() + "的纳税主体未维护统一社会信用码");
                    }
                }
                // 增值税专用发票
                if (CommonEnum.InvioceType.SPECIAL.getCode().equals(dto.getFicoInvoiceType())) {
                    //
                    if (StringUtils.isBlank(gaiaInvoicePayingBasic1.getFicoBankName()) || StringUtils.isBlank(gaiaInvoicePayingBasic1.getFicoBankNumber())) {
                        throw new CustomResultException("付款主体" + itemDto.getFicoCompanyCode() + "的纳税主体未维开户行名称或账号");
                    }
                }
            }
            item.setFicoPayingstoreName(gaiaInvoicePayingBasic.getFicoTaxSubjectName());
            item.setFicoPayingstoreCode(gaiaInvoicePayingBasic.getFicoTaxSubjectCode());
            list.add(item);
        }
        gaiaPayingItemMapper.insertBatch(list);

        // 支付申请
        UnionPay unionPay = new UnionPay();
        unionPay.setPayMethod(dto.getPayMethod());
        unionPay.setPaymentAmount(total);
        unionPay.setMerOrderId(billNo);
        String unionPayResult = UnionPayUtils.placeAnOrder(appPay, unionPay);

        return ResultUtil.success(unionPayResult);
    }

    /**
     * APP支付回调
     *
     * @param httpServletRequest
     */
    @Override
    public void appPayResult(HttpServletRequest httpServletRequest) {
        Map<String, String[]> properties = httpServletRequest.getParameterMap();
        log.info("APP支付回调结果：{}", JSONObject.toJSONString(properties));
        if (properties == null || properties.isEmpty()) {
            return;
        }
        String targetOrderId = properties.get("targetOrderId")[0];
        String merOrderId = properties.get("merOrderId")[0];
        String status = properties.get("status")[0];
        String ficoId = merOrderId.substring(8);
        GaiaPayingHeader header = new GaiaPayingHeader();
        header.setFicoId(ficoId);

        // 支付成功
        if (CommonEnum.PayResult.TRADE_SUCCESS.getCode().equals(status)) {
            header.setFicoPaymentStatus(CommonEnum.PaymentStatus.SUCCESS.getCode());
            header.setFicoPayingDate(DateUtils.getCurrentDateTimeStrYMDHMS());
            header.setFicoPayResult(JsonUtils.beanToJson(properties));
            gaiaPayingHeaderMapper.updateByFicoId(header);
            return;
        }
        // 支付失败
        header.setFicoPaymentStatus(CommonEnum.PaymentStatus.FAILE.getCode());
        header.setFicoPayResult(JsonUtils.beanToJson(properties));
        gaiaPayingHeaderMapper.updateByFicoId(header);
    }

    /**
     * 单价
     *
     * @return
     */
    @Override
    public Result payPrice() {
        Map<String, Integer> map = new HashMap<>();
        map.put("monthPrice", appPay.getMonthPrice());
        map.put("smsPrice", csaobPay.getSmsPrice());
        return ResultUtil.success(map);
    }

    /**
     * 发票生成
     *
     * @param ficoId
     */
    public void createInvoice(String ficoId) throws Exception {
        log.info("发票生成订单号：{}", ficoId);
        List<String> orderList = new ArrayList<>();
        // 付款主表
        GaiaPayingHeader payingHeader = gaiaPayingHeaderMapper.selectByFicoId(ficoId);
        //查询付款明细
        List<GetPayingItemDetailDTO> items = gaiaPayingItemMapper.selectByClientAndPicoId(payingHeader.getClient(), ficoId);
        if (!items.isEmpty()) {
            for (GetPayingItemDetailDTO item : items) {
                // 订单号
                String orderNo = "";
                //打印发票数量
                int invoiceCount;
                //付款金额
                BigDecimal amount = item.getFicoPaymentAmount();
                //发票最大额度
                BigDecimal quota = new BigDecimal(CommonConstants.INVICE_MAX_QUOTA);
                // 付款金额 <= 发票最大额度 开一张票  > 整数 + 1（余数 不为0.0000）
                if (amount.compareTo(quota) > 0) {
                    // 0取整  1取余
                    BigDecimal[] decimals = amount.divideAndRemainder(quota);
                    invoiceCount = Integer.valueOf(decimals[0].toString()) + (decimals[1].equals(new BigDecimal("0.0000")) ? 0 : 1);
                    for (int i = 0; i < invoiceCount; i++) {
                        while (true) {
                            orderNo = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(3);
                            if (!orderList.contains(orderNo)) {
                                orderList.add(orderNo);
                                break;
                            }
                        }
                        item.setFicoOrderNo(orderNo);
                        //填写最大余额
                        item.setFicoPaymentAmount(quota);
                        //最后一张发票 填写余数
                        if (invoiceCount - 1 == i && !decimals[1].equals(new BigDecimal("0.0000"))) {
                            item.setFicoPaymentAmount(decimals[1]);
                        }
                        item.setFicoOperator(payingHeader.getFicoOperator());
                        insertItem(item, orderNo, i + 1, NumberUtils.toInt(payingHeader.getFicoInvoiceType()));
                    }
                } else {
                    while (true) {
                        orderNo = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(3);
                        if (!orderList.contains(orderNo)) {
                            orderList.add(orderNo);
                            break;
                        }
                    }
                    item.setFicoOrderNo(orderNo);
                    item.setFicoOperator(payingHeader.getFicoOperator());
                    insertItem(item, orderNo, 1, NumberUtils.toInt(payingHeader.getFicoInvoiceType()));
                }
            }
        }
    }

    /**
     * 支付回调事务
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void payCallback() throws Exception {
        log.info("支付结果查询任务开始。");
        // 支付中的订单
        List<GaiaPayingHeader> list = gaiaPayingHeaderMapper.selectPayingOrder(csaobPay.getC2b_expireTime());
        if (CollectionUtils.isEmpty(list)) {
            log.info("支付结果查询任务结束。未查询到订单。");
            return;
        }
        log.info("未支付订单：{}", JSONObject.toJSONString(list));
        // 遍历支付状态
        for (GaiaPayingHeader gaiaPayingHeader : list) {
            // 扫码支付回调
            if (gaiaPayingHeader.getFicoType() != null && (
                    gaiaPayingHeader.getFicoType().intValue() == CommonEnum.PayType.SCAN.getCode()
                    ||gaiaPayingHeader.getFicoType().intValue() == CommonEnum.PayType.GATEWAY.getCode())) {
                // 支付结果查询
                String payResult = PayUtils.query(csaobPay,bsaobPay, gaiaPayingHeader.getFicoSerialNumber(),gaiaPayingHeader.getFicoType());
                // 支付状态
                String status = ((Map<String, String>) JSON.parse(payResult)).get("billStatus");
                // 如果支付成功 保存 并返回
                if (CommonEnum.PayResult.PAID.getCode().equals(status)) {
                    GaiaPayingHeader headerUpdate = new GaiaPayingHeader();
                    // 付款订单号
                    headerUpdate.setFicoId(gaiaPayingHeader.getFicoId());
                    // 支付状态
                    headerUpdate.setFicoPaymentStatus(CommonEnum.PaymentStatus.SUCCESS.getCode());
                    // 支付日期
                    headerUpdate.setFicoPayingDate(DateUtils.getCurrentDateTimeStrYMDHMS());
                    // 支付结果
                    headerUpdate.setFicoPayResult(payResult);
                    gaiaPayingHeaderMapper.updateByFicoId(headerUpdate);
                    // 短信充值
                    if (gaiaPayingHeader.getFicoOrderType() != null &&
                            gaiaPayingHeader.getFicoOrderType().intValue() == CommonEnum.ficoOrderType.SMSSERVICE.getCode().intValue()) {
                        // 修改短信充值记录状态
                        GaiaSmsRechargeRecord gaiaSmsRechargeRecord = gaiaSmsRechargeRecordMapper.selectByFicoId(gaiaPayingHeader.getFicoId());
                        if (gaiaSmsRechargeRecord != null) {
                            gaiaSmsRechargeRecord.setGsrrPayStatus(NumberUtils.toInt(CommonEnum.PaymentStatus.SUCCESS.getCode(), 1));
                            gaiaSmsRechargeRecordMapper.updateByPrimaryKeySelective(gaiaSmsRechargeRecord);
                        }
                    }
                }
            }
            // APP 支付回调
            if (gaiaPayingHeader.getFicoType() != null && gaiaPayingHeader.getFicoType().intValue() == CommonEnum.PayType.APP.getCode()) {
                // 支付结果查询
                String payResult = UnionPayUtils.query(appPay, gaiaPayingHeader.getFicoSerialNumber());
                // 支付状态
                String status = ((Map<String, String>) JSON.parse(payResult)).get("status");
                // 如果支付成功 保存 并返回
                if (CommonEnum.PayResult.TRADE_SUCCESS.getCode().equals(status)) {
                    GaiaPayingHeader headerUpdate = new GaiaPayingHeader();
                    // 付款订单号
                    headerUpdate.setFicoId(gaiaPayingHeader.getFicoId());
                    // 支付状态
                    headerUpdate.setFicoPaymentStatus(CommonEnum.PaymentStatus.SUCCESS.getCode());
                    // 支付日期
                    headerUpdate.setFicoPayingDate(DateUtils.getCurrentDateTimeStrYMDHMS());
                    // 支付结果
                    headerUpdate.setFicoPayResult(payResult);
                    gaiaPayingHeaderMapper.updateByFicoId(headerUpdate);
                }
            }
//            // 网关支付回调
//            if (gaiaPayingHeader.getFicoType() != null && gaiaPayingHeader.getFicoType().intValue() == CommonEnum.PayType.GATEWAY.getCode()) {
//
//            }
        }
        log.info("支付结果查询任务结束。");
    }

    /**
     * 生成发票
     */
    @Override
    public void initInvoice() {
        log.info("发票请求任务开始。");
        {
            try {
                List<GaiaPayingHeader> list = gaiaPayingHeaderMapper.selectUnInvoice();
                log.info("请求发票数据：{}", JSONObject.toJSONString(list));
                if (!CollectionUtils.isEmpty(list)) {
                    for (GaiaPayingHeader gaiaPayingHeader : list) {
                        // 订单开票状态
                        gaiaPayingHeader.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.INVOICING.getCode());
                        gaiaPayingHeaderMapper.updateByPrimaryKey(gaiaPayingHeader);
                        createInvoice(gaiaPayingHeader.getFicoId());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.info("生成发票数据报错。");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            }
        }

        {
            // 开票中数据
            List<Map<String, String>> smsList = new ArrayList<>();
            List<GaiaFicoInvoice> items = gaiaFicoInvoiceMapper.selectByClient();
            log.info("生成中发票数据：{}", JSONObject.toJSONString(items));
            if (!items.isEmpty()) {
                //过滤 发票订单号不为空、发票请求流水号不为空、pdf为空 的发票数据
                for (GaiaFicoInvoice item : items) {
                    List<Map<String, String>> itemList = new ArrayList<>();
                    Map<String, Object> map = new HashMap<>();
                    map.put("identity", invoiceConfig.getIdentity());
                    List<String> list = new ArrayList<>();
                    list.add(item.getFicoFpqqlsh());
                    map.put("fpqqlsh", list);
                    try {
                        log.info("发票流水号请求发票信息参数 map={}", JsonUtils.beanToJson(map));
                        InvoiceSendResponseDto resDto = InvoiceUtils.resSend(invoiceConfig.getInvoiceResUrl(), JsonUtils.beanToJson(map));
                        log.info("发票流水号请求发票信息返回值 resDto={}", resDto);
                        if (resDto != null && StringUtils.isNotBlank(resDto.getCMsg())) {
                            //更新 发票表
                            GaiaFicoInvoice record = new GaiaFicoInvoice();
                            BeanUtils.copyProperties(item, record);
                            record.setClient(item.getClient());
                            record.setFicoTaxSubjectCode(item.getFicoTaxSubjectCode());
                            record.setFicoKprq(resDto.getFicoFprq());
                            BeanUtils.copyProperties(resDto, record);
                            record.setFicoId(item.getFicoId());
                            //pdf地址
                            String cUrl = resDto.getCUrl();
                            log.info("发票流水号请求发票信息返回PDF地址 cUrl={}", cUrl);
                            //开票中
                            if (resDto.getCMsg().startsWith("开票完成")) {
                                if (StringUtils.isNotBlank(cUrl)) {
                                    record.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.SUCCESS.getCode());
                                    String path = tencent.getInvoicePath() + cUrl.substring(cUrl.lastIndexOf("/") + 1);
                                    log.info("PDF相对path={}", path);
                                    record.setFicoPdfUrl(path);
                                    //上传至腾讯云
                                    cosUtils.uploadInvoiceFile(InvoiceUtils.parse(new URL(cUrl).openStream()), cUrl.substring(cUrl.lastIndexOf("/") + 1));

                                    GaiaPayingHeaderKey gaiaPayingHeaderKey = new GaiaPayingHeaderKey();
                                    gaiaPayingHeaderKey.setClient(item.getClient());
                                    gaiaPayingHeaderKey.setFicoId(item.getFicoId());
                                    GaiaPayingHeader gaiaPayingHeader = gaiaPayingHeaderMapper.selectByPrimaryKey(gaiaPayingHeaderKey);
                                    if (gaiaPayingHeader != null) {
                                        GaiaUserDataKey gaiaUserDataKey = new GaiaUserDataKey();
                                        gaiaUserDataKey.setClient(item.getClient());
                                        gaiaUserDataKey.setUserId(gaiaPayingHeader.getFicoOperator());
                                        GaiaUserData gaiaUserData = gaiaUserDataMapper.selectByPrimaryKey(gaiaUserDataKey);
                                        if (gaiaUserData != null) {
                                            Map<String, String> smsMap = new HashMap<>();
                                            smsMap.put("date", record.getFicoKprq());
                                            smsMap.put("title", item.getFicoTaxSubjectName());
                                            Feistel feistel = new Feistel();
                                            String encrypt = feistel.encrypt(item.getFicoIndex().toString());
                                            smsMap.put("url", this.invoiceConfig.getDownloadPath() + encrypt);
                                            smsMap.put("phone", gaiaUserData.getUserTel());
                                            smsList.add(smsMap);
                                        }
                                    }
                                    record.setFicoOrderNo(item.getFicoOrderNo());
                                    record.setFicoInvoiceResult(JsonUtils.beanToJson(resDto));
                                    gaiaFicoInvoiceMapper.updateByPrimaryKeySelective(record);
                                }
                            } else if (resDto.getCMsg().contains("开票失败")) {
                                record.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.FAILE.getCode());
                                record.setFicoOrderNo(item.getFicoOrderNo());
                                record.setFicoInvoiceResult(JsonUtils.beanToJson(resDto));
                                gaiaFicoInvoiceMapper.updateByPrimaryKeySelective(record);
                            }
                        }
                    } catch (Exception e) {
                        log.error("发票流水号调用发票结果查询失败！");
                        e.printStackTrace();
//                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        smsList.clear();
                        continue;
                    }
                }
            }
            log.info("发票请求任务结束。");

            // 发送短信
            if (CollectionUtils.isNotEmpty(smsList)) {
                try {
                    // 短息内容
                    GaiaSmsTemplate smsTemplate = gaiaSmsTemplateMapper.selectByPrimaryKey(SmsUtils.SMS0000010);
                    log.info("发送短信：{}", JSONObject.toJSONString(smsList));
                    // 发送短信
                    smsList.forEach(item -> {
                        SmsEntity smsEntity = new SmsEntity();
                        smsEntity.setSmsId(SmsUtils.SMS0000010);
                        smsEntity.setPhone(item.get("phone"));
                        String date = item.get("date");
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                        String sd = sdf.format(new Date(Long.parseLong(date))); // 时间戳转换日期
                        String message = com.gov.purchase.utils.StringUtils.parse(smsTemplate.getSmsContent(),
                                this.invoiceConfig.getSgin() + StringUtils.right(StringUtils.left(sd, 6), 2),
                                StringUtils.right(sd, 2),
                                item.get("title"),
                                item.get("url")
                        );
                        smsEntity.setMsg(message);
                        smsUtils.sendMarketingSms(smsEntity);
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    log.info("短信发送失败");
                }
            }
        }
        {
            // 短信发票申请
            List<GaiaFicoInvoice> items = gaiaFicoInvoiceMapper.selectSmsByClient();
            if (CollectionUtils.isNotEmpty(items)) {
                items.forEach(item -> {
                    try {
                        GetPayingItemDetailDTO getPayingItemDetailDTO = new GetPayingItemDetailDTO();
                        getPayingItemDetailDTO.setFicoTaxSubjectName(item.getFicoTaxSubjectName());
                        getPayingItemDetailDTO.setFicoSocialCreditCode(item.getFicoSocialCreditCode());
                        getPayingItemDetailDTO.setFicoBankName(item.getFicoBankName());
                        getPayingItemDetailDTO.setFicoBankNumber(item.getFicoBankNumber());
                        getPayingItemDetailDTO.setFicoOrderNo(item.getFicoOrderNo());
                        getPayingItemDetailDTO.setFicoPaymentAmount(item.getFicoInvoiceAmount());
                        String reqJson = getReqOrderJson(getPayingItemDetailDTO, "短信服务费");
                        log.info("发票生成请求参数reqJson={}", reqJson);
                        String fpqqlsh = InvoiceUtils.reqSend(invoiceConfig.getInvoiceReqUrl(), reqJson);
                        log.info("发票生成请求返回状态fpqqlsh={}", fpqqlsh);
                        //开票中、开票成功、开票失败  都会返回 fpqqlsh
                        if (!"failure".equals(fpqqlsh) && StringUtils.isNotBlank(fpqqlsh)) {
                            item.setFicoFpqqlsh(fpqqlsh);
                            item.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.INVOICING.getCode());
                        }
                        gaiaFicoInvoiceMapper.updateByPrimaryKey(item);
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.info("发票请求失败");
                    }
                });
            }
        }
    }

    /**
     * 发票下载
     *
     * @param key
     */
    @Override
    public void download(HttpServletResponse response, String key) {
        BufferedInputStream bis = null;
        OutputStream os = null;
        BufferedOutputStream bos = null;
        InputStream inStream = null;
        try {
            Feistel feistel = new Feistel();
            String index = feistel.decrypt(key);
            GaiaFicoInvoice gaiaInvoice = gaiaFicoInvoiceMapper.selectByIndex(NumberUtils.toInt(index));
            if (gaiaInvoice == null) {
                return;
            }
            if (StringUtils.isBlank(gaiaInvoice.getFicoPdfUrl())) {
                return;
            }
            // 发票地址
            String dowurl = MessageFormat.format(this.tencent.getCosUrl(), this.tencent.getCosBucket()) + gaiaInvoice.getFicoPdfUrl();
            URL url = new URL(dowurl);
            //打开链接
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //设置请求方式为"GET"
            conn.setRequestMethod("GET");
            //通过输入流获取图片数据
            inStream = conn.getInputStream();
            bis = new BufferedInputStream(inStream);
            os = response.getOutputStream();
            bos = new BufferedOutputStream(os);
            response.setContentType("application/octet-stream;charset=" + "UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(System.currentTimeMillis() + ".pdf", "UTF-8"));
            byte[] buffer = new byte[1024];
            int len = bis.read(buffer);
            while (len != -1) {
                bos.write(buffer, 0, len);
                len = bis.read(buffer);
            }
            bos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 开票
     *
     * @param gaiaFicoInvoice
     */
    @Override
    public void invoice(GaiaFicoInvoice gaiaFicoInvoice) {
        // 发票数据
        GaiaFicoInvoiceKey gaiaFicoInvoiceKey = new GaiaFicoInvoiceKey();
        gaiaFicoInvoiceKey.setClient(gaiaFicoInvoice.getClient());
        gaiaFicoInvoiceKey.setFicoId(gaiaFicoInvoice.getFicoId());
        gaiaFicoInvoiceKey.setFicoInvoceLineNum(gaiaFicoInvoice.getFicoInvoceLineNum());
        gaiaFicoInvoiceKey.setFicoTaxSubjectCode(gaiaFicoInvoice.getFicoTaxSubjectCode());
        GaiaFicoInvoice entity = gaiaFicoInvoiceMapper.selectByPrimaryKey(gaiaFicoInvoiceKey);
        if (entity == null) {
            throw new CustomResultException("没查询到当前记录");
        }
        // 发票号码
        entity.setFicoFphm(gaiaFicoInvoice.getFicoFphm());
        // 发票代码
        entity.setFicoFpdm(gaiaFicoInvoice.getFicoFpdm());
        // 发票日期
        entity.setFicoKprq(gaiaFicoInvoice.getFicoKprq());
        // 开票状态
        entity.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.SUCCESS.getCode());
        gaiaFicoInvoiceMapper.updateByPrimaryKey(entity);
    }

    /**
     * 短信购买下单
     *
     * @param gsrrRechargeCount
     * @param gsrrRechargeAmt
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result buySms(Integer gsrrRechargeCount, Integer gsrrRechargeAmt) {
        // 短信单价
        Integer smsPrice = csaobPay.getSmsPrice();
        if (smsPrice == null || smsPrice.intValue() == 0) {
            throw new CustomResultException("短信单价未配置");
        }
        // 验证总金额是否正确
        Integer amt = smsPrice * gsrrRechargeCount;
        if (amt.intValue() != gsrrRechargeAmt.intValue()) {
            throw new CustomResultException("金额计算有误，请重新再试");
        }

        // 生成付款二维码
        String billNo = "10VJ" + PayUtils.getBillNo(csaobPay.getC2b_billNoPre());
        PayParams payParams = new PayParams();
        payParams.setMsgId("0001");
        payParams.setTotalAmount(amt);
        payParams.setBillNo(billNo);

        String res = null;
        String billQRCode = "";
        String qrCodeId = "";
        String ficoId = "";
        try {
            res = PayUtils.payV2(payParams, csaobPay);
            Map map = JSONObject.parseObject(res);
            billQRCode = (String) map.get("billQRCode");
            qrCodeId = (String) map.get("qrCodeId");
            ficoId = billNo.substring(8);
            if (StringUtils.isBlank(billQRCode)) {
                throw new CustomResultException(ResultEnum.E0164);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException(ResultEnum.E0164);
        }
        // 当前人
        TokenUser user = feignService.getLoginInfo();
        // 主表数据
        GaiaPayingHeader header = new GaiaPayingHeader();
        header.setClient(user.getClient());
        header.setFicoId(ficoId);
        header.setFicoPaymentAmountTotal(BigDecimal.valueOf(amt).divide(BigDecimal.valueOf(100), 2, BigDecimal.ROUND_HALF_UP));
        header.setFicoOrderDate(DateUtils.getCurrentDateTimeStrYMDHMS());
        header.setFicoOperator(user.getUserId());
        header.setFicoPaymentStatus(CommonEnum.PaymentStatus.UNPAID.getCode());
        header.setFicoInvoiceStatus(CommonEnum.InvoiceStatus.UN_INVOICE.getCode());
        // 流水号
        header.setFicoSerialNumber(billNo);
        header.setFicoQrcode(qrCodeId);
        header.setFicoType(CommonEnum.PayType.SCAN.getCode());
        // 发票类型
        header.setFicoInvoiceType(CommonEnum.InvioceType.ORDINARY.getCode());
        // 订单类型
        header.setFicoOrderType(CommonEnum.ficoOrderType.SMSSERVICE.getCode());
        gaiaPayingHeaderMapper.insertSelective(header);

        // 短信充值记录
        GaiaSmsRechargeRecord gaiaSmsRechargeRecord = new GaiaSmsRechargeRecord();
        // 加盟商
        gaiaSmsRechargeRecord.setClient(user.getClient());
        // 购买时间
        gaiaSmsRechargeRecord.setGsrrRechargeTime(org.apache.commons.lang3.time.DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS"));
        // 单价
        gaiaSmsRechargeRecord.setGsrrRechargePrice(smsPrice);
        // 条数
        gaiaSmsRechargeRecord.setGsrrRechargeCount(gsrrRechargeCount);
        // 金额
        gaiaSmsRechargeRecord.setGsrrRechargeAmt(amt);
        // 已用条数
        gaiaSmsRechargeRecord.setGsrrRechargeUseCount(0);
        // 付款订单号
        gaiaSmsRechargeRecord.setFicoId(ficoId);
        // 付款状态
        gaiaSmsRechargeRecord.setGsrrPayStatus(NumberUtils.toInt(CommonEnum.PaymentStatus.UNPAID.getCode(), 0));
        gaiaSmsRechargeRecordMapper.insert(gaiaSmsRechargeRecord);

        SavePayingResponseDTO resDto = new SavePayingResponseDTO();
        resDto.setBillQRCode(billQRCode);
        resDto.setFicoId(ficoId);
        resDto.setClient(user.getClient());
        return ResultUtil.success(resDto);
    }
}
