package com.gov.purchase.module.goods.dto;

import com.gov.purchase.common.entity.Pageable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode
public class GetProListRequestDTO extends Pageable {

    /**
     * 连锁总部id
     */
    private String compadmId;
    /**
     * 地点
     */
    private String proSite;
    /**
     * 商品编码
     */
    private String proSelfCode;
    /**
     * 商品名
     */
    private String proName;
    /**
     * 国际条形码
     */
    private String proBarcode;
    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 排序字段列表 (格式：supSelfCode desc)
     */
    private String orderItem;

    /**
     * 加盟商
     */
    private String client;

    /***
     * 商品编码_更多
     */
    private List<String> proSelfCodes;
    /**
     * 门店_更多
     */
    private List<String> proSites;
    /**
     * 用户ID
     */
    private String userId;
//    /**
//     * 商品定位
//     */
//    private String proPosition;

    /**
     * 首营日期开始时间
     */
    private String proCreateDateStart;
    /**
     * 首营日期结束时间
     */
    private String proCreateDateEnd;

    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 商品分类
     */
    private String proClass;
    /**
     * 商品分类Code
     */
    private String[][] proClassCode;
    /**
     * 商品分类Code
     */
    private List<String> proClassList;
    /**
     * 参数更多
     */
    @Valid
    private List<ColumnItem> paramList;

    @Data
    @EqualsAndHashCode
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ColumnItem {

        @NotBlank(message = "[查询字段设置] column不能为空")
        private String column;

        private Object value;

        @NotNull(message = "[查询字段设置] flag标识不能为空")
        private Integer flag;
    }

    /**
     * 是否有库存 0 ： 否，1 ： 是
     */
    private String isStore;
}
