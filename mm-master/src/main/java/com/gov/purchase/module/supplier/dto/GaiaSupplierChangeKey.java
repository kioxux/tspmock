package com.gov.purchase.module.supplier.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author zhoushuai
 * @date 2021/4/20 13:33
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class GaiaSupplierChangeKey {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String supSite;

    /**
     * 商品自编码
     */
    private String supSelfCode;

}
