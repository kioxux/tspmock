package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.DecimalUtils;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaTaxCode;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaTaxCodeMapper;
import com.gov.purchase.module.base.dto.businessImport.PurchaseContract;
import com.gov.purchase.module.base.dto.businessImport.SalesOrderProducts;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.purchase.dto.PurchaseContractDetailVO;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class PurchaseContractImport extends BusinessImport {
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaTaxCodeMapper gaiaTaxCodeMapper;

    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 采购主体
        String conCompanyCode = (String) field.get("conCompanyCode");
        if (StringUtils.isBlank(conCompanyCode)) {
            throw new CustomResultException("请选择采购主体");
        }
        TokenUser user = feignService.getLoginInfo();
        List<String> errorList = new ArrayList<>();
        List<PurchaseContractDetailVO> purchaseContractDetailVOList = new ArrayList<>();
        List<String> proSelfCodeList = new ArrayList<>();
        for (Integer key : map.keySet()) {
            // 行
            PurchaseContract purchaseContract = (PurchaseContract) map.get(key);
            proSelfCodeList.add(purchaseContract.getProSelfCode());
        }
        List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectProList(user.getClient(), conCompanyCode, proSelfCodeList);
        // 税率
        List<GaiaTaxCode> gaiaTaxCodeList = gaiaTaxCodeMapper.selectTaxList();
        // 行处理
        for (Integer key : map.keySet()) {
            PurchaseContractDetailVO purchaseContractDetailVO = new PurchaseContractDetailVO();
            // 行
            PurchaseContract purchaseContract = (PurchaseContract) map.get(key);
            // 商品验证
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessList.stream().filter(t -> t.getProSelfCode().equals(purchaseContract.getProSelfCode())).findFirst().orElse(null);
            if (gaiaProductBusiness == null) {
                errorList.add(MessageFormat.format("第{0}行，商品编码不正确", key + 1));
                continue;
            }
            // 税率验证
            if (StringUtils.isNotBlank(purchaseContract.getConProInputTax())) {
                GaiaTaxCode gaiaTaxCode = gaiaTaxCodeList.stream().filter(t -> t.getTaxCodeClass().equals("1") && t.getTaxCodeValue().equals(purchaseContract.getConProInputTax())).findFirst().orElse(null);
                if (gaiaTaxCode == null) {
                    errorList.add(MessageFormat.format("第{0}行，税率不正确", key + 1));
                    continue;
                } else {
                    purchaseContractDetailVO.setProInputTax(gaiaTaxCode.getTaxCode());
                }
            }
            purchaseContractDetailVO.setConProCode(gaiaProductBusiness.getProSelfCode());
            purchaseContractDetailVO.setConPrice(DecimalUtils.valueOf(purchaseContract.getConPrice()));
            if (StringUtils.isNotBlank(purchaseContract.getConQty())) {
                purchaseContractDetailVO.setConQty(DecimalUtils.valueOf(purchaseContract.getConQty()));
            }
            purchaseContractDetailVO.setProDepict(gaiaProductBusiness.getProDepict());
            purchaseContractDetailVO.setProSpecs(gaiaProductBusiness.getProSpecs());
            purchaseContractDetailVO.setProFactoryName(gaiaProductBusiness.getProFactoryName());
            purchaseContractDetailVO.setProPlace(gaiaProductBusiness.getProPlace());
            purchaseContractDetailVO.setProUnit(gaiaProductBusiness.getProUnit());
            purchaseContractDetailVOList.add(purchaseContractDetailVO);
        }
        // 验证报错
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success(purchaseContractDetailVOList);
    }
}
