package com.gov.purchase.module.base.service.impl.storeImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaArea;
import com.gov.purchase.entity.GaiaCompadm;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.entity.GaiaStoreDataKey;
import com.gov.purchase.mapper.GaiaAreaMapper;
import com.gov.purchase.mapper.GaiaCompadmMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.excel.Store;
import com.gov.purchase.utils.SpringUtil;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.21
 */
@Service
public class StoreImportEditImpl extends StoreImport {

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    GaiaAreaMapper gaiaAreaMapper;

    /**
     * 数据验证结果
     *
     * @param path 文件路径
     */
    @Override
    protected void checkEnd(String path) {
        // 生成导入日志
        createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.WAIT.getCode());
    }

    /**
     * 数据导入
     *
     * @param bulUpdateCode 批量操作编码
     * @param dataMap
     * @param <T>
     */
    @Override
    protected <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> dataMap) {
        List<String> errorList = new ArrayList<>();
        // 批量导入
        for (Integer key : dataMap.keySet()) {
            // 行数据
            Store store = (Store) dataMap.get(key);
            // 数据库对象
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(store.getClient());
            gaiaStoreDataKey.setStoCode(store.getStoCode());
            GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
            if (gaiaStoreData == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
                break;
            }
            SpringUtil.copyPropertiesIgnoreNull(store, gaiaStoreData);
            // 是否医保店
            if (super.isClear(store.getStoIfMedicalcare())) {
                // 清空
                gaiaStoreData.setStoIfMedicalcare(null);
            } else if (super.isNotBlank(store.getStoIfMedicalcareValue())) {
                gaiaStoreData.setStoIfMedicalcare(store.getStoIfMedicalcareValue());
            }
            // DTP
            if (super.isClear(store.getStoIfDtp())) {
                // 清空
                gaiaStoreData.setStoIfDtp(null);
            } else if (super.isNotBlank(store.getStoIfDtpValue())) {
                gaiaStoreData.setStoIfDtp(store.getStoIfDtpValue());
            }
            // 税分类
            if (super.isClear(store.getStoTaxClass())) {
                // 清空
                gaiaStoreData.setStoTaxClass(null);
            } else if (super.isNotBlank(store.getStoTaxClassValue())) {
                gaiaStoreData.setStoTaxClass(store.getStoTaxClassValue());
            }
            // 清空数据处理
            super.initNull(gaiaStoreData);
            //省
            gaiaStoreData.setStoProvince(store.getStoProvinceValue());
            //城市
            gaiaStoreData.setStoCity(store.getStoCityValue());
            //区/县
            gaiaStoreData.setStoDistrict(store.getStoDistrictValue());
            // 门店属性
            String sto_attribute = store.getStoAttribute();
            if (!CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(sto_attribute)) {
                store.setStoChainHead(null);
                store.setStoTaxSubject(null);
            }
            gaiaStoreDataMapper.updateByPrimaryKey(gaiaStoreData);
        }

        // 没有错误
        if (CollectionUtils.isEmpty(errorList)) {
            // 更新导入日志
            editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.COMPLETE.getCode());
            return ResultUtil.success();
        }
        Result result = ResultUtil.error(ResultEnum.E0115);
        result.setData(errorList);
        return result;
    }

    /**
     * 业务数据验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap) {
        return super.checkList((LinkedHashMap<Integer, Store>) dataMap);
    }

    /**
     * 相关数据验证
     *
     * @param store
     * @param errorList
     * @param key
     */
    @Override
    protected void otherCheck(Store store, List<String> errorList, int key) {
        // 门店编码存在验证
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(store.getClient());
        gaiaStoreDataKey.setStoCode(store.getStoCode());
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        if (gaiaStoreData == null) {
            errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
            return;
        }

        // 省
        String province = store.getStoProvince();
        if (StringUtils.isBlank(province)) {
            store.setStoProvinceValue(gaiaStoreData.getStoProvince());
        } else {
            List<GaiaArea> gaiaAreaProvince = gaiaAreaMapper.selectByName(province);
            if (CollectionUtils.isEmpty(gaiaAreaProvince)) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "省"));
                return;
            } else {
                store.setStoProvinceValue(gaiaAreaProvince.get(0).getAreaId());
            }
        }
        // 城市
        String city = store.getStoCity();
        if (StringUtils.isBlank(city)) {
            store.setStoCityValue(gaiaStoreData.getStoCity());
        } else {
            List<GaiaArea> gaiaAreaCity = gaiaAreaMapper.selectByName(city);
            if (CollectionUtils.isEmpty(gaiaAreaCity)) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "城市"));
                return;
            } else {
                boolean flg = false;
                for (GaiaArea gaiaArea : gaiaAreaCity) {
                    if (StringUtils.isNotBlank(gaiaArea.getParentId()) && gaiaArea.getParentId().equals(store.getStoProvinceValue())) {
                        store.setStoCityValue(gaiaArea.getAreaId());
                        flg = true;
                        break;
                    }
                }
                if (!flg) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "城市"));
                    return;
                }
            }
        }
        // 区/县
        String district = store.getStoDistrict();
        if (StringUtils.isBlank(district)) {
            store.setStoDistrictValue(gaiaStoreData.getStoDistrict());
        } else {
            List<GaiaArea> gaiaAreaDistrict = gaiaAreaMapper.selectByName(district);
            if (CollectionUtils.isEmpty(gaiaAreaDistrict)) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "区/县"));
                return;
            } else {
                boolean flg = false;
                for (GaiaArea gaiaArea : gaiaAreaDistrict) {
                    if (StringUtils.isNotBlank(gaiaArea.getParentId()) && gaiaArea.getParentId().equals(store.getStoCityValue())) {
                        store.setStoDistrictValue(gaiaArea.getAreaId());
                        flg = true;
                        break;
                    }
                }
                if (!flg) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "城市"));
                    return;
                }
            }
        }

        // 连锁总部判断
        // 门店属性
        String stoAttribute = store.getStoAttribute();
        // 连锁总部
        String stoChainHead = store.getStoChainHead();
        // 纳税主体
        String sto_tax_subject = store.getStoTaxSubject();
        // 连锁门店
        if (StringUtils.isNotBlank(stoAttribute)) {
            if (CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(stoAttribute)) {
                if (super.NULLVALUE.equals(stoChainHead)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "连锁门店"));
                    return;
                }
                if (StringUtils.isBlank(gaiaStoreData.getStoChainHead()) && StringUtils.isBlank(stoChainHead)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "连锁门店"));
                    return;
                }
            } else {
                if (super.isNotBlank(stoChainHead)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "非连锁门店不可以有连锁总部"));
                    return;
                }
            }
        } else {
            if (CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(gaiaStoreData.getStoAttribute())) {
                if (super.NULLVALUE.equals(stoChainHead)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "连锁门店"));
                    return;
                }
                if (StringUtils.isBlank(gaiaStoreData.getStoChainHead()) && StringUtils.isBlank(stoChainHead)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "连锁门店"));
                    return;
                }
                if (super.NULLVALUE.equals(sto_tax_subject)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "纳税主体"));
                    return;
                }
                if (StringUtils.isBlank(gaiaStoreData.getStoTaxSubject()) && StringUtils.isBlank(sto_tax_subject)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "纳税主体"));
                    return;
                }
            } else {
                if (StringUtils.isNotBlank(stoChainHead) && !super.NULLVALUE.equals(stoChainHead)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "非连锁门店不可以有连锁总部"));
                    return;
                }
                if (StringUtils.isNotBlank(sto_tax_subject) && !super.NULLVALUE.equals(sto_tax_subject)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "非连锁门店不可以有纳税主体"));
                    return;
                }
            }
        }
        if (super.isNotBlank(stoChainHead)) {
            GaiaCompadm gaiaCompadm = gaiaCompadmMapper.selectByPrimaryId(store.getClient(), stoChainHead);
            if (gaiaCompadm == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "连锁总部"));
                return;
            }
        }
        if (super.isNotBlank(sto_tax_subject)) {
            if (!sto_tax_subject.equals(stoChainHead)) {
                GaiaStoreDataKey gaiaStoreDataKey1 = new GaiaStoreDataKey();
                gaiaStoreDataKey1.setClient(store.getClient());
                gaiaStoreDataKey1.setStoCode(sto_tax_subject);
                GaiaStoreData gaiaStoreData1 = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey1);
                if (gaiaStoreData1 == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "纳税主体"));
                    return;
                }
            }
        }
    }
}
