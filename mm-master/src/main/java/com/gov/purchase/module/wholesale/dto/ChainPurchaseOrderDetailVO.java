package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.util.List;

/**
 * @description: 调取连锁采购单明细
 * @author: yzf
 * @create: 2021-11-18 09:48
 */
@Data
public class ChainPurchaseOrderDetailVO{

    /**
     * 订单号
     */
    private String poId;

    /**
     * 订单日期
     */
    private String poDate;

    /**
     * 客户
     */
    private String customerName;
    private String poCompanyCode;

    /**
     * 送达方id
     */
    private String deliveryPerCode;

    /**
     * 送达方
     */
    private String deliveryPerName;

    /**
     * 销售时间
     */
    private String saleDate;

    /**
     * 销售员
     */
    private String salePerName;
    private String salesManCode;

    /**
     * 审核状态
     */
    private String poSoFlag;

    /**
     * 备注
     */
    private String poHeadRemark;

    private List<ChainPurchaseOrderDetailDTO> details;

    private Integer priceType;

    private String poSupplierId;

}
