package com.gov.purchase.module.base.dto.excel;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.30
 */
@Data
public class Supplier {

    /**
     * 供应商编码
     */
    @ExcelValidate(index = 0, name = "供应商编码", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false)
    private String supCode;
    /**
     * 供应商名称
     */
    @ExcelValidate(index = 1, name = "供应商名称", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String supName;
    /**
     * 助记码
     */
    @ExcelValidate(index = 2, name = "助记码", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String supPym;
    /**
     * 营业期限
     */
    @ExcelValidate(index = 3, name = "营业期限", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", maxLength = 8, editRequired = false)
    private String supCreditDate;
    /**
     * /**
     * 统一社会信用代码
     */
    @ExcelValidate(index = 4, name = "统一社会信用代码", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String supCreditCode;
    /**
     * 供应商分类
     */
    @ExcelValidate(index = 5, name = "供应商分类", type = ExcelValidate.DataType.STRING, maxLength = 10,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.SUP_CLASS, editRequired = false)
    private String supClass;
    /**
     * 法人
     */
    @ExcelValidate(index = 6, name = "法人", type = ExcelValidate.DataType.STRING, maxLength = 20, editRequired = false)
    private String supLegalPerson;
    /**
     * 注册地址
     */
    @ExcelValidate(index = 7, name = "注册地址", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String supRegAdd;
    /**
     * 供应商状态
     */
    @ExcelValidate(index = 8, name = "供应商状态", type = ExcelValidate.DataType.STRING, maxLength = 2,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.SUP_STATUS, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "supStatusValue", editRequired = false)
    private String supStatus;
    /**
     * 供应商状态
     */
    private String supStatusValue;
    /**
     * 许可证编号
     */
    @ExcelValidate(index = 9, name = "许可证编号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String supLicenceNo;
    /**
     * 发证日期
     */
    @ExcelValidate(index = 10, name = "发证日期", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", maxLength = 8, addRequired = false, editRequired = false)
    private String supLicenceDate;
    /**
     * 有效期至
     */
    @ExcelValidate(index = 11, name = "有效期至", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", maxLength = 8, addRequired = false, editRequired = false)
    private String supLicenceValid;
    /**
     * 生产或经营范围
     */
    @ExcelValidate(index = 12, name = "生产或经营范围", type = ExcelValidate.DataType.STRING, maxLength = 200, addRequired = false, editRequired = false)
    private String supScope;
    /**
     * 加盟商
     */
    @ExcelValidate(index = 13, name = "加盟商", type = ExcelValidate.DataType.STRING, maxLength = 8, addRequired = false, editRequired = false,
            relyRequired = {"supSite", "supSelfCode", "supplierStatus", "supNoPurchase", "supNoSupplier", "supPayTerm",
                    "supBusinessContact", "supLeadTime", "supBankCode", "supBankName", "supAccountPerson", "supBankAccount",
                    "supPayMode", "supCreditAmt", ""})
    private String client;
    /**
     * 地点
     */
    @ExcelValidate(index = 14, name = "地点", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false, relyRequired = {"client"})
    private String supSite;
    /**
     * 供应商自编码
     */
    @ExcelValidate(index = 15, name = "供应商自编码", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false, relyRequired = {"client"})
    private String supSelfCode;
    /**
     * 地点供应商状态
     */
    @ExcelValidate(index = 16, name = "地点供应商状态", type = ExcelValidate.DataType.STRING, maxLength = 2, addRequired = false, editRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.SUP_STATUS, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            relyRequired = {"client"}, dictionaryStaticField = "supplierStatusValue")
    private String supplierStatus;
    /**
     * 地点供应商状态
     */
    private String supplierStatusValue;
    /**
     * 禁止采购
     */
    @ExcelValidate(index = 17, name = "禁止采购", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "supNoPurchaseValue", editRequired = false)
    private String supNoPurchase;
    /**
     * 禁止采购
     */
    private String supNoPurchaseValue;
    /**
     * 禁止退厂
     */
    @ExcelValidate(index = 18, name = "禁止退厂", type = ExcelValidate.DataType.STRING, maxLength = 2, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "supNoSupplierValue", editRequired = false)
    private String supNoSupplier;
    /**
     * 禁止退厂
     */
    private String supNoSupplierValue;
    /**
     * 付款条件
     */
    @ExcelValidate(index = 19, name = "付款条件", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String supPayTerm;
    /**
     * 业务联系人
     */
    @ExcelValidate(index = 20, name = "业务联系人", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String supBussinessContact;
    /**
     * 送货前置期
     */
    @ExcelValidate(index = 21, name = "送货前置期", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String supLeadTime;
    /**
     * 银行代码
     */
    @ExcelValidate(index = 22, name = "银行代码", type = ExcelValidate.DataType.STRING, maxLength = 12, addRequired = false, editRequired = false)
    private String supBankCode;
    /**
     * 银行名称
     */
    @ExcelValidate(index = 23, name = "银行名称", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false)
    private String supBankName;
    /**
     * 账户持有人
     */
    @ExcelValidate(index = 24, name = "账户持有人", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String supAccountPerson;
    /**
     * 银行账号
     */
    @ExcelValidate(index = 25, name = "银行账号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String supBankAccount;
    /**
     * 支付方式
     */
    @ExcelValidate(index = 26, name = "支付方式", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.SUP_PAY_MODE, editRequired = false)
    private String supPayMode;
    /**
     * 铺底授信额度
     */
    @ExcelValidate(index = 27, name = "铺底授信额度", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String supCreditAmt;
}
