package com.gov.purchase.module.store.service.impl;

import com.gov.purchase.entity.GaiaCustomerBusiness;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.mapper.GaiaCustomerBusinessMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.store.service.GdzqJobService;
import com.gov.purchase.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Slf4j
@Service("gdzqJobService")
public class GdzqJobServiceImpl implements GdzqJobService {

    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    public void autoExecModArrearsFlag(){
        log.info("<定时任务><欠款标志自动变更><执行开始>");
        //1-处理正常状态客户的欠款标志
        //如果当天日期为月末，查询固定到期日大于等于当前当前日期号，否则查询固定到期日为今天的客户列表
        LocalDate localDate = DateUtils.getCurrentLocalDate();
        int day = localDate.getDayOfMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        int monthEnd = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        List<GaiaCustomerBusiness> list = null;
        if(day < monthEnd){
            list = gaiaCustomerBusinessMapper.getCusListByZQTS(day);
        }else{
            list = gaiaCustomerBusinessMapper.getCusListByGreaterOrEqZQTS(monthEnd);
        }
        //判断客户的往来余额是否大于0，大于0，欠款标志变化0，禁止客户批量开单
        BigDecimal zero = BigDecimal.ZERO;
        for(GaiaCustomerBusiness gaiaCustomerBusiness : list){
            if(null != gaiaCustomerBusiness.getCusArAmt() && 1 == gaiaCustomerBusiness.getCusArAmt().compareTo(zero)){
                gaiaCustomerBusiness.setCusArrearsFlag("0");
                gaiaCustomerBusiness.setCusArAmt(null);
                gaiaCustomerBusinessMapper.updateByPrimaryKeySelective(gaiaCustomerBusiness);
            }

        }
        //2-处理正常状态门店的欠款标志
        List<GaiaStoreData> gaiaStoreDatas = null;
        if(day < monthEnd){
            gaiaStoreDatas = gaiaStoreDataMapper.getStoreListByZQTS(day);
        }else{
            gaiaStoreDatas = gaiaStoreDataMapper.getStoreListByGreaterOrEqZQTS(monthEnd);
        }
        //判断门店的往来余额是否大于0，大于0，欠款标志变化0，禁止客户批量开单
        for(GaiaStoreData gaiaStoreData : gaiaStoreDatas){
            if(null != gaiaStoreData.getStoArAmt() && 1 == gaiaStoreData.getStoArAmt().compareTo(zero)) {
                gaiaStoreData.setStoArrearsFlag("0");
                gaiaStoreData.setStoArAmt(null);
                gaiaStoreDataMapper.updateByPrimaryKeySelective(gaiaStoreData);
            }
        }
        log.info("<定时任务><欠款标志自动变更><执行结束>");
    }
}
