package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductBasicMapper;
import com.gov.purchase.module.base.dto.businessImport.GroupPriceProduct;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.delivery.dto.GaiaAllotProductPriceVO;
import com.gov.purchase.module.delivery.dto.SaveGaiaAllotProductPriceDTO;
import com.gov.purchase.module.delivery.service.AllotPriceService;
import com.gov.purchase.module.purchase.dto.GetProductListRequestDto;
import com.gov.purchase.module.purchase.dto.GetProductListResponseDto;
import com.gov.purchase.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class GroupPriceProductImport extends BusinessImport {
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    private AllotPriceService allotPriceService;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    @Transactional
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        List<String> errorList = new ArrayList<>();
        // 批量导入
        try {
            if (field.get("alpReceiveSite") == null) {
                errorList.add(MessageFormat.format(ResultEnum.SITE_ERROR.getMsg(), null));
            }
            if (field.get("gapgCode") == null) {
                errorList.add(MessageFormat.format(ResultEnum.GAPG_CODE_EMPTY.getMsg(), null));
            }
            if (field.get("gapgType") == null) {
                errorList.add(MessageFormat.format(ResultEnum.GAPG_TYPE_EMPTY.getMsg(), null));
            }

            if (errorList.size() > 0) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            GetProductListRequestDto dto = new GetProductListRequestDto();
            List<String> sitList = new ArrayList<>();
            List<String> proList = new ArrayList<>();
            sitList.add(field.get("alpReceiveSite").toString());
            dto.setProSite(sitList);
            dto.setClient(client);
            for (Integer key : map.keySet()) {
                GroupPriceProduct groupPriceProduct = (GroupPriceProduct) map.get(key);
                proList.add(groupPriceProduct.getProSelfCode());
            }
            dto.setProSelfCode3(StringUtils.join(proList.toArray(), ","));
            List<GetProductListResponseDto> list = gaiaProductBasicMapper.selectProductList(dto);
            List<String> proCodeList = new ArrayList<>();
            for (GetProductListResponseDto getProductListResponseDto : list) {
                proCodeList.add(getProductListResponseDto.getLabel());
            }
            List<GaiaAllotProductPriceVO> voList = new ArrayList<>();
            SaveGaiaAllotProductPriceDTO saveDto = new SaveGaiaAllotProductPriceDTO();
            saveDto.setGapgType(new Integer(field.get("gapgType").toString()));
            saveDto.setGapgCode(field.get("gapgCode").toString());
            saveDto.setAlpReceiveSite(field.get("alpReceiveSite").toString());
            for (Integer key : map.keySet()) {
                GroupPriceProduct groupPriceProduct = (GroupPriceProduct) map.get(key);
                if (!proCodeList.contains(groupPriceProduct.getProSelfCode())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品"));
                    continue;
                }
                GaiaAllotProductPriceVO vo = new GaiaAllotProductPriceVO();
                vo.setClient(client);
                vo.setGapgProSelfCode(groupPriceProduct.getProSelfCode());
                if (StringUtils.isNotEmpty(groupPriceProduct.getGapgAddAmt())) {
                    vo.setGapgAddAmt(new BigDecimal(groupPriceProduct.getGapgAddAmt()));
                }
                if (StringUtils.isNotEmpty(groupPriceProduct.getGapgAddRate())) {
                    vo.setGapgAddRate(new BigDecimal(groupPriceProduct.getGapgAddRate()));
                }
                if (StringUtils.isNotEmpty(groupPriceProduct.getGapgCataloguePrice())) {
                    vo.setGapgCataloguePrice(new BigDecimal(groupPriceProduct.getGapgCataloguePrice()));
                }
                voList.add(vo);
            }
            saveDto.setVoList(voList);
            if (errorList.size() > 0) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            allotPriceService.saveAllotProductPrice(saveDto);
            return ResultUtil.success();
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException(ResultEnum.E0126);
        }

    }
}
