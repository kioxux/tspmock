package com.gov.purchase.module.goods.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.11.12
 */
@Data
public class BatchEditParam {

    /**
     * 1：相等，2：模糊，3：in，4：>，5：>=，6：<，7：<=
     */
    private Integer type;

    private String field;

    private Object data;

}
