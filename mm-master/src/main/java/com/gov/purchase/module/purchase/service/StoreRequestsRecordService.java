package com.gov.purchase.module.purchase.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.purchase.dto.*;

import java.util.List;

/**
 * @author : Yzf
 * description: TODO
 * create time: 2021/11/15 11:02
 * @params
 * @return
 */
public interface StoreRequestsRecordService {

    /**
     * 连锁总部查询
     *
     * @return
     */
    List<CompadmVO> queryCompadms();

    /**
     * 门店列表
     * @return
     */
    List<GaiaStoreData> queryStores(String stoChainHead);


    /**
     * 请货订单数据查询
     *
     * @param compadmForStoreDto
     * @return
     */
    PageInfo<SdReplenishHExtandVO> queryStoreRequestsRecord(CompadmForStoreDto compadmForStoreDto);


    /**
     * 请货订单明细数据查询
     *
     * @param compadmForStoreDto
     * @return
     */
    SdReplenishDExtandVO queryStoreRequestsDetailRecord(CompadmForStoreDto compadmForStoreDto);

    /**
     * 请货明细数据查询
     *
     * @param compadmForStoreDto
     * @return
     */
    PageInfo<SdReplenishDExtandDTO> queryDetailRecord(CompadmForStoreDto compadmForStoreDto);


    /**
     * 请货订单关闭
     *
     * @param compadmForStoreDto
     * @return
     */
    int updateFlag(List<CompadmForStoreDto> compadmForStoreDto);


    /**
     * 请货订单审核功能
     *
     * @param list
     * @return
     */
    Result examineStoreRequests(List<CompadmForStoreDto> list);
}
