package com.gov.purchase.module.base.dto;

import com.gov.purchase.module.purchase.dto.GetProductListResponseDto;
import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021/5/7 14:42
 */
@Data
public class GetProductTranslationListDTO extends GetProductListResponseDto {

    /**
     * 厂家编码
     */
    private String proFactoryCode;

    /**
     * 进项税翻译
     */
    private String taxCodeName;

    /**
     * 地点编码
     */
    private String proSite;
    /**
     * 地点名
     */
    private String stoName;
}
