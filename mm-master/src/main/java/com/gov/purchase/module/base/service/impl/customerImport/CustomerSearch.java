package com.gov.purchase.module.base.service.impl.customerImport;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaCustomerBasic;
import com.gov.purchase.mapper.GaiaCustomerBasicMapper;
import com.gov.purchase.mapper.GaiaFranchiseeMapper;
import com.gov.purchase.module.base.dto.*;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.18
 */
@Service
public class CustomerSearch {

    @Resource
    GaiaCustomerBasicMapper gaiaCustomerBasicMapper;
    @Resource
    GaiaFranchiseeMapper gaiaFranchiseeMapper;
    @Resource
    CosUtils cosUtils;

    /**
     * 客户查询（通用）
     *
     * @param batSearchValBasic
     * @return
     */
    public Result selectCustomerBasicBatch(BatSearchValBasic batSearchValBasic) {
        // 查询条件
        Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
        initCustomerBasicWhere(map);
        PageHelper.startPage(batSearchValBasic.getPageNum(), batSearchValBasic.getPageSize());
        List<CustomerDto> batchList = gaiaCustomerBasicMapper.selectBasicBatch(map);
        PageInfo<CustomerDto> pageInfo = new PageInfo<>(batchList);

        return ResultUtil.success(pageInfo);
    }

    /**
     * 客户查询（通用+业务）
     *
     * @param batSearchValBasic
     * @return
     */
    public Result selectCustomerBusinessBatch(BatSearchValBasic batSearchValBasic) {
        // 查询条件
        Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
        initCustomerBusinessWhere(map);
        PageHelper.startPage(batSearchValBasic.getPageNum(), batSearchValBasic.getPageSize());
        List<CustomerDto> batchList = gaiaCustomerBasicMapper.selectBusinessBatch(map);
        PageInfo<CustomerDto> pageInfo = new PageInfo<>(batchList);

        return ResultUtil.success(pageInfo);
    }

    /**
     * 加载查询条件
     *
     * @param map
     */
    private void initCustomerBasicWhere(Map<String, SearchValBasic> map) {
        // 查询条件为空
        if (map == null || map.isEmpty()) {
            return;
        }

        // 客户分类
        SearchValBasic cus_class = map.get("c");
        if (cus_class != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(cus_class.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.CUS_CLASS, cus_class.getSearchVal()));
                list.add(cus_class.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(cus_class.getMoreVal())) {
                for (String item : cus_class.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.CUS_CLASS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                cus_class.setSearchVal(null);
            }
            cus_class.setMoreVal(list);
        }

        // 客户状态
        SearchValBasic cus_status = map.get("d");
        if (cus_status != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(cus_status.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.CUS_STATUS, cus_status.getSearchVal()));
                list.add(cus_status.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(cus_status.getMoreVal())) {
                for (String item : cus_status.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.CUS_STATUS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                cus_status.setSearchVal(null);
            }
            cus_status.setMoreVal(list);
        }
    }

    /**
     * 加载查询条件
     *
     * @param map
     */
    private void initCustomerBusinessWhere(Map<String, SearchValBasic> map) {
        // 查询条件为空
        if (map == null || map.isEmpty()) {
            return;
        }

        initCustomerBasicWhere(map);

        // 加盟商
        SearchValBasic client = map.get("e");
        if (client != null) {

            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(client.getSearchVal()) || CollectionUtils.isNotEmpty(client.getMoreVal())) {
                list.addAll(gaiaFranchiseeMapper.getFrancName(client));
                if (StringUtils.isNotBlank(client.getSearchVal())) {
                    list.add(client.getSearchVal());
                } else {
                    list.addAll(client.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                client.setSearchVal(null);
            }
            client.setMoreVal(list);
        }

        // 客户状态
        SearchValBasic cus_status = map.get("h");
        if (cus_status != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(cus_status.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.SUP_STATUS, cus_status.getSearchVal()));
                list.add(cus_status.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(cus_status.getMoreVal())) {
                for (String item : cus_status.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.SUP_STATUS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                cus_status.setSearchVal(null);
            }
            cus_status.setMoreVal(list);
        }
    }

    /**
     * 客户基础数据导出
     *
     * @param batSearchValBasic
     * @return
     */
    public Result exportCustomerBasicBatch(BatSearchValBasic batSearchValBasic) {
        try {
            // 查询条件
            Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
            initCustomerBasicWhere(map);
            List<CustomerDto> batchList = gaiaCustomerBasicMapper.selectBasicBatch(map);
            // 导出数据
            List<List<String>> basicListc = new ArrayList<>();
            initData(batchList, basicListc, null);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(basicHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(basicListc);
                    }},
                    new ArrayList<String>() {{
                        add("客户主数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 客户业务数据导出
     *
     * @param batSearchValBasic
     * @return
     */
    public Result exportCustomerBusinessBatch(BatSearchValBasic batSearchValBasic) {
        try {
            // 查询条件
            Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
            initCustomerBusinessWhere(map);
            List<CustomerDto> batchList = gaiaCustomerBasicMapper.selectBusinessBatch(map);
            // 导出数据
            List<List<String>> businessList = new ArrayList<>();
            initData(batchList, null, businessList);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(businessHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(businessList);
                    }},
                    new ArrayList<String>() {{
                        add("客户业务数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param basicListc
     * @param businessList
     */
    private void initData(List<CustomerDto> batchList, List<List<String>> basicListc, List<List<String>> businessList) {
        for (CustomerDto customerDto : batchList) {
            if (basicListc != null) {
                List<String> basicData = new ArrayList<>();
                //客户编码
                basicData.add(customerDto.getCusCode());
                //助记码
                basicData.add(customerDto.getCusPym());
                //客户名称
                basicData.add(customerDto.getCusName());
                //统一社会信用代码
                basicData.add(customerDto.getCusCreditCode());
                //营业期限
                basicData.add(customerDto.getCusCreditDate());
                //客户分类
                String cus_class = customerDto.getCusClass();
                initDictionaryStaticData(basicData, cus_class, CommonEnum.DictionaryStaticData.CUS_CLASS);
                //法人
                basicData.add(customerDto.getCusLegalPerson());
                //注册地址
                basicData.add(customerDto.getCusRegAdd());
                //客户状态
                String cus_status = customerDto.getCusStatus();
                initDictionaryStaticData(basicData, cus_status, CommonEnum.DictionaryStaticData.CUS_STATUS);
                //许可证编号
                basicData.add(customerDto.getCusLicenceNo());
                //发证日期
                basicData.add(customerDto.getCusLicenceDate());
                //有效期至
                basicData.add(customerDto.getCusLicenceValid());
                //生产或经营范围
                basicData.add(customerDto.getCusScope());
                basicListc.add(basicData);
            }
            if (businessList != null) {
                List<String> businessData = new ArrayList<>();
                //加盟商
                businessData.add(customerDto.getClient());
                //地点
                businessData.add(customerDto.getSiteName());
                //客户自编码
                businessData.add(customerDto.getCusSelfCode());
                //客户编码
                businessData.add(customerDto.getCusCode());
                // 匹配状态
                String cus_match_status = customerDto.getCusMatchStatus();
                initDictionaryStaticData(businessData, cus_match_status, CommonEnum.DictionaryStaticData.PRO_MATCH_STATUS);
                //助记码
                businessData.add(customerDto.getCusPym());
                //客户名称
                businessData.add(customerDto.getCusName());
                //统一社会信用代码
                businessData.add(customerDto.getCusCreditCode());
                //营业期限
                businessData.add(customerDto.getCusCreditDate());
                //客户分类
                String cus_class = customerDto.getCusClass();
                initDictionaryStaticData(businessData, cus_class, CommonEnum.DictionaryStaticData.CUS_CLASS);
                //法人
                businessData.add(customerDto.getCusLegalPerson());
                //注册地址
                businessData.add(customerDto.getCusRegAdd());
                //许可证编号
                businessData.add(customerDto.getCusLicenceNo());
                //发证日期
                businessData.add(customerDto.getCusLicenceDate());
                //有效期至
                businessData.add(customerDto.getCusLicenceValid());
                //生产或经营范围
                businessData.add(customerDto.getCusScope());
                //客户状态
                String cus_status = customerDto.getCusStatusBusiness();
                initDictionaryStaticData(businessData, cus_status, CommonEnum.DictionaryStaticData.CUS_STATUS);
                //禁止销售
                String cus_no_sale = customerDto.getCusNoSale();
                initDictionaryStaticData(businessData, cus_no_sale, CommonEnum.DictionaryStaticData.NO_YES);
                //禁止退货
                String cus_no_return = customerDto.getCusNoReturn();
                initDictionaryStaticData(businessData, cus_no_return, CommonEnum.DictionaryStaticData.NO_YES);
                //付款条件
                businessData.add(customerDto.getPayTypeDesc());
                //业务联系人
                businessData.add(customerDto.getCusBussinessContact());
                //联系人电话
                businessData.add(customerDto.getCusContactTel());
                //收货地址
                businessData.add(customerDto.getCusDeliveryAdd());
                //是否启用信用管理
                String cus_credit_flag = customerDto.getCusCreditFlag();
                initDictionaryStaticData(businessData, cus_credit_flag, CommonEnum.DictionaryStaticData.NO_YES);
                //信用额度
                BigDecimal cus_credit_quota = customerDto.getCusCreditQuota();
                businessData.add(cus_credit_quota == null ? "" : cus_credit_quota.stripTrailingZeros().toString());
                //信用检查点
                String cus_credit_check = customerDto.getCusCreditCheck();
                initDictionaryStaticData(businessData, cus_credit_check, CommonEnum.DictionaryStaticData.CUS_CREDIT_CHECK);
                //银行代码
                businessData.add(customerDto.getCusBankCode());
                //银行名称
                businessData.add(customerDto.getCusBankName());
                //账户持有人
                businessData.add(customerDto.getCusAccountPerson());
                //银行账号
                businessData.add(customerDto.getCusBankAccount());
                //支付方式
                String cus_pay_mode = customerDto.getCusPayMode();
                initDictionaryStaticData(businessData, cus_pay_mode, CommonEnum.DictionaryStaticData.SUP_PAY_MODE);
                //铺底授信额度
                businessData.add(customerDto.getCusCreditAmt());
                businessList.add(businessData);
            }
        }
    }

    /**
     * 查询数据静态字典数据处理
     * @param list
     * @param value
     */
    private void initDictionaryStaticData(List<String> list, String value, CommonEnum.DictionaryStaticData dictionaryStaticData) {
        if (StringUtils.isBlank(value)) {
            list.add("");
        } else {
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, value);
            if (dictionary != null) {
                list.add(dictionary.getLabel());
            } else {
                list.add("");
            }
        }
    }

    /**
     * 业务数据表头
     */
    private String[] businessHead = {
            "加盟商",
            "地点",
            "客户自编码",
            "客户编码",
            "匹配状态",
            "助记码",
            "客户名称",
            "统一社会信用代码",
            "营业期限",
            "客户分类",
            "法人",
            "注册地址",
            "许可证编号",
            "发证日期",
            "有效期至",
            "生产或经营范围",
            "客户状态",
            "禁止销售",
            "禁止退货",
            "付款条件",
            "业务联系人",
            "联系人电话",
            "收货地址",
            "是否启用信用管理",
            "信用额度",
            "信用检查点",
            "银行代码",
            "银行名称",
            "账户持有人",
            "银行账号",
            "支付方式",
            "铺底授信额度",
    };

    /**
     * 基础数据头
     */
    private String[] basicHead = {
            "客户编码",
            "助记码",
            "客户名称",
            "统一社会信用代码",
            "营业期限",
            "客户分类",
            "法人",
            "注册地址",
            "客户状态",
            "许可证编号",
            "发证日期",
            "有效期至",
            "生产或经营范围"
    };

//客户	3	数据库	1	客户编码	a
//		加盟商	2	客户名称	b
//				客户分类	c
//				客户状态	d
//				加盟商	e
//				地点	f
//				客户自编码	g
//				客户状态	h
}
