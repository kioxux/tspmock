package com.gov.purchase.module.base.dto.feign;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel(value = "物料凭证请求参数")
public class MaterialDocRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client")
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "主键ID", name = "guid")
    @NotBlank(message = "主键ID不能为空")
    private String guid;

    @ApiModelProperty(value = "序号", name = "guidNo")
    @NotBlank(message = "序号不能为空")
    private String guidNo;

    @ApiModelProperty(value = "业务类型", name = "matType")
    @NotBlank(message = "业务类型不能为空")
    private String matType;

    @ApiModelProperty(value = "过账日期", name = "matPostType")
    @NotBlank(message = "过账日期不能为空")
    private String matPostDate;

    @ApiModelProperty(value = "抬头备注", name = "matHeadRemark")
    private String matHeadRemark;

    @ApiModelProperty(value = "商品自编码", name = "matProCode")
    @NotBlank(message = "商品自编码不能为空")
    private String matProCode;

    @ApiModelProperty(value = "配送门店", name = "matStoCode")
    private String matStoCode;

    @ApiModelProperty(value = "地点", name = "matSiteCode")
    @NotBlank(message = "地点不能为空")
    private String matSiteCode;

    @ApiModelProperty(value = "库存地点（状态）", name = "matLocationCode")
    @NotBlank(message = "库存地点不能为空")
    private String matLocationCode;

    @ApiModelProperty(value = "转移库存地点（状态）", name = "matLocationTo")
    private String matLocationTo;

    @ApiModelProperty(value = "批次", name = "matBatch")
    private String matBatch;

    @ApiModelProperty(value = "交易数量", name = "matQty")
    @NotNull(message = "交易数量不能为空")
//    @DecimalMin(value = "0.0", message = "交易数量不能为负数")
    @DecimalMax(value = "999999999.9999", message = "交易数量格式不正确")
    private BigDecimal matQty;

    @ApiModelProperty(value = "交易金额", name = "matAmt")
    private BigDecimal matAmt;

    @ApiModelProperty(value = "交易价格", name = "matPrice")
    private BigDecimal matPrice;

    @ApiModelProperty(value = "基本计量单位", name = "matUnit")
    @NotBlank(message = "基本计量单位不能为空")
    private String matUnit;

    @ApiModelProperty(value = "借/贷标识", name = "matDebitCredit")
    @NotBlank(message = "借/贷标识不能为空")
    private String matDebitCredit;

    @ApiModelProperty(value = "订单号", name = "matPoId")
    private String matPoId;

    @ApiModelProperty(value = "订单行号", name = "matPoLineno")
    private String matPoLineno;

    @ApiModelProperty(value = "业务单号", name = "matDnId")
    private String matDnId;

    @ApiModelProperty(value = "业务单行号", name = "matDnLineno")
    private String matDnLineno;

    @ApiModelProperty(value = "物料凭证行备注", name = "matLineRemark")
    private String matLineRemark;

    @ApiModelProperty(value = "创建人", name = "matCreateBy")
    @NotBlank(message = "创建人不能为空")
    private String matCreateBy;

    @ApiModelProperty(value = "创建日期", name = "matCreateDate")
    @NotBlank(message = "创建日期不能为空")
    private String matCreateDate;

    @ApiModelProperty(value = "创建时间", name = "matCreateTime")
    @NotBlank(message = "创建时间不能为空")
    private String matCreateTime;

    @ApiModelProperty(value = "互调价", name = "matHdPrice")
    private BigDecimal matHdPrice;
}
