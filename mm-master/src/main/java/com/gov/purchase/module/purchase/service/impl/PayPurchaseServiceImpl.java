package com.gov.purchase.module.purchase.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaPayOrder;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaFiInvoiceRegisterMapper;
import com.gov.purchase.mapper.GaiaPayOrderMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.PayPurchaseService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service
public class PayPurchaseServiceImpl implements PayPurchaseService {

    @Resource
    GaiaFiInvoiceRegisterMapper gaiaFiInvoiceRegisterMapper;

    @Resource
    GaiaPayOrderMapper gaiaPayOrderMapper;

    @Resource
    FeignService feignService;

    /**
     * 发票登记查询
     *
     * @param dto InvoiceRegisterListRequestDto
     * @return PageInfo<InvoiceRegisterListResponseDto>
     */
    public PageInfo<InvoiceRegisterListResponseDto> selectInvoiceRegisterList(InvoiceRegisterListRequestDto dto) {
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        //发票登记查询
        List<InvoiceRegisterListResponseDto> InvoiceRegisterList = gaiaFiInvoiceRegisterMapper.selectInvoiceRegisterList(dto);
        PageInfo<InvoiceRegisterListResponseDto> pageInfo = new PageInfo<>(InvoiceRegisterList);
        return pageInfo;
    }

    /**
     * 发票列表查询
     *
     * @param dto InvoiceListRequestDto
     * @return PageInfo<InvoiceListResponseDto>
     */
    public PageInfo<InvoiceListResponseDto> selectInvoiceList(InvoiceListRequestDto dto) {

        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());

        //发票列表查询
        List<InvoiceListResponseDto> InvoiceList = gaiaFiInvoiceRegisterMapper.selectInvoiceList(dto);

        //本次付款金额不能大于未付款金额
        for (InvoiceListResponseDto invoiceListResponseDto : InvoiceList) {
            invoiceListResponseDto.setPaymentAmt(invoiceListResponseDto.getUnpaidAmt());
        }

        PageInfo<InvoiceListResponseDto> pageInfo = new PageInfo<>(InvoiceList);

        return pageInfo;
    }

    /**
     * 发票明细查询
     *
     * @param dto InvoiceDetailsRequestDto
     * @return InvoiceDetailsResponseDto
     */
    public InvoiceDetailsResponseDto selectInvoiceDetails(InvoiceDetailsRequestDto dto) {
        //发票号码
        if (dto.getInvoiceNum() == null || dto.getInvoiceNum().length == 0) {
            throw new CustomResultException(ResultEnum.E0019);
        }
        //发票列表查询
        List<InvoiceDetailsDto> invoiceDetailsList = gaiaFiInvoiceRegisterMapper.selectInvoiceDetails(dto);
        InvoiceDetailsResponseDto invoiceDetails = new InvoiceDetailsResponseDto();
        // 付款金额
        Map<String, BigDecimal> paymentAmountMap = dto.getPaymentAmount();
        if (null != invoiceDetailsList && invoiceDetailsList.size() > 0) {
            // 付款金额
            if (paymentAmountMap != null && !paymentAmountMap.isEmpty()) {
                // 根据发标循环
                for (String key : paymentAmountMap.keySet()) {
                    // 当前发票付款总金额
                    BigDecimal paymentAmount = paymentAmountMap.get(key);
                    // 异常数据不作处理
                    if (paymentAmount == null || paymentAmount.compareTo(BigDecimal.ZERO) <= 0) {
                        continue;
                    }
                    for (InvoiceDetailsDto invoiceDetailsDto : invoiceDetailsList) {
                        // 相同发票数据做计算
                        if (!key.equals(invoiceDetailsDto.getInvoiceNum())) {
                            continue;
                        }
                        // 本次付款金额
                        BigDecimal paymentAmt = invoiceDetailsDto.getPaymentAmt();
                        if (paymentAmt == null || paymentAmt.compareTo(BigDecimal.ZERO) <= 0) {
                            continue;
                        }
                        // 付款总金额大于0
                        if (paymentAmount.compareTo(BigDecimal.ZERO) > 0) {
                            invoiceDetailsDto.setSelectFlg(true);
                            if (paymentAmount.compareTo(paymentAmt) >= 0) {
                                paymentAmount = paymentAmount.subtract(paymentAmt);
                            } else {
                                invoiceDetailsDto.setPaymentAmt(paymentAmount);
                                paymentAmount = BigDecimal.ZERO;
                            }
                            // 剩余付款金额
                            invoiceDetailsDto.setLastPayableAmt(invoiceDetailsDto.getUnpaidAmt().subtract(invoiceDetailsDto.getPaymentAmt()));
                        }
                        if (paymentAmount.compareTo(BigDecimal.ZERO) <= 0) {
                            break;
                        }
                    }
                }
            }
            invoiceDetails.setInvoiceDetailsDto(invoiceDetailsList);
            invoiceDetails.setPaymentDate(invoiceDetailsList.get(0).getPaymentDate());
            invoiceDetails.setSupBankAccount(invoiceDetailsList.get(0).getSupBankAccount());
        }
        return invoiceDetails;
    }

    /**
     * 付款申请保存
     *
     * @param dto PaymentReqSaveRequestDto
     * @return String
     */
    public String insertPaymentReqSave(List<PaymentReqSaveRequestDto> dto) {
        TokenUser user = feignService.getLoginInfo();
        int index = 0;
        String payOrderId = "";
        for (PaymentReqSaveRequestDto paymentReqSaveRequestDto : dto) {
            //付款申请单号
            if (StringUtils.isBlank(payOrderId)) {
                //付款申请保存
                GaiaPayOrder payOrderNo = gaiaPayOrderMapper.selectPayOrderNo(paymentReqSaveRequestDto);
                payOrderId = payOrderNo.getPayOrderId();
            }
            GaiaPayOrder gaiaPayOrder = new GaiaPayOrder();
            BeanUtils.copyProperties(paymentReqSaveRequestDto, gaiaPayOrder);
            gaiaPayOrder.setPayOrderId(payOrderId);
            //付款申请单行号
            gaiaPayOrder.setPayOrderLineno(String.valueOf(index + 1));
            gaiaPayOrder.setPayOrderStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
            gaiaPayOrder.setPayInvoiceDoc(paymentReqSaveRequestDto.getInvoiceNum());
            gaiaPayOrder.setPayLineNo(paymentReqSaveRequestDto.getIvLineNo());
            gaiaPayOrder.setPayLineAmt(paymentReqSaveRequestDto.getPaymentAmt());
            gaiaPayOrder.setPayCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            gaiaPayOrder.setPayCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            gaiaPayOrder.setPayCreateUser(user.getUserId());
            gaiaPayOrderMapper.insertSelective(gaiaPayOrder);
            index++;
        }
        return payOrderId;
    }

    /**
     * 付款单查询
     *
     * @param dto PayOrderListRequestDto
     * @return PageInfo<PayOrderListResponseDto>
     */
    public PageInfo<PayOrderListResponseDto> selectPayOrderList(PayOrderListRequestDto dto) {

        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());

        //付款单查询
        List<PayOrderListResponseDto> payOrderList = gaiaPayOrderMapper.selectPayOrderList(dto);

        PageInfo<PayOrderListResponseDto> pageInfo = new PageInfo<>(payOrderList);

        return pageInfo;
    }

    /**
     * 付款单明细
     *
     * @param dto PayOrderDetailsRequestDto
     * @return List<PayOrderDetailsResponseDto>
     */
    public List<PayOrderDetailsResponseDto> selectPayOrderDetails(PayOrderDetailsRequestDto dto) {

        //付款单明细
        List<PayOrderDetailsResponseDto> payOrderDetails = gaiaFiInvoiceRegisterMapper.selectPayOrderDetails(dto);

        return payOrderDetails;
    }

    /**
     * 付款单发送审批
     * @param payOrderId 付款单号
     * @return
     */
    @Override
    public Result sendApproval(String payOrderId) {
        if(StringUtils.isBlank(payOrderId)){
            throw new CustomResultException(ResultEnum.PAY_ORDER_ID_EMPTY);
        }
        PayOrderApprovalDto payOrderApprovalDto = gaiaPayOrderMapper.selectListByOrderId(payOrderId);
        if(payOrderApprovalDto == null){
            throw new CustomResultException(ResultEnum.PAY_ORDER_ID_NOT_EXIST);
        }
        FeignResult result = feignService.createPayOrderWorkflow(payOrderApprovalDto);
        if (result.getCode() != 0) {
            throw new CustomResultException(result.getMessage());
        }

        return ResultUtil.success();
    }
}
