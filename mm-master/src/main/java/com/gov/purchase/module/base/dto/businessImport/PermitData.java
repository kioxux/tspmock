package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class PermitData {

    /**
     * 证照编码
     */
    @ExcelValidate(index = 0, name = "证照编码", type = ExcelValidate.DataType.STRING, maxLength = 3)
    private String perCode;

    /**
     * 证照名称
     */
    @ExcelValidate(index = 1, name = "证照名称", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String perName;

    /**
     * 证照号
     */
    @ExcelValidate(index = 2, name = "证照号", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String perId;

    /**
     * 有效期从
     */
    @ExcelValidate(index = 3, name = "有效期从", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd")
    private String perDateStart;

    /**
     * 有效期至
     */
    @ExcelValidate(index = 4, name = "有效期至", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd")
    private String perDateEnd;

    /**
     * 证照停用
     */
    @ExcelValidate(index = 5, name = "证照停用", type = ExcelValidate.DataType.STRING, maxLength = 1,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "perStopFlagValue")
    private String perStopFlag;

    private String perStopFlagValue;
}

