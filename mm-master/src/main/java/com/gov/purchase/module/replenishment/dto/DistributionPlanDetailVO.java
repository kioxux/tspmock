package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/10 10:07
 **/
@Data
public class DistributionPlanDetailVO {
    @ApiModelProperty(value = "序号", example = "1")
    private Integer index;
    @ApiModelProperty(value = "加盟商", example = "1001")
    private String client;
    @ApiModelProperty(value = "新品铺货计划单号", example = "XPJ20320001")
    private String planCode;
    @ApiModelProperty(value = "新品计划时间", example = "2021/06/01")
    private String planTime;
    @ApiModelProperty(value = "完成时间", example = "2021/06/30")
    private String finishTime;
    @ApiModelProperty(value = "商品编码", example = "10472351")
    private String proSelfCode;
    @ApiModelProperty(value = "商品描述", example = "黄狮颗粒")
    private String proSelfName;
    @ApiModelProperty(value = "商品规格", example = "8G*30袋")
    private String proSpec;
    @ApiModelProperty(value = "生产厂家", example = "富康药业")
    private String proFactoryName;
    @ApiModelProperty(value = "单位", example = "盒")
    private String proUnit;
    @ApiModelProperty(value = "门店ID", example = "1000002")
    private String storeId;
    @ApiModelProperty(value = "门店名称", example = "白龙路店")
    private String storeName;
    @ApiModelProperty(value = "店型", example = "社区店")
    private String storeType;
    @ApiModelProperty(value = "门店属性", example = "连锁")
    private String storeProperty;
    @ApiModelProperty(value = "计划铺货数量", example = "10")
    private BigDecimal planQuantity;
    @ApiModelProperty(value = "实际铺货数量", example = "5")
    private BigDecimal actualQuantity;
}
