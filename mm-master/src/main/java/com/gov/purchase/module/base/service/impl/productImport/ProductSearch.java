package com.gov.purchase.module.base.service.impl.productImport;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.BatSearchValBasic;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.ProductDto;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.18
 */
@Service
public class ProductSearch {

    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;

    @Resource
    GaiaProductClassMapper gaiaProductClassMapper;

    @Resource
    GaiaProductComponentMapper gaiaProductComponentMapper;

    @Resource
    GaiaProductFactoryMapper gaiaProductFactoryMapper;

    @Resource
    GaiaTaxCodeMapper gaiaTaxCodeMapper;

    @Resource
    GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Resource
    CosUtils cosUtils;

    /**
     * 商品查询（通用）
     *
     * @param batSearchValBasic
     * @return
     */
    public Result selectProductBasicBatch(BatSearchValBasic batSearchValBasic) {
        // 查询条件
        Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
        initProductBasicWhere(map);
        PageHelper.startPage(batSearchValBasic.getPageNum(), batSearchValBasic.getPageSize());
        List<ProductDto> batchList = gaiaProductBasicMapper.selectBasicBatch(map);
        PageInfo<ProductDto> pageInfo = new PageInfo<>(batchList);

        return ResultUtil.success(pageInfo);
    }

    /**
     * 商品查询（通用+业务）
     *
     * @param batSearchValBasic
     * @return
     */
    public Result selectProductBusinessBatch(BatSearchValBasic batSearchValBasic) {
        // 查询条件
        Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
        initProductBusinessWhere(map);
        PageHelper.startPage(batSearchValBasic.getPageNum(), batSearchValBasic.getPageSize());
        List<ProductDto> batchList = gaiaProductBasicMapper.selectBusinessBatch(map);
        PageInfo<ProductDto> pageInfo = new PageInfo<>(batchList);

        return ResultUtil.success(pageInfo);
    }

    /**
     * 加载查询条件
     *
     * @param map
     */
    private void initProductBasicWhere(Map<String, SearchValBasic> map) {
        // 查询条件为空
        if (map == null || map.isEmpty()) {
            return;
        }
        // 批准文号分类
        SearchValBasic pro_register_class = map.get("d");
        if (pro_register_class != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_register_class.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_REGISTER_CLASS, pro_register_class.getSearchVal()));
                list.add(pro_register_class.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_register_class.getMoreVal())) {
                for (String item : pro_register_class.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_REGISTER_CLASS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_register_class.setSearchVal(null);
            }
            pro_register_class.setMoreVal(list);
        }

        // 商品分类
        SearchValBasic pro_class = map.get("e");
        if (pro_class != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_class.getSearchVal()) || CollectionUtils.isNotEmpty(pro_class.getMoreVal())) {
                list.addAll(gaiaProductClassMapper.getProClass(pro_class));
                if (StringUtils.isNotBlank(pro_class.getSearchVal())) {
                    list.add(pro_class.getSearchVal());
                } else {
                    list.addAll(pro_class.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_class.setSearchVal(null);
            }
            pro_class.setMoreVal(list);
        }

        // 成分分类
        SearchValBasic pro_compclass = map.get("f");
        if (pro_compclass != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_compclass.getSearchVal()) || CollectionUtils.isNotEmpty(pro_compclass.getMoreVal())) {
                list.addAll(gaiaProductComponentMapper.getProComponent(pro_compclass));
                if (StringUtils.isNotBlank(pro_compclass.getSearchVal())) {
                    list.add(pro_compclass.getSearchVal());
                } else {
                    list.addAll(pro_compclass.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_compclass.setSearchVal(null);
            }
            pro_compclass.setMoreVal(list);
        }

        // 处方类别
        SearchValBasic pro_presclass = map.get("g");
        if (pro_presclass != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_presclass.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_PRESCLASS, pro_presclass.getSearchVal()));
                list.add(pro_presclass.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_presclass.getMoreVal())) {
                for (String item : pro_presclass.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_PRESCLASS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_presclass.setSearchVal(null);
            }
            pro_presclass.setMoreVal(list);
        }

        // 生产企业
        SearchValBasic pro_factory = map.get("f");
        if (pro_factory != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_factory.getSearchVal()) || CollectionUtils.isNotEmpty(pro_factory.getMoreVal())) {
                list.addAll(gaiaProductFactoryMapper.getProFactory(pro_factory));
                if (StringUtils.isNotBlank(pro_factory.getSearchVal())) {
                    list.add(pro_factory.getSearchVal());
                } else {
                    list.addAll(pro_factory.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_factory.setSearchVal(null);
            }
            pro_factory.setMoreVal(list);
        }

        // 品牌区分
        SearchValBasic pro_brand_class = map.get("k");
        if (pro_brand_class != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_brand_class.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_BRAND_CLASS, pro_brand_class.getSearchVal()));
                list.add(pro_brand_class.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_brand_class.getMoreVal())) {
                for (String item : pro_brand_class.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_BRAND_CLASS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_brand_class.setSearchVal(null);
            }
            pro_brand_class.setMoreVal(list);
        }

        // 进项税率
        SearchValBasic pro_input_tax = map.get("l");
        if (pro_input_tax != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_input_tax.getSearchVal()) || CollectionUtils.isNotEmpty(pro_input_tax.getMoreVal())) {
                list.addAll(gaiaTaxCodeMapper.getProInputTax(pro_input_tax));
                if (StringUtils.isNotBlank(pro_input_tax.getSearchVal())) {
                    list.add(pro_input_tax.getSearchVal());
                } else {
                    list.addAll(pro_input_tax.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_input_tax.setSearchVal(null);
            }
            pro_input_tax.setMoreVal(list);
        }

        // 销项税率
        SearchValBasic pro_output_tax = map.get("m");
        if (pro_output_tax != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_output_tax.getSearchVal()) || CollectionUtils.isNotEmpty(pro_output_tax.getMoreVal())) {
                list.addAll(gaiaTaxCodeMapper.getProInputTax(pro_output_tax));
                if (StringUtils.isNotBlank(pro_output_tax.getSearchVal())) {
                    list.add(pro_output_tax.getSearchVal());
                } else {
                    list.addAll(pro_output_tax.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_output_tax.setSearchVal(null);
            }
            pro_output_tax.setMoreVal(list);
        }

        // 管制特殊分类
        SearchValBasic pro_control_class = map.get("n");
        if (pro_control_class != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_control_class.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_CONTROL_CLASS, pro_control_class.getSearchVal()));
                list.add(pro_control_class.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_control_class.getMoreVal())) {
                for (String item : pro_control_class.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_CONTROL_CLASS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_control_class.setSearchVal(null);
            }
            pro_control_class.setMoreVal(list);
        }

        // 生产类别
        SearchValBasic pro_produce_class = map.get("o");
        if (pro_produce_class != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_produce_class.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_PRODUCE_CLASS, pro_produce_class.getSearchVal()));
                list.add(pro_produce_class.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_produce_class.getMoreVal())) {
                for (String item : pro_produce_class.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_PRODUCE_CLASS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_produce_class.setSearchVal(null);
            }
            pro_produce_class.setMoreVal(list);
        }

        // 贮存条件
        SearchValBasic pro_storage_condition = map.get("p");
        if (pro_storage_condition != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_storage_condition.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_STORAGE_CONDITION, pro_storage_condition.getSearchVal()));
                list.add(pro_storage_condition.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_storage_condition.getMoreVal())) {
                for (String item : pro_storage_condition.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_STORAGE_CONDITION, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_storage_condition.setSearchVal(null);
            }
            pro_storage_condition.setMoreVal(list);
        }

        // 商品仓储分区
        SearchValBasic pro_storage_area = map.get("q");
        if (pro_storage_area != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_storage_area.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA, pro_storage_area.getSearchVal()));
                list.add(pro_storage_area.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_storage_area.getMoreVal())) {
                for (String item : pro_storage_area.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_storage_area.setSearchVal(null);
            }
            pro_storage_area.setMoreVal(list);
        }

        // 国家医保品种
        SearchValBasic pro_med_prodct = map.get("r");
        if (pro_med_prodct != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_med_prodct.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.NO_YES, pro_med_prodct.getSearchVal()));
                list.add(pro_med_prodct.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_med_prodct.getMoreVal())) {
                for (String item : pro_med_prodct.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.NO_YES, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_med_prodct.setSearchVal(null);
            }
            pro_med_prodct.setMoreVal(list);
        }

        // 状态
        SearchValBasic pro_status = map.get("s");
        if (pro_status != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_status.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_STATUS, pro_status.getSearchVal()));
                list.add(pro_status.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_status.getMoreVal())) {
                for (String item : pro_status.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_STATUS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_status.setSearchVal(null);
            }
            pro_status.setMoreVal(list);
        }
    }

    /**
     * 加载查询条件
     *
     * @param map
     */
    private void initProductBusinessWhere(Map<String, SearchValBasic> map) {
        // 查询条件为空
        if (map == null || map.isEmpty()) {
            return;
        }

        initProductBasicWhere(map);

        // 加盟商
        SearchValBasic client = map.get("t");
        if (client != null) {

            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(client.getSearchVal()) || CollectionUtils.isNotEmpty(client.getMoreVal())) {
                list.addAll(gaiaFranchiseeMapper.getFrancName(client));
                if (StringUtils.isNotBlank(client.getSearchVal())) {
                    list.add(client.getSearchVal());
                } else {
                    list.addAll(client.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                client.setSearchVal(null);
            }
            client.setMoreVal(list);
        }

        // 商品状态
        SearchValBasic pro_status = map.get("w");
        if (pro_status != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_status.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_STATUS, pro_status.getSearchVal()));
                list.add(pro_status.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_status.getMoreVal())) {
                for (String item : pro_status.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.PRO_STATUS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_status.setSearchVal(null);
            }
            pro_status.setMoreVal(list);
        }

        // 是否拆零
        SearchValBasic pro_ifpart = map.get("y");
        if (pro_ifpart != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_ifpart.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.NO_YES, pro_ifpart.getSearchVal()));
                list.add(pro_ifpart.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_ifpart.getMoreVal())) {
                for (String item : pro_ifpart.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.NO_YES, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_ifpart.setSearchVal(null);
            }
            pro_ifpart.setMoreVal(list);
        }

        // 是否医保
        SearchValBasic pro_if_med = map.get("z");
        if (pro_if_med != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(pro_if_med.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.NO_YES, pro_if_med.getSearchVal()));
                list.add(pro_if_med.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(pro_if_med.getMoreVal())) {
                for (String item : pro_if_med.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.NO_YES, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                pro_if_med.setSearchVal(null);
            }
            pro_if_med.setMoreVal(list);
        }
    }

    /**
     * excel导出
     *
     * @param batSearchValBasic
     * @return
     */
    public Result exportProductBasicBatch(BatSearchValBasic batSearchValBasic) {
        try {
            // 查询条件
            Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
            initProductBasicWhere(map);
            List<ProductDto> batchList = gaiaProductBasicMapper.selectBasicBatch(map);
            // 导出数据
            List<List<String>> basicListc = new ArrayList<>();
            initData(batchList, basicListc, null);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(basicHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(basicListc);
                    }},
                    new ArrayList<String>() {{
                        add("商品主数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 商品导出
     *
     * @param batSearchValBasic
     * @return
     */
    public Result exportProductBusinessBatch(BatSearchValBasic batSearchValBasic) {
        try {
            // 查询条件
            Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
            initProductBusinessWhere(map);
            List<ProductDto> batchList = gaiaProductBasicMapper.selectBusinessBatch(map);
            // 导出数据
            List<List<String>> businessList = new ArrayList<>();
            initData(batchList, null, businessList);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(businessHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(businessList);
                    }},
                    new ArrayList<String>() {{
                        add("商品业务数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param basicListc
     * @param businessList
     */
    private void initData(List<ProductDto> batchList, List<List<String>> basicListc, List<List<String>> businessList) {
        for (ProductDto productDto : batchList) {
            if (basicListc != null) {
                List<String> basicData = new ArrayList<>();
                //商品编码
                basicData.add(productDto.getProCode());
                //通用名称
                basicData.add(productDto.getProCommonname());
                //商品描述
                basicData.add(productDto.getProDepict());
                //助记码
                basicData.add(productDto.getProPym());
                //商品名
                basicData.add(productDto.getProName());
                //规格
                basicData.add(productDto.getProSpecs());
                //计量单位
                basicData.add(productDto.getUnitName());
                //剂型
                basicData.add(productDto.getProForm());
                //细分剂型
                basicData.add(productDto.getProPartform());
                //最小剂量（以mg/ml计算）
                basicData.add(productDto.getProMindose());
                //总剂量（以mg/ml计算）
                basicData.add(productDto.getProTotaldose());
                //国际条形码1
                basicData.add(productDto.getProBarcode());
                //国际条形码2
                basicData.add(productDto.getProBarcode2());
                //批准文号分类
                String pro_register_class = productDto.getProRegisterClass();
                initDictionaryStaticData(basicData, pro_register_class, CommonEnum.DictionaryStaticData.PRO_REGISTER_CLASS);
                //批准文号
                basicData.add(productDto.getProRegisterNo());
                //批准文号批准日期
                basicData.add(productDto.getProRegisterDate());
                //批准文号失效日期
                basicData.add(productDto.getProRegisterExdate());
                //商品分类
                basicData.add(productDto.getProClass());
                //商品分类描述
                basicData.add(productDto.getProClassName());
                //成分分类
                basicData.add(productDto.getProCompclass());
                //成分分类描述
                basicData.add(productDto.getProCompclassName());
                //处方类别
                String pro_presclass = productDto.getProPresclass();
                initDictionaryStaticData(basicData, pro_presclass, CommonEnum.DictionaryStaticData.PRO_PRESCLASS);
                //生产企业代码
                basicData.add(productDto.getProFactoryCode());
                //生产企业
                basicData.add(productDto.getProFactoryName());
                //商标
                basicData.add(productDto.getProMark());
                //品牌标识名
                basicData.add(productDto.getProBrand());
                //品牌区分
                String pro_brand_class = productDto.getProBrandClass();
                initDictionaryStaticData(basicData, pro_brand_class, CommonEnum.DictionaryStaticData.PRO_BRAND_CLASS);
                //保质期
                basicData.add(productDto.getProLife());
                //保质期单位
                String pro_life_unit = productDto.getProLifeUnit();
                initDictionaryStaticData(basicData, pro_life_unit, CommonEnum.DictionaryStaticData.PRO_LIFE_UNIT);
                //上市许可持有人
                basicData.add(productDto.getProHolder());
                //进项税率
                basicData.add(productDto.getInputTax());
                //销项税率
                basicData.add(productDto.getOutputTax());
                //药品本位码
                basicData.add(productDto.getProBasicCode());
                //税务分类编码
                basicData.add(productDto.getProTaxClass());
                //管制特殊分类
                String pro_control_class = productDto.getProControlClass();
                initDictionaryStaticData(basicData, pro_control_class, CommonEnum.DictionaryStaticData.PRO_CONTROL_CLASS);
                //生产类别
                String pro_produce_class = productDto.getProProduceClass();
                initDictionaryStaticData(basicData, pro_produce_class, CommonEnum.DictionaryStaticData.PRO_PRODUCE_CLASS);
                //贮存条件
                String pro_storage_condition = productDto.getProStorageCondition();
                initDictionaryStaticData(basicData, pro_storage_condition, CommonEnum.DictionaryStaticData.PRO_STORAGE_CONDITION);
                //商品仓储分区
                String pro_storage_area = productDto.getProStorageArea();
                initDictionaryStaticData(basicData, pro_storage_area, CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA);
                //长（以MM计算）
                basicData.add(productDto.getProLong());
                //宽（以MM计算）
                basicData.add(productDto.getProWide());
                //高（以MM计算）
                basicData.add(productDto.getProHigh());
                //中包装量
                basicData.add(productDto.getProMidPackage());
                //大包装量
                basicData.add(productDto.getProBigPackage());
                //启用电子监管码
                String pro_electronic_code = productDto.getProElectronicCode();
                initDictionaryStaticData(basicData, pro_electronic_code, CommonEnum.DictionaryStaticData.NO_YES);
                //生产经营许可证号
                basicData.add(productDto.getProQsCode());
                //最大销售量
                basicData.add(productDto.getProMaxSales());
                //说明书代码
                basicData.add(productDto.getProInstructionCode());
                //说明书内容
                basicData.add(productDto.getProInstruction());
                //国家医保品种
                String pro_med_prodct = productDto.getProMedProdct();
                initDictionaryStaticData(basicData, pro_med_prodct, CommonEnum.DictionaryStaticData.NO_YES);
                //国家医保品种编码
                basicData.add(productDto.getProMedProdctcode());
                //生产国家
                basicData.add(productDto.getProCountry());
                //产地
                basicData.add(productDto.getProPlace());
                //状态
                String pro_status = productDto.getProStatus();
                initDictionaryStaticData(basicData, pro_status, CommonEnum.DictionaryStaticData.PRO_STATUS);
                //可服用天数
                basicData.add(productDto.getProTakeDays());
                //用法用量
                basicData.add(productDto.getProUsage());
                //禁忌说明
                basicData.add(productDto.getProContraindication());
                basicListc.add(basicData);
            }
            if (businessList != null) {
                List<String> businessData = new ArrayList<>();
                //加盟商
                businessData.add(productDto.getClient());
                //地点
                businessData.add(productDto.getSiteName());
                //商品自编码
                businessData.add(productDto.getProSelfCode());
                //商品编码
                businessData.add(productDto.getProCode());
                // 匹配状态
                String pro_match_status = productDto.getProMatchStatus();
                initDictionaryStaticData(businessData, pro_match_status, CommonEnum.DictionaryStaticData.PRO_MATCH_STATUS);
                //通用名称
                businessData.add(productDto.getProCommonname());
                //商品描述
                businessData.add(productDto.getProDepict());
                //助记码
                businessData.add(productDto.getProPym());
                //商品名
                businessData.add(productDto.getProName());
                //规格
                businessData.add(productDto.getProSpecs());
                //计量单位
                businessData.add(productDto.getUnitName());
                //剂型
                businessData.add(productDto.getProForm());
                //细分剂型
                businessData.add(productDto.getProPartform());
                //最小剂量（以mg/ml计算）
                businessData.add(productDto.getProMindose());
                //总剂量（以mg/ml计算）
                businessData.add(productDto.getProTotaldose());
                //国际条形码1
                businessData.add(productDto.getProBarcode());
                //国际条形码2
                businessData.add(productDto.getProBarcode2());
                //批准文号分类
                String pro_register_class = productDto.getProRegisterClass();
                initDictionaryStaticData(businessData, pro_register_class, CommonEnum.DictionaryStaticData.PRO_REGISTER_CLASS);
                //批准文号
                businessData.add(productDto.getProRegisterNo());
                //批准文号批准日期
                businessData.add(productDto.getProRegisterDate());
                //批准文号失效日期
                businessData.add(productDto.getProRegisterExdate());
                //商品分类
                businessData.add(productDto.getProClass());
                //商品分类描述
                businessData.add(productDto.getProClassName());
                //成分分类
                businessData.add(productDto.getProCompclass());
                //成分分类描述
                businessData.add(productDto.getProCompclassName());
                //处方类别
                String pro_presclass = productDto.getProPresclass();
                initDictionaryStaticData(businessData, pro_presclass, CommonEnum.DictionaryStaticData.PRO_PRESCLASS);
                //生产企业代码
                businessData.add(productDto.getProFactoryCode());
                //生产企业
                businessData.add(productDto.getProFactoryName());
                //商标
                businessData.add(productDto.getProMark());
                //品牌标识名
                businessData.add(productDto.getProBrand());
                //品牌区分
                String pro_brand_class = productDto.getProBrandClass();
                initDictionaryStaticData(businessData, pro_brand_class, CommonEnum.DictionaryStaticData.PRO_BRAND_CLASS);
                //保质期
                businessData.add(productDto.getProLife());
                //保质期单位
                String pro_life_unit = productDto.getProLifeUnit();
                initDictionaryStaticData(businessData, pro_life_unit, CommonEnum.DictionaryStaticData.PRO_LIFE_UNIT);
                //上市许可持有人
                businessData.add(productDto.getProHolder());
                //进项税率
                businessData.add(productDto.getInputTax());
                //销项税率
                businessData.add(productDto.getOutputTax());
                //药品本位码
                businessData.add(productDto.getProBasicCode());
                //税务分类编码
                businessData.add(productDto.getProTaxClass());
                //管制特殊分类
                String pro_control_class = productDto.getProControlClass();
                initDictionaryStaticData(businessData, pro_control_class, CommonEnum.DictionaryStaticData.PRO_CONTROL_CLASS);
                //生产类别
                String pro_produce_class = productDto.getProProduceClass();
                initDictionaryStaticData(businessData, pro_produce_class, CommonEnum.DictionaryStaticData.PRO_PRODUCE_CLASS);
                //贮存条件
                String pro_storage_condition = productDto.getProStorageCondition();
                initDictionaryStaticData(businessData, pro_storage_condition, CommonEnum.DictionaryStaticData.PRO_STORAGE_CONDITION);
                //商品仓储分区
                String pro_storage_area = productDto.getProStorageArea();
                initDictionaryStaticData(businessData, pro_storage_area, CommonEnum.DictionaryStaticData.PRO_STORAGE_AREA);
                //长（以MM计算）
                businessData.add(productDto.getProLong());
                //宽（以MM计算）
                businessData.add(productDto.getProWide());
                //高（以MM计算）
                businessData.add(productDto.getProHigh());
                //中包装量
                businessData.add(productDto.getProMidPackage());
                //大包装量
                businessData.add(productDto.getProBigPackage());
                //启用电子监管码
                String pro_electronic_code = productDto.getProElectronicCode();
                initDictionaryStaticData(businessData, pro_electronic_code, CommonEnum.DictionaryStaticData.NO_YES);
                //生产经营许可证号
                businessData.add(productDto.getProQsCode());
                //最大销售量
                businessData.add(productDto.getProMaxSales());
                //说明书代码
                businessData.add(productDto.getProInstructionCode());
                //说明书内容
                businessData.add(productDto.getProInstruction());
                //国家医保品种
                String pro_med_prodct = productDto.getProMedProdct();
                initDictionaryStaticData(businessData, pro_med_prodct, CommonEnum.DictionaryStaticData.NO_YES);
                //国家医保品种编码
                businessData.add(productDto.getProMedProdctcode());
                //生产国家
                businessData.add(productDto.getProCountry());
                //产地
                businessData.add(productDto.getProPlace());
                //可服用天数
                businessData.add(productDto.getProTakeDays());
                //用法用量
                businessData.add(productDto.getProUsage());
                //禁忌说明
                businessData.add(productDto.getProContraindication());
                //商品状态
                String pro_status = productDto.getProStatusBusiness();
                initDictionaryStaticData(businessData, pro_status, CommonEnum.DictionaryStaticData.PRO_STATUS);
                //商品定位
                businessData.add(productDto.getProPosition());
                //禁止销售
                String pro_no_retail = productDto.getProNoRetail();
                initDictionaryStaticData(businessData, pro_no_retail, CommonEnum.DictionaryStaticData.NO_YES);
                //禁止采购
                String pro_no_purchase = productDto.getProNoPurchase();
                initDictionaryStaticData(businessData, pro_no_purchase, CommonEnum.DictionaryStaticData.NO_YES);
                //禁止配送
                String pro_no_distributed = productDto.getProNoDistributed();
                initDictionaryStaticData(businessData, pro_no_distributed, CommonEnum.DictionaryStaticData.NO_YES);
                //禁止退厂
                String pro_no_supplier = productDto.getProNoSupplier();
                initDictionaryStaticData(businessData, pro_no_supplier, CommonEnum.DictionaryStaticData.NO_YES);
                //禁止退仓
                String pro_no_dc = productDto.getProNoDc();
                initDictionaryStaticData(businessData, pro_no_dc, CommonEnum.DictionaryStaticData.NO_YES);
                //禁止调剂
                String pro_no_adjust = productDto.getProNoAdjust();
                initDictionaryStaticData(businessData, pro_no_adjust, CommonEnum.DictionaryStaticData.NO_YES);
                //禁止批发
                String pro_no_sale = productDto.getProNoSale();
                initDictionaryStaticData(businessData, pro_no_sale, CommonEnum.DictionaryStaticData.NO_YES);
                //禁止请货
                String pro_no_apply = productDto.getProNoApply();
                initDictionaryStaticData(businessData, pro_no_apply, CommonEnum.DictionaryStaticData.NO_YES);
                //是否拆零
                String pro_ifpart = productDto.getProIfpart();
                initDictionaryStaticData(businessData, pro_ifpart, CommonEnum.DictionaryStaticData.NO_YES);
                //拆零单位
                businessData.add(productDto.getProPartUintName());
                //拆零比例
                businessData.add(productDto.getProPartRate());
                //采购单位
                businessData.add(productDto.getProPurchaseUnitName());
                //采购比例
                businessData.add(productDto.getProPurchaseRate());
                //销售单位
                businessData.add(productDto.getProSaleUnitName());
                //销售比例
                businessData.add(productDto.getProSaleRate());
                //最小订货量
                BigDecimal pro_min_qty = productDto.getProMinQty();
                businessData.add(pro_min_qty == null ? "" : pro_min_qty.stripTrailingZeros().toString());
                //是否医保
                String pro_if_med = productDto.getProIfMed();
                initDictionaryStaticData(businessData, pro_if_med, CommonEnum.DictionaryStaticData.NO_YES);
                //销售级别
                businessData.add(productDto.getProSlaeClass());
                //限购数量
                BigDecimal pro_limit_qty = productDto.getProLimitQty();
                businessData.add(pro_limit_qty == null ? "" : pro_limit_qty.stripTrailingZeros().toString());
                //中药规格
                businessData.add(productDto.getProTcmSpecs());
                //中药批准文号
                businessData.add(productDto.getProTcmRegisterNo());
                //中药生产企业代码
                businessData.add(productDto.getProTcmFactoryCode());
                //中药产地
                businessData.add(productDto.getProTcmPlace());
                //含麻最大配货量
                BigDecimal pro_max_qty = productDto.getProMaxQty();
                businessData.add(pro_max_qty == null ? "" : pro_max_qty.stripTrailingZeros().toString());
                //按中包装量倍数配货
                String pro_package_flag = productDto.getProPackageFlag();
                initDictionaryStaticData(businessData, pro_package_flag, CommonEnum.DictionaryStaticData.NO_YES);
                //是否重点养护
                String pro_key_care = productDto.getProKeyCare();
                initDictionaryStaticData(businessData, pro_key_care, CommonEnum.DictionaryStaticData.NO_YES);
                // 固定货位
                businessData.add(productDto.getProFixBin());
                businessList.add(businessData);
            }
        }
    }

    /**
     * 查询数据静态字典数据处理
     * @param list
     * @param value
     */
    private void initDictionaryStaticData(List<String> list, String value, CommonEnum.DictionaryStaticData dictionaryStaticData) {
        if (StringUtils.isBlank(value)) {
            list.add("");
        } else {
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, value);
            if (dictionary != null) {
                list.add(dictionary.getLabel());
            } else {
                list.add("");
            }
        }
    }

    /**
     * 业务数据表头
     */
    private String[] businessHead = {
            "加盟商",
            "地点",
            "商品自编码",
            "商品编码",
            "匹配状态",
            "通用名称",
            "商品描述",
            "助记码",
            "商品名",
            "规格",
            "计量单位",
            "剂型",
            "细分剂型",
            "最小剂量（以mg/ml计算）",
            "总剂量（以mg/ml计算）",
            "国际条形码1",
            "国际条形码2",
            "批准文号分类",
            "批准文号",
            "批准文号批准日期",
            "批准文号失效日期",
            "商品分类",
            "商品分类描述",
            "成分分类",
            "成分分类描述",
            "处方类别",
            "生产企业代码",
            "生产企业",
            "商标",
            "品牌标识名",
            "品牌区分",
            "保质期",
            "保质期单位",
            "上市许可持有人",
            "进项税率",
            "销项税率",
            "药品本位码",
            "税务分类编码",
            "管制特殊分类",
            "生产类别",
            "贮存条件",
            "商品仓储分区",
            "长（以MM计算）",
            "宽（以MM计算）",
            "高（以MM计算）",
            "中包装量",
            "大包装量",
            "启用电子监管码",
            "生产经营许可证号",
            "最大销售量",
            "说明书代码",
            "说明书内容",
            "国家医保品种",
            "国家医保品种编码",
            "生产国家",
            "产地",
            "可服用天数",
            "用法用量",
            "禁忌说明",
            "商品状态",
            "商品定位",
            "禁止销售",
            "禁止采购",
            "禁止配送",
            "禁止退厂",
            "禁止退仓",
            "禁止调剂",
            "禁止批发",
            "禁止请货",
            "是否拆零",
            "拆零单位",
            "拆零比例",
            "采购单位",
            "采购比例",
            "销售单位",
            "销售比例",
            "最小订货量",
            "是否医保",
            "销售级别",
            "限购数量",
            "中药规格",
            "中药批准文号",
            "中药生产企业代码",
            "中药产地",
            "含麻最大配货量",
            "按中包装量倍数配货",
            "是否重点养护",
            "固定货位"
    };

    /**
     * 基础数据头
     */
    private String[] basicHead = {
            "商品编码",
            "通用名称",
            "商品描述",
            "助记码",
            "商品名",
            "规格",
            "计量单位",
            "剂型",
            "细分剂型",
            "最小剂量（以mg/ml计算）",
            "总剂量（以mg/ml计算）",
            "国际条形码1",
            "国际条形码2",
            "批准文号分类",
            "批准文号",
            "批准文号批准日期",
            "批准文号失效日期",
            "商品分类",
            "商品分类描述",
            "成分分类",
            "成分分类描述",
            "处方类别",
            "生产企业代码",
            "生产企业",
            "商标",
            "品牌标识名",
            "品牌区分",
            "保质期",
            "保质期单位",
            "上市许可持有人",
            "进项税率",
            "销项税率",
            "药品本位码",
            "税务分类编码",
            "管制特殊分类",
            "生产类别",
            "贮存条件",
            "商品仓储分区",
            "长（以MM计算）",
            "宽（以MM计算）",
            "高（以MM计算）",
            "中包装量",
            "大包装量",
            "启用电子监管码",
            "生产经营许可证号",
            "最大销售量",
            "说明书代码",
            "说明书内容",
            "国家医保品种",
            "国家医保品种编码",
            "生产国家",
            "产地",
            "状态",
            "可服用天数",
            "用法用量",
            "禁忌说明"
    };


//商品	1	数据库	1	商品编码	a
//		加盟商	2	商品描述	b
//				剂型	c
//				批准文号分类	d
//				商品分类	e
//				成分分类	f
//				处方类别	g
//				生产企业	h
//				商标	i
//				品牌标识名	j
//				品牌区分	k
//				进项税率	l
//				销项税率	m
//				管制特殊分类	n
//				生产类别	o
//				贮存条件	p
//				商品仓储分区	q
//				国家医保品种	r
//				状态	s
//				加盟商	t
//				地点	u
//				商品自编码	v
//				商品状态	w
//				商品定位	x
//				是否拆零	y
//				是否医保	z
//				销售级别	aa
//

}
