package com.gov.purchase.module.goods.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "首营列表返回参数")
public class QueryGspResponseDto {

    private String client;

    private String proCode;

    private String proSelfCode;

    private String proSite;

    private String proSiteName;

    private String proName;

    private String proCommonname;

    private String proForm;

    private String proFormName;

    private String proSpecs;

    private String proPackSpecs;

    private String proRegisterNo;

    private String proFactoryName;

    private String proIfGmp;

    private String proQs;

    private String proFunction;

    private String proLifeDate;

    private String proStorageCondition;

    private String proSupplyName;

    private String proSupplyAdd;

    private String proSupplyContact;

    private String proSupplyTel;

    private String proGspDate;

    private String proFlowNo;

    private String proGspStatus;

    /**
     * 批准文号有效期
     */
    private String proRegisterExdate;
}
