package com.gov.purchase.module.qa.service;

import com.gov.purchase.module.qa.dto.*;

import java.util.List;

public interface QualityService {

    /**
     * 获取字段列表
     */
    List queryFieldList();

    /**
     * 商品状态更新-获取商品编号列表
     */
    List<QueryProductListResponseDto> queryProductList(QueryProductListRequestDto dto);


    /**
     * 商品状态更新提交
     */
    int updateProduct(List<UpdateProductResquestDto> list);


    /**
     * 质量管控-批次状态列表
     */
    List<QueryStockListResponseDto> queryStockList(QueryStockListResquestDto dto);

    /**
     * 质量管控-批次状态提交
     */
    int saveBatchStockInfo(List<SaveBatchStockInfoResquestDto> list);
}
