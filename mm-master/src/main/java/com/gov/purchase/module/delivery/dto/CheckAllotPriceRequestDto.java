package com.gov.purchase.module.delivery.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "调拨价格查看传入参数")
public class CheckAllotPriceRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 条件类型
     */
    @ApiModelProperty(value = "条件类型", name = "alpConditionType", required = true)
    @NotBlank(message = "条件类型不能为空")
    private String alpConditionType;
}