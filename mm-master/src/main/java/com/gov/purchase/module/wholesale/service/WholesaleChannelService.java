package com.gov.purchase.module.wholesale.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.*;
import com.gov.purchase.module.wholesale.dto.BatchProcessWholeSaleChannelDetailDTO;
import com.gov.purchase.module.wholesale.dto.WholeSaleChannelDetailDTO;
import com.gov.purchase.module.wholesale.dto.WholeSaleChannelDetailVo;

import java.util.List;

/**
 * @program: mm
 * @author: yzf
 * @create: 2021-11-01 13:43
 */

public interface WholesaleChannelService {

    /**
     * 加盟商对应的仓库列表
     *
     * @return
     */
    List<GaiaDcData> getDcData();


    /**
     * 获取批发渠道信息
     *
     * @param dcCode
     * @param channelName
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<WholeSaleChannelDetailDTO> queryWholeSaleChannelData(String dcCode, String channelName,
                                                                  String proCode, String cusCode,
                                                                  Integer pageNum, Integer pageSize);

    /**
     * 根据批发渠道自编码类型查询明细（类型：0，商品， 1，用户）
     *
     * @param dcCode
     * @param chaCode
     * @param type
     * @return
     */
    List<WholeSaleChannelDetailVo> queryWholeSaleChannelDetailByType(String dcCode, String chaCode, String type);

    /**
     * 新增批发渠道
     *
     * @param gaiaWholesaleChannel
     * @return
     */
    Result insertWholesaleChannel(GaiaWholesaleChannel gaiaWholesaleChannel);

    /**
     * 更新批发渠道
     *
     * @param gaiaWholesaleChannel
     * @return
     */
    Result updateWholesaleChannel(GaiaWholesaleChannel gaiaWholesaleChannel);

    /**
     * 删除批发渠道
     *
     * @param gaiaWholesaleChannel
     * @return
     */
    Result delWholesaleChannel(GaiaWholesaleChannel gaiaWholesaleChannel);

    /**
     * 批量更新
     *
     * @param processWholeSaleChannelDetailDTO
     */
    Result saveBatchWholesaleChannelDetail(BatchProcessWholeSaleChannelDetailDTO processWholeSaleChannelDetailDTO);

    /**
     * 未选客户
     *
     * @param dcCode
     * @return
     */
    List<GaiaCustomerBusiness> queryCustomerBusiness(String dcCode);

    /**
     * 未选商品
     *
     * @param dcCode
     * @return
     */
    List<GaiaProductBusiness> queryProductBusiness(String dcCode);
}
