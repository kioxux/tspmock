package com.gov.purchase.module.store.dto.price;


import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel(value = "调价单列表查询传入参数")
public class AdjustPriceRequestDto extends Pageable {

    @ApiModelProperty(value = "调价单号集合", name = "praAdjustNoList")
    private List<String> praAdjustNoList;

    @ApiModelProperty(value = "调价单号", name = "praAdjustNo")
    private String  praAdjustNo;

    @ApiModelProperty(value = "店组编码", name = "stogCode")
    private String  stogCode;

    @ApiModelProperty(value = "商品名称", name = "proName")
    private String  proName;

    @ApiModelProperty(value = "商品编码", name = "proSelfCode")
    private String proSelfCode;

    @ApiModelProperty(value = "商品编码集合", name = "proSelfCodeList")
    private List<String> proSelfCodeList;

    @ApiModelProperty(value = "调价开始日期", name = "startDate")
    private String  startDate;

    @ApiModelProperty(value = "调价结束日期", name = "endDate")
    private String  endDate;

    @ApiModelProperty(value = "审批状态", name = "praApprovalStatus")
    private String  praApprovalStatus;

    @ApiModelProperty(value = "同步状态", name = "parSendStatus")
    private String  praSendStatus;

    @ApiModelProperty(value = "门店编码", name = "stoCode")
    private String  stoCode;

    @ApiModelProperty(value = "门店编码集合", name = "stoCodeList")
    private List<String> stoCodeList;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String  client;
    /**
     * 门店编码
     */
    private String praStore;

    /**
     * 商品编码
     */
    private String praProduct;

    /**
     * 品牌
     */
    private String proBrand;
}
