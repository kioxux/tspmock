package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

@Data
@ApiModel(value = "商品列表传入参数")
public class GetProductListRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "proSite")
    private List<String> proSite;

    /**
     * 连锁总部
     */
    private String compadmId;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 参数
     */
    private String param;

    /**
     * 商品自编码
     */
    private String proSelfCode2;

    private String proSelfCode3;

    /**
     * 供应商
     */
    private String supCode;

    /**
     * 查询页面
     */
    private Integer pageNum;

    /**
     * 分页信息
     */
    private Integer pageSize;

    /**
     * 参数
     */
    private List<Map<String, String>> params;

    private Integer isPurchase;
}