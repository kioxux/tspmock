package com.gov.purchase.module.base.dto;

import com.gov.purchase.entity.GaiaSdProductPrice;
import lombok.Data;

/**
 * Author: 钱金华
 * Date: 2020-07-01
 * Time: 10:04
 */
@Data
public class GaiaSdProductPriceDto extends GaiaSdProductPrice {

    /**
     * 下标值  数组中的下标值
     */
    private Integer indexed;

    /**
     * 调价单号
     */
    private String praAdjustNo;

}
