package com.gov.purchase.module.base.dto;

import com.gov.purchase.entity.GaiaFranchisee;
import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.20
 */
@Data
public class GaiaFranchiseeResponseDto extends GaiaFranchisee {

    /**
     * 门店集合
     */
    List<GaiaStoreData> gaiaStoreDataList;
}

