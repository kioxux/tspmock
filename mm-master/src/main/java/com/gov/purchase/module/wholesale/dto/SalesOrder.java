package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.07
 */
@Data
public class SalesOrder {
    /**
     * 加盟商
     */
    private String client;
    /**
     * 加盟商名
     */
    private String francName;
    /**
     * 销售主体
     */
    private String soCompanyCode;
    /**
     * 销售主体名
     */
    private String dcName;
    /**
     * 创建人
     */
    private String soCreateBy;
    /**
     * 创建人
     */
    private String soCreateByName;
    /**
     * 销售订单号
     */
    private String soId;
    /**
     * 订单类型
     */
    private String soType;
    /**
     * 订单类型名称
     */
    private String ordTypeCesc;
    /**
     * 订单日期
     */
    private String soDate;
    /**
     * 客户编码
     */
    private String soCustomerId;
    /**
     * 客户名
     */
    private String cusName;
    /**
     * 付款条款
     */
    private String soPaymentId;
    /**
     * 付款条款描述
     */
    private String payTypeDesc;
    /**
     * 审批状态 0-不需审批，1-未审批，2-已审批
     */
    private String soApproveStatus;
    /**
     * 抬头备注
     */
    private String soHeadRemark;
    /**
     * 订单行号
     */
    private String soLineNo;
    /**
     * 商品编码
     */
    private String soProCode;
    /**
     * 商品名
     */
    private String proName;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 订单数量
     */
    private BigDecimal soQty;
    /**
     * 单位
     */
    private String soUnit;
    /**
     * 单位名
     */
    private String unitName;
    /**
     * 单价
     */
    private BigDecimal soPrice;
    /**
     * 税率
     */
    private String soRate;
    /**
     * 税率值
     */
    private String taxCodeValue;
    /**
     * 订单行金额
     */
    private BigDecimal soLineAmt;
    /**
     * 地点
     */
    private String soSiteCode;
    /**
     * 地点名
     */
    private String dcNameLine;
    /**
     * 库位
     */
    private String soLocationCode;
    /**
     * 批次
     */
    private String soBatch;
    /**
     * 交货日期
     */
    private String soDeliveryDate;
    /**
     * 行备注
     */
    private String soLineRemark;
    /**
     * 删除标记
     */
    private String soLineDelete;
    /**
     * 交货已完成标记
     */
    private String soCompleteRlag;
    /**
     * 已交货数量
     */
    private BigDecimal soDeliveredQty;
    /**
     * 已开票数量
     */
    private BigDecimal soInvoiceQty;
    /**
     * 已交货金额
     */
    private BigDecimal soDeliveredAmt;
    /**
     * 已开票金额
     */
    private BigDecimal soInvoiceAmt;
    /**
     * 订单总金额
     */
    private BigDecimal soLineAmtOrder;
    /**
     * 明细集合
     */
    private List<SalesOrderItem> detailList;
    /**
     * 客户编码
     */
    private String soCustomer2;
    /**
     * 客户名称
     */
    private String compadmName;

    /**
     * 销售员编号
     */
    private String gwsCode;

    /**
     * 通用名
     */
    private String proCommonname;

    /**
     * 上次开票价
     */
    private BigDecimal wmCkj;

    /**
     * 修改标识
     */
    private String editFlg;

    /**
     * 生产批号
     */
    private String soBatchNo;

    /**
     * 生产日期
     */
    private String batProductDate;

    /**
     * 有效期
     */
    private String batExpiryDate;

    /**
     * 审批人Id
     */
    private String soApproveBy;
    /**
     * 审批日期
     */
    private String soApproveDate;
    /**
     * 审批人姓名
     */
    private String soApproveName;
}
