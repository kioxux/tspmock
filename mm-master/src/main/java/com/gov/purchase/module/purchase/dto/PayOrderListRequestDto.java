package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import net.minidev.json.annotate.JsonIgnore;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "付款单查询传入参数")
public class PayOrderListRequestDto extends Pageable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 供应商
     */
    @ApiModelProperty(value = "供应商", name = "payOrderPayer")
    private String payOrderPayer;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "payCompanyCode", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String payCompanyCode;

    /**
     * 采购订单号
     */
    @JsonIgnore
    @ApiModelProperty(value = "采购订单号", name = "poLineNo")
    private String[] poLineNo;

    /**
     * 发票号
     */
    @JsonIgnore
    @ApiModelProperty(value = "发票号", name = "invoiceNum")
    private String[] invoiceNum;

    /**
     * 付款单号
     */
    @JsonIgnore
    @ApiModelProperty(value = "付款单号", name = "payOrderId")
    private String[] payOrderId;
}