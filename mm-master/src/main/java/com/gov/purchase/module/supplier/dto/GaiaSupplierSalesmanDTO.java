package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaSupplierSalesman;
import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021/5/12 10:40
 */
@Data
public class GaiaSupplierSalesmanDTO extends GaiaSupplierSalesman {

    private Integer usedFlag;
}
