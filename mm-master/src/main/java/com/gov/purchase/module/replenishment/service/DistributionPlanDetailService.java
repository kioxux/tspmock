package com.gov.purchase.module.replenishment.service;

import com.gov.purchase.entity.GaiaNewDistributionPlanDetail;

import java.util.List;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 14:52
 */
public interface DistributionPlanDetailService {

    GaiaNewDistributionPlanDetail update(GaiaNewDistributionPlanDetail distributionPlanDetail);

    List<GaiaNewDistributionPlanDetail> findList(GaiaNewDistributionPlanDetail cond);

}
