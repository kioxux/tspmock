package com.gov.purchase.module.priceCompare.vo;

import com.gov.purchase.entity.GaiaSspPriceComparePro;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PriceCompareProInfoVo extends GaiaSspPriceComparePro {

    private String priceCompareNo;

    private BigDecimal minPrice;

    /**
     * 提交用付款条款
     */
    private String supPayTerm;

    /**
     * 货主名字
     */
    private String poOwnerName;

    private String proZdy1;

    private String proZdy2;

    private String proZdy3;

    private String proZdy4;

    private String proZdy5;

    private List<PriceCompareSupplierInfoVo> supplierList;


}
