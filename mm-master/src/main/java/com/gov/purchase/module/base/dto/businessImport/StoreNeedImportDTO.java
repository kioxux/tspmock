package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class StoreNeedImportDTO {
    /**
     * 门店编码
     */
    @ExcelValidate(index = 0, name = "门店编码", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String stoCode;

    /**
     * 商品编码
     */
    @ExcelValidate(index = 1, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 20)
    private String proSelfCode;

    /**
     * 计划铺货数量
     */
    @ExcelValidate(index = 2, name = "计划铺货数量", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 9999999999.9999, addRequired = true)
    private String gsrdNeedQty;

    /**
     * 批号
     */
    @ExcelValidate(index = 3, name = "批号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false)
    private String batBatchNo;

    /**
     * 货位号
     */
    @ExcelValidate(index = 4, name = "货位号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false)
    private String wmHwh;
}
