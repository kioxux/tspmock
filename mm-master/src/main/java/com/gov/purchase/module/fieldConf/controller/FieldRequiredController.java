package com.gov.purchase.module.fieldConf.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.fieldConf.dto.FieldRequiredConfDTO;
import com.gov.purchase.module.fieldConf.service.FieldRequiredService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @description: 首营字段配置以及必填字段
 * @author: yzf
 * @create: 2021-12-08 09:51
 */
@Api(tags = "首营字段配置以及必填字段")
@RestController
@RequestMapping("fieldRequired")
public class FieldRequiredController {

    @Resource
    private FieldRequiredService fieldRequiredService;

    @Log("首营字段配置列表")
    @ApiOperation("首营字段配置列表")
    @PostMapping("queryFieldRequiredDataList")
    public Result queryFieldRequiredDataList() {
        return fieldRequiredService.queryFieldRequiredDataList();
    }

    @Log("首营必填字段列表")
    @ApiOperation("首营必填字段列表")
    @PostMapping("queryFieldRequiredConfList")
    public Result queryFieldRequiredConfList() {
        return fieldRequiredService.queryFieldRequiredConfList();
    }

    @Log("首营必填字段更新")
    @ApiOperation("首营必填字段更新")
    @PostMapping("updateFieldRequiredConf")
    public Result updateFieldRequiredConf(@RequestBody FieldRequiredConfDTO fieldConfDTO) {
        fieldRequiredService.updateFieldRequiredConf(fieldConfDTO);
        return ResultUtil.success();
    }

}
