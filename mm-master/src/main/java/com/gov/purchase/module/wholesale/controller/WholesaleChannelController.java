package com.gov.purchase.module.wholesale.controller;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaWholesaleChannel;
import com.gov.purchase.entity.GaiaWholesaleChannelDetail;
import com.gov.purchase.module.wholesale.dto.BatchProcessWholeSaleChannelDetailDTO;
import com.gov.purchase.module.wholesale.dto.WholeSaleChannelDetailDTO;
import com.gov.purchase.module.wholesale.dto.WholeSaleChannelDetailVo;
import com.gov.purchase.module.wholesale.service.WholesaleChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: mm
 * @description: 批发渠道
 * @author: Yzf
 * @create: 2021-11-01 13:27
 */
@Api(tags = "批发渠道")
@RestController
@RequestMapping("wholeSaleChannel")
public class WholesaleChannelController {

    @Resource
    private WholesaleChannelService wholesaleChannelService;

    /**
     * 仓库列表
     *
     * @return
     */
    @Log("获取仓库列表")
    @ApiOperation("获取仓库列表")
    @PostMapping("queryDcData")
    public Result getDcData() {
        return ResultUtil.success(wholesaleChannelService.getDcData());
    }

    /**
     * 批发渠道所有客户
     *
     * @return
     */
    @Log("批发渠道所有客户")
    @ApiOperation("批发渠道所有客户")
    @PostMapping("queryCustomerBusiness")
    public Result queryCustomerBusiness(@RequestJson(value = "dcCode") String dcCode) {
        return ResultUtil.success(wholesaleChannelService.queryCustomerBusiness(dcCode));
    }


    /**
     * 批发渠道所有商品
     *
     * @return
     */
    @Log("批发渠道所有商品")
    @ApiOperation("批发渠道所有商品")
    @PostMapping("queryProductBusiness")
    public Result queryProductBusiness(@RequestJson(value = "dcCode") String dcCode) {
        return ResultUtil.success(wholesaleChannelService.queryProductBusiness(dcCode));
    }


    /**
     * 获取批发渠道详细信息
     *
     * @return
     */
    @Log("获取批发渠道详细信息")
    @ApiOperation("获取批发渠道详细信息")
    @PostMapping("queryWholeSaleChannelData")
    public Result resultAllotPriceList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                       @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                       @RequestJson(value = "dcCode", required = false) String dcCode,
                                       @RequestJson(value = "channelName", required = false) String channelName,
                                       @RequestJson(value = "proCode", required = false) String proCode,
                                       @RequestJson(value = "cusCode", required = false) String cusCode) {
        PageInfo<WholeSaleChannelDetailDTO> list = wholesaleChannelService.queryWholeSaleChannelData(dcCode, channelName, proCode, cusCode, pageNum, pageSize);
        return ResultUtil.success(list);
    }

    /**
     * 获取批发渠道详细信息
     *
     * @return
     */
    @Log("获取批发渠道详细信息")
    @ApiOperation("获取批发渠道详细信息")
    @PostMapping("queryWholeSaleChannelDetailByType")
    public Result queryWholeSaleChannelDetailByType(@RequestBody GaiaWholesaleChannelDetail gaiaWholesaleChannelDetail) {
        List<WholeSaleChannelDetailVo> list = wholesaleChannelService.queryWholeSaleChannelDetailByType(
                gaiaWholesaleChannelDetail.getGwcdDcCode(),
                gaiaWholesaleChannelDetail.getGwcdChaCode(),
                gaiaWholesaleChannelDetail.getGwcdType());
        return ResultUtil.success(list);
    }


    /**
     * 新增批发渠道
     *
     * @return
     */
    @Log("新增批发渠道")
    @ApiOperation("新增批发渠道")
    @PostMapping("insertWholesaleChannel")
    public Result insertWholesaleChannel(@RequestBody GaiaWholesaleChannel gaiaWholesaleChannel) {
        return ResultUtil.success(wholesaleChannelService.insertWholesaleChannel(gaiaWholesaleChannel));
    }


    /**
     * 更新批发渠道
     *
     * @return
     */
    @Log("更新批发渠道")
    @ApiOperation("更新批发渠道")
    @PostMapping("updateWholesaleChannel")
    public Result updateWholesaleChannel(@RequestBody GaiaWholesaleChannel gaiaWholesaleChannel) {
        return ResultUtil.success(wholesaleChannelService.updateWholesaleChannel(gaiaWholesaleChannel));
    }

    /**
     * 删除批发渠道
     *
     * @return
     */
    @Log("删除批发渠道")
    @ApiOperation("删除批发渠道")
    @PostMapping("delWholesaleChannel")
    public Result delWholesaleChannel(@RequestBody GaiaWholesaleChannel gaiaWholesaleChannel) {
        return ResultUtil.success(wholesaleChannelService.delWholesaleChannel(gaiaWholesaleChannel));
    }

    @Log("批发渠道信息详情批量处理")
    @ApiOperation("批发渠道信息详情批量处理")
    @PostMapping("saveBatchWholesaleChannelDetail")
    public Result saveBatchWholesaleChannelDetail(@RequestBody BatchProcessWholeSaleChannelDetailDTO processWholeSaleChannelDetailDTO) {
        return wholesaleChannelService.saveBatchWholesaleChannelDetail(processWholeSaleChannelDetailDTO);
    }

}
