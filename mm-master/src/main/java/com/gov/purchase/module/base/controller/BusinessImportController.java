package com.gov.purchase.module.base.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.base.dto.businessImport.ImportDto;
import com.gov.purchase.module.base.service.BusinessImportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Api(tags = "业务数据导入功能")
@RestController
@RequestMapping("businessImport")
public class BusinessImportController {

    @Resource
    BusinessImportService businessImportService;

    /**
     * 导入
     *
     * @return
     */
    @Log("导入")
    @ApiOperation("导入")
    @PostMapping("businessImport/{type}")
    public Result businessImport(@PathVariable String type, @Valid @RequestBody ImportDto dto) {
        dto.setType(type);
        return businessImportService.businessImport(dto);
    }
}
