package com.gov.purchase.module.base.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.entity.GaiaSupplierBusinessKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaSdReplenishHMapper;
import com.gov.purchase.mapper.GaiaSupplierBusinessMapper;
import com.gov.purchase.module.base.dto.SupplierDto;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.StoreReplenishService;
import com.gov.purchase.module.replenishment.dto.*;
import com.gov.purchase.module.replenishment.service.impl.DcReplenishmentServiceImpl;
import com.gov.purchase.utils.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.27
 */
@Service
public class StoreReplenishServiceImpl implements StoreReplenishService {

    @Resource
    private GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Resource
    private DcReplenishmentServiceImpl dcReplenishmentService;
    @Resource
    private FeignService feignService;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;

    /**
     * 门店补货查询
     *
     * @return
     */
    @Override
    public Result queryReplenishList(QueryReplenishReq queryReplenishReq) {
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(queryReplenishReq.getPageNum(), queryReplenishReq.getPageSize());
        // 门店补货查询
        List<StoreReplenishmentListResponseDto> list = gaiaSdReplenishHMapper.queryReplenishList(queryReplenishReq.getClient(), queryReplenishReq.getGsrhBrId(),
                queryReplenishReq.getGsrhVoucherId(), queryReplenishReq.getGsrhDate(), user.getUserId(), queryReplenishReq.getGsrhBrIds());
        PageInfo<StoreReplenishmentListResponseDto> pageInfo = new PageInfo<>(list);
        List<StoreReplenishmentListResponseDto> result = pageInfo.getList();
        // 配送平均天数  默认为 3天
        for (StoreReplenishmentListResponseDto dto : result) {
            //  预计到货日期  = 当前日期 + 供应商的前置期
            if (dto.getSupLeadTime() != null) {
                dto.setPoDeliveryDate(DateUtils.formatLocalDate(LocalDate.now().plusDays(dto.getSupLeadTime()), "yyyyMMdd"));
                // （中包装从商品主数据中取值，若为空则默认是1）
                if (StringUtils.isBlank(dto.getProMidPackage())) {
                    dto.setProMidPackage("1");
                }
            }
            // 供应商下拉框
            SupplierRequestDto supplierRequestDto = new SupplierRequestDto();
            supplierRequestDto.setClient(queryReplenishReq.getClient());
            supplierRequestDto.setSiteCode(dto.getGsrhBrId());
            supplierRequestDto.setProSelfCode(dto.getGsrdProId());
            List<SupplierResponseDto> supplierResponseDtoList = dcReplenishmentService.querySupplierList(supplierRequestDto);
            dto.setSupplierResponseDtoList(supplierResponseDtoList);
        }
        return ResultUtil.success(pageInfo);

    }

    /**
     * 门店补货
     */
    @Override
    public void saveReplenish(List<DcReplenishmentRequestDto> list) {
        dcReplenishmentService.saveDcReplenishmentInfo(list, "2");
    }

    /**
     * 导出
     *
     * @param queryReplenishReq
     * @return
     */
    @Override
    public Result exportList(QueryReplenishReq queryReplenishReq) {
        try {
            TokenUser user = feignService.getLoginInfo();
            PageHelper.clearPage();
            // 门店补货查询
            List<StoreReplenishmentListResponseDto> list = gaiaSdReplenishHMapper.queryReplenishList(queryReplenishReq.getClient(), queryReplenishReq.getGsrhBrId(),
                    queryReplenishReq.getGsrhVoucherId(), queryReplenishReq.getGsrhDate(), user.getUserId(), queryReplenishReq.getGsrhBrIds());

            List<List<String>> dataList = new ArrayList<>();
            // 配送平均天数  默认为 3天
            for (StoreReplenishmentListResponseDto dto : list) {
                // 打印行
                List<String> item = new ArrayList<>();
                //  预计到货日期  = 当前日期 + 供应商的前置期
                if (dto.getSupLeadTime() != null) {
                    dto.setPoDeliveryDate(DateUtils.formatLocalDate(LocalDate.now().plusDays(dto.getSupLeadTime()), "yyyyMMdd"));
                    // （中包装从商品主数据中取值，若为空则默认是1）
                    if (StringUtils.isBlank(dto.getProMidPackage())) {
                        dto.setProMidPackage("1");
                    }
                }
                // 门店编码
                item.add(dto.getGsrhBrId());
                // 门店名称
                item.add(dto.getStoName());
                // 商品编码
                item.add(dto.getGsrdProId());
                // 商品名称
                item.add(dto.getProName());
                // 规格
                item.add(dto.getProSpecs());
                // 生产厂家
                item.add(dto.getProFactoryName());
                // 单位
                item.add(dto.getProUnit());
                // 中包装量
                item.add(dto.getProMidPackage());
                // 补货单号
                item.add(dto.getGsrhVoucherId());
                // 补货行号
                item.add(dto.getGsrdSerial());
                // 门店库存
                item.add(dto.getStoreStockTotal() == null ? "0" : dto.getStoreStockTotal().toString());
                // 最后一次进货日期
                item.add(StringUtils.formatDate("/", dto.getLastPurchaseDate()));
                // 最后进货供应商编码
                item.add(dto.getLastSupplierId());
                // 最后进货供应商
                item.add(dto.getLastSupplier());
                // 末次进货价
                item.add(dto.getLastPurchasePrice() == null ? "" : dto.getLastPurchasePrice().toString());
                // 30天门店销售量
                item.add(dto.getDaysSalesCount() == null ? "0" : dto.getDaysSalesCount().toString());
                // 在途量
                item.add(dto.getTrafficCount() == null ? "0" : dto.getTrafficCount().toString());
                // 待出量
                item.add(dto.getWaitCount() == null ? "0" : dto.getWaitCount().toString());
                // 建议补货数量
                item.add(dto.getReplenishmentValue() == null ? "0" : dto.getReplenishmentValue().toString());
                // 供应商编码
                item.add(dto.getLastSupplierId());
                // 供应商名称
                item.add(dto.getLastSupplier());
                // 采购价格
                item.add(dto.getLastPurchasePrice() == null ? "" : dto.getLastPurchasePrice().toString());
                // 税率
                item.add(dto.getTaxCodeName());
                // 预计到货日期
                item.add(StringUtils.formatDate("/", dto.getPoDeliveryDate()));
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("门店补货");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }

    }

    /**
     * 门店补货金额确认
     *
     * @param list
     * @return
     */
    @Override
    public Result checkAmt(List<DcReplenishmentRequestDto> list) {
        /**
         * 1.数据验证
         */
        if (list.size() == 0) {
            throw new CustomResultException(ResultEnum.DC_REPLENISHMENT_EMPTY);
        }
        Map<String, BigDecimal> supplier = new HashMap<>();
        for (DcReplenishmentRequestDto dto : list) {
            ValidationResult result = ValidateUtil.validateEntity(dto);
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
            // 门店+供应商
            String key = dto.getClient().concat("/_/").concat(dto.getDcCode()).concat("/_/").concat(dto.getPoSupplierId());
            BigDecimal amt = supplier.get(key);
            if (amt == null) {
                amt = BigDecimal.ZERO;
            }
            amt = amt.add(dto.getPoPrice().multiply(dto.getPoQty()).setScale(4, BigDecimal.ROUND_HALF_UP));
            supplier.put(key, amt);
        }
        List<SupplierDto> result = new ArrayList<>();
        for (String key : supplier.keySet()) {
            String[] keys = key.split("/_/");
            String client = keys[0];
            String site = keys[1];
            String supSelfCode = keys[2];
            GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
            // 加盟商
            gaiaSupplierBusinessKey.setClient(client);
            // 地点
            gaiaSupplierBusinessKey.setSupSite(site);
            // 供应商自编码
            gaiaSupplierBusinessKey.setSupSelfCode(supSelfCode);
            // 加盟商业务表数据
            GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(gaiaSupplierBusinessKey);
            SupplierDto supplierDto = new SupplierDto();
            // 供应商自编码
            supplierDto.setSupSelfCode(supSelfCode);
            // 供应商名
            supplierDto.setSupName(gaiaSupplierBusiness.getSupName());
            // 起订金额
            supplierDto.setSupMixAmt(gaiaSupplierBusiness.getSupMixAmt() == null ? BigDecimal.ZERO : gaiaSupplierBusiness.getSupMixAmt());
            // 本次金额
            supplierDto.setAmount(supplier.get(key));
            // 差值 = 本次金额-起订金额
            supplierDto.setDifference(
                    (supplier.get(key) == null ? BigDecimal.ZERO : supplier.get(key)).subtract(gaiaSupplierBusiness.getSupMixAmt() == null ? BigDecimal.ZERO : gaiaSupplierBusiness.getSupMixAmt())
            );
            result.add(supplierDto);
        }
        Collections.sort(result, new Comparator<SupplierDto>() {
            @Override
            public int compare(SupplierDto o1, SupplierDto o2) {
                // 返回值为int类型，大于0表示正序，小于0表示逆序
                return o1.getDifference().subtract(o2.getDifference()).compareTo(BigDecimal.ZERO);
            }
        });
        return ResultUtil.success(result);
    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "门店编码",
            "门店名称",
            "商品编码",
            "商品名称",
            "规格",
            "生产厂家",
            "单位",
            "中包装量",
            "补货单号",
            "补货行号",
            "门店库存",
            "最后一次进货日期",
            "最后进货供应商编码",
            "最后进货供应商",
            "末次进货价",
            "30天门店销售量",
            "在途量",
            "待出量",
            "建议补货数量",
            "供应商编码",
            "供应商名称",
            "采购价格",
            "税率",
            "预计到货日期"
    };
}

