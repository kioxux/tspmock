package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "供应商返回参数")
public class SupplierListReturnDto {

    /**
     * 供应商编码
     */
    private String supCode;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 统一社会信用码
     */
    private String supCreditCode;

    /**
     * 供应商自编码
     */
    private String supSelfCode;

    /**
     * 采购付款条件
     */
    private String supPayTerm;

    /**
     * 送货前置期
     */
    private String supLeadTime;
    /**
     * 拼音码
     */
    private String supPym;

    /**
     * 禁止采购
     */
    private String supNoPurchase;
}