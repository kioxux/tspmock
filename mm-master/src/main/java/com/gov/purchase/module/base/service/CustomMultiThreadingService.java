package com.gov.purchase.module.base.service;

import com.gov.purchase.entity.GaiaSdMarketing;
import com.gov.purchase.entity.GaiaStoreData;

import java.util.List;

public interface CustomMultiThreadingService {

    /**
     * 营销任务审批通过 发送短信到门店
     * @param storeList 门店列表
     * @param marketing 营销活动
     */
    void sendMarketTaskSms(List<GaiaStoreData> storeList, GaiaSdMarketing marketing);
}
