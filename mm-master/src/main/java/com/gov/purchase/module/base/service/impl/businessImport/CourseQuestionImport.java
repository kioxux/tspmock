package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.businessImport.CourseQuestion;
import com.gov.purchase.module.base.dto.businessImport.TrainCourseQuestionDTO;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CourseQuestionImport extends BusinessImport {


    /**
     * 业务验证(课件题目导入)
     *
     * @param map   数据
     * @param field 字段
     * @param <T>
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {


        // 批量导入
        try {
            // 错误LIST
            List<String> errorList = new ArrayList<>();
            // 数据回显LIST
            List<TrainCourseQuestionDTO> courseQuestionList = new ArrayList<>();
            // 数据验证
            for (Integer key : map.keySet()) {
                TrainCourseQuestionDTO questionDTO = new TrainCourseQuestionDTO();
                courseQuestionList.add(questionDTO);
                // 行数据
                CourseQuestion courseQuestion = (CourseQuestion) map.get(key);

                // 题目类型
                String questionType = courseQuestion.getQuestionType();
                Dictionary questionTypeDict = CommonEnum.DictionaryStaticData.getDictionaryByLabel(CommonEnum.DictionaryStaticData.QUESTION_TYPE, questionType);
                questionDTO.setQuestionType(Integer.valueOf(questionTypeDict.getValue()));
                // 难易级别
                String questionLevel = courseQuestion.getQuestionLevel();
                Dictionary questionLevelDict = CommonEnum.DictionaryStaticData.getDictionaryByLabel(CommonEnum.DictionaryStaticData.QUESTION_LEVEL, questionLevel);
                questionDTO.setQuestionLevel(Integer.valueOf(questionLevelDict.getValue()));
                // 题目名称
                questionDTO.setQuestionName(courseQuestion.getQuestionName());
                // 题目释义
                questionDTO.setQuestionRemark(courseQuestion.getQuestionRemark());
                //
                List<String> items = new ArrayList<String>() {{
                    add(courseQuestion.getQuestionItem1());
                    add(courseQuestion.getQuestionItem2());
                    add(courseQuestion.getQuestionItem3());
                    add(courseQuestion.getQuestionItem4());
                    add(courseQuestion.getQuestionItem5());
                }};
                boolean flg = true;
                // 答案
                List<String> standardAnswerList = Arrays.asList("1", "2", "3", "4", "5");
                List<String> questionAnswerStrList = Arrays.asList(courseQuestion.getQuestionAnswer().replace(" ", "").split(","));
                // 单选答案只有一个
                if (questionDTO.getQuestionType().equals(1) && questionAnswerStrList.size() > 1) {
                    errorList.add(MessageFormat.format("第{0}行：单选题只能有一个答案", key + 1));
                    flg = false;
                }
                for (String answer : questionAnswerStrList) {
                    if (NumberUtils.toInt(answer, -1) == -1) {
                        errorList.add(MessageFormat.format("第{0}行：题目答案不正确", key + 1));
                        flg = false;
                        break;
                    }
                    if (!standardAnswerList.contains(answer)) {
                        errorList.add(MessageFormat.format("第{0}行：答案{1}和选项不匹配", key + 1, answer));
                        flg = false;
                        break;
                    }
                    if (flg && StringUtils.isBlank(items.get(NumberUtils.toInt(answer) - 1))) {
                        errorList.add(MessageFormat.format("第{0}行：选项不能为空", key + 1, answer));
                        flg = false;
                    }
                }
                // 选项连贯校验
                for (int i = items.size() - 1; flg && i >= 0; i--) {
                    if (StringUtils.isNotBlank(items.get(i))) {
                        for (int j = i - 1; flg && i != 0 && j >= 0; j--) {
                            if (StringUtils.isBlank(items.get(j))) {
                                errorList.add(MessageFormat.format("第{0}行：选项不连贯", key + 1));
                                flg = false;
                            }
                        }
                        break;
                    }
                }
                if (!flg) {
                    break;
                }
                // 题目选项
                List<TrainCourseQuestionDTO.TrainCourseQuestionItem> questionItemList = new ArrayList<>();
                for (int i = 0; i < items.size(); i++) {
                    if (ObjectUtils.isEmpty(items.get(i))) {
                        break;
                    }
                    TrainCourseQuestionDTO.TrainCourseQuestionItem questionItem = new TrainCourseQuestionDTO.TrainCourseQuestionItem();
                    questionItem.setItemName(items.get(i));
                    questionItem.setItemValue(i + 1);
                    questionItemList.add(questionItem);
                }
                questionDTO.setQuestionItemList(questionItemList);
                List<Integer> questionAnswerList = questionAnswerStrList.stream().map(Integer::valueOf).collect(Collectors.toList());
                questionDTO.setQuestionAnswerList(questionAnswerList);

            }
            // 验证报错
            if (CollectionUtils.isNotEmpty(errorList)) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            return ResultUtil.success(courseQuestionList);

        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

    }

}
