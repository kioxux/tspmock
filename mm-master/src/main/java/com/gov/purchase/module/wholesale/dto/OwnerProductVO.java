package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaOwnerProduct;
import lombok.Data;

/**
 * @description: 批发货主商品明细
 * @author: yzf
 * @create: 2021-11-24 10:36
 */
@Data
public class OwnerProductVO extends GaiaOwnerProduct {

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 厂家
     */
    private String proFactoryName;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 参考零售价
     */
    private String proLsj;

    /**
     * 商品描述
     */
    private String proDepict;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 自定义字段1
     */
    private String proZdy1;

    /**
     * 自定义字段2
     */
    private String proZdy2;

    /**
     * 自定义字段3
     */
    private String proZdy3;

    /**
     * 自定义字段4
     */
    private String proZdy4;

    /**
     * 自定义字段5
     */
    private String proZdy5;
}
