package com.gov.purchase.module.store.service.impl;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaPriceAdjust;
import com.gov.purchase.entity.GaiaSdMessage;
import com.gov.purchase.entity.GaiaStogData;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.GaiaSdProductPriceDto;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.qa.dto.QueryProductBusinessResponseDto;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;
import com.gov.purchase.module.store.dto.store.StoreListResponseDto;
import com.gov.purchase.module.store.service.StoreGroupProductService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StoreGroupServiceImpl implements StoreGroupProductService {

    @Resource
    FeignService feignService;

    @Resource
    GaiaProductBusinessMapper productBusinessMapper;

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    GaiaPriceAdjustMapper priceAdjustMapper;

    @Resource
    GaiaSdProductPriceMapper productPriceMapper;

    @Resource
    GaiaStoreDataMapper storeDataMapper;

    @Resource
    GaiaStogDataMapper stogDataMapper;

    @Resource
    private CosUtils cosUtils;

    @Resource
    GaiaSdMessageMapper gaiaSdMessageMapper;


    @Override
    public List<QueryProductBusinessResponseDto> queryList(String param) {
        TokenUser user = feignService.getLoginInfo();
        List<QueryProductBusinessResponseDto> productBusinessResponseDtos = new ArrayList<>();
        Map<Object, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(param)) {
            //参数
            map.put("PARAM", param);
        }

        map.put("CLIENT", user.getClient());
        map.put("USERID", user.getUserId());
        map.put("SGROUR", "1");
        List<GaiaStoreData> storeList = gaiaStoreDataMapper.SelectStoreAuth(map);
        if (!storeList.isEmpty()) {
            map.put("STORELIST", storeList);
            productBusinessResponseDtos = productBusinessMapper.getStorePriceProList(map);
        }

        return productBusinessResponseDtos;
    }


    @Override
    public List<QueryProductBusinessResponseDto> queryGroupList(List<String> param) {
        TokenUser user = feignService.getLoginInfo();
        List<QueryProductBusinessResponseDto> newList = new ArrayList<>();
        Map<Object, Object> map = new HashMap<>();
        if (null == param || param.isEmpty()) {
            throw new CustomResultException("参数不正确");
        }
        map.put("STORELIST", param);
        map.put("CLIENT", user.getClient());
        map.put("SGROUR", "1");
        List<QueryProductBusinessResponseDto> productBusinessResponseDtos = productBusinessMapper.getStoreGroupProList(map);
        if (!productBusinessResponseDtos.isEmpty()) {
            //根据店组编码来分组
            Map<String, List<QueryProductBusinessResponseDto>> prodMap = productBusinessResponseDtos.stream().
                    collect(Collectors.groupingBy(item -> item.getStogCode()));
            //循环 map value
            for (List<QueryProductBusinessResponseDto> dto : prodMap.values()) {
                QueryProductBusinessResponseDto sto = new QueryProductBusinessResponseDto();
                sto.setStogCode(dto.get(0).getStogCode());
                sto.setStogName(dto.get(0).getStogName());
                sto.setChildren(dto);
                //库存总数  库存和7/30天销量取和
                BigDecimal gssQty = BigDecimal.ZERO;
                Integer proWeek = 0;
                Integer proMonth = 0;
                //价格的逻辑 1 取最多的数量中的那个价格  2 取最高的那个价格

                List<QueryProductBusinessResponseDto> priceList = priceList(dto);
                //去最多数量的价格

                //根据类型分组
                Map<Integer, List<QueryProductBusinessResponseDto>> priceMap = priceList.stream().
                        collect(Collectors.groupingBy(item -> item.getType()));

                if (priceMap.containsKey(1)) {
                    List<QueryProductBusinessResponseDto> p = priceMap.get(1);
                    //去最大的数量
                    QueryProductBusinessResponseDto max = p.stream().max(Comparator.comparing(goods -> goods.getNum())).get();
                    if (max.getNum() > 1) {
                        sto.setGsppPriceNormal(max.getPrice());
                    } else {
                        QueryProductBusinessResponseDto maxp = p.stream().max(Comparator.comparing(goods -> goods.getPrice())).get();
                        sto.setGsppPriceNormal(maxp.getPrice());
                    }


                }
                if (priceMap.containsKey(2)) {
                    List<QueryProductBusinessResponseDto> p = priceMap.get(2);
                    //去最大的数量
                    QueryProductBusinessResponseDto max = p.stream().max(Comparator.comparing(goods -> goods.getNum())).get();
                    if (max.getNum() > 1) {
                        sto.setGsppPriceHy(max.getPrice());
                    } else {
                        QueryProductBusinessResponseDto maxp = p.stream().max(Comparator.comparing(goods -> goods.getPrice())).get();
                        sto.setGsppPriceHy(maxp.getPrice());
                    }

                }
                if (priceMap.containsKey(3)) {
                    List<QueryProductBusinessResponseDto> p = priceMap.get(3);
                    //去最大的数量
                    QueryProductBusinessResponseDto max = p.stream().max(Comparator.comparing(goods -> goods.getNum())).get();
                    if (max.getNum() > 1) {
                        sto.setGsppPriceYb(max.getPrice());
                    } else {
                        QueryProductBusinessResponseDto maxp = p.stream().max(Comparator.comparing(goods -> goods.getPrice())).get();
                        sto.setGsppPriceYb(maxp.getPrice());
                    }

                }
                if (priceMap.containsKey(4)) {
                    List<QueryProductBusinessResponseDto> p = priceMap.get(4);
                    //去最大的数量
                    QueryProductBusinessResponseDto max = p.stream().max(Comparator.comparing(goods -> goods.getNum())).get();
                    if (max.getNum() > 1) {
                        sto.setGsppPriceCl(max.getPrice());
                    } else {
                        QueryProductBusinessResponseDto maxp = p.stream().max(Comparator.comparing(goods -> goods.getPrice())).get();
                        sto.setGsppPriceCl(maxp.getPrice());
                    }
                }
                if (priceMap.containsKey(5) == true) {
                    List<QueryProductBusinessResponseDto> p = priceMap.get(5);
                    //去最大的数量
                    QueryProductBusinessResponseDto max = p.stream().max(Comparator.comparing(goods -> goods.getNum())).get();
                    if (max.getNum() > 1) {
                        sto.setGsppPriceOnlineNormal(max.getPrice());
                    } else {
                        QueryProductBusinessResponseDto maxp = p.stream().max(Comparator.comparing(goods -> goods.getPrice())).get();
                        sto.setGsppPriceOnlineNormal(maxp.getPrice());
                    }
                }
                if (priceMap.containsKey(6) == true) {
                    List<QueryProductBusinessResponseDto> p = priceMap.get(6);
                    //去最大的数量
                    QueryProductBusinessResponseDto max = p.stream().max(Comparator.comparing(goods -> goods.getNum())).get();
                    if (max.getNum() > 1) {
                        sto.setGsppPriceOnlineHy(max.getPrice());
                    } else {
                        QueryProductBusinessResponseDto maxp = p.stream().max(Comparator.comparing(goods -> goods.getPrice())).get();
                        sto.setGsppPriceOnlineHy(maxp.getPrice());
                    }

                }


                for (QueryProductBusinessResponseDto item : dto) {

                    //库存
                    gssQty = gssQty.add(null != item.getGssQty() ? item.getGssQty() : BigDecimal.ZERO);
                    proWeek = proWeek + (null != item.getProWeek() ? item.getProWeek() : 0);
                    proMonth = proMonth + (null != item.getProMonth() ? item.getProMonth() : 0);
                }
                sto.setProMonth(proMonth);
                sto.setProWeek(proWeek);
                sto.setGssQty(gssQty);


                newList.add(sto);
            }

        }
        return newList;
    }


    /**
     * 塞数量
     *
     * @param sourceList
     * @return
     */
    private List<QueryProductBusinessResponseDto> priceList(List<QueryProductBusinessResponseDto> sourceList) {
        List<QueryProductBusinessResponseDto> priceList = new ArrayList<>();
        for (QueryProductBusinessResponseDto item : sourceList) {
            //价格

            if (null != item.getGsppPriceNormal()) {
                //零售价
                QueryProductBusinessResponseDto price1 = new QueryProductBusinessResponseDto();
                price1.setType(1);
                price1.setNum(1);
                price1.setPrice(item.getGsppPriceNormal());
                if (!priceList.isEmpty()) {
                    priceList.forEach(o -> {
                        if (o.getType() == 1 && o.getPrice().equals(item.getGsppPriceNormal())) {
                            price1.setNum(o.getNum() + 1);
                        }
                    });

                }

                priceList.add(price1);
            }
            if (null != item.getGsppPriceHy()) {
                //会员价
                QueryProductBusinessResponseDto price2 = new QueryProductBusinessResponseDto();
                price2.setType(2);
                price2.setPrice(item.getGsppPriceHy());
                price2.setNum(1);
                if (!priceList.isEmpty()) {
                    priceList.forEach(o -> {
                        if (o.getType() == 2 && o.getPrice().equals(item.getGsppPriceHy())) {
                            price2.setNum(o.getNum() + 1);
                        }
                    });

                }

                priceList.add(price2);
            }
            if (null != item.getGsppPriceYb()) {
                //医保价
                QueryProductBusinessResponseDto price3 = new QueryProductBusinessResponseDto();
                price3.setType(3);
                price3.setPrice(item.getGsppPriceYb());
                price3.setNum(1);
                if (!priceList.isEmpty()) {
                    priceList.forEach(o -> {
                        if (o.getType() == 3 && o.getPrice().equals(item.getGsppPriceYb())) {
                            price3.setNum(o.getNum() + 1);
                        }
                    });

                }
                priceList.add(price3);
            }
            if (null != item.getGsppPriceCl()) {
                //拆零价
                QueryProductBusinessResponseDto price4 = new QueryProductBusinessResponseDto();
                price4.setType(4);
                price4.setPrice(item.getGsppPriceCl());
                price4.setNum(1);
                if (!priceList.isEmpty()) {
                    priceList.forEach(o -> {
                        if (o.getType() == 4 && o.getPrice().equals(item.getGsppPriceCl())) {
                            price4.setNum(o.getNum() + 1);
                        }
                    });

                }
                priceList.add(price4);
            }
            if (null != item.getGsppPriceOnlineNormal()) {
                //网上拆零价
                QueryProductBusinessResponseDto price5 = new QueryProductBusinessResponseDto();
                price5.setType(5);
                price5.setPrice(item.getGsppPriceOnlineNormal());
                price5.setNum(1);
                if (!priceList.isEmpty()) {
                    priceList.forEach(o -> {
                        if (o.getType() == 5 && o.getPrice().equals(item.getGsppPriceOnlineNormal())) {
                            price5.setNum(o.getNum() + 1);
                        }
                    });

                }
                priceList.add(price5);

            }
            if (null != item.getGsppPriceOnlineHy()) {
                //网上会员价
                QueryProductBusinessResponseDto price6 = new QueryProductBusinessResponseDto();
                price6.setType(6);
                price6.setPrice(item.getGsppPriceOnlineHy());
                price6.setNum(1);
                if (!priceList.isEmpty()) {
                    priceList.forEach(o -> {
                        if (o.getType() == 6 && o.getPrice().equals(item.getGsppPriceOnlineHy())) {
                            price6.setNum(o.getNum() + 1);
                        }
                    });

                }
                priceList.add(price6);

            }
        }
        return priceList;

    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveList(List<GaiaPriceAdjustDto> gaiaPriceAdjustList) {
        if (null == gaiaPriceAdjustList || gaiaPriceAdjustList.isEmpty()) {
            throw new CustomResultException("至少一条参数");
        }
        TokenUser user = feignService.getLoginInfo();
        //连锁list
        List<GaiaPriceAdjust> chainList = new ArrayList<>();
        //单体list
        List<GaiaPriceAdjustDto> monomerList = new ArrayList<>();
        List<String> codeList = new ArrayList<>();
        gaiaPriceAdjustList.forEach(item -> {
            codeList.add(item.getPraStore());

        });
        Map<Object, Object> mapCode = new HashMap<>();
        mapCode.put("CLIENT", user.getClient());
        mapCode.put("STOCODELIST", codeList);
        //判断门店是单体还是连锁
        List<GaiaStoreData> storeDataList = storeDataMapper.storeListByMap(mapCode);
        if (storeDataList.isEmpty()) {
            throw new CustomResultException("门店已不存在，请重新选择");
        }


        Map<Object, Object> map = new HashMap<>();
        map.put("client", user.getClient());
        GaiaPriceAdjust priceAdjust = priceAdjustMapper.getCardNumber(map);
        String tjNo = priceAdjust.getPraAdjustNo();

        List<GaiaPriceAdjust> addList = new ArrayList<>();
        //记录调价明细表
        gaiaPriceAdjustList.forEach(item -> {
            item.setClient(user.getClient());
            item.setPraAdjustNo(tjNo);
            item.setPraCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            if (!storeDataList.isEmpty()) {
                storeDataList.forEach(o -> {
                    if (o.getStoCode().equals(item.getPraStore())) {
                        if ("2".equals(o.getStoAttribute())) {
                            item.setPraApprovalStatus("1");
                            //连锁list
                            chainList.add(item);
                        } else {
                            //单体list
                            monomerList.add(item);
                        }

                    }
                });

            }
            item.setPraCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            item.setPraSendStatus("1");
            GaiaPriceAdjust adjust = new GaiaPriceAdjust();
            BeanUtils.copyProperties(item, adjust);
            addList.add(adjust);
        });

        priceAdjustMapper.insertBatch(addList);


        //单体和连锁的逻辑
        if (!chainList.isEmpty()) {

            //连锁审批工作流
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            // 初始编码
            String FlowNo = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0");
            FeignResult flowResult = feignService.createProductAdjustWorkflow(FlowNo, chainList);
            if (flowResult.getCode() != 0) {
                throw new CustomResultException(flowResult.getMessage());
            }

            List<GaiaPriceAdjust> updateList = new ArrayList<>();
            //   更新审批流程单号
            chainList.forEach(item -> {
                GaiaPriceAdjust adjust = new GaiaPriceAdjust();
                adjust.setClient(item.getClient());
                adjust.setPraAdjustNo(item.getPraAdjustNo());
                adjust.setPraProduct(item.getPraProduct());
                adjust.setPraStore(item.getPraStore());
                adjust.setPraFlowNo(FlowNo);
                updateList.add(adjust);
            });
            priceAdjustMapper.updateBatch(updateList);
        }

        //单体的
        if (!monomerList.isEmpty()) {
            List<GaiaPriceAdjust> updateList = new ArrayList<>();
            List<GaiaSdProductPriceDto> productPrices = new ArrayList<>();
            //单体
            Integer today = Integer.valueOf(DateUtils.getCurrentDateStrYYMMDD());
            monomerList.forEach(item -> {
                //有效期起小于等于当前日期
                Integer mondy = Integer.valueOf(item.getPraStartDate());
                if (mondy <= today) {
                    GaiaSdProductPriceDto productPrice = new GaiaSdProductPriceDto();
                    productPrice.setClient(user.getClient());
                    productPrice.setGsppBrId(item.getPraStore());
                    productPrice.setGsppProId(item.getPraProduct());


                    //零售价
                    if (null != item.getPraPriceNormal()) {
                        //说明调过零售价了
                        productPrice.setGsppPriceNormal(item.getPraPriceNormal());
                    } else {

                        //调后没有价格 则 等于调前价格
                        if (null != item.getPraPriceNormalBefore()) {
                            //说明没调价格
                            productPrice.setGsppPriceNormal(item.getPraPriceNormalBefore());
                        } else {
                            //说明没有零售价 但是调最贵的价格

                            //选取其中最贵的一个价格
                            BigDecimal praPriceHy = null != item.getPraPriceHy() ? item.getPraPriceHy() : BigDecimal.ZERO;
                            BigDecimal gsppPriceYb = null != item.getPraPriceYb() ? item.getPraPriceYb() : BigDecimal.ZERO;
                            BigDecimal gsppPriceCl = null != item.getPraPriceCl() ? item.getPraPriceCl() : BigDecimal.ZERO;
                            BigDecimal praPriceOln = null != item.getPraPriceOln() ? item.getPraPriceOln() : BigDecimal.ZERO;
                            BigDecimal praPriceOlhy = null != item.getPraPriceOlhy() ? item.getPraPriceOlhy() : BigDecimal.ZERO;
                            BigDecimal max = BigDecimal.ZERO;
                            if (praPriceHy.compareTo(gsppPriceYb) > -1) {
                                max = praPriceHy;
                            } else {
                                max = gsppPriceYb;
                            }
                            if (gsppPriceCl.compareTo(max) > -1) {
                                max = gsppPriceCl;
                            }

                            if (praPriceOln.compareTo(max) > -1) {
                                max = praPriceOln;
                            }

                            if (praPriceOlhy.compareTo(max) > -1) {
                                max = praPriceOlhy;
                            }
                            productPrice.setGsppPriceNormal(max);
                        }

                    }

                    //调后没有价格 则 等于调前价格
                    productPrice.setGsppPriceHy(null != item.getPraPriceHy() ? item.getPraPriceHy() : item.getPraPriceYhBefore());
                    productPrice.setGsppPriceYb(null != item.getPraPriceYb() ? item.getPraPriceYb() : item.getPraPriceYbBefore());
                    productPrice.setGsppPriceCl(null != item.getPraPriceCl() ? item.getPraPriceCl() : item.getPraPriceClBefore());
                    productPrice.setGsppPriceOnlineNormal(null != item.getPraPriceOln() ? item.getPraPriceOln() : item.getPraPriceOlnBefore());
                    productPrice.setGsppPriceOnlineHy(null != item.getPraPriceOlhy() ? item.getPraPriceOlhy() : item.getPraPriceOlhyBefore());
                    productPrice.setGsppPriceHyr(item.getGsppPriceHyr());

                    productPrices.add(productPrice);

                    GaiaPriceAdjust adjust = new GaiaPriceAdjust();
                    adjust.setClient(item.getClient());
                    adjust.setPraAdjustNo(item.getPraAdjustNo());
                    adjust.setPraProduct(item.getPraProduct());
                    adjust.setPraStore(item.getPraStore());
                    adjust.setPraSendStatus("2");
                    updateList.add(adjust);
                }
            });

            if (!productPrices.isEmpty()) {

                //全删全插
                productPriceMapper.deleteList(productPrices);
                productPriceMapper.insertList(productPrices);

                // 过滤按照门店过滤
                Map<String, List<GaiaSdProductPriceDto>> mapList = productPrices.stream()
                        .collect(Collectors.groupingBy(GaiaSdProductPriceDto::getGsppBrId));
                for (Map.Entry<String, List<GaiaSdProductPriceDto>> entry : mapList.entrySet()) {
                    // 插入消息推送表
                    GaiaSdMessage sdMessage = new GaiaSdMessage();
                    sdMessage.setClient(user.getClient());
                    sdMessage.setGsmId(entry.getKey());  // 调价门店
                    sdMessage.setGsmVoucherId("MS" + LocalDate.now().getYear());  // 用来查询前缀
                    String currentVoucherId = gaiaSdMessageMapper.getCurrentVoucherId(sdMessage);
                    sdMessage.setGsmVoucherId(currentVoucherId);
                    sdMessage.setGsmValue("praAdjustNo=" + currentVoucherId + "&praStore=" + sdMessage.getGsmId());
                    String proSelfCodeJoin = entry.getValue().stream().filter(Objects::nonNull).map(GaiaSdProductPriceDto::getGsppProId).collect(Collectors.joining(","));
                    sdMessage.setGsmRemark(MessageFormat.format("您有新的商品调价信息,商品编码为[{0}],请及时查看", proSelfCodeJoin));
                    sdMessage.setGsmPlatform("FX");
                    sdMessage.setGsmFlag("N"); // 是否查看    默认为N-未查看
                    sdMessage.setGsmBusinessVoucherId(tjNo);  // 调价单号
                    sdMessage.setGsmArriveDate(DateUtils.getCurrentDateStrYYMMDD());
                    sdMessage.setGsmArriveTime(DateUtils.getCurrentTimeStrHHMMSS());
                    // TODO 缺少消息类型 消息内容 跳转页面
                    sdMessage.setGsmPage("pricingList");
                    gaiaSdMessageMapper.insertSelective(sdMessage);
                }

                //更新商品調價專題状态
                priceAdjustMapper.updateBatch(updateList);
            }

        }

        return ResultUtil.success();
    }


    /**
     * 门店列表
     *
     * @return
     */
    @Override
    public List<GaiaStoreData> queryStoreList() {
        TokenUser user = feignService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        map.put("CLIENT", user.getClient());
        List<GaiaStoreData> storeList = gaiaStoreDataMapper.storeListByMap(map);
        return storeList;
    }


    /**
     * 门店组列表
     *
     * @return
     */
    @Override
    public List<StoreListResponseDto> queryStoreGroupList() {
        TokenUser user = feignService.getLoginInfo();
        Map<Object, Object> map = new HashMap<>();
        map.put("CLIENT", user.getClient());
        return gaiaStoreDataMapper.storeGroupListByMap(map);
    }


    /**
     * 导出
     *
     * @param
     * @return
     */
    @Override
    public Result queryStoreGroupExportList() {
        try {
            TokenUser user = feignService.getLoginInfo();
            Map<Object, Object> map = new HashMap<>();
            map.put("CLIENT", user.getClient());
            map.put("USERID", user.getUserId());
            List<StoreListResponseDto> list = gaiaStoreDataMapper.storeGroupListExportByMap(map);

            List<List<String>> dataList = new ArrayList<>();
            for (StoreListResponseDto dto : list) {
                // 打印行
                List<String> item = new ArrayList<>();
                // 门店组编码
                item.add(dto.getStogCode());
                // 门店组名称
                item.add(dto.getStogName());
                // 门店编码
                item.add(dto.getStoCode());
                // 门店名称
                item.add(dto.getStoName());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("门店组导出");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }

    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "门店组编码",
            "门店组名称",
            "门店编码",
            "门店名称"
    };


    /**
     * 删除门店组信息
     *
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result deleteStoreGroupList(List<StoreListResponseDto> list) {
        TokenUser user = feignService.getLoginInfo();
        if (list.isEmpty()) {
            throw new CustomResultException("至少一条数据");
        }
        List<GaiaStogData> stogList = new ArrayList<>();
        List<String> codeList = new ArrayList<>();
        list.forEach(item -> {
            GaiaStogData stogData = new GaiaStogData();
            stogData.setClient(user.getClient());
            stogData.setStogCode(item.getStogCodeBefore());
            stogList.add(stogData);
            item.setClient(user.getClient());
            if (StringUtils.isNotBlank(item.getStogCodeBefore())) {
                codeList.add(item.getStogCodeBefore());
            }
        });
        stogDataMapper.deleteList(stogList);
        Map<Object, Object> map = new HashMap<>();
        map.put("CLIENT", user.getClient());
        map.put("STOGCODELIST", codeList);
        List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.storeListByMap(map);
        if (!storeDataList.isEmpty()) {
            List<GaiaStoreData> newList = new ArrayList<>();
            storeDataList.forEach(item -> {
                GaiaStoreData gaiaStoreData = new GaiaStoreData();
                gaiaStoreData.setStogCode("");
                gaiaStoreData.setClient(item.getClient());
                gaiaStoreData.setStoCode(item.getStoCode());
                gaiaStoreData.setStoModiDate(DateUtils.getCurrentDateStrYYMMDD());
                gaiaStoreData.setStoModiTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaStoreData.setStoModiId(user.getUserId());
                newList.add(gaiaStoreData);
            });
            gaiaStoreDataMapper.updateBatch(newList);
        }

        return ResultUtil.success();
    }


    /**
     * 保存门店组信息
     *
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveStoreGroupList(List<StoreListResponseDto> list) {
        TokenUser user = feignService.getLoginInfo();
        if (list.isEmpty()) {
            throw new CustomResultException("至少一条数据");
        }

        //门店组
        List<GaiaStogData> stogList = new ArrayList<>();
        List<GaiaStogData> stogAddList = new ArrayList<>();

        List<String> codeList = new ArrayList<>();

        //新的门店新的店组
        List<GaiaStoreData> newStogCodeList = new ArrayList<>();

        list.forEach(item -> {
            item.setClient(user.getClient());

            GaiaStogData stogDataBefore = new GaiaStogData();
            GaiaStogData stogData = new GaiaStogData();
            stogData.setClient(item.getClient());
            stogData.setStogCode(item.getStogCode());
            stogData.setStogName(item.getStogName());

            stogDataBefore.setClient(item.getClient());
            stogDataBefore.setStogCode(item.getStogCodeBefore());
            if (StringUtils.isNotBlank(item.getStogCodeBefore())) {
                stogList.add(stogDataBefore);
            }
            stogAddList.add(stogData);

            if (StringUtils.isNotBlank(item.getStogCodeBefore())) {
                codeList.add(item.getStogCodeBefore());
            }

            //门店
            if (StringUtils.isNotBlank(item.getStoCode())) {
                String[] storeList = item.getStoCode().split(",");
                for (String o : storeList) {
                    GaiaStoreData stog = new GaiaStoreData();
                    stog.setClient(item.getClient());
                    stog.setStogCode(item.getStogCode());
                    stog.setStoCode(o);
                    stog.setStoModiDate(DateUtils.getCurrentDateStrYYMMDD());
                    stog.setStoModiTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    stog.setStoModiId(user.getUserId());
                    newStogCodeList.add(stog);
                }
            }
        });

        //删除
        stogDataMapper.deleteList(stogList);
        //新增
        stogDataMapper.insertList(stogAddList);
        // 清空门店组
        gaiaStoreDataMapper.clearStog(user.getClient());
        // 更新门店组
        for (GaiaStoreData gaiaStoreData : newStogCodeList) {
            gaiaStoreDataMapper.updateByPrimaryKeySelective(gaiaStoreData);
        }
        return ResultUtil.success();
    }


}
