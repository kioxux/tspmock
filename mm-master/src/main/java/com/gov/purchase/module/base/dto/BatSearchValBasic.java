package com.gov.purchase.module.base.dto;

import com.gov.purchase.common.entity.Pageable;
import lombok.Data;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.15
 */
@Data
public class BatSearchValBasic extends Pageable {
    private String dataType;
    private String level;
    private LinkedHashMap<String, SearchValBasic> searchValBasic;

    public enum SearchBasicEnum {
        PRODUCT_DB("1", "1"),
        PRODUCT_CLIENT("1", "2"),
        SUPPLIER_DB("2", "1"),
        SUPPLIER_CLIENT("2", "2"),
        CUSTOMER_DB("3", "1"),
        CUSTOMER_CLIENT("3", "2"),
        STORE_CLIENT("4", "2"),
        DC_CLIENT("5", "2"),
        PERMIT_CLIENT("6", "2"),
        SCOPE_CLIENT("7", "2");

        private String dataType;

        private String level;

        public String getDataType() {
            return dataType;
        }

        public void setDataType(String dataType) {
            this.dataType = dataType;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        SearchBasicEnum(String dataType, String level) {
            this.dataType = dataType;
            this.level = level;
        }

        public static SearchBasicEnum getSearchBasicEnum(BatSearchValBasic batSearchValBasic) {
            for (SearchBasicEnum searchBasicEnum : SearchBasicEnum.values()) {
                if (searchBasicEnum.getDataType().equals(batSearchValBasic.getDataType()) && searchBasicEnum.getLevel().equals(batSearchValBasic.getLevel())) {
                    return searchBasicEnum;
                }
            }
            return null;
        }
    }

}

