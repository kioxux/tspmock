package com.gov.purchase.module.purchase.service.impl;

import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.ChangePurchaseService;
import com.gov.purchase.utils.CommonUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class ChangePurchaseServiceImpl implements ChangePurchaseService {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    private GaiaPoHeaderMapper gaiaPoHeaderMapper;
    @Resource
    private GaiaPoItemMapper gaiaPoItemMapper;
    @Resource
    private ChangePurchaseService changePurchaseService;
    @Resource
    private GaiaBatchInfoMapper gaiaBatchInfoMapper;
    @Resource
    private RedisClient redisClient;

    /**
     * 批次库存查询
     *
     * @param dto BatchStockListRequestDto
     * @return List<BatchStockListResponseDto>
     */
    public List<BatchStockListResponseDto> selectBatchStockList(BatchStockListRequestDto dto) {

        List<BatchStockListResponseDto> batchStockList = gaiaStoreDataMapper.selectBatchStockList(dto);
        return batchStockList;
    }

    /**
     * 物流模式门店
     *
     * @param dto DeliveryStoreRequestDto
     * @return List<DeliveryStoreResponseDto>
     */
    public List<DeliveryStoreResponseDto> selectDeliveryStore(DeliveryStoreRequestDto dto) {

        List<DeliveryStoreResponseDto> deliveryStore = gaiaStoreDataMapper.selectDeliveryStore(dto);
        return deliveryStore;
    }

    /**
     * 供应商列表
     *
     * @param dto SupplierListForReturnsRequestDto
     * @return List<SupplierListForReturnsResponseDto>
     */
    public List<SupplierListForReturnsResponseDto> selectSupplierListForReturns(SupplierListForReturnsRequestDto dto) {
        List<SupplierListForReturnsResponseDto> supplierList = gaiaSupplierBusinessMapper.selectSupplierList(dto);
        //设定返回值
        for (SupplierListForReturnsResponseDto returnDate : supplierList) {
            returnDate.setValue(returnDate.getSupSelfCode() + " " + returnDate.getSupName() + " " + returnDate.getSupCreditCode());
            returnDate.setLabel(returnDate.getSupSelfCode());
        }
        return supplierList;
    }

    /**
     * 采购退货
     *
     * @param dto PurchaseReturnsRequestDto
     * @return int
     */
    @SneakyThrows
    @Transactional
    public void insertPurchaseReturns(PurchaseReturnsRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();

        //参数空值检查
        if (null == dto.getReturnDto() || dto.getReturnDto().size() == 0) {
            throw new CustomResultException(ResultEnum.CHECK_PARAMETER);
        }

        for (ReturnDto returnDto : dto.getReturnDto()) {
            if (StringUtils.isBlank(returnDto.getPoLineNo()) || StringUtils.isBlank(returnDto.getPoProCode()) || StringUtils.isBlank(returnDto.getPoQty())
                    || StringUtils.isBlank(returnDto.getPoSiteCode()) || StringUtils.isBlank(returnDto.getPoLocationCode())
                    || StringUtils.isBlank(returnDto.getPoBatch()) || StringUtils.isBlank(returnDto.getPoRate())) {
                throw new CustomResultException(ResultEnum.CHECK_PARAMETER);
            }
        }

        //采购凭证号
        CommonUtils utils = new CommonUtils();
        String poId = utils.getPoId(dto.getClient(), "Z002", DateUtils.getCurrentDateStr("yyyy"), gaiaPoHeaderMapper, redisClient);

        //GAIA_PO_HEADER主键重复检查
        GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
        BeanUtils.copyProperties(dto, gaiaPoHeaderKey);
        gaiaPoHeaderKey.setPoId(poId);

        GaiaPoHeader gaiaPoHeaderInfo = gaiaPoHeaderMapper.selectByPrimaryKey(gaiaPoHeaderKey);
        if (null != gaiaPoHeaderInfo) {
            throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
        }

        //付款条款查询
        List<GaiaSupplierBusiness> payTerm = gaiaSupplierBusinessMapper.selectSupPayTerm(dto);

        if (payTerm.size() != 1) {
            throw new CustomResultException(ResultEnum.PAYTERM_ERROR);
        }

        //GAIA_PO_HEADER信息插入
        GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
        BeanUtils.copyProperties(dto, gaiaPoHeader);
        gaiaPoHeader.setPoId(poId);

        //采购订单类型设置
        if ("1".equals(dto.getPoSubjectType())) {
            gaiaPoHeader.setPoType(CommonEnum.PurchaseClass.PURCHASEGD.getFieldName());
        } else if ("2".equals(dto.getPoSubjectType())) {
            gaiaPoHeader.setPoType(CommonEnum.PurchaseClass.PURCHASETD.getFieldName());
        } else {
            throw new CustomResultException(ResultEnum.SUBJECTTYPE_ERROR);
        }

        gaiaPoHeader.setPoDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaPoHeader.setPoApproveStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
        gaiaPoHeader.setPoPaymentId(payTerm.get(0).getSupPayTerm());
        gaiaPoHeader.setPoCreateBy(user.getUserId());
        gaiaPoHeader.setPoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaPoHeader.setPoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));

        GaiaSupplierBusinessKey supplierBusinessKey = new GaiaSupplierBusinessKey();
        supplierBusinessKey.setClient(dto.getClient());
        supplierBusinessKey.setSupSite(dto.getPoCompanyCode());
        supplierBusinessKey.setSupSelfCode(dto.getPoSupplierId());
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(supplierBusinessKey);
        if (!ObjectUtils.isEmpty(gaiaSupplierBusiness)) {
            gaiaPoHeader.setPoLeadTime(gaiaSupplierBusiness.getSupLeadTime());
            gaiaPoHeader.setPoDeliveryMode(gaiaSupplierBusiness.getSupDeliveryMode());
            gaiaPoHeader.setPoCarrier(gaiaSupplierBusiness.getSupCarrier());
            gaiaPoHeader.setPoDeliveryTimes(gaiaSupplierBusiness.getSupDeliveryTimes());
        }
        gaiaPoHeaderMapper.insert(gaiaPoHeader);

        //GAIA_PO_ITEM信息插入
        for (ReturnDto returnList : dto.getReturnDto()) {

            //GAIA_PO_ITEM主键重复检查
            GaiaPoItemKey gaiaPoItemKey = new GaiaPoItemKey();
            gaiaPoItemKey.setClient(dto.getClient());
            gaiaPoItemKey.setPoId(poId);
            gaiaPoItemKey.setPoLineNo(returnList.getPoLineNo());

            GaiaPoItem gaiaPoItemInfo = gaiaPoItemMapper.selectByPrimaryKey(gaiaPoItemKey);

            if (null != gaiaPoItemInfo) {
                throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
            }

            //计划交货日期查询
            GaiaSupplierBusiness business = new GaiaSupplierBusiness();
            business.setClient(dto.getClient());
            business.setSupSite(returnList.getPoSiteCode());
            business.setSupSelfCode(dto.getPoSupplierId());

            List<GaiaSupplierBusiness> leadTime = gaiaSupplierBusinessMapper.selectLeadTime(business);

            if (leadTime.size() != 1 || StringUtils.isBlank(leadTime.get(0).getSupLeadTime())) {
                throw new CustomResultException(ResultEnum.LEADTIME_ERROR);
            }

            GaiaPoItem gaiaPoItem = new GaiaPoItem();
            BeanUtils.copyProperties(returnList, gaiaPoItem);

            Date delivery = DateUtils.addDate(DateUtils.stringToDate(DateUtils.getCurrentDateStr("yyyyMMdd")), Long.valueOf(leadTime.get(0).getSupLeadTime()));
            gaiaPoItem.setPoDeliveryDate(DateUtils.formatLocalDateTime(DateUtils.date2LocalDateTime(delivery), "yyyyMMdd"));
            gaiaPoItem.setClient(dto.getClient());
            gaiaPoItem.setPoId(poId);

            //订单行金额
            BigDecimal qty = new BigDecimal(returnList.getPoQty());
            // 订单单价统一可以去批次信息表里取
            GaiaBatchInfoKey gaiaBatchInfoKey = new GaiaBatchInfoKey();
            // 加盟商
            gaiaBatchInfoKey.setClient(dto.getClient());
            // 商品编码
            gaiaBatchInfoKey.setBatProCode(returnList.getPoProCode());
            // 地点
            gaiaBatchInfoKey.setBatSiteCode(returnList.getPoSiteCode());
            // 批次
            gaiaBatchInfoKey.setBatBatch(returnList.getPoBatch());
            // 批次信息表
            GaiaBatchInfo gaiaBatchInfo = gaiaBatchInfoMapper.selectByPrimaryKey(gaiaBatchInfoKey);
            if (null != gaiaBatchInfo) {
                gaiaPoItem.setPoPrice(gaiaBatchInfo.getBatPoPrice());
                BigDecimal amt = gaiaBatchInfo.getBatPoPrice().multiply(qty);
                gaiaPoItem.setPoLineAmt(amt);
            }

            //采购订单数量
            //不可大于可退货数量
            BatchStockListRequestDto batchStockListRequestDto = new BatchStockListRequestDto();
            batchStockListRequestDto.setClient(dto.getClient());
            batchStockListRequestDto.setBatSiteCode(returnList.getPoSiteCode());
            batchStockListRequestDto.setBatLocationCode(returnList.getPoLocationCode());
            batchStockListRequestDto.setBatProCode(returnList.getPoProCode());
            batchStockListRequestDto.setBatSupplierCode(dto.getPoSupplierId());
            batchStockListRequestDto.setBatBatch(returnList.getPoBatch());

            List<BatchStockListResponseDto> batchStockListResponseDto = changePurchaseService.selectBatchStockList(batchStockListRequestDto);

            for (BatchStockListResponseDto stockListResponseDto : batchStockListResponseDto) {
                if (qty.compareTo(stockListResponseDto.getRetractableNum()) == 1) {
                    throw new CustomResultException(ResultEnum.QTY_ERROR);
                }
            }
            gaiaPoItem.setPoQty(qty);

            gaiaPoItem.setPoLineDelete("0");
            gaiaPoItem.setPoCompleteFlag("0");

            gaiaPoItemMapper.insert(gaiaPoItem);
        }
    }
}
