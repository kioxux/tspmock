package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaPayingItem;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author staxc
 * @Date 2020/10/26 15:02
 * @desc
 */
@Data
@ApiModel(value = "支付明细传出参数")
public class GetPayingItemDetailDTO extends GaiaPayingItem {

    /**
     * 纳税主体名称
     */
    private String ficoTaxSubjectName;

    /**
     * 统一社会信用代码
     */
    private String ficoSocialCreditCode;

    /**
     * 开户行名称
     */
    private String ficoBankName;

    /**
     * 开户行账号
     */
    private String ficoBankNumber;

    /**
     * 地址
     */
    private String ficoCompanyAddress;

    /**
     * 电话号码
     */
    private String ficoCompanyPhoneNumber;

    /**
     * 发票类型
     */
    private String ficoInvoiceType;

    /**
     * 发票订单号
     */
    private String ficoOrderNo;

    /**
     * 操作者
     */
    private String ficoOperator;

}
