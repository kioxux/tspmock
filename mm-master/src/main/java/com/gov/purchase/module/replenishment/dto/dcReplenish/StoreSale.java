package com.gov.purchase.module.replenishment.dto.dcReplenish;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2022.01.07
 */
@Data
public class StoreSale {
    /**
     * 商品编号
     */
    private String gssdProId;
    private String type7;
    private BigDecimal storeSaleQty7;
    private String type30;
    private BigDecimal storeSaleQty30;
    private String type90;
    private BigDecimal storeSaleQty90;
}
