package com.gov.purchase.module.base.dto;

import com.gov.purchase.entity.GaiaBatchUpdateLog;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.20
 */
@Data
public class BatchResult extends GaiaBatchUpdateLog {
    /**
     * 用户名
     */
    private String userName;
}
