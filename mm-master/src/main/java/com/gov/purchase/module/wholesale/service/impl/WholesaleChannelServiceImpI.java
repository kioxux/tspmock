package com.gov.purchase.module.wholesale.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaWholesaleChannelMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.wholesale.dto.BatchProcessWholeSaleChannelDetailDTO;
import com.gov.purchase.module.wholesale.dto.WholeSaleChannelDetailDTO;
import com.gov.purchase.module.wholesale.dto.WholeSaleChannelDetailVo;
import com.gov.purchase.module.wholesale.service.WholesaleChannelService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: mm
 * @author: yzf
 * @create: 2021-11-01 13:43
 */
@Service
public class WholesaleChannelServiceImpI implements WholesaleChannelService {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaWholesaleChannelMapper gaiaWholesaleChannelMapper;


    /**
     * 获取当前用户下的仓库列表
     *
     * @return
     */
    @Override
    public List<GaiaDcData> getDcData() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaDcDataMapper.getDcList(user.getClient());
    }

    /**
     * 查询渠道
     *
     * @param dcCode
     * @param channelName
     * @param proCode
     * @param cusCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageInfo<WholeSaleChannelDetailDTO> queryWholeSaleChannelData(String dcCode, String channelName, String proCode, String cusCode, Integer pageNum, Integer pageSize) {
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(pageNum, pageSize);
        List<WholeSaleChannelDetailDTO> list = gaiaWholesaleChannelMapper.queryWholeSaleChannelData(user.getClient(), dcCode, proCode, cusCode, channelName);
        PageInfo<WholeSaleChannelDetailDTO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * 查询批发渠道已存在的商品或用户
     *
     * @param dcCode
     * @param chaCode
     * @param type
     * @return
     */
    @Override
    public List<WholeSaleChannelDetailVo> queryWholeSaleChannelDetailByType(String dcCode, String chaCode, String type) {
        TokenUser user = feignService.getLoginInfo();
        List<WholeSaleChannelDetailVo> lists = gaiaWholesaleChannelMapper.queryWholeSaleChannelDetailByType(
                user.getClient(), dcCode, chaCode, type
        );
        return lists;
    }

    /**
     * 新增批发渠道
     *
     * @param gaiaWholesaleChannel
     * @return
     */
    @Override
    public Result insertWholesaleChannel(GaiaWholesaleChannel gaiaWholesaleChannel) {
        // 判断仓库编码非空
        if (StringUtils.isBlank(gaiaWholesaleChannel.getGwcDcCode())) {
            return ResultUtil.error(ResultEnum.WHOLESALE_DC_CODE_EMPTY);
        }
        TokenUser user = feignService.getLoginInfo();
        gaiaWholesaleChannel.setClient(user.getClient());
        // 查询当前批发渠道列表
        List<GaiaWholesaleChannel> chaCodes = gaiaWholesaleChannelMapper.queryWholeSaleChannelChaCode(user.getClient());
        // 渠道自编码： 已有最大值加1
        gaiaWholesaleChannel.setGwcChaCode(chaCodes.size() > 0 ? String.valueOf(Integer.parseInt(chaCodes.get(0).getGwcChaCode()) + 1) : "1");
        gaiaWholesaleChannel.setGwcCreDate(DateUtils.getCurrentDateStrYYMMDD());
        gaiaWholesaleChannel.setGwcCreTime(DateUtils.getCurrentTimeStrHHMMSS());
        gaiaWholesaleChannel.setGwcCreId(user.getUserId());
        // 新增批发渠道
        return ResultUtil.success(gaiaWholesaleChannelMapper.insert(gaiaWholesaleChannel));
    }

    /**
     * 更新批发渠道
     *
     * @param gaiaWholesaleChannel
     * @return
     */
    @Override
    public Result updateWholesaleChannel(GaiaWholesaleChannel gaiaWholesaleChannel) {
        // 判断仓库编码非空
        if (StringUtils.isBlank(gaiaWholesaleChannel.getGwcDcCode())) {
            return ResultUtil.error(ResultEnum.WHOLESALE_DC_CODE_EMPTY);
        }
        // 判断批发渠道编码非空
        if (StringUtils.isBlank(gaiaWholesaleChannel.getGwcChaCode())) {
            return ResultUtil.error(ResultEnum.WHOLESALE_CHA_CODE_EMPTY);
        }
        TokenUser user = feignService.getLoginInfo();
        gaiaWholesaleChannel.setClient(user.getClient());
        gaiaWholesaleChannel.setGwcModiDate(DateUtils.getCurrentDateStrYYMMDD());
        gaiaWholesaleChannel.setGwcModiTime(DateUtils.getCurrentTimeStrHHMMSS());
        gaiaWholesaleChannel.setGwcModiId(user.getUserId());
        // 更新批发渠道信息
        return ResultUtil.success(gaiaWholesaleChannelMapper.updateByPrimaryKey(gaiaWholesaleChannel));
    }

    /**
     * 删除批发渠道
     *
     * @param gaiaWholesaleChannel
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result delWholesaleChannel(GaiaWholesaleChannel gaiaWholesaleChannel) {
        // 判断仓库编码非空
        if (StringUtils.isBlank(gaiaWholesaleChannel.getGwcDcCode())) {
            return ResultUtil.error(ResultEnum.WHOLESALE_DC_CODE_EMPTY);
        }
        // 判断批发渠道编码非空
        if (StringUtils.isBlank(gaiaWholesaleChannel.getGwcChaCode())) {
            return ResultUtil.error(ResultEnum.WHOLESALE_CHA_CODE_EMPTY);
        }
        TokenUser user = feignService.getLoginInfo();
        GaiaWholesaleChannelKey gaiaWholesaleChannelKey = new GaiaWholesaleChannelKey();
        gaiaWholesaleChannelKey.setClient(user.getClient());
        gaiaWholesaleChannelKey.setGwcDcCode(gaiaWholesaleChannel.getGwcDcCode());
        gaiaWholesaleChannelKey.setGwcChaCode(gaiaWholesaleChannel.getGwcChaCode());
        // 删除批发渠道信息
        gaiaWholesaleChannelMapper.deleteByPrimaryKey(gaiaWholesaleChannelKey);
        // 删除批发渠道下的所有明细数据
        gaiaWholesaleChannelMapper.deleteWholeSaleChannelDetail(user.getClient(), gaiaWholesaleChannel.getGwcDcCode(),
                gaiaWholesaleChannel.getGwcChaCode(), null);
        return ResultUtil.success();
    }

    /**
     * 批量保存明细
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveBatchWholesaleChannelDetail(BatchProcessWholeSaleChannelDetailDTO dto) {
        // 判断仓库编码非空
        if (StringUtils.isBlank(dto.getDcCode())) {
            return ResultUtil.error(ResultEnum.WHOLESALE_DC_CODE_EMPTY);
        }
        // 判断渠道编码非空
        if (StringUtils.isBlank(dto.getChaCode())) {
            return ResultUtil.error(ResultEnum.WHOLESALE_CHA_CODE_EMPTY);
        }
        // 判断编码类型非空
        if (StringUtils.isBlank(dto.getType())) {
            return ResultUtil.error(ResultEnum.WHOLESALE_TYPE_EMPTY);
        }
        // 判断明细列表非空
        if (CollectionUtils.isEmpty(dto.getChannelPro())) {
            return ResultUtil.success();
        }
        TokenUser user = feignService.getLoginInfo();
        //根据自编码类型全删除（0-商品   1-用户）
        gaiaWholesaleChannelMapper.deleteWholeSaleChannelDetail(
                user.getClient(),
                dto.getDcCode(),
                dto.getChaCode(),
                dto.getType()
        );
        dto.getChannelPro().forEach(item -> {
            item.setClient(user.getClient());
            item.setGwcdDcCode(dto.getDcCode());
            item.setGwcdChaCode(dto.getChaCode());
            item.setGwcdType(dto.getType());
            item.setGwcdCreDate(DateUtils.getCurrentDateStrYYMMDD());
            item.setGwcdCreTime(DateUtils.getCurrentTimeStrHHMMSS());
            item.setGwcdCreId(user.getUserId());
        });
        //批量插入批发渠道明细数据
        gaiaWholesaleChannelMapper.insertBatchWholeSaleChannelDetail(dto.getChannelPro());
        return ResultUtil.success();
    }

    /**
     * 根据地点查询所有客户
     *
     * @param dcCode
     * @return
     */
    @Override
    public List<GaiaCustomerBusiness> queryCustomerBusiness(String dcCode) {
        TokenUser user = feignService.getLoginInfo();
        return gaiaWholesaleChannelMapper.queryCustomerBusiness(user.getClient(), dcCode);
    }


    /**
     * 根据地点查询所有商品
     *
     * @param dcCode
     * @return
     */
    @Override
    public List<GaiaProductBusiness> queryProductBusiness(String dcCode) {
        TokenUser user = feignService.getLoginInfo();
        return gaiaWholesaleChannelMapper.queryProductBusiness(user.getClient(), dcCode);
    }
}
