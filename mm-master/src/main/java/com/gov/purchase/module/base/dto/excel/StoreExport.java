package com.gov.purchase.module.base.dto.excel;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.21
 */
@Data
public class StoreExport extends GaiaStoreData {

    /**
     * 关联门店名
     */
    private String stoRelationStoreName;

    /**
     * 省
     */
    private String stoProvinceName;

    /**
     * 市
     */
    private String stoCityName;

    /**
     * 区
     */
    private String stoDistrictName;

    /**
     * 委托配送公司
     */
    private String decName;

    /**
     * 连锁总部
     */
    private String stoChainHeadName;

    /**
     * 纳税主体
     */
    private String stoTaxSubjectName;

    /**
     * 加盟商名称
     */
    private String francName;
}

