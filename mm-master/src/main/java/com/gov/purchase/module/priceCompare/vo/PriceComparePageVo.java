package com.gov.purchase.module.priceCompare.vo;

import com.gov.purchase.common.entity.PriceCompareBase;
import com.gov.purchase.entity.GaiaSspPriceCompare;
import lombok.Data;

@Data
public class PriceComparePageVo extends GaiaSspPriceCompare {


    /**
     * 已反馈比价供应商数量
     */
    private String feedbackSupplierNum;

    /**
     * 参与比价供应商数
     */
    private String supplierTotal;


}
