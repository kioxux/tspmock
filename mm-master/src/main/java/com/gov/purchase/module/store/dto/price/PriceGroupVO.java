package com.gov.purchase.module.store.dto.price;

import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;

import java.util.List;

/**
 * @description: 价格组
 * @author: yzf
 * @create: 2021-12-17 09:47
 */
@Data
public class PriceGroupVO {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 价格组id
     */
    private String prcGroupId;

    /**
     * 价格组名称
     */
    private String prcGroupName;

    /**
     * 价格组门店列表
     */
    private List<GaiaStoreData> storeList;
}
