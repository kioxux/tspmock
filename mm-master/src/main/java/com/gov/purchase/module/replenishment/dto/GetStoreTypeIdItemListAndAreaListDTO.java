package com.gov.purchase.module.replenishment.dto;

import lombok.Data;

import java.util.List;

/**
 * @author zhoushuai
 * @date 2021-08-10 11:06
 */
@Data
public class GetStoreTypeIdItemListAndAreaListDTO {

    private List<Item> storeTypeIdItemList;
    private List<Item> storeAreaList;

    @Data
    public static class Item {
        private String id;
        private String name;
    }

}
