package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductBusiness;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class ProEditRequestDTO extends GaiaProductBusiness {


    @NotBlank(message = "地点不能为空")
    private String proSite;

    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;

    @NotBlank(message = "通用名称不能为空")
    private String proCommonname;

    @NotBlank(message = "商品描述不能为空")
    private String proDepict;

    @NotBlank(message = "助记码不能为空")
    private String proPym;

    //    @NotBlank(message = "商品名不能为空")
    private String proName;

    @NotBlank(message = "规格不能为空")
    private String proSpecs;

    @NotBlank(message = "计量单位不能为空")
    private String proUnit;

    @NotBlank(message = "生产企业不能为空")
    private String proFactoryName;

    @NotBlank(message = "进项税率不能为空")
    private String proInputTax;

    @NotBlank(message = "销项税率不能为空")
    private String proOutputTax;

    @NotBlank(message = "贮存条件不能为空")
    private String proStorageCondition;

    @NotBlank(message = "商品仓储分区不能为空")
    private String proStorageArea;

    @NotBlank(message = "中包装量不能为空")
    private String proMidPackage;

    @Length(max = 8, message = "批准文号批准日期最大长度为8")
    private String proRegisterDate;

    @Length(max = 8, message = "批准文号失效日期最大长度为8")
    private String proRegisterExdate;

}
