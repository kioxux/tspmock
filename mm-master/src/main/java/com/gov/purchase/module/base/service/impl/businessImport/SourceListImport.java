package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.businessImport.SourceList;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class SourceListImport extends BusinessImport {

    /**
     * 加盟商key
     */
    private final static String CLIENT = "client";

    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;

    @Resource
    private GaiaSourceListMapper gaiaSourceListMapper;

    @Resource
    private FeignService feignService;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        List<String> errorList = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();

        List<GaiaSourceList> resultList = new ArrayList<>();

        // 批量导入
        try {
            // 验证excel  数据是否正确
            for (Integer key : map.keySet()) {
                SourceList sourceList = (SourceList) map.get(key);

                // 验证商品存在性
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(user.getClient(), sourceList.getSouSiteCode(), sourceList.getSouProCode());
                if (gaiaProductBusiness == null) {
                    // 第几行商品不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "商品"));
                } else if (!gaiaProductBusiness.getProStatus().equals("0")) {
                    // 第几行商品不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "商品"));
                }

                // 门店主键对象
                GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                // 加盟商
                gaiaStoreDataKey.setClient(user.getClient());
                // 门店编码
                gaiaStoreDataKey.setStoCode(sourceList.getSouSiteCode());
                // 门店获取
                GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                // 门店号验证
                if (gaiaStoreData == null) {
                    GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
                    gaiaDcDataKey.setClient(user.getClient());
                    gaiaDcDataKey.setDcCode(sourceList.getSouSiteCode());
                    GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
                    if (gaiaDcData == null) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "工厂"));
                    } else if (!gaiaDcData.getDcStatus().equals("0")) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "工厂"));
                    }
                } else if (!gaiaStoreData.getStoStatus().equals("0")) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "工厂"));
                }


                //验证供应商存在性
                GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectSupplierStatus(user.getClient(), sourceList.getSouSupplierId(), sourceList.getSouSiteCode());
                if (gaiaSupplierBusiness == null) {
                    // 第几行供应商不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "供应商"));
                } else if (!gaiaSupplierBusiness.getSupStatus().equals("0")) {
                    // 第几行供应商不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "供应商"));
                }

                //有效期至不能早于有效期起
                if (DateUtils.parseLocalDate(sourceList.getSouEffectFrom(), "yyyyMMdd").compareTo(DateUtils.parseLocalDate(sourceList.getSouEffectEnd(), "yyyyMMdd")) > 0) {
                    errorList.add(MessageFormat.format(ResultEnum.E0150.getMsg(), key + 1));
                }

            }

            // 如果验证有错误则 把错误信息返回前台
            if (errorList.size() > 0) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }

            for (Integer key : map.keySet()) {
                SourceList sourceList = (SourceList) map.get(key);

                GaiaSourceList gaiaSourceListKey = new GaiaSourceList();
                BeanUtils.copyProperties(sourceList, gaiaSourceListKey);
                gaiaSourceListKey.setClient(user.getClient());
                // 查询最大的货源清单 如果为空则新增 反之更新
                GaiaSourceList gaiaSourceList = gaiaSourceListMapper.getMaxSourceList(gaiaSourceListKey);
                if (gaiaSourceList == null) {
                    gaiaSourceList = new GaiaSourceList();
                    BeanUtils.copyProperties(sourceList, gaiaSourceList);
                    // 获取最新货源清单编码
                    // 设置主键
                    GaiaSourceListKey sourceListKey = new GaiaSourceListKey();
                    gaiaSourceList.setClient(user.getClient());
                    BeanUtils.copyProperties(gaiaSourceList, sourceListKey);
                    gaiaSourceList.setSouCreateType("1"); //创建类型  1-手动
                    gaiaSourceList.setSouCreateBy(user.getUserId());  // 创建人
                    gaiaSourceList.setSouCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    gaiaSourceList.setSouCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    String souListCode = gaiaSourceListMapper.getNewSouListCode(sourceListKey);
                    if (StringUtils.isBlank(souListCode)) {
                        gaiaSourceList.setSouListCode("00001");
                    } else {
                        gaiaSourceList.setSouListCode(souListCode);
                    }
                    gaiaSourceListMapper.insert(gaiaSourceList);
                } else {
                    BeanUtils.copyProperties(sourceList, gaiaSourceList);
                    gaiaSourceList.setSouUpdateBy(user.getUserId());  // 创建人
                    gaiaSourceList.setSouUpdateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    gaiaSourceList.setSouUpdateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    gaiaSourceListMapper.updateByPrimaryKeySelective(gaiaSourceList);
                }
            }

            return ResultUtil.success();

        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

    }
}
