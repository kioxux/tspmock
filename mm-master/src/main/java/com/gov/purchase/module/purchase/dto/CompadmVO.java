package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;

import java.util.List;

/**
 * @description: 连锁总部及其下的门店列表
 * @author: yzf
 * @create: 2021-11-15 10:42
 */
@Data
public class CompadmVO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 连锁总部id
     */
    private String compadmId;

    /**
     * 连锁总部名称
     */
    private String compadmName;

}
