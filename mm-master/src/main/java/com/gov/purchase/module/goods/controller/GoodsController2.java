package com.gov.purchase.module.goods.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.module.goods.dto.*;
import com.gov.purchase.module.goods.service.GoodsInfoService;
import com.gov.purchase.module.goods.service.GoodsService2;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Api(tags = "药品首营")
@RestController
@RequestMapping("goods")
public class GoodsController2 {

    @Resource
    private GoodsService2 goodsService;
    @Resource
    private GoodsInfoService goodsInfoService;
    @Resource
    private OperationFeignService operationFeignService;

    @Log("商品首营_回车查询")
    @ApiOperation("商品首营_回车查询")
    @PostMapping("getProBasicList")
    public Result getProBasicList(@Valid @RequestBody GetProBasicListRequestDTO dto) {
        return ResultUtil.success(goodsService.getProBasicList(dto));
    }

    @Log("商品首营_获取商品自编码最大值")
    @ApiOperation("商品首营_获取商品自编码最大值")
    @PostMapping("getMaxProSelfCode")
    public Result getMaxProSelfCode(@Valid @RequestBody GetMaxProSelfCodeDTO dto) {
        return ResultUtil.success(goodsService.getMaxProSelfCode(dto));
    }

    @Log("商品主号码段配置表查询")
    @ApiOperation("商品主号码段配置表查询")
    @PostMapping("getProductNumber")
    public Result getProductNumber() {
        return ResultUtil.success(goodsService.getProductNumber());
    }

    @Log("商品首营_商品保存1")
    @ApiOperation("商品首营_商品保存1")
    @PostMapping("saveCompProList")
    public Result saveCompProList(@Valid @RequestBody List<SaveGaiaProductBusiness> list) {
        return ResultUtil.success(goodsService.saveCompProList(list));
    }

    @Log("商品首营_商品保存2")
    @ApiOperation("商品首营_商品保存2")
    @PostMapping("saveBasicProList")
    public Result saveBasicProList(@Valid @RequestBody SaveBasicProListRequestDTO dto) {
        Map<String, Object> map = goodsService.saveBasicProList(dto);
        try {
            // 第三方 gys-operation
            if (map != null && !map.isEmpty()) {
                List<String> proSiteList = (List<String>) map.get("proSiteList");
                List<String> proSelfCodeList = (List<String>) map.get("proSelfCodeList");
                if (CollectionUtils.isNotEmpty(proSiteList) && CollectionUtils.isNotEmpty(proSelfCodeList)) {
                    operationFeignService.multipleStoreProductSync(map.get("client").toString(), proSiteList, proSelfCodeList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultUtil.success();
    }

    @Log("商品分类下拉菜单")
    @ApiOperation("商品分类下拉菜单")
    @GetMapping("getClassList")
    public Result getClassList() {
        return ResultUtil.success(goodsService.getClassList());
    }

    @Log("供货单位")
    @ApiOperation("供货单位")
    @GetMapping("getSupplierList2")
    public Result getSupplierList() {
        return ResultUtil.success(goodsService.getSupplierList());
    }

    @Log("默认选中机构")
    @ApiOperation("默认选中机构")
    @GetMapping("getDefaultSite")
    public Result getDefaultSite() {
        return ResultUtil.success(goodsService.getDefaultSite());
    }

    @Log("默认选中机构")
    @ApiOperation("默认选中机构")
    @GetMapping("getDefaultSite2")
    public Result getDefaultSite2() {
        return ResultUtil.success(goodsService.getDefaultSite2());
    }

    @Log("商品查询及维护_商品列表")
    @ApiOperation("商品查询及维护_商品列表")
    @PostMapping("getProList")
    public Result getProList(@Valid @RequestBody GetProListRequestDTO dto) {
        return ResultUtil.success(goodsService.getProList(dto));
    }

    @Log("商品数据导出")
    @ApiOperation("商品数据导出")
    @PostMapping("exportProList")
    public Result exportProList(@Valid @RequestBody GetProListRequestDTO dto) {
        return goodsService.exportProList(dto);
    }

    @Log("商品查询及维护_商品列表批量保存")
    @ApiOperation("商品查询及维护_商品列表批量保存")
    @PostMapping("proEditList")
    public Result proEditList(@RequestBody @Valid ProEditListVO vo) {
        Map<String, Object> map = goodsService.proEditList(vo.getBusinessList(), vo.getRemarks(), vo.getEditNoWws());
        try {
            // 第三方 gys-operation
            if (map != null && !map.isEmpty()) {
                List<String> proSiteList = (List<String>) map.get("proSiteList");
                List<String> proSelfCodeList = (List<String>) map.get("proSelfCodeList");
                if (CollectionUtils.isNotEmpty(proSiteList) && CollectionUtils.isNotEmpty(proSelfCodeList)) {
                    operationFeignService.multipleStoreProductSync(map.get("client").toString(), proSiteList, proSelfCodeList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultUtil.success();
    }

    @Log("商品详情")
    @ApiOperation("商品详情")
    @PostMapping("getProDetails")
    public Result getProDetails(@RequestJson(value = "proSite") String proSite,
                                @RequestJson(value = "proSelfCode") String proSelfCode) {
        return ResultUtil.success(goodsService.getProDetails(proSite, proSelfCode));
    }

    @Log("商品详情编辑")
    @ApiOperation("商品详情编辑")
    @PostMapping("proEdit")
    public Result proEdit(@RequestBody @Valid ProEditVO vo) {
        Map<String, Object> map = goodsService.proEdit(vo.getBusiness(), vo.getRemarks(), vo.getEditNoWws());
        try {
            // 第三方 gys-operation
            if (map != null && !map.isEmpty()) {
                List<String> proSiteList = (List<String>) map.get("proSiteList");
                List<String> proSelfCodeList = (List<String>) map.get("proSelfCodeList");
                if (CollectionUtils.isNotEmpty(proSiteList) && CollectionUtils.isNotEmpty(proSelfCodeList)) {
                    operationFeignService.multipleStoreProductSync(map.get("client").toString(), proSiteList, proSelfCodeList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResultUtil.success();
    }

    @Log("商品主数据自定义字段名称")
    @ApiOperation("商品主数据自定义字段名称")
    @PostMapping("getProductZdy")
    public Result getProductZdy(@RequestJson(value = "client") String client,
                                @RequestJson(value = "proSite", required = false) String proSite) {
        return ResultUtil.success(goodsService.getProductZdy(client, proSite));
    }

    @Log("该商品在首营表中是否存在")
    @ApiOperation("该商品在首营表中是否存在")
    @PostMapping("getProductGspinfoExit")
    public Result getProductGspinfoExit(@RequestBody @Valid GaiaProductGspinfoKeyDTO dto) {
        return ResultUtil.success(goodsService.getProductGspinfoExit(dto));
    }

    @Log("补充首营")
    @ApiOperation("补充首营")
    @PostMapping("replenishProductGsp")
    public Result replenishProductGsp(@RequestBody @Valid GaiaProductGspinfoKeyDTO dto) {
        goodsService.replenishProductGsp(dto);
        return ResultUtil.success();
    }

    @Log("经营类别下拉列表")
    @ApiOperation("经营类别下拉列表")
    @PostMapping("getJyfwooDataList")
    public Result getJyfwooDataList(@RequestJson(value = "jyfwName", required = false) String jyfwName) {
        return ResultUtil.success(goodsService.getJyfwooDataList(jyfwName));
    }

    @Log("商品、供应商、客户修改是否发起工作流开关")
    @ApiOperation("商品、供应商、客户修改是否发起工作流开关")
    @PostMapping("getCountNeedWorkFlowDC")
    public Result getCountNeedWorkFlowDC() {
        return ResultUtil.success(goodsService.getCountNeedWorkFlowDC());
    }

    @Log("全局GSP开关")
    @ApiOperation("全局GSP开关")
    @PostMapping("getGlobalGspConfigureList")
    public Result getGlobalGspConfigureList(@RequestJson(value = "gggcSite", required = false) String gggcSite,
                                            @RequestJson(value = "gggcCode", required = false) String gggcCode) {
        return ResultUtil.success(goodsService.getGlobalGspConfigureList(gggcSite, gggcCode));
    }

    @Log("首营必输验证开关")
    @ApiOperation("首营必输验证开关")
    @PostMapping("getGspRequire")
    public Result getGspRequire(@RequestJson(value = "gggcCode") String gggcCode) {
        return ResultUtil.success(goodsService.getGspRequire(gggcCode));
    }

    @Log("指定加盟商_商品排除配置表默认值")
    @ApiOperation("指定加盟商_商品排除配置表默认值")
    @PostMapping("productPositionExcludeDefaultValue")
    public Result productPositionExcludeDefaultValue(@RequestJson("client") String client) {
        goodsService.productPositionExcludeDefaultValue(client);
        return ResultUtil.success();
    }

    @Log("获取铺货计划开关")
    @ApiOperation("获取铺货计划开关")
    @PostMapping("getSwitchForPH")
    public Result getSwitchForPH(@RequestJson("client") String client) {
        return ResultUtil.success(goodsService.getSwitchForPH(client));
    }

    @ApiOperation("商品首营")
    @PostMapping("firstSaleGoods")
    public Result firstSaleGoods(@RequestBody SaveBasicProListRequestDTO dto) {
        goodsInfoService.firstSaleGoods(dto);
        return ResultUtil.success();
    }
}
