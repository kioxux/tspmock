package com.gov.purchase.module.store.service;

/***
 * @desc:  客户或者门店固定到期日变更补货标志（欠款标志）
 * @author: wangchuan
 * @createTime: 2021/12/09 16:23
 **/
public interface GdzqJobService {

    void autoExecModArrearsFlag();

}
