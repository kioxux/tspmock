package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductBusiness;
import lombok.Data;

import java.util.List;

/**
 * @description: 药品质量档案
 * @author: yzf
 * @create: 2021-11-08 17:08
 */
@Data
public class DrugQualityVO extends GaiaProductBusiness {
    /**
     * 有效期
     */
    private String validDate;
    /**
     * 首次进货日期
     */
    private String firstPurchaseDate;
    /**
     * 进货列表
     */
    private List<GoodsQualityDTO> list;
    /**
     * 当前账号人
     */
    private String userName;
}
