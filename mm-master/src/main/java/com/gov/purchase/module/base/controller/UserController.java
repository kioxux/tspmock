package com.gov.purchase.module.base.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaFranchisee;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaFranchiseeMapper;
import com.gov.purchase.module.base.service.FeignService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.18
 */
@Api(tags = "用户信息")
@RestController
@RequestMapping("user")
public class UserController {

    @Resource
    private FeignService feignService;

    @Resource
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;


    /**
     * 获取用户信息
     *
     * @return
     */
    @Log("获取用户信息")
    @ApiOperation("获取用户信息")
    @GetMapping("getUserInfo")
    public Result getUserInfo() {
        TokenUser user = feignService.getLoginInfo();
        GaiaFranchisee gaiaFranchisee = gaiaFranchiseeMapper.selectByPrimaryKey(user.getClient());
        user.setFrancName(gaiaFranchisee.getFrancName());
        return ResultUtil.success(user);
    }

    /**
     * 获取用户加盟商信息
     *
     * @return
     */
    @Log("获取用户加盟商信息")
    @ApiOperation("获取用户加盟商信息")
    @GetMapping("getFranchisee")
    public Result getFranchisee() {
        // 现用户数据为 yml 文件配置，并非真正的登录用户，后期接入
        return ResultUtil.success(feignService.getLoginInfo().getClient());
    }
}