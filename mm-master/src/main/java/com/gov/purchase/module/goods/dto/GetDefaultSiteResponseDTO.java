package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaCompadm;
import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class GetDefaultSiteResponseDTO {
    /**
     * 连锁总部列表
     */
    private List<GaiaCompadmDTO> compadmList;
    /**
     * 门店列表
     */
    private List<GaiaStoreData> storeDataList;

    @Data
    @EqualsAndHashCode
    public static class GaiaCompadmDTO extends GaiaCompadm {
        /**
         * 与连锁关联的门店列表
         */
        private List<GaiaStoreData> storeList;
    }

    /**
     * 供应商首营机构
     */
    private List<GaiaStoreData> gspInfoSiteList;
}
