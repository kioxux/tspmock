package com.gov.purchase.module.base.dto.businessImport;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StoreDistributionResp {

    /**
     * 商品编码
     */
    private String proCode;

    /**
     * 加盟商编号
     */
    private String client;

    /**
     * DC编号
     */
    private String batSiteCode;

    /**
     * DC名称
     */
    private String dcName;

    /**
     * 商品自编码
     */
    private String batProCode;

    /**
     * 商品名
     */
    private String proName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 单位
     */
    private String proUnit;

    /**
     * 单位名称
     */
    private String unitName;
    /**
     * 库存地点
     */
    private String batLocationCode;

    /**
     * 可用库存数量
     */
    private BigDecimal batNormalQty;

    /**
     * 门店编号
     */
    private String stoCode;

    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 铺货数量
     */
    private BigDecimal stoNeedQty;

    /**
     * 铺货日期
     */
    private String stoNeedArrivalDate;

    /**
     * 批次
     */
    private String stoNeedBatch;

    /**
     * 备注
     */
    private String stoNeedRemark;


}
