package com.gov.purchase.module.replenishment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class GetBatchNumberListRequestDTO {

    @NotBlank(message = "门店编码不能为空")
    private String stoCode;
    @NotBlank(message = "商店自编码不能为空")
    private String proSeflCode;
    /**
     * 加盟商
     */
    private String client;


}
