package com.gov.purchase.module.base.service.impl;

import com.alibaba.fastjson.JSON;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.feign.WmsFeign;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.GaiaFranchiseeResponseDto;
import com.gov.purchase.module.base.service.BaseService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.26
 */
@Slf4j
@Service
public class BaseServiceImpl implements BaseService {

    @Resource
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaDeliveryCompanyMapper gaiaDeliveryCompanyMapper;
    @Resource
    private GaiaCompadmMapper gaiaCompadmMapper;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaUserDataMapper gaiaUserDataMapper;
    @Resource
    private GaiaProfitLevelMapper gaiaProfitLevelMapper;
    @Resource
    private GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Resource
    private GaiaMaterialAssessMapper gaiaMaterialAssessMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private WmsFeign wmsFeign;
    @Resource
    private GaiaProductClassMapper gaiaProductClassMapper;

    /**
     * 加盟商集合
     *
     * @return
     */
    @Override
    public List<GaiaFranchiseeResponseDto> getFranchiseeList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaFranchiseeMapper.franchiseeList(user.getClient());
    }

    /**
     * 实体集合
     *
     * @param client 加盟商
     * @param type   类型 1:门店 2:供应商 3:客户
     * @return
     */
    @Override
    public Result mainStore(String client, String type) {
        // 1:门店
        if (CommonEnum.StoreType.STORE.getCode().equals(type)) {
            return ResultUtil.success(gaiaStoreDataMapper.mainStore(client));
        }
        // 2:供应商
        if (CommonEnum.StoreType.SUPPLIER.getCode().equals(type)) {
            return ResultUtil.success(gaiaSupplierBusinessMapper.mainStore(client));
        }
        // 3:客户
        if (CommonEnum.StoreType.CUSTOMER.getCode().equals(type)) {
            return ResultUtil.success(gaiaCustomerBusinessMapper.mainStore(client));
        }
        return null;
    }

    /**
     * 配送中心列表
     *
     * @param client 加盟商
     * @return
     */
    @Override
    public Result getDcDataList(String client) {
        client = StringUtils.isBlank(client) ? feignService.getLoginInfo().getClient() : client;
        return ResultUtil.success(gaiaDcDataMapper.mainStore(client));
    }

    /**
     * 委托配送公司
     *
     * @return
     */
    @Override
    public Result getDeliveryCompanyList() {
        return ResultUtil.success(gaiaDeliveryCompanyMapper.mainStore());
    }

    /**
     * DC补货下位集合
     *
     * @return
     */
    @Override
    public Result getReplenishDcDataList() {
        TokenUser user = feignService.getLoginInfo();
        return ResultUtil.success(gaiaDcDataMapper.getReplenishDcDataList(user.getClient(), user.getUserId()));
    }

    /**
     * 连锁公司总部列表
     *
     * @param client 加盟商
     * @return
     */
    @Override
    public Result getCompadmList(String client) {
        List<GaiaCompadm> list = gaiaCompadmMapper.selectCompadmListByClien(client);
        return ResultUtil.success(list);
    }

    /**
     * 连锁公司总部列表 需要排除已使用的连锁公司
     *
     * @param client 加盟商
     * @param dcCode
     * @return
     */
    @Override
    public Result getCompadmListDC(String client, String dcCode) {
        List<GaiaCompadm> list = gaiaCompadmMapper.selectCompadmListByClienDC(client, dcCode);
        return ResultUtil.success(list);
    }

    /**
     * 用户列表
     *
     * @return
     */
    @Override
    public Result getUserListByClient() {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaUserData> list = gaiaUserDataMapper.selectByClient(user.getClient());
        return ResultUtil.success(list);
    }

    @Override
    public Result getCompadmAndCompadmWms(String client, String dcCode) {
        List<GaiaCompadm> list = gaiaCompadmMapper.getCompadmAndCompadmWms(client, dcCode);
        return ResultUtil.success(list);
    }

    /**
     * 所有加盟商列表
     *
     * @return
     */
    @Override
    public List<GaiaFranchiseeResponseDto> getAllFranchiseeList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaFranchiseeMapper.franchiseeList(null);
    }

    /**
     * 设置商品毛利
     *
     * @param client
     * @param proSite
     * @param proSelfCode
     */
    @Override
    public void setProProfit(String client, String proSite, String proSelfCode,
                             List<GaiaTaxCode> gaiaTaxCodeList, List<GaiaSdProductPrice> gaiaSdProductPriceList,
                             List<GaiaMaterialAssess> gaiaMaterialAssessList, List<GaiaProductBusiness> gaiaProductBusinessList,
                             List<GaiaStoreData> gaiaStoreDataList, List<GaiaProfitLevel>  gaiaProfitLevelList) {
        try {
            if (StringUtils.isBlank(client) || StringUtils.isBlank(proSite) || StringUtils.isBlank(proSelfCode)) {
                return;
            }
            if (CollectionUtils.isEmpty(gaiaProfitLevelList)) {
                return;
            }
            GaiaSdProductPrice gaiaSdProductPrice = null;
            if (CollectionUtils.isNotEmpty(gaiaSdProductPriceList)) {
                gaiaSdProductPrice = gaiaSdProductPriceList.stream().filter(
                        item -> client.equals(item.getClient()) && proSelfCode.equals(item.getGsppProId()) && proSite.equals(item.getGsppBrId())
                ).findFirst().orElse(null);
            } else {
                GaiaSdProductPriceKey gaiaSdProductPriceKey = new GaiaSdProductPriceKey();
                gaiaSdProductPriceKey.setClient(client);
                gaiaSdProductPriceKey.setGsppProId(proSelfCode);
                gaiaSdProductPriceKey.setGsppBrId(proSite);
                gaiaSdProductPrice = gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPriceKey);
            }
            if (gaiaSdProductPrice == null) {
                return;
            }
            // 收入
            BigDecimal gsppPriceNormal = gaiaSdProductPrice.getGsppPriceNormal();
            if (gsppPriceNormal == null || gsppPriceNormal.compareTo(BigDecimal.ZERO) == 0) {
                return;
            }
            GaiaMaterialAssess gaiaMaterialAssess = null;
            if (CollectionUtils.isNotEmpty(gaiaMaterialAssessList)) {
                gaiaMaterialAssess = gaiaMaterialAssessList.stream().filter(
                        item -> client.equals(item.getClient()) && proSelfCode.equals(item.getMatProCode()) && proSite.equals(item.getMatAssessSite())
                ).findFirst().orElse(null);
            } else {
                GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
                gaiaMaterialAssessKey.setClient(client);
                gaiaMaterialAssessKey.setMatAssessSite(proSite);
                gaiaMaterialAssessKey.setMatProCode(proSelfCode);
                gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
            }
            if (gaiaMaterialAssess == null) {
                return;
            }
            // 成本
            BigDecimal cost = BigDecimal.ZERO;
            // 总库存
            BigDecimal matTotalQty = gaiaMaterialAssess.getMatTotalQty();
            // 总库存为0
            if (matTotalQty == null || matTotalQty.compareTo(BigDecimal.ZERO) == 0) {
                GaiaProductBusiness gaiaProductBusiness = null;
                if (CollectionUtils.isNotEmpty(gaiaProductBusinessList)) {
                    gaiaProductBusiness = gaiaProductBusinessList.stream().filter(
                            item -> client.equals(item.getClient()) && proSelfCode.equals(item.getProSelfCode()) && proSite.equals(item.getProSite())
                    ).findFirst().orElse(null);
                } else {
                    // MAT_MOV_PRICE + MAT_MOV_PRICE*税率
                    GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                    gaiaProductBusinessKey.setClient(client);
                    gaiaProductBusinessKey.setProSite(proSite);
                    gaiaProductBusinessKey.setProSelfCode(proSelfCode);
                    gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                }
                if (gaiaProductBusiness == null) {
                    return;
                }
                // 税率
                String proOutputTax = gaiaProductBusiness.getProOutputTax();
                if (StringUtils.isBlank(proOutputTax)) {
                    return;
                }
                GaiaTaxCode gaiaTaxCode = gaiaTaxCodeList.stream().filter(item -> proOutputTax.equals(item.getTaxCode())).findFirst().orElse(null);
                if (gaiaTaxCode == null) {
                    return;
                }
                String taxCodeValue = gaiaTaxCode.getTaxCodeValue();
                if (StringUtils.isBlank(taxCodeValue)) {
                    return;
                }
                // 税率
                BigDecimal tax = new BigDecimal(taxCodeValue.replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP);
                cost = this.add(gaiaMaterialAssess.getMatMovPrice(), gaiaMaterialAssess.getMatMovPrice().multiply(tax));
            } else {
                // （MAT_ADD_AMT + MAT_ADD_TAX）/ MAT_TOTAL_QTY
                cost = this.add(gaiaMaterialAssess.getMatAddAmt(), gaiaMaterialAssess.getMatAddTax()).divide(matTotalQty, 4, RoundingMode.HALF_UP);
            }
            // 毛利=收入-成本
            BigDecimal profit = gsppPriceNormal.subtract(cost);
            // 毛利率=毛利/收入
            BigDecimal profitRate = profit.divide(gsppPriceNormal, 4, RoundingMode.HALF_UP);
            // 毛利级别
            GaiaProfitLevel gaiaProfitLevel = gaiaProfitLevelMapper.selectProfit(client, profitRate);
            if (gaiaProfitLevel == null) {
                return;
            }
            GaiaProductBusiness record = new GaiaProductBusiness();
            record.setClient(client);
            record.setProSite(proSite);
            record.setProSelfCode(proSelfCode);
            record.setProSlaeClass(gaiaProfitLevel.getGplLevel());

            // 自动毛利级别增加总部（配送中心）不打折的判断，如果配送中心下不打折字段为1，则不更新打折状态。
            // 如果总部级别设置了1不打折，就不能更新成0打折
            GaiaStoreData gaiaStoreData = null;
            if (CollectionUtils.isNotEmpty(gaiaStoreDataList)) {
                gaiaStoreData = gaiaStoreDataList.stream().filter(item -> proSite.equals(item.getStoCode())).findFirst().orElse(null);
            }
            boolean flg = true;
            if (gaiaStoreData != null) {
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                gaiaProductBusinessKey.setClient(client);
                gaiaProductBusinessKey.setProSelfCode(proSelfCode);
                //  配送中心或者主数据地点
                String sto_dc_code = gaiaStoreData.getStoDcCode();
                if (StringUtils.isNotBlank(sto_dc_code)) {
                    gaiaProductBusinessKey.setProSite(sto_dc_code);
                    GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                    String proBdz = gaiaProductBusiness.getProBdz();
                    if ("1".equals(proBdz)) {
                        flg = false;
                    }
                }
                String sto_md_site = gaiaStoreData.getStoMdSite();
                if (flg && StringUtils.isNotBlank(sto_md_site)) {
                    gaiaProductBusinessKey.setProSite(sto_md_site);
                    GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                    String proBdz = gaiaProductBusiness.getProBdz();
                    if ("1".equals(proBdz)) {
                        flg = false;
                    }
                }
            }
            if (StringUtils.isNotBlank(gaiaProfitLevel.getGplBdz())) {
                if ("0".equals(gaiaProfitLevel.getGplBdz())) {
                    if (flg) {
                        record.setProBdz(gaiaProfitLevel.getGplBdz());
                    }
                } else {
                    record.setProBdz(gaiaProfitLevel.getGplBdz());
                }
            }
            gaiaProductBusinessMapper.updateByPrimaryKeySelective(record);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("毛利级别调整失败{}，{}，{}", client, proSite, proSelfCode);
        }
    }

    /**
     * 配送中心列表_权限
     *
     * @param client
     * @return
     */
    @Override
    public Result getAuthDcDataList(String client) {
        TokenUser user = feignService.getLoginInfo();
        return ResultUtil.success(gaiaDcDataMapper.getAuthDcDataList(client, user.getUserId()));
    }

    @Override
    public void updateProFixBin(String client, String proSite, String userId, String commodityCode, String originalCargoLocationNo, String cargoLocationNo) {
        try {
            Map<String, String> params = new HashMap<>();
            // client	加盟商	是	String
            params.put("client", client);
            // proSite	地点	是	String
            params.put("proSite", proSite);
            // userId	用户ID	是	String
            params.put("userId", userId);
            // userName	用户名称	是	String
            GaiaUserDataKey gaiaUserDataKey = new GaiaUserDataKey();
            gaiaUserDataKey.setClient(client);
            gaiaUserDataKey.setUserId(userId);
            GaiaUserData gaiaUserData = gaiaUserDataMapper.selectByPrimaryKey(gaiaUserDataKey);
            if (gaiaUserData != null) {
                params.put("userName", gaiaUserData.getUserNam());
            }
            // commodityCode	商品编码	是	String
            params.put("commodityCode", commodityCode);
            params.put("originalCargoLocationNo", originalCargoLocationNo);
            params.put("cargoLocationNo", cargoLocationNo);
            log.info("在商品主数据修改字段：「固定货位-PRO_FIX_BIN」时，需要调用WMS的『修改商品的自动货位接口』。参数: {}", JSON.toJSONString(params));
            FeignResult feignResult = wmsFeign.updateProFixBin(params);
            log.info("在商品主数据修改字段：「固定货位-PRO_FIX_BIN」时，需要调用WMS的『修改商品的自动货位接口』。返回值: {}", JSON.toJSONString(feignResult));
        } catch (Exception e) {
            log.info("在商品主数据修改字段：「固定货位-PRO_FIX_BIN」时，需要调用WMS的『修改商品的自动货位接口』。失败");
            e.printStackTrace();
        }
    }

    /**
     * 商品9999分类
     *
     * @return
     */
    @Override
    public List<GaiaProductClass> selectGaiaProductClass() {
        List<GaiaProductClass> list = gaiaProductClassMapper.selectGaiaProductClass();
        return list;
    }

    /**
     * 数值相加
     *
     * @param decimals
     * @return
     */
    private BigDecimal add(BigDecimal... decimals) {
        if (decimals == null || decimals.length == 0) {
            return BigDecimal.ZERO;
        }
        BigDecimal result = BigDecimal.ZERO;
        for (BigDecimal decimal : decimals) {
            if (decimal != null) {
                result = result.add(decimal);
            }
        }
        return result;
    }

}
