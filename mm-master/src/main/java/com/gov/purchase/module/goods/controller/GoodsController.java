package com.gov.purchase.module.goods.controller;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaProductGspinfo;
import com.gov.purchase.entity.GaiaWorkflowLog;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.module.goods.dto.*;
import com.gov.purchase.module.goods.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Api(tags = "药品首营")
@RestController
@RequestMapping("goods")
public class GoodsController {
    @Resource
    private GoodsService goodsService;
    @Resource
    private OperationFeignService operationFeignService;

    /**
     * 药品库搜索
     *
     * @param dto QueryProductBasicRequestDto
     * @return pageInfo PageInfo<QueryProductBasicResponseDto>
     */
    @Log("药品库搜索")
    @ApiOperation("药品库搜索")
    @PostMapping("queryProductBasicList")
    public Result resultQueryProductBasicInfo(@Valid @RequestBody QueryProductBasicRequestDto dto) {
        // 药品信息查询
        PageInfo<QueryProductBasicResponseDto> goodsInfoList = goodsService.selectGoodsInfo(dto);
        return ResultUtil.success(goodsInfoList);
    }

    /*
     * 首营提交
     *
     * @param dto GoodsRequestDto
     * @return
     */
    @Log("首营提交")
    @ApiOperation("首营提交")
    @PostMapping("gspinfoSubmit")
    public Result resultGspinfoSubmitInfo(@Valid @RequestBody GaiaProductGspinfo dto) {
        // 首营提交
        Map<String, Object> map = goodsService.insertGspinfoSubmitInfo(dto);
        try {
            // 第三方 gys-operation
            if (map != null && !map.isEmpty()) {
                List<String> proSiteList = (List<String>) map.get("proSiteList");
                List<String> proSelfCodeList = (List<String>) map.get("proSelfCodeList");
                if (CollectionUtils.isNotEmpty(proSiteList) && CollectionUtils.isNotEmpty(proSelfCodeList)) {
                    operationFeignService.multipleStoreProductSync(map.get("client").toString(), proSiteList, proSelfCodeList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultUtil.success();
    }

    /**
     * 首营列表
     *
     * @param dto QueryProductBasicRequestDto
     * @return pageInfo PageInfo<QueryProductBasicResponseDto>
     */
    @Log("首营列表")
    @ApiOperation("首营列表")
    @PostMapping("queryGspinfoList")
    public Result resultQueryGspInfo(@Valid @RequestBody QueryGspRequestDto dto) {
        // 藥品信息查詢
        PageInfo<QueryGspResponseDto> pageInfo = goodsService.selectQueryGspinfo(dto);
        return ResultUtil.success(pageInfo);
    }

    /**
     * 首营列表参考地点
     *
     * @return
     */
    @Log("首营列表参考地点")
    @ApiOperation("首营列表参考地点")
    @PostMapping("queryReferenceSite")
    public Result queryReferenceSite() {
        return ResultUtil.success(goodsService.queryReferenceSite());
    }

    /**
     * 导出首营列表
     *
     * @param dto QueryProductBasicRequestDto
     * @return pageInfo PageInfo<QueryProductBasicResponseDto>
     */
    @Log("导出首营列表")
    @ApiOperation("导出首营列表")
    @PostMapping("exportQueryGspinfoList")
    public Result exportQueryGspinfoList(@Valid @RequestBody QueryGspRequestDto dto) throws Exception {
        // 藥品信息查詢
        Result result = goodsService.exportSelectQueryGspinfo(dto);
        return result;
    }


    /**
     * 首营审批日志
     *
     * @param dto QueryGspinfoWorkflowLogRequestDto
     * @return List<GaiaWorkflowLog>
     */
    @Log("首营审批日志")
    @ApiOperation("首营审批日志")
    @PostMapping("queryGspinfoWorkflowLog")
    public Result resultQueryGspinfoWorkflowLogInfo(@Valid @RequestBody QueryGspinfoWorkflowLogRequestDto dto) {
        // 藥品信息查詢
        List<GaiaWorkflowLog> gaiaWorkflowLog = goodsService.selectQueryGspinfoWorkflowLogInfo(dto);
        return ResultUtil.success(gaiaWorkflowLog);
    }

    /**
     * 首营详情
     *
     * @param dto QueryGspRequestDto
     * @return GaiaProductGspinfo
     */
    @Log("首营详情")
    @ApiOperation("首营详情")
    @PostMapping("queryGspinfo")
    public Result resultQueryGspValue(@Valid @RequestBody GspinfoRequestDto dto) {
        // 药品信息查询
        QueryGspinfoResponseDto gaiaProductGspinfo = goodsService.selectQueryGspValue(dto);
        return ResultUtil.success(gaiaProductGspinfo);
    }

    @Log("商品首营详情")
    @ApiOperation("商品首营详情")
    @PostMapping("getGspinfoDetails")
    public Result getGspinfoDetails(@Valid @RequestBody GetGspinfoDetailsDTO dto) {
        return ResultUtil.success(goodsService.getGspinfoDetails(dto));
    }

    /**
     * 自编码返回
     *
     * @param client
     * @param proSite
     * @param code
     * @return
     */
    @Log("自编码返回")
    @ApiOperation("自编码返回")
    @PostMapping("getMaxSelfCodeNum")
    public Result getMaxSelfCodeNum(@RequestJson("client") String client, @RequestJson("proSite") String proSite,
                                    @RequestJson(value = "code", required = false) String code) {
        return goodsService.getMaxSelfCodeNum(client, proSite, code);
    }

    /**
     * 商品通用数据详情获取
     *
     * @return proCode
     */
    @Log("商品通用数据详情获取")
    @ApiOperation("商品通用数据详情获取")
    @PostMapping("getProBasicByCode")
    public Result getProBasicByCode(@RequestParam("proCode") String proCode) {
        return goodsService.getProBasicByCode(proCode);
    }

    /**
     * 手工新增
     *
     * @return
     */
    @Log("手工新增")
    @ApiOperation("手工新增")
    @PostMapping("addBusioness")
    public Result addBusioness(@RequestBody GaiaProductGspinfo gaiaProductGspinfo) {
        Map<String, Object> map = goodsService.addBusioness(gaiaProductGspinfo);
        try {
            // 第三方 gys-operation
            if (map != null && !map.isEmpty()) {
                List<String> proSiteList = (List<String>) map.get("proSiteList");
                List<String> proSelfCodeList = (List<String>) map.get("proSelfCodeList");
                if (CollectionUtils.isNotEmpty(proSiteList) && CollectionUtils.isNotEmpty(proSelfCodeList)) {
                    operationFeignService.multipleStoreProductSync(map.get("client").toString(), proSiteList, proSelfCodeList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultUtil.success();
    }

    /**
     * 供货单位
     *
     * @return
     */
    @Log("供货单位")
    @ApiOperation("供货单位")
    @GetMapping("getSupplierList")
    public Result getSupplierList(@RequestParam("site") String site) {
        return goodsService.getSupplierList(site);
    }
}
