package com.gov.purchase.module.store.dto.price;

import com.gov.purchase.entity.GaiaProductBusiness;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 价格组商品明细表
 * @author: yzf
 * @create: 2021-12-16 17:24
 */
@Data
public class PriceGroupProductVO extends GaiaProductBusiness {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 价格组
     */
    private String prcGroupId;

    /**
     * 商品编码
     */
    private String prcProId;

    /**
     * 零售价
     */
    private BigDecimal prcPriceNormal;

    /**
     * 调价零售价
     */
    private BigDecimal prcPriceNormalEnd;

    /**
     * 会员价
     */
    private BigDecimal prcPriceHy;

    /**
     * 调价会员价
     */
    private BigDecimal prcPriceHyEnd;

    /**
     * 会员日价
     */
    private BigDecimal prcPriceHyr;

    /**
     * 调价会员日价
     */
    private BigDecimal prcPriceHyrEnd;

    /**
     * 创建日期
     */
    private String prcCreateDate;

    /**
     * 创建时间
     */
    private String prcCreateTime;

    /**
     * 创建人
     */
    private String prcCreateBy;

    /**
     * 修改日期
     */
    private String prcChangeDate;

    /**
     * 修改时间
     */
    private String prcChangeTime;

    /**
     * 进价
     */
    private BigDecimal batPoPrice;

    /**
     * 门店
     */
    private String prcStore;
}
