package com.gov.purchase.module.store.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;
import com.gov.purchase.module.store.dto.store.StoreListResponseDto;
import com.gov.purchase.module.store.service.StoreGroupProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.22
 */
@Api(tags = "门店组商品管理")
@RestController
@RequestMapping("storeGroupProduct")
public class StoreGroupProductController {

    @Autowired
    private StoreGroupProductService storeGroupProductService;


    /**
     * @param param 商品编码、商品名称、通用名称、助记码，精确匹配国际条形码1或2
     * @return
     */
    @Log("门店商品列表")
    @ApiOperation("门店列表")
    @PostMapping("queryList")
    public Result queryList(
            @RequestJson(value = "param", name = "商品编码、商品名称、通用名称、助记码，精确匹配国际条形码1或2", required = false) String param) {

        return ResultUtil.success(storeGroupProductService.queryList(param));
    }


    /**
     * @param param 商品编码、商品名称、通用名称、助记码，精确匹配国际条形码1或2
     * @return
     */
    @Log("门店组商品列表")
    @ApiOperation("门店组商品列表")
    @PostMapping("queryGroupList")
    public Result queryGroupList(
            @RequestJson(value = "selfCodeList", name = "商品编码", required = true) List<String> param) {

        return ResultUtil.success(storeGroupProductService.queryGroupList(param));
    }


    /**
     * @param
     * @return
     */
    @Log("保存调价后的数据")
    @ApiOperation("保存调价后的数据")
    @PostMapping("save")
    public Result saveList(@RequestBody List<GaiaPriceAdjustDto> gaiaPriceAdjustList) {

        return storeGroupProductService.saveList(gaiaPriceAdjustList);
    }


    /**
     * 门店列表
     *
     * @param
     * @return
     */
    @Log("门店列表")
    @ApiOperation("门店列表")
    @PostMapping("queryStoreList")
    public Result querStoreyList() {

        return ResultUtil.success(storeGroupProductService.queryStoreList());
    }


    /**
     * 门店列表
     *
     * @param
     * @return
     */
    @Log("门店组列表")
    @ApiOperation("门店列表")
    @PostMapping("queryStoreGroupList")
    public Result queryStoreGroupList() {

        return ResultUtil.success(storeGroupProductService.queryStoreGroupList());
    }

    /**
     * 门店列表
     *
     * @param
     * @return
     */
    @Log("门店组列表导出")
    @ApiOperation("门店列表导出")
    @PostMapping("queryStoreGroupExportList")
    public Result queryStoreGroupExportList() {

        return storeGroupProductService.queryStoreGroupExportList();
    }


    /**
     * 删除门店组信息
     *
     * @param
     * @return
     */
    @Log("删除门店组信息")
    @ApiOperation("删除门店组信息")
    @PostMapping("deleteStoreGroupList")
    public Result deleteStoreGroupList(@RequestBody List<StoreListResponseDto> list) {

        return storeGroupProductService.deleteStoreGroupList(list);
    }


    /**
     * 保存门店组信息
     *
     * @param
     * @return
     */
    @Log("保存门店组信息")
    @ApiOperation("保存门店组信息")
    @PostMapping("saveStoreGroupList")
    public Result saveStoreGroupList(@RequestBody List<StoreListResponseDto> list) {

        return storeGroupProductService.saveStoreGroupList(list);
    }


}
