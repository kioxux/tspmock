package com.gov.purchase.module.store.dto.price;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@ApiModel("商品名称筛选返回参数")
public class ProductBasicListReponseDto {

    @ApiModelProperty(value = "地点", name = "proSite")
    private String proSite;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    private String proSelfCode;

    @ApiModelProperty(value = "商品名称", name = "proName")
    private String proName;

    @ApiModelProperty(value = "商品描述", name = "proDepict")
    private String proDepict;

    @ApiModelProperty(value = "规格", name = "proSpecs")
    private String proSpecs;

    @ApiModelProperty(value = "生产厂家", name = "proFactoryName")
    private String proFactoryName;

    @ApiModelProperty(value = "单位", name = "proUnit")
    private String proUnit;

    @ApiModelProperty(value = "单位名称", name = "unitName")
    private String unitName;

    @ApiModelProperty(value = "调前价格", name = "prePrice")
    private BigDecimal prePrice;

    @ApiModelProperty(value = "调前价格有效期", name = "prePriceValid")
    private String prePriceValid;

    @ApiModelProperty(value = "门店编号", name = "prcStore")
    private String prcStore;

    /** 单价 */
    private BigDecimal prcAmount;
    /**
     * 生效日期
     */
    private String prcEffectDate;
    /**
     * 不允许积分
     */
    private String prcNoIntegral;
    /**
     * 不允许会员卡打折
     */
    private String prcNoDiscount;
    /**
     * 不允许积分兑换
     */
    private String prcNoExchange;
    /**
     * 限购数量
     */
    private String prcLimitAmount;
    /**
     * 批次信息表中最近一次 采购单价(入库收货价)
     */
    private BigDecimal batPoPrice;
}
