package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaBatchInfoMapper;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaWmsHuoweiMapper;
import com.gov.purchase.module.base.dto.ProductDto;
import com.gov.purchase.module.base.dto.businessImport.SalesOrderProducts;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.wholesale.dto.RecallInfo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author : Yzf
 * description: 销售订单批量导入
 * create time: 2021/12/22 16:42
 */
@Service
public class SalesOrderProductsImport extends BusinessImport {
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaBatchInfoMapper gaiaBatchInfoMapper;
    @Resource
    private GaiaWmsHuoweiMapper gaiaWmsHuoweiMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        List<String> errorList = new ArrayList<>();
        List<String> errors = new ArrayList<>();
        List<String> errorProBatch = new ArrayList<>();
        List<String> errorProCode = new ArrayList<>();
        List<RecallInfo> recallInfos = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        // 价格方式
        String priceType = (String) field.get("priceType");
        String customerId = (String) field.get("customerId");
        // 销售主体
        String proSite = (String) field.get("proSite");
        if (StringUtils.isBlank(proSite)) {
            return ResultUtil.error(ResultEnum.SITE_ERROR);
        }
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(user.getClient());
        gaiaDcDataKey.setDcCode(proSite);
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
        if (gaiaDcData == null) {
            throw new CustomResultException("销售主体不正确");
        }

        Set<SalesOrderProducts> products = new HashSet<>();
        List<String> wmHwhList = new ArrayList<>();
        for (Integer key : map.keySet()) {
            SalesOrderProducts salesOrderProducts = (SalesOrderProducts) map.get(key);
            products.add(salesOrderProducts);
            if (StringUtils.isNotBlank(salesOrderProducts.getSoHwh())) {
                wmHwhList.add(salesOrderProducts.getSoHwh());
            }
        }
        List<String> proIds = products.stream().map(SalesOrderProducts::getProCode).collect(Collectors.toList());
        List<GaiaProductBusiness> existPro = gaiaProductBusinessMapper.selectProList(user.getClient(), proSite, proIds);
        List<String> existProId = existPro.stream().map(GaiaProductBusiness::getProSelfCode).collect(Collectors.toList());
        // 货位号
        List<GaiaWmsHuowei> gaiaWmsHuoweiList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(wmHwhList)) {
            gaiaWmsHuoweiList = gaiaWmsHuoweiMapper.getListBySiteHwh(user.getClient(), proSite, wmHwhList);
        }
        for (Integer key : map.keySet()) {
            SalesOrderProducts salesOrderProducts = (SalesOrderProducts) map.get(key);
            // 商品编码不为空
            if (StringUtils.isBlank(salesOrderProducts.getProCode())) {
                errorProCode.add(MessageFormat.format("第{0}行，商品编码不为空", key + 1));
                continue;
            }
            // 筛选出不存在商品
            if (!existProId.contains(salesOrderProducts.getProCode())) {
                errorList.add(MessageFormat.format("第{0}行，销售主体不存在该商品", key + 1));
                continue;
            }
            if (StringUtils.isNotBlank(salesOrderProducts.getProBatch())) {
                GaiaBatchInfoKey gaiaBatchInfoKey = new GaiaBatchInfoKey();
                gaiaBatchInfoKey.setClient(user.getClient());
                gaiaBatchInfoKey.setBatProCode(salesOrderProducts.getProCode());
                gaiaBatchInfoKey.setBatSiteCode(proSite);
                List<String> batchInfo = gaiaBatchInfoMapper.selectBatch(gaiaBatchInfoKey);
                if (CollectionUtils.isEmpty(batchInfo) || !batchInfo.contains(salesOrderProducts.getProBatch())) {
                    errorProBatch.add(MessageFormat.format("第{0}行，生产批号不存在", key + 1));
                    continue;
                }
            }
            // 商品数量不为空
            if (StringUtils.isBlank(salesOrderProducts.getProQty())) {
                errors.add(MessageFormat.format("第{0}行，商品数量不为空", key + 1));
                continue;
            }
            // 货位号验证
            if (StringUtils.isNotBlank(salesOrderProducts.getSoHwh())) {
                GaiaWmsHuowei gaiaWmsHuowei = gaiaWmsHuoweiList.stream().filter(t -> t.getWmHwh().equals(salesOrderProducts.getSoHwh())).findFirst().orElse(null);
                if (gaiaWmsHuowei == null) {
                    errors.add(MessageFormat.format("第{0}行，请填入正确的货位号", key + 1));
                    continue;
                }
            }
        }
        if (!CollectionUtils.isEmpty(errorProCode)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(errorProCode);
            return result;
        }
        if (!CollectionUtils.isEmpty(errorList)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(errorList);
            return result;
        }
        if (!CollectionUtils.isEmpty(errorProBatch)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(errorProBatch);
            return result;
        }
        if (!CollectionUtils.isEmpty(errors)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(errors);
            return result;
        }
        List<ProductDto> productDtos = gaiaProductBusinessMapper.queryGoodsPrice(user.getClient(), proSite, null, proIds);
        products.forEach(
                item -> {
                    RecallInfo recallInfo = new RecallInfo();
                    recallInfo.setTotalQty(new BigDecimal(item.getProQty()));
                    recallInfo.setProSelfCode(item.getProCode());
                    recallInfo.setBatBatchNo(item.getProBatch());
                    recallInfo.setProSite(proSite);
                    if (StringUtils.isBlank(item.getProPrice())) {
                        if (StringUtils.isNotBlank(priceType)) {
                            for (ProductDto pro : productDtos) {
                                if (pro.getProSelfCode().equalsIgnoreCase(item.getProCode())) {
                                    recallInfo.setProLsj(pro.getProLsj());
                                    if (NumberUtils.toInt(priceType) == 1) {
                                        recallInfo.setSoPrice(pro.getProLsj());
                                    } else if (NumberUtils.toInt(priceType) == 2) {
                                        recallInfo.setSoPrice(pro.getProHyj());
                                    } else if (NumberUtils.toInt(priceType) == 3) {
                                        recallInfo.setSoPrice(pro.getProGpj());
                                    } else if (NumberUtils.toInt(priceType) == 4) {
                                        recallInfo.setSoPrice(pro.getProGlj());
                                    } else if (NumberUtils.toInt(priceType) == 5) {
                                        recallInfo.setSoPrice(pro.getProYsj1());
                                    } else if (NumberUtils.toInt(priceType) == 6) {
                                        recallInfo.setSoPrice(pro.getProYsj2());
                                    } else if (NumberUtils.toInt(priceType) == 7) {
                                        recallInfo.setSoPrice(pro.getProYsj3());
                                    } else if (NumberUtils.toInt(priceType) == 8) {
                                        recallInfo.setSoPrice(pro.getBatPoPrice());
                                    }
                                    break;
                                }
                            }
                        }
                    } else {
                        recallInfo.setSoPrice(new BigDecimal(item.getProPrice()));
                    }
                    recallInfo.setGsrdHwh(item.getSoHwh());
                    recallInfo.setDcCode(proSite);
                    recallInfo.setDcName(gaiaDcData.getDcName());
                    GaiaProductBusiness gaiaProductBusiness = existPro.stream().filter(t -> t.getProSelfCode().equals(item.getProCode())).findFirst().orElse(null);
                    if (gaiaProductBusiness != null) {
                        recallInfo.setProUnit(gaiaProductBusiness.getProUnit());
                        recallInfo.setUnitName(gaiaProductBusiness.getProUnit());
                    }
                    recallInfos.add(recallInfo);
                }
        );
        return ResultUtil.success(recallInfos);
    }
}
