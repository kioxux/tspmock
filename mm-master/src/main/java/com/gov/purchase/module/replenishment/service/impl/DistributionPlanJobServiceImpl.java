package com.gov.purchase.module.replenishment.service.impl;

import com.github.pagehelper.util.StringUtil;
import com.gov.purchase.constants.GenerateMessageEnum;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaNoplanMessage;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.WmsRkys;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaNoplanMessageMapper;
import com.gov.purchase.mapper.GaiaWmsRkysMapper;
import com.gov.purchase.module.replenishment.service.DistributionPlanJobService;
import com.gov.purchase.module.replenishment.service.DistributionPlanService;
import com.gov.purchase.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/29 14:24
 **/
@Slf4j
@Service("distributionPlanJobService")
public class DistributionPlanJobServiceImpl implements DistributionPlanJobService {
    @Resource
    private DistributionPlanService distributionPlanService;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaWmsRkysMapper gaiaWmsRkysMapper;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private GaiaNoplanMessageMapper gaiaNoplanMessageMapper;

    @Override
    public void autoExecPlan(String params) {
        log.info(String.format("<定时任务><自动铺货><开始执行,param=%s>", params));
        GaiaDcData dcCond = new GaiaDcData();
        List<GaiaDcData> dcDataList = gaiaDcDataMapper.findList(dcCond);
        for (GaiaDcData gaiaDcData : dcDataList) {
            //配送中心是否有新品入库
            WmsRkys cond = new WmsRkys();
            cond.setClient(gaiaDcData.getClient());
            cond.setProSite(gaiaDcData.getDcCode());
            cond.setState(2);//上架结束
            if (StringUtil.isNotEmpty(params)) {
                cond.setWmYsrq(params);
            }else {
                cond.setWmYsrq(DateUtils.formatLocalDate(LocalDate.now().plusDays(-1), "yyyyMMdd"));
            }
            List<WmsRkys> rkysList = gaiaWmsRkysMapper.findDistinctList(cond);
            if (rkysList == null || rkysList.size() == 0) {
                continue;
            }
            //新品入库，无铺货计划消息集合
            Map<String, Set<String>> noPlanMessageMap = new HashMap<>();
            //新品入库，库存不足消息集合
            Map<String, Set<String>> notEnoughMessageMap = new HashMap<>();
            for (WmsRkys wmsRkys : rkysList) {
                GaiaProductBusiness productBusiness = distributionPlanService.getProductInfoByRkys(wmsRkys);
                //商品定位是否“X”时执行新品铺货计划
                if (productBusiness != null) {
                    distributionPlanService.execSingleDistributionPlan(wmsRkys, noPlanMessageMap, notEnoughMessageMap, productBusiness.getProPosition());
                }
            }
            try {
                //发送新品入库，无铺货计划消息
                for (Map.Entry<String, Set<String>> message : noPlanMessageMap.entrySet()) {
                    distributionPlanService.generateDistributionMessage(message.getKey(), message.getValue(), GenerateMessageEnum.NOT_HAS_PLAN);
                    //处理无计划报表统计
                    try {
                        threadPoolTaskExecutor.execute(()->saveNoPlanMessage(message.getKey(),message.getValue()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.info("<定时任务><自动铺货><处理无计划报表统计失败> 参数{}",noPlanMessageMap);
                    }
                }
                //发送新品入库，库存不足消息
                for (Map.Entry<String, Set<String>> message : notEnoughMessageMap.entrySet()) {
                    distributionPlanService.generateDistributionMessage(message.getKey(), message.getValue(), GenerateMessageEnum.NOT_ENOUGH_STOCK);
                }
            } catch (Exception e) {
                log.error("<自动铺货><发送消息><发送铺货消息异常：{}>", e.getMessage(), e);
            }
        }
        log.info("<定时任务><自动铺货><执行结束>");
    }

    private void saveNoPlanMessage(String client, Set<String> proCodes){
        Date date = new Date();
        for (String proCode : proCodes) {
            GaiaNoplanMessage gaiaNoplanMessage = gaiaNoplanMessageMapper.findIn121Day(client,proCode);
            if (ObjectUtils.isEmpty(gaiaNoplanMessage)){
                GaiaNoplanMessage message = new GaiaNoplanMessage();
                message.setClient(client);
                message.setProCode(proCode);
                message.setMsgDate(date);
                message.setReadFlag("N");
                message.setIsWeekReadFlag("N");
                message.setIsWeekMsg("N");
                message.setCreateTime(date);
                gaiaNoplanMessageMapper.insert(message);
            }
        }
        log.info("<定时任务><自动铺货><处理无计划报表统计完成>");
    }

}
