package com.gov.purchase.module.base.dto;

import com.gov.purchase.entity.GaiaArea;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AreaDto extends GaiaArea {

    private List<AreaDto> children;

}
