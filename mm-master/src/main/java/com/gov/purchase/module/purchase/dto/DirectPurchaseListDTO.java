package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "要货查询传入参数")
public class DirectPurchaseListDTO {
    // 门店
    private List<String> stoCodeList;
    // 要货日期
    private String startDate;
    private String client;
    // 要货日期
    private String endDate;
    // 要货单号
    private String voucherCode;
    // 请货类型
    private String pattern;
    // 商品编码
    private List<String> proCode;
    private String proCodeStr;

    /**
     * 商品分类Code
     */
    private String[][] proClassCode;
    /**
     * 商品分类Code
     */
    private List<String> proClassList;

    /**
     * 供应商id
     */
    private String supplierId;
}
