package com.gov.purchase.module.store.dto;

import com.gov.purchase.entity.GaiaSdProductPrice;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.01.28
 */
@Data
public class SdProductPrice extends GaiaSdProductPrice {

    /**
     * 门店名
     */
    private String stoName;
    /**
     * 门店简称
     */
    private String stoShortName;

    /**
     * 商品通用名
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    private BigDecimal costPrice;
}

