package com.gov.purchase.module.wholesale.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.wholesale.dto.WholesaleConsignorDTO;
import com.gov.purchase.module.wholesale.service.WholesaleConsignorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 批发订单按货主管控功能
 * @author: yzf
 * @create: 2021-11-23 11:44
 */
@Api(tags = "批发订单按货主管控功能")
@RestController
@RequestMapping("wholesaleConsignor")
public class WholesaleConsignorController {

    @Resource
    private WholesaleConsignorService wholesaleConsignorService;

    /**
     * 根据仓库查询批发货主客户
     *
     * @return
     */
    @Log("根据仓库查询批发货主客户")
    @ApiOperation("根据仓库查询批发货主客户")
    @PostMapping("queryOwnerCustomer")
    public Result queryOwnerCustomer(@RequestJson(value = "dcCode") String dcCode) {
        return ResultUtil.success(wholesaleConsignorService.queryOwnerCustomer(dcCode));
    }

    /**
     * 批发货主订单查询
     *
     * @return
     */
    @Log("批发货主查询")
    @ApiOperation("批发货主查询")
    @PostMapping("queryWholesaleConsignorOrders")
    public Result queryWholesaleConsignorOrders(@RequestBody WholesaleConsignorDTO wholesaleConsignorDTO) {
        return wholesaleConsignorService.queryControlWholesaleOrders(wholesaleConsignorDTO);
    }

    /**
     * 新增批发货主
     *
     * @return
     */
    @Log("新增批发货主")
    @ApiOperation("新增批发货主")
    @PostMapping("insertWholesaleConsignor")
    public Result insertControlWholesaleOrders(@RequestBody WholesaleConsignorDTO wholesaleConsignorDTO) {
        return wholesaleConsignorService.insertControlWholesaleOrders(wholesaleConsignorDTO);
    }


    /**
     * 删除批发货主
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Log("删除批发货主")
    @ApiOperation("删除批发货主")
    @PostMapping("delWholesaleConsignor")
    public Result delWholesaleConsignor(@RequestBody WholesaleConsignorDTO wholesaleConsignorDTO) {
        return wholesaleConsignorService.delOwner(wholesaleConsignorDTO);
    }

    /**
     * 更新批发货主
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Log("更新批发货主")
    @ApiOperation("更新批发货主")
    @PostMapping("updateWholesaleConsignor")
    public Result updateWholesaleConsignor(@RequestBody WholesaleConsignorDTO wholesaleConsignorDTO) {
        return wholesaleConsignorService.updateOwner(wholesaleConsignorDTO);
    }

    /**
     * 查询货主已存在客户
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Log("查询货主已存在客户")
    @ApiOperation("查询货主已存在客户")
    @PostMapping("queryWholesaleConsignorCus")
    public Result queryWholesaleConsignorCus(@RequestBody WholesaleConsignorDTO wholesaleConsignorDTO) {
        return wholesaleConsignorService.queryOwnerCustomer(wholesaleConsignorDTO);
    }

    /**
     * 查询货主已存在商品
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Log("查询货主已存在商品")
    @ApiOperation("查询货主已存在商品")
    @PostMapping("queryWholesaleConsignorPro")
    public Result queryWholesaleConsignorPro(@RequestBody WholesaleConsignorDTO wholesaleConsignorDTO) {
        return wholesaleConsignorService.queryOwnerProduct(wholesaleConsignorDTO);
    }

    /**
     * 货主明细保存
     *
     * @param dto
     * @return
     */
    @Log("货主明细保存")
    @ApiOperation("货主明细保存")
    @PostMapping("saveWholesaleConsignorDetail")
    public Result saveWholesaleConsignorDetail(@RequestBody WholesaleConsignorDTO dto) {
        return wholesaleConsignorService.saveOwnerDetail(dto);
    }

    /**
     * 货主列表
     *
     * @param dcCode
     * @return
     */
    @Log("货主列表")
    @ApiOperation("货主列表")
    @PostMapping("queryWholesaleConsignor")
    public Result queryWholesaleConsignor(@RequestJson(value = "dcCode", name = "仓库", required = false) String dcCode,
                                          @RequestJson(value = "saleManCode", name = "销售员编码", required = false) String saleManCode) {
        return wholesaleConsignorService.queryWholesaleConsignor(dcCode, saleManCode);
    }

}
