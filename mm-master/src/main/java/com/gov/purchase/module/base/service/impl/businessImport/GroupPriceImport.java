package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.module.base.dto.businessImport.GroupPrice;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.delivery.dto.GaiaAllotGroupPriceVO;
import com.gov.purchase.module.delivery.dto.SaveAllotGroupPriceVO;
import com.gov.purchase.module.delivery.dto.StoreVO;
import com.gov.purchase.module.delivery.service.AllotPriceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class GroupPriceImport extends BusinessImport {
    @Resource
    private FeignService feignService;
    @Resource
    private AllotPriceService allotPriceService;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    @Transactional
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        List<String> errorList = new ArrayList<>();
        // 批量导入
        try {
            if (field.get("alpReceiveSite") == null) {
                errorList.add(MessageFormat.format(ResultEnum.SITE_ERROR.getMsg(), null));
            }
            if (field.get("gapgCode") == null) {
                errorList.add(MessageFormat.format(ResultEnum.GAPG_CODE_EMPTY.getMsg(), null));
            }
            if (field.get("gapgType") == null) {
                errorList.add(MessageFormat.format(ResultEnum.GAPG_TYPE_EMPTY.getMsg(), null));
            }
            if (errorList.size() > 0) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            List<StoreVO> list = allotPriceService.queryStoreList(field.get("alpReceiveSite").toString(), field.get("gapgType").toString());
            List<String> stoCodeList = new ArrayList<>();
            for (StoreVO storeVO : list) {
                stoCodeList.add(storeVO.getLabel());
            }
            List<GaiaAllotGroupPriceVO> voList = new ArrayList<>();
            SaveAllotGroupPriceVO saveDto = new SaveAllotGroupPriceVO();
            saveDto.setGapgType(new Integer(field.get("gapgType").toString()));
            saveDto.setGapgCode(field.get("gapgCode").toString());
            saveDto.setAlpReceiveSite(field.get("alpReceiveSite").toString());
            for (Integer key : map.keySet()) {
                GroupPrice groupPriceProduct = (GroupPrice) map.get(key);
                if (!stoCodeList.contains(groupPriceProduct.getStoCode())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "编码"));
                    continue;
                }
                GaiaAllotGroupPriceVO vo = new GaiaAllotGroupPriceVO();
                vo.setClient(client);
                vo.setGagpCus(groupPriceProduct.getStoCode());
                voList.add(vo);
            }
            saveDto.setVoList(voList);
            if (errorList.size() > 0) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            allotPriceService.saveAllotGroupPrice(saveDto);
            return ResultUtil.success();
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException(ResultEnum.E0126);
        }

    }
}
