package com.gov.purchase.module.store.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;
import com.gov.purchase.module.store.service.StoreProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.22
 */
@Api(tags = "门店商品管理")
@RestController
@RequestMapping("storeProduct")
public class StoreProductController {

    @Autowired
    private StoreProductService storeProductService;


    /**
     * @param stoCode 门店编码
     * @param param   商品编码、商品名称、通用名称、助记码，精确匹配国际条形码1或2
     * @return
     */
    @Log("门店商品列表")
    @ApiOperation("门店列表")
    @PostMapping("queryList")
    public Result queryList(
            @RequestJson(value = "stoCode", name = "门店", required = true) String stoCode,
            @RequestJson(value = "param", name = "商品编码、商品名称、通用名称、助记码，精确匹配国际条形码1或2", required = true) String param) {

        return ResultUtil.success(storeProductService.queryList(stoCode, param));
    }

    /**
     * @param
     * @return
     */
    @Log("保存调价后的数据")
    @ApiOperation("保存调价后的数据")
    @PostMapping("save")
    public Result saveList(@RequestBody List<GaiaPriceAdjustDto> gaiaPriceAdjustList) {

        return storeProductService.saveList(gaiaPriceAdjustList);
    }

    /**
     * @param
     * @return
     */
    @Log("定时更新")
    @ApiOperation("定时更新")
    @PostMapping("saveTiming")
    public Result updateList() {

        return storeProductService.updateList();
    }


}
