package com.gov.purchase.module.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "商品详情传入参数")
public class QueryPorRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "商品编码", name = "proCode", required = true)
    @NotBlank(message = "商品编码不能为空")
    private String proCode;

    @ApiModelProperty(value = "地点", name = "proSite")
    private String proSite;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode", required = true)
    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;
}
