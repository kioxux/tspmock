package com.gov.purchase.module.wholesale.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.wholesale.dto.QueryReturnOrder;
import com.gov.purchase.module.wholesale.dto.ReturnOrderByProductVO;
import com.gov.purchase.module.wholesale.dto.ReturnOrderDTO;
import com.gov.purchase.module.wholesale.dto.ReturnOrderReq;
import com.gov.purchase.module.wholesale.service.ReturnOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.08
 */
@Api(tags = "退货订单")
@Validated
@RestController
@RequestMapping("returnOrder")
public class ReturnOrderController {

    @Resource
    ReturnOrderService returnOrderService;

    /**
     * 销售退货订单列表
     *
     * @param queryReturnOrder
     */
    @Log("销售退货订单列表")
    @ApiOperation("销售退货订单列表")
    @PostMapping("queryReturnOrderList")
    public Result querySalesOrderList(@RequestBody QueryReturnOrder queryReturnOrder) {
        return returnOrderService.querySalesOrderList(queryReturnOrder);
    }

    /**
     * 销售退货
     *
     * @param list
     * @return
     */
    @Log("销售退货")
    @ApiOperation("销售退货")
    @PostMapping("returnOrder")
    public Result returnOrder(@RequestBody List<ReturnOrderReq> list) {
        return returnOrderService.returnOrder(list);
    }

    @Log("按商品退货")
    @ApiOperation("按商品退货")
    @PostMapping("returnOrderByProduct")
    public Result returnOrderByProduct(@RequestBody @Valid ReturnOrderByProductVO vo) {
        returnOrderService.returnOrderByProduct(vo);
        return ResultUtil.success();
    }

    @Log("商品批号信息")
    @ApiOperation("商品批号信息")
    @PostMapping("getProBatchInfo")
    public Result getProBatchInfo(@RequestJson(value = "site", name = "地点") String site,
                                  @RequestJson(value = "proSelfCode", name = "商品编码") String proSelfCode) {
        return ResultUtil.success(returnOrderService.getProBatchInfo(site, proSelfCode));
    }

    @Log("调取退货单")
    @ApiOperation("调取退货单")
    @PostMapping("queryReturnOrder")
    public Result queryReturnOrder(@RequestBody ReturnOrderDTO dto) {
        return returnOrderService.queryReturnOrder(dto);
    }

    @Log("关闭退货单")
    @ApiOperation("关闭退货单")
    @PostMapping("closeReturnOrder")
    public Result closeReturnOrder(@RequestBody ReturnOrderDTO dto) {
        returnOrderService.closeReturnOrder(dto);
        return ResultUtil.success();
    }

    @Log("退货单明细")
    @ApiOperation("退货单明细")
    @PostMapping("queryReturnOrderDetail")
    public Result queryReturnOrderDetail(@RequestBody ReturnOrderDTO dto) {
        return returnOrderService.queryReturnOrderDetail(dto);
    }

}
