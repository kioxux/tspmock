package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaInvoicePayingBasic;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("付款及发票查询接口返回参数")
public class StoreDataDTO extends GaiaInvoicePayingBasic {


}
