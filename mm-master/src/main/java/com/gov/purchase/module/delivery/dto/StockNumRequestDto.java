package com.gov.purchase.module.delivery.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "门店库存获取传入参数")
public class StockNumRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 地点（门店、配送中心）
     */
    @ApiModelProperty(value = "地点（门店、配送中心）", name = "siteCode", required = true)
    @NotBlank(message = "地点（门店、配送中心）不能为空")
    private String siteCode;

    /**
     * 商品自编码
     */
    @ApiModelProperty(value = "商品自编码", name = "proCode", required = true)
    @NotBlank(message = "商品自编码不能为空")
    private String proCode;

    /**
     * 批次号
     */
    @ApiModelProperty(value = "批次号", name = "batBatch", required = true)
    @NotBlank(message = "批次号不能为空")
    private String batBatch;

    /**
     * 供应商编号
     */
    @ApiModelProperty(value = "供应商编号", name = "supplierCode", required = true)
    @NotBlank(message = "供应商编号不能为空")
    private String supplierCode;
}