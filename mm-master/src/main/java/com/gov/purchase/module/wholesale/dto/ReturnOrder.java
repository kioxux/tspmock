package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.13
 */
@Data
public class ReturnOrder {
    /**
     * 加盟商编号
     */
    private String client;
    /**
     * 加盟商名称
     */
    private String francName;
    /**
     * 销售主体（DC编号）
     */
    private String soCompanyCode;
    /**
     * DC名称
     */
    private String dcName;
    /**
     * 创建人
     */
    private String soCreateBy;
    /**
     * 创建姓名
     */
    private String userNam;
    /**
     * 订单号
     */
    private String soId;
    /**
     * 订单行号
     */
    private String soLineNo;
    /**
     * 订单类型
     */
    private String soType;
    /**
     * 类型名称
     */
    private String ordTypeDesc;
    /**
     * 凭证日期
     */
    private String soDate;
    /**
     * 客户自编码
     */
    private String soCustomerId;
    /**
     * 客户名称
     */
    private String cusName;
    /**
     * 商品自编码
     */
    private String soProCode;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 单位名
     */
    private String unitName;
    /**
     * 订单数量
     */
    private BigDecimal soQty;
    /**
     * 销售订单单价
     */
    private BigDecimal soPrice;
    /**
     * 税率
     */
    private String soRate;
    /**
     * 税率值
     */
    private String taxCodeValue;
    /**
     * 行总价
     */
    private BigDecimal soLineAmt;
    /**
     * 地点
     */
    private String soSiteCode;
    /**
     * 地点名
     */
    private String siteName;
    /**
     * 库位
     */
    private String soLocationCode;
    /**
     * 批次号
     */
    private String soBatch;
    /**
     * 已交货数量
     */
    private BigDecimal soDeliveredQty;
    /**
     * 已退货数量
     */
    private BigDecimal returnQty;

    /**
     * 生产批号
     */
    private String batBatchNo;

    /**
     * 生产日期
     */
    private String batProductDate;

    /**
     * 有效期
     */
    private String batExpiryDate;
}

