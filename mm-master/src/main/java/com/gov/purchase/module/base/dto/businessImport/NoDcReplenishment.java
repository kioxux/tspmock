package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

@Data
public class NoDcReplenishment {
    /**
     * 门店号
     */
    @ExcelValidate(index = 0, name = "门店号", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String stoCode;

    /**
     * 供应商编码
     */
    @ExcelValidate(index = 1, name = "供应商编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String poSupplierId;

    private String poSupplierName;

    /**
     * 商品编码
     */
    @ExcelValidate(index = 2, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proSelfCode;

    /**
     * 采购单价
     */
    @ExcelValidate(index = 3, name = "采购单价", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.99, addRequired = false)
    private String purchasePrice;

    /**
     * 采购数量
     */
    @ExcelValidate(index = 4, name = "采购数量", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0)
    private String poQty;

}
