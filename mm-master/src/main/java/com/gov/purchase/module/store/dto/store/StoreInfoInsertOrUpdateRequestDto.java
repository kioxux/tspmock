package com.gov.purchase.module.store.dto.store;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@ApiModel(value = "门店信息更新或者新增传入参数")
public class StoreInfoInsertOrUpdateRequestDto {

    @ApiModelProperty(value = "门店编号", name = "stoCode")
    @NotBlank(message = "门店编号不能为空")
    @Size(min = 0, max = 10, message = "门店编号长度超限")
    private String stoCode;

    @ApiModelProperty(value = "门店名称", name = "stoName")
    @NotBlank(message = "门店名称不能为空")
    @Size(min = 0, max = 50, message = "门店名称长度超限")
    private String stoName;

    @ApiModelProperty(value = "助记码", name = "stoPym")
    @Size(min = 0, max = 50, message = "助记码长度超限")
    private String stoPym;

    @ApiModelProperty(value = "门店状态（0-营业、1-闭店）", name = "stoStatus")
    @NotBlank(message = "门店状态不能为空")
    @Size(min = 0, max = 1, message = "门店状态长度超限")
    private String stoStatus;

    @ApiModelProperty(value = "门店简称", name = "stoShortName")
    @NotBlank(message = "门店简称不能为空")
    @Size(min = 0, max = 10, message = "门店简称长度超限")
    private String stoShortName;

    @ApiModelProperty(value = "门店属性（1-单体、2-连锁、3-加盟、4-门诊）", name = "stoAttribute")
    @NotBlank(message = "门店属性不能为空")
    @Size(min = 0, max = 1, message = "门店属性长度超限")
    private String stoAttribute;

    @ApiModelProperty(value = "配送方式（1-自采、2-总部配送、3-委托配送）", name = "stoDeliveryMode")
    @Size(min = 0, max = 1, message = "配送方式长度超限")
    @NotBlank(message = "配送方式不能为空")
    private String stoDeliveryMode;

    @ApiModelProperty(value = "经营面积（1- 60-80㎡；80-100㎡；100-120㎡；120㎡以上）", name = "stoArea")
    @NotBlank(message = "经营面积不能为空")
    private String stoArea;

    @ApiModelProperty(value = "关联门店(GAIA_STORE_DATA)", name = "stoRelationStore")
    @Size(min = 0, max = 50, message = "关联门店长度超限")
    private String stoRelationStore;

    @ApiModelProperty(value = "省", name = "stoProvince")
    @NotBlank(message = "省不能为空")
    @Size(min = 0, max = 20, message = "省长度超限")
    private String stoProvince;

    @ApiModelProperty(value = "市", name = "stoCity")
    @NotBlank(message = "市不能为空")
    @Size(min = 0, max = 20, message = "市长度超限")
    private String stoCity;

    @ApiModelProperty(value = "区", name = "stoDistrict")
    @NotBlank(message = "区不能为空")
    @Size(min = 0, max = 20, message = "区长度超限")
    private String stoDistrict;

    @ApiModelProperty(value = "详细地址", name = "stoAdd")
    @Size(min = 0, max = 50, message = "详细地址长度超限")
    private String stoAdd;

    @ApiModelProperty(value = "所属加盟商编号（GAIA_FRANCHISEE）", name = "client")
    @NotBlank(message = "所属加盟商编号不能为空")
    @Size(min = 0, max = 8, message = "所属加盟商编号长度超限")
    private String client;

    @ApiModelProperty(value = "DTP（0-否；1-是）", name = "stoIfDtp")
    @Size(min = 0, max = 1, message = "DTP长度超限")
    private String stoIfDtp;

    @ApiModelProperty(value = "是否医保店（0-否；1-是）", name = "stoIfMedicalcare")
    @Size(min = 0, max = 1, message = "是否医保店长度超限")
    private String stoIfMedicalcare;

    @ApiModelProperty(value = "连锁总部 (源自：GAIA_DC_DATA)", name = "stoChainHead")
    @Size(min = 0, max = 10, message = "连锁总部长度超限")
    private String stoChainHead;

    @ApiModelProperty(value = "委托配送公司(源自：GAIA_DELIVERY_COMPANY)", name = "stoDeliveryCompany")
    @Size(min = 0, max = 50, message = "委托配送公司长度超限")
    private String stoDeliveryCompany;

    @ApiModelProperty(value = "开业日期", name = "stoOpenDate")
    @Size(min = 0, max = 8, message = "开业日期长度超限")
    private String stoOpenDate;

    @ApiModelProperty(value = "闭店日期", name = "stoCloseDate")
    @Size(min = 0, max = 8, message = "闭店日期长度超限")
    private String stoCloseDate;

    @ApiModelProperty(value = "税分类（1-小规模纳税人，2-一般纳税人）", name = "stoTaxClass")
    @NotBlank(message = "税分类不能为空")
    @Size(min = 0, max = 1, message = "税分类长度超限")
    private String stoTaxClass;

    @ApiModelProperty(value = "纳税主体 (源自：GAIA_DC_DATA)", name = "stoTaxSubject")
//    @Size(min = 0, max =  10,message = "纳税主体长度超限")
    private String stoTaxSubject;

    @ApiModelProperty(value = "税率（1-0%，2-3%，3-6%，4-10%，5-13%）", name = "stoTaxRate")
    @NotBlank(message = "税率不能为空")
    @Size(min = 0, max = 10, message = "税率长度超限")
    private String stoTaxRate;

    @ApiModelProperty(value = "配送中心", name = "stoDcCode")
    private String stoDcCode;

    /**
     * 收货人签章图片
     */
    private String stoShrqxtp;

    /**
     * 运输时效（小时）
     */
    private String stoYssx;

    /**
     * 是否启用信用管理（0-否，1-是）
     */
    private String stoCreditFlag;

    /**
     *信用额度
     */
    private BigDecimal stoCreditQuota;

    /**
     * 账期类型（0-账期天数，1-固定账期日）
     */
    private String stoZqlx;

    /**
     * 固定账期天数
     */
    private Integer stoZqts;

    /**
     * 欠款标志,0-禁止补货或铺货，1-不禁止
     */
    private String stoArrearsFlag;

    /**
     * 电商文案 1仓库所有合格品;2仅电商仓库
     */
    private String wmsDsfa;

}
