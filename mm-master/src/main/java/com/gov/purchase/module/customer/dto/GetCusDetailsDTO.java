package com.gov.purchase.module.customer.dto;

import com.gov.purchase.entity.GaiaCustomerBusiness;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetCusDetailsDTO extends GaiaCustomerBusiness {

    private String cusPictureFrwtsUrl;
    private String cusPictureGspUrl;
    private String cusPictureGmpUrl;
    private String cusPictureShtxdUrl;
    private String cusPictureQtUrl;



    /**
     * 食品许可证书图片
     */
    private String cusPictureSpxkzUrl;
    /**
     * 医疗器械经营许可证图片
     */
    private String cusPictureYlqxxkzUrl;
    /**
     * 医疗器械经营许可证有效期
     */
    private String cusYlqxxkzDateUrl;
    /**
     * 医疗机构执业许可证图片
     */
    private String cusPictureYljgxkzUrl;


}
