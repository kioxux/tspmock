package com.gov.purchase.module.base.service.impl;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.module.base.dto.businessImport.ImportDto;
import com.gov.purchase.module.base.service.BusinessImportService;
import com.gov.purchase.module.base.service.impl.businessImport.*;
import com.gov.purchase.utils.EnumUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class BusinessImportServiceImpl implements BusinessImportService {

    @Resource
    PriceAdjustmentImport priceAdjustmentImport;
    @Resource
    GoodsStateUpdateImport goodsStateUpdateImport;
    @Resource
    PermitDataImport permitDataImport;
    @Resource
    PermitAddImport permitAddImport;
    @Resource
    AddBusinessScopeImport addBusinessScopeImport;
    @Resource
    BusinessScopeInfoImport businessScopeInfoImport;
    @Resource
    PurchaseReturnsImport purchaseReturnsImport;
    @Resource
    DcReplenishmentImport dcReplenishmentImport;
    @Resource
    SourceListImport sourceListImport;
    @Resource
    GoodsRecallImport goodsRecallImport;
    @Resource
    AllotPriceImport allotPriceImport;
    @Resource
    StoreDistributionImport storeDistributionImport;
    @Resource
    StoreReplenishmentImport storeReplenishmentImport;
    @Resource
    InvioceInfoImport invioceInfoImport;
    @Resource
    ProductRelateImport productRelateImport;
    @Resource
    CourseQuestionImport courseQuestionImport;
    @Resource
    PaymentMaintenanceImport paymentMaintenanceImport;
    @Resource
    ProductMinQtyImport productMinQtyImport;
    @Resource
    StoreNeedImport storeNeedImport;
    @Resource
    AllotPriceCusImport allotPriceCusImport;
    @Resource
    PurchasePoImport purchasePoImport;
    @Resource
    DcreplenisServiceImpl dcreplenisService;
    @Resource
    NoDcReplenishmentImport noDcReplenishmentImport;
    @Resource
    ProductBatchUpdateImport productBatchUpdateImport;
    @Resource
    GroupPriceProductImport groupPriceProductImport;
    @Resource
    GroupPriceImport groupPriceImport;
    @Resource
    WholeSalePromport wholeSalePromport;
    @Resource
    WholeSaleConsignorProImport wholeSaleConsignorProImport;
    @Resource
    PriceGroupProductImport priceGroupProductImport;
    @Resource
    SalesOrderProductsImport salesOrderProductsImport;
    @Resource
    PurchaseContractImport purchaseContractImport;

    /**
     * 业务数据导入
     *
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result businessImport(ImportDto dto) {
        // 业务不存在
        if (!EnumUtils.contains(dto.getType(), CommonEnum.BusinessImportTypet.class)) {
            return ResultUtil.error(ResultEnum.E0102);
        }
        // 调价
        if (CommonEnum.BusinessImportTypet.PRICEADJUSTMENT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return priceAdjustmentImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 商品状态批次更新
        if (CommonEnum.BusinessImportTypet.GOODSSTATEUPDATE.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return goodsStateUpdateImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 编辑证照
        if (CommonEnum.BusinessImportTypet.PERMITDATAINFO.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return permitDataImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 新增证照
        if (CommonEnum.BusinessImportTypet.PERMITADD.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return permitAddImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 新增经营范围
        if (CommonEnum.BusinessImportTypet.ADDBUSINESSSCOPE.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return addBusinessScopeImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 新增范围编辑
        if (CommonEnum.BusinessImportTypet.BUSINESSSCOPEINFO.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return businessScopeInfoImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 采购退货单处理
        if (CommonEnum.BusinessImportTypet.PURCHASERETURNS.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return purchaseReturnsImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // DC补货
        if (CommonEnum.BusinessImportTypet.DCREPLENISHMENT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return dcReplenishmentImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 商品召回
        if (CommonEnum.BusinessImportTypet.GOODSRECALL.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return goodsRecallImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 调拨价格设置
        if (CommonEnum.BusinessImportTypet.ALLOTPRICE.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return allotPriceImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 货源清单
        if (CommonEnum.BusinessImportTypet.SOURCELIST.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return sourceListImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 门店铺货
        if (CommonEnum.BusinessImportTypet.STOREDISTRIBUTION.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return storeDistributionImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 门店补货
        if (CommonEnum.BusinessImportTypet.STOREREPLENISHMENT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return storeReplenishmentImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 发票维护
        if (CommonEnum.BusinessImportTypet.INVOICEINFO.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return invioceInfoImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 煎药维护
        if (CommonEnum.BusinessImportTypet.PRODUCTRELATE.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return productRelateImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 题目导入
        if (CommonEnum.BusinessImportTypet.COURSEQUESTION.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return courseQuestionImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 付款基础数据维护
        if (CommonEnum.BusinessImportTypet.PAYMENTMAINTENANCE.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return paymentMaintenanceImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 最小陈列量
        if (CommonEnum.BusinessImportTypet.PRODUCTMINQTY.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return productMinQtyImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 门店补货导入（新版）
        if (CommonEnum.BusinessImportTypet.STORENEED.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return storeNeedImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 客户调拨价格设置
        if (CommonEnum.BusinessImportTypet.ALLOTPRICECUS.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return allotPriceCusImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 采购订单导入
        if (CommonEnum.BusinessImportTypet.PURCHASEPO.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return purchasePoImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 仓库补货参数导入
        if (CommonEnum.BusinessImportTypet.DCREPLENISIMP.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return dcreplenisService.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 批量修改商品主数据
        if (CommonEnum.BusinessImportTypet.PRODUCTBATCHUPDATE.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return productBatchUpdateImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 无仓库补货导入
        if (CommonEnum.BusinessImportTypet.NODCREPLENISHMENT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return noDcReplenishmentImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 价格组目录商品导入
        if (CommonEnum.BusinessImportTypet.PRICEGROUPPROD.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return groupPriceProductImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 价格组目录组员导入
        if (CommonEnum.BusinessImportTypet.GROUPPRICE.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return groupPriceImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 批发渠道明细导入
        if (CommonEnum.BusinessImportTypet.WHOLESALEPRODUCT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return wholeSalePromport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 批发货主商品明细导入
        if (CommonEnum.BusinessImportTypet.WHOLESALECONSIGNORPRODUCT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return wholeSaleConsignorProImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 价格组商品导入
        if (CommonEnum.BusinessImportTypet.PRICEGROUPPRODUCT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return priceGroupProductImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 销售订单批量导入
        if (CommonEnum.BusinessImportTypet.SALESORDERPRODUCTS.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return salesOrderProductsImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }
        // 采购合同
        if (CommonEnum.BusinessImportTypet.PURCHASECONTRACT.equals(CommonEnum.BusinessImportTypet.getBusinessImportTypet(dto.getType()))) {
            return purchaseContractImport.businessImport(dto.getPath(), dto.getType(), dto.getDataMap());
        }

        return ResultUtil.success();
    }
}
