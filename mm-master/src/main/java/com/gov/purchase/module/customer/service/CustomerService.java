package com.gov.purchase.module.customer.service;


import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.customer.dto.*;

public interface CustomerService {


     PageInfo<CustomerListReponseDto> queryCustomerList(CustomerListRequestDto customerListRequestDto);

     CustomerInfoReponseDto  getCustomerInfo(CustomerInfoRequestDto dto);

     Result validCustomerInfo(CustomerInfoSaveRequestDto dto);

     Result saveCustomerInfo(CustomerInfoSaveRequestDto dto);


}
