package com.gov.purchase.module.wholesale.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author zhoushuai
 * @date 2021/4/1 13:22
 */
@Data
@EqualsAndHashCode
public class SalesOrderHeader {
    /**
     * 加盟商
     */
    private String client;
    /**
     * 加盟商名
     */
    private String francName;
    /**
     * 公司
     */
    private String dcNameLine;
    /**
     * 销售主体
     */
    private String soCompanyCode;
    /**
     * 销售主体名
     */
    private String dcName;
    /**
     * 创建人
     */
    private String soCreateBy;
    /**
     * 创建人
     */
    private String soCreateByName;
    /**
     * 销售订单号
     */
    private String soId;
    /**
     * 订单类型
     */
    private String soType;
    /**
     * 订单类型名称
     */
    private String ordTypeCesc;
    /**
     * 订单日期
     */
    private String soDate;
    /**
     * 客户编码
     */
    private String soCustomerId;
    /**
     * 客户名
     */
    private String cusName;
    /**
     * 付款条款
     */
    private String soPaymentId;
    /**
     * 付款条款描述
     */
    private String payTypeDesc;
    /**
     * 审批状态 0-不需审批，1-未审批，2-已审批
     */
    private String soApproveStatus;
    /**
     * 抬头备注
     */
    private String soHeadRemark;

    /**
     * 订单总金额
     */
    private BigDecimal soLineAmtOrder;

    /**
     * 客户编码
     */
    private String soCustomer2;
    /**
     * 客户名称
     */
    private String compadmName;
}
