package com.gov.purchase.module.wholesale.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaOwnerCustomerKey;
import com.gov.purchase.entity.GaiaOwnerProductKey;
import com.gov.purchase.entity.GaiaWholesaleOwner;
import com.gov.purchase.entity.GaiaWholesaleSalesman;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaOwnerCustomerMapper;
import com.gov.purchase.mapper.GaiaOwnerProductMapper;
import com.gov.purchase.mapper.GaiaWholesaleOwnerMapper;
import com.gov.purchase.mapper.GaiaWholesaleSalesmanMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.wholesale.dto.ControlWholesaleOrdersVO;
import com.gov.purchase.module.wholesale.dto.OwnerCustomerVO;
import com.gov.purchase.module.wholesale.dto.WholesaleConsignorDTO;
import com.gov.purchase.module.wholesale.service.WholesaleConsignorService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: 批发订单按货主管控功能
 * @author: yzf
 * @create: 2021-11-23 13:17
 */
@Service
public class WholesaleConsignorServiceImpl implements WholesaleConsignorService {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaOwnerCustomerMapper gaiaOwnerCustomerMapper;
    @Resource
    private GaiaOwnerProductMapper gaiaOwnerProductMapper;
    @Resource
    private GaiaWholesaleOwnerMapper gaiaWholesaleOwnerMapper;
    @Resource
    private GaiaWholesaleSalesmanMapper gaiaWholesaleSalesmanMapper;

    /**
     * 根据仓库查询批发货主客户
     *
     * @param dcCode
     * @return
     */
    @Override
    public List<OwnerCustomerVO> queryOwnerCustomer(String dcCode) {
        TokenUser user = feignService.getLoginInfo();
        return gaiaOwnerCustomerMapper.selectCusByDcCodeAndOwner(user.getClient(), dcCode, "");
    }

    /**
     * 批发货主订单查询
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Override
    public Result queryControlWholesaleOrders(WholesaleConsignorDTO wholesaleConsignorDTO) {
        // 判断仓库编码非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_SITE_EMPTY);
        }
        TokenUser user = feignService.getLoginInfo();
        wholesaleConsignorDTO.setClient(user.getClient());
        PageHelper.startPage(wholesaleConsignorDTO.getPageNum(), wholesaleConsignorDTO.getPageSize());
        // 销售员列表
        List<GaiaWholesaleSalesman> salesmen = gaiaWholesaleSalesmanMapper.getSalesmanList(user.getClient(), wholesaleConsignorDTO.getDcCode());
        PageInfo<ControlWholesaleOrdersVO> pageInfo = new PageInfo<>(gaiaWholesaleOwnerMapper.queryControlWholesaleOrders(wholesaleConsignorDTO));
        if (CollectionUtils.isNotEmpty(pageInfo.getList()) && CollectionUtils.isNotEmpty(salesmen)) {
            pageInfo.getList().forEach(
                    item -> {
                        if (StringUtils.isNotBlank(item.getGwoOwnerSaleman())) {
                            // 过滤获取销售员名称
                            List<String> saleIds = Arrays.asList(item.getGwoOwnerSaleman().split(","));
                            List<String> result = saleIds.stream()
                                    .map(saleId -> salesmen.stream()
                                            .filter(sales -> saleId.equals(sales.getGwsCode()))
                                            .findFirst()
                                            .map(GaiaWholesaleSalesman::getGssName).orElse(null))
                                    .collect(Collectors.toList());
                            if (CollectionUtils.isNotEmpty(result)) {
                                item.setSalesName(StringUtils.join(result.toArray(), ","));
                            }
                        }
                    }
            );
        }
        return ResultUtil.success(pageInfo);
    }

    /**
     * 新增批发货主
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Override
    public Result insertControlWholesaleOrders(WholesaleConsignorDTO wholesaleConsignorDTO) {
        GaiaWholesaleOwner gaiaWholesaleOwner = new GaiaWholesaleOwner();
        TokenUser user = feignService.getLoginInfo();
        gaiaWholesaleOwner.setClient(user.getClient());
        gaiaWholesaleOwner.setGwoOwnerSaleman(wholesaleConsignorDTO.getOwnerSaleman());
        gaiaWholesaleOwner.setGwoStatus(wholesaleConsignorDTO.getStatus());
        gaiaWholesaleOwner.setGwoOwnerTel(wholesaleConsignorDTO.getOwnerTel());
        // 判断仓库编码非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_SITE_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoSite(wholesaleConsignorDTO.getDcCode());
        }
        // 判断货主名称非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getOwnerName())) {
            return ResultUtil.error(ResultEnum.OWNER_NAME_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoOwnerName(wholesaleConsignorDTO.getOwnerName());
        }
        // 判断客户规则非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getCusRule())) {
            return ResultUtil.error(ResultEnum.OWNER_CUS_RULE_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoCusRule(wholesaleConsignorDTO.getCusRule());
        }
        // 判断商品规则非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_PRO_RULE_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoProRule((wholesaleConsignorDTO.getProRule()));
        }
        String owner = gaiaWholesaleOwnerMapper.queryOwner(user.getClient());
        // 渠道自编码： 已有最大值加1
        gaiaWholesaleOwner.setGwoOwner(StringUtils.isNotBlank(owner) ? String.valueOf(Integer.parseInt(owner) + 1) : "1");
        gaiaWholesaleOwner.setGwoCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        gaiaWholesaleOwner.setGwoCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        gaiaWholesaleOwner.setGwoCreateBy(user.getUserId());
        // 新增批发渠道
        return ResultUtil.success(gaiaWholesaleOwnerMapper.insert(gaiaWholesaleOwner));
    }


    /**
     * 更新批发货主
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Override
    public Result updateOwner(WholesaleConsignorDTO wholesaleConsignorDTO) {
        GaiaWholesaleOwner gaiaWholesaleOwner = new GaiaWholesaleOwner();
        TokenUser user = feignService.getLoginInfo();
        gaiaWholesaleOwner.setClient(user.getClient());
        gaiaWholesaleOwner.setGwoOwnerSaleman(wholesaleConsignorDTO.getOwnerSaleman());
        gaiaWholesaleOwner.setGwoStatus(wholesaleConsignorDTO.getStatus());
        gaiaWholesaleOwner.setGwoOwnerTel(wholesaleConsignorDTO.getOwnerTel());
        // 判断仓库编码非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_SITE_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoSite(wholesaleConsignorDTO.getDcCode());
        }
        // 判断货主名称非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getOwnerName())) {
            return ResultUtil.error(ResultEnum.OWNER_NAME_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoOwnerName(wholesaleConsignorDTO.getOwnerName());
        }
        // 判断客户规则非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getCusRule())) {
            return ResultUtil.error(ResultEnum.OWNER_CUS_RULE_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoCusRule(wholesaleConsignorDTO.getCusRule());
        }
        // 判断商品规则非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_PRO_RULE_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoProRule((wholesaleConsignorDTO.getProRule()));
        }
        gaiaWholesaleOwner.setGwoOwner(wholesaleConsignorDTO.getOwner());
        gaiaWholesaleOwner.setGwoChangeDate(DateUtils.getCurrentDateStrYYMMDD());
        gaiaWholesaleOwner.setGwoChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
        gaiaWholesaleOwner.setGwoChangeBy(user.getUserId());
        return ResultUtil.success(gaiaWholesaleOwnerMapper.updateOwner(gaiaWholesaleOwner));
    }

    /**
     * 根据仓库地点查询货主客户
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Override
    public Result queryOwnerCustomer(WholesaleConsignorDTO wholesaleConsignorDTO) {
        TokenUser user = feignService.getLoginInfo();
        // 判断仓库编码非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_SITE_EMPTY);
        }
        // 判断货主编码非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getOwner())) {
            return ResultUtil.error(ResultEnum.OWNER_EMPTY);
        }
        return ResultUtil.success(gaiaOwnerCustomerMapper.selectCusByDcCodeAndOwner(user.getClient(), wholesaleConsignorDTO.getDcCode(), wholesaleConsignorDTO.getOwner()));
    }

    /**
     * 根据仓库地点查询货主商品
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Override
    public Result queryOwnerProduct(WholesaleConsignorDTO wholesaleConsignorDTO) {
        TokenUser user = feignService.getLoginInfo();
        // 判断仓库编码非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_SITE_EMPTY);
        }
        // 判断货主编码非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getOwner())) {
            return ResultUtil.error(ResultEnum.OWNER_EMPTY);
        }
        return ResultUtil.success(gaiaOwnerProductMapper.selectProByDcCodeAndOwner(user.getClient(), wholesaleConsignorDTO.getDcCode(), wholesaleConsignorDTO.getOwner()));
    }


    /**
     * 删除批发货主
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result delOwner(WholesaleConsignorDTO wholesaleConsignorDTO) {
        GaiaWholesaleOwner gaiaWholesaleOwner = new GaiaWholesaleOwner();
        GaiaOwnerCustomerKey gaiaOwnerCustomer = new GaiaOwnerCustomerKey();
        GaiaOwnerProductKey gaiaOwnerProduct = new GaiaOwnerProductKey();
        TokenUser user = feignService.getLoginInfo();
        gaiaWholesaleOwner.setClient(user.getClient());
        gaiaOwnerCustomer.setClient(user.getClient());
        gaiaOwnerProduct.setClient(user.getClient());
        // 判断仓库编码非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_SITE_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoSite(wholesaleConsignorDTO.getDcCode());
            gaiaOwnerCustomer.setGocSite(wholesaleConsignorDTO.getDcCode());
            gaiaOwnerProduct.setGopSite(wholesaleConsignorDTO.getDcCode());
        }
        // 判断货主编码非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getOwner())) {
            return ResultUtil.error(ResultEnum.OWNER_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoOwner(wholesaleConsignorDTO.getOwner());
            gaiaOwnerCustomer.setGocOwner(wholesaleConsignorDTO.getOwner());
            gaiaOwnerProduct.setGopOwner(wholesaleConsignorDTO.getOwner());
        }
        // 判断客户规则非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getCusRule())) {
            return ResultUtil.error(ResultEnum.OWNER_CUS_RULE_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoCusRule(wholesaleConsignorDTO.getCusRule());
        }
        // 判断商品规则非空
        if (StringUtils.isBlank(wholesaleConsignorDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_PRO_RULE_EMPTY);
        } else {
            gaiaWholesaleOwner.setGwoProRule((wholesaleConsignorDTO.getProRule()));
        }
        // 删除货主
        gaiaWholesaleOwnerMapper.deleteByPrimaryKey(gaiaWholesaleOwner);
        // 删除货主客户
        gaiaOwnerCustomerMapper.deleteByPrimaryKey(gaiaOwnerCustomer);
        // 删除货主商品
        gaiaOwnerProductMapper.deleteByPrimaryKey(gaiaOwnerProduct);
        return ResultUtil.success();
    }

    /**
     * 批量保存明细
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveOwnerDetail(WholesaleConsignorDTO dto) {
        // 判断仓库编码非空
        if (StringUtils.isBlank(dto.getDcCode())) {
            return ResultUtil.error(ResultEnum.OWNER_SITE_EMPTY);
        }
        // 判断货主编码非空
        if (StringUtils.isBlank(dto.getOwner())) {
            return ResultUtil.error(ResultEnum.OWNER_EMPTY);
        }
        TokenUser user = feignService.getLoginInfo();
        if (dto.getProductList() != null) {
            gaiaOwnerProductMapper.deleteOwnerPro(user.getClient(), dto.getDcCode(), dto.getOwner());
            dto.getProductList().forEach(item -> {
                item.setClient(user.getClient());
                item.setGopOwner(dto.getOwner());
                item.setGopSite(dto.getDcCode());
                item.setGopCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                item.setGopCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                item.setGopCreateBy(user.getUserId());
            });
            //批量插入商品
            gaiaOwnerProductMapper.insertBatchOwnerProduct(dto.getProductList());
        }
        if (dto.getCustomerList() != null) {
            gaiaOwnerCustomerMapper.deleteOwnerCus(user.getClient(), dto.getDcCode(), dto.getOwner());
            dto.getCustomerList().forEach(item -> {
                item.setClient(user.getClient());
                item.setGocOwner(dto.getOwner());
                item.setGocSite(dto.getDcCode());
                item.setGocCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                item.setGocCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                item.setGocCreateBy(user.getUserId());
            });
            //批量插入客户
            gaiaOwnerCustomerMapper.insertBatchOwnerCustomer(dto.getCustomerList());
        }
        return ResultUtil.success();
    }

    @Override

    public Result queryWholesaleConsignor(String dcCode, String saleManCode) {
        TokenUser user = feignService.getLoginInfo();
        return ResultUtil.success(gaiaWholesaleOwnerMapper.queryWholesaleConsignor(user.getClient(), dcCode, saleManCode, null));
    }
}
