package com.gov.purchase.module.store.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "DC列表传入参数")
public class DcDataListRequestDto extends Pageable {

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "DC编码", name = "dcCode")
    private String dcCode;

    @ApiModelProperty(value = "DC名称", name = "dcName")
    private String dcName;

    @ApiModelProperty(value = "DC状态", name = "dcStatus")
    private String dcStatus;


}
