package com.gov.purchase.module.wholesale.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.08
 */
@Data
public class UpdateSalesOrderItem {
    /**
     * 订单行号(新增时，不填写)
     */
    private String soLineNo;
    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码", name = "soProCode")
    @NotBlank(message = "商品编码不能为空")
    private String soProCode;
    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", name = "soQty")
    @NotNull(message = "数量不能为空")
    @DecimalMin(value = "0.0", message = "数量不能为负数")
    @DecimalMax(value = "999999999.9999", message = "数量格式不正确")
    private BigDecimal soQty;
    /**
     * 单位
     */
    @ApiModelProperty(value = "单位", name = "soUnit")
    @NotBlank(message = "单位不能为空")
    private String soUnit;
    /**
     * 单价
     */
    @ApiModelProperty(value = "单价", name = "soPrice")
    @NotNull(message = "单价不能为空")
    @DecimalMin(value = "0.0", message = "单价不能为负数")
    @DecimalMax(value = "999999999.9999", message = "单价格式不正确")
    private BigDecimal soPrice;
    /**
     * 行总价
     */
    @ApiModelProperty(value = "行总价", name = "soLineAmt")
    @NotNull(message = "行总价不能为空")
    @DecimalMin(value = "0.0", message = "行总价不能为负数")
    @DecimalMax(value = "999999999.9999", message = "行总价格式不正确")
    private BigDecimal soLineAmt;
    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "soSiteCode")
    @NotBlank(message = "地点不能为空")
    private String soSiteCode;
    /**
     * 库位
     */
    private String soLocationCode;
    /**
     * 批次
     */
    @ApiModelProperty(value = "批次", name = "soBatch")
//    @NotBlank(message = "批次不能为空")
    private String soBatch;
    /**
     * 税率
     */
    private String soRate;
    /**
     * 计划发货日期
     */
    private String soDeliveryDate;
    /**
     * 行备注
     */
    private String soLineRemark;
    /**
     * 删除标记
     */
    private String soLineDelete;
    /**
     * 交货已完成标记
     */
    private String soCompleteFlag;

    /**
     * 生产批号
     */
    private String soBatchNo;
}

