package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "厂商库搜索传入参数")
public class QuerySupListRequestDto extends Pageable {

    @ApiModelProperty(value = "地点", name = "supSite")
    private String supSite;

    @ApiModelProperty(value = "供应商名称", name = "supName")
    private String supName;

    @ApiModelProperty(value = "统一社会信用代码", name = "supCreditCode")
    private String supCreditCode;

    @ApiModelProperty(value = "供应商状态", name = "supStatus")
    private String supStatus;

    @ApiModelProperty(value = "是否经营", name = "sell")
    private String sell;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    private String supNdbgyxq;
}
