package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "要货保存传入参数")
public class DirectPurchaseSummaryListVO {
    private String stoCodeName;
    private String voucherCode;
    // 请货类型
    private String pattern;
    private String gsrhDate;
    private Integer gsrdSerialTotal;
    private BigDecimal gsrdNeedQtyTotal;
    List<DirectPurchaseListVO> voList;
}
