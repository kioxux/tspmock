package com.gov.purchase.module.replenishment.dto;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.01.12
 */

import lombok.Data;

import java.math.BigDecimal;

/**
 * 批号信息
 */
@Data
public class BatchNoInfo {
    /**
     * 生产批号
     */
    private String batBatchNo;

    /**
     * 效期
     */
    private String batExpiryDate;

    /**
     * 批号库存
     */
    private BigDecimal wmKysl;

    /**
     * 铺货数量
     */
    private BigDecimal gsrdNeedQty;

    /**
     * 批次进价
     */
    private BigDecimal batPoPrice;

    /**
     * 货位号
     */
    private String wmHwh;

    /**
     * 批次
     */
    private String wmPch;
}
