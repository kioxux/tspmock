package com.gov.purchase.module.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "商品列表返回参数")
public class QueryProResponseDto {

    @ApiModelProperty(value = "商品编码", name = "proSelfCode")
    private String proSelfCode;

    @ApiModelProperty(value = "商品名", name = "proName")
    private String proName;

    @ApiModelProperty(value = "批准文号", name = "proRegisterNo")
    private String proRegisterNo;

    @ApiModelProperty(value = "生产厂家", name = "proFactoryName")
    private String proFactoryName;

    @ApiModelProperty(value = "规格", name = "proSpecs")
    private String proSpecs;

    @ApiModelProperty(value = "国际条形码", name = "proBarcode")
    private String proBarcode;

    @ApiModelProperty(value = "单位", name = "proUnit")
    private String proUnit;

    @ApiModelProperty(value = "状态", name = "proStatus")
    private String proStatus;

    @ApiModelProperty(value = "地点", name = "proSite")
    private String proSite;

    @ApiModelProperty(value = "地点名", name = "proSiteName")
    private String proSiteName;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "商品编码", name = "proCode")
    private String proCode;

    /**
     * 参考毛利率
     */
    @ApiModelProperty(value = "参考毛利率", name = "proMll")
    private BigDecimal proMll;

    @ApiModelProperty(value = "商品通用名", name = "proCommonname")
    private String proCommonname;
}
