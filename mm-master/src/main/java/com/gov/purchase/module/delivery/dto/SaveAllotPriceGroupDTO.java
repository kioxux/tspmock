package com.gov.purchase.module.delivery.dto;

import com.gov.purchase.entity.GaiaAllotPriceGroup;
import lombok.Data;

import java.util.List;

@Data
public class SaveAllotPriceGroupDTO {
    /**
     * 类型 1 配送 2 批发
     */
    private Integer gapgType;
    private String alpReceiveSite;
    private List<GaiaAllotPriceGroup> voList;
}
