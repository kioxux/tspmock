package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaSdReplenishH;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 门店要货订单列表
 * @author: yzf
 * @create: 2021-11-15 11:40
 */
@Data
public class SdReplenishHExtandVO extends GaiaSdReplenishH {
    /**
     * 连锁总部id
     */
    private String compadmId;

    /**
     * 门店名称
     */
    private String stoName;

    /**
     * 需求行数
     */
    private Integer line;

    /**
     * 需求总数量
     */
    private BigDecimal totalAmt;

    /**
     * 需求总金额
     */
    private BigDecimal totalQty;

    /**
     * 备注
     */
    private String gsrdBz;

    /**
     * 审核人
     */
    private String gsrhDnByName;

    /**
     * 补货地点
     */
    private String gsrhAddr;

    /**
     * 采购订单号
     */
    private String poId;
}
