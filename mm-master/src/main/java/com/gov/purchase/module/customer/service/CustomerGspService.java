package com.gov.purchase.module.customer.service;


import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaCustomerBasic;
import com.gov.purchase.module.customer.dto.*;

public interface CustomerGspService {

    /**
     * 客户库搜索列表
     *
     * @param dto 查询条件
     * @return
     */
    PageInfo<CustomerGspListResponseDto> queryCustomerList(CustomerGspListRequestDto dto);

    /**
     * 客户基础信息初始化
     *
     * @param cusCode 客户编号
     * @return
     */
    GaiaCustomerBasic getCustomerBasicInfo(String cusCode);

    /**
     * 客户首营提交
     *
     * @param dto
     * @return
     */
    Result saveCustomerGspInfo(CustomerGspRequestDto dto);

    /**
     * 客户首营列表
     *
     * @param dto
     * @return
     */
    PageInfo<CustomerGspInfoListReponseDto> queryCustomerGspList(CustomerGspInfoListRequestDto dto);

    /**
     * 客户首营详情
     *
     * @param dto
     * @return
     */
    CustomerGspInfoDetailReponseDto queryCustomerGspDetail(CustomerGspInfoDetailRequestDto dto);

    /**
     * 最大自编码
     * @param client
     * @param cusSite
     * @param code
     * @return
     */
    Result getMaxSelfCodeNum(String client, String cusSite, String code);

    /**
     * 手工新增
     * @param customerGspRequestDto
     * @return
     */
    Result addBusioness(CustomerGspRequestDto customerGspRequestDto);


    /**
     * 导出客户首营列表
     * @param dto
     * @return
     */
    Object exportQueryCustomerGspList(CustomerGspInfoListRequestDto dto);
}
