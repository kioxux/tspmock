package com.gov.purchase.module.purchase.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.purchase.dto.GaiaContractZDetails;
import com.gov.purchase.module.purchase.dto.PurchaseContractDTO;
import com.gov.purchase.module.purchase.dto.PurchaseContractVO;
import com.gov.purchase.module.purchase.dto.SalesOrderRecordDTO;
import com.gov.purchase.module.purchase.service.PurchaseContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 采购合同功能
 * @author: yzf
 * @create: 2022-01-28 16:30
 */
@Api(tags = "采购合同")
@RestController
@RequestMapping("purchaseContract")
public class PurchaseContractController {

    @Resource
    private PurchaseContractService purchaseContractService;

    /**
     * 采购合同列表
     *
     * @return
     */
    @Log("采购合同下拉列表")
    @ApiOperation("采购合同下拉列表")
    @PostMapping("queryPurchaseContractRecord")
    public Result queryPurchaseContractRecord(
            @RequestJson(value = "conCompanyCode", name = "采购主体", required = false) String conCompanyCode,
            @RequestJson(value = "conSupplierId", name = "供应商", required = false) String conSupplierId,
            @RequestJson(value = "conSalesmanCode", required = false) String conSalesmanCode) {
        return purchaseContractService.queryPurchaseContractRecord(conCompanyCode, conSupplierId, conSalesmanCode);
    }

    @Log("采购合同列表")
    @ApiOperation("采购合同列表")
    @PostMapping("queryPurchaseContractList")
    public Result queryPurchaseContractList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                            @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                            @RequestJson(value = "conCompanyCode", required = false) String conCompanyCode,
                                            @RequestJson(value = "conSupplierId", required = false) String conSupplierId,
                                            @RequestJson(value = "proSelfCode", required = false) String proSelfCode,
                                            @RequestJson(value = "conType", required = false) String conType,
                                            @RequestJson(value = "conCreateDateStart", required = false) String conCreateDateStart,
                                            @RequestJson(value = "conCreateDateEnd", required = false) String conCreateDateEnd) {
        return purchaseContractService.queryPurchaseContractList(pageSize, pageNum, conCompanyCode, conSupplierId, proSelfCode, conType,
                conCreateDateStart, conCreateDateEnd);
    }

    @Log("采购合同列表导出")
    @ApiOperation("采购合同列表导出")
    @PostMapping("queryPurchaseContractListExport")
    public Result queryPurchaseContractListExport(@RequestJson(value = "conCompanyCode", required = false) String conCompanyCode,
                                                  @RequestJson(value = "conSupplierId", required = false) String conSupplierId,
                                                  @RequestJson(value = "proSelfCode", required = false) String proSelfCode,
                                                  @RequestJson(value = "conType", required = false) String conType,
                                                  @RequestJson(value = "conCreateDateStart", required = false) String conCreateDateStart,
                                                  @RequestJson(value = "conCreateDateEnd", required = false) String conCreateDateEnd) {
        return purchaseContractService.queryPurchaseContractListExport(conCompanyCode, conSupplierId, proSelfCode, conType, conCreateDateStart, conCreateDateEnd);
    }

    /**
     * 采购合同明细
     *
     * @return
     */
    @Log("采购合同明细")
    @ApiOperation("采购合同明细")
    @PostMapping("queryPurchaseContractDetail")
    public Result queryPurchaseContractDetail(@RequestJson(value = "conId", name = "合同编号") String conId) {
        return purchaseContractService.queryPurchaseContractDetail(conId);
    }

    /**
     * 采购合同作废
     *
     * @return
     */
    @Log("采购合同作废")
    @ApiOperation("采购合同作废")
    @PostMapping("invalidPurchaseContract")
    public Result invalidPurchaseContract(@RequestJson(value = "conId", name = "合同单号") String conId) {
        return purchaseContractService.invalidPurchaseContract(conId);
    }

    /**
     * 采购合同提交
     *
     * @return
     */
    @Log("采购合同提交")
    @ApiOperation("采购合同提交")
    @PostMapping("submitPurchaseContract")
    public Result submitPurchaseContract(@RequestBody GaiaContractZDetails gaiaContractZDetails) {
        return purchaseContractService.submitPurchaseContract(gaiaContractZDetails);
    }

    @Log("合同审批集合")
    @ApiOperation("合同审批集合")
    @PostMapping("queryPurchaseApproveContractList")
    public Result queryPurchaseApproveContractList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                                   @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                                   @RequestJson(value = "conCompanyCode", required = false) String conCompanyCode,
                                                   @RequestJson(value = "conSupplierId", required = false) String conSupplierId,
                                                   @RequestJson(value = "conSalesmanCode", required = false) String conSalesmanCode) {
        return purchaseContractService.queryPurchaseApproveContractList(pageSize, pageNum, conCompanyCode, conSupplierId, conSalesmanCode);
    }

    /**
     * 合同审批明细
     *
     * @param conId
     * @param conCompileIndex
     * @return
     */
    @Log("合同审批明细")
    @ApiOperation("合同审批明细")
    @PostMapping("queryPurchaseApproveContractDetails")
    public Result queryPurchaseApproveContractDetails(@RequestJson(value = "conId", name = "合同编号") String conId,
                                                      @RequestJson(value = "conCompanyCode", name = "地点") String conCompanyCode,
                                                      @RequestJson(value = "conSupplierId", name = "供应商") String conSupplierId,
                                                      @RequestJson(value = "conCompileIndex", name = "index") Integer conCompileIndex) {
        return purchaseContractService.queryPurchaseApproveContractDetails(conId, conCompanyCode, conSupplierId, conCompileIndex);
    }

    /**
     * 采购合同拒绝
     *
     * @return
     */
    @Log("采购合同拒绝")
    @ApiOperation("采购合同拒绝")
    @PostMapping("refusePurchaseContract")
    public Result refusePurchaseContract(@RequestBody List<GaiaContractZDetails> list) {
        purchaseContractService.refusePurchaseContract(list);
        return ResultUtil.success();
    }

    /**
     * 采购合同同意
     *
     * @return
     */
    @Log("采购合同同意")
    @ApiOperation("采购合同同意")
    @PostMapping("approvePurchaseContract")
    public Result approvePurchaseContract(@RequestBody List<GaiaContractZDetails> list) {
        purchaseContractService.approvePurchaseContract(list);
        return ResultUtil.success();
    }
}
