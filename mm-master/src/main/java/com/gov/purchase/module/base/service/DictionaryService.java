package com.gov.purchase.module.base.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.DictionaryParams;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.18
 */
public interface DictionaryService {

    /**
     * 数据字典集合
     * @return
     */
    Result getDictionary(DictionaryParams dictionaryParams);

    /**
     * 权限地点集合
     * @return
     */
    Result siteList();

    /**
     * 加盟商集合
     * @param client 加盟商ID
     * @return
     */
    Result franchiseeList(String client);

    /**
     * 门店信息集合
     * @param client 加盟商ID
     * @return
     */
    Result storeDataList(String client);

    /**
     * 静态数据字典集合
     *
     * @param key
     * @return
     */
    Result getDictionaryStatic(String key);

    /**
     * 权限地点集合首营用
     * @return
     */
    Result siteListForGspinfo();

    /**
     * 纳税主体选择
     * @param chainHead
     * @return
     */
    Result getTaxSubject(String chainHead);
}

