package com.gov.purchase.module.store.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.module.store.dto.DcDataListRequestDto;
import com.gov.purchase.module.store.dto.DcDataRequestDto;
import com.gov.purchase.module.store.dto.DcDataStatusRequestDto;
import com.gov.purchase.module.store.dto.GaiaDcDataListDto;

public interface DcDataService {

    /**
     * DC列表搜索
     *
     * @param dto 查询条件
     * @return
     */
    public PageInfo<GaiaDcDataListDto> queryDcList(DcDataListRequestDto dto);

    /**
     * DC编辑初始化
     *
     * @param client 加盟商
     * @param dcCode DC编码
     * @return
     */
    public GaiaDcData getDcInfo(String client, String dcCode);

    /**
     * DC验证
     *
     * @param dto
     * @return
     */
    public Result validDcInfo(DcDataRequestDto dto);

    /**
     * DC保存
     *
     * @param dto
     * @return
     */
    Result saveDcInfo(DcDataRequestDto dto);


    /**
     * DC状态更新
     *
     * @param dto
     * @return
     */
    Result updateDcStatus(DcDataStatusRequestDto dto);

    /**
     * 委托配送中心
     *
     * @param dcCode
     * @return
     */
    Result getEntrustDcList(String dcCode);
}
