package com.gov.purchase.module.replenishment.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductBusinessKey;
import com.gov.purchase.entity.GaiaSourceList;
import com.gov.purchase.entity.GaiaSourceListKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaSourceListMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.replenishment.dto.*;
import com.gov.purchase.module.replenishment.service.SourceService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class SourceServiceImpl implements SourceService {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaSourceListMapper gaiaSourceListMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    CosUtils cosUtils;

    /**
     * 货源清单创建、修改列表
     *
     * @param dto
     * @return
     */
    @Override
    public List<SourceCreateListResponseDto> querySourceCreateList(SourceCreateListRequestDto dto) {
        return gaiaSourceListMapper.querySourceCreateList(dto);
    }

    /**
     * 货源清单创建、修改
     *
     * @param list
     * @return
     */
    @Override
    @Transactional
    public Result saveSourceList(List<SaveSourceRequestDto> list) {
        TokenUser user = feignService.getLoginInfo();
        /**
         * 1.数据验证
         */
        if (list.size() == 0) {
            throw new CustomResultException(ResultEnum.SOURCE_LIST_EMPTY);
        }
        for (SaveSourceRequestDto item : list) {
            ValidationResult result = ValidateUtil.validateEntity(item);
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setClient(item.getClient());
            gaiaProductBusinessKey.setProSite(item.getSouSiteCode());
            gaiaProductBusinessKey.setProSelfCode(item.getSouProCode());
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
            if (gaiaProductBusiness == null) {
                throw new CustomResultException("请选择正确的商品");
            }

            /**
             * 2.根据类型 对应 新增、更新、删除
             */
            GaiaSourceList sourceList = new GaiaSourceList();
            BeanUtils.copyProperties(item, sourceList);

            // 设置主键
            GaiaSourceListKey sourceListKey = new GaiaSourceListKey();
            BeanUtils.copyProperties(item, sourceListKey);
            switch (item.getType()) {
                case "1":   // 新增
                    int count = gaiaSourceListMapper.selectCountBySupplier(sourceList);
                    if (count > 0) {
                        throw new CustomResultException(ResultEnum.SOURCE_LIST_SUPPLIER_EXIST);
                    }
                    sourceList.setSouCreateType("1"); //创建类型  1-手动
                    sourceList.setSouCreateBy(user.getUserId());  // 创建人
                    sourceList.setSouCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    sourceList.setSouCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    // 获取最新货源清单编码
                    String souListCode = gaiaSourceListMapper.getNewSouListCode(sourceListKey);
                    if (StringUtils.isBlank(souListCode)) {
                        sourceList.setSouListCode("00001");
                    } else {
                        sourceList.setSouListCode(souListCode);
                    }
                    gaiaSourceListMapper.insert(sourceList);
                    break;
                case "2":     //更新
                    if (StringUtils.isBlank(sourceList.getSouListCode())) {
                        throw new CustomResultException(ResultEnum.SOURCE_LIST_CODE_EMPTY);
                    }

                    GaiaSourceList souList = gaiaSourceListMapper.selectByPrimaryKey(sourceListKey);
                    if (souList == null) {
                        throw new CustomResultException(ResultEnum.SOURCE_LIST_SUPPLIER_NOT_EXIST);
                    }
                    sourceList.setSouUpdateBy(user.getUserId());
                    sourceList.setSouUpdateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    sourceList.setSouUpdateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    gaiaSourceListMapper.updateByPrimaryKeySelective(sourceList);
                    break;
                default:    // 删除
                    if (StringUtils.isBlank(sourceList.getSouListCode())) {
                        throw new CustomResultException(ResultEnum.SOURCE_LIST_CODE_EMPTY);
                    }
                    GaiaSourceList sList = gaiaSourceListMapper.selectByPrimaryKey(sourceListKey);
                    if (sList == null) {
                        throw new CustomResultException(ResultEnum.SOURCE_LIST_SUPPLIER_NOT_EXIST);
                    }

                    gaiaSourceListMapper.deleteByPrimaryKey(sourceListKey);
                    break;
            }

        }

        return ResultUtil.success();
    }

    /**
     * 货源清单查看列表
     *
     * @param dto
     * @return
     */
    @Override
    public PageInfo<SourceListResponseDto> querySourceList(SourceListRequestDto dto) {
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());

        return new PageInfo<>(gaiaSourceListMapper.querySourceList(dto));
    }

    /**
     * 货源清单明细
     *
     * @param dto
     * @return
     */
    @Override
    public SourceDetailResponseDto querySourceDetail(SourceDetailRequestDto dto) {
        // 根据 加盟商 + 地点 + 商品自编码 查询 GAIA_SOURCE_LIST
        SourceListRequestDto sourceListRequestDto = new SourceListRequestDto();
        BeanUtils.copyProperties(dto, sourceListRequestDto);
        List<SourceListResponseDto> list = gaiaSourceListMapper.querySourceList(sourceListRequestDto);
        //  参数组装
        if (list != null && list.size() > 0) {
            SourceDetailResponseDto responseDto = new SourceDetailResponseDto();
            BeanUtils.copyProperties(list.get(0), responseDto);
            List<SourceCreateListResponseDto> itemList = new ArrayList<>();
            for (SourceListResponseDto sourceListResponseDto : list) {
                SourceCreateListResponseDto sourceCreateListResponseDto = new SourceCreateListResponseDto();
                BeanUtils.copyProperties(sourceListResponseDto, sourceCreateListResponseDto);
                itemList.add(sourceCreateListResponseDto);
            }
            responseDto.setSupplierList(itemList);
            return responseDto;
        }
        return null;
    }

    /**
     * 批量新增货源清单
     *
     * @param sourceList
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result batchSaveSourceList(List<SourceRequestNewDTO> sourceList) {
        TokenUser user = feignService.getLoginInfo();
        if (CollectionUtils.isNotEmpty(sourceList)) {
            for (SourceRequestNewDTO item : sourceList) {
                // 判断字段非空
                ValidationResult result = ValidateUtil.validateEntity(item);
                if (result.isHasErrors()) {
                    throw new CustomResultException(result.getMessage());
                }
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                // 加盟商
                gaiaProductBusinessKey.setClient(item.getClient());
                // 地点
                gaiaProductBusinessKey.setProSite(item.getSouSiteCode());
                // 商品编码
                gaiaProductBusinessKey.setProSelfCode(item.getSouProCode());
                // 查询商品信息
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                if (gaiaProductBusiness == null) {
                    throw new CustomResultException("该地点下商品不存在");
                }
                GaiaSourceList source = new GaiaSourceList();
                source.setClient(item.getClient());
                // 商品编码
                source.setSouProCode(item.getSouProCode());
                // 地点
                source.setSouSiteCode(item.getSouSiteCode());
                // 供应商
                source.setSouSupplierId(item.getSouSupplierId());
                // 查询货源清单
                int count = gaiaSourceListMapper.selectCountBySupplier(source);
                // 排除重复货源清单
                if (count > 0) {
                    throw new CustomResultException(ResultEnum.SOURCE_LIST_SUPPLIER_EXIST);
                }
                //创建类型  1-手动
                source.setSouCreateType("1");
                // 创建人
                source.setSouCreateBy(user.getUserId());
                source.setSouCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                source.setSouCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                // 获取最新货源清单编码
                GaiaSourceListKey sourceListKey = new GaiaSourceListKey();
                BeanUtils.copyProperties(item, sourceListKey);
                String souListCode = gaiaSourceListMapper.getNewSouListCode(sourceListKey);
                if (StringUtils.isBlank(souListCode)) {
                    source.setSouListCode("00001");
                } else {
                    source.setSouListCode(souListCode);
                }
                source.setSouDeleteFlag(item.getSouDeleteFlag());
                source.setSouEffectFrom(item.getSouEffectFrom().replace("-",""));
                source.setSouEffectEnd(item.getSouEffectEnd().replace("-",""));
                source.setSouMainSupplier(item.getSouMainSupplier());
                source.setSouLockSupplier(item.getSouLockSupplier());
                gaiaSourceListMapper.insert(source);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 批量更新货源清单
     *
     * @param sourceList
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result batchUpdateSourceList(List<GaiaSourceList> sourceList) {
        TokenUser user = feignService.getLoginInfo();
        // 判断更新列表是否为空
        if (CollectionUtils.isNotEmpty(sourceList)) {
            for (GaiaSourceList gaiaSourceList : sourceList) {
                // 有效期处理格式
                gaiaSourceList.setSouEffectFrom(gaiaSourceList.getSouEffectFrom().replace("-", ""));
                gaiaSourceList.setSouEffectEnd(gaiaSourceList.getSouEffectEnd().replace("-", ""));
                // 加盟商
                gaiaSourceList.setClient(user.getClient());
                // 更新人
                gaiaSourceList.setSouUpdateBy(user.getUserId());
                // 更新日期
                gaiaSourceList.setSouUpdateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 更新时间
                gaiaSourceList.setSouUpdateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            }
            // 批量更新
            gaiaSourceListMapper.batchUpdateSourceList(sourceList);
        }
        return ResultUtil.success();
    }

    /**
     * 导出货源清单
     *
     * @param dto
     * @return
     */
    @Override
    public Result exportSourceList(SourceListExportDto dto) {
        try {
            List<SourceListResponseDto> lists = gaiaSourceListMapper.querySourceExportList(dto);
            // 导出数据
            List<List<String>> sourceList = new ArrayList<>();
            initData(lists, sourceList);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(basicHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(sourceList);
                    }},
                    new ArrayList<String>() {{
                        add("货源清单列表");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param sourceList
     */
    private void initData(List<SourceListResponseDto> batchList, List<List<String>> sourceList) {
        for (SourceListResponseDto sourceListResponseDto : batchList) {
            if (sourceList != null) {
                List<String> basicData = new ArrayList<>();
                //地点
                basicData.add(sourceListResponseDto.getSouSiteCode());
                //地点名称
                basicData.add(sourceListResponseDto.getSiteName());
                //商品编码
                basicData.add(sourceListResponseDto.getSouProCode());
                //商品名
                basicData.add(sourceListResponseDto.getProName());
                //供应商
                basicData.add(sourceListResponseDto.getSouSupplierId());
                //供应商名称
                basicData.add(sourceListResponseDto.getSupName());
                //生产厂家
                basicData.add(sourceListResponseDto.getProFactoryName());
                //规格
                basicData.add(sourceListResponseDto.getProSpecs());
                //是否主供
                basicData.add("1".equalsIgnoreCase(sourceListResponseDto.getSouMainSupplier()) ? "是" : "否");
                //是否锁定
                basicData.add("1".equalsIgnoreCase(sourceListResponseDto.getSouLockSupplier()) ? "是" : "否");
                //冻结
                basicData.add("1".equalsIgnoreCase(sourceListResponseDto.getSouDeleteFlag()) ? "是" : "否");
                //创建日期
                basicData.add(sourceListResponseDto.getSouCreateDate());
                //创建时间
                basicData.add(sourceListResponseDto.getSouCreateTime());
                //创建人
                basicData.add(sourceListResponseDto.getSouCreateBy());
                //创建类型
                basicData.add("1".equalsIgnoreCase(sourceListResponseDto.getSouCreateType()) ? "手动" : "自动");
                //修改日期
                basicData.add(sourceListResponseDto.getSouUpdateDate());
                //修改时间
                basicData.add(sourceListResponseDto.getSouUpdateTime());
                //修改人
                basicData.add(sourceListResponseDto.getSouUpdateBy());
                sourceList.add(basicData);
            }
        }
    }

    /**
     * 基础数据头
     */
    private String[] basicHead = {
            "地点",
            "地点名称",
            "商品编码",
            "商品名",
            "供应商",
            "供应商名称",
            "生产厂家",
            "规格",
            "是否主供",
            "是否锁定",
            "冻结",
            "创建日期",
            "创建时间",
            "创建人",
            "创建类型",
            "修改日期",
            "修改时间",
            "修改人",
    };
}
