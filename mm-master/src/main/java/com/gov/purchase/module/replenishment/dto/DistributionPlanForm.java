package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc: 新品铺货计划列表请求实体
 * @author: ryan
 * @createTime: 2021/6/4 9:49
 **/
@Data
public class DistributionPlanForm {
    /**
     * 铺货计划单号
     */
    @ApiModelProperty(value = "铺货计划单号", example = "ndp2021050001")
    private String planCode;
    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码", example = "10083")
    private String proSelfCode;
    /**
     * 铺货计划状态
     */
    @ApiModelProperty(value = "铺货计划状态:1-待调用 2-调用中 3-已完成 4-已过期", example = "1")
    private Integer status;
    /**
     * 起始日期(计划日期)
     */
    @ApiModelProperty(value = "起始日期(计划日期)", example = "2021-05-01")
    private String startDate;
    /**
     * 结束日期(计划日期)
     */
    @ApiModelProperty(value = "结束日期(计划日期)", example = "2021-05-31")
    private String endDate;
    /**
     * 加盟商
     */
    @ApiModelProperty(value = "", hidden = true)
    private String client;
}
