package com.gov.purchase.module.store.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaPriceAdjustKey;
import com.gov.purchase.module.store.dto.price.AdjustPriceRequestDto;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;

import java.util.List;

public interface StoreProductAdjustService {

    /**
     * 根据参数来查询数据
     *
     * @return
     */
    PageInfo<GaiaPriceAdjustDto> queryList(AdjustPriceRequestDto adjustPriceRequestDto);

    /**
     * 根据参数来导出数据
     *
     * @return
     */
    Result queryExportList(AdjustPriceRequestDto adjustPriceRequestDto);

    /**
     * 根据参数来查询数据
     *
     * @return
     */
    Result batchAdjust(List<AdjustPriceRequestDto> list);

    /**
     * 根据参数来查询最新的价格
     *
     * @return
     */
    PageInfo<GaiaPriceAdjustDto> queryGoodsList(AdjustPriceRequestDto adjustPriceRequestDto);

    /**
     * 根据参数来查询最新的价格导出
     *
     * @return
     */
    Result queryGoodsListExport(AdjustPriceRequestDto adjustPriceRequestDto);


    /**
     * 根据加盟商 + 门店 + 调价单 查询 调价单商品信息
     *
     * @param gaiaPriceAdjustKey
     * @return
     */
    List<GaiaPriceAdjustDto> queryPriceAdjustList(GaiaPriceAdjustKey gaiaPriceAdjustKey);
}
