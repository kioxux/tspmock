package com.gov.purchase.module.customer.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaCustomerBusiness;
import com.gov.purchase.entity.GaiaCustomerBusinessKey;
import com.gov.purchase.entity.GaiaPaymentType;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaCustomerBasicMapper;
import com.gov.purchase.mapper.GaiaCustomerBusinessMapper;
import com.gov.purchase.mapper.GaiaPaymentTypeMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.customer.dto.*;
import com.gov.purchase.module.customer.service.CustomerService;
import com.gov.purchase.utils.EnumUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;

import static com.gov.purchase.module.customer.constant.Constant.DOUBLE_REGEX;


@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {


    @Resource
    private FeignService feignService;

    @Resource
    private GaiaCustomerBasicMapper gaiaCustomerBasicMapper;

    @Resource
    private GaiaPaymentTypeMapper gaiaPaymentTypeMapper;

    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;

    /**
     * 客户维护列表
     *
     * @param dto
     * @return
     */
    @Override
    public PageInfo<CustomerListReponseDto> queryCustomerList(CustomerListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        // 设置当前登录人的加盟商编号
        dto.setClient(user.getClient());
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        return new PageInfo<>(gaiaCustomerBasicMapper.queryCustomerMatainList(dto));
    }

    /**
     * 客户维护详情
     *
     * @param dto
     * @return
     */

    @Override
    public CustomerInfoReponseDto getCustomerInfo(CustomerInfoRequestDto dto) {
        CustomerInfoReponseDto customerInfo = gaiaCustomerBasicMapper.getCustomerInfo(dto);
        // 客户分类
        if (StringUtils.isNotEmpty(customerInfo.getCusClass())) {
            // 中文翻译
            customerInfo.setCusClass(CommonEnum.CusClass.getEnumLabel(customerInfo.getCusClass()));
        }
        return customerInfo;
    }

    /**
     * 客户维护验证
     *
     * @param dto
     * @return
     */

    @Override
    public Result validCustomerInfo(CustomerInfoSaveRequestDto dto) {
        // 验证当前客户是否存在
        GaiaCustomerBusinessKey gaiaCustomerBusinessKey = new GaiaCustomerBusinessKey();
        gaiaCustomerBusinessKey.setCusSelfCode(dto.getCusSelfCode()); //客户自编码
        gaiaCustomerBusinessKey.setClient(dto.getClient()); // 加盟商
        gaiaCustomerBusinessKey.setCusSite(dto.getCusSite()); // 地点
        if (gaiaCustomerBusinessMapper.selectByPrimaryKey(gaiaCustomerBusinessKey) == null) {
            throw new CustomResultException(ResultEnum.CUS_BUSINESS_NOT_EXIST);
        }
        if (dto.getCusCreditQuota() != null) {
            if (!StringUtils.isNumeric(dto.getCusCreditQuota()) && !dto.getCusCreditQuota().matches(DOUBLE_REGEX))
                //信用额度不为数字
                throw new CustomResultException(ResultEnum.QUOTA_NOT_VALID);
        }

        //验证枚举字段
        if (StringUtils.isNotBlank(dto.getCusNoSale()) && !EnumUtils.contains(dto.getCusNoSale(), CommonEnum.CusNoSale.class)) {
            // 禁止销售输入不合法
            throw new CustomResultException("客户状态禁止采购");
        }
        if (StringUtils.isNotBlank(dto.getCusNoReturn()) && !EnumUtils.contains(dto.getCusNoReturn(), CommonEnum.CusNoReturn.class)) {
            // 禁止退厂输入不合法
            throw new CustomResultException(ResultEnum.CUSPLIER_NOT_VALID);
        }
        if (StringUtils.isNotBlank(dto.getCusCreditFlag()) && !EnumUtils.contains(dto.getCusCreditFlag(), CommonEnum.CusCreditFlag.class)) {
            // 是否启用信用管理输入不合法
            throw new CustomResultException(ResultEnum.CREDIT_FLAG_NOT_VALID);
        }
        if (StringUtils.isNotBlank(dto.getCusPayMode()) && !EnumUtils.contains(dto.getCusPayMode(), CommonEnum.CusPayMode.class)) {
            // 支付方式输入不合法
            throw new CustomResultException(ResultEnum.PAY_MODE_NOT_VALID);
        }
        if (StringUtils.isNotBlank(dto.getCusCreditCheck()) && !EnumUtils.contains(dto.getCusCreditCheck(), CommonEnum.CusCreditCheck.class)) {
            // 信用检查点输入不合法
            throw new CustomResultException(ResultEnum.CREDIT_CHECK_NOT_VALID);
        }
        //验证付款条件是否在付款条件配置表( GAIA_PAYMENT_TYPE) 存在
        if (StringUtils.isNotBlank(dto.getCusPayTerm())) {
            GaiaPaymentType type = gaiaPaymentTypeMapper.selectByPrimaryKey(dto.getCusPayTerm());
            if (null == type) {
                //付款条件不存在
                throw new CustomResultException(ResultEnum.ORD_TYPE_NOT_EXIST);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 客户维护保存
     *
     * @param dto
     * @return
     */

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveCustomerInfo(CustomerInfoSaveRequestDto dto) {
        //验证合法性
        validCustomerInfo(dto);

        // 验证当前客户是否存在
        GaiaCustomerBusinessKey gaiaCustomerBusinessKey = new GaiaCustomerBusinessKey();
        gaiaCustomerBusinessKey.setCusSelfCode(dto.getCusSelfCode()); //客户自编码
        gaiaCustomerBusinessKey.setClient(dto.getClient()); // 加盟商
        gaiaCustomerBusinessKey.setCusSite(dto.getCusSite()); // 地点
        GaiaCustomerBusiness gaiaCustomerBusiness = gaiaCustomerBusinessMapper.selectByPrimaryKey(gaiaCustomerBusinessKey);
        if (gaiaCustomerBusiness == null) {
            throw new CustomResultException(ResultEnum.CUS_BUSINESS_NOT_EXIST);
        }
        //更新表
        gaiaCustomerBusiness.setCusBankCode(dto.getCusBankCode());
        gaiaCustomerBusiness.setCusBankName(dto.getCusBankName());
        gaiaCustomerBusiness.setCusBankAccount(dto.getCusBankAccount());
        gaiaCustomerBusiness.setCusAccountPerson(dto.getCusAccountPerson());//账户持有人
        gaiaCustomerBusiness.setCusPayTerm(dto.getCusPayTerm());//付款条件
        gaiaCustomerBusiness.setCusPayMode(dto.getCusPayMode());//付款方式
        gaiaCustomerBusiness.setCusCreditAmt(dto.getCusCreditAmt());//铺地授信额度
        gaiaCustomerBusiness.setCusNoSale(dto.getCusNoSale());//禁止销售
        gaiaCustomerBusiness.setCusNoReturn(dto.getCusNoReturn());//禁止退货
        gaiaCustomerBusiness.setCusBussinessContact(dto.getCusBussinessContact());//业务联系人
        gaiaCustomerBusiness.setCusContactTel(dto.getCusContactTel());//联系人电话
        gaiaCustomerBusiness.setCusCreditFlag(dto.getCusCreditFlag());//是否启用信用管理
        if (dto.getCusCreditQuota() != null && !dto.getCusCreditQuota().trim().equals("")) {
            gaiaCustomerBusiness.setCusCreditQuota(new BigDecimal(dto.getCusCreditQuota()));//信用额度
        }
        gaiaCustomerBusiness.setCusCreditCheck(dto.getCusCreditCheck());//信用检查点(1-销售订单，2-发货过账，3-开票)
        gaiaCustomerBusinessMapper.updateByPrimaryKeySelective(gaiaCustomerBusiness);
        return ResultUtil.success();
    }
}
