package com.gov.purchase.module.store.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@ApiModel(value = "DC更新状态传入参数")
public class DcDataStatusRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client")
    @NotBlank(message = "加盟商不能为空")
    @Size(max = 8, message = "加盟商长度不能大于8位")
    private String client;

    @ApiModelProperty(value = "DC编码", name = "dcCode")
    @NotBlank(message = "DC编码不能为空")
    @Size(max = 4, message = "DC编号长度不能大于4位")
    private String dcCode;

    @ApiModelProperty(value = "DC状态", name = "dcStatus")
    @NotBlank(message = "DC状态不能为空")
    private String dcStatus;



}
