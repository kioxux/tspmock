package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaCompadmMapper;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaInvoicePayingBasicMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.businessImport.InvioceInfo;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.purchase.dto.InvoiceDTO;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.05
 */
@Service
public class InvioceInfoImport extends BusinessImport {
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaCompadmMapper gaiaCompadmMapper;
    @Resource
    private GaiaInvoicePayingBasicMapper gaiaInvoicePayingBasicMapper;

    /**
     * 验证
     *
     * @param map   数据
     * @param field 字段
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 验证数据为空
        if (map == null || map.isEmpty()) {
            return ResultUtil.success();
        }
        TokenUser user = feignService.getLoginInfo();
        // 错误 消息
        List<String> errorList = new ArrayList<>();
        Map<String, List<Dictionary>> taxMap = new HashMap<>();
        // 遍历验证
        for (Integer key : map.keySet()) {
            // 增值税专用发票 开票信息验证
            InvioceInfo invioceInfo = (InvioceInfo) map.get(key);
            // 专用票 其他信息必填
            if (CommonEnum.InvioceType.SPECIAL.getCode().equals(invioceInfo.getInvioceType())) {
                // 开户行名称
                if (StringUtils.isBlank(invioceInfo.getBankName())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "开户行名称"));
                }
                // 开户行账号
                if (StringUtils.isBlank(invioceInfo.getBankNumber())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "开户行账号"));
                }
                // 地址
                if (StringUtils.isBlank(invioceInfo.getCompanyAddress())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "地址"));
                }
                // 电话号码
                if (StringUtils.isBlank(invioceInfo.getCompanyPhone())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "电话号码"));
                }
            }
            // 纳税主体判断
            if (StringUtils.isNotBlank(invioceInfo.getTaxCompanyCode()) && !invioceInfo.getCompanyCode().equals(invioceInfo.getTaxCompanyCode())) {
                map.forEach((k, v) -> {
                    if (k.intValue() != key.intValue()) {
                        InvioceInfo entity = (InvioceInfo) v;
                        if (entity.getCompanyCode().equals(invioceInfo.getTaxCompanyCode()) &&
                                StringUtils.isNotBlank(entity.getTaxCompanyCode()) &&
                                !entity.getCompanyCode().equals(entity.getTaxCompanyCode())) {
                            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "纳税主体编码"));
                            return;
                        }
                    }
                });
            }
            // 门店、DC、连锁编码有效验证
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(user.getClient());
            gaiaStoreDataKey.setStoCode(invioceInfo.getCompanyCode());
            GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(user.getClient());
            gaiaDcDataKey.setDcCode(invioceInfo.getCompanyCode());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            GaiaCompadmKey gaiaCompadmKey = new GaiaCompadmKey();
            gaiaCompadmKey.setClient(user.getClient());
            gaiaCompadmKey.setCompadmId(invioceInfo.getCompanyCode());
            GaiaCompadm gaiaCompadm = gaiaCompadmMapper.selectByPrimaryKey(gaiaCompadmKey);
            if (gaiaStoreData == null && gaiaDcData == null && gaiaCompadm == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "付款主体编码"));
            }
            // 纳税主体未填则为自己
            if (StringUtils.isBlank(invioceInfo.getTaxCompanyCode())) {
                invioceInfo.setTaxCompanyCode(invioceInfo.getCompanyCode());
            }
            // 门店纳税主体验证
            if (gaiaStoreData != null) {
                if (StringUtils.isBlank(invioceInfo.getTaxCompanyCode())) {
                    invioceInfo.setTaxCompanyCode(gaiaStoreData.getStoName());
                    invioceInfo.setTaxCompanyName(gaiaStoreData.getStoName());
                }
                // 非连锁门店 纳税主体为自己
                if (!CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(gaiaStoreData.getStoAttribute())) {
                    invioceInfo.setTaxCompanyCode(invioceInfo.getCompanyCode());
                    invioceInfo.setTaxCompanyName(gaiaStoreData.getStoName());
                } else {
                    if (StringUtils.isNotBlank(gaiaStoreData.getStoChainHead())) {
                        // 连锁门店
                        List<Dictionary> chainList = taxMap.get(gaiaStoreData.getStoChainHead());
                        if (chainList == null) {
                            chainList = gaiaStoreDataMapper.getTaxSubject(user.getClient(), gaiaStoreData.getStoChainHead());
                            taxMap.put(gaiaStoreData.getStoChainHead(), chainList);
                        }
                        List<Dictionary> list = chainList.stream().filter(item -> item.getValue().equals(invioceInfo.getTaxCompanyCode())).collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(list)) {
                            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "纳税主体编码"));
                        } else {
                            invioceInfo.setTaxCompanyName(list.get(0).getLabel());
                        }
                    }
                }
            }
            // DC 纳税主体验证
            if (gaiaDcData != null) {
                if (StringUtils.isBlank(invioceInfo.getTaxCompanyCode())) {
                    invioceInfo.setTaxCompanyCode(gaiaDcData.getDcCode());
                    invioceInfo.setTaxCompanyName(gaiaDcData.getDcName());
                }
                // 批发公司 纳税主体为自己
                if ("30".equals(gaiaDcData.getDcType())) {
                    invioceInfo.setTaxCompanyCode(invioceInfo.getCompanyCode());
                    invioceInfo.setTaxCompanyName(gaiaDcData.getDcName());
                } else { // 连锁配送中心
                    if (StringUtils.isNotBlank(gaiaDcData.getDcChainHead())) {
                        // 连锁门店
                        List<Dictionary> chainList = taxMap.get(gaiaDcData.getDcChainHead());
                        if (chainList == null) {
                            chainList = gaiaStoreDataMapper.getTaxSubject(user.getClient(), gaiaDcData.getDcChainHead());
                            taxMap.put(gaiaDcData.getDcChainHead(), chainList);
                        }
                        List<Dictionary> list = chainList.stream().filter(item -> item.getValue().equals(invioceInfo.getTaxCompanyCode())).collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(list)) {
                            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "纳税主体编码"));
                        } else {
                            invioceInfo.setTaxCompanyName(list.get(0).getLabel());
                        }
                    }
                }
            }
            // 连锁 纳税主体为自己
            if (gaiaCompadm != null) {
                invioceInfo.setTaxCompanyCode(gaiaCompadm.getCompadmNo());
                invioceInfo.setTaxCompanyName(gaiaCompadm.getCompadmName());
            }
        }
        // 验证报错
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        List<InvoiceDTO> list = new ArrayList();
        // 更新数据库
        for (Integer key : map.keySet()) {
            InvioceInfo invioceInfo = (InvioceInfo) map.get(key);
            InvoiceDTO entity = new InvoiceDTO();
            entity.setClient(user.getClient());
            entity.setFicoCompanyCode(invioceInfo.getCompanyCode());
            entity.setFicoTaxSubjectCode(invioceInfo.getTaxCompanyCode());
            entity.setFicoTaxSubjectName(invioceInfo.getTaxCompanyName());
            Dictionary invioce_type = CommonEnum.DictionaryStaticData.getDictionaryByLabel(CommonEnum.DictionaryStaticData.INVIOCE_TYPE, invioceInfo.getInvioceType());
            entity.setFicoInvoiceType(invioce_type.getValue());
            entity.setFicoSocialCreditCode(invioceInfo.getSocialCreditCode());
            entity.setFicoBankName(invioceInfo.getBankName());
            entity.setFicoBankNumber(invioceInfo.getBankNumber());
            entity.setFicoCompanyAddress(invioceInfo.getCompanyAddress());
            entity.setFicoCompanyPhoneNumber(invioceInfo.getCompanyPhone());
            entity.setFicoPayingbankName(invioceInfo.getPayingbankName());
            entity.setFicoPayingbankNumber(invioceInfo.getPayingbankNumber());
            Dictionary fico_tax_attributes = CommonEnum.DictionaryStaticData.getDictionaryByLabel(CommonEnum.DictionaryStaticData.FICO_TAX_ATTRIBUTES, invioceInfo.getFicoTaxAttributes());
            if (StringUtils.isBlank(invioceInfo.getFicoTaxAttributes())) {
                entity.setFicoTaxAttributes(null);
            } else {
                entity.setFicoTaxAttributes(NumberUtils.toInt(fico_tax_attributes.getValue()));
            }
            list.add(entity);
        }
        gaiaInvoicePayingBasicMapper.insertBatch(list);
        return ResultUtil.success();
    }
}
