package com.gov.purchase.module.purchase.controller;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.PayPurchaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "采购请款")
@RestController
@RequestMapping("purchase")
public class PayPurchaseController {

    @Resource
    private PayPurchaseService payPurchaseService;

    /**
     * 发票登记查询
     *
     * @param dto InvoiceRegisterListRequestDto
     * @return PageInfo<InvoiceRegisterListResponseDto>
     */
    @Log("发票登记查询")
    @ApiOperation("发票登记查询")
    @PostMapping("getInvoiceRegisterList")
    public Result resultInvoiceRegisterList(@Valid @RequestBody InvoiceRegisterListRequestDto dto) {

        //发票登记查询
        PageInfo<InvoiceRegisterListResponseDto> supplierList = payPurchaseService.selectInvoiceRegisterList(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 发票列表查询
     *
     * @param dto InvoiceListRequestDto
     * @return PageInfo<InvoiceListResponseDto>
     */
    @Log("发票列表查询")
    @ApiOperation("发票列表查询")
    @PostMapping("getInvoiceList")
    public Result resultInvoiceList(@Valid @RequestBody InvoiceListRequestDto dto) {

        //发票列表查询
        PageInfo<InvoiceListResponseDto> supplierList = payPurchaseService.selectInvoiceList(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 发票明细查询
     *
     * @param dto InvoiceListRequestDto
     * @return InvoiceDetailsResponseDto
     */
    @Log("发票明细查询")
    @ApiOperation("发票明细查询")
    @PostMapping("getInvoiceDetails")
    public Result resultInvoiceDetails(@Valid @RequestBody InvoiceDetailsRequestDto dto) {

        //发票列表查询
        InvoiceDetailsResponseDto invoiceDetails = payPurchaseService.selectInvoiceDetails(dto);
        return ResultUtil.success(invoiceDetails);
    }

    /**
     * 付款申请保存
     *
     * @param dto InvoiceListRequestDto
     * @return String
     */
    @Log("付款申请保存")
    @ApiOperation("付款申请保存")
    @PostMapping("paymentReqSave")
    public Result resultPaymentReqSave(@Valid @RequestBody List<PaymentReqSaveRequestDto> dto) {

        //付款申请保存
        String paymentReqSave = payPurchaseService.insertPaymentReqSave(dto);
        return ResultUtil.success(paymentReqSave);
    }

    /**
     * 付款单查询
     *
     * @param dto InvoiceListRequestDto
     * @return PageInfo<InvoiceListResponseDto>
     */
    @Log("付款单查询")
    @ApiOperation("付款单查询")
    @PostMapping("queryPayOrderList")
    public Result resultPayOrderList(@Valid @RequestBody PayOrderListRequestDto dto) {

        //发票列表查询
        PageInfo<PayOrderListResponseDto> supplierList = payPurchaseService.selectPayOrderList(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 付款单明细
     *
     * @param dto PayOrderDetailsRequestDto
     * @return List<PayOrderDetailsResponseDto>
     */
    @Log("付款单明细")
    @ApiOperation("付款单明细")
    @PostMapping("queryPayOrderDetails")
    public Result resultPayOrderDetails(@Valid @RequestBody PayOrderDetailsRequestDto dto) {

        //付款单明细
        List<PayOrderDetailsResponseDto> invoiceDetails = payPurchaseService.selectPayOrderDetails(dto);
        return ResultUtil.success(invoiceDetails);
    }

    /**
     * 发送审批
     * @param payOrderId 付款单号
     * @return
     */
    @Log("发送审批")
    @ApiOperation("发送审批")
    @PostMapping("sendApproval")
    public Result sendApproval(@RequestJson("payOrderId") String payOrderId) {
        return payPurchaseService.sendApproval(payOrderId);
    }
}
