package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

@Data
public class GroupPriceProduct {
    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proSelfCode;

    /**
     * 加点比例
     */
    @ExcelValidate(index = 1, name = "加点比例", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = false)
    private String gapgAddRate;

    /**
     * 加点金额
     */
    @ExcelValidate(index = 2, name = "加点金额", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = false)
    private String gapgAddAmt;

    /**
     * 目录价
     */
    @ExcelValidate(index = 3, name = "目录价", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = false)
    private String gapgCataloguePrice;

}
