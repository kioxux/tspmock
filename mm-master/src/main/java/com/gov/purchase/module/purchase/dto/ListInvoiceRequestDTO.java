package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("付款及发票查询接口传入参数")
public class ListInvoiceRequestDTO {

    private String ficoCompanyCode;

    /**
     * 门店编码
     */
    private String ficoStoreCode;

    /**
     * 门店名称
     */
    private String ficoStoreName;

    /**
     * 纳税主体编码
     */
    private String ficoTaxSubjectCode;

    /**
     * 纳税主体名称
     */
    private String ficoTaxSubjectName;

    /**
     * 纳税属性 1：一般纳税人，2：小规模，3：个体工商户
     */
    private Integer ficoTaxAttributes;

    /**
     * 配送中心类型
     */
    private String type;

    /**
     * 单体门店类型
     */
    private String[] typeList;

    private String client;

    private String chainHead;
}
