package com.gov.purchase.module.base.service;

import com.gov.purchase.common.response.Result;

public interface AreaService {

    /**
     * 获取省市区集合
     * @return
     */
    Result getAreaList();
}
