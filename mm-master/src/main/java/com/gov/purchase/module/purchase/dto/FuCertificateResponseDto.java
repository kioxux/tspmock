package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@Data
@ApiModel(value = "后续凭证返回参数")
public class FuCertificateResponseDto {

    /**
     * 物料凭证类型
     */
    private String matType;

    /**
     * 物料凭证类型名称
     */
    private String matTypeName;

    /**
     * 物料凭证号
     */
    private String matId;

    /**
     * 物料凭证行号
     */
    private String matLineNo;

    /**
     * 过账日期
     */
    private String matPostDate;

    /**
     * 基本计量单位
     */
    private String matUint;

    /**
     * 基本计量单位名称
     */
    private String matUintName;

    /**
     * 总金额（批次）
     */
    private BigDecimal matBatAmt;

    /**
     * 业务单号
     */
    private String matDnId;

    /**
     * 物料凭证数量
     */
    private BigDecimal matQty;
}