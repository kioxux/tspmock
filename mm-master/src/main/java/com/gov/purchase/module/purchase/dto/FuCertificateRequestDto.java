package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "后续凭证传入参数")
public class FuCertificateRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 采购订单号
     */
    @ApiModelProperty(value = "订单号", name = "matPoId", required = true)
    @NotBlank(message = "订单号不能为空")
    private String matPoId;

    /**
     * 采购订单行号
     */
    @ApiModelProperty(value = "订单行号", name = "matPoLineno", required = true)
    @NotBlank(message = "订单行号不能为空")
    private String matPoLineno;
}