package com.gov.purchase.module.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel(value = "订单明细参数")
public class GaiaPoItemList {

    /**
     * 订单行号
     */
    private String poLineNo;

    /**
     * 商品自编码
     */
    @ApiModelProperty(value = "商品自编码", name = "poProCode", required = true)
    @NotNull(message = "商品自编码不能为空")
    private String poProCode;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位", name = "poUnit", required = true)
    @NotNull(message = "单位不能为空")
    private String poUnit;

    /**
     * 单价
     */
    @ApiModelProperty(value = "单价", name = "poPrice", required = true)
    @NotNull(message = "单价不能为空")
    private BigDecimal poPrice;

    /**
     * 税率
     */
    @ApiModelProperty(value = "税率", name = "poRate", required = true)
    @NotNull(message = "税率不能为空")
    private String poRate;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量", name = "poQty", required = true)
    @NotNull(message = "数量不能为空")
    private BigDecimal poQty;

    /**
     * 行总价
     */
    @ApiModelProperty(value = "行总价", name = "poLineAmt", required = true)
    @NotNull(message = "行总价不能为空")
    private BigDecimal poLineAmt;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "poSiteCode", required = true)
    @NotNull(message = "地点不能为空")
    private String poSiteCode;

    /**
     * 库位
     */
    private String poLocationCode;

   // @NotBlank(message = "存储条件不为空")
    private String proStorageCondition;
    /**
     * 生产批号
     */
    private String poBatchNo;

    /**
     * 生产日期
     */
    private String poScrq;

    /**
     * 有效期
     */
    private String poYxq;

    /**
     * 批次
     */
    private String poBatch;

    /**
     * 计划交货日期
     */
    private String poDeliveryDate;

    /**
     * 行备注
     */
    private String poLineRemark;

    /**
     * 删除标记
     */
    private String poLineDelete;

    /**
     * 交货已完成标记
     */
    private String poCompleteFlag;

    /**
     * 发票价
     */
    private BigDecimal poFpj;
}
