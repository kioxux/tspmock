package com.gov.purchase.module.supplier.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "供应商维护入参数")
public class QueryUnDrugUpdateRequestDto {

    /**
     * 供应商自编码
     */
    @ApiModelProperty(value = "供应商自编码", name = "supSelfCode", required = true)
    @NotBlank(message = "供应商自编码不能为空")
    private String supSelfCode;

    /**
     * 供应商名称
     */
    @ApiModelProperty(value = "供应商名称", name = "supName", required = true)
    @NotBlank(message = "供应商名称不能为空")
    private String supName;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型", name = "type", required = true)
    @NotBlank(message = "类型不能为空")
    private String type;

    /**
     * 助记码
     */
    @ApiModelProperty(value = "助记码", name = "supPym", required = true)
    // @NotBlank(message = "助记码不能为空")
    private String supPym;

    /**
     * 统一社会信用代码
     */
    @ApiModelProperty(value = "统一社会信用代码", name = "supCreditCode", required = true)
    // @NotBlank(message = "统一社会信用代码不能为空")
    private String supCreditCode;

    /**
     * 统一社会信用代码
     */
    @ApiModelProperty(value = "营业期限", name = "supCreditDate", required = true)
    // @NotBlank(message = "营业期限不能为空")
    private String supCreditDate;

    /**
     * 供应商分类
     */
    @ApiModelProperty(value = "供应商分类", name = "supClass", required = true)
    // @NotBlank(message = "供应商分类不能为空")
    private String supClass;

    /**
     * 法人
     */
    @ApiModelProperty(value = "法人", name = "supLegalPerson", required = true)
    // @NotBlank(message = "法人不能为空")
    private String supLegalPerson;

    /**
     * 注册地址
     */
    @ApiModelProperty(value = "注册地址", name = "supRegAdd", required = true)
    // @NotBlank(message = "注册地址不能为空")
    private String supRegAdd;

    /**
     * 许可证编号
     */
    private String supLicenceNo;

    /**
     * 发证日期
     */
    private String supLicenceDate;

    /**
     * 有效期至
     */
    private String supLicenceValid;

    /**
     * 生产或经营范围
     */
    private String supScope;


    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 供应商编码
     */
    @ApiModelProperty(value = "供应商编码", name = "supCode", required = true)
    private String supCode;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "supSite", required = true)
    @NotBlank(message = "地点不能为空")
    private String supSite;

    /**
     * 禁止采购
     */
    @ApiModelProperty(value = "禁止采购", name = "supNoPurchase")
    private String supNoPurchase;

    /**
     * 禁止退厂
     */
    @ApiModelProperty(value = "禁止退厂", name = "supNoSupplier")
    private String supNoSupplier;

    /**
     * 采购付款条件
     */
    @ApiModelProperty(value = "采购付款条件", name = "supPayTerm")
    private String supPayTerm;

    /**
     * 业务联系人
     */
    @ApiModelProperty(value = "业务联系人", name = "supBussinessContact")
    private String supBussinessContact;

    /**
     * 联系人电话
     */
    @ApiModelProperty(value = "联系人电话", name = "supContactTel")
    private String supContactTel;

    /**
     * 送货前置期
     */
    @ApiModelProperty(value = "送货前置期", name = "supLeadTime")
    private String supLeadTime;

    /**
     * 银行代码
     */
    @ApiModelProperty(value = "银行代码", name = "supBankCode")
    private String supBankCode;

    /**
     * 银行名称
     */
    @ApiModelProperty(value = "银行名称", name = "supBankName")
    private String supBankName;

    /**
     * 账户持有人
     */
    @ApiModelProperty(value = "账户持有人", name = "supAccountPerson")
    private String supAccountPerson;

    /**
     * 银行账号
     */
    @ApiModelProperty(value = "银行账号", name = "supBankAccount")
    private String supBankAccount;

    /**
     * 支付方式
     */
    @ApiModelProperty(value = "支付方式", name = "supPayMode")
    private String supPayMode;

    /**
     * 铺底授信额度
     */
    @ApiModelProperty(value = "铺底授信额度", name = "supCreditAmt")
    private String supCreditAmt;

    /**
     * 供应商状态
     */
    @ApiModelProperty(value = "供应商状态", name = "supStatus", required = true)
    @NotBlank(message = "供应商状态不能为空")
    private String supStatus;

}
