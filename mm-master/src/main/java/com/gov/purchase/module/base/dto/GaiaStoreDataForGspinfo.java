package com.gov.purchase.module.base.dto;

import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.07.08
 */
@Data
public class GaiaStoreDataForGspinfo extends GaiaStoreData {
    /**
     * 1:门店，2:DC
     */
    private Integer type;

}
