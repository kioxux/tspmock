package com.gov.purchase.module.base.service;

import com.gov.purchase.entity.GaiaJyfwDataGspinfoKey;
import com.gov.purchase.module.base.dto.JyfwDTO;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.base.dto.businessScope.GetScopeListDTO;
import com.gov.purchase.module.goods.dto.JyfwooData;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BusinessScopeService {

    /**
     * 经营范围列表
     *
     * @param jyfwOrgid
     * @param jyfwOrgname
     * @param jyfwSite
     * @param jyfwType
     * @return
     */
    List<GetScopeListDTO> getScopeList(String jyfwOrgid, String jyfwOrgname, String jyfwSite, Integer jyfwType, String jyfwClass);

    /**
     * 经营范围编辑保存
     *
     * @param vo
     */
    void editScope(EditScopeVO vo);

    /**
     * 经营范围编辑保存 审批回调
     */
    void editScopeApproval(EditScopeVO vo, String client, String userId);


    /**
     * 特殊经营范围配置表保存
     *
     * @param client      加盟商
     * @param userId      用户id
     * @param jyfwOrgid   机构
     * @param jyfwOrgname 机构名
     * @param jyfwSite    地点
     * @param jyfwType    类型
     * @param jyfwList    范围列表
     */
    void saveJyfwData(String client, String userId,
                      String jyfwOrgid, String jyfwOrgname, String jyfwSite, Integer jyfwType,
                      List<JyfwDTO> jyfwList);

    /**
     * 首营回调保存
     *
     * @param client         加盟商
     * @param userId         用户id
     * @param jyfwOrgid      机构
     * @param jyfwOrgname    机构名
     * @param jyfwSite       地点
     * @param jyfwType       类型
     * @param jyfwList       范围列表
     * @param jyfwooDataList 所有特殊经营范围 k-v,
     */
    @Transactional(rollbackFor = Exception.class)
    void saveJyfwDataFromApproval(String client, String userId,
                                  String jyfwOrgid, String jyfwOrgname, String jyfwSite, Integer jyfwType,
                                  List<GaiaJyfwDataGspinfoKey> jyfwList, List<JyfwooData> jyfwooDataList);

    /**
     * 特殊经营范围首营表保存
     *
     * @param client     加盟商
     * @param jyfwOrgid  机构
     * @param jyfwFlowNo 首营流程编号
     * @param jyfwList   范围列表
     */
    void saveJyfwGspinfoData(String client, String jyfwOrgid, String jyfwFlowNo, List<JyfwDTO> jyfwList);

    /**
     * 经营范围列表全部
     *
     * @return
     */
    List<JyfwooData> getAllScopeList(String jyfwClass);

}
