package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductBusiness;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetProDetailsResponseDTO extends GaiaProductBusiness {


    /**
     * 外包装图片url
     */
    private String proPictureWbzUrl;

    /**
     * 说明书图片url
     */
    private String proPictureSmsUrl;

    /**
     * 批件图片url
     */
    private String proPicturePjUrl;

    /**
     * 内包装图片url
     */
    private String proPictureNbzUrl;

    /**
     * 生产企业证照图片url
     */
    private String proPictureZzUrl;

    /**
     * 其他图片url
     */
    private String proPictureQtUrl;
}
