package com.gov.purchase.module.store.dto.price;

import com.gov.purchase.module.store.dto.RetailStore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "调价单审批列表传入参数")
public class PriceApprovalListReponseDto {

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "门店编码", name = "prcStore")
    private String prcStore;

    @ApiModelProperty(value = "门店名称", name = "prcStoreName")
    private String prcStoreName;

    @ApiModelProperty(value = "商品编码", name = "prcProduct")
    private String prcProduct;

    @ApiModelProperty(value = "商品名称", name = "proName")
    private String proName;

    @ApiModelProperty(value = "商品描述", name = "proDepict")
    private String proDepict;

    @ApiModelProperty(value = "商品规格", name = "proSpecs")
    private String proSpecs;

    @ApiModelProperty(value = "价格分类", name = "prcClass")
    private String prcClass;

    @ApiModelProperty(value = "调价单号", name = "prcModfiyNo")
    private String prcModfiyNo;

    @ApiModelProperty(value = "修改前价格", name = "prcAmountBefore")
    private BigDecimal prcAmountBefore;

    @ApiModelProperty(value = "价格", name = "prcAmount")
    private BigDecimal prcAmount;

    @ApiModelProperty(value = "有效期起", name = "prcEffectDate")
    private String prcEffectDate;

    @ApiModelProperty(value = "单位", name = "prcUnit")
    private String prcUnit;

    @ApiModelProperty(value = "单位名称", name = "unitName")
    private String unitName;

    @ApiModelProperty(value = "不允许积分", name = "prcNoIntegral")
    private String prcNoIntegral;

    @ApiModelProperty(value = "不允许积分兑换", name = "prcNoDiscount")
    private String prcNoDiscount;

    @ApiModelProperty(value = "不允许会员卡打折", name = "prcNoExchange")
    private String prcNoExchange;

    @ApiModelProperty(value = "限购数量", name = "prcLimitAmount")
    private String prcLimitAmount;

    @ApiModelProperty(value = "申请日期", name = "prcCreateDate")
    private String prcCreateDate;

    @ApiModelProperty(value = "调价来源", name = "prcSource")
    private String prcSource;

    @ApiModelProperty(value = "总部调价", name = "prcHeadPrice")
    private String prcHeadPrice;

    /**
     * 门店集合
     */
    private List<RetailStore> prcStoreList;

    /**
     * 调价原因
     */
    @ApiModelProperty(value = "调价原因", name = "prcReason")
    private String prcReason;

    /**
     * 发起门店
     */
    @ApiModelProperty(value = "发起门店", name = "prcInitStore")
    private String prcInitStore;

    /**
     * 发起门店名
     */
    @ApiModelProperty(value = "发起门店名", name = "prcInitStoreName")
    private String prcInitStoreName;

    @ApiModelProperty(value = "生产厂家", name = "prcInitStoreName")
    private String proFactoryName;

    /**
     * 末次进价字段
     */
    private BigDecimal batPoPrice;

    /**
     * 销售级别
     */
    private String prcSlaeClass;
    /**
     * 自定义字段1
     */
    private String prcZdy1;
    /**
     * 自定义字段2
     */
    private String prcZdy2;
    /**
     * 自定义字段3
     */
    private String prcZdy3;
    /**
     * 自定义字段4
     */
    private String prcZdy4;
    /**
     * 自定义字段5
     */
    private String prcZdy5;
}
