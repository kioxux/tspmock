package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaChajiaM;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 采购入库差价单功能
 * @author: yzf
 * @create: 2021-12-13 10:21
 */
@Data
public class PurchaseReceiptPriceDetailVO extends GaiaChajiaM {

    /**
     * 商品名称
     */
    private String proDepict;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 厂家
     */
    private String proFactoryName;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 库存数量
     */
    private BigDecimal proQty;

    /**
     * 税率
     */
    private String proInputTax;

    /**
     * 税率名称
     */
    private String taxCodeName;
    /**
     * 税率类型
     */
    private String taxCodeClass;
    /**
     * 税率值
     */
    private String taxCodeValue;
    /**
     * 税码状态
     */
    private String taxCodeStatus;


}
