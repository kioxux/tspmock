package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class InvoiceDTO {

    @NotBlank(message = "加盟商不能为空")
    private String client;

    @NotBlank(message = "门店编码不能为空")
    private String ficoStoreCode;

    @NotBlank(message = "组织不能为空")
    private String ficoCompanyCode;

    @NotBlank(message = "门店名称不能为空")
    private String ficoStoreName;

    @NotBlank(message = "纳税主体编码不能为空")
    private String ficoTaxSubjectCode;

    @NotBlank(message = "纳税主体名称不能为空")
    private String ficoTaxSubjectName;

    @NotBlank(message = "发票类型不能为空")
    private String ficoInvoiceType;

    @NotBlank(message = "统一社会信用代码不能为空")
    private String ficoSocialCreditCode;

    @NotBlank(message = "开户行名称不能为空")
    private String ficoBankName;

    @NotBlank(message = "开户行账号不能为空")
    private String ficoBankNumber;

    @NotBlank(message = "地址不能为空")
    private String ficoCompanyAddress;

    @NotBlank(message = "电话号码不能为空")
    private String ficoCompanyPhoneNumber;

    @NotBlank(message = "付款银行名称不能为空")
    private String ficoPayingbankName;

    @NotBlank(message = "付款银行账号不能为空")
    private String ficoPayingbankNumber;
    /**
     * 纳税属性
     */
    private Integer ficoTaxAttributes;

    /**
     * 纳税主体  code:name
     */
    private String ficoTax;

    /**
     * 纳税主体
     */
    private List<InvoicePayingTaxDTO> taxDetails;


}
