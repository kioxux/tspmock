package com.gov.purchase.module.wholesale.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/3/17 10:01
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class GoodsOrderDTO {
    /**
     * 加盟商
     */
    private String client;
    /**
     * 门店
     */
    private String gsrhBrId;
    /**
     * 门店名称
     */
    private String stoName;
    /**
     * 门店简称
     */
    private String stoShortName;
    /**
     * 单号
     */
    private String gsrhVoucherId;
    /**
     * 开单日期
     */
    private String gsrhDate;
    /**
     * 补货方式
     */
    private String gsrhPattern;
    /**
     * 品项数
     */
    private String gsrdProCount;
    /**
     * 合计数量
     */
    private BigDecimal gsrhTotalQty;

    /**
     * 合计金额
     */
    private BigDecimal gsrhTotalAmt;

    /**
     * 明细数据
     */
    private List<RecallInfo> recallInfoList;

    /**
     * 地点编码
     */
    private String dcCode;
    /**
     * 地点名称
     */
    private String dcName;
    /**
     * 客户编码
     */
    private String compadmId;
    /**
     * 客户名称
     */
    private String compadmName;

}
