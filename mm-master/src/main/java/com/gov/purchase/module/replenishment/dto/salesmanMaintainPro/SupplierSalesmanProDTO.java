package com.gov.purchase.module.replenishment.dto.salesmanMaintainPro;


import com.gov.purchase.common.excel.annotation.ExcelField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("授权商品DTO")
@Data
public class SupplierSalesmanProDTO {

    @ExcelField(title = "商品自编码", type = 1, sort = 0)
    @ApiModelProperty("商品自编码")
    private String proCode;

    @ExcelField(title = "商品名称", type = 1, sort = 1)
    @ApiModelProperty("商品名称")
    private String proName;

    @ExcelField(title = "规格", type = 1, sort = 2)
    @ApiModelProperty("规格")
    private String proSpecs;

    @ExcelField(title = "厂家", type = 1, sort = 3)
    @ApiModelProperty("厂家")
    private String proFactoryName;

    @ExcelField(title = "产地", type = 1, sort = 4)
    @ApiModelProperty("产地")
    private String proPlace;

    @ExcelField(title = "单位", type = 1, sort = 5)
    @ApiModelProperty("单位")
    private String proUnit;
}
