package com.gov.purchase.module.purchase.dto;

import lombok.Data;

/**
 * @Author staxc
 * @Date 2020/10/29 10:09
 * @desc
 */
@Data
public class InvoicePayingTaxDTO {
    /**
     * 纳税主体编码
     */
    private String ficoTaxSubjectCode;

    /**
     * 纳税主体名称
     */
    private String ficoTaxSubjectName;

    /**
     * 纳税主体
     */
    private String ficoTaxLayper;
}
