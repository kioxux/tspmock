package com.gov.purchase.module.base.dto.feign;

import lombok.Data;

@Data
public class SupplierGspFeignDto {

    private String supCode;
    private String supName;
    private String supCreditCode;
    private String supCreditDate;
    private String supLegalPerson;
    private String supRegAdd;
    private String supLicenceNo;
    private String supLicenceOrgan;
    private String supLicenceDate;
    private String supLicenceValid;
    private String supPostalCode;
    private String supTaxCard;
    private String supOrgCard;
    private String supAccountName;
    private String supAccountBank;
    private String supBankAccount;
    private String supDocState;
    private String supSealState;
    private String supScope;
    private String supOperationMode;
    private String wfSite;
    private String supSelfCode;
}
