package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

/**
 * @description: 药品采购记录参数接收类
 * @author: yzf
 * @create: 2021-11-04 11:17
 */
@Data
public class DrugPurchaseDTO {

    private String client;

    /**
     * 仓库编码
     */
    private String dcCode;

    /**
     * 供应商编码
     */
    private String poSupplierId;

    /**
     * 商品编码
     */
    private String proSelfCodes;

    /**
     * 采购日期
     */
    private String purchaseDate;
    private String startDate;
    private String endDate;

    /**
     * 采购人员
     */
    private String purchasePer;

    private Integer pageSize;

    private Integer pageNum;

    /**
     * 地点类型
     */
    private String type;
}
