package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.common.entity.Pageable;
import com.gov.purchase.entity.GaiaStoreData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "供应商首营列表传入参数")
public class QueryGspinfoListRequestDto extends Pageable {

    @ApiModelProperty(value = "供应商自编码", name = "supSelfCode")
    private String supSelfCode;

    @ApiModelProperty(value = "供应商名称", name = "supName")
    private String supName;

    @ApiModelProperty(value = "统一社会信用代码", name = "supCreditCode")
    private String supCreditCode;

    @ApiModelProperty(value = "首营日期 （yyyyMMdd）", name = "supGspDate")
    private String supGspDate;

    @ApiModelProperty(value = "地点", name = "supSite")
    private String supSite;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "地点权限", name = "limitList")
    private List<GaiaStoreData> limitList;
}
