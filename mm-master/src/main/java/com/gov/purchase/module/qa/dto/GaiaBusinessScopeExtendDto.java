package com.gov.purchase.module.qa.dto;

import com.gov.purchase.entity.GaiaBusinessScope;
import lombok.Data;

import java.util.List;

@Data
public class GaiaBusinessScopeExtendDto extends GaiaBusinessScope {

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 实体名称
     */
    private String bscEntityName;

    /**
     * 证照名
     */
    private String perCodeName;

    /**
     * 实体名
     */
    private String bscEntityIdName;

    /**
     * 分组明细数据
     */
    private List<GaiaBusinessScopeExtendDto> details;

}