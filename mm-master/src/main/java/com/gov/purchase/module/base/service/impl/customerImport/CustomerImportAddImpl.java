package com.gov.purchase.module.base.service.impl.customerImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaCustomerBasic;
import com.gov.purchase.entity.GaiaCustomerBusiness;
import com.gov.purchase.entity.GaiaCustomerBusinessKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaCustomerBasicMapper;
import com.gov.purchase.mapper.GaiaCustomerBusinessMapper;
import com.gov.purchase.module.base.dto.excel.Customer;
import com.gov.purchase.module.base.service.impl.ImportData;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.21
 */
@Service
public class CustomerImportAddImpl extends CustomerImport {

    @Resource
    GaiaCustomerBasicMapper gaiaCustomerBasicMapper;

    @Resource
    GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;

    /**
     * 数据验证结果
     *
     * @param path 文件路径
     */
    @Override
    protected void checkEnd(String path) {
        // 生成导入日志
        createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.WAIT.getCode());
    }

    /**
     * 业务数据验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap) {
        return super.checkList((LinkedHashMap<Integer, Customer>) dataMap);
    }

    /**
     * 主数据表验证
     *
     * @param customer
     * @param errorList
     * @param key
     */
    protected void baseCheck(Customer customer, List<String> errorList, int key) {
    }

    /**
     * 业务数据验证
     *
     * @param customer
     * @param errorList
     * @param key
     */
    protected void businessCheck(Customer customer, List<String> errorList, int key) {
        // 加盟商 客户自编码 存在验证
        List<GaiaCustomerBusiness> businessList = gaiaCustomerBusinessMapper.selfOnlyCheck(customer.getClient(), customer.getCusSelfCode(), customer.getCusSite());
        if (CollectionUtils.isNotEmpty(businessList)) {
            errorList.add(MessageFormat.format(ResultEnum.E0139.getMsg(), key + 1));
        }
    }

    /**
     * 数据导入
     *
     * @param bulUpdateCode 批量操作编码
     * @param dataMap
     * @param <T>
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    protected <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> dataMap) {
        // 批量导入
        try {
            for (Integer key : dataMap.keySet()) {
                // 行数据
                //CustomerBasic表
                Customer customer = (Customer) dataMap.get(key);
                GaiaCustomerBasic gaiaCustomerBasic = new GaiaCustomerBasic();
                BeanUtils.copyProperties(customer, gaiaCustomerBasic);
                //商品状态
                gaiaCustomerBasic.setCusStatus(customer.getCusStatusValue());
                //清空数据处理
                super.initNull(gaiaCustomerBasic);
                //客户编码生成
                String cusCode = gaiaCustomerBasicMapper.getCusCode();
                gaiaCustomerBasic.setCusCode(cusCode);
                gaiaCustomerBasicMapper.insertSelective(gaiaCustomerBasic);

                // 业务表数据存在判断
                if (!super.isNotBlank(customer.getClient())) {
                    continue;
                }
                //CustomerBusiness表
                GaiaCustomerBusiness gaiaCustomerBusiness = new GaiaCustomerBusiness();
                BeanUtils.copyProperties(customer, gaiaCustomerBusiness);
                // 地点客户状态
                gaiaCustomerBusiness.setCusStatus(customer.getSiteCusStatusValue());
                // 禁止销售
                gaiaCustomerBusiness.setCusNoSale(customer.getCusNoSaleValue());
                // 禁止退货
                gaiaCustomerBusiness.setCusNoReturn(customer.getCusNoReturnValue());
                // 是否启用信用管理
                gaiaCustomerBusiness.setCusCreditFlag(customer.getCusCreditFlagValue());
                // 信用检查点
                gaiaCustomerBusiness.setCusCreditCheck(customer.getCusCreditCheckValue());
                // 清空数据处理
                super.initNull(gaiaCustomerBusiness);
                //信用额度
                if (!ImportData.NULLVALUE.equals(customer.getCusCreditQuota()) && !StringUtils.isBlank(customer.getCusCreditQuota())) {
                    gaiaCustomerBusiness.setCusCreditQuota(new BigDecimal(customer.getCusCreditQuota()));
                }
                //客户编码生成
                gaiaCustomerBusiness.setCusCode(cusCode);
                gaiaCustomerBusiness.setCusMatchStatus(CommonEnum.MatchStatus.EXACTMATCH.getCode());
                gaiaCustomerBusinessMapper.insertSelective(gaiaCustomerBusiness);
            }
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

        // 更新导入日志
        editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.COMPLETE.getCode());

        return ResultUtil.success();
    }
}
