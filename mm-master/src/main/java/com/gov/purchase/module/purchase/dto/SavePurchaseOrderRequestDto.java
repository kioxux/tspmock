package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.module.goods.dto.GaiaPoItemList;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel(value = "采购订单保存传入参数")
public class SavePurchaseOrderRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 采购凭证号
     */
    @ApiModelProperty(value = "采购凭证号", name = "poId", required = true)
    @NotBlank(message = "采购凭证号不能为空")
    private String poId;

    /**
     * 采购订单类型
     */
    @ApiModelProperty(value = "采购订单类型", name = "poType", required = true)
    @NotBlank(message = "采购订单类型不能为空")
    private String poType;

    /**
     * 供应商编码
     */
    @ApiModelProperty(value = "供应商编码", name = "poSupplierId", required = true)
    @NotBlank(message = "供应商编码不能为空")
    private String poSupplierId;

    /**
     * 公司代码
     */
    @ApiModelProperty(value = "公司代码", name = "poCompanyCode", required = true)
    @NotBlank(message = "公司代码不能为空")
    private String poCompanyCode;

    /**
     * 凭证日期
     */
    @ApiModelProperty(value = "凭证日期", name = "poDate", required = true)
    @NotBlank(message = "凭证日期不能为空")
    private String poDate;

    /**
     * 凭证日期
     */
    @ApiModelProperty(value = "凭证日期", name = "poDate", required = true)
    @NotBlank(message = "凭证日期不能为空")
    private String poDateStr;

    /**
     * 付款条款
     */
    private String poPaymentId;

    /**
     * 抬头备注
     */
    private String poHeadRemark;

    /**
     * 物流模式
     */
    private String poDeliveryType;

    /**
     * 物流模式门店
     */
    private String poDeliveryTypeStore;

    /**
     * 明细表
     */
    private List<GaiaPoItemList> gaiaPoItemList;

    /**
     * 送货前置期(整数>=0)
     */
    private String poLeadTime;
    /**
     * 配送方式(字符串)
     */
    private String poDeliveryMode;
    /**
     * 承运单位(字符串)
     */
    private String poCarrier;
    /**
     * 运输时间(字符串)
     */
    private String poDeliveryTimes;

    /**
     * 供应商业务员
     */
    private String poSupplierSalesman;

    /**
     * 业务员姓名
     */
    private String poSalesmanName;

    /**
     * 货主姓名
     */
    private String poOwnerName;

}