package com.gov.purchase.module.base.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.01
 */
@Data
public class MainStore {

    /**
     * 值
     */
    private String value;

    /**
     * 表示
     */
    private String label;

    /**
     * 统一社会信用代码
     */
    private String creditCode;

    /**
     * 连锁总部
     */
    private String dcChainHead;
}
