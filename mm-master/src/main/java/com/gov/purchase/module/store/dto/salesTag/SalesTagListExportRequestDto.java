package com.gov.purchase.module.store.dto.salesTag;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel("销售标签列表提交参数")
public class SalesTagListExportRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    private String client;

    @ApiModelProperty(value = "价格组ID", name = "prcGroupId")
    private String prcGroupId;

    @ApiModelProperty(value = "门店编号", name = "stoCode")
    private String stoCode;

    @ApiModelProperty(value = "调价单号", name = "prcModfiyNo")
    private String prcModfiyNo;

    @ApiModelProperty(value = "商品自编号", name = "prcProduct")
    private String prcProduct;

    @ApiModelProperty(value = "价格类型(P001-零售价、P002-会员价、P003-医保价、P004-会员日价、P005拆零价、P006网上零售价、P007-网上会员价)", name = "prcClass")
    private String prcClass;

    @ApiModelProperty(value = "生效开始日期", name = "prcEffectStartDate")
    private String prcEffectStartDate;

    @ApiModelProperty(value = "生效结束日期", name = "prcEffectEndDate")
    private String prcEffectEndDate;

    @ApiModelProperty(value = "仅查询有库存(1-是, 0-否)", name = "hasQty")
    private String hasQty;

    @ApiModelProperty(value = "商品分类", name = "proClass")
    private String proClass;

    /**
     * 商品新建日期开始
     */
    @ApiModelProperty(value = "商品新建日期开始", name = "proCreateDateStrat")
    private String proCreateDateStrat;

    /**
     * 商品新建日期结束
     */
    @ApiModelProperty(value = "商品新建日期结束", name = "proCreateDateEnd")
    private String proCreateDateEnd;

    /**
     * 商品调价日期开始
     */
    @ApiModelProperty(value = "商品调价日期开始", name = "priceDateStart")
    private String priceDateStart;

    /**
     * 商品调价日期结束
     */
    @ApiModelProperty(value = "商品调价日期结束", name = "priceDateEnd")
    private String priceDateEnd;

    /**
     * 排除中药
     */
    @ApiModelProperty(value = "排除中药", name = "excludeTcm")
    private Integer excludeTcm;

}
