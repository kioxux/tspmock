package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaSoHeader;
import lombok.Data;

import java.util.List;

/**
 * @description: 暂存批发订单明细
 * @author: yzf
 * @create: 2021-12-30 16:18
 */
@Data
public class TemporaryOrderDetailDTO extends GaiaSoHeader {

    /**
     * 明细
     */
    private List<SalesOrderItem> orderItems;
}
