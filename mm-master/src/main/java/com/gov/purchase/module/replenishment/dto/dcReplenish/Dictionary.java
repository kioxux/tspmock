package com.gov.purchase.module.replenishment.dto.dcReplenish;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.18
 */
@Data
public class Dictionary {

    public Dictionary(String code, String label) {
        this.value = code;
        this.label = label;
    }

    /**
     * 值
     */
    private String value;

    /**
     * 表示
     */
    private String label;

}

