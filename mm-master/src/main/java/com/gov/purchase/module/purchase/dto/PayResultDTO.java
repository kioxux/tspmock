package com.gov.purchase.module.purchase.dto;

import lombok.Data;

/**
 * 支付回调
 */
@Data
public class PayResultDTO {

    private String payTime;
    private String paySeqId;
    private String invoiceAmount;
    private String settleDate;
    private String buyerId;
    private String receiptAmount;
    private String totalAmount;
    private String billBizType;
    private String buyerPayAmount;
    private String targetOrderId;
    private String merOrderId;
    private String status;
    private String targetSys;
}
