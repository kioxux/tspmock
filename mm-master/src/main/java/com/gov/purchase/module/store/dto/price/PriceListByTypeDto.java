package com.gov.purchase.module.store.dto.price;

import com.gov.purchase.common.excel.annotation.ExcelField;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PriceListByTypeDto {

    @ExcelField(title = "门店名称", type = 1, sort = 0)
    private String prcStoreName;

    @ExcelField(title = "商品编码", type = 1, sort = 1)
    private String proSelfCode;

    @ExcelField(title = "商品描述", type = 1, sort = 2)
    private String proDepict;

    @ExcelField(title = "规格", type = 1, sort = 3)
    private String proSpecs;

    @ExcelField(title = "调价单号", type = 1, sort = 4)
    private String prcModfiyNo;

    @ExcelField(title = "价格类型", type = 1, sort = 5, dictType = CommonEnum.DictionaryStaticData.PRC_CLASS)
    private String prcClass;

    @ExcelField(title = "价格", type = 1, sort = 6)
    private BigDecimal prcAmount;

    @ExcelField(title = "单位", type = 1, sort = 7)
    private String unitName;

    @ExcelField(title = "有效期起", type = 1, sort = 8)
    private String prcEffectDate;

//    @ExcelField(title = "不允许积分", type = 1, sort = 9, dictType = CommonEnum.DictionaryStaticData.NO_YES)
//    private String prcNoIntegral;
//
//    @ExcelField(title = "不允许积分兑换", type = 1, sort = 10, dictType = CommonEnum.DictionaryStaticData.NO_YES)
//    private String prcNoDiscount;
//
//    @ExcelField(title = "不允许会员打折", type = 1, sort = 11, dictType = CommonEnum.DictionaryStaticData.NO_YES)
//    private String prcNoExchange;
//
//    @ExcelField(title = "限购数量", type = 1, sort = 12)
//    private String prcLimitAmount;

    @ExcelField(title = "审批状态", type = 1, sort = 13, dictType = CommonEnum.DictionaryStaticData.APPROVE_STATUS)
    private String prcApprovalSuatus;

}
