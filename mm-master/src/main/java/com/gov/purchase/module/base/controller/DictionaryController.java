package com.gov.purchase.module.base.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.base.dto.DictionaryParams;
import com.gov.purchase.module.base.service.DictionaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.18
 */
@Api(tags = "数据字典")
@RestController
@RequestMapping("dictionary")
public class DictionaryController {

    @Resource
    DictionaryService dictionaryService;

    /**
     * 静态数据字典获取
     *
     * @param key
     * @return
     */
    @Log("静态数据字典获取")
    @ApiOperation("静态数据字典获取")
    @PostMapping("getDictionaryStatic")
    public Result getDictionaryStatic(@RequestJson(value = "key", required = false) String key) {
        return dictionaryService.getDictionaryStatic(key);
    }

    /**
     * 数据字典获取
     *
     * @return
     */
    @Log("数据字典")
    @ApiOperation("数据字典")
    @PostMapping("getDictionary")
    public Result getDictionary(@Valid @RequestBody DictionaryParams dictionaryParams) {
        return dictionaryService.getDictionary(dictionaryParams);
    }

    /**
     * 权限地点集合
     *
     * @return
     */
    @Log("权限地点集合")
    @ApiOperation("权限地点集合")
    @GetMapping("siteList")
    public Result siteList() {
        return dictionaryService.siteList();
    }

    /**
     * 权限地点集合首营用
     *
     * @return
     */
    @Log("权限地点集合")
    @ApiOperation("权限地点集合")
    @GetMapping("siteListForGspinfo")
    public Result siteListForGspinfo(){
        return dictionaryService.siteListForGspinfo();
    }

    /**
     * 加盟商信息集合
     *
     * @param client 加盟商ID 非必传，不传查询所有，传则返回一条长度的加盟商数据
     * @return
     */
    @Log("加盟商信息集合")
    @ApiOperation("加盟商信息集合")
    @PostMapping("franchiseeList")
    public Result franchiseeList(@RequestJson(value = "client", required = false) String client) {
        return dictionaryService.franchiseeList(client);
    }

    /**
     * 加盟商 门店信息集合
     *
     * @param client 加盟商ID 非必传，不传查询所有加盟商及门店级联数据，传则返回一条长度的加盟商及之门店数据
     * @return
     */
    @Log("加盟商 门店信息集合")
    @ApiOperation("加盟商 门店信息集合")
    @PostMapping("storeDataList")
    public Result storeDataList(@RequestJson(value = "client", required = false) String client) {
        return dictionaryService.storeDataList(client);
    }

    /**
     * 选择纳税主体
     * @return
     */
    @Log("选择纳税主体")
    @ApiOperation("选择纳税主体")
    @PostMapping("getTaxSubject")
    public Result getTaxSubject(@RequestJson(value = "chainHead") String chainHead) {
        return dictionaryService.getTaxSubject(chainHead);
    }
}

