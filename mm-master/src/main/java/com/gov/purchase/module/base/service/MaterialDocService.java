package com.gov.purchase.module.base.service;

import com.gov.purchase.entity.*;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.module.base.dto.feign.MaterialDocRequestDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface MaterialDocService {

    String addLog(List<MaterialDocRequestDto> list);

    /**
     * 生成物料凭证
     *
     * @param info
     * @return
     */
    FeignResult insertMaterialDoc(MaterialDocRequestDto info, Map<String, GaiaSdSaleD> gaiaSdSaleDMapLs, Map<String, String> matIdMap, Map<String, Integer> indexMap,
                                  Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap, List<GaiaDcData> gaiaDcDataList, List<GaiaProfitLevel> gaiaProfitLevelList);

    List<GaiaMaterialDocLogHWithBLOBs> getMaterialDocBatchTask();

    void taskError(String logId, String message);

    void exeError(String logId, String message);

    void taskSucess(String logId);

    @Transactional
    FeignResult insertMaterialDocBatchTask(GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs);

    FeignResult insertMaterialDocBatch(List<MaterialDocRequestDto> list, String logId, boolean isExe);

    void initComeAndGo(String client, String stoCode, String startDate, String endDate);
    void updateSupplierBusiness(String client, String supCode, String siteCode, String date);
}
