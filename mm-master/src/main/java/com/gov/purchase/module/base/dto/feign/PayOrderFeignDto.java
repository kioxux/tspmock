package com.gov.purchase.module.base.dto.feign;

import lombok.Data;

@Data
public class PayOrderFeignDto {

    /**
     *
     */
    private String client;
    private String payCompanyCode;
    private String payOrderPayer;
    private String payOrderId;
    private String payOrderDate;
    private String payOrderMode;
    /**
     * 应付账款余额
     */
    private String payOrderPayable;
    private String payOrderAmt;
    private String payOrderRemark;

}
