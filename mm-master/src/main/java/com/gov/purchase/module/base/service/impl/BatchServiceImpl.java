package com.gov.purchase.module.base.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaBatchUpdateLog;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaBatchUpdateLogMapper;
import com.gov.purchase.module.base.dto.BatSearchValBasic;
import com.gov.purchase.module.base.dto.BatchResult;
import com.gov.purchase.module.base.service.BatchService;
import com.gov.purchase.module.base.service.impl.businessScope.ScopeSearch;
import com.gov.purchase.module.base.service.impl.customerImport.CustomerImportAddImpl;
import com.gov.purchase.module.base.service.impl.customerImport.CustomerImportEditImpl;
import com.gov.purchase.module.base.service.impl.customerImport.CustomerSearch;
import com.gov.purchase.module.base.service.impl.dcImport.DcSearch;
import com.gov.purchase.module.base.service.impl.extendImport.ExtendImportAddImpl;
import com.gov.purchase.module.base.service.impl.permitImport.PermitSearch;
import com.gov.purchase.module.base.service.impl.productImport.ProductImportAddImpl;
import com.gov.purchase.module.base.service.impl.productImport.ProductImportEditImpl;
import com.gov.purchase.module.base.service.impl.productImport.ProductSearch;
import com.gov.purchase.module.base.service.impl.storeImport.StoreImportAddImpl;
import com.gov.purchase.module.base.service.impl.storeImport.StoreImportEditImpl;
import com.gov.purchase.module.base.service.impl.storeImport.StoreSearch;
import com.gov.purchase.module.base.service.impl.supplierImport.SupplierImportAddImpl;
import com.gov.purchase.module.base.service.impl.supplierImport.SupplierImportEditImpl;
import com.gov.purchase.module.base.service.impl.supplierImport.SupplierSearch;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.EnumUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.20
 */
@Service
public class BatchServiceImpl implements BatchService {

    @Resource
    GaiaBatchUpdateLogMapper gaiaBatchUpdateLogMapper;

    @Resource
    ProductImportAddImpl productBasicAdd;

    @Resource
    ProductImportEditImpl productBasicEdit;

    @Resource
    SupplierImportAddImpl supplierBasicAdd;

    @Resource
    SupplierImportEditImpl supplierBasicEdit;

    @Resource
    CustomerImportAddImpl customerImportAdd;

    @Resource
    CustomerImportEditImpl customerImportEdit;

    @Resource
    StoreImportAddImpl storeImportAdd;

    @Resource
    StoreImportEditImpl storeImportEdit;

    @Resource
    ProductSearch productSearch;

    @Resource
    SupplierSearch supplierSearch;

    @Resource
    CustomerSearch customerSearch;

    @Resource
    StoreSearch storeSearch;

    @Resource
    DcSearch dcSearch;

    @Resource
    PermitSearch permitSearch;

    @Resource
    ScopeSearch scopeSearch;

    @Resource
    CosUtils cosUtils;

    /**
     * 批量导入列表
     *
     * @param pageSize
     * @param pageNum
     * @param bulDateType     数据类型
     * @param bulUpdateType   操作类型
     * @param bulUpdateStatus 操作状态
     * @return
     */
    @Override
    public Result queryBatchList(Integer pageSize, Integer pageNum, String bulDateType, String bulUpdateType, String bulUpdateStatus) {
        PageHelper.startPage(pageNum, pageSize);
        List<BatchResult> list = gaiaBatchUpdateLogMapper.queryBatchList(bulDateType, bulUpdateType, bulUpdateStatus);
        PageInfo<BatchResult> pageInfo = new PageInfo<>(list);
        List<BatchResult> result = pageInfo.getList();
        if (CollectionUtils.isNotEmpty(result)) {
            for (BatchResult batchResult : result) {
                batchResult.setBulRouteAddress(cosUtils.urlAuth(batchResult.getBulRouteAddress()));
            }
        }
        return ResultUtil.success(pageInfo);
    }

    /**
     * excel 上传
     *
     * @param path 文件
     * @return
     */
    @Override
    public Result uploadBatch(String type, String path) {
        // 业务不存在
        if (!EnumUtils.contains(type, CommonEnum.ImportType.class)) {
            return ResultUtil.error(ResultEnum.E0102);
        }
        // 商品数据新增
        if (CommonEnum.ImportType.CREATE_PRODUCT.equals(CommonEnum.ImportType.getImportType(type))) {
            return productBasicAdd.checkData(path, type);
        }
        // 商品数据编辑
        if (CommonEnum.ImportType.EDIT_PRODUCT.equals(CommonEnum.ImportType.getImportType(type))) {
            return productBasicEdit.checkData(path, type);
        }
        // 供应商数据新增
        if (CommonEnum.ImportType.CREATE_SUPPLIER.equals(CommonEnum.ImportType.getImportType(type))) {
            return supplierBasicAdd.checkData(path, type);
        }
        // 供应商数据编辑
        if (CommonEnum.ImportType.EDIT_SUPPLIER.equals(CommonEnum.ImportType.getImportType(type))) {
            return supplierBasicEdit.checkData(path, type);
        }
        // 门店新增
        if (CommonEnum.ImportType.CREATE_STORE.equals(CommonEnum.ImportType.getImportType(type))) {
            return storeImportAdd.checkData(path, type);
        }
        // 门店编辑
        if (CommonEnum.ImportType.EDIT_STORE.equals(CommonEnum.ImportType.getImportType(type))) {
            return storeImportEdit.checkData(path, type);
        }
        // 客户新增
        if (CommonEnum.ImportType.CREATE_CUSTOMER.equals(CommonEnum.ImportType.getImportType(type))) {
            return customerImportAdd.checkData(path, type);
        }
        // 客户编辑
        if (CommonEnum.ImportType.EDIT_CUSTOMER.equals(CommonEnum.ImportType.getImportType(type))) {
            return customerImportEdit.checkData(path, type);
        }
        // 扩展新增
//        if (CommonEnum.ImportType.EXTENT_PRODUCT.equals(CommonEnum.ImportType.getImportType(type))) {
//            return extendImportAdd.checkData(path, type);
//        }
        return ResultUtil.success();
    }

    /**
     * 数据导入
     *
     * @param bulUpdateCode
     * @return
     */
    @Override
    public Result importBatch(String bulUpdateCode) {
        // 批量操作日志数据
        GaiaBatchUpdateLog gaiaBatchUpdateLog = gaiaBatchUpdateLogMapper.selectByPrimaryKey(bulUpdateCode);
        if (gaiaBatchUpdateLog == null) {
            throw new CustomResultException(ResultEnum.E0124);
        }
        // 导入类型
        String type = gaiaBatchUpdateLog.getBulUpdateType() + "_" + gaiaBatchUpdateLog.getBulDateType();
        // 业务不存在
        if (!EnumUtils.contains(type, CommonEnum.ImportType.class)) {
            return ResultUtil.error(ResultEnum.E0102);
        }
        // 商品数据新增
        if (CommonEnum.ImportType.CREATE_PRODUCT.equals(CommonEnum.ImportType.getImportType(type))) {
            return productBasicAdd.importData(bulUpdateCode);
        }
        // 商品数据编辑
        if (CommonEnum.ImportType.EDIT_PRODUCT.equals(CommonEnum.ImportType.getImportType(type))) {
            return productBasicEdit.importData(bulUpdateCode);
        }
        // 供应商数据新增
        if (CommonEnum.ImportType.CREATE_SUPPLIER.equals(CommonEnum.ImportType.getImportType(type))) {
            return supplierBasicAdd.importData(bulUpdateCode);
        }
        // 供应商数据编辑
        if (CommonEnum.ImportType.EDIT_SUPPLIER.equals(CommonEnum.ImportType.getImportType(type))) {
            return supplierBasicEdit.importData(bulUpdateCode);
        }
        // 门店编辑
        if (CommonEnum.ImportType.EDIT_STORE.equals(CommonEnum.ImportType.getImportType(type))) {
            return storeImportEdit.importData(bulUpdateCode);
        }
        // 门店新增
        if (CommonEnum.ImportType.CREATE_STORE.equals(CommonEnum.ImportType.getImportType(type))) {
            return storeImportAdd.importData(bulUpdateCode);
        }
        // 客户新增
        if (CommonEnum.ImportType.CREATE_CUSTOMER.equals(CommonEnum.ImportType.getImportType(type))) {
            return customerImportAdd.importData(bulUpdateCode);
        }
        // 客户编辑
        if (CommonEnum.ImportType.EDIT_CUSTOMER.equals(CommonEnum.ImportType.getImportType(type))) {
            return customerImportEdit.importData(bulUpdateCode);
        }
        // 商品扩展属性
//        if (CommonEnum.ImportType.EXTENT_PRODUCT.equals(CommonEnum.ImportType.getImportType(type))) {
//            return extendImportAdd.importData(bulUpdateCode);
//        }
        return ResultUtil.success();
    }

    /**
     * 批量查询
     *
     * @param batSearchValBasic
     * @return
     */
    @Override
    public Result searchBatch(BatSearchValBasic batSearchValBasic) {
        // 查询对象
        BatSearchValBasic.SearchBasicEnum searchBasicEnum = BatSearchValBasic.SearchBasicEnum.getSearchBasicEnum(batSearchValBasic);
        if (searchBasicEnum == null) {
            return ResultUtil.success();
        }

        // 商品 数据库
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.PRODUCT_DB)) {
            return productSearch.selectProductBasicBatch(batSearchValBasic);
        }

        // 商品 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.PRODUCT_CLIENT)) {
            return productSearch.selectProductBusinessBatch(batSearchValBasic);
        }

        // 供应商 数据库
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.SUPPLIER_DB)) {
            return supplierSearch.selectSupplierBasicBatch(batSearchValBasic);
        }

        // 供应商 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.SUPPLIER_CLIENT)) {
            return supplierSearch.selectSupplierBusinessBatch(batSearchValBasic);
        }

        // 客户 数据库
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.CUSTOMER_DB)) {
            return customerSearch.selectCustomerBasicBatch(batSearchValBasic);
        }

        // 客户 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.CUSTOMER_CLIENT)) {
            return customerSearch.selectCustomerBusinessBatch(batSearchValBasic);
        }

        // 门店 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.STORE_CLIENT)) {
            return storeSearch.selectStoreBatch(batSearchValBasic);
        }

        // DC 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.DC_CLIENT)) {
            return dcSearch.selectDcBatch(batSearchValBasic);
        }

        // 证照资质 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.PERMIT_CLIENT)) {
            return permitSearch.selectPermitBatch(batSearchValBasic);
        }

        // 经营范围 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.SCOPE_CLIENT)) {
            return scopeSearch.selectScopeBatch(batSearchValBasic);
        }

        return ResultUtil.success();
    }

    /**
     * 导出
     * @param batSearchValBasic
     * @return
     */
    @Override
    public Result exportBatch(BatSearchValBasic batSearchValBasic) {
        // 查询对象
        BatSearchValBasic.SearchBasicEnum searchBasicEnum = BatSearchValBasic.SearchBasicEnum.getSearchBasicEnum(batSearchValBasic);
        if (searchBasicEnum == null) {
            return ResultUtil.success();
        }
        // 商品 数据库
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.PRODUCT_DB)) {
            return productSearch.exportProductBasicBatch(batSearchValBasic);
        }
        // 商品 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.PRODUCT_CLIENT)) {
            return productSearch.exportProductBusinessBatch(batSearchValBasic);
        }

        // 供应商 数据库
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.SUPPLIER_DB)) {
            return supplierSearch.exportSupplierBasicBatch(batSearchValBasic);
        }
        // 供应商 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.SUPPLIER_CLIENT)) {
            return supplierSearch.exportSupplierBusinessBatch(batSearchValBasic);
        }

        // 客户 数据库
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.CUSTOMER_DB)) {
            return customerSearch.exportCustomerBasicBatch(batSearchValBasic);
        }
        // 客户 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.CUSTOMER_CLIENT)) {
            return customerSearch.exportCustomerBusinessBatch(batSearchValBasic);
        }

        // 门店 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.STORE_CLIENT)) {
            return storeSearch.exportStoreBatch(batSearchValBasic);
        }

        // DC 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.DC_CLIENT)) {
            return dcSearch.exportDcBatch(batSearchValBasic);
        }

        // 证照资质 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.PERMIT_CLIENT)) {
            return permitSearch.exportPermitDetailBatch(batSearchValBasic);
        }

        // 经营范围 加盟商
        if (searchBasicEnum.equals(BatSearchValBasic.SearchBasicEnum.SCOPE_CLIENT)) {
            return scopeSearch.exportScopeDetailBatch(batSearchValBasic);
        }

        return ResultUtil.success();
    }


//供应商	2	数据库	1	供应商编码	a
//		加盟商	2	供应商名称	b
//				供应商分类	c
//				供应商状态	d
//				加盟商	e
//				地点	f
//				供应商自编码	g
//				供应商状态	h
//
//客户	3	数据库	1	客户编码	a
//		加盟商	2	客户名称	b
//				客户分类	c
//				客户状态	d
//				加盟商	e
//				地点	f
//				客户自编码	g
//				客户状态	h
//
//门店	4	加盟商	2	加盟商	a
//				门店编码	b
//				门店名称	c
//				门店属性	d
//				配送方式	e
//				门店状态	f
//				是否医保店	g
//				税分类	h
//				连锁总部	i
//
//DC	5	加盟商	2	加盟商	a
//				DC编码	b
//				DC名称	c
//				DC状态	d
//				连锁总部	e
//
//证照资质	6	加盟商	2	加盟商	a
//				实体类型	b
//				实体编码	c
//
//经营范围	1	加盟商	2	加盟商	a
//				实体类型	b
//				实体编码	c

}

