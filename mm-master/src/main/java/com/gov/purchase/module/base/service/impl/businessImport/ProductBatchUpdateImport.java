package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductUpdateItem;
import com.gov.purchase.entity.GaiaProductUpdateItemKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaProductUpdateItemMapper;
import com.gov.purchase.module.base.dto.businessImport.ProductBatchUpdate;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.goods.dto.ProductUpdateItem;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.08.09
 */
@Service
public class ProductBatchUpdateImport extends BusinessImport {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaProductUpdateItemMapper gaiaProductUpdateItemMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 加盟商
        String client = feignService.getLoginInfo().getClient();
        if (field.get("proSite") == null || StringUtils.isBlank(field.get("proSite").toString())) {
            throw new CustomResultException("请选择地点");
        }
        String proSite = field.get("proSite").toString();
        List<String> proSites = Arrays.asList(proSite.split(","));
        if (field.get("gpuiField") == null || StringUtils.isBlank(field.get("gpuiField").toString())) {
            throw new CustomResultException("请选择修改字段");
        }
        String gpuiField = field.get("gpuiField").toString();
        GaiaProductUpdateItemKey gaiaProductUpdateItemKey = new GaiaProductUpdateItemKey();
        gaiaProductUpdateItemKey.setClient(client);
        gaiaProductUpdateItemKey.setGpuiField(gpuiField);
        GaiaProductUpdateItem gaiaProductUpdateItem = gaiaProductUpdateItemMapper.selectByPrimaryKey(gaiaProductUpdateItemKey);
        if (gaiaProductUpdateItem == null) {
            throw new CustomResultException("修改字段已变更，请刷新后再试");
        }
        List<Map<String, String>> params = new ArrayList<>();
        List<String> proSelfCodeList = new ArrayList<>();
        List<String> errorList = new ArrayList<>();
        for (Integer key : map.keySet()) {
            // 行数据
            ProductBatchUpdate productBatchUpdate = (ProductBatchUpdate) map.get(key);
            Map<String, String> proMap = new HashMap<>();
            for (String item : proSites) {
                proMap.put("client", client);
                proMap.put("stoCode", item);
                proMap.put("proSelfCode", productBatchUpdate.getProSelfCode());
                params.add(proMap);
                if (proSelfCodeList.contains(productBatchUpdate.getProSelfCode() + "," + item)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0026.getMsg(), key + 1));
                } else {
                    proSelfCodeList.add(productBatchUpdate.getProSelfCode() + "," + item);
                }
            }
        }
        List<GaiaProductBusiness> list = gaiaProductBusinessMapper.selectByPrimaryKeyList(client, params);
        List<ProductUpdateItem> productUpdateItemList = new ArrayList<>();
        for (Integer key : map.keySet()) {
            // 行数据
            ProductBatchUpdate productBatchUpdate = (ProductBatchUpdate) map.get(key);
            // 商品数据
            GaiaProductBusiness gaiaProductBusiness = list.stream().filter(item ->
                    item.getProSelfCode().equals(productBatchUpdate.getProSelfCode())
            ).findFirst().orElse(null);
            if (gaiaProductBusiness == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品编码"));
                continue;
            }
            ProductUpdateItem productUpdateItem = new ProductUpdateItem();
            BeanUtils.copyProperties(gaiaProductUpdateItem, productUpdateItem);
            BeanUtils.copyProperties(gaiaProductBusiness, productUpdateItem);
            String humpField = StringUtils.lineToHump(gpuiField);
            Object value = StringUtils.getFieldValueByName(humpField, gaiaProductBusiness);
            productUpdateItem.setOldVal(value == null ? null : value.toString());
            productUpdateItem.setProSelfCode(productBatchUpdate.getProSelfCode());
            productUpdateItem.setNewVal(productBatchUpdate.getNewVal());
            productUpdateItemList.add(productUpdateItem);
        }
        // 验证报错
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success(productUpdateItemList);
    }

}
