package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.12
 */
@Data
public class DcreplenisImp {

    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String gdreCode;

    /**
     * 补货数量
     */
    @ExcelValidate(index = 1, name = "补货数量", type = ExcelValidate.DataType.INTEGER, min = 0)
    private String gdreReplenishNum;

}
