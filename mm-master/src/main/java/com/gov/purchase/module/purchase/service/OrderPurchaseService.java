package com.gov.purchase.module.purchase.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.purchase.dto.*;

import java.io.IOException;
import java.util.List;

public interface OrderPurchaseService {

    /**
     * 采购订单创建
     *
     * @param dto CreatePurchaseOrderRequestDto
     */
    Result insertPurchaseOrder(CreatePurchaseOrderRequestDto dto);

    /**
     * 供应商列表
     *
     * @param dto CreatePurchaseOrderRequestDto
     * @return List<SupplierListForPurchaseResponseDto>
     */
    List<SupplierListForPurchaseResponseDto> selectSupplierListForPurchaseo(SupplierListForPurchaseRequestDto dto);

    /**
     * 供应商列表
     *
     * @param dto CreatePurchaseOrderRequestDto
     * @return List<SupplierListForPurchaseResponseDto>
     */
    List<SupplierListForPurchaseResponseDto> resultGetSupplierListAndDCForPurchase(SupplierListForPurchaseRequestDto dto);

    /**
     * 商品列表
     *
     * @param dto GetProductListRequestDto
     * @return List<GetProductListResponseDto>
     */
    List<GetProductListResponseDto> selectGetProductList(GetProductListRequestDto dto);

    /**
     * 地点集合
     *
     * @param dto GetSiteListRequestDto
     * @return List<GetSiteListResponseDto>
     */
    List<GetSiteListResponseDto> selectGetSiteList(GetSiteListRequestDto dto);

    /**
     * 地点集合
     *
     * @param dto GetSiteListRequestDto
     * @return List<GetSiteListResponseDto>
     */
    List<GetSiteListResponseDto> selectGetSiteList1(GetSiteListRequestDto dto);

    /**
     * 门店选择
     *
     * @param dto StoreSelectRequestDto
     * @return List<GetSiteListResponseDto>
     */
    List<GetSiteListResponseDto> StoreSelect(StoreSelectRequestDto dto);

    /**
     * 客户信息
     *
     * @param client 加盟商
     * @param site   地点
     * @return
     */
    Result customerInfo(String client, String site);

    /**
     * 采购订单查询
     *
     * @param dto PoHeaderListRequestDto
     * @return PageInfo<PoHeaderListResponseDto>
     */
    PageInfo<PoHeaderListResponseDto> selectPoHeaderList(PoHeaderListRequestDto dto);

    /**
     * 采购订单明细
     *
     * @param dto PoItemRequestDto
     * @return PoItemResponseDto
     */
    PoItemResponseDto selectPoItemList(PoItemRequestDto dto);

    /**
     * 后续凭证
     *
     * @param dto FuCertificateRequestDto
     * @return List<FuCertificateResponseDto>
     */
    List<FuCertificateResponseDto> selectFuCertificate(FuCertificateRequestDto dto);


    /**
     * 采购订单保存
     *
     * @param dto SavePurchaseOrderRequestDto
     */
    void updateSavePurchaseOrder(SavePurchaseOrderRequestDto dto);

    /**
     * 采购订单打印信息
     *
     * @param dto
     * @return
     */
    PurchaseOrderResponseDto getPurchaseOrderInfo(PoItemRequestDto dto);

    /**
     * 商品编码
     *
     * @param dto
     * @return
     */
    List<GetProductListResponseDto> getProductListForAdjustment(GetProductListRequestDto dto);

    /**
     * 导出
     *
     * @param dto
     * @return
     */
    Result exportList(PoHeaderListRequestDto dto);

    /**
     * 采购订间汇总查询
     *
     * @param dto
     * @return
     */
    PageInfo<PoHeaderListResponseDto> getPoHeaderList(PoHeaderListRequestDto dto);

    /**
     * 暂存
     */
    Result temporaryStoragePurchaseOrder(CreatePurchaseOrderRequestDto createPurchaseOrderRequestDto);

    /**
     * 暂存数据保存
     */
    Result temporaryToSave(CreatePurchaseOrderRequestDto createPurchaseOrderRequestDto);

    /**
     * 经营范围管控
     *
     * @param createPurchaseOrderRequestDto
     * @return
     */
    void checkForJyFw(CreatePurchaseOrderRequestDto createPurchaseOrderRequestDto);

    PageInfo<GetProductListResponseDto> getProductListPage(GetProductListRequestDto dto);

    /**
     * 暂存明细
     */
    PoItemResponseDto temporaryDetails(String poId);

    /**
     * 暂存列表
     */
    List<PoHeaderListResponseDto> temporaryList();

    /**
     * 暂存删除
     */
    void temporaryRemove(String poId);

    /**
     * 商品列表_弹出框
     */
    PageInfo<GetProductListResponseDto> getProductListForAdjustmentPopup(GetProductListRequestDto dto);

    /**
     * 采购订单删除
     *
     * @param poId
     */
    void delPurchase(String poId);

    Result getPoFollow(String poId);

    Result selectHisPo(String proSelfCode, Integer pageSize, Integer pageNum);

    void supplierPaymentAtt(SupplierPaymentAtt supplierPaymentAtt) throws IOException;

    Result checkPurchaseOrder(CreatePurchaseOrderRequestDto createPurchaseOrderRequestDto);

    Result checkExistOrder(String proSelfCode, String poCompanyCode);

    /**
     * 采购订单创建，或者仓库补货生成采购订单是否允许同一张订单中有相同的多行商品（配置表查询  0：允许，1：不允许）
     *
     * @return
     */
    String queryPoConfig();

    String selectPoDateChange();
}
