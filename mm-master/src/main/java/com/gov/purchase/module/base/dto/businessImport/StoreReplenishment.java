package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class StoreReplenishment {

    /**
     * 门店编码
     */
    @ExcelValidate(index = 0, name = "门店编码", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String stoCode;

    /**
     * 门店名称
     */
    @ExcelValidate(index = 1, name = "门店名称", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String stoName;

    /**
     * 商品编码
     */
    @ExcelValidate(index = 2, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proCode;

    /**
     * 商品名称
     */
    @ExcelValidate(index = 3, name = "商品名称", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String proName;

    /**
     * 规格
     */
    @ExcelValidate(index = 4, name = "规格", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String proSpecs;

    /**
     * 生产厂家
     */
    @ExcelValidate(index = 5, name = "生产厂家", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String proFactoryName;

    /**
     * 单位
     */
    @ExcelValidate(index = 6, name = "单位", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String unitName;

    /**
     * 中包装量
     */
    @ExcelValidate(index = 7, name = "中包装量", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String proMidPackage;

    /**
     * 补货单号
     */
    @ExcelValidate(index = 8, name = "补货单号", type = ExcelValidate.DataType.STRING, addRequired = false, relyRequired = {"gsrdSerial"})
    private String gsrhVoucherId;

    /**
     * 补货行号
     */
    @ExcelValidate(index = 9, name = "补货行号", type = ExcelValidate.DataType.STRING, addRequired = false, relyRequired = {"gsrhVoucherId"})
    private String gsrdSerial;

    /**
     * 门店库存
     */
    @ExcelValidate(index = 10, name = "门店库存", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String storeStockTotal;

    /**
     * 最后一次进货日期
     */
    @ExcelValidate(index = 11, name = "最后一次进货日期", type = ExcelValidate.DataType.DATE, maxLength = 8, addRequired = false)
    private String lastPurchaseDate;

    /**
     * 最后进货供应商编码
     */
    @ExcelValidate(index = 12, name = "最后进货供应商编码", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false)
    private String lastSupplierId;

    /**
     * 最后进货供应商
     */
    @ExcelValidate(index = 13, name = "最后进货供应商", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String lastSupplier;

    /**
     * 末次进货价
     */
    @ExcelValidate(index = 14, name = "末次进货价", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String lastPurchasePrice;

    /**
     * 30天门店销售量
     */
    @ExcelValidate(index = 15, name = "30天门店销售量", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String daysSalesCount;

    /**
     * 在途量
     */
    @ExcelValidate(index = 16, name = "在途量", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String trafficCount;

    /**
     * 待出量
     */
    @ExcelValidate(index = 17, name = "待出量", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String waitCount;

    /**
     * 建议补货数量
     */
    @ExcelValidate(index = 18, name = "建议补货数量", type = ExcelValidate.DataType.DECIMAL)
    private String replenishmentValue;

    /**
     * 供应商编码
     */
    @ExcelValidate(index = 19, name = "供应商编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String supplierId;

    /**
     * 供应商名称
     */
    @ExcelValidate(index = 20, name = "供应商名称", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String supplier;

    /**
     * 采购价格
     */
    @ExcelValidate(index = 21, name = "采购价格", type = ExcelValidate.DataType.DECIMAL)
    private String purchasePrice;

    /**
     * 税率
     */
    @ExcelValidate(index = 22, name = "税率", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String proInputTax;

    /**
     * 预计到货日期
     */
    @ExcelValidate(index = 23, name = "预计到货日期", type = ExcelValidate.DataType.DATE, addRequired = false)
    private String poDeliveryDate;
}

