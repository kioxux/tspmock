package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

/**
 * @program: mm
 * @description:
 * @author: yzf
 * @create: 2021-11-01 14:35
 */
@Data
public class WholeSaleChannelDetailDTO {
    /**
     * 商品数量
     */
    private Integer proCount;
    /**
     * 用户数量
     */
    private Integer cusCount;
    /**
     * 渠道名
     */
    private String chaName;
    /**
     * 渠道编码
     */
    private String chaCode;
    /**
     * 渠道状态, 0-正常
     */
    private String status;
    /**
     * 仓库编码
     */
    private String dcCode;
}
