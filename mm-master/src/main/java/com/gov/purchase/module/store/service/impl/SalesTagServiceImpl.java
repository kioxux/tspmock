package com.gov.purchase.module.store.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaPriceTag;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaLabelApplyMapper;
import com.gov.purchase.mapper.GaiaPriceTagMapper;
import com.gov.purchase.mapper.GaiaRetailPriceMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListExportRequestDto;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListReponseDto;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListRequest;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListRequestDto;
import com.gov.purchase.module.store.service.SalesTagService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.JsonUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SalesTagServiceImpl implements SalesTagService {
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaRetailPriceMapper gaiaRetailPriceMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    private GaiaPriceTagMapper gaiaPriceTagMapper;
    @Resource
    private GaiaLabelApplyMapper gaiaLabelApplyMapper;
    @Resource
    private CosUtils cosUtils;

    @Override
    public PageInfo<SalesTagListReponseDto> querySalesTagList(SalesTagListRequestDto dto) {
        if (dto.getHasQty() == null || dto.getHasQty().equals("0")) {
            dto.setHasQty(null);
        }
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        dto.setPrcClass("P001");
        List<SalesTagListReponseDto> result = gaiaRetailPriceMapper.queryTagListWithDate(dto);
        if (result != null && result.size() > 0) {
            result.stream().forEach(o -> o.setPriceOfficer(user.getLoginName()));
        }
        return new PageInfo<>(result);
    }

    @Override
    public List<SalesTagListReponseDto> querySalesTagExportList(SalesTagListExportRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        SalesTagListRequestDto salesTagListRequestDto = new SalesTagListRequestDto();
        BeanUtils.copyProperties(dto, salesTagListRequestDto);
        if (salesTagListRequestDto.getHasQty() == null || salesTagListRequestDto.getHasQty().equals("0")) {
            salesTagListRequestDto.setHasQty(null);
        }
        List<SalesTagListReponseDto> result = gaiaRetailPriceMapper.queryTagListWithDate(salesTagListRequestDto);
        if (result != null && result.size() > 0) {
            result.stream().forEach(o -> {
                o.setPriceOfficer(user.getLoginName());
                o.setPrcClassName(CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PRC_CLASS, o.getPrcClass()).getLabel());
            });
        }
        return result;
    }

    /**
     * 商品标签查询 非套打
     *
     * @param salesTagListRequest
     * @return
     */
    @Override
    public Result querySalesTagList2(SalesTagListRequest salesTagListRequest) {
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(salesTagListRequest.getPageNum(), salesTagListRequest.getPageSize());
        salesTagListRequest.setClient(StringUtils.isBlank(salesTagListRequest.getClient()) ? user.getClient() : salesTagListRequest.getClient());
        List<SalesTagListReponseDto> list = gaiaRetailPriceMapper.querySalesTagList2(salesTagListRequest);
        PageInfo pageInfo = new PageInfo<>(list);
        return ResultUtil.success(pageInfo);
    }

    /**
     * 非套打打印数据保存
     *
     * @param salesTagListReponseDtoList
     * @return
     */
    @Override
    public Result printTag(List<SalesTagListReponseDto> salesTagListReponseDtoList) {
        String key = UUID.randomUUID().toString().replaceAll("-", "");
        redisClient.set(key, JsonUtils.beanToJson(salesTagListReponseDtoList), 3600);
        // 打印状态修改
        if (CollectionUtils.isNotEmpty(salesTagListReponseDtoList)) {
            TokenUser user = feignService.getLoginInfo();
            // 门店编码
            String labStore = salesTagListReponseDtoList.get(0).getLabStore();
            // 价签申请单号
            String labApplyNo = salesTagListReponseDtoList.get(0).getLabApplyNo();
            // 商品编码
            List<String> proSelfCodeList = salesTagListReponseDtoList.stream().filter(Objects::nonNull)
                    .map(SalesTagListReponseDto::getProSelfCode)
                    .collect(Collectors.toList());
            if (StringUtils.isNotBlank(labStore) && StringUtils.isNotBlank(labApplyNo) && CollectionUtils.isNotEmpty(proSelfCodeList)) {
                String[] nums = labApplyNo.split(",");
                for (String num : nums) {
                    if (StringUtils.isNotBlank(num)) {
                        gaiaLabelApplyMapper.print(user.getClient(), labStore, num, proSelfCodeList);
                    }
                }
            }
        }
        return ResultUtil.success(key);
    }

    /**
     * 打钱数据获取
     *
     * @param key
     * @return
     */
    @Override
    public Result getPrintTag(String key) {
        List<SalesTagListReponseDto> list = new ArrayList<>();
        try {
            String data = redisClient.get(key);
            list = JsonUtils.jsonToBeanList(data, SalesTagListReponseDto.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultUtil.success(list);
    }

    @Override
    public void savePriceTag(GaiaPriceTag gaiaPriceTag) {
        TokenUser user = feignService.getLoginInfo();
        gaiaPriceTag.setClient(user.getClient());
        gaiaPriceTag.setGptType(gaiaPriceTag.getGptType() == null ? 1 : gaiaPriceTag.getGptType());
        gaiaPriceTagMapper.insertSelective(gaiaPriceTag);
    }

    @Override
    public Result selectPriceTagList(String gptName, Integer gptType, Integer pageNum, Integer pageSize) {
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(pageNum, pageSize);
        gptType = gptType == null ? 1 : gptType;
        List<GaiaPriceTag> list = gaiaPriceTagMapper.selectPriceTagList(user.getClient(), gptName, gptType);
        PageInfo<GaiaPriceTag> pageInfo = new PageInfo<>(list);
        return ResultUtil.success(pageInfo);
    }

    @Override
    public void editTag(GaiaPriceTag gaiaPriceTag) {
        gaiaPriceTag.setGptType(gaiaPriceTag.getGptType() == null ? 1 : gaiaPriceTag.getGptType());
        gaiaPriceTagMapper.updateByPrimaryKeySelective(gaiaPriceTag);
    }

    @Override
    public void deleteTag(Integer id) {
        gaiaPriceTagMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Result selectTag(Integer id) {
        GaiaPriceTag gaiaPriceTag = gaiaPriceTagMapper.selectByPrimaryKey(id);
        return ResultUtil.success(gaiaPriceTag);
    }

    @Override
    public Result selectPriceTag(Integer gptType) {
        TokenUser user = feignService.getLoginInfo();
        gptType = gptType == null ? 1 : gptType;
        List<GaiaPriceTag> list = gaiaPriceTagMapper.selectPriceTagList(user.getClient(), null, gptType);
        return ResultUtil.success(list);
    }

    /**
     * 销售标签导出
     *
     * @param dto
     * @return
     */
    @Override
    public Result salesTagExprot(SalesTagListRequestDto dto) {
        try {
            if (dto.getHasQty() == null || dto.getHasQty().equals("0")) {
                dto.setHasQty(null);
            }
            dto.setPrcClass("P001");
            PageHelper.startPage(0, 0);
            List<SalesTagListReponseDto> list = gaiaRetailPriceMapper.queryTagListWithDate(dto);
            List<List<Object>> dataList = new ArrayList<>();
            TokenUser user = feignService.getLoginInfo();
            for (int i = 0; CollectionUtils.isNotEmpty(list) && i < list.size(); i++) {
                SalesTagListReponseDto salesTagListReponseDto = list.get(i);
                List<Object> data = new ArrayList<>();
                //  门店号
                data.add(salesTagListReponseDto.getPrcStore());
                //  门店名称
                data.add(StringUtils.isNotBlank(salesTagListReponseDto.getStoShortName()) ? salesTagListReponseDto.getStoShortName() : salesTagListReponseDto.getPrcStoreName());
                //  通用名
                data.add(salesTagListReponseDto.getProCommonname());
                //  商品名称
                data.add(salesTagListReponseDto.getProName());
                // 生产厂家
                data.add(salesTagListReponseDto.getProFactoryName());
                //  规格
                data.add(salesTagListReponseDto.getProSpecs());
                //  单位
                data.add(salesTagListReponseDto.getUnitName());
                //  商品编码
                data.add(salesTagListReponseDto.getProSelfCode());
                //  销售级别
                data.add(salesTagListReponseDto.getProSlaeClass());
                //  零售价
                data.add(salesTagListReponseDto.getPrcAmount());
                //  会员价
                data.add(salesTagListReponseDto.getPrcAmountHy());
                //  是否医保
                data.add(salesTagListReponseDto.getProIfMedName());
                // 不打折商品
                data.add("1".equals(salesTagListReponseDto.getProBdz()) ? "是" : "0".equals(salesTagListReponseDto.getProBdz()) ? "否" : "");
                //  产地
                data.add(salesTagListReponseDto.getProPlace());
                //  国际条形码
                data.add(salesTagListReponseDto.getProBarcode());
                //  核价员
                data.add(user.getLoginName());
                //  处方类别
                data.add(salesTagListReponseDto.getProPresclassName());
                //  商品定位
                data.add(salesTagListReponseDto.getProPosition());

                dataList.add(data);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<Object>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("销售标签");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            if (StringUtils.isNotBlank(dto.getStoCode()) && StringUtils.isNotBlank(dto.getLabApplyNo())) {
                gaiaLabelApplyMapper.output(user.getClient(), dto.getStoCode(), dto.getLabApplyNo());
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据表头
     */
    private final String[] headList = {
            "门店号",
            "门店名称",
            "通用名",
            "商品名称",
            "生产厂家",
            "规格",
            "单位",
            "商品编码",
            "销售级别",
            "零售价",
            "会员价",
            "是否医保",
            "不打折商品",
            "产地",
            "国际条形码",
            "核价员",
            "处方类别",
            "商品定位",
    };
}
