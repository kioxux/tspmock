package com.gov.purchase.module.replenishment.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.base.service.StoreReplenishService;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentRequestDto;
import com.gov.purchase.module.replenishment.dto.QueryReplenishReq;
import com.gov.purchase.module.replenishment.dto.StoreRequestDto;
import com.gov.purchase.module.replenishment.service.StoreNeedService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.27
 */
@Api(tags = "门店补货")
@RestController
@RequestMapping("storeReplenish")
public class StoreReplenishController {

    @Resource
    private StoreReplenishService storeReplenishService;

    @Log("门店补货查询")
    @ApiOperation("门店补货查询")
    @PostMapping("queryReplenishList")
    public Result queryReplenishList(@Valid @RequestBody QueryReplenishReq queryReplenishReq) {
        return storeReplenishService.queryReplenishList(queryReplenishReq);
    }

    @Log("门店补货保存")
    @ApiOperation("门店补货保存")
    @PostMapping("saveReplenish")
    public Result saveReplenish(@Valid @RequestBody List<DcReplenishmentRequestDto> list) {
        storeReplenishService.saveReplenish(list);
        return ResultUtil.success();
    }

    @Log("门店补货金额确认")
    @ApiOperation("门店补货金额确认")
    @PostMapping("checkAmt")
    public Result checkAmt(@Valid @RequestBody List<DcReplenishmentRequestDto> list) {
        return storeReplenishService.checkAmt(list);
    }

    @Log("门店补货查询导出")
    @ApiOperation("门店补货查询导出")
    @PostMapping("exportList")
    public Result exportList(@RequestBody QueryReplenishReq queryReplenishReq) {
        return storeReplenishService.exportList(queryReplenishReq);
    }
}

