package com.gov.purchase.module.base.dto.feign;

import lombok.Data;

@Data
public class ProductGspFeignDto {

    private String proCode;
    private String proName;
    private String proCommonname;
    private String proForm;
    private String proSpecs;
    private String proPackSpecs;
    private String sccj;
    private String proGmp;
    private String proRegisterNo;
    private String proQs;
    private String proFunction;
    private String proLifeDate;
    private String proStorageCondition;
    private String proStorageConditionName;
    private String proSupplyName;
    private String proSupplyAdd;
    private String proSupplyContact;
    private String proSupplyTel;
    private String wfSite;
    /**
     * 商品自编码
     */
    private String proSelfCode;
    /**
     * 商品仓储分区
     */
    private String proStorageArea;
}
