package com.gov.purchase.module.customer.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "客户库搜索列表传入参数")
public class CustomerGspListRequestDto extends Pageable {

    @ApiModelProperty(value = "加盟商编号", name = "client", hidden = true)
    private String client;

    @ApiModelProperty(value = "客户名称", name = "cusName")
    private String cusName;

    @ApiModelProperty(value = "统一社会信用代码", name = "cusCreditCode")
    private String cusCreditCode;

    @ApiModelProperty(value = "客户状态", name = "cusStatus")
    private String cusStatus;

    @ApiModelProperty(value = "是否经营", name = "sell")
    private String sell;

    @ApiModelProperty(value = "地点", name = "cusSite")
    private String cusSite;

}
