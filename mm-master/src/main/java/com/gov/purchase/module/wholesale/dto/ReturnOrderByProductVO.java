package com.gov.purchase.module.wholesale.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021-06-07 9:45
 */
@Data
public class ReturnOrderByProductVO {

    @NotBlank(message = "仓库编码不能为空")
    private String dcCode;

    @NotBlank(message = "客户编码不能空")
    private String customerId;

    private String remark;

    private String orderType;

    private String code;

    private String siteCode;

    @Valid
    @NotEmpty(message = "商品列表不能为空")
    private List<ReturnProduct> proList;

    @Data
    public static class ReturnProduct {

        @ApiModelProperty(value = "批号", name = "batBatchNo")
        private String batBatchNo;

        @ApiModelProperty(value = "税率", name = "proOutputTax")
        private String proOutputTax;

        @ApiModelProperty(value = "商品编码", name = "soProCode")
        @NotBlank(message = "商品编码不能为空")
        private String soProCode;
        @ApiModelProperty(value = "本次退货数量不能为空", name = "soQty")
        @NotNull(message = "本次退货数量不能为空")
        private BigDecimal soQty;
        @ApiModelProperty(value = "订单单位", name = "soUnit")
        @NotBlank(message = "订单单位不能为空")
        private String soUnit;
        @ApiModelProperty(value = "退货价不能为空", name = "soPrice")
        @NotNull(message = "退货价不能为空")
        private BigDecimal soPrice;
        @ApiModelProperty(value = "订单行金额", name = "soLineAmt")
//        @NotNull(message = "行总价不能为空")
        private BigDecimal soLineAmt;
        @NotBlank(message = "退库原因不能为空")
        private String reason;


        @ApiModelProperty(value = "批次", name = "soBatch")
//        @NotNull(message = "批次不能为空")
        private String soBatch;
        @ApiModelProperty(value = "库存地点", name = "soLocationCode")
        private String soLocationCode;
        @ApiModelProperty(value = "税率", name = "soRate")
        private String soRate;
        @ApiModelProperty(value = "计划发货日期", name = "soDeliveryDate")
        private String soDeliveryDate;
        @ApiModelProperty(value = "订单行备注", name = "soLineRemark")
        private String soLineRemark;
        @ApiModelProperty(value = "生产批号", name = "soBatchNo")
        private String soBatchNo;
        @ApiModelProperty(value = "货位号", name = "gsrdHwh")
        private String gsrdHwh;
        @ApiModelProperty(value = "有效期至", name = "batExpiryDate")
        private String batExpiryDate;

        private String orderId;

        private String lineNo;

    }

}
