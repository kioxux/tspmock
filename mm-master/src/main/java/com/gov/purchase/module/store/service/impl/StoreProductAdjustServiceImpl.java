package com.gov.purchase.module.store.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaPriceAdjust;
import com.gov.purchase.entity.GaiaPriceAdjustKey;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaPriceAdjustMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.store.dto.price.AdjustPriceRequestDto;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;
import com.gov.purchase.module.store.service.StoreProductAdjustService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Slf4j
public class StoreProductAdjustServiceImpl implements StoreProductAdjustService {
    @Resource
    private CosUtils cosUtils;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaPriceAdjustMapper priceAdjustMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;

    @Override
    public PageInfo<GaiaPriceAdjustDto> queryList(AdjustPriceRequestDto adjustPriceRequestDto) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaPriceAdjustDto> productBusinessResponseDtos = new ArrayList<>();
        Map<Object, Object> map = new HashMap<>();

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getStoCode())) {
            //门店编码
            map.put("stoCode", adjustPriceRequestDto.getStoCode());
        }

        if (null != adjustPriceRequestDto && !adjustPriceRequestDto.getStoCodeList().isEmpty()) {
            //门店编码集合
            map.put("stoCodeList", adjustPriceRequestDto.getStoCodeList());
        }

        if (null != adjustPriceRequestDto && !adjustPriceRequestDto.getPraAdjustNoList().isEmpty()) {
            //调价单号集合
            map.put("praAdjustNoList", adjustPriceRequestDto.getPraAdjustNoList());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getStogCode())) {
            //店组编码
            map.put("stogCode", adjustPriceRequestDto.getStogCode());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getPraAdjustNo())) {
            //调价单号
            map.put("praAdjustNo", adjustPriceRequestDto.getPraAdjustNo());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getProName())) {
            //商品名称
            map.put("proName", adjustPriceRequestDto.getProName());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getProSelfCode())) {
            //商品编码
            map.put("proSelfCode", adjustPriceRequestDto.getProSelfCode());
        }

        if (null != adjustPriceRequestDto && !adjustPriceRequestDto.getProSelfCodeList().isEmpty()) {
            //商品编码集合
            map.put("proSelfCodeList", adjustPriceRequestDto.getProSelfCodeList());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getStartDate())) {
            //调价开始日期
            map.put("startDate", adjustPriceRequestDto.getStartDate());
        }
        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getEndDate())) {
            //调价结束日期
            map.put("endDate", adjustPriceRequestDto.getEndDate());
        }
        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getPraApprovalStatus())) {
            //审批状态
            map.put("praApprovalStatus", adjustPriceRequestDto.getPraApprovalStatus());
        }
        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getPraSendStatus())) {
            //同步状态
            map.put("praSendStatus", adjustPriceRequestDto.getPraSendStatus());
        }

        Map<Object, Object> mapParam = new HashMap<>();

        mapParam.put("CLIENT", user.getClient());
        mapParam.put("USERID", user.getUserId());
        List<GaiaStoreData> storeList = gaiaStoreDataMapper.SelectStoreAuth(mapParam);
        if (!storeList.isEmpty()) {
            map.put("AUTHLIST", storeList);
            map.put("CLIENT", user.getClient());
            // 分页
            PageHelper.startPage(adjustPriceRequestDto.getPageNum(), adjustPriceRequestDto.getPageSize());
            productBusinessResponseDtos = priceAdjustMapper.selectByMapForList(map);
        }
        return new PageInfo<>(productBusinessResponseDtos);
    }

    @Override
    public Result queryExportList(AdjustPriceRequestDto adjustPriceRequestDto) {
        try {
            PageInfo<GaiaPriceAdjustDto> pageInfo = queryList(adjustPriceRequestDto);
            List<GaiaPriceAdjustDto> adjustDtoList = pageInfo.getList();
            List<List<String>> dataList = new ArrayList<>();
            for (GaiaPriceAdjustDto dto : adjustDtoList) {
                // 打印行
                List<String> item = new ArrayList<>();

                // 门店编码
                item.add(dto.getPraStore());
                // 门店名称
                item.add(dto.getStoName());
                // 商品编码
                item.add(dto.getProSelfCode());
                // 商品名称
                item.add(dto.getProName());
                // 规格
                item.add(dto.getProSpecs());
                // 厂家
                item.add(dto.getProFactoryName());
                // 单位
                item.add(dto.getProUnit());
                // 零售价
                item.add(null != dto.getPraPriceNormal() ? dto.getPraPriceNormal().toString() : "");
                // 会员价
                item.add(null != dto.getPraPriceHy() ? dto.getPraPriceHy().toString() : "");
                // 医保价
                item.add(null != dto.getPraPriceYb() ? dto.getPraPriceYb().toString() : "");
                // 拆零价
                item.add(null != dto.getPraPriceCl() ? dto.getPraPriceCl().toString() : "");
                // 网上零售价
                item.add(null != dto.getPraPriceOln() ? dto.getPraPriceOln().toString() : "");
                // 网上会员价
                item.add(null != dto.getPraPriceOlhy() ? dto.getPraPriceOlhy().toString() : "");
                // 调价单号
                item.add(dto.getPraAdjustNo());
                // 调价生效日期
                item.add(dto.getPraStartDate());
                // 调价日期
                item.add(dto.getPraCreateDate());
                // 审批状态
                item.add(null != dto.getPraApprovalStatus() ? "1".equals(dto.getPraApprovalStatus()) ? "待审批" : "已审批" : "");
                // 生效状态
                item.add(null != dto.getPraSendStatus() ? "1".equals(dto.getPraSendStatus()) ? "未同步" : "2".equals(dto.getPraSendStatus()) ? "已同步" : "作废" : "");
                // 店组
                item.add(dto.getStogCode());

                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("商品调价导出");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;


        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }

    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "门店",
            "店名",
            "商品编码",
            "商品名称",
            "规格",
            "厂家",
            "单位",
            "零售价",
            "会员价",
            "医保价",
            "拆零价",
            "网上零售价",
            "网上会员价",
            "调价单号",
            "调价生效日期",
            "调价日期",
            "审批状态",
            "生效状态",
            "店组"
    };


    @Override
    public Result batchAdjust(List<AdjustPriceRequestDto> list) {
        TokenUser user = feignService.getLoginInfo();
        if (!list.isEmpty()) {
            List<GaiaPriceAdjust> newList = new ArrayList<>();
            List<GaiaPriceAdjust> adjusts = priceAdjustMapper.selectList(user.getClient(), list);
            if (!adjusts.isEmpty()) {
                adjusts.forEach(item -> {
                    GaiaPriceAdjust adjust = new GaiaPriceAdjust();
                    adjust.setClient(user.getClient());
                    adjust.setPraAdjustNo(item.getPraAdjustNo());
                    adjust.setPraStore(item.getPraStore());
                    adjust.setPraProduct(item.getPraProduct());
                    adjust.setPraSendStatus("3");
                    newList.add(adjust);
                });
                priceAdjustMapper.updateBatch(newList);
            }
        }
        return ResultUtil.success();
    }


    @Override
    public PageInfo<GaiaPriceAdjustDto> queryGoodsList(AdjustPriceRequestDto adjustPriceRequestDto) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaPriceAdjustDto> productBusinessResponseDtos = new ArrayList<>();
        Map<Object, Object> map = getMap(adjustPriceRequestDto);
        map.put("CLIENT", user.getClient());
        Map<Object, Object> mapParam = new HashMap<>();
        mapParam.put("CLIENT", user.getClient());
        mapParam.put("USERID", user.getUserId());

        List<GaiaDcData> dcList = gaiaDcDataMapper.SelectDcAuth(mapParam);
        if (CollectionUtils.isNotEmpty(dcList)) {
            map.put("AUTHLIST", null);
        } else {
            List<GaiaStoreData> storeList = gaiaStoreDataMapper.SelectStoreAuth(mapParam);
            if (!storeList.isEmpty()) {
                map.put("AUTHLIST", storeList);
            } else {
                map.put("AUTHLIST", new ArrayList<GaiaStoreData>() {{
                    add(new GaiaStoreData());
                }});
            }
        }

        // 分页
        PageHelper.startPage(adjustPriceRequestDto.getPageNum(), adjustPriceRequestDto.getPageSize());
        productBusinessResponseDtos = priceAdjustMapper.selectByMapForGoodList2(map);

        return new PageInfo<>(productBusinessResponseDtos);
    }

    private Map<Object, Object> getMap(AdjustPriceRequestDto adjustPriceRequestDto) {
        Map<Object, Object> map = new HashMap<>();

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getStogCode())) {
            //店组编码
            map.put("stogCode", adjustPriceRequestDto.getStogCode());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getStoCode())) {
            //门店编码
            map.put("stoCode", adjustPriceRequestDto.getStoCode());
        }

        if (null != adjustPriceRequestDto && !adjustPriceRequestDto.getStoCodeList().isEmpty()) {
            //门店编码集合
            map.put("stoCodeList", adjustPriceRequestDto.getStoCodeList());
        }

        if (null != adjustPriceRequestDto && !adjustPriceRequestDto.getPraAdjustNoList().isEmpty()) {
            //调价单号集合
            map.put("praAdjustNoList", adjustPriceRequestDto.getPraAdjustNoList());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getPraAdjustNo())) {
            //调价单号
            map.put("praAdjustNo", adjustPriceRequestDto.getPraAdjustNo());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getProName())) {
            //商品名称
            map.put("proName", adjustPriceRequestDto.getProName());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getProSelfCode())) {
            //商品编码
            map.put("proSelfCode", adjustPriceRequestDto.getProSelfCode());
        }

        if (null != adjustPriceRequestDto && !adjustPriceRequestDto.getProSelfCodeList().isEmpty()) {
            //商品编码集合
            map.put("proSelfCodeList", adjustPriceRequestDto.getProSelfCodeList());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getStartDate())) {
            //调价开始日期
            map.put("startDate", adjustPriceRequestDto.getStartDate());
        }
        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getEndDate())) {
            //调价结束日期
            map.put("endDate", adjustPriceRequestDto.getEndDate());
        }

        if (null != adjustPriceRequestDto && StringUtils.isNotBlank(adjustPriceRequestDto.getProBrand())) {
            //品牌名
            map.put("proBrand", adjustPriceRequestDto.getProBrand());
        }
        return map;
    }

    @Override
    public Result queryGoodsListExport(AdjustPriceRequestDto adjustPriceRequestDto) {
        try {
            TokenUser user = feignService.getLoginInfo();
            List<GaiaPriceAdjustDto> productBusinessResponseDtos = new ArrayList<>();
            Map<Object, Object> map = getMap(adjustPriceRequestDto);
            map.put("CLIENT", user.getClient());
            Map<Object, Object> mapParam = new HashMap<>();
            mapParam.put("CLIENT", user.getClient());
            mapParam.put("USERID", user.getUserId());

            List<GaiaDcData> dcList = gaiaDcDataMapper.SelectDcAuth(mapParam);
            if (CollectionUtils.isNotEmpty(dcList)) {
                map.put("AUTHLIST", null);
            } else {
                List<GaiaStoreData> storeList = gaiaStoreDataMapper.SelectStoreAuth(mapParam);
                if (!storeList.isEmpty()) {
                    map.put("AUTHLIST", storeList);
                } else {
                    map.put("AUTHLIST", new ArrayList<GaiaStoreData>() {{
                        add(new GaiaStoreData());
                    }});
                }
            }
            productBusinessResponseDtos = priceAdjustMapper.selectByMapForGoodList(map);
            List<List<Object>> dataList = new ArrayList<>();
            for (GaiaPriceAdjustDto dto : productBusinessResponseDtos) {
                // 打印行
                List<Object> item = new ArrayList<>();

                // 门店编码
                item.add(dto.getPraStore());
                // 门店名称
                item.add(dto.getStoName());
                // 商品编码
                item.add(dto.getProSelfCode());
                // 商品名称
                item.add(dto.getProName());
                // 规格
                item.add(dto.getProSpecs());
                // 厂家
                item.add(dto.getProFactoryName());
                // 单位
                item.add(dto.getProUnit());
                // 品牌
                item.add(dto.getProBrand());
                // 零售价
                item.add(dto.getPraPriceNormal());
                // 会员价
                item.add(dto.getPraPriceHy());
                // 最新进价
                item.add(dto.getLastPurchasePrice());
                // 医保价
                item.add(dto.getPraPriceYb());
                // 拆零价
                item.add(dto.getPraPriceCl());
                // 网上零售价
                item.add(dto.getPraPriceOln());
                // 网上会员价
                item.add(dto.getPraPriceOlhy());
                // 店组
                item.add(dto.getStogCode());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                    new ArrayList<String[]>() {{
                        add(headList2);
                    }},
                    new ArrayList<List<List<Object>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("商品最新价导出");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }

    }

    /**
     * 根据加盟商 + 门店 + 调价单 查询 调价单商品信息
     *
     * @param gaiaPriceAdjustKey
     * @return
     */
    @Override
    public List<GaiaPriceAdjustDto> queryPriceAdjustList(GaiaPriceAdjustKey gaiaPriceAdjustKey) {
        TokenUser user = feignService.getLoginInfo();
        gaiaPriceAdjustKey.setClient(user.getClient());
        return priceAdjustMapper.selectPriceAdjustList(gaiaPriceAdjustKey);
    }

    /**
     * 导出数据表头
     */
    private String[] headList2 = {
            "门店",
            "店名",
            "商品编码",
            "商品名称",
            "规格",
            "厂家",
            "单位",
            "品牌",
            "零售价",
            "会员价",
            "最新进价",
            "医保价",
            "拆零价",
            "网上零售价",
            "网上会员价",
            "店组"
    };


}
