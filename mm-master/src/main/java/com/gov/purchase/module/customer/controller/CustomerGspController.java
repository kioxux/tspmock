package com.gov.purchase.module.customer.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaProductGspinfo;
import com.gov.purchase.module.customer.dto.*;
import com.gov.purchase.module.customer.service.CustomerGspService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "客户首营")
@RestController
@RequestMapping("customerGsp")
public class CustomerGspController {

    @Resource
    private CustomerGspService customerGspService;

    @Log("客户库搜索页面")
    @ApiOperation("客户库搜索页面")
    @PostMapping("queryCustomerList")
    public Result queryCustomerList(@Valid @RequestBody CustomerGspListRequestDto dto) {
        return ResultUtil.success(customerGspService.queryCustomerList(dto));
    }


    @Log("客户基础信息")
    @ApiOperation("客户基础信息")
    @GetMapping("getCustomerBasicInfo")
    public Result getCustomerBasicInfo(@RequestParam("cusCode") String cusCode) {
        return ResultUtil.success(customerGspService.getCustomerBasicInfo(cusCode));
    }

    /**
     * 手工新增
     *
     * @return
     */
    @Log("手工新增")
    @ApiOperation("手工新增")
    @PostMapping("addBusioness")
    public Result addBusioness(@RequestBody CustomerGspRequestDto customerGspRequestDto) {
        return customerGspService.addBusioness(customerGspRequestDto);
    }

    @Log("客户首营提交")
    @ApiOperation("客户首营提交")
    @PostMapping("saveCustomerGspInfo")
    public Result saveCustomerGspInfo(@Valid @RequestBody CustomerGspRequestDto dto) {
        return customerGspService.saveCustomerGspInfo(dto);
    }

    @Log("客户首营列表")
    @ApiOperation("客户首营列表")
    @PostMapping("queryCustomerGspList")
    public Result queryCustomerGspList(@Valid @RequestBody CustomerGspInfoListRequestDto dto) {
        return ResultUtil.success(customerGspService.queryCustomerGspList(dto));
    }


    @ApiOperation("导出客户首营列表")
    @PostMapping("exportQueryCustomerGspList")
    public Result exportQueryCustomerGspList(@Valid @RequestBody CustomerGspInfoListRequestDto dto) {
        return ResultUtil.success(customerGspService.exportQueryCustomerGspList(dto));
    }


    @Log("客户首营信息详情")
    @ApiOperation("客户首营信息详情")
    @PostMapping("getCustomerGspInfo")
    public Result getCustomerGspInfo(@Valid @RequestBody CustomerGspInfoDetailRequestDto dto) {
        return ResultUtil.success(customerGspService.queryCustomerGspDetail(dto));
    }

    /**
     * 自编码返回
     * @param client
     * @param cusSite
     * @param code
     * @return
     */
    @Log("自编码返回")
    @ApiOperation("自编码返回")
    @PostMapping("getMaxSelfCodeNum")
    public Result getMaxSelfCodeNum(@RequestJson("client") String client, @RequestJson("cusSite") String cusSite,
                                    @RequestJson(value = "code", required = false) String code) {
        return customerGspService.getMaxSelfCodeNum(client, cusSite, code);
    }

}
