package com.gov.purchase.module.supplier.controller;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.supplier.dto.*;
import com.gov.purchase.module.supplier.service.DrugSupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "药品供应商首营")
@RestController
@RequestMapping("supplier")
public class DrugSupplierController {

    @Resource
    private DrugSupplierService drugSupplierService;

    /**
     * 厂商库搜索
     *
     * @param dto QuerySupListRequestDto
     * @return PageInfo<QuerySupListResponseDto>
     */
    @Log("厂商库搜索")
    @ApiOperation("厂商库搜索")
    @PostMapping("querySupList")
    public Result resultQueryProductBasicInfo(@Valid @RequestBody QuerySupListRequestDto dto) {
        // 厂商库搜索
        PageInfo<QuerySupListResponseDto> goodsInfoList = drugSupplierService.selectDrugSupplierInfo(dto);
        return ResultUtil.success(goodsInfoList);
    }

    /*
     * 供应商首营提交
     *
     * @param dto GoodsRequestDto
     * @return
     */
    @Log("供应商首营提交")
    @ApiOperation("供应商首营提交")
    @PostMapping("gspinfoSubmit")
    public Result resultGspinfoSubmitInfo(@Valid @RequestBody GspinfoSubmitRequestDto dto) {
        // 供应商首营提交
        drugSupplierService.insertGspinfoSubmitInfo(dto);
        return ResultUtil.success();
    }

    /**
     * 手工新增
     *
     * @return
     */
    @Log("手工新增")
    @ApiOperation("手工新增")
    @PostMapping("addBusioness")
    public Result addBusioness(@RequestBody GspinfoSubmitRequestDto gspinfoSubmitRequestDto) {
        return drugSupplierService.addBusioness(gspinfoSubmitRequestDto);
    }

    /**
     * 供应商首营列表
     *
     * @param dto QueryProductBasicRequestDto
     * @return PageInfo<QueryGspListResponseDto>
     */
    @Log("供应商首营列表")
    @ApiOperation("供应商首营列表")
    @PostMapping("queryGspinfoList")
    public Result resultQueryGspinfoList(@Valid @RequestBody QueryGspinfoListRequestDto dto) {
        // 供应商首营列表查詢
        PageInfo<QueryGspListResponseDto> pageInfo = drugSupplierService.selectQueryGspinfo(dto);
        return ResultUtil.success(pageInfo);
    }


    /**
     * 供应商首营列表
     *
     * @param dto QueryProductBasicRequestDto
     * @return PageInfo<QueryGspListResponseDto>
     */
    @Log("导出供应商首营列表")
    @ApiOperation("导出供应商首营列表")
    @PostMapping("exportQueryGspinfoList")
    public Result exportQueryGspinfoList(@Valid @RequestBody QueryGspinfoListRequestDto dto) {
        // 供应商首营列表查詢
        Result result = drugSupplierService.exportQueryGspinfoList(dto);
        return ResultUtil.success(result);
    }


    /**
     * 供应商首营详情
     *
     * @param dto QueryGspRequestDto
     * @return GaiaProductGspinfo
     */
    @Log("供应商首营详情")
    @ApiOperation("供应商首营详情")
    @PostMapping("queryGspinfo")
    public Result resultQueryGspValue(@Valid @RequestBody GaiaSupplierRequestDto dto) {
        // 供应商首营详情查询
        QueryGspListResponseDto gaiaProductGspinfo = drugSupplierService.selectQueryGspValue(dto);
        return ResultUtil.success(gaiaProductGspinfo);
    }

    /**
     * 自编码返回
     * @param client
     * @param supSite
     * @param code
     * @return
     */
    @Log("自编码返回")
    @ApiOperation("自编码返回")
    @PostMapping("getMaxSelfCodeNum")
    public Result getMaxSelfCodeNum(@RequestJson("client") String client, @RequestJson("supSite") String supSite,
                                    @RequestJson(value = "code", required = false) String code) {
        return drugSupplierService.getMaxSelfCodeNum(client, supSite, code);
    }
}
