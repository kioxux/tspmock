package com.gov.purchase.module.wholesale.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaBatchInfo;
import com.gov.purchase.module.wholesale.dto.QueryReturnOrder;
import com.gov.purchase.module.wholesale.dto.ReturnOrderByProductVO;
import com.gov.purchase.module.wholesale.dto.ReturnOrderDTO;
import com.gov.purchase.module.wholesale.dto.ReturnOrderReq;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.10
 */
public interface ReturnOrderService {

    /**
     * 销售退货订单列表
     *
     * @param queryReturnOrder
     * @return
     */
    Result querySalesOrderList(QueryReturnOrder queryReturnOrder);

    /**
     * 退货
     *
     * @param list
     * @return
     */
    Result returnOrder(List<ReturnOrderReq> list);

    /**
     * 按商品退货
     */
    void returnOrderByProduct(ReturnOrderByProductVO vo);

    /**
     * 商品批号信息
     */
    List<GaiaBatchInfo> getProBatchInfo(String site, String proSelfCode);

    /**
     * 调取退货单
     *
     * @param dto
     * @return
     */
    Result queryReturnOrder(ReturnOrderDTO dto);

    /**
     * 退货单明细
     *
     * @param dto
     * @return
     */
    Result queryReturnOrderDetail(ReturnOrderDTO dto);

    /**
     * 退货单关闭
     * @param dto
     */
    void closeReturnOrder(ReturnOrderDTO dto);
}
