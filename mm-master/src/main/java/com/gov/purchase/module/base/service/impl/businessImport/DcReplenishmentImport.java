package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.businessImport.DcReplenishment;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentListResponseDto;
import com.gov.purchase.module.replenishment.dto.SupplierRequestDto;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class DcReplenishmentImport extends BusinessImport {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    private GaiaPaymentTypeMapper gaiaPaymentTypeMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaSupplierSalesmanMapper gaiaSupplierSalesmanMapper;
    @Resource
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        TokenUser user = feignService.getLoginInfo();

        String client = user.getClient();
        // 参数 验证
        if (field == null || field.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0140);
        }
        // DC编码
        String dcCode = null;
        Object dcCodeObject = field.get("dcCode");
        if (dcCodeObject == null || StringUtils.isBlank(dcCodeObject.toString())) {
            throw new CustomResultException(ResultEnum.E0151);
        }
        dcCode = dcCodeObject.toString();

        List<String> errorList = new ArrayList<>();
        // 返回结果集
        List<DcReplenishmentListResponseDto> resultList = new ArrayList<>();
        // 批量导入
        try {
            // 配送平均天数  默认为 3天
            Integer dcDeliveryDays = 3;
            List<String> proCodes = new ArrayList<>();
            List<String> supCodes = new ArrayList<>();
            for (Integer key : map.keySet()) {
                DcReplenishment dcReplenishment = (DcReplenishment) map.get(key);
                proCodes.add(dcReplenishment.getProSelfCode());
                if (StringUtils.isNotBlank(dcReplenishment.getPoSupplierId())) {
                    supCodes.add(dcReplenishment.getPoSupplierId());
                }
            }
            List<GaiaSupplierBusiness> gaiaSupplierBusinessList = null;
            if (!CollectionUtils.isEmpty(supCodes)) {
                //验证供应商存在性
                gaiaSupplierBusinessList = gaiaSupplierBusinessMapper.selectSupplierListStatus(client, supCodes, dcCode);
            }
            // 验证商品存在性
            Map<String, Object> params = new HashMap<>();
            params.put("dcCode", dcCode);  // 获取当前人的 dc编号
            params.put("client", user.getClient());
            params.put("proSelfCodes", proCodes);
            // 根据当前人的配送中心 查询DC补货列表
            List<DcReplenishmentListResponseDto> list = gaiaDcDataMapper.queryReplenishmentList(params);
            // 商品列表
            List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectProList(user.getClient(), dcCode, proCodes);
            // 昨日门店需求
            List<GaiaSdReplenishD> gaiaSdReplenishDList = gaiaSdReplenishDMapper.selectGsrdNeedQtyByPro(user.getClient(), proCodes);

            // 供应商下拉框
            SupplierRequestDto supplierRequestDto = new SupplierRequestDto();
            supplierRequestDto.setClient(user.getClient());
            supplierRequestDto.setSiteCode(dcCode);
//            supplierRequestDto.setProSelfCode(dcReplenishmentListResponseDto.getProSelfCode());
//            List<SupplierResponseDto> supplierResponseDtoList = dcReplenishmentService.querySupplierList(supplierRequestDto);
            // 付款条款
            List<GaiaPaymentType> gaiaPaymentTypeList = gaiaPaymentTypeMapper.selectPaymentTypeList();
            // 业务员
            List<GaiaSupplierSalesman> gaiaSupplierSalesmanList = gaiaSupplierSalesmanMapper.supplierSalesmanList(user.getClient(), dcCode, null);
            Map<String, List<GaiaSupplierSalesman>> gaiaSupplierSalesmanMap = gaiaSupplierSalesmanList.stream().collect(Collectors.groupingBy(t -> t.getSupSelfCode()));
            // 验证excel  数据是否正确
            for (Integer key : map.keySet()) {
                DcReplenishment dcReplenishment = (DcReplenishment) map.get(key);
                GaiaProductBusiness gaiaProductBusinesses = gaiaProductBusinessList.stream().filter(item -> (item.getProSelfCode().equals(dcReplenishment.getProSelfCode()))).findFirst().orElse(null);
                if (gaiaProductBusinesses == null) {
                    // 第几行商品不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品"));
                    continue;
                }
                if ("T".equals(gaiaProductBusinesses.getProPosition())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "商品定位不能为淘汰"));
                    continue;
                }
                if ("1".equals(gaiaProductBusinesses.getProNoPurchase())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "商品禁采"));
                    continue;
                }
                List<DcReplenishmentListResponseDto> row = list.stream().filter(item -> (item.getProSelfCode().equals(dcReplenishment.getProSelfCode()))).collect(Collectors.toList());
                if (CollectionUtils.isEmpty(row)) {
                    // 第几行商品不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品"));
                    continue;
                }
                DcReplenishmentListResponseDto dcReplenishmentListResponseDto = new DcReplenishmentListResponseDto();
                BeanUtils.copyProperties(row.get(0), dcReplenishmentListResponseDto);
                /**
                 * （中包装从商品主数据中取值，若为空则默认是1）
                 */
                if (StringUtils.isBlank(dcReplenishmentListResponseDto.getProMidPackage())) {
                    dcReplenishmentListResponseDto.setProMidPackage("1");
                }
                // 供应商
//                dcReplenishmentListResponseDto.setSupplierResponseDtoList(supplierResponseDtoList);
                // 供应商
                if (StringUtils.isNotBlank(dcReplenishment.getPoSupplierId())) {
                    if (CollectionUtils.isEmpty(gaiaSupplierBusinessList)) {
                        // 第几行供应商不存在
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "供应商"));
                        continue;
                    } else {
                        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessList.stream().filter(item -> (item.getSupSelfCode().equals(dcReplenishment.getPoSupplierId()))).findFirst().orElse(null);
                        if (gaiaSupplierBusiness == null) {
                            // 第几行供应商不存在
                            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "供应商"));
                            continue;
                        }
                        dcReplenishmentListResponseDto.setPoSupplierId(dcReplenishment.getPoSupplierId());

                        // 验证付款条件是否存在
                        if (StringUtils.isNotBlank(dcReplenishment.getSupPayTerm())) {
                            if (CollectionUtils.isEmpty(gaiaPaymentTypeList)) {
                                // 第几行付款条件不存在
                                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "付款条件"));
                                continue;
                            } else {
                                List<GaiaPaymentType> gaiaPaymentTypes = gaiaPaymentTypeList.stream().filter(item -> item.getPayType().equals(dcReplenishment.getSupPayTerm())).collect(Collectors.toList());
                                if (CollectionUtils.isEmpty(gaiaPaymentTypes)) {
                                    // 第几行付款条件不存在
                                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "付款条件"));
                                    continue;
                                }
                                dcReplenishmentListResponseDto.setSupPayTerm(dcReplenishment.getSupPayTerm());
                            }
                        } else {
                            dcReplenishmentListResponseDto.setSupPayTerm(gaiaSupplierBusiness.getSupPayTerm());
                        }
                    }
                }
                //计划交货时间不能早于当前时间
                if (StringUtils.isNotBlank(dcReplenishment.getPoDeliveryDate())) {
                    if (DateUtils.parseLocalDate(dcReplenishment.getPoDeliveryDate(), "yyyyMMdd").compareTo(LocalDate.now()) < 0) {
                        errorList.add(MessageFormat.format(ResultEnum.E0145.getMsg(), key + 1));
                        continue;
                    }
                } else {
                    dcReplenishment.setPoDeliveryDate(DateUtils.getCurrentDateStrYYMMDD());
                }
                dcReplenishmentListResponseDto.setReplenishmentValue(new BigDecimal(dcReplenishment.getPoQty()));
                dcReplenishmentListResponseDto.setPoDeliveryDate(dcReplenishment.getPoDeliveryDate());
                dcReplenishmentListResponseDto.setPoLineRemark(dcReplenishment.getPoLineRemark());
                dcReplenishmentListResponseDto.setPoPrice(StringUtils.isNotBlank(dcReplenishment.getPurchasePrice()) ? new BigDecimal(dcReplenishment.getPurchasePrice()) : null);
                dcReplenishmentListResponseDto.setProUnitName(dcReplenishmentListResponseDto.getProUnit());
                dcReplenishmentListResponseDto.setProName(row.get(0).getProDepict());
                dcReplenishmentListResponseDto.setProCommonname(gaiaProductBusinesses.getProCommonname());
                dcReplenishmentListResponseDto.setProIfWholesale(gaiaProductBusinesses.getProIfWholesale());
                dcReplenishmentListResponseDto.setPoBatchNo(dcReplenishment.getPoBatchNo());
                dcReplenishmentListResponseDto.setPoScrq(dcReplenishment.getPoScrq());
                dcReplenishmentListResponseDto.setPoYxq(dcReplenishment.getPoYxq());
                dcReplenishmentListResponseDto.setWmKysl(row.get(0).getWmKysl());
                //新增分单功能鉴别字段存储条件proStorageCondition
                dcReplenishmentListResponseDto.setProStorageCondition(gaiaProductBusinesses.getProStorageCondition());
                // 供应商
                if (StringUtils.isBlank(dcReplenishmentListResponseDto.getPoSupplierId())) {
                    dcReplenishmentListResponseDto.setPoSupplierId(dcReplenishmentListResponseDto.getLastSupplierId());
                }
                // 业务员
                if (StringUtils.isNotBlank(dcReplenishmentListResponseDto.getPoSupplierId())) {
                    List<GaiaSupplierSalesman> gaiaSupplierSalesmanList1 = gaiaSupplierSalesmanMap.get(dcReplenishmentListResponseDto.getPoSupplierId());
                    if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(gaiaSupplierSalesmanList1)) {
                        dcReplenishmentListResponseDto.setSalesList(gaiaSupplierSalesmanList1);
                        GaiaSupplierSalesman gaiaSupplierSalesman = gaiaSupplierSalesmanList1.stream().filter(
                                s -> s.getGssDefault() != null && s.getGssDefault().intValue() == 1).findFirst().orElse(null);
                        if (gaiaSupplierSalesman != null) {
                            dcReplenishmentListResponseDto.setPoSupplierSalesman(gaiaSupplierSalesman.getGssCode());
                            dcReplenishmentListResponseDto.setPoSupplierSalesmanName(gaiaSupplierSalesman.getGssName());
                        }
                    }
                }
                // 昨日门店需求
                GaiaSdReplenishD gaiaSdReplenishD = gaiaSdReplenishDList.stream().filter(item -> item.getGsrdProId().equals(dcReplenishmentListResponseDto.getProSelfCode())).findFirst().orElse(null);
                if (gaiaSdReplenishD != null && StringUtils.isNotBlank(gaiaSdReplenishD.getGsrdNeedQty())) {
                    dcReplenishmentListResponseDto.setGsrdNeedQty(NumberUtils.toScaledBigDecimal(gaiaSdReplenishD.getGsrdNeedQty(), 4, RoundingMode.HALF_UP));
                }
                resultList.add(dcReplenishmentListResponseDto);
            }

            if (errorList.size() > 0) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            return ResultUtil.success(resultList);

        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException(ResultEnum.E0126);
        }

    }
}
