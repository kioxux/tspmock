package com.gov.purchase.module.base.service.impl.customerImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaCustomerBasic;
import com.gov.purchase.entity.GaiaCustomerBusiness;
import com.gov.purchase.entity.GaiaCustomerBusinessKey;
import com.gov.purchase.entity.GaiaStoreDataKey;
import com.gov.purchase.mapper.GaiaCustomerBasicMapper;
import com.gov.purchase.mapper.GaiaCustomerBusinessMapper;
import com.gov.purchase.module.base.dto.excel.Customer;
import com.gov.purchase.module.base.service.impl.ImportData;
import com.gov.purchase.utils.SpringUtil;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.21
 */
@Service
public class CustomerImportEditImpl extends CustomerImport {

    @Resource
    GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;

    @Resource
    GaiaCustomerBasicMapper gaiaCustomerBasicMapper;

    /**
     * 数据验证结果
     *
     * @param path 文件路径
     */
    @Override
    protected void checkEnd(String path) {
        // 生成导入日志
        createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.WAIT.getCode());
    }

    /**
     * 业务数据验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap) {
        return super.checkList((LinkedHashMap<Integer, Customer>) dataMap);
    }

    /**
     * 业务数据验证
     *
     * @param customer
     * @param errorList
     * @param key
     */
    protected void businessCheck(Customer customer, List<String> errorList, int key) {
        // 改修数据存在验证
        GaiaCustomerBusinessKey gaiaCustomerBusinessKey = new GaiaCustomerBusinessKey();
        BeanUtils.copyProperties(customer, gaiaCustomerBusinessKey);
        GaiaCustomerBusiness gaiaCustomerBusinessInfo = gaiaCustomerBusinessMapper.selectByPrimaryKey(gaiaCustomerBusinessKey);
        if (gaiaCustomerBusinessInfo == null) {
            errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
        }
    }

    /**
     * 主数据表验证
     *
     * @param customer
     * @param errorList
     * @param key
     */
    protected void baseCheck(Customer customer, List<String> errorList, int key) {
        GaiaCustomerBasic gaiaCustomerBasic = gaiaCustomerBasicMapper.selectByPrimaryKey(customer.getCusCode());
        if (gaiaCustomerBasic == null) {
            errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
        }
    }

    /**
     * 数据导入
     *
     * @param bulUpdateCode 批量操作编码
     * @param dataMap
     * @param <T>
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    protected <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> dataMap) {
        List<String> errorList = new ArrayList<>();
        // 批量导入
        for (Integer key : dataMap.keySet()) {
            // 行数据
            Customer customer = (Customer) dataMap.get(key);
            // 数据库对象
            GaiaCustomerBasic gaiaCustomerBasic = gaiaCustomerBasicMapper.selectByPrimaryKey(customer.getCusCode());
            if (gaiaCustomerBasic == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
                break;
            }
            SpringUtil.copyPropertiesIgnoreNull(customer, gaiaCustomerBasic);
            // 客户状态
            if (super.isClear(customer.getCusStatus())) {
                // 清空
                gaiaCustomerBasic.setCusStatus(null);
            } else if (super.isNotBlank(customer.getCusStatusValue())) {
                gaiaCustomerBasic.setCusStatus(customer.getCusStatusValue());
            }
            // 清空数据处理
            super.initNull(gaiaCustomerBasic);
            gaiaCustomerBasicMapper.updateByPrimaryKey(gaiaCustomerBasic);

            // 业务表数据存在判断
            if (!super.isNotBlank(customer.getClient())) {
                continue;
            }
            // 数据库对象
            GaiaCustomerBusinessKey gaiaCustomerBusinessKey = new GaiaCustomerBusinessKey();
            gaiaCustomerBusinessKey.setClient(customer.getClient());
            gaiaCustomerBusinessKey.setCusSite(customer.getCusSite());
            gaiaCustomerBusinessKey.setCusSelfCode(customer.getCusSelfCode());
            GaiaCustomerBusiness gaiaCustomerBusiness = gaiaCustomerBusinessMapper.selectByPrimaryKey(gaiaCustomerBusinessKey);
            if (gaiaCustomerBusiness == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
                break;
            }
            SpringUtil.copyPropertiesIgnoreNull(customer, gaiaCustomerBusiness);
            // 地点客户状态
            if (super.isClear(customer.getSiteCusStatus())) {
                // 清空
                gaiaCustomerBusiness.setCusStatus(null);
            } else if (super.isNotBlank(customer.getSiteCusStatusValue())) {
                gaiaCustomerBusiness.setCusStatus(customer.getSiteCusStatusValue());
            }
            // 禁止销售
            if (super.isClear(customer.getCusNoSale())) {
                // 清空
                gaiaCustomerBusiness.setCusNoSale(null);
            } else if (super.isNotBlank(customer.getCusNoSaleValue())) {
                gaiaCustomerBusiness.setCusNoSale(customer.getCusNoSaleValue());
            }
            // 禁止退货
            if (super.isClear(customer.getCusNoReturn())) {
                // 清空
                gaiaCustomerBusiness.setCusNoReturn(null);
            } else if (super.isNotBlank(customer.getCusNoReturnValue())) {
                gaiaCustomerBusiness.setCusNoReturn(customer.getCusNoReturnValue());
            }
            // 是否启用信用管理
            if (super.isClear(customer.getCusCreditFlag())) {
                // 清空
                gaiaCustomerBusiness.setCusCreditFlag(null);
            } else if (super.isNotBlank(customer.getCusCreditFlagValue())) {
                gaiaCustomerBusiness.setCusCreditFlag(customer.getCusCreditFlagValue());
            }
            // 信用检查点
            if (super.isClear(customer.getCusCreditCheck())) {
                // 清空
                gaiaCustomerBusiness.setCusCreditCheck(null);
            } else if (super.isNotBlank(customer.getCusCreditCheckValue())) {
                gaiaCustomerBusiness.setCusCreditCheck(customer.getCusCreditCheckValue());
            }
            // 清空数据处理
            super.initNull(gaiaCustomerBusiness);
            //信用额度
            if (!ImportData.NULLVALUE.equals(customer.getCusCreditQuota()) && !StringUtils.isBlank(customer.getCusCreditQuota())) {
                gaiaCustomerBusiness.setCusCreditQuota(new BigDecimal(customer.getCusCreditQuota()));
            }
            gaiaCustomerBusinessMapper.updateByPrimaryKey(gaiaCustomerBusiness);
        }

        // 没有错误
        if (CollectionUtils.isEmpty(errorList)) {
            // 更新导入日志
            editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.COMPLETE.getCode());
            return ResultUtil.success();
        }
        Result result = ResultUtil.error(ResultEnum.E0115);
        result.setData(errorList);
        return result;
    }
}
