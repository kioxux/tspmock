package com.gov.purchase.module.qa.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.qa.dto.*;

import java.util.List;

public interface PermitService {

    /**
     * 证照资质验证
     * @param list
     * @return
     */
    Result validPermitDataInfo(List<ValidPermitDataInfoRequestDto> list);

    /**
     * 证照资质提交
     * @param list
     * @return
     */
    Result savePermitDataInfo(List<ValidPermitDataInfoRequestDto> list);


    /**
     * 证照资质列表
     * @param dto
     * @return
     */
    PageInfo<QueryPermitDataListResponseDto> queryPermitDataList(QueryPermitDataListRequestDto dto);


    /**
     * 证照资质详情
     * @param dto
     * @return
     */
    QueryPermitDataInfoResponseDto getPermitDataInfo(QueryPermitDataInfoRequestDto dto);
}
