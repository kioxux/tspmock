package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "质量管控-批次状态列表返回结果")
public class QueryStockListResponseDto {

    @ApiModelProperty(value = "商品编号", name = "proCode")
    private String proCode;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    private String proSelfCode;

    @ApiModelProperty(value = "商品描述", name = "proDepict")
    private String proDepict;

    @ApiModelProperty(value = "商品名称", name = "proName")
    private String proName;

    @ApiModelProperty(value = "地点", name = "proSite")
    private String proSite;

    @ApiModelProperty(value = "地点名称", name = "siteName")
    private String siteName;

    @ApiModelProperty(value = "批次号", name = "batBatch")
    private String batBatch;

    @ApiModelProperty(value = "数量（非限制使用库存）", name = "batNormalQty")
    private BigDecimal batNormalQty;

    @ApiModelProperty(value = "库存状态", name = "status")
    private String status;

}
