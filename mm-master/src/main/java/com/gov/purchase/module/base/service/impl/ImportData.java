package com.gov.purchase.module.base.service.impl;

import com.gov.purchase.common.entity.Tencent;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaBatchUpdateLog;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaBatchUpdateLogMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.excel.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.EnumUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.30
 */
public abstract class ImportData {

    @Resource
    Tencent tencent;

    @Resource
    CosUtils cosUtils;

    @Resource
    GaiaBatchUpdateLogMapper gaiaBatchUpdateLogMapper;

    @Resource
    private FeignService feignService;


    /**
     * 文件名
     */
    protected String fileName;

    /**
     * 列名
     */
    private List<String> nameList;

    /**
     * 导入类型
     */
    protected CommonEnum.ImportType importType;

    /**
     * 操作对旬类
     */
    private Class<?> cs;

    /**
     * 清空
     */
    protected final static String NULLVALUE = "清空";

    /**
     * 初始数据加载
     */
    private void initClass() {
        // 商品
        if (CommonEnum.ImportType.CREATE_PRODUCT.equals(this.importType) || CommonEnum.ImportType.EDIT_PRODUCT.equals(this.importType)) {
            this.cs = Product.class;
            this.nameList = getNames();
        } else if (CommonEnum.ImportType.CREATE_SUPPLIER.equals(this.importType) || CommonEnum.ImportType.EDIT_SUPPLIER.equals(this.importType)) {
            // 供应商
            this.cs = Supplier.class;
            this.nameList = getNames();
        } else if (CommonEnum.ImportType.CREATE_STORE.equals(this.importType) || CommonEnum.ImportType.EDIT_STORE.equals(this.importType)) {
            // 门店
            this.cs = Store.class;
            this.nameList = getNames();
        } else if (CommonEnum.ImportType.CREATE_CUSTOMER.equals(this.importType) || CommonEnum.ImportType.EDIT_CUSTOMER.equals(this.importType)) {
            // 客户
            this.cs = Customer.class;
            this.nameList = getNames();
//        } else if (CommonEnum.ImportType.EXTENT_PRODUCT.equals(this.importType)) {
//            // 商品扩展属性
//            this.cs = Extend.class;
//            this.nameList = getNames();
        } else {
            throw new CustomResultException(ResultEnum.E0105);
        }
    }

    /**
     * 导入数据
     *
     * @param bulUpdateCode 导入主键
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Result importData(String bulUpdateCode) {
        // 批量操作日志数据
        GaiaBatchUpdateLog gaiaBatchUpdateLog = gaiaBatchUpdateLogMapper.selectByPrimaryKey(bulUpdateCode);
        if (gaiaBatchUpdateLog == null) {
            throw new CustomResultException(ResultEnum.E0124);
        }
        // 待提交验证
        if (!CommonEnum.BulUpdateStatus.WAIT.getCode().equals(gaiaBatchUpdateLog.getBulUpdateStatus())) {
            return ResultUtil.error(ResultEnum.E0127);
        }
        // 文件路径
        String path = gaiaBatchUpdateLog.getBulRouteAddress();
        // 导入类型
        String type = gaiaBatchUpdateLog.getBulUpdateType() + "_" + gaiaBatchUpdateLog.getBulDateType();
        // 业务不存在
        if (!EnumUtils.contains(type, CommonEnum.ImportType.class)) {
            return ResultUtil.error(ResultEnum.E0102);
        }
        // 导入类型
        this.importType = CommonEnum.ImportType.getImportType(type);
        // 数据初始化
        initClass();
        // excel文件
        Workbook workbook = getWorkbook(path);
        // 模板格式验证
        Result result = templateCheck(workbook);
        if (ResultUtil.hasError(result)) {
            // 更新导入日志
            editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.ERROR.getCode());
            return result;
        }
        // 获取数据
        LinkedHashMap<Integer, Object> map = dataList(workbook);
        // 数据为空
        if (map == null || map.isEmpty()) {
            // 更新导入日志
            editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.ERROR.getCode());
            return ResultUtil.error(ResultEnum.E0104);
        }
        // 文件基础数据验证
        result = dataFormatCheck(map);
        if (ResultUtil.hasError(result)) {
            // 错误 消息
            setError(workbook, result);
            Result uploadResult = cosUtils.uploadWorkbook(workbook, this.fileName);
            if (ResultUtil.hasError(uploadResult)) {
                return uploadResult;
            }
            // 更新导入日志
            editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.ERROR.getCode());
            return result;
        }
        // 业务验证
        result = businessValidate(map);
        if (ResultUtil.hasError(result)) {
            // 错误 消息
            setError(workbook, result);
            Result uploadResult = cosUtils.uploadWorkbook(workbook, this.fileName);
            if (ResultUtil.hasError(uploadResult)) {
                return uploadResult;
            }
            // 更新导入日志
            editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.ERROR.getCode());
            return result;
        }
        // 完成导入
        result = importEnd(bulUpdateCode, map);
        if (ResultUtil.hasError(result)) {
            // 错误 消息
            setError(workbook, result);
            Result uploadResult = cosUtils.uploadWorkbook(workbook, this.fileName);
            if (ResultUtil.hasError(uploadResult)) {
                return uploadResult;
            }
            // 更新导入日志
            editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.ERROR.getCode());
            return result;
        }

        return ResultUtil.success();
    }

    /**
     * 导入
     */
    @Transactional(rollbackFor = Exception.class)
    public Result checkData(String path, String type) {
        // 业务不存在
        if (!EnumUtils.contains(type, CommonEnum.ImportType.class)) {
            return ResultUtil.error(ResultEnum.E0102);
        }
        // 导入类型
        this.importType = CommonEnum.ImportType.getImportType(type);
        // 数据初始化
        initClass();
        // excel文件
        Workbook workbook = getWorkbook(path);
        // 模板格式验证
        Result result = templateCheck(workbook);
        if (ResultUtil.hasError(result)) {
            return result;
        }
        // 获取数据
        LinkedHashMap<Integer, Object> map = dataList(workbook);
        // 数据为空
        if (map == null || map.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0104);
        }
        // 文件基础数据验证
        result = dataFormatCheck(map);
        if (ResultUtil.hasError(result)) {
            // 错误 消息
            setError(workbook, result);
            Result uploadResult = cosUtils.uploadWorkbook(workbook, this.fileName);
            if (ResultUtil.hasError(uploadResult)) {
                return uploadResult;
            }
            // 生成导入日志
            createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.ERROR.getCode());
            return result;
        }
        // 业务验证
        result = businessValidate(map);
        if (ResultUtil.hasError(result)) {
            // 错误 消息
            setError(workbook, result);
            Result uploadResult = cosUtils.uploadWorkbook(workbook, this.fileName);
            if (ResultUtil.hasError(uploadResult)) {
                return uploadResult;
            }
            // 生成导入日志
            createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.ERROR.getCode());
            return result;
        }
        // 完成验证
        checkEnd(path);

        return ResultUtil.success();
    }

    /**
     * 模板验证
     *
     * @param workbook
     * @return
     */
    private Result templateCheck(Workbook workbook) {
        Sheet sheet = this.getSheet(workbook);
        // 导入数据标题
        Row row = sheet.getRow(0);
        if (row == null) {
            throw new CustomResultException(ResultEnum.E0104);
        }
        int cellCnt = row.getPhysicalNumberOfCells();
        // 模板不正确
        if (this.nameList.size() != cellCnt) {
            throw new CustomResultException(ResultEnum.E0105);
        }
        // 模板比较
        for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
            Cell cell = row.getCell(i);
            if (cell == null) {
                throw new CustomResultException(ResultEnum.E0105);
            }
            String name = row.getCell(i).toString();
            if (!this.nameList.contains(name)) {
                throw new CustomResultException(ResultEnum.E0105);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 数据格式验证
     *
     * @param dataMap 数据
     */
    private Result dataFormatCheck(LinkedHashMap<Integer, Object> dataMap) {
        List<String> errorList = new ArrayList<>();
        LinkedHashMap<Integer, Object> map = (LinkedHashMap<Integer, Object>) dataMap;
        // 数据格式验证
        for (Integer key : map.keySet()) {
            Object entiey = map.get(key);
            // 数据验证
            checkData(key + 1, entiey, errorList);
        }
        // 验证报错
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success();
    }

    /**
     * 导入对象列名
     *
     * @return
     */
    private List<String> getNames() {
        List<String> list = new ArrayList<>();
        Field[] declaredFields = this.cs.getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(ExcelValidate.class)) {
                ExcelValidate annotation = field.getAnnotation(ExcelValidate.class);
                list.add(annotation.name());
            }
        }
        return list;
    }

    /**
     * 根据属性名设置属性值
     *
     * @param fieldName
     * @param object
     * @return
     */
    private void setFieldValueByFieldName(String fieldName, Object object, String value) {
        try {
            // 获取obj类的字节文件对象
            Class c = object.getClass();
            // 获取该类的成员变量
            Field f = c.getDeclaredField(fieldName);
            // 取消语言访问检查
            f.setAccessible(true);
            // 给变量赋值
            f.set(object, value);
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.UNKNOWN_ERROR);
        }
    }

    /**
     * 数据验证
     *
     * @param rowIndex
     * @param entity
     */
    private void checkData(int rowIndex, Object entity, List<String> errorList) {
        Field[] declaredFields = this.cs.getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(ExcelValidate.class)) {
                ExcelValidate annotation = field.getAnnotation(ExcelValidate.class);
                // 属性
                String attr = field.getName();
                // 名称
                String name = annotation.name();
                // 新增必须
                boolean addRequired = annotation.addRequired();
                // 编辑必须
                boolean editRequired = annotation.editRequired();
                // 类型
                ExcelValidate.DataType dataType = annotation.type();
                // 最大长度
                int maxLength = annotation.maxLength();
                // 依赖的非空项目
                String[] relyRequired = annotation.relyRequired();
                // 值
                Object object = getFieldValueByName(attr, entity);

                // 新增 空 清空 均不可以
                if (this.importType.getBulUpdateType().equals(CommonEnum.BulUpdateType.CREATE.getCode()) ||
                        this.importType.getBulUpdateType().equals(CommonEnum.BulUpdateType.EXTENT.getCode())) {
                    if (addRequired) {
                        if (object == null || !this.isNotBlank(object.toString())) {
                            errorList.add("第" + rowIndex + "行：" + name + "不能为空");
                            continue;
                        }
                    }
                }
                // 编辑 不可以等于清空
                if (this.importType.getBulUpdateType().equals(CommonEnum.BulUpdateType.EDIT.getCode())) {
                    // 编辑项目不能为空
                    if (editRequired) {
                        if (object == null || StringUtils.isBlank(object.toString())) {
                            errorList.add("第" + rowIndex + "行：" + name + "不能为空");
                            continue;
                        }
                        if (object != null && ImportData.NULLVALUE.equals(object.toString())) {
                            errorList.add("第" + rowIndex + "行：" + name + "不能清空");
                            continue;
                        }
                    }
                    // 新增必填项目和关联项目不可以清空
                    if (addRequired) {
                        if (object != null && ImportData.NULLVALUE.equals(object.toString())) {
                            errorList.add("第" + rowIndex + "行：" + name + "不能清空");
                            continue;
                        }
                    }
                }
                // 清空当前值
                if (object != null && ImportData.NULLVALUE.equals(object.toString())) {
                    object = null;
                }
                // 字符串
                if (dataType.equals(ExcelValidate.DataType.STRING)) {
                    // 超长
                    if (maxLength != 0 && object != null && StringUtils.length(object.toString()) > maxLength) {
                        errorList.add("第" + rowIndex + "行：" + name + "长度不能超过" + maxLength + "位");
                        continue;
                    }
                }
                // 整数
                if (dataType.equals(ExcelValidate.DataType.INTEGER) || dataType.equals(ExcelValidate.DataType.DECIMAL)) {
                    // 最大值
                    double max = annotation.max();
                    // 最小值
                    double min = annotation.min();
                    // 格式验证 比较大小
                    if (object != null && StringUtils.isNotBlank(object.toString())) {
                        double value = 0;
                        // 数据格式验证
                        if (dataType.equals(ExcelValidate.DataType.INTEGER)) {
                            try {
                                String reg = "^[0-9]+(.[0-9]+)?$";
                                if (!object.toString().matches(reg)) {
                                    errorList.add("第" + rowIndex + "行：" + name + "为整数格式");
                                    continue;
                                }
                                if (!object.toString().matches("^\\d+$$")) {
                                    errorList.add("第" + rowIndex + "行：" + name + "为整数格式");
                                    continue;
                                }
                                value = NumberUtils.toLong(object.toString());
                            } catch (Exception e) {
                                errorList.add("第" + rowIndex + "行：" + name + "为整数格式");
                                continue;
                            }
                        }
                        if (dataType.equals(ExcelValidate.DataType.DECIMAL)) {
                            try {
                                String reg = "^[0-9]+(.[0-9]+)?$";
                                if (!object.toString().matches(reg)) {
                                    errorList.add("第" + rowIndex + "行：" + name + "为数字格式");
                                    continue;
                                }
                                value = Double.parseDouble(object.toString());
                            } catch (Exception e) {
                                errorList.add("第" + rowIndex + "行：" + name + "为数字格式");
                                continue;
                            }
                        }
                        DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
                        // 比较大小
                        if (value < min || value > max) {
                            errorList.add("第" + rowIndex + "行：" + name + "必须为" + decimalFormat.format(min)
                                    + "到" + decimalFormat.format(max) + "之间");
                            continue;
                        }
                        setFieldValueByFieldName(attr, entity, decimalFormat.format(value));
                    }
                }
                // 日期
                if (dataType.equals(ExcelValidate.DataType.DATE)) {
                    // 格式化
                    String dateFormat = annotation.dateFormat();
                    // 格式验证
                    if (object != null && StringUtils.isNotBlank(object.toString())) {
                        String value = object.toString();
                        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
                        try {
                            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
                            format.setLenient(false);
                            format.parse(value);
                        } catch (Exception e) {
                            errorList.add("第" + rowIndex + "行：" + name + "应为日期格式");
                            continue;
                        }
                    }
                }
                // 关联非空判断
                if (this.importType.getBulUpdateType().equals(CommonEnum.BulUpdateType.CREATE.getCode()) && relyRequired != null && relyRequired.length > 0) {
                    // 遍历项目非空判断
                    for (String relyItem : relyRequired) {
                        if (StringUtils.isNotBlank(relyItem)) {
                            // 关联属性值
                            Object relyObject = getFieldValueByName(relyItem, entity);
                            // 关联属性有值
                            if (relyObject != null && this.isNotBlank(relyObject.toString())) {
                                // 当前项目为空
                                if (object == null || StringUtils.isBlank(object.toString())) {
                                    errorList.add("第" + rowIndex + "行：" + name + "不能为空");
                                    continue;
                                }
                            }
                        }
                    }
                }
                // 存在数据
                if (object != null && StringUtils.isNotBlank(object.toString())) {
                    // 静态字典数据验证
                    CommonEnum.DictionaryStaticData dictionaryStaticData = annotation.dictionaryStaticData();
                    // 静态字典比较字段
                    ExcelValidate.DictionaryStaticType dictionaryStaticType = annotation.dictionaryStaticType();
                    if (dictionaryStaticData != null && dictionaryStaticType != null && !dictionaryStaticData.equals(CommonEnum.DictionaryStaticData.DEFAULT)) {
                        Dictionary dictionary = null;
                        if (dictionaryStaticType.equals(ExcelValidate.DictionaryStaticType.VALUE)) {
                            dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, object.toString());
                        }
                        if (dictionaryStaticType.equals(ExcelValidate.DictionaryStaticType.LABEL)) {
                            dictionary = CommonEnum.DictionaryStaticData.getDictionaryByLabel(dictionaryStaticData, object.toString());
                            // 字典数据存在
                            if (dictionary != null) {
                                // 值字段
                                String dictionaryStaticField = annotation.dictionaryStaticField();
                                if (StringUtils.isNotBlank(dictionaryStaticField)) {
                                    setFieldValueByFieldName(dictionaryStaticField, entity, dictionary.getValue());
                                }
                            }
                        }
                        if (dictionary == null) {
                            errorList.add("第" + rowIndex + "行：请填写正确的" + name);
                            continue;
                        }
                    }
                }
            }
        }
    }

    /**
     * 对象属性值
     *
     * @param fieldName 属性名
     * @param o         对象
     * @return 返回值
     */
    private Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[]{});
            Object value = method.invoke(o, new Object[]{});
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 验证完成
     *
     * @return
     */
    protected abstract void checkEnd(String path);

    /**
     * 导入完成
     *
     * @param bulUpdateCode
     * @param map
     */
    protected abstract <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> map);

    /**
     * 数据业务验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    protected abstract <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap);

    /**
     * 生成导入日志
     *
     * @param importType
     * @param path
     */
    protected void createBatchLog(CommonEnum.ImportType importType, String path, String status) {
        try {
            TokenUser user = feignService.getLoginInfo();

            GaiaBatchUpdateLog gaiaBatchUpdateLog = new GaiaBatchUpdateLog();
            // 批量操作编码
            gaiaBatchUpdateLog.setBulUpdateCode(gaiaBatchUpdateLogMapper.selectBulUpdateCode());
            // 数据类型
            gaiaBatchUpdateLog.setBulDateType(importType.getBulDateType());
            // 操作类型
            gaiaBatchUpdateLog.setBulUpdateType(importType.getBulUpdateType());
            // 操作状态
            gaiaBatchUpdateLog.setBulUpdateStatus(status);
            // 文件地址
            gaiaBatchUpdateLog.setBulRouteAddress(path);
            // 创建人
            gaiaBatchUpdateLog.setBulCreateBy(user.getUserId());
            // 创建日期
            gaiaBatchUpdateLog.setBulCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            // 创建时间
            gaiaBatchUpdateLog.setBulCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            gaiaBatchUpdateLogMapper.insertSelective(gaiaBatchUpdateLog);
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0123);
        }
    }

    /**
     * 更新导入日志
     *
     * @param bulUpdateCode
     * @param status
     */
    protected void editBatchLog(String bulUpdateCode, String status) {
        try {
            // 导入日志
            GaiaBatchUpdateLog gaiaBatchUpdateLog = gaiaBatchUpdateLogMapper.selectByPrimaryKey(bulUpdateCode);
            gaiaBatchUpdateLog.setBulUpdateStatus(status);
            gaiaBatchUpdateLogMapper.updateByPrimaryKeySelective(gaiaBatchUpdateLog);
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0124);
        }
    }

    /**
     * excel 工作表
     *
     * @param workbook
     * @return
     */
    private Sheet getSheet(Workbook workbook) {
        // 对象为空
        if (workbook == null) {
            throw new CustomResultException(ResultEnum.E0105);
        }
        // 获取第一个张表
        Sheet sheet = workbook.getSheetAt(0);
        // 对象为空
        if (sheet == null) {
            throw new CustomResultException(ResultEnum.E0104);
        }
        // 无行数据
        if (sheet.getLastRowNum() <= 0) {
            throw new CustomResultException(ResultEnum.E0104);
        }
        return sheet;
    }

    /**
     * 文件转为数据
     *
     * @return
     */
    private <T> LinkedHashMap<Integer, T> dataList(Workbook workbook) {
        Sheet sheet = this.getSheet(workbook);
        LinkedHashMap<Integer, Object> map = new LinkedHashMap<Integer, Object>();
        // 遍历excel表格
        for (int i = 2; i < sheet.getLastRowNum() + 1; i++) {
            // 行
            Row row = sheet.getRow(i);
            // 当前行为空，继续下一行
            if (row == null) {
                continue;
            }
            Object entity = getObject(row);
            // Excel取出数据
            if (entity != null) {
                map.put(i, entity);
            }
        }
        return (LinkedHashMap<Integer, T>) map;
    }

    /**
     * 数据返回
     *
     * @param row
     * @param <T>
     * @return
     */
    private <T> T getObject(Row row) {
        Object entiey = null;
        try {
            entiey = this.cs.newInstance();
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0123);
        }
        boolean flg = false;
        // 每列均无数据
        for (int i = 0; i < this.nameList.size(); i++) {
            Cell cell = row.getCell(i);
            String value = getCellValue(cell);
            if (this.isNotBlank(value)) {
                flg = true;
                break;
            }
        }
        if (!flg) {
            return null;
        }
        // 取值
        for (int i = 0; i < this.nameList.size(); i++) {
            Cell cell = row.getCell(i);
            String value = getCellValue(cell);
            if (StringUtils.isNotBlank(value)) {
                Field[] declaredFields = this.cs.getDeclaredFields();
                for (Field field : declaredFields) {
                    if (field.isAnnotationPresent(ExcelValidate.class)) {
                        ExcelValidate annotation = field.getAnnotation(ExcelValidate.class);
                        // 属性
                        String attr = field.getName();
                        // 列
                        int index = annotation.index();
                        if (index == i) {
                            this.setFieldValueByFieldName(attr, entiey, value);
                            break;
                        }
                    }
                }
            }
        }
        return (T) entiey;
    }

    /**
     * 单元格取值
     *
     * @param cell
     * @return
     */
    private String getCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        try {
            if ("yyyy/mm;@".equals(cell.getCellStyle().getDataFormatString()) || "m/d/yy".equals(cell.getCellStyle().getDataFormatString())
                    || "yy/m/d".equals(cell.getCellStyle().getDataFormatString()) || "mm/dd/yy".equals(cell.getCellStyle().getDataFormatString())
                    || "dd-mmm-yy".equals(cell.getCellStyle().getDataFormatString()) || "yyyy/m/d".equals(cell.getCellStyle().getDataFormatString())) {
                return new SimpleDateFormat("yyyyMMdd").format(cell.getDateCellValue());
            }
        } catch (Exception e) {
            return cell.toString().trim();
        }
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            return cell.toString().trim();
        }
        return cell.toString().trim();
    }

    /**
     * 根据url取excel
     *
     * @param path
     * @return
     * @throws Exception
     */
    private Workbook getWorkbook(String path) {
        try {
            // 文件全路径
            String url = cosUtils.urlAuth(path);
            String type = url.substring(url.lastIndexOf(".") + 1, url.indexOf("?"));
            Workbook wb;
            //根据文件后缀（xls/xlsx）进行判断
            InputStream input = new URL(url).openStream();
            if ("xls".equals(type)) {
                //文件流对象
                wb = new HSSFWorkbook(input);
            } else if ("xlsx".equals(type)) {
                wb = new XSSFWorkbook(input);
            } else {
                throw new CustomResultException(ResultEnum.E0103);
            }
            this.fileName = url.substring(url.lastIndexOf("/") + 1);
            return wb;
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0103);
        }
    }

    /**
     * 错误消息写入
     *
     * @param workbook
     * @param result
     */
    private void setError(Workbook workbook, Result result) {
        // 获取sheet
        Sheet sheet = this.getSheet(workbook);
        // 最后一行
        int index = sheet.getLastRowNum() + 1;
        List<String> list = (List<String>) result.getData();
        // 遍历写入错误 消息
        for (int i = 0; i < list.size(); i++) {
            // 错误消息
            String item = list.get(i);
            // 取行
            Row row = sheet.getRow(index + i);
            if (row == null) {
                row = sheet.createRow(index + i);
            }
            // 第一个单元格
            Cell cell = row.getCell(1);
            if (cell == null) {
                cell = row.createCell(1);
            }
            CellStyle cellStyle = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setColor(HSSFColor.RED.index);
            cellStyle.setFont(font);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(item);
        }
    }

    /**
     * 判断对象属性不为空
     *
     * @param value
     * @return
     */
    protected boolean isNotBlank(String value) {
        // 不为空 返回 TRUE
        boolean flg = StringUtils.isNotBlank(value);
        if (!flg) {
            return flg;
        }
        // 值为清空
        if (ImportData.NULLVALUE.equals(value)) {
            return false;
        }
        return true;
    }

    /**
     * 是否清空数据
     *
     * @param value
     * @return
     */
    protected boolean isClear(String value) {
        if (ImportData.NULLVALUE.equals(value)) {
            return true;
        }
        return false;
    }

    /**
     * 值为清空属性设置null
     *
     * @param object
     */
    protected void initNull(Object object) {
        // 对象属性
        Field[] field = object.getClass().getDeclaredFields();
        //遍历所有属性
        for (int j = 0; j < field.length; j++) {
            try {
                //获取属性的名字
                String fieldName = field[j].getName();
                String name = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
                Method m = object.getClass().getMethod("get" + name);
                Object returnResult = m.invoke(object);
                if (returnResult != null) {
                    String typeName = returnResult.getClass().getTypeName();
                    String value = "";
                    if (typeName.equals("java.math.BigDecimal")) {
                        value = returnResult.toString();
                    } else {
                        value = (String) returnResult;
                    }
                    if (ImportData.NULLVALUE.equals(value)) {
                        setFieldValueByFieldName(fieldName, object, null);
                    }
                }
            } catch (Exception e) {
                throw new CustomResultException(ResultEnum.E0126);
            }
        }
    }

}

