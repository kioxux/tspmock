package com.gov.purchase.module.supplier.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetSupBasicListRequestDTO {


    /**
     * 连锁总部id
     */
    private String compadmId;
    /**
     * 地点
     */
    private String proSite;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 统一社会信用代码
     */
    private String supCreditCode;



    /**
     * 当前行供应商自编码
     */
    private String currentRowSupSeflCode;

    /**
     * 最后一行供应商自编码
     */
    private String lastRowSupSeflCode;

}
