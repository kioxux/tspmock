package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ListTaxpayerResponseDto {

    /**
     * 纳税主体编码
     */
    private String ficoTaxSubjectCode;

    /**
     * 纳税主体名称
     */
    private String ficoTaxSubjectName;

}
