package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.entity.GaiaDcReplenishExclud;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.10
 */
@Data
public class GaiaDcReplenishExcludDto extends GaiaDcReplenishExclud {

    /**
     * 分类 名称
     */
    private String className;

    /**
     * 商品描述
     */
    private String proDepict;

    /**
     * 商品规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;
}

