package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.module.base.dto.Dictionary;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.07
 */
@Data
public class SalesOrderItem {
    /**
     * 加盟商
     */
    private String client;
    /**
     * 订单号
     */
    private String soId;
    /**
     * 订单行号
     */
    private String soLineNo;
    /**
     * 商品编号
     */
    private String soProCode;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 生产厂家
     */
    private String proFactoryName;
    /**
     * 订单数量
     */
    private BigDecimal soQty;
    /**
     * 单位
     */
    private String soUnit;
    /**
     * 单位名
     */
    private String unitName;
    /**
     * 订单单价
     */
    private BigDecimal soPrice;
    /**
     * 订单行总价
     */
    private BigDecimal soLineAmt;
    /**
     * 地点
     */
    private String soSiteCode;
    /**
     * 地点名称
     */
    private String siteName;
    /**
     * 库位地点
     */
    private String soLocationCode;
    /**
     * 批次号
     */
    private String soBatch;
    /**
     * 税率
     */
    private String soRate;
    /**
     * 税率值
     */
    private String soRateValue;
    /**
     * 计划发货日期
     */
    private String soDeliveryDate;
    /**
     * 行备注
     */
    private String soLineRemark;
    /**
     * 删除标记
     */
    private String soLineDelete;
    /**
     * 交货已完成标记
     */
    private String soCompleteFlag;

    /**
     * 库存
     */
    private BigDecimal stockNum;

    /**
     * 商品描述
     */
    private String proDepict;
    /**
     * 批准文号
     */
    private String proRegisterNo;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 生产批号
     */
    private String soBatchNo;
    /**
     * 有效期
     */
    private String batExpiryDate;
    /**
     * 生产批号
     */
    private String batBatchNo;
    /**
     * 生产日期
     */
    private String batProductDate;
    /**
     * 零售价
     */
    private BigDecimal proLsj;
    /**
     * 预设售价1
     */
    private BigDecimal proYsj1;
    /**
     * 预设售价2
     */
    private BigDecimal proYsj2;
    /**
     * 预设售价3
     */
    private BigDecimal proYsj3;
    /**
     * 自定义字段1
     */
    private String proZdy1;
    /**
     * 自定义字段2
     */
    private String proZdy2;
    /**
     * 自定义字段3
     */
    private String proZdy3;
    /**
     * 自定义字段4
     */
    private String proZdy4;
    /**
     * 自定义字段5
     */
    private String proZdy5;
    /**
     * 客户id
     */
    private String soCustomerId;
    /**
     * 上次开票价
     */
    private BigDecimal wmCkj;

    /**
     * 审批人Id
     */
    private String soApproveBy;
    /**
     * 审批日期
     */
    private String soApproveDate;
    /**
     * 审批人姓名
     */
    private String soApproveName;

    /**
     * 上次开票数量
     */
    private BigDecimal wmGzsl;

    /**
     * 上次开票时间
     */
    private String wmCjrq;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    List<Dictionary> dictionaryList;

    /**
     * 最近进价
     */
    private BigDecimal batPoPrice;

    /**
     * 会员价
     */
    private BigDecimal proHyj;

    /**
     * 国批价
     */
    private BigDecimal proGpj;

    /**
     * 国零价
     */
    private BigDecimal proGlj;

    /**
     * 库存地点
     */
    private String batLocationCode;

}

