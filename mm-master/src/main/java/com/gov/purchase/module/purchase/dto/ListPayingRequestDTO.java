package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ListPayingRequestDTO{


    @NotBlank(message = "组织类型不能为空，0:单体,1:连锁,2:批发")
    private String ficoCompanyCode;

    @NotBlank(message = "付款方式 (0:月/ 1:季/ 2:半年/ 3:年)")
    private String ficoPaymentMethod;

    /**
     * 付款主体编码
     */
    private String ficoPayingstoreCode;

    /**
     * 付款主体名称
     */
    private String ficoPayingstoreName;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单体门店类型
     */
    private String[] stoTypeList;

    /**
     * 配送中心
     */
    private String dcType;

}
