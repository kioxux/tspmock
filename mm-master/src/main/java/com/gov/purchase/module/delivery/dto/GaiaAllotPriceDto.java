package com.gov.purchase.module.delivery.dto;

import com.gov.purchase.entity.GaiaAllotPrice;
import com.gov.purchase.module.purchase.dto.GetProductListResponseDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.01.06
 */
@Data
public class GaiaAllotPriceDto extends GaiaAllotPrice {
    /**
     * 商品gqge
     */
    private List<GetProductListResponseDto> goodsList;

    //商品描述
    private String proDepict;
    //规格
    private String proSpecs;
    //单位
    private String proUnit;
    //厂家
    private String proFactName;
    //地点名称
    private String siteName;
    //零售价
    private BigDecimal priceNormal;
    //仓库成本价
    private BigDecimal costPrice;
}
