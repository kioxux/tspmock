package com.gov.purchase.module.store.dto.store;

import com.gov.purchase.common.entity.Pageable;
import lombok.Data;

@Data
public class StoreMinQtySearchDto extends Pageable {

    private String client;

    /**
     * 门店编号
     */
    private String gpmdBrId;

    /**
     * 商品编号
     */
    private String gpmdProId;

    /**
     * 商品分类
     */
    private String  proCompClass;
    
    
}
