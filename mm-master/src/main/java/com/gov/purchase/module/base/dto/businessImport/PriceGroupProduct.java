package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * @author tl
 */
@Data
public class PriceGroupProduct {
    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String prcProId;

    /**
     * 调价零售价
     */
    @ExcelValidate(index = 1, name = "调后售价", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String prcPriceNormal;

    /**
     * 调价会员价
     */
    @ExcelValidate(index = 2, name = "调后会员价", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String prcPriceHy;

    /**
     * 调价会员日价
     */
    @ExcelValidate(index = 3, name = "调后会员日价", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String prcPriceHyr;

}
