package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaChajiaZ;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description: 采购入库差价单功能
 * @author: yzf
 * @create: 2021-12-13 10:21
 */
@Data
public class PurchaseReceiptPriceDTO extends GaiaChajiaZ {

    private String cjDateStart;

    private String cjDateEnd;

    /**
     * 差价单明细列表
     */
    List<PurchaseReceiptPriceDetailVO> details;

    private String type;

    private Integer pageSize;

    private Integer pageNum;
}
