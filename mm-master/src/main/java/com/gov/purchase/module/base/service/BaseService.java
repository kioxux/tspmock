package com.gov.purchase.module.base.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.*;
import com.gov.purchase.module.base.dto.GaiaFranchiseeResponseDto;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.26
 */
public interface BaseService {

    /**
     * 加盟商列表
     *
     * @return
     */
    List<GaiaFranchiseeResponseDto> getFranchiseeList();

    /**
     * 实体集合
     *
     * @param client 加盟商
     * @param type   类型 1:门店 2:供应商 3:客户
     * @return
     */
    Result mainStore(String client, String type);

    /**
     * 配送中心集合
     *
     * @param client
     * @return
     */
    Result getDcDataList(String client);

    /**
     * 委托配送公司列表
     *
     * @return
     */
    Result getDeliveryCompanyList();

    /**
     * dc补货集合
     *
     * @return
     */
    Result getReplenishDcDataList();

    /**
     * 连锁公司总部列表
     *
     * @param client 加盟商
     * @return
     */
    Result getCompadmList(String client);

    /**
     * 连锁公司总部列表 需要排除已使用的连锁公司
     *
     * @param client 加盟商
     * @return
     */
    Result getCompadmListDC(String client, String dcCode);

    /**
     * 获取当前加盟商用户
     *
     * @return
     */
    Result getUserListByClient();

    Result getCompadmAndCompadmWms(String client, String dcCode);

    /**
     * 所有加盟商列表
     *
     * @return
     */
    List<GaiaFranchiseeResponseDto> getAllFranchiseeList();

    /**
     * 销售级别设置
     *
     * @param client
     * @param proSite
     * @param proSelfCode
     */
    void setProProfit(String client, String proSite, String proSelfCode,
                      List<GaiaTaxCode> gaiaTaxCodeList, List<GaiaSdProductPrice> gaiaSdProductPriceList,
                      List<GaiaMaterialAssess> gaiaMaterialAssessList, List<GaiaProductBusiness> gaiaProductBusiness,
                      List<GaiaStoreData> gaiaStoreDataList, List<GaiaProfitLevel>  gaiaProfitLevelList);

    /**
     * 配送中心列表_权限
     *
     * @param client
     * @return
     */
    Result getAuthDcDataList(String client);

    /**
     * @param client                  加盟商
     * @param proSite                 地点
     * @param userId                  用户ID
     * @param commodityCode           商品编码
     * @param originalCargoLocationNo 原货位
     * @param cargoLocationNo         固定货位号
     * @return
     */
    void updateProFixBin(String client, String proSite, String userId,
                           String commodityCode, String originalCargoLocationNo, String cargoLocationNo);

    List<GaiaProductClass> selectGaiaProductClass();
}
