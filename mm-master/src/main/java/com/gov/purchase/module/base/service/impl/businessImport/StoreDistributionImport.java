package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaBatchStockMapper;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.businessImport.StoreDistribution;
import com.gov.purchase.module.base.dto.businessImport.StoreDistributionRequestDto;
import com.gov.purchase.module.base.dto.businessImport.StoreDistributionResp;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.replenishment.dto.StoreRequestDto;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

@Service
public class StoreDistributionImport extends BusinessImport {

    /**
     * 加盟商key
     */
    private final static String CLIENT = "client";
    /**
     * 加盟商key
     */
    private final static String DC_CODE = "dcCode";

    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;

    @Resource
    private GaiaBatchStockMapper gaiaBatchStockMapper;


    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        List<String> errorList = new ArrayList<>();
        // 加盟商
        Object client = field.get(StoreDistributionImport.CLIENT);
        if (client == null || StringUtils.isBlank(client.toString())) {
            throw new CustomResultException(ResultEnum.E0140);
        }
        // 配送中心
        Object dcCode = field.get(StoreDistributionImport.DC_CODE);
        if (dcCode == null || StringUtils.isBlank(dcCode.toString())) {
            throw new CustomResultException(ResultEnum.E0151);
        }

        try {
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(client.toString());
            gaiaDcDataKey.setDcCode(dcCode.toString());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            if (gaiaDcData == null) {
                throw new CustomResultException(ResultEnum.E0151);
            }

            StoreRequestDto storeRequestDto = new StoreRequestDto();
            storeRequestDto.setClient(client.toString());
            storeRequestDto.setStoChainHead(dcCode.toString());

            List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.getStoreListByDc(storeRequestDto);

            // 将门店名称暂时放在map中，便于整合数据
            Map<String, String> stoMap = new HashMap<>();

            // 验证excel  数据是否正确
            for (Integer key : map.keySet()) {
                StoreDistribution storeDistribution = (StoreDistribution) map.get(key);

                List<GaiaProductBusinessKey> gaiaProductBusinessKeyList = new ArrayList<>();
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                gaiaProductBusinessKey.setClient(client.toString());
                gaiaProductBusinessKey.setProSite(storeDistribution.getStoNeedStore());
                gaiaProductBusinessKey.setProSelfCode(storeDistribution.getStoNeedPro());
                gaiaProductBusinessKeyList.add(gaiaProductBusinessKey);
                List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectByPrimaryByBuyunxupinlei(client.toString(), gaiaProductBusinessKeyList);
                if (CollectionUtils.isNotEmpty(gaiaProductBusinessList)) {
                    errorList.add(MessageFormat.format("第[{0}]行,商品{1}对于门店{2}为不允许铺货的品类，不可铺货至门店。",
                            key, storeDistribution.getStoNeedPro(), storeDistribution.getStoNeedStore()));
                    continue;
                }

                // 门店编码
                //   1. 是否存在
                //   2. 门店是否属于配送中心

                // 门店主键对象
                GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                // 加盟商
                gaiaStoreDataKey.setClient(client.toString());
                // 门店编码
                gaiaStoreDataKey.setStoCode(storeDistribution.getStoNeedStore());
                // 门店获取
                GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                // 门店号验证
                if (gaiaStoreData == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "门店编码"));
                    continue;
                } else if (!gaiaStoreData.getStoStatus().equals("0")) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "门店编码"));
                    continue;
                }
                stoMap.put(storeDistribution.getStoNeedStore(), gaiaStoreData.getStoName());

                //店是否属于配送中心
                boolean bool = storeDataList.stream().anyMatch(t -> t.getStoCode().equals(storeDistribution.getStoNeedStore()));
                if (!bool) {
                    errorList.add(MessageFormat.format(ResultEnum.E0153.getMsg(), key + 1));
                    continue;
                }

                // 验证商品存在性
                GaiaProductBusiness gaiaProductBusiness =
                        gaiaProductBusinessMapper.getProductBusiness(client.toString(), storeDistribution.getStoNeedStore(), storeDistribution.getStoNeedPro());
                if (gaiaProductBusiness == null) {
                    // 第几行商品不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "商品编码"));
                    continue;
                } else if (!gaiaProductBusiness.getProStatus().equals("0")) {
                    // 第几行商品不存在
//                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "商品编码"));
                }

                // 铺货日期 大于等于当前时间
                if (DateUtils.getCurrentLocalDate().compareTo(DateUtils.parseLocalDate(storeDistribution.getStoNeedArrivalDate(), "yyyyMMdd")) > 0) {
                    errorList.add(MessageFormat.format(ResultEnum.E0152.getMsg(), key + 1));
                    continue;
                }
            }

            if (errorList.size() > 0) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }

            List<StoreDistributionResp> storeDistributionResp = new ArrayList<>();
            for (Integer key : map.keySet()) {
                StoreDistribution storeDistribution = (StoreDistribution) map.get(key);
                // 验证商品存在性
                GaiaProductBusiness gaiaProductBusiness =
                        gaiaProductBusinessMapper.getProductBusiness(client.toString(), storeDistribution.getStoNeedStore(), storeDistribution.getStoNeedPro());
                StoreDistributionRequestDto batchStockRequestDto = new StoreDistributionRequestDto();
                batchStockRequestDto.setClient(client.toString());
                batchStockRequestDto.setProSelfCode(storeDistribution.getStoNeedPro());
                batchStockRequestDto.setSiteCode(storeDistribution.getStoNeedStore());
                batchStockRequestDto.setStoChainHead(dcCode.toString());
                StoreDistributionResp batchStockListResponseDto = new StoreDistributionResp();
                StoreDistributionResp batchStockListResponseDto1 = gaiaBatchStockMapper.selectStoreDistribution(batchStockRequestDto);
                if (batchStockListResponseDto1 != null) {
                    BeanUtils.copyProperties(batchStockListResponseDto1, batchStockListResponseDto);
                }
                batchStockListResponseDto.setStoNeedQty(new BigDecimal(storeDistribution.getStoNeedQty())); // 铺货数量
                batchStockListResponseDto.setStoCode(storeDistribution.getStoNeedStore());   // 门店编号
                batchStockListResponseDto.setStoName(stoMap.get(storeDistribution.getStoNeedStore()));  // 门店名称
                batchStockListResponseDto.setStoNeedBatch(storeDistribution.getStoNeedBatch());  // 批次
                batchStockListResponseDto.setStoNeedArrivalDate(storeDistribution.getStoNeedArrivalDate()); // 铺货日期
                batchStockListResponseDto.setStoNeedRemark(storeDistribution.getStoNeedRemark()); //备注
                batchStockListResponseDto.setProCode(gaiaProductBusiness.getProCode());
                // 商品自编码
                if (StringUtils.isBlank(batchStockListResponseDto.getBatProCode())) {
                    batchStockListResponseDto.setBatProCode(storeDistribution.getStoNeedPro());
                }
                // 地点
                batchStockListResponseDto.setBatSiteCode(dcCode.toString());
                storeDistributionResp.add(batchStockListResponseDto);
            }
            return ResultUtil.success(storeDistributionResp);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException(ResultEnum.E0126);
        }
    }
}
