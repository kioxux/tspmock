package com.gov.purchase.module.replenishment.dto;

import lombok.Data;

import java.util.List;

/**
 * @description: 上品自定义列表
 * @author: yzf
 * @create: 2022-01-11 15:14
 */
@Data
public class ProZdyDTO {
    private List<String> zdy1;
    private List<String> zdy2;
    private List<String> zdy3;
    private List<String> zdy4;
    private List<String> zdy5;
}
