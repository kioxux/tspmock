package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import java.util.List;

@Data
public class InvoicePayingDTO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 门店编码
     */
    private String ficoStoreCode;

    /**
     * 组织
     */
    private String ficoCompanyCode;

    /**
     * 门店名称
     */
    private String ficoStoreName;

    /**
     * 发票类型
     */
    private String ficoInvoiceType;

    /**
     * 统一社会信用代码
     */
    private String ficoSocialCreditCode;

    /**
     * 开户行名称
     */
    private String ficoBankName;

    /**
     * 开户行账号
     */
    private String ficoBankNumber;

    /**
     * 地址
     */
    private String ficoCompanyAddress;

    /**
     * 电话号码
     */
    private String ficoCompanyPhoneNumber;

    /**
     * 付款银行名称
     */
    private String ficoPayingbankName;

    /**
     * 付款银行账号
     */
    private String ficoPayingbankNumber;

    /**
     * 纳税主体  code:name
     */
    private String ficoTax;

    /**
     * 纳税主体编码
     */
    private String ficoTaxSubjectName;

    /**
     * 纳税主体名
     */
    private String ficoTaxSubjectCode;

    /**
     * 纳税属性
     */
    private Integer ficoTaxAttributes;

    /**
     * 纳税主体
     */
    private List<InvoicePayingTaxDTO> taxDetails;
}
