package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;
/**
 * @author : Yzf
 * description: 采购计划查询
 * create time: 2021/12/2 11:36
 * @params
 * @return
 */

@Data
@ApiModel(value = "采购计划查询")
public class PurchasePlanReviewDto{

    /**
     * 加盟商
     */
    private String client;

    /**
     * 采购主体（地点）
     */
    private String poCompanyCode;

    /**
     * 采购员（订单创建人）
     */
    private String poCreateBy;

    /**
     * 采购订单号
     */
    private String poId;

    /**
     * 订单日期开始
     */
    private String poDateStart;

    /**
     * 订单日期结束
     */
    private String poDateEnd;

    /**
     * 供应商
     */
    private String poSupplierId;

    /**
     * 审批状态
     */
    private String poApproveStatus;

    /**
     * 审批人
     */
    private String poApproveBy;

    /**
     * 审批日期
     */
    private String poApproveDate;

    private Integer pageSize;

    private Integer pageNum;

}