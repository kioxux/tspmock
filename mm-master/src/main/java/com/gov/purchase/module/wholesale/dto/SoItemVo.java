package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaSoItem;
import lombok.Data;

/**
 * description: TODO
 * create time: 2021/11/24 9:40
 * @author Yzf
 */
@Data
public class SoItemVo extends GaiaSoItem {

    /**
     * 退货品项
     */
    private String proCount;
}

