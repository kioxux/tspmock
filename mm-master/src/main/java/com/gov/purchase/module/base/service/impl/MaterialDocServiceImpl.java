package com.gov.purchase.module.base.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.MatTypeEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.BusinessException;
import com.gov.purchase.exception.FeignResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.wholesale.dto.GaiaBillOfApVO;
import com.gov.purchase.module.wholesale.dto.GaiaBillOfArVO;
import com.gov.purchase.module.base.dto.feign.MaterialDocRequestDto;
import com.gov.purchase.module.base.service.BaseService;
import com.gov.purchase.module.base.service.MaterialDocService;
import com.gov.purchase.module.wholesale.dto.GaiaFiArBalance;
import com.gov.purchase.module.wholesale.dto.SupplierDealOutData;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.JsonUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MaterialDocServiceImpl implements MaterialDocService {

    @Resource
    private GaiaMaterialAssessMapper gaiaMaterialAssessMapper;
    @Resource
    private GaiaPoItemMapper gaiaPoItemMapper;
    @Resource
    private GaiaTaxCodeMapper gaiaTaxCodeMapper;
    @Resource
    private GaiaMaterialDocMapper gaiaMaterialDocMapper;
    @Resource
    private GaiaBatchInfoMapper gaiaBatchInfoMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;
    @Resource
    private GaiaWmsDiaoboMMapper gaiaWmsDiaoboMMapper;
    @Resource
    private GaiaWmsTkshMMapper gaiaWmsTkshMMapper;
    @Resource
    private GaiaMaterialDocLogMapper gaiaMaterialDocLogMapper;
    @Resource
    private GaiaAllotPriceMapper gaiaAllotPriceMapper;
    @Resource
    private GaiaMaterialDocLogHMapper gaiaMaterialDocLogHMapper;
    @Resource
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Resource
    private GaiaSdSaleHMapper gaiaSdSaleHMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaSoItemMapper gaiaSoItemMapper;
    @Resource
    private BaseService baseService;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaProfitLevelMapper gaiaProfitLevelMapper;

    @Resource
    private FeignService feignService;

    @Autowired
    private GaiaBillOfParticularsMapper gaiaBillOfParticularsMapper;

    private List<GaiaTaxCode> gaiaTaxCodeList;
    private final int MING = -1; // 调用执行中
    private final int ERROR = 0; // 调用执行失败
    private final int SUCESS = 1; // 执行成功
    private final int TASKERROR = -11; // 任务执行失败
    @Resource
    private GaiaBillOfArMapper gaiaBillOfArMapper;
    @Resource
    private GaiaFiArBalanceMapper gaiaFiArBalanceMapper;

    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;

    /**
     * @param client
     * @param proSite
     * @param proCode
     * @return
     */
    private GaiaAllotPrice getGaiaAllotPrice(Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap,
                                             String client, String proSite, String proCode) {
        if (gaiaAllotPriceMap == null || gaiaAllotPriceMap.isEmpty()) {
            return null;
        }
        List<GaiaAllotPrice> gaiaAllotPriceList = gaiaAllotPriceMap.get(client);
        if (CollectionUtils.isEmpty(gaiaAllotPriceList)) {
            return null;
        }
        List<GaiaAllotPrice> entityList = null;
        if (StringUtils.isNotBlank(proCode)) {
            entityList = gaiaAllotPriceList.stream()
                    .filter(item -> (
                            client.equals(item.getClient()) &&
                                    proSite.equals(item.getAlpReceiveSite()) &&
                                    proCode.equals(item.getAlpProCode()))
                    )
                    .collect(Collectors.toList());
        }
        if (CollectionUtils.isEmpty(entityList)) {
            entityList = gaiaAllotPriceList.stream()
                    .filter(item -> (
                            client.equals(item.getClient()) &&
                                    proSite.equals(item.getAlpReceiveSite()) &&
                                    StringUtils.isBlank(item.getAlpProCode())) &&
                            item.getAlpCataloguePrice() == null
                    )
                    .collect(Collectors.toList());
        }
        if (CollectionUtils.isEmpty(entityList)) {
            return null;
        }
        return entityList.get(0);
    }

    /**
     * 税率
     *
     * @param taxCode
     * @return
     */
    private GaiaTaxCode getGaiaTaxCode(String taxCode) {
        if (StringUtils.isBlank(taxCode)) {
            return null;
        }
        if (CollectionUtils.isEmpty(this.gaiaTaxCodeList)) {
            return null;
        }
        List<GaiaTaxCode> list = this.gaiaTaxCodeList.stream().filter(item -> taxCode.equals(item.getTaxCode())).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public List<GaiaMaterialDocLogHWithBLOBs> getMaterialDocBatchTask() {
        log.info("物料凭证定时任务开启");
        List<GaiaMaterialDocLogHWithBLOBs> list = gaiaMaterialDocLogHMapper.selectGaiaMaterialDocLogHError();
        if (CollectionUtils.isEmpty(list)) {
            log.info("物料凭证定时任务结束_{}", "执行数据为空");
            return null;
        }
        log.info("物料凭证定时任务执行件数_{}", list.size());
        List<String> collect = list.stream().map(GaiaMaterialDocLogH::getGuid).collect(Collectors.toList());
        log.info("物料凭证定时任务执行数据_{}", JsonUtils.beanToJson(collect));
        return list;
    }

    @Override
    public void exeError(String logId, String message) {
        GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs = new GaiaMaterialDocLogHWithBLOBs();
        gaiaMaterialDocLogHWithBLOBs.setGmdlhSucess(ERROR);
        gaiaMaterialDocLogHWithBLOBs.setGuid(logId);
        gaiaMaterialDocLogHWithBLOBs.setGmdlhErrorResult(message);
        gaiaMaterialDocLogHMapper.updateByPrimaryKeySelective(gaiaMaterialDocLogHWithBLOBs);
    }

    @Override
    public void taskError(String logId, String message) {
        GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs = new GaiaMaterialDocLogHWithBLOBs();
        gaiaMaterialDocLogHWithBLOBs.setGmdlhSucess(TASKERROR);
        gaiaMaterialDocLogHWithBLOBs.setGuid(logId);
        gaiaMaterialDocLogHWithBLOBs.setGmdlhErrorResult(message);
        gaiaMaterialDocLogHMapper.updateByPrimaryKeySelective(gaiaMaterialDocLogHWithBLOBs);
    }

    @Override
    public void taskSucess(String logId) {
        GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs = new GaiaMaterialDocLogHWithBLOBs();
        gaiaMaterialDocLogHWithBLOBs.setGmdlhSucess(SUCESS);
        gaiaMaterialDocLogHWithBLOBs.setGuid(logId);
        gaiaMaterialDocLogHWithBLOBs.setGmdlhErrorResult("");
        gaiaMaterialDocLogHMapper.updateByPrimaryKeySelective(gaiaMaterialDocLogHWithBLOBs);
    }

    /**
     * 物料凭证定时任务
     */
    @Override
    @Transactional
    public FeignResult insertMaterialDocBatchTask(GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs) {
        log.info("物料凭证定时任务执行数据_{}", JsonUtils.beanToJson(gaiaMaterialDocLogHWithBLOBs));
        List<MaterialDocRequestDto> materialDocRequestDtoList =
                JsonUtils.jsonToBeanList(gaiaMaterialDocLogHWithBLOBs.getGmdlhContent(), MaterialDocRequestDto.class);
        FeignResult feignResult = insertMaterialDocBatch(materialDocRequestDtoList, gaiaMaterialDocLogHWithBLOBs.getGuid(), false);
        log.info("物料凭证定时任务执行数据结果_{}", JsonUtils.beanToJson(feignResult));
        return feignResult;
    }

    @Override
    @Transactional
    public FeignResult insertMaterialDocBatch(List<MaterialDocRequestDto> list, String logId, boolean isExe) {
        try {
            FeignResult feignResult = new FeignResult();
            // 调拨价格
            String client = list.get(0).getClient();
            String matDnId = list.get(0).getMatDnId();
            String matDnLineno = list.get(0).getMatDnLineno();
            String matSiteCode = list.get(0).getMatSiteCode();
            MatTypeEnum matTypeEnum = MatTypeEnum.getEnumByCode(list.get(0).getMatType());
            // MAT_DN_ID 重复验证
            List<GaiaMaterialDoc> gaiaMaterialDocList = null;
            if (StringUtils.isNotBlank(matDnId)) {
                switch (matTypeEnum) {
                    case CD:
                        gaiaMaterialDocList = gaiaMaterialDocMapper.getGaiaMaterialDocByMatDnId(client, matDnId, matDnLineno, matSiteCode);
                        break;
                    default:
                        gaiaMaterialDocList = gaiaMaterialDocMapper.getGaiaMaterialDocByMatDnId(client, matDnId, null, matSiteCode);
                        break;
                }
                if (!CollectionUtils.isEmpty(gaiaMaterialDocList)) {
                    feignResult.setCode(0);
                    feignResult.setMessage("执行成功");
                    log.info("MAT_DN_ID 重复，物料凭证表已存在！");

                    // 修改日志状态
                    if (StringUtils.isNotBlank(logId)) {
                        GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs = new GaiaMaterialDocLogHWithBLOBs();
                        gaiaMaterialDocLogHWithBLOBs.setGuid(logId);
                        gaiaMaterialDocLogHWithBLOBs.setGmdlhSucess(SUCESS);
                        gaiaMaterialDocLogHMapper.updateByPrimaryKeySelective(gaiaMaterialDocLogHWithBLOBs);
                    }
                    return feignResult;
                }
            }
            Map<String, String> matIdMap = new HashMap<>();
            Map<String, Integer> indexMap = new HashMap<>();
            Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap = new HashMap<>();
            List<GaiaDcData> gaiaDcDataList = gaiaDcDataMapper.selectDcListByClient(client);
            // LS
            Map<String, GaiaSdSaleD> gaiaSdSaleDMapLs = new HashMap<>();
            // 税率
            if (CollectionUtils.isEmpty(this.gaiaTaxCodeList)) {
                this.gaiaTaxCodeList = gaiaTaxCodeMapper.selectTaxList();
            }
            List<String> siteList = list.stream().map(MaterialDocRequestDto::getMatSiteCode).filter(s -> StringUtils.isNotBlank(s)).distinct().collect(Collectors.toList());
            List<String> stoList = list.stream().map(MaterialDocRequestDto::getMatStoCode).filter(s -> StringUtils.isNotBlank(s)).distinct().collect(Collectors.toList());
            List<String> proList = list.stream().map(MaterialDocRequestDto::getMatProCode).filter(s -> StringUtils.isNotBlank(s)).distinct().collect(Collectors.toList());
            siteList.addAll(stoList);
            // 调拨价格
            List<GaiaAllotPrice> gaiaAllotPriceList = new ArrayList<>();
            if (!CollectionUtils.isEmpty(siteList)) {
                gaiaAllotPriceList = gaiaAllotPriceMapper.selectAllotPriceByClient(client, siteList, proList);
                // 调拨价格
                gaiaAllotPriceMap = gaiaAllotPriceList.stream().collect(Collectors.groupingBy(GaiaAllotPrice::getClient));
            }
            // 毛利级别
            List<GaiaProfitLevel> gaiaProfitLevelList = gaiaProfitLevelMapper.selectProfitList(client);
            for (MaterialDocRequestDto dto : list) {
                // 交易数量为空或0，不处理
                if (dto.getMatQty() == null || dto.getMatQty().compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }
                FeignResult result = insertMaterialDoc(dto, gaiaSdSaleDMapLs, matIdMap, indexMap, gaiaAllotPriceMap, gaiaDcDataList, gaiaProfitLevelList);
                if (result != null && result.getCode() != 0) {
                    // 执行异常
                    if (isExe && StringUtils.isNotBlank(logId)) {
                        this.exeError(logId, "");
                    }
                    log.info("执行报错{}", JsonUtils.beanToJson(result));
                    throw new FeignResultException(result.getCode(), result.getMessage());
                }
            }
            // 修改日志状态
            if (StringUtils.isNotBlank(logId)) {
                GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs = new GaiaMaterialDocLogHWithBLOBs();
                gaiaMaterialDocLogHWithBLOBs.setGuid(logId);
                gaiaMaterialDocLogHWithBLOBs.setGmdlhSucess(SUCESS);
                gaiaMaterialDocLogHMapper.updateByPrimaryKeySelective(gaiaMaterialDocLogHWithBLOBs);
            }
            log.info("gaiaSdSaleDMapLs{}", JsonUtils.beanToJson(gaiaSdSaleDMapLs));
            List<GaiaSdSaleD> resultList = new ArrayList<>();
            if (gaiaSdSaleDMapLs != null && !gaiaSdSaleDMapLs.isEmpty()) {
                for (String key : gaiaSdSaleDMapLs.keySet()) {
                    GaiaSdSaleD entity = gaiaSdSaleDMapLs.get(key);
                    resultList.add(entity);
                }
            }
            // LS
            // 再根据加盟商+销售单号（物料凭证接口传入的业务单号MAT_DN_ID）+店号（物料凭证接口传入的地点MAT_SITE_CODE）+行号（物料凭证接口传入的业务单行号MAT_DN_LINENO）+销售日期（物料凭证接口传入的业务单行号MAT_DN_LINENO）
            // 更新GAIA_SD_SALE_D中的批次成本价GSSD_BATCH_COST（物料凭证中的总金额（移动））
            if (gaiaSdSaleDMapLs != null && !gaiaSdSaleDMapLs.isEmpty()) {
                for (String key : gaiaSdSaleDMapLs.keySet()) {
                    GaiaSdSaleD entity = gaiaSdSaleDMapLs.get(key);
                    GaiaSdSaleDKey gaiaSdSaleDKey = new GaiaSdSaleDKey();
                    // 加盟商
                    gaiaSdSaleDKey.setClient(entity.getClient());
                    // 销售单号
                    gaiaSdSaleDKey.setGssdBillNo(entity.getGssdBillNo());
                    // 店号
                    gaiaSdSaleDKey.setGssdBrId(entity.getGssdBrId());
                    // 销售日期
                    gaiaSdSaleDKey.setGssdDate(entity.getGssdDate());
                    // 行号
                    gaiaSdSaleDKey.setGssdSerial(entity.getGssdSerial());
                    GaiaSdSaleD gaiaSdSaleD = gaiaSdSaleDMapper.selectByPrimaryKey(gaiaSdSaleDKey);
                    if (gaiaSdSaleD != null) {
                        GaiaSdSaleD record = new GaiaSdSaleD();
                        // 加盟商
                        record.setClient(entity.getClient());
                        // 销售单号
                        record.setGssdBillNo(entity.getGssdBillNo());
                        // 店号
                        record.setGssdBrId(entity.getGssdBrId());
                        // 销售日期
                        record.setGssdDate(entity.getGssdDate());
                        // 行号
                        record.setGssdSerial(entity.getGssdSerial());
                        // 移动平均总价
                        record.setGssdMovPrices(entity.getGssdBatchCost());
                        // 移动税额
                        record.setGssdTaxRate(entity.getGssdTaxRate());
                        // 移动平均单价
                        record.setGssdMovPrice(entity.getGssdMovPrice());
                        // 加点后金额
                        record.setGssdAddAmt(entity.getGssdAddAmt());
                        // 加点后税金
                        record.setGssdAddTax(entity.getGssdAddTax());
                        gaiaSdSaleDMapper.updateByPrimaryKeySelective(record);
                        gaiaSdSaleHMapper.lastUpdateTime(record.getClient(), record.getGssdBillNo());
                    }
                }
            }
            /**
             *  生成应收款  XD 销售单
             */
            insertArBillForXD(list, client, matTypeEnum);
            /**
             *  生成应收款  ED 退订单
             */
            insertArBillForED(list, client, matTypeEnum);

            /**
             *  计算往来管理余额 PD, TD, MD, ND
             */
//            computeComeAndGoResAmt(list, client);

            /**
             * 实时插入物料凭证金额  暂时注释
             */
            //insertApBillForXD(list, client,matTypeEnum.getCode());

            feignResult.setCode(0);
            feignResult.setMessage("执行成功");
            feignResult.setData(resultList);
            log.info("执行成功");
            return feignResult;
        } catch (Exception e) {
            // 执行异常
            if (isExe && StringUtils.isNotBlank(logId)) {
                this.exeError(logId, "");
            }
            log.info("执行失败{}", e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }


    private void computeComeAndGoResAmt(List<MaterialDocRequestDto> list, String client) {
        log.info("<物料凭证><生成门店往来明细>:正在执行计算往来管理余额");
        ArrayList<String> storeMatType = Lists.newArrayList("PD", "TD");    // 仓库往来 9999
        ArrayList<String> allotMatType = Lists.newArrayList("MD", "ND");    // 调拨往来 6666
        // 根据业务单号，批次，商品自编码，业务类型 进行分组
        Map<String, List<MaterialDocRequestDto>> dataMap = list.stream().collect(Collectors.groupingBy(
                item -> item.getMatDnId() + "_" + item.getMatBatch() + "_" + item.getMatProCode() + "_" + item.getMatType()));
        log.info("<物料凭证><生成门店往来明细>" + "物料凭证源数据：" + list.toString());
        for (String key : dataMap.keySet()) {
            try {
                // 每组的第一条物料凭证数据
                MaterialDocRequestDto materialDocRequestDto = dataMap.get(key).get(0);
                log.info("门店往来明细分组数据：" + materialDocRequestDto.toString());
                if (storeMatType.contains(materialDocRequestDto.getMatType())) {
                    // 查询判断GAIA_BILL_OF_PARTICULARS是否存在对应8888记录
                    Integer count = gaiaBillOfParticularsMapper.countListByPaymentId(client, materialDocRequestDto.getMatStoCode(), materialDocRequestDto.getMatSiteCode(), "8888", materialDocRequestDto.getMatCreateDate());
                    if (count > 0) {
                        // 计算仓库往来余额
                        log.info("<物料凭证><生成门店往来明细>" + "存在对应8888记录，开始计算9999");
                        computeStoreResAmt(dataMap.get(key));
                    }
                } else if (allotMatType.contains(materialDocRequestDto.getMatType())) {
                    Integer count = gaiaBillOfParticularsMapper.countListByPaymentId(client, materialDocRequestDto.getMatStoCode(), materialDocRequestDto.getMatSiteCode(), "8888", materialDocRequestDto.getMatCreateDate());
                    if (count > 0) {
                        // 计算调拨往来余额
                        log.info("<物料凭证><生成门店往来明细>" + "存在对应8888记录，开始计算6666");
                        computeAllotResAmt(dataMap.get(key));
                    }
                }
            } catch (Exception e) {
                log.info(String.format("<物料凭证><生成门店往来明细><报错：%s>", e.getMessage()));
                e.printStackTrace();
            }
        }
    }

    private void computeStoreResAmt(List<MaterialDocRequestDto> materialDocRequestDtos) {
        // 两条数据matSiteCode一条仓库，一条门店，取门店那条数据
        GaiaBillOfParticulars gaiaBillOfParticulars = new GaiaBillOfParticulars();
        log.info("<物料凭证><生成门店往来明细>" + "存在对应8888记录，开始计算9999:" + String.format("size %s", materialDocRequestDtos.size()));
        for (MaterialDocRequestDto materialDocRequestDto : materialDocRequestDtos) {
            // 查询对应配送单据数据
            List<WholesaleSaleOutData> outData = gaiaBillOfParticularsMapper.selectWholesaleSalePage(materialDocRequestDto.getClient(), materialDocRequestDto.getMatStoCode(), materialDocRequestDto.getMatDnId());
            if (outData.size() == 0) {
                throw new BusinessException("不存在该物料凭证表数据");
            }
            WholesaleSaleOutData wholesaleSaleOutData = outData.get(0);
            BigDecimal matAddAmt = wholesaleSaleOutData.getMovTotalAmount();    // 实际支付金额
            gaiaBillOfParticulars.setDcCode(materialDocRequestDto.getMatSiteCode());
            gaiaBillOfParticulars.setDcName(gaiaBillOfParticularsMapper.queryDcNameByCode(materialDocRequestDto.getClient(), materialDocRequestDto.getMatSiteCode()));
            gaiaBillOfParticulars.setClient(materialDocRequestDto.getClient());
            gaiaBillOfParticulars.setStoCode(wholesaleSaleOutData.getStoCode());
            gaiaBillOfParticulars.setStoName(wholesaleSaleOutData.getStoName());     // 门店名称
            gaiaBillOfParticulars.setPaymentId("9999");
            gaiaBillOfParticulars.setAmountOfPaymentRealistic(matAddAmt);
            gaiaBillOfParticulars.setAmountRate("1.00");            // 折扣率固定为1
            gaiaBillOfParticulars.setAmountOfPayment(matAddAmt);
            gaiaBillOfParticulars.setRemarks("system"); // 备注
            gaiaBillOfParticulars.setPaymentDate(wholesaleSaleOutData.getPostDate());
            Date date = new Date();
            gaiaBillOfParticulars.setUploadDate(DateFormatUtils.format(date, "yyyyMMdd"));
            gaiaBillOfParticulars.setUploadTime(DateFormatUtils.format(date, "HHmmss"));
            // 将数据插入GAIA_BILL_OF_PARTICULARS
            gaiaBillOfParticularsMapper.insert(gaiaBillOfParticulars);
            // 将余额合计到GAIA_STORE_DATA
            gaiaStoreDataMapper.increaserRemainingSum(materialDocRequestDtos.get(0).getClient(), gaiaBillOfParticulars.getStoCode(), gaiaBillOfParticulars.getAmountOfPaymentRealistic());
        }
    }


    private void computeAllotResAmt(List<MaterialDocRequestDto> materialDocRequestDtos) {
        // 两条数据matSiteCode一条仓库，一条门店，取门店那条数据
        GaiaBillOfParticulars gaiaBillOfParticulars = new GaiaBillOfParticulars();
        log.info("<物料凭证><生成门店往来明细>" + "存在对应8888记录，开始计算6666:" + String.format("size %s", materialDocRequestDtos.size()));
        for (MaterialDocRequestDto materialDocRequestDto : materialDocRequestDtos) {
            // 查询对应配送单据数据
            List<WholesaleSaleOutData> outData = gaiaBillOfParticularsMapper.selectWholesaleSalePage(materialDocRequestDto.getClient(), materialDocRequestDto.getMatStoCode(), materialDocRequestDto.getMatDnId());
            if (outData.size() == 0) {
                throw new BusinessException("不存在该物料凭证表数据");
            }
            WholesaleSaleOutData wholesaleSaleOutData = outData.get(0);
            BigDecimal matAddAmt = wholesaleSaleOutData.getMovTotalAmount();    // 实际支付金额
            gaiaBillOfParticulars.setDcCode(materialDocRequestDto.getMatSiteCode());
            gaiaBillOfParticulars.setDcName(gaiaBillOfParticularsMapper.queryDcNameByCode(materialDocRequestDto.getClient(), materialDocRequestDto.getMatSiteCode()));
            gaiaBillOfParticulars.setClient(materialDocRequestDto.getClient());
            gaiaBillOfParticulars.setStoCode(wholesaleSaleOutData.getStoCode());
            gaiaBillOfParticulars.setStoName(wholesaleSaleOutData.getStoName());     // 门店名称
            gaiaBillOfParticulars.setPaymentId("6666");
            gaiaBillOfParticulars.setAmountOfPaymentRealistic(matAddAmt);
            gaiaBillOfParticulars.setAmountRate("1.00");            // 折扣率固定为1
            gaiaBillOfParticulars.setAmountOfPayment(matAddAmt);
            gaiaBillOfParticulars.setRemarks("system"); // 备注
            gaiaBillOfParticulars.setPaymentDate(wholesaleSaleOutData.getPostDate());
            Date date = new Date();
            gaiaBillOfParticulars.setUploadDate(DateFormatUtils.format(date, "yyyyMMdd"));
            gaiaBillOfParticulars.setUploadTime(DateFormatUtils.format(date, "HHmmss"));
            // 将数据插入GAIA_BILL_OF_PARTICULARS
            gaiaBillOfParticularsMapper.insert(gaiaBillOfParticulars);
            // 将余额合计到GAIA_STORE_DATA
            gaiaStoreDataMapper.increaserRemainingSum(materialDocRequestDtos.get(0).getClient(), gaiaBillOfParticulars.getStoCode(), gaiaBillOfParticulars.getAmountOfPaymentRealistic());
        }
    }


    /**
     * 销售单
     *
     * @param list   销售集合
     * @param client 加盟商
     * @param
     */
    private void insertApBillForXD(List<MaterialDocRequestDto> list, String client, String matTypeEnum) {
        try {
            if (org.apache.commons.lang.StringUtils.isBlank(matTypeEnum) || Objects.equals(matTypeEnum, MatTypeEnum.CD.getCode()) ||
                    Objects.equals(matTypeEnum, MatTypeEnum.CX.getCode()) || Objects.equals(matTypeEnum, MatTypeEnum.GD.getCode())) {
                Set<String> matDnIdSet = list.stream().map(MaterialDocRequestDto::getMatDnId).collect(Collectors.toSet());
                //获取时间
                //String date = DateUtil.format(DateUtil.parse(DateUtil.today(), "yyyy-MM-dd"), "yyyyMMdd");
           /* Calendar   cal   =   Calendar.getInstance();
            cal.add(Calendar.DATE,   -1);
            String date = new SimpleDateFormat( "yyyyMMdd ").format(cal.getTime());*/
                String date = DateUtils.getCurrentDateStrYYMMDD();
                for (String matDnId : matDnIdSet) {
                    //判断该客户是否有起初
                    log.info(String.format("<物料凭证供应商><生成应收账款><client:" + client + "matDnId:" + matDnId + ">"));
                    List<SupplierDealOutData> supplierDealList = gaiaMaterialDocMapper.selectSupplierDealPageBySiteCode(matTypeEnum, client, matDnId, date);
                    if (!CollectionUtils.isEmpty(supplierDealList)) {
                        supplierDealList.forEach(supplier -> {
                            //获取这个仓库地点下是否有期初
                            Integer count = gaiaBillOfArMapper.getBeginningOfPeriod(client, supplier.getSupCode(), supplier.getSiteCode());
                            if (Objects.nonNull(count) && count > 0) {
                                //根据业务员进行拆分数据
                                List<SupplierDealOutData> suppliers = gaiaBillOfArMapper.getBatSupplier(supplier);
                                if (!CollectionUtils.isEmpty(suppliers)) {
                                    //获取到批次号
                                    BigDecimal decimal = BigDecimal.ZERO;
                                    //去税额度
                                    BigDecimal batAmt = BigDecimal.ZERO;
                                    //税额度
                                    BigDecimal rateBat = BigDecimal.ZERO;
                                    for (SupplierDealOutData supp : suppliers) {
                                        if (org.apache.commons.lang.StringUtils.isNotBlank(supp.getSuppName())) {
                                            List<String> name = getName(client, supplier.getSiteCode(), supplier.getSupCode(), supp.getSuppName());
                                            gaiaMaterialDocMapper.insertBillOfAp(
                                                    new GaiaBillOfApVO()
                                                            .setClient(client)
                                                            .setDcCode(supplier.getSiteCode())
                                                            .setAmountOfPayment(supp.getBillingAmount())
                                                            .setAmountExcludingTax(supp.getBatAmt())
                                                            .setAmountOfTax(supp.getRateBat())
                                                            .setPaymentId("9999")
                                                            .setPaymentDate(supplier.getPostDate())
                                                            .setSupSelfCode(supplier.getSupCode())
                                                            .setSupName(supplier.getSupName())
                                                            .setSalesmanId(supp.getSuppName())
                                                            .setSalesmanName(!CollectionUtils.isEmpty(name) ? name.get(0) : null)
                                            );
                                            decimal = decimal.add(supp.getBillingAmount());
                                            batAmt = batAmt.add(supp.getBatAmt());
                                            rateBat = rateBat.add(supp.getRateBat());
                                        }
                                    }
                                    //查看金额   返回1表示大于0，返回-1表示小于0
                                    if (!(decimal.compareTo(supplier.getBillingAmount()) == 0)) {
                                        gaiaMaterialDocMapper.insertBillOfAp(
                                                new GaiaBillOfApVO()
                                                        .setClient(client)
                                                        .setDcCode(supplier.getSiteCode())
                                                        .setAmountOfPayment(supplier.getBillingAmount().subtract(decimal))
                                                        .setAmountExcludingTax(supplier.getBatAmt().subtract(batAmt))
                                                        .setAmountOfTax(supplier.getRateBat().subtract(rateBat))
                                                        .setPaymentId("9999")
                                                        .setPaymentDate(supplier.getPostDate())
                                                        .setSupSelfCode(supplier.getSupCode())
                                                        .setSupName(supplier.getSupName())
                                        );
                                    }
                                }

/*
                            //新增
                            GaiaBillOfApVO ofApVO = new GaiaBillOfApVO();
                            ofApVO.setClient(client);
                            ofApVO.setDcCode(supp.getSiteCode());
                            ofApVO.setPaymentId("9999");
                            ofApVO.setAmountOfPayment(supp.getBillingAmount());
                            ofApVO.setAmountExcludingTax(supp.getBatAmt());
                            ofApVO.setAmountOfTax(supp.getRateBat());
                            ofApVO.setPaymentDate(supp.getPostDate());
                            ofApVO.setSupSelfCode(supp.getSupCode());
                            ofApVO.setSupName(supp.getSupName());
                            ofApVO.setRemarks(supp.getPoHeadRemark());
                            gaiaMaterialDocMapper.insertBillOfAp(ofApVO);*/
                                //修改供应商主业务数据表金额字段
                                updateSupplierBusiness(client, supplier.getSupCode(), supplier.getSiteCode(), date);

                            }
                        });
                    }
                }
            }
        } catch (Exception e) {
            log.info(String.format("<物料凭证供应商9999><生成应收账款><报错：%s>", e.getMessage()));
            //e.printStackTrace();
        }
    }

    private List<String> getName(String client, String supSite, String supSelfCode, String salesmanId) {
        List<String> name = this.gaiaBillOfArMapper.getName(client, supSite, supSelfCode, salesmanId);
        return name;
    }

    /**
     * 修改供应商主业务金额字段
     *
     * @param client   加盟商
     * @param supCode  供应商
     * @param siteCode 地点
     * @param date     时间
     */
    @Override
    public void updateSupplierBusiness(String client, String supCode, String siteCode, String date) {
        List<GaiaBillOfApVO> billOfApVOList = gaiaBillOfArMapper.getAmt(client, supCode, siteCode, date);
        //期初是否存在
        if (!CollectionUtils.isEmpty(billOfApVOList)) {
            GaiaBillOfApVO ofApVO = billOfApVOList.get(0);
            List<GaiaBillOfApVO> billOfList = gaiaBillOfArMapper.getBillOfAp(client, supCode, siteCode, date);
            //为空说明期初在时间后面
            if (CollectionUtils.isEmpty(billOfList)) {
                //修改
                gaiaBillOfArMapper.updateSupplierBusiness(client, supCode, siteCode, ofApVO.getAmountOfPayment());
            } else {
                //获取本月第一天
                String firstDay = DateUtils.getTheFirstDayOfTheMonth();
                //昨天
                //String yestDay = DateUtil.format(DateUtil.parse(DateUtil.yesterday().toString(), "yyyy-MM-dd"), "yyyyMMdd");
                String yestDay = DateUtils.getYesterdayDate();
                GaiaBillOfApVO billOf = gaiaBillOfArMapper.getBillOf(client, supCode, siteCode, yestDay);
                BigDecimal amountOfPayment = BigDecimal.ZERO;
                if (Objects.nonNull(billOf)) {
                    amountOfPayment = billOfList.stream().map(GaiaBillOfApVO::getAmountOfPayment).reduce(BigDecimal.ZERO, BigDecimal::add).add(billOf.getAmountOfPayment());
                } else {
                    amountOfPayment = billOfList.stream().map(GaiaBillOfApVO::getAmountOfPayment).reduce(BigDecimal.ZERO, BigDecimal::add);
                }
                gaiaBillOfArMapper.updateSupplierBusiness(client, supCode, siteCode, amountOfPayment);
            }
        }
    }

    private void insertArBillForED(List<MaterialDocRequestDto> list, String client, MatTypeEnum matTypeEnum) {
        if ("ED".equals(matTypeEnum.getCode())) {
            try {
                Set<String> matDnIdSet = list.stream().map(MaterialDocRequestDto::getMatDnId).collect(Collectors.toSet());
                for (String matDnId : matDnIdSet) {
                    //判断该客户是否有起初
                    log.info(String.format("<物料凭证><生成应收账款ED><client:" + client + "matDnId:" + matDnId + ">"));
                    GaiaBillOfArVO billOfAr = gaiaMaterialDocMapper.getReceivablesInfoForED(client, matDnId);
                    if (ObjectUtils.isEmpty(billOfAr.getCusSelfCode())) {
                        String cusSelfCode = gaiaMaterialDocMapper.getCusSelfCodeForED(client, matDnId);
                        billOfAr.setCusSelfCode(cusSelfCode);
                    }
                    int count = gaiaBillOfArMapper.getOriBill(billOfAr);
                    if (count > 0 && ObjectUtils.isNotEmpty(billOfAr)) {
                        Date now = new Date();
                        billOfAr.setClient(client);
                        String billCode = gaiaBillOfArMapper.getBillCode(billOfAr);
                        billOfAr.setBillCode(billCode);
                        billOfAr.setArMethodId("9999");
                        billOfAr.setDcCode(list.get(0).getMatSiteCode());
                        billOfAr.setHxAmt(BigDecimal.ZERO);
                        billOfAr.setBillDate(now);
                        billOfAr.setStatus(0);
                        billOfAr.setIsDelete(0);
                        billOfAr.setRemark(matDnId);
                        billOfAr.setCreateUser(list.get(0).getMatCreateBy());
                        billOfAr.setCreateTime(now);
                        billOfAr.setUpdateUser(list.get(0).getMatCreateBy());
                        billOfAr.setUpdateTime(now);
                        gaiaBillOfArMapper.insert(billOfAr);
                        //修改客户余额
                        updatLastAmt(billOfAr, now);
                    }
                }
            } catch (Exception e) {
                log.info(String.format("<物料凭证><生成应收账款ED><报错：%s>", e.getMessage()));
                e.printStackTrace();
            }
        }
    }

    private void insertArBillForXD(List<MaterialDocRequestDto> list, String client, MatTypeEnum matTypeEnum) {
        if ("XD".equals(matTypeEnum.getCode())) {
            try {
                Set<String> matDnIdSet = list.stream().map(MaterialDocRequestDto::getMatDnId).collect(Collectors.toSet());
                for (String matDnId : matDnIdSet) {
                    //判断该客户是否有起初
                    log.info(String.format("<物料凭证><生成应收账款XD><client:" + client + "matDnId:" + matDnId + ">"));
                    GaiaBillOfArVO billOfAr = gaiaMaterialDocMapper.getReceivablesInfoForXD(client, matDnId);
                    if (ObjectUtils.isEmpty(billOfAr.getCusSelfCode())) {
                        String cusSelfCode = gaiaMaterialDocMapper.getCusSelfCodeForXD(client, matDnId);
                        billOfAr.setCusSelfCode(cusSelfCode);
                    }
                    int count = gaiaBillOfArMapper.getOriBill(billOfAr);
                    if (count > 0 && ObjectUtils.isNotEmpty(billOfAr)) {
                        Date now = new Date();
                        billOfAr.setClient(client);
                        String billCode = gaiaBillOfArMapper.getBillCode(billOfAr);
                        billOfAr.setBillCode(billCode);
                        billOfAr.setArMethodId("9999");
                        billOfAr.setHxAmt(BigDecimal.ZERO);
                        billOfAr.setBillDate(now);
                        billOfAr.setStatus(0);
                        billOfAr.setIsDelete(0);
                        billOfAr.setRemark(matDnId);
                        billOfAr.setCreateUser(list.get(0).getMatCreateBy());
                        billOfAr.setCreateTime(now);
                        billOfAr.setUpdateUser(list.get(0).getMatCreateBy());
                        billOfAr.setUpdateTime(now);
                        gaiaBillOfArMapper.insert(billOfAr);
                        GaiaBillOfArVO gaiaBillOfArVO = gaiaBillOfArMapper.selectSumByArAmt(billOfAr);
                        if (gaiaBillOfArVO != null) {
                            gaiaCustomerBusinessMapper.updateByCusArAmt(billOfAr, gaiaBillOfArVO.getArAmt());
                        }
                        //修改客户余额
                        updatLastAmt(billOfAr, now);
                    }
                }
            } catch (Exception e) {
                log.info(String.format("<物料凭证><生成应收账款XD><报错：%s>", e.getMessage()));
                e.printStackTrace();
            }
        }
    }

    /**
     * 物料凭证日志
     *
     * @param list
     */
    @Override
    @Transactional
    public String addLog(List<MaterialDocRequestDto> list) {
        try {
            if (CollectionUtils.isEmpty(list)) {
                return null;
            }
            log.info("日志数据：{}", JsonUtils.beanToJson(list));
            String logId = UUID.randomUUID().toString().replaceAll("-", "");
            GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs = new GaiaMaterialDocLogHWithBLOBs();
            gaiaMaterialDocLogHWithBLOBs.setGuid(logId);
            gaiaMaterialDocLogHWithBLOBs.setGmdlhContent(JsonUtils.beanToJson(list));
            gaiaMaterialDocLogHWithBLOBs.setGmdlhSucess(MING);
            gaiaMaterialDocLogHWithBLOBs.setGmdlhTime(DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS"));
            gaiaMaterialDocLogHMapper.insertSelective(gaiaMaterialDocLogHWithBLOBs);

            // 生成日志数据
            List<GaiaMaterialDocLog> gaiaMaterialDocLogList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                MaterialDocRequestDto materialDocRequestDto = list.get(i);
                GaiaMaterialDocLog gaiaMaterialDocLog = new GaiaMaterialDocLog();
                // 加盟商
                gaiaMaterialDocLog.setClient(materialDocRequestDto.getClient());
                // 主键ID
                gaiaMaterialDocLog.setGuid(materialDocRequestDto.getGuid());
                // 序号
                gaiaMaterialDocLog.setGuidNo(String.valueOf(i + 1));
                // 业务类型
                gaiaMaterialDocLog.setMatType(materialDocRequestDto.getMatType());
                // 过账日期
                gaiaMaterialDocLog.setMatPostDate(materialDocRequestDto.getMatPostDate());
                // 抬头备注
                gaiaMaterialDocLog.setMatHeadRemark(materialDocRequestDto.getMatHeadRemark());
                // 商品编码
                gaiaMaterialDocLog.setMatProCode(materialDocRequestDto.getMatProCode());
                // 配送门店
                gaiaMaterialDocLog.setMatStoCode(materialDocRequestDto.getMatStoCode());
                // 地点
                gaiaMaterialDocLog.setMatSiteCode(materialDocRequestDto.getMatSiteCode());
                // 库存地点
                gaiaMaterialDocLog.setMatLocationCode(materialDocRequestDto.getMatLocationCode());
                // 转移库存地点
                gaiaMaterialDocLog.setMatLocationTo(materialDocRequestDto.getMatLocationTo());
                // 批次
                gaiaMaterialDocLog.setMatBatch(materialDocRequestDto.getMatBatch());
                // 交易数量
                gaiaMaterialDocLog.setMatQty(materialDocRequestDto.getMatQty());
                // 交易金额
                gaiaMaterialDocLog.setMatAmt(materialDocRequestDto.getMatAmt());
                // 交易价格
                gaiaMaterialDocLog.setMatPrice(materialDocRequestDto.getMatPrice());
                // 基本计量单位
                gaiaMaterialDocLog.setMatUint(materialDocRequestDto.getMatUnit());
                // 借/贷标识
                gaiaMaterialDocLog.setMatDebitCredit(materialDocRequestDto.getMatDebitCredit());
                // 订单号
                gaiaMaterialDocLog.setMatPoId(materialDocRequestDto.getMatPoId());
                // 订单行号
                gaiaMaterialDocLog.setMatPoLineno(materialDocRequestDto.getMatPoLineno());
                // 业务单号
                gaiaMaterialDocLog.setMatDnId(materialDocRequestDto.getMatDnId());
                // 业务单行号
                gaiaMaterialDocLog.setMatDnLineno(materialDocRequestDto.getMatDnLineno());
                // 物料凭证行备注
                gaiaMaterialDocLog.setMatLineRemark(materialDocRequestDto.getMatLineRemark());
                // 创建人
                gaiaMaterialDocLog.setMatCreateBy(materialDocRequestDto.getMatCreateBy());
                // 创建日期
                gaiaMaterialDocLog.setMatCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 创建时间
                gaiaMaterialDocLog.setMatCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                // 互调价
                gaiaMaterialDocLog.setMatHdPrice(materialDocRequestDto.getMatHdPrice());
//                gaiaMaterialDocLogMapper.insert(gaiaMaterialDocLog);
                gaiaMaterialDocLogList.add(gaiaMaterialDocLog);
                if (gaiaMaterialDocLogList.size() >= 500) {
                    gaiaMaterialDocLogMapper.insertList(gaiaMaterialDocLogList);
                    gaiaMaterialDocLogList.clear();
                }
            }
            if (!CollectionUtils.isEmpty(gaiaMaterialDocLogList)) {
                gaiaMaterialDocLogMapper.insertList(gaiaMaterialDocLogList);
            }
            return logId;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            log.info("日志插入失败");
            return null;
        }
    }

    @Override
    public FeignResult insertMaterialDoc(MaterialDocRequestDto info, Map<String, GaiaSdSaleD> gaiaSdSaleDMapLs, Map<String, String> matIdMap, Map<String, Integer> indexMap,
                                         Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap, List<GaiaDcData> gaiaDcDataList, List<GaiaProfitLevel> gaiaProfitLevelList) {
        FeignResult feignResult = new FeignResult();
        feignResult.setCode(0);
        log.info("接收到参数：{}", JsonUtils.beanToJson(info));

        /**
         * 数据验证
         */
        ValidationResult result = ValidateUtil.validateEntity(info);
        if (result.isHasErrors()) {
            feignResult.setCode(1001);
            feignResult.setMessage(result.getMessage());
            return feignResult;
        }

        MatTypeEnum matTypeEnum = MatTypeEnum.getEnumByCode(info.getMatType());
        if (matTypeEnum == null) {
            feignResult.setCode(1002);
            feignResult.setMessage("业务类型不存在");
            return feignResult;
        }

        switch (matTypeEnum) {
            case CD:
                feignResult = insertMaterialDocByCD(info, matIdMap, indexMap, gaiaAllotPriceMap, gaiaProfitLevelList);
                break;
            case GD:
                feignResult = insertMaterialDocByGD(info, matIdMap, indexMap);
                break;
            case XD:
                feignResult = insertMaterialDocByXD(info, matIdMap, indexMap);
                break;
            case ED:
                feignResult = insertMaterialDocByED(info, matIdMap, indexMap);
                break;
            case PD:
                feignResult = insertMaterialDocByPD(info, matIdMap, indexMap, gaiaProfitLevelList);
                break;
            case TD:
                feignResult = insertMaterialDocByTD(info, matIdMap, indexMap);
                break;
            case LS:
                feignResult = insertMaterialDocByLS(info, gaiaSdSaleDMapLs, matIdMap, indexMap, gaiaAllotPriceMap, gaiaProfitLevelList);
                break;
            case MD:
                feignResult = insertMaterialDocByMD(info, matIdMap, indexMap, gaiaAllotPriceMap);
                break;
            case ND:
                feignResult = insertMaterialDocByND(info, matIdMap, indexMap, gaiaAllotPriceMap, gaiaProfitLevelList);
                break;
            case BD:
                feignResult = insertMaterialDocByBD(info, matIdMap, indexMap);
                break;
            case ZD:
                feignResult = insertMaterialDocByZD(info, matIdMap, indexMap);
                break;
            case SY:
                feignResult = insertMaterialDocBySY(info, matIdMap, indexMap, gaiaAllotPriceMap, gaiaDcDataList, gaiaProfitLevelList);
                break;
            case LX:
                feignResult = insertMaterialDocByLX(info, gaiaSdSaleDMapLs, matIdMap, indexMap);
                break;
            case CX:
                feignResult = insertMaterialDocByCX(info, matIdMap, indexMap);
                break;
            case PX:
                feignResult = insertMaterialDocByPX(info, matIdMap, indexMap);
                break;
            default:
                break;

        }
        log.info("结果:" + JsonUtils.beanToJson(feignResult));
        return feignResult;
    }

    /**
     * 根据加盟商+商品编码更新商品定位为X
     */
    private void updateProductPosition(String client, String matProCode) {
        gaiaProductBusinessMapper.updateProductPosition(client, matProCode);
    }

    /**
     * 配送（代销）
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByPX(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap) {

        FeignResult result = new FeignResult();
        if (StringUtils.isBlank(info.getMatStoCode())) {
            result.setCode(1001);
            result.setMessage("门店不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatSiteCode())) {
            result.setCode(1001);
            result.setMessage("仓库不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatBatch())) {
            result.setCode(1001);
            result.setMessage("批次不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoId())) {
            result.setCode(1001);
            result.setMessage("订单号不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoLineno())) {
            result.setCode(1001);
            result.setMessage("订单行号不能为空");
            return result;
        }
        if (info.getMatPrice() == null) {
            result.setCode(1001);
            result.setMessage("交易价格不能为空");
            return result;
        }
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        // 税率
        BigDecimal inputTax = BigDecimal.ZERO;
        // 仓库发货凭证 生成
        // 税率
        inputTax = getInputTax2(MatTypeEnum.PD.getCode(), info.getClient(), info.getMatPoId(), info.getMatProCode(), info.getMatSiteCode());
        // 采购单价
        BigDecimal price = info.getMatPrice();
        // 物料凭证生成
        GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
        // 仓库
        gaiaMaterialDoc.setMatSiteCode(info.getMatSiteCode());
        // 数量
        gaiaMaterialDoc.setMatQty(matQty);
        // 借/贷标识
        gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S
        // 物料凭证数量
        gaiaMaterialDoc.setMatQty(matQty);
        // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
        gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
        // 总金额（批次） = 交易价格*交易数量-税金(批次)
        gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
        // 总金额（移动）= 总金额（批次）
        gaiaMaterialDoc.setMatMovAmt(gaiaMaterialDoc.getMatBatAmt());
        // 税金（移动）= 税率 * 总金额（移动）
        gaiaMaterialDoc.setMatRateMov(inputTax.multiply(gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
        gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

        // 交易价格

        //  step 3: 再生成物料凭证，更新物料凭证表GAIA_MATERIAL_DOC
        GaiaMaterialDoc gaiaMaterialDoc2 = setMaterialDoc(info, matIdMap, indexMap);
        // 借/贷标识
        gaiaMaterialDoc2.setMatDebitCredit("S"); // 减库存-H；加库存-S
        // 物料凭证数量
        gaiaMaterialDoc2.setMatQty(matQty);
        gaiaMaterialDoc2.setMatSiteCode(info.getMatStoCode());  // 配送门店的地点编码
        gaiaMaterialDoc2.setMatLocationCode(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(0).getValue());  // 默认1000-合格品库
        // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
        gaiaMaterialDoc2.setMatRateBat(price.multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
        // 总金额（批次） = 交易价格*交易数量-税金(批次)
        gaiaMaterialDoc2.setMatBatAmt(matQty.multiply(price).subtract(gaiaMaterialDoc2.getMatRateBat()).setScale(2, RoundingMode.DOWN));
        // 总金额（移动）= 总金额（批次）
        gaiaMaterialDoc2.setMatMovAmt(gaiaMaterialDoc2.getMatBatAmt());
        // 「税金（移动）」=「税金（批次）」
        gaiaMaterialDoc2.setMatRateMov(gaiaMaterialDoc2.getMatRateBat());
        gaiaMaterialDocMapper.insert(gaiaMaterialDoc2);

        /**
         * step 7：调用财务接口
         */

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 采购（代销）
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByCX(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap) {
        FeignResult result = new FeignResult();
        /**
         * step 1: 先更新物料评估表的移动平均价
         * 1.根据加盟商+商品编码+地点至物料评估数据表（GAIA_MATERIAL_ASSESS）过滤数据
         * 2.无数据： 新增物料评估数据表
         * 3.有数据： 重算移动平均价后直接更新对应移动平均价的
         */
        if (StringUtils.isBlank(info.getMatPoId())) {
            result.setCode(1001);
            result.setMessage("订单号不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoLineno())) {
            result.setCode(1001);
            result.setMessage("订单行号不能为空");
            return result;
        }
        if (info.getMatPrice() == null) {
            result.setCode(1001);
            result.setMessage("交易价格不能为空");
            return result;
        }
        if (info.getMatQty() == null) {
            result.setCode(1001);
            result.setMessage("交易数量不能为空");
            return result;
        }
        // ----- 验证结束
        //税率
        BigDecimal inputTax = getInputTax(info);
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        // 生成物料凭证，更新物料凭证表 GAIA_MATERIAL_DOC
        GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
        // 交易数量
        gaiaMaterialDoc.setMatQty(matQty);
        gaiaMaterialDoc.setMatDebitCredit("S");  // 贷-加库存
        // 税金（批次）= 交易价格*交易数量/(1+税率)*税率
        gaiaMaterialDoc.setMatRateBat(info.getMatPrice().multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
        // 总金额（批次）=  交易价格*交易数量-税金(批次)
        gaiaMaterialDoc.setMatBatAmt(matQty.multiply(info.getMatPrice()).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
        // 总金额（移动）= 总金额（批次）
        gaiaMaterialDoc.setMatMovAmt(gaiaMaterialDoc.getMatBatAmt());
        // 「税金（移动）」=「税金（批次）」
        gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatRateBat());
        gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

        /**
         * step 4: 调用财务接口
         */

        result.setCode(0);
        result.setMessage("执行成功");
        return result;

    }

    /**
     * 采购代销
     *
     * @param info
     * @param gaiaSdSaleDMapLx
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByLX(MaterialDocRequestDto info, Map<String, GaiaSdSaleD> gaiaSdSaleDMapLx, Map<String, String> matIdMap, Map<String, Integer> indexMap) {
        FeignResult result = new FeignResult();
        if (StringUtils.isBlank(info.getMatPoId())) {
            result.setCode(1001);
            result.setMessage("订单号不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoLineno())) {
            result.setCode(1001);
            result.setMessage("订单行号不能为空");
            return result;
        }
        if (info.getMatQty() == null) {
            result.setCode(1001);
            result.setMessage("交易数量不能为空");
            return result;
        }

        if (info.getMatPrice() == null) {
            result.setCode(1001);
            result.setMessage("交易金额不能为空");
            return result;
        }

        // 交易数量
        BigDecimal matQty = info.getMatQty().abs();
        // 税率
        BigDecimal tax = getInputTax3(info);
        // 交易价格
        BigDecimal price = info.getMatPrice();
        // 移动价
        BigDecimal matMovAmt = BigDecimal.ZERO;
        // 税额
        BigDecimal matRateMov = BigDecimal.ZERO;
        // 加点后金额
        BigDecimal matAddAmt = BigDecimal.ZERO;
        // 加点后税金
        BigDecimal matAddTax = BigDecimal.ZERO;
        // 增加逻辑 收货
        if (info.getMatQty().compareTo(BigDecimal.ZERO) > 0) {
            // 物料凭证生成
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 总金额（移动）= 总金额（批次）
            gaiaMaterialDoc.setMatMovAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("S"); // 减库存-H；加库存-S
            // 「税金（移动）」=「税金（批次）」
            gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatRateBat());
            gaiaMaterialDoc.setMatAddAmt(gaiaMaterialDoc.getMatMovAmt());
            gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatRateMov());
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);
            matMovAmt = gaiaMaterialDoc.getMatMovAmt();
            matRateMov = gaiaMaterialDoc.getMatRateMov();
            matAddAmt = gaiaMaterialDoc.getMatAddAmt();
            matAddTax = gaiaMaterialDoc.getMatAddTax();
        } else {
            // 物料凭证生成
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 总金额（移动）= 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatMovAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S物料凭证接口传入的业务单行号MAT_DN_LINENO
            // 税金（移动）= 税率 * 总金额（移动）
            gaiaMaterialDoc.setMatRateMov(tax.multiply(gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
            gaiaMaterialDoc.setMatAddAmt(gaiaMaterialDoc.getMatMovAmt());
            gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatRateMov());
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);
            matMovAmt = gaiaMaterialDoc.getMatMovAmt();
            matRateMov = gaiaMaterialDoc.getMatRateMov();
            matAddAmt = gaiaMaterialDoc.getMatAddAmt();
            matAddTax = gaiaMaterialDoc.getMatAddTax();
        }

        // 再根据加盟商+销售单号（物料凭证接口传入的业务单号MAT_DN_ID）+店号（物料凭证接口传入的地点MAT_SITE_CODE）+行号（物料凭证接口传入的业务单行号MAT_DN_LINENO）+销售日期（物料凭证接口传入的业务单行号MAT_POST_DATE）
        if (StringUtils.isNotBlank(info.getClient()) &&
                StringUtils.isNotBlank(info.getMatDnId()) &&
                StringUtils.isNotBlank(info.getMatSiteCode()) &&
                StringUtils.isNotBlank(info.getMatDnLineno()) &&
                StringUtils.isNotBlank(info.getMatPostDate())) {
            String key = info.getClient() + "_" + info.getMatDnId() + "_" + info.getMatSiteCode() + "_" + info.getMatDnLineno() + "_" + info.getMatPostDate();
            GaiaSdSaleD gaiaSdSaleD = gaiaSdSaleDMapLx.get(key);
            if (gaiaSdSaleD == null) {
                gaiaSdSaleD = new GaiaSdSaleD();
                // 加盟商
                gaiaSdSaleD.setClient(info.getClient());
                // 销售单号
                gaiaSdSaleD.setGssdBillNo(info.getMatDnId());
                // 店号
                gaiaSdSaleD.setGssdBrId(info.getMatSiteCode());
                // 销售日期
                gaiaSdSaleD.setGssdDate(info.getMatPostDate());
                // 行号
                gaiaSdSaleD.setGssdSerial(info.getMatDnLineno());
            }
            // 批次成本价
            BigDecimal gssdBatchCost = gaiaSdSaleD.getGssdBatchCost() == null ? BigDecimal.ZERO : gaiaSdSaleD.getGssdBatchCost();
            // 物料凭证中的总金额（移动）
            gaiaSdSaleD.setGssdBatchCost(this.add(gssdBatchCost, matMovAmt));
            // 移动税额
            BigDecimal gssdTaxRate = gaiaSdSaleD.getGssdTaxRate() == null ? BigDecimal.ZERO : gaiaSdSaleD.getGssdTaxRate();
            gaiaSdSaleD.setGssdTaxRate(this.add(gssdTaxRate, matRateMov));
            // 加点后金额
            BigDecimal gssdAddAmt = gaiaSdSaleD.getGssdAddAmt();
            gaiaSdSaleD.setGssdAddAmt(this.add(gssdAddAmt, matAddAmt));
            // 加点后税金
            BigDecimal gssdAddTax = gaiaSdSaleD.getGssdAddTax();
            gaiaSdSaleD.setGssdAddTax(this.add(gssdAddTax, matAddTax));
            gaiaSdSaleDMapLx.put(key, gaiaSdSaleD);
        }

        result.setCode(0);
        result.setMessage("执行成功");
        return result;

    }

    /**
     * 损益
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocBySY(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap,
                                              Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap, List<GaiaDcData> gaiaDcDataList,
                                              List<GaiaProfitLevel> gaiaProfitLevelList) {
        FeignResult result = new FeignResult();
        if (StringUtils.isBlank(info.getMatBatch())) {
            result.setCode(1001);
            result.setMessage("批次不能为空");
            return result;
        }
        // 交易数量
        if (info.getMatQty() == null) {
            info.setMatQty(BigDecimal.ZERO);
        }
        GaiaBatchInfoKey gaiaBatchInfoKey = new GaiaBatchInfoKey();
        gaiaBatchInfoKey.setClient(info.getClient());
        gaiaBatchInfoKey.setBatSiteCode(info.getMatSiteCode());
        gaiaBatchInfoKey.setBatProCode(info.getMatProCode());
        gaiaBatchInfoKey.setBatBatch(info.getMatBatch());
        GaiaBatchInfo gaiaBatchInfo = gaiaBatchInfoMapper.selectForMdoc(gaiaBatchInfoKey);
        if (gaiaBatchInfo == null) {
            throw new FeignResultException(1002, "批次信息表不存在");
        }
        // 交易价格
        BigDecimal price = this.getPrice(info);
        // 交易数量绝对值
        BigDecimal matQty = info.getMatQty().abs();
        // 税率
        BigDecimal tax = getInputTax(info.getClient(), gaiaBatchInfo.getBatPoId(), gaiaBatchInfo.getBatPoLineno(), gaiaBatchInfo.getBatSiteCode(), gaiaBatchInfo.getBatProCode());
        // 增加逻辑 收货
        if (info.getMatQty().compareTo(BigDecimal.ZERO) > 0) {
            // 物料凭证生成
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("S"); // 减库存-H；加库存-S
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 总金额（移动）= 总金额（批次）
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialDoc.getMatBatAmt());
            // 「税金（移动）」=「税金（批次）」 → 移动税金 * 税率
            gaiaMaterialDoc.setMatRateMov(tax.multiply(gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
            // 加点计算
            // 加点后金额 = 总金额（移动）
            gaiaMaterialDoc.setMatAddAmt(gaiaMaterialDoc.getMatMovAmt());
            // 税额 = 总金额（移动） * 税率
            gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatRateMov());
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

            // 根据加盟商+商品编码+收货地点到物料评估表中过滤数据
            GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
            assessKey.setClient(info.getClient());   // 加盟商
            assessKey.setMatProCode(info.getMatProCode());   // 商品编码
            assessKey.setMatAssessSite(info.getMatSiteCode()); // 门店
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
            // 若无数据直接插入
            if (gaiaMaterialAssess == null) {
                gaiaMaterialAssess = new GaiaMaterialAssess();
                // 加盟商
                gaiaMaterialAssess.setClient(info.getClient());
                // 商品编码
                gaiaMaterialAssess.setMatProCode(info.getMatProCode());
                // 估价地点
                gaiaMaterialAssess.setMatAssessSite(info.getMatSiteCode());
                // 总库存 = 物料凭证数量
                gaiaMaterialAssess.setMatTotalQty(gaiaMaterialDoc.getMatQty());
                // 总金额 = 总金额（移动）
                gaiaMaterialAssess.setMatTotalAmt(gaiaMaterialDoc.getMatMovAmt());
                // 移动平均价 = 总金额/总数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                // 税金 = 交易价格*交易数量/(1+税率)*税率
                gaiaMaterialAssess.setMatRateAmt(gaiaMaterialDoc.getMatRateMov());
                // 创建日期
                gaiaMaterialAssess.setBatCreateDate(info.getMatCreateDate());
                // 创建时间
                gaiaMaterialAssess.setBatCreateTime(info.getMatCreateTime());
                // 创建人
                gaiaMaterialAssess.setBatCreateUser(info.getMatCreateBy());
                // 加点计算
                addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatSiteCode(), info.getMatProCode(), tax, matQty, gaiaAllotPriceMap);
                gaiaMaterialAssessMapper.insert(gaiaMaterialAssess);
            } else {
                // 「总库存」即为原有数量+物料凭证数量，
                gaiaMaterialAssess.setMatTotalQty(this.add(gaiaMaterialAssess.getMatTotalQty(), gaiaMaterialDoc.getMatQty()));
                // 「总金额」为原有金额+物料凭证中的「总金额（移动）」的值
                gaiaMaterialAssess.setMatTotalAmt(this.add(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
                // 税金
                gaiaMaterialAssess.setMatRateAmt(this.add(gaiaMaterialAssess.getMatRateAmt(), gaiaMaterialDoc.getMatRateMov()));
                // 移动平均价 = 总金额/总数量
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                } else {
                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                }
                gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                // 加点计算
                addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatSiteCode(), info.getMatProCode(), tax, matQty, gaiaAllotPriceMap);
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
                }
                gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
            }
            // 毛利级别更新
            baseService.setProProfit(info.getClient(), info.getMatSiteCode(), info.getMatProCode(), this.gaiaTaxCodeList, null, null, null, null, gaiaProfitLevelList);
        } else { // 增加逻辑 发货
            // 根据加盟商+商品编码+发货地点到物料评估表中过滤出数据
            GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
            assessKey.setClient(info.getClient());   // 加盟商
            assessKey.setMatProCode(info.getMatProCode());   // 商品编码
            assessKey.setMatAssessSite(info.getMatSiteCode()); // 门店
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
            if (gaiaMaterialAssess == null) {
                result.setCode(1001);
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatSiteCode(), info.getMatProCode());
                if (gaiaProductBusiness == null) {
                    result.setMessage("商品没有成本记录，无法过账");
                } else {
                    result.setMessage("商品" + info.getMatProCode() + "_" + gaiaProductBusiness.getProName() + "没有成本记录，无法过账");
                }
                return result;
            } else {
                boolean flg = true;
                // DC 集合
                if (!CollectionUtils.isEmpty(gaiaDcDataList)) {
                    GaiaDcData gaiaDcData = gaiaDcDataList.stream().filter(item -> item.getDcCode().equals(info.getMatSiteCode())).findFirst().orElse(null);
                    if (gaiaDcData != null) {
                        flg = false;
                    }
                }
                // 物料凭证生成
                GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
                // 物料凭证数量
                gaiaMaterialDoc.setMatQty(matQty);
                // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
                gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
                // 总金额（批次） = 交易价格*交易数量-税金(批次)
                gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()));
                // 总金额（移动）= 取原物料评估表中的总金额/总数量*发生数；如果总金额、总数量任一为0，则直接取原移动平均单价*发生数。
                if (gaiaMaterialAssess.getMatTotalAmt() == null || gaiaMaterialAssess.getMatTotalAmt().compareTo(BigDecimal.ZERO) == 0 ||
                        gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).setScale(2, RoundingMode.DOWN));
                } else {
                    gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                }
                // 借/贷标识
                gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S
                // 税金（移动）= 税率 * 总金额（移动）
//                gaiaMaterialDoc.setMatRateMov(tax.multiply(gaiaMaterialDoc.getMatMovAmt()));
                // 物料评估税金/物料评估总数量 * 交易数据
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    // 总金额（移动） * 税
                    gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatMovAmt().multiply(tax).setScale(2, RoundingMode.DOWN));
                } else {
                    gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                }
                // 加点计算
                if (flg) {
                    subtractMaterialDoc(gaiaMaterialDoc, gaiaMaterialAssess, tax, matQty);
                }
                gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

                // 若物料凭证数量等于总库存，则总库存、总金额的值全部清空成0
                if (gaiaMaterialDoc.getMatQty().compareTo(gaiaMaterialAssess.getMatTotalQty()) == 0) {
                    gaiaMaterialAssess.setMatTotalQty(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                } else {
                    gaiaMaterialAssess.setMatTotalAmt(this.subtract(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
                    //  物料凭证数量作为扣减数量
                    gaiaMaterialAssess.setMatTotalQty(gaiaMaterialAssess.getMatTotalQty().subtract(gaiaMaterialDoc.getMatQty()));
                }
                // 税金 = 税金 + 税金（移动）
                gaiaMaterialAssess.setMatRateAmt((gaiaMaterialAssess.getMatRateAmt() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatRateAmt())
                        .subtract(gaiaMaterialDoc.getMatRateMov() == null ? BigDecimal.ZERO : gaiaMaterialDoc.getMatRateMov()));
                // 移动平均价 = 总金额/总数量
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                } else {
                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                }
                gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                // 加点计算
                if (flg) {
                    subtractMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess);
                }
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
                }
                gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
            }
        }
        /**
         * 财务接口
         */
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 自用
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByZD(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap) {
        FeignResult result = new FeignResult();
        if (StringUtils.isBlank(info.getMatBatch())) {
            result.setCode(1001);
            result.setMessage("批次不能为空");
            return result;
        }

        // 数量
        BigDecimal matQty = info.getMatQty();
        GaiaMaterialAssessKey storeAssess = new GaiaMaterialAssessKey();
        storeAssess.setClient(info.getClient());   // 加盟商
        storeAssess.setMatProCode(info.getMatProCode());   // 商品编码
        storeAssess.setMatAssessSite(info.getMatSiteCode()); // 门店
        GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(storeAssess);
        if (gaiaMaterialAssess == null) {
            result.setCode(1001);
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatSiteCode(), info.getMatProCode());
            if (gaiaProductBusiness == null) {
                result.setMessage("商品没有成本记录，无法过账");
            } else {
                result.setMessage("商品" + info.getMatProCode() + "_" + gaiaProductBusiness.getProName() + "没有成本记录，无法过账");
            }
            return result;
        }

        GaiaBatchInfoKey gaiaBatchInfoKey = new GaiaBatchInfoKey();
        gaiaBatchInfoKey.setClient(info.getClient());
        gaiaBatchInfoKey.setBatSiteCode(info.getMatSiteCode());
        gaiaBatchInfoKey.setBatProCode(info.getMatProCode());
        gaiaBatchInfoKey.setBatBatch(info.getMatBatch());
        GaiaBatchInfo gaiaBatchInfo = gaiaBatchInfoMapper.selectForMdoc(gaiaBatchInfoKey);
        if (gaiaBatchInfo == null) {
            throw new FeignResultException(1002, "批次信息表不存在");
        }
        // 价格
        BigDecimal price = this.getPrice(info);
        // 税率
        BigDecimal tax = this.getInputTax(gaiaBatchInfo.getClient(), gaiaBatchInfo.getBatPoId(), gaiaBatchInfo.getBatPoLineno(), gaiaBatchInfo.getBatSiteCode(), gaiaBatchInfo.getBatProCode());
        GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
        // 物料凭证数量
        gaiaMaterialDoc.setMatQty(matQty);
        // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
        gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
        // 总金额（批次） = 交易价格*交易数量-税金(批次)
        gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
        // 总金额（移动）= 取原物料评估表中的总金额/总数量*发生数；如果总金额、总数量任一为0，则直接取原移动平均单价*发生数。
        if (gaiaMaterialAssess.getMatTotalAmt() == null || gaiaMaterialAssess.getMatTotalAmt().compareTo(BigDecimal.ZERO) == 0 ||
                gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).setScale(2, RoundingMode.DOWN));
        } else {
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
        }
        // 借/贷标识
        gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S
        // 税金（移动）= 税率 * 总金额（移动）
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            gaiaMaterialDoc.setMatRateMov(tax.multiply(gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
        } else {
            // 评估表 税金/评估表 数量 * 交易数量。
            gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
        }
        // 加点计算
        subtractMaterialDoc(gaiaMaterialDoc, gaiaMaterialAssess, tax, matQty);
        gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

        // 若物料凭证数量等于总库存，则总库存、总金额的值全部清空成0
        if (gaiaMaterialDoc.getMatQty().compareTo(gaiaMaterialAssess.getMatTotalQty()) == 0) {
            gaiaMaterialAssess.setMatTotalQty(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
        } else {
            gaiaMaterialAssess.setMatTotalAmt(this.subtract(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
            //  物料凭证数量作为扣减数量
            gaiaMaterialAssess.setMatTotalQty(gaiaMaterialAssess.getMatTotalQty().subtract(gaiaMaterialDoc.getMatQty()));
        }
        // 税金 = 税金 + 税金（移动）
        gaiaMaterialAssess.setMatRateAmt((gaiaMaterialAssess.getMatRateAmt() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatRateAmt())
                .subtract(gaiaMaterialDoc.getMatRateMov() == null ? BigDecimal.ZERO : gaiaMaterialDoc.getMatRateMov()));
        // 移动平均价 = 总金额/总数量
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
        } else {
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
        }
        gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
        gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
        // 加点计算
        subtractMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess);
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
        }
        gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);

        /**
         * 财务接口
         */

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 报损
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByBD(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap) {
        FeignResult result = new FeignResult();
        if (StringUtils.isBlank(info.getMatBatch())) {
            result.setCode(1001);
            result.setMessage("批次不能为空");
            return result;
        }
        if (info.getMatPrice() == null) {
            result.setCode(1001);
            result.setMessage("交易价格不能为空");
            return result;
        }
        // 税率
        BigDecimal tax = BigDecimal.ZERO;
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        GaiaMaterialAssessKey storeAssess = new GaiaMaterialAssessKey();
        storeAssess.setClient(info.getClient());   // 加盟商
        storeAssess.setMatProCode(info.getMatProCode());   // 商品编码
        storeAssess.setMatAssessSite(info.getMatSiteCode()); // 门店
        GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(storeAssess);
        if (gaiaMaterialAssess == null) {
            result.setCode(1001);
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatSiteCode(), info.getMatProCode());
            if (gaiaProductBusiness == null) {
                result.setMessage("商品没有成本记录，无法过账");
            } else {
                result.setMessage("商品" + info.getMatProCode() + "_" + gaiaProductBusiness.getProName() + "没有成本记录，无法过账");
            }
            return result;
        }

        GaiaBatchInfoKey gaiaBatchInfoKey = new GaiaBatchInfoKey();
        gaiaBatchInfoKey.setClient(info.getClient());
        gaiaBatchInfoKey.setBatSiteCode(info.getMatSiteCode());
        gaiaBatchInfoKey.setBatProCode(info.getMatProCode());
        gaiaBatchInfoKey.setBatBatch(info.getMatBatch());
        GaiaBatchInfo gaiaBatchInfo = gaiaBatchInfoMapper.selectForMdoc(gaiaBatchInfoKey);
        if (gaiaBatchInfo == null) {
            throw new FeignResultException(1002, "批次信息表不存在");
        }
        // 门店主数据
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(info.getClient());
        gaiaStoreDataKey.setStoCode(info.getMatSiteCode());
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);

        // 税率
        tax = this.getInputTax(gaiaBatchInfo.getClient(), gaiaBatchInfo.getBatPoId(), gaiaBatchInfo.getBatPoLineno(), gaiaBatchInfo.getBatSiteCode(), gaiaBatchInfo.getBatProCode());
        // 价格
        BigDecimal price = this.getPrice(info);
        // 物料凭证生成
        GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
        // 物料凭证数量
        gaiaMaterialDoc.setMatQty(matQty);
        // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
        gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
        // 总金额（批次） = 交易价格*交易数量-税金(批次)
        gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
        // 总金额（移动）= 取原物料评估表中的总金额/总数量*发生数；如果总金额、总数量任一为0，则直接取原移动平均单价*发生数。
        if (gaiaMaterialAssess.getMatTotalAmt() == null || gaiaMaterialAssess.getMatTotalAmt().compareTo(BigDecimal.ZERO) == 0 ||
                gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).setScale(2, RoundingMode.DOWN));
        } else {
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
        }
        // 借/贷标识
        gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S
        // 税金（移动）= 税率 * 总金额（移动）
//        gaiaMaterialDoc.setMatRateMov(tax.multiply(gaiaMaterialDoc.getMatMovAmt()));
        // 物料评估税金/物料评估总数量 * 交易数据
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            // 总金额（移动） * 税
            gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatMovAmt().multiply(tax).setScale(2, RoundingMode.DOWN));
        } else {
            gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
        }
        // 加点计算
        if (gaiaStoreData != null) {
            subtractMaterialDoc(gaiaMaterialDoc, gaiaMaterialAssess, tax, matQty);
        }
        gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

        // 若物料凭证数量等于总库存，则总库存、总金额的值全部清空成0
        if (gaiaMaterialDoc.getMatQty().compareTo(gaiaMaterialAssess.getMatTotalQty()) == 0) {
            gaiaMaterialAssess.setMatTotalQty(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
        } else {
            gaiaMaterialAssess.setMatTotalAmt(this.subtract(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
            //  物料凭证数量作为扣减数量
            gaiaMaterialAssess.setMatTotalQty(gaiaMaterialAssess.getMatTotalQty().subtract(gaiaMaterialDoc.getMatQty()));
        }
        // 税金 = 税金 + 税金（移动）
        gaiaMaterialAssess.setMatRateAmt((gaiaMaterialAssess.getMatRateAmt() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatRateAmt())
                .subtract(gaiaMaterialDoc.getMatRateMov() == null ? BigDecimal.ZERO : gaiaMaterialDoc.getMatRateMov()));
        // 移动平均价 = 总金额/总数量
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
        } else {
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
        }
        gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
        gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
        // 加点计算
        if (gaiaStoreData != null) {
            subtractMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess);
        }
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
        }
        gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);

        /**
         * 财务接口
         */

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 互调配送
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByND(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap,
                                              Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap, List<GaiaProfitLevel> gaiaProfitLevelList) {
        FeignResult result = new FeignResult();
        if (StringUtils.isBlank(info.getMatStoCode())) {
            result.setCode(1001);
            result.setMessage("门店不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatBatch())) {
            result.setCode(1001);
            result.setMessage("批次不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoId())) {
            result.setCode(1001);
            result.setMessage("订单号不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoLineno())) {
            result.setCode(1001);
            result.setMessage("订单行号不能为空");
            return result;
        }
        if (info.getMatPrice() == null) {
            result.setCode(1001);
            result.setMessage("交易价格不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatDnId())) {
            result.setCode(1001);
            result.setMessage("业务ID不能为空");
            return result;
        }
        if (info.getMatHdPrice() == null) {
            result.setCode(1001);
            result.setMessage("互调价不能为空");
            return result;
        }

        // 总金额（移动）
        BigDecimal matMovAmt = null;
        //税金（移动）
        BigDecimal matRateMov = null;
        {
            /**
             * step 1: 仓库发货凭证 生成
             */
            // 税率
            BigDecimal inputTax = getInputTax(info);
            // 数量
            BigDecimal matQty = info.getMatQty();
            // 价格
            BigDecimal price = info.getMatPrice();

            GaiaWmsTkshM gaiaWmsTkshM = new GaiaWmsTkshM();
            gaiaWmsTkshM.setClient(info.getClient());
            gaiaWmsTkshM.setWmTkdh("MY" + info.getMatDnId().substring(2, info.getMatDnId().length()));
            gaiaWmsTkshM.setWmSpBm(info.getMatProCode());
            gaiaWmsTkshM.setWmPch(info.getMatBatch());
            GaiaWmsTkshM tkshm = gaiaWmsTkshMMapper.selectWmsTkshM(gaiaWmsTkshM);
            if (tkshm == null) {
                throw new FeignResultException(1008, "退库收货明细不存在");
            }
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            gaiaMaterialDoc.setMatDebitCredit("H");  // 借-减库存
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 移动总金额、移动总税额：0
            mdNdMov(gaiaMaterialDoc, info.getClient(), info.getMatStoCode(), info.getMatProCode(), info.getMatHdPrice(),
                    matQty, inputTax,
                    gaiaAllotPriceMap);
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);
            matMovAmt = gaiaMaterialDoc.getMatMovAmt();
            matRateMov = gaiaMaterialDoc.getMatRateMov();
        }
        {
            // 税率
            BigDecimal inputTax = getInputTax(info);
            // 数量
            BigDecimal matQty = info.getMatQty();
            // 价格
            BigDecimal price = info.getMatPrice();

            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            // 配送门店的地点编码
            gaiaMaterialDoc.setMatSiteCode(info.getMatStoCode());
            gaiaMaterialDoc.setMatLocationCode(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(0).getValue());  // 默认1000-合格品库
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("S"); // 减库存-H；加库存-S
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 总金额（移动）= 总金额（批次）
            gaiaMaterialDoc.setMatMovAmt(matMovAmt);
            // 「税金（移动）」=「税金（批次）」 → 移动税金 * 税率
            gaiaMaterialDoc.setMatRateMov(matRateMov);
            // 加点计算
            // 【 交易价格*交易数量】/（1+税率）=加点后金额
            gaiaMaterialDoc.setMatAddAmt(
                    (info.getMatPrice().multiply(matQty)).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN)
            );
            // 【 交易价格*交易数量】-加点后金额=加点后税额
            gaiaMaterialDoc.setMatAddTax(
                    this.subtract(info.getMatPrice().multiply(matQty), gaiaMaterialDoc.getMatAddAmt()).setScale(2, RoundingMode.DOWN)
            );
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

            // 移动平均价
            BigDecimal matMovPrice = BigDecimal.ZERO;
            // 根据加盟商+商品编码+发货地点到物料评估表中过滤出数据
            GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
            assessKey.setClient(info.getClient());   // 加盟商
            assessKey.setMatProCode(info.getMatProCode());   // 商品编码
            assessKey.setMatAssessSite(info.getMatStoCode()); // 门店
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
            if (gaiaMaterialAssess == null) {
                gaiaMaterialAssess = new GaiaMaterialAssess();
                // 加盟商
                gaiaMaterialAssess.setClient(info.getClient());
                // 商品编码
                gaiaMaterialAssess.setMatProCode(info.getMatProCode());
                // 估价地点
                gaiaMaterialAssess.setMatAssessSite(info.getMatStoCode());
                // 总库存 = 物料凭证数量
                gaiaMaterialAssess.setMatTotalQty(gaiaMaterialDoc.getMatQty());
                // 总金额 = 总金额（移动）
                gaiaMaterialAssess.setMatTotalAmt(gaiaMaterialDoc.getMatMovAmt());
                // 移动平均价 = 总金额/总数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                // 税金 = 交易价格*交易数量/(1+税率)*税率
                gaiaMaterialAssess.setMatRateAmt(gaiaMaterialDoc.getMatRateMov());
                gaiaMaterialAssess.setBatCreateUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatCreateDate(info.getMatCreateDate());
                gaiaMaterialAssess.setBatCreateTime(info.getMatCreateTime());
                // 加点计算
                addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatStoCode(), info.getMatProCode(), inputTax, matQty, gaiaAllotPriceMap);
                gaiaMaterialAssessMapper.insert(gaiaMaterialAssess);
                matMovPrice = gaiaMaterialAssess.getMatMovPrice();
            } else {
                // 总金额
                gaiaMaterialAssess.setMatTotalAmt(this.add(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
                //  物料凭证数量作为扣减数量
                gaiaMaterialAssess.setMatTotalQty(this.add(gaiaMaterialAssess.getMatTotalQty(), gaiaMaterialDoc.getMatQty()));
                // 税金 = 税金 + 税金（移动）
                gaiaMaterialAssess.setMatRateAmt((gaiaMaterialAssess.getMatRateAmt() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatRateAmt())
                        .add(gaiaMaterialDoc.getMatRateMov() == null ? BigDecimal.ZERO : gaiaMaterialDoc.getMatRateMov()));
                // 移动平均价 = 总金额/总数量
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                } else {
                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                }

                gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                // 加点计算
                addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatStoCode(), info.getMatProCode(), inputTax, matQty, gaiaAllotPriceMap);
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
                }
                gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
                matMovPrice = gaiaMaterialAssess.getMatMovPrice();
            }
            // 毛利级别更新
            baseService.setProProfit(info.getClient(), info.getMatStoCode(), info.getMatProCode(), this.gaiaTaxCodeList, null, null, null, null, gaiaProfitLevelList);

            /**
             * step 6: 更新wms 补货明细表GAIA_SD_REPLENISH_D 和 调拨开单明细表GAIA_WMS_DIAOBO_M
             */

            log.info("更新GAIA_SD_REPLENISH_D");
            GaiaSdReplenishD gaiaSdReplenishD = new GaiaSdReplenishD();
            gaiaSdReplenishD.setClient(info.getClient());
            gaiaSdReplenishD.setGsrdVoucherId(info.getMatPoId());
            gaiaSdReplenishD.setGsrdSerial(info.getMatPoLineno());
            gaiaSdReplenishD.setGsrdProId(info.getMatProCode());
            gaiaSdReplenishD.setGsrdVoucherAmt(matMovPrice);
            gaiaSdReplenishDMapper.updateVoucherAmt(gaiaSdReplenishD);

            log.info("更新GAIA_WMS_DIAOBO_M");
            GaiaWmsDiaoboM gaiaWmsDiaoboM = new GaiaWmsDiaoboM();
            gaiaWmsDiaoboM.setClient(info.getClient());
            gaiaWmsDiaoboM.setWmPsdh(info.getMatPoId());
            gaiaWmsDiaoboM.setWmSpBm(info.getMatProCode());
            gaiaWmsDiaoboM.setWmCkj(matMovPrice);
            gaiaWmsDiaoboMMapper.updateCkj(gaiaWmsDiaoboM);
        }

        /**
         * step 7：调用财务接口
         */

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 互调退库
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByMD(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap,
                                              Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap) {
        FeignResult result = new FeignResult();
        if (StringUtils.isBlank(info.getMatStoCode())) {
            result.setCode(1001);
            result.setMessage("门店不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatBatch())) {
            result.setCode(1001);
            result.setMessage("批次不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoId())) {
            result.setCode(1001);
            result.setMessage("订单号不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoLineno())) {
            result.setCode(1001);
            result.setMessage("订单行号不能为空");
            return result;
        }
        if (info.getMatPrice() == null) {
            result.setCode(1001);
            result.setMessage("交易价格不能为空");
            return result;
        }
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        // 价格
        BigDecimal price = info.getMatPrice();
        // 税率
        BigDecimal tax = getInputTax(info);
        // 总金额（移动）
        BigDecimal matMovAmt = BigDecimal.ZERO;
        //税金（移动）
        BigDecimal matRateMov = BigDecimal.ZERO;
        {
            GaiaMaterialAssessKey storeAssess = new GaiaMaterialAssessKey();
            storeAssess.setClient(info.getClient());   // 加盟商
            storeAssess.setMatProCode(info.getMatProCode());   // 商品编码
            storeAssess.setMatAssessSite(info.getMatStoCode()); // 门店
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(storeAssess);
            if (gaiaMaterialAssess == null) {
                result.setCode(1001);
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatStoCode(), info.getMatProCode());
                if (gaiaProductBusiness == null) {
                    result.setMessage("商品没有成本记录，无法过账");
                } else {
                    result.setMessage("商品" + info.getMatProCode() + "_" + gaiaProductBusiness.getProName() + "没有成本记录，无法过账");
                }
                return result;
            }
            // 门店凭证
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            gaiaMaterialDoc.setMatSiteCode(info.getMatStoCode());  // 门店
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 总金额（移动）= 取原物料评估表中的总金额/总数量*发生数；如果总金额、总数量任一为0，则直接取原移动平均单价*发生数。
            if (gaiaMaterialAssess.getMatTotalAmt() == null || gaiaMaterialAssess.getMatTotalAmt().compareTo(BigDecimal.ZERO) == 0 ||
                    gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).setScale(2, RoundingMode.DOWN));
            } else {
                gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
            }
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S
            // 税金（移动）= 税率 * 总金额（移动）
            // gaiaMaterialDoc.setMatRateMov(tax.multiply(gaiaMaterialDoc.getMatMovAmt()));
            // 物料评估税金/物料评估总数量*交易数据 →　物料评估税金/物料评估总数量 * 交易数据
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                // 总金额（移动） * 税
                gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatMovAmt().multiply(tax).setScale(2, RoundingMode.DOWN));
            } else {
                gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
            }
            // 加点计算
            subtractMaterialDoc(gaiaMaterialDoc, gaiaMaterialAssess, tax, matQty);
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);
            matMovAmt = gaiaMaterialDoc.getMatMovAmt();
            matRateMov = gaiaMaterialDoc.getMatRateMov();

            // 若物料凭证数量等于总库存，则总库存、总金额的值全部清空成0
            if (gaiaMaterialDoc.getMatQty().compareTo(gaiaMaterialAssess.getMatTotalQty()) == 0) {
                gaiaMaterialAssess.setMatTotalQty(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
            } else {
                gaiaMaterialAssess.setMatTotalAmt(this.subtract(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
                //  物料凭证数量作为扣减数量
                gaiaMaterialAssess.setMatTotalQty(gaiaMaterialAssess.getMatTotalQty().subtract(gaiaMaterialDoc.getMatQty()));
            }
            // 税金 = 税金 + 税金（移动）
            gaiaMaterialAssess.setMatRateAmt((gaiaMaterialAssess.getMatRateAmt() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatRateAmt())
                    .subtract(gaiaMaterialDoc.getMatRateMov() == null ? BigDecimal.ZERO : gaiaMaterialDoc.getMatRateMov()));
            // 移动平均价 = 总金额/总数量
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
            } else {
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
            }
            gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
            gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
            // 加点计算
            subtractMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess);
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
            }
            gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
        }

        {
            // 生成物料凭证，更新物料凭证表GAIA_MATERIAL_DOC
            // 物料凭证生成
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("S"); // 减库存-H；加库存-S
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));

            mdNdMov(gaiaMaterialDoc, info.getClient(), info.getMatStoCode(), info.getMatProCode(), price,
                    matQty, tax,
                    gaiaAllotPriceMap);
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

        }

        /**
         * step 4: 调用财务接口
         */
        result.setCode(0);
        result.setMessage("执行成功");
        return result;

    }

    /**
     * 移动总金额、移动总税额 计算
     *
     * @param gaiaMaterialDoc
     * @param price
     * @param gaiaAllotPriceMap
     */
    private void mdNdMov(GaiaMaterialDoc gaiaMaterialDoc, String client, String proSite, String proCode, BigDecimal price,
                         BigDecimal matQty, BigDecimal inputTax,
                         Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap) {
        // 调拨价格
        GaiaAllotPrice gaiaAllotPrice = getGaiaAllotPrice(gaiaAllotPriceMap, client, proSite, proCode);
        if (gaiaAllotPrice == null) {
            // 总金额（移动）= price * 交易数量/(1+税率)
            gaiaMaterialDoc.setMatMovAmt((price.multiply(matQty)).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
            // 税金（移动）
            gaiaMaterialDoc.setMatRateMov(this.subtract(price.multiply(matQty), gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
        } else {
            // 加价金额
            BigDecimal alpAddAmt = gaiaAllotPrice.getAlpAddAmt();
            //加价比例
            BigDecimal alpAddRate = gaiaAllotPrice.getAlpAddRate();
            // 目录价
            BigDecimal alpCataloguePrice = gaiaAllotPrice.getAlpCataloguePrice();
            // 移动总金额=目录价 * 数量 - 税金（批次）
            if (alpCataloguePrice != null) {
                gaiaMaterialDoc.setMatMovAmt(this.subtract(alpCataloguePrice.multiply(matQty), gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
                // 目录价 * 数量 - 移动总金额
                gaiaMaterialDoc.setMatRateMov(this.subtract(alpCataloguePrice.multiply(matQty), gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
            }
            // 移动总金额=price/(1+加点率)  * 交易数量   /（1+税率）
            if (alpAddRate != null) {
                gaiaMaterialDoc.setMatMovAmt(
                        price.multiply(matQty).divide(BigDecimal.ONE.add(alpAddRate.divide(new BigDecimal(100))), 4, RoundingMode.HALF_UP)
                                .divide(BigDecimal.ONE.add(inputTax), 2, RoundingMode.DOWN)
                );
                // 移动总税额= price/(1+加点率)   * 交易数量     - 移动总金额
                gaiaMaterialDoc.setMatRateMov(
                        this.subtract(price.multiply(matQty).divide(BigDecimal.ONE.add(alpAddRate.divide(new BigDecimal(100))), 4, RoundingMode.HALF_UP),
                                gaiaMaterialDoc.getMatMovAmt()
                        ).setScale(2, RoundingMode.DOWN)
                );
            }
            // 移动总金额=(price- 加点金额) * 数量 - 税金批次
            if (alpAddAmt != null) {
                gaiaMaterialDoc.setMatMovAmt(this.subtract(this.subtract(price, alpAddAmt).multiply(matQty), gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
                gaiaMaterialDoc.setMatRateMov(this.subtract(this.subtract(price, alpAddAmt).multiply(matQty), gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
            }
        }
    }

    /**
     * 零售
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByLS(MaterialDocRequestDto info, Map<String, GaiaSdSaleD> gaiaSdSaleDMapLs, Map<String, String> matIdMap,
                                              Map<String, Integer> indexMap, Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap, List<GaiaProfitLevel> gaiaProfitLevelList) {
        FeignResult result = new FeignResult();
        if (StringUtils.isBlank(info.getMatPoId())) {
            result.setCode(1001);
            result.setMessage("订单号不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoLineno())) {
            result.setCode(1001);
            result.setMessage("订单行号不能为空");
            return result;
        }
        if (info.getMatQty() == null) {
            result.setCode(1001);
            result.setMessage("交易数量不能为空");
            return result;
        }
        // 交易数量
        BigDecimal matQty = info.getMatQty().abs();
        // 税率
        BigDecimal tax = getInputTax3(info);
        // 交易价格
        BigDecimal price = this.getPrice(info);
        // 移动价
        BigDecimal matMovAmt = BigDecimal.ZERO;
        // 税额
        BigDecimal matRateMov = BigDecimal.ZERO;
        // 移动平均价
        BigDecimal gssdMovPrice = BigDecimal.ZERO;
        // 加点后金额
        BigDecimal matAddAmt = BigDecimal.ZERO;
        // 加点后税金
        BigDecimal matAddTax = BigDecimal.ZERO;
        // 增加逻辑 收货
        if (info.getMatQty().compareTo(BigDecimal.ZERO) > 0) {
            // 物料凭证生成
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 总金额（移动）= 总金额（批次）
            // 11.12新增。LS退货（负数 加库存）业务的移动价的逻辑取值调整。
            // 根据传入的加盟商+ PO_ID（PO_ID=GSSH_BILL_NO）到GAIA_SD_SALE_H中找到「退货原销售单号GSSH_BILL_NO_RETURN」；
            // 再根据加盟商+原销售单号（=MAT_PO_ID）+商品（=MAT_PRO_CODE）到GAIA_MATERIAL_DOC中找到「总金额MAT_MOV_AMT」和「总数量MAT_QTY」
            // 对比若传入的交易数量=「总数量MAT_QTY」则「总金额（移动）」=取出的「总金额MAT_MOV_AMT」；
            // 否则「总金额（移动）」=取出的「总金额MAT_MOV_AMT」/取出的「总数量MAT_QTY」*传入的交易数量
            GaiaMaterialDoc entity = gaiaMaterialDocMapper.selectMaterialDocByPoId(info.getClient(), info.getMatProCode(), info.getMatPoId());
            if (entity == null) {
                // 原逻辑
                gaiaMaterialDoc.setMatMovAmt(gaiaMaterialDoc.getMatBatAmt());
                gaiaMaterialDoc.setMatRateMov(tax.multiply(gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
                gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatRateMov());
                gaiaMaterialDoc.setMatAddAmt(gaiaMaterialDoc.getMatMovAmt());
            } else {
                // 对比若传入的交易数量=「总数量MAT_QTY」则「总金额（移动）」=取出的「总金额MAT_MOV_AMT」；
                if (info.getMatQty().compareTo(entity.getMatQty()) == 0) {
                    gaiaMaterialDoc.setMatMovAmt(entity.getMatMovAmt());
                    gaiaMaterialDoc.setMatRateMov(entity.getMatRateMov());
                    gaiaMaterialDoc.setMatAddTax(entity.getMatAddTax());
                    gaiaMaterialDoc.setMatAddAmt(entity.getMatAddAmt());
                } else {
                    if (entity.getMatQty() == null || entity.getMatQty().compareTo(BigDecimal.ZERO) == 0) {
                        gaiaMaterialDoc.setMatMovAmt(gaiaMaterialDoc.getMatBatAmt());
                        gaiaMaterialDoc.setMatRateMov(tax.multiply(gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
                        gaiaMaterialDoc.setMatAddTax(entity.getMatAddTax());
                        gaiaMaterialDoc.setMatAddAmt(entity.getMatAddAmt());
                    } else {
                        // 取出的「总金额MAT_MOV_AMT」/取出的「总数量MAT_QTY」*传入的交易数量
                        gaiaMaterialDoc.setMatMovAmt(entity.getMatMovAmt().multiply(info.getMatQty()).divide(entity.getMatQty(), 2, RoundingMode.DOWN));
                        gaiaMaterialDoc.setMatRateMov(entity.getMatRateMov().multiply(info.getMatQty()).divide(entity.getMatQty(), 2, RoundingMode.DOWN));
                        gaiaMaterialDoc.setMatAddTax(entity.getMatAddTax().multiply(info.getMatQty()).divide(entity.getMatQty(), 2, RoundingMode.DOWN));
                        gaiaMaterialDoc.setMatAddAmt(entity.getMatAddAmt().multiply(info.getMatQty()).divide(entity.getMatQty(), 2, RoundingMode.DOWN));
                    }
                }
            }
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("S"); // 减库存-H；加库存-S
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);
            matMovAmt = gaiaMaterialDoc.getMatMovAmt();
            matRateMov = gaiaMaterialDoc.getMatRateMov();
            matAddAmt = gaiaMaterialDoc.getMatAddAmt();
            matAddTax = gaiaMaterialDoc.getMatAddTax();
            // 根据加盟商+商品编码+收货地点到物料评估表中过滤数据
            GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
            assessKey.setClient(info.getClient());   // 加盟商
            assessKey.setMatProCode(info.getMatProCode());   // 商品编码
            assessKey.setMatAssessSite(info.getMatSiteCode()); // 门店
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
            // 若无数据直接插入
            if (gaiaMaterialAssess == null) {
                gaiaMaterialAssess = new GaiaMaterialAssess();
                // 加盟商
                gaiaMaterialAssess.setClient(info.getClient());
                // 商品编码
                gaiaMaterialAssess.setMatProCode(info.getMatProCode());
                // 估价地点
                gaiaMaterialAssess.setMatAssessSite(info.getMatSiteCode());
                // 总库存 = 物料凭证数量
                gaiaMaterialAssess.setMatTotalQty(gaiaMaterialDoc.getMatQty());
                // 总金额 = 总金额（移动）
                gaiaMaterialAssess.setMatTotalAmt(gaiaMaterialDoc.getMatMovAmt());
                // 移动平均价 = 总金额/总数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                // 税金 = 交易价格*交易数量/(1+税率)*税率
                gaiaMaterialAssess.setMatRateAmt(gaiaMaterialDoc.getMatRateMov());
                // 创建日期
                gaiaMaterialAssess.setBatCreateDate(info.getMatCreateDate());
                // 创建时间
                gaiaMaterialAssess.setBatCreateTime(info.getMatCreateTime());
                // 创建人
                gaiaMaterialAssess.setBatCreateUser(info.getMatCreateBy());
                // 加点计算
                addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatSiteCode(), info.getMatProCode(), tax, matQty, gaiaAllotPriceMap);
                gaiaMaterialAssessMapper.insert(gaiaMaterialAssess);
                gssdMovPrice = gaiaMaterialAssess.getMatMovPrice();
            } else {
                log.info("物料评估原数据：{}", JsonUtils.beanToJson(gaiaMaterialAssess));
                // 「总库存」即为原有数量+物料凭证数量，
                gaiaMaterialAssess.setMatTotalQty(this.add(gaiaMaterialAssess.getMatTotalQty(), gaiaMaterialDoc.getMatQty()));
                // 「总金额」为原有金额+物料凭证中的「总金额（移动）」的值
                gaiaMaterialAssess.setMatTotalAmt(this.add(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
                // 税金
                gaiaMaterialAssess.setMatRateAmt(this.add(gaiaMaterialAssess.getMatRateAmt(), gaiaMaterialDoc.getMatRateMov()));
                // 总库存 = 0
                if (gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                    // 物料凭证LS的 过账逻辑里，如果QTY是正数时，不更新MAT_MOV_PRICE 2022年1月10日
//                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(matQty, 4, RoundingMode.HALF_UP));
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
                } else {
                    // 移动平均价 = 总金额/总数量
                    // 物料凭证LS的 过账逻辑里，如果QTY是正数时，不更新MAT_MOV_PRICE 2022年1月10日
//                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                }
                gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
                } else {
                    // 加点计算
                    addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatSiteCode(), info.getMatProCode(), tax, matQty, gaiaAllotPriceMap);
                }

                gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
                gssdMovPrice = gaiaMaterialAssess.getMatMovPrice();
            }
            // 毛利级别更新
            baseService.setProProfit(info.getClient(), info.getMatSiteCode(), info.getMatProCode(), this.gaiaTaxCodeList, null, null, null, null, gaiaProfitLevelList);
        } else { // 增加逻辑 发货
            // 根据加盟商+商品编码+发货地点到物料评估表中过滤出数据
            GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
            assessKey.setClient(info.getClient());   // 加盟商
            assessKey.setMatProCode(info.getMatProCode());   // 商品编码
            assessKey.setMatAssessSite(info.getMatSiteCode()); // 门店
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
            if (gaiaMaterialAssess == null) {
                result.setCode(1001);
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatSiteCode(), info.getMatProCode());
                if (gaiaProductBusiness == null) {
                    result.setMessage("商品没有成本记录，无法过账");
                } else {
                    result.setMessage("商品" + info.getMatProCode() + "_" + gaiaProductBusiness.getProName() + "没有成本记录，无法过账");
                }
                return result;
            } else {
                log.info("物料凭证表原数据;{}", JsonUtils.beanToJson(gaiaMaterialAssess));

                // 物料凭证生成
                GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
                // 物料凭证数量
                gaiaMaterialDoc.setMatQty(matQty);
                // 借/贷标识
                gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；

                // 物料凭估表库存 为 正
                if (gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) > 0 && gaiaMaterialAssess.getMatTotalAmt().compareTo(BigDecimal.ZERO) > 0) {
                    // 交易数据小于等于库存数
                    if (matQty.compareTo(gaiaMaterialAssess.getMatTotalQty()) <= 0) {
                        // 交易数量等于库存
                        if (matQty.compareTo(gaiaMaterialAssess.getMatTotalQty()) == 0) {
                            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt());
                            // 税金（移动）
                            gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt());
                            // 加点后金额
                            gaiaMaterialDoc.setMatAddAmt(gaiaMaterialAssess.getMatAddAmt());
                            // 加点后税金
                            gaiaMaterialDoc.setMatAddTax(gaiaMaterialAssess.getMatAddTax());
                        } else {
                            // 总金额 * 交易数量 / 库存 保留2位小数，第三位舍掉
                            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                            //  税金（移动)
                            gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                            // 加点后金额
                            gaiaMaterialDoc.setMatAddAmt(gaiaMaterialAssess.getMatAddAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                            // 加点后税金
                            gaiaMaterialDoc.setMatAddTax(gaiaMaterialAssess.getMatAddTax().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                        }
                    } else { // 交易数量大于库存
                        BigDecimal diffQty = this.subtract(matQty, gaiaMaterialAssess.getMatTotalQty());
                        // 总金额 + 总金额 * (交易数量-库存) / 库存
                        BigDecimal docMatMovAmt = this.add(gaiaMaterialAssess.getMatTotalAmt(),
                                gaiaMaterialAssess.getMatTotalAmt().multiply(diffQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                        gaiaMaterialDoc.setMatMovAmt(docMatMovAmt);
                        //  税金（移动)
                        BigDecimal docMatRateMov = this.add(gaiaMaterialAssess.getMatRateAmt(),
                                gaiaMaterialAssess.getMatRateAmt().multiply(diffQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                        gaiaMaterialDoc.setMatRateMov(docMatRateMov);
                        // 加点后金额
                        BigDecimal docMatAddAmt = this.add(gaiaMaterialAssess.getMatAddAmt(),
                                gaiaMaterialAssess.getMatAddAmt().multiply(diffQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                        gaiaMaterialDoc.setMatAddAmt(docMatAddAmt);
                        // 加点后税金
                        BigDecimal docMatAddTax = this.add(gaiaMaterialAssess.getMatAddTax(),
                                gaiaMaterialAssess.getMatAddTax().multiply(diffQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                        gaiaMaterialDoc.setMatAddTax(docMatAddTax);
                    }
                } else { // 物料凭估表库存 为 负
                    // assessMovAmt - (assessQty - matQty) * assess.mov_price)
                    BigDecimal matMovAmtDoc = this.subtract(gaiaMaterialAssess.getMatTotalAmt(),
                            this.subtract(gaiaMaterialAssess.getMatTotalQty(), matQty).multiply(gaiaMaterialAssess.getMatMovPrice())).setScale(2, RoundingMode.DOWN);
                    gaiaMaterialDoc.setMatMovAmt(matMovAmtDoc);
                    //  税金（移动)
                    gaiaMaterialDoc.setMatRateMov(matMovAmtDoc.multiply(tax).setScale(2, RoundingMode.DOWN));
                    // 加点后金额
                    BigDecimal matAddAmtDoc = this.subtract(gaiaMaterialAssess.getMatAddAmt(),
                            this.subtract(gaiaMaterialAssess.getMatTotalQty(), matQty).multiply(gaiaMaterialAssess.getMatMovPrice())).setScale(2, RoundingMode.DOWN);
                    gaiaMaterialDoc.setMatAddAmt(matAddAmtDoc);
                    // 加点后税金
                    gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatAddAmt().multiply(tax).setScale(2, RoundingMode.DOWN));
                }
                // 税金（批次） = 交易价格 * 交易数量 / (1+税率)*税率
                gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
                // 总金额（批次） = 交易价格*交易数量-税金(批次)
                gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
                gaiaMaterialDocMapper.insert(gaiaMaterialDoc);
                matMovAmt = gaiaMaterialDoc.getMatMovAmt();
                matRateMov = gaiaMaterialDoc.getMatRateMov();
                matAddAmt = gaiaMaterialDoc.getMatAddAmt();
                matAddTax = gaiaMaterialDoc.getMatAddTax();

                // 总金额
                gaiaMaterialAssess.setMatTotalAmt(this.subtract(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
                // 物料凭证数量作为扣减数量
                gaiaMaterialAssess.setMatTotalQty(this.subtract(gaiaMaterialAssess.getMatTotalQty(), gaiaMaterialDoc.getMatQty()));
                // 税金 = 税金 - 税金（移动）
                gaiaMaterialAssess.setMatRateAmt(this.subtract(gaiaMaterialAssess.getMatRateAmt(), gaiaMaterialDoc.getMatRateMov()));
                // 加点计算
                subtractMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess);

                gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
                gssdMovPrice = gaiaMaterialAssess.getMatMovPrice();
            }
        }

        // 再根据加盟商+销售单号（物料凭证接口传入的业务单号MAT_DN_ID）+店号（物料凭证接口传入的地点MAT_SITE_CODE）+行号（物料凭证接口传入的业务单行号MAT_DN_LINENO）+销售日期（物料凭证接口传入的业务单行号MAT_POST_DATE）
        if (StringUtils.isNotBlank(info.getClient()) &&
                StringUtils.isNotBlank(info.getMatDnId()) &&
                StringUtils.isNotBlank(info.getMatSiteCode()) &&
                StringUtils.isNotBlank(info.getMatDnLineno()) &&
                StringUtils.isNotBlank(info.getMatPostDate())) {
            String key = info.getClient() + "_" + info.getMatDnId() + "_" + info.getMatSiteCode() + "_" + info.getMatDnLineno() + "_" + info.getMatPostDate();
            GaiaSdSaleD gaiaSdSaleD = gaiaSdSaleDMapLs.get(key);
            if (gaiaSdSaleD == null) {
                gaiaSdSaleD = new GaiaSdSaleD();
                // 加盟商
                gaiaSdSaleD.setClient(info.getClient());
                // 销售单号
                gaiaSdSaleD.setGssdBillNo(info.getMatDnId());
                // 店号
                gaiaSdSaleD.setGssdBrId(info.getMatSiteCode());
                // 销售日期
                gaiaSdSaleD.setGssdDate(info.getMatPostDate());
                // 行号
                gaiaSdSaleD.setGssdSerial(info.getMatDnLineno());
            }
            // 批次成本价
            BigDecimal gssdBatchCost = gaiaSdSaleD.getGssdBatchCost() == null ? BigDecimal.ZERO : gaiaSdSaleD.getGssdBatchCost();
            // 物料凭证中的总金额（移动）
            gaiaSdSaleD.setGssdBatchCost(this.add(gssdBatchCost, matMovAmt));
            // 移动税额
            BigDecimal gssdTaxRate = gaiaSdSaleD.getGssdTaxRate() == null ? BigDecimal.ZERO : gaiaSdSaleD.getGssdTaxRate();
            gaiaSdSaleD.setGssdTaxRate(this.add(gssdTaxRate, matRateMov));
            // 移动平均单价
            gaiaSdSaleD.setGssdMovPrice(gssdMovPrice == null ? null : gssdMovPrice.setScale(4, BigDecimal.ROUND_HALF_UP));
            // 加点后金额
            BigDecimal gssdAddAmt = gaiaSdSaleD.getGssdAddAmt();
            gaiaSdSaleD.setGssdAddAmt(this.add(gssdAddAmt, matAddAmt));
            // 加点后税金
            BigDecimal gssdAddTax = gaiaSdSaleD.getGssdAddTax();
            gaiaSdSaleD.setGssdAddTax(this.add(gssdAddTax, matAddTax));
            gaiaSdSaleDMapLs.put(key, gaiaSdSaleD);
        }

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 退库
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByTD(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap) {
        FeignResult result = new FeignResult();

        if (StringUtils.isBlank(info.getMatStoCode())) {
            result.setCode(1001);
            result.setMessage("门店不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatSiteCode())) {
            result.setCode(1001);
            result.setMessage("仓库不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatBatch())) {
            result.setCode(1001);
            result.setMessage("批次不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoId())) {
            result.setCode(1001);
            result.setMessage("订单号不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoLineno())) {
            result.setCode(1001);
            result.setMessage("订单行号不能为空");
            return result;
        }
        if (info.getMatPrice() == null) {
            result.setCode(1001);
            result.setMessage("交易价格不能为空");
            return result;
        }
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        // 价格
        BigDecimal price = info.getMatPrice();
        // 税率
        BigDecimal tax = getInputTax2(MatTypeEnum.TD.getCode(), info.getClient(), info.getMatPoId(), info.getMatProCode(), info.getMatSiteCode());
        // 总金额（移动）
        BigDecimal matMovAmt = BigDecimal.ZERO;
        //税金（移动）
        BigDecimal matRateMov = BigDecimal.ZERO;
        {
            GaiaMaterialAssessKey storeAssess = new GaiaMaterialAssessKey();
            storeAssess.setClient(info.getClient());   // 加盟商
            storeAssess.setMatProCode(info.getMatProCode());   // 商品编码
            storeAssess.setMatAssessSite(info.getMatStoCode()); // 门店
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(storeAssess);
            if (gaiaMaterialAssess == null) {
                result.setCode(1001);
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatStoCode(), info.getMatProCode());
                if (gaiaProductBusiness == null) {
                    result.setMessage("商品没有成本记录，无法过账");
                } else {
                    result.setMessage("商品" + info.getMatProCode() + "_" + gaiaProductBusiness.getProName() + "没有成本记录，无法过账");
                }
                return result;
            }

            // 门店凭证
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            gaiaMaterialDoc.setMatSiteCode(info.getMatStoCode());  // 门店
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 总金额（移动）= 取原物料评估表中的总金额/总数量*发生数；如果总金额、总数量任一为0，则直接取原移动平均单价*发生数。
            if (gaiaMaterialAssess.getMatTotalAmt() == null || gaiaMaterialAssess.getMatTotalAmt().compareTo(BigDecimal.ZERO) == 0 ||
                    gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).setScale(2, RoundingMode.DOWN));
            } else {
                gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
            }
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S
            // 税金（移动）= 税率 * 总金额（移动）
//            gaiaMaterialDoc.setMatRateMov(tax.multiply(gaiaMaterialDoc.getMatMovAmt()));
            // 物料评估税金/物料评估总数量 * 交易数据
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                // 总金额（移动） * 税
                gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatMovAmt().multiply(tax).setScale(2, RoundingMode.DOWN));
            } else {
                gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
            }
            // 加点计算
            subtractMaterialDoc(gaiaMaterialDoc, gaiaMaterialAssess, tax, matQty);
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);
            matMovAmt = gaiaMaterialDoc.getMatMovAmt();
            matRateMov = gaiaMaterialDoc.getMatRateMov();

            // 若物料凭证数量等于总库存，则总库存、总金额的值全部清空成0
            if (gaiaMaterialDoc.getMatQty().compareTo(gaiaMaterialAssess.getMatTotalQty()) == 0) {
                gaiaMaterialAssess.setMatTotalQty(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
            } else {
                gaiaMaterialAssess.setMatTotalAmt(this.subtract(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
                //  物料凭证数量作为扣减数量
                gaiaMaterialAssess.setMatTotalQty(gaiaMaterialAssess.getMatTotalQty().subtract(gaiaMaterialDoc.getMatQty()));

                // 税金 = 税金 + 税金（移动）
                gaiaMaterialAssess.setMatRateAmt((gaiaMaterialAssess.getMatRateAmt() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatRateAmt())
                        .subtract(gaiaMaterialDoc.getMatRateMov() == null ? BigDecimal.ZERO : gaiaMaterialDoc.getMatRateMov()));
            }
            // 移动平均价 = 总金额/总数量
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
            } else {
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
            }
            gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
            gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
            // 加点计算
            subtractMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess);
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
            }
            gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
        }
        {
            // 单价
            // 先根据批号找配送单（有多条配送记录的，取时间最近的一条）。
            // 配送主表GAIA_WMS_DIAOBO_Z中的是否过账WM_SFGZ为1-已过帐，配送明细表GAIA_WMS_DIAOBO_M中 加盟商+商品+批次+过账数量WM_GZSL不为0。
            // 取 出库价WM_CKJ为含税单价，进行后续处理。
            BigDecimal wm_cbj = gaiaWmsDiaoboMMapper.selectCbj(info.getClient(), info.getMatBatch(), info.getMatProCode());
            if (wm_cbj != null) {
                price = wm_cbj;
            }

            // 生成物料凭证，更新物料凭证表GAIA_MATERIAL_DOC
            // 物料凭证生成
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            // 仓库
            gaiaMaterialDoc.setMatSiteCode(info.getMatSiteCode());
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("S"); // 减库存-H；加库存-S
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(tax).divide((BigDecimal.ONE.add(tax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            if (wm_cbj == null) {
                // 总金额（移动）= 总金额（批次）
                gaiaMaterialDoc.setMatMovAmt(matMovAmt);
                // 「税金（移动）」=「税金（批次）」 → 移动税金 * 税率
                gaiaMaterialDoc.setMatRateMov(matRateMov);
            } else {
                // 总金额移动 = 含税单价*交易数量/（1+0.13）
                gaiaMaterialDoc.setMatMovAmt(wm_cbj.multiply(matQty).divide(this.add(BigDecimal.ONE, tax), 2, RoundingMode.DOWN));
                // 移动税金 = 含税单价*交易数量 - 总金额移动
                gaiaMaterialDoc.setMatRateMov(this.subtract(wm_cbj.multiply(matQty), gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
            }
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

            // 更新物料评估表的移动平均价
            GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
            assessKey.setClient(info.getClient());   // 加盟商
            assessKey.setMatProCode(info.getMatProCode());   // 商品编码
            assessKey.setMatAssessSite(info.getMatSiteCode()); // 仓库
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
            if (gaiaMaterialAssess == null) {
                gaiaMaterialAssess = new GaiaMaterialAssess();
                // 加盟商
                gaiaMaterialAssess.setClient(info.getClient());
                // 商品编码
                gaiaMaterialAssess.setMatProCode(info.getMatProCode());
                // 估价地点
                gaiaMaterialAssess.setMatAssessSite(info.getMatSiteCode());
                // 总库存 = 物料凭证数量
                gaiaMaterialAssess.setMatTotalQty(gaiaMaterialDoc.getMatQty());
                // 总金额 = 总金额（移动）
                gaiaMaterialAssess.setMatTotalAmt(gaiaMaterialDoc.getMatMovAmt());
                // 移动平均价 = 总金额/总数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                // 税金 = 交易价格*交易数量/(1+税率)*税率
                gaiaMaterialAssess.setMatRateAmt(gaiaMaterialDoc.getMatRateMov());
                // 创建日期
                gaiaMaterialAssess.setBatCreateDate(info.getMatCreateDate());
                // 创建时间
                gaiaMaterialAssess.setBatCreateTime(info.getMatCreateTime());
                // 创建人
                gaiaMaterialAssess.setBatCreateUser(info.getMatCreateBy());
                gaiaMaterialAssessMapper.insert(gaiaMaterialAssess);
            } else {  // 有数据时
                // 「总库存」即为原有数量+物料凭证数量，
                gaiaMaterialAssess.setMatTotalQty(this.add(gaiaMaterialAssess.getMatTotalQty(), gaiaMaterialDoc.getMatQty()));
                // 「总金额」为原有金额+物料凭证中的「总金额（批次）」的值
                gaiaMaterialAssess.setMatTotalAmt(this.add(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatBatAmt()));
                // 税金
                gaiaMaterialAssess.setMatRateAmt(this.add(gaiaMaterialAssess.getMatRateAmt(), gaiaMaterialDoc.getMatRateMov()));
                // 移动平均价 = 总金额/总数量
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                } else {
                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                }
                gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
                }
                gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
            }
        }

        /**
         * step 4: 调用财务接口
         */
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * PD-配送（门店入）；ND-互调配送（门店入）；SY-损溢（门店入）
     *
     * @param gaiaMaterialDoc
     * @param client
     * @param proSite
     * @param proCode
     */
    private void addMaterialDoc(GaiaMaterialDoc gaiaMaterialDoc, String client, String proSite, String proCode, BigDecimal inputTax, BigDecimal matQty, Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap) {
        // 调拨价格
        GaiaAllotPrice gaiaAllotPrice = getGaiaAllotPrice(gaiaAllotPriceMap, client, proSite, proCode);
        if (gaiaAllotPrice == null) {
            // 加点后金额 = 总金额（移动）
            gaiaMaterialDoc.setMatAddAmt(gaiaMaterialDoc.getMatMovAmt());
            // 税额 = 总金额（移动） * 税率
            gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatAddAmt().multiply(inputTax).setScale(2, RoundingMode.DOWN));
        } else {
            // 加价金额
            BigDecimal alpAddAmt = gaiaAllotPrice.getAlpAddAmt();
            //加价比例
            BigDecimal alpAddRate = gaiaAllotPrice.getAlpAddRate();
            // 目录价
            BigDecimal alpCataloguePrice = gaiaAllotPrice.getAlpCataloguePrice();
            if (alpCataloguePrice != null) {
                gaiaMaterialDoc.setMatAddAmt(alpCataloguePrice);
                gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatAddAmt().multiply(inputTax).setScale(2, RoundingMode.DOWN));
            }
            // 加价比例
            if (alpAddRate != null) {
                //  物料凭证表- GAIA_MATERIAL_DOC
                // 加价比例： 加点后金额=  移动金额(总金额（移动）)*（1+加价比例/100）  加点后金额*税率=加点后税金
                gaiaMaterialDoc.setMatAddAmt(
                        gaiaMaterialDoc.getMatMovAmt().multiply(new BigDecimal(1).add(alpAddRate.divide(new BigDecimal(100)))).setScale(2, RoundingMode.DOWN)
                );
                gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatAddAmt().multiply(inputTax).setScale(2, RoundingMode.DOWN));
            }
            // 加价金额
            if (alpAddAmt != null) {
                //  物料凭证表- GAIA_MATERIAL_DOC
                // 加价金额：加点后金额= 总金额（移动） + （数量 * 加价金额）/(1+税率)     加点后税金 =加点后金额*税率
                gaiaMaterialDoc.setMatAddAmt(
                        gaiaMaterialDoc.getMatMovAmt().add((matQty.multiply(alpAddAmt)).divide(BigDecimal.ONE.add(inputTax), 2, RoundingMode.DOWN))
                );
                gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatAddAmt().multiply(inputTax).setScale(2, RoundingMode.DOWN));
            }
        }
    }

    /**
     * PD-配送（门店入）；ND-互调配送（门店入）；SY-损溢（门店入）
     */
    private void addMaterialAssess(GaiaMaterialDoc gaiaMaterialDoc, GaiaMaterialAssess gaiaMaterialAssess,
                                   String client, String proSite, String proCode, BigDecimal inputTax, BigDecimal matQty,
                                   Map<String, List<GaiaAllotPrice>> gaiaAllotPriceMap) {
        BigDecimal matAddAmt = gaiaMaterialAssess.getMatAddAmt();
        gaiaMaterialAssess.setMatAddAmt(this.add(matAddAmt, gaiaMaterialDoc.getMatAddAmt()));
        BigDecimal matAddTax = gaiaMaterialAssess.getMatAddTax();
        gaiaMaterialAssess.setMatAddTax(this.add(matAddTax, gaiaMaterialDoc.getMatAddTax()));
    }

    /**
     * TD-退库（门店出）；MD-互调（门店出）；BD-报损；ZD-自用；SY-损溢（门店出）
     *
     * @param gaiaMaterialDoc
     * @param inputTax
     * @param matQty
     */
    private void subtractMaterialDoc(GaiaMaterialDoc gaiaMaterialDoc, GaiaMaterialAssess gaiaMaterialAssess, BigDecimal inputTax, BigDecimal matQty) {
        // 加点后金额 = GAIA_MATERIAL_ASSESS.加价后金额/总数*发生数
        if (gaiaMaterialAssess.getMatAddAmt() == null) {
            gaiaMaterialDoc.setMatAddAmt(BigDecimal.ZERO);
        } else {
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                gaiaMaterialDoc.setMatAddAmt(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).setScale(2, RoundingMode.DOWN));
            } else {
                gaiaMaterialDoc.setMatAddAmt(gaiaMaterialAssess.getMatAddAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
            }
        }
        // 加点税金 = 交易数量/物料评估加点后税金 * 物料评估总数量
        if (gaiaMaterialAssess.getMatAddTax() == null) {
            gaiaMaterialDoc.setMatAddTax(BigDecimal.ZERO);
        } else {
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                // 移动平均价 * 交易数量 * 税率
                gaiaMaterialDoc.setMatAddTax(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).multiply(inputTax).setScale(2, RoundingMode.DOWN));
            } else {
                gaiaMaterialDoc.setMatAddTax(gaiaMaterialAssess.getMatAddTax().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
            }
        }
    }

    /**
     * TD-退库（门店出）；MD-互调（门店出）；BD-报损；ZD-自用；SY-损溢（门店出）
     *
     * @param gaiaMaterialDoc
     * @param gaiaMaterialAssess
     */
    private void subtractMaterialAssess(GaiaMaterialDoc gaiaMaterialDoc, GaiaMaterialAssess gaiaMaterialAssess) {
        // 加点后金额 = 原加点后金额 -加点后金额
        gaiaMaterialAssess.setMatAddAmt(this.subtract(gaiaMaterialAssess.getMatAddAmt(), gaiaMaterialDoc.getMatAddAmt()));
        //加点后税金 =原加点后税金 -加点后税金
        gaiaMaterialAssess.setMatAddTax(this.subtract(gaiaMaterialAssess.getMatAddTax(), gaiaMaterialDoc.getMatAddTax()));
    }

    /**
     * 配送
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByPD(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap, List<GaiaProfitLevel> gaiaProfitLevelList) {
        FeignResult result = new FeignResult();
        if (StringUtils.isBlank(info.getMatStoCode())) {
            result.setCode(1001);
            result.setMessage("门店不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatSiteCode())) {
            result.setCode(1001);
            result.setMessage("仓库不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatBatch())) {
            result.setCode(1001);
            result.setMessage("批次不能为空");
            return result;
        }
        if (info.getMatPrice() == null) {
            result.setCode(1001);
            result.setMessage("交易价格不能为空");
            return result;
        }
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        // 税率
        BigDecimal inputTax = BigDecimal.ZERO;
        // 税金（移动）
        BigDecimal matRateMov = BigDecimal.ZERO;
        // 总金额（移动）
        BigDecimal matMovAmt = BigDecimal.ZERO;
        {
            // 更新物料评估表
            GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
            assessKey.setClient(info.getClient());   // 加盟商
            assessKey.setMatProCode(info.getMatProCode());   // 商品编码
            assessKey.setMatAssessSite(info.getMatSiteCode()); // 门店
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
            if (gaiaMaterialAssess == null) {
                result.setCode(1001);
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatSiteCode(), info.getMatProCode());
                if (gaiaProductBusiness == null) {
                    result.setMessage("商品没有成本记录，无法过账");
                } else {
                    result.setMessage("商品" + info.getMatProCode() + "_" + gaiaProductBusiness.getProName() + "没有成本记录，无法过账");
                }
                return result;
            } else {
                log.info("物料评估原数据：{}", JsonUtils.beanToJson(gaiaMaterialAssess));
                // 仓库发货凭证 生成
                GaiaBatchInfoKey gaiaBatchInfoKey = new GaiaBatchInfoKey();
                gaiaBatchInfoKey.setClient(info.getClient());
                gaiaBatchInfoKey.setBatSiteCode(info.getMatSiteCode());
                gaiaBatchInfoKey.setBatProCode(info.getMatProCode());
                gaiaBatchInfoKey.setBatBatch(info.getMatBatch());
                GaiaBatchInfo gaiaBatchInfo = gaiaBatchInfoMapper.selectForMdoc(gaiaBatchInfoKey);
                if (gaiaBatchInfo == null) {
                    throw new FeignResultException(1002, "批次信息表不存在");
                }
                // 采购单价
                BigDecimal price = gaiaBatchInfo.getBatPoPrice();
                // 税率
                inputTax = getInputTax2(MatTypeEnum.PD.getCode(), info.getClient(), gaiaBatchInfo.getBatPoId(), gaiaBatchInfo.getBatProCode(), gaiaBatchInfo.getBatSiteCode());

                // 物料凭证生成
                GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
                // 仓库
                gaiaMaterialDoc.setMatSiteCode(info.getMatSiteCode());
                // 数量
                gaiaMaterialDoc.setMatQty(matQty);
                // 借/贷标识
                gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S
                // 物料凭证数量
                gaiaMaterialDoc.setMatQty(matQty);
                // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
                gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
                // 总金额（批次） = 交易价格*交易数量-税金(批次)
                gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
                // 总金额（移动）= 总金额（批次）
//            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialDoc.getMatBatAmt());
                // 总金额（移动）= 取原物料评估表中的总金额/总数量*发生数；如果总金额、总数量任一为0，则直接取原移动平均单价*发生数。
                if (gaiaMaterialAssess.getMatTotalAmt() == null || gaiaMaterialAssess.getMatTotalAmt().compareTo(BigDecimal.ZERO) == 0 ||
                        gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).setScale(2, RoundingMode.DOWN));
                    // 移动平均单价*发生数*税率
//                    gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).multiply(inputTax));
                } else {
                    gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                    // 税额/总数量*发生数
//                    gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP).multiply(matQty));
                }
                // 税金（移动）= 税率 * 总金额（移动）
//                gaiaMaterialDoc.setMatRateMov(inputTax.multiply(gaiaMaterialDoc.getMatMovAmt()));
                // 物料评估税金/物料评估总数量 * 交易数据
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    // 总金额（移动） * 税
                    gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatMovAmt().multiply(inputTax).setScale(2, RoundingMode.DOWN));
                } else {
                    gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
                }
                matMovAmt = gaiaMaterialDoc.getMatMovAmt();
                matRateMov = gaiaMaterialDoc.getMatRateMov();
                gaiaMaterialDocMapper.insert(gaiaMaterialDoc);
                // 「总库存」即为原有数量+物料凭证数量，
                gaiaMaterialAssess.setMatTotalQty(this.subtract(gaiaMaterialAssess.getMatTotalQty(), gaiaMaterialDoc.getMatQty()));
                // 「总金额」为原有金额 - 总金额（移动）
                gaiaMaterialAssess.setMatTotalAmt(this.subtract(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
                // 税金 - 税金（移动）
                gaiaMaterialAssess.setMatRateAmt(this.subtract(gaiaMaterialAssess.getMatRateAmt(), gaiaMaterialDoc.getMatRateMov()));
                // 移动平均价 = 总金额/总数量
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                    // 仓库里 无需计算 MovPrice
//                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                } else {
                    // 仓库里 无需计算 MovPrice
//                    gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                }
                // 空库存
                if (gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                }
                gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
                }
                gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
            }
        }
        {
            // 移动平均价
            BigDecimal matMovPrice = BigDecimal.ZERO;
            // 交易价格
            BigDecimal price = getPrice(info);
            //  step 3: 再生成物料凭证，更新物料凭证表GAIA_MATERIAL_DOC
            GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
            // 借/贷标识
            gaiaMaterialDoc.setMatDebitCredit("S"); // 减库存-H；加库存-S
            // 物料凭证数量
            gaiaMaterialDoc.setMatQty(matQty);
            gaiaMaterialDoc.setMatSiteCode(info.getMatStoCode());  // 配送门店的地点编码
            gaiaMaterialDoc.setMatLocationCode(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(0).getValue());  // 默认1000-合格品库
            // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
            // 总金额（批次） = 交易价格*交易数量-税金(批次)
            gaiaMaterialDoc.setMatBatAmt(matQty.multiply(price).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
            // 总金额（移动）= 总金额（批次） 门店的这两个金额 需要直接等于仓库计算出来的那两个值。
//            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialDoc.getMatBatAmt());
            gaiaMaterialDoc.setMatMovAmt(matMovAmt);
            // 「税金（移动）」=「税金（批次）」→ 移动税金 * 税率 门店的这两个金额 需要直接等于仓库计算出来的那两个值。
//            gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatRateBat());
//            gaiaMaterialDoc.setMatRateMov(inputTax.multiply(gaiaMaterialDoc.getMatMovAmt()));
            gaiaMaterialDoc.setMatRateMov(matRateMov);

            // 加点计算 主要是PD进来会要算加点。其他业务是按比例扣的
//            addMaterialDoc(gaiaMaterialDoc, info.getClient(), info.getMatStoCode(), info.getMatProCode(), inputTax, matQty);
            // 【 交易价格*交易数量】/（1+税率）=加点后金额
            gaiaMaterialDoc.setMatAddAmt(
                    (info.getMatPrice().multiply(matQty)).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN)
            );
            // 【 交易价格*交易数量】-加点后金额=加点后税额
            gaiaMaterialDoc.setMatAddTax(
                    this.subtract(info.getMatPrice().multiply(matQty), gaiaMaterialDoc.getMatAddAmt()).setScale(2, RoundingMode.DOWN)
            );
            gaiaMaterialDocMapper.insert(gaiaMaterialDoc);
            // （matAddAmt + matAddTax）/ matQty
            matMovPrice = (this.add(gaiaMaterialDoc.getMatAddAmt(), gaiaMaterialDoc.getMatAddTax()).divide(matQty, 4, RoundingMode.HALF_UP));

            // 根据加盟商+商品编码+发货地点到物料评估表中过滤出数据
            GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
            assessKey.setClient(info.getClient());   // 加盟商
            assessKey.setMatProCode(info.getMatProCode());   // 商品编码
            assessKey.setMatAssessSite(info.getMatStoCode()); // 门店
            GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
            if (gaiaMaterialAssess == null) {
                gaiaMaterialAssess = new GaiaMaterialAssess();
                // 加盟商
                gaiaMaterialAssess.setClient(info.getClient());
                // 商品编码
                gaiaMaterialAssess.setMatProCode(info.getMatProCode());
                // 估价地点
                gaiaMaterialAssess.setMatAssessSite(info.getMatStoCode());
                // 总库存 = 物料凭证数量
                gaiaMaterialAssess.setMatTotalQty(gaiaMaterialDoc.getMatQty());
                // 总金额 = 总金额（移动）
                gaiaMaterialAssess.setMatTotalAmt(gaiaMaterialDoc.getMatMovAmt());
                // 移动平均价 = 总金额/总数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                // 税金 = 交易价格*交易数量/(1+税率)*税率
                gaiaMaterialAssess.setMatRateAmt(gaiaMaterialDoc.getMatRateMov());
                gaiaMaterialAssess.setBatCreateUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatCreateDate(info.getMatCreateDate());
                gaiaMaterialAssess.setBatCreateTime(info.getMatCreateTime());
                // 加点计算
//                addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatStoCode(), info.getMatProCode(), inputTax, matQty);
                gaiaMaterialAssess.setMatAddTax(gaiaMaterialDoc.getMatAddTax());
                gaiaMaterialAssess.setMatAddAmt(gaiaMaterialDoc.getMatAddAmt());
                gaiaMaterialAssessMapper.insert(gaiaMaterialAssess);
            } else {
                log.info("物料评估原数据：{}", JsonUtils.beanToJson(gaiaMaterialAssess));
                // 原库存
                BigDecimal oldTotalQty = gaiaMaterialAssess.getMatTotalQty() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatTotalQty();
                // 总金额
                gaiaMaterialAssess.setMatTotalAmt(this.add(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
                //  物料凭证数量作为扣减数量
                gaiaMaterialAssess.setMatTotalQty(this.add(gaiaMaterialAssess.getMatTotalQty(), gaiaMaterialDoc.getMatQty()));
                // 税金 = 税金 + 税金（移动）
                gaiaMaterialAssess.setMatRateAmt((gaiaMaterialAssess.getMatRateAmt() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatRateAmt())
                        .add(gaiaMaterialDoc.getMatRateMov() == null ? BigDecimal.ZERO : gaiaMaterialDoc.getMatRateMov()));
                // 移动平均价 = 总金额/总数量
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                    // 原来为负库存，入库后无论数量为正还是为负，单位评估价assess的mov_price为原价,不动.
                    if (oldTotalQty.compareTo(BigDecimal.ZERO) >= 0) {
                        gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
                    }
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                } else {
                    // 原来为负库存，入库后无论数量为正还是为负，单位评估价assess的mov_price为原价,不动.
                    if (oldTotalQty.compareTo(BigDecimal.ZERO) >= 0) {
                        gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
                    }
                }
                gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
                gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                // 加点计算 物料评估表  就是用 物料凭证中计算出的对应的值 来相加。
//                addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatStoCode(), info.getMatProCode(), inputTax, matQty);
                gaiaMaterialAssess.setMatAddTax(this.add(gaiaMaterialAssess.getMatAddTax(), gaiaMaterialDoc.getMatAddTax()));
                gaiaMaterialAssess.setMatAddAmt(this.add(gaiaMaterialAssess.getMatAddAmt(), gaiaMaterialDoc.getMatAddAmt()));
                if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                    gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                    gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
                }
                gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
            }
            // 毛利级别更新
            baseService.setProProfit(info.getClient(), info.getMatStoCode(), info.getMatProCode(), this.gaiaTaxCodeList, null, null, null, null, gaiaProfitLevelList);
        }

        /**
         * step 7：调用财务接口
         */

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 销退
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByED(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap) {
        FeignResult result = new FeignResult();
        /**
         * step 1: 先更新物料评估表的移动平均价
         * 1.根据加盟商+商品编码+地点至物料评估数据表（GAIA_MATERIAL_ASSESS）过滤数据
         * 2.无数据： 新增物料评估数据表
         * 3.有数据： 重算移动平均价后直接更新对应移动平均价的
         */
        if (StringUtils.isBlank(info.getMatPoId())) {
            result.setCode(1001);
            result.setMessage("订单号不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoLineno())) {
            result.setCode(1001);
            result.setMessage("订单行号不能为空");
            return result;
        }
        if (info.getMatQty() == null) {
            result.setCode(1001);
            result.setMessage("交易数量不能为空");
            return result;
        }
        // ----- 验证结束
        // 税率
        BigDecimal inputTax = getInputTax(info);
        // 交易价格
        BigDecimal price = this.getPrice(info);
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        // 生成物料凭证，更新物料凭证表 GAIA_MATERIAL_DOC
        GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
        // 数量
        gaiaMaterialDoc.setMatQty(matQty);
        gaiaMaterialDoc.setMatDebitCredit("S");  // 贷-加库存
        // 税金（批次）= 交易价格*交易数量/(1+税率)*税率
        gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
        // 总金额（批次）=  交易价格*交易数量-税金(批次)
        gaiaMaterialDoc.setMatBatAmt(matQty.multiply(price).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
        GaiaSoItemKey gaiaSoItemKey = new GaiaSoItemKey();
        gaiaSoItemKey.setClient(info.getClient());
        gaiaSoItemKey.setSoId(info.getMatPoId());
        gaiaSoItemKey.setSoLineNo(info.getMatPoLineno());
        GaiaSoItem gaiaSoItem = gaiaSoItemMapper.selectByPrimaryKey(gaiaSoItemKey);
        boolean flg = false;
        if (gaiaSoItem != null) {
            if (StringUtils.isNotBlank(gaiaSoItem.getSoReferOrder()) && StringUtils.isNotBlank(gaiaSoItem.getSoReferOrderLineno())) {
                GaiaMaterialDoc entity = gaiaMaterialDocMapper.selectMaterialDocByPoIdForEd(info.getClient(), gaiaSoItem.getSoReferOrder(), gaiaSoItem.getSoReferOrderLineno());
                if (entity != null) {
                    // 对比若传入的交易数量=「总数量MAT_QTY」则「总金额（移动）」=取出的「总金额MAT_MOV_AMT」；
                    if (info.getMatQty().compareTo(entity.getMatQty()) == 0) {
                        gaiaMaterialDoc.setMatMovAmt(entity.getMatMovAmt());
                        gaiaMaterialDoc.setMatRateMov(entity.getMatRateMov());
                        flg = true;
                    } else {
                        if (entity.getMatQty() != null && entity.getMatQty().compareTo(BigDecimal.ZERO) != 0) {
                            // 取出的「总金额MAT_MOV_AMT」/取出的「总数量MAT_QTY」*传入的交易数量
                            gaiaMaterialDoc.setMatMovAmt(entity.getMatMovAmt().multiply(info.getMatQty()).divide(entity.getMatQty(), 2, RoundingMode.DOWN));
                            gaiaMaterialDoc.setMatRateMov(entity.getMatRateMov().multiply(info.getMatQty()).divide(entity.getMatQty(), 2, RoundingMode.DOWN));
                            flg = true;
                        }
                    }
                }
            }
        }
        if (!flg) {
            // 总金额（移动）= 总金额（批次）
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialDoc.getMatBatAmt());
            // 「税金（移动）」=「税金（批次）」 → 移动税金 * 税率
            gaiaMaterialDoc.setMatRateMov(inputTax.multiply(gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
        }
        gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

        GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
        assessKey.setClient(info.getClient());   // 加盟商
        assessKey.setMatProCode(info.getMatProCode());   // 商品编码
        assessKey.setMatAssessSite(info.getMatSiteCode()); // 地点
        GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
        if (gaiaMaterialAssess == null) {
            gaiaMaterialAssess = new GaiaMaterialAssess();
            gaiaMaterialAssess.setClient(info.getClient());
            gaiaMaterialAssess.setMatProCode(info.getMatProCode());
            gaiaMaterialAssess.setMatAssessSite(info.getMatSiteCode());
            // 创建人
            gaiaMaterialAssess.setBatCreateUser(info.getMatCreateBy());
            // 创建日期
            gaiaMaterialAssess.setBatCreateDate(info.getMatCreateDate());
            // 创建时间
            gaiaMaterialAssess.setBatCreateTime(info.getMatCreateTime());
            // 总库存 「总库存」即为物料凭证数量
            gaiaMaterialAssess.setMatTotalQty(gaiaMaterialDoc.getMatQty());
            // 总金额
            gaiaMaterialAssess.setMatTotalAmt(gaiaMaterialDoc.getMatMovAmt());
            // 移动平均价 = 总金额/总数量
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
            // 税金 = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialAssess.setMatRateAmt(gaiaMaterialDoc.getMatRateMov());
            // 创建日期
            gaiaMaterialAssess.setBatCreateDate(info.getMatCreateDate());
            // 创建时间
            gaiaMaterialAssess.setBatCreateTime(info.getMatCreateTime());
            // 创建人
            gaiaMaterialAssess.setBatCreateUser(info.getMatCreateBy());
            gaiaMaterialAssessMapper.insert(gaiaMaterialAssess);
        } else {
            // 「总库存」即为原有数量+物料凭证数量，
            gaiaMaterialAssess.setMatTotalQty(this.add(gaiaMaterialAssess.getMatTotalQty(), gaiaMaterialDoc.getMatQty()));
            // 「总金额」为原有金额+物料凭证中的「总金额（移动）」的值
            gaiaMaterialAssess.setMatTotalAmt(this.add(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
            // 税金
            gaiaMaterialAssess.setMatRateAmt(this.add(gaiaMaterialAssess.getMatRateAmt(), gaiaMaterialDoc.getMatRateMov()));
            // 总库存 = 0
            if (gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
                gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
            } else {
                // 移动平均价 = 总金额/总数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
            }
            gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
            gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
            }
            gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
        }

        /**
         * step 4: 调用财务接口
         */

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 销售
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByXD(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap) {
        FeignResult result = new FeignResult();

        log.info("物料评估查询");
        GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
        gaiaMaterialAssessKey.setClient(info.getClient());
        gaiaMaterialAssessKey.setMatAssessSite(info.getMatSiteCode());
        gaiaMaterialAssessKey.setMatProCode(info.getMatProCode());
        GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
        if (gaiaMaterialAssess == null) {
            result.setCode(1001);
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatSiteCode(), info.getMatProCode());
            if (gaiaProductBusiness == null) {
                result.setMessage("商品没有成本记录，无法过账");
            } else {
                result.setMessage("商品" + info.getMatProCode() + "_" + gaiaProductBusiness.getProName() + "没有成本记录，无法过账");
            }
            return result;
        }
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        // 税率
        BigDecimal inputTax = getInputTax(info);
        // 价格
        BigDecimal price = this.getPrice(info);
        // step 1: 生成物料凭证表 GAIA_MATERIAL_DOC
        log.info("生成物料凭证表生成");
        // 物料凭证生成
        GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
        // 物料凭证数量
        gaiaMaterialDoc.setMatQty(matQty);
        // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
        gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
        // 总金额（批次） = 交易价格*交易数量-税金(批次)
        gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
        // 总金额（移动）= 取原物料评估表中的总金额/总数量*发生数；如果总金额、总数量任一为0，则直接取原移动平均单价*发生数。
        if (gaiaMaterialAssess.getMatTotalAmt() == null || gaiaMaterialAssess.getMatTotalAmt().compareTo(BigDecimal.ZERO) == 0 ||
                gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).setScale(2, RoundingMode.DOWN));
        } else {
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
        }
        // 借/贷标识
        gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S
        // 税金（移动）= 税率 * 总金额（移动）
//        gaiaMaterialDoc.setMatRateMov(inputTax.multiply(gaiaMaterialDoc.getMatMovAmt()));
        // 物料评估税金/物料评估总数量 * 交易数据
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            // 总金额（移动） * 税
            gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatMovAmt().multiply(inputTax).setScale(2, RoundingMode.DOWN));
        } else {
            gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
        }
        gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

        // 若物料凭证数量等于总库存，则总库存、总金额的值全部清空成0
        if (gaiaMaterialDoc.getMatQty().compareTo(gaiaMaterialAssess.getMatTotalQty()) == 0) {
            gaiaMaterialAssess.setMatTotalQty(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
        } else {
            gaiaMaterialAssess.setMatTotalAmt(this.subtract(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
            //  物料凭证数量作为扣减数量
            gaiaMaterialAssess.setMatTotalQty(gaiaMaterialAssess.getMatTotalQty().subtract(gaiaMaterialDoc.getMatQty()));
        }
        // 税金 = 税金 + 税金（移动）
        gaiaMaterialAssess.setMatRateAmt((gaiaMaterialAssess.getMatRateAmt() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatRateAmt())
                .subtract(gaiaMaterialDoc.getMatRateMov() == null ? BigDecimal.ZERO : gaiaMaterialDoc.getMatRateMov()));
        // 移动平均价 = 总金额/总数量
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
        } else {
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
        }
        gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
        gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
        }
        gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);

        /**
         * step 3: 调用财务接口
         */

        result.setCode(0);
        result.setMessage("执行成功");
        log.info("执行成功");
        return result;
    }


    private void updatLastAmt(GaiaBillOfArVO insertOrUpdate, Date now) {
        GaiaFiArBalance balance = gaiaFiArBalanceMapper.getByDcCodeAndCusSelfCode(insertOrUpdate);
        Double lastAmt = gaiaBillOfArMapper.getLastAmt(insertOrUpdate);
        log.info(String.format("<物料凭证><更新客户余额><请求参数：%s>", JSON.toJSONString(insertOrUpdate)));
        if (ObjectUtils.isEmpty(balance)) {
            GaiaFiArBalance insert = new GaiaFiArBalance();
            insert.setClient(insertOrUpdate.getClient());
            insert.setDcCode(insertOrUpdate.getDcCode());
            insert.setResidualAmt(new BigDecimal(lastAmt));
            insert.setCusSelfCode(insertOrUpdate.getCusSelfCode());
            insert.setCusName(insertOrUpdate.getCusName());
            insert.setIsDelete(0);
            insert.setCreateTime(now);
            insert.setCreateUser(insertOrUpdate.getCreateUser());
            insert.setUpdateTime(now);
            insert.setUpdateUser(insertOrUpdate.getCreateUser());
            gaiaFiArBalanceMapper.insert(insert);

        } else {
            balance.setResidualAmt(new BigDecimal(lastAmt));
            balance.setUpdateTime(now);
            balance.setUpdateUser(insertOrUpdate.getCreateUser());
            gaiaFiArBalanceMapper.update(balance);
        }
    }

    /**
     * 退厂
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByGD(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap) {
        FeignResult result = new FeignResult();
        if (info.getMatQty() == null) {
            result.setCode(1002);
            result.setMessage("交易数量不能为空");
            return result;
        }

        GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
        gaiaMaterialAssessKey.setClient(info.getClient());
        gaiaMaterialAssessKey.setMatAssessSite(info.getMatSiteCode());
        gaiaMaterialAssessKey.setMatProCode(info.getMatProCode());
        GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
        if (gaiaMaterialAssess == null) {
            result.setCode(1001);
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatSiteCode(), info.getMatProCode());
            if (gaiaProductBusiness == null) {
                result.setMessage("商品没有成本记录，无法过账");
            } else {
                result.setMessage("商品" + info.getMatProCode() + "_" + gaiaProductBusiness.getProName() + "没有成本记录，无法过账");
            }
            return result;
        }

        // 价格
        BigDecimal price = this.getPrice(info);
        // 税率
        BigDecimal inputTax = getInputTax(info);
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        // 生成物料凭证表 GAIA_MATERIAL_DOC
        GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
        // 交易数量
        gaiaMaterialDoc.setMatQty(matQty);
        // 税金（批次） = 交易价格*交易数量/(1+税率)*税率
        gaiaMaterialDoc.setMatRateBat(price.multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
        // 总金额（批次） = 交易价格*交易数量-税金(批次)
        gaiaMaterialDoc.setMatBatAmt(price.multiply(matQty).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
        // 总金额（移动）= 取原物料评估表中的总金额/总数量*发生数；如果总金额、总数量任一为0，则直接取原移动平均单价*发生数。
        if (gaiaMaterialAssess.getMatTotalAmt() == null || gaiaMaterialAssess.getMatTotalAmt().compareTo(BigDecimal.ZERO) == 0 ||
                gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatMovPrice().multiply(matQty).setScale(2, RoundingMode.DOWN));
        } else {
            gaiaMaterialDoc.setMatMovAmt(gaiaMaterialAssess.getMatTotalAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
        }
        // 借/贷标识
        gaiaMaterialDoc.setMatDebitCredit("H"); // 减库存-H；加库存-S
        // 税金（移动）= 税率 * 总金额（移动）
//        gaiaMaterialDoc.setMatRateMov(inputTax.multiply(gaiaMaterialDoc.getMatMovAmt()));
        // 物料评估税金/物料评估总数量 * 交易数据
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            // 总金额（移动） * 税
            gaiaMaterialDoc.setMatRateMov(gaiaMaterialDoc.getMatMovAmt().multiply(inputTax).setScale(2, RoundingMode.DOWN));
        } else {
            gaiaMaterialDoc.setMatRateMov(gaiaMaterialAssess.getMatRateAmt().multiply(matQty).divide(gaiaMaterialAssess.getMatTotalQty(), 2, RoundingMode.DOWN));
        }
        // 加点计算;
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(info.getClient());
        gaiaStoreDataKey.setStoCode(info.getMatSiteCode());
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        // 当地点是门店时
        if (gaiaStoreData != null) {
            subtractMaterialDoc(gaiaMaterialDoc, gaiaMaterialAssess, inputTax, matQty);
        }
        gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

        // 更新物料评估表
        // 若物料凭证数量等于总库存，则总库存、总金额的值全部清空成0
        if (gaiaMaterialDoc.getMatQty().compareTo(gaiaMaterialAssess.getMatTotalQty()) == 0) {
            gaiaMaterialAssess.setMatTotalQty(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
        } else {
            gaiaMaterialAssess.setMatTotalAmt(this.subtract(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
            //  物料凭证数量作为扣减数量
            gaiaMaterialAssess.setMatTotalQty(gaiaMaterialAssess.getMatTotalQty().subtract(gaiaMaterialDoc.getMatQty()));
        }
        // 税金 = 税金 + 税金（移动）
        gaiaMaterialAssess.setMatRateAmt((gaiaMaterialAssess.getMatRateAmt() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatRateAmt())
                .subtract(gaiaMaterialDoc.getMatRateMov() == null ? BigDecimal.ZERO : gaiaMaterialDoc.getMatRateMov()));
        // 移动平均价 = 总金额/总数量
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
        } else {
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
        }
        gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
        gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
        // 加点计算
        // 当地点是门店时
        if (gaiaStoreData != null) {
            subtractMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess);
        }
        if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
            gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
            gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
        }
        gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);

        /**
         * step 3: 调用财务接口
         */
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 采购
     *
     * @param info
     * @param matIdMap
     * @param indexMap
     * @return
     */
    private FeignResult insertMaterialDocByCD(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap, Map<String,
            List<GaiaAllotPrice>> gaiaAllotPriceMap, List<GaiaProfitLevel> gaiaProfitLevelList) {
        FeignResult result = new FeignResult();
        /**
         * step 1: 先更新物料评估表的移动平均价
         * 1.根据加盟商+商品编码+地点至物料评估数据表（GAIA_MATERIAL_ASSESS）过滤数据
         * 2.无数据： 新增物料评估数据表
         * 3.有数据： 重算移动平均价后直接更新对应移动平均价的
         */
        if (StringUtils.isBlank(info.getMatPoId())) {
            result.setCode(1001);
            result.setMessage("订单号不能为空");
            return result;
        }
        if (StringUtils.isBlank(info.getMatPoLineno())) {
            result.setCode(1001);
            result.setMessage("订单行号不能为空");
            return result;
        }
        if (info.getMatPrice() == null) {
            result.setCode(1001);
            result.setMessage("交易价格不能为空");
            return result;
        }
        if (info.getMatQty() == null) {
            result.setCode(1001);
            result.setMessage("交易数量不能为空");
            return result;
        }
        // ----- 验证结束
        //税率
        BigDecimal inputTax = getInputTax(info);
        // 数量
        BigDecimal matQty = info.getMatQty().abs();
        // 生成物料凭证，更新物料凭证表 GAIA_MATERIAL_DOC
        GaiaMaterialDoc gaiaMaterialDoc = setMaterialDoc(info, matIdMap, indexMap);
        // 交易数量
        gaiaMaterialDoc.setMatQty(matQty);
        gaiaMaterialDoc.setMatDebitCredit("S");  // 贷-加库存
        // 税金（批次）= 交易价格*交易数量/(1+税率)*税率
        gaiaMaterialDoc.setMatRateBat(info.getMatPrice().multiply(matQty).multiply(inputTax).divide((BigDecimal.ONE.add(inputTax)), 2, RoundingMode.DOWN));
        // 总金额（批次）=  交易价格*交易数量-税金(批次)
        gaiaMaterialDoc.setMatBatAmt(matQty.multiply(info.getMatPrice()).subtract(gaiaMaterialDoc.getMatRateBat()).setScale(2, RoundingMode.DOWN));
        // 总金额（移动）= 总金额（批次）
        gaiaMaterialDoc.setMatMovAmt(gaiaMaterialDoc.getMatBatAmt());
        // 「税金（移动）」=「税金（批次）」 → 移动税金 * 税率
        gaiaMaterialDoc.setMatRateMov(inputTax.multiply(gaiaMaterialDoc.getMatMovAmt()).setScale(2, RoundingMode.DOWN));
        // 加点计算
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(info.getClient());
        gaiaStoreDataKey.setStoCode(info.getMatSiteCode());
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        // 当地点是门店时
        if (gaiaStoreData != null) {
            // 加点后金额 = 总金额（移动）
            gaiaMaterialDoc.setMatAddAmt(gaiaMaterialDoc.getMatMovAmt());
            // 税额 = 总金额（移动） * 税率
            gaiaMaterialDoc.setMatAddTax(gaiaMaterialDoc.getMatRateMov());
        }
        gaiaMaterialDocMapper.insert(gaiaMaterialDoc);

        GaiaMaterialAssessKey assessKey = new GaiaMaterialAssessKey();
        assessKey.setClient(info.getClient());   // 加盟商
        assessKey.setMatProCode(info.getMatProCode());   // 商品编码
        assessKey.setMatAssessSite(info.getMatSiteCode()); // 地点
        GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(assessKey);
        if (gaiaMaterialAssess == null) {
            gaiaMaterialAssess = new GaiaMaterialAssess();
            gaiaMaterialAssess.setClient(info.getClient());
            gaiaMaterialAssess.setMatProCode(info.getMatProCode());
            gaiaMaterialAssess.setMatAssessSite(info.getMatSiteCode());
            // 创建人
            gaiaMaterialAssess.setBatCreateUser(info.getMatCreateBy());
            // 创建日期
            gaiaMaterialAssess.setBatCreateDate(info.getMatCreateDate());
            // 创建时间
            gaiaMaterialAssess.setBatCreateTime(info.getMatCreateTime());
            // 总库存 「总库存」即为物料凭证数量
            gaiaMaterialAssess.setMatTotalQty(gaiaMaterialDoc.getMatQty());
            // 总金额
            gaiaMaterialAssess.setMatTotalAmt(gaiaMaterialDoc.getMatMovAmt());
            // 移动平均价 = 总金额/总数量
            gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
            // 税金 = 交易价格*交易数量/(1+税率)*税率
            gaiaMaterialAssess.setMatRateAmt(gaiaMaterialDoc.getMatRateMov());
            // 创建日期
            gaiaMaterialAssess.setBatCreateDate(info.getMatCreateDate());
            // 创建时间
            gaiaMaterialAssess.setBatCreateTime(info.getMatCreateTime());
            // 创建人
            gaiaMaterialAssess.setBatCreateUser(info.getMatCreateBy());
            // 加点计算
            // 当地点是门店时
            if (gaiaStoreData != null) {
                addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatSiteCode(), info.getMatProCode(), inputTax, matQty, gaiaAllotPriceMap);
            }
            gaiaMaterialAssessMapper.insert(gaiaMaterialAssess);
        } else {
            // 「总库存」即为原有数量+物料凭证数量，
            gaiaMaterialAssess.setMatTotalQty(this.add(gaiaMaterialAssess.getMatTotalQty(), gaiaMaterialDoc.getMatQty()));
            // 「总金额」为原有金额+物料凭证中的「总金额（移动）」的值
            gaiaMaterialAssess.setMatTotalAmt(this.add(gaiaMaterialAssess.getMatTotalAmt(), gaiaMaterialDoc.getMatMovAmt()));
            // 税金
            gaiaMaterialAssess.setMatRateAmt(this.add(gaiaMaterialAssess.getMatRateAmt(), gaiaMaterialDoc.getMatRateMov()));
            // 总库存 = 0
            if (gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                // 移动平均价 = 物料凭证的总金额（移动）/ 交易数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialDoc.getMatMovAmt().divide(gaiaMaterialDoc.getMatQty(), 4, RoundingMode.HALF_UP));
                gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
            } else {
                // 移动平均价 = 总金额/总数量
                gaiaMaterialAssess.setMatMovPrice(gaiaMaterialAssess.getMatTotalAmt().divide(gaiaMaterialAssess.getMatTotalQty(), 4, RoundingMode.HALF_UP));
            }
            gaiaMaterialAssess.setBatChangeUser(info.getMatCreateBy());
            gaiaMaterialAssess.setBatChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            gaiaMaterialAssess.setBatChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
            // 加点计算
            // 当地点是门店时
            if (gaiaStoreData != null) {
                addMaterialAssess(gaiaMaterialDoc, gaiaMaterialAssess, info.getClient(), info.getMatSiteCode(), info.getMatProCode(), inputTax, matQty, gaiaAllotPriceMap);
            }
            if (gaiaMaterialAssess.getMatTotalQty() == null || gaiaMaterialAssess.getMatTotalQty().compareTo(BigDecimal.ZERO) == 0) {
                gaiaMaterialAssess.setMatTotalAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatRateAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatAddAmt(BigDecimal.ZERO);
                gaiaMaterialAssess.setMatAddTax(BigDecimal.ZERO);
            }
            gaiaMaterialAssessMapper.updateByPrimaryKeySelective(gaiaMaterialAssess);
        }
        // 毛利级别更新
        baseService.setProProfit(info.getClient(), info.getMatSiteCode(), info.getMatProCode(), this.gaiaTaxCodeList, null, null, null, null, gaiaProfitLevelList);

        updateProductPosition(info.getClient(), info.getMatProCode());

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 设置物料凭证对象
     *
     * @param info
     * @return
     */
    private GaiaMaterialDoc setMaterialDoc(MaterialDocRequestDto info, Map<String, String> matIdMap, Map<String, Integer> indexMap) {
        // 凭证单号
        String matId = matIdMap.get(info.getClient());
        if (StringUtils.isBlank(matId)) {
            // matId = gaiaMaterialDocMapper.selectNo(info.getClient());
            matId = DateFormatUtils.format(new Date(), "yyyyMMddHHmmssSSS") + RandomStringUtils.randomNumeric(9);
            matIdMap.put(info.getClient(), matId);
        }
        GaiaMaterialDoc gaiaMaterialDoc = new GaiaMaterialDoc();
        gaiaMaterialDoc.setClient(info.getClient());
        gaiaMaterialDoc.setMatId("WL" + matId);
        gaiaMaterialDoc.setMatYear(String.valueOf(LocalDate.now().getYear()));
        Integer rowIndex = indexMap.get(gaiaMaterialDoc.getMatId());
        if (rowIndex == null) {
            rowIndex = 1;
        } else {
            rowIndex++;
        }
        indexMap.put(gaiaMaterialDoc.getMatId(), rowIndex);
        gaiaMaterialDoc.setMatLineNo(rowIndex.toString());
        gaiaMaterialDoc.setMatType(info.getMatType());
        gaiaMaterialDoc.setMatDocDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaMaterialDoc.setMatPostDate(info.getMatPostDate());
        gaiaMaterialDoc.setMatHeadRemark(info.getMatHeadRemark());
        gaiaMaterialDoc.setMatProCode(info.getMatProCode());
        gaiaMaterialDoc.setMatSiteCode(info.getMatSiteCode());
        gaiaMaterialDoc.setMatLocationCode(info.getMatLocationCode());
        gaiaMaterialDoc.setMatBatch(info.getMatBatch());
        gaiaMaterialDoc.setMatQty(info.getMatQty());
        gaiaMaterialDoc.setMatUint(info.getMatUnit());
        gaiaMaterialDoc.setMatDebitCredit(info.getMatDebitCredit());
        gaiaMaterialDoc.setMatPoId(info.getMatPoId());
        gaiaMaterialDoc.setMatPoLineno(info.getMatPoLineno());
        gaiaMaterialDoc.setMatDnId(info.getMatDnId());
        gaiaMaterialDoc.setMatDnLineno(info.getMatDnLineno());
        gaiaMaterialDoc.setMatLineRemark(info.getMatLineRemark());
        gaiaMaterialDoc.setMatCreateBy(info.getMatCreateBy());
        gaiaMaterialDoc.setMatCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaMaterialDoc.setMatCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
        return gaiaMaterialDoc;
    }

    /**
     * 获取进项税率
     *
     * @param info
     * @return
     */
    private BigDecimal getInputTax(MaterialDocRequestDto info) {
        String rate = "";
        GaiaPoItemKey gaiaPoItemKey = new GaiaPoItemKey();
        gaiaPoItemKey.setClient(info.getClient());
        gaiaPoItemKey.setPoId(info.getMatPoId());
        gaiaPoItemKey.setPoLineNo(info.getMatPoLineno());
        GaiaPoItem gaiaPoItem = gaiaPoItemMapper.selectByPrimaryKey(gaiaPoItemKey);
        if (gaiaPoItem == null || StringUtils.isBlank(gaiaPoItem.getPoRate())) {
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatSiteCode(), info.getMatProCode());
            if (gaiaProductBusiness == null) {
                throw new FeignResultException(1004, "商品不存在");
            }
            rate = gaiaProductBusiness.getProInputTax();
        } else {
            rate = gaiaPoItem.getPoRate();
        }
        GaiaTaxCode gaiaTaxCode = getGaiaTaxCode(rate);
        if (gaiaTaxCode == null) {
            throw new FeignResultException(1005, "税率不存在");
        }
        if (gaiaTaxCode.getTaxCodeValue().endsWith("%")) {
            String taxCode = gaiaTaxCode.getTaxCodeValue().replace("%", "");
            return new BigDecimal(taxCode).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP);
        } else {
            return new BigDecimal(gaiaTaxCode.getTaxCodeValue());
        }
    }

    /**
     * 税率
     *
     * @param client
     * @param poId
     * @param poLineno
     * @param site
     * @param proCode
     * @return
     */
    private BigDecimal getInputTax(String client, String poId, String poLineno, String site, String proCode) {
        String rate = "";
        if (StringUtils.isBlank(poId) || StringUtils.isBlank(poLineno)) {
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(client, site, proCode);
            if (gaiaProductBusiness == null) {
                throw new FeignResultException(1004, "商品不存在");
            }
            rate = gaiaProductBusiness.getProInputTax();
        } else {
            GaiaPoItemKey gaiaPoItemKey = new GaiaPoItemKey();
            gaiaPoItemKey.setClient(client);
            gaiaPoItemKey.setPoId(poId);
            gaiaPoItemKey.setPoLineNo(poLineno);
            GaiaPoItem gaiaPoItem = gaiaPoItemMapper.selectByPrimaryKey(gaiaPoItemKey);
            if (gaiaPoItem == null || StringUtils.isBlank(gaiaPoItem.getPoRate())) {
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(client, site, proCode);
                if (gaiaProductBusiness == null) {
                    throw new FeignResultException(1004, "商品不存在");
                }
                rate = gaiaProductBusiness.getProInputTax();
            } else {
                rate = gaiaPoItem.getPoRate();
            }
        }
        GaiaTaxCode gaiaTaxCode = getGaiaTaxCode(rate);
        if (gaiaTaxCode == null) {
            throw new FeignResultException(1005, "税率不存在");
        }
        if (gaiaTaxCode.getTaxCodeValue().endsWith("%")) {
            String taxCode = gaiaTaxCode.getTaxCodeValue().replace("%", "");
            return new BigDecimal(taxCode).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP);
        } else {
            return new BigDecimal(gaiaTaxCode.getTaxCodeValue());
        }
    }

    /**
     * 1022物料凭证税率调整 ls
     *
     * @return
     */
    private BigDecimal getInputTax3(MaterialDocRequestDto info) {
        // 配送单号
        String wm_ddh = gaiaWmsDiaoboMMapper.selectPoId(info.getClient(), info.getMatProCode(), info.getMatBatch());
        if (StringUtils.isNotBlank(wm_ddh)) {
            // 补货表
            String inputTax = gaiaSdReplenishDMapper.getInputTax1(info.getClient(), wm_ddh, info.getMatProCode());
            if (StringUtils.isNotBlank(inputTax)) {
                GaiaTaxCode gaiaTaxCode = getGaiaTaxCode(inputTax);
                if (gaiaTaxCode == null) {
                    return new BigDecimal(inputTax.replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP);
                } else {
                    if (StringUtils.isNotBlank(gaiaTaxCode.getTaxCodeValue())) {
                        String taxCodeValue = gaiaTaxCode.getTaxCodeValue().replaceAll("%", "");
                        return new BigDecimal(taxCodeValue).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP);
                    }
                }
            }
        }
        String rate = "";
        GaiaPoItemKey gaiaPoItemKey = new GaiaPoItemKey();
        gaiaPoItemKey.setClient(info.getClient());
        gaiaPoItemKey.setPoId(info.getMatPoId());
        gaiaPoItemKey.setPoLineNo(info.getMatPoLineno());
        GaiaPoItem gaiaPoItem = gaiaPoItemMapper.selectByPrimaryKey(gaiaPoItemKey);
        if (gaiaPoItem == null || StringUtils.isBlank(gaiaPoItem.getPoRate())) {
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(info.getClient(), info.getMatSiteCode(), info.getMatProCode());
            if (gaiaProductBusiness == null) {
                throw new FeignResultException(1004, "商品不存在");
            }
            rate = gaiaProductBusiness.getProInputTax();
            if (StringUtils.isBlank(rate)) {
                throw new FeignResultException(1005, "商品税率为空");
            }
        } else {
            rate = gaiaPoItem.getPoRate();
        }
        GaiaTaxCode gaiaTaxCode = getGaiaTaxCode(rate);
        if (gaiaTaxCode == null) {
            throw new FeignResultException(1005, "税率不存在");
        }
        if (gaiaTaxCode.getTaxCodeValue().endsWith("%")) {
            String taxCode = gaiaTaxCode.getTaxCodeValue().replace("%", "");
            return new BigDecimal(taxCode).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP);
        } else {
            return new BigDecimal(gaiaTaxCode.getTaxCodeValue());
        }
    }

    /**
     * 1022物料凭证税率调整 pd td
     *
     * @return
     */
    private BigDecimal getInputTax2(String type, String client, String poId, String proCode, String site) {
        // 税率
        String inputTax = "";
        if (MatTypeEnum.PD.getCode().equals(type)) {
            inputTax = gaiaSdReplenishDMapper.getInputTax1(client, poId, proCode);
        }
        if (MatTypeEnum.TD.getCode().equals(type)) {
            inputTax = gaiaSdReplenishDMapper.getInputTax2(client, poId, proCode);
        }
        // 配送订单明细表 未取到税率
        if (StringUtils.isBlank(inputTax)) {
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(client, site, proCode);
            // 商品不存在
            if (gaiaProductBusiness == null) {
                throw new FeignResultException(1004, "商品不存在");
            }
            // 商品销项税不存在
            if (StringUtils.isBlank(gaiaProductBusiness.getProOutputTax())) {
                throw new FeignResultException(1005, "商品" + gaiaProductBusiness.getProName() + "销项税为空");
            }
            GaiaTaxCode gaiaTaxCode = getGaiaTaxCode(gaiaProductBusiness.getProOutputTax());
            if (gaiaTaxCode == null) {
                throw new FeignResultException(1005, "商品" + gaiaProductBusiness.getProName() + "销项税不存在");
            }
            inputTax = gaiaTaxCode.getTaxCodeValue();
        }
        return new BigDecimal(inputTax.replaceAll("%", "")).divide(new BigDecimal(100), 4, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 采购价获取
     *
     * @param info
     * @return
     */
    private BigDecimal getPrice(MaterialDocRequestDto info) {
        GaiaBatchInfoKey gaiaBatchInfoKey = new GaiaBatchInfoKey();
        gaiaBatchInfoKey.setClient(info.getClient());
        gaiaBatchInfoKey.setBatProCode(info.getMatProCode());
        gaiaBatchInfoKey.setBatBatch(info.getMatBatch());
        GaiaBatchInfo gaiaBatchInfo = gaiaBatchInfoMapper.selectForMdoc(gaiaBatchInfoKey);
        // 交易价格
        BigDecimal price = null;
        if (gaiaBatchInfo != null && gaiaBatchInfo.getBatPoPrice() != null) {
            price = gaiaBatchInfo.getBatPoPrice();
        } else {
            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setClient(info.getClient());
            gaiaProductBusinessKey.setProSite(info.getMatSiteCode());
            gaiaProductBusinessKey.setProSelfCode(info.getMatProCode());
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
            if (gaiaProductBusiness == null || gaiaProductBusiness.getProCgj() == null || gaiaProductBusiness.getProCgj().compareTo(BigDecimal.ZERO) == 0) {
                throw new FeignResultException(1007, "商品" + (gaiaProductBusiness == null ? "" : gaiaProductBusiness.getProName()) + "无成本价，请先维护商品主数据中的建议采购价后再试。");
            }
            price = gaiaProductBusiness.getProCgj();
        }
        return price;
    }

    /**
     * 数值相加
     *
     * @param decimals
     * @return
     */
    private BigDecimal add(BigDecimal... decimals) {
        if (decimals == null || decimals.length == 0) {
            return BigDecimal.ZERO;
        }
        BigDecimal result = BigDecimal.ZERO;
        for (BigDecimal decimal : decimals) {
            if (decimal != null) {
                result = result.add(decimal).setScale(4, BigDecimal.ROUND_HALF_UP);
            }
        }
        return result.setScale(4, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 数值相减
     *
     * @param bigDecimal1
     * @param bigDecimal2
     * @return
     */
    private BigDecimal subtract(BigDecimal bigDecimal1, BigDecimal bigDecimal2) {
        if (bigDecimal1 == null) {
            bigDecimal1 = BigDecimal.ZERO;
        }
        if (bigDecimal2 == null) {
            bigDecimal2 = BigDecimal.ZERO;
        }
        return bigDecimal1.subtract(bigDecimal2);
    }


    @Override
    public void initComeAndGo(String client, String stoCode, String startDate, String endDate) {
        if (StringUtils.isEmpty(client)) {
            TokenUser user = feignService.getLoginInfo();
            client = user.getClient();
        }
        ArrayList<String> stoCodeList = new ArrayList<>();
        if (StringUtils.isEmpty(stoCode)) {
            // 如果未指定门店，则计算所有门店
            stoCodeList = gaiaBillOfParticularsMapper.queryAllStoCode(client);
        } else {
            stoCodeList.add(stoCode);
        }
        if (stoCodeList.size() > 0) {
            for (String code : stoCodeList) {
                // 通过明细汇总余额
                BigDecimal sum = gaiaBillOfParticularsMapper.sumInitComeAndGo(client, code, startDate, endDate);
                // 更新门店余额
                if (ObjectUtils.isNotEmpty(sum)) {
                    // 将余额合计到GAIA_STORE_DATA
                    gaiaStoreDataMapper.initRemainingSum(client, code, sum);
                }
            }
        }
    }
}
