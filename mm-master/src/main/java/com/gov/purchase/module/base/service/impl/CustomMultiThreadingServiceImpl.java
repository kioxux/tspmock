package com.gov.purchase.module.base.service.impl;

import com.alibaba.fastjson.JSON;
import com.gov.purchase.common.entity.SmsEntity;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.utils.SmsUtils;
import com.gov.purchase.constants.CommonConstants;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.mapper.GaiaSdExasearchDMapper;
import com.gov.purchase.mapper.GaiaSdMarketingSearchMapper;
import com.gov.purchase.mapper.GaiaSdMemberCardMapper;
import com.gov.purchase.mapper.GaiaSmsTemplateMapper;
import com.gov.purchase.module.base.dto.GetMemberListDTO;
import com.gov.purchase.module.base.dto.MemberDTO;
import com.gov.purchase.module.base.service.CustomMultiThreadingService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CustomMultiThreadingServiceImpl implements CustomMultiThreadingService {

    @Resource
    private GaiaSmsTemplateMapper gaiaSmsTemplateMapper;
    @Resource
    private GaiaSdMemberCardMapper gaiaSdMemberCardMapper;
    @Resource
    private GaiaSdExasearchDMapper gaiaSdExasearchDMapper;
    @Resource
    private GaiaSdMarketingSearchMapper gaiaSdMarketingSearchMapper;
    @Resource
    private SmsUtils smsUtils;

    /**
     * 营销任务审批通过 发送短信到门店
     * @param storeList 门店列表
     * @param marketing 营销活动
     */
    @Async
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void sendMarketTaskSms(List<GaiaStoreData> storeList, GaiaSdMarketing marketing) {
        if (CollectionUtils.isEmpty(storeList) || ObjectUtils.isEmpty(marketing)) {
            return;
        }
        // 1,查询短信模板
        String gsmTempid = marketing.getGsmTempid();
        if (StringUtils.isEmpty(gsmTempid)) {
            return;
        }
        GaiaSmsTemplate smsTemplate = gaiaSmsTemplateMapper.selectByPrimaryKey(gsmTempid);
        if (ObjectUtils.isEmpty(smsTemplate)) {
            return;
        }
        // 2,查询模板门店会员(根据查询条件,以及人数上限)
        storeList.forEach(store->{
            synchronized (CommonConstants.LOCK_SEND_MARKET_TASK_SMS) {
                List<GaiaSdExasearchD> list = new ArrayList<>();
                // (1)构建查询条件
                GetMemberListDTO getMemberListDTO = null;
                try {
                    GaiaSdMarketingSearch gaiaSdMarketingSearch = gaiaSdMarketingSearchMapper.selectByPrimaryKey(marketing.getGsmSmscondi());
                    getMemberListDTO = JSON.parseObject(gaiaSdMarketingSearch.getGsmsDetail(), GetMemberListDTO.class);
                    getMemberListDTO.setGsmsId(marketing.getGsmSmscondi());
                    int i = marketing.getGsmSmstimes().intValue();
                    getMemberListDTO.setGsmSmstimes(i);
                } catch (Exception e) {
                    log.info("获取会员列表失败",e.getMessage());
                }

                // (2)查询会员
                List<MemberDTO> memberList = gaiaSdMemberCardMapper.getMemberList(getMemberListDTO);
                if (CollectionUtils.isEmpty(memberList)) {
                    return;
                }
                memberList.forEach(member -> {
                    try {
                        // 签名
                        String sign = null;
                        if (StringUtils.isNotEmpty(store.getStoName())) {
                            if (store.getStoName().length() < 20) {
                                sign = splicing(store.getStoName());
                            } else {
                                sign = splicing(store.getStoName().substring(0, 20));
                            }
                        } else {
                            sign = splicing(store.getStoShortName());
                        }

                        // 短息内容
                        String msgTemp = smsTemplate.getSmsContent().replaceFirst(CommonConstants.SMS_COMTENT_PD_REGEX, sign)
                                .replace(CommonConstants.SMS_COMTENT_PD_NAME, splicing(member.getGmbName()))
                                .replace(CommonConstants.SMS_COMTENT_PD_SCOPE, splicing(member.getGmbIntegral()));

                        String gseCretime = DateUtils.getCurrentDateTimeStrYMDHMS();
                        Result result = null;
                        try {
                            SmsEntity smsEntity = new SmsEntity();
                            smsEntity.setSmsId(smsTemplate.getSmsTemplateId());
                            smsEntity.setPhone(member.getGmbMobile());
                            smsEntity.setMsg(msgTemp);
                            result = smsUtils.sendMarketingSms(smsEntity);
                        } catch (Exception e) {
                            log.info(e.getMessage());
                        }
                        String gseSendtime = DateUtils.getCurrentDateTimeStrYMDHMS();

                        GaiaSdExasearchD sdExasearchD = new GaiaSdExasearchD();
                        list.add(sdExasearchD);
                        // 0-发送成功, 1-发送失败
                        if (!ObjectUtils.isEmpty(result) && ResultEnum.SUCCESS.getCode().equals(result.getCode())) {
                            sdExasearchD.setGseStatus("0");
                        } else {
                            sdExasearchD.setGseStatus("1");
                        }
                        // 加盟商 /当前加盟商
                        sdExasearchD.setClient(marketing.getClient());
                        // 营销活动id
                        sdExasearchD.setGseMarketid(marketing.getGsmMarketid());
                        // 查询id /年月日时分秒
                        sdExasearchD.setGseSearchid(DateUtils.getCurrentDateTimeStrYMDHMS());
                        // 查询主题 /常用查询id
                        sdExasearchD.setGseTheid(marketing.getGsmSmscondi());
                        // 会员卡号
                        sdExasearchD.setGseCardId(member.getGmbCardId());
                        // 会员姓名
                        sdExasearchD.setGseMenname(member.getGmbName());
                        // 会员电话
                        sdExasearchD.setGseMobile(member.getGmbMobile());
                        // 性别
                        sdExasearchD.setGseSex(member.getGmbSex());
                        // 年龄
                        sdExasearchD.setGseAge(member.getGmbAge());
                        // 生日
                        sdExasearchD.setGseBirth(member.getGmbBirth());
                        // 积分
                        sdExasearchD.setGseIntegral(new BigDecimal(member.getGmbIntegral()));
                        // 所属门店编码
                        sdExasearchD.setGseBrId(store.getStoCode());
                        // 所属门店名称
                        sdExasearchD.setGseBrName(store.getStoName());
                        // 会员卡类型
                        sdExasearchD.setGseClassId(member.getGsmbcClassId());
//                         最后积分日期
                        sdExasearchD.setGseIntegralLastdate(member.getGsmbcIntegralLastdate());
                        // 会员级别
                        sdExasearchD.setGseGlid("");
                        // 会员微信
                        sdExasearchD.setGseWxid("");
                        // 是否发送短信 /
                        sdExasearchD.setGseSms("0");
                        // 是否发送微信 /
                        sdExasearchD.setGseWx("1");
                        // 类型 /1-短信, 2-电话
                        sdExasearchD.setGseNoticeType("1");
                        // 信息创建时间
                        sdExasearchD.setGseCretime(gseCretime);
                        // 信息发送时间
                        sdExasearchD.setGseSendtime(gseSendtime);
                        // 短信模板ID
                        sdExasearchD.setGseTempid(smsTemplate.getSmsId());
                    } catch (Exception e) {
                        log.info(e.getMessage());
                    }

                });
                gaiaSdExasearchDMapper.saveBatch(list);
                try {
                    // 线程等待1秒，
                    // 1>减少主键冲突
                    // 2>相同手机号 短信发送频率太高会失败
                    Thread.currentThread().sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String splicing(String source) {
        return "【" + source + "】";
    }

}
