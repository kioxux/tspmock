package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.mapper.GaiaProductBasicMapper;
import com.gov.purchase.module.base.dto.GetProductTranslationListDTO;
import com.gov.purchase.module.base.dto.businessImport.PurchasePo;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.purchase.dto.PoItemListDto;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author zhoushuai
 * @date 2021/5/7 11:12
 */
@Service
public class PurchasePoImport extends BusinessImport {

    @Resource
    FeignService feignService;
    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;

    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> param) {
        List<String> errorList = new ArrayList<>();
        String client = feignService.getLoginInfo().getClient();
        String proSite = (String) param.get("proSite");
        if (StringUtils.isBlank(proSite)) {
            errorList.add("采购主体不能为空");
        }
        Set<String> proSelfCodeSet = new HashSet<>();
        for (Integer key : map.keySet()) {
            PurchasePo purchasePo = (PurchasePo) map.get(key);
            proSelfCodeSet.add(purchasePo.getPoProCode());
        }
        // 导入的商品编码列表
        List<String> proSelfCodeList = gaiaProductBasicMapper.getDistinctSiteList(client, proSite, new ArrayList<String>(proSelfCodeSet));
        List<PurchasePo> purchasePoList = new ArrayList<>();
        for (Integer key : map.keySet()) {
            PurchasePo purchasePo = (PurchasePo) map.get(key);
            if (!proSelfCodeList.contains(purchasePo.getPoProCode())) {
                errorList.add(MessageFormat.format("第{0}行，商品编码不存在", key + 1));
            }
            purchasePoList.add(purchasePo);
        }
        if (!CollectionUtils.isEmpty(errorList)) {
            Result result = new Result();
            result.setCode(ResultEnum.E0115.getCode());
            result.setData(errorList);
            return result;
        }

        List<GetProductTranslationListDTO> proListForPurchasePo = gaiaProductBasicMapper.getProListForPurchasePo(client, proSite, proSelfCodeList);
        Map<String, GetProductTranslationListDTO> proDetailMap = proListForPurchasePo.stream().collect(Collectors.toMap(GetProductTranslationListDTO::getLabel, item -> item));
        AtomicInteger lineNo = new AtomicInteger(1);
        List<PoItemListDto> poItemListDtoList = purchasePoList.stream().map(purchasePo -> {
            GetProductTranslationListDTO pro = proDetailMap.get(purchasePo.getPoProCode());
            PoItemListDto poItemListDto = new PoItemListDto();
            poItemListDto.setPoLineNo(String.valueOf(lineNo.getAndIncrement()));
            poItemListDto.setPoProCode(pro.getLabel());
            poItemListDto.setPoProName(pro.getValue());
            poItemListDto.setProSpecs(pro.getProSpecs());
            poItemListDto.setProFactoryCode(pro.getProFactoryCode());
            poItemListDto.setProFactoryName(pro.getProFactoryName());
            poItemListDto.setProPlace(pro.getProPlace());
            poItemListDto.setPoQty(new BigDecimal(purchasePo.getPoQty()));
            poItemListDto.setPoUnit(pro.getProUnit());
            poItemListDto.setPoUnitName(pro.getProUnit());
            poItemListDto.setPoPrice(new BigDecimal(purchasePo.getPoPrice()));
            poItemListDto.setPoRate(pro.getProInputTax());
            poItemListDto.setPoRateName(pro.getTaxCodeName());
            poItemListDto.setPoLineTotalaMount(poItemListDto.getPoQty().multiply(poItemListDto.getPoPrice()));
            poItemListDto.setPoSiteCode(pro.getProSite());
            poItemListDto.setPoSiteName(pro.getStoName());
            // 库位默认 “1000”
            poItemListDto.setPoLocationCode("1000");
            poItemListDto.setProStorageCondition(pro.getProStorageCondition());
            poItemListDto.setPoBatch("");
            poItemListDto.setPoDeliveryDate("");
            poItemListDto.setPoLineRemark("");
            poItemListDto.setPoLineDelete("0");
            poItemListDto.setPoCompleteFlag("0");
            poItemListDto.setPoDeliveredQty(null);
            poItemListDto.setPoInvoiceQty(null);
            poItemListDto.setPoDeliveredAmt(null);
            poItemListDto.setPoInvoiceAmt(null);
            poItemListDto.setPoBatchNo(purchasePo.getPoBatchNo());
            poItemListDto.setPoScrq(purchasePo.getPoScrq());
            poItemListDto.setPoYxq(purchasePo.getPoYxq());
            poItemListDto.setProductList(Collections.singletonList(pro));
            poItemListDto.setWmKcsl(pro.getWmKcsl());
            poItemListDto.setLastPoPrice(pro.getLastPoPrice());
            return poItemListDto;
        }).collect(Collectors.toList());

        return ResultUtil.success(poItemListDtoList);
    }
}
