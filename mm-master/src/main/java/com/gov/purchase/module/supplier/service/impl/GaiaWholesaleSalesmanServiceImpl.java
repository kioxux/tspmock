package com.gov.purchase.module.supplier.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaUserData;
import com.gov.purchase.entity.GaiaWholesaleSalesman;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaSupplierSalesmanMapper;
import com.gov.purchase.mapper.GaiaUserDataMapper;
import com.gov.purchase.mapper.GaiaWholesaleSalesmanMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.supplier.dto.GaiaSupplierSalesmanDTO;
import com.gov.purchase.module.supplier.dto.WholesaleSalesVO;
import com.gov.purchase.module.supplier.service.GaiaWholesaleSalesmanService;
import com.gov.purchase.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class GaiaWholesaleSalesmanServiceImpl implements GaiaWholesaleSalesmanService {


    @Resource
    GaiaUserDataMapper gaiaUserDataMapper;
    @Resource
    FeignService feignService;
    @Resource
    GaiaWholesaleSalesmanMapper gaiaWholesaleSalesmanMapper;
    @Resource
    GaiaSupplierSalesmanMapper gaiaSupplierSalesmanMapper;

    @Override
    public List<GaiaUserData> getSalesList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaUserDataMapper.selectByClient(user.getClient());
    }

    @Override
    public List<GaiaSupplierSalesmanDTO> getSalesManList(String supSite) {
        TokenUser user = feignService.getLoginInfo();
        return gaiaSupplierSalesmanMapper.getSupplierSalesmanList(user.getClient(), supSite, null);
    }

    @Override
    public PageInfo<WholesaleSalesVO> getWholesaleSalesList(Integer pageSize, Integer pageNum, String userId, String salesManId) {
        TokenUser user = feignService.getLoginInfo();
        if (pageSize != null && pageNum != null) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<WholesaleSalesVO> list = gaiaWholesaleSalesmanMapper.getWholesaleSalesList(user.getClient(), userId, salesManId);
        PageInfo<WholesaleSalesVO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    @Transactional
    public Result deleteWholesaleSales(Long id) {
        gaiaWholesaleSalesmanMapper.deleteByPrimaryKey(id);
        return ResultUtil.success("删除成功");
    }

    @Override
    public Result saveWholesaleSales(GaiaWholesaleSalesman vo) {
        TokenUser user = feignService.getLoginInfo();
        vo.setClient(user.getClient());
        if (vo.getGssName() == null) {
            vo.setGssName("");
        }
        vo.setGwsCreateUser(user.getUserId());
        vo.setGwsCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        vo.setGwsCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        gaiaWholesaleSalesmanMapper.insert(vo);
        return ResultUtil.success("保存成功");
    }
}
