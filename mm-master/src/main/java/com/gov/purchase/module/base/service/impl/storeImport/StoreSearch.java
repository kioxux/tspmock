package com.gov.purchase.module.base.service.impl.storeImport;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.mapper.GaiaFranchiseeMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.BatSearchValBasic;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.ProductDto;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.base.dto.excel.StoreExport;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.18
 */
@Service
public class StoreSearch {

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Resource
    CosUtils cosUtils;

    /**
     * 门店查询
     *
     * @param batSearchValBasic
     * @return
     */
    public Result selectStoreBatch(BatSearchValBasic batSearchValBasic) {
        // 查询条件
        Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
        initStoreWhere(map);
        PageHelper.startPage(batSearchValBasic.getPageNum(), batSearchValBasic.getPageSize());
        List<StoreExport> batchList = gaiaStoreDataMapper.selectStoreBatch(map);
        PageInfo<StoreExport> pageInfo = new PageInfo<>(batchList);

        return ResultUtil.success(pageInfo);
    }

    /**
     * 加载查询条件
     *
     * @param map
     */
    private void initStoreWhere(Map<String, SearchValBasic> map) {
        // 查询条件为空
        if (map == null || map.isEmpty()) {
            return;
        }

        // 加盟商
        SearchValBasic client = map.get("a");
        if (client != null) {

            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(client.getSearchVal()) || CollectionUtils.isNotEmpty(client.getMoreVal())) {
                list.addAll(gaiaFranchiseeMapper.getFrancName(client));
                if (StringUtils.isNotBlank(client.getSearchVal())) {
                    list.add(client.getSearchVal());
                } else {
                    list.addAll(client.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                client.setSearchVal(null);
            }
            client.setMoreVal(list);
        }

        // 门店属性
        SearchValBasic sto_attribute = map.get("d");
        if (sto_attribute != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(sto_attribute.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.STO_ATTRIBUTE, sto_attribute.getSearchVal()));
                list.add(sto_attribute.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(sto_attribute.getMoreVal())) {
                for (String item : sto_attribute.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.STO_ATTRIBUTE, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                sto_attribute.setSearchVal(null);
            }
            sto_attribute.setMoreVal(list);
        }

        // 配送方式
        SearchValBasic sto_delivery_mode = map.get("e");
        if (sto_delivery_mode != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(sto_delivery_mode.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.STO_DELIVERY_MODE, sto_delivery_mode.getSearchVal()));
                list.add(sto_delivery_mode.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(sto_delivery_mode.getMoreVal())) {
                for (String item : sto_delivery_mode.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.STO_DELIVERY_MODE, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                sto_delivery_mode.setSearchVal(null);
            }
            sto_delivery_mode.setMoreVal(list);
        }

        // 门店状态
        SearchValBasic sto_status = map.get("f");
        if (sto_status != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(sto_status.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.STO_STATUS, sto_status.getSearchVal()));
                list.add(sto_status.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(sto_status.getMoreVal())) {
                for (String item : sto_status.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.STO_STATUS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                sto_status.setSearchVal(null);
            }
            sto_status.setMoreVal(list);
        }

        // 是否医保店
        SearchValBasic sto_if_medicalcare = map.get("g");
        if (sto_if_medicalcare != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(sto_if_medicalcare.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.NO_YES, sto_if_medicalcare.getSearchVal()));
                list.add(sto_if_medicalcare.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(sto_if_medicalcare.getMoreVal())) {
                for (String item : sto_if_medicalcare.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.NO_YES, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                sto_if_medicalcare.setSearchVal(null);
            }
            sto_if_medicalcare.setMoreVal(list);
        }

        // 税分类
        SearchValBasic sto_tax_class = map.get("h");
        if (sto_tax_class != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(sto_tax_class.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.STO_TAX_CLASS, sto_tax_class.getSearchVal()));
                list.add(sto_tax_class.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(sto_tax_class.getMoreVal())) {
                for (String item : sto_tax_class.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.STO_TAX_CLASS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                sto_tax_class.setSearchVal(null);
            }
            sto_tax_class.setMoreVal(list);
        }
    }

    /**
     * 门店导出
     * @param batSearchValBasic
     * @return
     */
    public Result exportStoreBatch(BatSearchValBasic batSearchValBasic) {
        try {
            // 查询条件
            Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
            initStoreWhere(map);
            List<StoreExport> batchList = gaiaStoreDataMapper.selectStoreBatch(map);

            // 导出数据
            List<List<String>> basicListc = new ArrayList<>();
            initData(batchList, basicListc, null);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(basicHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(basicListc);
                    }},
                    new ArrayList<String>() {{
                        add("门店主数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param basicListc
     * @param businessList
     */
    private void initData(List<StoreExport> batchList, List<List<String>> basicListc, List<List<String>> businessList) {
        for (StoreExport storeExport : batchList) {
            if (basicListc != null) {
                List<String> basicData = new ArrayList<>();
                //加盟商
                basicData.add(storeExport.getClient());
                //门店编码
                basicData.add(storeExport.getStoCode());
                //门店名称
                basicData.add(storeExport.getStoName());
                //助记码
                basicData.add(storeExport.getStoPym());
                //门店简称
                basicData.add(storeExport.getStoShortName());
                //门店属性
                String sto_attribute = storeExport.getStoAttribute();
                initDictionaryStaticData(basicData, sto_attribute, CommonEnum.DictionaryStaticData.STO_ATTRIBUTE);
                //配送方式
                String sto_delivery_mode = storeExport.getStoDeliveryMode();
                initDictionaryStaticData(basicData, sto_delivery_mode, CommonEnum.DictionaryStaticData.STO_DELIVERY_MODE);
                //关联门店
                basicData.add(storeExport.getStoRelationStoreName());
                //门店状态
                String sto_status = storeExport.getStoStatus();
                initDictionaryStaticData(basicData, sto_status, CommonEnum.DictionaryStaticData.STO_STATUS);
                //经营面积
                basicData.add(storeExport.getStoArea());
                //开业日期
                basicData.add(storeExport.getStoOpenDate());
                //关店日期
                basicData.add(storeExport.getStoCloseDate());
                //详细地址
                basicData.add(storeExport.getStoAdd());
                //省
                basicData.add(storeExport.getStoProvinceName());
                //城市
                basicData.add(storeExport.getStoCityName());
                //区/县
                basicData.add(storeExport.getStoDistrictName());
                //是否医保店
                String sto_if_medicalcare = storeExport.getStoIfMedicalcare();
                initDictionaryStaticData(basicData, sto_if_medicalcare, CommonEnum.DictionaryStaticData.NO_YES);
                //DTP
                String sto_if_dtp = storeExport.getStoIfDtp();
                initDictionaryStaticData(basicData, sto_if_dtp, CommonEnum.DictionaryStaticData.NO_YES);
                //税分类
                String sto_tax_class  = storeExport.getStoTaxClass();
                initDictionaryStaticData(basicData, sto_tax_class, CommonEnum.DictionaryStaticData.STO_TAX_CLASS);
                //委托配送公司
                basicData.add(storeExport.getDecName());
                //连锁总部
                basicData.add(storeExport.getStoChainHeadName());
                //纳税主体
                basicData.add(storeExport.getStoTaxSubjectName());
                //税率
                basicData.add(storeExport.getStoTaxRate());
                basicListc.add(basicData);
            }
        }
    }

    /**
     * 查询数据静态字典数据处理
     * @param list
     * @param value
     */
    private void initDictionaryStaticData(List<String> list, String value, CommonEnum.DictionaryStaticData dictionaryStaticData) {
        if (StringUtils.isBlank(value)) {
            list.add("");
        } else {
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, value);
            if (dictionary != null) {
                list.add(dictionary.getLabel());
            } else {
                list.add("");
            }
        }
    }


    /**
     * 基础数据头
     */
    private String[] basicHead = {
            "加盟商",
            "门店编码",
            "门店名称",
            "助记码",
            "门店简称",
            "门店属性",
            "配送方式",
            "关联门店",
            "门店状态",
            "经营面积",
            "开业日期",
            "关店日期",
            "详细地址",
            "省",
            "城市",
            "区/县",
            "是否医保店",
            "DTP",
            "税分类",
            "委托配送公司",
            "连锁总部",
            "纳税主体",
            "税率"
    };
//门店	4	加盟商	2	加盟商	a
//				门店编码	b
//				门店名称	c
//				门店属性	d
//				配送方式	e
//				门店状态	f
//				是否医保店	g
//				税分类	h
//				连锁总部	i
}
