package com.gov.purchase.module.purchase.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.purchase.dto.*;

import java.util.List;

public interface IDirectPurchaseService {

    List<DeliveryStoreResponseDto> selectStore();

    List<DirectPurchaseListVO> selectList(DirectPurchaseListDTO directPurchaseListDTO);

    void save(List<DirectPurchaseSaveDTO> voList);

    List<DirectPurchaseSummaryListVO> selectSummaryList(DirectPurchaseListDTO directPurchaseListDTO);

    Result export(List<DirectPurchaseSaveDTO> voList);
}
