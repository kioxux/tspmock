package com.gov.purchase.module.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.WmsFeign;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.mapper.GaiaUserDataMapper;
import com.gov.purchase.module.base.service.BaseService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.goods.dto.QueryProRequestDto;
import com.gov.purchase.module.goods.dto.QueryProResponseDto;
import com.gov.purchase.module.goods.dto.QueryUnDrugCheckRequestDto;
import com.gov.purchase.module.goods.service.EditService;
import com.gov.purchase.module.goods.service.GrugService;
import com.gov.purchase.module.replenishment.dto.StoreRequestDto;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EditServiceImpl implements EditService {

    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    FeignService feignService;
    @Resource
    private GrugService grugService;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    OperationFeignService operationFeignService;
    @Resource
    BaseService baseService;

    /**
     * 商品列表
     *
     * @param dto QueryProRequestDto
     * @return PageInfo<QueryProResponseDto>
     */
    public PageInfo<QueryProResponseDto> selectQueryProInfo(QueryProRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        // 设置当前登录人的加盟商编号
        dto.setClient(user.getClient());
        //分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<QueryProResponseDto> QueryProList = gaiaProductBusinessMapper.selectQueryProInfo(dto);
        PageInfo<QueryProResponseDto> pageInfo = new PageInfo<>(QueryProList);
        return pageInfo;
    }

    /**
     * 商品维护
     *
     * @param dto QueryUnDrugCheckRequestDto
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> updateProEditInfo(QueryUnDrugCheckRequestDto dto) {
        Map<String, Object> result = new HashMap<>();
        //非药新建校验
        grugService.checkUnDrugCheckInfo(dto);
        GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
        //加盟商
        gaiaProductBusinessKey.setClient(dto.getClient());
        //地点
        gaiaProductBusinessKey.setProSite(dto.getProSite());
        //商品自编码
        gaiaProductBusinessKey.setProSelfCode(dto.getProSelfCode());
        GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
        String proFixBin = "";
        if (gaiaProductBusiness == null) {
            throw new CustomResultException(ResultEnum.E0161);
        }
        proFixBin = gaiaProductBusiness.getProFixBin();
        BeanUtils.copyProperties(dto, gaiaProductBusiness);
        // 商品描述
        if (org.apache.commons.lang3.StringUtils.isBlank(gaiaProductBusiness.getProDepict())) {
            gaiaProductBusiness.setProDepict("(".concat(dto.getProName()).concat(")").concat(dto.getProCommonname()));
        }
        // 中包装量
        gaiaProductBusiness.setProMidPackage(StringUtils.isBlank(dto.getProMidPackage()) ? "1" : dto.getProMidPackage());
        // 固定货位
        gaiaProductBusiness.setProFixBin(StringUtils.isBlank(dto.getProFixBin()) ? dto.getProSelfCode() : dto.getProFixBin());
        // 参考毛利率按照零售、采购价自动计算存入，公式：（参考售价-参考进价）/参考售价*100%（若参考售价为空或零，直接赋值0）
        // 参考进货价
        BigDecimal pro_cgj = dto.getProCgj() == null ? BigDecimal.ZERO : dto.getProCgj();
        // 参考零售价
        BigDecimal pro_lsj = dto.getProLsj() == null ? BigDecimal.ZERO : dto.getProLsj();
        // 参考毛利率
        BigDecimal pro_mll = BigDecimal.ZERO;
        // 若参考售价为空或零，直接赋值0
        if (pro_lsj.compareTo(BigDecimal.ZERO) != 0) {
            // （参考售价-参考进价）/参考售价*100%
            pro_mll = (pro_lsj.subtract(pro_cgj)).divide(pro_lsj, 4, BigDecimal.ROUND_HALF_UP);
        }
        gaiaProductBusiness.setProMll(pro_mll);
        if (StringUtils.isBlank(gaiaProductBusiness.getProCompclass())) {
            gaiaProductBusiness.setProCompclass("N999999");
            gaiaProductBusiness.setProCompclassName("N999999未匹配");
        }
        gaiaProductBusinessMapper.updateByPrimaryKey(gaiaProductBusiness);
        if (!StringUtils.equals(proFixBin, gaiaProductBusiness.getProFixBin())) {
            TokenUser user = feignService.getLoginInfo();
            baseService.updateProFixBin(gaiaProductBusiness.getClient(), gaiaProductBusiness.getProSite(), user.getUserId(),
                    gaiaProductBusiness.getProSelfCode(), proFixBin, gaiaProductBusiness.getProFixBin());
        }

        result.put("client", gaiaProductBusiness.getClient());
        result.put("proSiteList", Collections.singletonList(gaiaProductBusiness.getProSite()));
        result.put("proSelfCodeList", Collections.singletonList(gaiaProductBusiness.getProSelfCode()));

        // 配送中心判断
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(gaiaProductBusiness.getClient());
        gaiaDcDataKey.setDcCode(gaiaProductBusiness.getProSite());
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
        if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcChainHead())) {
            StoreRequestDto entity = new StoreRequestDto();
            entity.setClient(gaiaProductBusiness.getClient());
            entity.setStoChainHead(gaiaProductBusiness.getProSite());
            List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.getStoreListByDc(entity);
            for (GaiaStoreData storeData : storeDataList) {
                gaiaProductBusiness.setProSite(storeData.getStoCode());
                gaiaProductBusinessMapper.updateByPrimaryKey(gaiaProductBusiness);
                if (!StringUtils.equals(proFixBin, gaiaProductBusiness.getProFixBin())) {
                    TokenUser user = feignService.getLoginInfo();
                    baseService.updateProFixBin(gaiaProductBusiness.getClient(), gaiaProductBusiness.getProSite(), user.getUserId(),
                            gaiaProductBusiness.getProSelfCode(), proFixBin, gaiaProductBusiness.getProFixBin());
                }
            }
        }

        return result;
    }

    /**
     * 商品详情
     *
     * @param client      加盟商
     * @param proSite     地点
     * @param proSelfCode 自编码
     * @return
     */
    @Override
    public Result queryBusinessPor(String client, String proSite, String proSelfCode) {
        GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
        //加盟商
        gaiaProductBusinessKey.setClient(client);
        //地点
        gaiaProductBusinessKey.setProSite(proSite);
        //商品自编码
        gaiaProductBusinessKey.setProSelfCode(proSelfCode);
        return ResultUtil.success(gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey));
    }
}
