package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaSdReplenishD;
import com.gov.purchase.entity.GaiaSdReplenishH;
import lombok.Data;

import java.util.List;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/28 19:17
 **/
@Data
public class AutoNeedProDTO {
    private List<GaiaSdReplenishH> replenishHList;
    private List<GaiaSdReplenishD> replenishDList;
    private Result result;
}
