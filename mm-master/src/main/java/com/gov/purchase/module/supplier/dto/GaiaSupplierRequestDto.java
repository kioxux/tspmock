package com.gov.purchase.module.supplier.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "供应商首营详情传入参数")
public class GaiaSupplierRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 供应商自编码
     */
    @ApiModelProperty(value = "供应商自编码", name = "supSelfCode", required = true)
    @NotBlank(message = "供应商自编码不能为空")
    private String supSelfCode;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "supSite", required = true)
    @NotBlank(message = "地点不能为空")
    private String supSite;

    /**
     * 供应商编码
     */
    @ApiModelProperty(value = "供应商编码", name = "supCode", required = true)
    private String supCode;

    @ApiModelProperty(value = "首营流程编号", name = "supFlowNo", required = true)
    private String supFlowNo;

}