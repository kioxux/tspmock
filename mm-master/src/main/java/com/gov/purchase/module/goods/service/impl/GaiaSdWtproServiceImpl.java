package com.gov.purchase.module.goods.service.impl;

import com.gov.purchase.entity.GaiaSdWtpro;
import com.gov.purchase.mapper.GaiaSdWtproMapper;
import com.gov.purchase.module.goods.service.GaiaSdWtproService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 委托配送第三方商品编码对照表(GaiaSdWtpro)表服务实现类
 *
 * @author makejava
 * @since 2021-08-13 15:53:55
 */
@Service("gaiaSdWtproService")
public class GaiaSdWtproServiceImpl implements GaiaSdWtproService {
    @Resource
    private GaiaSdWtproMapper gaiaSdWtproDao;


    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<GaiaSdWtpro> queryAllByLimit(int offset, int limit) {
        return this.gaiaSdWtproDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param gaiaSdWtpro 实例对象
     * @return 实例对象
     */
    @Override
    public GaiaSdWtpro insert(GaiaSdWtpro gaiaSdWtpro) {
        this.gaiaSdWtproDao.insert(gaiaSdWtpro);
        return gaiaSdWtpro;
    }


    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.gaiaSdWtproDao.deleteById(id) > 0;
    }
}
