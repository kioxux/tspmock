package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.businessImport.AllotPriceCus;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.29
 */
@Service
public class AllotPriceCusImport extends BusinessImport {

    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaAllotPriceCusMapper gaiaAllotPriceCusMapper;
    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;

    /**
     * 导入
     *
     * @param map   数据
     * @param field 字段
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        //错误LIST
        List<String> errorList = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        // 行数据验证
        for (Integer key : map.keySet()) {
            // 行数据
            AllotPriceCus allotPriceCus = (AllotPriceCus) map.get(key);
            int priceCount = 0;
            if (StringUtils.isNotBlank(allotPriceCus.getAlpAddAmt())) {
                priceCount++;
            }
            if (StringUtils.isNotBlank(allotPriceCus.getAlpAddRate())) {
                priceCount++;
            }
            if (StringUtils.isNotBlank(allotPriceCus.getAlpCataloguePrice())) {
                if (StringUtils.isBlank(allotPriceCus.getAlpProCode())) {
                    errorList.add(MessageFormat.format("第{0}行：选填目录价时，商品编码不能为空", key + 1));
                }
                priceCount++;
            }
            if (priceCount != 1) {
                errorList.add(MessageFormat.format("第{0}行：[加点比例]、[加点金额]、[目录价]必填一个,且只能填一个", key + 1));
            }

            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(user.getClient());
            gaiaDcDataKey.setDcCode(allotPriceCus.getAlpReceiveSite());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            if (gaiaDcData == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "收货地点"));
                continue;
            }

            GaiaCustomerBusinessKey customerBusinessKey = new GaiaCustomerBusinessKey();
            customerBusinessKey.setClient(user.getClient());
            customerBusinessKey.setCusSite(allotPriceCus.getAlpReceiveSite());
            customerBusinessKey.setCusSelfCode(allotPriceCus.getAlpCusCode());
            GaiaCustomerBusiness gaiaCustomerBusiness = gaiaCustomerBusinessMapper.selectByPrimaryKey(customerBusinessKey);
            if (gaiaCustomerBusiness == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "客户"));
                continue;
            }
            if (StringUtils.isNotBlank(allotPriceCus.getAlpProCode())) {
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                gaiaProductBusinessKey.setClient(user.getClient());
                gaiaProductBusinessKey.setProSite(allotPriceCus.getAlpReceiveSite());
                gaiaProductBusinessKey.setProSelfCode(allotPriceCus.getAlpProCode());
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                if (gaiaProductBusiness == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品编码"));
                    continue;
                }
            }
        }
        // 验证不通过
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }

        for (Integer key : map.keySet()) {
            // 行数据
            AllotPriceCus allotPriceCus = (AllotPriceCus) map.get(key);
            GaiaAllotPriceCus gaiaAllotPriceCus = null;
            if (StringUtils.isBlank(allotPriceCus.getAlpProCode())) {
                gaiaAllotPriceCus = gaiaAllotPriceCusMapper.selectAllotPriceCusBySite(user.getClient(),
                        allotPriceCus.getAlpReceiveSite(), allotPriceCus.getAlpCusCode());
            } else {
                gaiaAllotPriceCus = gaiaAllotPriceCusMapper.selectAllotPriceCusByPro(user.getClient(),
                        allotPriceCus.getAlpReceiveSite(), allotPriceCus.getAlpCusCode(), allotPriceCus.getAlpProCode());
            }
            if (gaiaAllotPriceCus == null) {
                gaiaAllotPriceCus = new GaiaAllotPriceCus();
                // 加盟商
                gaiaAllotPriceCus.setClient(user.getClient());
                // 收货地点
                gaiaAllotPriceCus.setAlpReceiveSite(allotPriceCus.getAlpReceiveSite());
                // 客户
                gaiaAllotPriceCus.setAlpCusCode(allotPriceCus.getAlpCusCode());
                // 商品编码
                gaiaAllotPriceCus.setAlpProCode(allotPriceCus.getAlpProCode());
                // 加价金额
                gaiaAllotPriceCus.setAlpAddAmt(StringUtils.isBlank(allotPriceCus.getAlpAddAmt()) ? null
                        : NumberUtils.toScaledBigDecimal(allotPriceCus.getAlpAddAmt()));
                // 加价比例
                gaiaAllotPriceCus.setAlpAddRate(StringUtils.isBlank(allotPriceCus.getAlpAddRate()) ? null
                        : NumberUtils.toScaledBigDecimal(allotPriceCus.getAlpAddRate()));
                // 目录价
                gaiaAllotPriceCus.setAlpCataloguePrice(StringUtils.isBlank(allotPriceCus.getAlpCataloguePrice()) ? null
                        : NumberUtils.toScaledBigDecimal(allotPriceCus.getAlpCataloguePrice()));
                // 创建人
                gaiaAllotPriceCus.setAlpCreateBy(user.getUserId());
                // 创建日期
                gaiaAllotPriceCus.setAlpCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 创建时间
                gaiaAllotPriceCus.setAlpCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaAllotPriceCusMapper.insertSelective(gaiaAllotPriceCus);
            } else {
                // 加价金额
                gaiaAllotPriceCus.setAlpAddAmt(StringUtils.isBlank(allotPriceCus.getAlpAddAmt()) ? null
                        : NumberUtils.toScaledBigDecimal(allotPriceCus.getAlpAddAmt()));
                // 加价比例
                gaiaAllotPriceCus.setAlpAddRate(StringUtils.isBlank(allotPriceCus.getAlpAddRate()) ? null
                        : NumberUtils.toScaledBigDecimal(allotPriceCus.getAlpAddRate()));
                // 目录价
                gaiaAllotPriceCus.setAlpCataloguePrice(StringUtils.isBlank(allotPriceCus.getAlpCataloguePrice()) ? null
                        : NumberUtils.toScaledBigDecimal(allotPriceCus.getAlpCataloguePrice()));
                // 更新人
                gaiaAllotPriceCus.setAlpUpdateBy(user.getUserId());
                // 更新日期
                gaiaAllotPriceCus.setAlpUpdateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 更新时间
                gaiaAllotPriceCus.setAlpUpdateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaAllotPriceCusMapper.updateByPrimaryKey(gaiaAllotPriceCus);
            }
        }
        return ResultUtil.success();
    }
}
