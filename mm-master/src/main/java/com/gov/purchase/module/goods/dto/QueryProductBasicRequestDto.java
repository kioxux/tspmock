package com.gov.purchase.module.goods.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "药品库搜索传入参数")
public class QueryProductBasicRequestDto extends Pageable {

    @ApiModelProperty(value = "国际条形码", name = "proBarcode")
    private String proBarcode;

    @ApiModelProperty(value = "商品名", name = "proName")
    private String proName;

    @ApiModelProperty(value = "批准文号", name = "proRegisterNo")
    private String proRegisterNo;

    @ApiModelProperty(value = "生产厂家", name = "proFactoryName")
    private String proFactoryName;

    @ApiModelProperty(value = "规格", name = "proSpecs")
    private String proSpecs;

    @ApiModelProperty(value = "国际条形码2", name = "proBarcode2")
    private String proBarcode2;

    @ApiModelProperty(value = "单位", name = "proUnit")
    private String proUnit;

    @ApiModelProperty(value = "状态 “正常”、“停用“", name = "proStatus")
    private String proStatus;

    @ApiModelProperty(value = "是否经营 “是”、“否”", name = "sell")
    private String sell;

    @ApiModelProperty(value = "地点", name = "proSite")
    private String proSite;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;
}
