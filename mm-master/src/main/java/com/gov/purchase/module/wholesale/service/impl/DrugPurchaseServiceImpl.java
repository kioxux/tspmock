package com.gov.purchase.module.wholesale.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaPoItemMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.SalesOrderRecordVO;
import com.gov.purchase.module.wholesale.dto.DrugPurchaseDTO;
import com.gov.purchase.module.wholesale.dto.DrugPurchaseVo;
import com.gov.purchase.module.wholesale.service.DrugPurchaseService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: yzf
 * @create: 2021-11-04 11:24
 */
@Service
public class DrugPurchaseServiceImpl implements DrugPurchaseService {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaPoItemMapper gaiaPoItemMapper;
    @Resource
    CosUtils cosUtils;

    @Override
    public PageInfo<DrugPurchaseVo> queryDrugPurchaseRecord(DrugPurchaseDTO drugPurchaseDTO) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        drugPurchaseDTO.setClient(user.getClient());
        if (StringUtils.isNotBlank(drugPurchaseDTO.getPurchaseDate())) {
            drugPurchaseDTO.setPurchaseDate(drugPurchaseDTO.getPurchaseDate().replace("-", ""));
            drugPurchaseDTO.setStartDate(drugPurchaseDTO.getPurchaseDate().split(",")[0]);
            drugPurchaseDTO.setEndDate(drugPurchaseDTO.getPurchaseDate().split(",")[1]);
        }
        PageHelper.startPage(drugPurchaseDTO.getPageNum(), drugPurchaseDTO.getPageSize());
        // 查询采购明细
        PageInfo<DrugPurchaseVo> pageInfo = new PageInfo<>(gaiaPoItemMapper.queryDrugPurchaseRecord(drugPurchaseDTO));
        // 到货数量：通过加盟商+地点+采购订单+采购订单行号关联  汇总物料凭证数量。
        pageInfo.getList().forEach(
                item -> {
                    item.setPoCount(gaiaPoItemMapper.queryQtyCount(user.getClient(), item.getPoSiteCode(),
                            item.getPoId(), item.getPoLineNo(), item.getPoProCode()));
                }
        );
        return pageInfo;
    }

    @Override
    public Result queryProPurchaseRate(DrugPurchaseDTO drugPurchaseDTO) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        drugPurchaseDTO.setClient(user.getClient());
        // 判断仓库编码非空
        if (StringUtils.isBlank(drugPurchaseDTO.getDcCode())) {
            return ResultUtil.error(ResultEnum.SITE_ERROR);
        }
        // 获取采购员列表
        List<String> lists = gaiaPoItemMapper.queryProPurchaseRate(drugPurchaseDTO);
        return ResultUtil.success(lists);
    }

    @Override
    public Result exportDrugPurchaseRecord(DrugPurchaseDTO drugPurchaseDTO) {
        try {
            // 当前用户信息
            TokenUser user = feignService.getLoginInfo();
            drugPurchaseDTO.setClient(user.getClient());
            if (StringUtils.isNotBlank(drugPurchaseDTO.getPurchaseDate())) {
                drugPurchaseDTO.setPurchaseDate(drugPurchaseDTO.getPurchaseDate().replace("-", ""));
                drugPurchaseDTO.setStartDate(drugPurchaseDTO.getPurchaseDate().split(",")[0]);
                drugPurchaseDTO.setEndDate(drugPurchaseDTO.getPurchaseDate().split(",")[1]);
            }

            List<DrugPurchaseVo> lists = gaiaPoItemMapper.queryDrugPurchaseRecord(drugPurchaseDTO);
            // 到货数量：通过加盟商+地点+采购订单+采购订单行号关联  汇总物料凭证数量。
            lists.forEach(
                    item -> {
                        item.setPoCount(gaiaPoItemMapper.queryQtyCount(user.getClient(), item.getPoSiteCode(),
                                item.getPoId(), item.getPoLineNo(), item.getPoProCode()));
                    }
            );
            List<List<String>> dataList = new ArrayList<>();
            for (int i = 0; CollectionUtils.isNotEmpty(lists) && i < lists.size(); i++) {
                // 明细数据
                DrugPurchaseVo drugPurchaseVo = lists.get(i);
                // 打印行
                List<String> item = new ArrayList<>();
                // 序号
                item.add(String.valueOf(i + 1));
                // 采购日期
                item.add(drugPurchaseVo.getPoCreateDate());
                // 采购订单
                item.add(drugPurchaseVo.getPoId());
                // 供应商编码
                item.add(drugPurchaseVo.getPoSupplierId());
                // 供应商名称
                item.add(drugPurchaseVo.getSupName());
                // 商品编码
                item.add(drugPurchaseVo.getPoProCode());
                // 通用名称
                item.add(drugPurchaseVo.getProCommonName());
                // 规格
                item.add(drugPurchaseVo.getProSpecs());
                // 剂型
                item.add(drugPurchaseVo.getProForm());
                // 批准文号
                item.add(drugPurchaseVo.getProRegisterNo());
                // 生产厂家
                item.add(drugPurchaseVo.getProFactoryName());
                // 产地
                item.add(drugPurchaseVo.getProPlace());
                // 采购单价
                item.add(drugPurchaseVo.getPoPrice() != null ? drugPurchaseVo.getPoPrice().stripTrailingZeros().toPlainString() : "");
                // 订购数量
                item.add(drugPurchaseVo.getPoQty() != null ? drugPurchaseVo.getPoQty().stripTrailingZeros().toPlainString() : "");
                // 单位
                item.add(drugPurchaseVo.getProUnit());
                // 价税合计
                item.add(drugPurchaseVo.getPoTotal() != null ? drugPurchaseVo.getPoTotal().stripTrailingZeros().toPlainString() : "");
                // 到货数量
                item.add(drugPurchaseVo.getPoCount() != null ? drugPurchaseVo.getPoCount().stripTrailingZeros().toPlainString() : "");
                // 上市许可持有人
                item.add(drugPurchaseVo.getProHolder());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("销售记录");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 业务数据表头
     */
    private String[] headList = {
            "序号",
            "采购日期",
            "采购订单",
            "供应商编码",
            "供应商名称",
            "商品编码",
            "通用名称",
            "规格",
            "剂型",
            "批准文号",
            "生产厂家",
            "产地",
            "采购单价",
            "订购数量",
            "单位",
            "价税合计",
            "到货数量",
            "上市许可持有人",
    };
}
