package com.gov.purchase.module.base.dto.businessScope;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetScopeListDTO {

    /**
     * 经营范围编码
     */
    private String jyfwId;
    /**
     * 经营范围描述
     */
    private String jyfwName;
    /**
     * 是否选中（0:不选 1:选中）
     */
    private Integer checked;


}
