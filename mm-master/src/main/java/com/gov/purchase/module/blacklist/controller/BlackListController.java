package com.gov.purchase.module.blacklist.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.blacklist.dto.BlackListRequestDto;
import com.gov.purchase.module.blacklist.service.BlackListService;
import com.gov.purchase.module.customer.dto.CustomerInfoRequestDto;
import com.gov.purchase.module.customer.dto.CustomerInfoSaveRequestDto;
import com.gov.purchase.module.customer.dto.CustomerListRequestDto;
import com.gov.purchase.module.customer.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "批号黑名单")
@RestController
@RequestMapping("blackList")
public class BlackListController {

    @Autowired
    private BlackListService blackListService;


    @Log("批号黑名单保存")
    @ApiOperation("批号黑名单保存")
    @PostMapping("saveBlackList")
    public Result saveCustomerInfo(@RequestBody BlackListRequestDto dto) {
        return ResultUtil.success(blackListService.saveBlackList(dto));
    }

    @Log("批号黑名单-批量删除")
    @ApiOperation("批号黑名单-批量删除")
    @PostMapping("deleteBatchBlackList")
    public Result deleteBatchBlackList(@RequestBody List<BlackListRequestDto> dtoList) {
        return ResultUtil.success(blackListService.deleteBatchBlackList(dtoList));
    }

    @Log("批号黑名单-查询")
    @ApiOperation("批号黑名单-查询")
    @PostMapping("listBlack")
    public Result listBlack(@RequestBody BlackListRequestDto dto) {
        return ResultUtil.success(blackListService.listBlack(dto));
    }

    @Log("批号黑名单-导出")
    @ApiOperation("批号黑名单-导出")
    @PostMapping("exportBlackList")
    public Result exportBlackList(@RequestBody BlackListRequestDto dto) {
        return blackListService.exportBlackList(dto);
    }
}
