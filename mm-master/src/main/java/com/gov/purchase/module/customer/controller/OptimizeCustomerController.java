package com.gov.purchase.module.customer.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaCustomerBusinessKey;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.customer.dto.*;
import com.gov.purchase.module.customer.service.OptimizeCustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "客户")
@RestController
@RequestMapping("customer")
public class OptimizeCustomerController {

    @Autowired
    OptimizeCustomerService optimizeCustomerService;

    @Log("选择机构")
    @ApiOperation("选择机构")
    @GetMapping("getSiteList")
    public Result getSiteList() {
        return ResultUtil.success(optimizeCustomerService.getSiteList());
    }

    @Log("回车查询")
    @ApiOperation("回车查询")
    @PostMapping("enterSelect")
    public Result enterSelect(@RequestJson(value = "cusName", name = "客户名称", required = false) String cusName,
                              @RequestJson(value = "cusCreditCode", name = "社会统一信用代码", required = false) String cusCreditCode) {
        return ResultUtil.success(optimizeCustomerService.enterSelect(cusName, cusCreditCode));
    }

    @Log("保存1")
    @ApiOperation("保存1")
    @PostMapping("saveCusList1")
    public Result saveCusList1(@Valid @RequestBody List<SaveCusList1RequestDTO> list) {
        return ResultUtil.success(optimizeCustomerService.saveCusList1(list));
    }

    @Log("保存2")
    @ApiOperation("保存2")
    @PostMapping("saveCusList2")
    public Result saveCusList2(@Valid @RequestBody SaveCusList2RequestDTO dto) {
        optimizeCustomerService.saveCusList2(dto);
        return ResultUtil.success();
    }

    @Log("客户列表")
    @ApiOperation("客户列表")
    @PostMapping("getCusList")
    public Result getCusList(@Valid @RequestBody GetCusListRequestDTO dto) {
        return ResultUtil.success(optimizeCustomerService.getCusList(dto));
    }

    @Log("导出")
    @ApiOperation("导出")
    @PostMapping("exportCusList")
    public Result exportCusList(@Valid @RequestBody GetCusListRequestDTO dto) {
        return optimizeCustomerService.exportCusList(dto);
    }

    @Log("批量编辑保存")
    @ApiOperation("批量编辑保存")
    @PostMapping("editCusList")
    public Result editCusList(@RequestBody @Valid EditCusListVO vo) {
        optimizeCustomerService.editCusList(vo.getBusinessList(), vo.getRemarks());
        return ResultUtil.success();
    }

    @Log("详情")
    @ApiOperation("详情")
    @PostMapping("getCusDetails")
    public Result getCusDetails(@RequestBody GaiaCustomerBusinessKey key) {
        return ResultUtil.success(optimizeCustomerService.getCusDetails(key));
    }

    @Log("单条数据编辑保存")
    @ApiOperation("单条数据编辑保存")
    @PostMapping("editCus")
    public Result editCus(@RequestBody @Valid EditCusVO vo) {
        optimizeCustomerService.editCus(vo.getBusiness(), vo.getRemarks());
        return ResultUtil.success();
    }

    @Log("客户经营范围编辑")
    @ApiOperation("客户经营范围编辑")
    @PostMapping("editCusScope")
    public Result editCusScope(@RequestBody @Valid EditScopeVO vo) {
        optimizeCustomerService.editCusScope(vo);
        return ResultUtil.success();
    }


}
